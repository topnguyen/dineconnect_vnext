﻿namespace DineConnect.Gateway.Core.Consts
{
    public class GatewayConstants
    {
        public static string DeliveryHeroKey = "DeliveryHeroKey";
        public static string UrbanPiperApiKey = "UrbanPiperApiKey";
        public static string UrbanPiperApiUserName = "UrbanPiperApiUserName";
        public static string UrbanPiperUrl = "UrbanPiperUrl";
        
        public static string DineConnectUrl = "DineConnectUrl";
        public static string DineConnectLocationId = "DineConnectLocationId";
        public static string DineConnectUser = "DineConnectUser";
        public static string DineConnectPassword = "DineConnectPassword";
        public static string DineConnectTenantId = "DineConnectTenantId";
        public static string DineConnectTenantName = "DineConnectTenantName";


    }
}

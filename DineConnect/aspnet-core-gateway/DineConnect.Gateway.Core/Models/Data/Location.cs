﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DineConnect.Gateway.Core.Models.Data
{
   
    [Table("Locations")]
    public class Location : TrackedEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PlatformName { get; set; }
        public Brand Brand { get; set; }
        [ForeignKey("Brand")]
        public int BrandId { get; set; }
        public string Settings { get; set; }
        public string AddOn { get; set; }


    }

}

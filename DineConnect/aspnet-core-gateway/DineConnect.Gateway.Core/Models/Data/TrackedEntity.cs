﻿using System;

namespace DineConnect.Gateway.Core.Models.Data
{
    public class TrackedEntity
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}

﻿namespace DineConnect.Gateway.Core.Models.Data
{
    public class WithAddon
    {
        public string AddOn { get; set; }
    }
}

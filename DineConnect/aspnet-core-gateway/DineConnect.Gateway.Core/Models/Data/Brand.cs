﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DineConnect.Gateway.Core.Models.Data
{
    [Table("Brands")]
    public class Brand : TrackedEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOn { get; set; }
    }

}

﻿namespace DineConnect.Gateway.Core.Models
{
    public class LogFile
    {
        public string Location { get; set; }

        public string FileName
        {
            get
            {
                return System.IO.Path.GetFileName(Location);
            }
        }


        public static LogFile Create(string location)
        {
            return new LogFile() { Location = location };
        }
    }
}

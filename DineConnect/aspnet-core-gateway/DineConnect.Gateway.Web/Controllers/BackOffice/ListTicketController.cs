﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Pagination;
using DineConnect.Gateway.Web.ViewModels.ExternalTicket;
using DineConnect.Gateway.Web.ViewModels.ListTicket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class ListTicketController : Controller
    {
        private IRepository<ExternalDeliveryTicket> _ticketRepo;
        private IRepository<Location> _locationRepository;


        public ListTicketController(IRepository<ExternalDeliveryTicket> ticketRepo,
            IRepository<Location> locationRepo)
        {
            _locationRepository = locationRepo;
            _ticketRepo = ticketRepo;
        }

        [HttpPost]
        public async Task<ActionResult> Index(ListTicketInputViewModel input)
        {
            var response = new ListTicketOutputViewModel(input);
            response.Items = await GetListTickets(response);

            return View(response);
        }

        public async Task<ActionResult> Index()
        {
            var response = new ListTicketOutputViewModel(new ListTicketInputViewModel());
            response.Items = await GetListTickets(response);
            return View(response);
        }

        private async Task<PaginationViewModel<ExternalDeliveryTicket>> GetListTickets(ListTicketInputViewModel input)
        {
            var tickets = _ticketRepo.Find(t => (input.LocationId == null || t.LocationId == input.LocationId)
            && (string.IsNullOrEmpty(input.Status) || t.Status.Contains(input.Status))
            && (input.Claimed == null || t.Claimed == input.Claimed)).OrderByDescending(t => t.CreateDate);
            return new PaginationViewModel<ExternalDeliveryTicket>()
            {
                CurrentPage = input.CurrentPage,
                PerPage = input.PageSize,
                Contents = tickets.Skip(input.CurrentPage * input.PageSize).Take(input.PageSize),
                TotalCount = tickets.Count()
            };
        }

        public async Task<ActionResult> Details(int id)
        {
            var tickets = await _ticketRepo.GetByIdAsync(id);

            if (tickets == null)
            {
                return NotFound();
            }


            return View(tickets);
        }

        private void SetViewBagClaimed()
        {
            ViewBag.Claimed = new List<bool>
            {
                true,
                false
            };
        }

        public async Task<ActionResult> Edit(int id)
        {
            ExternalDeliveryTicket externalDelivery = await _ticketRepo.GetByIdAsync(id);
            var viewModel = new ViewModels.ExternalTicket.Edit(_locationRepository, externalDelivery);

            if (externalDelivery == null)
            {
                return NotFound();
            }

            SetViewBagClaimed();
            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ExternalDeliveryTicket externalDelivery)
        {
            if (ModelState.IsValid)
            {
                var external = await _ticketRepo.GetByIdAsync(externalDelivery.Id);
                external.DeliveryContents = externalDelivery.DeliveryContents;
                external.LocationId = externalDelivery.LocationId;
                external.Status = externalDelivery.Status;
                external.TicketNumber = externalDelivery.TicketNumber;
                external.Claimed = externalDelivery.Claimed;


                await _ticketRepo.Update(external);
                await _ticketRepo.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(new Edit(_locationRepository, externalDelivery));
        }

        public async Task<ActionResult> Delete(int id)
        {

            ExternalDeliveryTicket externalDelivery = await _ticketRepo.GetByIdAsync(id);
            if (externalDelivery == null)
            {
                return NotFound();
            }
            return View(externalDelivery);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ExternalDeliveryTicket externalDelivery = await _ticketRepo.GetByIdAsync(id);
            await _ticketRepo.Delete(externalDelivery);
            await _ticketRepo.UnitOfWork.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _locationRepository.UnitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }


        public ActionResult Create()
        {
            var viewModel = new ViewModels.ExternalTicket.Create(_locationRepository);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("TicketNumber", "Status", "DeliveryContents", "LocationId", "Claimed")] ExternalDeliveryTicket external)
        {
            if (ModelState.IsValid)
            {
                external.CreateDate = DateTime.Now;
                _ticketRepo.Add(external);
                await _locationRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return await Index();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Pagination;
using DineConnect.Gateway.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DineConnect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class BrandsController : Controller
    {
        private IRepository<Brand> _brandRepository;
        private IRepository<Location> _locationRepository;

 
        public BrandsController(IRepository<Brand> brandRepo,
            IRepository<Location> locationRepo
            )
        {
            _locationRepository = locationRepo;
            _brandRepository = brandRepo;
        }

        public async Task<ActionResult> Index(int page=0)
        {
            var paginationViewModel = new PaginationViewModel<Brand>()  
            {  
                CurrentPage = page,  
                Contents = await _brandRepository.GetAll(page * AppConst.DefaultPageSize, AppConst.DefaultPageSize),
                TotalCount = _brandRepository.GetTotalRecords()
            };  
            return View(paginationViewModel);   
          
        }

        public async Task<ActionResult> Details(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);

            if (brand == null)
            {
                return NotFound();
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View(brand);
        }

        public ActionResult Create()
        {
            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id", "Code", "Name", "AddOn")] Brand brand)
        {
            if (ModelState.IsValid)
            {
                brand.CreateDate = DateTime.Now;
                _brandRepository.Add(brand);
                await _brandRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View(brand);
        }

        // GET: Brands/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Brand brand = await _brandRepository.GetByIdAsync(id);

            if (brand == null)
            {
                return NotFound();
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(brand.AddOn);

            return View(brand);
        }

        // POST: Brands/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Brand brand)
        {
            if (ModelState.IsValid)
            {
                var brandInDb = await _brandRepository.GetByIdAsync(brand.Id);
                brandInDb.Code = brand.Code;
                brandInDb.Name = brand.Name;
                brandInDb.AddOn = brand.AddOn;

                await _brandRepository.Update(brandInDb);
                await _brandRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(brand.AddOn);

            return View(brand);
        }

        // GET: Brands/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
   
            Brand brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }
            return View(brand);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Brand brand = await _brandRepository.GetByIdAsync(id);
            await _brandRepository.Delete(brand);
            await _brandRepository.UnitOfWork.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _brandRepository.UnitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private void SetViewBagAddOnKey()
        {
            var listAddOnKeys = new List<string>
            {
                "Brand_Test"
            };

            ViewBag.AddOnKeys = listAddOnKeys;
        }

        private void SetViewBagAddOnData(string addOnData)
        {
            var listAddOnData = string.IsNullOrWhiteSpace(addOnData) ? new List<AddOn>() : JsonConvert.DeserializeObject<List<AddOn>>(addOnData);

            ViewBag.AddOnData = listAddOnData;
        }
    }
}

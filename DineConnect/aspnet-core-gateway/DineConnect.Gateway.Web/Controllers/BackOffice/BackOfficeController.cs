﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class BackOfficeController : Controller
    {
        public BackOfficeController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }
     
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class AdminsController : Controller
    {
        readonly DineConnectGatewayDbContext _dbContext;
        private readonly UserManager<GatewayUser> _userManager;

        public AdminsController(DineConnectGatewayDbContext dbContext
            , UserManager<GatewayUser> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public ActionResult Create()
        {

            return View();
        }


        public async Task<ActionResult> Index(int page = 0)
        {
            var paginationViewModel = new PaginationViewModel<GatewayUser>()
            {
                CurrentPage = page,
                Contents = _dbContext.Users.OrderBy(t => t.UserName).Skip(page * AppConst.DefaultPageSize).Take(AppConst.DefaultPageSize).ToList(),
                TotalCount = _dbContext.Users.Count()
            };
            return View(paginationViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(string username, string email, string password)
        {
            var user = new GatewayUser
            {
                Email = email,
                UserName = username,
                RegistrationDate = DateTime.Now
            };

            var result = await _userManager.CreateAsync(user, password);

            var users = _dbContext.Users.ToList();
            
            return View(users);
        }

       
    }
}
﻿using DineConnect.Gateway.Application;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class LogsController : Controller
    {
        readonly DineConnectGatewayDbContext _dbContext;
        private readonly ILogFileProvider _logFileProvider;
        private readonly ILogFileReader _logFileReader;
        public LogsController(DineConnectGatewayDbContext dbContext,
            ILogFileProvider logFileProvider,
            ILogFileReader logFileReader)
        {
            _logFileReader = logFileReader;
            _logFileProvider = logFileProvider;
            _dbContext = dbContext;
        }

        [HttpPost]
        public ActionResult Read(LogFile logFile)
        {
            return Json(new { content = _logFileReader.Read(logFile) });
        }

        public ActionResult Index()
        {
            var vm = new ViewModels.Logs.Index();
            vm.LogFiles = _logFileProvider.GetLogFiles();

            return View(vm);
        }
    }
}
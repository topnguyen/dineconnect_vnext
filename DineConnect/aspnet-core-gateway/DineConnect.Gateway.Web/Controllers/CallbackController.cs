﻿using DineConnect.Gateway._3rdPartyLibs.OAuthv2;
using DineConnect.Gateway._3rdPartyLibs.Xero;
using DineConnect.Gateway._3rdPartyLibs.ZohoBooks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using DineConnect.Gateway._3rdPartyLibs.DineConnect;

namespace DineConnect.Gateway.Web.Controllers
{
    [Route("callback")]
    public class CallbackController : Controller
    {
        private readonly IConfiguration _configuration;

        private readonly IXeroAuthenticationService _xeroAuthenticationService = new XeroAuthenticationService();
        private readonly IOAuthv2Client _oAuthv2Client = new OAuthv2Client();
        private readonly IZohoBooksAuthService _zohoBooksAuthService = new ZohoBooksAuthService();

        private readonly string _qbRedirectUrl;
        private readonly string _quickBooksClientId;
        private readonly string _quickBooksClientSecret;


        private readonly string _zohoBooksUrl;
        readonly string _zohoBooksClientId;
        readonly string _zohoBooksClientSecret;

        readonly string _xeroRedirectUrl;
        readonly string _xeroClientId;
        readonly string _xeroClientSecret;

        public CallbackController(IConfiguration configuration)
        {
            _configuration = configuration;

            _qbRedirectUrl = $"{configuration.GetValue<string>("Setting:DineConnectGatewayUrl")}/callback/quickbooks";
            _quickBooksClientId = configuration.GetValue<string>("Setting:QuickBooksClientId");
            _quickBooksClientSecret = configuration.GetValue<string>("Setting:QuickBooksClientSecret");

            _zohoBooksUrl = $"{configuration.GetValue<string>("Setting:DineConnectGatewayUrl")}/callback/zohobooks";
            _zohoBooksClientId = configuration.GetValue<string>("Setting:ZohoBooksClientId");
            _zohoBooksClientSecret = configuration.GetValue<string>("Setting:ZohoBooksClientSecret");

            _xeroRedirectUrl = $"{configuration.GetValue<string>("Setting:DineConnectGatewayUrl")}/callback/xero";
            _xeroClientId = configuration.GetValue<string>("Setting:XeroClientId");
            _xeroClientSecret = configuration.GetValue<string>("Setting:XeroClientSecret");
        }

        public ActionResult Index()
        {
            return Ok(new { message = "Couldn't get the organizations from Xero API endpoint. Message: " });
        }

        [HttpGet]
        [Route("xero")]
        public ActionResult Xero(string code, string state)
        {
            try
            {
                var applicationUrl = state;

                var authResult = _oAuthv2Client.RequestAccessToken("https://identity.xero.com/connect/token", _xeroClientId, _xeroClientSecret, code, _xeroRedirectUrl);

                if (authResult == null || authResult.Entity == null || string.IsNullOrEmpty(authResult.Entity.AccessToken))
                {
                    return Ok(new { message = "Couldn't get the token from Xero authorization server. Message: " + authResult.Message });
                }

                if (!authResult.IsSuccessful)
                {
                    return Ok(new { message = "Couldn't get the token from Xero authorization server. Message: " + authResult.Message });
                }

                if (string.IsNullOrEmpty(authResult.Entity.AccessToken))
                {
                    return Ok(new { message = "Couldn't get the token from Xero authorization server. Message: " + authResult.Message });
                }

                var fetchConnectionsResult = _xeroAuthenticationService.GetXeroConnections(authResult.Entity);

                if (!fetchConnectionsResult.IsSuccessful)
                {
                    return Ok(new { message = "Couldn't get the organizations from Xero API endpoint. Message: " + authResult.Message });
                }

                ViewBag.applicationUrl = applicationUrl;
                ViewBag.accessToken = authResult.Entity.AccessToken;
                ViewBag.refreshToken = authResult.Entity.RefreshToken;
                ViewBag.expiresIn = authResult.Entity.ExpiresIn;

                return View(fetchConnectionsResult.Entity);
            }
            catch (Exception e)
            {
                return Ok(new { message = "Error in the Page " + e.StackTrace });
            }

        }

        [HttpPost]
        [Route("xero")]
        public ActionResult Xero(string accessToken, string expiresIn, string refreshToken, string xeroConnectionId, string applicationUrl)
        {
            try
            {
                if (!string.IsNullOrEmpty(applicationUrl) && applicationUrl.EndsWith("/"))
                {
                    applicationUrl = applicationUrl.Substring(0, applicationUrl.Length - 1);
                }

                var tenantAddonUrl = "gatewaycallback/xero";

                string urlBuiltForTenant = $"{applicationUrl}/{tenantAddonUrl}?access_token={accessToken}&expires_in={expiresIn}&refresh_token={refreshToken}&xero_tenant_id={xeroConnectionId}";

                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Ok(new { message = e.Message, detailedMessage = e.InnerException.Message });
            }

        }

        /// <summary>
        /// RealmId is the company id.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <param name="realmId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("quickbooks")]
        public ActionResult Quickbooks(string code, string state, string realmId)
        {
            var applicationUrl = state;

            var authResult = _oAuthv2Client.RequestAccessToken("https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
                    _quickBooksClientId, _quickBooksClientSecret, code, _qbRedirectUrl);

            if (!authResult.IsSuccessful)
                return Ok(new { message = "Couldn't get the token from QuickBooks authorization server. Message: " + authResult.Message });

            try
            {

                var tenantAddonUrl = $"gatewaycallback/quickbooks";

                string urlBuiltForTenant =
                    $"{applicationUrl}/{tenantAddonUrl}?access_token={authResult.Entity.AccessToken}&expires_in={authResult.Entity.ExpiresIn}&refresh_token={authResult.Entity.RefreshToken}&realm_id={realmId}";

                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Ok(new { message = e.Message, detailedMessage = e.InnerException.Message });
            }
        }


        [HttpGet]
        [Route("zohobooks")]
        public ActionResult ZohoBooks(string code, string state)
        {
            var applicationUrl = state;

            var authResult = _oAuthv2Client.RequestAccessToken(" https://accounts.zoho.com/oauth/v2/token",
                    _zohoBooksClientId, _zohoBooksClientSecret, code, _zohoBooksUrl);

            if (!authResult.IsSuccessful)
                return Ok(new { message = "Couldn't get the token from ZohoBooks authorization server. Message: " + authResult.Message });

            if (string.IsNullOrEmpty(authResult.Entity.AccessToken))
                return Ok(new { message = "Couldn't get the token from ZohoBooks authorization server. Message: " + authResult.Message });

            var fetchOrganisationsResult = _zohoBooksAuthService.GetOrganisations(authResult.Entity);


            if (!fetchOrganisationsResult.IsSuccessful)
            {
                return Ok(new { message = "Couldn't get the organizations from ZohoBooks API endpoint. Message: " + authResult.Message });
            }

            ViewBag.applicationUrl = applicationUrl;
            ViewBag.accessToken = authResult.Entity.AccessToken;
            ViewBag.refreshToken = authResult.Entity.RefreshToken;
            ViewBag.expiresIn = authResult.Entity.ExpiresIn;

            return View(fetchOrganisationsResult.Entity);
        }

        [HttpPost]
        [Route("zohobooks")]
        public ActionResult ZohoBooks(string accessToken, string expiresIn, string refreshToken, string organizationId, string applicationUrl)
        {
            try
            {
                var tenantAddonUrl = $"/gatewaycallback/zohobooks";

                string urlBuiltForTenant = $"{applicationUrl}/{tenantAddonUrl}?access_token={accessToken}&expires_in={expiresIn}&refresh_token={refreshToken}&organization_id={organizationId}";

                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Ok(new { message = e.Message, detailedMessage = e.InnerException.Message });
            }

        }

    }
}
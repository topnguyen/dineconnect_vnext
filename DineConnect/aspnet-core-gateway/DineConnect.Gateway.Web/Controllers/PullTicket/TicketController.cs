﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Controllers.Dto;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers.PullTicket
{
    public class TicketController :  Controller
    {
        private IRepository<Location> _locRepo;
        private IRepository<ExternalDeliveryTicket> _edtRepo;

        public TicketController(IRepository<Location> locRepo, IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
        }

        [HttpPost]
        public async Task<JsonResult> Index([FromBody] TenantLocationTicketInput input)
        {

            await DeleteTickets();

            IEnumerable<ExternalDeliveryTicket> tickets =
                _edtRepo.Find(
                    a =>
                        a.LocationId == input.LocationId &&
                        !a.Claimed && input.Status.Contains(a.Status)).OrderByDescending(a=>a.CreateDate).Take(15);

            

            if (!string.IsNullOrEmpty(input.TicketNumber))
            {
                tickets = tickets.Where(a => a.TicketNumber.Equals(input.TicketNumber));
            }
            return Json(tickets);  
        }

        private async Task DeleteTickets()
        {

            var lastDays = DateTime.Now.AddDays(-1);
            IEnumerable<ExternalDeliveryTicket> tickets =
                _edtRepo.Find(a=>a.CreateDate<=lastDays).OrderByDescending(a=>a.CreateDate)
                    .Take(10);

            var allIds = tickets.Select(a => a.Id).ToArray();

            foreach (var allId in allIds)
            {
                ExternalDeliveryTicket myLastTicket =
                    _edtRepo.Find(a => a.Id == allId).OrderBy(a=>a.Id).LastOrDefault();
                if(myLastTicket!=null)
                    await _edtRepo.Delete(myLastTicket);
            }

            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }

        [HttpPost]
        public async Task<JsonResult> ClaimTicket([FromBody] TenantLocationTicketInput input)
        {
            var claimTicket = 
                _edtRepo.Find(
                    a =>
                        !a.Claimed && 
                        a.LocationId == input.LocationId &&
                        a.TicketNumber.Equals(input.TicketNumber) && 
                        !a.Claimed).LastOrDefault();

            if (claimTicket != null)
            {
                claimTicket.Claimed = true;
                if (!string.IsNullOrEmpty(input.ChangeStatus))
                {
                    claimTicket.Status = input.ChangeStatus;
                }
            }

            
            await _edtRepo.UnitOfWork.SaveChangesAsync();

            return Json(claimTicket);  
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using DineConnect.Gateway.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DineConnect.Gateway.Web.Controllers
{
    public class BaseController : Controller
    {
        protected string GetValueFromList(List<AddOn> listofKeys, string lastKey)
        {
            var myKey = listofKeys.LastOrDefault(a=>a.Key.Equals(lastKey));
            if (myKey != null)
            {
                return myKey.Value;
            }

            return "";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Controllers.DeliveryHero.Dto;
using DineConnect.Gateway.Web.Controllers.Dto;
using DineConnect.Gateway.Web.Controllers.Urban.Client;
using DineConnect.Gateway.Web.Controllers.Urban.Dto;
using DineConnect.Gateway.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;

namespace DineConnect.Gateway.Web.Controllers.Zomato
{
    [Authorize] 
    public class ZomatoController : Controller
    {
        private readonly IRepository<ExternalDeliveryTicket> _edtRepo;
        private readonly IRepository<Location> _locRepo;

        public ZomatoController(IRepository<Location> locRepo, IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
        }

        public JsonResult StoreUpdate(string input)
        {
            return Json("Success");
        }

        public JsonResult StoreStatusUpdate(string input)
        {
            return Json("Success");
        }

        public JsonResult CatalogueUpdate(string input)
        {
            return Json("Success");
        }

        public JsonResult ItemUpdate(string input)
        {
            return Json("Success");
        }

        [HttpPost]
        public async Task<JsonResult> PushOrder([FromBody] Object myInput)
        {
            if (myInput == null) throw new Exception("Wrong Object");
            UrbanPushHead input  = JsonConvert.DeserializeObject<UrbanPushHead>(myInput.ToString());

            if (!ValidatePushOrder(input)) throw new Exception("Validations Failed");
            var locationForOrder =
                _locRepo.Find(a => a.Code.Equals(input.order.store.merchant_ref_id)).LastOrDefault();

            if (locationForOrder == null) throw new Exception("Locations is not exist");

            var locationId = locationForOrder.Id;
            var orderId = input.order.details.id.ToString();
            var externalId = "";
            if (input.order.details.ext_platforms.Any())
            {
                var firtP = input.order.details.ext_platforms.Last();
                externalId = firtP.id;
            }

            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = myInput,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushOrder,
                ExternalId = externalId
            });

            return Json("Success");
        }

        [HttpPost]
        public async Task<JsonResult> PushOrderStatus([FromBody]  Object myInput)
        {
            if (myInput == null) throw new Exception("Wrong Object");
           
            UrbanPushStatusHead input  = JsonConvert.DeserializeObject<UrbanPushStatusHead>(myInput.ToString());

            if (!ValidatePushOrderStatus(input))
                throw new Exception("Validations Failed : " + input.order_id);

            var locationForOrder =
                _locRepo.Find(a => a.Code.Equals(input.store_id)).LastOrDefault();

            if (locationForOrder == null) throw new Exception("Locations is not exist : " + input.order_id);

            var locationId = locationForOrder.Id;
            var orderId = input.order_id.ToString();

            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = myInput,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushOrderStatus
            });


            return Json("");
        }

        [HttpPost]
        public async Task<JsonResult> PushRiderStatus([FromBody] Object myInput)
        {
            if (myInput == null) throw new Exception("Wrong Object");
            UrbanPushRiderStatusHead input = JsonConvert.DeserializeObject<UrbanPushRiderStatusHead>(myInput.ToString());
            if (!ValidatePushRiderStatus(input)) throw new Exception("Validations Failed");

            var deliveryTicket =
                _edtRepo.Find(a => a.TicketNumber.Equals(input.order_id.ToString())).LastOrDefault();
            var locationId = 0;

            if (deliveryTicket != null)
                locationId = deliveryTicket.LocationId;
            else
                throw new Exception("No Ticket Available : " + input.order_id);

            var orderId = input.order_id.ToString();
            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = myInput,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushRider
            });
            return Json("");
        }

        [HttpPost]
        public async Task<JsonResult> SendOrderStatus([FromBody] UpdateOrderStatusInput input)
        {

            var apiUserName = "biz_adm_73577076culctpvsxe";
            var apiKey = "d62a09b3a72188add8ce7564287597791137a440";
            var baseAddress = "https://pos-int.urbanpiper.com";


            var ticket = _edtRepo.Find(a => a.LocationId.Equals(input.LocationId)
                                            && a.TicketNumber.Equals(input.TicketNumber)).LastOrDefault();

            var location = _locRepo.Find(a => a.Id.Equals(input.LocationId)).LastOrDefault();




            if (ticket != null && location != null)
            {

                if (!string.IsNullOrEmpty(location.AddOn))
                {
                    var allKeys = JsonConvert.DeserializeObject<List<AddOn>>(location.AddOn);
                    if (allKeys != null && allKeys.Any())
                    {
                        apiUserName = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiUserName);
                        apiKey = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiKey);
                        baseAddress = GetValueFromList(allKeys, GatewayConstants.UrbanPiperUrl);
                    }
                }

                ticket.Claimed = true;
                ticket.Status = input.Status;
                await _edtRepo.Update(ticket);
                await _edtRepo.UnitOfWork.SaveChangesAsync();

                try
                {
                    input.OrderStatus = new UrbanOrderStatusInput()
                    {
                        message = input.Message,
                        new_status = input.NewStatus
                    };
                    input.OrderId = Convert.ToInt32(input.TicketNumber);

                  
                    var apiFinal = $"apikey {apiUserName}:{apiKey}";
                    var urlRequest = $"{UPApiUrls.ManagingOrders}/{input.OrderId}/status/";

                    var dto = input.OrderStatus;

                 


                    var result =
                        await UPApiClient.Instance.Request<CommonResponse<string>>(urlRequest, apiFinal, dto, "PUT",
                            baseAddress);

                 

                }
                catch (Exception exception)
                {
                    return Json(exception.Message);
                }
                   
            }

            return Json("");
        }

        private string GetValueFromList(List<AddOn> listofKeys, string lastKey)
        {
            var myKey = listofKeys.LastOrDefault(a=>a.Key.Equals(lastKey));
            if (myKey != null)
            {
                return myKey.Value;
            }

            return "";
        }

        [HttpPost]
        public async Task<JsonResult> PushMenu([FromBody]Object myInput)
        {
            try
            {
                if (myInput == null) throw new Exception("Wrong Object");
                UbSyncInput input = JsonConvert.DeserializeObject<UbSyncInput>(myInput.ToString());
                var location = _locRepo.Find(a => a.Id == input.LocationId).LastOrDefault();
                if (location == null)
                {
                    return Json("");
                }

                var apiUserName = "biz_adm_73577076culctpvsxe";
                var apiApi = "d62a09b3a72188add8ce7564287597791137a440";
                var baseAddress = "https://pos-int.urbanpiper.com";


                if (!string.IsNullOrEmpty(location.AddOn))
                {
                    var allKeys = JsonConvert.DeserializeObject<List<AddOn>>(location.AddOn);
                    if (allKeys != null && allKeys.Any())
                    {
                        apiUserName = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiUserName);
                        apiApi = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiKey);
                        baseAddress = GetValueFromList(allKeys, GatewayConstants.UrbanPiperUrl);
                    }
                }
                    

           
                string urlUncatalogued = "external/api/v1/inventory/locations/";

                urlUncatalogued = urlUncatalogued + location.Code + "/";

                var items = input.SyncRoot.items;

                var dto = new UbSyncRootDto();

                var apiKey = $"apikey {apiUserName}:{apiApi}";
                const int batchSize = 150;
                var page = 0;

                while (true)
                {
                    var itemsToPush = items.Skip(page * batchSize).Take(batchSize).ToList();
                    dto.items = itemsToPush;
                    var result =
                        await UPApiClient.Instance.Request<CommonResponse<string>>(urlUncatalogued, apiKey, dto, "POST",
                            baseAddress);
                    if (!result.Success) throw new Exception("Request Failed");

                    Thread.Sleep(6000);
                    if (itemsToPush.Count() < batchSize) break;
                    page = page + 1;
                }
                return Json("");
            }
            catch (Exception exception)
            {
                return Json(exception.Message);

            }
          
        }


        #region Validation

        private bool ValidatePushOrder(UrbanPushHead input)
        {
            return true;
        }

        private bool ValidatePushOrderStatus(UrbanPushStatusHead input)
        {
            return true;
        }

        private bool ValidatePushRiderStatus(UrbanPushRiderStatusHead input)
        {
            return true;
        }

        #endregion Validation

        #region Ticket Creation

        private async Task CreateOrUpdateDeliveryTicket(UrbanPiperInput input)
        {
            var count = _edtRepo.Find(
                a =>
                    a.TicketNumber != null && a.TicketNumber.Equals(input.OrderId) &&
                    a.LocationId.Equals(input.LocationId)).Count();

            var updateTicket = count > 0;
            var createTicket = true;
            if (updateTicket)
            {
                var ticket =
                    _edtRepo.Find(
                        a =>
                            a.TicketNumber.Equals(input.OrderId) &&
                            a.LocationId.Equals(input.LocationId)).LastOrDefault();

                if (ticket != null)
                {
                    createTicket = false;
                    if (input.ContentType.Equals(UrbanPiperConsts.PushOrder))
                        if (!ticket.Status.Equals(DeliveryConsts.Initial))
                            await UpdateTicket(ticket, input);

                    if (input.ContentType.Equals(UrbanPiperConsts.PushOrderStatus)) await UpdateTicket(ticket, input);

                    if (input.ContentType.Equals(UrbanPiperConsts.PushRider))
                        if (!DeliveryConsts.CompletedTicketStatus.Contains(ticket.Status))
                            await UpdateTicket(ticket, input);
                }
            }

            if (createTicket) await CreateTicket(input);
        }

        private async Task<int> CreateTicket(UpdateExternalDeliveryTicketInput input)
        {
            var extT = new ExternalDeliveryTicket
            {
                TicketNumber = input.OrderId,
                LocationId = input.LocationId,
                Claimed = false,
                Status = DeliveryConsts.Initial
            };

            extT.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });

            _edtRepo.Add(extT);
            await _edtRepo.UnitOfWork.SaveChangesAsync();

            return extT.Id;
        }

        private async Task UpdateTicket(ExternalDeliveryTicket ticket, UpdateExternalDeliveryTicketInput input)
        {
            ticket.Claimed = false;
            ticket.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });
            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }

        #endregion Ticket Creation
    }
}
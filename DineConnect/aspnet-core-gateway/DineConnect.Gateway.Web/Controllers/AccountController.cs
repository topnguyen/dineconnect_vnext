﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace DineConnect.Gateway.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<GatewayUser> _userManager;
        private readonly SignInManager<GatewayUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<GatewayUser> userManager, SignInManager<GatewayUser> signInManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("account/reset-password/{userId}/{newPassword}")]
        public async Task<ActionResult> ResetPassword(string userId, string newPassword)
        {
            try
            {
                var currentUser = await _userManager.FindByIdAsync(Guid.Parse(userId).ToString());

                await _userManager.RemovePasswordAsync(currentUser);

                await _userManager.AddPasswordAsync(currentUser, newPassword);

                return new JsonResult(new { Data = new { IsSuccessful = true } });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<ActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login");
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }


        /// <summary>
        /// May get a redirect url as well.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Login(string username, string password, string returnUrl = "/Home")
        {
            var result = await _signInManager.PasswordSignInAsync(username, password, true, false);

            if (result.Succeeded)
            {
                return LocalRedirect(returnUrl);
            }
#if DEBUG
            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                user = new GatewayUser
                {
                    UserName = username,
                    RegistrationDate = DateTime.Now
                };

                var identityResult = await _userManager.CreateAsync(user, password);

                var signInResult = await _signInManager.PasswordSignInAsync(username, password, true, false);

                return LocalRedirect(returnUrl);
            }
#endif

            ModelState.AddModelError("InvalidCredentials", "Invalid username or password");

            return View();
        }

       

        [Route("ouath/token")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> Token([FromBody] RequestTokenModel model)
        {
            if (model.GrantType != "client_credentials")
            {
                return Json(new
                {
                    error = "Invalid grant_type"
                });
            }

            var result = await _signInManager.PasswordSignInAsync(model.ClientId, model.ClientSecret, true, false);

            if (!result.Succeeded)
            {
                return Json(new
                {
                    error = "Invalid client_id or client_secret"
                });
            }

            var user = await _userManager.FindByNameAsync(model.ClientId);

            string key = _configuration.GetValue<string>("Setting:JwtTokenSecret");
            var issuer = typeof(Startup).Namespace;

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sid, user.Id)
            };

            //Create Security Token object by giving required parameters    

            var expiresTime = DateTime.Now.AddDays(1);

            var token = new JwtSecurityToken(issuer,
                issuer,
                permClaims,
                expires: expiresTime,
                signingCredentials: credentials);

            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return Json(new
            {
                access_token = jwt_token,
                token_type = "Bearer",
                expires_in = (int) TimeSpan.FromDays(1).TotalSeconds
            });
        }
    }
}
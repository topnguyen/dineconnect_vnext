﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DineConnect.Gateway._3rdPartyLibs.DineConnect;
using DineConnect.Gateway._3rdPartyLibs.DineConnect.Model;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.Web.Controllers.DeliveryHero.Dto;
using DineConnect.Gateway.Web.Controllers.Dto;
using DineConnect.Gateway.Web.Controllers.Grab.Dto;
using DineConnect.Gateway.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace DineConnect.Gateway.Web.Controllers.Grab
{
    public class GrabController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IRepository<ExternalDeliveryTicket> _edtRepo;
        private readonly IRepository<Location> _locRepo;
        private readonly ITokenService _tokenService = new TokenService();

        public GrabController(IRepository<Location> locRepo,
            IRepository<ExternalDeliveryTicket> edtRepo, IConfiguration configuration)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
            _configuration = configuration;
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> Index([FromBody] object input)
        {

            Response.StatusCode = 500;

            var returnOutput = new OrderOutput();
            try
            {
                if (input == null)
                {
                    returnOutput = new OrderOutput
                    {
                        code = "INVALID_REQUEST",
                        message = "Invalid Request"
                    };
                    return Json(returnOutput);
                }

                var myConvert = JsonConvert.DeserializeObject<GrabRootObject>(input.ToString());
                if (myConvert == null)
                {
                    returnOutput = new OrderOutput
                    {
                        code = "INVALID_REQUEST",
                        message = "Invalid Request"
                    };
                    return Json(returnOutput);
                }

                if (!ValidatePushOrder(myConvert))
                {
                    returnOutput = new OrderOutput
                    {
                        code = "VALIDATION_ERROR",
                        message = "Validation Error"
                    };
                    return Json(returnOutput);
                }

                var locationForOrder =
                    _locRepo.Find(a => a.Code.Equals(myConvert.merchantID)).LastOrDefault();

                if (locationForOrder == null)
                {
                    returnOutput = new OrderOutput
                    {
                        code = "POS_ERROR",
                        message = "Location is not available"
                    };
                    return Json(returnOutput);
                }

                var locationId = locationForOrder.Id;
                var externalId = "";

                var output = await CreateOrUpdateDeliveryTicket(new UpdateExternalDeliveryTicketInput
                {
                    OrderId = myConvert.orderID,
                    LocationId = locationId,
                    ContentType = DeliveryOrderConsts.PushOrder,
                    ExternalId = externalId,
                    Contents = input.ToString()
                });
                returnOutput.remoteResponse.remoteOrderId = output.ToString();
                Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                returnOutput = new OrderOutput
                {
                    code = "POS_ERROR",
                    message = ex.Message
                };
            }

            return Json(returnOutput);
        }

        [Authorize]
        [HttpPut]
        public async Task<JsonResult> State([FromBody] object input)
        {
            Response.StatusCode = 500;
            var returnOutput = new OrderOutput();

            if (input == null)
            {
                returnOutput = new OrderOutput
                {
                    code = "INVALID_REQUEST",
                    message = "Invalid Request"
                };
                return Json(returnOutput);
            }

            var myConvert = JsonConvert.DeserializeObject<GrabRootOrderStateObject>(input.ToString());
            if (myConvert == null)
            {
                returnOutput = new OrderOutput
                {
                    code = "INVALID_REQUEST",
                    message = "Invalid Request"
                };
                return Json(returnOutput);
            }

            if (!ValidateStateOrder(myConvert))
            {
                returnOutput = new OrderOutput
                {
                    code = "VALIDATION_ERROR",
                    message = "Validation Error"
                };
                return Json(returnOutput);
            }

            var locationForOrder =
                _locRepo.Find(a => a.Code.Equals(myConvert.merchantID)).LastOrDefault();

            if (locationForOrder == null)
            {
                returnOutput = new OrderOutput
                {
                    code = "POS_ERROR",
                    message = "Location is not available"
                };
                return Json(returnOutput);
            }

            var locationId = locationForOrder.Id;
            var externalId = "";

            var output = await CreateOrUpdateDeliveryTicket(new UpdateExternalDeliveryTicketInput
            {
                OrderId = myConvert.orderID,
                LocationId = locationId,
                ContentType = DeliveryOrderConsts.PushOrderStatus,
                ExternalId = externalId,
                Contents = input.ToString()
            });
            returnOutput.remoteResponse.remoteOrderId = output.ToString();
            Response.StatusCode = 200;
           
            return Json("");
        }

        [HttpGet]
        public async Task<JsonResult> Menu(string merchantId)
        {
            if (!string.IsNullOrEmpty(merchantId))
            {
                Response.StatusCode = 200;

                var defLocation =
                    _locRepo.Find(
                        a =>
                            a.Code.Equals(merchantId)).LastOrDefault();

                if (defLocation != null)
                {
                    if (!string.IsNullOrEmpty(defLocation.AddOn))
                    {
                        var allKeys = JsonConvert.DeserializeObject<List<AddOn>>(defLocation.AddOn);
                        if (allKeys != null && allKeys.Any())
                        {
                            var myLocationId = GetValueFromList(allKeys, GatewayConstants.DineConnectLocationId);
                            var myUrl = GetValueFromList(allKeys, GatewayConstants.DineConnectUrl);
                            var myUser = GetValueFromList(allKeys, GatewayConstants.DineConnectUser);
                            var myPassword = GetValueFromList(allKeys, GatewayConstants.DineConnectPassword);
                            var myTenant = GetValueFromList(allKeys, GatewayConstants.DineConnectTenantName);
                            var myTenantId = GetValueFromList(allKeys, GatewayConstants.DineConnectTenantId);

                            if (!string.IsNullOrEmpty(myLocationId) && !string.IsNullOrEmpty(myUrl) &&
                                !string.IsNullOrEmpty(myUser) && !string.IsNullOrEmpty(myPassword) &&
                                !string.IsNullOrEmpty(myTenant) && !string.IsNullOrEmpty(myTenantId))
                            {
                                var myMenu = _tokenService.GetResponse(
                                    "api/services/app/delAggApi/ApiForGrab",
                                    new LocationInput
                                    {
                                        LocationId = Convert.ToInt32(myLocationId),
                                        TenantId = Convert.ToInt32(myTenantId),
                                        MerchantCode = defLocation.Id.ToString(),
                                        PartnerMerchantCode = defLocation.Code
                                    }, new ConnectAuthInputDto
                                    {
                                        Url = myUrl,
                                        TenantName = myTenant,
                                        TenantId = myTenantId,
                                        UserName = myUser,
                                        Password = myPassword,
                                        SyncvNext = false
                                    });
                                if (myMenu != null)
                                {
                                    var myObject = JsonConvert.DeserializeObject<GrabMenuRoot>(myMenu.ToString(),new JsonSerializerSettings() { Culture = CultureInfo.InvariantCulture});
                                    if (myObject != null)
                                        return Json(myObject);
                                }
                                else
                                {
                                    Response.StatusCode = 500;
                                    return Json("Menu is empty");
                                }
                            }
                        }
                    }
                }
                else
                {
                    Response.StatusCode = 500;
                    return Json("Merchant Code is not available in the Gateway");
                }
            }

            Response.StatusCode = 500;
            return Json("Merchant Code is empty");
        }

        [HttpPost]
        public async Task<JsonResult> PushMenuToGrab(string merchantId)
        {
            if (!string.IsNullOrEmpty(merchantId))
            {
                Response.StatusCode = 200;

                var defLocation =
                    _locRepo.Find(
                        a =>
                            a.Code.Equals(merchantId)).LastOrDefault();

                if (defLocation != null)
                {
                    if (!string.IsNullOrEmpty(defLocation.AddOn))
                    {
                        var allKeys = JsonConvert.DeserializeObject<List<AddOn>>(defLocation.AddOn);
                        if (allKeys != null && allKeys.Any())
                        {
                            var myLocationId = GetValueFromList(allKeys, GatewayConstants.DineConnectLocationId);
                            var myUrl = GetValueFromList(allKeys, GatewayConstants.DineConnectUrl);
                            var myUser = GetValueFromList(allKeys, GatewayConstants.DineConnectUser);
                            var myPassword = GetValueFromList(allKeys, GatewayConstants.DineConnectPassword);
                            var myTenant = GetValueFromList(allKeys, GatewayConstants.DineConnectTenantName);
                            var myTenantId = GetValueFromList(allKeys, GatewayConstants.DineConnectTenantId);

                            if (!string.IsNullOrEmpty(myLocationId) && !string.IsNullOrEmpty(myUrl) &&
                                !string.IsNullOrEmpty(myUser) && !string.IsNullOrEmpty(myPassword) &&
                                !string.IsNullOrEmpty(myTenant) && !string.IsNullOrEmpty(myTenantId))
                            {
                                var myMenu = _tokenService.GetResponse(
                                    "api/services/app/delAggApi/ApiForGrab",
                                    new LocationInput
                                    {
                                        LocationId = Convert.ToInt32(myLocationId),
                                        TenantId = Convert.ToInt32(myTenantId),
                                        MerchantCode = defLocation.Id.ToString(),
                                        PartnerMerchantCode = defLocation.Code
                                    }, new ConnectAuthInputDto
                                    {
                                        Url = myUrl,
                                        TenantName = myTenant,
                                        TenantId = myTenantId,
                                        UserName = myUser,
                                        Password = myPassword,
                                        SyncvNext = false
                                    });
                                if (myMenu != null)
                                {
                                    var myObject = JsonConvert.DeserializeObject<GrabMenuRoot>(myMenu.ToString());
                                    if (myObject != null)
                                        return Json(myObject);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Response.StatusCode = 500;
                    return Json("Merchant Code is not available in the Gateway");
                }
            }

            Response.StatusCode = 500;
            return Json("Merchant Code is empty");
        }

        [HttpPost]
        public async Task<JsonResult> SendOrderStatus([FromBody] UpdateOrderStatusInput input)
        {
            Response.StatusCode = 200;

            var ticket = _edtRepo.Find(a => a.LocationId.Equals(input.LocationId)
                                            && a.TicketNumber.Equals(input.TicketNumber)).LastOrDefault();

            if (ticket != null)
            {
                ticket.Claimed = true;
                ticket.Status = input.Status;
                await _edtRepo.Update(ticket);
                await _edtRepo.UnitOfWork.SaveChangesAsync();
            }
            return Json("");
        }

        #region Private

        private bool ValidatePushOrder(GrabRootObject input)
        {
            if (string.IsNullOrEmpty(input.orderID))
                return false;

            if (string.IsNullOrEmpty(input.merchantID))
                return false;

            if (string.IsNullOrEmpty(input.partnerMerchantID))
                return false;


            return true;
        }
        private bool ValidateStateOrder(GrabRootOrderStateObject input)
        {
            if (string.IsNullOrEmpty(input.orderID))
                return false;

            if (string.IsNullOrEmpty(input.merchantID))
                return false;

            if (string.IsNullOrEmpty(input.state))
                return false;


            return true;
        }

        private async Task<int> CreateOrUpdateDeliveryTicket(UpdateExternalDeliveryTicketInput input)
        {
            var output = 0;
            var count = _edtRepo.Find(
                a =>
                    a.TicketNumber != null && a.TicketNumber.Equals(input.OrderId) &&
                    a.LocationId.Equals(input.LocationId)).Count();

            var updateTicket = count > 0;
            var createTicket = true;
            if (updateTicket)
            {
                var ticket =
                    _edtRepo.Find(
                        a =>
                            a.TicketNumber.Equals(input.OrderId) &&
                            a.LocationId.Equals(input.LocationId)).LastOrDefault();

                if (ticket != null)
                {
                    createTicket = false;
                    output = ticket.Id;
                    if (input.ContentType.Equals(DeliveryOrderConsts.PushOrder))
                    {
                            await UpdateTicket(ticket, input);
                    }
                    else
                    {
                        await UpdateTicket(ticket, input);
                    }
                }
            }

            if (createTicket)
                output = await CreateTicket(input);

            return output;
        }

        private async Task UpdateTicket(ExternalDeliveryTicket ticket, UpdateExternalDeliveryTicketInput input)
        {
            ticket.Claimed = false;
            ticket.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });
            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }

        private async Task<int> CreateTicket(UpdateExternalDeliveryTicketInput input)
        {
            var extT = new ExternalDeliveryTicket
            {
                TicketNumber = input.OrderId,
                LocationId = input.LocationId,
                Claimed = false,
                Status = DeliveryConsts.Initial
            };

            extT.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });

            _edtRepo.Add(extT);
            await _edtRepo.UnitOfWork.SaveChangesAsync();
            return extT.Id;
        }

        #endregion
    }
}
﻿using System.Collections.Generic;

namespace DineConnect.Gateway.Web.Controllers.Grab.Dto
{
    public class GrabRootOrderStateObject
    {
        public string merchantID { get; set; }
        public string orderID { get; set; }
        public string state { get; set; }
        public string message { get; set; }
    }
    public class GrabRootObject
    {
        public string orderID { get; set; }
        public string shortOrderNumber { get; set; }
        public string merchantID { get; set; }
        public string partnerMerchantID { get; set; }
    }

    public class GrabMenuCurrency
    {
        public string code { get; set; }
        public string symbol { get; set; }
        public int exponent { get; set; }
    }

    public class GrabMenuPeriod
    {
        public string startTime { get; set; }
        public string endTime { get; set; }
    }

    public class GrabMenuMon
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuTue
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuWed
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuThu
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuFri
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuSat
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuSun
    {
        public string openPeriodType { get; set; }
        public List<GrabMenuPeriod> periods { get; set; }
    }

    public class GrabMenuServiceHours
    {
        public GrabMenuMon mon { get; set; }
        public GrabMenuTue tue { get; set; }
        public GrabMenuWed wed { get; set; }
        public GrabMenuThu thu { get; set; }
        public GrabMenuFri fri { get; set; }
        public GrabMenuSat sat { get; set; }
        public GrabMenuSun sun { get; set; }
    }

    public class GrabMenuModifier
    {
        public string id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public string availableStatus { get; set; }
        public int price { get; set; }
    }

    public class GrabMenuModifierGroup
    {
        public string id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public string availableStatus { get; set; }
        public int selectionRangeMin { get; set; }
        public int selectionRangeMax { get; set; }
        public List<GrabMenuModifier> modifiers { get; set; }
    }

    public class GrabMenuItem
    {
        public string id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public string availableStatus { get; set; }
        public int price { get; set; }
        public string description { get; set; }
        public List<string> photos { get; set; }
        public List<GrabMenuModifierGroup> modifierGroups { get; set; }
    }

    public class GrabMenuCategory
    {
        public string id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public string availableStatus { get; set; }
        public List<GrabMenuItem> items { get; set; }
    }

    public class GrabMenuSection
    {
        public string id { get; set; }
        public string name { get; set; }
        public int sequence { get; set; }
        public GrabMenuServiceHours serviceHours { get; set; }
        public List<GrabMenuCategory> categories { get; set; }
    }

    public class GrabMenuRoot
    {
        public string merchantID { get; set; }
        public string partnerMerchantID { get; set; }
        public GrabMenuCurrency currency { get; set; }
        public List<GrabMenuSection> sections { get; set; }
    }
}
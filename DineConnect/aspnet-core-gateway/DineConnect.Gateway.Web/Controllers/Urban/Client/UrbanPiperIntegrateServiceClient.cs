﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Extensions;
using DineConnect.Gateway.Core.MimeTypes;
using DineConnect.Gateway.Web.Controllers.Urban.Dto;
using Newtonsoft.Json;
using Serilog;

namespace DineConnect.Gateway.Web.Controllers.Urban.Client
{
    public class UPApiClient
    {

        private readonly HttpClient _httpClient = new HttpClient(new HttpClientHandler())
        {
            Timeout = TimeSpan.FromSeconds(600)
        };

        private const string AuthenticationHeaderName = "Authorization";

        static UPApiClient()
        {
        }

      

        public static UPApiClient Instance { get; } = new UPApiClient();

        public async Task<T> Request<T>(string url, string upApiKey, object requestObj, string method = "POST",string baseAddress="https://api.urbanpiper.com")
        {
            try
            {
               
                var request = new HttpRequestMessage(new HttpMethod(method), CreateFullUrl(url,baseAddress));
                request.Headers.Add(AuthenticationHeaderName, upApiKey);
                request.Headers.Add("Cache-Control", "no-cache");
                request.Content = new StringContent(JsonConvert.SerializeObject(requestObj), Encoding.UTF8, MimeTypeNames.ApplicationJson);
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));

                var response = await _httpClient.SendAsync(request);
                var success = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(success))
                {
                    var myComRequest = new CommonResponse<string>()
                    {
                        message = "Done",
                        status = "success",
                        reference = "Nothing"
                    };
                    success = JsonConvert.SerializeObject(myComRequest);
                }
                if (!response.IsSuccessStatusCode)
                {
                    Log.Fatal("Client - " + url +" Method - " + method +" Base Address : " + baseAddress);
                    
                    Log.Fatal("Request");
                    Log.Fatal((JsonConvert.SerializeObject(requestObj)));
                    Log.Fatal("Request End");
                    
                    Log.Fatal("Response");
                    HttpContent requestContent = response.Content;
                    string jsonContent = await requestContent.ReadAsStringAsync();
                    Log.Fatal(jsonContent);
                    Log.Fatal("Response End");
                    
                    Log.Fatal("Client End - " + url);
                }
               

                T result = JsonConvert.DeserializeObject<T>(success);
                return result;
            }
            catch (Exception ex)
            {
                Log.Fatal("Error in Integrate Client Issue", ex);
                throw new Exception(ex.Message);
            }
        }

        private Uri CreateFullUrl(string url, string baseAddress)
        {

            if (!baseAddress.EndsWith("/"))
            {
                baseAddress = baseAddress + "/";
            }

            return new Uri(baseAddress + url);
        }
       
    }
}
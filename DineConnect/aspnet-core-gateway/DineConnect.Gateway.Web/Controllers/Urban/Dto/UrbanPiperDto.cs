﻿using System.Collections.Generic;

namespace DineConnect.Gateway.Web.Controllers.Urban.Dto
{
    public abstract class IUrbanInput
    {
    }


    public class UrbanExtPlatform
    {
        public string id { get; set; }
       
    }

    public class UrbanDetails
    {
       
        public List<UrbanExtPlatform> ext_platforms { get; set; }
        public int id { get; set; }
       
    }

   
    public class UrbanStore
    {
        public string merchant_ref_id { get; set; }
    }
    public class UrbanOrder
    {
        public UrbanDetails details { get; set; }
        public UrbanStore store { get; set; }
       
    }

    public class UrbanPushHead : IUrbanInput
    {
        public UrbanOrder order { get; set; }
    }

  

   

    public class UrbanPushStatusHead : IUrbanInput
    {
        public string new_state { get; set; }
        public int order_id { get; set; }
        public string store_id { get; set; }
    }


    public class UrbanPushRiderStatusHead : IUrbanInput
    {
        public int order_id { get; set; }
    }

   
  

   
}
﻿namespace DineConnect.Gateway.Web.Controllers.Urban.Dto
{
    namespace DinePlan.DineConnect.External.Zomato.Dto
    {
        public class UrbanPiperConnect
        {
            public bool Connected { get; set; }
            public string ApiKey { get; set; }
            public int TagId { get; set; }

        }

        public class UrbanPiperSettingDto
        {
            public string ApiKey { get; set; }
            public int TagId { get; set; }

        }

    }

}

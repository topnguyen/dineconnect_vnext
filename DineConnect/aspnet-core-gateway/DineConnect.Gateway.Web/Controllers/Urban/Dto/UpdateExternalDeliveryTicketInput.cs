﻿using System;
using DineConnect.Gateway.Web.Controllers.Dto;

namespace DineConnect.Gateway.Web.Controllers.Urban.Dto
{
    public class UrbanPiperInput : UpdateExternalDeliveryTicketInput
    {
        public Object UrbanInput {get;set;}
      

    }

    public class CommonResponse<T>
    {
        public string status { get; set; }

        public string message { get; set; }

        public T reference { get; set; }

        public bool Success
        {
            get { return status.Equals("success"); }

        }
    }

    public class RequestRespomse
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace DineConnect.Gateway.Web.Controllers.Dto
{
    public class OrderOutput
    {
        public OrderOutput()
        {
            remoteResponse = new OrderRemoteResponse();
        }

        public string code { get; set; }
        public string message { get; set; }
        public OrderRemoteResponse remoteResponse { get; set; }
    }

    public class OrderRemoteResponse
    {
        public OrderRemoteResponse()
        {
            remoteOrderId = "";
        }
        public string remoteOrderId { get; set; }

    }
    public class OrderStatusInput
    {
        public string new_status { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }

    public class UrbanOrderStatusInput
    {
        public string new_status { get; set; }
        public string message { get; set; }
    }
    public class DeHeRootObject
    {
        public string token { get; set; }
        public string code { get; set; }
      
        public DeHePlatformRestaurant platformRestaurant { get; set; }
      
    }
    public class DeHePlatformRestaurant
    {
        public string id { get; set; }
    }
}
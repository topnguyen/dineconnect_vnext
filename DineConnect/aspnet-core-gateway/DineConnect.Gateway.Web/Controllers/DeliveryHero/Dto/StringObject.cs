﻿namespace DineConnect.Gateway.Web.Controllers.Dto
{
    public class StringObject
    {
        public string Content { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }

    }
}

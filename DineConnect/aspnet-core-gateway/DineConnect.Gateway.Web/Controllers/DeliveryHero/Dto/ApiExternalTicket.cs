﻿using System.Collections.Generic;

namespace DineConnect.Gateway.Web.Controllers.Dto
{
    public class UpdateExternalDeliveryTicketInput
    {
        public int LocationId {get;set;}
        public string OrderId {get;set;}
        public string Contents {get;set;}
        public string ContentType {get;set;}
        public string ExternalId {get;set;}

    }

    public class DeliveryOrderConsts
    {
        public static string PushOrder = "PushOrder";
        public static string PushOrderStatus = "PushOrderStatus";
    }

    public class DeliveryConsts
    {

        public static string[] CompletedTicketStatus = {"CD","RD","CE"};


        public static string Initial = "IN";
        public static string FoodReady = "FR";
        public static string Completed = "CD";
        public static string Accepted = "AC";
        public static string Rejected = "RD";

        public static string CancelledByDeliveryHero = "CE";
        public static string CancelledUrbanPiper = "CU";
    }
    public class ApiExternalTicket
    {
        public virtual List<ApiExternalTicketOutput> Items { get; set; }
    }

    public class ApiExternalTicketOutput
    {
        public int Id { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string TicketNumber { get; set; }
        public virtual string Status { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual string DeliveryContents { get; set; }
        public bool Accepted { get; set; }
        public bool Completed { get; set; }

      
    }

    public class TenantLocationTicketInput 
    {
        public int LocationId { get; set; }
        public string TicketNumber { get; set; }
        public string Provider { get; set; }
        public List<string> Status { get; set; }
        public string ChangeStatus { get; set; }

        public int PlatForm { get; set; }

        public bool IsDeliveryHero => PlatForm == 1;
        public bool IsUrbanPiper => PlatForm == 2;

    }
}

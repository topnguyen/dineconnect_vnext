﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DineConnect.Gateway.Web.Controllers
{
    [Route("dineconnect")]
    public class DineConnectController : Controller
    {
        private readonly string XeroCallbackUrl;
        private readonly string QuickbooksCallbackUrl;
        private readonly string ZohoBooksCallbackUrl;
        private readonly string xeroClientId;
        private readonly string QuickBooksClientId;
        private readonly string ZohoBooksClientId;
        private readonly string ZohoBooksClientSecret;

        public DineConnectController(IConfiguration configuration)
        {
           XeroCallbackUrl = string.Format("{0}/callback/xero", configuration.GetValue<string>("Setting:DineConnectGatewayUrl"));
           QuickbooksCallbackUrl = string.Format("{0}/callback/quickbooks", configuration.GetValue<string>("Setting:DineConnectGatewayUrl"));
           ZohoBooksCallbackUrl = string.Format("{0}/callback/zohobooks", configuration.GetValue<string>("Setting:DineConnectGatewayUrl"));
           xeroClientId = configuration.GetValue<string>("Setting:XeroClientId");
           QuickBooksClientId = configuration.GetValue<string>("Setting:QuickBooksClientId");
           ZohoBooksClientId = configuration.GetValue<string>("Setting:ZohoBooksClientId");
           ZohoBooksClientSecret = configuration.GetValue<string>("Setting:ZohoBooksClientSecret");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationType">Xero, QuickBooks</param>
        /// <param name="tenantId">DineConnect TenantId</param>
        /// <param name="applicationUrl">http://abc.dineconnect.net, http://ced.dineconnect.net</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(string applicationType, string applicationUrl, string dineConnectTenant = "1")
        {
            string loginUrl = string.Empty;

            if (applicationType.ToLower().Equals("xero"))
            {
                var scopes = "openid%20profile%20email%20accounting.transactions%20accounting.settings%20accounting.contacts%20offline_access";
                loginUrl = string.Format("https://login.xero.com/identity/connect/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope={2}&state={3}",
                     xeroClientId,
                     XeroCallbackUrl,
                     scopes,
                     applicationUrl);
            }

            else if (applicationType.ToLower().Equals("quickbooks"))
            {
                var scope = "com.intuit.quickbooks.accounting";

                loginUrl =
                    string.Format("https://appcenter.intuit.com/connect/oauth2?client_id={0}&scope={1}&redirect_uri={2}&response_type=code&state={3}",
                    QuickBooksClientId,
                    scope,
                    QuickbooksCallbackUrl,
                    applicationUrl);
            }


            else if(applicationType.ToLower().Equals("zohobooks"))
            {
                var scope = "ZohoBooks.fullaccess.all";
                loginUrl = $"https://accounts.zoho.com/oauth/v2/auth?client_id={ZohoBooksClientId}&scope={scope}&response_type=code&redirect_uri={ZohoBooksCallbackUrl}&access_type=offline&prompt=consent&state={applicationUrl}";
            }

            else
            {
                throw new InvalidOperationException("Please check the application type. It must be one of those: Xero, Quickbooks");
            }

            return Redirect(loginUrl);
        }
    }
}
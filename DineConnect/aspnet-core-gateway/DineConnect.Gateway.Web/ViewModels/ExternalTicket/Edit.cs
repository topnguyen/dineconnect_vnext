﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using Newtonsoft.Json;

namespace DineConnect.Gateway.Web.ViewModels.ExternalTicket
{
    public class Edit
    {
        public Edit(IRepository<Core.Models.Data.Location> locationRepository, DineConnect.Gateway.Core.Models.Data.ExternalDeliveryTicket external)
        {
            ExternalDelivery = external;
            Location = locationRepository.GetAll().Result;
        }
        public IList<Core.Models.Data.Location> Location { get; set; }

        public DineConnect.Gateway.Core.Models.Data.ExternalDeliveryTicket ExternalDelivery { get; set; }

    }
}
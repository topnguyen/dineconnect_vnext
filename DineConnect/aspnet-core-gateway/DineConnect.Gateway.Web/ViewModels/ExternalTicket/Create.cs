﻿using System.Collections.Generic;
using System.Linq;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;

namespace DineConnect.Gateway.Web.ViewModels.ExternalTicket
{
    public class Create
    {
        public Create(IRepository<Core.Models.Data.Location> location)
        {
            Location = location.GetAll().Result;
        }

        public IList<Core.Models.Data.Location> Location { get; set; }
    }
}
﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;

namespace DineConnect.Gateway.Web.ViewModels.Location
{
    public class Create
    {
        public Create(IRepository<Brand> brandRepo)
        {
            Brands = brandRepo.GetAll().Result;
        }

        public IList<Brand> Brands { get; set; }

        public List<AddOn> AddOns { get; set; } = new List<AddOn>();
    }
}
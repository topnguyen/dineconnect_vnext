﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using Newtonsoft.Json;

namespace DineConnect.Gateway.Web.ViewModels.Location
{
    public class Edit
    {
        public Edit(IRepository<Brand> brandRepo, DineConnect.Gateway.Core.Models.Data.Location location)
        {
            Location = location;
            Brands = brandRepo.GetAll().Result;
            AddOns = string.IsNullOrWhiteSpace(location.AddOn) ? new List<AddOn>() : JsonConvert.DeserializeObject<List<AddOn>>(location.AddOn);
        }
        public IList<Brand> Brands { get; set; }

        public DineConnect.Gateway.Core.Models.Data.Location Location { get; set; }

        public List<AddOn> AddOns { get; set; }
    }
}
﻿using System.Collections.Generic;
using DineConnect.Gateway.Web.Extensions;

namespace DineConnect.Gateway.Web.ViewModels.Location
{
    public class Index
    {
        public Index(List<DineConnect.Gateway.Core.Models.Data.Location> locations)
        {
            Locations = locations.ToViewModel();
        }
        public List<LocationModel> Locations { get; set; }
    }
}
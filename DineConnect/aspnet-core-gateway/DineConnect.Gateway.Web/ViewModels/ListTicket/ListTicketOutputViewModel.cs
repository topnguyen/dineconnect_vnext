﻿using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.Web.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Web.ViewModels.ListTicket
{
    public class ListTicketOutputViewModel : ListTicketInputViewModel
    {
        public ListTicketOutputViewModel(ListTicketInputViewModel input)
        {
            base.LocationId = input.LocationId;
            base.Claimed = input.Claimed;
            base.CurrentPage = input.CurrentPage;
            base.Status = input.Status;
        }
        public PaginationViewModel<ExternalDeliveryTicket> Items {get;set;}
    }
}

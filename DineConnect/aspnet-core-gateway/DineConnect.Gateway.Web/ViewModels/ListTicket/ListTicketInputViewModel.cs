﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Web.ViewModels.ListTicket
{
    public class ListTicketInputViewModel : PagedInputDto
    {
        public int? LocationId { get; set; } = null;

        public bool? Claimed { get; set; } = null;

        public string Status { get; set; }
    }
}

﻿using DineConnect.Gateway.Core.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Web.ViewModels
{
    public class PagedInputDto
    {
        public int PageSize { get; set; }

        public int CurrentPage { get; set; }

        public PagedInputDto()
        {
            PageSize = AppConst.DefaultPageSize;
        }
    }
}

﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.Web.ViewModels.Logs
{
    public class Index
    {
        public List<LogFile> LogFiles { get; set; }
    }
}
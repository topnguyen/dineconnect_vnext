﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace DineConnect.Gateway.Web.Filters
{
    public class JwtAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;

        public JwtAuthorizationMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var isProtectedAction = IsProtectedAction(context);

                if (!isProtectedAction)
                {
                    await _next(context);
                    return;
                }

                var accessToken = context.Request.Headers["Authorization"].ToString();

                if (!string.IsNullOrWhiteSpace(accessToken))
                {
                    accessToken = accessToken.Split(' ').LastOrDefault();

                    var userId = ValidateJwtToken(accessToken);

                    if (userId != null)
                    {
                        var claim = new Claim("sub", userId);

                        var principle = new ClaimsPrincipal(new ClaimsIdentity(new[] { claim }, CookieAuthenticationDefaults.AuthenticationScheme));

                        context.User = principle;

                        await context.SignInAsync(principle);
                    }
                }

                await _next(context);
            }
            catch (Exception e)
            {
                Log.Fatal("Error in Authentication",e);
                throw;
            }
           
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {

        }

        private static bool IsProtectedAction(HttpContext context)
        {
            var endpoint = context.GetEndpoint();

            // Action Level

            var isActionAllowAnonymous = endpoint?.Metadata?.GetMetadata<AllowAnonymousAttribute>() != null;

            if (isActionAllowAnonymous)
            {
                return false;
            }

            var isAuthenRequire = endpoint?.Metadata?.GetMetadata<AuthorizeAttribute>() != null;

            if (isAuthenRequire)
            {
                return true;
            }

            return false;
        }

        public string ValidateJwtToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            string key = _configuration.GetValue<string>("Setting:JwtTokenSecret");

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = securityKey,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

                var userId = jwtToken.Claims.First(x => x.Type == JwtRegisteredClaimNames.Sid).ToString();

                // return user id from JWT token if validation successful
                return userId;
            }
            catch (Exception ex)
            {
                // return null if validation fails
                return null;
            }
        }
    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using DineConnect.Gateway.Application;
using DineConnect.Gateway.Application.LogFileProvider;
using DineConnect.Gateway.Application.LogFileReader;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using DineConnect.Gateway.EntityFramework.Repository.Impl;
using DineConnect.Gateway.Web.Filters;
using Microsoft.AspNetCore.Identity;

namespace DineConnect.Gateway.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<GatewayUser, GatewayRole>()
                .AddEntityFrameworkStores<DineConnectGatewayDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(1);
                options.LoginPath = "/Account/Login";
                options.SlidingExpiration = true;
            });

            services.AddControllersWithViews(option =>
            {
                option.EnableEndpointRouting = true;
            }).AddNewtonsoftJson();

            // DI
            services.AddScoped<ILogFileReader, DefaultLogFileReader>();
            services.AddScoped<ILogFileProvider>(x => ActivatorUtilities.CreateInstance<DefaultLogFileProvider>(x, @"D:\Home\LogFiles\"));
            services.AddScoped<DineConnectGatewayDbContext, DineConnectGatewayDbContext>();
            services.AddScoped<IRepository<Brand>, BrandsRepository>();
            services.AddScoped<IRepository<Location>, LocationsRepository>();
            services.AddScoped<IRepository<ExternalDeliveryTicket>, ExternalTicketRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseRouting();
            app.UseMiddleware<JwtAuthorizationMiddleware>();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                #region DeliveryHero
                
                endpoints.MapControllerRoute(name: "DeliveryHeroOrderStatus",
                    pattern: "remoteId/{remoteId}/remoteOrder/{remoteOrderId}/posOrderStatus",
                    defaults: new { controller = "DeliveryHeroStatus", action = "Index" });

                endpoints.MapControllerRoute(name: "DeliveryHeroOrder",
                    pattern: "Order/{id}",
                    defaults: new { controller = "DeliveryHero", action = "Index" });

                #endregion
                #region Grab

                endpoints.MapControllerRoute(name: "GrabOrderState",
                    pattern: "grab/order/state",
                    defaults: new { controller = "Grab", action = "State" });


                endpoints.MapControllerRoute(name: "GrabOrder",
                    pattern: "grab/orders",
                    defaults: new { controller = "Grab", action = "Index" });

                
                endpoints.MapControllerRoute(name: "GrabMenu",
                    pattern: "grab/merchant/menu",
                    defaults: new { controller = "Grab", action = "Menu" });

                #endregion
                
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

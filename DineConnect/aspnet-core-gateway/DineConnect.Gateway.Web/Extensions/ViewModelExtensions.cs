﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.Web.ViewModels.Location;

namespace DineConnect.Gateway.Web.Extensions
{
    public static class ViewModelExtensions
    {
        public static List<LocationModel> ToViewModel(this List<Location> locations)
        {
            var list = new List<LocationModel>();

            foreach(var item in locations)
            {
                list.Add(item.ToViewModel());
            }

            return list;
        }

        public static LocationModel ToViewModel(this Location location)
        {
            return new LocationModel()
            {
                Id = location.Id,
                BrandId = location.BrandId,
                BrandName = location.Brand.Name,
                City = location.City,
                Code = location.Code,
                Country = location.Country,
                Name = location.Name,
                PlatformName = location.PlatformName,
                CreateDate = location.CreateDate,
                UpdateDate = location.UpdateDate
            };
        }
    }
}
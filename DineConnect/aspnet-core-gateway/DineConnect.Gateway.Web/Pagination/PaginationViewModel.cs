﻿using System;
using System.Collections.Generic;
using System.Linq;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models.Data;

namespace DineConnect.Gateway.Web.Pagination
{
    public class PaginationViewModel
    {
        public int PerPage = AppConst.DefaultPageSize;
        public int CurrentPage { get; set; }
        public int TotalCount { get; set; }

        public int PageCount()
        {
            return Convert.ToInt32(Math.Ceiling(TotalCount / (double)PerPage));
        }
    }
    public class PaginationViewModel<T>: PaginationViewModel
    {
        public IEnumerable<T> Contents { get; set; }
        
        public IEnumerable<T> PaginatedContents()  
        {  
            return Contents;  
        }  
    }
}
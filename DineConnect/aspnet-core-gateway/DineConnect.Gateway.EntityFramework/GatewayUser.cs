﻿using System;
using Microsoft.AspNetCore.Identity;

namespace DineConnect.Gateway.EntityFramework
{
    public class GatewayRole : IdentityRole
    {
        public GatewayRole()
        {
        }

        public GatewayRole(string name) : base(name) { }
    }

    public class GatewayUser : IdentityUser
    {
        public DateTime RegistrationDate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace DineConnect.Gateway.EntityFramework.Repository.Impl
{
    public class ExternalTicketRepository : IRepository<ExternalDeliveryTicket>
    {
        private readonly DineConnectGatewayDbContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;

        public ExternalTicketRepository(DineConnectGatewayDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Add(ExternalDeliveryTicket entity)
        {
            _dbContext.ExternalDeliveryTickets.Add(entity);

        }

        public Task Delete(ExternalDeliveryTicket entity)
        {
            _dbContext.ExternalDeliveryTickets.Remove(entity);
            return Task.CompletedTask;
        }

        public IQueryable<ExternalDeliveryTicket> Find(Expression<Func<ExternalDeliveryTicket, bool>> expression)
        {
            return _dbContext.ExternalDeliveryTickets.OrderBy(a => a.Id).Where(expression);
        }

        public int GetTotalRecords()
        {
            return _dbContext.ExternalDeliveryTickets.Count();

        }

        public Task<List<ExternalDeliveryTicket>> GetAll()
        {
            return _dbContext.ExternalDeliveryTickets.ToListAsync();
        }

        public async Task<List<ExternalDeliveryTicket>> GetAll(int skipRecord, int takeRecord)
        {
            var totalCount = _dbContext.ExternalDeliveryTickets.Count();
            var myResult = await _dbContext.ExternalDeliveryTickets.OrderByDescending(a=>a.Id).Skip(skipRecord).Take(takeRecord).ToListAsync();
            if (skipRecord > totalCount && totalCount!=0)
            {
                return await _dbContext.ExternalDeliveryTickets.ToListAsync();
            }
            return myResult;
        }

        public Task<ExternalDeliveryTicket> GetByIdAsync(int id)
        {
            return _dbContext.ExternalDeliveryTickets.Where(e => e.Id == id).SingleOrDefaultAsync();
        }

        public async Task Update(ExternalDeliveryTicket entity)
        {
            var brand = await GetByIdAsync(entity.Id);
            brand.UpdateDate = DateTime.Now;
        }
    }
}

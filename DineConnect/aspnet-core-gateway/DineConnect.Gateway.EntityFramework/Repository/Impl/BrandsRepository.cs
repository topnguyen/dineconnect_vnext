﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace DineConnect.Gateway.EntityFramework.Repository.Impl
{
    public class BrandsRepository : IRepository<Brand>
    {
        private readonly DineConnectGatewayDbContext _dbContext;

        public IUnitOfWork UnitOfWork => _dbContext;

        public BrandsRepository(DineConnectGatewayDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public void Add(Brand entity)
        {
            _dbContext.Brands.Add(entity);
        }

        public Task Delete(Brand entity)
        {
            _dbContext.Brands.Remove(entity);
            return Task.CompletedTask;
        }

        public IQueryable<Brand> Find(Expression<Func<Brand, bool>> expression)
        {
            return _dbContext.Brands.OrderBy(a=>a.Id).Where(expression);
        }

        public int GetTotalRecords()
        {
            return _dbContext.Brands.Count();

        }

        public async Task<List<Brand>> GetAll()
        {
            return await _dbContext.Brands.ToListAsync();
        }

        public async Task<List<Brand>> GetAll(int skipRecord, int takeRecord)
        {
            var totalCount = _dbContext.Brands.Count();
            var myResult = await _dbContext.Brands.Skip(skipRecord).Take(takeRecord).ToListAsync();
            if (skipRecord > totalCount && totalCount!=0)
            {
                return await _dbContext.Brands.ToListAsync();
            }
            return myResult;
        }

        public Task<Brand> GetByIdAsync(int id)
        {
            return _dbContext.Brands.Where(e => e.Id == id).SingleOrDefaultAsync();
        }

        public async Task Update(Brand entity)
        {
            var brand = await GetByIdAsync(entity.Id);
            brand.Code = entity.Code;
            brand.Name = entity.Name;
            brand.UpdateDate = DateTime.Now;
            brand.AddOn = entity.AddOn;
        }
    }
}

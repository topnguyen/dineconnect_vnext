﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DineConnect.Gateway.EntityFramework.Repository
{
    public interface IRepository<T>
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(T entity);
        Task Delete(T entity);
        Task Update(T entity);
        Task<List<T>> GetAll();
        Task<List<T>> GetAll(int skipRecord, int takeRecord);

        Task<T> GetByIdAsync(int id);
        IQueryable<T> Find(Expression<Func<T, bool>> expression);
        int GetTotalRecords();
    }
}

﻿using System.Threading;
using System.Threading.Tasks;

namespace DineConnect.Gateway.EntityFramework.Repository
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        void Dispose();
    }
}
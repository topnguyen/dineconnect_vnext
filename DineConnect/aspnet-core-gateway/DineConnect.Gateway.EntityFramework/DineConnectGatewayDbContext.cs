﻿using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using Elect.Core.ConfigUtils;
using Elect.Core.Constants;
using Elect.Core.EnvUtils;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DineConnect.Gateway.EntityFramework
{
    public class DineConnectGatewayDbContext : IdentityDbContext<GatewayUser, GatewayRole, string>, IUnitOfWork
    {

        public DbSet<Location> Locations { get; set; }

        public DbSet<Brand> Brands { get; set; }

        public DbSet<ExternalDeliveryTicket> ExternalDeliveryTickets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                return;
            }

            var config =
                new ConfigurationBuilder()
                    .AddJsonFile(ConfigurationFileName.ConnectionConfig, false, true)
                    .Build();

            var connectionString = config.GetValueByEnv<string>(ConfigurationSectionName.ConnectionStrings);

            optionsBuilder.UseSqlServer(connectionString, sqlServerOptionsAction =>
            {
                // Command timeout in seconds
                sqlServerOptionsAction.CommandTimeout(1 * 60);

                sqlServerOptionsAction.MigrationsAssembly(typeof(DineConnectGatewayDbContext).GetTypeInfo().Assembly.GetName().Name);

                sqlServerOptionsAction.MigrationsHistoryTable("Migration");
            });

            optionsBuilder.EnableSensitiveDataLogging(EnvHelper.IsDevelopment());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public new Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}

﻿using System.IO;
using System.Text;
using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.Application.LogFileReader
{
    public class DefaultLogFileReader : ILogFileReader
    {
        public string Read(LogFile logFile)
        {
            using (var fs = new FileStream(logFile.Location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                return sr.ReadToEnd();
            }
        }
    }
}

﻿using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.Application
{
    public interface ILogFileReader
    {
        string Read(LogFile logFile);
    }
}

﻿using System.Collections.Generic;
using System.IO;
using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.Application.LogFileProvider
{
    public class DefaultLogFileProvider : ILogFileProvider
    {
        private readonly string _loggingDir;

        public DefaultLogFileProvider(string loggingDir)
        {
            _loggingDir = loggingDir;
        }

        public string LoggingDirectory => _loggingDir;

        public List<LogFile> GetLogFiles()
        {
            string[] filePaths = Directory.GetFiles(_loggingDir);

            List<LogFile> logFiles = new List<LogFile>();

            foreach (string path in filePaths)
            {
                logFiles.Add(LogFile.Create(path));
            }

            return logFiles;
        }
    }
}

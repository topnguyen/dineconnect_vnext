﻿using System.Collections.Generic;
using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.Application
{
    public interface ILogFileProvider
    {
        string LoggingDirectory { get; }

        List<LogFile> GetLogFiles();
    }
}

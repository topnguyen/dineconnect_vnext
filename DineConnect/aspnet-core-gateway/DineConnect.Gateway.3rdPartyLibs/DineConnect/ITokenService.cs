﻿using DineConnect.Gateway._3rdPartyLibs.DineConnect.Model;

namespace DineConnect.Gateway._3rdPartyLibs.DineConnect
{
    public interface ITokenService
    {
        object GetResponse(string method, IInputDto input,ConnectAuthInputDto connectInput,  string url = "");
    }
}

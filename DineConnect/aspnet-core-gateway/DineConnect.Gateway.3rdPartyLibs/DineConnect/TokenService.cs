﻿using System;
using System.Net;
using DineConnect.Gateway._3rdPartyLibs.DineConnect.Model;
using Newtonsoft.Json;
using RestSharp;

namespace DineConnect.Gateway._3rdPartyLibs.DineConnect
{
    public class TokenService : ITokenService
    {
      

       

        public object GetResponse(string method, IInputDto input, ConnectAuthInputDto connectInput, string url = "")
        {
            try
            {
                var cUrl = connectInput.Url;
                var cTenant = connectInput.TenantName;
                var cUser = connectInput.UserName;
                var cPass = connectInput.Password;
                var tokenServer = GetToken(connectInput);
                var cTenantId = connectInput.TenantId;

                if (!string.IsNullOrEmpty(url)) cUrl = url;

                if (connectInput.SyncvNext)
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                if (!string.IsNullOrEmpty(url) || !string.IsNullOrEmpty(cUrl) && !string.IsNullOrEmpty(cTenant) &&
                    !string.IsNullOrEmpty(cUser) &&
                    !string.IsNullOrEmpty(cPass) && !string.IsNullOrEmpty(tokenServer))
                {
                    var client = new RestClient(cUrl);

                    var request = new RestRequest(method, Method.POST);
                    request.AddHeader("Abp.TenantId", cTenantId);

                    if (string.IsNullOrEmpty(url)) request.AddHeader("Authorization", $"Bearer {tokenServer}");

                    request.AddHeader("Content-Type", "application/json");
                    if (input != null)
                    {
                        var body = JsonConvert.SerializeObject(input);
                        request.AddParameter("application/json", body, ParameterType.RequestBody);
                    }

                    var response = (RestResponse) client.Execute(request);

                    if (ValidateRestResponse(response))
                    {
                        var connectResponse = JsonConvert.DeserializeObject<ConnectResponse>(response.Content);
                        if (ValidateConnectResponse(connectResponse))
                            if (connectResponse != null)
                                return connectResponse.result;
                    }
                }
            }
            catch (Exception exception)
            {
            }

            return null;
        }
        private RestRequest CreateRequest(string resource, Method method)
        {
            var request = new RestRequest(resource, method);
            request.AddHeader("Content-Type", "application/json");
            return request;
        }
        private string GetToken(ConnectAuthInputDto input)
        {
            string status = null;

            var client = new RestClient(input.Url);
            var request = CreateRequest("api/Account/Authenticate", Method.POST);
            request.RequestFormat = DataFormat.Json;

            var authRequest = new AuthRequest
            {
                tenancyName = input.TenantName,
                usernameOrEmailAddress = input.UserName,
                password = input.Password
            };

            var body = JsonConvert.SerializeObject(authRequest);
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            request.AddHeader(input.SyncvNext? "Abp-TenantId" : "Abp.TenantId",
                input.TenantId);

            var response = (RestResponse)client.Execute(request);

            if (ValidateRestResponse(response))
                try
                {
                    var authResponse = JsonConvert.DeserializeObject<ConnectResponse>(response.Content);
                    if (ValidateConnectResponse(authResponse))
                    {
                        status = authResponse.result.ToString();
                    }
                }
                catch (Exception)
                {
                    return null;
                }

            return status;
        }

        public bool ValidateConnectResponse(ConnectResponse response)
        {
            if (response.success) return true;
            return false;
        }

        public bool ValidateRestResponse(RestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK) return true;
            return false;
        }
    }
}
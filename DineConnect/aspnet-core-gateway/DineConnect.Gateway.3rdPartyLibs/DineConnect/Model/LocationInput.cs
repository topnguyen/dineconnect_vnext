﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DineConnect.Gateway._3rdPartyLibs.DineConnect.Model
{
    public class LocationInput : IInputDto
    {
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public string MerchantCode { get; set; }
        public string PartnerMerchantCode { get; set; }
    }

    public interface IInputDto
    {
    }

    public class ConnectAuthInputDto
    {
        public string Url { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TenantId { get; set; }
        public string TenantName { get; set; }
        public bool SyncvNext { get; set; }



    }
    public class AuthRequest
    {
        public string tenancyName { get; set; }
        public string usernameOrEmailAddress { get; set; }
        public string password { get; set; }
    }

    public class ConnectException
    {
        public int code { get; set; }
        public string message { get; set; }
        public string details { get; set; }
        public object validationErrors { get; set; }
    }

    public class ConnectResponse
    {
        public bool success { get; set; }
        public object result { get; set; }
        public ConnectException error { get; set; }
        public bool unAuthorizedRequest { get; set; }
    }
}

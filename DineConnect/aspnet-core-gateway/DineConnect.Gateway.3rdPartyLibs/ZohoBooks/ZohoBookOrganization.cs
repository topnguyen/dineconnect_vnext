﻿using Newtonsoft.Json;

namespace DineConnect.Gateway._3rdPartyLibs.ZohoBooks
{
    public class ZohoBookOrganization
    {
        [JsonProperty("organization_id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

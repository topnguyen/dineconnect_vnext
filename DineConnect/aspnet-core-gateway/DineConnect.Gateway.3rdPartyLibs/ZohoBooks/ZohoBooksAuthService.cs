﻿using DineConnect.Gateway._3rdPartyLibs.OAuthv2;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;

namespace DineConnect.Gateway._3rdPartyLibs.ZohoBooks
{
    public class ZohoBooksAuthService : IZohoBooksAuthService
    {
        public APIResult<List<ZohoBookOrganization>> GetOrganisations(OAuthv2AccessToken token)
        {
            RestClient client1 = new RestClient("https://books.zoho.com/api/v3/organizations");
            client1.Authenticator = new JwtAuthenticator(token.AccessToken);
            RestClient client = client1;
            RestRequest request = new RestRequest(Method.GET);
            try
            {
                IRestResponse response = client.Execute(request);
                return (response.IsSuccessful ? APIResult<List<ZohoBookOrganization>>.Success(JsonConvert.DeserializeObject<List<ZohoBookOrganization>>(JObject.Parse(response.Content)["organizations"].ToString())) : APIResult<List<ZohoBookOrganization>>.Fail(response.Content));
            }
            catch (Exception exception1)
            {
                return APIResult<List<ZohoBookOrganization>>.Fail(exception1.Message);
            }
        }
    }
}

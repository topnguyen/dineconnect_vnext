﻿using System.Collections.Generic;
using DineConnect.Gateway._3rdPartyLibs.OAuthv2;

namespace DineConnect.Gateway._3rdPartyLibs.ZohoBooks
{
    public interface IZohoBooksAuthService
    {
        APIResult<List<ZohoBookOrganization>> GetOrganisations(OAuthv2AccessToken token);
    }
}

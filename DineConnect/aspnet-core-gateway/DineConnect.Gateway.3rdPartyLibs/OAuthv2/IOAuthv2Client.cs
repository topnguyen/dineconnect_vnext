﻿namespace DineConnect.Gateway._3rdPartyLibs.OAuthv2
{
    public interface IOAuthv2Client
    {
        OAuthRequestResult<OAuthv2AccessToken> RequestAccessToken(
            string requestTokenEndpoint,
            string clientId,
            string clientSecret,
            string authorizationCode,
            string redirectUri
            );
    }
}

﻿using Newtonsoft.Json;
using RestSharp;
using System;

namespace DineConnect.Gateway._3rdPartyLibs.OAuthv2
{
    public class OAuthv2Client : IOAuthv2Client
    {
        public OAuthRequestResult<OAuthv2AccessToken> RequestAccessToken(string requestTokenEndpoint, string clientId, string clientSecret, string authorizationCode, string redirectUri)
        {
            OAuthRequestResult<OAuthv2AccessToken> result;
            RestClient client = new RestClient(requestTokenEndpoint);
            RestRequest request = new RestRequest(Method.POST);
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", authorizationCode);
            request.AddParameter("redirect_uri", redirectUri);
            request.AddParameter("client_id", clientId);
            request.AddParameter("client_secret", clientSecret);
            try
            {
                IRestResponse response = client.Execute(request);
                if (!response.IsSuccessful)
                {
                    result = OAuthRequestResult<OAuthv2AccessToken>.Fail(response.Content);
                }
                else
                {
                    OAuthv2AccessToken entity = JsonConvert.DeserializeObject<OAuthv2AccessToken>(response.Content);
                    entity.ExpiresOn = DateTime.UtcNow.AddSeconds(entity.ExpiresIn);
                    result = OAuthRequestResult<OAuthv2AccessToken>.Success(entity);
                }
            }
            catch (Exception exception1)
            {
                result = OAuthRequestResult<OAuthv2AccessToken>.Fail(exception1.Message);
            }
            return result;
        }
    }
}

﻿using System.Collections.Generic;
using DineConnect.Gateway._3rdPartyLibs.OAuthv2;

namespace DineConnect.Gateway._3rdPartyLibs.Xero
{
    public interface IXeroAuthenticationService
    {
        APIResult<List<XeroConnection>> GetXeroConnections(OAuthv2AccessToken token);
    }
}

﻿using DineConnect.Gateway._3rdPartyLibs.OAuthv2;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using DineConnect.Gateway._3rdPartyLibs.DineConnect;

namespace DineConnect.Gateway._3rdPartyLibs.Xero
{
    public class XeroAuthenticationService : IXeroAuthenticationService
    {
        public APIResult<List<XeroConnection>> GetXeroConnections(OAuthv2AccessToken token)
        {
            RestClient client1 = new RestClient("https://api.xero.com/connections");
            client1.Authenticator = new JwtAuthenticator(token.AccessToken);
            RestClient client = client1;
            RestRequest request = new RestRequest(Method.GET);
            try
            {
                IRestResponse response = client.Execute(request);
                return (response.IsSuccessful ? APIResult<List<XeroConnection>>.Success(JsonConvert.DeserializeObject<List<XeroConnection>>(response.Content)) : APIResult<List<XeroConnection>>.Fail(response.Content));
            }
            catch (Exception exception1)
            {
                return APIResult<List<XeroConnection>>.Fail(exception1.Message);
            }
        }
    }
}

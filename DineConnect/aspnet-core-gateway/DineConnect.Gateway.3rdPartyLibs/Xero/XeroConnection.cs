﻿namespace DineConnect.Gateway._3rdPartyLibs.Xero
{
    public class XeroConnection
    {
        public string TenantId { get; set; }
        public string TenantType { get; set; }
        public string TenantName { get; set; }
    }
}

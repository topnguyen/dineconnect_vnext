import { Injectable } from "@angular/core";

import { ScriptLoadingService } from "./script-loading.service";

@Injectable({
    providedIn: "root",
})
export class PaypalScriptService {
    private baseUrl: string = "https://www.paypal.com/sdk/js";
    private globalVar: string = "paypal";

    constructor(private scriptLoadingService: ScriptLoadingService) {}

    registerScript(configuration, loaded: (payPalApi: any) => void): void {
        this.scriptLoadingService.registerScript(this.getPayPalUrl(configuration), this.globalVar, loaded);
    }

    getPayPalUrl(configuration): string {
        return `${this.baseUrl}?client-id=${configuration.clientId}&currency=${configuration.currency}&locale=${configuration.locale}&components=${configuration.components.join(',')}`;
    }
}

export interface DataApproved {
    billingToken: null;
    facilitatorAccessToken: string;
    orderID: string;
    payerID: string;
    paymentID: null;
}

import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppLocalizationService } from "@app/shared/common/localization/app-localization.service";
import { DineConnectCommonModule } from "@shared/common/common.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { RouterModule } from "@angular/router";
import { AppAuthService } from "./auth/app-auth.service";
import { AppRouteGuard } from "./auth/auth-route-guard";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { MenuComponent } from "@app/shared/common/menu/menu.component";
import { TermAndConditionsComponent } from "./term-and-conditions/term-and-conditions.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { AboutUsComponent } from "./aboutUs/aboutUs.component";
import { FeedbackComponent } from "./feedback/feedback.component";
import { ContactComponent } from "./contact/contact.component";
import { FaqComponent } from "./faq/faq.component";
import { CollapseQuestionsComponent } from "./collapse-questions/collapse-questions.component";
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig } from 'ngx-bootstrap/datepicker';
import { TiffinMealTimesServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalContentComponent, PopupImageComponent } from './popup-image/popup-image.component';
import { CardIdGuiderModalComponent } from './card-id-guide-modal/card-id-guider-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        UtilsModule,
        DineConnectCommonModule,
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        RouterModule,
        AppBsModalModule,
    ],
    declarations: [
        MenuComponent,
        TermAndConditionsComponent,
        PrivacyComponent,
        AboutUsComponent,
        FeedbackComponent,
        ContactComponent,
        FaqComponent,
        CollapseQuestionsComponent,
        PopupImageComponent,
        ModalContentComponent,
        CardIdGuiderModalComponent

    ],
    exports: [
        MenuComponent,
        TermAndConditionsComponent,
        PrivacyComponent,
        AboutUsComponent,
        FeedbackComponent,
        ContactComponent,
        FaqComponent,
        PopupImageComponent,
        CollapseQuestionsComponent,
        CardIdGuiderModalComponent
    ],
    providers: [
        AppLocalizationService,
        TiffinMealTimesServiceProxy,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
    ],
})
export class AppCommonModule {
    static forRoot(): ModuleWithProviders<AppCommonModule> {
        return {
            ngModule: AppCommonModule,
            providers: [
                AppAuthService,
                AppRouteGuard
            ]
        };
    }
}

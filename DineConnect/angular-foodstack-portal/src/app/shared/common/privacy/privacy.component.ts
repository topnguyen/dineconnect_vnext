import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
  ) { 
      super(injector)
  }

  ngOnInit() {
  }

}

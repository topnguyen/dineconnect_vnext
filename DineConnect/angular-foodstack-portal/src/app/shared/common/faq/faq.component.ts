import { Component, Injector, OnInit } from '@angular/core';

import { finalize } from 'rxjs/internal/operators/finalize';

import { AppComponentBase } from '@shared/common/app-component-base';

import { FaqListDto, TiffinFaqServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
})
export class FaqComponent extends AppComponentBase implements OnInit {
    questions: FaqListDto[];

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getFaqs();
    }

    getFaqs(): void {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinFaqServiceProxy.getFaqs()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.questions = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
}

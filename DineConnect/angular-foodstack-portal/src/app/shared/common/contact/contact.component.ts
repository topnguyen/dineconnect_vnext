import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
  ) { 
      super(injector)
  }

  ngOnInit() {
  }

}

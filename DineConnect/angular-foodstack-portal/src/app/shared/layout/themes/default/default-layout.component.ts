import { Injector, Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import { Router } from '@angular/router';

@Component({
    templateUrl: './default-layout.component.html',
    styleUrls: ['./default-layout.component.scss'],
    selector: 'default-layout',
    animations: [appModuleAnimation()]
})
export class DefaultLayoutComponent extends ThemesLayoutBaseComponent implements OnInit {
    cartProductsCount = 0;

    constructor(injector: Injector, private _router: Router) {
        super(injector);
    }

    ngOnInit() {
        this.registerToEvents();
    }

    registerToEvents() {
        abp.event.on('app.chat.cartProductsCountChanged', (cartProductsCount) => {
            this.cartProductsCount = cartProductsCount;
        });
    }
}

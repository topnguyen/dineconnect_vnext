import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ChatSignalrService } from '@app/shared/layout/chat/chat-signalr.service';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploadModule as PrimeNgFileUploadModule } from 'primeng/fileupload';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultLayoutComponent } from './shared/layout/themes/default/default-layout.component';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppCommonModule } from './shared/common/app-common.module';
import { ChatBarComponent } from './shared/layout/chat/chat-bar.component';
import { ChatFriendListItemComponent } from './shared/layout/chat/chat-friend-list-item.component';
import { ChatMessageComponent } from './shared/layout/chat/chat-message.component';
import { FooterComponent } from './shared/layout/footer.component';
import { TopBarComponent } from './shared/layout/topbar.component';
import { TextMaskModule } from 'angular2-text-mask';
// import { CoreModule } from "@metronic/app/core/core.module";
import { NgxSpinnerModule, NgxSpinnerComponent } from 'ngx-spinner';
import { OAuthModule } from 'angular-oauth2-oidc';
import {
    DefaultLocationServiceProxy,
    TimeSettingsServiceProxy,
    AddressesServiceProxy,
    UserDelegationServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ProductService } from './shared/services/product.service';
import { CommonModule } from '@angular/common';
import { BsDropdownDirective, BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DineConnectCommonModule } from '@shared/common/common.module';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { ChatToggleButtonComponent } from './shared/layout/topbar/chat-toggle-button.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true
};

@NgModule({
    declarations: [
        AppComponent,
        DefaultLayoutComponent,
        FooterComponent,
        ChatBarComponent,
        ChatFriendListItemComponent,
        ChatMessageComponent,
        TopBarComponent,
        ChatToggleButtonComponent
    ],
    imports: [
        AppCommonModule.forRoot(),
        AppRoutingModule,
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        // CoreModule,
        FileUploadModule,
        FormsModule,
        HttpClientJsonpModule,
        HttpClientModule,
        ModalModule.forRoot(),
        CommonModule,
        DineConnectCommonModule,
        NgxSpinnerModule,
        OAuthModule.forRoot(),
        PopoverModule.forRoot(),
        ServiceProxyModule,
        TabsModule.forRoot(),
        TextMaskModule,
        TooltipModule.forRoot(),
        UtilsModule,
        CommonModule,
        AppBsModalModule,
        CollapseModule,
        CollapseModule.forRoot(),
        PrimeNgFileUploadModule,
        PerfectScrollbarModule
    ],
    providers: [
        UserDelegationServiceProxy,
        ChatSignalrService,
        DefaultLocationServiceProxy,
        TimeSettingsServiceProxy,
        AddressesServiceProxy,
        BsDropdownDirective,
        ProductService,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ],
    entryComponents: [NgxSpinnerComponent]
})
export class AppModule {}

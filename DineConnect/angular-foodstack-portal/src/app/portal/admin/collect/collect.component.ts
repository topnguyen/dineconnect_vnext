import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinOrderServiceProxy, OrderHistoryDto, ChangePickupStatus, SendPickUpOrderRemindInput } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Paginator } from 'primeng';
import { finalize } from 'rxjs/operators';
import moment from 'moment';

@Component({
  selector: 'app-collect',
  templateUrl: './collect.component.html',
  styleUrls: ['./collect.component.scss'],
  animations: [appModuleAnimation()]
})
export class CollectComponent extends AppComponentBase implements OnInit {
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    term: string;
    isPickedUp = false;
    date: moment.Moment = moment(moment.now());

    constructor(injector: Injector, private _tiffinOrderProxy: TiffinOrderServiceProxy) {
        super(injector);
    }

    ngOnInit() {}

    getAll(event?) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._tiffinOrderProxy
            .getOrderHistory(
                undefined,
                this.isPickedUp,
                this.term,
                this.date ? moment(this.date) : undefined,
                'OrderDate DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = (result.items || []).map((item) => {
                    return {
                        ...item,
                        addOn: item.addOn ? JSON.parse(item.addOn) : [],
                        timeSlotFormTo: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null,
                        jsonDataObject: item.jsonData ? JSON.parse(item.jsonData) : {}
                    };
                });
            });
    }

    markCollected(id: number, isPickedup = true) {
        const input = new ChangePickupStatus();
        input.isPickup = isPickedup;
        input.orderId = id;
        this._tiffinOrderProxy.changePickupStatus(input).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    remind(id: number) {
        const input = new SendPickUpOrderRemindInput();
        input.orderIds = [id];

        this._tiffinOrderProxy.sendPickUpOrderRemindEmail(input).subscribe(ressult => {
            this.notify.success(this.l('SendRemindSuccessfully'))
        })
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    refresh() {
        this.term = undefined;
        this.date = undefined;
        this.isPickedUp = false;
        this.paginator.changePage(0);
    }

    isRemind(order: OrderHistoryDto) {
        return !order.pickedUpTime;
    }

}

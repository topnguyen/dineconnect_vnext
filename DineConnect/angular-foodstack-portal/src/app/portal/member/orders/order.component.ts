import { Component, OnInit, Injector, ViewChild, NgZone, Renderer2, ElementRef, ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AddonListDto,
    AddonSettingsServiceProxy,
    BalanceDetailDto,
    BillingInfo,
    CreateOrderInput,
    CreateOrderPaymentInputDto,
    CreateScheduleDto,
    CustomerCardDto,
    DayOfWeek,
    DeliveryTimeSlotDto,
    DeliveryTimeSlotServiceProxy,
    GetAllTiffinProductOffersInput,
    GetWheelPaymentMethodForViewDto,
    LocationDto,
    LocationServiceProxy,
    OmisePaymentResponseDto,
    ScheduleDetailStatus,
    ScheduleServiceProxy,
    SwipeCardServiceProxy,
    TiffinDeliveryTimeSlotType,
    TiffinMealTimeDto,
    TiffinMealTimesServiceProxy,
    TiffinMemberPortalServiceProxy,
    TiffinMemberProductOfferDto,
    TiffinOrderServiceProxy,
    TiffinProductOfferLookupTableDto,
    TiffinPromotionType,
    UpdateMyAddressInputDto,
    WheelPaymentMethodsServiceProxy,
    WheelServiceProxy
} from '@shared/service-proxies/service-proxies';
import { cloneDeep, groupBy, orderBy, sumBy, isEqual, maxBy, uniqBy } from 'lodash';
import {
    AdyenState,
    DateOfWeek,
    DateRange,
    OrderFilter,
    OrderItem,
    OrderMenu,
    PAYMENT_METHOD,
    STEP_ORDER,
    SYSTEM_NAME,
    TimeSlot,
    OrderDateList
} from '@app/portal/services/order.service';
import { Router } from '@angular/router';
import { finalize, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
import { StripeHelper } from '@shared/helpers/StripeHelper';

import AdyenCheckout from '@adyen/adyen-web';
import '@adyen/adyen-web/dist/adyen.css';
import { DataApproved, PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { ReadyToPayChangeResponse } from '@google-pay/button-angular';
import { AddonDialogComponent } from './addon-dialog/addon-dialog.component';
import { QRCodeDialogComponent } from '../cart-calculator/qr-code-dialog/qr-code-dialog.component';

declare let Stripe: any;
declare let OmiseCard: any;
declare let Omise: any;
declare const google: any;
@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss', 'order-payment-method.component.scss'],
    animations: [appModuleAnimation()]
})
export class OrderComponent extends AppComponentBase implements OnInit {
    @ViewChild('addonModal') addonModal: AddonDialogComponent;
    @ViewChild('QRCodeModal') QRCodeModal: QRCodeDialogComponent;

    orderFilter: OrderFilter = {} as OrderFilter;
    order: OrderMenu = {} as OrderMenu;
    isLoading = false;
    geocoder: any;
    dateRanges: DateRange[] = [];
    selecteProductOffer: TiffinMemberProductOfferDto;

    productOffers: TiffinMemberProductOfferDto[];
    productSets: TiffinProductOfferLookupTableDto[];
    schedules: CreateScheduleDto[];
    schedulesFilter: CreateScheduleDto[];
    mealTimes: TiffinMealTimeDto[];
    addons: AddonListDto[];
    listAddress: UpdateMyAddressInputDto[] = [];
    listLocation: LocationDto[] | any[] = [];
    deliveryTimeSlots: DeliveryTimeSlotDto[] = [];
    selfPickuptimeSlots: DeliveryTimeSlotDto[] = [];
    timeZone = abp.setting.get('TimeZone');
    isActiveDelivery = this.appSession.tenant['isActiveDelivery'];

    omiseConfig = {
        publishKey: '',
        secretKey: '',
        enablePaynowQR: false
    };
    stripeConfig = {
        secretKey: '',
        publishKey: ''
    };
    adyenConfig = {
        apiKey: '',
        merchantAccount: '',
        environment: '',
        clientKey: ''
    };
    paypalConfig = {
        clientId: '',
        clientSecret: '',
        environment: 0
    };
    googlePayConfig = {
        merchantId: '',
        merchantName: '',
        publicKey: '',
        environment: ''
    };
    omiseData = {
        label: '',
        omiseToken: '',
        omisePaynowToken: ''
    };
    stripeData = {
        label: '',
        stripeToken: ''
    };
    paypalData = {
        label: '',
        orderId: null
    };
    adyenData = {
        label: '',
        adyenEncryptedCardNumber: '',
        adyenEncryptedExpiryMonth: '',
        adyenEncryptedExpiryYear: '',
        adyenEncryptedSecurityCode: ''
    };
    googlePayData = {
        label: '',
        googlePayToken: null
    };
    paymentRequest: any;
    payPalButtonId: 'payPalButton';
    checkoutAdyen: AdyenCheckout;
    stripe: any;
    paymentMethod: string;
    serviceFeePercentage: number;
    serviceFees = [];
    paynowToken = '';
    billingInfo = new BillingInfo();
    saving = false;
    MOQItems: any[] = [];

    ScheduleDetailStatus = ScheduleDetailStatus;
    TiffinPromotionType = TiffinPromotionType;
    TiffinDeliveryTimeSlotType = TiffinDeliveryTimeSlotType;

    swipeCardList: CustomerCardDto[];
    swipeCard: string;
    swipeOTP: string;

    STEP_ORDER = STEP_ORDER;
    PAYMENT_METHOD = PAYMENT_METHOD;

    constructor(
        injector: Injector,
        public mapsApiLoader: MapsAPILoader,
        private _cdRef: ChangeDetectorRef,
        private _ngZone: NgZone,
        private _router: Router,
        private _render: Renderer2,
        private _elRef: ElementRef,
        private _tiffinMemberPortalServiceProxy: TiffinMemberPortalServiceProxy,
        private _scheduleServiceProxy: ScheduleServiceProxy,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy,
        private _tiffinOrderServiceProxy: TiffinOrderServiceProxy,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _addonSettingsServiceProxy: AddonSettingsServiceProxy,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _wheelServiceProxy: WheelServiceProxy,
        private _paypalScriptService: PaypalScriptService,
        private _swipeCardServiceProxy: SwipeCardServiceProxy
    ) {
        super(injector);

        this.orderFilter.step = STEP_ORDER.ORDER;

        this.mapsApiLoader = mapsApiLoader;
        this.mapsApiLoader.load().then(() => {
            this.geocoder = new google.maps.Geocoder();

            this._getAllAddress();
        });
    }

    ngOnInit() {
        this._getMealTimes();
        this._getGetCutOffTime();
        this._getAddons();
        this._getTimeSlots();
        this._getSelfPickupLocation();
        this._getBillingInfo();

        this._getMealPlan();
    }

    onChangeProductOffer() {
        if (this.selecteProductOffer) {
            this.orderFilter.productOfferId = this.selecteProductOffer.id;
            this.orderFilter.customerProductOfferId =
                this.selecteProductOffer && this.selecteProductOffer['balanceDetail'] && this.selecteProductOffer['balanceDetail'].customerOfferOrderId;
            this.productSets = orderBy(cloneDeep(this.selecteProductOffer.productSets) || [], 'name', 'asc');

            this.productSets.unshift({
                id: undefined,
                name: 'All Meals'
            } as any);

            this._getSchedules();
        }
    }

    onChangeProductSet(id?: number) {
        if (this.dateRanges.length == 0) {
            return;
        }

        this.orderFilter.productSetId = id;
        this.orderFilter.mealTimeId = this.mealTimes[0] ? this.mealTimes[0].id : null;

        this._filter();
    }

    onChangeDayofWeed(dayOfWeek: DateOfWeek) {
        if (!this.isDisabledDate(dayOfWeek)) {
            this.orderFilter.date = dayOfWeek.date;

            this._filter();
        }
    }

    onChangeMealTime(mealTimeId?) {
        this.orderFilter.mealTimeId = mealTimeId;

        this._filter();
    }

    onChangeTab(dateRange: DateRange) {
        const validDate = dateRange.dateOfWeeks.find((date) => !this.isDisabledDate(date));
        if (validDate) {
            this.orderFilter.date = validDate.date;
        } else {
            this.message.error('There are no valid date');
            return;
        }

        this._filter();
    }

    increaseQuantity(schedule: CreateScheduleDto) {
        schedule['quantity'] = schedule['quantity'] + 1;

        this.updateSelectedItems(schedule);
    }

    decreaseQuantity(schedule: CreateScheduleDto) {
        if (schedule['quantity'] > 1) {
            schedule['quantity'] = schedule['quantity'] - 1;
        } else {
            schedule['isSelected'] = false;
        }

        this.updateSelectedItems(schedule);
    }

    updateAddon(data: { item: OrderItem; addOn: AddonListDto[] }) {
        let orderItem = (this.order.items || []).find((item) => item.id == data.item.id);

        if (orderItem) {
            orderItem.addOn = data.addOn;
        }
    }

    showAddonModal(schedule: CreateScheduleDto) {
        const orderItem = (this.order.items || []).find((order) => order.id == schedule.id);

        this.addonModal.show(orderItem, this.addons);
    }

    increaseAddonQuantity(addon: AddonListDto) {
        addon['quantity'] = addon['quantity'] + 1;
    }

    decreaseAddonQuantity(item: OrderItem, addon: AddonListDto) {
        addon['quantity'] = addon['quantity'] - 1;

        if (addon['quantity'] == 0) {
            const index = item.addOn.findIndex((item) => item.value == addon.value);
            if (index !== -1) {
                item.addOn.splice(index, 1);
            }
        }
    }

    onChangeDeliveryType(event: { checked: boolean }, type) {
        if (event.checked) {
            this.order.deliveryType = type;
        } else {
            this.order.deliveryType = type == TiffinDeliveryTimeSlotType.Delivery ? TiffinDeliveryTimeSlotType.Delivery : TiffinDeliveryTimeSlotType.SelfPickup;
        }
    }

    onChangeDeliveryLocation(event: { checked: boolean }, address: UpdateMyAddressInputDto) {
        if (event.checked) {
            this.order.deliverylocationName = address.name;
            this.order.deliverylocation = address.addressId;

            this.order.deliveryCharge = address['deliveryFee'];
        } else {
            this.order.deliverylocationName = null;
            this.order.deliverylocation = null;

            this.order.deliveryCharge = 0;
        }
    }

    onChangeSelfPickupLocation(event: { checked: boolean }, location: LocationDto) {
        if (event.checked) {
            this.order.selfPickupLocationName = location.name;
            this.order.selfPickupLocation = location.id;
        } else {
            this.order.selfPickupLocationName = null;
            this.order.selfPickupLocation = null;
        }
    }

    onSelectDeliveryTimeSlot(orderDate: OrderDateList, timeSlot: TimeSlot) {
        orderDate.timeSlot = timeSlot as any;

        this._updatePurcharsedOrderList();
        this._updateMOQ();
    }

    onSelectSelfPickupTimeSlot(location: LocationDto, timeSlot: TimeSlot) {
        if (location.id != this.order.selfPickupLocation) {
            return;
        }

        this.order.selfPickupTimeSlot = timeSlot;
        this._updatePurcharsedOrderList();
        this._updateMOQ();
    }

    showGoogleMap(address: UpdateMyAddressInputDto) {
        let location = `${address.address1}, ${address.address2}, ${address.address3}, ${address.cityName}, ${address.countryName}`;
        location = location.replace(/, null/g, '');
        location = location.replace(/&/g, '%26');
        location = location.replace(/ /g, '+');

        const url = `https://www.google.com/maps/place/${location}`;

        const win = window.open(url, '_blank');
        win && win.focus();
    }

    showGoogleMapDirection(location: LocationDto) {
        var address = `${location.address1}, ${location.address2}, ${location.address3}, ${location.cityName}, ${location.countryName}`;
        address = address.replace(/, null/g, '');
        address = address.replace(/&/g, '%26');
        address = address.replace(/ /g, '+');

        const url = `https://www.google.com/maps?saddr=My+Location&daddr=${address}`;
        const win = window.open(url, '_blank');
        win && win.focus();
    }

    isDisabledDate(dateOfWeek: DateOfWeek) {
        if (dateOfWeek.date ) {
            const currentTime = Number(moment().hour().toString() + (moment().minute() < 10 ? '0' + moment().minute() : moment().minute().toString()));

            if (dateOfWeek.date.format('dddd') == 'Sunday' || dateOfWeek.date.format('DDMMYYYY') == moment().format('DDMMYYYY')) {
                return true;
            }

            if (currentTime > this.orderFilter.cutOffTime && dateOfWeek.date.format('DDMMYYYY') == moment().add(1, 'day').format('DDMMYYYY')) {
                return true;
            }

            return dateOfWeek.date.isSameOrBefore(this.orderFilter.cutOffDate);
        } else {
            return false;
        }
    }

    choosepaymentMethod() {
        this._getServiceCharge(this.paymentMethod);

        let els = this._elRef.nativeElement.querySelectorAll('div.payment-method');
        els.forEach((el) => {
            this._render.removeClass(el, 'selected');
        });

        if (this.paymentMethod === PAYMENT_METHOD.Omise || this.paymentMethod === PAYMENT_METHOD.Adyen) {
            els.forEach((el) => {
                this._render.addClass(el, 'selected');
            });
        }
    }

    payWithAdyen() {
        this._createOrderList(PAYMENT_METHOD.Adyen);
    }

    payWithWallet() {
        if (this.swipeCard && this.swipeOTP) {
            this._swipeCardServiceProxy.validateCardOTP(this.swipeCard, this.swipeOTP).subscribe((result) => {
                this._createOrderList(PAYMENT_METHOD.Swipe);
            });
        }
    }

    sendOTP() {
        if (this.swipeCard) {
            this._swipeCardServiceProxy.generateCardOTP(this.swipeCard).subscribe((result) => {
                this.notify.success('Sent Successfully');
            });
        } else {
            this.notify.error('Please select Card Number');
        }
    }

    increaseItemQuantity(item: CreateOrderInput) {
        let schedule = this.schedules.find((s) => s.id == item.id);

        this.increaseQuantity(schedule);
        this._updateMOQ();
    }

    decreaseItemQuantity(item: OrderItem) {
        let schedule = this.schedules.find((s) => s.id == item.id);
        this.decreaseQuantity(schedule);
        this._updateMOQ();
    }

    onDelivery() {
        if (!this.order.items || this.order.items.length == 0) {
            this.message.info('Order', 'Should order at least one set');
            return;
        }

        this.orderFilter.step = STEP_ORDER.DELIVERY;
        this.order.deliveryType = this.order.deliveryType || TiffinDeliveryTimeSlotType.SelfPickup;

        this._initTimeSlot();

        this.MOQItems.length = 0;
        this._updatePurcharsedOrderList();

        document.getElementsByClassName('site_body')[0].scrollTo(0, 0);
    }

    onPlace() {
        if (
            (this.order.items || []).length == 0 ||
            (!this.order.deliverylocation && !this.order.selfPickupLocation) ||
            this.isInvalidTimeSlot ||
            this.isInvalidMOQ
        ) {
            this.message.error('Invalid Order! Please check again');
        }
        this.saving = true;
        this._updatePurcharsedOrderList();
        this.saving = false;

        if (this.amount > 0) {
            this.orderFilter.step = STEP_ORDER.PAYMENT;

            this._getPaymentMethods();
            this._getSwipeCardList();
        } else {
            this._createOrderList();
        }
        document.getElementsByClassName('site_body')[0].scrollTo(0, 0);
    }

    back() {
        if (this.orderFilter.step == STEP_ORDER.DELIVERY) {
            this.orderFilter.step = STEP_ORDER.ORDER;
        }

        if (this.orderFilter.step == STEP_ORDER.PAYMENT) {
            this.orderFilter.step = STEP_ORDER.DELIVERY;
        }
    }

    onLoadGooglePaymentData = (event: CustomEvent<google.payments.api.PaymentData>): void => {
        console.log('load payment data', event.detail);
        this.googlePayData.googlePayToken = event?.detail?.paymentMethodData?.tokenizationData?.token;

        this._createOrderList(PAYMENT_METHOD.Google);
    };

    onGooglePayError = (event: ErrorEvent): void => {
        console.error('error', event.error);
    };

    onGooglePaymentDataAuthorized: google.payments.api.PaymentAuthorizedHandler = (paymentData) => {
        console.log('payment authorized', paymentData);

        return {
            transactionState: 'SUCCESS'
        };
    };

    onReadyToPayChange = (event: CustomEvent<ReadyToPayChangeResponse>): void => {
        console.log('ready to pay change', event.detail);
    };

    updateSelectedItems(schedule: CreateScheduleDto) {
        this.order.items = [];

        this.schedules.forEach((s) => {
            if (s.id === schedule.id) {
                s['isSelected'] = schedule['isSelected'];
                s['quantity'] = s['isSelected'] ? schedule['quantity'] : 1;
            }

            if (s['isSelected']) {
                this.order.items.push(s as any);
            }
        });

        /* filter if change quantity from summary */
        this._filter();

        const countOrder = this.order.purcharsedOrderList?.length;
        this._updatePurcharsedOrderList();

        /* update orderDateList if remove order Item */
        if (this.orderFilter.step != STEP_ORDER.ORDER && countOrder != this.order.purcharsedOrderList?.length) {
            this._generateOrderDateList();
        }
    }

    totalMealQuantityForOrder(purcharsedOrder: CreateOrderInput) {
        return purcharsedOrder.quantity;
    }

    onCloseQRCodeModal(response: OmisePaymentResponseDto) {
        if (response.isSuccess) {
            this.omiseData.omisePaynowToken = response.paymentId;
            this._createOrderList(PAYMENT_METHOD.Paynow);
        }
    }

    private _updatePurcharsedOrderList() {
        this.order.purcharsedOrderList = this._transformDataToPurcharsedOrderList();

        if (this.balanceMeal < 0) {
            this.message.error('Exceed maximum meals allowed , Please reduce some meals to continue process');
        }
    }

    private _updateMOQ() {
        if (!this.minimumOrderQuantity) {
            return;
        }

        this.MOQItems.length = 0;
        this.order.purcharsedOrderList.forEach((item) => {
            const MOQItem = this.MOQItems.find(
                (moq) =>
                    moq.orderDate.format('DDMMYYY') == item.orderDate.format('DDMMYYY') &&
                    moq.mealTimeId == item.mealTimeId &&
                    isEqual(moq.timeSlotFormTo, item.timeSlotFormTo)
            );
            if (!MOQItem) {
                this.MOQItems.push({
                    ...item,
                    moqTotal: item.quantity
                });
            } else {
                MOQItem['moqTotal'] += item.quantity;
            }
        });
    }

    private _generateOrderDateList() {
        if (this.order.deliveryType == TiffinDeliveryTimeSlotType.Delivery) {
            this.order.orderDateList = this.order.purcharsedOrderList.map((order) => {
                return {
                    date: order.orderDate,
                    productSetId: order.productSetId,
                    productSetName: order.productSetName,
                    mealTimeId: order.mealTimeId,
                    timeSlots: this._getTimeSlotsForDiliveryDate(order),
                    timeSlot: {},
                    mealTimeName: order.mealTimeString,
                    id: order.orderDate.format('DDMMYYYY')
                };
            });
        }
    }

    private _generateSeflPickUpTimeSlots() {
        this.listLocation.forEach((location: LocationDto | any) => {
            let timeSlots = (this.selfPickuptimeSlots || []).filter((slot) => {
                return location.id == slot.locationId;
            });

            timeSlots = timeSlots ? timeSlots.map((slot) => JSON.parse(slot.timeSlots.toLowerCase())) : [];
            location['timeSlots'] = timeSlots;
        });
    }

    private _initData() {
        this.selecteProductOffer = this.productOffers[0];
        this.onChangeProductOffer();
    }

    private _getPaymentMethods() {
        this._wheelPaymentMethodsServiceProxy.getAll(undefined, undefined, undefined, undefined, -1, -1, -1, -1, 1, undefined, 0, 1000).subscribe((result) => {
            this._getSettings(result.items);
            this._initPayment();
        });
    }

    private _getSettings(items: GetWheelPaymentMethodForViewDto[]) {
        items.map((item) => {
            switch (item.systemName) {
                case SYSTEM_NAME.Omise:
                    this.omiseConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Stripe:
                    this.stripeConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Adyen:
                    this.adyenConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.PayPal:
                    this.paypalConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.GooglePay:
                    this.googlePayConfig = item.dynamicFieldData;
                    break;

                default:
                    break;
            }
        });
    }

    private _initPayment() {
        this.stripeData.label = this.l('OnlinePayment') + ' - ' + this.l('Stripe');
        this.omiseData.label = this.l('OnlinePayment') + ' - ' + this.l('Omise');
        this.adyenData.label = this.l('OnlinePayment') + ' - ' + this.l('Adyen');
        this.paypalData.label = this.l('OnlinePayment') + ' - ' + this.l('PayPal');
        this.googlePayData.label = this.l('OnlinePayment') + ' - ' + this.l('GooglePay');

        if (this.stripeConfig?.publishKey) {
            this.initStripeElements();
        }
        if (this.omiseConfig?.publishKey) {
            this.initOmiseElements();
        }
        if (this.adyenConfig?.apiKey) {
            this.initAdyenElements();
        }
        if (this.paypalConfig?.clientId) {
            this.initPayPalElements();
        }
        if (this.googlePayConfig?.merchantId) {
            this.initGooglePayElements();
        }
    }

    private initPayPalElements() {
        const config = {
            clientId: this.paypalConfig.clientId,
            currency: this.appSession.application.currency,
            locale: 'en_SG',
            components: ['buttons', 'funding-eligibility']
        };
        this._paypalScriptService.registerScript(config, (payPal: any) => {
            this.initPayPalConfig(payPal);
        });
    }

    private initPayPalConfig(payPal) {
        const self = this;
        this._ngZone.runOutsideAngular(() => {
            payPal
                .Buttons({
                    fundingSource: payPal.FUNDING.PAYPAL,
                    createOrder: (data: any, actions: { order: any }) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        currency_code: this.appSession.application.currency,
                                        value: this.totalAmount
                                    }
                                }
                            ]
                        });
                    },
                    onApprove: (data: DataApproved, actions: any) => {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function (details) {
                            // This function shows a transaction success message to your buyer.

                            self.paypalData.orderId = data.orderID;
                            self._ngZone.run(() => {
                                self._createOrderList(PAYMENT_METHOD.PayPal);
                            });
                        });
                    }
                })
                .render(`#${this.payPalButtonId}`);
        });
    }

    private initGooglePayElements() {
        this.paymentRequest = {
            environment: this.googlePayConfig.environment,
            buttonType: 'buy',
            buttonColor: 'default',
            apiVersion: 2,
            apiVersionMinor: 0,
            allowedPaymentMethods: [
                {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
                        allowedCardNetworks: ['MASTERCARD', 'VISA']
                    },
                    tokenizationSpecification: {
                        type: 'DIRECT',
                        parameters: {
                            protocolVersion: 'ECv2',
                            publicKey: this.googlePayConfig.publicKey
                        }
                    }
                }
            ],
            merchantInfo: {
                merchantId: this.googlePayConfig.merchantId,
                merchantName: this.googlePayConfig.merchantName
            }
        };
    }

    private initStripeElements() {
        if (this.stripeConfig.publishKey) {
            this.stripe = Stripe(this.stripeConfig.publishKey);

            let elements = this.stripe.elements({
                fonts: [
                    {
                        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro'
                    }
                ]
            });

            let elementStyles = {
                base: {
                    color: '#32325D',
                    fontWeight: 500,
                    fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',

                    '::placeholder': {
                        color: '#CFD7DF'
                    },
                    ':-webkit-autofill': {
                        color: '#e39f48'
                    }
                },
                invalid: {
                    color: '#E25950',

                    '::placeholder': {
                        color: '#FFCCA5'
                    }
                }
            };

            let elementClasses = {
                focus: 'focused',
                empty: 'empty',
                invalid: 'invalid'
            };

            let cardNumber = elements.create('cardNumber', {
                style: elementStyles,
                classes: elementClasses
            });
            cardNumber.mount('#stripe-card-number');

            let cardExpiry = elements.create('cardExpiry', {
                style: elementStyles,
                classes: elementClasses
            });
            cardExpiry.mount('#stripe-card-expiry');

            let cardCvc = elements.create('cardCvc', {
                style: elementStyles,
                classes: elementClasses
            });

            cardCvc.mount('#stripe-card-cvc');

            let self = this;
            StripeHelper.registerElements(this.stripe, [cardNumber, cardExpiry, cardCvc], 'stripe-check-out-container', function (token) {
                self.stripeData.stripeToken = token.id;
                self._createOrderList(PAYMENT_METHOD.Stripe);
            });
        }
    }

    private initOmiseElements() {
        OmiseCard.configure({
            publicKey: this.omiseConfig.publishKey
        });
        Omise.setPublicKey(this.omiseConfig.publishKey);

        let button = document.querySelector('#checkoutButton');
        if (!button) {
            return;
        }

        button.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this._isValidBillingInformation()) {
                return;
            }

            let self = this;

            OmiseCard.open({
                amount: this.totalAmount * 100,
                currency: this.appSession.application.currency,
                defaultPaymentMethod: 'credit_card',
                onCreateTokenSuccess: (nonce) => {
                    if (nonce.startsWith('tokn_')) {
                        this.omiseData.omiseToken = nonce;

                        self._createOrderList(PAYMENT_METHOD.Omise);
                    }
                }
            });
        });

        let myButton = document.querySelector('#payNow');
        if (!myButton || !this.omiseConfig?.enablePaynowQR) {
            return;
        }

        myButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this.isValidBillingInformation()) {
                return;
            }

            this._tiffinMemberPortalServiceProxy
                .createOmisePaymentNowRequest(this.totalAmount * 100, this.appSession.application.currency)
                .subscribe((result) => {
                    console.log(result);
                    const response = result ? JSON.parse(result) : null;

                    if (response && response.source && response.source.scannable_code && response.source.scannable_code.image) {
                        this.QRCodeModal.show(response.source.scannable_code.image.download_uri, response.id, this.totalAmount);
                    }
                });
        });
    }

    private isValidBillingInformation() {
        return true;
    }

    private initAdyenElements() {
        const configuration = {
            clientKey: this.adyenConfig.clientKey,
            locale: 'en-US',
            environment: this.adyenConfig.environment,
            amount: {
                currency: this.appSession.application.currency,
                value: this.totalAmount * 100
            },
            countryCode: 'SG'
        };

        this.checkoutAdyen = new AdyenCheckout(configuration);
        this.checkoutAdyen
            .create('securedfields', {
                // Optional configuration
                type: 'card',
                brands: ['mc', 'visa', 'amex', 'bcmc', 'maestro'],
                styles: {
                    base: {
                        color: 'black',
                        fontSize: '14px',
                        fontFamily: 'Helvetica'
                    },
                    error: {
                        color: 'red'
                    },
                    validated: {
                        color: 'green'
                    },
                    placeholder: {
                        color: '#d8d8d8'
                    }
                },
                ariaLabels: {
                    lang: 'en-GB',
                    encryptedCardNumber: {
                        label: 'Credit or debit card number field'
                    }
                },
                // Events
                onChange: (state: AdyenState, component) => {
                    const data = state.data.paymentMethod;

                    if (state?.data?.paymentMethod) {
                        this.adyenData.adyenEncryptedCardNumber = data.encryptedCardNumber;
                        this.adyenData.adyenEncryptedExpiryMonth = data.encryptedExpiryMonth;
                        this.adyenData.adyenEncryptedExpiryYear = data.encryptedExpiryYear;
                        this.adyenData.adyenEncryptedSecurityCode = data.encryptedSecurityCode;
                    }
                }
            })
            .mount('#customCard-container');
    }

    private _getServiceCharge(name) {
        this.serviceFeePercentage = 0;
        this._addonSettingsServiceProxy.getServiceCharge(name).subscribe((result) => {
            this.serviceFees = result || [];
            this.serviceFees.map((item) => {
                this.serviceFeePercentage = this.serviceFeePercentage + item.percentage;
            });
        });
    }

    private _getMealPlan() {
        const isExpiryOffer = false;
        let productOffersOrigin: TiffinMemberProductOfferDto[] = [];
        let balances: BalanceDetailDto[] = [];

        this.spinnerService.show();
        this._tiffinMemberPortalServiceProxy
            .getActiveOffers(new GetAllTiffinProductOffersInput())
            .pipe(
                switchMap((result) => {
                    productOffersOrigin = result.items || [];

                    return this._tiffinMemberPortalServiceProxy.getBalancesForCustomer(undefined, isExpiryOffer, undefined, undefined, 0, 1000);
                }),
                finalize(() => this.spinnerService.hide())
            )
            .subscribe((result) => {
                const listOfBalance = result.items || [];
                const balanceGroup = groupBy(listOfBalance, 'offerId');

                for (const offerId in balanceGroup) {
                    if (Object.prototype.hasOwnProperty.call(balanceGroup, offerId)) {
                        let balancesByofferId: BalanceDetailDto[] = balanceGroup[offerId];

                        balancesByofferId = orderBy(balancesByofferId, ['paymentDate'], 'desc');
                        const totalCreditBalance = sumBy(balancesByofferId, 'creditBalance');
                        const selectedBalance = {
                            ...balancesByofferId[0],
                            creditBalance: totalCreditBalance
                        } as BalanceDetailDto;
                        balances.push(selectedBalance);
                    }
                }

                this.productOffers = balances.map((item) => {
                    const productOffer = productOffersOrigin.find((po) => po.id == item.offerId) || {};

                    return {
                        ...productOffer,
                        balanceDetail: item
                    } as any;
                });

                if (this.productOffers.length > 0) {
                    this._initData();
                } else {
                    this._showConfirmOrderMealPlan();
                }
            });
    }

    private _showConfirmOrderMealPlan() {
        this.message.confirm('Do you want to order a plan ?', 'There is no active meal plan in your account', (result) => {
            if (result) {
                this._router.navigateByUrl('app/portal/member/buy-offer');
            } else {
                this._router.navigateByUrl('');
            }
        });
    }

    private _getSchedules() {
        this.isLoading = true;
        this.spinnerService.show();
        this._scheduleServiceProxy
            .getAll(undefined, undefined, moment().startOf('isoWeek'), undefined, ScheduleDetailStatus.Published, undefined, 0, 1000)
            .pipe(
                finalize(() => {
                    this.isLoading = false;
                    this.spinnerService.hide();
                })
            )
            .subscribe((result) => {
                this.order.items = [];
                this.schedules = cloneDeep(result.items).filter((item) => this.productSets.some((e) => e.id == item.productSetId));
                this._initScheduleList();
                this._initCalendar();
                this._initDateRange();

                this.onChangeProductSet(this.productSets[0].id);
            });
    }

    private _getTimeSlotsForDiliveryDate(order: CreateOrderInput) {
        const timeSlots = (this.deliveryTimeSlots || []).filter((slot) => {
            return DayOfWeek[slot.dayOfWeek].toLowerCase() == order.orderDate.format('dddd').toLowerCase() && slot.mealTimeId == order.mealTimeId;
        });
        return timeSlots ? timeSlots.map((slot) => JSON.parse(slot.timeSlots.toLowerCase())) : [];
    }

    private _initCalendar() {
        let weeks = [0];
        if (this.schedules.length > 0) {
            const lastScheduleDate = maxBy(this.schedules, (s) => s.date).date;
            const diffWeekNumber = Math.round(lastScheduleDate.diff(moment().startOf('isoWeek'), 'weeks', true)) + 1;

            weeks = [...Array(diffWeekNumber).keys()];
            if (weeks.length == 0) {
                weeks = [0];
            }
        }

        this.dateRanges = weeks.map((week, i) => {
            return {
                id: week,
                name: 'Week ' + moment.utc().add(i, 'week').week(),
                startDate: moment.utc().startOf('week').add(i, 'week').toDate(),
                endDate: moment().utc().endOf('week').add(i, 'week').subtract('1', 'day').toDate(),
                dateOfWeeks: Array.apply(null, Array(7)).map((e, j) => {
                    return {
                        timeSlots: [],
                        date: moment().utc().startOf('week').add(i, 'week').add(j, 'day')
                    };
                })
            };
        });
        this.orderFilter.dateRange = this.dateRanges[0];
    }

    private _getMealTimes() {
        this.spinnerService.show();
        this._tiffinMealTimesServiceProxy
            .getAll(undefined, undefined, 0, 1000)
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe((result) => {
                this.mealTimes = result.items;
                this.orderFilter.mealTimeId = this.mealTimes[0].id;
            });
    }

    private _filter() {
        let schedulesClone = cloneDeep(this.schedules) || [];

        if (this.orderFilter.productSetId) {
            schedulesClone = schedulesClone.filter((item) => item.productSetId === this.orderFilter.productSetId);
        }

        if (this.orderFilter.dateRange) {
            moment(this.orderFilter.dateRange.endDate);
            schedulesClone = schedulesClone.filter(
                (item) =>
                    item.date.isSameOrAfter(moment(this.orderFilter.dateRange.startDate)) ||
                    item.date.isSameOrBefore(moment(this.orderFilter.dateRange.endDate))
            );
        }

        if (this.orderFilter.mealTimeId) {
            schedulesClone = schedulesClone.filter((item) => item.mealTimeId === this.orderFilter.mealTimeId);
        }

        if (this.orderFilter.date) {
            const filterDate = this.orderFilter.date.format('DDMMYYYY');
            schedulesClone = schedulesClone.filter((item) => item.date.format('DDMMYYYY') == filterDate);
        }

        if (this.expiryDate) {
            schedulesClone = schedulesClone.filter((item) => item.date.isSameOrBefore(this.expiryDate));
        }

        this.schedulesFilter = schedulesClone;
    }

    private _getAddons() {
        this._tiffinOrderServiceProxy.getAllOrderTags().subscribe((result) => {
            this.addons = result.items;
        });
    }

    private _getAllAddress() {
        this._tiffinMemberPortalServiceProxy.getAllAddress().subscribe((listAddress) => {
            this.listAddress = listAddress || [];
            this.listAddress.forEach((address) => {
                address.address2 = this._getAddress2(address);

                let location = `${address.address1}, ${address.address2}, ${address.address3}, ${address.cityName}, ${address.countryName}`;
                location = location.replace(/&/g, '%26');
                location = location.replace(/ /g, '+');

                const addressDefault = this.listAddress.find((item) => item.isDefault) || this.listAddress[0];
                this.onChangeDeliveryLocation({ checked: true }, addressDefault);

                this._getLocation(location).subscribe((position) => {
                    if (position.lat() && position.lng()) {
                        return this._wheelServiceProxy.getDeliveryZonesContainedPoint(position.lng(), position.lat()).subscribe((result) => {
                            if (result && result.length > 0) {
                                address['deliveryFee'] = result[0].deliveryFee;

                                if (address.isDefault) {
                                    this.order.deliveryCharge = address['deliveryFee'];
                                }
                            }
                        });
                    } else {
                        this.message.error('Delivery address do not suported');
                    }
                });
            });
        });
    }

    private _getAddress2(addressDetail: UpdateMyAddressInputDto): string {
        if (addressDetail.address2) {
            let addressSpilit = addressDetail.address2.split(',');
            let floorNo = '';
            let unitNo = '';
            let tower = '';
            if (addressSpilit[0]) {
                floorNo = addressSpilit[0];
            }
            if (addressSpilit[1]) {
                unitNo = addressSpilit[1];
            }
            if (addressSpilit[2]) {
                tower = addressSpilit[2];
            }
            let address2 = [];
            address2.push(floorNo);
            address2.push(unitNo);
            address2.push(tower);
            return address2.filter((x) => typeof x === 'string' && x.length > 0).join();
        }
    }

    private _getTimeSlots() {
        this.isLoading = true;
        if (this.isActiveDelivery) {
            this._deliveryTimeSlotServiceProxy
                .getAll(undefined, TiffinDeliveryTimeSlotType.Delivery, 'id', 0, 1000)
                .pipe(finalize(() => (this.isLoading = false)))
                .subscribe((result) => {
                    this.deliveryTimeSlots = result.items;

                    this._generateOrderDateList();
                });
        }

        this._deliveryTimeSlotServiceProxy
            .getAll(undefined, TiffinDeliveryTimeSlotType.SelfPickup, 'id', 0, 1000)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.selfPickuptimeSlots = result.items;

                this._generateSeflPickUpTimeSlots();
            });
    }

    private _getSelfPickupLocation() {
        this._locationServiceProxy.getList(undefined, false, 0, undefined, 1000, 0).subscribe((result) => {
            this.listLocation = result.items || [];
            if (this.listLocation.length > 0) {
                this.order.selfPickupLocationName = this.listLocation[0].name;
                this.order.selfPickupLocation = this.listLocation[0].id;
            }
        });
    }

    private _transformDataToPurcharsedOrderList() {
        const purcharsedOrderList: CreateOrderInput[] = [];

        this.order.items.forEach((item) => {
            purcharsedOrderList.push({
                customerProductOfferId: item.customerProductOfferId,
                productSetId: item.productSetId,
                productSetName: item.productSetName,
                orderDate: item.date,
                amount: this._getAmountOrderItem(item),
                quantity: item.quantity,
                addOn: item.addOn ? JSON.stringify(item.addOn) : null,
                customerAddressId: this.order.deliveryType == TiffinDeliveryTimeSlotType.Delivery ? this.order.deliverylocation : null,
                mealTimeId: item.mealTimeId,
                mealTimeString: item.mealTimeName,
                paymentId: this.order.paymentId,
                deliveryType: this.order.deliveryType,
                deliveryCharge: this.order.deliveryCharge,
                timeSlotFormTo: this.getTimeSlotForPurcharsedOrder(item),
                selfPickupLocationId: this.order.deliveryType == TiffinDeliveryTimeSlotType.SelfPickup ? this.order.selfPickupLocation : null
            } as CreateOrderInput);
        });

        return purcharsedOrderList;
    }

    private getTimeSlotForPurcharsedOrder(item: OrderItem) {
        if (this.order.deliveryType == TiffinDeliveryTimeSlotType.Delivery) {
            const orderDateListItem = (this.order.orderDateList || []).find(
                (e) => e.date.format('DDMMYYYY') == item.date.format('DDMMYYYY') && e.mealTimeId == item.mealTimeId && e.productSetId == item.productSetId
            );
            return orderDateListItem ? orderDateListItem.timeSlot : null;
        } else {
            return this.order.selfPickupTimeSlot;
        }
    }

    private _isValidBillingInformation() {
        return true;
    }

    private _getLocation(address: string): Observable<any> {
        return new Observable((observer) => {
            this.geocoder.geocode(
                {
                    address: address
                },
                (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        observer.next(results[0].geometry.location);
                        observer.complete();
                    } else {
                        observer.error();
                    }
                }
            );
        });
    }

    private _createOrderList(paymentOption?: string) {
        this.saving = true;
        let request = new CreateOrderPaymentInputDto();
        request.purcharsedOrderList = this.order.purcharsedOrderList.map((item) => {
            item.timeSlotFormTo = JSON.stringify(item.timeSlotFormTo);

            return CreateOrderInput.fromJS(item);
        });

        request.paymentOption = paymentOption || null;
        request.source = '101';
        request.billingInfo = this.billingInfo;
        request.paidAmount = this.totalAmount;
        request.callbackUrl = location.origin + '/app/portal/member/payment-order-completed/';

        switch (paymentOption) {
            case PAYMENT_METHOD.Omise:
                {
                    request.omiseToken = this.omiseData.omiseToken;
                }

                break;
            case PAYMENT_METHOD.Paynow:
                {
                    request.omiseToken = this.omiseData.omisePaynowToken;
                }

                break;
            case PAYMENT_METHOD.Stripe:
                {
                    request.stripeToken = this.stripeData.stripeToken;
                }

                break;
            case PAYMENT_METHOD.Adyen:
                {
                    request.adyenEncryptedCardNumber = this.adyenData.adyenEncryptedCardNumber;
                    request.adyenEncryptedExpiryMonth = this.adyenData.adyenEncryptedExpiryMonth;
                    request.adyenEncryptedExpiryYear = this.adyenData.adyenEncryptedExpiryYear;
                    request.adyenEncryptedSecurityCode = this.adyenData.adyenEncryptedSecurityCode;
                }

                break;
            case PAYMENT_METHOD.PayPal:
                {
                    request.paypalOrderId = this.paypalData.orderId;
                }

                break;
            case PAYMENT_METHOD.Google:
                {
                    request.googlePayToken = this.googlePayData.googlePayToken;
                }

                break;

            case PAYMENT_METHOD.Swipe:
                {
                    request.swipeCardNumber = this.swipeCard;
                    request.swipeOTP = this.swipeOTP;
                }

                break;
        }
        const orderDates = this.order.purcharsedOrderList.map((item) => item.orderDate);
        this._tiffinOrderServiceProxy
            .validateCutOff(orderDates)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result) => {
                this.saving = true;
                this._tiffinOrderServiceProxy
                    .createPaymentForOrder(request)
                    .pipe(finalize(() => (this.saving = false)))
                    .subscribe((result) => {
                        if (this.amount == 0) {
                            this.message.success(this.l('SavedSuccessfully'), '', {
                                timer: 700
                            });

                            setTimeout(() => {
                                this._router.navigate(['/app/portal/member/order-history']);
                            }, 700);
                        } else {
                            if (result.purchasedSuccess) {
                                this.order.paymentId = result.paymentId;

                                this.message.success(this.l('PaymentedSuccessfully'), '', {
                                    timer: 1500
                                });

                                setTimeout(() => {
                                    this._router.navigate(['/app/portal/member/order-history']);
                                }, 700);
                            } else if (!result.purchasedSuccess && result.authenticateUri != null) {
                                this.message.warn(this.l('Redirecting'), '', {
                                    timer: 500
                                });
                                window.open(result.authenticateUri, '_self');
                            } else if (!result.purchasedSuccess) {
                                this.message.warn(result.message);
                            }
                        }
                    });
            });
    }

    private _getBillingInfo() {
        this._tiffinMemberPortalServiceProxy.getMyInfo().subscribe((myInfo) => {
            this.billingInfo.init(myInfo);
        });
    }

    private _getGetCutOffTime() {
        this._tiffinOrderServiceProxy.getCutOffTime().subscribe((result) => {
            if (result) {
                this.orderFilter.cutOffTime = Number(result.hour().toString() + (result.minute() < 10 ? '0' + result.minute() : result.minute().toString()));
                this.orderFilter.cutOffDate = moment([result.year(), result.month(), result.date()]);
                console.log(this.orderFilter.cutOffDate, this.orderFilter.cutOffTime);
            }
        });
    }

    private _getAmountOrderItem(item: OrderItem) {
        let total = 0;

        (item.addOn || []).forEach((addon) => {
            total = total + addon.price * addon.quantity;
        });
        return total;
    }

    private _getSwipeCardList() {
        this._swipeCardServiceProxy.getAllCustomerCard(this.appSession.customerId, undefined, undefined, 'id', 0, 1000).subscribe((result) => {
            this.swipeCardList = result.items;
        });
    }

    private _initScheduleList() {
        this.schedules.forEach((schedule) => {
            schedule['quantity'] = schedule['quantity'] || 1;
            schedule['customerProductOfferId'] = this.orderFilter.customerProductOfferId;
            schedule['productOfferId'] = this.orderFilter.productOfferId;

            schedule.products = uniqBy(schedule.products, (item) => {
                return item.productName?.toLowerCase();
            });
            schedule.products = orderBy(schedule.products, ['creationTime'], 'asc');
        });
    }

    private _initDateRange() {
        const isHasValidDate = this.dateRanges[0].dateOfWeeks.some((date) => !this.isDisabledDate(date));

        if (isHasValidDate) {
            this.orderFilter.dateRange = this.dateRanges[0];
            this.orderFilter.date = this.orderFilter.dateRange.dateOfWeeks.find((date) => !this.isDisabledDate(date)).date;
        } else {
            this.orderFilter.dateRange = this.dateRanges[1];
            this.orderFilter.date = this.orderFilter.dateRange ? this.orderFilter.dateRange.dateOfWeeks.find((date) => !this.isDisabledDate(date)).date : null;
        }
    }

    private _initTimeSlot() {
        /* init time slot */
        if (this.order.deliveryType == TiffinDeliveryTimeSlotType.SelfPickup) {
            this._generateSeflPickUpTimeSlots();
            if (this.listLocation?.length > 0) {
                this.onChangeSelfPickupLocation({ checked: true }, this.listLocation[0]);
                if (this.listLocation[0]?.timeSlots?.length > 0) {
                    this.onSelectSelfPickupTimeSlot(this.listLocation[0], this.listLocation[0]?.timeSlots[0]);
                }
            }
        } else {
            this._generateOrderDateList();
            if (this.listAddress?.length > 0) {
                this.onChangeDeliveryLocation({ checked: true }, this.listAddress[0]);
                if (this.order.orderDateList?.length > 0 && this.order.orderDateList[0].timeSlots?.length > 0) {
                    this.onSelectDeliveryTimeSlot(this.order.orderDateList[0], this.order.orderDateList[0].timeSlots[0]);
                }
            }
        }
    }

    get isInvalidTimeSlot() {
        if (this.order.deliveryType == TiffinDeliveryTimeSlotType.Delivery) {
            return (this.order.orderDateList || []).some((item) => !item.timeSlot || !item.timeSlot.from || !item.timeSlot.to);
        } else {
            return !this.order.selfPickupTimeSlot;
        }
    }

    get isInvalidMOQ() {
        let invalid = false;

        if (!this.minimumOrderQuantity) {
            return false;
        }

        for (let index = 0; index < this.MOQItems.length; index++) {
            const item = this.MOQItems[index];

            if (item.moqTotal < this.minimumOrderQuantity) {
                invalid = true;
                break;
            }
        }

        return invalid;
    }

    get total() {
        let total = 0;

        (this.order.items || []).forEach((orderItem) => {
            (orderItem.addOn || []).forEach((item) => {
                total = total + item.price * item.quantity;
            });
        });

        return total;
    }

    get amount() {
        let amount = this.total;

        if (this.order.deliveryCharge && this.order.deliveryType == TiffinDeliveryTimeSlotType.Delivery) {
            amount = amount + this.order.deliveryCharge;
        }

        return amount;
    }

    get totalAmount() {
        return this.amount + (this.amount * this.serviceFeePercentage) / 100;
    }

    get balanceMeal() {
        return this.originBalanceMeal - this.totalMealQuantity;
    }

    get totalMealQuantity() {
        return sumBy(this.order.purcharsedOrderList, 'quantity');
    }

    get minimumOrderQuantity() {
        return (this.selecteProductOffer ? this.selecteProductOffer['minimumOrderQuantity'] : 0) || 0;
    }

    get originBalanceMeal() {
        return (this.selecteProductOffer && this.selecteProductOffer['balanceDetail'] && this.selecteProductOffer['balanceDetail'].creditBalance) || 0;
    }

    get expiryDate() {
        return this.selecteProductOffer && this.selecteProductOffer['balanceDetail'] && this.selecteProductOffer['balanceDetail'].expiryDate;
    }
}

import { Component, OnInit, Injector } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TiffinMemberPortalServiceProxy, EntityDto } from "@shared/service-proxies/service-proxies";
import { ProductService } from "@app/shared/services/product.service";

@Component({
    selector: "app-payment-completed",
    templateUrl: "./payment-completed.component.html",
    styleUrls: ["./payment-completed.component.scss"],
    animations: [appModuleAnimation()],
})
export class PaymentCompletedComponent extends AppComponentBase implements OnInit 
{
    public id: number;
    errorMessage  = "";
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _productService: ProductService,
        private _router: Router,

    ) {
        super(injector);
    }

    ngOnInit() {
        this.id = Number(this._activatedRoute.snapshot.paramMap.get('id'));
        
        const input = new EntityDto();
        input.id = this.id;
        
        this._portalServiceProxy.validatePayment(input).subscribe((result) => {
           if(result.purchasedSuccess){
               this._productService.removeAllLocalCartProducts();
               this.message
               .success(this.l("PaymentedSuccessfully"), "", {
                   timer: 1500,
               })
               .then(() => {
                   this._router.navigate([
                       "/app/portal/member/order",
                   ]);
               });
           }else{
               this.errorMessage = result.message;
           }
        });            
     }
   
}

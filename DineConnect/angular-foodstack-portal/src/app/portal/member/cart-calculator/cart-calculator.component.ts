import {
    Component,
    OnInit,
    Input,
    Injector,
    SimpleChanges,
    SimpleChange,
    EventEmitter,
    Output,
    OnChanges,
    ViewChild,
    ElementRef,
    AfterViewInit,
    OnDestroy,
    Renderer2,
    NgZone
} from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    TiffinMemberProductOfferDto,
    BillingInfo,
    AddonSettingsServiceProxy,
    DinePlanTaxesServiceProxy,
    TiffinMemberPortalServiceProxy,
    PromotionDiscountPaymentDto,
    TiffinPromotionServiceProxy,
    GetPromotionDiscountProductOfferInputDto,
    WheelPaymentMethodsServiceProxy,
    GetWheelPaymentMethodForViewDto,
    CreatePaymentInputDto,
    OmisePaymentResponseDto,
    SwipeCardServiceProxy,
    CustomerCardDto
} from '@shared/service-proxies/service-proxies';
import { ProductService } from '@app/shared/services/product.service';
import { StripeHelper } from '@shared/helpers/StripeHelper';
import { Router } from '@angular/router';
import { AddPromoCodeDialogComponent } from './add-promo-code-dialog/add-promo-code-dialog.component';

import AdyenCheckout from '@adyen/adyen-web';
import '@adyen/adyen-web/dist/adyen.css';
import { DataApproved, PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { PAYMENT_METHOD, SYSTEM_NAME } from '@app/portal/services/order.service';
import { ReadyToPayChangeResponse } from '@google-pay/button-angular';
import { CoreOptions } from '@adyen/adyen-web/dist/types/core/types';
import { QRCodeDialogComponent } from './qr-code-dialog/qr-code-dialog.component';
declare let Stripe: any;
declare let OmiseCard: any;
declare let Omise: any;

interface AdyenState {
    isValid: true;
    data: {
        paymentMethod: {
            type: string;
            encryptedCardNumber: string;
            encryptedExpiryMonth: string;
            encryptedExpiryYear: string;
            encryptedSecurityCode: string;
            holderName: string;
        };
    };
}

@Component({
    selector: 'app-cart-calculator',
    templateUrl: './cart-calculator.component.html',
    styleUrls: ['./cart-calculator.component.scss']
})
export class CartCalculatorComponent extends AppComponentBase implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    @Input() cartClass = '';
    @Input() products: TiffinMemberProductOfferDto[] = [];
    @Input() isChillComponent = false;
    @Input() billingInfo: BillingInfo = new BillingInfo();

    @Output() removeItem: EventEmitter<any> = new EventEmitter<any>();
    @Output() changeQty: EventEmitter<any> = new EventEmitter<any>();
    @Output() acceptTerm: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('stripeCardElement', { static: false }) cardelement: ElementRef;
    @ViewChild('addPromoCodeModal') addPromoCodeModal: AddPromoCodeDialogComponent;
    @ViewChild('QRCodeModal') QRCodeModal: QRCodeDialogComponent;

    totalValue = 0;
    totalQty = 0;
    isCheckOut = false;
    isReceiveEmail = false;

    omiseConfig = {
        publishKey: '',
        secretKey: '',
        enablePaynowQR: false
    };
    stripeConfig = {
        secretKey: '',
        publishKey: ''
    };
    adyenConfig = {
        apiKey: '',
        merchantAccount: '',
        environment: '',
        clientKey: ''
    };
    paypalConfig = {
        clientId: '',
        clientSecret: '',
        environment: 0
    };
    googlePayConfig = {
        merchantId: '',
        merchantName: '',
        publicKey: '',
        environment: ''
    };

    paymentMethod: string;

    stripe: any;
    elements: any;
    card: any;
    validCard = false;

    serviceFees = [];
    taxPercentage = 0;
    serviceFeePercentage = 0;

    omiseData = {
        label: '',
        omiseToken: '',
        omisePaynowToken: ''
    };
    stripeData = {
        label: '',
        stripeToken: ''
    };
    paypalData = {
        label: '',
        orderId: null
    };
    adyenData = {
        label: '',
        adyenEncryptedCardNumber: '',
        adyenEncryptedExpiryMonth: '',
        adyenEncryptedExpiryYear: '',
        adyenEncryptedSecurityCode: ''
    };
    googlePayData = {
        label: '',
        googlePayToken: null
    };
    paymentRequest: any;
    payPalButtonId: 'payPalButton';

    promotionDiscountPayment: PromotionDiscountPaymentDto;

    checkoutAdyen: AdyenCheckout;
    swipeCardList: CustomerCardDto[];
    swipeCard: string;
    swipeOTP: string;

    PAYMENT_METHOD = PAYMENT_METHOD;

    private payPalLoaded: boolean;

    constructor(
        injector: Injector,
        private _ngZone: NgZone,
        private render: Renderer2,
        private elRef: ElementRef,
        private _router: Router,
        private _productService: ProductService,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy,
        private _addonService: AddonSettingsServiceProxy,
        private _taxService: DinePlanTaxesServiceProxy,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _paypalScriptService: PaypalScriptService,
        private _swipeCardServiceProxy: SwipeCardServiceProxy
    ) {
        super(injector);
    }

    ngOnDestroy() {
        if (this.card) {
            this.card.unmount(this.cardelement.nativeElement);
            this.card.destroy();
        }
    }

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this.init();
    }

    removeCartProduct(product: TiffinMemberProductOfferDto) {
        this.message.confirm(this.l('RemoveProductFromCart', product.productSetName), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this.removeItem.emit(product);
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        const dataChanges: SimpleChange = changes.products;

        const products: TiffinMemberProductOfferDto[] = dataChanges.currentValue;
        this._updateTotalValue(products);
    }

    returnOnlyNumber(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }

        if (keynum === 11) {
            return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[0123456789]/;
        return charcheck.test(keychar);
    }

    increaseQty(product) {
        product.quantity++;
        this._updateTotalValue();
        this.changeQty.emit(product);
    }

    decreaseQty(product) {
        if (product.quantity > 1) {
            product.quantity--;
            this.onChangedQty(product);
            return;
        }
        this.removeCartProduct(product);
    }

    onChangedQty(product) {
        if (product.quantity === 0) {
            this.removeCartProduct(product);
        }
        this._updateTotalValue();
        this.changeQty.emit(product);
    }

    acceptTermAndCondition() {
        this.acceptTerm.emit(this.isCheckOut);

        if (!this.isCheckOut) {
            this.paymentMethod = '';
        }

        let els = this.elRef.nativeElement.querySelectorAll('div.payment-method');
        els.forEach((el) => {
            this.render.removeClass(el, 'selected');
        });
    }

    choosepaymentMethod() {
        this.getServiceCharge(this.paymentMethod);

        let els = this.elRef.nativeElement.querySelectorAll('div.payment-method');
        els.forEach((el) => {
            this.render.removeClass(el, 'selected');
        });

        if (this.paymentMethod === PAYMENT_METHOD.Omise || this.paymentMethod === PAYMENT_METHOD.Adyen) {
            els.forEach((el) => {
                this.render.addClass(el, 'selected');
            });
        }
    }

    getServiceCharge(name) {
        this.serviceFeePercentage = 0;
        this._addonService.getServiceCharge(name).subscribe((result) => {
            this.serviceFees = result || [];
            this.serviceFees.map((item) => {
                this.serviceFeePercentage = this.serviceFeePercentage + item.percentage;
            });
        });
    }

    getTaxPercentage() {
        this._taxService.getTaxPercentageForTiffin().subscribe((result) => {
            this.taxPercentage = result;
        });
    }

    showAddCodeModal() {
        let promoCodes = [];

        if (this.promotionDiscountPayment && this.promotionDiscountPayment.discountDetails) {
            promoCodes = this.promotionDiscountPayment.discountDetails.map((d) => d.code);
        }

        this.addPromoCodeModal.show(this.products, promoCodes);
    }

    addPromoCode(promotionDiscountPayment: PromotionDiscountPaymentDto) {
        this.promotionDiscountPayment = promotionDiscountPayment;
        this.totalValue = promotionDiscountPayment.discountedAmount;
    }

    removePromoCode(index) {
        this.promotionDiscountPayment.discountDetails.splice(index, 1);

        this._updateDiscountedAmount();
    }

    payWithAdyen() {
        this.submitPayment(PAYMENT_METHOD.Adyen);
    }

    payWithWallet() {
        if (this.swipeCard && this.swipeOTP) {
            this._swipeCardServiceProxy.validateCardOTP(this.swipeCard, this.swipeOTP).subscribe(result => {
                this.submitPayment(PAYMENT_METHOD.Swipe);
            }) 
        }
    }

    sendOTP() {
        if (this.swipeCard) {
            this._swipeCardServiceProxy.generateCardOTP(this.swipeCard).subscribe(result => {
                this.notify.success('Sent Successfully')
            })  
        } else {
            this.notify.error('Please select Card Number')
        }
    }

    onLoadGooglePaymentData = (event: CustomEvent<google.payments.api.PaymentData>): void => {
        console.log('load payment data', event.detail);
        this.googlePayData.googlePayToken = event?.detail?.paymentMethodData?.tokenizationData?.token;

        this.submitPayment(PAYMENT_METHOD.Google);
    };

    onGooglePayError = (event: ErrorEvent): void => {
        console.error('error', event.error);
    };

    onGooglePaymentDataAuthorized: google.payments.api.PaymentAuthorizedHandler = (paymentData) => {
        console.log('payment authorized', paymentData);

        return {
            transactionState: 'SUCCESS'
        };
    };

    onReadyToPayChange = (event: CustomEvent<ReadyToPayChangeResponse>): void => {
        console.log('ready to pay change', event.detail);
    };

    createPaymentWithoutPaymentMethod() {
        /* TODO: payment method should be null*/
        this.submitPayment(PAYMENT_METHOD.Omise);
    }

    onCloseQRCodeModal(response: OmisePaymentResponseDto) {
        if (response.isSuccess) {
            this.omiseData.omisePaynowToken = response.paymentId;
            this.submitPayment(PAYMENT_METHOD.Paynow);
        }
    }

    private init() {
        if (this.isChillComponent) {
            this._getPaymentMethods();
            this._getSwipeCardList();
        }
    }

    private _getPaymentMethods() {
        this._wheelPaymentMethodsServiceProxy.getAll(undefined, undefined, undefined, undefined, -1, -1, -1, -1, 1, undefined, 0, 1000).subscribe((result) => {
            this.getSettings(result.items);
            this.initPayment();
        });
    }

    private initPayment() {
        this.stripeData.label = this.l('OnlinePayment') + ' - ' + this.l('Stripe');
        this.omiseData.label = this.l('OnlinePayment') + ' - ' + this.l('Omise');
        this.adyenData.label = this.l('OnlinePayment') + ' - ' + this.l('Adyen');
        this.paypalData.label = this.l('OnlinePayment') + ' - ' + this.l('PayPal');
        this.googlePayData.label = this.l('OnlinePayment') + ' - ' + this.l('GooglePay');

        if (this.stripeConfig?.publishKey) {
            this.initStripeElements();
        }
        if (this.omiseConfig?.publishKey) {
            this.initOmiseElements();
        }
        if (this.adyenConfig?.apiKey) {
            this.initAdyenElements();
        }
        if (this.paypalConfig?.clientId) {
            this.initPayPalElements();
        }
        if (this.googlePayConfig?.merchantId) {
            this.initGooglePayElements();
        }
    }

    private getSettings(items: GetWheelPaymentMethodForViewDto[]) {
        items.map((item) => {
            switch (item.systemName) {
                case SYSTEM_NAME.Omise:
                    this.omiseConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Stripe:
                    this.stripeConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Adyen:
                    this.adyenConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.PayPal:
                    this.paypalConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.GooglePay:
                    this.googlePayConfig = item.dynamicFieldData;
                    break;

                default:
                    break;
            }
        });
    }

    private initStripeElements() {
        if (this.stripeConfig.publishKey) {
            this.stripe = Stripe(this.stripeConfig.publishKey);

            let elements = this.stripe.elements({
                fonts: [
                    {
                        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro'
                    }
                ]
            });

            let elementStyles = {
                base: {
                    color: '#32325D',
                    fontWeight: 500,
                    fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',

                    '::placeholder': {
                        color: '#CFD7DF'
                    },
                    ':-webkit-autofill': {
                        color: '#e39f48'
                    }
                },
                invalid: {
                    color: '#E25950',

                    '::placeholder': {
                        color: '#FFCCA5'
                    }
                }
            };

            let elementClasses = {
                focus: 'focused',
                empty: 'empty',
                invalid: 'invalid'
            };

            let cardNumber = elements.create('cardNumber', {
                style: elementStyles,
                classes: elementClasses
            });
            cardNumber.mount('#stripe-card-number');

            let cardExpiry = elements.create('cardExpiry', {
                style: elementStyles,
                classes: elementClasses
            });
            cardExpiry.mount('#stripe-card-expiry');

            let cardCvc = elements.create('cardCvc', {
                style: elementStyles,
                classes: elementClasses
            });

            cardCvc.mount('#stripe-card-cvc');

            let self = this;
            StripeHelper.registerElements(this.stripe, [cardNumber, cardExpiry, cardCvc], 'stripe-check-out-container', function (token) {
                self.stripeData.stripeToken = token.id;
                self.submitPayment(PAYMENT_METHOD.Stripe);
            });
        }
    }

    private initPayPalElements() {
        if (!this.payPalLoaded) {
            const config = {
                clientId: this.paypalConfig.clientId,
                currency: this.appSession.application.currency,
                locale: 'en_SG',
                components: ['buttons', 'funding-eligibility']
            };
            this._paypalScriptService.registerScript(config, (payPal: any) => {
                this.payPalLoaded = true;

                this.initPayPalConfig(payPal);
            });
        }
    }

    private initPayPalConfig(payPal) {
        const self = this;
        this._ngZone.runOutsideAngular(() => {
            payPal
                .Buttons({
                    fundingSource: payPal.FUNDING.PAYPAL,
                    createOrder: (data: any, actions: { order: any }) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        currency_code: this.appSession.application.currency,
                                        value: this.totalAmount
                                    }
                                }
                            ]
                        });
                    },
                    onApprove: (data: DataApproved, actions: any) => {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function (details) {
                            // This function shows a transaction success message to your buyer.

                            self.paypalData.orderId = data.orderID;
                            self._ngZone.run(() => {
                                self.submitPayment(PAYMENT_METHOD.PayPal);
                            });
                        });
                    }
                })
                .render(`#${this.payPalButtonId}`);
        });
    }

    private initGooglePayElements() {
        this.paymentRequest = {
            environment: this.googlePayConfig.environment,
            buttonType: 'buy',
            buttonColor: 'default',
            apiVersion: 2,
            apiVersionMinor: 0,
            allowedPaymentMethods: [
                {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
                        allowedCardNetworks: ['MASTERCARD', 'VISA']
                    },
                    tokenizationSpecification: {
                        type: 'DIRECT',
                        parameters: {
                            protocolVersion: 'ECv2',
                            publicKey: this.googlePayConfig.publicKey
                        }
                    }
                }
            ],
            merchantInfo: {
                merchantId: this.googlePayConfig.merchantId,
                merchantName: this.googlePayConfig.merchantName
            }
        };
    }

    private initOmiseElements() {
        OmiseCard.configure({
            publicKey: this.omiseConfig.publishKey
        });
        Omise.setPublicKey(this.omiseConfig.publishKey);

        let button = document.querySelector('#checkoutButton');
        if (!button) {
            return;
        }

        button.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this.isValidBillingInformation()) {
                return;
            }

            let self = this;

            OmiseCard.open({
                amount: this.totalAmount * 100,
                currency: this.appSession.application.currency,
                defaultPaymentMethod: 'credit_card',
                onCreateTokenSuccess: (nonce) => {
                    if (nonce.startsWith('tokn_')) {
                        this.omiseData.omiseToken = nonce;

                        self.submitPayment(PAYMENT_METHOD.Omise);
                    }
                }
            });
        });

        let myButton = document.querySelector('#payNow');
        if (!myButton || !this.omiseConfig.enablePaynowQR) {
            return;
        }

        myButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this.isValidBillingInformation()) {
                return;
            }

            this._memberServiceProxy.createOmisePaymentNowRequest(this.totalAmount * 100, this.appSession.application.currency).subscribe((result) => {
                console.log(result);
                const response = result ? JSON.parse(result) : null;

                if (response && response.source && response.source.scannable_code && response.source.scannable_code.image) {
                    this.QRCodeModal.show(response.source.scannable_code.image.download_uri, response.id, this.totalAmount);
                }
            });
        });
    }

    private initAdyenElements() {
        const configuration: CoreOptions = {
            clientKey: this.adyenConfig.clientKey,
            locale: 'en_SG',
            environment: this.adyenConfig.environment,
            amount: {
                currency: this.appSession.application.currency,
                value: this.totalAmount * 100
            },
            countryCode: 'SG'
        };

        this.checkoutAdyen = new AdyenCheckout(configuration);
        this.checkoutAdyen
            .create('securedfields', {
                // Optional configuration
                type: 'card',
                brands: ['mc', 'visa', 'amex', 'bcmc', 'maestro'],
                styles: {
                    base: {
                        color: 'black',
                        fontSize: '14px',
                        fontFamily: 'Helvetica'
                    },
                    error: {
                        color: 'red'
                    },
                    validated: {
                        color: 'green'
                    },
                    placeholder: {
                        color: '#d8d8d8'
                    }
                },
                ariaLabels: {
                    lang: 'en-GB',
                    encryptedCardNumber: {
                        label: 'Credit or debit card number field'
                    }
                },
                // Events
                onChange: (state: AdyenState, component) => {
                    const data = state.data.paymentMethod;

                    if (state?.data?.paymentMethod) {
                        this.adyenData.adyenEncryptedCardNumber = data.encryptedCardNumber;
                        this.adyenData.adyenEncryptedExpiryMonth = data.encryptedExpiryMonth;
                        this.adyenData.adyenEncryptedExpiryYear = data.encryptedExpiryYear;
                        this.adyenData.adyenEncryptedSecurityCode = data.encryptedSecurityCode;
                    }
                }
            })
            .mount('#customCard-container');
    }

    private submitPayment(paymentOption: string) {
        let request = new CreatePaymentInputDto();

        request.paymentOption = paymentOption;
        request.source = '101';
        request.billingInfo = this.billingInfo;
        request.paidAmount = this.totalAmount;
        request.purcharsedOfferList = this.products.map((item) => {
            return item;
        });
        request.callbackUrl = location.origin + '/app/portal/member/payment-completed/';
        request.discountDetails = this.promotionDiscountPayment ? this.promotionDiscountPayment.discountDetails : [];

        switch (paymentOption) {
            case PAYMENT_METHOD.Omise:
                {
                    request.omiseToken = this.omiseData.omiseToken;
                }

                break;
            case PAYMENT_METHOD.Paynow:
                {
                    request.omiseToken = this.omiseData.omisePaynowToken;
                }

                break;
            case PAYMENT_METHOD.Stripe:
                {
                    request.stripeToken = this.stripeData.stripeToken;
                }

                break;
            case PAYMENT_METHOD.Adyen:
                {
                    request.adyenEncryptedCardNumber = this.adyenData.adyenEncryptedCardNumber;
                    request.adyenEncryptedExpiryMonth = this.adyenData.adyenEncryptedExpiryMonth;
                    request.adyenEncryptedExpiryYear = this.adyenData.adyenEncryptedExpiryYear;
                    request.adyenEncryptedSecurityCode = this.adyenData.adyenEncryptedSecurityCode;
                }

                break;
            case PAYMENT_METHOD.PayPal:
                {
                    request.paypalOrderId = this.paypalData.orderId;
                }

                break;
            case PAYMENT_METHOD.Google:
                {
                    request.googlePayToken = this.googlePayData.googlePayToken;
                }

                break;
            case PAYMENT_METHOD.Swipe:
                {
                    request.swipeCardNumber = this.swipeCard;
                    request.swipeOTP = this.swipeOTP;
                }

                break;
        }

        this.createPayment(request);
    }

    private createPayment(request: CreatePaymentInputDto) {
        this._memberServiceProxy.createPayment(Object.assign({}, request)).subscribe((result) => {
            if (result.purchasedSuccess) {
                this._productService.removeAllLocalCartProducts();
                this.message
                    .success(this.l('PaymentedSuccessfully'), '', {
                        timer: 1500
                    })
                    .then(() => {
                        this._router.navigate(['/app/portal/member/order']);
                    });
            } else if (!result.purchasedSuccess && result.authenticateUri != null) {
                this.message.warn(this.l('Redirecting'), '', {
                    timer: 500
                });
                window.open(result.authenticateUri, '_self');
            } else if (!result.purchasedSuccess) {
                this.message.warn(result.message);
            }
        });
    }

    private isValidBillingInformation() {
        return true;
    }

    private _updateTotalValue(products = this.products) {
        this.totalValue = 0;
        this.totalQty = 0;
        products.forEach((product) => {
            this.totalValue += product.price * +product.quantity;
            this.totalQty += +product.quantity;
        });

        if (this.promotionDiscountPayment?.discountDetails?.length > 0) {
            this._updateDiscountedAmount();
        }
    }

    private _updateDiscountedAmount() {
        const promoCodes = this.promotionDiscountPayment.discountDetails.map((d) => d.code);
        const products = this.products.map((item) => {
            return {
                id: item.id,
                quantity: item.quantity
            } as GetPromotionDiscountProductOfferInputDto;
        });

        this._tiffinPromotionServiceProxy.getPromotionDiscountPayment(products, promoCodes).subscribe((result) => {
            this.promotionDiscountPayment = result ? result : ({} as PromotionDiscountPaymentDto);
            this.totalValue = this.promotionDiscountPayment.discountedAmount;
        });
    }

    private _getSwipeCardList() {
        this._swipeCardServiceProxy.getAllCustomerCard(this.appSession.customerId, undefined, undefined, 'id', 0, 1000).subscribe((result) => {
            this.swipeCardList = result.items;
        });
    }

    get totalAmount() {
        return this.totalValue + (this.totalValue * this.serviceFeePercentage) / 100;
    }

    get paymentMethodNotAvailable() {
        return (
            !this.stripeConfig?.secretKey &&
            !this.omiseConfig?.publishKey &&
            !this.adyenConfig?.apiKey &&
            !this.paypalConfig?.clientId &&
            this.googlePayConfig?.merchantId
        );
    }

    get isCreatePaymentWithoutPaymentMethod() {
        return this.products?.length > 0 && this.promotionDiscountPayment?.discountDetails?.length > 0 && this.totalAmount == 0;
    }
}

import { Component, Injector, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    TiffinMemberPortalServiceProxy,
    PaymentCustomerHistoryDto,
} from "@shared/service-proxies/service-proxies";
import { Router } from "@angular/router";
import { FileDownloadService } from "@shared/utils/services/file-download.service";
import { Table } from "primeng/table";
import { LazyLoadEvent } from "primeng";
import { Paginator } from "primeng/paginator";
import { finalize } from "rxjs/operators";
import * as moment from "moment";

@Component({
    selector: "app-payment-history",
    templateUrl: "./new-payment-history.component.html",
    styleUrls: ["./payment-history.component.less"],
})
export class PaymentHistoryComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    startDate: moment.Moment;
    endDate = undefined;

    paymentHistory: PaymentCustomerHistoryDto[] = [];
    dateRange = [];
    paymentFilter: any;
    constructor(
        injector: Injector,
        private _router: Router,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    viewPayment(id?): void {
        this._router.navigate([
            "/app/portal/member/payment-detail",
            id ? id : "null",
        ]);
    }

    getAll(event?: LazyLoadEvent) {
        if ((this.dateRange as any) !== undefined && this.dateRange.length) {
            this.startDate = moment(this.dateRange[0]).startOf("day");
            this.endDate = moment(this.dateRange[1]).endOf("day");
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._portalServiceProxy
            .getPaymentHistory(
                this.paymentFilter,
                this.startDate,
                this.endDate,
                this.primengTableHelper.getSorting(this.dataTable) || 'id DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    printPayment() {
        this._portalServiceProxy.printPaymentHistory().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    refresh() {
        this.paymentFilter = undefined;
        this.dateRange = undefined;
        this.endDate = undefined;
        this.startDate = undefined;
        this.reloadPage();
    }
}

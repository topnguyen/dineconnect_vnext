import { Component, OnInit, ViewChild, ElementRef, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PasswordComplexitySetting, ProfileServiceProxy, ChangePasswordInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent extends AppComponentBase implements OnInit {
  @ViewChild('currentPasswordInput', { static: true }) currentPasswordInput: ElementRef;

  passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
  currentPassword: string;
  password: string;
  passwordConfirm: string;

  saving = false;

  constructor(
    injector: Injector,
    private _profileService: ProfileServiceProxy
  ) {
    super(injector);
  }
  ngOnInit() {
    this.show();
  }

  show(): void {
    this.currentPassword = '';
    this.password = '';
    this.passwordConfirm = '';

    this._profileService.getPasswordComplexitySetting().subscribe(result => {
      this.passwordComplexitySetting = result.setting;
    });
  }

  save(): void {
    let input = new ChangePasswordInput();
    input.currentPassword = this.currentPassword;
    input.newPassword = this.password;

    this.saving = true;
    this._profileService.changePassword(input)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.info(this.l('YourPasswordHasChangedSuccessfully'), '', { timer: 500 });
      });
  }
}

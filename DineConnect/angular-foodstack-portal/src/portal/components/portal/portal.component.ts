import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';
import {
    AccountServiceProxy,
    ChangeUserLanguageDto,
    ProfileServiceProxy,
    IsTenantAvailableInput,
    IsTenantAvailableOutput
} from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { AppConsts } from '@shared/AppConsts';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'app-portal',
    templateUrl: './portal.component.html',
    styleUrls: ['./portal.component.scss'],
    animations: [appModuleAnimation()]
})
export class PortalComponent extends AppComponentBase implements OnInit {
    userName: string;
    languages: abp.localization.ILanguageInfo[];
    currentLanguage: abp.localization.ILanguageInfo;
    tenancyName: string = AppConsts.tenancyName;
    collapse: boolean = false;
    isLoginPage = false;
    isPortalIndexPage = false;

    constructor(
        injector: Injector,
        private _authService: AppAuthService,
        private _accountService: AccountServiceProxy,
        private _profileServiceProxy: ProfileServiceProxy,
        private _router: Router
    ) {
        super(injector);
        this.isLoginPage =
                    this._router.url.includes('portal/login') ||
                    this._router.url.includes('portal/forgot-password') ||
                    this._router.url.includes('portal/register');
        this.isPortalIndexPage =
                    this._router.url.includes('portal/index');
        this._router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.isLoginPage =
                    this._router.url.includes('portal/login') ||
                    this._router.url.includes('portal/forgot-password') ||
                    this._router.url.includes('portal/register');
                this.isPortalIndexPage =
                    this._router.url.includes('portal/index');
            }
        });
    }

    ngOnInit() {
        this.languages = _.filter(this.localization.languages, (l) => l.isDisabled === false);
        this.currentLanguage = this.localization.currentLanguage;
        // this.setDeFaultTenant();
        this.showLoginName();

        this._router.events.subscribe(() => (this.collapse = false));
    }

    logout(): void {
        this._authService.logout();
    }

    showLoginName(): void {
        if (this.appSession.user !== undefined) {
            this.userName = this.appSession.user.name;
        }
    }

    setDeFaultTenant() {
        let input = new IsTenantAvailableInput();
        input.tenancyName = this.tenancyName;

        this._accountService.isTenantAvailable(input).subscribe((result: IsTenantAvailableOutput) => {
            abp.multiTenancy.setTenantIdCookie(result.tenantId);
            return;
        });
    }

    changeLanguage(languageName: string): void {
        const input = new ChangeUserLanguageDto();
        input.languageName = languageName;

        this._profileServiceProxy.changeLanguage(input).subscribe(() => {
            abp.utils.setCookieValue(
                'Abp.Localization.CultureName',
                languageName,
                new Date(new Date().getTime() + 5 * 365 * 86400000), //5 year
                abp.appPath
            );

            window.location.reload();
        });
    }

    redirectForMember() {
        if (this.userName !== undefined) {
            this._router.navigate(['/app/portal/member/profile']);
            return;
        } else {
            this._router.navigate(['/account/login']);
            return;
        }
    }

    redirectForMenu() {
        if (this.userName !== undefined) {
            this._router.navigate(['/app/portal/member/buy-offer']);
            return;
        } else {
            this._router.navigate(['/menu']);
            return;
        }
    }

    redirectForOrder() {
        if (this.userName !== undefined) {
            this._router.navigate(['/app/portal/member/buy-offer']);
            return;
        } else {
            this._router.navigate(['/view-offers']);
            return;
        }
    }

    mouseLeave() {
        this.collapse = false;
        console.log('Mouse leave');
    }
}

import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    PasswordComplexitySetting,
    ProfileServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
    AccountServiceProxy,
    RegisterOutput
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'portal/services/login.service';
import { RegisterMemberModel } from './register-member.model';
import { CardIdGuiderModalComponent } from '../../../app/shared/common/card-id-guide-modal/card-id-guider-modal.component';

@Component({
    selector: 'app-create-member',
    templateUrl: './create-member.component.html',
    styleUrls: ['./create-member.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateMemberComponent extends AppComponentBase implements OnInit {
    @ViewChild("CardIdGuiderModal") CardIdGuiderModal: CardIdGuiderModalComponent;

    model: RegisterMemberModel = new RegisterMemberModel();
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    recaptchaSiteKey: string = AppConsts.recaptchaSiteKey;
    saving = false;
    lstCity: ERPComboboxItem[] = [];
    lstCountry: ERPComboboxItem[] = [];
    callingCode: number = 65;
    floorNo = '';
    unitNo = '';
    tower = '';
    numberMask: any;
    referrerReferralCode: string;
    grade: string;
    cardID: string;
    grades: string[] = [' 1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'School Staff'];
    ADDRESS_DEFAULT = {
        ADDRESS1: 'Stack Cafeteria @ NPS International School, 10 Chai Chee Lane, Singapore 469021',
        ADDRESS2: null,
        ADDRESS3: null,
        POSTCALCODE: '469021'
    }

    section: string;
    sections: string[] = [' A', 'B', 'C', 'D', 'E', 'F', 'CBSE', 'NA'];

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private readonly _loginService: LoginService,
        private _profileService: ProfileServiceProxy,
        private _commonService: CommonServiceProxy,
    ) {
        super(injector);

        this.referrerReferralCode = this._activatedRoute.snapshot.queryParams['referralCode'];
    }

    ngOnInit() {
        this._profileService.getPasswordComplexitySetting().subscribe((result) => {
            this.passwordComplexitySetting = result.setting;
        });

        this._commonService.getLookups('Cities', this.appSession.tenantId, undefined).subscribe((result) => {
            this.lstCity = result;
        });
        this._commonService.getLookups('Countries', this.appSession.tenantId, undefined).subscribe((result) => {
            this.lstCountry = result;
        });
    }

    get useCaptcha(): boolean {
        return this.setting.getBoolean('App.UserManagement.UseCaptchaOnRegistration');
    }

    save(): void {
        this.saving = true;

        this.model.address1 = this.ADDRESS_DEFAULT.ADDRESS1;
        this.model.address2 = this.ADDRESS_DEFAULT.ADDRESS2;
        this.model.address3 = this.ADDRESS_DEFAULT.ADDRESS3;

        if (this.lstCountry?.length > 0) {
            this.model.countryId = this.lstCountry[0].value
        } else {
            this.message.error('Please go to the admin page and add the country');
            return;
        }

        if (this.lstCity?.length > 0) {
            this.model.cityId = this.lstCity[0].value
        } else {
            this.message.error('Please go to the admin page and add the city');
            return;
        }

        this.model.userName = this.model.emailAddress;
        this.model.referrerReferralCode = this.referrerReferralCode;
        this.model.jsonData = JSON.stringify({
            grade: this.grade,
            cardID: this.cardID,
            section: this.section,
        })

        this._accountService
            .registerMember(this.model)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result: RegisterOutput) => {
                if (!result.canLogin) {
                    this.notify.success(this.l('SuccessfullyRegistered'));
                    this._router.navigate(['/app/portal/member/buy-offer']);
                    return;
                }

                //Autheticate
                this.saving = true;
                this._loginService.authenticateModel.userNameOrEmailAddress = this.model.userName;
                this._loginService.authenticateModel.password = this.model.password;
                this._loginService.authenticate(() => {
                    this.saving = false;
                });
            });
    }

    showGetCardIdGuide() {
        this.CardIdGuiderModal.show();
    }

    captchaResolved(captchaResponse: string): void {
        this.model.captchaResponse = captchaResponse;
    }

    changeCountry(id) {
        this._commonService.getLookups('Cities', this.appSession.tenantId, id).subscribe((result) => {
            this.lstCity = result;
        });

        this._commonService.getCountryCallCode(id).subscribe((result) => {
            this.callingCode = result;
        });
    }

    setAddress2(): string {
        let address2 = [];
        address2.push(this.floorNo);
        address2.push(this.unitNo);
        address2.push(this.tower);
        return address2.join();
    }
}

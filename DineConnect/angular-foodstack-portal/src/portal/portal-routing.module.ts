import { RouterModule } from "@angular/router";
import { MenuComponent } from "@app/shared/common/menu/menu.component";
import { TermAndConditionsComponent } from "@app/shared/common/term-and-conditions/term-and-conditions.component";
import { NgModule } from "@angular/core";
import { PrivacyComponent } from "@app/shared/common/privacy/privacy.component";
import { AboutUsComponent } from "@app/shared/common/aboutUs/aboutUs.component";
import { FaqComponent } from "@app/shared/common/faq/faq.component";
import { ContactComponent } from "@app/shared/common/contact/contact.component";
import { FeedbackComponent } from "@app/shared/common/feedback/feedback.component";
import { CreateMemberComponent } from "portal/components/create-member/create-member.component";
import { ForgotPasswordComponent } from "portal/components/forgot-password/forgot-password.component";
import { PortalRouteGuard } from "@app/shared/common/auth/portal-route-guard";
import { ResetPasswordComponent } from "portal/components/reset-password/reset-password.component";
import { PortalComponent } from "./components/portal/portal.component";
import { PortalIndexComponent } from "./components/portal-index/portal-index.component";
import { ViewOffersComponent } from "./components/view-offers/view-offers.component";
import { LoginComponent } from "./components/login/login.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                redirectTo: "/portal/index",
                pathMatch: "full",
            },
            {
                path: "portal",
                component: PortalComponent,
                canActivate: [PortalRouteGuard],
                canActivateChild: [PortalRouteGuard],
                children: [
                    { path: "index", component: PortalIndexComponent },
                    { path: "menu", component: MenuComponent },
                    { path: "login", component: LoginComponent },
                    { path: "register", component: CreateMemberComponent },
                    { path: "forgot-password",component: ForgotPasswordComponent},
                    { path: "reset-password", component: ResetPasswordComponent },
                    { path: "aboutUs", component: AboutUsComponent },
                    { path: "faq", component: FaqComponent },
                    { path: "contact", component: ContactComponent },
                    { path: "feedback", component: FeedbackComponent },
                    { path: "privacy", component: PrivacyComponent },
                    { path: "term-and-conditions",component: TermAndConditionsComponent},
                    { path: "view-offers", component: ViewOffersComponent },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class PortalRoutingModule {}

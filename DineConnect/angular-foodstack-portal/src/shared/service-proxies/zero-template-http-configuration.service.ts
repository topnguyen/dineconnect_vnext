import { Injectable } from '@angular/core';
import { AbpHttpConfigurationService, IErrorInfo, LogService, MessageService } from 'abp-ng2-module';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ZeroTemplateHttpConfigurationService extends AbpHttpConfigurationService {

    constructor(
        messageService: MessageService,
        logService: LogService,
        private _route: Router) {
        super(messageService, logService);
    }

    // Override handleUnAuthorizedRequest so it doesn't refresh the page during failed login attempts.
    handleUnAuthorizedRequest(messagePromise: any, targetUrl?: string) {
        if (this._route.url.includes('login')) {
            return;
        }

        const self = this;

        if (messagePromise) {
            messagePromise.done(() => {
                this.handleTargetUrl(targetUrl || '/');
            });
        } else {
            self.handleTargetUrl(targetUrl || '/');
        }
    }

    showError(error: IErrorInfo): any {
        const messageService = new MessageService();
        if (error.details && !environment.production) {
            return messageService.error(error.details, error.message || this.defaultError.message);
        } else {
            return messageService.error(error.message || this.defaultError.message);
        }
    }
}

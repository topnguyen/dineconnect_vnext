﻿using Xamarin.Forms;

namespace DinePlan.DineConnect.Views
{
    public partial class InitialView : ContentPage, IXamarinView
    {
        public InitialView()
        {
            InitializeComponent();
        }
    }
}
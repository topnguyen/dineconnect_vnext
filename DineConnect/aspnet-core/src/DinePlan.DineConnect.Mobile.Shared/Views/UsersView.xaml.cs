﻿using DinePlan.DineConnect.Models.Users;
using DinePlan.DineConnect.ViewModels;
using Xamarin.Forms;

namespace DinePlan.DineConnect.Views
{
    public partial class UsersView : ContentPage, IXamarinView
    {
        public UsersView()
        {
            InitializeComponent();
        }

        public async void ListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await ((UsersViewModel) BindingContext).LoadMoreUserIfNeedsAsync(e.Item as UserListModel);
        }
    }
}
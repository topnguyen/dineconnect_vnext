﻿using DinePlan.DineConnect.Models.Tenants;
using DinePlan.DineConnect.ViewModels;
using Xamarin.Forms;

namespace DinePlan.DineConnect.Views
{
    public partial class TenantsView : ContentPage, IXamarinView
    {
        public TenantsView()
        {
            InitializeComponent();
        }

        private async void ListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await ((TenantsViewModel)BindingContext).LoadMoreTenantsIfNeedsAsync(e.Item as TenantListModel);
        }
    }
}
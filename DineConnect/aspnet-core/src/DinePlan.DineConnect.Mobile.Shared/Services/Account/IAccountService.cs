﻿using System.Threading.Tasks;
using DinePlan.DineConnect.ApiClient.Models;

namespace DinePlan.DineConnect.Services.Account
{
    public interface IAccountService
    {
        AbpAuthenticateModel AbpAuthenticateModel { get; set; }
        
        AbpAuthenticateResultModel AuthenticateResultModel { get; set; }
        
        Task LoginUserAsync();

        Task LogoutAsync();
    }
}

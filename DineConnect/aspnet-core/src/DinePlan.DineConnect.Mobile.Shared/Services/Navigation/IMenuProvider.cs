using System.Collections.Generic;
using MvvmHelpers;
using DinePlan.DineConnect.Models.NavigationMenu;

namespace DinePlan.DineConnect.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}
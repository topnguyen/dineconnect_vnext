﻿namespace DinePlan.DineConnect.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}
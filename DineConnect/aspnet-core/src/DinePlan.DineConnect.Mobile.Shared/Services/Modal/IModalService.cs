﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Views;
using Xamarin.Forms;

namespace DinePlan.DineConnect.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}

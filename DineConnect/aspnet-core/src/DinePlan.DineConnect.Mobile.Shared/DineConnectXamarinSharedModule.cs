﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    [DependsOn(typeof(DineConnectClientModule), typeof(AbpAutoMapperModule))]
    public class DineConnectXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectXamarinSharedModule).GetAssembly());
        }
    }
}
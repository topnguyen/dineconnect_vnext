﻿using Abp.AutoMapper;
using DinePlan.DineConnect.ApiClient;

namespace DinePlan.DineConnect.Models.Common
{
    [AutoMapFrom(typeof(TenantInformation)),
     AutoMapTo(typeof(TenantInformation))]
    public class TenantInformationPersistanceModel
    {
        public string TenancyName { get; set; }

        public int TenantId { get; set; }
    }
}
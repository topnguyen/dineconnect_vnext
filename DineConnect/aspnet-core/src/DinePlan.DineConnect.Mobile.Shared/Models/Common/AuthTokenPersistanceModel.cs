﻿using System;
using Abp.AutoMapper;
using DinePlan.DineConnect.Sessions.Dto;

namespace DinePlan.DineConnect.Models.Common
{
    [AutoMapFrom(typeof(ApplicationInfoDto)),
     AutoMapTo(typeof(ApplicationInfoDto))]
    public class ApplicationInfoPersistanceModel
    {
        public string Version { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
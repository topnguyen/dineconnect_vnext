﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Organizations.Dto;

namespace DinePlan.DineConnect.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}
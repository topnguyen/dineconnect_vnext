﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    [DependsOn(typeof(DineConnectXamarinSharedModule))]
    public class DineConnectXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectXamarinIosModule).GetAssembly());
        }
    }
}
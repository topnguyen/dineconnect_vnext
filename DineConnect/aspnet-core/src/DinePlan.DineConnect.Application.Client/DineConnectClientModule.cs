﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    public class DineConnectClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectClientModule).GetAssembly());
        }
    }
}

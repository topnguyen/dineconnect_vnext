﻿using Abp.Dependency;
using GraphQL;
using GraphQL.Types;
using DinePlan.DineConnect.Queries.Container;

namespace DinePlan.DineConnect.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IDependencyResolver resolver) :
            base(resolver)
        {
            Query = resolver.Resolve<QueryContainer>();
        }
    }
}
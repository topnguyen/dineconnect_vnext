using Abp.Dependency;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Url;

namespace DinePlan.DineConnect.Web.Url
{
    public class WebUrlService : WebUrlServiceBase, IWebUrlService, ITransientDependency
    {
        public WebUrlService(
            IAppConfigurationAccessor configurationAccessor) :
            base(configurationAccessor)
        {
        }

        public override string WebSiteRootAddressFormatKey => "App:ClientRootAddress";

        public override string ServerRootAddressFormatKey => "App:ServerRootAddress";

        public override string WebSiteTiffinRootAddressKey => "App:ClientTiffinRootAddress";
    }
}
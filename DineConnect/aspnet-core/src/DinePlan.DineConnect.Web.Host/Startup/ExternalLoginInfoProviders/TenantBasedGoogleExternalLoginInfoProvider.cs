﻿using Abp.AspNetZeroCore.Web.Authentication.External;
using Abp.AspNetZeroCore.Web.Authentication.External.Google;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Json;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authentication;
using DinePlan.DineConnect.Configuration;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Web.Startup.ExternalLoginInfoProviders
{
    public class TenantBasedGoogleExternalLoginInfoProvider : TenantBasedExternalLoginInfoProviderBase,
        ISingletonDependency
    {
        private readonly ISettingManager _settingManager;
        private readonly IAbpSession _abpSession;
        public override string Name { get; } = GoogleAuthProviderApi.Name;

        public TenantBasedGoogleExternalLoginInfoProvider(
            ISettingManager settingManager,
            IAbpSession abpSession,
            ICacheManager cacheManager) : base(abpSession, cacheManager)
        {
            _settingManager = settingManager;
            _abpSession = abpSession;
        }

        private ExternalLoginProviderInfo CreateExternalLoginInfo(GoogleExternalLoginProviderSettings settings)
        {
            return new ExternalLoginProviderInfo(
                GoogleAuthProviderApi.Name,
                settings.ClientId,
                settings.ClientSecret,
                typeof(GoogleAuthProviderApi),
                new Dictionary<string, string>
                {
                    {"UserInfoEndpoint", settings.UserInfoEndpoint}
                }
            );
        }

        protected override bool TenantHasSettings()
        {
            if (_abpSession.TenantId != null)
            {
                var settings = new GoogleExternalLoginProviderSettings()
                {
                    ClientId = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.ClientId, _abpSession.TenantId.Value),
                    ClientSecret = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.ClientSecret, _abpSession.TenantId.Value),
                    UserInfoEndpoint = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.UserInfoEndpoint, _abpSession.TenantId.Value),
                };
                return settings.IsValid();
            }

            return false;
        }

        protected override ExternalLoginProviderInfo GetTenantInformation()
        {
            if (_abpSession.TenantId != null)
            {
                var settings = new GoogleExternalLoginProviderSettings()
                {
                    ClientId = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.ClientId, _abpSession.TenantId.Value),
                    ClientSecret = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.ClientSecret, _abpSession.TenantId.Value),
                    UserInfoEndpoint = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Google.UserInfoEndpoint, _abpSession.TenantId.Value),
                };

                return CreateExternalLoginInfo(settings);
            }

            return null;
        }

        protected override ExternalLoginProviderInfo GetHostInformation()
        {
            string settingValue = _settingManager.GetSettingValueForApplication(AppSettings.ExternalLoginProvider.Host.Google);
            var settings = settingValue.FromJsonString<GoogleExternalLoginProviderSettings>();
            return CreateExternalLoginInfo(settings);
        }
    }
}
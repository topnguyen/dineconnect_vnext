﻿using Abp.AspNetZeroCore.Web.Authentication.External;
using Abp.AspNetZeroCore.Web.Authentication.External.Facebook;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Json;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authentication;
using DinePlan.DineConnect.Configuration;

namespace DinePlan.DineConnect.Web.Startup.ExternalLoginInfoProviders
{
    public class TenantBasedFacebookExternalLoginInfoProvider : TenantBasedExternalLoginInfoProviderBase, ISingletonDependency
    {
        private readonly ISettingManager _settingManager;
        private readonly IAbpSession _abpSession;
        public override string Name { get; } = FacebookAuthProviderApi.Name;

        public TenantBasedFacebookExternalLoginInfoProvider(
            ISettingManager settingManager,
            IAbpSession abpSession,
            ICacheManager cacheManager) : base(abpSession, cacheManager)
        {
            _settingManager = settingManager;
            _abpSession = abpSession;
        }

        private ExternalLoginProviderInfo CreateExternalLoginInfo(FacebookExternalLoginProviderSettings settings)
        {
            return new ExternalLoginProviderInfo(Name, settings.AppId, settings.AppSecret, typeof(FacebookAuthProviderApi));
        }

        protected override bool TenantHasSettings()
        {
            if (_abpSession.TenantId != null)
            {
                var settings = new FacebookExternalLoginProviderSettings()
                {
                    AppId = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Facebook.AppId, _abpSession.TenantId.Value),
                    AppSecret = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Facebook.AppSecret, _abpSession.TenantId.Value),
                };
                return settings.IsValid();
            }

            return false;
        }

        protected override ExternalLoginProviderInfo GetTenantInformation()
        {
            if (_abpSession.TenantId != null)
            {
                var settings = new FacebookExternalLoginProviderSettings()
                {
                    AppId = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Facebook.AppId, _abpSession.TenantId.Value),
                    AppSecret = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Facebook.AppSecret, _abpSession.TenantId.Value),
                };

                return CreateExternalLoginInfo(settings);
            }

            return null;
        }

        protected override ExternalLoginProviderInfo GetHostInformation()
        {
            string settingValue = _settingManager.GetSettingValueForApplication(AppSettings.ExternalLoginProvider.Host.Facebook);
            var settings = settingValue.FromJsonString<FacebookExternalLoginProviderSettings>();
            return CreateExternalLoginInfo(settings);
        }
    }
}
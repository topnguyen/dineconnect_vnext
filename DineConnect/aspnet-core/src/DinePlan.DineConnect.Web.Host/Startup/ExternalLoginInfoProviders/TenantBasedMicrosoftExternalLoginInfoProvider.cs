﻿using Abp.AspNetZeroCore.Web.Authentication.External;
using Abp.AspNetZeroCore.Web.Authentication.External.Microsoft;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Json;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authentication;
using DinePlan.DineConnect.Configuration;

namespace DinePlan.DineConnect.Web.Startup.ExternalLoginInfoProviders
{
    public class TenantBasedMicrosoftExternalLoginInfoProvider : TenantBasedExternalLoginInfoProviderBase,
        ISingletonDependency
    {
        private readonly ISettingManager _settingManager;
        private readonly IAbpSession _abpSession;
        public override string Name { get; } = MicrosoftAuthProviderApi.Name;

        public TenantBasedMicrosoftExternalLoginInfoProvider(
            ISettingManager settingManager,
            IAbpSession abpSession,
            ICacheManager cacheManager) : base(abpSession, cacheManager)
        {
            _settingManager = settingManager;
            _abpSession = abpSession;
        }

        private ExternalLoginProviderInfo CreateExternalLoginInfo(MicrosoftExternalLoginProviderSettings settings)
        {
            return new ExternalLoginProviderInfo(
                MicrosoftAuthProviderApi.Name,
                settings.ClientId,
                settings.ClientSecret,
                typeof(MicrosoftAuthProviderApi)
            );
        }

        protected override bool TenantHasSettings()
        {
            if (_abpSession.TenantId != null)
            {
                var settingValue = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Microsoft, _abpSession.TenantId.Value);
                return !settingValue.IsNullOrWhiteSpace();
            }

            return false;
        }

        protected override ExternalLoginProviderInfo GetTenantInformation()
        {
            if (_abpSession.TenantId != null)
            {
                string settingValue = _settingManager.GetSettingValueForTenant(AppSettings.ExternalLoginProvider.Tenant.Microsoft, _abpSession.TenantId.Value);
                var settings = settingValue.FromJsonString<MicrosoftExternalLoginProviderSettings>();
                return CreateExternalLoginInfo(settings);
            }

            return null;
        }

        protected override ExternalLoginProviderInfo GetHostInformation()
        {
            string settingValue = _settingManager.GetSettingValueForApplication(AppSettings.ExternalLoginProvider.Host.Microsoft);
            var settings = settingValue.FromJsonString<MicrosoftExternalLoginProviderSettings>();
            return CreateExternalLoginInfo(settings);
        }
    }
}
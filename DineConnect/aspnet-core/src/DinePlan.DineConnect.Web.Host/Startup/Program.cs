﻿using Abp.Timing;
using DinePlan.DineConnect.Web.Helpers;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace DinePlan.DineConnect.Web.Startup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Clock.Provider = ClockProviders.Utc;
            CurrentDirectoryHelpers.SetCurrentDirectory();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return new WebHostBuilder()
                .UseKestrel(opt =>
                {
                    opt.AddServerHeader = false;
                    opt.Limits.MaxRequestLineSize = 16 * 1024;
                })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIIS()
                .UseIISIntegration()
                .UseStartup<Startup>();
        }
    }
}

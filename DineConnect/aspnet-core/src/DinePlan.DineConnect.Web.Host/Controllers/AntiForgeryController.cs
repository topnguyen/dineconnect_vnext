﻿using Microsoft.AspNetCore.Antiforgery;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class AntiForgeryController : DineConnectControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.UI;
using Abp.Web.Models;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Web.Models.FileManagerInput;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class FileManagerController : DineConnectControllerBase
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public FileManagerController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [System.Web.Http.HttpPost]
        [DontWrapResult]
        public JsonResult GetListUrl([FromBody] GetFileManagerInput input)
        {
            var path = "_Tickets/" + input.Path;
           
            var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

            DirectoryInfo di = new DirectoryInfo(fullPath);

            var list = new List<FileManager>();

            try
            {
                var directories = di.GetDirectories("*");
                foreach (var dir in directories)
                {
                    list.Add(new FileManager
                    {
                        Name = dir.Name,
                        FullPath = dir.FullName,
                        Rights = "drwxr-xr-x",
                        Size = GetDirectorySize(dir).ToString(),
                        DateStr = dir.CreationTime.ToString("dd-MM-yy HH:mm"),
                        Date = dir.CreationTime,
                        Type = "dir"
                    });
                }

                list.AddRange(GetFiles(di));
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("There was an error with UnauthorizedAccessException");
            }

            var result = new FileManagerResult { Result = list };

            return Json(result);
        }

        public long GetDirectorySize(DirectoryInfo dir)
        {
            FileInfo[] files = dir.GetFiles("*", SearchOption.AllDirectories);

            long size = 0;
            foreach (var file in files)
            {
                size += file.Length;
            }
            return size;
        }

        public List<FileManager> GetFiles(DirectoryInfo dir)
        {
            var files = dir.GetFiles("*");
            var list = files.Select(f => new FileManager
            {
                Name = f.Name,
                FullPath = f.FullName,
                Rights = "-rw-r--r--",
                Size = f.Length.ToString(),
                DateStr = f.CreationTime.ToString("dd-MM-yy HH:mm"),
                Date = f.CreationTime,
                Type = "file"
            }).ToList();

            return list;
        }

        public ActionResult DownloadFileUrl([FromBody] GetFileManagerInput input)
        {
            var pathSplit = input.Path.Split(new[] { "/" }, StringSplitOptions.None);

            var fileName = pathSplit[pathSplit.Length - 1];

            var path = "_Tickets/" + input.Path;

            var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

            if (!System.IO.File.Exists(fullPath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }

            return File(fullPath, MimeTypeNames.ApplicationOctetStream, fileName);
        }

        public ActionResult DownloadMultipleUrl([FromBody] DownloadMultipleUrlInput input)
        {
            //var pathSplit = input.Path.Split(new[] { "/" }, StringSplitOptions.None);
            //var fileName = pathSplit[pathSplit.Length - 1];

            //var path = Server.MapPath("~/Common" + input.Path);

            //var filePath = Path.Combine(path);
            //if (!System.IO.File.Exists(filePath))
            //{
            //    throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            //}

            //return File(filePath, MimeTypeNames.ApplicationOctetStream, fileName);

            return null;
        }
    }
}

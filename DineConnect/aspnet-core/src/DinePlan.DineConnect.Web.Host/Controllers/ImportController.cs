﻿using Abp.Authorization;
using DinePlan.DineConnect.Connect.Card.Card;
using DinePlan.DineConnect.Tiffins.TrackPod.TrackRoute;

namespace DinePlan.DineConnect.Web.Controllers
{
	[AbpAuthorize]
	public class ImportController : ImportControllerBase
	{
		public ImportController(IConnectCardAppService connectCardAppService, IShipmentRouteAppService shipmentRouteAppService) : base(connectCardAppService, shipmentRouteAppService)
		{
		}
	}
}
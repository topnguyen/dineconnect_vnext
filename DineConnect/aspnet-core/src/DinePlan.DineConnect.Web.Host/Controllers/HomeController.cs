﻿using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Timing;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Tiffins.Orders;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class HomeController : DineConnectControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ITiffinOrderAppService _tiffinOrderAppService;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public HomeController(IWebHostEnvironment webHostEnvironment, ITiffinOrderAppService tiffinOrderAppService, 
            ITimeZoneConverter timeZoneConverter)
        {
            _webHostEnvironment = webHostEnvironment;
            _tiffinOrderAppService = tiffinOrderAppService;
            _timeZoneConverter = timeZoneConverter;
        }

        [DisableAuditing]
        public IActionResult Index()
        {
            if (_webHostEnvironment.IsDevelopment())
            {
                return RedirectToAction("Index", "Ui");
            }

            return Redirect("/index.html");
        }

        [HttpGet("~/env")]
        [DisableAuditing]
        public async Task<IActionResult> Env()
        {
            var dateTimeNow = Clock.Now;

            if (AbpSession.TenantId.HasValue)
            {
                if (_timeZoneConverter != null)
                    // ReSharper disable once PossibleInvalidOperationException
                    dateTimeNow = _timeZoneConverter.Convert(dateTimeNow, AbpSession.TenantId.Value).Value;
            }

            var dateTimeTomorrow = dateTimeNow.AddDays(1);

            return Ok(new
            {
                Environment = _webHostEnvironment.EnvironmentName,
                System.Diagnostics.Process.GetCurrentProcess().StartTime,
                DateTimeNow = dateTimeNow,
                DateTimeKind = Clock.Kind.ToString(),
                CutOffTime = await _tiffinOrderAppService.GetCutOffTime(),
                CutOffProceedForNow = await _tiffinOrderAppService.GetCutOffProceed(dateTimeNow),
                CutOffProceedForTomorrow = await _tiffinOrderAppService.GetCutOffProceed(dateTimeTomorrow)
            });
        }
    }
}

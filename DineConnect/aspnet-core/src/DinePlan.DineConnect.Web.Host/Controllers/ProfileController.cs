﻿using Microsoft.AspNetCore.Authorization;
using DinePlan.DineConnect.Authorization.Users.Profile;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Web.Controllers
{
    [Authorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(
            ITempFileCacheManager tempFileCacheManager,
            IProfileAppService profileAppService) :
            base(tempFileCacheManager, profileAppService)
        {
        }
    }
}
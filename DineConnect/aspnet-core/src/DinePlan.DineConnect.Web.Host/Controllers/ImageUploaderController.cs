﻿using Abp.Authorization;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpAuthorize]
    public class ImageUploaderController : ImageUploaderControllerBase
    {
        public ImageUploaderController(ITempFileCacheManager tempFileCacheManager, 
            TenantManager tenantManager,
            IBinaryObjectManager binaryObjectManager, ICloudImageUploader imageUploader) :
            base(tempFileCacheManager, tenantManager, imageUploader, binaryObjectManager)
        {
        }
    }
}
﻿using Abp.AspNetCore.Mvc.Views;

namespace DinePlan.DineConnect.Web.Views
{
    public abstract class DineConnectRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected DineConnectRazorPage()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }
    }
}

﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Web.Models.FileManagerInput
{
    public class DownloadMultipleUrlInput
    {
        public string Action { get; set; }

        public string Path { get; set; }

        public List<string> Items { get; set; }
    }
}
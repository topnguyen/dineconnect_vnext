﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Web.Models.FileManagerInput
{
    public class FileManagerResult
    {
        public FileManagerResult()
        {
            Result = new List<FileManager>();
        }
        public List<FileManager> Result { get; set; }
    }

    public class FileManager
    {
        public string Name { get; set; }
        public string Rights { get; set; }

        public string Size { get; set; }
        public DateTime Date { get; set; }
        public string DateStr { get; set; }

        public string Type { get; set; }
        public string FullPath { get; set; }
    }
}

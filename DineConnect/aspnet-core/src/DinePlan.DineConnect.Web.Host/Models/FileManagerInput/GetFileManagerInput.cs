﻿namespace DinePlan.DineConnect.Web.Models.FileManagerInput
{
    public class GetFileManagerInput
    {
        public string Action { get; set; }

        public string Path { get; set; }
    }
}
﻿using System;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.BackgroundJobs;
using Abp.DynamicEntityParameters;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using DinePlan.DineConnect.AddonSettings.Dtos;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Authorization.Accounts.Dto;
using DinePlan.DineConnect.Authorization.Delegation;
using DinePlan.DineConnect.Authorization.Permissions.Dto;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Roles.Dto;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Delegation.Dto;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Authorization.Users.Importing.Dto;
using DinePlan.DineConnect.Authorization.Users.Profile.Dto;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Chat;
using DinePlan.DineConnect.Chat.Dto;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Language;
using DinePlan.DineConnect.Connect.Master.Calculations;
using DinePlan.DineConnect.Connect.Master.Calculations.Dtos;
using DinePlan.DineConnect.Connect.Master.Cards;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master.DineDevices;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.LocationGroups;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using DinePlan.DineConnect.Connect.Master.LocationTags;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Numberators;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.Connect.Master.PlanReasons;
using DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Master.Terminals;
using DinePlan.DineConnect.Connect.Master.Terminals.Dtos;
using DinePlan.DineConnect.Connect.Master.Tickets.Dtos;
using DinePlan.DineConnect.Connect.Master.TicketTags.Dtos;
using DinePlan.DineConnect.Connect.Master.TicketTypes;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using DinePlan.DineConnect.Connect.MutilLanguage;
using DinePlan.DineConnect.Connect.MutilLanguage.Dtos;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Print.Condition.Dtos;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Connect.Print.Job.Dtos;
using DinePlan.DineConnect.Connect.Print.Printers.Dtos;
using DinePlan.DineConnect.Connect.Print.Template.Dtos;
using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Table;
using DinePlan.DineConnect.Connect.Table.Dtos;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos;
using DinePlan.DineConnect.Core.TemplateEngines;
using DinePlan.DineConnect.Cors.Dto;
using DinePlan.DineConnect.CORS;
using DinePlan.DineConnect.DineQueue;
using DinePlan.DineConnect.DineQueue.Dtos;
using DinePlan.DineConnect.DynamicEntityParameters.Dto;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.Editions.Dto;
using DinePlan.DineConnect.Framework;
using DinePlan.DineConnect.Friendships;
using DinePlan.DineConnect.Friendships.Cache;
using DinePlan.DineConnect.Friendships.Dto;
using DinePlan.DineConnect.Go;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Localization.Dto;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.MultiTenancy.Dto;
using DinePlan.DineConnect.MultiTenancy.HostDashboard.Dto;
using DinePlan.DineConnect.MultiTenancy.Payments;
using DinePlan.DineConnect.MultiTenancy.Payments.Dto;
using DinePlan.DineConnect.Notifications.Dto;
using DinePlan.DineConnect.Organizations.Dto;
using DinePlan.DineConnect.PaymentProcessor.Dto;
using DinePlan.DineConnect.Sessions.Dto;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Shipment.Dtos;
using DinePlan.DineConnect.Tenants.TenantDatabases.Dtos;
using DinePlan.DineConnect.Tiffins;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.Dtos;
using DinePlan.DineConnect.Tiffins.Faq;
using DinePlan.DineConnect.Tiffins.Faq.Dtos;
using DinePlan.DineConnect.Tiffins.HomeBanner;
using DinePlan.DineConnect.Tiffins.HomeBanner.Dtos;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MealTimes;
using DinePlan.DineConnect.Tiffins.MealTimes.Dtos;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.OrderTags;
using DinePlan.DineConnect.Tiffins.OrderTags.Dtos;
using DinePlan.DineConnect.Tiffins.ProductOffers.Dtos;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos;
using DinePlan.DineConnect.WebHooks.Dto;
using DinePlan.DineConnect.Wheel;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.WheelTable;
using DinePlan.DineConnect.Wheel.V2;
using System.Linq;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using LocationSchedule = DinePlan.DineConnect.Connect.Master.Locations.LocationSchedule;
using TiffinDeliveryTimeSlot = DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.TiffinDeliveryTimeSlot;
using System.Collections.Generic;
using DinePlan.DineConnect.BaseCore.TemplateEngines;
using DinePlan.DineConnect.Clique.Payment.Dtos;
using DinePlan.DineConnect.Clique.CliqueScreenMenu.Dtos;
using DinePlan.DineConnect.Clique.CliqueMealTimes.Dtos;
using DinePlan.DineConnect.Clique;
using DinePlan.DineConnect.Clique.CliqueCustomer.Dtos;
using DinePlan.DineConnect.Cluster;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Flyers.Flyers.Dtos;

namespace DinePlan.DineConnect
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateOrEditSyncerDto, Syncer>().ReverseMap();
            configuration.CreateMap<SyncerDto, Syncer>().ReverseMap();
            configuration.CreateMap<CreateOrEditLanguageDescriptionDto, LanguageDescription>().ReverseMap();
            configuration.CreateMap<LanguageDescriptionDto, LanguageDescription>().ReverseMap();

            #region Core


            configuration.CreateMap<CreateOrEditAddonDto, Addon>().ReverseMap();

            configuration.CreateMap<CreateOrEditTemplateEngineDto, TemplateEngine>().ReverseMap();
            configuration.CreateMap<TemplateEngineDto, TemplateEngine>().ReverseMap();

            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            configuration.CreateMap<DinePlanLanguageText, DinePlanLanguageTextListDto>();

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();
            configuration.CreateMap<TenantDatabaseDto, TenantDatabase>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicParameter, DynamicParameterDto>().ReverseMap();
            configuration.CreateMap<DynamicParameterValue, DynamicParameterValueDto>().ReverseMap();
            configuration.CreateMap<EntityDynamicParameter, EntityDynamicParameterDto>()
                .ForMember(dto => dto.DynamicParameterName,
                    options => options.MapFrom(entity => entity.DynamicParameter.ParameterName));
            configuration.CreateMap<EntityDynamicParameterDto, EntityDynamicParameter>();

            configuration.CreateMap<EntityDynamicParameterValue, EntityDynamicParameterValueDto>().ReverseMap();
            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */

            //Address
            configuration.CreateMap<Address, AddressDto>()
                .ForMember(ldto => ldto.CityName, opt => opt.MapFrom(src => src.City.Name))
                .ForMember(ldto => ldto.StateName, opt => opt.MapFrom(src => src.City.State.Name))
                .ForMember(ldto => ldto.StateId, opt => opt.MapFrom(src => src.City.StateId))
                .ForMember(ldto => ldto.CountryName, opt => opt.MapFrom(src => src.City.Country.Name))
                .ForMember(ldto => ldto.CountryId, opt => opt.MapFrom(src => src.City.CountryId))
                .ReverseMap();

            configuration.CreateMap<City, CityDto>().ReverseMap();
            configuration.CreateMap<State, State>().ReverseMap();
            configuration.CreateMap<StateDto, State>().ReverseMap();
            configuration.CreateMap<State, StateDto>()
                .ForMember(ldto => ldto.CountryName, opt => opt.MapFrom(src => src.Country.Name));
            configuration.CreateMap<Country, CountryDto>().ReverseMap();
            configuration.CreateMap<Country, Country>().ReverseMap();

            //TenatDatabase
            configuration.CreateMap<TenantDatabase, TenantDatabaseListDto>();
            configuration.CreateMap<CreateOrUpdateTenantDatabaseInput, TenantDatabase>();
            configuration.CreateMap<TenantDatabase, TenantDatabaseEditDto>();

            configuration.CreateMap<AllowOrigin, GetAllowOriginForViewDto>();
            configuration.CreateMap<AllowOrigin, GetAllowOriginForEditDto>();
            configuration.CreateMap<CreateOrEditAllowOriginInput, AllowOrigin>();

            #endregion Core

            #region Connect

            #region Taxes

            configuration.CreateMap<DinePlanTaxMappingEditDto, DinePlanTaxMapping>().ReverseMap();
            configuration.CreateMap<DinePlanTaxLocationEditDto, DinePlanTaxLocation>().ReverseMap();
            configuration.CreateMap<CreateOrEditDinePlanTaxDto, DinePlanTax>().ReverseMap();
            configuration.CreateMap<DinePlanTax, DinePlanTaxDto>()
                .ForMember(ldto => ldto.TransactionTypeName, opt => opt.MapFrom(src => src.TransactionType.Name))
                .ReverseMap();
            configuration.CreateMap<DinePlanOpeningHoursDto, WheelOpeningHour>().ReverseMap();

            #endregion Taxes

            #region Promotions

            configuration.CreateMap<DemandPromotionExecutionEditDto, DemandPromotionExecution>().ReverseMap();
            configuration.CreateMap<FreeValuePromotionExecutionEditDto, FreeValuePromotionExecution>().ReverseMap();
            configuration.CreateMap<FreePromotionExecutionEditDto, FreePromotionExecution>().ReverseMap();
            configuration.CreateMap<TimerPromotionEditDto, TimerPromotion>().ReverseMap();
            configuration.CreateMap<FixedPromotionEditDto, FixedPromotion>().ReverseMap();
            configuration.CreateMap<FreePromotionEditDto, FreePromotion>().ReverseMap();
            configuration.CreateMap<FreeValuePromotionEditDto, FreeValuePromotion>().ReverseMap();
            configuration.CreateMap<DemandPromotionEditDto, DemandDiscountPromotion>().ReverseMap();
            configuration.CreateMap<FreeItemPromotionEditDto, FreeItemPromotion>().ReverseMap();
            configuration.CreateMap<TicketDiscountPromotionDto, TicketDiscountPromotion>().ReverseMap();
            configuration.CreateMap<FreeItemPromotionExecution, FreeItemPromotionExecutionEditDto>().ReverseMap();
            configuration.CreateMap<BuyXAndYAtZValuePromotion, BuyXAndYAtZValuePromotionEditDto>().ReverseMap();
            configuration.CreateMap<BuyXAndYAtZValuePromotionExecutionEditDto, BuyXAndYAtZValuePromotionExecution>().ReverseMap();
            configuration.CreateMap<StepDiscountPromotion, StepDiscountPromotionEditDto>().ReverseMap();
            configuration.CreateMap<StepDiscountPromotionExecution, StepDiscountPromotionExecutionEditDto>().ReverseMap();
            configuration.CreateMap<StepDiscountPromotionMappingExecution, StepDiscountPromotionMappingExecutionEditDto>().ReverseMap();

            configuration.CreateMap<PromotionScheduleEditDto, PromotionSchedule>().ReverseMap();

            configuration.CreateMap<PromotionRestrictItemEditDto, PromotionRestrictItem>()
                .ReverseMap();

            configuration.CreateMap<Promotion, CreateOrEditPromotionDto>()
                .ReverseMap();
            configuration.CreateMap<Promotion, Connect.Discount.Dtos.PromotionsDto>()
                .ForMember(ldto => ldto.PromotionType,
                    opt => opt.MapFrom(src => ((PromotionTypes)src.PromotionTypeId).ToString()))
                .ReverseMap();

            configuration.CreateMap<PromotionCategoryListDto, PromotionCategory>();

            configuration.CreateMap<PromotionCategory, PromotionCategoryListDto>();

            configuration.CreateMap<PromotionCategoryEditDto, PromotionCategory>();

            configuration.CreateMap<PromotionCategory, PromotionCategoryEditDto>();
            configuration.CreateMap<PromotionQuota, PromotionQuotaDto>().ReverseMap(); ;

            #endregion Promotions

            configuration.CreateMap<LocationTagLocationDto, LocationTagLocation>().ReverseMap();

            configuration.CreateMap<CreateOrEditDepartmentDto, Department>();

            configuration.CreateMap<Department, CreateOrEditDepartmentDto>()
                .ForMember(ldto => ldto.DepartmentGroupName, opt => opt.MapFrom(src => src.DepartmentGroup.Name));

            configuration.CreateMap<Department, DepartmentDto>()
                .ForMember(ldto => ldto.PriceTagName, opt => opt.MapFrom(src => src.PriceTag.Name))
                .ForMember(ldto => ldto.DepartmentGroupName, opt => opt.MapFrom(src => src.DepartmentGroup.Name))
                .ReverseMap();

            configuration.CreateMap<DepartmentGroupListDto, DepartmentGroup>().ReverseMap();

            configuration.CreateMap<DepartmentGroupEditDto, DepartmentGroup>().ReverseMap();

            configuration.CreateMap<CreateOrEditPriceTagDto, PriceTag>().ReverseMap();
            configuration.CreateMap<PriceTagDto, PriceTag>().ReverseMap();
            configuration.CreateMap<PriceTagLocationPrice, PriceTagLocationPriceEditDto>().ReverseMap();
            configuration.CreateMap<PriceTagLocationPrice, PriceTagLocationPriceListDto>()
                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            configuration.CreateMap<CreateOrEditTicketTagGroupDto, TicketTagGroup>().ReverseMap();
            configuration.CreateMap<TicketTagGroup, TicketTagGroupDto>()
                .ForMember(ldto => ldto.DataTypeName,
                    opt => opt.MapFrom(src => ((TicketTagGroupType)src.DataType).ToString()))
                .ReverseMap();
            configuration.CreateMap<TicketTagDto, TicketTag>().ReverseMap();

            configuration.CreateMap<CreateOrEditFutureDateInformationDto, FutureDateInformation>().ReverseMap();
            configuration.CreateMap<FutureDateInformation, FutureDateInformationDto>()
                .ForMember(ldto => ldto.FutureDateTypeName,
                    opt => opt.MapFrom(src => ((FutureDateType)src.FutureDateType).ToString()))
                .ReverseMap();

            configuration.CreateMap<CreateOrEditTerminalDto, Terminal>().ReverseMap();
            configuration.CreateMap<TerminalDto, Terminal>().ReverseMap();

            configuration.CreateMap<Calculation, ApiCalculation>().ReverseMap();
            configuration.CreateMap<CreateOrEditCalculationDto, Calculation>().ReverseMap();
            configuration.CreateMap<Calculation, CalculationDto>()
                .ForMember(ldto => ldto.TransactionTypeName, opt => opt.MapFrom(src => src.TransactionType.Name))
                .ForMember(ldto => ldto.CalculationTypeName,
                    opt => opt.MapFrom(src => ((CalculationType)src.CalculationTypeId).ToString()))
                .ForMember(ldto => ldto.ConfirmationTypeName,
                    opt => opt.MapFrom(src => ((ConfirmationType)src.ConfirmationType).ToString()))
                .ReverseMap();

            configuration.CreateMap<CreateOrEditTillAccountDto, TillAccount>().ReverseMap();
            configuration.CreateMap<TillAccount, TillAccountDto>()
                .ForMember(ldto => ldto.TillAccountType,
                    opt => opt.MapFrom(src => ((Connect.Master.TillAccounts.TillAccounTypes)src.TillAccountTypeId).ToString()))
                .ReverseMap();

            configuration.CreateMap<CreateOrEditTransactionTypeDto, TransactionType>().ReverseMap();
            configuration.CreateMap<TransactionTypeDto, TransactionType>().ReverseMap();

            configuration.CreateMap<CreateOrEditPlanReasonDto, PlanReason>().ReverseMap();
            configuration.CreateMap<PlanReasonDto, PlanReason>().ReverseMap();

            configuration.CreateMap<CreateOrEditForeignCurrencyDto, ForeignCurrency>().ReverseMap();
            configuration.CreateMap<ForeignCurrency, ForeignCurrencyDto>()
                .ForMember(ldto => ldto.PaymentTypeName, opt => opt.MapFrom(src => src.PaymentType.Name))
                .ReverseMap();

            configuration.CreateMap<PaymentType, CreateOrEditPaymentTypeDto>();

            configuration.CreateMap<CreateOrEditPaymentTypeDto, PaymentType>();

            configuration.CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();
            configuration.CreateMap<PaymentType, PaymentTypeDto>()
                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            configuration.CreateMap<CreateOrEditLocationTagDto, LocationTag>().ReverseMap();
            configuration.CreateMap<LocationTagDto, LocationTag>().ReverseMap();
            configuration.CreateMap<LocationScheduleEditDto, LocationSchedule>().ReverseMap();
            configuration.CreateMap<LocationBranchEditDto, LocationBranch>().ReverseMap();
            configuration.CreateMap<IdNameDto, LocationTag>().ReverseMap();

            configuration.CreateMap<CreateOrEditProductGroupDto, ProductGroup>().ReverseMap();
            configuration.CreateMap<ProductGroupDto, ProductGroup>().ReverseMap();

            configuration.CreateMap<ScreenMenuEditDto, ScreenMenu>().ReverseMap();
            configuration.CreateMap<GetAllScreenMenuDto, ScreenMenu>().ReverseMap();
            configuration.CreateMap<ScreenMenuCategoryListDto, ScreenMenuCategory>().ReverseMap();
            configuration.CreateMap<ScreenMenuCategoryEditInput, ScreenMenuCategory>().ReverseMap();
            configuration.CreateMap<CliqueScreenMenuDto, ScreenMenu>().ReverseMap();

            configuration.CreateMap<ImportLocationDto, Location>();

            //Company
            configuration.CreateMap<Brand, BrandDto>()
                .ReverseMap();

            //Location
            configuration.CreateMap<Location, LocationDto>()
                .ForMember(ldto => ldto.CityName, opt => opt.MapFrom(src => src.City.Name))
                .ForMember(ldto => ldto.CountryName, opt => opt.MapFrom(src => src.City.Country.Name))
                .ForMember(ldto => ldto.StateName, opt => opt.MapFrom(src => src.City.State.Name))
                .ForMember(ldto => ldto.CountryId, opt => opt.MapFrom(src => src.City.CountryId))
                .ForMember(ldto => ldto.StateId, opt => opt.MapFrom(src => src.City.StateId))
                .ForMember(ldto => ldto.LocationGroups, opt => opt.MapFrom(src => src.LocationGroups))
                .ForMember(ldto => ldto.LocationTags, opt => opt.MapFrom(src => src.LocationTags))
                .ReverseMap();

            configuration.CreateMap<LocationTagLocation, CreateOrEditLocationTagDto>()
                .ForMember(ldto => ldto.Id, opt => opt.MapFrom(src => src.LocationTag.Id))
                .ForMember(ldto => ldto.Name, opt => opt.MapFrom(src => src.LocationTag.Name))
                .ForMember(ldto => ldto.Code, opt => opt.MapFrom(src => src.LocationTag.Code)).ReverseMap(); 
            configuration.CreateMap<LocationTagLocation, LocationTagDto>()
                .ForMember(ldto => ldto.Id, opt => opt.MapFrom(src => src.LocationTag.Id))
                .ForMember(ldto => ldto.Name, opt => opt.MapFrom(src => src.LocationTag.Name))
                .ForMember(ldto => ldto.Code, opt => opt.MapFrom(src => src.LocationTag.Code)).ReverseMap(); 

            configuration.CreateMap<LocationGroup, LocationGroupDto>()
               .ForMember(ldto => ldto.Id, opt => opt.MapFrom(src => src.Group.Id))
               .ForMember(ldto => ldto.Name, opt => opt.MapFrom(src => src.Group.Name))
               .ForMember(ldto => ldto.Code, opt => opt.MapFrom(src => src.Group.Code)).ReverseMap();

            configuration.CreateMap<Location, SimpleLocationDto>().ReverseMap();
            configuration.CreateMap<LocationGroup, SimpleLocationGroupDto>().ReverseMap();

            //Category
            configuration.CreateMap<Category, CategoryDto>()
                .ForMember(ldto => ldto.GroupName, opt => opt.MapFrom(src => src.ProductGroup.Name))
                .ReverseMap();

            //MenuItem
            configuration.CreateMap<MenuItem, MenuItemListDto>()
                .ForMember(ldto => ldto.CategoryCode, opt => opt.MapFrom(src => src.Category.Code))
                .ForMember(ldto => ldto.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                .ForMember(ldto => ldto.ProductGroupCode, opt => opt.MapFrom(src => src.Category.ProductGroup.Name))
                .ForMember(ldto => ldto.ProductGroupName, opt => opt.MapFrom(src => src.Category.ProductGroup.Name))
                .ReverseMap();

            configuration.CreateMap<MenuItem, MenuItemEditDto>()                
                .ReverseMap()
                .ForMember(x => x.CategoryId, opt => opt.Ignore())
                .ForMember(x => x.Category, opt => opt.Ignore())
                .ForMember(x => x.TransactionTypeId, opt => opt.Ignore())
                .ForMember(x => x.TransactionType, opt => opt.Ignore());
            configuration.CreateMap<Category, CategoryOutputEditDto>().ReverseMap();
            configuration.CreateMap<ProductGroup, ProductGroupOutputEditDto>().ReverseMap();

            configuration.CreateMap<MenuItemPortionEditDto, MenuItemPortion>()
                .ForMember(ldto => ldto.MenuItem, opt => opt.Ignore())
                .ReverseMap();

            configuration.CreateMap<MenuItem, MenuItemAndPriceForSelection>()
                .ForMember(ldto => ldto.Prices, opt => opt.MapFrom(src => src.Portions));

            configuration.CreateMap<MenuBarCode, MenuBarCodeEditDto>().ReverseMap();
            configuration.CreateMap<UpMenuItemEditDto, UpMenuItem>()
                .ForMember(ldto => ldto.MenuItem, opt => opt.Ignore())
                .ReverseMap();

            #region Combo

            configuration.CreateMap<ProductCombo, ProductComboDto>()
                .ForMember(x => x.ProductComboGroupDtos,
                    o => o.MapFrom(y => y.ComboGroups))
                .ReverseMap();
            configuration.CreateMap<CreateOrEditComboInputDto, ProductCombo>().ReverseMap();

            configuration.CreateMap<ProductComboGroup, ProductComboGroupEditDto>().ReverseMap();
            configuration.CreateMap<ProductComboItem, ProductComboItemEditDto>().ReverseMap();
            //configuration.CreateMap<ProductCombo, ProductComboEditDto>().ReverseMap();

            configuration.CreateMap<ProductComboGroup, GetAllProductComboGroupDto>().ReverseMap();
            configuration.CreateMap<ProductComboGroup, ProductComboGroupDto>().ReverseMap();
            configuration.CreateMap<ProductComboGroup, CreateEditProductComboGroupInput>().ReverseMap();
            configuration.CreateMap<ProductComboItem, CreateEditProductComboItemDto>().ReverseMap();
            configuration.CreateMap<ApiProductComboOutput, ProductCombo>().ReverseMap();
            configuration.CreateMap<ApiProductComboGroupOutput, ProductComboGroup>().ReverseMap();
            configuration.CreateMap<ApiProductComboItemOutput, ProductComboItem>().ReverseMap();

            #endregion Combo

            configuration.CreateMap<LocationMenuItem, LocationMenuItemDataReaderDto>().ReverseMap();

            // Menu Screen

            configuration.CreateMap<ScreenMenuItem, ScreenMenuItemListDto>();
            configuration.CreateMap<ScreenMenuItem, GetMenuItemsOuputDto>();
            configuration.CreateMap<ScreenMenuItem, ScreenMenuItemEditDto>().ReverseMap();
            configuration.CreateMap<ScreenMenuItemEditDto, ScreenMenuItemListDto>();

            // Clique
            configuration.CreateMap<CliqueMealTime, CliqueMealTimeListDto>();

            // Order Tag

            configuration.CreateMap<OrderTagGroup, OrderTagGroupListDto>();
            configuration.CreateMap<OrderTagGroup, WheelOrderTagGroupListDto>();
            configuration.CreateMap<OrderTagGroup, OrderTagGroupEditDto>()
                .ForMember(ldto => ldto.Files, opt => opt.Ignore());
            configuration.CreateMap<OrderTagGroupEditDto, OrderTagGroup>();

            configuration.CreateMap<OrderTag, OrderTagDto>()
                .ForMember(ldto => ldto.Files, opt => opt.Ignore());

            configuration.CreateMap<OrderTagDto, OrderTag>();

            configuration.CreateMap<OrderTag, WheelOrderTagDto>();
            configuration.CreateMap<OrderTagMap, OrderMapDto>().ReverseMap();

            configuration.CreateMap<OrderTagLocationPriceEditDto, OrderTagLocationPrice>();
            configuration.CreateMap<OrderTagLocationPrice, OrderTagLocationPriceEditDto>()
                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            // Numerator
            configuration.CreateMap<NumeratorListDto, Numerator>().ReverseMap();

            configuration.CreateMap<NumeratorEditDto, Numerator>().ReverseMap();

            #region DinePlanUSer

            configuration.CreateMap<DinePlanUser, ApiDinePlanUserListDto>()
                .ForMember(dto => dto.DinePlanUserRoleName, opts => opts.MapFrom(a => a.DinePlanUserRole.Name));

            #endregion DinePlanUSer

            #region Ticket

            configuration.CreateMap<TicketEditDto, Ticket>().ReverseMap();
            configuration.CreateMap<OrderEditDto, Order>().ReverseMap();
            configuration.CreateMap<PaymentEditDto, Connect.Transaction.Payment>().ReverseMap();
            configuration.CreateMap<TicketTransactionDto, TicketTransaction>().ReverseMap();

            // Ticket Type
            configuration.CreateMap<TicketTypeListDto, TicketType>().ReverseMap();

            configuration.CreateMap<CreateOrUpdateTicketTypeInput, TicketType>().ReverseMap();

            configuration.CreateMap<CreateOrUpdateTicketTypeInput, TicketTypeEditDto>().ReverseMap();

            configuration.CreateMap<TicketTypeEditDto, TicketType>().ReverseMap();

            // Ticket Type Configuration
            configuration.CreateMap<TicketTypeConfigurationListDto, TicketTypeConfiguration>().ReverseMap();

            configuration.CreateMap<TicketTypeConfigurationEditDto, TicketTypeConfiguration>().ReverseMap();

            #endregion Ticket

            configuration.CreateMap<ProductCombo, ApiProductComboOutput>().ReverseMap();
            configuration.CreateMap<MenuBarCode, ApiMenuBarcodeOutput>().ReverseMap();
            configuration.CreateMap<UpMenuItem, ApiUpMenuItemOutput>().ReverseMap();
            configuration.CreateMap<MenuItemSchedule, ApiMenuItemSchedule>().ReverseMap();

            configuration.CreateMap<ScreenMenu, ApiScreenMenu>().ReverseMap();
            configuration.CreateMap<ScreenMenuCategory, ApiScreenCategory>().ReverseMap();
            configuration.CreateMap<ScreenMenuItem, ApiScreenMenuItem>().ReverseMap();

            configuration.CreateMap<DinePlanUser, ApiDinePlanUserListDto>().ReverseMap();
            configuration.CreateMap<TicketTag, TicketTagListDto>().ReverseMap();
            configuration.CreateMap<TicketTagGroup, TicketTagGroupEditDto>().ReverseMap();

            #region Print

            configuration.CreateMap<PrintConfiguration, GetAllPrinterDto>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, CreatePrinterInput>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, EditPrinterInput>().ReverseMap();

            configuration.CreateMap<Printer, GetAllPrinterMappingDto>().ReverseMap();
            configuration.CreateMap<Printer, CreateOrEditPrinterMappingInput>().ReverseMap();
            configuration.CreateMap<Printer, ApiPrinterDto>().ReverseMap();

            configuration.CreateMap<PrintConfiguration, GetAllPrintTemplateDto>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, CreatePrintTemplateInput>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, EditPrintTemplateInput>().ReverseMap();

            configuration.CreateMap<PrintTemplate, GetAllPrintTemplateMappingDto>().ReverseMap();
            configuration.CreateMap<PrintTemplate, CreateOrEditPrintTemplateMappingInput>().ReverseMap();
            configuration.CreateMap<PrintTemplate, ApiPrinterTemplateDto>().ReverseMap();

            configuration.CreateMap<PrintConfiguration, GetAllPrintJobDto>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, CreatePrintJobInput>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, EditPrintJobInput>().ReverseMap();

            configuration.CreateMap<PrintJob, GetAllPrintJobMappingDto>().ReverseMap();
            configuration.CreateMap<PrintJob, CreateOrEditPrintJobMappingInput>().ReverseMap();
            configuration.CreateMap<PrinterMap, CreateOrEditPrinterMapInput>()
                .ForMember(ldto => ldto.DepartmentName, opt => opt.MapFrom(src => src.Department.Name))
                .ForMember(ldto => ldto.MenuItemName, opt => opt.MapFrom(src => src.MenuItemGroupCode))
                .ReverseMap();
            configuration.CreateMap<PrintJob, ApiPrintJob>().ReverseMap();
            configuration.CreateMap<PrinterMap, ApiPrinterMap>().ReverseMap();

            configuration.CreateMap<PrintConfiguration, GetAllPrintConditionDto>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, CreatePrintConditionInput>().ReverseMap();
            configuration.CreateMap<PrintConfiguration, EditPrintConditionInput>().ReverseMap();

            configuration.CreateMap<PrintTemplateCondition, GetAllPrintConditionMappingDto>().ReverseMap();
            configuration.CreateMap<PrintTemplateCondition, CreateOrEditPrintConditionMappingInput>().ReverseMap();
            configuration.CreateMap<PrintTemplateCondition, ApiPrinterTemplateConditionDto>().ReverseMap();

            configuration.CreateMap<DineDeviceListDto, DineDevice>().ReverseMap();

            configuration.CreateMap<DineDeviceEditDto, DineDevice>().ReverseMap();

            configuration.CreateMap<DineDeviceListDto, DineDeviceEditDto>().ReverseMap();

            #endregion Print

            #region ProgramSetting

            configuration.CreateMap<ProgramSettingTemplate, GetAllProgramSettingTemplateDto>().ReverseMap();
            configuration.CreateMap<ProgramSettingTemplate, CreateProgramSettingTemplateInput>().ReverseMap();
            configuration.CreateMap<ProgramSettingTemplate, EditProgramSettingTemplateInput>().ReverseMap();

            configuration.CreateMap<ProgramSettingValue, GetAllProgramSettingValueDto>().ReverseMap();
            configuration.CreateMap<ProgramSettingValue, CreateOrEditProgramSettingValueInput>().ReverseMap();

            #endregion ProgramSetting

            #region Table

            configuration.CreateMap<ConnectTable, ConnectTableListDto>();

            configuration.CreateMap<ConnectTable, ConnectTableEditDto>();

            configuration.CreateMap<ConnectTableEditDto, ConnectTable>();

            configuration.CreateMap<ConnectTableGroup, ConnectTableGroupListDto>();

            configuration.CreateMap<ConnectTableGroup, ConnectTableGroupEditDto>()
                .ForMember(ldto => ldto.ConnectTables, opt => opt.MapFrom(x => x.ConnectTableInGroups.Select(y => y.ConnectTable)))
                ;

            configuration.CreateMap<ConnectTableGroupEditDto, ConnectTableGroup>();

            #endregion Table

            #region Card

            // Card Category

            configuration.CreateMap<ConnectCardTypeCategory, ConnectCardTypeCategoryListDto>().ReverseMap();

            configuration.CreateMap<ConnectCardTypeCategory, ConnectCardTypeCategoryEditDto>().ReverseMap();

            configuration.CreateMap<ConnectCardTypeCategoryListDto, ConnectCardTypeCategoryEditDto>().ReverseMap();

            // Card Type

            configuration.CreateMap<ConnectCardTypeEditDto, ConnectCardType>().ReverseMap();

            configuration.CreateMap<ConnectCardTypeListDto, ConnectCardType>().ReverseMap();

            configuration.CreateMap<ConnectCardTypeListDto, ConnectCardTypeEditDto>().ReverseMap();

            // Card

            configuration.CreateMap<ConnectCardEditDto, ConnectCard>().ReverseMap();

            configuration.CreateMap<ConnectCardListDto, ConnectCard>().ReverseMap();

            configuration.CreateMap<ConnectCardListDto, ConnectCardEditDto>().ReverseMap();

            configuration.CreateMap<ImportCardDataDetailEditDto, ImportCardDataDetail>().ReverseMap();
            configuration.CreateMap<ImportCardDataDetailListDto, ImportCardDataDetail>().ReverseMap();

            #endregion Card

            #region Plan User

            configuration.CreateMap<DinePlanUser, DinePlanUserListDto>()
                .ForMember(dto => dto.DinePlanUserRoleName, opts => opts.MapFrom(a => a.DinePlanUserRole.Name));

            configuration.CreateMap<DinePlanUser, DinePlanUserEditDto>();

            configuration.CreateMap<DinePlanUserEditDto, DinePlanUser>();

            configuration.CreateMap<DinePlanUserRole, DinePlanUserRoleListDto>();

            configuration.CreateMap<DinePlanUserRole, DinePlanUserRoleEditDto>();

            configuration.CreateMap<DinePlanUserRoleEditDto, DinePlanUserRole>();

            #endregion Plan User

            #region Report

            configuration.CreateMap<Ticket, TicketListDto>()
                                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            configuration.CreateMap<TicketViewDto, Ticket>().ReverseMap();

            configuration.CreateMap<AllTicketListDto, Ticket>().ReverseMap();

            configuration.CreateMap<Order, OrderViewDto>()
                                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            configuration.CreateMap<Order, OrderListDto>()
                                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name))
                                .ForMember(ldto => ldto.TicketNumber, opt => opt.MapFrom(src => src.Ticket.TicketNumber))
                                .ForMember(ldto => ldto.TerminalName, opt => opt.MapFrom(src => src.Ticket.TerminalName));

            configuration.CreateMap<AllOrderListDto, Order>().ReverseMap();

            configuration.CreateMap<TicketTransaction, TicketTransactionListDto >()
                .ForMember(ldto => ldto.TransactionTypeName, opt => 
                    opt.MapFrom(src => src.TransactionType.Name));

            configuration.CreateMap<Connect.Transaction.Payment, PaymentListDto >()
                .ForMember(ldto => ldto.PaymentTypeName, opt => 
                    opt.MapFrom(src => src.PaymentType.Name));


            configuration.CreateMap<TransactionOrderTag, TransactionOrderTagReportDto>().ReverseMap();

            #endregion Report

            #region WorkPeriod

            configuration.CreateMap<WorkPeriodEditDto, WorkPeriod>().ReverseMap();

            #endregion WorkPeriod

            #region TillTransactions

            configuration.CreateMap<TillTransactionInput, TillTransaction>().ReverseMap();

            #endregion TillTransactions

            #endregion Connect

            #region Tiffins

            configuration.CreateMap<TiffinOrderTag, AddonListDto>()
                .ForMember(t => t.Label, opt => opt.MapFrom(src => src.Name))
                .ForMember(t => t.Quantity, opt => opt.MapFrom(src => 0))
                .ForMember(t => t.Value, opt => opt.MapFrom(src => src.Id));

            configuration.CreateMap<CreateOrEditTiffinOrderTagDto, TiffinOrderTag>().ReverseMap();
            configuration.CreateMap<TiffinOrderTagDto, TiffinOrderTag>().ReverseMap();

            configuration.CreateMap<CreateOrEditTiffinMealTimeDto, TiffinMealTime>().ReverseMap();
            configuration.CreateMap<TiffinMealTimeDto, TiffinMealTime>().ReverseMap();

            configuration.CreateMap<TiffinCustomerProductOffer, TiffinCustomerProductOfferDto>()
                .ForMember(t => t.ProductOfferName, opt => opt.MapFrom(src => src.ProductOffer.Name))
                .ForMember(t => t.Expiry, opt => opt.MapFrom(src => src.ProductOffer.Expiry))
                .ReverseMap();

            configuration.CreateMap<CreateOrEditCustomerTransactionDto, TiffinCustomerTransaction>().ReverseMap();
            configuration.CreateMap<TiffinCustomerTransactionDto, TiffinCustomerTransaction>().ReverseMap();

            configuration.CreateMap<TiffinCustomerProductOfferOrder, CreateOrderInput>()
                .ForMember(t => t.Address,
                    opt => opt.MapFrom(src =>
                        $"{src.CustomerAddress.Address1}, {src.CustomerAddress.Address2}, {src.CustomerAddress.Address3}, {src.CustomerAddress.City.Name}, {src.CustomerAddress.City.Country.Name}"))
                .ReverseMap();

            configuration.CreateMap<CreateOrEditTiffinProductOfferTypeDto, TiffinProductOfferType>().ReverseMap();
            configuration.CreateMap<TiffinProductOfferTypeDto, TiffinProductOfferType>().ReverseMap();

            configuration.CreateMap<CreateOrEditTiffinProductOfferDto, TiffinProductOffer>().ReverseMap();
            configuration.CreateMap<TiffinProductOfferDto, TiffinProductOffer>().ReverseMap();

            //Product Set
            configuration.CreateMap<TiffinProductSetProduct, ProductSetProductDto>()
                .ForMember(t => t.ProductId, opt => opt.MapFrom(src => src.ProductId))
                .ForMember(t => t.ProductName, opt => opt.MapFrom(src => src.Product.Name));

            configuration.CreateMap<CreateOrEditProductSetDto, TiffinProductSet>()
                .ForMember(t => t.Tag, opt => opt.MapFrom(src => (string.Join(",", src.Tags))))
                .ForMember(ldto => ldto.Products, opt => opt.Ignore());

            configuration.CreateMap<TiffinProductSet, CreateOrEditProductSetDto>()
                .ForMember(t => t.Tags,
                    opt => opt.MapFrom(
                        src => ((src.Tag ?? "").Split(",", System.StringSplitOptions.RemoveEmptyEntries))));

            configuration.CreateMap<TiffinProductSet, TiffinProductOfferLookupTableDto>()
                .ForMember(t => t.Value, opt => opt.MapFrom(src => (src.Id.ToString())));

            configuration.CreateMap<ProductSetDto, TiffinProductSet>().ReverseMap()
                .ForMember(t => t.Tags,
                    opt => opt.MapFrom(
                        src => ((src.Tag ?? "").Split(",", System.StringSplitOptions.RemoveEmptyEntries))));
            ;

            // customer
            configuration.CreateMap<Customer, CreateCustomerDto>().ReverseMap();
            configuration.CreateMap<CustomerAddressDetail, CreateCustomerDto>().ReverseMap();

            configuration.CreateMap<Customer, CustomerListDto>()
                .ForMember(ldto => ldto.Name, opt => opt.MapFrom(src => src.User.Name))
                .ForMember(ldto => ldto.AcceptReceivePromotionEmail, opt => opt.MapFrom(src => src.User.AcceptReceivePromotionEmail))
                .ForMember(ldto => ldto.EmailAddress, opt => opt.MapFrom(src => src.User.EmailAddress))
                .ForMember(ldto => ldto.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber)).ReverseMap();

            configuration.CreateMap<Customer, CliqueCustomerListDto>()
                .ForMember(ldto => ldto.Name, opt => opt.MapFrom(src => src.User.Name))
                .ForMember(ldto => ldto.AcceptReceivePromotionEmail, opt => opt.MapFrom(src => src.User.AcceptReceivePromotionEmail))
                .ForMember(ldto => ldto.EmailAddress, opt => opt.MapFrom(src => src.User.EmailAddress))
                .ForMember(ldto => ldto.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber)).ReverseMap();

            configuration.CreateMap<TiffinProductOffer, TiffinMemberProductOfferDto>()
                .ForMember(ldto => ldto.Quantity, opt => opt.MapFrom(src => 1));
            // schedule
            configuration.CreateMap<TiffinSchedule, CreateScheduleDto>().ReverseMap();
            configuration.CreateMap<TiffinScheduleDetailProduct, ProductSetProductDto>()
                .ForMember(t => t.ProductId, opt => opt.MapFrom(src => src.ProductId))
                .ForMember(t => t.ProductName, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(t => t.Id, opt => opt.MapFrom(src => src.Product.Id))
                .ReverseMap();

            configuration.CreateMap<Customer, MyInfoDto>()
                .ForMember(t => t.Surname, opt => opt.MapFrom(src => src.User.Surname));

            configuration.CreateMap<UpdateMyAddressInputDto, Customer>();
            configuration.CreateMap<CustomerAddressDetail, UpdateMyAddressInputDto>()
                .ForMember(t => t.CallingCode, opt => opt.MapFrom(src => src.Customer.Country.CallingCode))
                .ForMember(t => t.AddressId, opt => opt.MapFrom(src => src.Id))
                .ForMember(t => t.CityName, opt => opt.MapFrom(src => src.City.Name))
                .ForMember(t => t.CountryId, opt => opt.MapFrom(src => src.Customer.Country.Id))
                .ForMember(t => t.CountryName, opt => opt.MapFrom(src => src.Customer.Country.Name));

            configuration.CreateMap<UpdateMyAddressInputDto, CustomerAddressDetail>();
            configuration.CreateMap<TiffinPayment, TiffinPaymentDetailDto>();

            // FAQ
            configuration.CreateMap<FaqCreateDto, TiffinFaq>();
            configuration.CreateMap<TiffinFaq, FaqEditDto>();
            configuration.CreateMap<FaqEditDto, TiffinFaq>();
            configuration.CreateMap<TiffinFaq, FaqListDto>();

            // Home Banner
            configuration.CreateMap<HomeBannerEditDto, TiffinHomeBanner>();
            configuration.CreateMap<TiffinHomeBanner, HomeBannerEditDto>();

            // Promotion
            configuration.CreateMap<PromotionCreateDto, Tiffins.Promotion.TiffinPromotion>();
            configuration.CreateMap<Tiffins.Promotion.TiffinPromotion, PromotionEditDto>();
            configuration.CreateMap<PromotionEditDto, Tiffins.Promotion.TiffinPromotion>()
                .ForMember(x => x.Codes, o => o.Ignore());

            configuration.CreateMap<Tiffins.Promotion.TiffinPromotion, Tiffins.Promotion.Dtos.PromotionListDto>();

            configuration.CreateMap<Tiffins.Promotion.TiffinPromotionCode, PromotionCodeDto>()
                .ForMember(x => x.CustomerName, o => o.MapFrom(x => x.Customer.Name));

            configuration.CreateMap<PromotionEditDto, PromotionCreateDto>();

           

            // Delivery Time Slot

            configuration.CreateMap<TiffinDeliveryTimeSlot, DeliveryTimeSlotDto>()
                .ForMember(x => x.LocationName, o => o.MapFrom(x => x.Location.Name))
                .ForMember(x => x.MealTimeName, o => o.MapFrom(x => x.MealTime.Name));

            configuration.CreateMap<DeliveryTimeSlotDto, TiffinDeliveryTimeSlot>();

            configuration.CreateMap<ShipmentOrder, ShipmentRoutePointDeliveryDto>()
                .ForMember(x=>x.DepotAddress, o=>o.MapFrom(x=>x.ShipmentDepot.FullAddress))
                ;
            configuration.CreateMap<ShipmentItem, ShipmentItemListDto>().ReverseMap();

            configuration.CreateMap<ShipmentOrder, CreateOrUpdateOrderDto>()
                .ForMember(x=>x.Items, o=>o.MapFrom(x=>x.Items)).ReverseMap();


            configuration.CreateMap<CreateOrUpdateOrderDto, ShipmentOrder>()
                .ForMember(user => user.Items, options => options.Ignore()); ;


            #endregion Tiffins

            #region Wheel

            configuration.CreateMap<CreateOrEditWheelSetupDto, WheelSetup>().ReverseMap();
            configuration.CreateMap<WheelSetupDto, WheelSetup>().ReverseMap();
            //configuration.CreateMap<WheelSetup, GetWheelLocationForViewDto>();
            configuration.CreateMap<CreateOrEditWheelDepartmentDto, WheelDepartment>().ReverseMap();
            configuration.CreateMap<WheelDepartmentDto, WheelDepartment>().ReverseMap();
            configuration.CreateMap<CreateOrEditWheelLocationDto, WheelLocation>().ReverseMap();
            configuration.CreateMap<WheelLocationDto, WheelLocation>().ReverseMap();
            configuration.CreateMap<CreateOrEditWheelTaxDto, WheelTax>().ReverseMap();
            configuration.CreateMap<WheelTaxDto, WheelTax>().ReverseMap();
            configuration.CreateMap<EditWheelPaymentMethodDto, WheelPaymentMethod>().ReverseMap();
            configuration.CreateMap<WheelPaymentMethodDto, WheelPaymentMethod>().ReverseMap();
            configuration.CreateMap<CreateOrEditWheelDeliveryDurationDto, WheelDeliveryDuration>().ReverseMap();
            configuration.CreateMap<WheelDeliveryDurationDto, WheelDeliveryDuration>().ReverseMap();
            configuration.CreateMap<WheelDeliveryZone, WheelDeliveryZoneDto>().ReverseMap();
            configuration.CreateMap<WheelOpeningHour, WheelOpeningHourDto>().ReverseMap();
            configuration.CreateMap<WheelServiceFee, WheelServiceFeeDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditWheelDynamicPagesInput, WheelDynamicPages>();
            configuration.CreateMap<WheelDynamicPages, WheelDynamicPageEditDto>();
            configuration.CreateMap<WheelDynamicPages, WheelDynamicPagesListDto>();
            configuration.CreateMap<WheelTicketShippingOrder, WheelTicketShippingOrderDto>().ReverseMap();

            configuration.CreateMap<PaymentProcessorInfo, GetWheelPaymentMethodForViewDto>();
            configuration.CreateMap<PaymentProcessorInfo, GetWheelPaymentMethodForEditOutput>();
            configuration.CreateMap<EditWheelPaymentMethodDto, PaymentProcessorInfo>();
            configuration.CreateMap<PaymentProcessorInfo, CliquePaymentMethodDto>();

            configuration.CreateMap<WheelDepartment, GetWheelDepartmentForViewDto>()
                .ForMember(ldto => ldto.WheelPaymentMethods, opt => opt.Ignore());

            configuration.CreateMap<WheelDepartment, GetWheelDepartmentOutput>();
            configuration.CreateMap<ScreenMenu, WheelScreenMenusListDto>();
            configuration.CreateMap<PriceTag, PriceTagDto>();

            configuration.CreateMap<WheelLocation, GetWheelLocationForViewDto>();
            configuration.CreateMap<WheelLocation, GetWheelLocationForEditOutput>();
            configuration.CreateMap<WheelLocation, GetWheelLocationOutput>()
                .ForMember(ldto => ldto.City, opt => opt.MapFrom(src => src.Location.City.Name))
                .ForMember(ldto => ldto.Country, opt => opt.MapFrom(src => src.Location.City.Country.Name));

            configuration.CreateMap<WheelOrderDto, WheelTicket>();

            configuration.CreateMap<CreateOrEditWheelTableModel, WheelTable>().ReverseMap();
            configuration.CreateMap<EditWheelTableModel, WheelTable>();
            configuration.CreateMap<WheelTableBaseModel, WheelTable>()
                .ReverseMap();
            configuration.CreateMap<WheelTable, WheelTableViewModel>()
                .ForMember(wheelTableViewModel => wheelTableViewModel.Location, opt => opt.MapFrom(src => src.WheelLocation != null ? src.WheelLocation.Name : ""))
                .ForMember(wheelTableViewModel => wheelTableViewModel.Department, opt => opt.MapFrom(src => src.WheelDepartment != null ? src.WheelDepartment.Name : ""))
                ;

            configuration.CreateMap<TimeSlotDto, WheelDepartmentTimeSlot>().ReverseMap();
            configuration.CreateMap<WheelOrderTrackingDto, WheelOrderTracking>().ReverseMap();

            configuration.CreateMap<ScreenMenuItem, MenuItemAndPriceForSelection>()
                .ForMember(ldto => ldto.Prices, opt => opt.Ignore());
            configuration.CreateMap<WheelItemSoldOut, WheelProductSoldOutListDto>();
            configuration.CreateMap<WheelRecommendationDayPeriod, WheelRecommendationDayPeriodDto>();
            configuration.CreateMap<WheelRecommendationMenuItem, WheelRecommendationItemDto>()
                .ForMember(x => x.MenuName, opt => opt.MapFrom(x => x.MenuItem != null ? x.MenuItem.Name : string.Empty));
            configuration.CreateMap<WheelRecommendationMenu, WheelRecommendationMenuDto>()
                .ForMember(x => x.TriggerApplicable,
                    opt => opt.MapFrom(x =>
                        !string.IsNullOrEmpty(x.TriggerApplicable)
                            ? x.RepeatTime.Split(',', StringSplitOptions.None).ToList()
                            : null));

            #endregion Wheel

            #region DineQueue

            configuration.CreateMap<QueueLocationOptionDto, QueueLocationOption>().ReverseMap();

            configuration.CreateMap<QueueLocationDto, QueueLocation>().ReverseMap();

            configuration.CreateMap<CustomerQueueDto, CustomerQueue>().ReverseMap();

            #endregion DineQueue

            #region Go

            configuration.CreateMap<DineGoDevice, DineGoDeviceListDto>().ReverseMap();

            configuration.CreateMap<DineGoDevice, DineGoDeviceEditDto>()
                .ForMember(ldto => ldto.DineGoBrands, opt => opt.MapFrom(src => src.DineGoBrands.Select(x => x.DineGoBrand)))
                .ForMember(ldto => ldto.DineGoPaymentTypes, opt => opt.MapFrom(src => src.DineGoPaymentTypes.Select(x => x.DineGoPaymentType)))
                ;

            configuration.CreateMap<DineGoBrand, DineGoBrandListDto>()
                .ForMember(ldto => ldto.ScreenMenuName, opt => opt.MapFrom(src => src.ScreenMenu.Name))
                .ForMember(ldto => ldto.LocationName, opt => opt.MapFrom(src => src.Location.Name));

            configuration.CreateMap<DineGoBrand, DineGoBrandEditDto>()
                .ForMember(ldto => ldto.DineGoDepartments, opt => opt.MapFrom(src => src.DineGoDepartments.Select(x => x.DineGoDepartment)))
                ;

            configuration.CreateMap<DineGoDepartment, DineGoDepartmentListDto>()
                .ForMember(ldto => ldto.DepartmentName, opt => opt.MapFrom(src => src.Department.Name));

            configuration.CreateMap<DineGoDepartment, DineGoDepartmentEditDto>()
                .ForMember(ldto => ldto.DineGoCharges, opt => opt.MapFrom(src => src.DineGoCharges.Select(x => x.DineGoCharge)));

            configuration.CreateMap<DineGoPaymentType, DineGoPaymentTypeListDto>()
                .ForMember(ldto => ldto.PaymentTypeName, opt => opt.MapFrom(src => src.PaymentType.Name));

            configuration.CreateMap<DineGoPaymentTypeEditDto, DineGoPaymentType>().ReverseMap();

            configuration.CreateMap<DineGoCharge, DineGoChargeListDto>()
                .ForMember(ldto => ldto.TransactionTypeName, opt => opt.MapFrom(src => src.TransactionType.Name));

            configuration.CreateMap<DineGoChargeEditDto, DineGoCharge>().ReverseMap();

            #endregion Go

            #region Track

            configuration.CreateMap<ShipmentDepot, ShipmentDepotDto>();
            configuration.CreateMap<ShipmentDepot, CreateOrEditShipmentDepotDto>().ReverseMap();

            configuration.CreateMap<ShipmentDriver, TrackShipmentDto>()
                .ForMember(x => x.ShipFrom, o => o.MapFrom(x => x.TrackDepot.FullAddress));
            configuration.CreateMap<ShipmentDriver, CreateOrEditShipmentDriverDto>().ReverseMap();

            configuration.CreateMap<ShipmentRoute, ShipmentRouteDto>()
             .ForMember(x => x.ShipFrom, o => o.MapFrom(x => x.ShipFrom.FullAddress))
             .ForMember(x => x.Driver, o => o.MapFrom(x => x.Driver.Driver))
             ;
            configuration.CreateMap<ShipmentRoute, CreateOrEditShipmentRouteDto>().ReverseMap();

            configuration.CreateMap<ShipmentRoutePoint, ShipmentRoutePointDto>();
            configuration.CreateMap<ShipmentRoutePoint, CreateOrEditShipmentRoutePointDto>().ReverseMap();

            configuration.CreateMap<ShipmentOrder, ShipmentRoutePointOrderListDto>();

            configuration.CreateMap<ShipmentClient, ShipmentClientDto>();
            configuration.CreateMap<ShipmentClient, CreateOrEditShipmentClientDto>().ReverseMap();    
            
            configuration.CreateMap<ShipmentAddress, ShipmentAddressDto>();
            configuration.CreateMap<ShipmentAddress, CreateOrEditShipmentAddressDto>().ReverseMap();
            configuration.CreateMap<ShipmentAnalyticDto, ShipmentOrder>().ReverseMap();

            #endregion

            #region V2

            configuration.CreateMap<AddItemToCartInputDto, WheelCartItem>();

            configuration.CreateMap<WheelCartItem, WheelOrderItem>();
            configuration.CreateMap<ShipmentClientAddress, CreateOrEditShipmentClientAddressDto>().ReverseMap();

            configuration.CreateMap<WheelCartItem, WheelCartItemDto>().ForMember(t=>t.ScreenMenuItem, x=>x.Ignore())
                .ForMember(t=>t.Portion,x=>x.MapFrom(s=> Newtonsoft.Json.JsonConvert.DeserializeObject<WheelCartItemPortionDto>(s.Portion)))
                .ForMember(t => t.Tags, x => x.MapFrom(s => Newtonsoft.Json.JsonConvert.DeserializeObject<List<WheelCartItemOrderTagDto>>(s.Tags)));

            #endregion

            #region Flyer

            // Flyer
            configuration.CreateMap<FlyerCreateDto, Flyer.Flyer>();
            configuration.CreateMap<Flyer.Flyer, FlyerEditDto>();
            configuration.CreateMap<FlyerEditDto, Flyer.Flyer>();
            configuration.CreateMap<Flyer.Flyer, FlyerListDto>();

            #endregion

            #region Cluster
            
            configuration.CreateMap<DelAggCategoryListDto, DelAggCategory>().ReverseMap();
            configuration.CreateMap<DelAggCategoryEditDto, DelAggCategory>().ReverseMap();
            configuration.CreateMap<DelAggChargeListDto, DelAggCharge>().ReverseMap();
            configuration.CreateMap<DelAggChargeEditDto, DelAggCharge>().ReverseMap();
            configuration.CreateMap<DelAggChargeMappingListDto, DelAggChargeMapping>().ReverseMap();
            configuration.CreateMap<DelAggImageListDto, DelAggImage>().ReverseMap();
            configuration.CreateMap<DelAggImageEditDto, DelAggImage>().ReverseMap();
            configuration.CreateMap<DelAggItemListDto, DelAggItem>().ReverseMap();
            configuration.CreateMap<DelAggItemEditDto, DelAggItem>().ReverseMap();
            configuration.CreateMap<DelAggItemGroupListDto, DelAggItemGroup>().ReverseMap();
            configuration.CreateMap<DelAggItemGroupEditDto, DelAggItemGroup>().ReverseMap();
            configuration.CreateMap<DelAggLanguageListDto, DelAggLanguage>().ReverseMap();
            configuration.CreateMap<DelAggLanguageEditDto, DelAggLanguage>().ReverseMap();
            configuration.CreateMap<DelAggLocationGroupListDto, DelAggLocationGroup>().ReverseMap();
            configuration.CreateMap<DelAggLocationGroupEditDto, DelAggLocationGroup>().ReverseMap();
            configuration.CreateMap<DelAggLocationItemListDto, DelAggLocationItem>().ReverseMap();
            configuration.CreateMap<DelAggLocMappingListDto, DelAggLocMapping>().ReverseMap();
            configuration.CreateMap<DelAggLocMappingEditDto, DelAggLocMapping>().ReverseMap();
            configuration.CreateMap<DelAggModifierListDto, DelAggModifier>().ReverseMap();
            configuration.CreateMap<DelAggModifierGroupListDto, DelAggModifierGroup>().ReverseMap();
            configuration.CreateMap<DelAggModifierGroupEditDto, DelAggModifierGroup>().ReverseMap();
            configuration.CreateMap<DelAggModifierGroupItemEditDto, DelAggModifierGroupItem>().ReverseMap();
            configuration.CreateMap<DelAggTaxListDto, DelAggTax>().ReverseMap();
            configuration.CreateMap<DelAggTaxEditDto, DelAggTax>().ReverseMap();
            configuration.CreateMap<DelAggTaxMappingListDto, DelAggTaxMapping>().ReverseMap();
            configuration.CreateMap<DelAggVariantListDto, DelAggVariant>().ReverseMap();
            configuration.CreateMap<DelAggVariantEditDto, DelAggVariant>().ReverseMap();
            configuration.CreateMap<DelAggVariantGroupListDto, DelAggVariantGroup>().ReverseMap();
            configuration.CreateMap<DelAggVariantGroupEditDto, DelAggVariantGroup>().ReverseMap();
            configuration.CreateMap<DelTimingGroupListDto, DelTimingGroup>().ReverseMap();
            configuration.CreateMap<DelTimingGroupEditDto, DelTimingGroup>().ReverseMap();
            configuration.CreateMap<DelTimingDetailEditDto, DelTimingDetail>().ReverseMap();
            
            // Del Agg Location
            configuration.CreateMap<DelAggLocationEditDto, DelAggLocation>();
            configuration.CreateMap<DelAggLocation, DelAggLocationEditDto>()
                .ForMember(t => t.LocationName, opt => opt.MapFrom(src => src.Location.Name))
                .ForMember(t => t.DelAggLocationGroupName, opt => opt.MapFrom(src => src.DelAggLocationGroup.Name))
                ;
            
            configuration.CreateMap<DelAggLocationListDto, DelAggLocation>().ReverseMap();
            
            configuration.CreateMap<DelAggLocationItem, DelAggLocationItemEditDto>()
                .ForMember(t => t.DelAggItemName, opt => opt.MapFrom(src => src.DelAggItem.Name))
                ;
            
            configuration.CreateMap<DelAggLocationItemEditDto, DelAggLocationItem>();
            
            configuration.CreateMap<DelAggLocationItemListDto, DelAggLocationItem>().ReverseMap();
            
            configuration.CreateMap<DelAggModifierListDto, DelAggModifier>().ReverseMap();
            configuration.CreateMap<DelAggModifierEditDto, DelAggModifier>().ReverseMap();

            #endregion
        }
    }
}
﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using DinePlan.DineConnect.EntityFrameworkCore;

namespace DinePlan.DineConnect.HealthChecks
{
    public class DineConnectDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public DineConnectDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("DineConnectDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("DineConnectDbContext could not connect to database"));
        }
    }
}

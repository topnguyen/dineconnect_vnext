﻿using Abp;
using Abp.Dependency;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.UiCustomization.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.UiCustomization
{
    public interface IWheelUiCustomizer : ISingletonDependency
    {
        Task<UiCustomizationSettingsDto> GetUiSettings();

        Task UpdateTenantUiManagementSettingsAsync(int tenantId, ThemeSettingsDto settings);

        Task<ThemeSettingsDto> GetTenantUiCustomizationSettings(int tenantId);
    }
}

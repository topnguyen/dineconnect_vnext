﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace DinePlan.DineConnect.UiCustomization
{
    public interface IUiThemeCustomizerFactory : ISingletonDependency
    {
        Task<IUiCustomizer> GetCurrentUiCustomizer();

        IUiCustomizer GetUiCustomizer(string theme);

        IWheelUiCustomizer GetWheelUiCustomizer(string theme);
    }
}
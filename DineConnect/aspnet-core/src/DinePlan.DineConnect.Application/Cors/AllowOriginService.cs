﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Cors.Dto;
using DinePlan.DineConnect.CORS;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cors
{
    public class AllowOriginService : DineConnectAppServiceBase, IAllowOriginService
    {
        private readonly IRepository<AllowOrigin> _allowOriginRepository;
        private readonly ICorsPolicyAccessor _corsPolicyAccessor;

        public AllowOriginService(IRepository<AllowOrigin> allowOriginRepository,
            ICorsPolicyAccessor corsPolicyAccessor)
        {
            _allowOriginRepository = allowOriginRepository;
            _corsPolicyAccessor = corsPolicyAccessor;
        }

        public async Task CreateOrEdit(CreateOrEditAllowOriginInput input)
        {
            var data = ObjectMapper.Map<AllowOrigin>(input);

            var entity = await _allowOriginRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (entity == null)
            {
                await _allowOriginRepository.InsertAsync(data);

                _corsPolicyAccessor.GetPolicy(AppConsts.CorsPublicName).Origins.Add(data.Origin.RemovePostFix("/"));
            }
            else
            {
                if (data.Origin != entity.Origin)
                {
                    _corsPolicyAccessor.GetPolicy(AppConsts.CorsPublicName).Origins.Add(data.Origin.RemovePostFix("/"));
                    _corsPolicyAccessor.GetPolicy(AppConsts.CorsPublicName).Origins.Remove(entity.Origin);
                }

                entity.Origin = data.Origin;

                await _allowOriginRepository.UpdateAsync(entity);
            }    
        }

        public async Task Delete(EntityDto input)
        {
            var entity = await _allowOriginRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

            await _allowOriginRepository.DeleteAsync(input.Id);

            _corsPolicyAccessor.GetPolicy(AppConsts.CorsPublicName).Origins.Remove(entity.Origin);
        }

        public async Task<PagedResultDto<GetAllowOriginForViewDto>> GetAll(GetAllowOriginsInput input)
        {
            var query = _allowOriginRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => x.Origin.Contains(input.Filter));

            if (!string.IsNullOrWhiteSpace(input.Sorting))
                query = query.OrderBy(input.Sorting);

            var allowOriginCount = await query.CountAsync();
            var allowOrigins = await query.PageBy(input).ToListAsync();

            return new PagedResultDto<GetAllowOriginForViewDto>
            {
                Items = ObjectMapper.Map<List<GetAllowOriginForViewDto>>(allowOrigins),
                TotalCount = allowOriginCount
            };
        }

        public async Task<GetAllowOriginForEditDto> GetAllowOriginForEdit(EntityDto input)
        {
            var data = await _allowOriginRepository.GetAsync(input.Id);

            return ObjectMapper.Map<GetAllowOriginForEditDto>(data);
        }
    }
}

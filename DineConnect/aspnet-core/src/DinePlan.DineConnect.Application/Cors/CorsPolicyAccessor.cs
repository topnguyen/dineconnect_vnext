﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Cors
{
    public class CorsPolicyAccessor : ICorsPolicyAccessor
    {
        private readonly CorsOptions _corsOptions;

        public CorsPolicyAccessor(IOptions<CorsOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            _corsOptions = options.Value;
        }

        public CorsPolicy GetPolicy()
        {
            return _corsOptions.GetPolicy(_corsOptions.DefaultPolicyName);
        }

        public CorsPolicy GetPolicy(string name)
        {
            return _corsOptions.GetPolicy(name);
        }
    }
}

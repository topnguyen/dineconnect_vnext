﻿using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.BaseCore.TemplateEngines;
using DinePlan.DineConnect.Net.Whatapp;
using FluentEmail.Liquid;
using Microsoft.Extensions.Options;

namespace DinePlan.DineConnect.Whatapp
{
    public class TenantWhatappSendAppService : DineConnectAppServiceBase, ITenantWhatappSendAppService
    {
        private readonly IWhatappSender _whatappSender;
        private readonly IRepository<TemplateEngine> _templateEngineRepository;

        public TenantWhatappSendAppService(
            IWhatappSender whatappSender,
            IRepository<TemplateEngine> templateEngineRepository)
        {
            _whatappSender = whatappSender;
            _templateEngineRepository = templateEngineRepository;
        }

        public async Task SendWhatappAsync(string toPhone, string message)
        {
            await _whatappSender.SendAsync(toPhone, message);
        }

        public async Task SendWhatappByTemplateAsync(string toPhone, string module, int? tenantId, object variables)
        {
            // Template
            var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(e => e.Module == "Register" && e.TenantId == tenantId);
            
            // Render Content
            FluentEmail.Core.Email.DefaultRenderer = new LiquidRenderer(Options.Create(new LiquidRendererOptions()));
            var whatappMessage = await FluentEmail.Core.Email.DefaultRenderer.ParseAsync(templateEngine.ShortTemplate, variables);

            if(!string.IsNullOrEmpty(toPhone))
                await SendWhatappAsync(toPhone, whatappMessage);
        }
    }
}

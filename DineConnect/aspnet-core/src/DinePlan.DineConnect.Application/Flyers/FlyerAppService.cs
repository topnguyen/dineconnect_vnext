using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Flyers.Flyers;
using DinePlan.DineConnect.Flyers.Flyers.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Flyers
{
    
    public class FlyerAppService : DineConnectAppServiceBase, IFlyerAppService
    {
        private readonly IRepository<Flyer.Flyer> _flyerRepository;

        public FlyerAppService(IRepository<Flyer.Flyer> flyerRepository)
        {
            _flyerRepository = flyerRepository;
        }

        [AbpAllowAnonymous]
        public async Task<ListResultDto<FlyerListDto>> GetFlyers()
        {
            var flyers = await _flyerRepository
                .GetAll()
                .Select(x => ObjectMapper.Map<FlyerListDto>(x))
                .ToListAsync();

            return new ListResultDto<FlyerListDto>(flyers);
        }

        public async Task<FlyerEditDto> GetFlyerForEdit(EntityDto input)
        {
            var flyer = await _flyerRepository.GetAsync(input.Id);

            return ObjectMapper.Map<FlyerEditDto>(flyer);
        }

        
        public async Task CreateFlyer(FlyerCreateDto input)
        {
            var flyer = ObjectMapper.Map<Flyer.Flyer>(input);

            await _flyerRepository.InsertAsync(flyer);
        }

        
        public async Task UpdateFlyer(FlyerEditDto input)
        {
            var flyer = ObjectMapper.Map<Flyer.Flyer>(input);

            if (AbpSession.TenantId != null)
            {
                flyer.TenantId = AbpSession.TenantId.Value;
            }

            await _flyerRepository.UpdateAsync(flyer);
        }

        
        public async Task DeleteFlyer(EntityDto input)
        {
            await _flyerRepository.DeleteAsync(input.Id);
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Exporting
{
    public interface ISwipeCardTypeExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<SwipeCardTypeDto> swipeCardTypes);
    }
}

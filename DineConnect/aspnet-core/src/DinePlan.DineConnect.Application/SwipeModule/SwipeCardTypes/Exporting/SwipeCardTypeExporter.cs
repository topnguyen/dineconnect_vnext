﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Exporting
{
    public class SwipeCardTypeExporter : NpoiExcelExporterBase, ISwipeCardTypeExporter
    {
        public SwipeCardTypeExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<SwipeCardTypeDto> swipeCardTypes)
        {
            var excel = CreateExcelPackage(
                "SwipeCardTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SwipeCardTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DepositRequired"),
                        L("DepositAmount"),
                        L("RefundAllowed"),
                        L("ExpiryDaysFromIssue"),
                        L("MinimumTopUpAmount")
                    );

                    AddObjects(
                        sheet, 2, swipeCardTypes,
                        _ => _.Name,
                        _ => _.DepositRequired,
                        _ => _.DepositAmount,
                        _ => _.RefundAllowed,
                        _ => _.ExpiryDaysFromIssue,
                        _ => _.MinimumTopUpAmount
                    );

                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

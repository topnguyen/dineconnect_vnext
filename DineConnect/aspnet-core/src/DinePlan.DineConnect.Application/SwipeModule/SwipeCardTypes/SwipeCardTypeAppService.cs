﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes
{
    public class SwipeCardTypeAppService : DineConnectAppServiceBase, ISwipeCardTypeAppService
    {
        private readonly IRepository<Core.Swipe.SwipeCardType> _swipeCardRepo;
        private readonly ISwipeCardTypeExporter _swipeCardTypeExporter;

        public SwipeCardTypeAppService(IRepository<Core.Swipe.SwipeCardType> swipeCardRepo, ISwipeCardTypeExporter swipeCardTypeExporter)
        {
            _swipeCardRepo = swipeCardRepo;
            _swipeCardTypeExporter = swipeCardTypeExporter;
        }

        public async Task<PagedResultDto<SwipeCardTypeDto>> GetAll(GetSwipeCardTypeInputDto typeInput)
        {
            var query = _swipeCardRepo.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(typeInput.Name), x => x.Name.Contains(typeInput.Name));

            var totalCount = await query.CountAsync();

            var swipeCards = await query
                .OrderBy(typeInput.Sorting)
                .PageBy(typeInput)
                .ToListAsync();

            var swipeCardsDto = ObjectMapper.Map<List<SwipeCardTypeDto>>(swipeCards);

            return new PagedResultDto<SwipeCardTypeDto>(totalCount, swipeCardsDto);
        }

        public Task<SwipeCardTypeDto> Get(EntityDto entityDto)
        {
            var entity = _swipeCardRepo.Get(entityDto.Id);

            var dto = ObjectMapper.Map<SwipeCardTypeDto>(entity);

            return Task.FromResult(dto);
        }

        public async Task<FileDto> GetToExcel(GetSwipeCardTypeInputDto typeInput)
        {
            var data = await GetAll(typeInput);

            return await _swipeCardTypeExporter.ExportToFile(data.Items.ToList());
        }

        public async Task<int> AddOrUpdate(SwipeCardTypeDto dto)
        {
            var entity = ObjectMapper.Map<Core.Swipe.SwipeCardType>(dto);

            entity.TenantId = AbpSession.TenantId ?? 0;

            var id = await _swipeCardRepo.InsertOrUpdateAndGetIdAsync(entity);

            return id;
        }

        public async Task Delete(EntityDto entityDto)
        {
            await _swipeCardRepo.DeleteAsync(entityDto.Id);
        }
    }
}

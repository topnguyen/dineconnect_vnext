﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect.SwipeModule
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(
        typeof(DineConnectApplicationSharedModule),
        typeof(DineConnectCoreModule)
    )]
    public class SwipeDineConnectApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding custom AutoMapper configuration
            Configuration.Modules.AbpAutoMapper().Configurators.Add(SwipeCustomDtoMapper.CreateMappings);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SwipeDineConnectApplicationModule).GetAssembly());
        }
    }
}

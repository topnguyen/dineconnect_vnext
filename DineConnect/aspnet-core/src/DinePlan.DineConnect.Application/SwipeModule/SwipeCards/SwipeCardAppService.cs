﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Json;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Core.Swipe;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Exporting;
using DinePlan.DineConnect.Tiffins.EmailNotification;
using Elect.Core.StringUtils;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards
{
    public class SwipeCardAppService : DineConnectAppServiceBase, ISwipeCardAppService
    {
        private readonly IRepository<Core.Swipe.SwipeCard> _swipeCardRepo;
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepo;
        private readonly IRepository<SwipeCardType> _swipeCardTypeRepo;
        private readonly IRepository<SwipeTransactionDetail> _swipeTransactionRepository;
        private readonly IRepository<SwipeCardOTP> _swipeCardOtpRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly ITenantEmailSendAppService _emailNotificationAppService;
        private readonly ISwipeCardExporter _swipeCardExporter;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        private readonly IAdyenPaymentProcessor _adyenPaymentProcessor;
        private readonly IStripePaymentProcessor _stripePaymentProcessor;
        private readonly IPaypalPaymentProcessor _paypalPaymentProcessor;
        private readonly IGooglePaymentProcessor _googlePaymentProcessor;

        public SwipeCardAppService(
            IRepository<Core.Swipe.SwipeCard> swipeCardRepo,
            IRepository<Core.Swipe.SwipeCustomerCard> swipeCustomerCardRepo,
            IRepository<Core.Swipe.SwipeCardType> swipeCardTypeRepo,
            IRepository<Core.Swipe.SwipeTransactionDetail> swipeTransactionRepository,
            IRepository<Core.Swipe.SwipeCardOTP> swipeCardOtpRepository,
            IRepository<Customer> customerRepository,
            ITenantEmailSendAppService emailNotificationAppService,
            ISwipeCardExporter swipeCardExporter,
            IOmisePaymentProcessor omisePaymentProcessor,
            IAdyenPaymentProcessor adyenPaymentProcessor,
            IStripePaymentProcessor stripePaymentProcessor,
            IPaypalPaymentProcessor paypalPaymentProcessor,
            IGooglePaymentProcessor googlePaymentProcessor)
        {
            _swipeCardRepo = swipeCardRepo;
            _swipeCustomerCardRepo = swipeCustomerCardRepo;
            _swipeCardTypeRepo = swipeCardTypeRepo;
            _swipeTransactionRepository = swipeTransactionRepository;
            _swipeCardOtpRepository = swipeCardOtpRepository;
            _customerRepository = customerRepository;
            _emailNotificationAppService = emailNotificationAppService;
            _swipeCardExporter = swipeCardExporter;
            _omisePaymentProcessor = omisePaymentProcessor;
            _adyenPaymentProcessor = adyenPaymentProcessor;
            _stripePaymentProcessor = stripePaymentProcessor;
            _paypalPaymentProcessor = paypalPaymentProcessor;
            _googlePaymentProcessor = googlePaymentProcessor;
        }

        public async Task<PagedResultDto<SwipeCardDto>> GetAll(GetSwipeCardInputDto typeInput)
        {
            var query = _swipeCardRepo.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(typeInput.CardNumber), x => x.CardNumber.Contains(typeInput.CardNumber))
                .WhereIf(typeInput.SwipeCardTypeId != null, x => x.CardTypeRefId == typeInput.SwipeCardTypeId);

            var totalCount = query.Count();

            var swipeCards = await query
                .OrderBy(typeInput.Sorting)
                .PageBy(typeInput)
                .ToListAsync();

            var swipeCardsDto = ObjectMapper.Map<List<SwipeCardDto>>(swipeCards);

            return new PagedResultDto<SwipeCardDto>(totalCount, swipeCardsDto);
        }

        public Task<SwipeCardDto> Get(EntityDto entityDto)
        {
            var entity = _swipeCardRepo.Get(entityDto.Id);

            var dto = ObjectMapper.Map<SwipeCardDto>(entity);

            if (dto != null)
            {
                var customer = _swipeCustomerCardRepo.GetAll().Where(x => x.CardRefId == entity.Id).Select(x => x.Customer).FirstOrDefault();
                dto.CustomerId = customer?.Id;
                dto.CustomerName = customer?.Name;
            }

            return Task.FromResult(dto);
        }

        public async Task<FileDto> GetToExcel(GetSwipeCardInputDto typeInput)
        {
            var data = await GetAll(typeInput);

            return await _swipeCardExporter.ExportToFile(data.Items.ToList());
        }

        public async Task<int> AddOrUpdate(SwipeCardDto dto)
        {
            var entity = ObjectMapper.Map<Core.Swipe.SwipeCard>(dto);

            entity.TenantId = AbpSession.TenantId.Value;

            var isExist = _swipeCardRepo.GetAll().Any(x => x.CardNumber == dto.CardNumber && x.Id != dto.Id);

            if (isExist)
            {
                throw new UserFriendlyException(L("SwipeCardAlreadyExists", dto.CardNumber));
            }

            entity.TenantId = AbpSession.TenantId ?? 0;

            var id = await _swipeCardRepo.InsertOrUpdateAndGetIdAsync(entity);

            return id;
        }

        public async Task Delete(EntityDto entityDto)
        {
            await _swipeCardRepo.DeleteAsync(entityDto.Id);
        }

        public Task<SwipeCardDto> CheckCard(string cardNumber)
        {
            var entity = _swipeCardRepo.GetAll().FirstOrDefault(x => x.CardNumber == cardNumber);

            var dto = ObjectMapper.Map<SwipeCardDto>(entity);

            if (dto != null)
            {
                dto.CustomerId = _swipeCustomerCardRepo.GetAll().Where(x => x.CardRefId == entity.Id).Select(x => x.CustomerId).FirstOrDefault();
            }
            else
            {
                throw new UserFriendlyException(L("SwipeCardNotFound", cardNumber));
            }

            if (!dto.Active)
            {
                throw new UserFriendlyException(L("CardIsNotInActiveStatus", cardNumber));
            }

            return Task.FromResult(dto);
        }

        public async Task<string> GenerateCardOTP(string cardNumber)
        {
            var card = await CheckCard(cardNumber);

            var allExistingOTPNotExpire = _swipeCardOtpRepository.GetAll().Where(x => x.ExpiredAt >= Clock.Now).ToList();

            foreach (var swipeCardOtp in allExistingOTPNotExpire)
            {
                swipeCardOtp.ExpiredAt = Clock.Now.AddMinutes(-1);

                await _swipeCardOtpRepository.UpdateAsync(swipeCardOtp);
            }

            var otp = new SwipeCardOTP
            {
                OTP = StringHelper.Generate(6, false, false),
                ExpiredAt = Clock.Now.AddHours(2),
                SwipeCardId = card.Id
            };

            await _swipeCardOtpRepository.InsertAndGetIdAsync(otp);

            // Send Email to Customer

            var customer = _swipeCustomerCardRepo.GetAll().Where(x => x.CardRefId == card.Id).Select(x => x.Customer).FirstOrDefault();

            await _emailNotificationAppService.SendAsyc(customer.EmailAddress, "Swipe Card OTP", $"Your OTP is {otp.OTP}", false, customer.TenantId);

            return otp.OTP;
        }

        public async Task ValidateCardOTP(string cardNumber, string OTP)
        {
            var card = await CheckCard(cardNumber);

            var cardOtp = _swipeCardOtpRepository.GetAll().FirstOrDefault(x => x.SwipeCardId == card.Id && x.OTP == OTP);

            if (cardOtp == null)
            {
                throw new UserFriendlyException("Invalid OTP");
            }

            if (cardOtp.ExpiredAt < Clock.Now)
            {
                throw new UserFriendlyException("Expired OTP");
            }
        }

        public async Task<SwipeCardTransactionDto> AddTransaction(AddTransactionDto transaction)
        {
            var card = await CheckCard(transaction.CardNumber);

            var cardType = _swipeCardTypeRepo.GetAll().FirstOrDefault(x => x.Id == card.CardTypeRefId);

            // Validation
            if (transaction.Amount >= 0)
            {
                if (cardType.MinimumTopUpAmount > transaction.Amount)
                {
                    throw new UserFriendlyException($"Minimum topup amount is {cardType.MinimumTopUpAmount}");
                }
            }
            else
            {
                if (!cardType.RefundAllowed)
                {
                    throw new UserFriendlyException($"The card does not allow to refund");
                }

                if (Math.Abs(transaction.Amount) > card.Balance)
                {
                    throw new UserFriendlyException($"The card balance does not enough");
                }
            }

            // Adjust Balance Card
            var cardEntity = await _swipeCardRepo.GetAsync(card.Id);
            cardEntity.Balance += transaction.Amount;
            await _swipeCardRepo.UpdateAsync(cardEntity);

            int? customerId = transaction.CustomerId;

            if (AbpSession.UserId != null && customerId == null)
            {
                customerId = _customerRepository.GetAll().Where(x => x.UserId == AbpSession.UserId).Select(x => x.Id)
                    .FirstOrDefault();
            }

            // Add Card Transaction
            var transactionEntity = new SwipeTransactionDetail
            {
                CardRefId = card.Id,
                CustomerId = customerId,
                TransactionTime = Clock.Now,
                PaymentType = transaction.PaymentType,
                PaymentTransactionDetails = transaction.PaymentTransactionDetails,
                TenantId = AbpSession.TenantId.Value
            };

            if (transaction.Amount >= 0)
            {
                transactionEntity.TransactionType = (int)SwipeTransaction.Topup;
                transactionEntity.Credit = transaction.Amount;
            }
            else
            {
                transactionEntity.TransactionType = (int)SwipeTransaction.Refund;
                transactionEntity.Debit = Math.Abs(transaction.Amount);
            }

            // Customer Card
            if (card.CustomerId == null && transaction.CustomerId != null)
            {
                if (cardType.DepositRequired)
                {
                    if (cardType.DepositAmount > transaction.Amount)
                    {
                        throw new UserFriendlyException("Deposit amount does not enough");
                    }
                }

                transactionEntity.TransactionType = (int)SwipeTransaction.Deposit;

                // Add Card to the Customer
                var customerCard = new SwipeCustomerCard
                {
                    Active = true,
                    CardRefId = card.Id,
                    CustomerId = transaction.CustomerId,
                    IssueDate = Clock.Now,
                    DepositAmount = transaction.Amount
                };

                if (cardType.ExpiryDaysFromIssue != null)
                {
                    customerCard.ExpiryDate = Clock.Now.AddDays(cardType.ExpiryDaysFromIssue.Value);
                }

                await _swipeCustomerCardRepo.InsertAndGetIdAsync(customerCard);
            }

            // Add Tranasction to database

            transactionEntity.Id = await _swipeTransactionRepository.InsertAndGetIdAsync(transactionEntity);

            await UnitOfWorkManager.Current.SaveChangesAsync();

            var transactionInDb = _swipeTransactionRepository.GetAll().Where(x => x.Id == transactionEntity.Id)
                .Include(x => x.Customer)
                .Include(x => x.SwipeCard)
                .FirstOrDefault();

            var transactionResult = ObjectMapper.Map<SwipeCardTransactionDto>(transactionInDb);

            return transactionResult;
        }

        public async Task<SwipeCardTransactionDto> TopUp(SwipeCardTopUpDto input)
        {
            string outputMessage;
            bool isSuccess = false;
            string paymentGatewayResponseData = string.Empty;

            // Validation

            await CheckCard(input.CardNumber);

            // Omise

            if (input.PaymentOption == "omise" || input.PaymentOption == "paynow")
            {
                OmisePaymentResponseDto omiseChargeResult;

                if (input.PaymentOption == "omise")
                {
                    omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                    {
                        OmiseToken = input.OmiseToken,
                        Amount = Convert.ToInt64(input.PaidAmount * 100),
                        Currency = DineConnectConsts.Currency,
                    });
                }
                else
                {
                    omiseChargeResult = await _omisePaymentProcessor.GetStatus(input.OmiseToken);
                }

                if (omiseChargeResult != null)
                {
                    outputMessage = omiseChargeResult.Message;
                    isSuccess = omiseChargeResult.IsSuccess;
                    paymentGatewayResponseData = omiseChargeResult.ToJsonString();
                }
            }

            // Adyen

            if (input.PaymentOption == "adyen")
            {
                var adyenChargeResult = await _adyenPaymentProcessor.ProcessPayment(new AdyenPaymentRequestDto
                {
                    EncryptedCardNumber = input.AdyenEncryptedCardNumber,
                    EncryptedExpiryMonth = input.AdyenEncryptedExpiryMonth,
                    EncryptedExpiryYear = input.AdyenEncryptedExpiryYear,
                    EncryptedSecurityCode = input.AdyenEncryptedSecurityCode,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency,
                });

                if (adyenChargeResult != null)
                {
                    outputMessage = adyenChargeResult.Message;
                    isSuccess = adyenChargeResult.IsSuccess;
                    paymentGatewayResponseData = adyenChargeResult.ToJsonString();
                }
            }

            // Stripe

            if (input.PaymentOption == "stripe")
            {
                var stripeChargeResult = await _stripePaymentProcessor.ProcessPayment(new StripePaymentRequestDto
                {
                    StripeToken = input.StripeToken,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency
                });

                if (stripeChargeResult != null)
                {
                    outputMessage = stripeChargeResult.Message;
                    isSuccess = stripeChargeResult.IsSuccess;
                    paymentGatewayResponseData = stripeChargeResult.ToJsonString();
                }
            }

            // Paypal

            if (input.PaymentOption == "paypal")
            {
                var paypalChargeResult = await _paypalPaymentProcessor.ProcessPayment(new PaypalPaymentRequestDto
                {
                    OrderId = input.PaypalOrderId
                });

                if (paypalChargeResult != null)
                {

                    outputMessage = paypalChargeResult.Message;
                    isSuccess = paypalChargeResult.IsSuccess;
                    paymentGatewayResponseData = paypalChargeResult.ToJsonString();
                }
            }

            // Google

            if (input.PaymentOption == "google")
            {
                var googleChargeResult = await _googlePaymentProcessor.ProcessPayment(new GooglePaymentRequestDto
                {
                    GooglePayToken = input.GooglePayToken,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency
                });

                if (googleChargeResult != null)
                {
                    outputMessage = googleChargeResult.Message;
                    isSuccess = googleChargeResult.IsSuccess;
                    paymentGatewayResponseData = googleChargeResult.ToJsonString();
                }
            }

            if (isSuccess)
            {
                // TOPUP
                var transaction = new AddTransactionDto
                {
                    Amount = input.PaidAmount,
                    PaymentTransactionDetails = paymentGatewayResponseData,
                    CardNumber = input.CardNumber,
                    PaymentType = input.PaymentOption
                };

                var transactionResult = await AddTransaction(transaction);

                return transactionResult;
            }

            throw new UserFriendlyException("Topup fail, please check your payment information and try again!");
        }

        public async Task<PagedResultDto<CustomerCardDto>> GetAllCustomerCard(GetCustomerCardInputDto input)
        {
            var query = _swipeCustomerCardRepo.GetAll()
                .Include(x => x.Customer)
                .Include(x => x.SwipeCard)
                .WhereIf(input.CustomerId != null, x => x.CustomerId == input.CustomerId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.CardNumber), x => x.SwipeCard.CardNumber.Contains(input.CardNumber))
                .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerName), x => x.Customer.Name.Contains(input.CustomerName))
                .Include(x => x.SwipeCard);

            var totalCount = query.ToList().Count;

            var swipeCustomerCards = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var swipeCustomerCardDtos = ObjectMapper.Map<List<CustomerCardDto>>(swipeCustomerCards);

            return new PagedResultDto<CustomerCardDto>(totalCount, swipeCustomerCardDtos);
        }

        public async Task<PagedResultDto<SwipeCardTransactionDto>> GetAllTransaction(GetCardTransactionInputDto input)
        {
            var query = _swipeTransactionRepository.GetAll().Where(x => x.SwipeCard.Id == input.SwipeCardId).Include(x => x.Customer).Include(x => x.SwipeCard);

            var totalCount = query.Count();

            var swipeCardTransaction = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var swipeCardTransactionDtos = ObjectMapper.Map<List<SwipeCardTransactionDto>>(swipeCardTransaction);

            return new PagedResultDto<SwipeCardTransactionDto>(totalCount, swipeCardTransactionDtos);
        }
    }
}

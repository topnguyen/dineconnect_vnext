﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Exporting
{
    public interface ISwipeCardExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<SwipeCardDto> swipeCards);
    }
}

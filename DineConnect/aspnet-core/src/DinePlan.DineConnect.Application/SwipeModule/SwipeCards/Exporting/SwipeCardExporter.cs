﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Exporting
{
    public class SwipeCardExporter : NpoiExcelExporterBase, ISwipeCardExporter
    {
        public SwipeCardExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<SwipeCardDto> swipeCards)
        {
            var excel = CreateExcelPackage(
                "SwipeCards.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SwipeCards"));

                    AddHeader(
                        sheet,
                        L("CardNumber"),
                        L("Active"),
                        L("Balance")
                    );

                    AddObjects(
                        sheet, 2, swipeCards,
                        _ => _.CardNumber,
                        _ => _.Active,
                        _ => _.Balance
                    );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

﻿using AutoMapper;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos;

namespace DinePlan.DineConnect.SwipeModule
{
    internal static class SwipeCustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Core.Swipe.SwipeCardType, SwipeCardTypeDto>().ReverseMap();
            configuration.CreateMap<Core.Swipe.SwipeCard, SwipeCardDto>().ReverseMap();
            configuration.CreateMap<Core.Swipe.SwipeCustomerCard, CustomerCardDto>()
                .ForMember(x => x.CustomerName, o => o.MapFrom(s => s.Customer.Name))
                .ForMember(x => x.CardNumber, o => o.MapFrom(s => s.SwipeCard.CardNumber))
                .ForMember(x => x.CardId, o => o.MapFrom(s => s.SwipeCard.Id))
                .ForMember(x => x.Balance, o => o.MapFrom(s => s.SwipeCard.Balance))
                .ReverseMap();
            configuration.CreateMap<Core.Swipe.SwipeTransactionDetail, SwipeCardTransactionDto>()
                .ForMember(x => x.SwipeCardNumber, o => o.MapFrom(s => s.SwipeCard.CardNumber))
                .ForMember(x => x.SwipeCardId, o => o.MapFrom(s => s.SwipeCard.Id))
                .ForMember(x => x.CustomerName, o => o.MapFrom(s => s.Customer.Name))
                .ReverseMap();
        }
    }
}

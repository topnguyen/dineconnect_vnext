﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Net.Mail;
using Abp.UI;
using DinePlan.DineConnect.BaseCore.TemplateEngines;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using FluentEmail.Liquid;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace DinePlan.DineConnect.Email
{
    public class TenantEmailSendAppService : DineConnectAppServiceBase, ITenantEmailSendAppService
    {
        private readonly IRepository<TemplateEngine> _templateEngineRepository;
        private readonly ISettingManager _settingManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TenantEmailSendAppService(IRepository<TemplateEngine> templateEngineRepository,
            ISettingManager settingManager, IUnitOfWorkManager unitOfWorkManager)
        {
            _templateEngineRepository = templateEngineRepository;
            _settingManager = settingManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public void Send(string toEmail, string subject, string htmlContent, bool html, FileAttach fileAttachment, int? inputTenantId)
        {
            try
            {
                if (string.IsNullOrEmpty(htmlContent)) throw new UserFriendlyException("No Content");

                var tenantId = AbpSession.TenantId ?? inputTenantId;

                string apiKey = GetSendGridApiKey(tenantId).Result;

                var message = BuildMailMessage(toEmail, subject, htmlContent, html, fileAttachment, tenantId);

                var client = new SendGridClient(apiKey);

                client.SendEmailAsync(message);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
        public async Task SendAsyc(string toEmail, string subject, string htmlContent, bool html, int? inputTenantId)
        {
            try
            {
                if (string.IsNullOrEmpty(htmlContent)) throw new UserFriendlyException("No Content");

                var tenantId = AbpSession.TenantId;

                if (tenantId == null)
                    tenantId = inputTenantId;

                string apiKey = await GetSendGridApiKey(tenantId);

                var message = BuildMailMessage(toEmail, subject, htmlContent, html, null, inputTenantId);

                var client = new SendGridClient(apiKey);

                await client.SendEmailAsync(message);
            }
            catch (Exception e)
            {
                Logger.Error("Send Email : " + e);
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task SendEmailWithAttachmentAsync(string toEmail, string subject, string htmlContent, bool html,
            FileAttach fileAttachment, int? inputTenantId)
        {
            try
            {
                if (string.IsNullOrEmpty(htmlContent)) throw new UserFriendlyException("No Content");

                var tenantId = AbpSession.TenantId;

                if (tenantId == null)
                    tenantId = inputTenantId;

                string apiKey = await GetSendGridApiKey(tenantId);

                var message = BuildMailMessage(toEmail, subject, htmlContent, html, fileAttachment, inputTenantId);

                var client = new SendGridClient(apiKey);

                await client.SendEmailAsync(message);
            }
            catch (Exception e)
            {
                Logger.Error("Send Email : " + e);
                throw new UserFriendlyException(e.Message);
            }
        }

        private SendGridMessage BuildMailMessage(string toEmail, string subject, string htmlContent, in bool html,
            FileAttach fileAttachment, int? inputTenantId)
        {
            if (string.IsNullOrEmpty(htmlContent)) return null;

            var tenantId = AbpSession.TenantId ?? inputTenantId;

            var message = new SendGridMessage()
            {
                HtmlContent = htmlContent,
                Subject = subject,
            };

            if (tenantId != null)
            {
                message.From = new EmailAddress(
                        _settingManager.GetSettingValueForTenant(EmailSettingNames.DefaultFromAddress, tenantId.Value),
                        _settingManager.GetSettingValueForTenant(EmailSettingNames.DefaultFromDisplayName, tenantId.Value));
            }
            else
            {
                message.From = new EmailAddress(
                          _settingManager.GetSettingValue(EmailSettingNames.DefaultFromAddress),
                          _settingManager.GetSettingValue(EmailSettingNames.DefaultFromDisplayName));
            }

            toEmail.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(t =>
            {
                message.AddTo(new EmailAddress { Email = t });
            });

            if (fileAttachment?.FileBytes != null)
            {
                message.Attachments = new System.Collections.Generic.List<Attachment>();
                message.Attachments.Add(new SendGrid.Helpers.Mail.Attachment()
                {
                    Content = Convert.ToBase64String(fileAttachment?.FileBytes),
                    Filename = fileAttachment.FileName,
                    Type = fileAttachment.FileMime
                });
            }

            return message;
        }

        public async Task SendEmailByTemplateAsync(SendEmailInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input?.TenantId))
            {
                var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(e =>
                    e.Module.Equals(input.Module) && e.TenantId == input.TenantId);

                if (templateEngine == null || templateEngine.Template == null)
                {
                    throw new UserFriendlyException("Template is not available");
                }

                // Generate Template by liquid via Email Fluent
                FluentEmail.Core.Email.DefaultRenderer = new LiquidRenderer(Options.Create(new LiquidRendererOptions()));
                var emailContent = await FluentEmail.Core.Email.DefaultRenderer.ParseAsync(templateEngine.Template, input.Variables);
                emailContent = HttpUtility.HtmlDecode(emailContent);
                
                try
                {
                    await SendEmailWithAttachmentAsync(input.EmailAddress, templateEngine.Subject, emailContent, true, null, input.TenantId);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.StackTrace);
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        //[UnitOfWork]
        public async Task SendEmailByTemplateInBackgroundAsync(SendEmailInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(e =>
                    e.Module.Equals(input.Module) && e.TenantId == input.TenantId);
                if (templateEngine == null)
                {
                    throw new UserFriendlyException("No Template");
                }
                
                // Generate Template by liquid via Email Fluent
                FluentEmail.Core.Email.DefaultRenderer = new LiquidRenderer(Options.Create(new LiquidRendererOptions()));
                var emailContent = await FluentEmail.Core.Email.DefaultRenderer.ParseAsync(templateEngine.Template, input.Variables);
                emailContent = HttpUtility.HtmlDecode(emailContent);

                try
                {
                    await SendEmailWithAttachmentAsync(input.EmailAddress, templateEngine.Subject, emailContent, true, input.FileAttach,
                        input.TenantId);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.StackTrace);

                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        private async Task<string> GetSendGridApiKey(int? tenantId)
        {
            string apiKey = string.Empty;
            if (tenantId != null)
            {
                apiKey = _settingManager.GetSettingValueForTenant(AppSettings.Email.SendGridApiKey, tenantId.Value);
            }

            if (string.IsNullOrWhiteSpace(apiKey))
            {
                apiKey = _settingManager.GetSettingValueForApplication(AppSettings.Email.SendGridApiKey);
            }

            if (tenantId == null || string.IsNullOrWhiteSpace(apiKey))
            {
                apiKey = await _settingManager.GetSettingValueAsync(AppSettings.Email.SendGridApiKey);
            }

            if (string.IsNullOrEmpty(apiKey))
                throw new UserFriendlyException("SendGridApiKeyNotFound");

            return apiKey;
        }

        public async Task<string> GetEmailTemplateContentAsync(int? tenantId, string module)
        {
            using (_unitOfWorkManager.Current.SetTenantId(tenantId))
            {
                var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(e =>
                    e.Module.Equals(module) && e.TenantId == tenantId);
                if (templateEngine == null)
                    throw new UserFriendlyException("No Template");

                return templateEngine.Template;
            }
        }
    }
}
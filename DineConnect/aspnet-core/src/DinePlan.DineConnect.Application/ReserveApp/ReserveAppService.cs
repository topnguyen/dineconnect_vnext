﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Reserve;
using DinePlan.DineConnect.ReserveAppConfig;
using static DinePlan.DineConnect.Reserve.ReserveDto;

namespace DinePlan.DineConnect.ReserveApp
{
    public class ReserveAppService : DineConnectAppServiceBase, IReserveAppService
    {
        private readonly IRepository<Reservation> _reservation;
        private readonly IRepository<ReserveSeatMap> _reserveSeatMap;
        private readonly IRepository<ReserveTable> _reserveTable;
        private readonly IRepository<Customer> _customer;
        private readonly IRepository<ReserveSeatMapBackgroundUrl> _reserveSeatMapBackgroundUrl;
        public ReserveAppService(
            IRepository<Reservation> reservation,
            IRepository<ReserveTable> reserveTable,
            IRepository<ReserveSeatMap> reserveSeatMap,
            IRepository<Customer> customer,
            IRepository<ReserveSeatMapBackgroundUrl> reserveSeatMapBackgroundUrl
            )
        {
            _reservation = reservation;
            _reserveSeatMap = reserveSeatMap;
            _reserveTable = reserveTable;
            _customer = customer;
            _reserveSeatMapBackgroundUrl = reserveSeatMapBackgroundUrl;
        }

        public async Task<List<ScheduleOutputModel>> GetReservation(DateTime dateBook)
        {
            try
            {
                DateTime startDate = dateBook.Date;
                DateTime endDate = startDate.AddDays(1).Date;
                List<Reservation> listReservation = _reservation.GetAll().Where(c => !c.IsDeleted && c.FromDate >= startDate && c.FromDate <= endDate).ToList();
                List<ScheduleOutputModel> response = new List<ScheduleOutputModel>();
                foreach (var item in listReservation)
                {
                    if (!String.IsNullOrEmpty(item.ReserveTableIds))
                    {
                        List<string> listTableId = new List<string>();
                        listTableId = item.ReserveTableIds.Split(',').ToList();
                        for (int i = 0; i < listTableId.Count(); i++)
                        {
                            if (item.FromDate.HasValue && item.ToDate.HasValue)
                            {
                                ScheduleOutputModel itemResponse = new ScheduleOutputModel();
                                itemResponse.tableId = Convert.ToInt32(listTableId[i]);
                                BookingOutputModel itemBooking = new BookingOutputModel();
                                itemBooking.bookingId = item.Id;
                                TimeSpan differenceFrom = item.FromDate.Value.Subtract(startDate);
                                itemBooking.from = differenceFrom.TotalMinutes;
                                TimeSpan differenceTo = item.ToDate.Value.Subtract(startDate);
                                itemBooking.to = differenceTo.TotalMinutes;
                                itemResponse.booking = itemBooking;
                                itemResponse.status = item.Status;
                                CustomerOutput itemClient = await GetCustomerDataById(item.CustomerId.HasValue ? item.CustomerId.Value : 0);
                                if (itemClient != null)
                                {
                                    itemResponse.name = itemClient.name;
                                    response.Add(itemResponse);
                                }
                            }
                        }
                    }
                }
                return response;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        private async Task<ReserveTableOutput> GetSeatMapById(int id)
        {
            ReserveTable reserveTable = await _reserveTable.GetAsync(id);
            ReserveTableOutput data = new ReserveTableOutput();
            data.Capacity = reserveTable.Capacity;
            return data;
        }

        public async Task<bool> CheckAvailabelBooking(DateTime dateBook, int people)
        {
            try
            {
                var result = GetReservationByTime(dateBook).ToList();
                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List<string> listTableId = new List<string>();
                        listTableId = item.ReserveTableIds.Split(',').ToList();
                        foreach (var tableId in listTableId)
                        {
                            ReserveTableOutput tableResult = await GetSeatMapById(Convert.ToInt32(tableId));
                            if (tableResult.Capacity <= people)
                            {
                                throw new UserFriendlyException("Not avaiable booking");
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<List<ReserveSeatMapInputOutputModel>> GetSeatMapList()
        {
            try
            {
                List<ReserveSeatMapInputOutputModel> seatMapOutputList = new List<ReserveSeatMapInputOutputModel>();
                List<ReserveSeatMap> reserveSeatMapList = _reserveSeatMap.GetAll().Where(c => !c.IsDeleted).ToList();
                foreach (ReserveSeatMap reserveSeatMap in reserveSeatMapList)
                {
                    ReserveSeatMapInputOutputModel seatMapOutput = new ReserveSeatMapInputOutputModel();
                    ReserveTable reserveTable = _reserveTable.GetAll().Where(c => c.ReserveSeatMapId == reserveSeatMap.Id).FirstOrDefault();
                    seatMapOutput.capacity = reserveTable.Capacity;
                    seatMapOutput.minimum = reserveTable.Minimum;
                    seatMapOutput.name = reserveTable.Name;
                    seatMapOutput.positionX = reserveSeatMap.PositionX;
                    seatMapOutput.positionZ = reserveSeatMap.PositionZ;
                    seatMapOutput.width = reserveSeatMap.Width;
                    seatMapOutput.height = reserveSeatMap.Height;
                    seatMapOutput.id = reserveSeatMap.Id;

                    seatMapOutputList.Add(seatMapOutput);
                }
                return seatMapOutputList;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task SaveReserveTable(ReserveSeatMapInputOutputModel reserveTable)
        {
            try
            {
                if (reserveTable.id > 0)
                {
                    ReserveTable reserveTableUpdate = _reserveTable.GetAll().Where(c => c.ReserveSeatMapId == reserveTable.id).FirstOrDefault();
                    reserveTableUpdate.Name = reserveTable.name;
                    reserveTableUpdate.Minimum = reserveTable.minimum;
                    reserveTableUpdate.Capacity = reserveTable.capacity;
                    await _reserveTable.DeleteAsync(reserveTableUpdate);
                    await _reserveTable.UpdateAsync(reserveTableUpdate);
                }
                else
                {
                    ReserveSeatMap reserveSeatmapNew = new ReserveSeatMap();
                    reserveSeatmapNew.Name = reserveTable.name;
                    reserveSeatmapNew.Width = reserveTable.width;
                    reserveSeatmapNew.Height = reserveTable.height;
                    reserveSeatmapNew.PositionX = reserveTable.positionX;
                    reserveSeatmapNew.PositionZ = reserveTable.positionZ;
                    int reserveSeatMapId = await _reserveSeatMap.InsertAndGetIdAsync(reserveSeatmapNew);
                    ReserveTable reserveTableNew = new ReserveTable();
                    reserveTableNew.Name = reserveTable.name;
                    reserveTableNew.Minimum = reserveTable.minimum;
                    reserveTableNew.Capacity = reserveTable.capacity;
                    reserveTableNew.ReserveSeatMapId = reserveSeatMapId;
                    await _reserveTable.InsertAsync(reserveTableNew);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task SaveReserveSeatMap(List<ReserveSeatMapInputOutputModel> reserveSeatMapList)
        {
            try
            {
                foreach (ReserveSeatMapInputOutputModel reserveSeatMap in reserveSeatMapList)
                {
                    if (reserveSeatMap.id > 0)
                    {
                        ReserveSeatMap reserveSeatMapUpdate = await _reserveSeatMap.GetAsync(reserveSeatMap.id);
                        reserveSeatMapUpdate.Name = reserveSeatMap.name;
                        reserveSeatMapUpdate.Width = reserveSeatMap.width;
                        reserveSeatMapUpdate.Height = reserveSeatMap.height;
                        reserveSeatMapUpdate.PositionX = reserveSeatMap.positionX;
                        reserveSeatMapUpdate.PositionZ = reserveSeatMap.positionZ;
                        reserveSeatMapUpdate.IsDeleted = reserveSeatMap.isDeleted;

                        await _reserveSeatMap.DeleteAsync(reserveSeatMapUpdate);
                        await _reserveSeatMap.UpdateAsync(reserveSeatMapUpdate);
                    }
                    else
                    {
                        ReserveSeatMap reserveSeatMapNew = new ReserveSeatMap();
                        reserveSeatMapNew.Name = reserveSeatMap.name;
                        reserveSeatMapNew.Width = reserveSeatMap.width;
                        reserveSeatMapNew.Height = reserveSeatMap.height;
                        reserveSeatMapNew.PositionX = reserveSeatMap.positionX;
                        reserveSeatMapNew.PositionZ = reserveSeatMap.positionZ;
                        reserveSeatMapNew.IsDeleted = reserveSeatMap.isDeleted;
                        int reserveSeatMapId = await _reserveSeatMap.InsertAndGetIdAsync(reserveSeatMapNew);
                        ReserveTable reserveTableNew = new ReserveTable();
                        reserveTableNew.Name = reserveSeatMap.name;
                        reserveTableNew.Minimum = reserveSeatMap.minimum;
                        reserveTableNew.Capacity = reserveSeatMap.capacity;
                        reserveTableNew.ReserveSeatMapId = reserveSeatMapId;
                        await _reserveTable.InsertAsync(reserveTableNew);
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task SaveReservationDetail(ReserveDetailInputModel reserveDetailInputModel)
        {
            try
            {
                if (reserveDetailInputModel.id > 0)
                {

                    Customer customerSave = await _customer.GetAsync(reserveDetailInputModel.customerId);
                    customerSave.Name = reserveDetailInputModel.firstName + reserveDetailInputModel.lastName;
                    customerSave.EmailAddress = reserveDetailInputModel.email;
                    customerSave.PhoneNumber = reserveDetailInputModel.phone;
                    await _customer.DeleteAsync(customerSave);
                    await _customer.UpdateAsync(customerSave);

                    Reservation reservation = await _reservation.GetAsync(reserveDetailInputModel.id);
                    reservation.FromDate = reserveDetailInputModel.fromDate;
                    reservation.ToDate = reserveDetailInputModel.toDate;
                    reservation.Status = reserveDetailInputModel.status;
                    reservation.DepositFee = reserveDetailInputModel.depositFee;
                    reservation.PaymentDetails = reserveDetailInputModel.paymentMethod;
                    reservation.DepositPay = reserveDetailInputModel.depositPaid;
                    reservation.TotalPeopleCount = reserveDetailInputModel.people;
                    reservation.ReserveTableIds = String.Join(",", reserveDetailInputModel.tableIds);
                    await _reservation.DeleteAsync(reservation);
                    await _reservation.UpdateAsync(reservation);
                }
                else
                {
                    Customer customerSave = new Customer();
                    customerSave.Name = reserveDetailInputModel.firstName + reserveDetailInputModel.lastName;
                    customerSave.EmailAddress = reserveDetailInputModel.email;
                    customerSave.PhoneNumber = reserveDetailInputModel.phone;

                    var idCustomer = await _customer.InsertAndGetIdAsync(customerSave);


                    var reservationSave = new Reservation();
                    reservationSave.FromDate = reserveDetailInputModel.fromDate;
                    reservationSave.ToDate = reserveDetailInputModel.toDate;
                    reservationSave.Status = reserveDetailInputModel.status;
                    reservationSave.DepositFee = reserveDetailInputModel.depositFee;
                    reservationSave.PaymentDetails = reserveDetailInputModel.paymentMethod;
                    reservationSave.DepositPay = reserveDetailInputModel.depositPaid;
                    reservationSave.TotalPeopleCount = reserveDetailInputModel.people;
                    reservationSave.CustomerId = Convert.ToInt32(idCustomer);
                    reservationSave.ReserveTableIds = String.Join(",", reserveDetailInputModel.tableIds);
                    await _reservation.InsertAsync(reservationSave);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        private async Task<CustomerOutput> GetCustomerDataById(int id)
        {
            Customer customer = await _customer.GetAsync(id);
            CustomerOutput data = new CustomerOutput();
            data.name = customer.Name;
            data.email = customer.EmailAddress;
            return data;
        }

        public async Task<CustomerOutput> GetCustomerById(int id)
        {
            try
            {
                return await GetCustomerDataById(id);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        private List<ReservationOutput> GetReservationByTime(DateTime timeBook)
        {
            List<ReservationOutput> data = new List<ReservationOutput>();
            List<Reservation> listReservation = _reservation.GetAll().Where(c => !c.IsDeleted && c.FromDate <= timeBook && c.ToDate >= timeBook).ToList();
            foreach (Reservation item in listReservation)
            {
                ReservationOutput itemOut = new ReservationOutput();
                itemOut.ReserveTableIds = item.ReserveTableIds;
                data.Add(itemOut);
            }
            return data;
        }

        public async Task<ReservationOutput> GetReservationById(int id)
        {
            try
            {
                Reservation reservation = await _reservation.GetAsync(id);
                ReservationOutput data = new ReservationOutput();
                data.FromDate = reservation.FromDate;
                data.ToDate = reservation.ToDate;
                data.Status = reservation.Status;
                data.DepositFee = reservation.DepositFee;
                data.PaymentDetails = reservation.PaymentDetails;
                data.DepositPay = reservation.DepositPay;
                data.TotalPeopleCount = reservation.TotalPeopleCount;
                data.ReserveTableIds = reservation.ReserveTableIds;
                data.CustomerId = reservation.CustomerId;
                data.Id = reservation.Id;

                return data;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public Task<SeatMapBackgroundUrlInputOutputModel> GetSeatMapBackgroundUrl()
        {
            try
            {
                SeatMapBackgroundUrlInputOutputModel response = new SeatMapBackgroundUrlInputOutputModel();
                if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
                {
                    string url = _reserveSeatMapBackgroundUrl.GetAll().Where(c => !c.IsDeleted && c.TenantId == AbpSession.TenantId && c.Url != null).Select(x => x.Url).FirstOrDefault();
                    response.url = (url != null) ? url : "";
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task DeleteSeatMap()
        {
            try
            {
                if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
                {
                    ReserveSeatMapBackgroundUrl reserveSeatMapBackgroundUrl = _reserveSeatMapBackgroundUrl.GetAll().Where(x => x.TenantId == AbpSession.TenantId).FirstOrDefault();
                    if (reserveSeatMapBackgroundUrl != null)
                    {
                        reserveSeatMapBackgroundUrl.IsDeleted = true;
                        reserveSeatMapBackgroundUrl.LastModificationTime = DateTime.Now;
                        reserveSeatMapBackgroundUrl.DeletionTime = DateTime.Now;
                        reserveSeatMapBackgroundUrl.DeleterUserId = AbpSession.UserId;
                        reserveSeatMapBackgroundUrl.LastModifierUserId = AbpSession.UserId;

                        await _reserveSeatMapBackgroundUrl.UpdateAsync(reserveSeatMapBackgroundUrl);
                    }
                    List<ReserveSeatMap> reserveSeatMapList = _reserveSeatMap.GetAll().Where(x => x.TenantId == AbpSession.TenantId).ToList();

                    if (reserveSeatMapList != null)
                    {
                        foreach (var reserveSeatMapItem in reserveSeatMapList)
                        {
                            reserveSeatMapItem.IsDeleted = true;
                            reserveSeatMapItem.LastModificationTime = DateTime.Now;
                            reserveSeatMapItem.DeletionTime = DateTime.Now;
                            reserveSeatMapItem.DeleterUserId = AbpSession.UserId;
                            reserveSeatMapItem.LastModifierUserId = AbpSession.UserId;

                            await _reserveSeatMap.UpdateAsync(reserveSeatMapItem);

                            List<ReserveTable> reserveTablesList = _reserveTable.GetAllList().Where(x => !x.IsDeleted && x.TenantId == AbpSession.TenantId && x.ReserveSeatMapId == reserveSeatMapItem.Id).ToList();

                            foreach (var reserveTablesItem in reserveTablesList)
                            {
                                ReserveTable reserveTable = await _reserveTable.GetAsync(reserveTablesItem.Id);
                                reserveTable.IsDeleted = true;
                                reserveTable.LastModificationTime = DateTime.Now;
                                reserveTable.DeletionTime = DateTime.Now;
                                reserveTable.DeleterUserId = AbpSession.UserId;
                                reserveTable.LastModifierUserId = AbpSession.UserId;
                                await _reserveTable.UpdateAsync(reserveTable);
                            }
                        }
                    }
                }
            }

            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task SaveSeatMapBackgroundUrl(SeatMapBackgroundUrlInputOutputModel request)
        {
            try
            {
                ReserveSeatMapBackgroundUrl reserveSeatMapBackgroundUrl = new ReserveSeatMapBackgroundUrl();
                if (request.url != null && AbpSession.TenantId.HasValue)
                {
                    reserveSeatMapBackgroundUrl.Url = request.url;
                    reserveSeatMapBackgroundUrl.LastModificationTime = DateTime.Now;
                    reserveSeatMapBackgroundUrl.TenantId = (int)AbpSession.TenantId;

                    await _reserveSeatMapBackgroundUrl.InsertAsync(reserveSeatMapBackgroundUrl);

                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

    }
}

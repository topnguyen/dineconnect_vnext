﻿using System.Collections.Generic;
using Abp;
using DinePlan.DineConnect.Chat.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}

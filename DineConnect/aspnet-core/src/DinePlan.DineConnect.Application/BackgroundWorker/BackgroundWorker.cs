using Abp.Dependency;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using DinePlan.DineConnect.BackgroundJob;

namespace DinePlan.DineConnect.BackgroundWorker
{
    public class BackgroundWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private const int CheckPeriodAsMilliseconds = 36000000; // 1 hours
        private readonly IBackgroundJobService _backgroundJobService;

        public BackgroundWorker(IBackgroundJobService backgroundJobService, AbpTimer timer) : base(timer)
        {
            _backgroundJobService = backgroundJobService;
            Timer.Period = CheckPeriodAsMilliseconds;
            Timer.RunOnStart = true;
        }

        protected override void DoWork()
        {
            this.StartEmailJob();
            this.StartReportJob();
        }
        public void StartEmailJob()
        {
            _backgroundJobService.SendMailTestAsync("email@gmail.com");
        }

        public void StartReportJob()
        {
            _backgroundJobService.GenerateReport("", "");
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}

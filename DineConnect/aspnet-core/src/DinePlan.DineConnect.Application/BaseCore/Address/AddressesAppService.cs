﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.Address.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.BaseCore.Address
{
    public class AddressesAppService : DineConnectAppServiceBase, IAddressesAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAddressesManager _addressesManager;
        private readonly ConnectSession _connectSession;
        private readonly IAddressesExcelExporter _addressesExcelExporter;

        public AddressesAppService(IUnitOfWorkManager unitOfWorkManager, IAddressesManager addressesManager, ConnectSession connectSession, IAddressesExcelExporter addressesExcelExporter)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _addressesManager = addressesManager;
            _connectSession = connectSession;
            _addressesExcelExporter = addressesExcelExporter;
        }

        public async Task<int> CreateorUpdateAddressAsync(AddressDto address)
        {
            var add = ObjectMapper.Map<Addresses.Address>(address);
            add.TenantId = (int)_connectSession.TenantId;
            return await _addressesManager.CreateorUpdateAddressAsync(add);
        }

        
        public async Task<int> CreateorUpdateCityAsync(CityDto city)
        {
            var add = ObjectMapper.Map<City>(city);
            add.Country = null;
            add.State = null;
            add.StateId = add.StateId == 0 ? null : add.StateId;
            add.TenantId = AbpSession.TenantId ?? 0;
            return await _addressesManager.CreateorUpdateCityAsync(add);
        }

        
        public async Task<int> CreateorUpdateCountryAsync(CountryDto country)
        {
            var add = ObjectMapper.Map<Country>(country);
            add.TenantId = AbpSession.TenantId ?? 0;
            return await _addressesManager.CreateorUpdateCountryAsync(add);
        }

        
        public async Task<int> CreateorUpdateStateAsync(StateDto state)
        {
            var add = ObjectMapper.Map<State>(state);
            add.TenantId = AbpSession.TenantId ?? 0;
            return await _addressesManager.CreateorUpdateStateAsync(add);
        }

        
        public async Task DeleteCountryAsync(int countryId)
        {
            try
            {
                await _addressesManager.DeleteCountryAsync(countryId);
            }
            catch (DbUpdateException ex)
            {
                HandleException(ex);
            }
        }

        
        public async Task DeleteCityAsync(int cityId)
        {
            try
            {
                await _addressesManager.DeleteCityAsync(cityId);
            }
            catch (DbUpdateException ex)
            {
                HandleException(ex);
            }
        }

        
        public async Task DeleteStateAsync(int stateId)
        {
            try
            {
                await _addressesManager.DeleteStateAsync(stateId);
            }
            catch (DbUpdateException ex)
            {
                HandleException(ex);
            }
        }

        
        public async Task ActivateCoutry(int id)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _addressesManager.GetCountryAsync(id);

                item.IsDeleted = false;
            }
        }

        
        public async Task ActivateCity(int id)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _addressesManager.GetCityAsync(id);

                item.IsDeleted = false;
            }
        }

        
        public async Task ActivateState(int id)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _addressesManager.GetStateAsync(id);

                item.IsDeleted = false;
            }
        }

        private void HandleException(DbUpdateException ex)
        {
            if (ex.Message.Contains("REFERENCE constraint"))
                throw new UserFriendlyException("Can not delete, delete related information and try again");

            if (ex.InnerException != null)
            {
                if (ex.InnerException.Message.Contains("REFERENCE constraint"))
                    throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            throw new UserFriendlyException("Something went wrong");
        }

        public async Task<AddressDto> GetAddressAsync(int id)
        {
            return ObjectMapper.Map<AddressDto>(await _addressesManager.GetAddressAsync(id));
        }

        public Task<PagedResultDto<AddressDto>> GetAddressList(string filter)
        {
            throw new NotImplementedException();
        }

        public async Task<CityDto> GetCityAsync(int id)
        {
            var result = await _addressesManager.GetCityAsync(id);

            return new CityDto
            {
                CountryId = result.State.CountryId,
                StateId = result.StateId ?? 0,
                Id = result.Id,
                Name = result.Name
            };
        }

        public async Task<PagedResultDto<CityDto>> GetCityList(GetCityInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var result = _addressesManager.GetCityList(input.Filter, input.CountryId, input.StateId)
                    .Where(x => x.IsDeleted == input.IsDeleted);

                var cityCount = await result.CountAsync();
                result = result.OrderBy(input.Sorting).PageBy(input);
                var s = result.Select(c => new CityDto
                {
                    CountryId = c.CountryId,
                    StateId = c.StateId ?? 0,
                    Id = c.Id,
                    Name = c.Name,
                    StateName = c.State.Name ?? "",
                    CountryName = c.Country.Name ?? ""
                });
                return new PagedResultDto<CityDto>(cityCount, await s.ToListAsync());
            }
        }

        public async Task<FileDto> GetCityListToExcel(GetCityForExcelInput input)
        {
            var result = _addressesManager.GetCityList(input.Filter, input.CountryId, input.StateId);

            var s = result.Select(c => new CityDto
            {
                CountryId = c.CountryId,
                StateId = c.StateId ?? 0,
                Id = c.Id,
                Name = c.Name,
                StateName = c.State.Name ?? "",
                CountryName = c.Country.Name ?? ""
            });

            return _addressesExcelExporter.ExportCityToFile(await s.ToListAsync());
        }

        public async Task<CountryDto> GetCountryAsync(int id)
        {
            return ObjectMapper.Map<CountryDto>(await _addressesManager.GetCountryAsync(id));
        }

        public async Task<PagedResultDto<CountryDto>> GetCountryList(GetCountryInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var result = _addressesManager.GetCountryList(input.Filter)
                    .Where(x => x.IsDeleted == input.IsDeleted);

                var countryCount = await result.CountAsync();
                result = result.OrderBy(input.Sorting).PageBy(input);
                var resultToReturn = ObjectMapper.Map<List<CountryDto>>(await result.ToListAsync());
                return new PagedResultDto<CountryDto>(countryCount, resultToReturn);
            }
        }

        public async Task<FileDto> GetCountryListToExcel(GetCountryForExcelInput input)
        {
            var result = _addressesManager.GetCountryList(input.Filter);
            var list = ObjectMapper.Map<List<CountryDto>>(await result.ToListAsync());

            return _addressesExcelExporter.ExportCountryToFile(list);
        }

        public async Task<PagedResultDto<StateDto>> GetStateList(GetStateInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var result = _addressesManager.GetStateList(input.Filter,
                input.CoundtryId).Where(x => x.IsDeleted == input.IsDeleted);

                var stateCount = await result.CountAsync();
                result = result.OrderBy(input.Sorting).PageBy(input);
                var resultToReturn = ObjectMapper.Map<List<StateDto>>(await result.ToListAsync());
                return new PagedResultDto<StateDto>(stateCount, resultToReturn);
            }
        }

        public async Task<FileDto> GetStateListToExcel(GetStateForExcelInput input)
        {
            var result = _addressesManager.GetStateList(input.Filter,
                input.CoundtryId);

            var list = ObjectMapper.Map<List<StateDto>>(await result.ToListAsync());

            return _addressesExcelExporter.ExportStatesToFile(list);
        }

        public async Task<List<StateDto>> GetStateListByCountry(int id)
        {
            return ObjectMapper.Map<List<StateDto>>(await _addressesManager.GetStateListByCountry(id));
        }

        public async Task<StateDto> GetSteteAsync(int id)
        {
            return ObjectMapper.Map<StateDto>(await _addressesManager.GetStateAsync(id));
        }
    }
}
﻿using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.BaseCore.Address.Exporting
{
    public interface IAddressesExcelExporter
    {
        FileDto ExportCountryToFile(List<CountryDto> countries);
        FileDto ExportStatesToFile(List<StateDto> states);
        FileDto ExportCityToFile(List<CityDto> cities);
    }
}

﻿using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.BaseCore.Address.Exporting
{
    public class AddressesExcelExporter : NpoiExcelExporterBase, IAddressesExcelExporter
    {

        public AddressesExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public FileDto ExportCountryToFile(List<CountryDto> countries)
        {
            return CreateExcelPackage(
               "Countries.xlsx",
               excelPackage =>
               {

                   var sheet = excelPackage.CreateSheet(L("Countries"));

                   AddHeader(
                       sheet,
                       L("Name"),
                       L("Code")
                       );

                   AddObjects(
                       sheet, 2, countries,
                       _ => _.Name,
                       _ => _.CallingCode
                       );

               });
        }

        public FileDto ExportStatesToFile(List<StateDto> states)
        {
            return CreateExcelPackage(
               "States.xlsx",
               excelPackage =>
               {

                   var sheet = excelPackage.CreateSheet(L("States"));

                   AddHeader(
                       sheet,
                       L("Name"),
                       L("Country")
                       );

                   AddObjects(
                       sheet, 2, states,
                       _ => _.Name,
                       _ => _.CountryName
                       );

               });
        }

        public FileDto ExportCityToFile(List<CityDto> cities)
        {
            return CreateExcelPackage(
              "Cities.xlsx",
              excelPackage =>
              {

                  var sheet = excelPackage.CreateSheet(L("Cities"));

                  AddHeader(
                      sheet,
                      L("Name"),
                      L("State"),
                      L("Country")
                      );

                  AddObjects(
                      sheet, 2, cities,
                      _ => _.Name,
                      _ => _.StateName,
                      _ => _.CountryName
                      );

              });
        }
    }
}

﻿using System;
using System.Data.Entity;
using System.Linq;
using Abp.Application.Features;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Abp.Timing;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Net.Whatapp;
using DinePlan.DineConnect.Tiffins;
using DinePlan.DineConnect.Whatapp;
using Microsoft.Extensions.DependencyInjection;

namespace DinePlan.DineConnect.BaseCore.Jobs.TiffinWhatappJob
{
    public class TiffinWhatappJob : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IFeatureChecker _featureChecker;
        private readonly IServiceProvider _serviceProvider;
        private readonly IRepository<Customer.Customer> _customerRepository;
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepository;

        public TiffinWhatappJob(AbpTimer timer,
            IRepository<Tenant> tenantRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IFeatureChecker featureChecker,
            IServiceProvider serviceProvider,
            IRepository<BaseCore.Customer.Customer> customerRepository,
            IRepository<TiffinCustomerProductOffer> customerProductOfferRepository 
        ) : base(timer)
        {
            _tenantRepository = tenantRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _featureChecker = featureChecker;
            _serviceProvider = serviceProvider;
            _customerRepository = customerRepository;
            _customerProductOfferRepository = customerProductOfferRepository;
            Timer.RunOnStart = true;
            Timer.Period = 1000 * 60 * 60 * 1; // 1 hours
        }

        protected override void DoWork()
        {
            var now = Clock.Now;

            // Check in a specific Time to run
            if (now.ToUniversalTime().Hour != 9)
            {
                return;
            }

            using (var uow = _unitOfWorkManager.Begin())
            {
                var tenants = _tenantRepository.GetAll().ToList();

                foreach (var tenant in tenants)
                {
                    if (!_featureChecker.IsEnabled(tenant.Id, AppFeatures.Tiffins))
                    {
                        continue;
                    }

                    using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                    {
                        // When customer doesn’t buy and leaves the cart
                        SendRemindBuyMealPlan();
                        // When customer meal plan is expiring
                        SendRemindMealPlanExpiring();
                    }
                }
                
                uow.Complete();
            }
        }

        private void SendRemindBuyMealPlan()
        {
            var customers = _customerRepository
                .GetAll()
                .Where(x => x.JsonData != null)
                .ToList();

            var whatappSendAppService = _serviceProvider.GetService<ITenantWhatappSendAppService>();
            foreach (var customer in customers)
            {
                if (customer.CartJsonData.IsNullOrWhiteSpace())
                {
                    continue;
                }

                // Send Whatapp
                AsyncHelper.RunSync(() => whatappSendAppService.SendWhatappByTemplateAsync(customer.PhoneNumber,
                    "BuyMealPlanReminder", customer.TenantId, new
                    {
                        CustomerName = customer.Name
                    }));
            }
        }

        private void SendRemindMealPlanExpiring()
        {
            var now = Clock.Now;
            var dateToRemindMealPlanExpiring = now.Date.AddDays(3); // expiring, 3 days

            var mealPlans = _customerProductOfferRepository
                .GetAll()
                .Where(x => x.ExpiryDate < dateToRemindMealPlanExpiring)
                .Include(x => x.ProductOffer)
                .ToList();

            var customerIds = mealPlans.Select(x => x.CustomerId).Distinct().ToList();

            var customers = _customerRepository
                .GetAll()
                .Where(x => customerIds.Contains(x.Id) && !x.IsDisableDailyOrderReminder)
                .ToList();

            var whatappSendAppService = _serviceProvider.GetService<ITenantWhatappSendAppService>();

            foreach (var customer in customers)
            {
                var mealPlansOfCustomer = mealPlans.FirstOrDefault(x => x.CustomerId == customer.Id);

                if (mealPlansOfCustomer == null)
                {
                    continue;
                }

                // Send Whatapp

                AsyncHelper.RunSync(() => whatappSendAppService.SendWhatappByTemplateAsync(customer.PhoneNumber,
                    "MinExpiryDay", customer.TenantId, new
                    {
                        First_name = customer.Name,
                        ExpiryDate = mealPlansOfCustomer.ExpiryDate.ToString(),
                        AvailableBalance = mealPlansOfCustomer.BalanceCredit.HasValue
                            ? mealPlansOfCustomer.BalanceCredit.Value.ToString()
                            : "0"
                    }));
            }
        }
    }
}
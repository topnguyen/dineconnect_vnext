﻿using System;
using Abp;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;

using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.BaseCore.Report.Implementation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.DriverReport;
using DinePlan.DineConnect.Tiffins.Report.KitchenReport;
using DinePlan.DineConnect.Tiffins.Report.OrderReport;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;

namespace DinePlan.DineConnect.BaseCore.Jobs.TiffinReportJob
{
    public class TiffinReportJob : BackgroundJob<TiffinReportInputArgs>, ITransientDependency
    {
        private readonly IOrderSummaryExporter _orderExporter;
        private readonly IPaymentSummaryExporter _paymentExporter;
        private readonly IBalanceSummaryExporter _balanceExporter;
        private readonly IReportBackgroundAppService _reportAppService;
        private readonly IDriverReportExporter _driverExporter;
        private readonly IKitchenReportExporter _kitchenExporter;
        private readonly IPreparationReportExporter _preparationReportExporter;
        private readonly ReportNotificationAppService _reportNotificationService;

        public TiffinReportJob(IOrderSummaryExporter orderExporter,
            IPaymentSummaryExporter paymentExporter,
            IBalanceSummaryExporter balanceExporter,
            IReportBackgroundAppService reportAppService,
            IDriverReportExporter driverExporter,
            IKitchenReportExporter kitchenExporter,
            IPreparationReportExporter preparationReportExporter,
            ReportNotificationAppService reportNotificationService)
        {
            _orderExporter = orderExporter;
            _paymentExporter = paymentExporter;
            _balanceExporter = balanceExporter;
            _reportAppService = reportAppService;
            _driverExporter = driverExporter;
            _kitchenExporter = kitchenExporter;
            _preparationReportExporter = preparationReportExporter;
            _reportNotificationService = reportNotificationService;
        }

        [UnitOfWork]
        public override void Execute(TiffinReportInputArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = null;
                switch (args.ReportName)
                {
                    case ReportNames.ORDERSUMMARY:
                        fileDto = AsyncHelper.RunSync(() =>
                            _orderExporter.ExportOrderSummary(args.ExportOrderInput));
                        break;

                    case ReportNames.PAYMENTSUMMARY:
                        fileDto = AsyncHelper.RunSync(() =>
                            _paymentExporter.ExportPaymentSummary(args.GetPaymentReportForExport));
                        break;

                    case ReportNames.BALANCE:
                        fileDto = AsyncHelper.RunSync(() =>
                            _balanceExporter.ExportBalanceSummary(args.ExportBalanceInput));
                        break;

                    case ReportNames.DRIVER:
                        fileDto = AsyncHelper.RunSync(() =>
                            _driverExporter.ExportSummary(args.ExportDriverInput));
                        break;

                    case ReportNames.KITCHEN:
                        fileDto = AsyncHelper.RunSync(() =>
                            _kitchenExporter.ExportSummary(args.ExportKitchenInput));
                        break;

                    case ReportNames.PREPARATIONREPORT:
                        fileDto = AsyncHelper.RunSync(() =>
                            _preparationReportExporter.ExportPreparationReport(args.ExportPreparationReportInput));
                        break;
                }
                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() =>
                        _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                    AsyncHelper.RunSync(() =>
                        _reportNotificationService.Publish_SentReportNotificationRequest(new UserIdentifier(args.TenantId, args.UserId)));
                }
            }
        }
    }

    [Serializable]
    public class TiffinReportInputArgs
    {
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public ExportOrderInput ExportOrderInput { get; set; }
        public ExportBalanceInput ExportBalanceInput { get; set; }
        public ExportPreparationReportInput ExportDriverInput { get; set; }
        public ExportPreparationReportInput ExportKitchenInput { get; set; }
        public ExportPreparationReportInput ExportPreparationReportInput { get; set; }
        public GetPaymentReportForExportInput GetPaymentReportForExport { get; set; }
    }
}
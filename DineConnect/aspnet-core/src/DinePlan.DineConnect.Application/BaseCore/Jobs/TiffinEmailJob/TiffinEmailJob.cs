﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.EmailNotification;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;

namespace DinePlan.DineConnect.BaseCore.Jobs.TiffinEmailJob
{
    public class TiffinEmailJob : BackgroundJob<ConnectEmailInputArgs>, ITransientDependency
    {
        private readonly EmailNotificationAppService _emailNotificationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TiffinEmailJob(EmailNotificationAppService emailNotificationAppService, IUnitOfWorkManager unitOfWorkManager)
        {
            _emailNotificationAppService = emailNotificationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
       
        public override void Execute(ConnectEmailInputArgs args)
        {
            if (args != null)
            {
                switch (args.EmailName)
                {
                    case EmailName.BUYMEAL:
                        AsyncHelper.RunSync(() =>
                            _emailNotificationAppService.SendEmailWhenCustomerBuyMealPlan(args.PaymentDetail, args.TenantId, args.FileAttach, args.CustomerAddress));
                        break;

                    case EmailName.BUYORDER:
                        AsyncHelper.RunSync(() =>
                            _emailNotificationAppService.SendEmailWhenCustomerOrder(args.OrderSendEmails, args.TenantId, args.CustomerAddress));
                        break;

                    case EmailName.MEALCOUNT:
                        AsyncHelper.RunSync(() =>
                            _emailNotificationAppService.SendEmailWhenMinimumQuantity(args.OrderMinQuantity, args.TenantId, args.CustomerAddress));
                        break;
                }
            }
        }
    }

    [Serializable]
    public class ConnectEmailInputArgs
    {
        public string EmailName { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public long CustomerId { get; set; }
        public long PaymentId { get; set; }

        public int MinMealCount { get; set; }

        public TiffinPaymentDetailDto PaymentDetail { get; set; }
        public FileDto FileAttach { get; set; }
        public CustomerAddressSendEmail CustomerAddress { get; set; }
        public List<OrderSendEmail> OrderSendEmails { get; set; }
        public OrderMinQuantity OrderMinQuantity { get; set; }
    }
}

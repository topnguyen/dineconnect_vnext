﻿using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.CashPaymentDto;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class CashPaymentProcessor : BasePaymentProcessor, ICashPaymentProcessor, ITransientDependency
    {
        public CashPaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(settingRepository, session, PaymentConsts.CashPaymentSystemName)
        {
        }

        public Task<CashPaymentResponseDto> ProcessPayment(CashPaymentRequestDto input)
        {
            return null;
        }
    }
}

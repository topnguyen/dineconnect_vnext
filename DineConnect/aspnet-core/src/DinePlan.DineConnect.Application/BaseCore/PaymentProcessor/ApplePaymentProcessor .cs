﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.ApplePaymentUtil;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.ApplePaymentDto;
using Flurl.Http;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class ApplePaymentProcessor : BasePaymentProcessor, IApplePaymentProcessor, ITransientDependency
    {
        public ApplePaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(
            settingRepository, session, PaymentConsts.ApplePaymentSystemName)
        {
        }

        public Task<ApplePaymentResponseDto> ProcessPayment(ApplePaymentRequestDto input)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetMerchantSessionAsync(string url, MerchantSessionRequest request, CancellationToken cancellationToken = default)
        {
            var response = await url.PostJsonAsync(request, cancellationToken: cancellationToken).ReceiveString();

            return response;
        }

        public async Task<string> ValidationAsync(string url, CancellationToken cancellationToken = default)
        {
            var settings = await GetSetting();

            ApplePayConfiguration config = JsonConvert.DeserializeObject<ApplePayConfiguration>(settings.DynamicFieldData.ToString());

            var merchantCertificate = new MerchantCertificate(config);

            // Create the JSON payload to POST to the Apple Pay merchant validation URL.
            var request = new MerchantSessionRequest()
            {
                DisplayName = config.StoreName,
                Initiative = "web",
                InitiativeContext = config.Domain,
                MerchantIdentifier = merchantCertificate.GetMerchantIdentifier(),
            };

            var merchantSession = await GetMerchantSessionAsync(url, request, cancellationToken);

            // Return the merchant session as-is to the JavaScript as JSON.
            return merchantSession;
        }

        public async Task<MerchantIdentity> GetMerchantIdentityAsync(CancellationToken cancellationToken = default)
        {
            var settings = await GetSetting();

            ApplePayConfiguration config = JsonConvert.DeserializeObject<ApplePayConfiguration>(settings.DynamicFieldData.ToString());

            var merchantCertificate = new MerchantCertificate(config);

            return new MerchantIdentity
            {
                Identity = merchantCertificate.GetMerchantIdentifier(),
                StoreName = config.StoreName
            };
        }
    }
}
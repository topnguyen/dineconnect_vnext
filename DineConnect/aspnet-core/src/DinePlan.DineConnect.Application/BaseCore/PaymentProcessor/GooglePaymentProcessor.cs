﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class GooglePaymentProcessor : BasePaymentProcessor, IGooglePaymentProcessor, ITransientDependency
    {
        public GooglePaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(
            settingRepository, session, PaymentConsts.GooglePaySystemName)
        {
        }

        public async Task<GooglePaymentResponseDto> ProcessPayment(GooglePaymentRequestDto input)
        {
            var paymentResponse = new GooglePaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                GooglePayConfiguration config = JsonConvert.DeserializeObject<GooglePayConfiguration>(settings.DynamicFieldData.ToString());
                
                // Try to parse the encrypted data
                // If success parse => valid payment, else invalid

                var isTestEnv = !config.Environment.Equals("Production", StringComparison.OrdinalIgnoreCase);
                
                var keyProvider = new GooglePay.PaymentDataCryptography.GoogleKeyProvider(isTestEnv);
                
                var parser = new GooglePay.PaymentDataCryptography.PaymentMethodTokenRecipient($"merchant:{config.MerchantId}", keyProvider);
                
                parser.AddPrivateKey(config.PrivateKey);
                
                var signedMessage = parser.VerifyAndGetSignedMessage(input.GooglePayToken);

                paymentResponse.PaymentId = signedMessage.EncryptedMessage;

                paymentResponse.IsSuccess = true;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }
    }
}
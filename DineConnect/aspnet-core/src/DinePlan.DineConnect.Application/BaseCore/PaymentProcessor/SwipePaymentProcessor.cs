﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.Core.Swipe;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto;
using DinePlan.DineConnect.SwipeModule.SwipeCards;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class SwipePaymentProcessor : BasePaymentProcessor, ISwipePaymentProcessor, ITransientDependency
    {
        private readonly ISwipeCardAppService _cardAppService;
        private readonly IRepository<SwipeCardOTP> _swipeCardOtpRepository;

        public SwipePaymentProcessor(IRepository<Setting, long> settingRepository,
            IAbpSession session,
            ISwipeCardAppService cardAppService,
            IRepository<SwipeCardOTP> swipeCardOtpRepository
            ) : base(settingRepository, session, PaymentConsts.SwipePaymentSystemName)
        {
            _cardAppService = cardAppService;
            _swipeCardOtpRepository = swipeCardOtpRepository;
        }

        public async Task<SwipePaymentResponseDto> ProcessPayment(SwipePaymentRequestDto input)
        {
            var paymentResponse = new SwipePaymentResponseDto();
            try
            {
                await _cardAppService.ValidateCardOTP(input.CardNumber, input.OTP);

                var cardOtp = _swipeCardOtpRepository.GetAll().FirstOrDefault(x => x.SwipeCard.CardNumber == input.CardNumber && x.OTP == input.OTP);
                await _swipeCardOtpRepository.DeleteAsync(cardOtp);

                var transactionResult = await _cardAppService.AddTransaction(new AddTransactionDto
                {
                    Amount = input.Amount * (-1),
                    CardNumber = input.CardNumber,
                    PaymentTransactionDetails = input.Note,
                    PaymentType = SwipeCardPaymentType.EWallet.ToString()
                });

                paymentResponse.PaymentId = transactionResult.Id.ToString();

                paymentResponse.IsSuccess = true;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;
using Newtonsoft.Json;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class PayPalPaymentProcessor : BasePaymentProcessor, IPaypalPaymentProcessor, ITransientDependency
    {
        public PayPalPaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(settingRepository, session, PaymentConsts.PayPalPaymentSystemName)
        {
        }

        public async Task<PaypalPaymentResponseDto> ProcessPayment(PaypalPaymentRequestDto input)
        {
            var paymentResponse = new PaypalPaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                PaypalConfiguration config =
                    JsonConvert.DeserializeObject<PaypalConfiguration>(settings.DynamicFieldData.ToString());

                var environment = GetEnvironment(config);

                var client = new PayPalHttpClient(environment);

                var request = new OrdersCaptureRequest(input.OrderId);

                request.RequestBody(new OrderActionRequest());

                var response = await client.Execute(request);

                var result = response.Result<Order>();

                paymentResponse.IsSuccess = result.Status == "COMPLETED";

                paymentResponse.PaymentId = result.Id;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }

        private static PayPalEnvironment GetEnvironment(PaypalConfiguration configuration)
        {
            switch (configuration.Environment)
            {
                case PaypalEnvironmentEnum.Sandbox:
                    {
                        return new SandboxEnvironment(configuration.ClientId, configuration.ClientSecret);
                    }
                case PaypalEnvironmentEnum.Live:
                    {
                        return new LiveEnvironment(configuration.ClientId, configuration.ClientSecret);
                    }
                default:
                    {
                        throw new ApplicationException("Unknown PayPal environment");
                    }
            }
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using Newtonsoft.Json;
using Omise;
using Omise.Models;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class OmisePaymentProcessor : BasePaymentProcessor, IOmisePaymentProcessor, ITransientDependency
    {
        public OmisePaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(
            settingRepository, session, PaymentConsts.OmisePaymentSystemName)
        {
        }

        public async Task<OmisePaymentResponseDto> ProcessPayment(OmisePaymentRequestDto input)
        {
            var paymentResponse = new OmisePaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                OmiseConfiguration config = JsonConvert.DeserializeObject<OmiseConfiguration>(settings.DynamicFieldData.ToString());

                var client = new Client(config.PublishKey, config.SecretKey);

                var chargeRequest = new CreateChargeRequest
                {
                    Amount = input.Amount,
                    Currency = input.Currency,
                    Card = input.OmiseToken,
                    ReturnUri = input.ReturnUrl
                };

                var result = await client.Charges.Create(chargeRequest);

                paymentResponse.IsSuccess = result.Status == ChargeStatus.Successful;

                paymentResponse.PaymentId = result.Id;

                paymentResponse.AuthorizeUri = result.AuthorizeURI;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }

        public async Task<OmisePaymentResponseDto> ProcessPaymentNow(OmisePaymentRequestDto input)
        {
            var paymentResponse = new OmisePaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                OmiseConfiguration config = JsonConvert.DeserializeObject<OmiseConfiguration>(settings.DynamicFieldData.ToString());

                var client = new Client(config.PublishKey, config.SecretKey);

                var source = await client.Sources.Create(new CreatePaymentSourceRequest()
                {
                    Amount = input.Amount,
                    Currency = input.Currency,
                    Type = OffsiteTypes.Paynow
                });

                var chargeRequest = new CreateChargeRequest
                {
                    Amount = input.Amount,
                    Currency = input.Currency,
                    Card = input.OmiseToken,
                    ReturnUri = input.ReturnUrl,
                    Source = source
                };

                var result = await client.Charges.Create(chargeRequest);

                paymentResponse.IsSuccess = result.Status == ChargeStatus.Successful;

                paymentResponse.PaymentId = result.Id;

                paymentResponse.AuthorizeUri = result.AuthorizeURI;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }

        public async Task<OmisePaymentResponseDto> GetStatus(string reference)
        {
            var paymentResponse = new OmisePaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                OmiseConfiguration config = JsonConvert.DeserializeObject<OmiseConfiguration>(settings.DynamicFieldData.ToString());

                var client = new Client(config.PublishKey, config.SecretKey);

                var result = await client.Charges.Get(reference);

                paymentResponse.IsSuccess = result.Status == ChargeStatus.Successful;

                paymentResponse.PaymentId = result.Id;

                paymentResponse.AuthorizeUri = result.AuthorizeURI;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }

        public async Task<string> CreateOmisePaymentNowRequest(long amount, string currency)
        {
            try
            {
                var settings = await GetSetting();

                OmiseConfiguration config = JsonConvert.DeserializeObject<OmiseConfiguration>(settings.DynamicFieldData.ToString());

                var client = new Client(config.PublishKey, config.SecretKey);

                var source = await client.Sources.Create(new CreatePaymentSourceRequest()
                {
                    Amount = amount,
                    Currency = currency,
                    Type = OffsiteTypes.Paynow
                });

                var chargeRequest = new CreateChargeRequest
                {
                    Amount = amount,
                    Currency = currency,
                    Source = source
                };

                var result = await client.Charges.Create(chargeRequest);

                var jsonStr = JsonConvert.SerializeObject(result);

                return jsonStr;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
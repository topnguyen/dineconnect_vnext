﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using static DinePlan.DineConnect.Wheel.WheelPaymentMethodConsts;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Base
{
    public abstract class BasePaymentProcessor : IBasePaymentProcessor
    {
        public virtual string PaymentProcessorName { get; set; }

        private readonly IRepository<Setting, long> _settingRepository;
        private IAbpSession _abpSession;

        public BasePaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session, string paymentProcessorName)
        {
            _settingRepository = settingRepository;
            _abpSession = session;
            PaymentProcessorName = paymentProcessorName;
        }

        public virtual async Task<PaymentProcessorInfo> GetSetting()
        {
            var settings = await _settingRepository.GetAll()
                .Where(x => x.Name.Contains(PaymentProcessorName))
                .ToListAsync();

            var friendlyName = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.FriendlyNameSetting}"))?.Value;
            var logo = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.LogoSetting}"))?.Value;
            var refund = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.RefundSetting}"))?.Value;
            var partialRefund = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.PartialRefundSetting}"))?.Value;
            var active = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.ActiveSetting}"))?.Value;
            var displayOrder = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.DisplayOrderSetting}"))?.Value;
            var recurringSupport = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.RecurringSupportSetting}"))?.Value;
            var supportsCapture = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.SupportsCaptureSetting}"))?.Value;
            var voidSetting = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.VoidSetting}"))?.Value;
            var dynamicFieldConfig = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.DynamicFieldConfig}"))?.Value;
            var dynamicFieldData = settings.FirstOrDefault(x => x.Name.Contains($"{PaymentProcessorName}.{PaymentConsts.DynamicFieldData}"))?.Value;

            var data = new PaymentProcessorInfo
            {
                SystemName = PaymentProcessorName,
                FriendlyName = friendlyName ?? PaymentConsts.DefaultFriendlyName(PaymentProcessorName),
                Logo = logo != null ? logo : PaymentConsts.DefaultLogoSetting,
                Refund = !string.IsNullOrEmpty(refund) ? refund == true.ToString() : PaymentConsts.DefaultRefundSetting,
                PartialRefund = !string.IsNullOrEmpty(partialRefund) ? partialRefund == true.ToString() : PaymentConsts.DefaultRefundSetting,
                Active = !string.IsNullOrEmpty(active) ? active == true.ToString() : PaymentConsts.DefaultRefundSetting,
                DisplayOrder = !string.IsNullOrEmpty(displayOrder) ? int.Parse(displayOrder) : PaymentConsts.DefaultDisplayOrderSetting,
                RecurringSupport =  !string.IsNullOrEmpty(recurringSupport) ? (RecurringPaymentType)Enum.Parse(typeof(RecurringPaymentType), recurringSupport) : PaymentConsts.DefaultRecurringSupportSetting,
                SupportsCapture = !string.IsNullOrEmpty(supportsCapture) ? supportsCapture == true.ToString() : PaymentConsts.DefaultRefundSetting,
                Void = !string.IsNullOrEmpty(voidSetting) ? voidSetting == true.ToString() : PaymentConsts.DefaultRefundSetting
            };


            try
            {
                data.DynamicFieldConfig = !string.IsNullOrWhiteSpace(dynamicFieldConfig)
                    ? (dynamic)JArray.Parse(dynamicFieldConfig)
                    : null;
            }
            catch
            {
                // Ignore
            }

            try
            {
                data.DynamicFieldData = !string.IsNullOrWhiteSpace(dynamicFieldData)
                    ? (dynamic)JObject.Parse(dynamicFieldData)
                    : null;
            }
            catch
            {
                // Ignore
            }
            

           

            return data;
        }

        public virtual async Task UpdateSetting(PaymentProcessorInfo input)
        {
            var settings = await _settingRepository.GetAll()
               .Where(x => x.Name.Contains(PaymentProcessorName))
               .ToListAsync();

            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.FriendlyNameSetting}", input.FriendlyName);
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.LogoSetting}", input.Logo);
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.RefundSetting}", input.Refund.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.PartialRefundSetting}", input.PartialRefund.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.ActiveSetting}", input.Active.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.DisplayOrderSetting}", input.DisplayOrder.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.RecurringSupportSetting}", input.RecurringSupport.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.SupportsCaptureSetting}", input.SupportsCapture.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.VoidSetting}", input.Void.ToString());
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.DynamicFieldConfig}", !string.IsNullOrWhiteSpace(input.DynamicFieldConfig) ? input.DynamicFieldConfig.ToString() : null);
            await InsertOrEdit(settings, $"{PaymentConsts.PrefixSettingPayment}.{PaymentProcessorName}.{PaymentConsts.DynamicFieldData}", !string.IsNullOrWhiteSpace(input.DynamicFieldData) ? input.DynamicFieldData.ToString() : null);
        }

        private async Task InsertOrEdit(List<Setting> listSettings, string key, string value)
        {
            var setting = listSettings.FirstOrDefault(x => x.Name == key);

            if (setting == null)
            {
                setting = new Setting(_abpSession.TenantId, null, key, value);
                await _settingRepository.InsertAsync(setting);
            }    
            else
            {
                setting.Value = value;
                await _settingRepository.UpdateAsync(setting);
            }    

        }
    }
}

﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class AdyenConfiguration
    {
        public string ApiKey { get; set; }

        public string MerchantAccount { get; set; }

        public Adyen.Model.Enum.Environment Environment { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class ApplePayConfiguration
    {
        public string StoreName { get; set; }

        public string Domain { get; set; }

        public bool UseCertificateStore { get; set; }

        public string MerchantCertificateFileName { get; set; }

        public string MerchantCertificatePassword { get; set; }

        public string MerchantCertificateThumbprint { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class HitPayConfiguration
    {
        public string ApiKey { get; set; }

        public PaypalEnvironmentEnum Environment { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class StripeConfiguration
    {
        public string SecretKey { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class GooglePayConfiguration
    {
        public string MerchantId { get; set; }

        /// <summary>
        ///     'TEST' or 'PRODUCTION'
        /// </summary>
        public string Environment { get; set; }
        
        public string PublicKey { get; set; }
        
        public string PrivateKey { get; set; }
    }
}
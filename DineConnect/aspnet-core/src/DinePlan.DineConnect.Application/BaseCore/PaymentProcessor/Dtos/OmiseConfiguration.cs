﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class OmiseConfiguration
    {
        public string PublishKey { get; set; }

        public string SecretKey { get; set; }
    }
}
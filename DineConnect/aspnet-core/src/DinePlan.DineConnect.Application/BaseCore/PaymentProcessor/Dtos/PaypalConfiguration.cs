﻿namespace DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos
{
    public class PaypalConfiguration
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public PaypalEnvironmentEnum Environment { get; set; }
    }

    public enum PaypalEnvironmentEnum
    {
        Sandbox,
        Live
    }
}
﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto;
using Flurl.Http;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class HitPayPaymentProcessor : BasePaymentProcessor, IHitPayPaymentProcessor, ITransientDependency
    {
        public HitPayPaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(settingRepository, session, PaymentConsts.HitPayPaymentSystemName)
        {
        }

        public async Task<HitPayCreatePaymentResponseDto> CreatePaymentRequest(HitPayPaymentRequestDto input)
        {
            var paymentResponse = new HitPayCreatePaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                HitPayConfiguration config =
                    JsonConvert.DeserializeObject<HitPayConfiguration>(settings.DynamicFieldData?.ToString());

                var baseUrl = GetBaseUrlByEnvironment(config);
                var apiToken = config.ApiKey;

                var url = $"{baseUrl}/payment-requests";
                var request = new
                {
                    amount = input.Amount,
                    email = input.Email,
                    currency = input.Currency,
                    webhook = input.Webhook
                };

                paymentResponse = await url
                    .WithHeader("X-BUSINESS-API-KEY", apiToken)
                    .WithHeader("X-Requested-With", "XMLHttpRequest")
                    .WithHeader("Content-Type", "application/x-www-form-urlencoded")
                    .SetQueryParams(request)
                    .PostUrlEncodedAsync(request)
                    .ReceiveJson<HitPayCreatePaymentResponseDto>();
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }

        public async Task<HitPayPaymentStatusResponse> ProcessPayment(HitPayGetPaymentRequestDto input)
        {
            var response = new HitPayPaymentStatusResponse();
            try
            {
                var settings = await GetSetting();

                HitPayConfiguration config =
                    JsonConvert.DeserializeObject<HitPayConfiguration>(settings.DynamicFieldData?.ToString());

                var baseUrl = GetBaseUrlByEnvironment(config);
                var apiToken = config.ApiKey;

                var url = $"{baseUrl}/payment-requests/{input.PaymentRequestId}";

                response = await url
                   .WithHeader("X-BUSINESS-API-KEY", apiToken)
                   .GetAsync()
                   .ReceiveJson<HitPayPaymentStatusResponse>();

                response.IsSuccess = response.Status == HitPayPaymentStatus.Completed;
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        private string GetBaseUrlByEnvironment(HitPayConfiguration configuration)
        {
            switch (configuration.Environment)
            {
                case PaypalEnvironmentEnum.Sandbox:
                    {
                        return "https://api.sandbox.hit-pay.com/v1";
                    }
                case PaypalEnvironmentEnum.Live:
                    {
                        return "https://api.hit-pay.com/v1/";
                    }
                default:
                    {
                        throw new ApplicationException("Unknown HitPay environment");
                    }
            }
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Adyen.Model.Checkout;
using Adyen.Service;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class AdyenPaymentProcessor : BasePaymentProcessor, IAdyenPaymentProcessor, ITransientDependency
    {
        public AdyenPaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(
            settingRepository, session, PaymentConsts.AdyenPaymentSystemName)
        {
        }

        public async Task<AdyenPaymentResponseDto> ProcessPayment(AdyenPaymentRequestDto input)
        {
            var paymentResponse = new AdyenPaymentResponseDto();

            try
            {
                var settings = await GetSetting();

                AdyenConfiguration config = JsonConvert.DeserializeObject<AdyenConfiguration>(settings.DynamicFieldData.ToString());

                var client = new Adyen.Client(config.ApiKey, config.Environment);

                var checkout = new Checkout(client);

                var details = new DefaultPaymentMethodDetails
                {
                    Type = "scheme",
                    EncryptedCardNumber = input.EncryptedCardNumber,
                    EncryptedExpiryMonth = input.EncryptedExpiryMonth,
                    EncryptedExpiryYear = input.EncryptedExpiryYear,
                    EncryptedSecurityCode = input.EncryptedSecurityCode
                };

                // Create a paymentsRequest
                var paymentAmount = new Amount(input.Currency, input.Amount);

                var paymentsRequest = new PaymentRequest
                {
                    Amount = paymentAmount,
                    ReturnUrl = input.ReturnUrl,
                    Reference = Guid.NewGuid().ToString("N"),
                    MerchantAccount = config.MerchantAccount,
                    PaymentMethod = details
                };


                var result = await checkout.PaymentsAsync(paymentsRequest);

                paymentResponse.IsSuccess = result.ResultCode == PaymentsResponse.ResultCodeEnum.Authorised;

                paymentResponse.PaymentId = result.PspReference;

                if (!paymentResponse.IsSuccess)
                {
                    paymentResponse.Message = result.RefusalReason;
                }
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }
    }
}
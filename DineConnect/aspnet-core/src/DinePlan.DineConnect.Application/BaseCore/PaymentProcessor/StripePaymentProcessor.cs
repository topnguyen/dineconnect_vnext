﻿using System;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;
using Newtonsoft.Json;
using Stripe;
using StripeConfiguration = DinePlan.DineConnect.BaseCore.PaymentProcessor.Dtos.StripeConfiguration;

namespace DinePlan.DineConnect.BaseCore.PaymentProcessor
{
    public class StripePaymentProcessor : BasePaymentProcessor, IStripePaymentProcessor, ITransientDependency
    {
        public StripePaymentProcessor(IRepository<Setting, long> settingRepository, IAbpSession session) : base(
            settingRepository, session, PaymentConsts.StripePaymentSystemName)
        {
        }

        public async Task<StripePaymentResponseDto> ProcessPayment(StripePaymentRequestDto input)
        {
            var paymentResponse = new StripePaymentResponseDto();
            try
            {
                var settings = await GetSetting();

                StripeConfiguration config = JsonConvert.DeserializeObject<StripeConfiguration>(settings.DynamicFieldData.ToString());

                var client = new StripeClient(config.SecretKey);

                var chargeRequest = new ChargeCreateOptions
                {
                    Amount = input.Amount,
                    Currency = input.Currency,
                    Source = input.StripeToken
                };

                var chargeService = new ChargeService(client);

                var result = await chargeService.CreateAsync(chargeRequest);

                paymentResponse.IsSuccess = result.Status == "succeeded";

                paymentResponse.PaymentId = result.Id;
            }
            catch (Exception e)
            {
                paymentResponse.Message = e.Message;
            }

            return paymentResponse;
        }
    }
}
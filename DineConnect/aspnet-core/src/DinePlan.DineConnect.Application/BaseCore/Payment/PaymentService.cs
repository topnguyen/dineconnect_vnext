﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using DinePlan.DineConnect.BaseCore.PaymentProcessor.Base;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using Microsoft.Extensions.DependencyInjection;

namespace DinePlan.DineConnect.BaseCore.Payment
{
    public class PaymentService : IPaymentService, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public PaymentService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<List<IPaymentProcessor>> GetAllPaymentProcessor()
        {
            var paymentProcessors = GetListPaymentProcessors();

            return await Task.FromResult(paymentProcessors);
        }

        public async Task<IPaymentProcessor> GetPaymentProcessorBySystemName(string systemName)
        {
            var paymentProcessor = GetListPaymentProcessors()
                .FirstOrDefault(x => x.PaymentProcessorName == systemName);

            return await Task.FromResult(paymentProcessor);
        }

        private List<IPaymentProcessor> GetListPaymentProcessors()
        {
            var paymentProcessors = System.Reflection.Assembly.GetAssembly(typeof(BasePaymentProcessor))
               .GetTypes()
               .Where(t => typeof(IPaymentProcessor).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
               .Select(t => ActivatorUtilities.CreateInstance(_serviceProvider, t) as IPaymentProcessor)
               .ToList();

            return paymentProcessors;
        }
    }
}

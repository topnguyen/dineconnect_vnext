﻿using System.Threading.Tasks;

namespace DinePlan.DineConnect.BaseCore.ScheduleEmail
{
    public interface IScheduleEmailAppService
    {
        Task Emailing();
    }
}

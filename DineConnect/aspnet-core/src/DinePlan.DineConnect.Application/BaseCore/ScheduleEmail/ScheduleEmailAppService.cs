﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Tiffins;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.ScheduleEmail
{
    public class ScheduleEmailAppService : DineConnectAppServiceBase, IScheduleEmailAppService
    {
        private readonly ITenantEmailSendAppService _emailAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepository;
        private readonly IConfigurationRoot _appConfiguration;

        public ScheduleEmailAppService(IAppConfigurationAccessor configurationAccessor,
            ITenantEmailSendAppService emailAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<TiffinCustomerProductOffer> customerProductOfferRepository)
        {
            _appConfiguration = configurationAccessor.Configuration;
            _emailAppService = emailAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _customerProductOfferRepository = customerProductOfferRepository;
        }

        public async Task Emailing()
        {
            var allTenants = TenantManager.GetAllTenants();
            foreach (var allTenant in allTenants)
            {
                if (!await FeatureChecker.IsEnabledAsync( allTenant.Id,AppFeatures.Tiffins))
                {
                    continue;
                }
                using (_unitOfWorkManager.Current.SetTenantId(allTenant.Id))
                {
                    var minExpiryDayJson =
                        await SettingManager.GetSettingValueForTenantAsync(AppSettings.MealSetting.MinExpiryDay, allTenant.Id);

                    int? minExpiryDay = null;
                    if (!minExpiryDayJson.IsNullOrEmpty() && minExpiryDayJson != "null")
                    {
                        minExpiryDay = JsonConvert.DeserializeObject<int>(minExpiryDayJson);
                    }
                   
                    if (minExpiryDay.HasValue)
                    {
                
                        var customerOffer = await _customerProductOfferRepository.GetAll()
                            .Include(t => t.Customer)
                            .ThenInclude(t => t.CustomerAddressDetails)
                            .Where(c => (int)(DateTime.Now.Date - c.ExpiryDate.Date).TotalDays < minExpiryDay.Value)
                            .Distinct()
                            .ToListAsync();

                        foreach (var item in customerOffer)
                        {
                            var sendEmail = new SendEmailInput
                            {
                                TenantId = allTenant.Id,
                                EmailAddress = item.Customer.EmailAddress,
                                Variables = new
                                {
                                    First_name = item.Customer.Name,
                                    ExpiryDate = minExpiryDay.ToString(),
                                    AvailableBalance = item.BalanceCredit.HasValue?item.BalanceCredit.Value.ToString():"0"

                                },
                                Module = "MinExpiryDay"
                            };
                            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
                        }
                    }
                }
            }
            
        }
    }
}
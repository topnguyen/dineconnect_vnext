﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.BaseCore.TemplateEngines.Exporting;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos;

namespace DinePlan.DineConnect.Core.TemplateEngines.Exporting
{
    public class TemplateEnginesExcelExporter : NpoiExcelExporterBase, ITemplateEnginesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TemplateEnginesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTemplateEngineForViewDto> templateEngines)
        {
            return CreateExcelPackage(
                "TemplateEngines.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TemplateEngines"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Feature"),
                        L("Module"),
                        L("Variables")
                        );

                    AddObjects(
                        sheet, 2, templateEngines,
                        _ => _.TemplateEngine.Name,
                        _ => _.TemplateEngine.Feature,
                        _ => _.TemplateEngine.Module,
                        _ => _.TemplateEngine.Variables
                        );
                });
        }
    }
}
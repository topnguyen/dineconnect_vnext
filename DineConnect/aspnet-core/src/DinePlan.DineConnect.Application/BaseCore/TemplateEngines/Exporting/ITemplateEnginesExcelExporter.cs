﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos;

namespace DinePlan.DineConnect.BaseCore.TemplateEngines.Exporting
{
    public interface ITemplateEnginesExcelExporter
    {
        FileDto ExportToFile(List<GetTemplateEngineForViewDto> templateEngines);
    }
}
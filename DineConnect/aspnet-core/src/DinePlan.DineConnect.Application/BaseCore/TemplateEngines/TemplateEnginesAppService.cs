﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.TemplateEngines.Exporting;
using DinePlan.DineConnect.Core.TemplateEngines;
using DinePlan.DineConnect.Core.TemplateEngines.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.BaseCore.TemplateEngines
{
    
    public class TemplateEnginesAppService : DineConnectAppServiceBase, ITemplateEnginesAppService
    {
        private readonly IRepository<TemplateEngine> _templateEngineRepository;
        private readonly ITemplateEnginesExcelExporter _templateEnginesExcelExporter;

        public TemplateEnginesAppService(IRepository<TemplateEngine> templateEngineRepository, ITemplateEnginesExcelExporter templateEnginesExcelExporter)
        {
            _templateEngineRepository = templateEngineRepository;
            _templateEnginesExcelExporter = templateEnginesExcelExporter;
        }

        public async Task<PagedResultDto<GetTemplateEngineForViewDto>> GetAll(GetAllTemplateEnginesInput input)
        {
            var filteredTemplateEngines = _templateEngineRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Feature, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Module, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Variables, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Template, input.Filter.ToILikeString()));

            var pagedAndFilteredTemplateEngines = filteredTemplateEngines
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var templateEngines = pagedAndFilteredTemplateEngines
                                  .Select(o => new GetTemplateEngineForViewDto()
                                  {
                                      TemplateEngine = ObjectMapper.Map<TemplateEngineDto>(o)
                                  });

            var totalCount = await filteredTemplateEngines.CountAsync();

            return new PagedResultDto<GetTemplateEngineForViewDto>(
                totalCount,
                await templateEngines.ToListAsync()
            );
        }

        
        public async Task<GetTemplateEngineForEditOutput> GetTemplateEngineForEdit(NullableIdDto input)
        {
            var output = new GetTemplateEngineForEditOutput();
            if (input.Id.HasValue)
            {
                var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(input.Id.Value);

                output = new GetTemplateEngineForEditOutput { TemplateEngine = ObjectMapper.Map<CreateOrEditTemplateEngineDto>(templateEngine) };
            }
            return output;
        }

        
        public async Task<CreateOrEditTemplateEngineDto> GetTemplateEngineByTypeForEdit(string input)
        {
            var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync(e => e.Module == input);

            if (templateEngine == null)
            {
                throw new UserFriendlyException(L("TemplateTypeNotExists", input));
            }

            var output = ObjectMapper.Map<CreateOrEditTemplateEngineDto>(templateEngine);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTemplateEngineDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTemplateEngineDto input)
        {
            var templateEngine = ObjectMapper.Map<TemplateEngine>(input);

            if (AbpSession.TenantId != null)
            {
                templateEngine.TenantId = (int)AbpSession.TenantId;
            }

            await _templateEngineRepository.InsertAsync(templateEngine);
        }

        
        protected virtual async Task Update(CreateOrEditTemplateEngineDto input)
        {
            var templateEngine = await _templateEngineRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, templateEngine);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _templateEngineRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetTemplateEnginesToExcel(GetAllTemplateEnginesForExcelInput input)
        {
            var filteredTemplateEngines = _templateEngineRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Feature, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Module, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Variables, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Template, input.Filter.ToILikeString()));

            var query = filteredTemplateEngines
                                  .Select(o => new GetTemplateEngineForViewDto()
                                  {
                                      TemplateEngine = ObjectMapper.Map<TemplateEngineDto>(o)
                                  });

            var templateEngineListDtos = await query.ToListAsync();

            return _templateEnginesExcelExporter.ExportToFile(templateEngineListDtos);
        }

        public ListResultDto<NameValueDto> GetTemplateVariables()
        {
            var returnList = new List<NameValueDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(TemplateVariable)))
            {
                returnList.Add(new NameValueDto()
                {
                    Name = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<NameValueDto>(returnList);
        }

        public ListResultDto<ComboboxItemDto> GetTemplateTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(TemplateType)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = name
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        private void ValidateForCreateOrUpdate(CreateOrEditTemplateEngineDto input)
        {
            var exists = _templateEngineRepository.GetAll()
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id)
                .Where(e => e.Module == input.Module);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("TemplateTypeAlreadyExists", input.Module));
            }
        }
    }
}
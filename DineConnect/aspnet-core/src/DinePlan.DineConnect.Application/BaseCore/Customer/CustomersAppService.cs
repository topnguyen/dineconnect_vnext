﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.BaseCore.Customer.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using DinePlan.DineConnect.Wheel.V2;
using Elect.Core.StringUtils;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.BaseCore.Customer
{
    public class CustomersAppService : DineConnectAppServiceBase, ICustomerAppService
    {
        private readonly IRepository<CustomerAddressDetail> _customerAddressRepository;
        private readonly CustomerExcelExporter _customerExcelExporter;
        private readonly CustomerManager _customerManager;
        private readonly IRepository<Customer> _customerRepository;
        private readonly ITenantEmailSendAppService _emailConfigurationAppService;
        private readonly IRepository<User, long> _userRepository;

        public CustomersAppService(
            IRepository<Customer> customerRepository,
            IRepository<CustomerAddressDetail> customerAddressRepository,
            CustomerExcelExporter customerExcelExporter,
            ITenantEmailSendAppService emailConfigurationAppService,
            IRepository<User, long> userRepository
            , CustomerManager customerManager)
        {
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _customerExcelExporter = customerExcelExporter;
            _emailConfigurationAppService = emailConfigurationAppService;
            _userRepository = userRepository;
            _customerManager = customerManager;
        }

        public async Task<PagedResultDto<CustomerListDto>> GetAll(GetAllCustomerInputDto input)
        {
            var list = _customerRepository.GetAll()
                .Include(x => x.User)
                .Include(x => x.CustomerAddressDetails)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.PhoneNumber.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter));

            var totalCount = await list.CountAsync();
            var result = await list.OrderBy(input.Sorting).PageBy(input).ToListAsync();
            var customers = ObjectMapper.Map<List<CustomerListDto>>(result);
            foreach (var item in customers) item.LastLoginDate = item.CreationTime;
            return new PagedResultDto<CustomerListDto>(totalCount, customers);
        }

        public async Task CreateOrEdit(CreateCustomerDto input)
        {
            if (input.Id > 0)
                await Update(input);
            else
                await Create(input);
        }

        public async Task Delete(int id)
        {
            var user = await _customerRepository.FirstOrDefaultAsync(id);
            if (user.UserId.HasValue) await _userRepository.DeleteAsync(user.UserId.Value);
            await _customerRepository.DeleteAsync(id);
        }

        public async Task<CreateCustomerDto> GetCustomerForEdit(NullableIdDto input)
        {
            if (input.Id.HasValue)
            {
                var customer = await _customerRepository
                    .GetAll()
                    .Include(c => c.CustomerAddressDetails)
                    .FirstOrDefaultAsync(i => i.Id == input.Id.Value);
                return ObjectMapper.Map<CreateCustomerDto>(customer);
            }

            return new CreateCustomerDto();
        }

        public async Task<CreateCustomerDto> GetCustomerForEditByUserId(NullableIdDto input)
        {
            if (input.Id.HasValue)
            {
                var user = await _userRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

                var customer = await _customerRepository
                    .GetAll()
                    .Include(c => c.CustomerAddressDetails)
                    .FirstOrDefaultAsync(i => i.UserId == input.Id);

                var result = ObjectMapper.Map<CreateCustomerDto>(customer);
                result.PhoneNumber = user.PhoneNumber;
                result.EmailAddress = user.EmailAddress;
                result.Name = user.Surname;

                return result;
            }

            return new CreateCustomerDto();
        }

        public async Task<FileDto> GetCustomersToExcel(GetAllCustomerInputDto input)
        {
            var list = await GetAll(input);
            var dto = list.Items.ToList();
            return await _customerExcelExporter.ExportToFile(dto);
        }

        public async Task UpdateLastLoginTime(long userId)
        {
            var customer = await _customerRepository.FirstOrDefaultAsync(e => e.UserId == userId);

            if (customer != null)
            {
                customer.LastLoginTime = Clock.Now;

                await _customerRepository.UpdateAsync(customer);

                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<CreateCustomerDto> GetCustomerWithUserId(int userId)
        {
            var customer = await _customerRepository
                .FirstOrDefaultAsync(e => e.UserId == userId);
            return ObjectMapper.Map<CreateCustomerDto>(customer);
        }

        [AbpAllowAnonymous]
        public async Task<int> UpdateUserAndCreateOrEditCustomer(CreateCustomerDto input)
        {
            var user = await _userRepository.FirstOrDefaultAsync(x => x.Id == input.UserId);
            if (user != null)
            {
                user.PhoneNumber = input.PhoneNumber;
                user.EmailAddress = input.EmailAddress;
                user.UserName = input.EmailAddress;
                user.Surname = input.Name;
                var customerId = await CreateOrEditCustomer(input);
                return customerId;
            }

            return 0;
        }

        private async Task Create(CreateCustomerDto input)
        {
            input.TenantId = AbpSession.TenantId;
            var customer = ObjectMapper.Map<Customer>(input);
            var customerId = await _customerRepository.InsertAndGetIdAsync(customer);
            var customerAddress = ObjectMapper.Map<CustomerAddressDetail>(input);
            customerAddress.IsDefault = true;
            customerAddress.CustomerId = customerId;

            await _customerAddressRepository.InsertAsync(customerAddress);

            await CurrentUnitOfWork.SaveChangesAsync();

            var sendEmail = new SendEmailInput
            {
                TenantId = input.TenantId,
                EmailAddress = input.EmailAddress,
                Module = "Register",
                Variables = new
                {
                    First_name = customer.Name,
                    Registered_date = Clock.Now.ToString("dd/MM/yyyy")
                }
            };
            await _emailConfigurationAppService.SendEmailByTemplateAsync(sendEmail);
        }

        private async Task Update(CreateCustomerDto input)
        {
            var customer = await _customerRepository
                .GetAll()
                .Include(c => c.CustomerAddressDetails)
                .FirstOrDefaultAsync(c => c.Id == input.Id);
            ObjectMapper.Map(input, customer);
            await _customerRepository.UpdateAsync(customer);

            var customerAddress = customer.CustomerAddressDetails
                .FirstOrDefault(t => t.IsDefault);
            if (customerAddress == null)
            {
                customerAddress = new CustomerAddressDetail
                {
                    PhoneNumber = input.PhoneNumber,
                    Address1 = input.Address1,
                    Address2 = input.Address2,
                    Address3 = input.Address3,
                    CityId = input.CityId,
                    Zone = input.Zone,
                    PostcalCode = input.PostcalCode,
                    CustomerId = input.Id,
                    IsDefault = true
                };

                await _customerAddressRepository.InsertAsync(customerAddress);
            }
            else
            {
                customerAddress.PhoneNumber = input.PhoneNumber;
                customerAddress.Address1 = input.Address1;
                customerAddress.Address2 = input.Address2;
                customerAddress.Address3 = input.Address3;
                customerAddress.CityId = input.CityId;
                customerAddress.Zone = input.Zone;
                customerAddress.PostcalCode = input.PostcalCode;

                await _customerAddressRepository.UpdateAsync(customerAddress);
            }
        }

        public async Task<CreateCustomerDto> GetCurrentCustomerForEdit()
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();
            return ObjectMapper.Map<CreateCustomerDto>(customer);
        }

        private async Task<int> CreateOrEditCustomer(CreateCustomerDto input)
        {
            if (input.Id > 0)
            {
                await Update(input);
                return input.Id;
            }

            var customerId = await CreateCustomer(input);
            return customerId;
        }

        private async Task<int> CreateCustomer(CreateCustomerDto input)
        {
            input.TenantId = AbpSession.TenantId;

            var customer = ObjectMapper.Map<Customer>(input);

            var customerId = await _customerRepository.InsertAndGetIdAsync(customer);

            var customerAddress = ObjectMapper.Map<CustomerAddressDetail>(input);

            customerAddress.IsDefault = true;
            customerAddress.CustomerId = customerId;

            await _customerAddressRepository.InsertAsync(customerAddress);

            await CurrentUnitOfWork.SaveChangesAsync();
            return customerId;
        }
    }
}
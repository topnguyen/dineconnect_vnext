﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Dto;

namespace DinePlan.DineConnect.BaseCore.Customer.Exporting
{
    public interface ICustomerExcelExporter
    {
        Task<FileDto> ExportToFile(List<CustomerListDto> customers);

    }
}

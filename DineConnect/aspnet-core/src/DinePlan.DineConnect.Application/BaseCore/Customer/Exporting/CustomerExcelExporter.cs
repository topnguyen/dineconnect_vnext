﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.Customer.Exporting
{
    public class CustomerExcelExporter : NpoiExcelExporterBase, ICustomerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public CustomerExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<CustomerListDto> customers)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;

            return CreateExcelPackage(
                "Customers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Customers"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("RegisteredDate"),
                        L("LastLoginDate"),
                        L("ContactEmail"),
                        L("ContactPhone"),
                        L("Grade"),
                        L("Section"),
                        L("AcceptReceivePromotionEmail"),
                        L("Address")
                        );

                    AddObjects(
                        sheet, 2, customers,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateTimeSetting),
                        _ => _timeZoneConverter.Convert(_.LastLoginDate, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateTimeSetting),
                        _ => _.EmailAddress,
                        _ => _.PhoneNumber,
                        _ => GetGrade(_.JsonData),
                        _ => GetSection(_.JsonData),
                        _ => _.AcceptReceivePromotionEmail,
                        _ => GetAddress(_)
                        );

                    sheet.AutoSizeColumn(4);
                    sheet.AutoSizeColumn(5);

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                });
        }

        private string GetGrade(string jsonData)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<CustomerJsonData>(jsonData);
                return data?.grade;
            }
            catch
            {
                return null;
            }
        }

        private string GetSection(string jsonData)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<CustomerJsonData>(jsonData);
                return data?.section;
            }
            catch
            {
                return null;
            }
        }

        public string GetAddress(CustomerListDto customer)
        {
            var addressess = customer.CustomerAddressDetails.Select(x => $"{x.Address1}, {x.Address2}, {x.Address3}");
            return string.Join(Environment.NewLine, addressess);
        }

        private class CustomerJsonData
        {
            public string grade { get; set; }

            public string cardID { get; set; }

            public string section { get; set; }
        }
    }
}

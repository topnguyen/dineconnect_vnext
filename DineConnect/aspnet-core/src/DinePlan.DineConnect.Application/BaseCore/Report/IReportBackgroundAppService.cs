﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Report.Dtos;

namespace DinePlan.DineConnect.BaseCore.Report
{
    public interface IReportBackgroundAppService: IApplicationService
    {
        Task<PagedResultDto<BackgroundReportOutput>> GetAll(BackgroundReportInput tenant);
        Task<int> CreateOrUpdate(CreateBackgroundReportInput tenant);
        Task<FileDto> GetFileDto(int id);
        Task UpdateFileOutputCompleted(FileDto fileDto, int argsBackGroundId);
    }
}

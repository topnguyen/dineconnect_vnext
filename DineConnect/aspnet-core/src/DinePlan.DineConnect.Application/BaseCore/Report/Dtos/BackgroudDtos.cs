﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report;
using System;
using System.Collections.Generic;
using System.Text;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Report.Dtos
{
    public class BackgroundReportInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public List<ComboboxItemDto> Reports { get; set; }

        public string ReportName { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id desc";
            }
        }
    }

    [AutoMapFrom(typeof(ReportBackground))]
    public class BackgroundReportOutput
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime LastModificationTime { get; set; }
        public bool Completed { get; set; }
        public DateTime CreationTime { get; set; }

    }

    [AutoMapFrom(typeof(ReportBackground))]
    public class CreateBackgroundReportInput
    {
        public int Id { get; set; }
        public string ReportOutput { get; set; }
        public string ReportName { get; set; }
        public string ReportDescription { get; set; }
        public bool Completed { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }
}

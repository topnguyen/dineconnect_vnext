﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Core.QueryScheduler.Interfaces;
using DinePlan.DineConnect.Core.Report;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Report.Dtos;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.BaseCore.Report.Implementation
{
    public class ReportBackgroundAppService : DineConnectAppServiceBase, IReportBackgroundAppService
    {
        private IRepository<ReportBackground> _reportRepo;
        private readonly IQuerySchedularService<ReportBackground> _backGroundJobSchedular;

        public ReportBackgroundAppService(IRepository<ReportBackground> reportRepo, IQuerySchedularService<ReportBackground> backGroundJobSchedular)
        {
            _reportRepo = reportRepo;
            _backGroundJobSchedular = backGroundJobSchedular;
        }

        public async Task<PagedResultDto<BackgroundReportOutput>> GetAll(BackgroundReportInput input)
        {
            var userId = AbpSession.UserId;
            if (userId == null)
                return null;
            var allItems = _reportRepo
                .GetAll();

            allItems = allItems.Where(a => a.UserId == userId);

            if (input.Reports != null && input.Reports.Any())
            {
                var allReports = input.Reports.Select(a => a.Value).ToList();
                allItems = allItems.Where(a => allReports.Contains(a.ReportName));
            }
            var sortMenuItems = allItems
              .OrderBy(input.Sorting)
              .PageBy(input);
            var allListDtos = sortMenuItems.ToList().MapTo<List<BackgroundReportOutput>>();

            //_backGroundJobSchedular.ExcuteQueryJob("karim.azab@hotmail.com", "Subject 1", "Hello", false, "Driver report", sortMenuItems, "Server=01T-TECH-KARIM;Initial Catalog=DineConnect_vNext;Trusted_Connection=True;MultipleActiveResultSets=true;");

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<BackgroundReportOutput>(
                allItemCount,
                allListDtos
                );

        }

        public async Task<int> CreateOrUpdate(CreateBackgroundReportInput input)
        {
            int returnOutput = 0;

            if (input.Id > 0)
            {
                await UpdateReportBackground(input);
                returnOutput = input.Id;
            }
            else
            {
                returnOutput = await CreateReportBackground(input);
            }
            return returnOutput;
        }

        public async Task<FileDto> GetFileDto(int id)
        {
            if (id > 0)
            {
                var myRepo = await _reportRepo.GetAsync(id);
                if (!string.IsNullOrEmpty(myRepo?.ReportOutput))
                {
                    FileDto myFile = JsonConvert.DeserializeObject<FileDto>(myRepo.ReportOutput);
                    return myFile;
                }
            }
            return null;
        }

        public async Task UpdateFileOutputCompleted(FileDto fileDto, int argsBackGroundId)
        {
            var myBack = await _reportRepo.GetAsync(argsBackGroundId);
            if (myBack != null)
            {
                myBack.Completed = true;
                myBack.ReportOutput = JsonConvert.SerializeObject(fileDto);
            }
        }

        private async Task UpdateReportBackground(CreateBackgroundReportInput input)
        {
            var item = await _reportRepo.GetAsync(input.Id);
            if (item != null)
            {
                item.ReportOutput = input.ReportOutput;
                item.ReportName = input.ReportName;
                item.Completed = input.Completed;
            }
        }

        private async Task<int> CreateReportBackground(CreateBackgroundReportInput input)
        {
            int returnValue = 0;
            try
            {
                ReportBackground background = new ReportBackground
                {
                    ReportOutput = input.ReportOutput,
                    ReportName = input.ReportName,
                    Completed = input.Completed,
                    TenantId = input.TenantId,
                    UserId = input.UserId,
                    ReportDescription = input.ReportDescription
                };
                returnValue = await _reportRepo.InsertAndGetIdAsync(background);

                var totalCompletedRecords =
                     _reportRepo.GetAll()
                        .Where(a => a.TenantId == input.TenantId && a.UserId == input.UserId && a.Completed == true);
                int myReportCount = 15;

                if (totalCompletedRecords.Count() > myReportCount)
                {
                    var counter = 1;
                    List<int> allCompletedIds = new List<int>();
                    foreach (var my in totalCompletedRecords)
                    {
                        allCompletedIds.Add(my.Id);
                        counter++;
                        if (counter >= myReportCount)
                        {
                            break;
                        }
                    }
                    foreach (var delRecord in allCompletedIds)
                    {
                        _reportRepo.Delete(delRecord);
                    }
                }
                return returnValue;
            }
            catch (Exception exce)
            {
                var mess = exce.Message;
            }
            return returnValue;

        }
    }
}

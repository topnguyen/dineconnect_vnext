﻿using System.Threading.Tasks;
using Abp;
using Abp.Dependency;
using Abp.Notifications;

namespace DinePlan.DineConnect.BaseCore.Report.Implementation
{
    public class ReportNotificationAppService : DineConnectAppServiceBase, ITransientDependency
    {
        private readonly INotificationPublisher _notificationPublisher;

        public ReportNotificationAppService(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }

        public async Task Publish_SentReportNotificationRequest(UserIdentifier targetUserId)
        {
            await _notificationPublisher.PublishAsync("SentReportNotificationRequest", new MessageNotificationData(L("ExportReportSuccessful")), userIds: new[] { targetUserId });
        }

    }
}

﻿using System.Collections.Generic;
using DinePlan.DineConnect.Authorization.Users.Importing.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}

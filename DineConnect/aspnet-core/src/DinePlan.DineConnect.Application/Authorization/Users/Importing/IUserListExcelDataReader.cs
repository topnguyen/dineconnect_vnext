﻿using System.Collections.Generic;
using DinePlan.DineConnect.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace DinePlan.DineConnect.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}

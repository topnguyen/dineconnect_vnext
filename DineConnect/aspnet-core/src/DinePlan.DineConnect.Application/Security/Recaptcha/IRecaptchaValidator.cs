using System.Threading.Tasks;

namespace DinePlan.DineConnect.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}
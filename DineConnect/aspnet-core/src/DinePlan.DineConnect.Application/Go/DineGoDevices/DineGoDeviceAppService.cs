﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.DineGoDevices.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Go.DineGoDevices
{
    public class DineGoDeviceAppService : DineConnectAppServiceBase, IDineGoDeviceAppService
    {
        private readonly IRepository<DineGoDevice> _dineGoDeviceRepo;
        private readonly IRepository<DineGoBrand> _dineGoBrandRepo;
        private readonly IRepository<DineGoBrandDevice> _dineGoBrandDeviceRepo;
        private readonly IRepository<DineGoPaymentType> _dineGoPaymentTypeRepo;
        private readonly IRepository<DineGoPaymentTypesDevice> _dineGoPaymentTypeDeviceRepo;
        private readonly IDineGoDeviceExporter _dineGoDeviceExporter;

        public DineGoDeviceAppService(
            IRepository<DineGoDevice> dineGoDeviceRepo, 
            IRepository<DineGoBrand> dineGoBrandRepo, 
            IRepository<DineGoBrandDevice> dineGoBrandDeviceRepo, 
            IRepository<DineGoPaymentType> dineGoPaymentTypeRepo, 
            IRepository<DineGoPaymentTypesDevice> dineGoPaymentTypeDeviceRepo, 
            IDineGoDeviceExporter dineGoDeviceExporter)
        {
            _dineGoDeviceRepo = dineGoDeviceRepo;
            _dineGoBrandRepo = dineGoBrandRepo;
            _dineGoBrandDeviceRepo = dineGoBrandDeviceRepo;
            _dineGoPaymentTypeRepo = dineGoPaymentTypeRepo;
            _dineGoPaymentTypeDeviceRepo = dineGoPaymentTypeDeviceRepo;
            _dineGoDeviceExporter = dineGoDeviceExporter;
        }

        public async Task<PagedResultDto<DineGoDeviceListDto>> GetAll(GetDineGoDeviceInput input)
        {
            var allItems = _dineGoDeviceRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineGoDeviceListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineGoDeviceListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoDeviceInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineGoDeviceListDto>>(allList.Items);

            return await _dineGoDeviceExporter.ExportToFile(allListDtos);
        }

        public async Task<DineGoDeviceDto> GetDineGoDeviceForEdit(EntityDto input)
        {
            DeviceThemeSettingsDto editDeviceThemeSettingDto = new DeviceThemeSettingsDto();

            var hDto = await _dineGoDeviceRepo
                .GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.DineGoBrands)
                .ThenInclude(x => x.DineGoBrand)
                .Include(x => x.DineGoPaymentTypes)
                .ThenInclude(x => x.DineGoPaymentType)
                .FirstOrDefaultAsync()
                ;

            var editDto = ObjectMapper.Map<DineGoDeviceEditDto>(hDto);

            if (editDto.ThemeSettings != null)
            {
                editDeviceThemeSettingDto = JsonConvert.DeserializeObject<DeviceThemeSettingsDto>(editDto.ThemeSettings);
            }


            return new DineGoDeviceDto
            {
                DineGoDevice = editDto,
                DeviceThemeSettings = editDeviceThemeSettingDto
            };
        }

        public async Task<EntityDto> CreateOrUpdateDineGoDevice(DineGoDeviceDto input)
        {
            EntityDto returnId;
            
            if (input.DineGoDevice.Id.HasValue)
            {
                returnId = await UpdateDineGoDevice(input);
            }
            else
            {
                returnId = await CreateDineGoDevice(input);
            }
            
            return returnId;
        }

        public async Task DeleteDineGoDevice(EntityDto input)
        {
            await _dineGoDeviceRepo.DeleteAsync(input.Id);
        }

        public async Task<List<DineGoBrandListDto>> GetDineGoBrandForCombobox()
        {
            var lstBrands = await _dineGoBrandRepo.GetAllListAsync(a => !a.IsDeleted);

            return ObjectMapper.Map<List<DineGoBrandListDto>>(lstBrands);
        }

        public async Task<List<DineGoPaymentTypeListDto>> GetDineGoPaymentTypeForCombobox()
        {
            var listPaymentType = await _dineGoPaymentTypeRepo.GetAllListAsync(a => !a.IsDeleted);

            return ObjectMapper.Map<List<DineGoPaymentTypeListDto>>(listPaymentType);
        }

        public async Task<List<ComboboxItemDto>> ApiGetAllDevices()
        {
            var allLists = await _dineGoDeviceRepo.GetAllListAsync();

            var allReturns = allLists.Select(a => new ComboboxItemDto
            {
                Value = a.Id.ToString(),
                DisplayText = a.Name
            }).ToList();

            return allReturns;
        }

        protected virtual async Task<EntityDto> CreateDineGoDevice(DineGoDeviceDto input)
        {
            var dto = new DineGoDevice
            {
                Name = input.DineGoDevice.Name,
                IdleTime = input.DineGoDevice.IdleTime,
                OrderLink = input.DineGoDevice.OrderLink,
                PinCode = input.DineGoDevice.PinCode,
                PushToConnect = input.DineGoDevice.PushToConnect
            };

            var retId = await _dineGoDeviceRepo.InsertAndGetIdAsync(dto);

            // Brands
            foreach (var dineGoBrand in input.DineGoDevice.DineGoBrands)
            {
                var brandEntity = await _dineGoBrandRepo.GetAsync(dineGoBrand.Id.Value);

                await _dineGoBrandDeviceRepo.InsertAndGetIdAsync(new DineGoBrandDevice
                {
                    DineGoBrandId = brandEntity.Id,
                    DineGoDeviceId = retId
                });
            }

            // Payment Types
            foreach (var dineGoPaymentType in input.DineGoDevice.DineGoPaymentTypes)
            {
                var paymentTypeEntity = await _dineGoPaymentTypeRepo.GetAsync(dineGoPaymentType.Id.Value);

                await _dineGoPaymentTypeDeviceRepo.InsertAndGetIdAsync(new DineGoPaymentTypesDevice
                {
                    DineGoPaymentTypeId = paymentTypeEntity.Id,
                    DineGoDeviceId = retId
                });
            }

            return new EntityDto { Id = retId };
        }

        protected virtual async Task<EntityDto> UpdateDineGoDevice(DineGoDeviceDto input)
        {
            var dto = input.DineGoDevice;

            var item = await _dineGoDeviceRepo.GetAsync(input.DineGoDevice.Id.Value);

            item.Name = dto.Name;
            item.OrderLink = dto.OrderLink;
            item.IdleTime = dto.IdleTime;
            item.PinCode = dto.PinCode;
            item.PushToConnect = dto.PushToConnect;

            if (input.DineGoDevice.RefreshImage)
            {
                item.ChangeGuid = Guid.NewGuid();
            }

            DeviceThemeSettingsDto dtoJson = new DeviceThemeSettingsDto
            {
                IdleContents = input.DeviceThemeSettings.IdleContents,
                BannerContents = input.DeviceThemeSettings.BannerContents,
                BackgroundColor = input.DeviceThemeSettings.BackgroundColor,
                Logo = input.DeviceThemeSettings.Logo
            };

            var objectJson = JsonConvert.SerializeObject(dtoJson);

            item.ThemeSettings = objectJson;

            await _dineGoDeviceRepo.UpdateAsync(item);

            // Brands

            // Remove all Brands of Devices

            var brandDeviceIds = _dineGoBrandDeviceRepo.GetAll().Where(x => x.DineGoDeviceId == item.Id)
                .Select(x => x.Id).ToList();

            foreach (var brandDeviceId in brandDeviceIds)
            {
                await _dineGoBrandDeviceRepo.DeleteAsync(brandDeviceId);
            }

            // Re-insert following update

            foreach (var dineGoBrand in input.DineGoDevice.DineGoBrands)
            {
                var brandEntity = await _dineGoBrandRepo.GetAsync(dineGoBrand.Id.Value);

                await _dineGoBrandDeviceRepo.InsertAndGetIdAsync(new DineGoBrandDevice
                {
                    DineGoBrandId = brandEntity.Id,
                    DineGoDeviceId = item.Id
                });
            }

            // Payment Types


            // Remove all Brands of Devices

            var paymentTypeIds = _dineGoPaymentTypeDeviceRepo.GetAll().Where(x => x.DineGoDeviceId == item.Id)
                .Select(x => x.Id).ToList();

            foreach (var paymentTypeId in paymentTypeIds)
            {
                await _dineGoPaymentTypeDeviceRepo.DeleteAsync(paymentTypeId);
            }

            // Re-insert following update

            foreach (var dineGoPaymentType in input.DineGoDevice.DineGoPaymentTypes)
            {
                var paymentTypeEntity = await _dineGoPaymentTypeRepo.GetAsync(dineGoPaymentType.Id.Value);

                await _dineGoPaymentTypeDeviceRepo.InsertAndGetIdAsync(new DineGoPaymentTypesDevice
                {
                    DineGoPaymentTypeId = paymentTypeEntity.Id,
                    DineGoDeviceId = item.Id
                });
            }

            if (dto.Id != null)
            {
                return new EntityDto { Id = dto.Id.Value };
            }

            return new EntityDto { Id = 0 };
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.DineGoDevices.Exporter
{
    public interface IDineGoDeviceExporter: IApplicationService
    {
        Task<FileDto> ExportToFile(List<DineGoDeviceListDto> dtos);
    }
}

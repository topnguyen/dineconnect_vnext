﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Go.DineGoDevices.Exporter
{
    public class DineGoDeviceExporter : NpoiExcelExporterBase, IDineGoDeviceExporter
    {
        public DineGoDeviceExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<DineGoDeviceListDto> dtos)
        {
            var excel = CreateExcelPackage(
                "DineGoDeviceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineGoDevice"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("IdleTime"),
                        "OrderLink"
                    );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.IdleTime,
                        _ => _.OrderLink
                    );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.DineGoBrands.Exporter
{
    public interface IDineGoBrandExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<DineGoBrandListDto> dtos);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Go.DineGoBrands.Exporter
{
    public class DineGoBrandExporter : NpoiExcelExporterBase, IDineGoBrandExporter
    {
        public DineGoBrandExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<DineGoBrandListDto> dtos)
        {
            var excel = CreateExcelPackage(
                "DineGoBrandList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineGoBrand"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        L("Location"),
                        L("ScreenMenu")
                    );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.LocationName,
                        _ => _.ScreenMenuName
                    );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.DineGoBrands.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Go.DineGoBrands
{
    public class DineGoBrandAppService : DineConnectAppServiceBase, IDineGoBrandAppService
    {
        private readonly IRepository<DineGoBrand> _dineGoBrandRepo;
        private readonly IDineGoBrandExporter _dineGoBrandExporter;
        private readonly IRepository<ScreenMenu> _screenMenuRepo;
        private readonly IRepository<DineGoDepartment> _dineGoDepartmentRepo;
        private readonly IRepository<DineGoBrandDepartment> _dineGoBrandDepartmentRepo;
        private readonly IRepository<Location> _locationRepo;

        public DineGoBrandAppService(IRepository<DineGoBrand> dineGoBrandRepo, 
            IDineGoBrandExporter dineGoBrandExporter, 
            IRepository<ScreenMenu> screenMenuRepo,
            IRepository<DineGoDepartment> dineGoDepartmentRepo,
            IRepository<DineGoBrandDepartment> dineGoBrandDepartmentRepo,
            IRepository<Location> locationRepo)
        {
            _dineGoBrandRepo = dineGoBrandRepo;
            _dineGoBrandExporter = dineGoBrandExporter;
            _screenMenuRepo = screenMenuRepo;
            _dineGoDepartmentRepo = dineGoDepartmentRepo;
            _dineGoBrandDepartmentRepo = dineGoBrandDepartmentRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultDto<DineGoBrandListDto>> GetAll(GetDineGoBrandInput input)
        {
            var allItems = _dineGoBrandRepo.GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Label.Contains(input.Filter)
                )
                .Include(x => x.Location)
                .Include(x => x.ScreenMenu)
                ;
     
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineGoBrandListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineGoBrandListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoBrandInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineGoBrandListDto>>(allList.Items);

            return await _dineGoBrandExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoBrandForEditOutput> GetDineGoBrandForEdit(EntityDto input)
        {
            var hDto = await _dineGoBrandRepo
                .GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.DineGoDepartments)
                .ThenInclude(x => x.DineGoDepartment)
                .FirstOrDefaultAsync();

            var editDto = ObjectMapper.Map<DineGoBrandEditDto>(hDto);

            return new GetDineGoBrandForEditOutput
            {
                DineGoBrand = editDto
            };
        }

        public async Task<EntityDto> CreateOrUpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            EntityDto returnId;
            
            if (input.DineGoBrand.Id.HasValue)
            {
                returnId = await UpdateDineGoBrand(input);
            }
            else
            {
                returnId = await CreateDineGoBrand(input);
            }
            
            return returnId;
        }

        public async Task DeleteDineGoBrand(EntityDto input)
        {
            await _dineGoBrandRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetScreenMenuForCombobox()
        {
            var screenMenuLists = await _screenMenuRepo.GetAllListAsync(a => !a.IsDeleted);
            return
                new ListResultDto<ComboboxItemDto>(
                    screenMenuLists.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<List<DineGoDepartmentListDto>> GetDineGodepartmentForCombobox()
        {
            var lstDepartments = await _dineGoDepartmentRepo.GetAllListAsync(a => !a.IsDeleted);
            return ObjectMapper.Map<List<DineGoDepartmentListDto>>(lstDepartments);
        }

        protected virtual async Task<EntityDto> CreateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            var dto = new DineGoBrand
            {
                Label = input.DineGoBrand.Label,
                LocationId = input.DineGoBrand.LocationId,
                ScreenMenuId = input.DineGoBrand.ScreenMenuId
            };

            var retId = await _dineGoBrandRepo.InsertAndGetIdAsync(dto);

            // Dine Go Department

            foreach (var dineGoBrandDepartment in input.DineGoBrand.DineGoDepartments)
            {
                var entity = await _dineGoBrandDepartmentRepo.GetAsync(dineGoBrandDepartment.Id.Value);

                await _dineGoBrandDepartmentRepo.InsertAndGetIdAsync(new DineGoBrandDepartment
                {
                    DineGoDepartmentId = entity.Id,
                    DineGoBrandId = retId
                });
            }

            return new EntityDto { Id = retId };
        }

        protected virtual async Task<EntityDto> UpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            var dto = input.DineGoBrand;

            var item = await _dineGoBrandRepo.GetAsync(input.DineGoBrand.Id.Value);

            item.Label = dto.Label;
            item.LocationId = dto.LocationId;
            item.ScreenMenuId = dto.ScreenMenuId;

            await _dineGoBrandRepo.UpdateAsync(item);

            // Dine Go Department

            var brandDepartmentIds = _dineGoBrandDepartmentRepo.GetAll().Where(x => x.DineGoBrandId == item.Id)
                .Select(x => x.Id).ToList();

            foreach (var brandDeviceDepartmentId in brandDepartmentIds)
            {
                await _dineGoBrandDepartmentRepo.DeleteAsync(brandDeviceDepartmentId);
            }

            foreach (var dineGoBrandDepartment in input.DineGoBrand.DineGoDepartments)
            {
                var entity = await _dineGoDepartmentRepo.GetAsync(dineGoBrandDepartment.Id.Value);

                await _dineGoBrandDepartmentRepo.InsertAndGetIdAsync(new DineGoBrandDepartment
                {
                    DineGoDepartmentId = entity.Id,
                    DineGoBrandId = item.Id
                });
            }

            if (dto.Id != null)
            {
                return new EntityDto { Id = dto.Id.Value };
            }

            return new EntityDto { Id = 0 };
        }
    }
}

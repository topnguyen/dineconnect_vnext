﻿using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.DineGoPaymentTypes.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using Microsoft.EntityFrameworkCore;
namespace DinePlan.DineConnect.Go.DineGoPaymentTypes
{
    public class DineGoPaymentTypeAppService : DineConnectAppServiceBase, IDineGoPaymentTypeAppService
    {
        private readonly IRepository<DineGoPaymentType> _dineGoPaymentTypeRepo;
        private readonly IDineGoPaymentTypeExporter _dineGoPaymentTypeExporter;
        private readonly IRepository<PaymentType> _PaymentTypeManager;

        public DineGoPaymentTypeAppService(IRepository<DineGoPaymentType> dineGoPaymentTypeRepo, IDineGoPaymentTypeExporter dineGoPaymentTypeExporter,
            IRepository<PaymentType> paymentTypeManager)
        {
            _dineGoPaymentTypeRepo = dineGoPaymentTypeRepo;
            _dineGoPaymentTypeExporter = dineGoPaymentTypeExporter;
            _PaymentTypeManager = paymentTypeManager;
        }

        public async Task<PagedResultDto<DineGoPaymentTypeListDto>> GetAll(GetDineGoPaymentTypeInput input)
        {
            var allItems = _dineGoPaymentTypeRepo
                .GetAll().WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Label.Contains(input.Filter))
                .Include(x => x.PaymentType);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineGoPaymentTypeListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineGoPaymentTypeListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoPaymentTypeInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineGoPaymentTypeListDto>>(allList.Items);

            return await _dineGoPaymentTypeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoPaymentTypeForEditOutput> GetDineGoPaymentTypeForEdit(EntityDto input)
        {
            var hDto = await _dineGoPaymentTypeRepo.GetAsync(input.Id);

            var editDto = ObjectMapper.Map<DineGoPaymentTypeEditDto>(hDto);

            return new GetDineGoPaymentTypeForEditOutput
            {
                DineGoPaymentType = editDto
            };
        }

        public async Task<EntityDto> CreateOrUpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            EntityDto returnId;
            
            if (input.DineGoPaymentType.Id.HasValue)
            {
                returnId = await UpdateDineGoPaymentType(input);
            }
            else
            {
                returnId = await CreateDineGoPaymentType(input);
            }
            
            return returnId;
        }

        public async Task DeleteDineGoPaymentType(EntityDto input)
        {
            await _dineGoPaymentTypeRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<EntityDto> CreateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            var dto = new DineGoPaymentType
            {
                Label = input.DineGoPaymentType.Label,
                PaymentTypeId = input.DineGoPaymentType.PaymentTypeId,
            };

            var retId = await _dineGoPaymentTypeRepo.InsertAndGetIdAsync(dto);

            return new EntityDto { Id = retId };
        }

        protected virtual async Task<EntityDto> UpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            var dto = input.DineGoPaymentType;

            var item = await _dineGoPaymentTypeRepo.GetAsync(input.DineGoPaymentType.Id.Value);

            item.Label = dto.Label;
            item.PaymentTypeId = dto.PaymentTypeId;

            await _dineGoPaymentTypeRepo.UpdateAsync(item);

            if (dto.Id != null)
            {
                return new EntityDto { Id = dto.Id.Value };
            }

            return new EntityDto { Id = 0 };
        }
    }
}

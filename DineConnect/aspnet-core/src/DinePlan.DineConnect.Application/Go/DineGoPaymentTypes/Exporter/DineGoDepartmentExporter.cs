﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Go.DineGoPaymentTypes.Exporter
{
    public class DineGoPaymentTypeExporter : NpoiExcelExporterBase, IDineGoPaymentTypeExporter
    {
        public DineGoPaymentTypeExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<DineGoPaymentTypeListDto> dtos)
        {
            var excel = CreateExcelPackage(
                "DineGoPaymentTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineGoPaymentType"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        "Payment Type"
                    );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.PaymentTypeName
                    );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

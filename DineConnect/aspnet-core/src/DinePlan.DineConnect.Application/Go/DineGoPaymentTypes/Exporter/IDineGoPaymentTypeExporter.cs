﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.DineGoPaymentTypes.Exporter
{
    public interface IDineGoPaymentTypeExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<DineGoPaymentTypeListDto> dtos);
    }
}

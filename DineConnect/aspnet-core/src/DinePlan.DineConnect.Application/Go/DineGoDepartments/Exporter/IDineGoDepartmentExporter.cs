﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.DineGoDepartments.Exporter
{
    public interface IDineGoDepartmentExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<DineGoDepartmentListDto> dtos);
    }
}

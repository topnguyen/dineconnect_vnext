﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Go.DineGoDepartments.Exporter
{
    public class DineGoDepartmentExporter : NpoiExcelExporterBase, IDineGoDepartmentExporter
    {
        public DineGoDepartmentExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<DineGoDepartmentListDto> dtos)
        {
            var excel = CreateExcelPackage(
                "DineGoDepartmentList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineGoDepartment"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        L("Department")
                    );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.DepartmentName
                    );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

﻿using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.DineGoDepartments.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Go.DineGoDepartments
{
    public class DineGoDepartmentAppService : DineConnectAppServiceBase, IDineGoDepartmentAppService
    {
        private readonly IRepository<DineGoDepartment> _dineGoDepartmentRepo;
        private readonly IRepository<DineGoChargeDepartment> _dineGoChargeDepartmentRepo;
        private readonly IRepository<DineGoCharge> _dineGoChargeRepo;
        private readonly IDineGoDepartmentExporter _dineGoDepartmentExporter;
        private readonly IRepository<Department> _departmentManager;

        public DineGoDepartmentAppService(
            IRepository<DineGoDepartment> dineGoDepartmentRepo, 
            IRepository<DineGoChargeDepartment> dineGoChargeDepartmentRepo, 
            IRepository<DineGoCharge> dineGoChargeRepo, 
            IDineGoDepartmentExporter dineGoDepartmentExporter,
            IRepository<Department> departmentManager)
        {
            _dineGoDepartmentRepo = dineGoDepartmentRepo;
            _dineGoChargeDepartmentRepo = dineGoChargeDepartmentRepo;
            _dineGoChargeRepo = dineGoChargeRepo;
            _dineGoDepartmentExporter = dineGoDepartmentExporter;
            _departmentManager = departmentManager;
        }

        public async Task<PagedResultDto<DineGoDepartmentListDto>> GetAll(GetDineGoDepartmentInput input)
        {
            var allItems = _dineGoDepartmentRepo
                .GetAll()
                .Include(x => x.Department)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Label.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineGoDepartmentListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineGoDepartmentListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoDepartmentInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineGoDepartmentListDto>>(allList.Items);

            return await _dineGoDepartmentExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoDepartmentForEditOutput> GetDineGoDepartmentForEdit(EntityDto input)
        {
            var hDto = await _dineGoDepartmentRepo
                .GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.DineGoCharges)
                .ThenInclude(x => x.DineGoCharge)
                .FirstOrDefaultAsync();

            var editDto = ObjectMapper.Map<DineGoDepartmentEditDto>(hDto);

            return new GetDineGoDepartmentForEditOutput
            {
                DineGoDepartment = editDto
            };
        }

        public async Task<EntityDto> CreateOrUpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            EntityDto returnId;
            
            if (input.DineGoDepartment.Id.HasValue)
            {
                returnId = await UpdateDineGoDepartment(input);
            }
            else
            {
                returnId = await CreateDineGoDepartment(input);
            }
            
            return returnId;
        }

        public async Task DeleteDineGoDepartment(EntityDto input)
        {
            await _dineGoDepartmentRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<EntityDto> CreateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            var dto = new DineGoDepartment
            {
                Label = input.DineGoDepartment.Label,
                DepartmentId = input.DineGoDepartment.DepartmentId,
            };

            var retId = await _dineGoDepartmentRepo.InsertAndGetIdAsync(dto);


            // Dine Go Charges

            foreach (var dineGoCharges in input.DineGoDepartment.DineGoCharges)
            {
                var entity = await _dineGoChargeRepo.GetAsync(dineGoCharges.Id.Value);

                await _dineGoChargeDepartmentRepo.InsertAndGetIdAsync(new DineGoChargeDepartment
                {
                    DineGoChargeId = entity.Id,
                    DineGoDepartmentId = retId
                });
            }

            return new EntityDto { Id = retId };
        }

        protected virtual async Task<EntityDto> UpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            var dto = input.DineGoDepartment;

            var item = await _dineGoDepartmentRepo.GetAsync(input.DineGoDepartment.Id.Value);

            item.Label = dto.Label;
            item.DepartmentId = dto.DepartmentId;

            await _dineGoDepartmentRepo.UpdateAsync(item);

            // Dine Go Charges

            var chargeDepartmentIds = _dineGoChargeDepartmentRepo.GetAll().Where(x => x.DineGoDepartmentId == item.Id).Select(x => x.Id).ToList();

            foreach (var brandDeviceDepartmentId in chargeDepartmentIds)
            {
                await _dineGoChargeDepartmentRepo.DeleteAsync(brandDeviceDepartmentId);
            }

            foreach (var dineGoCharges in input.DineGoDepartment.DineGoCharges)
            {
                var entity = await _dineGoChargeRepo.GetAsync(dineGoCharges.Id.Value);

                await _dineGoChargeDepartmentRepo.InsertAndGetIdAsync(new DineGoChargeDepartment
                {
                    DineGoChargeId = entity.Id,
                    DineGoDepartmentId = item.Id
                });
            }

            if (dto.Id != null)
            {
                return new EntityDto { Id = dto.Id.Value };
            }

            return new EntityDto { Id = 0 };
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Go.DineGoCharges.Exporter
{
    public class DineGoChargeExporter : NpoiExcelExporterBase, IDineGoChargeExporter
    {
        public DineGoChargeExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public Task<FileDto> ExportToFile(List<DineGoChargeListDto> dtos)
        {
            var excel = CreateExcelPackage(
                "DineGoChargeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineGoCharge"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        "TransactionTypeName",
                        "IsPercent",
                        "ChargeValue",
                        L("TaxIncluded")
                    );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.TransactionTypeName,
                        _ => _.IsPercent,
                        _ => _.ChargeValue,
                        _ => _.TaxIncluded
                    );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });


            return Task.FromResult(excel);
        }
    }
}

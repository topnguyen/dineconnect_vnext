﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.DineGoCharges.Exporter
{
    public interface IDineGoChargeExporter : IApplicationService
    {
        Task<FileDto> ExportToFile(List<DineGoChargeListDto> dtos);
    }
}

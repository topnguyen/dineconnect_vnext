﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.DineGoCharges.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Go.DineGoCharges
{
    public class DineGoChargeAppService : DineConnectAppServiceBase, IDineGoChargeAppService
    {
        private readonly IRepository<DineGoCharge> _dineGoChargeRepo;
        private readonly IDineGoChargeExporter _dineGoChargeExporter;
        private readonly IRepository<DineGoCharge> _dinegochargeManager;
        private readonly IRepository<TransactionType> _transactiontypeManager;

        public DineGoChargeAppService(IRepository<DineGoCharge> dineGoChargeRepo, IDineGoChargeExporter dineGoChargeExporter,
            IRepository<DineGoCharge> chargeManager,
            IRepository<TransactionType> transactiontypeManager)
        {
            _dineGoChargeRepo = dineGoChargeRepo;
            _dineGoChargeExporter = dineGoChargeExporter;
            _dinegochargeManager = chargeManager;
            _transactiontypeManager = transactiontypeManager;
        }

        public async Task<PagedResultDto<DineGoChargeListDto>> GetAll(GetDineGoChargeInput input)
        {
            var allItems = _dinegochargeManager.GetAll()
                .Include(x => x.TransactionType)
                .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Label.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineGoChargeListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineGoChargeListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoChargeInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineGoChargeListDto>>(allList.Items);

            return await _dineGoChargeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoChargeForEditOutput> GetDineGoChargeForEdit(EntityDto input)
        {
            var hDto = await _dineGoChargeRepo.GetAsync(input.Id);

            var editDto = ObjectMapper.Map<DineGoChargeEditDto>(hDto);

            return new GetDineGoChargeForEditOutput
            {
                DineGoCharge = editDto
            };
        }

        public async Task<EntityDto> CreateOrUpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            EntityDto returnId;
            
            if (input.DineGoCharge.Id.HasValue)
            {
                returnId = await UpdateDineGoCharge(input);
            }
            else
            {
                returnId = await CreateDineGoCharge(input);
            }
            
            return returnId;
        }

        public async Task DeleteDineGoCharge(EntityDto input)
        {
            await _dineGoChargeRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetChargesForCombobox()
        {
            var lstLocation = await _dinegochargeManager.GetAllListAsync(a => !a.IsDeleted);

            return
                new ListResultDto<ComboboxItemDto>(lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Label)).ToList());
        }

        public async Task<List<DineGoChargeListDto>> GetChargesListDtos()
        {
            var lstCharges = await _dinegochargeManager.GetAllListAsync(a => !a.IsDeleted);

            return ObjectMapper.Map<List<DineGoChargeListDto>>(lstCharges);
        }

        protected virtual async Task<EntityDto> CreateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            var dto = ObjectMapper.Map<DineGoCharge>(input.DineGoCharge);

            var retId = await _dineGoChargeRepo.InsertAndGetIdAsync(dto);

            return new EntityDto { Id = retId };
        }

        protected virtual async Task<EntityDto> UpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            var dto = input.DineGoCharge;

            var item = await _dineGoChargeRepo.GetAsync(input.DineGoCharge.Id.Value);

           ObjectMapper.Map(dto, item);

            await _dineGoChargeRepo.UpdateAsync(item);

            if (dto.Id != null)
            {
                return new EntityDto { Id = dto.Id.Value };
            }

            return new EntityDto { Id = 0 };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Auditing;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.EFPlus;
using Abp.EntityHistory;
using Abp.Logging;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Abp.Timing;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Auditing
{
    public class EntityChangeDeleterWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        
        public const bool IsEnabled = true;

        private const int CheckPeriodAsMilliseconds = 1 * 1000 * 60 * 30; // 30 Mins
        private const int MaxRetailRecordCount = 1000;

        private readonly IRepository<EntityChange, long> _entityChangeRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public EntityChangeDeleterWorker(
            AbpTimer timer,
            IRepository<EntityChange, long> entityChangeRepository,
            IRepository<Tenant> tenantRepository
            )
            : base(timer)
        {
            _entityChangeRepository = entityChangeRepository;
            _tenantRepository = tenantRepository;

            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;

            Timer.Period = CheckPeriodAsMilliseconds;
            Timer.RunOnStart = true;
        }

        protected override void DoWork()
        {

            List<int> tenantIds;
            using (var uow = UnitOfWorkManager.Begin())
            {
                tenantIds = _tenantRepository.GetAll()
                    .Where(t => t.Id>0)
                    .Select(t => t.Id)
                    .ToList();

                uow.Complete();
            }

            DeleteEntityChangesOnHostDatabase();

            foreach (var tenantId in tenantIds)
            {
                DeleteEntityChangesOnTenantDatabase(tenantId);
            }
        }

        protected virtual void DeleteEntityChangesOnHostDatabase()
        {
            try
            {
                using (var uow = UnitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(null))
                    {
                        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                        {
                            DeleteEntityChanges();
                            uow.Complete();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogSeverity.Error, $"An error occured while deleting audit logs on host database", e);
            }
        }

        protected virtual void DeleteEntityChangesOnTenantDatabase(int tenantId)
        {
            try
            {
                using (var uow = UnitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(tenantId))
                    {
                        DeleteEntityChanges();
                        uow.Complete();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogSeverity.Error, $"An error occured while deleting audit log for tenant. TenantId: {tenantId}", e);
            }
        }

        private void DeleteEntityChanges()
        {
            var totalCount = _entityChangeRepository.Count();
            if (totalCount == 0)
                return;

            var finalCount = totalCount - MaxRetailRecordCount;
            if (finalCount <= 0)
                return;

            var deleteStartId = _entityChangeRepository.GetAll().OrderByDescending(l => l.Id).Skip(MaxRetailRecordCount).Select(x => x.Id).First();
            AsyncHelper.RunSync(() => _entityChangeRepository.BatchDeleteAsync(l => l.Id < deleteStartId));
        }
    }
}

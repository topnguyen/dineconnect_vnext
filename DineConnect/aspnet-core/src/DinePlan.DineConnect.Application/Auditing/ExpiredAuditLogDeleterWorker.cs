﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Auditing;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.EFPlus;
using Abp.Logging;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Abp.Timing;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Auditing
{
    public class ExpiredAuditLogDeleterWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        /// <summary>
        /// Set this const field to true if you want to enable ExpiredAuditLogDeleterWorker.
        /// Be careful, If you enable this, all expired logs will be permanently deleted.
        /// </summary>
        public const bool IsEnabled = true;

        private const int CheckPeriodAsMilliseconds = 1 * 1000 * 60 * 30; //30 Mins
        private const int MaxRetailRecordCount = 10000;

        private readonly IRepository<AuditLog, long> _auditLogRepository;
        private readonly IRepository<Tenant> _tenantRepository;

        public ExpiredAuditLogDeleterWorker(
            AbpTimer timer,
            IRepository<AuditLog, long> auditLogRepository,
            IRepository<Tenant> tenantRepository
            )
            : base(timer)
        {
            _auditLogRepository = auditLogRepository;
            _tenantRepository = tenantRepository;

            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;

            Timer.Period = CheckPeriodAsMilliseconds;
            Timer.RunOnStart = true;
        }

        protected override void DoWork()
        {

            List<int> tenantIds;
            using (var uow = UnitOfWorkManager.Begin())
            {
                tenantIds = _tenantRepository.GetAll()
                    .Where(t => t.Id>0)
                    .Select(t => t.Id)
                    .ToList();

                uow.Complete();
            }

            DeleteAuditLogsOnHostDatabase();

            foreach (var tenantId in tenantIds)
            {
                DeleteAuditLogsOnTenantDatabase(tenantId);
            }
        }

        protected virtual void DeleteAuditLogsOnHostDatabase()
        {
            try
            {
                using (var uow = UnitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(null))
                    {
                        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                        {
                            DeleteAuditLogs();
                            uow.Complete();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogSeverity.Error, $"An error occured while deleting audit logs on host database", e);
            }
        }

        protected virtual void DeleteAuditLogsOnTenantDatabase(int tenantId)
        {
            try
            {
                using (var uow = UnitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(tenantId))
                    {
                        DeleteAuditLogs();
                        uow.Complete();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogSeverity.Error, $"An error occurred while deleting audit log for tenant. TenantId: {tenantId}", e);
            }
        }

        private void DeleteAuditLogs()
        {
            var totalCount = _auditLogRepository.Count();
            if (totalCount == 0)
                return;

            var finalCount = totalCount - MaxRetailRecordCount;
            if (finalCount <= 0)
                return;

            var deleteStartId = _auditLogRepository.GetAll().OrderByDescending(l => l.Id).Skip(MaxRetailRecordCount).Select(x => x.Id).First();
            AsyncHelper.RunSync(() => _auditLogRepository.BatchDeleteAsync(l => l.Id < deleteStartId));
        }
    }
}

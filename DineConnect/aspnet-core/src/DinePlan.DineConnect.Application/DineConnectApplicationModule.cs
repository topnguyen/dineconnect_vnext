﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using DinePlan.DineConnect.Authorization;
using System.ComponentModel;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(
        typeof(DineConnectApplicationSharedModule),
        typeof(DineConnectCoreModule)
        )]
    public class DineConnectApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();

            //Adding custom AutoMapper configuration
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);

        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectApplicationModule).GetAssembly());
        }
    }
}
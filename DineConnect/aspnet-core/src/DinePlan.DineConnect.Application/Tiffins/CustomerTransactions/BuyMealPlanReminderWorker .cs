﻿using System.Linq;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Abp.Timing;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.EmailNotification;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions
{
    public class BuyMealPlanReminderWorker  : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private const int CheckPeriodAsMilliseconds = 1 * 60 * 60 * 1000; // 1 hours

        private readonly IEmailNotificationAppService _emailNotificationAppService;
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepo;
        private readonly IRepository<BaseCore.Customer.Customer> _customerRepo;
        private readonly IRepository<TiffinCustomerMealPlanReminder> _customerMealPlanReminderRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public BuyMealPlanReminderWorker (
            AbpTimer timer,
            IEmailNotificationAppService emailNotificationAppService,
            IRepository<TiffinCustomerProductOffer> customerProductOfferRepo,
            IRepository<BaseCore.Customer.Customer> customerRepo,
            IRepository<TiffinCustomerMealPlanReminder> customerMealPlanReminderRepo,
            IUnitOfWorkManager unitOfWorkManager
            )
            : base(timer)
        {
            _emailNotificationAppService = emailNotificationAppService;
            _customerProductOfferRepo = customerProductOfferRepo;
            _customerRepo = customerRepo;
            _customerMealPlanReminderRepo = customerMealPlanReminderRepo;
            _unitOfWorkManager = unitOfWorkManager;

            Timer.Period = CheckPeriodAsMilliseconds;
            Timer.RunOnStart = true;

            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            
            if (Clock.Now.ToUniversalTime().Hour != 9) 
            {
                return;
            }
            
            using (var uow = _unitOfWorkManager.Begin())
            {
                var newCustomerRegistrationTimeFrom = Clock.Now.AddDays(-7);
                var newCustomers = _customerRepo.GetAll().Where(x => x.CreationTime > newCustomerRegistrationTimeFrom).ToList();
                var newCustomerIds = newCustomers.Select(x => x.Id).ToList();
                var oldCustomerIds = _customerProductOfferRepo.GetAll().Where(x => newCustomerIds.Contains(x.CustomerId)).Select(x => x.CustomerId).ToList();
                var newCustomerWithoutMealPlan = newCustomers.Where(x => !oldCustomerIds.Contains(x.Id)).ToList();

                foreach (var customer in newCustomerWithoutMealPlan)
                {
                    var customerMealPlanReminder = _customerMealPlanReminderRepo.GetAll()
                        .FirstOrDefault(x => x.CustomerId == customer.Id);

                    if (customerMealPlanReminder == null)
                    {
                        customerMealPlanReminder = new TiffinCustomerMealPlanReminder
                        {
                            CustomerId = customer.Id
                        };

                        _customerMealPlanReminderRepo.InsertAndGetIdAsync(customerMealPlanReminder);
                    }

                    if (customerMealPlanReminder.BuyMealPlanReminderTimes >= 3)
                    {
                        // Only remind max 3 times
                        continue;
                    }

                    var registeredDays = Clock.Now.Date.Subtract(customer.CreationTime.Date).Days;

                    if (customerMealPlanReminder.BuyMealPlanReminderTimes >= registeredDays)
                    {
                        continue;
                    }

                    if (customerMealPlanReminder.LastTimeSentBuyMealPlanReminder.Date == Clock.Now.Date)
                    {
                        // Does not remind same date
                        continue;
                    }
                    
                    // Send reminder email
                    
                    AsyncHelper.RunSync(() => _emailNotificationAppService.SendEmailRemindBuyMealPlan(customer.EmailAddress, customer.Name, customer.TenantId));

                    customerMealPlanReminder.BuyMealPlanReminderTimes++;
                    customerMealPlanReminder.LastTimeSentBuyMealPlanReminder = Clock.Now;
                    _customerMealPlanReminderRepo.Update(customerMealPlanReminder);
                }
                
                uow.Complete();
            }
        }
    }
}

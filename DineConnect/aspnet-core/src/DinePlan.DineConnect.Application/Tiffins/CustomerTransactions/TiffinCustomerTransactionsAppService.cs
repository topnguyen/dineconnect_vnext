﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.Customer.Transactions;
using DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos;
using DinePlan.DineConnect.Tiffins.CustomerTransactions.Dtos;
using DinePlan.DineConnect.Tiffins.CustomerTransactions.Exporting;
using DinePlan.DineConnect.Tiffins.Invoice;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions
{
    
    public class TiffinCustomerTransactionsAppService : DineConnectAppServiceBase, ITiffinCustomerTransactionsAppService
    {
        private readonly IRepository<TiffinCustomerTransaction> _tiffinCustomerTransactionRepository;
        private readonly IRepository<TiffinCustomerProductOffer> _tiffinCustomerProductOfferRepository;
        private readonly IRepository<TiffinPayment> _tiffinPaymentRepo;
        private readonly IRepository<BaseCore.Customer.Customer> _tiffinCustomerRepository;
        private ICustomerTransactionExcelExporter _exporter;

        public TiffinCustomerTransactionsAppService(IRepository<TiffinCustomerTransaction> tiffinCustomerTransactionRepository
            , IRepository<TiffinCustomerProductOffer> tiffinCustomerProductOfferRepository
            , IRepository<TiffinPayment>  tiffinPaymentRepo, ICustomerTransactionExcelExporter exporter
            , IRepository<BaseCore.Customer.Customer> tiffinCustomerRepository)
        {
            _tiffinCustomerTransactionRepository = tiffinCustomerTransactionRepository;
            _tiffinCustomerProductOfferRepository = tiffinCustomerProductOfferRepository;
            _tiffinCustomerRepository = tiffinCustomerRepository;
            _tiffinPaymentRepo = tiffinPaymentRepo;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<CustomerOfferForTransactionDto>> GetAllCustomerOffer(GetAllCustomerTransactionsInput input)
        {
            DateTime? nullDateTime = null;

            var query = _tiffinCustomerProductOfferRepository.GetAll()
                .Include(e => e.ProductOffer)
                .Include(e => e.Customer)
                .Include(e => e.Payment)
                .WhereIf(input.StartDate.HasValue, x => x.CreationTime.Date >= input.StartDate)
                .WhereIf(input.EndDate.HasValue, x => x.CreationTime.Date <= input.EndDate)
                .WhereIf(input.DaysToExpiry.HasValue, e => e.ProductOffer.Expiry
                                                           && e.ExpiryDate.Date >= Clock.Now.Date.ToDayEnd()
                                                           && e.ExpiryDate.Date <= Clock.Now.Date.AddDays(input.DaysToExpiry.Value).ToDayEnd())
                .WhereIf(input.CustomerId!=null, e => e.Customer.Id.Equals(input.CustomerId))
                .WhereIf(!input.PaymentFilter.IsNullOrEmpty(), e => EF.Functions.Like(e.Payment.PaymentOption, input.PaymentFilter.ToILikeString()))
                .Select(e => new CustomerOfferForTransactionDto
                {
                    CustomerOfferId = e.Id,
                    CustomerName = e.Customer.Name,
                    CustomerEmail = e.Customer.EmailAddress,
                    CreationTime = e.CreationTime,
                    ActualCredit = e.ActualCredit,
                    BalanceCredit = e.BalanceCredit,
                    OfferName = e.ProductOffer.Name,
                    ExpiryDate = e.ProductOffer.Expiry ? e.ExpiryDate : nullDateTime,
                    Amount = e.PaidAmount,
                    Paymode = e.Payment.PaymentOption
                });

            var totalCount = await query.CountAsync();

            var result = query
                .OrderBy(input.Sorting)
                .PageBy(input);

            return new PagedResultDto<CustomerOfferForTransactionDto>(totalCount, await result.ToListAsync());
        }
        public async Task<PagedResultDto<CustomerOfferForTransactionDto>> GetCustomerTransactions(GetAllCustomerTransactionsInput input)
        {
            var query = _tiffinPaymentRepo.GetAll()
                .Include(e => e.Customer)
                .WhereIf(input.StartDate.HasValue, x => x.CreationTime.Date >= input.StartDate)
                .WhereIf(input.EndDate.HasValue, x => x.CreationTime.Date <= input.EndDate)
                .WhereIf(input.CustomerId!=null, e => e.Customer.Id.Equals(input.CustomerId.Value))
                .WhereIf(!input.PaymentFilter.IsNullOrEmpty(), e => EF.Functions.Like(e.PaymentOption, input.PaymentFilter.ToILikeString()))
                .Select(e => new CustomerOfferForTransactionDto
                {
                    CustomerOfferId = e.Id,
                    CustomerName = e.Customer.Name,
                    CustomerEmail = e.Customer.EmailAddress,
                    CreationTime = e.CreationTime,
                    Amount = e.PaidAmount,
                    Paymode = e.PaymentOption
                });

            var totalCount = await query.CountAsync();

            var result = query
                .OrderBy(input.Sorting)
                .PageBy(input);

            return new PagedResultDto<CustomerOfferForTransactionDto>(totalCount, await result.ToListAsync());
        }
        public async Task<FileDto> GetCustomerTransactionsExcel(GetAllCustomerTransactionsInput input)
        {
            return await _exporter.ExportToFile(input, this);
        }

        public async Task<TiffinCustomerProductOfferDto> GetCustomerProductOfferForEdit(long id)
        {
            var customerOffer = await _tiffinCustomerProductOfferRepository.GetAllIncluding(e => e.ProductOffer).FirstOrDefaultAsync(e => e.Id == id);

            var resutl = ObjectMapper.Map<TiffinCustomerProductOfferDto>(customerOffer);
            return resutl;
        }

        public async Task UpdateCustomerOffer(TiffinCustomerProductOfferDto input)
        {
            var customerOffer = await _tiffinCustomerProductOfferRepository.GetAsync(input.Id);

            var changeList = new List<BalanceChangeListDto>();

            if (input.ExpiryDate.Date != customerOffer.ExpiryDate.Date)
            {
                changeList.Add(new BalanceChangeListDto
                {
                    PropertyName = "Expiry Date",
                    OriginalValue = customerOffer.ExpiryDate.ToString(),
                    NewValue = input.ExpiryDate.ToString()
                });

                customerOffer.ExpiryDate = input.ExpiryDate;
            }

            if (input.BalanceCredit != customerOffer.BalanceCredit)
            {
                changeList.Add(new BalanceChangeListDto
                {
                    PropertyName = "Balance Credit",
                    OriginalValue = customerOffer.BalanceCredit.ToString(),
                    NewValue = input.BalanceCredit.ToString()
                });

                customerOffer.BalanceCredit = input.BalanceCredit;
            }

            var customerTransaction = new TiffinCustomerTransaction
            {
                Reason = input.CustomerTransactionDto.Reason,
                ChangeDetails = JsonConvert.SerializeObject(changeList),
                CustomerProductOfferId = customerOffer.Id,
                TenantId = (int)AbpSession.TenantId
            };

            await _tiffinCustomerProductOfferRepository.UpdateAsync(customerOffer);

            await _tiffinCustomerTransactionRepository.InsertAsync(customerTransaction);
        }

        public async Task<PagedResultDto<TiffinCustomerTransactionDto>> GetEditHistories(long customerOfferId)
        {
            var tiffinCustomerTransactions = _tiffinCustomerTransactionRepository.GetAll()
                .Where(e => e.CustomerProductOfferId == customerOfferId)
                .Select(o => new TiffinCustomerTransactionDto
                {
                    Reason = o.Reason,
                    Id = o.Id,
                    ChangeDetails = o.ChangeDetails,
                    CreationTime = o.CreationTime,
                })
                .OrderByDescending(e => e.CreationTime);

            return new PagedResultDto<TiffinCustomerTransactionDto>(
                await tiffinCustomerTransactions.CountAsync(),
                await tiffinCustomerTransactions.ToListAsync()
            );
        }

        public async Task<List<ComboboxItemDto>> GetAllCustomerForCombobox()
        {
            var result = await _tiffinCustomerRepository.GetAll()
                .Select(e => new ComboboxItemDto(e.Name, e.Name))
                .Distinct()
                .ToListAsync();
            return new List<ComboboxItemDto>(result);
        }
    }
}
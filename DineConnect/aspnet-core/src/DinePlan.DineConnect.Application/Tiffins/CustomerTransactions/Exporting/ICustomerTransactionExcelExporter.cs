﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos;
using DinePlan.DineConnect.Tiffins.CustomerTransactions.Dtos;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions.Exporting
{
    public interface ICustomerTransactionExcelExporter
    {
        Task<FileDto> ExportToFile(GetAllCustomerTransactionsInput input, ITiffinCustomerTransactionsAppService appService);
    }
}

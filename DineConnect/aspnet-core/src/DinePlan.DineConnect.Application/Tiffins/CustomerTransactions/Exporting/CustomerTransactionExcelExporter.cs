﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetZeroCore.Net;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using DinePlan.DineConnect.Tiffins.CustomerTransactions.Dtos;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions.Exporting
{
    public class CustomerTransactionExcelExporter : NpoiExcelExporterBase, ICustomerTransactionExcelExporter
    {
        private readonly IAbpSession _abpSession;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public CustomerTransactionExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public async Task<FileDto> ExportToFile(GetAllCustomerTransactionsInput input,
            ITiffinCustomerTransactionsAppService appService)
        {
            var file = new FileDto("Transactions-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var excelPackage = new XSSFWorkbook();

            var sheet = excelPackage.CreateSheet("Transaction");

            AddHeader(
                sheet,
                L("PurchaseDate"),
                L("PurchaseTime"),
                L("CustomerName"),
                L("Paymode"),
                L("Amount")
            );
            var rowCount = 2;
            var colCount = 1;
            var totalAmount = 0M;
            var secondTime = false;
            var ticketAvailable = false;
            while (true)
            {
                input.MaxResultCount = 100;
                var orderDetailDos = await appService.GetCustomerTransactions(input);
                if (!secondTime) secondTime = true;

                if (orderDetailDos.Items.Any())
                {
                    totalAmount += (orderDetailDos.Items.Where(a => a.Amount != null)
                        .Sum(a => a.Amount.Value));

                    ticketAvailable = true;
                    foreach (var order in orderDetailDos.Items)
                    {
                        colCount = 0;

                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(_timeZoneConverter.Convert(order.CreationTime,
                            _abpSession.TenantId, _abpSession.GetUserId())?.ToString("dd/MM/yyyy"));
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(_timeZoneConverter.Convert(order.CreationTime,
                            _abpSession.TenantId, _abpSession.GetUserId())?.ToLongTimeString());
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.CustomerName);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.Paymode);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue((double)order.Amount);
                        rowCount++;
                    }
                }
                else
                {
                    break;
                }

                input.SkipCount += input.MaxResultCount;
            }

            if (ticketAvailable)
            {
                sheet.GetSheetRow(rowCount).GetSheetCell(1).SetCellValue(L("Total"));
                sheet.GetSheetRow(rowCount).GetSheetCell(4).SetCellValue((double)totalAmount);
            }

            for (var i = 1; i <= colCount + 2; i++) sheet.AutoSizeColumn(i);
            Save(excelPackage, file);


            return file;
        }

        public FileDto ExportToFile(List<CustomerListDto> customers)
        {
            return CreateExcelPackage(
                "Customers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Customers"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("RegisteredDate"),
                        L("LastLoginDate"),
                        L("ContactEmail"),
                        L("ContactPhone")
                    );

                    AddObjects(
                        sheet, 2, customers,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.LastLoginDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.EmailAddress,
                        _ => _.PhoneNumber
                    );

                    for (var i = 1; i <= 5; i++) sheet.AutoSizeColumn(i);
                });
        }
    }
}
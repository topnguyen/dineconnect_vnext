﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto;
using DinePlan.DineConnect.Settings;
using DinePlan.DineConnect.Tiffins.EmailNotification;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Payment;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using Elect.Core.StringUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.Net.Whatapp;
using DinePlan.DineConnect.Whatapp;
using TiffinCustomerReferral = DinePlan.DineConnect.Tiffins.Customer.TiffinCustomerReferral;

namespace DinePlan.DineConnect.Tiffins.MemberPortal
{
    public class TiffinMemberPortalAppService : DineConnectAppServiceBase, ITiffinMemberPortalAppService
    {
        private readonly IRepository<BaseCore.Customer.Customer> _customerRepository;
        private readonly IRepository<TiffinCustomerReferral> _tiffinCustomerReferralRepository;
        private readonly IRepository<CustomerAddressDetail> _customerAddressRepository;
        private readonly IRepository<TiffinProductOffer> _tiffinProductOfferRepository;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;
        private readonly ITiffinInvoiceNumberGenerator _tiffinInvoiceNumberGenerator;
        private readonly IStripePaymentGatewayManager _stripePaymentGatewayManager;
        private readonly IRepository<TiffinPayment> _tiffinPaymentRepository;
        private readonly IRepository<TiffinPaymentRequest> _tiffinPaymentRequestRepo;
        private readonly ITiffinPaymentExporter _tfPaymentExporter;
        private readonly IEmailNotificationBackgroundJobService _emailNotificationService;
        private readonly ITenantEmailSendAppService _emailSendAppService;
        private readonly IRepository<TiffinPromotionCode> _tiffinPromotionCode;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly ITenantEmailSendAppService _tenantEmailSendAppService;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        private readonly IAdyenPaymentProcessor _adyenPaymentProcessor;
        private readonly IStripePaymentProcessor _stripePaymentProcessor;
        private readonly IPaypalPaymentProcessor _paypalPaymentProcessor;
        private readonly IGooglePaymentProcessor _googlePaymentProcessor;
        private readonly ISwipePaymentProcessor _swipePaymentProcessor;
        private readonly IServiceProvider _serviceProvider;
        private readonly ITenantWhatappSendAppService _tenantWhatappSendAppService;

        public TiffinMemberPortalAppService(
            IRepository<BaseCore.Customer.Customer> customerRepository,
            IRepository<TiffinCustomerReferral> tiffinCustomerReferralRepository,
            IRepository<CustomerAddressDetail> customerAddressRepository,
            IRepository<TiffinProductOffer> tiffinProductOfferRepository,
            IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository,
            IStripePaymentGatewayManager stripePaymentGatewayManager,
            ITiffinInvoiceNumberGenerator tiffinInvoiceNumberGenerator,
            IRepository<TiffinPayment> tiffinPaymentRepository,
            IRepository<TiffinPaymentRequest> tiffinPaymentRequestRepo,
            ITiffinPaymentExporter tfPaymentExporter,
            IEmailNotificationBackgroundJobService emailNotificationService,
            ITenantEmailSendAppService emailSendAppService,
            IRepository<TiffinPromotionCode> tiffinPromotionCode,
            ITenantSettingsAppService tenantSettingsAppService,
            ITenantEmailSendAppService tenantEmailSendAppService,
            IOmisePaymentProcessor omisePaymentProcessor,
            IAdyenPaymentProcessor adyenPaymentProcessor,
            IStripePaymentProcessor stripePaymentProcessor,
            IPaypalPaymentProcessor paypalPaymentProcessor,
            IGooglePaymentProcessor googlePaymentProcessor,
            ISwipePaymentProcessor swipePaymentProcessor,
            IServiceProvider serviceProvider,
            ITenantWhatappSendAppService tenantWhatappSendAppService)
        {
            _customerRepository = customerRepository;
            _tiffinCustomerReferralRepository = tiffinCustomerReferralRepository;
            _customerAddressRepository = customerAddressRepository;
            _tiffinProductOfferRepository = tiffinProductOfferRepository;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
            _stripePaymentGatewayManager = stripePaymentGatewayManager;
            _tiffinInvoiceNumberGenerator = tiffinInvoiceNumberGenerator;
            _tiffinPaymentRepository = tiffinPaymentRepository;
            _tfPaymentExporter = tfPaymentExporter;
            _tiffinPaymentRequestRepo = tiffinPaymentRequestRepo;
            _emailNotificationService = emailNotificationService;
            _emailSendAppService = emailSendAppService;
            _tiffinPromotionCode = tiffinPromotionCode;
            _tenantSettingsAppService = tenantSettingsAppService;
            _tenantEmailSendAppService = tenantEmailSendAppService;
            _omisePaymentProcessor = omisePaymentProcessor;
            _adyenPaymentProcessor = adyenPaymentProcessor;
            _stripePaymentProcessor = stripePaymentProcessor;
            _paypalPaymentProcessor = paypalPaymentProcessor;
            _googlePaymentProcessor = googlePaymentProcessor;
            _swipePaymentProcessor = swipePaymentProcessor;
            _serviceProvider = serviceProvider;
            _tenantWhatappSendAppService = tenantWhatappSendAppService;
        }

        [AbpAllowAnonymous]
        [DisableAuditing]
        public async Task<MyInfoDto> GetMyInfo()
        {
            var customer = await _customerRepository.GetAll()
                .Include(c => c.CustomerAddressDetails)
                .Include(c => c.Country)
                .Include(c => c.User)
                .FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

            if (customer == null)
            {
                return null;
            }

            // Check customer referral

            var result = ObjectMapper.Map<MyInfoDto>(customer);

            var defaultAdd = customer.CustomerAddressDetails.FirstOrDefault(e => e.IsDefault);

            result.DefaultAddresId = defaultAdd != null ? defaultAdd.Id : 0;

            result.CallingCode = customer.Country != null ? customer.Country.CallingCode : 0;

            // Referral Code

            var customerReferral = await _tiffinCustomerReferralRepository.GetAll().FirstOrDefaultAsync(x => x.CustomerId == customer.Id);

            if (customerReferral == null)
            {
                customerReferral = new TiffinCustomerReferral
                {
                    CustomerId = customer.Id
                };

                // Generate Referral customer.Id

                var isUniqueReferralCode = false;

                while (!isUniqueReferralCode)
                {
                    customerReferral.ReferralCode = StringHelper.Generate(8, true, false, false, false);

                    var isExistingReferralCode = _tiffinCustomerReferralRepository.GetAll().Any(x => x.ReferralCode == customerReferral.ReferralCode);

                    isUniqueReferralCode = !isExistingReferralCode;
                }

                await _tiffinCustomerReferralRepository.InsertAndGetIdAsync(customerReferral);
            }

            result.ReferralCode = customerReferral?.ReferralCode;

            return result;
        }

        public async Task<List<UpdateMyAddressInputDto>> GetAllAddress()
        {
            var customer = await _customerRepository.GetAll()
                .Include(c => c.CustomerAddressDetails)
                .Include(c => c.Country)
                .FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

            if (customer == null)
            {
                return new List<UpdateMyAddressInputDto>();
            }

            var result = customer.CustomerAddressDetails
                .Select(cus => ObjectMapper.Map<UpdateMyAddressInputDto>(cus)).ToList();
            return result;
        }

        public async Task SetDefaultAddress(long addressId)
        {
            var newAddressDefault = _customerAddressRepository.FirstOrDefault(l => l.Id == addressId);

            var listAddress = _customerAddressRepository.GetAll()
                .Where(c => c.CustomerId == newAddressDefault.CustomerId);

            var oldAddressDefaults = await listAddress.Where(l => l.IsDefault).ToListAsync();            

            foreach (var oldAddressDefault in oldAddressDefaults)
            {
                if (oldAddressDefault != null && oldAddressDefault.Id != addressId)
                {
                    oldAddressDefault.IsDefault = false;
                    await _customerAddressRepository.UpdateAsync(oldAddressDefault);
                }
            }

            if (newAddressDefault != null)
            {
                newAddressDefault.IsDefault = true;
                await _customerAddressRepository.UpdateAsync(newAddressDefault);
            }
        }

        public async Task<UpdateMyAddressInputDto> GetMyAddress(long addressId)
        {
            var customerAddress = await _customerAddressRepository
                .GetAll()
                .Include(e => e.Customer)
                .Include(e => e.City)
                .ThenInclude(e => e.Country)
                .FirstOrDefaultAsync(t => t.Id == addressId);
            var result = ObjectMapper.Map<UpdateMyAddressInputDto>(customerAddress);
            if (customerAddress.Customer?.Country?.CallingCode != null)
                result.CallingCode = (int)customerAddress.Customer?.Country?.CallingCode;

            return result;
        }

        public async Task DeleteAddress(long addressId)
        {
            var customerAddress = await _customerAddressRepository
                .FirstOrDefaultAsync(t => t.Id == addressId);
            if (customerAddress != null)
            {
                await _customerAddressRepository.DeleteAsync(customerAddress);
            }
        }

        public async Task UpdateCountry(int countryId)
        {
            var customer = await _customerRepository.FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

            customer.CountryId = countryId;
            await _customerRepository.UpdateAsync(customer);
        }

        public async Task UpdateMyBasicInfo(UpdateMyInfoDto input)
        {
            if (input != null && !string.IsNullOrEmpty(input.Name))
            {
                var customer = await _customerRepository.FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

                customer.Name = input.Name;
                customer.PhoneNumber = input.PhoneNumber;
                customer.WhatAppNumber = input.WhatAppNumber;
                customer.IsDisableDailyOrderReminder = input.IsDisableDailyOrderReminder;
                customer.CartJsonData = input.CartJsonData;
                customer.JsonData = input.JsonData;
                var user = await GetCurrentUserAsync();

                user.Name = input.Name;
                user.Surname = input.Surname;
                user.PhoneNumber = input.PhoneNumber;

                CheckErrors(await UserManager.UpdateAsync(user));

                await _customerRepository.UpdateAsync(customer);
            }
        }

        public async Task AddOrUpdateMyAddress(UpdateMyAddressInputDto input)
        {
            var customerId = input.CustomerId;
            if(customerId == null)
            {
                var customer = await _customerRepository.FirstOrDefaultAsync(c => c.UserId == AbpSession.UserId);
                customerId = customer?.Id;
            }
            if(customerId == null)
            {
                throw new UserFriendlyException("No customer");
            }
            if (input.AddressId.HasValue)
            {
                var address = await _customerAddressRepository.FirstOrDefaultAsync(t => t.Id == input.AddressId);
                ObjectMapper.Map(input, address);
                address.CustomerId = customerId.Value;
                await _customerAddressRepository.UpdateAsync(address);

                if (input.IsDefault)
                {
                    await SetDefaultAddress(input.AddressId.Value);
                }
            }
            else
            {
                var newAddress = ObjectMapper.Map<CustomerAddressDetail>(input);
                newAddress.CustomerId = customerId.Value;
                var addressId = await _customerAddressRepository.InsertAndGetIdAsync(newAddress);

                if (input.IsDefault)
                {
                    await SetDefaultAddress(addressId);
                }
            }
        }

        [AbpAllowAnonymous]
        public async Task<ListResultDto<TiffinMemberProductOfferDto>> GetActiveOffers(GetAllTiffinProductOffersInput input)
        {
            var activeProductOffers = _tiffinProductOfferRepository.GetAll()
                .Where(e => !e.InActive)
                .Include(e => e.ProductOfferSets)
                .ThenInclude(oc => oc.TiffinProductSet)
                .Include(e => e.TiffinProductOfferType)
                .OrderBy(e => e.SortOrder)
                .Select(o => ObjectMapper.Map<TiffinMemberProductOfferDto>(o));

            var myOyut = await activeProductOffers.ToListAsync();

            return new ListResultDto<TiffinMemberProductOfferDto>(myOyut);
        }

        public async Task<PagedResultDto<BalanceDetailDto>> GetBalancesForCustomer(GetBalanceInput input)
        {
            DateTime? nullDate = null;
            var query = _tfCustomerProductOfferRepository.GetAll()
                .Include(e => e.ProductOffer)
                .ThenInclude(e => e.TiffinProductOfferType)
                .Include(e => e.Customer)
                .WhereIf(input.CustomerId.HasValue, c => c.CustomerId == input.CustomerId)
                .WhereIf(!input.CustomerId.HasValue, c => c.Customer.UserId == AbpSession.UserId)
                .WhereIf(input.IsExpiryOffer == true,
                    e => (e.ProductOffer.Expiry && e.ExpiryDate.Date <= Clock.Now.Date) || e.BalanceCredit <= 0)
                .WhereIf(input.IsExpiryOffer == false,
                    e => (!e.ProductOffer.Expiry || e.ExpiryDate.Date > Clock.Now.Date) && e.BalanceCredit > 0)
                .Select(i => new BalanceDetailDto
                {
                    CustomerOfferOrderId = i.Id,
                    CustomerOfferOrderDate = i.CreationTime,
                    TypeId = i.ProductOffer.TiffinProductOfferType.Id,
                    Type = i.ProductOffer.TiffinProductOfferType.Name,
                    OfferId = i.ProductOffer.Id,
                    Offer = i.ProductOffer.Name,
                    ActualCredit = i.ActualCredit,
                    CreditBalance = i.BalanceCredit,
                    PaidAmount = i.PaidAmount,
                    Expiry = i.ProductOffer.Expiry,
                    DaysOnFirstOrder = i.ProductOffer.DaysOnFirstOrder,
                    PaymentDate = i.Payment.PaymentDate,
                    ExpiryDate = i.ProductOffer.Expiry ? i.ExpiryDate : nullDate
                })
                .WhereIf(!input.FilterText.IsNullOrEmpty(), e =>
                    EF.Functions.Like(e.Type, input.FilterText.ToILikeString())
                    || EF.Functions.Like(e.Offer, input.FilterText.ToILikeString()));

            var totalCount = await query.CountAsync();
            var pagedResult = await query
                .OrderBy(input.Sorting)
                .PageBy(input).ToListAsync();

            return new PagedResultDto<BalanceDetailDto>(
                totalCount, pagedResult
            );
        }

        public async Task<PagedResultDto<PaymentCustomerHistoryDto>> GetPaymentHistory(GetPaymentInput input)
        {
            var query = _tiffinPaymentRepository
                .GetAll()
                .Include(t => t.Customer)
                .Include(t => t.CustomerProductOffers)
                .ThenInclude(c => c.ProductOffer)
                .ThenInclude(p => p.ProductOfferSets)
                .ThenInclude(pos => pos.TiffinProductSet)
                .Where(t => t.Customer.UserId == AbpSession.UserId && t.PaymentType == TiffinPaymentType.MealPlan)
                .Select(p => new PaymentCustomerHistoryDto
                {
                    Id = p.Id,
                    InvoiceNo = p.InvoiceNo,
                    PaidAmount = p.PaidAmount,
                    PaymentDate = p.PaymentDate,
                    ModeOfPayment = p.PaymentOption
                })
                .WhereIf(input.StartDate.HasValue, x => x.PaymentDate.Date >= input.StartDate)
                .WhereIf(input.EndDate.HasValue, x => x.PaymentDate.Date <= input.EndDate)
                .WhereIf(!input.PaymentMethodText.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.ModeOfPayment, input.PaymentMethodText.ToILikeString()));

            var totalCount = await query.CountAsync();
            var pagedResult = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<PaymentCustomerHistoryDto>(
                totalCount, pagedResult
            );
        }

        public async Task<TiffinPaymentDetailDto> GetPaymentDetail(long id)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var detail = await _tiffinPaymentRepository
                    .GetAll()
                    .Include(t => t.Customer)
                    .Include(t => t.CustomerProductOffers)
                    .ThenInclude(x => x.ProductOffer)
                    .ThenInclude(x => x.ProductOfferSets)
                    .ThenInclude(oc => oc.TiffinProductSet)
                    .ThenInclude(tp => tp.Products)
                    .ThenInclude(pd => pd.Product)
                    .FirstOrDefaultAsync(t => t.Id == id);

                var output = ObjectMapper.Map<TiffinPaymentDetailDto>(detail);
                output.EmailAddress = detail.Customer.EmailAddress;
                output.PaymentOption = detail.PaymentOption;

                //var list1 = detail.CustomerProductOffers.Select(x =>
                //    x.ProductOffer.ProductOfferSets.Select(c =>
                //        c.TiffinProductSet.Products.Select(g => g.Product.Name).ToList()).ToList()).ToList();

                //var list2 = list1.SelectMany(l => l).Distinct().ToList();
                //var list3 = list2.SelectMany(l => l).Distinct().ToList();

                output.Products = detail.CustomerProductOffers.Select(x => new CustomerProductOfferDetailDto
                {
                    ProductOfferName = string.Join(", ", x.ProductOffer.Name),
                    PaidAmount = x.PaidAmount,
                    Quantity = x.ActualCredit != null && x.ProductOffer.TotalDeliveryCount != 0
                        ? (x.ActualCredit.Value / x.ProductOffer.TotalDeliveryCount)
                        : 0,
                    Price = x.ProductOffer.Price
                }).ToList();

                var cardCharge = output.PaidAmount - output.Products.Sum(o => o.PaidAmount);
                output.CardCharge = cardCharge;
                return output;
            }
        }

        public Task<string> CreateOmisePaymentNowRequest(long amount, string currency)
        {
            return _omisePaymentProcessor.CreateOmisePaymentNowRequest(amount, currency);
        }

        public async Task<OmisePaymentResponseDto> GetOmisePaymentStatus(string chargeId)
        {
            return await _omisePaymentProcessor.GetStatus(chargeId);
        }

        public async Task<PaymentOutputDto> CreatePayment(CreatePaymentInputDto input)
        {
            PaymentOutputDto output = new PaymentOutputDto();

            var tiffPaymentRe = new TiffinPaymentRequest
            {
                PaymentOption = input.PaymentOption,
                PaidAmount = input.PaidAmount ?? 0,
                PaymentDate = Clock.Now,
                Request = JsonConvert.SerializeObject(input)
            };

            var repoId = await _tiffinPaymentRequestRepo.InsertAndGetIdAsync(tiffPaymentRe);

            if (input.CallbackUrl.EndsWith("/"))
            {
                input.CallbackUrl += repoId;
            }
            else
            {
                input.CallbackUrl += "/" + repoId;
            }

            if (input.DiscountDetails.Any() && (input.PaidAmount <= 0 || input.PaidAmount == null))
            {
                await CreatePaymentRecords(input);

                output.PurchasedSuccess = true;

                return output;
            }

            // Omise

            if (input.PaymentOption == "omise" || input.PaymentOption == "paynow")
            {
                OmisePaymentResponseDto omiseChargeResult;

                if (input.PaymentOption == "omise")
                {
                    omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                    {
                        OmiseToken = input.OmiseToken,
                        Amount = Convert.ToInt64(input.PaidAmount * 100),
                        Currency = DineConnectConsts.Currency,
                        ReturnUrl = input.CallbackUrl
                    });
                }
                else
                {
                    omiseChargeResult = await _omisePaymentProcessor.GetStatus(input.OmiseToken);
                }

                if (omiseChargeResult != null)
                {
                    output.Message = omiseChargeResult.Message;
                    output.PurchasedSuccess = omiseChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = output.OmiseId = omiseChargeResult.PaymentId;
                    output.AuthenticateUri = omiseChargeResult.AuthorizeUri;
                }
            }

            // Adyen

            if (input.PaymentOption == "adyen")
            {
                var adyenChargeResult = await _adyenPaymentProcessor.ProcessPayment(new AdyenPaymentRequestDto
                {
                    EncryptedCardNumber = input.AdyenEncryptedCardNumber,
                    EncryptedExpiryMonth = input.AdyenEncryptedExpiryMonth,
                    EncryptedExpiryYear = input.AdyenEncryptedExpiryYear,
                    EncryptedSecurityCode = input.AdyenEncryptedSecurityCode,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency,
                    ReturnUrl = input.CallbackUrl
                });

                if (adyenChargeResult != null)
                {
                    output.Message = adyenChargeResult.Message;
                    output.PurchasedSuccess = adyenChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = adyenChargeResult.PaymentId;
                }
            }

            // Stripe

            if (input.PaymentOption == "stripe")
            {
                var stripeChargeResult = await _stripePaymentProcessor.ProcessPayment(new StripePaymentRequestDto
                {
                    StripeToken = input.StripeToken,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency
                });

                if (stripeChargeResult != null)
                {
                    output.Message = stripeChargeResult.Message;
                    output.PurchasedSuccess = stripeChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = stripeChargeResult.PaymentId;
                }
            }

            // Paypal

            if (input.PaymentOption == "paypal")
            {
                var paypalChargeResult = await _paypalPaymentProcessor.ProcessPayment(new PaypalPaymentRequestDto
                {
                    OrderId = input.PaypalOrderId
                });

                if (paypalChargeResult != null)
                {
                    output.Message = paypalChargeResult.Message;
                    output.PurchasedSuccess = paypalChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = paypalChargeResult.PaymentId;
                }
            }

            // Google

            if (input.PaymentOption == "google")
            {
                var googleChargeResult = await _googlePaymentProcessor.ProcessPayment(new GooglePaymentRequestDto
                {
                    GooglePayToken = input.GooglePayToken,
                    Amount = Convert.ToInt64(input.PaidAmount),
                    Currency = DineConnectConsts.Currency
                });

                if (googleChargeResult != null)
                {
                    output.Message = googleChargeResult.Message;
                    output.PurchasedSuccess = googleChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = googleChargeResult.PaymentId;
                }
            }

            // Swipe

            if (input.PaymentOption == "swipe")
            {
                var swipeChargeResult = await _swipePaymentProcessor.ProcessPayment(new SwipePaymentRequestDto
                {
                    Amount = input.PaidAmount.Value,
                    OTP = input.SwipeOTP,
                    CardNumber = input.SwipeCardNumber
                });

                if (swipeChargeResult != null)
                {
                    output.Message = swipeChargeResult.Message;
                    output.PurchasedSuccess = swipeChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = swipeChargeResult.PaymentId;
                }
            }

            if (output.PurchasedSuccess)
            {
                await CreatePaymentRecords(input);
            }

            return output;
        }

        private async Task<PaymentOutputDto> CreatePaymentRecords(CreatePaymentInputDto input)
        {
            var customer = await _customerRepository.FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);
            var invoiceNo = await _tiffinInvoiceNumberGenerator.GetNewInvoiceNumber();
            var payment = new TiffinPayment
            {
                InvoiceNo = invoiceNo,
                PaymentDate = Clock.Now,
                CustomerId = customer.Id,
                CustomerName = customer.Name,
                CustomerAddress = input.BillingInfo.GetFullAddress(),
                PaidAmount = input.PaidAmount,
                PaymentOption = input.PaymentOption,
                PaymentType = input.PurcharsedOfferList?.Any() == true ? TiffinPaymentType.MealPlan : TiffinPaymentType.Order
            };
            var paymentId = await _tiffinPaymentRepository.InsertAndGetIdAsync(payment);

            await CurrentUnitOfWork.SaveChangesAsync();

            // Handle for buy meal plan / offer
            if (input.PurcharsedOfferList?.Any() == true)
            {
                foreach (var offer in input.PurcharsedOfferList)
                {
                    var customerOffer = new TiffinCustomerProductOffer
                    {
                        ProductOfferId = offer.Id,
                        CustomerId = customer.Id,
                        BalanceCredit = offer.TotalDeliveryCount * offer.Quantity,
                        ActualCredit = offer.TotalDeliveryCount * offer.Quantity,
                        PaidAmount = offer.Price * offer.Quantity,
                        TiffinPaymentId = paymentId,
                        ExpiryDate = payment.PaymentDate.AddDays(offer.DaysOnFirstOrder)
                    };

                    await _tfCustomerProductOfferRepository.InsertAsync(customerOffer);
                }
            }

            // Mask Promotion Code Claimed if have
            if (input.DiscountDetails?.Any() == true)
            {
                var claimedTime = Clock.Now;

                input.DiscountDetails = input.DiscountDetails.DistinctBy(x => x.Id).ToList();

                var promotionCodeIds = input.DiscountDetails.Select(x => x.Id).ToList();

                var promotionCodes = await _tiffinPromotionCode.GetAll()
                    .Where(x => promotionCodeIds.Contains(x.Id) && x.IsClaimed == false)
                    .ToListAsync();

                foreach (var promotionCode in promotionCodes)
                {
                    promotionCode.ClaimedAmount = input.DiscountDetails.First(x => x.Id == promotionCode.Id).ClaimAmount;

                    promotionCode.ClaimedTime = claimedTime;

                    promotionCode.IsClaimed = true;

                    promotionCode.CustomerId = customer.Id;

                    promotionCode.PaymentId = paymentId;

                    await _tiffinPromotionCode.UpdateAsync(promotionCode);
                }
            }

            // Earning Referral for Referrer Checking
            await EarningReferralHandle(customer.Id, paymentId);

            await CurrentUnitOfWork.SaveChangesAsync();

            // Send email about buy meal plan / offer
            if (input.PurcharsedOfferList?.Any() == true)
            {
                await _emailNotificationService.SendEmailWhenCustomerBuyMealPlanInBackground(paymentId, await GetPaymentDetail(paymentId), await GetCompanyInfo());
            }

            return new PaymentOutputDto { PurchasedSuccess = true, PaymentId = paymentId };
        }

        public async Task<TotalOfferDto> GetTotalOffer()
        {
            var query = _tfCustomerProductOfferRepository.GetAll()
                .Include(e => e.ProductOffer)
                .Include(e => e.Customer)
                .Where(c => c.Customer.UserId == AbpSession.UserId)
                .Where(c => c.ProductOffer.Expiry);

            var totalAmount = query.Sum(e => e.PaidAmount);
            var totalCount = await query.CountAsync();
            return new TotalOfferDto
            {
                TotalAmount = totalAmount,
                TotalOffer = totalCount
            };
        }

        public async Task<FileDto> PrintPaymentHistory()
        {
            var payment = await _tiffinPaymentRepository
                .GetAll()
                .Include(t => t.Customer)
                .Include(t => t.CustomerProductOffers)
                .ThenInclude(c => c.ProductOffer)
                .ThenInclude(p => p.ProductOfferSets)
                .ThenInclude(pos => pos.TiffinProductSet)
                .Where(t => t.Customer.UserId == AbpSession.UserId)
                .Select(p => new PaymentCustomerHistoryDto
                {
                    Id = p.Id,
                    InvoiceNo = p.InvoiceNo,
                    PaidAmount = p.PaidAmount,
                    PaymentDate = p.PaymentDate,
                    ModeOfPayment = p.PaymentOption
                }).ToListAsync();
            return _tfPaymentExporter.PrintPayment(payment);
        }

        public async Task<FileDto> PrintPaymentDetailHistory(long paymentId)
        {
            var paymentDetail = await GetPaymentDetail(paymentId);
            var companyInfo = await GetCompanyInfo();

            return _tfPaymentExporter.PrintPaymentDetail(paymentDetail, companyInfo);
        }

        public async Task<CompanySettingDto> GetCompanyInfo()
        {
            var companyNameJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.CompanySetting.CompanyName,
                AbpSession.GetTenantId());

            var companyLogoUrlJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CompanySetting.CompanyLogoUrl,
                    AbpSession.GetTenantId());

            var companyAddress =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingAddress,
                    AbpSession.GetTenantId());

            var taxNo =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingTaxVatNo,
                    AbpSession.GetTenantId());
            var companyName = string.Empty;
            if (!companyNameJson.IsNullOrEmpty())
            {
                companyName = JsonConvert.DeserializeObject<string>(companyNameJson);
            }

            var companyLogoUrl = string.Empty;
            if (!companyLogoUrlJson.IsNullOrEmpty())
            {
                companyLogoUrl = JsonConvert.DeserializeObject<string>(companyLogoUrlJson);
            }

            return new CompanySettingDto
            {
                CompanyName = companyName,
                CompanyLogoUrl = companyLogoUrl,
                CompanyAddress = companyAddress,
                TaxNo = taxNo
            };
        }

        public async Task<PaymentOutputDto> ValidatePayment(EntityDto<int> input)
        {
            PaymentOutputDto output = new PaymentOutputDto();

            if (input.Id > 0)
            {
                var paymentRequest = await _tiffinPaymentRequestRepo.GetAsync(input.Id);

                if (paymentRequest != null)
                {
                    var changeResult = await _omisePaymentProcessor.GetStatus(paymentRequest.PaymentReference);

                    output.Message = changeResult.Message;
                    output.OmiseId = changeResult.PaymentId;
                    output.PurchasedSuccess = changeResult.IsSuccess;

                    if (changeResult.IsSuccess)
                    {
                        var myReturn = await CreatePaymentRecords(
                            JsonConvert.DeserializeObject<CreatePaymentInputDto>(paymentRequest.Request));

                        output.PaymentId = myReturn.PaymentId;

                        await _tiffinPaymentRequestRepo.DeleteAsync(input.Id);
                    }
                }
            }
            else
            {
                output.PurchasedSuccess = false;
                output.Message = "No Payment Request";
            }

            return output;
        }

        [AbpAllowAnonymous]
        public async Task SendFeedback(FeedbackEmailInput feedback)
        {
            // Send Email
            await _emailSendAppService.SendAsyc(feedback.Email, feedback.Subject, feedback.Feedback, false, AbpSession.GetTenantId());

            // Send Whatapp
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            if(!string.IsNullOrEmpty(allSettings.General.AdminWhatappNumber))
                await _tenantWhatappSendAppService.SendWhatappAsync(allSettings.General.AdminWhatappNumber, feedback.Feedback);
        }

        private async Task EarningReferralHandle(int customerId, int paymentId)
        {
            // Referral Code

            var customerReferral = await _tiffinCustomerReferralRepository.GetAll()
                .FirstOrDefaultAsync(x => x.CustomerId == customerId);

            if (customerReferral?.ReferrerCustomerId != null)
            {
                var tenantSettings = await _tenantSettingsAppService.GetAllSettings();

                // Only adding earning value for the first payment / only 1 time earning referral value

                if (!customerReferral.ReferrerCustomerEarnedTime.HasValue)
                {
                    customerReferral.ReferrerCustomerEarnedTime = Clock.Now;

                    customerReferral.ReferrerCustomerEarnedType = tenantSettings.ReferralEarning.ReferralEarnType;

                    customerReferral.ReferrerCustomerEarnedValue = tenantSettings.ReferralEarning.ReferralEarnValue;

                    customerReferral.ReferrerCustomerEarnedByPaymentId = paymentId;

                    // Handle for referral type, free product offer or generate promotion code

                    var referrerCustomer = await _customerRepository.GetAll().Include(x => x.User)
                        .FirstOrDefaultAsync(t => t.Id == customerReferral.ReferrerCustomerId);

                    if (customerReferral.ReferrerCustomerEarnedType == ReferralEarningType.Promotion)
                    {
                        var tiffinPromotionAppService = _serviceProvider.GetService<ITiffinPromotionAppService>();

                        var promotionCode =
                            await tiffinPromotionAppService
                                .GeneratePromotionCodeForReferralEarning(referrerCustomer.Id);

                        if (!string.IsNullOrWhiteSpace(promotionCode))
                        {
                            customerReferral.ReferrerCustomerEarnedRemarks =
                                $"Referrer Earned Promotion Code {promotionCode}";
                        }
                    }
                    else if (customerReferral.ReferrerCustomerEarnedType == ReferralEarningType.ProductOffer)
                    {
                        var referralInvoiceNo = await _tiffinInvoiceNumberGenerator.GetNewInvoiceNumber();

                        var referralEarnedPayment = new TiffinPayment
                        {
                            InvoiceNo = referralInvoiceNo,
                            PaymentDate = Clock.Now,
                            CustomerId = referrerCustomer.Id,
                            CustomerName = referrerCustomer.Name,
                            CustomerAddress = null,
                            PaidAmount = 0, // FREE
                            PaymentOption = "Referral"
                        };

                        var referralEarnedPaymentId =
                            await _tiffinPaymentRepository.InsertAndGetIdAsync(referralEarnedPayment);

                        var referralProductOffer =
                            await _tiffinProductOfferRepository.FirstOrDefaultAsync(t =>
                                t.Id == customerReferral.ReferrerCustomerEarnedValue);

                        var referralCustomerOffer = new TiffinCustomerProductOffer
                        {
                            ProductOfferId = referralProductOffer.Id,
                            CustomerId = referrerCustomer.Id,
                            BalanceCredit = referralProductOffer.TotalDeliveryCount,
                            ActualCredit = referralProductOffer.TotalDeliveryCount,
                            PaidAmount = 0, // FREE
                            TiffinPaymentId = referralEarnedPaymentId,
                            ExpiryDate =
                                referralEarnedPayment.PaymentDate.AddDays(referralProductOffer.DaysOnFirstOrder)
                        };

                        await _tfCustomerProductOfferRepository.InsertAsync(referralCustomerOffer);

                        customerReferral.ReferrerCustomerEarnedRemarks =
                            $"Referrer Earned Product Offer {referralProductOffer.Id}";

                        // Send Email

                        var emailInformation = new SendEmailInput
                        {
                            EmailAddress = referrerCustomer.EmailAddress ?? referrerCustomer.User.EmailAddress,
                            Module = "ReferralEarningProductOffer",
                            Variables = new
                            {
                                CustomerName = referrerCustomer.Name,
                                ProductOfferName = referralProductOffer.Name,
                                BalanceCredit = referralProductOffer.TotalDeliveryCount.ToString()
                            },
                            TenantId = referrerCustomer.TenantId
                        };

                        await _tenantEmailSendAppService.SendEmailByTemplateAsync(emailInformation);
                    }

                    // Update customer info

                    await _tiffinCustomerReferralRepository.UpdateAsync(customerReferral);

                    await CurrentUnitOfWork.SaveChangesAsync();
                }
            }
        }
    }
}
﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using IdentityServer4.Extensions;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public class TiffinPaymentExporter : NpoiExcelExporterBase, ITiffinPaymentExporter
    {
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public TiffinPaymentExporter(
            ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager
                )
        {
            _tempFileCacheManager = tempFileCacheManager;
        }

        public FileDto PrintPayment(List<PaymentCustomerHistoryDto> listPayment)
        {
            return GeneratePayment(listPayment);
        }

        public FileDto PrintPaymentDetail(TiffinPaymentDetailDto paymentDetail, CompanySettingDto companyInfo)
        {
            //return GeneratePaymentDetail(paymentDetail);
            return GenerateInvoice(paymentDetail, companyInfo);
        }

        protected FileDto GeneratePayment(List<PaymentCustomerHistoryDto> listPayment)
        {
            var document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            var normalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            var boldFont = FontFactory.GetFont("Arial", 13, Font.BOLD, BaseColor.BLACK);
            var titleFont = FontFactory.GetFont("Arial", 18);
            using (var memoryStream = new System.IO.MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPTable table = null;

                document.Open();

                // Add title
                var title = new Paragraph("Payment History", titleFont)
                {
                    Alignment = Element.ALIGN_LEFT,
                    SpacingAfter = 20
                };
                document.Add(title);

                //Add table
                table = new PdfPTable(4)
                {
                    TotalWidth = 400f,
                    LockedWidth = true
                };
                PdfPCell dateTitleCell = new PdfPCell(new Phrase("Date", boldFont));
                dateTitleCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(dateTitleCell);

                PdfPCell invoiceNoCell = new PdfPCell(new Phrase("InvoiceNo", boldFont));
                invoiceNoCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(invoiceNoCell);

                PdfPCell amountTitleCell = new PdfPCell(new Phrase("Amount", boldFont));
                amountTitleCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(amountTitleCell);

                PdfPCell modeTitleCell = new PdfPCell(new Phrase("Mode of payment", boldFont));
                modeTitleCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(modeTitleCell);

                foreach (var item in listPayment)
                {
                    table.AddCell(new PdfPCell(new Phrase(item.PaymentDate.ToString("dd/MM/yyyy"), normalFont)));
                    table.AddCell(new PdfPCell(new Phrase(item.InvoiceNo, normalFont)));

                    PdfPCell amountCell = item.PaidAmount.HasValue
                        ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " +item.PaidAmount.Value.ToString("0.00"),
                            normalFont))
                        : new PdfPCell(new Phrase("", normalFont));
                    amountCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    amountCell.PaddingLeft = 10f;
                    table.AddCell(amountCell);

                    PdfPCell modePaymentCell = new PdfPCell(new Phrase(item.ModeOfPayment, normalFont));
                    modePaymentCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(modePaymentCell);
                }
                
                document.Add(table);

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = "PaymentHistory.pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        protected FileDto GeneratePaymentDetail(TiffinPaymentDetailDto paymentDetail)
        {
            var document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            var normalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            var boldFont = FontFactory.GetFont("Arial", 15, Font.BOLD, BaseColor.BLACK);
            var titleFont = FontFactory.GetFont("Arial", 16);
            var infoFont = FontFactory.GetFont("Arial", 13);
            const float spacing = 20;
            using (var memoryStream = new System.IO.MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPTable table = null;

                document.Open();

                // Add title
                var c1 = new Paragraph
                {
                    new Chunk("INVOICE: " + paymentDetail.InvoiceNo, titleFont),
                    Chunk.TABBING,
                    Chunk.TABBING,
                    new Chunk("Date order : " + paymentDetail.PaymentDate.ToString("dd/MM/yyyy"), titleFont)
                };
                c1.SpacingAfter = spacing;
                document.Add(c1);

                // Add info
                var c2 = new Paragraph
                {
                    new Chunk("Billed To: " + paymentDetail.CustomerName + "\n " + paymentDetail.CustomerAddress, infoFont),
                };
                c2.SpacingAfter = spacing;
                document.Add(c2);

                var c3 = new Paragraph
                {
                    new Chunk("Billed To: " + paymentDetail.CustomerName + "\n " + paymentDetail.CustomerAddress, infoFont),
                };
                c3.SpacingAfter = spacing;
                document.Add(c3);

                var c4 = new Paragraph
                {
                    new Chunk("Payment Method: " + paymentDetail.PaymentOption, infoFont),
                };
                c4.SpacingAfter = spacing;
                document.Add(c4);


                //Add table
                table = new PdfPTable(4)
                {
                    TotalWidth = 400f,
                    LockedWidth = true
                };

                table.AddCell(new PdfPCell(new Phrase("Product Offers", boldFont)));
                table.AddCell(new PdfPCell(new Phrase("Price", boldFont)));
                table.AddCell(new PdfPCell(new Phrase("Quantity", boldFont)));
                table.AddCell(new PdfPCell(new Phrase("Total", boldFont)));

                foreach (var product in paymentDetail.Products)
                {
                    table.AddCell(new PdfPCell(new Phrase(product.ProductOfferName, normalFont)));
                    table.AddCell(product.Price != null
                        ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + product.Price.Value.ToString("0.00"), normalFont))
                        : new PdfPCell(new Phrase("", normalFont)));
                    table.AddCell(new PdfPCell(new Phrase(product.Quantity.ToString(), normalFont)));
                    table.AddCell(product.PaidAmount != null
                        ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + product.PaidAmount.Value.ToString("0.00"), normalFont))
                        : new PdfPCell(new Phrase("", normalFont)));
                }


                table.AddCell(new PdfPCell(new Phrase("", boldFont)));
                table.AddCell(new PdfPCell(new Phrase("", boldFont)));
                table.AddCell(new PdfPCell(new Phrase("Total", boldFont)));

                table.AddCell(paymentDetail.PaidAmount != null
                    ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + paymentDetail.PaidAmount.Value.ToString("0.00"), boldFont))
                    : new PdfPCell(new Phrase("", boldFont)));


                document.Add(table);

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = $"Invoice-{paymentDetail.InvoiceNo}.pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        public FileDto  GenerateInvoice(TiffinPaymentDetailDto paymentDetail, CompanySettingDto companyInfo)
        {
            using (var PDFData = new System.IO.MemoryStream())
            {
                Document pdfDoc = new Document(PageSize.A4, 30, 30, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PDFData);

                var titleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
                var titleFontBlue = FontFactory.GetFont("Arial", 14, Font.NORMAL, BaseColor.BLUE);
                var boldTableFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
                var bodyFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
                var emailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
                BaseColor TabelHeaderBackGroundColor = WebColors.GetRGBColor("#EEEEEE");

                Rectangle pageSize = writer.PageSize;
                // Open the Document for writing
                pdfDoc.Open();
                //Add elements to the document here

                #region Top table
                // Create the header table 
                PdfPTable headertable = new PdfPTable(3);
                headertable.HorizontalAlignment = 0;
                headertable.WidthPercentage = 100;
                headertable.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                headertable.DefaultCell.Border = Rectangle.BOX; //for testing           

                var imageUrl = companyInfo.CompanyLogoUrl;

                if (!imageUrl.IsNullOrEmpty())
                {
                    Image logo = Image.GetInstance(imageUrl);
                    logo.ScaleToFit(200, 45);

                    {
                        PdfPCell pdfCelllogo = new PdfPCell(logo);
                        pdfCelllogo.Border = Rectangle.NO_BORDER;
                        pdfCelllogo.PaddingBottom = 10f;
                        //pdfCelllogo.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        //pdfCelllogo.BorderWidthBottom = 1f;
                        headertable.AddCell(pdfCelllogo);
                    }
                }


                {
                    PdfPCell middlecell = new PdfPCell();
                    middlecell.Border = Rectangle.NO_BORDER;
                    //middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //middlecell.BorderWidthBottom = 1f;
                    headertable.AddCell(middlecell);
                }

                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase(companyInfo.CompanyName, titleFont));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);

                    PdfPCell nextPostCell1Address = new PdfPCell(new Phrase(companyInfo.CompanyAddress, bodyFont));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1Address);

                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //nesthousing.BorderWidthBottom = 1f;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    headertable.AddCell(nesthousing);
                }


                PdfPTable Invoicetable = new PdfPTable(3);
                Invoicetable.HorizontalAlignment = 0;
                Invoicetable.WidthPercentage = 100;
                Invoicetable.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                Invoicetable.DefaultCell.Border = Rectangle.NO_BORDER;

                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("INVOICE TO:", bodyFont));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);
                    PdfPCell nextPostCell2 = new PdfPCell(new Phrase(paymentDetail.CustomerName, titleFont));
                    nextPostCell2.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell2);
                    PdfPCell nextPostCell3 = new PdfPCell(new Phrase(paymentDetail.CustomerAddress, bodyFont));
                    nextPostCell3.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell3);
                    PdfPCell nextPostCell4 = new PdfPCell(new Phrase(paymentDetail.EmailAddress, emailFont));
                    nextPostCell4.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell4);
                    nested.AddCell("");
                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //nesthousing.BorderWidthBottom = 1f;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    Invoicetable.AddCell(nesthousing);
                }

                {
                    PdfPCell middlecell = new PdfPCell();
                    middlecell.Border = Rectangle.NO_BORDER;
                    //middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //middlecell.BorderWidthBottom = 1f;
                    Invoicetable.AddCell(middlecell);
                }


                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("INVOICE #" + paymentDetail.InvoiceNo, titleFontBlue));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);
                    PdfPCell nextPostCell2 = new PdfPCell(new Phrase("TaxNo: " + companyInfo.TaxNo, bodyFont));
                    nextPostCell2.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell2);
                    PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Date of Invoice: " + paymentDetail.PaymentDate.ToShortDateString(), bodyFont));
                    nextPostCell3.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell3);
                    //PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Due Date: " + DateTime.Now.AddDays(30).ToShortDateString(), bodyFont));
                    //nextPostCell3.Border = Rectangle.NO_BORDER;
                    //nested.AddCell(nextPostCell3);
                    nested.AddCell("");
                    nested.AddCell("");
                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //nesthousing.BorderWidthBottom = 1f;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    Invoicetable.AddCell(nesthousing);
                }


                pdfDoc.Add(headertable);
                Invoicetable.PaddingTop = 10f;

                pdfDoc.Add(Invoicetable);
                #endregion

                #region Items Table
                //Create body table
                PdfPTable itemTable = new PdfPTable(5);

                itemTable.HorizontalAlignment = 0;
                itemTable.WidthPercentage = 100;
                itemTable.SetWidths(new float[] { 5, 40, 10, 20, 25 });  // then set the column's __relative__ widths
                itemTable.SpacingAfter = 40;
                itemTable.DefaultCell.Border = Rectangle.BOX;
                PdfPCell cell1 = new PdfPCell(new Phrase("NO", boldTableFont));
                cell1.BackgroundColor = TabelHeaderBackGroundColor;
                cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell1);
                PdfPCell cell2 = new PdfPCell(new Phrase("PRODUCT OFFERS", boldTableFont));
                cell2.BackgroundColor = TabelHeaderBackGroundColor;
                cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell2);
                PdfPCell cell3 = new PdfPCell(new Phrase("PRICE", boldTableFont));
                cell3.BackgroundColor = TabelHeaderBackGroundColor;
                cell3.HorizontalAlignment = Element.ALIGN_RIGHT;
                itemTable.AddCell(cell3);
                PdfPCell cell4 = new PdfPCell(new Phrase("QUANTITY", boldTableFont));
                cell4.BackgroundColor = TabelHeaderBackGroundColor;
                cell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                itemTable.AddCell(cell4);
                PdfPCell cell5 = new PdfPCell(new Phrase("TOTAL", boldTableFont));
                cell5.BackgroundColor = TabelHeaderBackGroundColor;
                cell5.HorizontalAlignment = Element.ALIGN_RIGHT;
                itemTable.AddCell(cell5);
                int rowIndex = 1;
                foreach (var row in paymentDetail.Products)
                {
                    PdfPCell numberCell = new PdfPCell(new Phrase(rowIndex.ToString(), bodyFont));
                    numberCell.HorizontalAlignment = 1;
                    numberCell.PaddingLeft = 10f;
                    numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    itemTable.AddCell(numberCell);

                    PdfPCell descCell = new PdfPCell(new Phrase(row.ProductOfferName, bodyFont));
                    descCell.HorizontalAlignment = 0;
                    descCell.PaddingLeft = 10f;
                    descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    itemTable.AddCell(descCell);

                    PdfPCell qtyCell = row.Price.HasValue
                        ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + row.Price.Value.ToString("0.00"), bodyFont))
                        : new PdfPCell(new Phrase("", bodyFont));
                    qtyCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    qtyCell.PaddingLeft = 10f;
                    qtyCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    itemTable.AddCell(qtyCell);

                    PdfPCell amountCell = new PdfPCell(new Phrase(row.Quantity.ToString(), bodyFont));
                    amountCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    amountCell.PaddingLeft = 10f;
                    amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    itemTable.AddCell(amountCell);

                    PdfPCell totalamtCell = row.PaidAmount.HasValue 
                        ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + row.PaidAmount.Value.ToString("0.00"), bodyFont))
                        : new PdfPCell(new Phrase("", bodyFont));
                    totalamtCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    itemTable.AddCell(totalamtCell);

                    rowIndex++;
                }
                // Table footer
                PdfPCell cardChargeCell1 = new PdfPCell(new Phrase(""));
                cardChargeCell1.Border = Rectangle.TOP_BORDER;
                itemTable.AddCell(cardChargeCell1);
                PdfPCell cardChargeCell2 = new PdfPCell(new Phrase(""));
                cardChargeCell2.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                itemTable.AddCell(cardChargeCell2);
                PdfPCell cardChargeCell3 = new PdfPCell(new Phrase(""));
                cardChargeCell3.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                itemTable.AddCell(cardChargeCell3);
                PdfPCell cardChargeCell4 = new PdfPCell(new Phrase("Card Charges", boldTableFont));
                cardChargeCell4.Border = Rectangle.TOP_BORDER;   //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                cardChargeCell4.HorizontalAlignment = Element.ALIGN_LEFT;
                itemTable.AddCell(cardChargeCell4);
                PdfPCell cardChargeCell5 = paymentDetail.CardCharge.HasValue
                    ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + paymentDetail.CardCharge.Value.ToString("0.00"), boldTableFont))
                    : new PdfPCell(new Phrase("", boldTableFont));
                cardChargeCell5.Border = Rectangle.TOP_BORDER;
                cardChargeCell5.HorizontalAlignment = Element.ALIGN_RIGHT;
                itemTable.AddCell(cardChargeCell5);

                // Table footer
                PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(""));
                totalAmtCell1.Border = Rectangle.NO_BORDER;
                itemTable.AddCell(totalAmtCell1);
                PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(""));
                totalAmtCell2.Border = Rectangle.NO_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                itemTable.AddCell(totalAmtCell2);
                PdfPCell totalAmtCell3 = new PdfPCell(new Phrase(""));
                totalAmtCell3.Border = Rectangle.NO_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                itemTable.AddCell(totalAmtCell3);
                PdfPCell totalAmtStrCell = new PdfPCell(new Phrase("Total", boldTableFont));
                totalAmtStrCell.Border = Rectangle.NO_BORDER;   //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                totalAmtStrCell.HorizontalAlignment = Element.ALIGN_LEFT;
                itemTable.AddCell(totalAmtStrCell);
                PdfPCell totalAmtCell = paymentDetail.PaidAmount.HasValue
                    ? new PdfPCell(new Phrase(DineConnectConsts.CurrencySign + " " + paymentDetail.PaidAmount.Value.ToString("0.00"), boldTableFont))
                    : new PdfPCell(new Phrase("", boldTableFont));
                totalAmtCell.Border = Rectangle.NO_BORDER;
                totalAmtCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                itemTable.AddCell(totalAmtCell);

                pdfDoc.Add(itemTable);
                #endregion

                pdfDoc.Close();

                byte[] bytes = PDFData.ToArray();
                PDFData.Close();

                var file = new FileDto()
                {
                    FileName = $"Invoice-{paymentDetail.InvoiceNo}" + DateTime.Now.ToString("dd-MM-yyyy-HH:mm") + ".pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

    }
}

using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Tiffins.Faq.Dtos;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
namespace DinePlan.DineConnect.Tiffins.Faq
{
    
    public class TiffinFaqAppService : DineConnectAppServiceBase, ITiffinFaqAppService
    {
        private readonly IRepository<TiffinFaq> _faqRepository;

        public TiffinFaqAppService(IRepository<TiffinFaq> faqRepository)
        {
            _faqRepository = faqRepository;
        }

        [AbpAllowAnonymous]
        public async Task<ListResultDto<FaqListDto>> GetFaqs()
        {
            var faqs = await _faqRepository
                .GetAll()
                .OrderBy(x => x.DisplayOrder)
                .ThenBy(x => x.Questions)
                .Select(x => ObjectMapper.Map<FaqListDto>(x))
                .ToListAsync();

            return new ListResultDto<FaqListDto>(faqs);
        }

        public async Task<FaqEditDto> GetFaqForEdit(EntityDto input)
        {
            var faq = await _faqRepository.GetAsync(input.Id);

            return ObjectMapper.Map<FaqEditDto>(faq);
        }

        
        public async Task CreateFaq(FaqCreateDto input)
        {
            var faq = ObjectMapper.Map<TiffinFaq>(input);

            CheckUniqueQuestion(faq.Questions);

            await _faqRepository.InsertAsync(faq);
        }

        
        public async Task UpdateFaq(FaqEditDto input)
        {
            var faq = ObjectMapper.Map<TiffinFaq>(input);

            if (AbpSession.TenantId != null)
            {
                faq.TenantId = AbpSession.TenantId.Value;
            }

            CheckUniqueQuestion(faq.Questions, faq.Id);

            await _faqRepository.UpdateAsync(faq);
        }

        
        public async Task DeleteFaq(EntityDto input)
        {
            await _faqRepository.DeleteAsync(input.Id);
        }

        public void CheckUniqueQuestion(string question, int? excludeId = null)
        {
            var isExistQuestionQuery = _faqRepository.GetAll().Where(x => x.Questions == question);

            if (excludeId.HasValue)
            {
                isExistQuestionQuery = isExistQuestionQuery.Where(x => x.Id != excludeId);
            }

            var isExist = isExistQuestionQuery.Any();

            if (isExist)
            {
                throw new UserFriendlyException(L("TiffinFaqQuestionIsMustUnique"));
            }
        }
    }
}
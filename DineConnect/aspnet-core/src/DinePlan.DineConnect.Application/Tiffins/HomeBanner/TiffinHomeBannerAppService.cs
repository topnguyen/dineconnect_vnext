using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Tiffins.HomeBanner.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.HomeBanner
{
    
    public class TiffinHomeBannerAppService : DineConnectAppServiceBase, ITiffinHomeBannerAppService
    {
        private readonly IRepository<TiffinHomeBanner> _homeBannerRepository;

        public TiffinHomeBannerAppService(IRepository<TiffinHomeBanner> homeBannerRepository)
        {
            _homeBannerRepository = homeBannerRepository;
        }

        [AbpAllowAnonymous]
        public async Task<HomeBannerEditDto> GetHomeBannerForEdit()
        {
            var homeBanner = await _homeBannerRepository.GetAll().FirstOrDefaultAsync() ?? new TiffinHomeBanner();

            return ObjectMapper.Map<HomeBannerEditDto>(homeBanner);
        }

        
        public async Task UpdateHomeBanner(HomeBannerEditDto input)
        {
            var homeBanner = await _homeBannerRepository.GetAll().FirstOrDefaultAsync() ?? new TiffinHomeBanner();

            homeBanner.Images = input.Images;
            
            if (AbpSession.TenantId != null)
            {
                homeBanner.TenantId = AbpSession.TenantId.Value;
            }
            
            await _homeBannerRepository.InsertOrUpdateAsync(homeBanner);
        }
    }
}
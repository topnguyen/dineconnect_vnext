﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Tiffins.OrderTags.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.OrderTags
{
    
    public class TiffinOrderTagsAppService : DineConnectAppServiceBase, ITiffinOrderTagsAppService
    {
        private readonly IRepository<TiffinOrderTag> _tiffinOrderTagRepository;

        public TiffinOrderTagsAppService(IRepository<TiffinOrderTag> tiffinOrderTagRepository)
        {
            _tiffinOrderTagRepository = tiffinOrderTagRepository;
        }

        public async Task<PagedResultDto<TiffinOrderTagDto>> GetAll(GetAllTiffinOrderTagsInput input)
        {
            var filteredTiffinOrderTags = _tiffinOrderTagRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredTiffinOrderTags = filteredTiffinOrderTags
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var tiffinOrderTags = pagedAndFilteredTiffinOrderTags
                                 .Select(o => ObjectMapper.Map<TiffinOrderTagDto>(o));

            var totalCount = await filteredTiffinOrderTags.CountAsync();

            return new PagedResultDto<TiffinOrderTagDto>(
                totalCount,
                await tiffinOrderTags.ToListAsync()
            );
        }

        
        public async Task<CreateOrEditTiffinOrderTagDto> GetTiffinOrderTagForEdit(EntityDto input)
        {
            var tiffinOrderTag = await _tiffinOrderTagRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTiffinOrderTagDto>(tiffinOrderTag);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTiffinOrderTagDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTiffinOrderTagDto input)
        {
            var tiffinOrderTag = ObjectMapper.Map<TiffinOrderTag>(input);

            if (AbpSession.TenantId != null)
            {
                tiffinOrderTag.TenantId = (int)AbpSession.TenantId;
            }

            await _tiffinOrderTagRepository.InsertAsync(tiffinOrderTag);
        }

        
        protected virtual async Task Update(CreateOrEditTiffinOrderTagDto input)
        {
            var tiffinOrderTag = await _tiffinOrderTagRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, tiffinOrderTag);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _tiffinOrderTagRepository.DeleteAsync(input.Id);
        }
    }
}
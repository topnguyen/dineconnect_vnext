﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.ProductOffers.Dtos;
using DinePlan.DineConnect.Tiffins.ProductOffers.Exporting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.ProductOffers
{
    
    public class TiffinProductOffersAppService : DineConnectAppServiceBase, ITiffinProductOffersAppService
    {
        private readonly IRepository<TiffinProductOffer> _tiffinProductOfferRepository;
        private readonly ITiffinProductOffersExcelExporter _tiffinProductOffersExcelExporter;
        private readonly IRepository<TiffinProductSet, int> _tiffinProductSetRepository;
        private readonly IRepository<TiffinProductOfferType, int> _tiffinProductOfferTypeRepository;
        private readonly IRepository<TiffinProductOfferSet> _tiffinProductOfferSetRepository;
        private readonly ICloudImageUploader _cloudImageUploader;
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public TiffinProductOffersAppService(IRepository<TiffinProductOffer> tiffinProductOfferRepository
            , ITiffinProductOffersExcelExporter tiffinProductOffersExcelExporter
            , IRepository<TiffinProductSet, int> tiffinProductSetRepository
            , IRepository<TiffinProductOfferType, int> tiffinProductOfferTypeRepository
            , IRepository<TiffinProductOfferSet> tiffinProductOfferSetRepository
            , ICloudImageUploader cloudImageUploader
            , ITempFileCacheManager tempFileCacheManager)
        {
            _tiffinProductOfferRepository = tiffinProductOfferRepository;
            _tiffinProductOffersExcelExporter = tiffinProductOffersExcelExporter;
            _tiffinProductSetRepository = tiffinProductSetRepository;
            _tiffinProductOfferTypeRepository = tiffinProductOfferTypeRepository;
            _tiffinProductOfferSetRepository = tiffinProductOfferSetRepository;
            _cloudImageUploader = cloudImageUploader;
            _tempFileCacheManager = tempFileCacheManager;
        }

        public async Task<PagedResultDto<GetTiffinProductOfferForViewDto>> GetAll(GetAllTiffinProductOffersInput input)
        {
            var filteredTiffinProductOffers = _tiffinProductOfferRepository.GetAll()
                        .Include(e => e.ProductOfferSets)
                        .ThenInclude(oc => oc.TiffinProductSet)
                        .Include(e => e.TiffinProductOfferType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Description, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Images, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Tag, input.Filter.ToILikeString()))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.TiffinProductSetNameFilter), e => e.ProductSet != null && e.ProductSet.Name == input.TiffinProductSetNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TiffinProductOfferTypeNameFilter), e => e.TiffinProductOfferType != null && e.TiffinProductOfferType.Name == input.TiffinProductOfferTypeNameFilter);

            var pagedAndFilteredTiffinProductOffers = filteredTiffinProductOffers
                .OrderBy(input.Sorting ?? "SortOrder")
                .PageBy(input);

            var tiffinProductOffers = pagedAndFilteredTiffinProductOffers.
                                      Select(o => new GetTiffinProductOfferForViewDto()
                                      {
                                          TiffinProductOffer = ObjectMapper.Map<TiffinProductOfferDto>(o),
                                          //TiffinProductSetName = o.ProductSet.Name,
                                          TiffinProductOfferTypeName = o.TiffinProductOfferType.Name
                                      });

            var totalCount = await filteredTiffinProductOffers.CountAsync();

            return new PagedResultDto<GetTiffinProductOfferForViewDto>(
                totalCount,
                await tiffinProductOffers.ToListAsync()
            );
        }

        
        public async Task<GetTiffinProductOfferForEditOutput> GetTiffinProductOfferForEdit(NullableIdDto input)
        {
            var output = new GetTiffinProductOfferForEditOutput();

            if (input.Id.HasValue)
            {
                var tiffinProductOffer = await _tiffinProductOfferRepository.GetAll()
                    .Include(e => e.ProductOfferSets)
                    .ThenInclude(oc => oc.TiffinProductSet)
                    .Include(e => e.TiffinProductOfferType)
                    .Where(e => e.Id == input.Id.Value)
                    .FirstOrDefaultAsync();
                output = new GetTiffinProductOfferForEditOutput { TiffinProductOffer = ObjectMapper.Map<CreateOrEditTiffinProductOfferDto>(tiffinProductOffer) };

                output.TiffinProductOfferTypeName = tiffinProductOffer.TiffinProductOfferType?.Name;
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTiffinProductOfferDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (!input.Expiry)
            {
                input.DaysOnFirstOrder = 0;
            }

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTiffinProductOfferDto input)
        {
            var tiffinProductOffer = ObjectMapper.Map<TiffinProductOffer>(input);

            if (AbpSession.TenantId != null)
            {
                tiffinProductOffer.TenantId = (int)AbpSession.TenantId;
            }

            var id = await _tiffinProductOfferRepository.InsertAndGetIdAsync(tiffinProductOffer);

            CurrentUnitOfWork.SaveChanges();

            if (input.ProductSets.Any())
            {
                input.ProductSets.ForEach(item =>
                {
                    var productSetOffer = new TiffinProductOfferSet
                    {
                        TiffinProductOfferId = id,
                        TiffinProductSetId = item.Id
                    };

                    _tiffinProductOfferSetRepository.InsertAsync(productSetOffer);
                });
            }
        }

        
        protected virtual async Task Update(CreateOrEditTiffinProductOfferDto input)
        {
            var tiffinProductOffer = await _tiffinProductOfferRepository.GetAll()
                    .Include(e => e.ProductOfferSets)
                    .ThenInclude(sn => sn.TiffinProductSet)
                    .Include(e => e.TiffinProductOfferType)
                    .Where(e => e.Id == input.Id.Value)
                    .FirstOrDefaultAsync();

            ObjectMapper.Map(input, tiffinProductOffer);

            (await _tiffinProductOfferSetRepository.GetAll().Where(e => e.TiffinProductOfferId == tiffinProductOffer.Id).ToListAsync()).ForEach(r =>
            {
                _tiffinProductOfferSetRepository.Delete(r);
            });

            if (input.ProductSets.Any())
            {
                input.ProductSets.ForEach(item =>
                {
                    var productSetOffer = new TiffinProductOfferSet
                    {
                        TiffinProductOfferId = input.Id.Value,
                        TiffinProductSetId = item.Id
                    };

                    _tiffinProductOfferSetRepository.Insert(productSetOffer);
                });
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            await _tiffinProductOfferRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetTiffinProductOffersToExcel(GetAllTiffinProductOffersForExcelInput input)
        {
            var filteredTiffinProductOffers = _tiffinProductOfferRepository.GetAll()
                        .Include(e => e.ProductSets)
                        .Include(e => e.TiffinProductOfferType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Description, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Images, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Tag, input.Filter.ToILikeString()))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TiffinProductOfferTypeNameFilter), e => e.TiffinProductOfferType != null && e.TiffinProductOfferType.Name == input.TiffinProductOfferTypeNameFilter);

            var query = filteredTiffinProductOffers.
                                      Select(o => new GetTiffinProductOfferForViewDto()
                                      {
                                          TiffinProductOffer = ObjectMapper.Map<TiffinProductOfferDto>(o),
                                          TiffinProductSetName = o.ProductSets.Select(e => e.Name).JoinAsString("; "),
                                          TiffinProductOfferTypeName = o.TiffinProductOfferType.Name
                                      });

            var tiffinProductOfferListDtos = await query.ToListAsync();

            return _tiffinProductOffersExcelExporter.ExportToFile(tiffinProductOfferListDtos);
        }

        
        public async Task<PagedResultDto<TiffinProductOfferLookupTableDto>> GetAllTiffinProductSetForLookupTable(GetAllForLookupTableInput input)
        {
            var allList = _tiffinProductSetRepository.GetAll()
                .WhereIf(!input.Filter.IsNullOrEmpty(), t =>
                EF.Functions.Like(t.Name, input.Filter.ToILikeString()))
                .Select(t => new TiffinProductOfferLookupTableDto
                {
                    Id = t.Id,
                    Name = t.Name,
                    Value = t.Id.ToString()
                });

            var result = await allList.OrderBy(t => t.Name).PageBy(input).ToListAsync();

            return new PagedResultDto<TiffinProductOfferLookupTableDto>(allList.Count(), result);
        }

        
        public async Task<PagedResultDto<TiffinProductOfferLookupTableDto>> GetAllTiffinProductOfferTypeForLookupTable(GetAllForLookupTableInput input)
        {
            var allList = _tiffinProductOfferTypeRepository.GetAll()
                .WhereIf(!input.Filter.IsNullOrEmpty(), t =>
                 EF.Functions.Like(t.Name, input.Filter.ToILikeString()))
                .Select(t => new TiffinProductOfferLookupTableDto
                {
                    Id = t.Id,
                    Name = t.Name,
                    Value = t.Id.ToString()
                });

            var result = await allList.OrderBy(t => t.Name).PageBy(input).ToListAsync();

            return new PagedResultDto<TiffinProductOfferLookupTableDto>(allList.Count(), result);
        }

        protected void ValidateForCreateOrUpdate(CreateOrEditTiffinProductOfferDto input)
        {
            var exists = _tiffinProductOfferRepository.GetAll()
                .Where(e => e.Name.Equals(input.Name))
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }
    }
}
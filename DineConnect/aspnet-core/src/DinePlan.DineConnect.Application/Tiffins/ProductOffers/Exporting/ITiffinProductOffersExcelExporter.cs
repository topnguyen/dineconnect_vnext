﻿using System.Collections.Generic;
using DinePlan.DineConnect.Tiffins.ProductOffers.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Exporting
{
    public interface ITiffinProductOffersExcelExporter
    {
        FileDto ExportToFile(List<GetTiffinProductOfferForViewDto> tiffinProductOffers);
    }
}
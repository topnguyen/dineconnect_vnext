﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.ProductOffers.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Exporting
{
    public class TiffinProductOffersExcelExporter : NpoiExcelExporterBase, ITiffinProductOffersExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TiffinProductOffersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTiffinProductOfferForViewDto> tiffinProductOffers)
        {
            return CreateExcelPackage(
                "TiffinProductOffers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TiffinProductOffers"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Description"),
                        L("Price"),
                        L("TotalDeliveryCount"),
                        L("Expiry"),
                        L("DaysOnFirstOrder"),
                        (L("TiffinProductSet")) + L("Name"),
                        (L("TiffinProductOfferType")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, tiffinProductOffers,
                        _ => _.TiffinProductOffer.Name,
                        _ => _.TiffinProductOffer.Description,
                        _ => _.TiffinProductOffer.Price,
                        _ => _.TiffinProductOffer.TotalDeliveryCount,
                        _ => _.TiffinProductOffer.Expiry,
                        _ => _.TiffinProductOffer.DaysOnFirstOrder,
                        _ => _.TiffinProductSetName,
                        _ => _.TiffinProductOfferTypeName
                        );
                });
        }
    }
}
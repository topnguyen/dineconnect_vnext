﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Exporting
{
    public class ProductSetsExcelExporter : NpoiExcelExporterBase, IProductSetsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductSetsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProductSetForViewDto> ProductSets)
        {
            return CreateExcelPackage(
                "ProductSets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProductSets"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, ProductSets,
                        _ => _.ProductSet.Name,
                        _ => _.ProductSet.CreationTime.ToShortDateString()
                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
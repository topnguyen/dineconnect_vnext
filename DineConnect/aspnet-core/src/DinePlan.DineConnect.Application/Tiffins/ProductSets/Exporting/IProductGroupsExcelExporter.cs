﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Exporting
{
    public interface IProductSetsExcelExporter
    {
        FileDto ExportToFile(List<GetProductSetForViewDto> ProductSets);
    }
}
﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.ProductSets
{
    
    public class ProductSetsAppService : DineConnectAppServiceBase, IProductSetsAppService
    {
        private readonly IRepository<TiffinProductSet> _productSetRepository;
        private readonly IRepository<TiffinProductSetProduct> _tiffinProdSetProdRepo;
        private readonly IProductSetsExcelExporter _productSetsExcelExporter;
       
        public ProductSetsAppService(
            IRepository<TiffinProductSet> productSetRepository,
            IRepository<TiffinProductSetProduct> tiffinProdSetProdRepo,
            IProductSetsExcelExporter productSetsExcelExporter
            )
        {
            _productSetRepository = productSetRepository;
            _tiffinProdSetProdRepo = tiffinProdSetProdRepo;
            _productSetsExcelExporter = productSetsExcelExporter;
          
        }

        public async Task<PagedResultDto<GetProductSetForViewDto>> GetAll(GetAllProductSetsInput input)
        {
            var filteredProductSets = _productSetRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var pagedAndFilteredProductSets = filteredProductSets
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var productSets = pagedAndFilteredProductSets.Select(g => new GetProductSetForViewDto { ProductSet = ObjectMapper.Map<ProductSetDto>(g) });

            var totalCount = await filteredProductSets.CountAsync();

            return new PagedResultDto<GetProductSetForViewDto>(
                totalCount,
                await productSets.ToListAsync()
            );
        }

        public async Task<GetProductSetForViewDto> GetProductSetForView(int id)
        {
            var productSet = await _productSetRepository.GetAsync(id);

            var output = new GetProductSetForViewDto { ProductSet = ObjectMapper.Map<ProductSetDto>(productSet) };

            return output;
        }

        
        public async Task<GetProductSetForEditOutput> GetProductSetForEdit(NullableIdDto input)
        {
            var output = new GetProductSetForEditOutput();

            if (input.Id.HasValue)
            {
                var productSet = await _productSetRepository.GetAll()
                    .Include(x => x.Products)
                    .ThenInclude(x => x.Product).FirstOrDefaultAsync(t => t.Id == input.Id.Value);

                output = new GetProductSetForEditOutput { ProductSet = ObjectMapper.Map<CreateOrEditProductSetDto>(productSet) };
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProductSetDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditProductSetDto input)
        {
            var productSet = ObjectMapper.Map<TiffinProductSet>(input);
            if (AbpSession.TenantId != null)
            {
                productSet.TenantId = AbpSession.TenantId.Value;
            }
            if (input.Products.Any())
            {
                productSet.Products = new System.Collections.Generic.List<TiffinProductSetProduct>();
                input.Products.ForEach(t => productSet.Products.Add(new TiffinProductSetProduct
                {
                    ProductId = t.ProductId
                }));
            }
            await _productSetRepository.InsertAsync(productSet);
        }

        
        protected virtual async Task Update(CreateOrEditProductSetDto input)
        {
            var productSet = await _productSetRepository.GetAllIncluding(t => t.Products).FirstOrDefaultAsync(t => t.Id == input.Id.Value);

            foreach (var product in productSet.Products)
            {
                await _tiffinProdSetProdRepo.DeleteAsync(product);
            }
            ObjectMapper.Map(input, productSet);
            if (input.Products.Any())
            {
                input.Products.ForEach(t => productSet.Products.Add(new TiffinProductSetProduct
                {
                    ProductId = t.ProductId
                }));
            }

            await _productSetRepository.UpdateAsync(productSet);
        }
       
        
        public async Task Delete(EntityDto input)
        {
            await _productSetRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetProductSetsToExcel(GetAllProductSetsInput input)
        {
            var filteredProductSets = _productSetRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var query = filteredProductSets.Select(g => new GetProductSetForViewDto { ProductSet = ObjectMapper.Map<ProductSetDto>(g) });

            var productSetListDtos = await query.ToListAsync();

            return _productSetsExcelExporter.ExportToFile(productSetListDtos);
        }

        protected void ValidateForCreateOrUpdate(CreateOrEditProductSetDto input)
        {
            var exists = _productSetRepository.GetAll()
                .Where(e => e.Name.Equals(input.Name))
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

    }
}
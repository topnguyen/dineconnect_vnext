﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report
{
    public class ReportNames
    {
        public const string ORDERSUMMARY = "OrderSummary";
        public const string PAYMENTSUMMARY = "PaymentSummary";
        public const string BALANCE = "Balance";
        public const string DRIVER = "Driver";
        public const string KITCHEN = "Kitchen";
        public const string PREPARATIONREPORT = "PreparationReport";
    }
}

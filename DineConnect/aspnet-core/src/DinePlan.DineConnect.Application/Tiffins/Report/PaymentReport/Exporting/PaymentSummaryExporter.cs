﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport.Exporting
{
    public class PaymentSummaryExporter : NpoiExcelExporterBase, IPaymentSummaryExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;

        public PaymentSummaryExporter(
                ITimeZoneConverter timeZoneConverter,
                IAbpSession abpSession,
                ITempFileCacheManager tempFileCacheManager,
                IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository)
        : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
        }

        public async Task<FileDto> ExportPaymentSummary(GetPaymentReportForExportInput input)
        {
            var payments = await GetPaymentSummary(input);
            return CreateExcelPackage(
                "PaymentSummary-" + DateTime.Now.ToString("dd-MM-yyyy-HH:mm") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PaymentSummary");

                    AddHeader(
                        sheet,
                        L("PurchaseDate"),
                        L("PurchaseTime"),
                        L("InvoiceNo"),
                        L("Mobile"),
                        L("Name"),
                        L("Plan"),
                        L("Amount"),
                        L("CurrentBalance")
                        );

                    var rowCount = 2;

                    foreach (var item in payments)
                    {
                        var colCount = 0;

                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.PurchaseDateTime?.Date.ToString("dd/MM/yyyy"));
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.PurchaseDateTime?.ToLongTimeString());
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.InvoiceNo);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.Mobile);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.Name);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.Plan);
                        if (item.Amount != null)
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++)
                                .SetCellValue(double.Parse(item.Amount.Value.ToString()));
                        else
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++)
                                .SetCellValue("");

                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.CurrentBalance ?? 0);
                        rowCount++;
                    }

                    sheet.GetSheetRow(rowCount).GetSheetCell(6).SetCellValue(double.Parse(payments.Sum(p => p.Amount.Value).ToString()));
                    sheet.GetSheetRow(rowCount).GetSheetCell(7).SetCellValue(payments.Sum(p => p.CurrentBalance.Value));

                    //sheet.Cells[1, 1, lastRow + 1, 10].Style.WrapText = true;
                    for (var i = 1; i <= 8; i++) sheet.AutoSizeColumn(i);
                });
        }

        private async Task<List<PaymentSummaryViewDto>> GetPaymentSummary(GetPaymentReportForExportInput input)
        {
            var query = _tfCustomerProductOfferRepository.GetAll()
                .Include(t => t.Payment)
                .Include(t => t.Customer)
                .Include(t => t.ProductOffer)
                .ThenInclude(p => p.TiffinProductOfferType)
                .Include(t => t.Orders)
                .WhereIf(input.CustomerId.HasValue, t => t.CustomerId == input.CustomerId)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductOffer.ProductOfferSets.Select(p => p.TiffinProductOfferId).Contains(input.ProductSetId.Value))
                .WhereIf(input.Zone.HasValue, x => x.Customer.CustomerAddressDetails.Select(c => c.Zone).Contains(input.Zone))
                .Select(e => new PaymentSummaryViewDto
                {
                    Name = e.Customer.Name,
                    Mobile = e.Customer.PhoneNumber,
                    InvoiceNo = e.Payment.InvoiceNo,
                    Plan = e.ProductOffer.TiffinProductOfferType.Name,
                    Amount = e.PaidAmount,
                    CurrentBalance = e.BalanceCredit,
                    PurchaseDateTime = e.Payment.PaymentDate
                });

            var listPayment = await query.ToListAsync();
            var paymentSummary = listPayment
                .GroupBy(c => new
                {
                    c.Name,
                    c.InvoiceNo
                })
                .Select(e => new PaymentSummaryViewDto
                {
                    Name = e.Key.Name,
                    Mobile = e.First().Mobile,
                    InvoiceNo = e.Key.InvoiceNo,
                    Plan = e.First().Plan,
                    Amount = e.Sum(i => i.Amount),
                    CurrentBalance = e.Sum(i => i.CurrentBalance),
                    PurchaseDateTime = e.First().PurchaseDateTime
                }).AsQueryable()
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Mobile, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.InvoiceNo, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Plan, input.Filter.ToILikeString()))
                .ToList();

            return paymentSummary;
        }
    }
}
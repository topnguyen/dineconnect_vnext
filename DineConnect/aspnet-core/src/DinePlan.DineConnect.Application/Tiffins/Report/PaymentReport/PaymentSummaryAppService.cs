﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport
{
    public class PaymentSummaryAppService : DineConnectAppServiceBase, IPaymentSummaryAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;
        private readonly IPaymentSummaryExporter _exporter;

        public PaymentSummaryAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm, IPaymentSummaryExporter exporter,
            IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<PaymentSummaryViewDto>> GetAll(GetPaymentSummaryInput input)
        {
            var query = _tfCustomerProductOfferRepository.GetAll()
                        .Include(t => t.Payment)
                        .Include(t => t.Customer)
                        .Include(t => t.ProductOffer)
                        .ThenInclude(p => p.TiffinProductOfferType)
                        .Include(t => t.Orders)
                        .WhereIf(input.CustomerId.HasValue, t => t.CustomerId == input.CustomerId)
                        .WhereIf(input.ProductSetId.HasValue, x => x.ProductOffer.ProductOfferSets.Select(p => p.TiffinProductOfferId).Contains(input.ProductSetId.Value))
                        .WhereIf(input.Zone.HasValue, x => x.Customer.CustomerAddressDetails.Select(c => c.Zone).Contains(input.Zone))
                        .WhereIf(input.FromDate.HasValue, x => x.Payment.PaymentDate.Date >= input.FromDate)
                        .WhereIf(input.ToDate.HasValue, x => x.Payment.PaymentDate.Date <= input.ToDate)

                        .Select(e => new PaymentSummaryViewDto
                        {
                            Name = e.Customer.Name,
                            Mobile = e.Customer.PhoneNumber,
                            InvoiceNo = e.Payment.InvoiceNo,
                            Plan = e.ProductOffer.TiffinProductOfferType.Name,
                            Amount = e.PaidAmount,
                            CurrentBalance = e.BalanceCredit,
                            PurchaseDateTime = e.Payment.PaymentDate
                        });

            var listPayment = await query.ToListAsync();
            var paymentSummary = listPayment
                .GroupBy(c => new
                {
                    c.Name,
                    c.InvoiceNo
                })
                .Select(e => new PaymentSummaryViewDto
                {
                    Name = e.Key.Name,
                    Mobile = e.First().Mobile,
                    InvoiceNo = e.Key.InvoiceNo,
                    Plan = e.First().Plan,
                    Amount = e.Sum(i => i.Amount),
                    CurrentBalance = e.Sum(i => i.CurrentBalance),
                    PurchaseDateTime = e.First().PurchaseDateTime
                }).ToList();

            var result = paymentSummary.AsQueryable()
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Mobile, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.InvoiceNo, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Plan, input.Filter.ToILikeString()));

            var totalCount = result.Count();

            var pagedResult = result
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            return new PagedResultDto<PaymentSummaryViewDto>(
                totalCount, pagedResult
            );
        }

        public async Task<FileDto> GetPaymentSummaryExcel(GetPaymentReportForExportInput input)
        {
            //if (AbpSession.UserId != null && AbpSession.TenantId != null)
            //{
            //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
            //    {
            //        ReportName = ReportNames.PAYMENTSUMMARY,
            //        Completed = false,
            //        UserId = AbpSession.UserId.Value,
            //        TenantId = AbpSession.TenantId.Value,
            //        ReportDescription = ""
            //    });
            //    if (backGroundId > 0)
            //    {
            //        await _bgm.EnqueueAsync<TiffinReportJob, TiffinReportInputArgs>(new TiffinReportInputArgs
            //        {
            //            BackGroundId = backGroundId,
            //            ReportName = ReportNames.PAYMENTSUMMARY,
            //            UserId = AbpSession.UserId.Value,
            //            TenantId = AbpSession.TenantId.Value,
            //            GetPaymentReportForExport = input
            //        });
            //    }
            //}
            //return null;

            return await _exporter.ExportPaymentSummary(input);
        }
    }
}
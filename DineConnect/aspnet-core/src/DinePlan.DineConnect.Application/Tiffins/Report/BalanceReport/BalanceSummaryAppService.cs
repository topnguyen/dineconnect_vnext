﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport
{
    public class BalanceSummaryAppService : DineConnectAppServiceBase, IBalanceSummaryAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;
        private readonly IBalanceSummaryExporter _exporter;

        public BalanceSummaryAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm, IBalanceSummaryExporter exporter,
            IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<BalanceSummaryViewDto>> GetAll(GetBalanceSummaryInput input)
        {
            var listBalance = await _tfCustomerProductOfferRepository.GetAll()
                .Include(t => t.Customer)
                .Include(t => t.ProductOffer)
                .ThenInclude(p => p.TiffinProductOfferType)
                .Include(t => t.Orders)
                .ThenInclude(o => o.ProductSet)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerId == input.CustomerId)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductOffer.ProductOfferSets.Any(t => t.TiffinProductSetId == input.ProductSetId))
                .WhereIf(input.FromDate.HasValue, x => x.Customer.CreationTime.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.Customer.CreationTime.Date <= input.ToDate)
                .WhereIf(input.Zone.HasValue, x => x.Customer.CustomerAddressDetails.Select(c => c.Zone).Contains(input.Zone))
               .Select(i => new BalanceSummaryViewDto
                {
                    CustomerId = i.CustomerId,
                    SignUp = i.Customer.CreationTime,
                    PhoneNumber = i.Customer.PhoneNumber,
                    CustomerName = i.Customer.Name,
                    Plan = i.ProductOffer.TiffinProductOfferType.Name,
                    MealBought = i.ActualCredit,
                    MealBalance = i.BalanceCredit,
                    Amount = i.PaidAmount
                }).ToListAsync();

            var balanceGroup = listBalance
                .GroupBy(c => new
                {
                    c.CustomerId,
                    c.CustomerName,
                    c.Plan
                }).Select(group => new BalanceSummaryViewDto
                {
                    CustomerId = group.Key.CustomerId,
                    SignUp = group.First().SignUp,
                    PhoneNumber = group.First().PhoneNumber,
                    CustomerName = group.Key.CustomerName,
                    Plan = group.Key.Plan,
                    MealBought = group.Sum(g => g.MealBought),
                    MealBalance = group.Sum(g => g.MealBalance),
                    Amount = group.Sum(g => g.Amount)
                }).ToList();

            var balanceSummary = balanceGroup.AsQueryable()
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.CustomerName, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Plan, input.Filter.ToILikeString()));

            var totalCount = balanceSummary.Count();
            var pagedResult = balanceSummary
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            return new PagedResultDto<BalanceSummaryViewDto>(
                totalCount, pagedResult
            );
        }

        public async Task<FileDto> GetBalanceSummaryExcel(ExportBalanceInput input)
        {
            //if (AbpSession.UserId != null && AbpSession.TenantId != null)
            //{
            //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
            //    {
            //        ReportName = ReportNames.BALANCE,
            //        Completed = false,
            //        UserId = AbpSession.UserId.Value,
            //        TenantId = AbpSession.TenantId.Value,
            //        ReportDescription = ""
            //    });
            //    if (backGroundId > 0)
            //    {
            //        await _bgm.EnqueueAsync<TiffinReportJob, TiffinReportInputArgs>(new TiffinReportInputArgs
            //        {
            //            BackGroundId = backGroundId,
            //            ReportName = ReportNames.BALANCE,
            //            UserId = AbpSession.UserId.Value,
            //            TenantId = AbpSession.TenantId.Value,
            //            ExportBalanceInput = input
            //        });
            //    }
            //}
            //return null;
            return await _exporter.ExportBalanceSummary(input);

        }
    }
}
﻿using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Extensions;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport.Exporting
{
    public class BalanceSummaryExporter : NpoiExcelExporterBase, IBalanceSummaryExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public BalanceSummaryExporter(
                ITimeZoneConverter timeZoneConverter,
                IAbpSession abpSession,
                ITempFileCacheManager tempFileCacheManager,
                IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository,
                ITenantSettingsAppService tenantSettingsAppService
            )
        : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportBalanceSummary(ExportBalanceInput input)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            
            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            var fileDayMonthSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDayMonthFormat)
                ? allSettings.FileDateTimeFormat.FileDayMonthFormat
                : AppConsts.FileDayMonthFormat;
            
            var balanceSummary = await GetBalanceSummary(input);
            return CreateExcelPackage(
                "BalanceSummary-" + DateTime.Now.ToString(fileDateSetting) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Balance"));
                    var headers = new List<string>
                        {
                            L("SignUpDate"),
                            L("MobileNumber"),
                            L("CustomerName"),
                            L("Plan"),
                            L("MealsBought"),
                            L("MealsBalance"),
                            L("AmountPaid")
                        };
                    var lastCol = 7;
                    var listDateIndex = new List<DateIndex>();
                    for (DateTime date = input.FromDateOrder; date <= input.ToDateOrder; date = date.AddDays(1))
                    {
                        listDateIndex.Add(new DateIndex
                        {
                            DateHeader = date,
                            Index = lastCol + 1
                        });
                        headers.Add(date.Date.ToString(AppConsts.FileDayMonthFormat));
                        lastCol++;
                    }

                    headers.Add(L("Remaining"));

                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 2;

                    foreach (var balance in balanceSummary)
                    {
                        var colCount = 0;
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(balance.SignUp.Date.ToString(fileDayMonthSetting));
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(balance.PhoneNumber);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(balance.CustomerName);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(balance.Plan);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++)
                            .SetCellValue(balance.MealBought ?? 0);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++)
                            .SetCellValue(balance.MealBalance ?? 0);
                        if(balance.Amount!=null)
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount).SetCellValue(double.Parse(balance.Amount.Value.ToString()));
                        else
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount).SetCellValue(0);

                        foreach (var orderDetail in balance.OrderDetails)
                        {
                            var dateMatch = listDateIndex.Find(i => i.DateHeader.Date.Equals(orderDetail.DateOrder.Date));
                            if (dateMatch != null)
                            {
                                sheet.GetSheetRow(rowCount).GetSheetCell(dateMatch.Index-1).SetCellValue(orderDetail.Quantity);
                            }
                        }

                        if (balance.MealBalance != null)
                        {
                            if (balance.MealBought != null)
                            {
                                sheet.GetSheetRow(rowCount).GetSheetCell(lastCol)
                                    .SetCellValue(balance.MealBought.Value - balance.MealBalance.Value);
                            }
                            else
                            {
                                sheet.GetSheetRow(rowCount).GetSheetCell(lastCol)
                                    .SetCellValue(balance.MealBalance.Value);
                            }
                        }
                        else
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(lastCol)
                                .SetCellValue(0);
                        }

                        rowCount++;
                    }

                    var lastRow = rowCount;
                    sheet.GetSheetRow(lastRow).GetSheetCell(4).SetCellValue(balanceSummary.Where(a=>a.MealBought!=null).Sum(b => b.MealBought.Value));
                    sheet.GetSheetRow(lastRow).GetSheetCell(5).SetCellValue(balanceSummary.Where(a=>a.MealBalance!=null).Sum(b => b.MealBalance.Value));
                    sheet.GetSheetRow(lastRow).GetSheetCell(6).SetCellValue(double.Parse(balanceSummary.Where(a=>a.Amount!=null).Sum(b => b.Amount.Value).ToString()));
                    for (var i = 1; i <= lastCol; i++) sheet.AutoSizeColumn(i);
                });
        }

        private async Task<List<BalanceSummaryExcelDto>> GetBalanceSummary(ExportBalanceInput input)
        {

            if (input.FromDateOrder == input.ToDateOrder)
            {
                input.ToDateOrder = input.ToDateOrder.AddDays(1);
            }
            var listO = _tfCustomerProductOfferRepository.GetAll()
                .Include(t => t.Customer)
                .Include(t => t.ProductOffer)
                .ThenInclude(p => p.TiffinProductOfferType)
                .Include(t => t.Orders)
                .ThenInclude(o => o.ProductSet)
                .Select(i => new BalanceSummaryExcelDto
                {
                    CustomerId = i.CustomerId,
                    SignUp = i.Customer.CreationTime,
                    PhoneNumber = i.Customer.PhoneNumber,
                    CustomerName = i.Customer.Name,
                    Plan = i.ProductOffer.TiffinProductOfferType.Name,
                    MealBought = i.ActualCredit,
                    MealBalance = i.BalanceCredit,
                    Amount = i.PaidAmount,
                    OrderDetails = i.Orders
                        .Where(o => o.OrderDate.Date >= input.FromDateOrder.Date && o.OrderDate.Date <= input.ToDateOrder.Date)
                        .Select(o => new OrderDetail
                        {
                            DateOrder = o.OrderDate,
                            Quantity = o.Quantity
                        }).ToList()
                });

            var listBalance = await listO.ToListAsync();

            var balanceSummary = listBalance
                .GroupBy(c => new
                {
                    c.CustomerId,
                    c.CustomerName
                }).Select(group => new BalanceSummaryExcelDto
                {
                    CustomerId = group.Key.CustomerId,
                    SignUp = group.First().SignUp,
                    PhoneNumber = group.First().PhoneNumber,
                    CustomerName = group.Key.CustomerName,
                    Plan = group.First().Plan,
                    MealBought = group.Sum(g => g.MealBought),
                    MealBalance = group.Sum(g => g.MealBalance),
                    Amount = group.Sum(g => g.Amount),
                    OrderDetails = group.SelectMany(g => g.OrderDetails)
                                        .GroupBy(g => g.DateOrder.Date)
                                        .Select(groupAvg => new OrderDetail
                                        {
                                            DateOrder = groupAvg.Key,
                                            Quantity = groupAvg.Sum(q => q.Quantity)
                                        }).ToList()
                }).ToList();

            return balanceSummary;
        }
    }
}
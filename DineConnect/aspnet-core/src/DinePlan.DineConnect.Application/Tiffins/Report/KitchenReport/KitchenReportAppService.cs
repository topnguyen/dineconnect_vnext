﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.KitchenReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport
{
    public class KitchenReportAppService : DineConnectAppServiceBase, IKitchenReportAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly IKitchenReportExporter _exporter;

        public KitchenReportAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm,IKitchenReportExporter exporter,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<KitchenReportViewDto>> GetAll(GetPreparationReportInput input)
        {
            var orders = _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => x.MealTimeId == input.MealPlan)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo), x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) || input.TimeSlotFormTo.Contains(x.TimeSlotFormTo))
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .Select(c => new KitchenReportViewDto
                {
                    Name = c.CustomerProductOffer.Customer.Name,
                    PhoneNumber = c.CustomerAddress.PhoneNumber,
                    ZoneNumber = c.CustomerAddress.Zone,
                    Plan = c.CustomerProductOffer.ProductOffer.TiffinProductOfferType.Name,
                    Meal = c.TiffinMealTime.Name,
                    TimeSlotFormTo = c.TimeSlotFormTo,
                    DateOrder = c.OrderDate,
                    Addon = c.AddOn
                });

            var result = orders
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Zone, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Meal, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Plan, input.Filter.ToILikeString()));

            var totalCount = result.Count();
            var pagedResult = await result
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            foreach (var item in pagedResult)
            {
                item.Zone = item.ZoneNumber.HasValue && item.ZoneNumber != 0 ? L(((ZoneEnum)item.ZoneNumber.Value).ToString()) : string.Empty;
            }

            return new PagedResultDto<KitchenReportViewDto>(totalCount, pagedResult);
        }

        public async Task<FileDto> GetListCustomerForKitchenExcel(ExportPreparationReportInput input)
        {
            //if (AbpSession.UserId != null && AbpSession.TenantId != null)
            //{
            //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
            //    {
            //        ReportName = ReportNames.KITCHEN,
            //        Completed = false,
            //        UserId = AbpSession.UserId.Value,
            //        TenantId = AbpSession.TenantId.Value,
            //        ReportDescription = ""
            //    });
            //    if (backGroundId > 0)
            //    {
            //        await _bgm.EnqueueAsync<TiffinReportJob, TiffinReportInputArgs>(new TiffinReportInputArgs
            //        {
            //            BackGroundId = backGroundId,
            //            ReportName = ReportNames.KITCHEN,
            //            UserId = AbpSession.UserId.Value,
            //            TenantId = AbpSession.TenantId.Value,
            //            ExportKitchenInput = input
            //        });
            //    }
            //}
            //return null;

            return await _exporter.ExportSummary(input);
        }

        protected static string ConvertJsonAddOnToString(string input)
        {
            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
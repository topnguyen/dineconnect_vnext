﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Shared;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NPOI.XSSF.UserModel;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport.Exporting
{
    public class KitchenReportExporter : NpoiExcelExporterBase, IKitchenReportExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly ICommonAppService _commonAppService;

        public KitchenReportExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository,
            ICommonAppService commonAppService
        )
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _commonAppService = commonAppService;
        }

        public async Task<FileDto> ExportSummary(ExportPreparationReportInput input)
        {
            var orderSummary = await GetAllOrderForKitchen(input);
            var productSetHeader = await _commonAppService.GetAllProductSets();

            const int totalFixHeader = 8;

            return CreateExcelPackage(
                "KitchenReport-" + DateTime.Now.ToString("dd-MM-yyyy-HH:mm") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("KitchenReport"));

                    var headers = new List<string>
                        {
                            L("DateOrder"),
                            L("TimeSlot"),
                            L("Name"),
                            L("PhoneNumber"),
                            L("Zone"),
                            L("Meal"),
                            L("Plan")
                        };
                    var positionProductSet = new Dictionary<int, string>();
                    var keyStart = 6;
                    foreach (var productSet in productSetHeader)
                    {
                        headers.Add(productSet.DisplayText);
                        positionProductSet.Add(keyStart, productSet.DisplayText);
                        keyStart++;
                    }

                    headers.Add(L("Total"));
                    headers.Add(L("Remarks"));

                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 1;

                    foreach (var order in orderSummary)
                    {
                        var colCount = 0;
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.OrderProductSet.DateOrder.ToString("dd/MM/yyyy"));
                        if (string.IsNullOrWhiteSpace(order.TimeSlotFormTo))
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(string.Empty);
                        }
                        else
                        {
                            var fromToDynamic = JsonConvert.DeserializeObject<dynamic>(order.TimeSlotFormTo);

                            var fromToString = $"{fromToDynamic.from} - {fromToDynamic.to}";

                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(fromToString);
                        }
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.Name);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.PhoneNumber);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.ZoneDisplay);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(order.MealDisplay);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount).SetCellValue(order.Plan);

                        foreach (KeyValuePair<int, string> entry in positionProductSet)
                        {
                            if (order.OrderProductSet.NameProductSet == entry.Value)
                            {
                                sheet.GetSheetRow(rowCount).GetSheetCell(entry.Key).SetCellValue(order.OrderProductSet.Quantity);
                            }
                            else sheet.GetSheetRow(rowCount).GetSheetCell(entry.Key).SetCellValue(0);
                        }

                        var colTotal = totalFixHeader + productSetHeader.Count - 1;

                        sheet.GetSheetRow(rowCount).GetSheetCell(colTotal).SetCellValue(order.OrderProductSet.Quantity);
                        colTotal++;
                        sheet.GetSheetRow(rowCount).GetSheetCell(colTotal).SetCellValue(order.OrderProductSet.Remarks);
                        rowCount++;
                    }

                    var lastRow = rowCount;

                    // Sum
                    var indexSum = 5;
                    sheet.GetSheetRow(lastRow).GetSheetCell(indexSum).SetCellValue("Total");

                    for (var i = 1; i <= productSetHeader.Count + 2; i++)
                    {
                        indexSum++;

                        double colSumData = 0;
                        for (int j = 1; j < lastRow; j++)
                        {
                            try
                            {
                                colSumData += sheet.GetSheetRow(j).GetSheetCell(indexSum).NumericCellValue;
                            }
                            catch
                            {
                                colSumData += 0;
                            }
                        }
                        sheet.GetSheetRow(lastRow).GetSheetCell(indexSum).SetCellValue(colSumData);
                    }

                    // fit column
                    var lastCol = totalFixHeader + productSetHeader.Count;

                    for (var i = 1; i <= lastCol; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        private async Task<List<OrderSummaryExcelDto>> GetAllOrderForKitchen(ExportPreparationReportInput input)
        {
            var list = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => (int)x.MealTimeId == input.MealPlan)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo), x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) || input.TimeSlotFormTo.Contains(x.TimeSlotFormTo))
                .ToListAsync();

            var list2 = list
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .GroupBy(e => e.CustomerProductOffer)
                .Select(e => new OrderGroupProductSet
                {
                    CustomerId = e.Key.CustomerId,
                    PhoneNumber = e.Key.Customer.PhoneNumber,
                    Name = e.Key.Customer.Name,
                    Address = e.Key.Customer.Address1,
                    UnitNumber = e.Key.Customer.Address2,
                    PostalCode = e.Key.Customer.PostcalCode,
                    ZoneDisplay = e.Key.Customer?.Zone != null ? L(((ZoneEnum)e.Key.Customer.Zone.Value).ToString()) : string.Empty,
                    Plan = e.Key.ProductOffer.TiffinProductOfferType.Name,
                    OrderProductSets = e
                    .Select(o => new OrderProductSet
                    {
                        Meal = (int)o.MealTimeId,
                        MealTime = o.TiffinMealTime.Name,
                        DateOrder = o.OrderDate,
                        NameProductSet = o.ProductSet.Name,
                        Quantity = o.Quantity,
                        Remarks = ConvertJsonAddOnToString(o.AddOn),
                        TimeSlotFormTo = o.TimeSlotFormTo,
                    })
                        .ToList()
                }).ToList();

            var list3 = list2
                .Where(c => c.OrderProductSets.Any())
                .Select(o => new OrderGroupProductSet
                {
                    CustomerId = o.CustomerId,
                    PhoneNumber = o.PhoneNumber,
                    Name = o.Name,
                    Address = o.Address,
                    UnitNumber = o.UnitNumber,
                    PostalCode = o.PostalCode,
                    ZoneDisplay = o.ZoneDisplay,
                    Plan = o.Plan,
                    OrderProductSets = o.OrderProductSets.GroupBy(i => new { i.DateOrder.Date, i.NameProductSet, i.Meal, i.Remarks, i.MealTime })
                        .Select(e => new OrderProductSet
                        {
                            Meal = e.Key.Meal,
                            MealTime = e.Key.MealTime,
                            DateOrder = e.Key.Date,
                            NameProductSet = e.Key.NameProductSet,
                            Quantity = e.Sum(i => i.Quantity),
                            Remarks = e.First().Remarks
                        }).ToList()
                }).ToList();

            var result = (from cus in list3
                          from order in cus.OrderProductSets
                          select new OrderSummaryExcelDto
                          {
                              CustomerId = cus.CustomerId,
                              PhoneNumber = cus.PhoneNumber,
                              Name = cus.Name,
                              Address = cus.Address,
                              UnitNumber = cus.UnitNumber,
                              PostalCode = cus.PostalCode,
                              ZoneDisplay = cus.ZoneDisplay,
                              Plan = cus.Plan,
                              MealDisplay = order.MealTime,
                              OrderProductSet = order
                          }).ToList();
            return result;
        }

        protected string ConvertJsonAddOnToString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
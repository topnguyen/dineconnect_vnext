﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.EmailNotification;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport
{
    public class CustomerReportAppService : DineConnectAppServiceBase, ICustomerReportAppService
    {
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepo;
        private readonly IRepository<BaseCore.Customer.Customer> _customerRepo;
        private readonly ICustomerReportExporter _customerReportExporter;
        private readonly IEmailNotificationAppService _emailNotificationAppService;
        private readonly IRepository<TiffinCustomerMealPlanReminder> _customerMealPlanReminderRepo;

        public CustomerReportAppService(
            IRepository<TiffinCustomerProductOffer> customerProductOfferRepo,
            IRepository<BaseCore.Customer.Customer> customerRepo,
            ICustomerReportExporter customerReportExporter,
            IEmailNotificationAppService emailNotificationAppService,
            IRepository<TiffinCustomerMealPlanReminder> customerMealPlanReminderRepo
            )
        {
            _customerProductOfferRepo = customerProductOfferRepo;
            _customerRepo = customerRepo;
            _customerReportExporter = customerReportExporter;
            _emailNotificationAppService = emailNotificationAppService;
            _customerMealPlanReminderRepo = customerMealPlanReminderRepo;
        }

        public async Task<FileDto> ExportExcel(CustomerReportInput input)
        {
            var customerReports = await GetData(input);

            return await _customerReportExporter.Export(customerReports);
        }

        public async Task<PagedResultDto<CustomerReportViewDto>> GetAll(CustomerReportInput input)
        {
            var customerReports = await GetData(input);

            var totalCount = customerReports.Count;
            
            var pagedResult = customerReports.AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            return new PagedResultDto<CustomerReportViewDto>(totalCount, pagedResult);
        }

        public async Task SendEmailRemindBuyMealPlan(long customerId)
        {
            var customer = _customerRepo.GetAll().FirstOrDefault(x => x.Id == customerId);

            if (customer == null)
            {
                return;
            }

            await _emailNotificationAppService.SendEmailRemindBuyMealPlan(customer.EmailAddress, customer.Name, customer.TenantId);

            var customerMealPlanReminder = _customerMealPlanReminderRepo.GetAll()
                .FirstOrDefault(x => x.CustomerId == customer.Id);

            if (customerMealPlanReminder == null)
            {
                customerMealPlanReminder = new TiffinCustomerMealPlanReminder
                {
                    CustomerId = customer.Id
                };

                await _customerMealPlanReminderRepo.InsertAndGetIdAsync(customerMealPlanReminder);
            }


            // DO NOT increase the reminder times for BuyMealPlanReminderWorker checking it self reminder times.
            //customerMealPlanReminder.BuyMealPlanReminderTimes++;

            customerMealPlanReminder.LastTimeSentBuyMealPlanReminder = Clock.Now;

            await _customerMealPlanReminderRepo.UpdateAsync(customerMealPlanReminder);
        }

        public async Task SendEmailRemindBuyMealPlanToNewCustomers()
        {
            var newCustomers = await GetData(new CustomerReportInput
            {
                NewCustomer = true
            });

            foreach (var customerReport in newCustomers)
            {
                await SendEmailRemindBuyMealPlan(customerReport.CustomerId);
            }
        }

        public async Task<List<CustomerReportViewDto>> GetData(CustomerReportInput input)
        {
            var customerReports = await
                _customerRepo.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerName), x => x.Name == input.CustomerName || x.Name.Contains(input.CustomerName) || input.CustomerName.Contains(x.Name))
                .Select(x => new CustomerReportViewDto
                {
                    CustomerId = x.Id,
                    CustomerEmail = x.EmailAddress,
                    CustomerName = x.Name,
                    RegistrationDate = x.CreationTime,
                    LastLoginTime = x.LastLoginTime
                })
                .ToListAsync();

            var customerProductOffers = _customerProductOfferRepo
                .GetAll()
                .Include(x => x.Payment)
                .Include(x => x.ProductOffer)
                .ToList();

            foreach (var customerReport in customerReports)
            {
                var customerMealPlanReminder = _customerMealPlanReminderRepo.GetAll()
                    .FirstOrDefault(x => x.CustomerId == customerReport.CustomerId);

                if (customerMealPlanReminder == null)
                {
                    customerMealPlanReminder = new TiffinCustomerMealPlanReminder
                    {
                        CustomerId = (int)customerReport.CustomerId
                    };

                    await _customerMealPlanReminderRepo.InsertAndGetIdAsync(customerMealPlanReminder);
                }

                customerReport.LastTimeSendBuyMealPlanRemind = customerMealPlanReminder.LastTimeSentBuyMealPlanReminder;

                customerReport.TotalSpent = customerProductOffers
                    .Where(x => x.CustomerId == customerReport.CustomerId)
                    .Select(x => x.PaidAmount)
                    .DefaultIfEmpty()
                    .Sum() ?? 0;

                customerReport.TotalTimeBuyMealPlan = customerProductOffers.Count(x => x.CustomerId == customerReport.CustomerId);

                var activePlans = customerProductOffers
                    .Where(e => e.CustomerId == customerReport.CustomerId && (!e.ProductOffer.Expiry || e.ExpiryDate.Date > Clock.Now.Date) && e.BalanceCredit > 0)
                    .Select(x => x.ProductOffer.Name)
                    .ToList();

                customerReport.ActiveMealPlans = string.Join(", ", activePlans);

                customerReport.LastTimeBuyMealPlan = customerProductOffers
                    .Where(x => x.CustomerId == customerReport.CustomerId)
                    .OrderByDescending(x => x.Payment.CreationTime)
                    .Select(x => x.Payment.CreationTime)
                    .FirstOrDefault();
            }

            // Filter New Customer

            if (input.NewCustomer == true)
            {
                customerReports = customerReports.Where(x => x.TotalTimeBuyMealPlan <= 0).ToList();
            }

            if (input.NewCustomer == false)
            {
                customerReports = customerReports.Where(x => x.TotalTimeBuyMealPlan > 0).ToList();
            }
            
            // Filter Active Customer
            
            var activeDateThreshold = Clock.Now.AddDays(-15);
            
            if (input.ActiveCustomer == true)
            {
                customerReports = customerReports.Where(x => x.LastTimeBuyMealPlan >= activeDateThreshold).ToList();
            }

            if (input.ActiveCustomer == false)
            {
                customerReports = customerReports.Where(x => x.LastTimeBuyMealPlan < activeDateThreshold).ToList();
            }

            return customerReports;
        }
    }
}
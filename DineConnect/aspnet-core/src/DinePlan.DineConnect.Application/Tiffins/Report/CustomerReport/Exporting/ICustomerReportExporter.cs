using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport.Exporting
{
    public interface ICustomerReportExporter
    {
        Task<FileDto> Export(List<CustomerReportViewDto> customerReports);
    }
}
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;
using Newtonsoft.Json;
using NPOI.SS.Format;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport.Exporting
{
    public class CustomerReportExporter : NpoiExcelExporterBase, ICustomerReportExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public CustomerReportExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> Export(List<CustomerReportViewDto> customerReports)
        {
             var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "CustomerReport-" + Clock.Now.ToString(fileDateSetting) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Driver"));

                    var headers = new List<string>
                        {
                            L("CustomerName"),
                            L("Registration"),
                            L("Email"),
                            L("LastLoginDate"),
                            L("ActiveMealPlans"),
                            L("TotalSpent")
                        };

                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 1;

                    foreach (var customer in customerReports)
                    {
                        var colCount = 0;
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.CustomerName);
                        var registrationDateCell = sheet.GetSheetRow(rowCount).GetSheetCell(colCount++);
                        registrationDateCell.SetCellValue(customer.RegistrationDate.ToString("d"));
                        registrationDateCell.SetCellValue(customer.CustomerEmail);

                        if (customer.LastLoginTime == null)
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(string.Empty);
                        }
                        else
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.LastLoginTime.Value.DateTime.ToString("d"));
                        }
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.ActiveMealPlans);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount).SetCellValue(customer.TotalSpent.ToString(CultureInfo.InvariantCulture));
                        rowCount++;
                    }

                    // fit column
                    for (var i = 1; i <= headers.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
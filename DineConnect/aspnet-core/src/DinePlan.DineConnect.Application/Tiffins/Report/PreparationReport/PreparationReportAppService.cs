﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport
{
    public class PreparationReportAppService : DineConnectAppServiceBase, IPreparationReportAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IPreparationReportExporter _preparationReportExporter;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;

        public PreparationReportAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm,
            IPreparationReportExporter preparationReportExporter,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _preparationReportExporter = preparationReportExporter;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
        }

        public async Task<PagedResultDto<PreparationReportViewDto>> GetAll(GetPreparationReportInput input)
        {
            var list = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => x.MealTimeId == input.MealPlan)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo), x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) || input.TimeSlotFormTo.Contains(x.TimeSlotFormTo))
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .ToListAsync();

            var orders = list
                   .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                   .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                   .Select(g => new OrderProductSet
                   {
                       DateOrder = g.OrderDate,
                       Meal = g.MealTimeId,
                       MealTime = g.TiffinMealTime?.Name,
                       NameProductSet = g.ProductSet.Name,
                       Quantity = g.Quantity,
                       OrderId = g.Id
                   }).Distinct()
                   .ToList();

            var result = orders
                .GroupBy(i => new { i.DateOrder.Date, i.Meal, i.NameProductSet, i.MealTime, i.TimeSlotFormTo })
                .Select(group => new PreparationReportViewDto
                {
                    DateOrder = group.Key.Date,
                    NameProductSet = group.Key.NameProductSet,
                    MealTime = group.Key.MealTime,
                    Quantity = group.Sum(g => g.Quantity),
                    OrderIdList = group.Select(e => e.OrderId).ToList(),
                    TimeSlotFormTo = group.Key.TimeSlotFormTo
                }).WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.NameProductSet, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.MealTime, input.Filter.ToILikeString()))
                .ToList();

            var totalCount = result.Count();

            var pagedResult = result
                .AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            return new PagedResultDto<PreparationReportViewDto>(
                totalCount, pagedResult
            );
        }

        public async Task<FileDto> GetPreparationReportExcel(ExportPreparationReportInput input)
        {
            //if (AbpSession.UserId != null && AbpSession.TenantId != null)
            //{
            //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
            //    {
            //        ReportName = ReportNames.PREPARATIONREPORT,
            //        Completed = false,
            //        UserId = AbpSession.UserId.Value,
            //        TenantId = AbpSession.TenantId.Value,
            //        ReportDescription = ""
            //    });
            //    if (backGroundId > 0)
            //    {
            //        await _bgm.EnqueueAsync<TiffinReportJob, TiffinReportInputArgs>(new TiffinReportInputArgs
            //        {
            //            BackGroundId = backGroundId,
            //            ReportName = ReportNames.PREPARATIONREPORT,
            //            UserId = AbpSession.UserId.Value,
            //            TenantId = AbpSession.TenantId.Value,
            //            ExportPreparationReportInput = input
            //        });
            //    }
            //}

            //return null;

            return await _preparationReportExporter.ExportPreparationReport(input);
        }

        public async Task<FileDto> PrintPreparationReport(ExportPreparationReportInput input)
        {
            var list = await _tfCustomerProductOfferOrderRepository.GetAll()
                  .Include(e => e.CustomerProductOffer)
                  .Include(e => e.ProductSet)
                  .ToListAsync();

            var orders = list
                   .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                   .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                   .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                   .WhereIf(input.MealPlan.HasValue, x => (int)x.MealTimeId == input.MealPlan)
                   .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                   .Select(g => new OrderProductSet
                   {
                       DateOrder = g.OrderDate,
                       Meal = (int)g.MealTimeId,
                       MealTime = g.TiffinMealTime?.Name,
                       NameProductSet = g.ProductSet.Name,
                       Quantity = g.Quantity,
                       OrderId = (int)g.Id
                   }).Distinct()
                   .ToList();

            var result = orders
                .GroupBy(i => new { i.DateOrder.Date, i.Meal, i.NameProductSet, i.MealTime })
                .Select(group => new PreparationReportViewDto
                {
                    DateOrder = group.Key.Date,
                    NameProductSet = group.Key.NameProductSet,
                    MealTime = group.Key.MealTime,
                    Quantity = group.Sum(g => g.Quantity),
                    OrderIdList = group.Select(e => e.OrderId).ToList()
                }).ToList();

            var lstResult = result
                .AsQueryable()
                .OrderBy(c => c.DateOrder)
                .ToList();

            return _preparationReportExporter.PrintListPreparation(lstResult);
        }
    }
}
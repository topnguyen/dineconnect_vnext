﻿using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Shared;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport.Exporting
{
    public class PreparationReportExporter : NpoiExcelExporterBase, IPreparationReportExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly ICommonAppService _commonAppService;

        public PreparationReportExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository,
            ICommonAppService commonAppService)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tempFileCacheManager = tempFileCacheManager;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _commonAppService = commonAppService;
        }

        public async Task<FileDto> ExportPreparationReport(ExportPreparationReportInput input)
        {
            var preparationsList = await GetPreparationReport(input);
            var productSetHeader = await _commonAppService.GetAllProductSets();
            const int totalFixHeader = 3;
            return CreateExcelPackage(
                "PreparationReport-" + DateTime.Now.ToString("dd-MM-yyyy-HH:mm") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Preparation"));

                    var headers = new List<string>
                        {
                            L("DateOrder"),
                            L("TimeSlot"),
                            L("MealTime")
                        };

                    var positionProductSet = new Dictionary<int, string>();
                    var keyStart = 3;
                    foreach (var productSet in productSetHeader)
                    {
                        headers.Add(productSet.DisplayText);
                        positionProductSet.Add(keyStart, productSet.DisplayText);
                        keyStart++;
                    }

                    headers.Add(L("TotalQuantity"));
                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 1;

                    foreach (var item in preparationsList)
                    {
                        var colCount = 0;

                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(item.DateOrder.Date.ToString("dd/MM/yyyy"));
                        if (string.IsNullOrWhiteSpace(item.TimeSlotFormTo))
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(string.Empty);
                        }
                        else
                        {
                            var fromToString = string.Empty;

                            try
                            {
                                var fromToDynamic = JsonConvert.DeserializeObject<dynamic>(item.TimeSlotFormTo);
                                fromToString = $"{fromToDynamic.from} - {fromToDynamic.to}";
                            }
                            catch
                            {
                                // Ignore
                            }

                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(fromToString);
                        }
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount).SetCellValue(item.MealTime);

                        foreach (KeyValuePair<int, string> entry in positionProductSet)
                        {
                            foreach (var itemOrder in item.ProductOrderExport)
                            {
                                if (itemOrder.NameProductSet == entry.Value)
                                {
                                    sheet.GetSheetRow(rowCount).GetSheetCell(entry.Key).SetCellValue(itemOrder.Quantity);
                                }
                            }
                        }

                        var colTotal = totalFixHeader + productSetHeader.Count;

                        sheet.GetSheetRow(rowCount).GetSheetCell(colTotal).SetCellValue(item.ProductOrderExport.Sum(i => i.Quantity));

                        rowCount++;
                    }

                    var lastRow = rowCount;

                    // Sum
                    var indexSum = 1;
                    sheet.GetSheetRow(lastRow).GetSheetCell(indexSum).SetCellValue("Total");
                    var lastCol = totalFixHeader + productSetHeader.Count;
                    for (var i = 3; i <= lastCol; i++)
                    {
                        var cell = sheet.GetSheetRow(lastRow).GetSheetCell(i);

                        double colSumData = 0;

                        for (int j = 2; j < lastRow; j++)
                        {
                            try
                            {
                                colSumData += sheet.GetSheetRow(j).GetSheetCell(i).NumericCellValue;
                            }
                            catch
                            {
                                colSumData += 0;
                            }
                        }

                        cell.SetCellValue(colSumData);

                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto PrintListPreparation(List<PreparationReportViewDto> preparationReports)
        {
            return GeneratePreparationPdf(preparationReports);
        }

        protected FileDto GeneratePreparationPdf(List<PreparationReportViewDto> preparationReports)
        {
            var document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            var normalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            var boldFont = FontFactory.GetFont("Arial", 13, Font.BOLD, BaseColor.BLACK);
            var titleFont = FontFactory.GetFont("Arial", 18);
            using (var memoryStream = new System.IO.MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPTable table = null;

                document.Open();

                // Add title
                var title = new Paragraph("Preparation Report", titleFont)
                {
                    Alignment = Element.ALIGN_LEFT,
                    SpacingAfter = 20
                };
                document.Add(title);

                //Add table
                table = new PdfPTable(4)
                {
                    TotalWidth = 400f,
                    LockedWidth = true
                };
                PdfPCell dateTitleCell = new PdfPCell(new Phrase("Date Order", boldFont));
                dateTitleCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(dateTitleCell);

                PdfPCell mealTimeCell = new PdfPCell(new Phrase("Meal Time", boldFont));
                mealTimeCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(mealTimeCell);

                PdfPCell productSetTitleCell = new PdfPCell(new Phrase("Product Set", boldFont));
                productSetTitleCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(productSetTitleCell);

                PdfPCell quantityTitleCell = new PdfPCell(new Phrase("Quantity", boldFont));
                quantityTitleCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(quantityTitleCell);

                foreach (var item in preparationReports)
                {
                    table.AddCell(new PdfPCell(new Phrase(item.DateOrder.ToString("dd/MM/yyyy"), normalFont)));
                    table.AddCell(new PdfPCell(new Phrase(item.MealTime, normalFont)));
                    table.AddCell(new PdfPCell(new Phrase(item.NameProductSet, normalFont)));

                    PdfPCell quantityCell = new PdfPCell(new Phrase(item.Quantity.ToString(), normalFont));
                    quantityCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    quantityCell.PaddingLeft = 10f;
                    table.AddCell(quantityCell);
                }

                document.Add(table);

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = "Preparation.pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        private async Task<List<PreparationReportExportDto>> GetPreparationReport(ExportPreparationReportInput input)
        {
            var list = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => x.MealTimeId == input.MealPlan)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo), x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) || input.TimeSlotFormTo.Contains(x.TimeSlotFormTo))

                .ToListAsync();

            var list2 = list
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .GroupBy(e => e.CustomerProductOffer)
                .Select(c => new
                {
                    c.Key.CustomerId,
                    OrderProductSets = c
                    .Select(g => new OrderProductSet
                    {
                        DateOrder = g.OrderDate,
                        Meal = g.MealTimeId,
                        MealTime = g.TiffinMealTime.Name,
                        TimeSlotFormTo = g.TimeSlotFormTo,
                        NameProductSet = g.ProductSet.Name,
                        Quantity = g.Quantity
                    }).ToList()
                }).ToList();

            var list3 = list2
                .Where(c => c.OrderProductSets.Any())
                .Select(c => new
                {
                    c.CustomerId,
                    OrderProductSets = c.OrderProductSets.GroupBy(i => new { i.NameProductSet, i.Meal, i.DateOrder.Date, i.MealTime, i.TimeSlotFormTo })
                        .Select(g => new OrderProductSet
                        {
                            DateOrder = g.Key.Date,
                            Meal = g.Key.Meal,
                            MealTime = g.Key.MealTime,
                            TimeSlotFormTo = g.Key.TimeSlotFormTo,
                            NameProductSet = g.Key.NameProductSet,
                            Quantity = g.Sum(f => f.Quantity)
                        }).ToList()
                }).ToList();

            var list4 = list3.SelectMany(l1 => l1.OrderProductSets, (od1, od2) => new PreparationReportViewDto
            {
                DateOrder = od2.DateOrder,
                NameProductSet = od2.NameProductSet,
                MealTime = od2.MealTime,
                Quantity = od2.Quantity
            }).ToList();

            var list5 = list4
                .GroupBy(i => new { i.DateOrder.Date, i.MealTime, i.NameProductSet, i.TimeSlotFormTo })
                .Select(group => new PreparationReportViewDto
                {
                    DateOrder = group.Key.Date,
                    NameProductSet = group.Key.NameProductSet,
                    MealTime = group.Key.MealTime,
                    TimeSlotFormTo = group.Key.TimeSlotFormTo,
                    Quantity = group.Sum(g => g.Quantity)
                })
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.NameProductSet, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.MealTime, input.Filter.ToILikeString()))
                .ToList();

            var list6 = list5
                .GroupBy(i => new { i.DateOrder.Date, i.MealTime, i.TimeSlotFormTo })
                .Select(group => new PreparationReportExportDto
                {
                    DateOrder = group.Key.Date,
                    MealTime = group.Key.MealTime,
                    TimeSlotFormTo= group.Key.TimeSlotFormTo,
                    ProductOrderExport = group.Select(g => new ProductOrderExport
                    {
                        NameProductSet = g.NameProductSet,
                        Quantity = g.Quantity,
                        
                    }).ToList()
                }).ToList();

            return list6;
        }
    }
}
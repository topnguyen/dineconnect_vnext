﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.DriverReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;
using DinePlan.DineConnect.Tiffins.Report.KitchenReport;
using System.Data.Entity.SqlServer;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport
{
    public class DriverReportAppService : DineConnectAppServiceBase, IDriverReportAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<TiffinCustomerProductOffer> _tfCustomerProductOfferRepository;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly IDriverReportExporter _exporter;

        public DriverReportAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm,IDriverReportExporter exporter,
            IRepository<TiffinCustomerProductOffer> tfCustomerProductOfferRepository,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _tfCustomerProductOfferRepository = tfCustomerProductOfferRepository;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<DriverReportViewDto>> GetAll(GetPreparationReportInput input)
        {
            var tiffinProductOffers = _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => (int)x.MealTimeId == input.MealPlan)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo), x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) || input.TimeSlotFormTo.Contains(x.TimeSlotFormTo));

            var productOffers = tiffinProductOffers.Select(c => new DriverReportViewDto
            {
                Name = c.CustomerProductOffer == null ? "" : c.CustomerProductOffer.Customer == null ? "" : c.CustomerProductOffer.Customer.Name,
                PhoneNumber = c.CustomerProductOffer == null ? "" : c.CustomerProductOffer.Customer == null ? "" : c.CustomerProductOffer.Customer.PhoneNumber,
                Address = c.CustomerAddress == null ? "" : c.CustomerAddress.FullAddress,
                UnitNumber = c.CustomerAddress == null ? "" : c.CustomerAddress.Address2,
                PostalCode = c.CustomerAddress == null ? "" : c.CustomerAddress.PostcalCode,
                ZoneNumber = c.CustomerProductOffer == null ? null : c.CustomerProductOffer.Customer == null ? null : c.CustomerProductOffer.Customer.Zone,
                MealTime = c.TiffinMealTime == null ? "" : c.TiffinMealTime.Name,
                DateOrder = c.OrderDate,
                Remarks = c.AddOn,
                Total = c.Quantity,
                TimeSlotFormTo = c.TimeSlotFormTo
            })
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Zone, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.MealTime, input.Filter.ToILikeString()));

            var result = productOffers
                .GroupBy(c => new
                {
                    c.Name,
                    c.PhoneNumber,
                    c.Address,
                    c.UnitNumber,
                    c.PostalCode,
                    c.ZoneNumber,
                    c.MealTime,
                    DateOrder = c.DateOrder.Date,
                    c.Remarks,
                    c.TimeSlotFormTo
                }).Select(c => new DriverReportViewDto
                {
                    Name = c.Key.Name,
                    PhoneNumber = c.Key.PhoneNumber,
                    Address = c.Key.Address,
                    UnitNumber = c.Key.UnitNumber,
                    PostalCode = c.Key.PostalCode,
                    ZoneNumber = c.Key.ZoneNumber,
                    Zone = c.Key.ZoneNumber.HasValue && c.Key.ZoneNumber != 0 ? L(((ZoneEnum)c.Key.ZoneNumber.Value).ToString()) : string.Empty,
                    MealTime = c.Key.MealTime,
                    DateOrder = c.Key.DateOrder,
                    Remarks = string.IsNullOrWhiteSpace(c.Key.Remarks) ? string.Empty : ConvertJsonAddOnToString(c.Key.Remarks),
                    Total = c.Sum(e => e.Total),
                    TimeSlotFormTo = c.Key.TimeSlotFormTo
                });

            var totalCount = result.Count();
            var pagedResult = result.AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            return new PagedResultDto<DriverReportViewDto>(totalCount, pagedResult);
        }

        public async Task<FileDto> GetListCustomerForDriverExcel(ExportPreparationReportInput input)
        {
            //if (AbpSession.UserId != null && AbpSession.TenantId != null)
            //{
            //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
            //    {
            //        ReportName = ReportNames.DRIVER,
            //        Completed = false,
            //        UserId = AbpSession.UserId.Value,
            //        TenantId = AbpSession.TenantId.Value,
            //        ReportDescription = ""
            //    });
            //    if (backGroundId > 0)
            //    {
            //        await _bgm.EnqueueAsync<TiffinReportJob, TiffinReportInputArgs>(new TiffinReportInputArgs
            //        {
            //            BackGroundId = backGroundId,
            //            ReportName = ReportNames.DRIVER,
            //            UserId = AbpSession.UserId.Value,
            //            TenantId = AbpSession.TenantId.Value,
            //            ExportDriverInput = input
            //        });
            //    }
            //}
            //return null;
            return await _exporter.ExportSummary(input);
        }

        protected string ConvertJsonAddOnToString(string input)
        {
            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
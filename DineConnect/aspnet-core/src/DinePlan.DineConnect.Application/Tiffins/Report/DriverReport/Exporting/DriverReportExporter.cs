﻿using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.DriverReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport.Exporting
{
    public class DriverReportExporter : NpoiExcelExporterBase, IDriverReportExporter
    {
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public DriverReportExporter(ITempFileCacheManager tempFileCacheManager,
                IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository,
                ITenantSettingsAppService tenantSettingsAppService
            )
        : base(tempFileCacheManager)
        {
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportSummary(ExportPreparationReportInput input)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var listCustomer = await GetAllListCustomerForDriver(input);

            return CreateExcelPackage(
                "DriverReport-" + Clock.Now.ToString(fileDateSetting) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Driver"));

                    var headers = new List<string>
                        {
                            L("DateOrder"),
                            L("TimeSlot"),
                            L("Name"),
                            L("PhoneNumber"),
                            L("Address2Full"),
                            L("UnitNumber"),
                            L("PostalCode"),
                            L("Zone"),
                            L("MealTime"),
                            L("Total"),
                            L("Remarks")
                        };

                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 1;

                    foreach (var customer in listCustomer)
                    {
                        var colCount = 0;
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.DateOrder.ToString(fileDateSetting));
                        if (string.IsNullOrWhiteSpace(customer.TimeSlotFormTo))
                        {
                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(string.Empty);
                        }
                        else
                        {
                            var fromToString = string.Empty;

                            try
                            {
                                var fromToDynamic = JsonConvert.DeserializeObject<dynamic>(customer.TimeSlotFormTo);
                                fromToString = $"{fromToDynamic.from} - {fromToDynamic.to}";
                            }
                            catch
                            {
                                // Ignore
                            }

                            sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(fromToString);
                        }
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Name);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.PhoneNumber);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Address);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.UnitNumber);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.PostalCode);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Zone);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.MealTime);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Total);
                        sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Remarks);
                        rowCount++;
                    }

                    // fit column
                    for (var i = 1; i <= headers.Count(); i++) sheet.AutoSizeColumn(i);
                });
        }

        private async Task<List<DriverReportViewDto>> GetAllListCustomerForDriver(ExportPreparationReportInput input)
        {
            var productOffers = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.MealPlan.HasValue, x => (int)x.MealTimeId == input.MealPlan)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .Select(c => new DriverReportViewDto
                {
                    Name = c.CustomerProductOffer.Customer.Name,
                    PhoneNumber = c.CustomerProductOffer.Customer.PhoneNumber,
                    Address = c.CustomerAddress.FullAddress,
                    UnitNumber = c.CustomerAddress.Address2,
                    PostalCode = c.CustomerAddress.PostcalCode,
                    ZoneNumber = c.CustomerProductOffer.Customer.Zone,
                    MealTime = c.TiffinMealTime.Name,
                    DateOrder = c.OrderDate,
                    Total = c.Quantity,
                    Remarks = c.AddOn,
                })
                .ToListAsync();

            var result = productOffers
                .GroupBy(c => new
                {
                    c.Name,
                    c.PhoneNumber,
                    c.Address,
                    c.UnitNumber,
                    c.PostalCode,
                    c.ZoneNumber,
                    c.MealTime,
                    DateOrder = c.DateOrder.Date,
                    c.Remarks
                }).Select(c => new DriverReportViewDto
                {
                    Name = c.Key.Name,
                    PhoneNumber = c.Key.PhoneNumber,
                    Address = c.Key.Address,
                    UnitNumber = c.Key.UnitNumber,
                    PostalCode = c.Key.PostalCode,
                    ZoneNumber = c.Key.ZoneNumber,
                    Zone = c.Key.ZoneNumber.HasValue && c.Key.ZoneNumber != 0 ? L(((ZoneEnum)c.Key.ZoneNumber.Value).ToString()) : string.Empty,
                    MealTime = c.Key.MealTime,
                    DateOrder = c.Key.DateOrder,
                    Remarks = ConvertJsonAddOnToString(c.Key.Remarks),
                    Total = c.Sum(e => e.Total)
                })
                .OrderBy(e => e.DateOrder)
                .ToList();

            return result;
        }

        protected string ConvertJsonAddOnToString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Shared;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.AspNetZeroCore.Net;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport.Exporting
{
    public class OrderSummaryExporter : NpoiExcelExporterBase, IOrderSummaryExporter
    {
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly ICommonAppService _commonAppService;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public OrderSummaryExporter(
                ITempFileCacheManager tempFileCacheManager,
                IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository,
                ICommonAppService commonAppService,
                ITenantSettingsAppService tenantSettingsAppService
        )
        : base(tempFileCacheManager)
        {
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _commonAppService = commonAppService;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportOrderSummary(ExportOrderInput input)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            var orderSummary = await GetOrderSummary(input);
            var productSetHeader = await _commonAppService.GetAllProductSets();


            return CreateExcelPackage(
                "OrderSummary" + Clock.Now.ToString(fileDateSetting) + ".xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("OrderReport"));
                    var headers = new List<string>
                        {
                            L("CustomerId"),
                            L("Name"),
                            L("PhoneNumber"),
                            L("Email"),
                            L("Address2Full"),
                            L("UnitNumber"),
                            L("PostalCode"),
                            L("Zone"),
                            L("DateOfOrder"),
                            L("Meal"),
                            L("Plan")
                        };
                    var positionProductSet = new Dictionary<int, string>();
                    var keyStart = headers.Count()+1;
                    foreach (var productSet in productSetHeader)
                    {
                        headers.Add(productSet.DisplayText);
                        positionProductSet.Add(keyStart, productSet.DisplayText);
                        keyStart++;
                    }

                    headers.Add(L("Total"));
                    headers.Add(L("Remarks"));

                    AddHeader(sheet, headers.ToArray());

                    var rowCount = 2;

                    var lastData = new Dictionary<int, int>();

                    foreach (var order in orderSummary)
                    {
                        var colCount = 1;
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.CustomerId);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.Name);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.PhoneNumber);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.Email);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.Address);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.UnitNumber);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.PostalCode);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.ZoneDisplay);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.OrderProductSet.DateOrder.ToString(fileDateSetting));
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.MealDisplay);
                        sheet.GetRow(rowCount).GetCell(colCount++).SetCellValue(order.Plan);

                        foreach (KeyValuePair<int, string> entry in positionProductSet)
                        {
                            if (order.OrderProductSet.NameProductSet == entry.Value)
                            {
                                sheet.GetRow(rowCount).GetCell(entry.Key).SetCellValue(order.OrderProductSet.Quantity);
                                if (!lastData.ContainsKey(entry.Key))
                                {
                                    lastData.Add(entry.Key, order.OrderProductSet.Quantity);
                                }
                                else lastData[entry.Key] = lastData[entry.Key] + order.OrderProductSet.Quantity;
                            }
                        }

                        var colTotal = 12 + productSetHeader.Count;
                        sheet.GetRow(rowCount).GetCell(colTotal).SetCellValue(order.OrderProductSet.Quantity);

                        if (!lastData.ContainsKey(colTotal))
                        {
                            lastData.Add(colTotal, order.OrderProductSet.Quantity);
                        }
                        else lastData[colTotal] = lastData[colTotal] + order.OrderProductSet.Quantity;

                        colTotal++;
                        sheet.GetRow(rowCount).GetCell(colTotal).SetCellValue(order.OrderProductSet.Remarks);
                        rowCount++;
                    }

                    var indexSum = 11;
                    sheet.GetRow(rowCount).GetCell(indexSum).SetCellValue("Total");
                    foreach (KeyValuePair<int, int> entry in lastData)
                    {
                        sheet.GetRow(rowCount).GetCell(entry.Key).SetCellValue(entry.Value);
                    }

                    // fit column
                    var lastCol = headers.Count();
                    for (var i = 1; i <= lastCol; i++) sheet.AutoSizeColumn(i);
                });
        }

        public async Task<FileDto> ExportOrderSummaryNewFormat(IOrderSummaryAppService appService, ExportOrderInput input)
        {
            if (input.FromDate == null || input.ToDate == null)
            {
                return null;
            }
            
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            int skipCount = 0;
            
            var orderSummary = await appService.GetListOfOrderViews(new GetOrderSummaryInput()
            {
                FromDate = input.FromDate.Value,
                ToDate = input.ToDate.Value,
                CustomerId = input.CustomerId,
                Filter = input.Filter,
                MaxResultCount = AppConsts.MaxPageSize,
                SkipCount = skipCount,
                Sorting = "DateOrder DESC",
                Meal = input.Meal,
                DeliveryType = input.DeliveryType,
                TimeSlotFormTo = input.TimeSlotFormTo
            });
            var file = new FileDto("OrderSummary-" + DateTime.Now.ToString("dd-MM-yyyy-HH:mm") + ".xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            var excelPackage = new XSSFWorkbook();
            var sheet = excelPackage.CreateSheet(L("OrderReport"));
            var headers = new List<string>

            {
                L("CustomerId"),
                L("Email"),
                L("Name"),
                L("PhoneNumber"),
                L("Address2Full"),
                L("UnitNumber"),
                L("PostalCode"),
                L("Grade"),
                L("Section"),
                L("Plan"),
                L("DateOfOrder"),
                L("TimeSlot"),
                L("Meal"),
                "Goods",
                "Qty",
                L("Remarks")
            };
            AddHeader(
                sheet, headers.ToArray()
            );

            AddHeader(sheet, headers.ToArray());

            var rowCount = 2;
            foreach (var customer in orderSummary)
            {
                var colCount = 0;
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Id);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Email);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Name);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.PhoneNumber);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Address);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.UnitNumber);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.PostalCode);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Grade);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Section);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Plan);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.DateOrder.ToString(fileDateSetting));
                if (string.IsNullOrWhiteSpace(customer.TimeSlotFormTo))
                {
                    sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(string.Empty);
                }
                else
                {

                    var fromToString = string.Empty;

                    try
                    {
                        var fromToDynamic = JsonConvert.DeserializeObject<dynamic>(customer.TimeSlotFormTo);
                        fromToString = $"{fromToDynamic.from} - {fromToDynamic.to}";
                    }
                    catch
                    {
                        // Ignore
                    }
                   

                    sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(fromToString);
                }
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Meal);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.SetName);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.Total);
                sheet.GetSheetRow(rowCount).GetSheetCell(colCount++).SetCellValue(customer.StrRemarks);
                rowCount++;
            }

            var myStyle = GetCellStyle(excelPackage, 18, true, false);

            sheet.GetSheetRow(rowCount).GetSheetCell(0,myStyle).SetCellValue(L("Total"));
            sheet.GetSheetRow(rowCount).GetSheetCell(12,myStyle).SetCellValue(orderSummary.Sum(a=>a.Total));


            for (var i = 1; i <= headers.Count(); i++) sheet.AutoSizeColumn(i);
            Save(excelPackage, file);
            return file;
        }

        private async Task<List<OrderSummaryExcelDto>> GetOrderSummary(ExportOrderInput input)
        {
            var list = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate)
                .WhereIf(input.Meal.HasValue, x => x.MealTimeId == input.Meal)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .ToListAsync();

            var list2 = list
                .GroupBy(e =>new { e.CustomerProductOffer, e.CustomerAddress})
                .Select(e => new OrderGroupProductSet
                {
                    CustomerId = e.Key.CustomerProductOffer.CustomerId,
                    PhoneNumber = e.Key.CustomerProductOffer.Customer.PhoneNumber,
                    Name = e.Key.CustomerProductOffer.Customer.Name,
                    Address = e.Key.CustomerAddress.Address1,
                    UnitNumber = e.Key.CustomerAddress.Address2,
                    PostalCode = e.Key.CustomerAddress.PostcalCode,
                    Plan = e.Key.CustomerProductOffer.ProductOffer.TiffinProductOfferType.Name,
                    Email = e.Key.CustomerProductOffer.Customer.EmailAddress,
                    ZoneDisplay = e.Key.CustomerAddress.Zone.HasValue
                        ? L(((ZoneEnum)e.Key.CustomerAddress.Zone.Value).ToString())
                        : string.Empty,
                    OrderProductSets = e
                        .Select(o => new OrderProductSet
                        {
                            Meal = o.MealTimeId,
                            MealTime = o.TiffinMealTime.Name,
                            DateOrder = o.OrderDate.Date,
                            NameProductSet = o.ProductSet.Name,
                            Quantity = o.Quantity,
                            Remarks = ConvertJsonAddOnToString(o.AddOn)
                        }).ToList()
                }).ToList();

            var list3 = list2
                .Where(c => c.OrderProductSets.Any())
                .Select(o => new OrderGroupProductSet
                {
                    CustomerId = o.CustomerId,
                    PhoneNumber = o.PhoneNumber,
                    Name = o.Name,
                    Address = o.Address,
                    UnitNumber = o.UnitNumber,
                    PostalCode = o.PostalCode,
                    ZoneDisplay = o.ZoneDisplay,
                    Email = o.Email,
                    Plan = o.Plan,
                    OrderProductSets = o.OrderProductSets.GroupBy(i => new { i.DateOrder.Date, i.NameProductSet, i.Meal, i.Remarks, i.MealTime })
                        .Select(e => new OrderProductSet
                        {
                            Meal = e.Key.Meal,
                            MealTime = e.Key.MealTime,
                            DateOrder = e.Key.Date,
                            NameProductSet = e.Key.NameProductSet,
                            Quantity = e.Sum(i => i.Quantity),
                            Remarks = e.First().Remarks
                        }).ToList()
                }).ToList();

            var result = (from cus in list3
                          from order in cus.OrderProductSets
                          select new OrderSummaryExcelDto
                          {
                              CustomerId = cus.CustomerId,
                              PhoneNumber = cus.PhoneNumber,
                              Email = cus.Email,
                              Name = cus.Name,
                              Address = cus.Address,
                              UnitNumber = cus.UnitNumber,
                              PostalCode = cus.PostalCode,
                              ZoneDisplay = cus.ZoneDisplay,
                              Plan = cus.Plan,
                              MealDisplay = order.MealTime,
                              OrderProductSet = order
                          }).WhereIf(!input.Filter.IsNullOrEmpty(),
                          e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                               EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                               EF.Functions.Like(e.Address, input.Filter.ToILikeString()) ||
                               EF.Functions.Like(e.UnitNumber, input.Filter.ToILikeString()) ||
                               EF.Functions.Like(e.PostalCode, input.Filter.ToILikeString()))
                          .OrderBy(o => o.OrderProductSet.DateOrder)
                         .ToList();
            return result;
        }

        protected string ConvertJsonAddOnToString(string input)
        {
            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
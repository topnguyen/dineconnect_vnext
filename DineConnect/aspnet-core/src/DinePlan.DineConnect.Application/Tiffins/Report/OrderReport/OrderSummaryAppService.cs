﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Report.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Core.Report;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport
{
    public class OrderSummaryAppService : DineConnectAppServiceBase, IOrderSummaryAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _tfCustomerProductOfferOrderRepository;
        private readonly IOrderSummaryExporter _exporter;

        public OrderSummaryAppService(IReportBackgroundAppService rbas, IBackgroundJobManager bgm,
            IOrderSummaryExporter orderSummaryExporter,
            IRepository<TiffinCustomerProductOfferOrder> tfCustomerProductOfferOrderRepository)
        {
            _rbas = rbas;
            _bgm = bgm;
            _tfCustomerProductOfferOrderRepository = tfCustomerProductOfferOrderRepository;
            _exporter = orderSummaryExporter;
        }

        public async Task<PagedResultDto<OrderSummaryViewDto>> GetAll(GetOrderSummaryInput input)
        {

            var offferOrders = await _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate.Value.Date)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate.Value.Date)
                .WhereIf(input.Meal.HasValue, x => x.MealTimeId == input.Meal)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo),
                    x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) ||
                         input.TimeSlotFormTo.Contains(x.TimeSlotFormTo))
                .ToListAsync();

            var orders = offferOrders
                .Select(e => new OrderSummaryViewDto
                {
                    Id = e.Id,
                    Address = e.CustomerAddress?.FullAddress,
                    UnitNumber = e.CustomerAddress?.Address2,
                    PostalCode = e.CustomerAddress?.PostcalCode,
                    Zone = e.CustomerAddress?.Zone,
                    Name = e.CustomerProductOffer?.Customer?.Name,
                    Plan = e.CustomerProductOffer?.ProductOffer?.TiffinProductOfferType.Name,
                    SetName = e.ProductSet.Name,
                    PhoneNumber = e.CustomerProductOffer?.Customer?.PhoneNumber,
                    DateOrder = e.OrderDate.Date,
                    Total = e.Quantity,
                    Email = e.CustomerProductOffer?.Customer?.EmailAddress,
                    TimeSlotFormTo = e.TimeSlotFormTo,
                    JsonData = e.CustomerProductOffer?.Customer?.JsonData,
                })
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.Address, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.UnitNumber, input.Filter.ToILikeString()) ||
                         EF.Functions.Like(e.PostalCode, input.Filter.ToILikeString()))
                .ToList();

            var totalCount = orders.Count();
            var pagedResult = orders.AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            foreach (var item in pagedResult)
            {
                item.ZoneText = item.Zone.HasValue && item.Zone != 0 ? L(((ZoneEnum)item.Zone.Value).ToString()) : string.Empty;
            }

            return new PagedResultDto<OrderSummaryViewDto>(totalCount, pagedResult);
        }
        public async Task<List<OrderSummaryViewDto>> GetListOfOrderViews(GetOrderSummaryInput input)
        {

            var orders = _tfCustomerProductOfferOrderRepository.GetAll()
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.Customer)
                .Include(t => t.CustomerProductOffer)
                .ThenInclude(t => t.ProductOffer)
                .ThenInclude(t => t.TiffinProductOfferType)
                .Include(t => t.ProductSet)
                .Include(t => t.TiffinMealTime)
                .Include(t => t.CustomerAddress)
                .Include(t => t.Payment)
                .WhereIf(input.CustomerId.HasValue, e => e.CustomerProductOffer.CustomerId == input.CustomerId)
                .WhereIf(input.FromDate.HasValue, x => x.OrderDate.Date >= input.FromDate.Value.Date)
                .WhereIf(input.ToDate.HasValue, x => x.OrderDate.Date <= input.ToDate.Value.Date)
                .WhereIf(input.Meal.HasValue, x => x.MealTimeId == input.Meal)
                .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                .WhereIf(input.Zone.HasValue, x => x.CustomerAddress.Zone == input.Zone)
                .WhereIf(input.DeliveryType.HasValue, x => x.DeliveryType == input.DeliveryType)
                .WhereIf(!string.IsNullOrWhiteSpace(input.TimeSlotFormTo),
                    x => x.TimeSlotFormTo == input.TimeSlotFormTo || x.TimeSlotFormTo.Contains(input.TimeSlotFormTo) ||
                         input.TimeSlotFormTo.Contains(x.TimeSlotFormTo)).ToList();

            var result = orders.Select(e => new OrderSummaryViewDto
            {
                Id = e.CustomerProductOffer.CustomerId,
                Address = e.CustomerAddress?.FullAddress,
                UnitNumber = e.CustomerAddress?.Address2,
                PostalCode = e.CustomerAddress?.PostcalCode,
                Zone = e.CustomerAddress?.Zone,
                Name = e.CustomerProductOffer.Customer.Name,
                Plan = e.CustomerProductOffer.ProductOffer.TiffinProductOfferType.Name,
                SetName = e.ProductSet.Name,
                PhoneNumber = e.CustomerProductOffer.Customer.PhoneNumber,
                DateOrder = e.OrderDate.Date,
                Total = e.Quantity,
                Email = e.CustomerProductOffer.Customer.EmailAddress,
                Meal = e.TiffinMealTime.Name,
                Remarks = e.AddOn,
                TimeSlotFormTo = e.TimeSlotFormTo,
                JsonData = e.CustomerProductOffer?.Customer?.JsonData,
            })
               .WhereIf(!input.Filter.IsNullOrEmpty(),
                   e => EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.PhoneNumber, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Address, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.UnitNumber, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.PostalCode, input.Filter.ToILikeString()))
               .ToList();

            foreach (var item in result)
            {
                item.ZoneText = item.Zone.HasValue && item.Zone != 0 ? L(((ZoneEnum)item.Zone.Value).ToString()) : string.Empty;
            }

            return result;
        }

        public async Task<FileDto> GetOrderSummaryExcel(ExportOrderInput input)
        {

            return await _exporter.ExportOrderSummaryNewFormat(this, input);
        }

        protected string ConvertJsonAddOnToString(string input)
        {
            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.MealPlanReport.Dto;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.Report.MealPlanReport
{
    public class MealPlanReportAppService : DineConnectAppServiceBase, IMealPlanReportAppService
    {
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepo;
        private readonly ICustomerReportAppService _customerReportService;

        public MealPlanReportAppService(IRepository<TiffinCustomerProductOffer> customerProductOfferRepo, ICustomerReportAppService customerReportService)
        {
            _customerProductOfferRepo = customerProductOfferRepo;
            _customerReportService = customerReportService;
        }

        #region Get Report

        public async Task<ListResultDto<MealPlanReportViewDto>> GetReport(MealPlanReportInput input)
        {
            var customerOffers = await _customerProductOfferRepo.GetAll()
                .Where(x => x.CreationTime >= input.From && x.CreationTime <= input.To)
                .Include(x => x.ProductOffer)
                .ToListAsync();

            switch (input.TimeRangeMode)
            {
                case TimeRangeMode.Day:
                    {
                        var report = GetDaysReport(input, customerOffers);

                        return new ListResultDto<MealPlanReportViewDto>(report);
                    }
                case TimeRangeMode.Month:
                    {

                        var report = GetMonthsReport(input, customerOffers);

                        return new ListResultDto<MealPlanReportViewDto>(report);
                    }
                case TimeRangeMode.Year:
                    {
                        var report = GetYearsReport(input, customerOffers);

                        return new ListResultDto<MealPlanReportViewDto>(report);
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private List<MealPlanReportViewDto> GetDaysReport(MealPlanReportInput input, List<TiffinCustomerProductOffer> customerProductOffers)
        {
            List<MealPlanReportViewDto> reports = new List<MealPlanReportViewDto>();

            for (int i = 0; i <= input.To.Subtract(input.From).Days; i++)
            {
                var date = input.From.AddDays(i);

                var report = new MealPlanReportViewDto
                {
                    Date = date,
                    Details = new List<MealPlanReportViewDetailDto>()
                };

                var customerProductOffersReport = customerProductOffers
                    .Where(x => x.CreationTime.Date == date.Date)
                    .GroupBy(x => x.ProductOfferId)
                    .ToList();

                foreach (var customerProductOfferReport in customerProductOffersReport)
                {
                    report.Details.Add(new MealPlanReportViewDetailDto
                    {
                        MealPlanId = customerProductOfferReport.Key,
                        MealPlanName = customerProductOfferReport.FirstOrDefault()?.ProductOffer.Name,
                        MealPlanTotalBought = customerProductOfferReport.Sum(x => x.PaidAmount).GetValueOrDefault()
                    });
                }

                reports.Add(report);
            }

            return reports;
        }

        private List<MealPlanReportViewDto> GetMonthsReport(MealPlanReportInput input, List<TiffinCustomerProductOffer> customerProductOffers)
        {
            List<MealPlanReportViewDto> reports = new List<MealPlanReportViewDto>();

            for (; new DateTime(input.From.Year, input.From.Month, 1) <= new DateTime(input.To.Year, input.To.Month, 1); input.From = input.From.AddMonths(1))
            {
                var report = new MealPlanReportViewDto
                {
                    Date = new DateTime(input.From.Year, input.From.Month, 1),
                    Details = new List<MealPlanReportViewDetailDto>()
                };

                var customerProductOffersReport = customerProductOffers
                    .Where(x => x.CreationTime.Month == input.From.Month && x.CreationTime.Year == input.From.Year)
                    .GroupBy(x => x.ProductOfferId)
                    .ToList();

                foreach (var customerProductOfferReport in customerProductOffersReport)
                {
                    report.Details.Add(new MealPlanReportViewDetailDto
                    {
                        MealPlanId = customerProductOfferReport.Key,
                        MealPlanName = customerProductOfferReport.FirstOrDefault()?.ProductOffer.Name,
                        MealPlanTotalBought = customerProductOfferReport.Sum(x => x.PaidAmount).GetValueOrDefault()
                    });
                }

                reports.Add(report);
            }


            return reports;
        }

        private List<MealPlanReportViewDto> GetYearsReport(MealPlanReportInput input, List<TiffinCustomerProductOffer> customerProductOffers)
        {
            List<MealPlanReportViewDto> reports = new List<MealPlanReportViewDto>();

            for (; input.From.Year <= input.To.Year; input.From = input.From.AddYears(1))
            {
                var report = new MealPlanReportViewDto
                {
                    Date = new DateTime(input.From.Year, 1, 1),
                    Details = new List<MealPlanReportViewDetailDto>()
                };

                var customerProductOffersReport = customerProductOffers
                    .Where(x => x.CreationTime.Year == input.From.Year)
                    .GroupBy(x => x.ProductOfferId)
                    .ToList();

                foreach (var customerProductOfferReport in customerProductOffersReport)
                {
                    report.Details.Add(new MealPlanReportViewDetailDto
                    {
                        MealPlanId = customerProductOfferReport.Key,
                        MealPlanName = customerProductOfferReport.FirstOrDefault()?.ProductOffer.Name,
                        MealPlanTotalBought = customerProductOfferReport.Sum(x => x.PaidAmount).GetValueOrDefault()
                    });
                }

                reports.Add(report);
            }

            return reports;
        }

        #endregion

        #region Get Customer Acquisition Report

        public async Task<ListResultDto<MealPLanCustomerAcquisitionViewDto>> GetCustomerAcquisitionReport(DateTime fromDate, DateTime toDate)
        {
            var customerProductOffersReports = await _customerProductOfferRepo.GetAll()
                .Where(x => x.CreationTime >= fromDate && x.CreationTime <= toDate)
                .Include(x => x.ProductOffer)
                .ToListAsync();


            var customerProductOffersReportGroup = customerProductOffersReports.GroupBy(x => x.ProductOfferId).ToList();

            var customersReport = await _customerReportService.GetData(new CustomerReportInput());

            var newCustomerIds = customersReport.Where(x => x.TotalTimeBuyMealPlan <= 1).Select(x => x.CustomerId).ToList();

            List<MealPLanCustomerAcquisitionViewDto> reports = new List<MealPLanCustomerAcquisitionViewDto>();

            foreach (var customerProductOfferReportGroup in customerProductOffersReportGroup)
            {
                var report = new MealPLanCustomerAcquisitionViewDto
                {
                    MealPlanId = customerProductOfferReportGroup.Key,
                    MealPlanName = customerProductOfferReportGroup.FirstOrDefault()?.ProductOffer.Name,
                    MealPlanTotalCountBoughtByNewCustomer = customerProductOfferReportGroup.Count(x => newCustomerIds.Contains(x.CustomerId))
                };

                report.MealPlanTotalCountBoughtByExistingCustomer = customerProductOfferReportGroup.Count() - report.MealPlanTotalCountBoughtByNewCustomer;

                reports.Add(report);
            }

            return new ListResultDto<MealPLanCustomerAcquisitionViewDto>(reports);
        }

        #endregion
    }
}
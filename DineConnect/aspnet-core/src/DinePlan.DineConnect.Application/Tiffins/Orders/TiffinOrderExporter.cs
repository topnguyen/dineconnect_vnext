﻿using Abp.AspNetZeroCore.Net;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Tiffins.Orders
{
    public class TiffinOrderExporter : NpoiExcelExporterBase, ITiffinOrderExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public TiffinOrderExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tempFileCacheManager = tempFileCacheManager;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportTiffinOrderSummary(List<OrderHistoryDto> orderHistory)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            return CreateExcelPackage(
                "OrderHistory.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("History"));

                    AddHeader(
                        sheet,
                        L("Date"),
                        L("MealTime"),
                        L("ChoiceOfSet"),
                        L("Quantity"),
                        L("DeliveryAddress")
                    );

                    AddObjects(
                        sheet, 2, orderHistory,
                        _ => _.OrderDate.ToString(fileDateSetting),
                        _ => _.MealTime,
                        _ => _.ChoiceOfSet,
                        _ => _.Quantity,
                        _ => _.DeliveryAddress
                    );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto PrintOrder(OrderHistoryForPrintDto orderHistory)
        {
            var pdffile = GenerateReport(orderHistory);

            return pdffile;
        }

        protected FileDto GenerateReport(OrderHistoryForPrintDto orderHistory)
        {
            Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            Font NormalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPCell cell = null;
                PdfPTable table = null;

                document.Open();

                table = new PdfPTable(1);
                table.TotalWidth = 400f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 1f });
                table.HorizontalAlignment = Element.ALIGN_RIGHT;

                InsertValueToCell(orderHistory.CustomerName, NormalFont, table, cell);
                InsertValueToCell(orderHistory.PhoneNumber, NormalFont, table, cell);
                InsertValueToCell(orderHistory.DeliveryAddress, NormalFont, table, cell);
                InsertValueToCell(orderHistory.PostalCode, NormalFont, table, cell);
                InsertValueToCell(orderHistory.Zone, NormalFont, table, cell);
                InsertValueToCell(orderHistory.DeliveryDate.ToString(AppConsts.FileDateFormat), NormalFont, table, cell);
                InsertValueToCell(orderHistory.MealTime, NormalFont, table, cell);
                InsertValueToCell(orderHistory.ProductSetName + " x " + orderHistory.Quantity, NormalFont, table, cell);
                InsertValueToCell(orderHistory.ProducOfferType, NormalFont, table, cell);

                document.Add(table);

                //Add border to page
                PdfContentByte content = writer.DirectContent;
                Rectangle rectangle = new Rectangle(document.PageSize);
                rectangle.Left += document.LeftMargin;
                rectangle.Right -= document.RightMargin;
                rectangle.Top -= document.TopMargin;
                rectangle.Bottom += document.BottomMargin;
                content.SetColorStroke(BaseColor.BLACK);
                content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                content.Stroke();

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = $"{orderHistory.CustomerName}-Order-{orderHistory.DeliveryDate.ToString(AppConsts.FileDateFormat)}.pdf",
                    FileToken = Guid.NewGuid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf,
                    Date = Clock.Now
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        private static void InsertValueToCell(string value, Font NormalFont, PdfPTable table, PdfPCell cell)
        {
            table.AddCell(PhraseCell(new Phrase(value, NormalFont), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.PaddingBottom = 10f;
            table.AddCell(cell);
        }

        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderColor = BaseColor.WHITE;
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 5f;
            return cell;
        }

        public FileDto PrintOrderDetail(PreparationReportViewDto input)
        {
            var pdffile = GenerateReportDetail(input);

            return pdffile;
        }

        private FileDto GenerateReportDetail(PreparationReportViewDto input)
        {
            Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            Font NormalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPCell cell = null;
                PdfPTable table = null;

                document.Open();

                table = new PdfPTable(1);
                table.TotalWidth = 400f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 1f });
                table.HorizontalAlignment = Element.ALIGN_RIGHT;

                InsertValueToCell(input.DateOrder.ToString(AppConsts.FileDateFormat), NormalFont, table, cell);
                InsertValueToCell(input.MealTime, NormalFont, table, cell);
                InsertValueToCell(input.NameProductSet + " x " + input.Quantity, NormalFont, table, cell);

                document.Add(table);

                //Add border to page
                PdfContentByte content = writer.DirectContent;
                Rectangle rectangle = new Rectangle(document.PageSize);
                rectangle.Left += document.LeftMargin;
                rectangle.Right -= document.RightMargin;
                rectangle.Top -= document.TopMargin;
                rectangle.Bottom += document.BottomMargin;
                content.SetColorStroke(BaseColor.BLACK);
                content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                content.Stroke();

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = $"Order-{input.DateOrder.ToString(AppConsts.FileDateFormat)}.pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf,
                    Date = input.DateOrder
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        public async Task<FileDto> PrintOrderDetailsAsZip(List<FileDto> files)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            var zipFileDto = new FileDto()
            {
                FileName = $"Order-{files.FirstOrDefault()?.Date?.ToString(fileDateSetting)}.zip",
                FileToken = new Guid().ToString("N"),
                FileType = MimeTypeNames.ApplicationZip
            };

            using (var outputZipFileStream = new MemoryStream())
            {
                using (var zipStream = new ZipArchive(outputZipFileStream, ZipArchiveMode.Create))
                {
                    foreach (var logFile in files)
                    {
                        var bytes = _tempFileCacheManager.GetFile(logFile.FileToken);

                        var entry = zipStream.CreateEntry(logFile.FileToken + ".pdf");
                        using (var entryStream = entry.Open())
                        {
                            using (var fs = new MemoryStream(bytes))
                            {
                                fs.CopyTo(entryStream);
                                entryStream.Flush();
                            }
                        }
                    }
                }

                _tempFileCacheManager.SetFile(zipFileDto.FileToken, outputZipFileStream.ToArray());
            }

            return zipFileDto;
        }
    }
}
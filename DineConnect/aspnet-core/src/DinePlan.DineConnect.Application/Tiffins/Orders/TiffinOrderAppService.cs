﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.Timing.Timezone;
using Abp.UI;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;
using DinePlan.DineConnect.Tiffins.EmailNotification;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MealTimes;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.OrderTags;
using DinePlan.DineConnect.Tiffins.Payment;
using DinePlan.DineConnect.Tiffins.Payment.Omise;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NUglify.Helpers;

namespace DinePlan.DineConnect.Tiffins.Orders
{
    public class TiffinOrderAppService : DineConnectAppServiceBase, ITiffinOrderAppService
    {
        private readonly IRepository<CustomerAddressDetail> _customerAddressRepository;
        private readonly IRepository<TiffinCustomerProductOfferOrder> _customerProductOfferOrderRepository;
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepository;
        private readonly IRepository<BaseCore.Customer.Customer> _customerRepository;
        private readonly IEmailNotificationBackgroundJobService _emailNotificationBackgroundJobService;
        private readonly IRepository<TiffinScheduleDetail> _scheduleDetailRepository;
        private readonly IStripePaymentGatewayManager _stripePaymentGatewayManager;
        private readonly ITiffinInvoiceNumberGenerator _tiffinInvoiceNumberGenerator;
        private readonly IRepository<TiffinMealTime> _tiffinMealTimeRepository;
        private readonly IRepository<TiffinProductOffer> _tiffinProductOfferRepository;
        private readonly ITiffinOrderExporter _tiffinOrderExporter;
        private readonly IRepository<TiffinOrderTag> _tiffinOrderTagRepository;
        private readonly IRepository<TiffinPayment> _tiffinPaymentRepository;
        private readonly IRepository<TiffinPaymentRequest> _tiffinPaymentRequestRepo;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        private readonly IAdyenPaymentProcessor _adyenPaymentProcessor;
        private readonly IStripePaymentProcessor _stripePaymentProcessor;
        private readonly IPaypalPaymentProcessor _paypalPaymentProcessor;
        private readonly IGooglePaymentProcessor _googlePaymentProcessor;
        private readonly IRepository<TiffinPromotionCode> _tiffinPromotionCode;
        private readonly ISwipePaymentProcessor _swipePaymentProcessor;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IEmailNotificationAppService _emailNotificationAppService;

        public TiffinOrderAppService(IRepository<TiffinScheduleDetail> scheduleDetailRepository,
            ITimeZoneConverter timeZoneConverter
            , IRepository<TiffinCustomerProductOffer> customerProductOfferRepository
            , IRepository<TiffinCustomerProductOfferOrder> customerProductOfferOrderRepository
            , IRepository<BaseCore.Customer.Customer> customerRepository
            , IRepository<CustomerAddressDetail> customerAddressRepository,
            ITiffinOrderExporter tiffinOrderExporter, IRepository<TiffinPaymentRequest> tiffinPaymentRequestRepo
            , IRepository<TiffinOrderTag> tiffinOrderTagRepository
            , IStripePaymentGatewayManager stripePaymentGatewayManager,
            ITiffinInvoiceNumberGenerator tiffinInvoiceNumberGenerator,
            IRepository<TiffinPayment> tiffinPaymentRepository,
            IEmailNotificationBackgroundJobService emailNotificationBackgroundJobService
            , IRepository<TiffinMealTime> tiffinMealTimeRepository
            , IRepository<TiffinProductOffer> tiffinProductOfferRepository,
            IOmisePaymentProcessor omisePaymentProcessor,
            IAdyenPaymentProcessor adyenPaymentProcessor,
            IStripePaymentProcessor stripePaymentProcessor,
            IPaypalPaymentProcessor paypalPaymentProcessor,
            IGooglePaymentProcessor googlePaymentProcessor,
            IRepository<TiffinPromotionCode> tiffinPromotionCode,
            ISwipePaymentProcessor swipePaymentProcessor,
            ITenantSettingsAppService tenantSettingsAppService,
            IEmailNotificationAppService emailNotificationAppService
        )
        {
            _scheduleDetailRepository = scheduleDetailRepository;
            _customerProductOfferRepository = customerProductOfferRepository;
            _customerProductOfferOrderRepository = customerProductOfferOrderRepository;
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _tiffinOrderExporter = tiffinOrderExporter;
            _tiffinOrderTagRepository = tiffinOrderTagRepository;
            _stripePaymentGatewayManager = stripePaymentGatewayManager;
            _tiffinInvoiceNumberGenerator = tiffinInvoiceNumberGenerator;
            _tiffinPaymentRepository = tiffinPaymentRepository;
            _emailNotificationBackgroundJobService = emailNotificationBackgroundJobService;
            _tiffinMealTimeRepository = tiffinMealTimeRepository;
            _tiffinProductOfferRepository = tiffinProductOfferRepository;
            _tiffinPaymentRequestRepo = tiffinPaymentRequestRepo;
            _timeZoneConverter = timeZoneConverter;
            _omisePaymentProcessor = omisePaymentProcessor;
            _adyenPaymentProcessor = adyenPaymentProcessor;
            _stripePaymentProcessor = stripePaymentProcessor;
            _paypalPaymentProcessor = paypalPaymentProcessor;
            _googlePaymentProcessor = googlePaymentProcessor;
            _tiffinPromotionCode = tiffinPromotionCode;
            _swipePaymentProcessor = swipePaymentProcessor;
            _tenantSettingsAppService = tenantSettingsAppService;
            _emailNotificationAppService = emailNotificationAppService;
        }

        public async Task<PagedResultDto<ProducSetForOrderDto>> GetProductSets(GetProductSetForOrderInput input)
        {
            var offerType = await _customerProductOfferRepository
                .GetAllIncluding(e => e.ProductOffer.TiffinProductOfferType)
                .WhereIf(input.CustomerOfferId.HasValue, e => e.Id == input.CustomerOfferId)
                .Select(e => e.ProductOffer.TiffinProductOfferType)
                .FirstOrDefaultAsync();

            var productOffers = _customerProductOfferRepository.GetAll()
                .Include(e => e.ProductOffer)
                .ThenInclude(e => e.ProductOfferSets)
                .ThenInclude(oc => oc.TiffinProductSet)
                .Where(e => e.Customer.UserId == AbpSession.UserId)
                .WhereIf(!input.IsEdit, e => e.BalanceCredit > 0)
                .WhereIf(input.CustomerOfferId.HasValue, e => e.ProductOffer.TiffinProductOfferTypeId == offerType.Id)
                .Where(e => e.ProductOffer.Expiry && e.ExpiryDate.Date >= input.Date.Date || !e.ProductOffer.Expiry);

            var scheduleDetails = _scheduleDetailRepository.GetAll()
                .Include(e => e.ProductSet)
                .Include(e => e.Products)
                .ThenInclude(e => e.Product)
                .WhereIf(input.ScheduleType.HasValue, e => e.Schedule.MealTimeId == input.ScheduleType)
                .Where(e => e.Schedule.Date.Date == input.Date.Date);

            var productSets = new List<ProducSetForOrderDto>();
            foreach (var item in await productOffers.ToListAsync())
            {
                var expiry = item.ProductOffer.Expiry ? $"{item.ExpiryDate:dd/MM/yyyy}" : "NA";

                DateTime? expiryDate = item.ProductOffer.Expiry ? item.ExpiryDate : new DateTime(9999, 01, 01);

                var producSetForOrders = await scheduleDetails.Where(e =>
                        item.ProductOffer.ProductSets.Select(p => p.Id).Contains(e.ProductSetId))
                    .Select(e => new ProducSetForOrderDto
                    {
                        ProductSetName = e.ProductSet.Name,
                        ProductSetNameExtra = $"{e.ProductSet.Name} (Expiry: {expiry})",
                        BalanceCredit = (int)item.BalanceCredit,
                        CustomerProductOfferId = item.Id,
                        Image = e.ProductSet.Images,
                        ScheduleDate = e.Schedule.Date.Date,
                        ProductSetId = e.ProductSetId,
                        ExpiryDate = expiryDate
                    })
                    .Distinct()
                    .ToListAsync();

                productSets.AddRange(producSetForOrders);
            }

            productSets = productSets.Distinct()
                .OrderBy(p => p.ProductSetName)
                .ThenBy(p => p.BalanceCredit).ToList();

            var result = GetDistinctProductSets(input, productSets).Where(p => p != null).ToList();
            //var result = productSets.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ProducSetForOrderDto>(productSets.Count(), result);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetOfferByCustomerId(int customerId)
        {
            var query = _customerProductOfferRepository.GetAll()
                .Where(e => e.Customer.UserId == AbpSession.UserId) // customerId = AbpSession.UserId
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.ProductOffer.Name));
            var result = await query.ToListAsync();

            return new ListResultDto<ComboboxItemDto>(result);
        }

        public async Task<List<CreateOrderInput>> CreateListOrder(List<CreateOrderInput> orders)
        {
            var finalOrders = new List<CreateOrderInput>();

            foreach (var item in orders)
            {
                if (item.OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    throw new UserFriendlyException("Oops... Sunday, We do not accept Order");
                }

                var curtOffTime = await GetCutOffProceed(item.OrderDate);

                if (!curtOffTime)
                {
                    throw new UserFriendlyException("Oops... Cut Off Time already Passed");
                }

                var customerOffer = await _customerProductOfferRepository.GetAsync(item.CustomerProductOfferId);

                if (customerOffer.BalanceCredit > 0)
                {
                    await CreateOrder(item);

                    finalOrders.Add(item);
                }
            }

            return finalOrders;
        }

        [UnitOfWork]
        public async Task CreateOrder(CreateOrderInput input)
        {
            var order = ObjectMapper.Map<TiffinCustomerProductOfferOrder>(input);

            if (input.DeliveryType == TiffinDeliveryTimeSlotType.SelfPickup)
            {
                order.CustomerAddressId = null;
            }
            else
            {
                order.SelfPickupLocationId = null;
            }

            order.ProductSet = null;

            order.AddOn = input.AddOn;

            if (input.UseDefaultAddress)
            {
                order.Address = (await _customerRepository.FirstOrDefaultAsync(c => c.UserId == AbpSession.UserId))?.Address1;
            }

            await _customerProductOfferOrderRepository.InsertAsync(order);

            var customerOffer = await _customerProductOfferRepository.GetAsync(input.CustomerProductOfferId);

            var customerOffers =
                await _customerProductOfferRepository
                    .GetAll()
                    .Where(c => c.Customer.UserId == AbpSession.UserId)
                    .Where(e => e.ProductOfferId == customerOffer.ProductOfferId)
                    .Where(e => (!e.ProductOffer.Expiry || e.ExpiryDate.Date > Clock.Now.Date) && e.BalanceCredit > 0)
                    .ToListAsync();

            var minQuantity = await GetMinQuantityInSetting();

            var companyInfo = await GetCompanyInfo();

            var k = 0;

            for (int i = input.Quantity; i > 0; k++)
            {
                if (customerOffers.Count - 1 < k)
                {
                    break;
                }

                var tiffinCustomerProductOffer = customerOffers[k];

                if (i > tiffinCustomerProductOffer.BalanceCredit)
                {
                    i -= tiffinCustomerProductOffer.BalanceCredit.Value;

                    tiffinCustomerProductOffer.BalanceCredit = 0;
                }
                else
                {
                    tiffinCustomerProductOffer.BalanceCredit = tiffinCustomerProductOffer.BalanceCredit - i;

                    i = 0;
                }

                if (minQuantity.HasValue)
                {
                    if (minQuantity >= tiffinCustomerProductOffer.BalanceCredit)
                    {
                        await _emailNotificationBackgroundJobService.SendEmailWhenMinimumQuantityInBackground(
                            new OrderMinQuantity
                            {
                                MealPlanName = input.ProductSetName,
                                AvailableBalance = tiffinCustomerProductOffer.BalanceCredit ?? 0,
                                ExpiryDate = tiffinCustomerProductOffer.ExpiryDate
                            }, companyInfo);
                    }
                }
                await _customerProductOfferRepository.UpdateAsync(tiffinCustomerProductOffer);
            }
        }

        public async Task<PagedResultDto<OrderHistoryDto>> GetOrderHistory(GetOrderHistoryInputDto input)
        {
            var userId = AbpSession.UserId;

            var query = _customerProductOfferOrderRepository.GetAll()
                .Include(c => c.CustomerProductOffer)
                .ThenInclude(c => c.Customer)
                .Include(c => c.CustomerAddress)
                .Include(c => c.ProductSet)
                //.Where(e => e.CustomerProductOffer.Customer.UserId == userId)
                .Select(i => new OrderHistoryDto
                {
                    ProductOfferName = i.CustomerProductOffer.ProductOffer.Name,
                    CustomerId = i.CustomerProductOffer.CustomerId,
                    CustomerName = i.CustomerProductOffer.Customer.Name,
                    OrderId = i.Id,
                    OrderDate = i.OrderDate.Date,
                    MealTime = i.TiffinMealTime.Name,
                    ChoiceOfSet = i.ProductSet.Name,
                    Quantity = i.Quantity,
                    DeliveryAddress = i.CustomerAddress == null ? string.Empty : i.CustomerAddress.FullAddress,
                    CustomerProductOfferId = i.CustomerProductOfferId,
                    Amount = i.Amount,
                    Address = i.Address,
                    CustomerAddressId = i.CustomerAddressId,
                    MealTimeId = i.MealTimeId,
                    AddOn = i.AddOn,
                    PaymentId = i.PaymentId,
                    MenuItemsDynamic = i.MenuItemsDynamic,
                    DeliveryType = i.DeliveryType,
                    DeliveryCharge = i.DeliveryCharge,
                    TimeSlotFormTo = i.TimeSlotFormTo,
                    SelfPickupLocationId = i.SelfPickupLocationId,
                    PickedUpTime = i.PickedUpTime,
                    JsonData = i.CustomerProductOffer.Customer.JsonData
                });

            if (input.CustomerId != null)
            {
                query = query.Where(x => x.CustomerId == input.CustomerId);
            }

            if (input.OrderDate != null)
            {
                query = query.Where(x => x.OrderDate == input.OrderDate.Value);
            }

            if (!string.IsNullOrWhiteSpace(input.Term))
            {
                query = query.Where(x => x.JsonData.Contains(input.Term) || x.CustomerName.Contains(input.Term));
            }

            if (input.PickedUp != null)
            {
                query = input.PickedUp == true ? query.Where(x => x.PickedUpTime != null) : query.Where(x => x.PickedUpTime == null);
            }

            var result = await query.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var count = query.Select(x => x.OrderId).ToList().Count;

            return new PagedResultDto<OrderHistoryDto>(count, result);
        }

        public Task<ListResultDto<PickUpReportDto>> GetPickUpOrderReport(GetPickUpOrderReportInputDto input)
        {
            var allOrders = _customerProductOfferOrderRepository.GetAll()
                .Where(e => e.OrderDate >= input.From && e.OrderDate <= input.To)
                .Select(i => new
                {
                    OrderId = i.Id,
                    Date = i.OrderDate.Date,
                    PickedUpTime = i.PickedUpTime
                }).ToList();

            var pickupReport = allOrders.GroupBy(x => x.Date.Date).Select(x => new PickUpReportDto
            {
                Date = x.Key,
                TotalPickedUpOrder = x.Count(y => y.PickedUpTime != null),
                TotalNotPickedUpOrder = x.Count(y => y.PickedUpTime == null)
            }).ToList();

            return Task.FromResult(new ListResultDto<PickUpReportDto>(pickupReport));
        }

        public async Task SendPickUpOrderRemindEmail(SendPickUpOrderRemindInput input)
        {
            if (input?.OrderIds?.Any() != true)
            {
                return;
            }

            input.OrderIds = input.OrderIds.Distinct().ToList();

            var orders =
                await _customerProductOfferOrderRepository
                    .GetAll()
                    .Where(x => input.OrderIds.Contains(x.Id) && x.DeliveryType == TiffinDeliveryTimeSlotType.SelfPickup)
                    .Include(x => x.CustomerProductOffer.Customer)
                    .ToListAsync();

            foreach (var order in orders)
            {
                await _emailNotificationAppService.SendPickUpOrderRemind(order.CustomerProductOffer.Customer.EmailAddress, order.CustomerProductOffer.Customer.Name, order.CustomerProductOffer.Customer.TenantId);
            }
        }

        public async Task ChangePickupStatus(ChangePickupStatus input)
        {
            var entity =
                _customerProductOfferOrderRepository
                    .GetAll()
                    .Include(x => x.CustomerProductOffer)
                    .ThenInclude(x => x.Customer)
                .FirstOrDefault(x => x.Id == input.OrderId);

            entity.PickedUpTime = input.IsPickup ? Clock.Now : (DateTime?)null;

            _customerProductOfferOrderRepository.Update(entity);

            if (entity.PickedUpTime != null)
            {
                await _emailNotificationAppService.SendPickUpOrderSuccess(entity.CustomerProductOffer.Customer.EmailAddress, entity.CustomerProductOffer.Customer.Name, entity.CustomerProductOffer.Customer.TenantId);
            }
        }

        public async Task<FileDto> ExportOrderHistory()
        {
            var orderHistory = await _customerProductOfferOrderRepository.GetAll()
                .Include(c => c.CustomerProductOffer)
                .ThenInclude(c => c.Customer)
                .Include(c => c.CustomerAddress)
                .Include(c => c.ProductSet)
                .Where(e => e.CustomerProductOffer.Customer.UserId == AbpSession.UserId)
                .Select(i => new OrderHistoryDto
                {
                    OrderId = i.Id,
                    OrderDate = i.OrderDate,
                    MealTime = i.TiffinMealTime.Name,
                    ChoiceOfSet = i.ProductSet.Name,
                    Quantity = i.Quantity,
                    DeliveryAddress = i.CustomerAddress.FullAddress
                }).ToListAsync();

            return await _tiffinOrderExporter.ExportTiffinOrderSummary(orderHistory);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetScheduleTimes()
        {
            var query = _tiffinMealTimeRepository.GetAll() // customerId = AbpSession.UserId
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name));
            var result = await query.ToListAsync();

            return new ListResultDto<ComboboxItemDto>(result);
        }

        public async Task<FileDto> PrintOrderHistory(long orderId)
        {
            var result = await GetOrderDetailForPrint(orderId);

            return _tiffinOrderExporter.PrintOrder(result);
        }

        public async Task<List<UpdateMyAddressInputDto>> GetCustomerAddresses()
        {
            var customer = await _customerRepository.GetAll()
                .Include(c => c.CustomerAddressDetails)
                .ThenInclude(c => c.City)
                .Include(c => c.Country)
                .FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

            var result = ObjectMapper.Map<List<UpdateMyAddressInputDto>>(customer?.CustomerAddressDetails);
            return result;
        }

        public async Task<UpdateMyAddressInputDto> GetCustomerAddressesById(int addressId)
        {
            var customerAddress = await _customerAddressRepository
                .FirstOrDefaultAsync(t => t.Id == addressId);

            var result = ObjectMapper.Map<UpdateMyAddressInputDto>(customerAddress);
            return result;
        }

        public async Task<bool> GetCutOffProceed(DateTime orderDate)
        {
            DateTime? orderCutOffTime = await GetCutOffTime();

            if (AbpSession.TenantId != null)
            {
                var nowDateTime = _timeZoneConverter.Convert(Clock.Now, AbpSession.TenantId.Value);

                if (nowDateTime != null)
                {
                    var nowTime = nowDateTime.Value.TimeOfDay;

                    var orderTime = orderDate.Date.Add(nowTime);

                    if (orderCutOffTime != null && orderTime.Date <= orderCutOffTime.Value.Date)
                    {
                        return false;
                    }

                    var tomorrowDate = nowDateTime.Value.Date.AddDays(1);

                    if (orderTime.Date == tomorrowDate)
                    {
                        if (orderCutOffTime != null && nowTime < orderCutOffTime.Value.TimeOfDay)
                        {
                            return true;
                        }

                        return false;
                    }
                }
            }

            return true;
        }

        public async Task ValidateCutOff(List<DateTime> orderDate)
        {
            var tenantSettings = await _tenantSettingsAppService.GetAllSettings();

            var allowOrderInFutureDate = Clock.Now.Date.AddDays(tenantSettings.TiffinAllowOrderInFutureDays);

            foreach (var dateTime in orderDate)
            {
                if (dateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    throw new UserFriendlyException("Oops... Sunday, We do not accept Order");
                }

                var curtOffTime = await GetCutOffProceed(dateTime);

                if (!curtOffTime)
                {
                    throw new UserFriendlyException($"Oops... Your requested date {dateTime.Date:d} error - Cut Off Time already Passed");
                }

                if (tenantSettings.TiffinAllowOrderInFutureDays >= 0)
                {
                    if (dateTime > allowOrderInFutureDate)
                    {
                        throw new UserFriendlyException($"Cannot order for the date {dateTime:d} because it so far out");
                    }
                }
            }
        }

        public async Task<DateTime?> GetCutOffTime()
        {
            if (AbpSession.TenantId != null)
            {
                var cutOffTime = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.MealSetting.OrderCutOffTime,
                    (int)AbpSession.TenantId);

                DateTime? orderCutOffTime = null;
                if (!cutOffTime.IsNullOrEmpty() && cutOffTime != "null")
                {
                    var orderCutOffTimeString = JsonConvert.DeserializeObject<string>(cutOffTime);
                    orderCutOffTime = DateTime.Parse(orderCutOffTimeString);
                    orderCutOffTime = _timeZoneConverter.Convert(orderCutOffTime, AbpSession.TenantId.Value);
                }

                if (orderCutOffTime.HasValue)
                {
                    var tenantSettings = await _tenantSettingsAppService.GetAllSettings();

                    var cutoffDate = Clock.Now;

                    if (_timeZoneConverter != null)
                                cutoffDate = _timeZoneConverter.Convert(cutoffDate, AbpSession.TenantId.Value).Value;

                    cutoffDate = cutoffDate.AddDays(tenantSettings.TiffinCutOffDays).Date;

                    var cutoffTime = orderCutOffTime.Value.TimeOfDay;

                    return cutoffDate + cutoffTime;
                }
            }

            return null;
        }

        public async Task<CreateOrderInput> GetOrderForEdit(int id)
        {
            var order = await _customerProductOfferOrderRepository.GetAll().Include(e => e.ProductSet)
                .FirstOrDefaultAsync(e => e.Id == id);

            return ObjectMapper.Map<CreateOrderInput>(order);
        }

        public async Task UpdateDeliveryInfo(CreateOrderInput input)
        {
            if (input.OrderDate.DayOfWeek == DayOfWeek.Sunday)
            {
                throw new UserFriendlyException("Oops... Sunday, We do not accept Order");
            }

            var curtOffTime = await GetCutOffProceed(input.OrderDate);
            if (!curtOffTime)
                throw new UserFriendlyException("Oops... Cut Off Time already Passed");

            var order = await _customerProductOfferOrderRepository.GetAll()
                .FirstOrDefaultAsync(e => e.Id == input.Id.Value);

            var customerOffer = await _customerProductOfferRepository.GetAsync(input.CustomerProductOfferId);
            var lastCustomerOffer = await _customerProductOfferRepository.GetAsync(order.CustomerProductOfferId);

            if (lastCustomerOffer != customerOffer)
            {
                customerOffer.BalanceCredit = customerOffer.BalanceCredit - input.Quantity;
                lastCustomerOffer.BalanceCredit = lastCustomerOffer.BalanceCredit + order.Quantity;
            }
            else
            {
                customerOffer.BalanceCredit = customerOffer.BalanceCredit - input.Quantity + order.Quantity;
            }

            order.ProductSetId = input.ProductSetId;
            order.CustomerProductOfferId = input.CustomerProductOfferId;
            order.CustomerAddressId = input.CustomerAddressId;
            order.Quantity = input.Quantity;
            order.OrderDate = input.OrderDate;
            order.Address = input.Address;
            order.AddOn = input.AddOn;
            order.MealTimeId = input.MealTimeId;

            await _customerProductOfferOrderRepository.UpdateAsync(order);
            await _customerProductOfferRepository.UpdateAsync(customerOffer);
            await _customerProductOfferRepository.UpdateAsync(lastCustomerOffer);
        }

        public async Task<PaymentOutputDto> CreatePaymentForOrder(CreateOrderPaymentInputDto input)
        {
            var tenantSettings = await _tenantSettingsAppService.GetAllSettings();

            var allowOrderInFutureDate = Clock.Now.Date.AddDays(tenantSettings.TiffinAllowOrderInFutureDays);

            foreach (var createOrderInput in input.PurcharsedOrderList)
            {
                // Validation for PurcharsedOrderList

                if (createOrderInput.DeliveryType == TiffinDeliveryTimeSlotType.Delivery && createOrderInput.CustomerAddressId == null)
                {
                    throw new UserFriendlyException("Order Delivery must have Address");
                }

                // Validation Future Date

                if (tenantSettings.TiffinAllowOrderInFutureDays >= 0)
                {
                    if (createOrderInput.OrderDate > allowOrderInFutureDate)
                    {
                        throw new UserFriendlyException($"Cannot order for the date {createOrderInput.OrderDate:d} because it so far out");
                    }
                }
            }


            PaymentOutputDto output = new PaymentOutputDto();

            var tiffPaymentRe = new TiffinPaymentRequest
            {
                PaymentOption = input.PaymentOption,
                PaidAmount = input.PaidAmount ?? 0,
                PaymentDate = Clock.Now,
                Request = JsonConvert.SerializeObject(input)
            };

            var repoId = await _tiffinPaymentRequestRepo.InsertAndGetIdAsync(tiffPaymentRe);

            await CurrentUnitOfWork.SaveChangesAsync();

            if (input.CallbackUrl.EndsWith("/"))
            {
                input.CallbackUrl += repoId;
            }
            else
            {
                input.CallbackUrl += "/" + repoId;
            }

            // Omise

            if (input.PaymentOption == "omise" || input.PaymentOption == "paynow")
            {
                OmisePaymentResponseDto omiseChargeResult;

                if (input.PaymentOption == "omise")
                {
                    omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                    {
                        OmiseToken = input.OmiseToken,
                        Amount = Convert.ToInt64(input.PaidAmount * 100),
                        Currency = DineConnectConsts.Currency,
                        ReturnUrl = input.CallbackUrl
                    });
                }
                else
                {
                    omiseChargeResult = await _omisePaymentProcessor.GetStatus(input.OmiseToken);
                }

                if (omiseChargeResult != null)
                {
                    output.Message = omiseChargeResult.Message;
                    output.PurchasedSuccess = omiseChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = output.OmiseId = omiseChargeResult.PaymentId;
                    output.AuthenticateUri = omiseChargeResult.AuthorizeUri;
                }
            }

           
            // Swipe

            if (input.PaymentOption == "swipe")
            {
                var swipeChargeResult = await _swipePaymentProcessor.ProcessPayment(new SwipePaymentRequestDto
                {
                    Amount = input.PaidAmount.Value,
                    OTP = input.SwipeOTP,
                    CardNumber = input.SwipeCardNumber
                });

                if (swipeChargeResult != null)
                {
                    output.Message = swipeChargeResult.Message;
                    output.PurchasedSuccess = swipeChargeResult.IsSuccess;
                    tiffPaymentRe.PaymentReference = swipeChargeResult.PaymentId;
                }
            }

            if (output.PurchasedSuccess || string.IsNullOrWhiteSpace(input.PaymentOption))
            {
                await CreatePaymentRecords(input);
            }

            return output;
        }

        public async Task DeleteOrder(EntityDto input)
        {
            var order = await _customerProductOfferOrderRepository.GetAll()
                .Include(e => e.ProductSet)
                .Include(e => e.CustomerProductOffer)
                .FirstOrDefaultAsync(e => e.Id == input.Id);

            var customerOffer = await _customerProductOfferRepository.GetAsync(order.CustomerProductOfferId);

            customerOffer.BalanceCredit = customerOffer.BalanceCredit + order.Quantity;

            await _customerProductOfferRepository.UpdateAsync(customerOffer);
            await CurrentUnitOfWork.SaveChangesAsync();

            await _customerProductOfferOrderRepository.DeleteAsync(order);
        }

        public async Task<ListResultDto<AddonListDto>> GetAllOrderTags()
        {
            var tiffinOrderTags = _tiffinOrderTagRepository.GetAll()
                .Select(o => ObjectMapper.Map<AddonListDto>(o));

            return new ListResultDto<AddonListDto>(await tiffinOrderTags.ToListAsync());
        }

        public FileDto PrintOrderDetail(PreparationReportViewDto input)
        {
            return _tiffinOrderExporter.PrintOrderDetail(input);
        }

        public async Task<FileDto> PrintOrderHistories(List<long> orderIds)
        {
            var files = new List<FileDto>();

            foreach (var orderId in orderIds)
            {
                var order = await GetOrderDetailForPrint(orderId);
                var quantity = order.Quantity;
                for (var i = 0; i < quantity; i++)
                {
                    var orderDetail = order;
                    orderDetail.Quantity = 1;
                    var file = _tiffinOrderExporter.PrintOrder(orderDetail);
                    files.Add(file);
                }
            }

            return await _tiffinOrderExporter.PrintOrderDetailsAsZip(files);
        }

        public async Task<List<OrderHistoryForPrintDto>> GetOrderDetailsForPrintPreparationReport(List<long> orderIds)
        {
            var orderDetails = new List<OrderHistoryForPrintDto>();

            foreach (var orderId in orderIds)
            {
                var order = await GetOrderDetailForPrint(orderId);
                var quantity = order.Quantity;
                for (var i = 0; i < quantity; i++)
                {
                    var orderDetail = order;
                    orderDetail.Quantity = 1;
                    orderDetails.Add(orderDetail);
                }
            }

            return orderDetails;
        }

        public async Task<PaymentOutputDto> ValidatePayment(EntityDto<int> input)
        {
            var output = new PaymentOutputDto();

            if (input.Id > 0)
            {
                var paymentRequest = await _tiffinPaymentRequestRepo.GetAsync(input.Id);
                if (paymentRequest != null)
                {
                    var changeResult =
                        await _omisePaymentProcessor.GetStatus(paymentRequest.PaymentReference);

                    output.Message = changeResult.Message;
                    output.AuthenticateUri = changeResult.AuthorizeUri;

                    output.Message = changeResult.Message;
                    output.OmiseId = changeResult.PaymentId;
                    output.PurchasedSuccess = changeResult.IsSuccess;

                    if (changeResult.IsSuccess)
                    {
                        var myReturn = await CreatePaymentRecords(
                            JsonConvert.DeserializeObject<CreateOrderPaymentInputDto>(paymentRequest.Request));
                        output.PurchasedSuccess = myReturn.PurchasedSuccess;
                        output.PaymentId = myReturn.PaymentId;

                        await _tiffinPaymentRequestRepo.DeleteAsync(input.Id);

                    }
                }
            }
            else
            {
                output.PurchasedSuccess = false;
                output.Message = "No Payment Request";
            }

            return output;
        }

        private async Task<CompanySettingDto> GetCompanyInfo()
        {
            var companyNameJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.CompanySetting.CompanyName,
                AbpSession.GetTenantId());

            var companyLogoUrlJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CompanySetting.CompanyLogoUrl,
                    AbpSession.GetTenantId());

            var companyAddress =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingAddress,
                    AbpSession.GetTenantId());

            var taxNo =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingTaxVatNo,
                    AbpSession.GetTenantId());
            var companyName = string.Empty;
            if (!companyNameJson.IsNullOrEmpty()) companyName = JsonConvert.DeserializeObject<string>(companyNameJson);

            var companyLogoUrl = string.Empty;
            if (!companyLogoUrlJson.IsNullOrEmpty())
                companyLogoUrl = JsonConvert.DeserializeObject<string>(companyLogoUrlJson);

            return new CompanySettingDto
            {
                CompanyName = companyName,
                CompanyLogoUrl = companyLogoUrl,
                CompanyAddress = companyAddress,
                TaxNo = taxNo
            };
        }

        private static List<ProducSetForOrderDto> GetDistinctProductSets(GetProductSetForOrderInput input, List<ProducSetForOrderDto> productSets)
        {
            return productSets.GroupBy(e => new { e.ProductSetName, e.ProductSetId, e.ScheduleDate })
                .Select(g =>
                {
                    var expiryProd = g.Where(e => e.BalanceCredit > 0).OrderBy(i => i.ExpiryDate)
                        .ThenBy(i => i.BalanceCredit).FirstOrDefault();

                    if (g.Key.ProductSetId == input.ProductSetId)
                    {
                        expiryProd = g.OrderBy(i => i.ExpiryDate).ThenBy(i => i.BalanceCredit)
                            .WhereIf(input.CustomerOfferId.HasValue,
                                e => e.CustomerProductOfferId == input.CustomerOfferId)
                            .FirstOrDefault();

                        if (expiryProd == null)
                            expiryProd = g.OrderBy(i => i.ExpiryDate).ThenBy(i => i.BalanceCredit)
                                .WhereIf(input.CustomerOfferId.HasValue,
                                    e => e.CustomerProductOfferId != input.CustomerOfferId)
                                .FirstOrDefault();
                    }

                    if (input.AddedOrders.Any())
                    {
                        var offers = g.Where(e => e.BalanceCredit > 0).OrderBy(i => i.ExpiryDate)
                            .ThenBy(i => i.BalanceCredit).ToList();

                        foreach (var addOrder in input.AddedOrders)
                        {
                            var firstOffer = offers.FirstOrDefault();
                            if (firstOffer != null && firstOffer.BalanceCredit <= addOrder.Quantity &&
                                firstOffer.CustomerProductOfferId ==
                                addOrder.CustomerProductOfferId)
                            {
                                if (offers.Count > 1)
                                    offers = offers.Skip(1).ToList();
                                expiryProd = offers
                                    .WhereIf(offers.Count > 1,
                                        i => i.CustomerProductOfferId != addOrder.CustomerProductOfferId)
                                    .FirstOrDefault();
                            }
                        }
                    }

                    ProducSetForOrderDto prod = null;
                    if (expiryProd != null)
                        prod = new ProducSetForOrderDto
                        {
                            ProductSetId = g.Key.ProductSetId,
                            ProductSetName = g.Key.ProductSetName,
                            ScheduleDate = g.Key.ScheduleDate,
                            BalanceCredit = expiryProd.BalanceCredit,
                            ExpiryDate = expiryProd.ExpiryDate,
                            CustomerProductOfferId = expiryProd.CustomerProductOfferId,
                            ProductSetNameExtra = expiryProd.ProductSetNameExtra
                        };
                    return prod;
                }).ToList();
        }

        private async Task<OrderHistoryForPrintDto> GetOrderDetailForPrint(long orderId)
        {
            var order = await _customerProductOfferOrderRepository.GetAll()
                .Include(c => c.CustomerProductOffer)
                .ThenInclude(c => c.Customer)
                .Include(c => c.CustomerAddress)
                .ThenInclude(e => e.City)
                .Include(c => c.CustomerProductOffer)
                .ThenInclude(c => c.ProductOffer)
                .ThenInclude(c => c.TiffinProductOfferType)
                .Include(c => c.ProductSet)
                .Include(c => c.TiffinMealTime)
                //.Where(e => e.CustomerProductOffer.Customer.UserId == AbpSession.UserId)
                .Where(e => e.Id == orderId)
                .FirstOrDefaultAsync();

            var result = new OrderHistoryForPrintDto
            {
                CustomerName = order.CustomerProductOffer?.Customer?.Name,
                PhoneNumber = order.CustomerProductOffer?.Customer?.PhoneNumber,
                DeliveryAddress =
                    $"{order.CustomerAddress?.Address1}, {order.CustomerAddress?.Address2}, {order.CustomerAddress?.Address3}, {order.CustomerAddress?.City?.Name}",
                DeliveryDate = order.OrderDate,
                MealTime = order.TiffinMealTime?.Name,
                ProductSetName = order.ProductSet?.Name,
                Quantity = order.Quantity,
                ProducOfferType = order.CustomerProductOffer?.ProductOffer?.TiffinProductOfferType?.Name,
                PostalCode = order.CustomerAddress?.PostcalCode
            };

            if (order.CustomerAddress?.Zone != null)
            {
                result.Zone = L(((ZoneEnum)order.CustomerAddress?.Zone).ToString());
            };

            return result;
        }

        private async Task<PaymentOutputDto> CreatePaymentRecords(CreateOrderPaymentInputDto input)
        {
            int? paymentId = null;

            var customer = await _customerRepository.FirstOrDefaultAsync(t => t.UserId == AbpSession.UserId);

            if (!string.IsNullOrWhiteSpace(input.PaymentOption))
            {
                var invoiceNo = await _tiffinInvoiceNumberGenerator.GetNewInvoiceNumber();

                var payment = new TiffinPayment
                {
                    InvoiceNo = invoiceNo,
                    PaymentDate = Clock.Now,
                    CustomerId = customer.Id,
                    CustomerName = customer.Name,
                    CustomerAddress = input.BillingInfo.GetFullAddress(),
                    PaidAmount = input.PaidAmount,
                    PaymentOption = input.PaymentOption
                };

                paymentId = await _tiffinPaymentRepository.InsertAndGetIdAsync(payment);
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            foreach (var offer in input.PurcharsedOrderList)
            {
                offer.PaymentId = paymentId;
            }

            var allFinalOrdersList = await CreateListOrder(input.PurcharsedOrderList);

            // Mask Promotion Code Claimed if have
            if (input.DiscountDetails?.Any() == true)
            {
                var claimedTime = Clock.Now;

                input.DiscountDetails = input.DiscountDetails.DistinctBy(x => x.Id).ToList();

                var promotionCodeIds = input.DiscountDetails.Select(x => x.Id).ToList();

                var promotionCodes = await _tiffinPromotionCode.GetAll()
                    .Where(x => promotionCodeIds.Contains(x.Id) && x.IsClaimed == false)
                    .ToListAsync();

                foreach (var promotionCode in promotionCodes)
                {
                    promotionCode.ClaimedAmount = input.DiscountDetails.First(x => x.Id == promotionCode.Id).ClaimAmount;

                    promotionCode.ClaimedTime = claimedTime;

                    promotionCode.IsClaimed = true;

                    promotionCode.CustomerId = customer.Id;

                    promotionCode.PaymentId = paymentId;

                    await _tiffinPromotionCode.UpdateAsync(promotionCode);
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            await _emailNotificationBackgroundJobService.SendEmailWhenCustomerOrderInBackground(allFinalOrdersList, await GetCompanyInfo());

            return new PaymentOutputDto
            {
                PurchasedSuccess = true,
                PaymentId = paymentId ?? -1
            };
        }

        private async Task<int?> GetMinQuantityInSetting()
        {
            if (AbpSession.TenantId != null)
            {
                var minMealCountJson = await SettingManager
                    .GetSettingValueForTenantAsync(AppSettings.MealSetting.MinMealCount, AbpSession.TenantId.Value);

                int? minMealCount = null;
                if (!minMealCountJson.IsNullOrEmpty() && minMealCountJson != "null")
                    minMealCount = JsonConvert.DeserializeObject<int>(minMealCountJson);

                return minMealCount;
            }

            return null;
        }
    }
}
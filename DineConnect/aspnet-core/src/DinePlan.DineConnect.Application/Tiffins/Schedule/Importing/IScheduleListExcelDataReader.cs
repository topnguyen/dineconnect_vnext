﻿using System.Collections.Generic;
using Abp.Application.Services;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;

namespace DinePlan.DineConnect.Tiffins.Schedule.Importing
{
    public interface IScheduleListExcelDataReader : IApplicationService
    {
        List<CreateScheduleDto> GetMenuItemsFromExcel(byte[] fileBytes);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Tiffins.MealTimes;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.Tiffins.Schedule.Importing
{
    public class ScheduleListExcelDataReader : NpoiExcelImporterBase<CreateScheduleDto>, IScheduleListExcelDataReader
    {
        private readonly IRepository<TiffinProductSet> _productSetRepo;
        private readonly IRepository<TiffinMealTime> _mealTimeRepo;
        private readonly IRepository<MenuItem> _menuItemRepo;
        private readonly ILogger _logger;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ScheduleListExcelDataReader(
            IRepository<TiffinProductSet> productSetRepo,
            IRepository<TiffinMealTime> mealTimeRepo,
            IRepository<MenuItem> menuItemRepo,
            ILogger logger,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _productSetRepo = productSetRepo;
            _mealTimeRepo = mealTimeRepo;
            _menuItemRepo = menuItemRepo;
            _logger = logger;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public List<CreateScheduleDto> GetMenuItemsFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private CreateScheduleDto ProcessExcelRow(ISheet worksheet, int row)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                if (IsRowEmpty(worksheet, row))
                {
                    return null;
                }

                var scheduleDto = new CreateScheduleDto();

                // Date
                scheduleDto.Date = worksheet.GetRow(row).GetCell(0).DateCellValue;

                // Meal Time
                var mealTime = worksheet.GetRow(row).GetCell(1).StringCellValue.Trim();
                var mealTimeId = _mealTimeRepo.GetAll().FirstOrDefault(x => x.Name == mealTime)?.Id;
                if (!mealTimeId.HasValue)
                {
                    throw new UserFriendlyException($"Row {row + 1} is empty Meal Time data");
                }

                scheduleDto.MealTimeId = mealTimeId.Value;

                // Product Set
                var productSet = worksheet.GetRow(row).GetCell(2).StringCellValue.Trim();
                var productSetId = _productSetRepo.GetAll().FirstOrDefault(x => x.Name == productSet)?.Id;
                if (!productSetId.HasValue)
                {
                    throw new UserFriendlyException($"Row {row + 1} is empty Product Set data");
                }

                scheduleDto.ProductSetId = productSetId.Value;

                // Products
                var productNames = worksheet.GetRow(row).GetCell(3).StringCellValue.Trim();

                var listProductNames = productNames.Split(";")
                    .Select(x => x.Trim())
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Distinct()
                    .ToList();

                if (!listProductNames.Any())
                {
                    throw new UserFriendlyException($"Row {row + 1} is empty Products data");
                }

                var products = _menuItemRepo.GetAll().Where(x => listProductNames.Contains(x.Name))
                    .ToList();

                if (!products.Any())
                {
                    throw new UserFriendlyException($"Row {row + 1} is empty Products data");
                }

                if(scheduleDto.Products == null)
                {
                    scheduleDto.Products = new List<ProductSetProductDto>();
                }

                foreach (var productName in listProductNames)
                {
                    var productEntity = products.FirstOrDefault(x => x.Name.Trim().Equals(productName, StringComparison.InvariantCultureIgnoreCase));

                    if (productEntity == null)
                    {
                        continue;
                    }

                    var product = new ProductSetProductDto
                    {
                        Id = productSetId,
                        ProductId = productEntity.Id,
                        ProductName = productEntity.Name,
                        MealTimeId = mealTimeId.Value
                    };

                    scheduleDto.Products.Add(product);
                }

                return scheduleDto;
            }
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var rowData = worksheet.GetRow(row);

            if (rowData == null)
            {
                return true;
            }

            var isEmptyDate = GetDateTime(rowData.GetCell(0)) == null;
            var isEmptyMealTime = string.IsNullOrWhiteSpace(rowData.GetCell(1)?.StringCellValue);
            var isEmptyProductSet = string.IsNullOrWhiteSpace(rowData.GetCell(2)?.StringCellValue);
            var isEmptyProducts = string.IsNullOrWhiteSpace(rowData.GetCell(3)?.StringCellValue);

            if (isEmptyDate && isEmptyMealTime && isEmptyProductSet && isEmptyProducts)
            {
                return true;
            }

            if(isEmptyDate)
            {
                throw new UserFriendlyException($"Row {row + 1} is empty Date data");
            }

            if (isEmptyMealTime)
            {
                throw new UserFriendlyException($"Row {row + 1} is empty Meal Time data");
            }

            if (isEmptyProductSet)
            {
                throw new UserFriendlyException($"Row {row + 1} is empty Product Set data");
            }
            
            if (isEmptyProducts)
            {
                throw new UserFriendlyException($"Row {row + 1} is empty Products data");
            }

            return false;
        }

        private DateTime? GetDateTime(ICell cell)
        {
            if (cell == null)
            {
                return null;
            }

            try
            {
                if (cell.CellType == CellType.Numeric)
                {
                    if (DateUtil.IsCellDateFormatted(cell))
                    {
                        try
                        {
                            return cell.DateCellValue;
                        }
                        catch (Exception)
                        {
                            return DateUtil.GetJavaDate(cell.NumericCellValue);
                        }
                    }
                    
                    return DateUtil.GetJavaDate(cell.NumericCellValue);
                }

                var dateTimeStr = cell.StringCellValue;

                if (string.IsNullOrWhiteSpace(dateTimeStr))
                {
                    return null;
                }

                DateTime.TryParse(dateTimeStr, out var dateTime);

                return dateTime;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException($"Row {cell.RowIndex + 1}, Col {cell.ColumnIndex + 1} contain invalid data is {cell.NumericCellValue}, {ex.Message}, {ex.StackTrace}");
            }
        }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using DinePlan.DineConnect.Tiffins.Schedule.Exporting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Timing;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Schedule.Importing;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Tiffins.Schedule
{
    public class ScheduleAppService : DineConnectAppServiceBase, IScheduleAppService
    {
        private readonly IRepository<TiffinSchedule> _tiffinScheduleRepository;
        private readonly IRepository<TiffinScheduleDetail> _tiffinScheduleDetailRepository;
        private readonly IRepository<TiffinScheduleDetailProduct> _tiffinScheduleDetailProductRepository;
        private readonly ScheduleExcelExporter _scheduleExcelExporter;
        private readonly IRepository<TiffinProductSet> _tiffinProductSetRepository;
        private readonly IRepository<TiffinProductSetProduct> _tiffinProductSetProductRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IScheduleListExcelDataReader _scheduleListExcelDataReader;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly ILogger _logger;
        private readonly IBackgroundJobManager _backgroundJobManager;

        public ScheduleAppService(IRepository<TiffinSchedule> tiffinScheduleRepository,
            IRepository<TiffinScheduleDetailProduct> tiffinScheduleDetailProductRepository,
            IRepository<TiffinScheduleDetail> tiffinScheduleDetailRepository,
            ScheduleExcelExporter scheduleExcelExporter,
            IRepository<TiffinProductSet> tiffinProductSetRepository,
            IRepository<TiffinProductSetProduct> tiffinProductSetProductRepository,
            ITempFileCacheManager tempFileCacheManager,
            IScheduleListExcelDataReader scheduleListExcelDataReader,
            IRepository<MenuItem> menuItemRepository,
            ILogger logger,
            IBackgroundJobManager backgroundJobManager
        )
        {
            _tiffinScheduleRepository = tiffinScheduleRepository;
            _tiffinScheduleDetailProductRepository = tiffinScheduleDetailProductRepository;
            _tiffinScheduleDetailRepository = tiffinScheduleDetailRepository;
            _scheduleExcelExporter = scheduleExcelExporter;
            _tiffinProductSetRepository = tiffinProductSetRepository;
            _tiffinProductSetProductRepository = tiffinProductSetProductRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _scheduleListExcelDataReader = scheduleListExcelDataReader;
            _menuItemRepository = menuItemRepository;
            _logger = logger;
            _backgroundJobManager = backgroundJobManager;
        }

        public async Task<bool> CreateListSchedule(List<CreateScheduleDto> lst)
        {
            if (lst == null)
            {
                return false;
            }

            foreach (var item in lst)
            {
                await Create(item);
            }

            //await UnitOfWorkManager.Current.SaveChangesAsync();

            return true;
        }

        public async Task<int> Create(CreateScheduleDto input)
        {
            var scheduleId = 0;
            if (input.TenantId == null)
            {
                input.TenantId = AbpSession.TenantId;
            }

            var schedule = await CheckExitsSchedule(input.Date, input.MealTimeId) ?? new TiffinSchedule
            {
                Date = input.Date,
                MealTimeId = input.MealTimeId,
                ScheduleDetails = new List<TiffinScheduleDetail>(),
                TenantId = input.TenantId ?? 1
            };

            var products = input.Products.Select(p => new TiffinScheduleDetailProduct
            {
                TenantId = AbpSession.TenantId ?? 1,
                ProductId = p.ProductId
            }).ToList();

            var tiffinScheduleDetail = new TiffinScheduleDetail
            {
                ProductSetId = input.ProductSetId,
                TenantId = input.TenantId ?? 1,
                Products = products,
                CreationTime = Clock.Now
            };

            schedule.ScheduleDetails.Add(tiffinScheduleDetail);

            scheduleId = await _tiffinScheduleRepository.InsertOrUpdateAndGetIdAsync(schedule);

            await UnitOfWorkManager.Current.SaveChangesAsync();

            return scheduleId;
        }

        public async Task<PagedResultDto<CreateScheduleDto>> GetAll(GetScheduleInputDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var schedules = _tiffinScheduleRepository.GetAll().AsNoTracking()
                    .WhereIf(input.StartDate.HasValue, x => x.Date >= input.StartDate)
                    .WhereIf(input.EndDate.HasValue, x => x.Date <= input.EndDate);

                var scheduleDetails = _tiffinScheduleDetailRepository.GetAll()
                    .Include(t => t.Products)
                    .ThenInclude(p => p.Product)
                    .AsNoTracking()
                    .WhereIf(input.ProductSetId.HasValue, x => x.ProductSetId == input.ProductSetId)
                    .WhereIf(input.ProductId.HasValue, x => x.Products.Any(a => a.ProductId == input.ProductId))
                    .WhereIf(input.StartDate.HasValue, x => x.Schedule.Date >= input.StartDate)
                    .WhereIf(input.EndDate.HasValue, x => x.Schedule.Date <= input.EndDate)
                    .WhereIf(input.Status.HasValue, x => x.Status == input.Status);

                var productSets = _tiffinProductSetRepository.GetAll().AsNoTracking()
                    .WhereIf(input.ProductSetId.HasValue, x => x.Id == input.ProductSetId);

                var linq = from sd in scheduleDetails
                           join s in schedules.Include(e => e.MealTime) on sd.ScheduleId equals s.Id into sdJoined
                           from sdj in sdJoined.DefaultIfEmpty()
                           join ps in productSets on sd.ProductSetId equals ps.Id into psdJoined
                           from psd in psdJoined.DefaultIfEmpty()
                           select new
                           {
                               ScheduleDetail = sd,
                               Schedule = sdj,
                               ProductSet = psd
                           };

                var totalCount = await linq.CountAsync();

                var list = await linq
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var output = list.Select(scheduleDetail =>
                {
                    var dto = new CreateScheduleDto
                    {
                        ProductSetName = scheduleDetail.ProductSet?.Name,
                        Images = scheduleDetail.ProductSet?.Images
                    };

                    if (scheduleDetail.Schedule != null)
                    {
                        dto.Date = scheduleDetail.Schedule.Date;
                        dto.DayOfWeek = scheduleDetail.Schedule.Date.DayOfWeek.ToString();
                        dto.MealTimeName = scheduleDetail.Schedule.MealTime?.Name;
                        dto.MealTimeId = scheduleDetail.Schedule.MealTimeId;
                    }

                    if (scheduleDetail.ScheduleDetail != null)
                    {
                        dto.Id = scheduleDetail.ScheduleDetail.Id;
                        dto.ProductSetId = scheduleDetail.ScheduleDetail.ProductSetId;
                        dto.TenantId = scheduleDetail.ScheduleDetail.TenantId;
                        dto.Status = scheduleDetail.ScheduleDetail.Status;
                        dto.Products = scheduleDetail.ScheduleDetail.Products?.Select(p =>
                        {
                            //var menu = _menuItemRepository.FirstOrDefault(p.ProductId);
                            var menu = p.Product;
                            return new ProductSetProductDto
                            {
                                Id = p.Id,
                                ScheduleId = scheduleDetail.ScheduleDetail.Id,
                                ProductId = p.ProductId,
                                ProductName = menu?.Name,
                                ProductImage = menu?.Images,
                                ProductDescription = menu?.ItemDescription
                            };
                        }).ToList();
                    }

                    return dto;
                }).ToList();

                return new PagedResultDto<CreateScheduleDto>(totalCount, output);
            }
        }

        public async Task<CreateScheduleDto> GetDataForEdit(int id)
        {
            var scheduleDetail = await _tiffinScheduleDetailRepository
                .GetAll()
                .Include(x => x.Schedule)
                .Include(x => x.ProductSet)
                .Include(x => x.Products).ThenInclude(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == id);

            return new CreateScheduleDto()
            {
                Id = id,
                Date = scheduleDetail.Schedule.Date,
                ProductSetId = scheduleDetail.ProductSetId,
                TenantId = scheduleDetail.TenantId,
                ProductSetName = scheduleDetail.ProductSet.Name,
                Products = ObjectMapper.Map<List<ProductSetProductDto>>(scheduleDetail.Products),
                DayOfWeek = scheduleDetail.Schedule.Date.DayOfWeek.ToString(),
                Status = scheduleDetail.Status,
                Images = scheduleDetail.ProductSet.Images
            };
        }

        public async Task Delete(int id)
        {
            var scheduleDetail = await _tiffinScheduleDetailRepository.FirstOrDefaultAsync(x => x.Id == id);

            var scheduleDetailProduct = await _tiffinScheduleDetailProductRepository.GetAll()
                .Where(x => x.ScheduleDetailId == scheduleDetail.Id).ToListAsync();

            foreach (var item in scheduleDetailProduct)
            {
                await _tiffinScheduleDetailProductRepository.DeleteAsync(item.Id);
            }

            await _tiffinScheduleDetailRepository.DeleteAsync(scheduleDetail.Id);
        }

        public async Task BulkDelete(List<int> ids)
        {
            foreach (var id in ids)
            {
                await Delete(id);
            }
        }

        private async Task<TiffinSchedule> CheckExitsSchedule(DateTime input, int mealTime)
        {
            var schedule = await _tiffinScheduleRepository.GetAll().Include(s => s.ScheduleDetails)
                .Where(x => x.Date == input && x.MealTimeId == mealTime).FirstOrDefaultAsync();
            return schedule;
        }

        public async Task UpdateList(List<CreateScheduleDto> lst)
        {
            foreach (var item in lst)
            {
                await Update(item);
            }
        }

        public async Task Update(CreateScheduleDto input)
        {
            //
            var scheduleDetail = await _tiffinScheduleDetailRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            var scheduleDetailProduct = await _tiffinScheduleDetailProductRepository.GetAll()
                .Where(x => x.ScheduleDetailId == scheduleDetail.Id).ToListAsync();
            scheduleDetail.ProductSetId = input.ProductSetId;
            foreach (var item in scheduleDetailProduct)
            {
                var index = input.Products.Any(x => x.ProductId == item.ProductId);
                if (index != true)
                {
                    await _tiffinScheduleDetailProductRepository.DeleteAsync(item.Id);
                }
            }

            foreach (var item in input.Products)
            {
                var index = scheduleDetailProduct.Where(x =>
                    x.ProductId == item.ProductId && x.ScheduleDetailId == input.Id);
                if (!index.Any())
                {
                    var newSdp = new TiffinScheduleDetailProduct
                    {
                        TenantId = AbpSession.TenantId ?? 1,
                        ProductId = item.ProductId,
                        ScheduleDetailId = input.Id ?? 1
                    };
                    await _tiffinScheduleDetailProductRepository.InsertAsync(newSdp);
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<List<GetScheduleForMenu>> GetScheduleForMenu(GetScheduleInputDto input)
        {
            if (input.StartDate != null && input.EndDate != null)
            {
                List<CreateScheduleDto> listSchedule;
                using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
                {
                    listSchedule = await _tiffinScheduleDetailRepository
                        .GetAll()
                        .Include(t => t.Schedule)
                        .Include(t => t.ProductSet)
                        .Include(t => t.Products)
                        .ThenInclude(x => x.Product)
                        .WhereIf(input.StartDate.HasValue, x => x.Schedule.Date >= input.StartDate.Value.Date)
                        .WhereIf(input.EndDate.HasValue, x => x.Schedule.Date <= input.EndDate.Value.Date)
                        .WhereIf(input.Status.HasValue, x => x.Status == input.Status)
                        .Select(scheduleDetail => new CreateScheduleDto
                        {
                            Id = scheduleDetail.Id,
                            Date = scheduleDetail.Schedule.Date,
                            ProductSetId = scheduleDetail.ProductSetId,
                            TenantId = scheduleDetail.TenantId,
                            ProductSetName = scheduleDetail.ProductSet.Name,
                            Products = ObjectMapper.Map<List<ProductSetProductDto>>(scheduleDetail.Products),
                            Ordinal = scheduleDetail.ProductSet.Ordinal,
                            Color = scheduleDetail.ProductSet.Color,
                            Images = scheduleDetail.ProductSet.Images,
                            MealTimeId = scheduleDetail.Schedule.MealTimeId
                        }).ToListAsync();
                }

                foreach (var schedule in listSchedule)
                {
                    foreach (var scheduleProduct in schedule.Products)
                    {
                        scheduleProduct.MealTimeId = schedule.MealTimeId;
                    }
                }

                var output = listSchedule.GroupBy(x => new { x.ProductSetName, x.Color, x.Ordinal, x.ProductSetId })
                    .Select(
                        x => new GetScheduleForMenu
                        {
                            ProductSetName = x.Key.ProductSetName,
                            ProductSetId = x.Key.ProductSetId,
                            Color = x.Key.Color,
                            Ordinal = x.Key.Ordinal,
                            ListSchedule = x.ToList()
                        }
                    ).ToList();

                var listDay = new List<string>
                    {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

                foreach (var item1 in output)
                {
                    var days = new List<CreateScheduleDto>();

                    foreach (var day in listDay)
                    {
                        var dayData = item1.ListSchedule.Where(d => d.DateDayOfWeek == day).ToList();

                        if (dayData != null)
                        {
                            var listProduct = new List<ProductSetProductDto>();

                            foreach (var item in dayData)
                            {
                                foreach (var i in item.Products)
                                {
                                    // if (listProduct.Count > 0)
                                    // {
                                    //     var index = listProduct.FirstOrDefault(x => x.ProductId == i.ProductId);
                                    //     if (index == null)
                                    //         listProduct.Add(i);
                                    // }
                                    // else
                                    // {
                                    //     listProduct.Add(i);
                                    // }

                                    listProduct.Add(i);
                                }
                            }

                            days.Add(new CreateScheduleDto
                            {
                                DayOfWeek = day,
                                Products = listProduct
                            });
                        }
                        else
                        {
                            days.Add(new CreateScheduleDto
                            {
                                DayOfWeek = day
                            });
                        }
                    }

                    item1.ListSchedule = days;
                }

                return output
                    .OrderBy(e => e.Ordinal)
                    .ThenBy(a => a.ProductSetName)
                    .ThenBy(a => a.ListSchedule.OrderBy(f => f.Products.OrderBy(g => g.ProductName))).ToList();
            }

            return null;
        }

        public async Task<FileDto> GetScheduleToExcel(GetScheduleInputDto input)
        {
            var schedules = await GetAll(input);

            return await _scheduleExcelExporter.ExportToFile(schedules.Items.ToList());
        }

        public async Task<FileDto> GetImportScheduleTemplateToExcel()
        {
            return await _scheduleExcelExporter.ExportToFileForImportTemplate();
        }

        public async Task ImportScheduleToDatabase(string fileToken)
        {
            if (!string.IsNullOrWhiteSpace(fileToken))
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                        List<string> headers = new List<string>
                        {
                            "Date",
                            "Meal Times",
                            "Product Set",
                            "Products"
                        };

                        var columns = worksheet.Dimension.End.Column;

                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (int i = 1; i < columns + 1; i++)
                            {
                                sheetHeaders.Add(worksheet.GetValue(1, i).ToString());
                            }

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }

                // _logger.Error("Menu Schedule Import DEBUG as below ----------");

                var schedules = _scheduleListExcelDataReader.GetMenuItemsFromExcel(fileBytes);

                // _logger.Error(schedules.ToJsonString());

                foreach (var createScheduleDto in schedules)
                {
                    createScheduleDto.TenantId = AbpSession.TenantId;
                }

                await CreateListSchedule(schedules);

                //await _backgroundJobManager.EnqueueAsync<ScheduleJob, List<CreateScheduleDto>>(schedules);
            }
        }

        public async Task UpdateStatus(UpdateScheduleStatusDto input)
        {
            if (input?.Ids?.Any() != true)
            {
                return;
            }

            input.Ids = input.Ids.Distinct().ToList();

            var scheduleDetails =
                await _tiffinScheduleDetailRepository
                    .GetAll()
                    .Where(x => input.Ids.Contains(x.Id))
                    .ToListAsync();

            foreach (var scheduleDetail in scheduleDetails)
            {
                scheduleDetail.Status = input.NewStatus;

                await _tiffinScheduleDetailRepository.UpdateAsync(scheduleDetail);
            }
        }

        private string GetListNameProduct(List<TiffinScheduleDetailProduct> products)
        {
            var list = "";
            foreach (var item in products)
            {
                list = list + item.Product.Name + ";";
            }

            return list;
        }

        public async Task<PagedResultDto<ProductSetProductDto>> GetMenuItemForMulti(GetMenuItemForProductSetInputDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var list = new List<ProductSetProductDto>();
                if (input.ProductSetId == null)
                {
                    list = _menuItemRepository.GetAll()
                        .WhereIf(!input.Filter.IsNullOrEmpty(),
                            t => EF.Functions.Like(t.Name, input.Filter.ToILikeString()))
                        .Select(t => new ProductSetProductDto
                        {
                            Id = t.Id,
                            ProductId = t.Id,
                            ProductName = t.Name
                        }).ToList();
                }
                else
                {
                    var listMenuItemOfProductSet = await _tiffinProductSetProductRepository.GetAll()
                        .WhereIf(true, x => x.ProductSetId == input.ProductSetId).ToListAsync();
                    var listMenuItem = await _menuItemRepository.GetAll().ToListAsync();
                    list = (from a in listMenuItemOfProductSet
                        join b in listMenuItem
                            on a.ProductId equals b.Id
                        select new ProductSetProductDto
                        {
                            Id = b.Id,
                            ProductId = b.Id,
                            ProductName = b.Name
                        }).WhereIf(!input.Filter.IsNullOrEmpty(),
                        t => EF.Functions.Like(t.ProductName, input.Filter.ToILikeString())).ToList();
                }

                var totalCount = list.Count();
                var result = list.AsQueryable().PageBy(input)
                    .ToList();

                return new PagedResultDto<ProductSetProductDto>(totalCount, result);
            }
        }
    }
}
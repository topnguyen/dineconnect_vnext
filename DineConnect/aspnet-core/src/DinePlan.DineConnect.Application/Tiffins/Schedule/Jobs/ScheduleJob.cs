﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;

namespace DinePlan.DineConnect.Tiffins.Schedule.Jobs
{
    public class ScheduleJob : BackgroundJob<List<CreateScheduleDto>>, ITransientDependency
    {
        private readonly IScheduleAppService _scheduleAppService;

        public ScheduleJob(IScheduleAppService scheduleAppService)
        {
            _scheduleAppService = scheduleAppService;
        }

        public override void Execute(List<CreateScheduleDto> args)
        {
            try
            {
                var task = _scheduleAppService.CreateListSchedule(args);

                task.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}

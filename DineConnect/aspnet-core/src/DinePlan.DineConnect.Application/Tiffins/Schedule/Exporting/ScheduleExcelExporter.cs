﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Tiffins.Schedule.Exporting
{
    public class ScheduleExcelExporter : NpoiExcelExporterBase, IScheduleExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public ScheduleExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<CreateScheduleDto> list)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "Schedules.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Schedules"));

                    AddHeader(
                        sheet,
                        L("Date"),
                        L("MealTime"),
                        L("ProductSet"),
                        L("Products")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _timeZoneConverter.Convert(_.Date, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateSetting),
                        _ => _.MealTimeName,
                        _ => _.ProductSetName,
                        _ => string.Join("; ", _.Products.Select(x => x.ProductName))
                    );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public async Task<FileDto> ExportToFileForImportTemplate()
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            
            return CreateExcelPackage(
                @L("Schedules") + "-" + Clock.Now.ToString(fileDateSetting) + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Schedules"));

                    AddHeader(
                        sheet,
                        L("Date"),
                        L("MealTime"),
                        L("ProductSet"),
                        L("Products")
                    );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Schedule.Exporting
{
  public interface IScheduleExcelExporter
    {
        Task<FileDto> ExportToFile(List<CreateScheduleDto> list);
        
        Task<FileDto> ExportToFileForImportTemplate();
    }
}

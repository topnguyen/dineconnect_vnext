﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;
using DinePlan.DineConnect.Tiffins.MemberPortal;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Payment;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.BaseCore.Jobs.TiffinEmailJob;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;

namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public class EmailNotificationBackgroundJobService : DineConnectAppServiceBase, IEmailNotificationBackgroundJobService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<BaseCore.Customer.Customer> _customerRepository;
        private readonly ITiffinPaymentExporter _tfPaymentExporter;
        private readonly IRepository<TiffinCustomerProductOffer> _customerProductOfferRepository;
        private readonly IRepository<CustomerAddressDetail> _tfCustomerAddressRepository;

        public EmailNotificationBackgroundJobService(IBackgroundJobManager bgm,
            IRepository<BaseCore.Customer.Customer> customerRepository,
            ITiffinPaymentExporter tfPaymentExporter,
            IRepository<TiffinCustomerProductOffer> customerProductOfferRepository,
            IRepository<CustomerAddressDetail> tfCustomerAddressRepository)
        {
            _bgm = bgm;
            _customerRepository = customerRepository;
            _tfPaymentExporter = tfPaymentExporter;
            _customerProductOfferRepository = customerProductOfferRepository;
            _tfCustomerAddressRepository = tfCustomerAddressRepository;
        }

        public async Task SendEmailWhenCustomerBuyMealPlanInBackground(long paymentId,TiffinPaymentDetailDto paymentDetail,CompanySettingDto companyInfo)
        {
            if (AbpSession.TenantId != null)
            {
                var fileAttach = _tfPaymentExporter.GenerateInvoice(paymentDetail, companyInfo);
                var customer = await GetCustomerAddressSendEmail(companyInfo);
                await _bgm.EnqueueAsync<TiffinEmailJob, ConnectEmailInputArgs>(new ConnectEmailInputArgs
                {
                    EmailName = EmailName.BUYMEAL,
                    PaymentId = paymentId,
                    TenantId = AbpSession.TenantId.Value,
                    PaymentDetail = paymentDetail,
                    FileAttach = fileAttach,
                    CustomerAddress = customer
                });
            }
        }

        public async Task SendEmailWhenCustomerOrderInBackground(List<CreateOrderInput> orderInputs, CompanySettingDto companyInfo)
        {
            if (AbpSession.TenantId != null)
            {
                var lstOrderSendEmails = await GetListOrderSendEmail(orderInputs);
                var customer = await GetCustomerAddressSendEmail(companyInfo);
                await _bgm.EnqueueAsync<TiffinEmailJob, ConnectEmailInputArgs>(new ConnectEmailInputArgs
                {
                    EmailName = EmailName.BUYORDER,
                    TenantId = AbpSession.TenantId.Value,
                    OrderSendEmails = lstOrderSendEmails,
                    CustomerAddress = customer
                });
            }
        }

        public async Task SendEmailWhenMinimumQuantityInBackground(OrderMinQuantity orderMinQuantity, CompanySettingDto companyInfo)
        {
            if (AbpSession.TenantId.HasValue && AbpSession.UserId.HasValue)
            {
                var customer = await GetCustomerAddressSendEmail(companyInfo);
                await _bgm.EnqueueAsync<TiffinEmailJob, ConnectEmailInputArgs>(new ConnectEmailInputArgs
                {
                    EmailName = EmailName.MEALCOUNT,
                    TenantId = AbpSession.TenantId.Value,
                    CustomerAddress = customer,
                    OrderMinQuantity = orderMinQuantity
                });
            }
        }

        protected async Task<CustomerAddressSendEmail> GetCustomerAddressSendEmail(CompanySettingDto companyInfo)
        {
            var customer = await _customerRepository
                .GetAll()
                .Include(c => c.CustomerAddressDetails)
                .ThenInclude(t => t.City)
                .Include(c => c.Country)
                .FirstOrDefaultAsync(c => c.UserId == AbpSession.UserId);

            var result = new CustomerAddressSendEmail
            {
                CustomerName = customer.Name,   
                CustomerEmail = customer.EmailAddress,
                Address1 = customer.Address1,
                Address2 = customer.Address2,
                Address3 = customer.Address3,
                MobileNumber = customer.PhoneNumber,
                PostcalCode = customer.PostcalCode,
                Zone = customer.Zone.HasValue ? ((ZoneEnum)customer.Zone).ToString() : string.Empty,
                CountryName = customer.Country?.Name
            };

            if (companyInfo != null)
            {
                result.CompanyAddress = companyInfo.CompanyAddress;
            }

            var customerDefault = customer.CustomerAddressDetails.FirstOrDefault(t => t.IsDefault);
            if (customerDefault != null)
            {
                if (customerDefault.City != null)
                {
                    result.CityName = customerDefault.City.Name;
                }
                else
                {
                    result.CityName = string.Empty;
                }
            }

            return result;
        }

        protected async Task<List<OrderSendEmail>> GetListOrderSendEmail(List<CreateOrderInput> orderInputs)
        {
            var lstCustomerProductOffer = _customerProductOfferRepository.GetAll()
                .Include(p => p.ProductOffer)
                .ThenInclude(po => po.TiffinProductOfferType);

            var result = new List<OrderSendEmail>();
            foreach (var item in orderInputs)
            {
                var customerAddress = await _tfCustomerAddressRepository.FirstOrDefaultAsync(c => c.Id == item.CustomerAddressId);
                var customerProductOffer =  lstCustomerProductOffer.Where(c => c.Id == item.CustomerProductOfferId).ToList().FirstOrDefault();

                result.Add(new OrderSendEmail
                {
                    DeliveryAddress = customerAddress != null ? customerAddress.FullAddress : string.Empty,
                    OrderDate = item.OrderDate,
                    ProductSetName = item.ProductSetName,
                    Plan = customerProductOffer != null ? customerProductOffer.ProductOffer.TiffinProductOfferType.Name : string.Empty,
                    MealTime = item.MealTimeString,
                    Quantity = item.Quantity,
                    Addon = string.Join(", ", item.AddOn)
                });
            }

            return result;
        }
    }
}
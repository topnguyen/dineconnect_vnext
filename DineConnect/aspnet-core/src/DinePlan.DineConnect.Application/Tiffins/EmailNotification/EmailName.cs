﻿namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public class EmailName
    {
        public const string BUYMEAL = "BuyMeal";
        public const string BUYORDER = "BuyOrder";
        public const string MEALCOUNT = "MealCount";
    }
}

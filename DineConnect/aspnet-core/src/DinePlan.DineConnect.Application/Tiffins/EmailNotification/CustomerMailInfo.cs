﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public class CustomerMailInfo
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
    }
}

﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Tiffins.MemberPortal;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Payment;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Linq;
using Abp.Net.Mail;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public class EmailNotificationAppService : DineConnectAppServiceBase, IEmailNotificationAppService
    {
        private readonly ITenantEmailSendAppService _emailAppService;
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public EmailNotificationAppService(ITenantEmailSendAppService emailAppService,
            ITempFileCacheManager tempFileCacheManager)
        {
            _emailAppService = emailAppService;
            _tempFileCacheManager = tempFileCacheManager;
        }


        public async Task SendEmailWhenCustomerBuyMealPlan(TiffinPaymentDetailDto paymentDetail, int tenantId, FileDto fileAttach, CustomerAddressSendEmail customerAddress)
        {
            var sb = new StringBuilder();
            sb.Append(@"<h3>Invoice: #{{Invoice_Data}}</h3>");
            sb.Append(@"<p>Bill To: {{AddressBill_Data}}</p>");
            sb.Append(@"<p>Ship To: {{AddressShip_Data}}</p>");
            sb.Replace("{{Invoice_Data}}", paymentDetail.InvoiceNo);
            sb.Replace("{{AddressBill_Data}}", paymentDetail.CustomerAddress);
            sb.Replace("{{AddressShip_Data}}", paymentDetail.CustomerAddress);

            sb.Append(@"<table style=""{{Style_Table}}"">");
            sb.Append(@"<tr>
                            <th style=""{{Style_Title}}"">Product Offers</th>
                            <th style=""{{Style_Title_Decimal}}"">Price</th>
                            <th style=""{{Style_Title_Decimal}}"">Quantity</th>
                            <th style=""{{Style_Title_Decimal}}"">Total</th>
                        </tr>");
            foreach (var product in paymentDetail.Products)
            {
                sb.Append(@"<tr>
                                <td style=""{{Style_Contend}}"">{{ProductName_Date}}</td>
                                <td style=""{{Style_Contend_Decimal}}"">{{Price_Data}}</td>
                                <td style=""{{Style_Contend_Decimal}}"">{{Quantity_Data}}</td>
                                <td style=""{{Style_Contend_Decimal}}"">{{Total_Data}}</td>
                            </tr>");
                sb.Replace("{{ProductName_Date}}", product.ProductOfferName);
                sb.Replace("{{Price_Data}}", product.Price.HasValue ? DineConnectConsts.CurrencySign + product.Price.Value.ToString("F") : string.Empty);
                sb.Replace("{{Quantity_Data}}", product.Quantity.ToString());
                sb.Replace("{{Total_Data}}", product.PaidAmount.HasValue ? DineConnectConsts.CurrencySign + product.PaidAmount.Value.ToString("F") : string.Empty);
            }
            sb.Append(@"<tr>
                                <td colspan=""3"" style=""{{Style_Footer_Title_Decimal}}"">Card Charges</td>
                                <td style=""{{Style_Footer_Decimal}}"">{{Card_Charge_Data}}</td>
                       </tr>");
            sb.Append(@"<tr>
                                <td colspan=""3"" style=""{{Style_Footer_Title_Decimal}}"">Total</td>
                                <td style=""{{Style_Footer_Decimal}}"">{{Paid_Amount_Data}}</td>
                       </tr>");
            sb.Replace("{{Card_Charge_Data}}", paymentDetail.CardCharge.HasValue ? DineConnectConsts.CurrencySign + paymentDetail.CardCharge.Value.ToString("F") : string.Empty);
            sb.Replace("{{Paid_Amount_Data}}", paymentDetail.PaidAmount.HasValue ? DineConnectConsts.CurrencySign + paymentDetail.PaidAmount.Value.ToString("F") : string.Empty);
            sb.Append(@"</table>");

            // Add style
            sb.Replace("{{Style_Table}}", "font-family:arial, sans-serif;border-collapse:collapse;width:100%");
            sb.Replace("{{Style_Title}}", "border:1px solid #dddddd;text-align:left;padding:8px;");
            sb.Replace("{{Style_Contend}}", "border:1px solid #dddddd;text-align:left;padding:8px;");
            sb.Replace("{{Style_Title_Decimal}}", "border:1px solid #dddddd;text-align:right;padding:8px;");
            sb.Replace("{{Style_Contend_Decimal}}", "border:1px solid #dddddd;text-align:right;padding:8px;");

            sb.Replace("{{Style_Footer_Title_Decimal}}", "font-weight: bold;text-align:right;padding:8px;");
            sb.Replace("{{Style_Footer_Decimal}}", "font-weight: bold;text-align:right;padding:8px;");

            var fileBytes = _tempFileCacheManager.GetFile(fileAttach.FileToken);
            var fileName = fileAttach.FileName;
            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = paymentDetail.EmailAddress,
                Module = "BuyMealPlan",
                FileAttach = new FileAttach { FileName = fileName, FileBytes = fileBytes, FileMime = "application/pdf"},
                Variables = new
                {
                    Company_Address = customerAddress.CompanyAddress,
                    First_name = customerAddress.CustomerName,
                    Last_name = customerAddress.CustomerName,
                    Email_address = customerAddress.CustomerEmail,
                    Phone_number = customerAddress.MobileNumber,
                    Address1 = customerAddress.Address1,
                    Address2 = customerAddress.Address2,
                    Address3 = customerAddress.Address3,
                    City = customerAddress.CityName,
                    Country = customerAddress.CountryName,
                    Postcal_code = customerAddress.PostcalCode,
                    Zone = customerAddress.Zone,
                    PaidAmount = paymentDetail.PaidAmount.ToString(),
                    Payment_Detail = sb.ToString()
                }
            };
            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);

        }

        public async Task SendEmailWhenCustomerOrder(List<OrderSendEmail> orderSendEmails, int tenantId, CustomerAddressSendEmail customerAddress)
        {
            var sb = new StringBuilder();

            sb.Append(@"<table style=""{{Style_Table}}"">");
            sb.Append(@"<tr>
                            <th style=""{{Style_Title}}"">Order Date</th>
                            <th style=""{{Style_Title}}"">Meal Choice</th>
                            <th style=""{{Style_Title_Decimal}}"">Quantity</th>
                            <th style=""{{Style_Title}}"">Pack Size</th>
                            <th style=""{{Style_Title}}"">Meal Time</th>
                            <th style=""{{Style_Title}}"">Special Request</th>
                        </tr>");
            foreach (var order in orderSendEmails)
            {
                sb.Append(@"<tr>
                                <td style=""{{Style_Contend}}"">{{OrderDate_Data}}</td>
                                <td style=""{{Style_Contend}}"">{{MealChoice_Data}}</td>
                                <td style=""{{Style_Contend_Decimal}}"">{{Quantity_Data}}</td>
                                <td style=""{{Style_Contend}}"">{{Plan_Data}}</td>
                                <td style=""{{Style_Contend}}"">{{MealTime_Data}}</td>
                                <td style=""{{Style_Contend}}"">{{Addon_Data}}</td>
                            </tr>");
                sb.Replace("{{OrderDate_Data}}", order.OrderDate.ToString("dd/MM/yyyy"));
                sb.Replace("{{MealChoice_Data}}", order.ProductSetName);
                sb.Replace("{{Quantity_Data}}", order.Quantity.ToString());
                sb.Replace("{{Plan_Data}}", order.Plan);
                sb.Replace("{{MealTime_Data}}", order.MealTime);

                var lstAddonObject = JsonConvert.DeserializeObject<List<AddonListDto>>(order.Addon);
                var lstAddonString = new List<string>();
                if (lstAddonObject != null)
                {
                    foreach (var item in lstAddonObject)
                    {
                        lstAddonString.Add(item.Label + "x" + item.Quantity.ToString());
                    }
                }

                sb.Replace("{{Addon_Data}}", string.Join(", ", lstAddonString));
            }
            sb.Append(@"</table>");

            // Add style
            sb.Replace("{{Style_Table}}", "font-family:arial, sans-serif;border-collapse:collapse;width:100%");
            sb.Replace("{{Style_Title}}", "border:1px solid #dddddd;text-align:left;padding:8px;");
            sb.Replace("{{Style_Contend}}", "border:1px solid #dddddd;text-align:left;padding:8px;");
            sb.Replace("{{Style_Title_Decimal}}", "border:1px solid #dddddd;text-align:right;padding:8px;");
            sb.Replace("{{Style_Contend_Decimal}}", "border:1px solid #dddddd;text-align:right;padding:8px;");

            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = customerAddress.CustomerEmail,
                Module = "Order",
                Variables = new
                {
                    Company_Address = customerAddress.CompanyAddress,
                    First_name = customerAddress.CustomerName,
                    Last_name = customerAddress.CustomerName,
                    Email_address = customerAddress.CustomerEmail,
                    Phone_number = customerAddress.MobileNumber,
                    Address1 = customerAddress.Address1,
                    Address2 = customerAddress.Address2,
                    Address3 = customerAddress.Address3,
                    City = customerAddress.CityName,
                    Country = customerAddress.CountryName,
                    Postcal_code = customerAddress.PostcalCode,
                    Zone = customerAddress.Zone,
                    List_Order = sb.ToString()
                }
            };
            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
        }

        public async Task SendEmailWhenMinimumQuantity(OrderMinQuantity orderMinQuantity, int tenantId, CustomerAddressSendEmail customerAddress)
        {
            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = customerAddress.CustomerEmail,
                Variables = new 
                {
                    Company_Address = customerAddress.CompanyAddress,
                    First_name = customerAddress.CustomerName,
                    Last_name = customerAddress.CustomerName,
                    Email_address = customerAddress.CustomerEmail,
                    Phone_number = customerAddress.MobileNumber,
                    Address1 = customerAddress.Address1,
                    Address2 = customerAddress.Address2,
                    Address3 = customerAddress.Address3,
                    City = customerAddress.CityName,
                    Country = customerAddress.CountryName,
                    Postcal_code = customerAddress.PostcalCode,
                    Zone = customerAddress.Zone,
                    MealPlanName = orderMinQuantity.MealPlanName,
                    AvailableBalance = orderMinQuantity.AvailableBalance.ToString(),
                    ExpiryDate = orderMinQuantity.ExpiryDate.ToString("dd/MM/yyyy")
                },
                Module = "MinQuantity"
            };
            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
        }
        
        public async Task SendEmailRemindBuyMealPlan(string customerEmail, string customerName, int tenantId)
        {
            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = customerEmail,
                Variables = new
                {
                    CustomerName = customerName,
                },
                Module = "BuyMealPlanReminder"
            };
            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
        }

        public async Task SendPickUpOrderRemind(string customerEmail, string customerName, int tenantId)
        {

            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = customerEmail,
                Variables = new
                {
                    CustomerName = customerName,
                },
                Module = "PickUpOrderReminder"
            };

            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
        }

        public async Task SendPickUpOrderSuccess(string customerEmail, string customerName, int tenantId)
        {

            var sendEmail = new SendEmailInput
            {
                TenantId = tenantId,
                EmailAddress = customerEmail,
                Variables = new 
                {
                    CustomerName = customerName,
                },
                Module = "PickUpOrderSuccess"
            };

            await _emailAppService.SendEmailByTemplateInBackgroundAsync(sendEmail);
        }
    }
}
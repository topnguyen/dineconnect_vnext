﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;

namespace DinePlan.DineConnect.Tiffins.Promotion.Exporting
{
  public interface IPromotionExcelExporter
    {
        FileDto ExportToFile(List<PromotionListDto> list);
        
        FileDto ExportToFile(List<PromotionCodeDto> list);
    }
}

﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;

namespace DinePlan.DineConnect.Tiffins.Promotion.Exporting
{
    public class PromotionExcelExporter : NpoiExcelExporterBase, IPromotionExcelExporter
    {
        public PromotionExcelExporter(
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<PromotionListDto> list)
        {
            return CreateExcelPackage(
                "Promotions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Promotions"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Validity"),
                        L("VoucherCount"),
                        L("CreationTime")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => 
                            _.Name,
                        _ => _.Validity.ToShortDateString(),
                        _ => _.Codes.Count,
                        _ => _.CreationTime.ToLongTimeString()
                    );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportToFile(List<PromotionCodeDto> list)
        {
            return CreateExcelPackage(
                "Promotions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Promotions"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("ClaimedDate"),
                        L("ClaimedAmount"),
                        L("PaymentId"),
                        L("CustomerId"),
                        L("Remarks")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => 
                            _.Id,
                        _ => _.Code,
                        _ => _.ClaimedTime?.ToLongTimeString() ?? string.Empty,
                        _ => _.ClaimedAmount,
                        _ => _.PaymentId.HasValue ? _.PaymentId.Value.ToString() : string.Empty,
                        _ => _.CustomerId.HasValue ? _.CustomerId.Value.ToString() : string.Empty,
                        _ => _.Remarks
                    );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
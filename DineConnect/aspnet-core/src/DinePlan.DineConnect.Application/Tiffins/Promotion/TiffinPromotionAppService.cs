using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Net.Whatapp;
using DinePlan.DineConnect.Tiffins.MemberPortal;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion.Exporting;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using DinePlan.DineConnect.Whatapp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUglify.Helpers;

namespace DinePlan.DineConnect.Tiffins.Promotion
{
    public class TiffinPromotionAppService : DineConnectAppServiceBase, ITiffinPromotionAppService
    {
        protected readonly IRepository<TiffinPromotion> _promotionRepository;
        protected readonly IRepository<TiffinPromotionCode> _promotionCodeRepository;
        protected readonly IRepository<BaseCore.Customer.Customer> _customerRepository;
        protected readonly IPromotionExcelExporter _promotionExcelExporter;
        protected readonly ITenantEmailSendAppService _tenantEmailSendAppService;
        protected readonly ITenantSettingsAppService _tenantSettingsAppService;
        protected readonly IServiceProvider _serviceProvider;
        protected readonly ICustomerReportAppService _customerReportAppService;
        private readonly ITenantWhatappSendAppService _whatappSendAppService;
        protected static Random _random;
        protected const double PercentageOfMaximumGenerateCode = 0.7;

        public TiffinPromotionAppService(IRepository<TiffinPromotion> promotionRepository,
            IRepository<TiffinPromotionCode> promotionCodeRepository,
            IRepository<BaseCore.Customer.Customer> customerRepository,
            IPromotionExcelExporter promotionExcelExporter,
            ITiffinMemberPortalAppService tiffinMemberPortalAppService,
            ITenantEmailSendAppService tenantEmailSendAppService,
            ITenantSettingsAppService tenantSettingsAppService,
            IServiceProvider serviceProvider,
            ICustomerReportAppService customerReportAppService,
            ITenantWhatappSendAppService whatappSendAppService)
        {
            _promotionRepository = promotionRepository;
            _promotionCodeRepository = promotionCodeRepository;
            _customerRepository = customerRepository;
            _promotionExcelExporter = promotionExcelExporter;
            _tenantEmailSendAppService = tenantEmailSendAppService;
            _tenantSettingsAppService = tenantSettingsAppService;
            _serviceProvider = serviceProvider;
            _customerReportAppService = customerReportAppService;
            _whatappSendAppService = whatappSendAppService;
            _random = new Random();
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<PromotionListDto>> GetPromotions(GetPromotionInputDto input)
        {
            var query = _promotionRepository
                .GetAll()
                .WhereIf(input.FromDate.HasValue, x => x.Validity >= input.FromDate)
                .WhereIf(input.ToDate.HasValue, x => x.Validity <= input.ToDate)
                .WhereIf(input.Type.HasValue, x => x.Type == input.Type)
                .WhereIf(input.IsAutoAttach.HasValue, x => x.IsAutoAttach == input.IsAutoAttach)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Name), x => input.Name.Contains(x.Name) || x.Name.Contains(input.Name));

            var totalCount = await query.CountAsync();

            var list = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .Include(x => x.Codes)
                .ToListAsync();

            var promotions = ObjectMapper.Map<List<PromotionListDto>>(list);

            return new PagedResultDto<PromotionListDto>(totalCount, promotions);
        }

        public async Task<FileDto> GetPromotionsToExcel(GetPromotionInputDto input)
        {
            var pagedPromotions = await GetPromotions(input);

            return _promotionExcelExporter.ExportToFile(pagedPromotions.Items.ToList());
        }

        public async Task<PromotionEditDto> GetPromotionForEdit(EntityDto input)
        {
            var promotion = await _promotionRepository.GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.Codes)
                .ThenInclude(x => x.Customer)
                .FirstOrDefaultAsync();

            return ObjectMapper.Map<PromotionEditDto>(promotion);
        }

        public async Task<int> CreatePromotion(PromotionCreateDto input)
        {
            ValidateGenerateCode(input.GenerateTotalDigits, input.GenerateTotalVouchers);

            var promotion = ObjectMapper.Map<TiffinPromotion>(input);

            var id = await _promotionRepository.InsertAndGetIdAsync(promotion);

            return id;
        }

        public async Task UpdatePromotion(PromotionEditDto input)
        {
            ValidateGenerateCode(input.GenerateTotalDigits, input.GenerateTotalVouchers);

            var promotion = ObjectMapper.Map<TiffinPromotion>(input);

            if (AbpSession.TenantId != null)
            {
                promotion.TenantId = AbpSession.TenantId.Value;
            }

            await _promotionRepository.UpdateAsync(promotion);
        }

        public async Task DeletePromotion(EntityDto input)
        {
            await _promotionRepository.DeleteAsync(input.Id);
        }

        #region Promotion Code

        public async Task<PagedResultDto<PromotionCodeDto>> GetPromotionCodes(GetPromotionCodeInputDto input)
        {
            var query = _promotionCodeRepository
                .GetAll()
                .WhereIf(input.CustomerId.HasValue, x => x.CustomerId == input.CustomerId.Value)
                .WhereIf(!input.CustomerId.HasValue, x => x.CustomerId == null)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Code), x => input.Code.Contains(x.Code) || x.Code.Contains(input.Code));

            var totalCount = await query.CountAsync();

            var list = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var promotionCodes = ObjectMapper.Map<List<PromotionCodeDto>>(list);

            return new PagedResultDto<PromotionCodeDto>(totalCount, promotionCodes);
        }

        public async Task RegisterCustomerToPromotionCode(CustomerPromotionCodeInputDto input)
        {
            var promotionCode = await _promotionCodeRepository.GetAll().FirstOrDefaultAsync(x => x.Id == input.CodeId);

            if (promotionCode == null)
            {
                return;
            }

            if (promotionCode.CustomerId != null && promotionCode.CustomerId != input.CustomerId)
            {
                throw new UserFriendlyException(L("TiffinPromotionCodeAlreadyAssignedToAnotherCustomer"));
            }

            promotionCode.CustomerId = input.CustomerId;

            await _promotionCodeRepository.UpdateAsync(promotionCode);
        }

        public async Task RemoveCustomerToPromotionCode(CustomerPromotionCodeInputDto input)
        {
            var promotionCode = await _promotionCodeRepository.GetAll().FirstOrDefaultAsync(x => x.Id == input.CodeId && x.CustomerId == input.CustomerId);

            if (promotionCode == null)
            {
                return;
            }

            promotionCode.CustomerId = null;

            await _promotionCodeRepository.UpdateAsync(promotionCode);
        }

        public async Task SendPromotionCodeToCustomer(CustomerPromotionCodeInputDto input)
        {
            var promotionCode = await _promotionCodeRepository
                .GetAll()
                .Include(x => x.Customer)
                .ThenInclude(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == input.CodeId && x.CustomerId == input.CustomerId);

            if (promotionCode == null)
            {
                return;
            }

            if (input.SendBy == CustomerPromotionCodeSendBy.Email ||
                input.SendBy == CustomerPromotionCodeSendBy.EmailAndWhatapp)
            {
                var emailInformation = new SendEmailInput
                {
                    EmailAddress = promotionCode.Customer.EmailAddress ?? promotionCode.Customer.User.EmailAddress,
                    Module = "CustomerPromotionCode",
                    Variables = new
                    {
                        CustomerName = promotionCode.Customer.Name,
                        PromotionCode = promotionCode.Code
                    },
                    TenantId = promotionCode.Customer.TenantId
                };

                await _tenantEmailSendAppService.SendEmailByTemplateAsync(emailInformation);
            }

            if (input.SendBy == CustomerPromotionCodeSendBy.Whatapp ||
                input.SendBy == CustomerPromotionCodeSendBy.EmailAndWhatapp)
            {
                // Send Whatapp
                await _whatappSendAppService.SendWhatappByTemplateAsync(promotionCode.Customer.PhoneNumber ?? promotionCode.Customer.User.PhoneNumber, "CustomerPromotionCode", promotionCode.Customer.TenantId, new
                {
                    CustomerName = promotionCode.Customer.Name,
                    PromotionCode = promotionCode.Code
                });
            }
        }

        public async Task<FileDto> GetPromotionCodeToExcel(EntityDto input)
        {
            var promotionCodes = await GetPromotionForEdit(input);

            return _promotionExcelExporter.ExportToFile(promotionCodes.Codes);
        }

        public async Task<List<string>> GeneratePromotionCode(PromotionCodeGenerateDto input)
        {
            if (input.TotalVouchers <= 0 || input.TotalDigits <= 0)
            {
                return new List<string>();
            }

            var maxValue = int.Parse(new string('9', input.TotalDigits));

            ValidateGenerateCode(input.TotalDigits, input.TotalVouchers);

            List<string> promotionCodes = new List<string>();

            var random = new Random();

            while (promotionCodes.Count < input.TotalVouchers)
            {
                var promotionCode = random.Next(0, maxValue).ToString($"D{input.TotalDigits}");

                if (!string.IsNullOrWhiteSpace(input.Prefix))
                {
                    promotionCode = $"{input.Prefix}{promotionCode}";
                }

                if (!string.IsNullOrWhiteSpace(input.Suffix))
                {
                    promotionCode = $"{promotionCode}{input.Suffix}";
                }

                if (!promotionCodes.Contains(promotionCode))
                {
                    promotionCodes.Add(promotionCode);
                }
            }

            var promotionCodeEntities = promotionCodes.Select(x => new TiffinPromotionCode
            {
                Code = x,
                PromotionId = input.PromotionId
            });

            foreach (var promotionCodeEntity in promotionCodeEntities)
            {
                await _promotionCodeRepository.InsertAsync(promotionCodeEntity);
            }

            return promotionCodes;
        }

        public async Task<string> GeneratePromotionCodeForReferralEarning(int customerId)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var promotion = await _promotionRepository
                .GetAll()
                .Include(x => x.Codes)
                .FirstOrDefaultAsync(x => x.IsAutoAttach && x.Id == allSettings.ReferralEarning.ReferralEarnValue);

            var promotionCode = await GeneratePromotionCodeAndSendToCustomer(promotion, customerId);

            return promotionCode;
        }

        public async Task GeneratePromotionCodeAndSendToAllCustomer(int promotionId, CustomerReportInput filter)
        {
            var customersReport = await _customerReportAppService.GetData(filter);

            var customerIds = customersReport.Select(x => (int)x.CustomerId).ToList();

            await GeneratePromotionCodeAndSendToCustomers(promotionId, customerIds);
        }

        public async Task GeneratePromotionCodeAndSendToCustomers(int promotionId, List<int> customerIds)
        {
            if (customerIds?.Any() != true)
            {
                return;
            }

            customerIds = customerIds.Distinct().ToList();

            var promotion = await _promotionRepository
                 .GetAll()
                 .Include(x => x.Codes)
                 .FirstOrDefaultAsync(x => x.IsAutoAttach && x.Id == promotionId);

            ValidateGenerateCode(promotion.GenerateTotalDigits, promotion.Codes.Count + customerIds.Count);

            foreach (var customerId in customerIds)
            {
                await GeneratePromotionCodeAndSendToCustomer(promotion, customerId);
            }
        }

        public async Task<string> GeneratePromotionCodeAndSendToCustomer(TiffinPromotion promotion, int customerId)
        {
            string promotionCode = null;

            if (promotion == null || promotion.GenerateTotalDigits <= 0)
            {
                return null;
            }

            var promotionCodes = promotion.Codes?.Select(x => x.Code).ToList() ?? new List<string>();

            // Validation before generate

            try
            {
                ValidateGenerateCode(promotion.GenerateTotalDigits, promotionCodes.Count + 1);
            }
            catch
            {
                return null;
            }

            if (promotionCodes.Any())
            {
                while (promotionCodes.Contains(promotionCode))
                {
                    promotionCode = GeneratePromotionCode(promotion);
                }
            }
            else
            {
                promotionCode = GeneratePromotionCode(promotion);
            }

            // Finish generate promotion code, register to customer
            var promotionCodeEntity = new TiffinPromotionCode
            {
                CustomerId = customerId,
                Code = promotionCode,
                PromotionId = promotion.Id,
                Remarks = "Referral Award",
                IsClaimed = false
            };

            var promotionCodeId = await _promotionCodeRepository.InsertAndGetIdAsync(promotionCodeEntity);

            // Send Email Promotion Code to Customer have acknowledge

            await SendPromotionCodeToCustomer(new CustomerPromotionCodeInputDto
            {
                CustomerId = customerId,
                CodeId = promotionCodeId
            });

            return promotionCode;
        }

        private static string GeneratePromotionCode(TiffinPromotion promotion)
        {
            var maxValue = int.Parse(new string('9', promotion.GenerateTotalDigits));

            var promotionCode = _random.Next(0, maxValue).ToString($"D{promotion.GenerateTotalDigits}");

            if (!string.IsNullOrWhiteSpace(promotion.GeneratePrefix))
            {
                promotionCode = $"{promotion.GeneratePrefix}{promotionCode}";
            }

            if (!string.IsNullOrWhiteSpace(promotion.GenerateSuffix))
            {
                promotionCode = $"{promotionCode}{promotion.GenerateSuffix}";
            }

            return promotionCode;
        }

        public async Task<PromotionEditDto> GetPromotionByCode(string code, bool isIncludeCodesDetail = false, bool? claimedStatus = false)
        {
            var customer = await _customerRepository.FirstOrDefaultAsync(c => c.UserId == AbpSession.UserId);

            var promotionId = await _promotionCodeRepository.GetAll()
                .Where(x => x.Code == code && (x.CustomerId == null || x.CustomerId == customer.Id))
                .WhereIf(claimedStatus.HasValue, x => x.IsClaimed == claimedStatus)
                .Select(x => x.PromotionId)
                .FirstOrDefaultAsync();

            if (promotionId == default)
            {
                return null;
            }

            var promotion = await GetPromotionForEdit(new EntityDto
            {
                Id = promotionId
            });

            if (!isIncludeCodesDetail)
            {
                promotion.Codes = new List<PromotionCodeDto>();
            }

            return promotion;
        }

        public async Task<PromotionDiscountPaymentDto> GetPromotionDiscountPayment(GetPromotionDiscountPaymentInputDto input)
        {
            var promotionDiscountPaymentInfo = new PromotionDiscountPaymentDto();

            if (input.Offers?.Any() != true)
            {
                return promotionDiscountPaymentInfo;
            }

            // Get Original Price by Offer

            input.Offers = input.Offers.DistinctBy(x => x.Id).ToList();

            var tiffinMemberPortalAppService = _serviceProvider.GetService<TiffinMemberPortalAppService>();

            var allActiveOffer = await tiffinMemberPortalAppService.GetActiveOffers(new GetAllTiffinProductOffersInput());

            foreach (var orderOffer in input.Offers)
            {
                var offerInfo = allActiveOffer.Items.FirstOrDefault(x => x.Id == orderOffer.Id);

                if (offerInfo == null)
                {
                    continue;
                }

                orderOffer.Quantity = orderOffer.Quantity > 0 ? orderOffer.Quantity : 0;

                var orderOfferAmount = offerInfo.Price * orderOffer.Quantity;

                promotionDiscountPaymentInfo.OriginalAmount += (double)orderOfferAmount;
            }

            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.OriginalAmount;

            // Get Promotion Info

            input.Codes = input.Codes?.Distinct().ToList();

            var appliedPromotion = await ValidateAndGetPromotionInfo(input.Codes);

            if (appliedPromotion == null)
            {
                return promotionDiscountPaymentInfo;
            }

            if (appliedPromotion.MinimumOrderAmount > promotionDiscountPaymentInfo.OriginalAmount)
            {
                throw new UserFriendlyException(string.Format(L("TiffinPromotionMinimumOrderAmountRequire"), appliedPromotion.MinimumOrderAmount, DineConnectConsts.Currency));
            }

            double onePromotionCodeClaimAmount;

            if (appliedPromotion.Type == TiffinPromotionType.Percentage)
            {
                onePromotionCodeClaimAmount = promotionDiscountPaymentInfo.OriginalAmount * appliedPromotion.VoucherValue / 100;
            }
            else
            {
                onePromotionCodeClaimAmount = appliedPromotion.VoucherValue;
            }

            // Fill Detail Discount for each Code

            promotionDiscountPaymentInfo.DiscountDetails = new List<PromotionCodeDiscountDetailPaymentDto>();

            foreach (var code in input.Codes)
            {
                var promotionCode = appliedPromotion.Codes.FirstOrDefault(x => x.Code == code);

                if (promotionCode == null)
                {
                    continue;
                }

                var promotionCodeDiscountDetail = new PromotionCodeDiscountDetailPaymentDto
                {
                    Id = promotionCode.Id,
                    Code = promotionCode.Code,
                    ClaimAmount = onePromotionCodeClaimAmount
                };

                promotionDiscountPaymentInfo.DiscountDetails.Add(promotionCodeDiscountDetail);
            }

            promotionDiscountPaymentInfo.TotalClaimAmount = promotionDiscountPaymentInfo.DiscountDetails.Sum(x => x.ClaimAmount);

            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.OriginalAmount - promotionDiscountPaymentInfo.TotalClaimAmount;

            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.DiscountedAmount > 0
                ? promotionDiscountPaymentInfo.DiscountedAmount
                : 0;

            return promotionDiscountPaymentInfo;
        }

        #endregion

        #region Helper

        protected async Task<PromotionEditDto> ValidateAndGetPromotionInfo(List<string> codes)
        {
            codes = codes?.Distinct().ToList();

            // Get Promotion Info

            PromotionEditDto mainPromotion = null;

            if (codes?.Any() == true)
            {
                var promotions = new List<PromotionEditDto>();

                var customer = await _customerRepository.FirstOrDefaultAsync(c => c.UserId == AbpSession.UserId);

                foreach (var code in codes)
                {
                    var promotion = await GetPromotionByCode(code, true);

                    if (promotions.All(x => x.Id != promotion.Id))
                    {
                        promotions.Add(promotion);
                    }

                    var promotionCode = promotion.Codes.FirstOrDefault(x => x.Code == code);

                    if (promotionCode?.CustomerId != null && promotionCode?.CustomerId != customer.Id)
                    {
                        throw new UserFriendlyException(L("TiffinPromotionYouNotHaveConditionToUseThisPromotion"));
                    }
                }

                if (promotions.Count > 1)
                {
                    throw new UserFriendlyException(L("TiffinPromotionMaximumOnePromotionPerPayment"));
                }

                mainPromotion = promotions.Single();

                if (!mainPromotion.AllowMultipleRedemption && codes.Count > 1)
                {
                    throw new UserFriendlyException(string.Format(L("TiffinPromotionMaximumPromotionCodeCanApply"), 1));
                }

                mainPromotion.MultiRedemptionCount = mainPromotion.MultiRedemptionCount < 1 ? 1 : mainPromotion.MultiRedemptionCount;

                if (codes.Count > mainPromotion.MultiRedemptionCount)
                {
                    throw new UserFriendlyException(string.Format(L("TiffinPromotionMaximumPromotionCodeCanApply"), mainPromotion.MultiRedemptionCount));
                }
            }

            return mainPromotion;
        }

        protected void ValidateGenerateCode(int totalDigits, int totalVouchers)
        {
            if (totalVouchers <= 0 || totalDigits <= 0)
            {
                return;
            }

            var maxValue = int.Parse(new string('9', totalDigits));

            var maximumNumberOfCodeAllowed = maxValue * PercentageOfMaximumGenerateCode;

            if (totalVouchers > maximumNumberOfCodeAllowed)
            {
                throw new UserFriendlyException(string.Format(L("TiffinPromotionCodeMaximumCodeAllowToGeneration"), maximumNumberOfCodeAllowed, totalDigits));
            }
        }

        #endregion
    }
}
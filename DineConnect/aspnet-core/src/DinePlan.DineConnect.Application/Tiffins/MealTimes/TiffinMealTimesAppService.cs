﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Tiffins.MealTimes.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.MealTimes
{
    
    public class TiffinMealTimesAppService : DineConnectAppServiceBase, ITiffinMealTimesAppService
    {
        private readonly IRepository<TiffinMealTime> _tiffinMealTimeRepository;
        private readonly IRepository<User, long> _userRepository;

        public TiffinMealTimesAppService(IRepository<TiffinMealTime> tiffinMealTimeRepository
            , IRepository<User, long> userRepository)
        {
            _tiffinMealTimeRepository = tiffinMealTimeRepository;
            _userRepository = userRepository;
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<TiffinMealTimeDto>> GetAll(GetAllTiffinMealTimesInput input)
        {
            var query = from mealTime in _tiffinMealTimeRepository.GetAll()
                        join user in _userRepository.GetAll() on mealTime.LastModifierUserId equals user.Id into userJoin
                        from joinedUser in userJoin.DefaultIfEmpty()
                        select new TiffinMealTimeDto
                        {
                            Id = mealTime.Id,
                            Code = mealTime.Code,
                            Name = mealTime.Name,
                            LastModificationTime = mealTime.LastModificationTime,
                            Modifier = joinedUser.Name,
                            IsDefault = mealTime.IsDefault
                        };

            var filteredTiffinMealTimes = query
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                         EF.Functions.Like(e.Name, input.Filter.ToILikeString())
                         || EF.Functions.Like(e.Code, input.Filter.ToILikeString())
                         || EF.Functions.Like(e.Modifier, input.Filter.ToILikeString()));

            var pagedAndFilteredTiffinMealTimes = filteredTiffinMealTimes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var totalCount = await filteredTiffinMealTimes.CountAsync();

            return new PagedResultDto<TiffinMealTimeDto>(
                totalCount,
                await pagedAndFilteredTiffinMealTimes.ToListAsync()
            );
        }

        
        public async Task<CreateOrEditTiffinMealTimeDto> GetTiffinMealTimeForEdit(EntityDto input)
        {
            var tiffinMealTime = await _tiffinMealTimeRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTiffinMealTimeDto>(tiffinMealTime);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTiffinMealTimeDto input)
        {
            ValidateForCreate(input);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTiffinMealTimeDto input)
        {
            var tiffinMealTime = ObjectMapper.Map<TiffinMealTime>(input);

            if (AbpSession.TenantId != null)
            {
                tiffinMealTime.TenantId = (int)AbpSession.TenantId;
            }

            tiffinMealTime.LastModificationTime = Clock.Now;
            tiffinMealTime.LastModifierUserId = AbpSession.UserId;

            if (tiffinMealTime.IsDefault)
            {
                var mealTimes = _tiffinMealTimeRepository.GetAll().ToList();

                foreach (var mealTime in mealTimes)
                {
                    mealTime.IsDefault = false;

                    await _tiffinMealTimeRepository.UpdateAsync(mealTime);
                }
            }

            await _tiffinMealTimeRepository.InsertAsync(tiffinMealTime);
        }

        
        protected virtual async Task Update(CreateOrEditTiffinMealTimeDto input)
        {
            var tiffinMealTime = await _tiffinMealTimeRepository.FirstOrDefaultAsync((int)input.Id);
         
            ObjectMapper.Map(input, tiffinMealTime);

            await _tiffinMealTimeRepository.UpdateAsync(tiffinMealTime);

            if (tiffinMealTime.IsDefault)
            {
                var mealTimes = _tiffinMealTimeRepository.GetAll().ToList();

                foreach (var mealTime in mealTimes)
                {
                    if (tiffinMealTime.Id == mealTime.Id)
                    {
                        continue;
                    }

                    mealTime.IsDefault = false;

                    await _tiffinMealTimeRepository.UpdateAsync(mealTime);
                }
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            await _tiffinMealTimeRepository.DeleteAsync(input.Id);
        }

        private void ValidateForCreate(CreateOrEditTiffinMealTimeDto input)
        {
            var exists = _tiffinMealTimeRepository.GetAll()
               .WhereIf(input.Id.HasValue, e => e.Id != input.Id);

            if (exists.Any(e => e.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            if (exists.Any(e => e.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }
        }
    }
}
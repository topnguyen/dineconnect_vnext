﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.ProductOfferType;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.ProductOfferTypes
{
    
    public class TiffinProductOfferTypesAppService : DineConnectAppServiceBase, ITiffinProductOfferTypesAppService
    {
        private readonly IRepository<TiffinProductOfferType> _tiffinProductOfferTypeRepository;
        private readonly ITiffinProductOfferTypesExcelExporter _tiffinProductOfferTypesExcelExporter;

        public TiffinProductOfferTypesAppService(IRepository<TiffinProductOfferType> tiffinProductOfferTypeRepository, ITiffinProductOfferTypesExcelExporter tiffinProductOfferTypesExcelExporter)
        {
            _tiffinProductOfferTypeRepository = tiffinProductOfferTypeRepository;
            _tiffinProductOfferTypesExcelExporter = tiffinProductOfferTypesExcelExporter;
        }

        public async Task<PagedResultDto<GetTiffinProductOfferTypeForViewDto>> GetAll(GetAllTiffinProductOfferTypesInput input)
        {
            var filteredTiffinProductOfferTypes = _tiffinProductOfferTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFilteredTiffinProductOfferTypes = filteredTiffinProductOfferTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var tiffinProductOfferTypes = from o in pagedAndFilteredTiffinProductOfferTypes
                                          select new GetTiffinProductOfferTypeForViewDto()
                                          {
                                              TiffinProductOfferType = new TiffinProductOfferTypeDto
                                              {
                                                  Name = o.Name,
                                                  Id = o.Id
                                              }
                                          };

            var totalCount = await filteredTiffinProductOfferTypes.CountAsync();

            return new PagedResultDto<GetTiffinProductOfferTypeForViewDto>(
                totalCount,
                await tiffinProductOfferTypes.ToListAsync()
            );
        }

        
        public async Task<GetTiffinProductOfferTypeForEditOutput> GetTiffinProductOfferTypeForEdit(EntityDto input)
        {
            var tiffinProductOfferType = await _tiffinProductOfferTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTiffinProductOfferTypeForEditOutput { TiffinProductOfferType = ObjectMapper.Map<CreateOrEditTiffinProductOfferTypeDto>(tiffinProductOfferType) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTiffinProductOfferTypeDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTiffinProductOfferTypeDto input)
        {
            var tiffinProductOfferType = ObjectMapper.Map<TiffinProductOfferType>(input);

            if (AbpSession.TenantId != null)
            {
                tiffinProductOfferType.TenantId = (int)AbpSession.TenantId;
            }

            await _tiffinProductOfferTypeRepository.InsertAsync(tiffinProductOfferType);
        }

        
        protected virtual async Task Update(CreateOrEditTiffinProductOfferTypeDto input)
        {
            var tiffinProductOfferType = await _tiffinProductOfferTypeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, tiffinProductOfferType);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _tiffinProductOfferTypeRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetTiffinProductOfferTypesToExcel(GetAllTiffinProductOfferTypesForExcelInput input)
        {
            var filteredTiffinProductOfferTypes = _tiffinProductOfferTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredTiffinProductOfferTypes
                         select new GetTiffinProductOfferTypeForViewDto()
                         {
                             TiffinProductOfferType = new TiffinProductOfferTypeDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             }
                         });

            var tiffinProductOfferTypeListDtos = await query.ToListAsync();

            return _tiffinProductOfferTypesExcelExporter.ExportToFile(tiffinProductOfferTypeListDtos);
        }

        protected void ValidateForCreateOrUpdate(CreateOrEditTiffinProductOfferTypeDto input)
        {
            var exists = _tiffinProductOfferTypeRepository.GetAll()
                .Where(e => e.Name.Equals(input.Name))
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }
    }
}
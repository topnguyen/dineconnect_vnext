﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Exporting
{
    public class TiffinProductOfferTypesExcelExporter : NpoiExcelExporterBase, ITiffinProductOfferTypesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TiffinProductOfferTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTiffinProductOfferTypeForViewDto> tiffinProductOfferTypes)
        {
            return CreateExcelPackage(
                "TiffinProductOfferTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TiffinProductOfferTypes"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, tiffinProductOfferTypes,
                        _ => _.TiffinProductOfferType.Name
                        );
                });
        }
    }
}
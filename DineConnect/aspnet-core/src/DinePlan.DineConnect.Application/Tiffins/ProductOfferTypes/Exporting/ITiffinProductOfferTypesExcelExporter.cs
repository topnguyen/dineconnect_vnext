﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Exporting
{
    public interface ITiffinProductOfferTypesExcelExporter
    {
        FileDto ExportToFile(List<GetTiffinProductOfferTypeForViewDto> tiffinProductOfferTypes);
    }
}
﻿using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot
{
    
    public class DeliveryTimeSlotAppService : DineConnectAppServiceBase, IDeliveryTimeSlotAppService
    {
        private readonly IRepository<TiffinDeliveryTimeSlot> _tiffinDeliveryTimeSlotRepo;

        public DeliveryTimeSlotAppService(IRepository<TiffinDeliveryTimeSlot> tiffinDeliveryTimeSlotRepo)
        {
            _tiffinDeliveryTimeSlotRepo = tiffinDeliveryTimeSlotRepo;
        }

        
        public async Task UpsertList(List<DeliveryTimeSlotDto> data)
        {
            foreach (var deliveryTimeSlotDto in data)
            {
                var entity = ObjectMapper.Map<TiffinDeliveryTimeSlot>(deliveryTimeSlotDto);

                if (entity.Type == TiffinDeliveryTimeSlotType.Delivery)
                {
                    entity.LocationId = null;
                }
                else
                {
                    entity.DayOfWeek = null;
                }

                await _tiffinDeliveryTimeSlotRepo.InsertOrUpdateAsync(entity);
            }
        }

        public async Task<PagedResultDto<DeliveryTimeSlotDto>> GetAll(GetDeliveryTimeSlotInputDto input)
        {
            var deliveryTimeSlotsQuery = _tiffinDeliveryTimeSlotRepo.GetAll()
                .WhereIf(input.DayOfWeek.HasValue, x => x.DayOfWeek == input.DayOfWeek)
                .WhereIf(input.Type.HasValue, x => x.Type == input.Type);

            var totalCount = await deliveryTimeSlotsQuery.CountAsync();

            var deliveryTimeSlots = await deliveryTimeSlotsQuery
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var deliveryTimeSlotsDto = ObjectMapper.Map<List<DeliveryTimeSlotDto>>(deliveryTimeSlots);

            return new PagedResultDto<DeliveryTimeSlotDto>(totalCount, deliveryTimeSlotsDto);
        }

        public async Task<DeliveryTimeSlotDto> GetByLocationAndMealTimeId(int locationId, int mealTimeId)
        {
            var deliveryTimeSlot = await _tiffinDeliveryTimeSlotRepo.GetAll().FirstOrDefaultAsync(x => x.LocationId == locationId && x.MealTimeId == mealTimeId);

            var deliveryTimeSlotDto = ObjectMapper.Map<DeliveryTimeSlotDto>(deliveryTimeSlot);

            return deliveryTimeSlotDto;
        }

        
        public async Task Delete(int id)
        {
            await _tiffinDeliveryTimeSlotRepo.DeleteAsync(id);
        }
    }
}

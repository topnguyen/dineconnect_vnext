﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Job
{
    public class DelAggItemBackgroundClone : BackgroundJob<DelAggItemBackgroundCloneIdJobArgs>, ITransientDependency
    {
        private readonly IDelAggItemAppService _delAggItemAppService;
        private readonly IReportBackgroundAppService _reportAppService;

        public DelAggItemBackgroundClone(IDelAggItemAppService delAggItemAppService,
            IReportBackgroundAppService reportAppService)
        {
            _delAggItemAppService = delAggItemAppService;
            _reportAppService = reportAppService;
        }

        [UnitOfWork]
        public override void Execute(DelAggItemBackgroundCloneIdJobArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = new FileDto
                {
                };
                AsyncHelper.RunSync(() => _delAggItemAppService.CloneItems(new CloneDelAggItemDtos
                {
                    BackGroundId = args.BackGroundId,
                    TenantId = args.TenantId,
                    UserId = args.UserId,
                }));
            }
        }
    }

    [Serializable]
    public class DelAggItemBackgroundCloneIdJobArgs
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public DelAggItemBackgroundCloneIdJobArgs()
        {
        }
    }
}

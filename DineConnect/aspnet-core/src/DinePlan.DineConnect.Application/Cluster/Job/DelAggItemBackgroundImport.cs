﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Job
{
    public class DelAggItemBackgroundImport : BackgroundJob<DelAggItemBackgroundImportIdJobArgs>, ITransientDependency
    {
        private readonly IDelAggItemAppService _delAggItemAppService;
        private readonly IReportBackgroundAppService _reportAppService;

        public DelAggItemBackgroundImport(IDelAggItemAppService delAggItemAppService,
            IReportBackgroundAppService reportAppService)
        {
            _delAggItemAppService = delAggItemAppService;
            _reportAppService = reportAppService;
        }

        [UnitOfWork]
        public override void Execute(DelAggItemBackgroundImportIdJobArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = new FileDto
                {
                };
                AsyncHelper.RunSync((() => _delAggItemAppService.BulkDelAggItemImport(new BulkDelAggItemImportDtos
                {
                    BackGroundId = args.BackGroundId,
                    TenantId = args.TenantId,
                    UserId = args.UserId,
                    CreateOrUpdateDelAggItemInputs = args.BulkList
                })));

                AsyncHelper.RunSync(() =>
                       _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
            }


        }
    }

    [Serializable]
    public class DelAggItemBackgroundImportIdJobArgs
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public List<CreateOrUpdateDelAggItemInput> BulkList { get; set; }
        public DelAggItemBackgroundImportIdJobArgs()
        {
            BulkList = new List<CreateOrUpdateDelAggItemInput>();
        }
    }
}

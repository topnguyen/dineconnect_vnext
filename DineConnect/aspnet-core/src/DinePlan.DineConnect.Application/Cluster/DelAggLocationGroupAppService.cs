﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggLocationGroupAppService : DineConnectAppServiceBase, IDelAggLocationGroupAppService
    {
        private readonly IDelAggLocationGroupListExcelExporter _delagglocationgroupExporter;
        private readonly IRepository<DelAggLocationGroup> _delagglocationgroupRepo;
        private readonly IDelAggLocationGroupManager _delAggLocationGroupManager;
        private readonly IRepository<DelAggLocation> _delagglocationRepo;
        public DelAggLocationGroupAppService(IRepository<DelAggLocationGroup> delagglocationgroupRepo,
            IDelAggLocationGroupListExcelExporter delagglocationgroupExporter,
            IDelAggLocationGroupManager delAggLocationGroupManager,
            IRepository<DelAggLocation> delagglocationRepo)
        {
            _delagglocationgroupRepo = delagglocationgroupRepo;
            _delagglocationgroupExporter = delagglocationgroupExporter;
            _delAggLocationGroupManager = delAggLocationGroupManager;
            _delagglocationRepo = delagglocationRepo;
        }

        public async Task<PagedResultDto<DelAggLocationGroupListDto>> GetAll(GetDelAggLocationGroupInput input)
        {
            var allItems = _delagglocationgroupRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLocationGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DelAggLocationGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggLocationGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggLocationGroupListDto>>();
            return _delagglocationgroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggLocationGroupForEditOutput> GetDelAggLocationGroupForEdit(EntityDto input)
        {
            DelAggLocationGroupEditDto editDto;

            var hDto = await _delagglocationgroupRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<DelAggLocationGroupEditDto>();

            return new GetDelAggLocationGroupForEditOutput
            {
                DelAggLocationGroup = editDto
            };
        }

        public async Task CreateOrUpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            if (input.DelAggLocationGroup.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _delagglocationgroupRepo.FirstOrDefault(p => p.Name == input.DelAggLocationGroup.Name && p.Id != input.DelAggLocationGroup.Id);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await UpdateDelAggLocationGroup(input);
            }
            else
            {
                var alreadyExist = _delagglocationgroupRepo.FirstOrDefault(p => p.Name == input.DelAggLocationGroup.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await CreateDelAggLocationGroup(input);
            }
        }

        public async Task DeleteDelAggLocationGroup(EntityDto input)
        {
            var locRefCount = await _delagglocationRepo.CountAsync(t => t.DelAggLocationGroupId.Equals(input.Id));
            if( locRefCount == 0)
            {
                await _delagglocationgroupRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException( L("ReferenceExists", L("DelAggLocationGroup") ,L("DelAggLocation")));
            }
            
        }

        protected virtual async Task UpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            var item = await _delagglocationgroupRepo.GetAsync(input.DelAggLocationGroup.Id.Value);
            var dto = input.DelAggLocationGroup;
            //TODO: SERVICE DelAggLocationGroup Update Individually
            item.Name = dto.Name;
        }

        protected virtual async Task CreateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            var dto = input.DelAggLocationGroup.MapTo<DelAggLocationGroup>();
            CheckErrors(await _delAggLocationGroupManager.CreateSync(dto));
        }
        public async Task<ListResultDto<DelAggLocationGroupListDto>> GetNames()
        {
            var lstDelAggLocationGroup = await _delagglocationgroupRepo.GetAll().ToListAsync();
            return new ListResultDto<DelAggLocationGroupListDto>(lstDelAggLocationGroup.MapTo<List<DelAggLocationGroupListDto>>());
        }
    }
}


﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggTaxAppService : DineConnectAppServiceBase, IDelAggTaxAppService
    {
        private readonly IDelAggTaxListExcelExporter _delaggtaxExporter;
        private readonly IRepository<DelAggTax> _delaggtaxRepo;
        private readonly IDelAggTaxManager _delaggtaxManager;
        private readonly IRepository<DelAggTaxMapping> _delaggtaxmappingRepo;

        public DelAggTaxAppService(IRepository<DelAggTax> delaggtaxRepo, IDelAggTaxManager delaggtaxManager,
            IDelAggTaxListExcelExporter delaggtaxExporter,
            IRepository<DelAggTaxMapping> delaggtaxmappingRepo)
        {
            _delaggtaxRepo = delaggtaxRepo;
            _delaggtaxManager = delaggtaxManager;
            _delaggtaxExporter = delaggtaxExporter;
            _delaggtaxmappingRepo = delaggtaxmappingRepo;
        }

        public async Task<PagedResultDto<DelAggTaxListDto>> GetAll(GetDelAggTaxInput input)
        {
            var delaggtax = _delaggtaxRepo.GetAll();
            if (input.TaxTypeId.HasValue)
            {
                delaggtax = delaggtax.Where(t => t.TaxTypeId == input.TaxTypeId);
            }


            var allItems = (from deltax in delaggtax
                            select new DelAggTaxListDto()
                            {
                                Id = deltax.Id,
                                Name = deltax.Name,
                                TaxTypeId=deltax.TaxTypeId,
                                TaxPercentage=deltax.TaxPercentage,
                                LocalRefCode=deltax.LocalRefCode,
                                CreationTime = deltax.CreationTime
                            }).WhereIf(
                          !input.Filter.IsNullOrEmpty(),
                          p => p.Name.Contains(input.Filter)
                      );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggTaxListDto>>();

            var allItemCount = await allItems.CountAsync();

            var delAggTaxTypeList = await GetDelAggTaxTypeForCombobox();
            var delAggTaxTypeDtos = delAggTaxTypeList.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t => t.TaxTypeId))
            {
                var dat = delAggTaxTypeDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (dat != null)
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.TaxTypeName = dat.DisplayText;
                    }
                }
            }

            return new PagedResultDto<DelAggTaxListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggTaxInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggTaxListDto>>();
            return _delaggtaxExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggTaxForEditOutput> GetDelAggTaxForEdit(EntityDto input)
        {
            DelAggTaxEditDto editDto;
            List<DelAggTaxMappingListDto> editDetailDto;

            var hDto = await _delaggtaxRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<DelAggTaxEditDto>();

            editDetailDto = await (from tt in _delaggtaxmappingRepo.GetAll().Where(a => a.DelAggTaxId == input.Id)
                select new DelAggTaxMappingListDto
                {
                    Id = tt.Id,
                    DelAggTaxId = tt.DelAggTaxId,
                    DelAggLocationGroupId = tt.DelAggLocationGroupId,
                    DelItemGroupId = tt.DelItemGroupId
                }).ToListAsync();

            return new GetDelAggTaxForEditOutput
            {
                DelAggTax = editDto,
                DelAggTaxMapping = editDetailDto
            };
        }

        public async Task CreateOrUpdateDelAggTax(CreateOrUpdateDelAggTaxInput input)
        {
            int taxId = 0;
            if (input.DelAggTax.Id.HasValue)
            {
                taxId = input.DelAggTax.Id.Value;
                await UpdateDelAggTax(input);
            }
            else
            {
                taxId = await CreateDelAggTax(input);
            }
        }

        public async Task DeleteDelAggTax(EntityDto input)
        {

            var mapdetail = await _delaggtaxmappingRepo.GetAll().Where(t => t.DelAggTaxId == input.Id).ToListAsync();

            foreach (var map in mapdetail)
            {
                await _delaggtaxmappingRepo.DeleteAsync(map.Id);
            }

            await _delaggtaxRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggTax(CreateOrUpdateDelAggTaxInput input)
        {
            var item = await _delaggtaxRepo.GetAsync(input.DelAggTax.Id.Value);
            var dto = input.DelAggTax;
            //TODO: SERVICE DelAggTax Update Individually
            item.Name = dto.Name;
            item.TaxTypeId= dto.TaxTypeId;
            item.TaxPercentage = dto.TaxPercentage;
            item.LocalRefCode = dto.LocalRefCode;

            List<int> recordsToBeRetained = new List<int>();
            if (input.DelAggTaxMapping != null && input.DelAggTaxMapping.Count > 0)
            {
                foreach (var items in input.DelAggTaxMapping)
                {
                    List<DelAggTaxMapping> existingIssueDetail;
                    if (items.Id != 0)
                    {
                        var recId = (int)items.Id;
                        recordsToBeRetained.Add(recId);
                        existingIssueDetail = _delaggtaxmappingRepo.GetAllList(tt => tt.DelAggTaxId == dto.Id && tt.Id == recId);
                    }
                    else
                    {
                        existingIssueDetail = _delaggtaxmappingRepo.GetAllList(tt => tt.DelAggTaxId == dto.Id && tt.DelAggLocationGroupId == items.DelAggLocationGroupId && tt.DelItemGroupId == items.DelItemGroupId);
                    }

                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        DelAggTaxMapping tm = new DelAggTaxMapping();
                        tm.DelAggTaxId = (int)dto.Id;
                        tm.DelAggLocationGroupId = items.DelAggLocationGroupId;
                        tm.DelItemGroupId = items.DelItemGroupId;

                        var ret2 = await _delaggtaxmappingRepo.InsertOrUpdateAndGetIdAsync(tm);
                        recordsToBeRetained.Add(ret2);
                    }
                    else
                    {
                        var editDetailDto = await _delaggtaxmappingRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.DelAggTaxId = (int)dto.Id;
                        editDetailDto.DelAggLocationGroupId = items.DelAggLocationGroupId;
                        editDetailDto.DelItemGroupId = items.DelItemGroupId;
                        var ret3 = await _delaggtaxmappingRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);
                        recordsToBeRetained.Add(ret3);
                    }
                }
            }

            var delTTList = _delaggtaxmappingRepo.GetAll().Where(a => a.DelAggTaxId == input.DelAggTax.Id.Value && !recordsToBeRetained.Contains((int)a.Id)).ToList();
            foreach (var a in delTTList)
            {
                _delaggtaxmappingRepo.Delete(a.Id);
            }


            CheckErrors(await _delaggtaxManager.CreateSync(item));
        }

        protected virtual async Task<int> CreateDelAggTax(CreateOrUpdateDelAggTaxInput input)
        {
            var dto = input.DelAggTax.MapTo<DelAggTax>();
            if (!input.DelAggTaxMapping.Any())
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }
            int retId = await _delaggtaxRepo.InsertAndGetIdAsync(dto);
            foreach (var items in input.DelAggTaxMapping.ToList())
            {
                DelAggTaxMapping tm = new DelAggTaxMapping
                {
                    DelAggTaxId = (int)retId,
                    DelAggLocationGroupId = items.DelAggLocationGroupId,
                    DelItemGroupId = items.DelItemGroupId
                };

                await _delaggtaxmappingRepo.InsertAndGetIdAsync(tm);
            }
            CheckErrors(await _delaggtaxManager.CreateSync(dto));
            return dto.Id;

        }


        public async Task<ListResultDto<ComboboxItemDto>> GetDelAggTaxForCombobox()
        {
            var lst = await _delaggtaxRepo.GetAll().ToListAsync(); ;
            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }
        public async Task<ListResultDto<ComboboxItemDto>> GetDelAggTaxTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(ClusterEnum.DelAggTaxType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(ClusterEnum.DelAggTaxType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

    }
}

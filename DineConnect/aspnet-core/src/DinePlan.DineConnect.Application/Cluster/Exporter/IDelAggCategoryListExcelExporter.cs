﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public interface IDelAggCategoryListExcelExporter
    {
        FileDto ExportToFile(List<DelAggCategoryListDto> dtos);
    }
}
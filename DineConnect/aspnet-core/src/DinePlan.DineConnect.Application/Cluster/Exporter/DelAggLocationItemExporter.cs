﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggLocationItemListExcelExporter : EpPlusExcelExporterBase, IDelAggLocationItemListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggLocationItemListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggLocationItemList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggLocationItem"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("DelAggLocMapping"),
                        L("DelAggItem"),
                        L("DelAggVariant"),
                        L("DelAggModifier"),
                        L("Price"),
                        L("DelPriceType")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.DelAggLocMappingName,
                        _ => _.DelAggItemName,
                        _ => _.DelAggVariantName,
                        _ => _.DelAggModifierName,
                        _ => _.Price,
                        _ => _.DelAggPriceTypeRefName
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggLocationItemListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}
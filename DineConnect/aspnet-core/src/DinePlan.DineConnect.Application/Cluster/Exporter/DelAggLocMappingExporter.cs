﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggLocMappingListExcelExporter : EpPlusExcelExporterBase, IDelAggLocMappingListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggLocMappingListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggLocMappingList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggLocMapping"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Location"),
                        L("DelAggType"),
                        L("Name"),
                        L("GatewayCode"),
                        L("RemoteCode"),
                        L("Delivery") + L("Url")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.DelAggLocationName,
                        _ => _.DelAggTypeRefName,
                        _ => _.Name,
                        _ => _.GatewayCode,
                        _ => _.RemoteCode,
                        _ => _.DeliveryUrl
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggLocMappingListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}

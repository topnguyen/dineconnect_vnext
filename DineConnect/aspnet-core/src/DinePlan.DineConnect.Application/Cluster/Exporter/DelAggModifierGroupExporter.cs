﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggModifierGroupListExcelExporter : EpPlusExcelExporterBase, IDelAggModifierGroupListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggModifierGroupListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggModifierGroupList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggModifierGroup"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Minimum"),
                        L("Maximum"),
                        L("OrderTagGroup")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.Min,
                        _ => _.Max,
                        _ => _.OrderTagGroupName
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggModifierGroupListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}


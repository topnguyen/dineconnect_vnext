﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggTaxListExcelExporter : EpPlusExcelExporterBase, IDelAggTaxListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggTaxListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggTaxList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggTax"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("TaxType"),
                        L("Percentage"),
                        L("LocalRefCode")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                         _ => _.Id,
                        _ => _.Name,
                        _=>_.TaxTypeName,
                        _ => _.TaxPercentage,
                        _ => _.LocalRefCode
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggTaxListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}


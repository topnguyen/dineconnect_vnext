﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggChargeListExcelExporter : EpPlusExcelExporterBase, IDelAggChargeListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggChargeListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggChargeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggCharge"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _=>_.Name
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggChargeListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}

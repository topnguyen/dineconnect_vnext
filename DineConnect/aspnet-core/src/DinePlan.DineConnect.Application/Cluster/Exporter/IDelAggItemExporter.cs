﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public interface IDelAggItemListExcelExporter
    {
        FileDto ExportToFile(List<DelAggItemListDto> dtos);
    }
}

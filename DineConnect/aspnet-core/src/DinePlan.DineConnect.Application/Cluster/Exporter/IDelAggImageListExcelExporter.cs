﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public interface IDelAggImageListExcelExporter
    {
        FileDto ExportToFile(List<DelAggImageListDto> dtos);
    }
}
﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggVariantListExcelExporter : EpPlusExcelExporterBase, IDelAggVariantListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggVariantListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggVariantList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggVariant"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Code"),
                        L("DelAggVariantGroup"),
                        L("Portion"),
                        L("SalesPrice"),
                        L("MarkupPrice")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _=>_.Code,
                        _=>_.DelAggVariantGroupName,
                        _=>_.MenuItemPortionName,
                        _=>_.SalesPrice,
                        _ => _.MarkupPrice

                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggVariantListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}


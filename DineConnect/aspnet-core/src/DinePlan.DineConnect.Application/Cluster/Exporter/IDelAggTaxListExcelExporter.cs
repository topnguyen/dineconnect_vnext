﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public interface IDelAggTaxListExcelExporter
    {
        FileDto ExportToFile(List<DelAggTaxListDto> dtos);
    }
}

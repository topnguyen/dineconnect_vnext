﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggModifierListExcelExporter : EpPlusExcelExporterBase, IDelAggModifierListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggModifierListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggModifierList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggModifier"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Code"),
                        L("DelAggModifierGroup"),
                        L("OrderTag"),
                        L("LocalRefCode"),
                        L("Price")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _=>_.Code,
                        _=>_.DelAggModifierGroupName,
                        _=>_.OrderTagName,
                        _=>_.LocalRefCode,
                        _=>_.Price

                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public DelAggModifierListExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }
    }
}


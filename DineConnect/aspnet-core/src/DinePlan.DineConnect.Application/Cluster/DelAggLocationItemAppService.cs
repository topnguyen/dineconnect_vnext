﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.AddonSettings.Grab;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggLocationItemAppService : DineConnectAppServiceBase, IDelAggLocationItemAppService
    {
        private readonly IDelAggLocationItemListExcelExporter _delagglocationitemExporter;
        private readonly IRepository<DelAggLocationItem> _delagglocationitemRepo;
        private readonly IDelAggLocMappingAppService _delAggLocMappingService;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        private readonly IRepository<DelAggLocMapping> _delagglocmappingRepo;
        private readonly IRepository<DelAggVariant> _delaggvariantRepo;
        private readonly IRepository<DelAggModifier> _delaggmodifierRepo;
        private readonly IDelAggLocationItemManager _delagglocationitemManager;
        private readonly IGrabAppService _grabService;

        public DelAggLocationItemAppService(IRepository<DelAggLocationItem> delagglocationitemRepo,
            IDelAggLocationItemListExcelExporter delagglocationitemExporter, IDelAggLocMappingAppService delAggLocMappingService,
            IRepository<DelAggItem> delaggitemRepo, IRepository<DelAggLocMapping> delagglocmappingRepo,
            IRepository<DelAggVariant> delaggvariantRepo, IRepository<DelAggModifier> delaggmodifierRepo,
            IDelAggLocationItemManager delagglocationitemManager, IGrabAppService grabAppService)
        {
            _delagglocationitemRepo = delagglocationitemRepo;
            _delagglocationitemExporter = delagglocationitemExporter;
            _delAggLocMappingService = delAggLocMappingService;
            _delaggitemRepo = delaggitemRepo;
            _delagglocmappingRepo = delagglocmappingRepo;
            _delaggvariantRepo = delaggvariantRepo;
            _delaggmodifierRepo = delaggmodifierRepo;
            _delagglocationitemManager = delagglocationitemManager;
            _grabService = grabAppService;
        }

        public async Task<PagedResultDto<DelAggLocationItemListDto>> GetAll(GetDelAggLocationItemInput input)
        {
            var aggItem = _delaggitemRepo.GetAll();
            var agglocMap = _delagglocmappingRepo.GetAll();
            var aggvariant = _delaggvariantRepo.GetAll();
            var aggModifier = _delaggmodifierRepo.GetAll();

            var delAggLocationItems = _delagglocationitemRepo.GetAll();

            if (input.DelAggItemId.HasValue)
            {
                delAggLocationItems = delAggLocationItems.Where(t => t.DelAggItemId == input.DelAggItemId);
            }
            if (input.DelAggLocMappingId.HasValue)
            {
                delAggLocationItems = delAggLocationItems.Where(t => t.DelAggLocMappingId == input.DelAggLocMappingId);
            }
            if (input.DelAggVariantId.HasValue)
            {
                delAggLocationItems = delAggLocationItems.Where(t => t.DelAggVariantId == input.DelAggVariantId);
            }
            if (input.DelAggModifierId.HasValue)
            {
                delAggLocationItems = delAggLocationItems.Where(t => t.DelAggModifierId == input.DelAggModifierId);
            }
            if (input.DelAggPriceTypeRefId.HasValue)
            {
                delAggLocationItems = delAggLocationItems.Where(t => (int) t.DelAggPriceTypeRefId == input.DelAggPriceTypeRefId.Value);
            }
            if (input.InActive.HasValue && input.InActive.Value)
            {
                delAggLocationItems = delAggLocationItems.Where(t => t.InActive);
            }

            var allItems = (from locitem in delAggLocationItems
                            select new DelAggLocationItemListDto()
                            {
                                Id=locitem.Id,
                                DelAggItemId=locitem.DelAggItemId.Value,
                                DelAggLocMappingId=locitem.DelAggLocMappingId,
                                DelAggVariantId=locitem.DelAggVariantId.Value,
                                DelAggModifierId=locitem.DelAggModifierId.Value,
                                DelAggPriceTypeRefId=(int)locitem.DelAggPriceTypeRefId,
                                Price=locitem.Price,
                                InActive = locitem.InActive,
                                AddOns=locitem.AddOns,
                                CreationTime=locitem.CreationTime
                            }).WhereIf(
                              !input.Filter.IsNullOrEmpty(),
                              p => p.Id.Equals(input.Filter)
                          );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLocationItemListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.DelAggItemId != null)
                {
                    var lstaggitem = aggItem.FirstOrDefault(t => t.Id == lst.DelAggItemId);
                    if (lstaggitem != null)
                        lst.DelAggItemName = lstaggitem.Name;
                }
                if (lst.DelAggLocMappingId != null)
                {
                    var lstagglocmap = agglocMap.FirstOrDefault(t => t.Id == lst.DelAggLocMappingId);
                    if (lstagglocmap != null)
                        lst.DelAggLocMappingName = lstagglocmap.Name;
                }
                if (lst.DelAggModifierId != null)
                {
                    var lstModifier = aggModifier.FirstOrDefault(t => t.Id == lst.DelAggModifierId);
                    if (lstModifier != null)
                        lst.DelAggModifierName = lstModifier.Name;
                }
                if (lst.DelAggVariantId != null)
                {
                    var lstVariant = aggvariant.FirstOrDefault(t => t.Id == lst.DelAggVariantId);
                    if (lstVariant != null)
                        lst.DelAggVariantName = lstVariant.Name;
                }

            }
            var delPriceTypeList = await _delAggLocMappingService.GetDelPriceTypeForCombobox();
            var delPriceTypeDtos = delPriceTypeList.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t => t.DelAggPriceTypeRefId))
            {
                var dat = delPriceTypeDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (dat != null)
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.DelAggPriceTypeRefName = dat.DisplayText;
                    }
                }
            }

            return new PagedResultDto<DelAggLocationItemListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggLocationItemInput input)
        {
            input.MaxResultCount = 1000;
            input.SkipCount = 0;

            var allList = await GetAll(input);

            var allListDtos = allList.Items.MapTo<List<DelAggLocationItemListDto>>();
            return _delagglocationitemExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggLocationItemForEditOutput> GetDelAggLocationItemForEdit(EntityDto input)
        {
            DelAggLocationItemEditDto editDto;

            var hDto = await _delagglocationitemRepo.GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.DelAggItem)
                .FirstOrDefaultAsync();
            
            editDto = hDto.MapTo<DelAggLocationItemEditDto>();

            return new GetDelAggLocationItemForEditOutput
            {
                DelAggLocationItem = editDto
            };
        }

        public async Task CreateOrUpdateDelAggLocationItem(CreateOrUpdateDelAggLocationItemInput input)
        {
            if (input.DelAggLocationItem.Id.HasValue)
            {
                await UpdateDelAggLocationItem(input);
            }
            else
            {
                await CreateDelAggLocationItem(input);
            }

            if (input.DelAggLocationItem.DelAggLocMappingId > 0 && input.DelAggLocationItem.DelAggItemId!=null && input.DelAggLocationItem.DelAggItemId>0)
            {
                var myLoMapp = await  _delagglocmappingRepo.GetAsync(input.DelAggLocationItem.DelAggLocMappingId);
                var myIUtem =await _delaggitemRepo.GetAsync(input.DelAggLocationItem.DelAggItemId.Value);
                // var myGrab = await _grabService.ChangeGrabItem(new GrabChangeTenantInput()
                // {
                //     MerchantId = myLoMapp.RemoteCode,
                //     Id = myIUtem.Code,
                //     InActive = input.DelAggLocationItem.InActive,
                //     Price = (int)input.DelAggLocationItem.Price * 100
                // });
            }
        }

        public async Task DeleteDelAggLocationItem(EntityDto input)
        {
            await _delagglocationitemRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggLocationItem(CreateOrUpdateDelAggLocationItemInput input)
        {
            var item = await _delagglocationitemRepo.GetAsync(input.DelAggLocationItem.Id.Value);
            var dto = input.DelAggLocationItem;
            item.AddOns = dto.AddOns;
            item.DelAggItemId = dto.DelAggItemId;
            item.DelAggLocMappingId = dto.DelAggLocMappingId;
            item.DelAggModifierId = dto.DelAggModifierId;
            item.DelAggPriceTypeRefId = dto.DelAggPriceTypeRefId;
            item.DelAggVariantId = dto.DelAggVariantId;
            item.InActive = dto.InActive;
            item.Price = dto.Price;
            CheckErrors(await _delagglocationitemManager.UpdateSync(item));
        }

        protected virtual async Task CreateDelAggLocationItem(CreateOrUpdateDelAggLocationItemInput input)
        {
            var dto = input.DelAggLocationItem.MapTo<DelAggLocationItem>();
            CheckErrors(await _delagglocationitemManager.CreateSync(dto));
        }
    }
}

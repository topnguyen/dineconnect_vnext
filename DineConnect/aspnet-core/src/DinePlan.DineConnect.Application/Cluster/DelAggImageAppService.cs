﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggImageAppService : DineConnectAppServiceBase, IDelAggImageAppService
    {
        private readonly IDelAggImageListExcelExporter _delaggimageExporter;
        private readonly IRepository<DelAggImage> _delaggimageRepo;
        private readonly IDelAggLocMappingAppService _delagglocmappingAppService;
        private readonly IDelAggImageManager _delaggimageManager;

        public DelAggImageAppService(IRepository<DelAggImage> delaggimageRepo,
            IDelAggLocMappingAppService delagglocmappingAppService,
            IDelAggImageManager delaggimageManager,
            IDelAggImageListExcelExporter delaggimageExporter)
        {
            _delaggimageRepo = delaggimageRepo;
            _delagglocmappingAppService = delagglocmappingAppService;
            _delaggimageManager = delaggimageManager;
            _delaggimageExporter = delaggimageExporter;
        }

        public async Task<PagedResultDto<DelAggImageListDto>> GetAll(GetDelAggImageInput input)
        {
            var delaggimage = _delaggimageRepo.GetAll();
            if (input.DelAggImageTypeRefId.HasValue)
            {
                delaggimage = delaggimage.Where(t => t.DelAggImageTypeRefId == input.DelAggImageTypeRefId);
            }
            if (input.DelAggTypeRefId.HasValue)
            {
                delaggimage = delaggimage.Where(t => t.DelAggTypeRefId == input.DelAggTypeRefId);
            }

            var allItems = (from image in delaggimage 
                            select new DelAggImageListDto()
                            {
                                Id = image.Id,
                                DelAggImageTypeRefId=image.DelAggImageTypeRefId,
                                DelAggTypeRefId = image.DelAggTypeRefId,
                                ReferenceId=image.ReferenceId,
                                ImagePath=image.ImagePath,
                                AddOns = image.AddOns,
                                CreationTime = image.CreationTime
                            }).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Id.Equals(input.Filter)
                            );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggImageListDto>>();

            var allItemCount = await allItems.CountAsync();

            var delAggTypeList = await _delagglocmappingAppService.GetDelAggTypeForCombobox();
            var delAggTypeDtos = delAggTypeList.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t => t.DelAggTypeRefId))
            {
                var dat = delAggTypeDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (dat != null)
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.DelAggTypeRefName = dat.DisplayText;
                    }
                }
            }

            var delAggImageTypeList = await _delagglocmappingAppService.GetDelAggImageTypeForCombobox();
            var delAggImageTypeDtos = delAggImageTypeList.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t => t.DelAggImageTypeRefId))
            {
                var dat = delAggImageTypeDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (dat != null)
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.DelAggImageTypeRefName = dat.DisplayText;
                    }
                }
            }

            return new PagedResultDto<DelAggImageListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggImageInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggImageListDto>>();
            return _delaggimageExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggImageForEditOutput> GetDelAggImageForEdit(EntityDto input)
        {
            DelAggImageEditDto editDto;

            var hDto = await _delaggimageRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<DelAggImageEditDto>();

            return new GetDelAggImageForEditOutput
            {
                DelAggImage = editDto
            };
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDelAggImageInput input)
        {

            int delAggImageTypeId = input.DelAggImage.DelAggImageTypeRefId;
            int delAggTypeId = input.DelAggImage.DelAggTypeRefId.Value;

            if (delAggImageTypeId == 0)
            {
                throw new UserFriendlyException(L("ImageTypeErr"));
            }
            if (delAggTypeId == 0)
            {
                throw new UserFriendlyException(L("AggTypeErr"));
            }

            if (input.DelAggImage.Id.HasValue)
            {
                var delAggImageExists = await _delaggimageRepo.FirstOrDefaultAsync(t => t.DelAggImageTypeRefId == delAggImageTypeId && t.DelAggTypeRefId == delAggTypeId);
                if (delAggImageExists != null)
                {
                    if (delAggImageExists.Id != input.DelAggImage.Id)
                    {
                        throw new UserFriendlyException(L("SameDataAlreadyExists"));
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            else
            {
                var delAggImageExists = await _delaggimageRepo.FirstOrDefaultAsync(t => t.DelAggImageTypeRefId == delAggImageTypeId && t.DelAggTypeRefId == delAggTypeId);
                if (delAggImageExists != null)
                {
                    throw new UserFriendlyException(L("SameDataAlreadyExists"));
                }
                else
                {

                }
            }
            return true;
        }
        public async Task CreateOrUpdateDelAggImage(CreateOrUpdateDelAggImageInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result == true)
            {
                    if (input.DelAggImage.Id.HasValue)
                    {
                        await UpdateDelAggImage(input);
                    }
                    else
                    {
                        await CreateDelAggImage(input);
                    }
            }
            else
            {

            }
        }

        public async Task DeleteDelAggImage(EntityDto input)
        {
            await _delaggimageRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggImage(CreateOrUpdateDelAggImageInput input)
        {
            var item = await _delaggimageRepo.GetAsync(input.DelAggImage.Id.Value);
            var dto = input.DelAggImage;
            //TODO: SERVICE DelAggImage Update Individually
            item.DelAggImageTypeRefId = dto.DelAggImageTypeRefId;
            item.DelAggTypeRefId = dto.DelAggTypeRefId;
            item.ImagePath = dto.ImagePath;
            item.AddOns = dto.AddOns;
            item.ReferenceId = dto.ReferenceId;

            CheckErrors(await _delaggimageManager.CreateSync(item));
        }

        protected virtual async Task CreateDelAggImage(CreateOrUpdateDelAggImageInput input)
        {
            var dto = input.DelAggImage.MapTo<DelAggImage>();
            CheckErrors(await _delaggimageManager.CreateSync(dto));
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggVariantAppService : DineConnectAppServiceBase, IDelAggVariantAppService
    {
        private readonly IDelAggVariantListExcelExporter _delaggvariantExporter;
        private readonly IRepository<DelAggVariant> _delaggvariantRepo;
        private readonly IDelAggVariantManager _delAggVariantManager;
        private readonly IRepository<DelAggVariantGroup> _delaggvariantgroupRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<DelAggImage> _delaggimageRepo;

        public DelAggVariantAppService(IRepository<DelAggVariant> delaggvariantRepo,
            IDelAggVariantListExcelExporter delaggvariantExporter,
            IDelAggVariantManager delAggVariantManager,
            IRepository<DelAggVariantGroup> delaggvariantgroupRepo,
            IRepository<MenuItemPortion> menuitemportionRepo, IRepository<DelAggImage> delaggimageRepo)
        {
            _delaggvariantRepo = delaggvariantRepo;
            _delaggvariantExporter = delaggvariantExporter;
            _delAggVariantManager = delAggVariantManager;
            _delaggvariantgroupRepo = delaggvariantgroupRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _delaggimageRepo = delaggimageRepo;
        }

        public async Task<PagedResultDto<DelAggVariantListDto>> GetAll(GetDelAggVariantInput input)
        {
            var rsDelAggVariant = _delaggvariantRepo.GetAll();

            var rsDelAggVarGroup = _delaggvariantgroupRepo.GetAll();
            if (input.DelAggVariantGroupId.HasValue)
            {
                rsDelAggVariant = rsDelAggVariant.Where(t => t.DelAggVariantGroupId == input.DelAggVariantGroupId.Value);
            }

            var rsMenuItemPortion = _menuitemportionRepo
            .GetAll()
            .Include(x => x.MenuItem);
            
            if (input.MenuItemPortionId.HasValue && input.MenuItemPortionId.Value > 0)
            {
                rsDelAggVariant = rsDelAggVariant.Where(t => t.MenuItemPortionId == input.MenuItemPortionId.Value);
            }

            var allItems = (from v in rsDelAggVariant
                            join vg in rsDelAggVarGroup
                            on v.DelAggVariantGroupId equals vg.Id
                            join mip in rsMenuItemPortion
                            on v.MenuItemPortionId equals mip.Id
                            select new DelAggVariantListDto()
                            {
                                Id=v.Id,
                                Name=v.Name,
                                Code=v.Code,
                                DelAggVariantGroupId=v.DelAggVariantGroupId,
                                MenuItemPortionId=v.MenuItemPortionId,
                                SalesPrice=v.SalesPrice,
                                MarkupPrice = v.MarkupPrice,
                                SortOrder=v.SortOrder,
                                LocalRefCode=v.LocalRefCode
                            }).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter)
                            );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggVariantListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.DelAggVariantGroupId != null)
                {
                    var lstDelAggGrp = rsDelAggVarGroup.FirstOrDefault(t => t.Id == lst.DelAggVariantGroupId);
                    if (lstDelAggGrp != null)
                        lst.DelAggVariantGroupName = lstDelAggGrp.Name;
                }
                if (lst.MenuItemPortionId != null)
                {
                    var lstMenuItemPortion = rsMenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                    if (lstMenuItemPortion != null)
                        lst.MenuItemPortionName = lstMenuItemPortion.Id + " - " + lstMenuItemPortion.MenuItem.Name;
                    //lst.MenuItemPortionName = lstMenuItemPortion.Name;

                }
            }

            return new PagedResultDto<DelAggVariantListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggVariantInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggVariantListDto>>();
            return _delaggvariantExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggVariantForEditOutput> GetDelAggVariantForEdit(EntityDto input)
        {
            DelAggVariantEditDto editDto; 
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();

            var hDto = await _delaggvariantRepo.GetAsync(input.Id);

            var rsMenuItemPortion = _menuitemportionRepo.GetAll().Include(x => x.MenuItem);
            var lstMenuItemPortion = rsMenuItemPortion.FirstOrDefault(t => t.Id == hDto.MenuItemPortionId);

            editDto = hDto.MapTo<DelAggVariantEditDto>();
            if (lstMenuItemPortion != null)
                editDto.MenuItemPortionName = lstMenuItemPortion.MenuItem.Name;
            #region Image Edit

            var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.Variant);

            if (imageItem != null && imageItem.Count > 0)
                editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
            else
                editImageDto = new List<DelAggImageEditDto>();

            #endregion

            return new GetDelAggVariantForEditOutput
            {
                DelAggVariant = editDto,
                DelAggImage = editImageDto
            };
        }

        public async Task CreateOrUpdateDelAggVariant(CreateOrUpdateDelAggVariantInput input)
        {
            if (input.DelAggVariant.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _delaggvariantRepo.FirstOrDefault(p => p.Name == input.DelAggVariant.Name && p.Id != input.DelAggVariant.Id);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await UpdateDelAggVariant(input);
            }
            else
            {
                var alreadyExist = _delaggvariantRepo.FirstOrDefault(p => p.Name == input.DelAggVariant.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await CreateDelAggVariant(input);
            }
        }

        public async Task DeleteDelAggVariant(EntityDto input)
        {
            //var locRefCount = await _delagglocationRepo.CountAsync(t => t.DelAggVariantId.Equals(input.Id));
            //if( locRefCount == 0)
            //{  
                await _delaggvariantRepo.DeleteAsync(input.Id);
            //}
            //else
            //{
            //    throw new UserFriendlyException( L("ReferenceExists", L("DelAggVariant") ,L("DelAggLocation")));
            //}
            
        }

        protected virtual async Task UpdateDelAggVariant(CreateOrUpdateDelAggVariantInput input)
        {
            var item = await _delaggvariantRepo.GetAsync(input.DelAggVariant.Id.Value);
            var dto = input.DelAggVariant;
            //TODO: SERVICE DelAggVariant Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.SortOrder = dto.SortOrder;
            item.LocalRefCode = dto.LocalRefCode;
            item.DelAggVariantGroupId = dto.DelAggVariantGroupId;
            item.MenuItemPortionId = dto.MenuItemPortionId;
            item.NestedModGroupId = dto.NestedModGroupId;
            item.SalesPrice = dto.SalesPrice;
            item.MarkupPrice = dto.MarkupPrice;

            CheckErrors(await _delAggVariantManager.CreateSync(item));
            #region DelAggImage Creation

            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.Variant &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)ClusterEnum.DelAggImageType.Variant,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id

                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }
            }

            var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
            foreach (var imgToBeDelete in delImagesToBe)
            {
                await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
            }

            #endregion

        }

        protected virtual async Task CreateDelAggVariant(CreateOrUpdateDelAggVariantInput input)
        {
            var dto = input.DelAggVariant.MapTo<DelAggVariant>();
            CheckErrors(await _delAggVariantManager.CreateSync(dto));
        }
        public async Task<ListResultDto<DelAggVariantListDto>> GetNames()
        {
            var lstDelAggVariant = await _delaggvariantRepo.GetAll().ToListAsync();
            return new ListResultDto<DelAggVariantListDto>(lstDelAggVariant.MapTo<List<DelAggVariantListDto>>());
        }
    }
}


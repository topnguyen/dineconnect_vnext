﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggVariantGroupAppService : DineConnectAppServiceBase, IDelAggVariantGroupAppService
    {
        private readonly IDelAggVariantGroupListExcelExporter _delaggvariantgroupExporter;
        private readonly IRepository<DelAggVariantGroup> _delaggvariantgroupRepo;
        private readonly IRepository<DelAggVariant> _delaggvariantRepo;
        private readonly IDelAggVariantGroupManager _delAggVariantGroupManager;
        private readonly IRepository<DelAggImage> _delaggimageRepo;

        public DelAggVariantGroupAppService(IRepository<DelAggVariantGroup> delaggvariantgroupRepo,
            IRepository<DelAggVariant> delaggvariantRepo,
            IDelAggVariantGroupListExcelExporter delaggvariantgroupExporter,
            IDelAggVariantGroupManager delAggVariantGroupManager, IRepository<DelAggImage> delaggimageRepo)
        {
            _delaggvariantgroupRepo = delaggvariantgroupRepo;
            _delaggvariantRepo = delaggvariantRepo;
            _delaggvariantgroupExporter = delaggvariantgroupExporter;
            _delAggVariantGroupManager = delAggVariantGroupManager;
            _delaggimageRepo = delaggimageRepo;
        }

        public async Task<PagedResultDto<DelAggVariantGroupListDto>> GetAll(GetDelAggVariantGroupInput input)
        {
            var allItems = _delaggvariantgroupRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggVariantGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DelAggVariantGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _delaggvariantgroupRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DelAggVariantGroupListDto>>();
            return _delaggvariantgroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggVariantGroupForEditOutput> GetDelAggVariantGroupForEdit(EntityDto input)
        {
            DelAggVariantGroupEditDto editDto;
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();

            var hDto = await _delaggvariantgroupRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<DelAggVariantGroupEditDto>();
            #region Image Edit

            var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.VariantGroup);

            if (imageItem != null && imageItem.Count > 0)
                editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
            else
                editImageDto = new List<DelAggImageEditDto>();

            #endregion

            return new GetDelAggVariantGroupForEditOutput
            {
                DelAggVariantGroup = editDto,
                DelAggImage = editImageDto
            };
        }

        public async Task CreateOrUpdateDelAggVariantGroup(CreateOrUpdateDelAggVariantGroupInput input)
        {
            if (input.DelAggVariantGroup.Id.HasValue)
            {
                await UpdateDelAggVariantGroup(input);
            }
            else
            {
                await CreateDelAggVariantGroup(input);
            }
        }

        public async Task DeleteDelAggVariantGroup(EntityDto input)
        {
            var locRefCount = await _delaggvariantRepo.CountAsync(t => t.DelAggVariantGroupId.Equals(input.Id));
            if (locRefCount == 0)
            {
                await _delaggvariantgroupRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DelAggVariantGroup"), L("DelAggVariant")));
            }

        }

        protected virtual async Task UpdateDelAggVariantGroup(CreateOrUpdateDelAggVariantGroupInput input)
        {
            var item = await _delaggvariantgroupRepo.GetAsync(input.DelAggVariantGroup.Id.Value);
            var dto = input.DelAggVariantGroup;
            //TODO: SERVICE DelAggVariantGroup Update Individually
            item.Name = dto.Name;
            item.SortOrder = dto.SortOrder;
            item.LocalRefCode = dto.LocalRefCode;
            item.Code = dto.Code;

            CheckErrors(await _delAggVariantGroupManager.CreateSync(item));

            #region DelAggImage Creation

            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.VariantGroup &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)ClusterEnum.DelAggImageType.VariantGroup,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id

                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }

                var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
                foreach (var imgToBeDelete in delImagesToBe)
                {
                    await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
                }

            }

            #endregion

        }

        protected virtual async Task<int> CreateDelAggVariantGroup(CreateOrUpdateDelAggVariantGroupInput input)
        {
            var dto = input.DelAggVariantGroup.MapTo<DelAggVariantGroup>();
  
            int retId = await _delaggvariantgroupRepo.InsertAndGetIdAsync(dto);

            CheckErrors(await _delAggVariantGroupManager.CreateSync(dto));

            return dto.Id;
        }
        public async Task<ListResultDto<DelAggVariantGroupListDto>> GetNames()
        {
            var lstDelAggVariantGroup = await _delaggvariantgroupRepo.GetAll().ToListAsync();
            return new ListResultDto<DelAggVariantGroupListDto>(lstDelAggVariantGroup.MapTo<List<DelAggVariantGroupListDto>>());
        }
    }
}


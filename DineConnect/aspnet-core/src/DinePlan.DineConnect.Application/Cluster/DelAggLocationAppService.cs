﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Exporter;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public class DelAggLocationAppService : DineConnectAppServiceBase, IDelAggLocationAppService
    {
        private readonly IDelAggLocationListExcelExporter _delagglocationExporter;
        private readonly IRepository<DelAggLocation> _delagglocationRepo;
        private readonly IRepository<DelAggLocationGroup> _delagglocationgroupRepo;
        private readonly IDelAggLocationManager _delAggLocationManager;
        private readonly IRepository<Location> _locationRepo;
        private readonly IDelAggApiAppService _delAggApiAppService;
        private readonly IRepository<DelAggImage> _delaggimageRepo;

        public DelAggLocationAppService(IRepository<DelAggLocation> delagglocationRepo, 
            IRepository<DelAggLocationGroup> delagglocationgroupRepo,
            IDelAggLocationListExcelExporter delagglocationExporter, 
            IDelAggLocationManager delAggLocationManager,
            IRepository<Location> locationRepo,
            IDelAggApiAppService delAggApiAppService,
            IRepository<DelAggImage> delaggimageRepo)
        {
            _delagglocationRepo = delagglocationRepo;
            _delagglocationgroupRepo = delagglocationgroupRepo;
            _delagglocationExporter = delagglocationExporter;
            _delAggLocationManager = delAggLocationManager;
            _locationRepo = locationRepo;
            _delAggApiAppService = delAggApiAppService;
            _delaggimageRepo = delaggimageRepo;
        }

        public async Task<PagedResultDto<DelAggLocationListDto>> GetAll(GetDelAggLocationInput input)
        {
            var allLocations = _delagglocationRepo.GetAll();
            var rsDelAggLocGroup = _delagglocationgroupRepo.GetAll();
            if (input.DelAggLocationGroupId.HasValue)
            {
                allLocations = allLocations.Where(t => t.DelAggLocationGroupId == input.DelAggLocationGroupId.Value);
            }

            var allConnectLocations = _locationRepo.GetAll();
            if (input.LocationId.HasValue)
            {
                allLocations = allLocations.Where(t => t.LocationId == input.LocationId.Value);
            }

            var allItems = (from delloc in allLocations
                            join loc in allConnectLocations
                            on delloc.LocationId equals loc.Id
                            select new DelAggLocationListDto()
                            { 
                                Id=delloc.Id,
                                Name=delloc.Name,
                                LocationId = delloc.LocationId,
                                LocationName= loc.Name,
                                DelAggLocationGroupName = delloc.DelAggLocationGroup.Name,
                                DelAggLocationGroupId=delloc.DelAggLocationGroupId,
                            })
                .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Id.Equals(input.Filter)
            );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLocationListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                if (lst.DelAggLocationGroupId!=null)
                {
                    var lstDelAggGrp = rsDelAggLocGroup.FirstOrDefault(t => t.Id == lst.DelAggLocationGroupId);
                    if (lstDelAggGrp != null)
                        lst.DelAggLocationGroupName = lstDelAggGrp.Name;
                }
            }

            return new PagedResultDto<DelAggLocationListDto>(
            allItemCount,
            allListDtos
            );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            //input.MaxResultCount = 1000;
            //input.SkipCount = 0;

            //var allList = await GetAll(input);
            //var allListDtos = allList.Items.MapTo<List<DelAggLocationListDto>>();
            //var baseE = new BaseExportObject
            //{
            //    ExportObject = allListDtos,
            //    ColumnNames = new string[] { "Id", "LocationName", "DelAggLocationGroupName" },
            //    ColumnDisplayNames = new string[] { "Id", "Name", "DelAggLocationGroupName" }
            //};
            //return _delagglocationExporter.ExportToFile(baseE, L("Location Group"));

            var allList = await _delagglocationRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DelAggLocationListDto>>();
            return _delagglocationExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggLocationForEditOutput> GetDelAggLocationForEdit(EntityDto input)
        {
            GetDelAggLocationForEditOutput output = new GetDelAggLocationForEditOutput();
            DelAggLocationEditDto editDto;
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();

            var hDto = await _delagglocationRepo
                .GetAll()
                .Where(x => x.Id == input.Id)
                .Include(x => x.Location)
                .Include(x => x.DelAggLocationGroup)
                .FirstOrDefaultAsync();
            
            editDto = hDto.MapTo<DelAggLocationEditDto>();
            #region Image Edit

            var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.Location);

            if (imageItem != null && imageItem.Count > 0)
                editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
            else
                editImageDto = new List<DelAggImageEditDto>();

            #endregion

            UpdateLocations(hDto, output.LocationGroup);

            output.DelAggLocation = editDto;
            output.DelAggImage = editImageDto;
            return output;
        }

        public async Task CreateOrUpdateDelAggLocation(CreateOrUpdateDelAggLocationInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result == true)
            {
                if (input.DelAggLocation.Id.HasValue)
                {
                    await UpdateDelAggLocation(input);
                }
                else
                {
                    await CreateDelAggLocation(input);
                }
            }
            else
            {

            }
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDelAggLocationInput input)
        {
            int locationId = input.DelAggLocation.LocationId;
            if (locationId == 0)
                throw new UserFriendlyException(L("LocationErr"));

            var locationExists = await _locationRepo.FirstOrDefaultAsync(t => t.Id == (locationId));

            if (input.DelAggLocation.Id.HasValue)
            {
                var recordExists = await _delagglocationRepo.FirstOrDefaultAsync(t => t.LocationId.Equals(locationExists.Id));
                if (recordExists != null)
                {
                    if (recordExists.Id != input.DelAggLocation.Id)
                    {
                        throw new UserFriendlyException(L("SameDataAlreadyExists") + " " + locationExists.Name);
                    }
                }
            }
            else
            {
                var recordExists = await _delagglocationRepo.FirstOrDefaultAsync(t => t.LocationId == (locationExists.Id));
                if (recordExists != null)
                {
                       throw new UserFriendlyException(L("SameDataAlreadyExists") + " " + locationExists.Name);
                }
            }
            return true;
        }

        public async Task DeleteDelAggLocation(EntityDto input)
        {
            await _delagglocationRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggLocation(CreateOrUpdateDelAggLocationInput input)
        {
            var item = await _delagglocationRepo.GetAsync(input.DelAggLocation.Id.Value);
            var dto = input.DelAggLocation;
            item.Name = dto.Name;
            item.LocationId = dto.LocationId;
            item.DelAggLocationGroupId = dto.DelAggLocationGroupId;

            if(dto.DelAggLocationGroupId==0)
                item.DelAggLocationGroupId = null;

            item.AddOns = dto.AddOns;
            CheckErrors(await _delAggLocationManager.CreateSync(item));


            #region DelAggImage Creation
            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)ClusterEnum.DelAggImageType.Location &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)ClusterEnum.DelAggImageType.Location,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id
                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }
            }

            var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
            foreach (var imgToBeDelete in delImagesToBe)
            {
                await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
            }

            #endregion

        }

        protected virtual async Task CreateDelAggLocation(CreateOrUpdateDelAggLocationInput input)
        {
            var dto = input.DelAggLocation.MapTo<DelAggLocation>();
            CheckErrors(await _delAggLocationManager.CreateSync(dto));
        }
        public async Task<ListResultDto<DelAggLocationListDto>> GetNames()
        {
            var lstDelAggLocation = await _delagglocationRepo.GetAll().ToListAsync();
            return new ListResultDto<DelAggLocationListDto>(lstDelAggLocation.MapTo<List<DelAggLocationListDto>>());
        }
    }
}

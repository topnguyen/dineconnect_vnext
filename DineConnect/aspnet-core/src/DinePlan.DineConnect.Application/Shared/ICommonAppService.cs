﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shared
{
    public interface ICommonAppService : IApplicationService
    {
        Task<List<ERPComboboxItem>> GetLookups(string type, int? tenantId = null, int? parentId = null);

        Task<string> GetCountryName(int countryId);

        Task<int> GetCountryCallCode(int countryId);

        Task<string> GetCityName(int cityId);

        Task<int?> GetCountryIdForCustomer(long customerId);
        Task<int?> GetDefaultCountryIdForCustomer();

        Task<List<ERPComboboxItem>> GetAllProductSets();

        Task<List<ERPComboboxItem>> GetAllCustomer();
        Task<List<ComboboxItemDto>> GetDays();
    }
}

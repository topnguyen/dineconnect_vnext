﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shared.Dto
{
    public class ERPComboboxItem
    {
        public long Value { get; set; }

        public string DisplayText { get; set; }
        public string DisplayCode { get; set; }

        public ERPComboboxItem()
        {
        }

        public ERPComboboxItem(long value, string displayText)
        {
            Value = value;
            DisplayText = displayText;
        }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Shared.Dto;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Tiffins;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.String;

namespace DinePlan.DineConnect.Shared
{
    public class CommonAppService : DineConnectAppServiceBase, ICommonAppService
    {
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<Customer> _tfCustomerRepository;
        private readonly IRepository<TiffinProductSet> _tfProductSet;
        private readonly IRepository<ShipmentDepot> _trackDepotRepository;
        private readonly IRepository<ShipmentDriver> _trackDriverRepository;

        public CommonAppService(IRepository<City> cityRepository,
            IRepository<Country> countryRepository,
            IRepository<Customer> tfCustomerRepository,
            IRepository<TiffinProductSet> tfProductSet,
            IRepository<ShipmentDepot> trackDepotRepository,
            IRepository<ShipmentDriver> trackDriverRepository
            )
        {
            _cityRepository = cityRepository;
            _countryRepository = countryRepository;
            _tfCustomerRepository = tfCustomerRepository;
            _tfProductSet = tfProductSet;
            _trackDepotRepository = trackDepotRepository;
            _trackDriverRepository = trackDriverRepository;
        }

        public async Task<List<ERPComboboxItem>> GetLookups(string type, int? tenantId = null, int? parentId = null)
        {
            try
            {
                var result = new List<ERPComboboxItem>();

                switch (type)
                {
                    case "Cities":
                        result = _cityRepository.GetAll()
                            .Where(x => x.TenantId == tenantId).AsEnumerable()
                            .WhereIf(parentId != null, x => x.CountryId == parentId)
                            .Select(x => new ERPComboboxItem { Value = x.Id, DisplayText = x.Name }).ToList();
                        break;

                    case "Countries":
                        result = await _countryRepository.GetAll()
                            .Where(x => x.TenantId == tenantId)
                            .Select(x => new ERPComboboxItem { Value = x.Id, DisplayText = x.Name })
                            .ToListAsync();
                        break;

                    case "ShipmentDepots":
                        result = await _trackDepotRepository.GetAll()
                            .Where(x => x.TenantId == tenantId)
                            .Select(x => new ERPComboboxItem { Value = x.Id, DisplayText = x.FullAddress })
                            .ToListAsync();
                        break;

                    case "ShipmentDrivers":
                        result = await _trackDriverRepository.GetAll()
                            .Where(x => x.TenantId == tenantId && x.Active)
                            .Select(x => new ERPComboboxItem { Value = x.Id, DisplayText = x.Driver })
                            .ToListAsync();
                        break;

                    case "ShipmentTypes":
                        result = Enum.GetValues(typeof(ShipmentType))
                              .Cast<ShipmentType>()
                              .Select(x => new ERPComboboxItem
                              {
                                  Value = (long)x,
                                  DisplayText = Enum.GetName(typeof(ShipmentType), x)
                              }).ToList();
                        break;

                    case "ShipmentTimes":
                        var times = new List<string>();
                        DateTime today = DateTime.Today;
                        DateTime tomorrow = today.AddDays(1);
                        for (var i = today; i < tomorrow; i = i.AddMinutes(30))
                        {
                            times.Add(i.ToString("HH:mm"));
                        }
                        result = times.Select(x => new ERPComboboxItem
                        {
                            Value = 0,
                            DisplayText = x
                        }).ToList();
                        break;

                    default:
                        break;
                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public async Task<string> GetCountryName(int countryId)
        {
            var result = await _countryRepository
                .FirstOrDefaultAsync(x => x.Id == countryId);
            return result != null ? result.Name : Empty;
        }

        public async Task<int> GetCountryCallCode(int countryId)
        {
            var result = await _countryRepository
                .FirstOrDefaultAsync(x => x.Id == countryId);
            if (result == null)
            {
                return 0;
            }

            return result.CallingCode;
        }

        public async Task<string> GetCityName(int cityId)
        {
            var result = await _cityRepository
                .FirstOrDefaultAsync(x => x.Id == cityId);
            return result != null ? result.Name : Empty;
        }

        public async Task<int?> GetDefaultCountryIdForCustomer()
        {
            return await GetCountryIdForCustomer(AbpSession.UserId.GetValueOrDefault());
        }

        public async Task<int?> GetCountryIdForCustomer(long customerId)
        {
            var result = await _tfCustomerRepository
                .FirstOrDefaultAsync(x => x.UserId == customerId);

            return result?.CountryId;
        }

        public async Task<List<ERPComboboxItem>> GetAllProductSets()
        {
            var query = await _tfProductSet.GetAll()
                .Select(t => new ERPComboboxItem
                {
                    Value = t.Id,
                    DisplayText = t.Name
                })
                .Distinct()
                .ToListAsync();

            var result = query.GroupBy(q => q.DisplayText)
                              .Select(g => g.First())
                              .ToList();

            return new List<ERPComboboxItem>(result);
        }

        public async Task<List<ERPComboboxItem>> GetAllCustomer()
        {
            var result = await _tfCustomerRepository.GetAll()
                .Select(e => new ERPComboboxItem(e.Id, e.Name))
                .Distinct()
                .ToListAsync();
            return new List<ERPComboboxItem>(result);
        }

        public async Task<List<ComboboxItemDto>> GetDays()
        {
            return new List<ComboboxItemDto>(new List<ComboboxItemDto>
            {
                new ComboboxItemDto{DisplayText = DayOfWeek.Monday.ToString(), Value = ((int)DayOfWeek.Monday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Tuesday.ToString(), Value = ((int)DayOfWeek.Tuesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Wednesday.ToString(), Value = ((int)DayOfWeek.Wednesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Thursday.ToString(), Value = ((int)DayOfWeek.Thursday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Friday.ToString(), Value = ((int)DayOfWeek.Friday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Saturday.ToString(), Value = ((int)DayOfWeek.Saturday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Sunday.ToString(), Value = ((int)DayOfWeek.Sunday).ToString()},
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.MultiTenancy.HostDashboard.Dto;

namespace DinePlan.DineConnect.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}
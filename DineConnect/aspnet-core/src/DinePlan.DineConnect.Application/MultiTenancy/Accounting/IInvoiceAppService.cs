﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.MultiTenancy.Accounting.Dto;

namespace DinePlan.DineConnect.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}

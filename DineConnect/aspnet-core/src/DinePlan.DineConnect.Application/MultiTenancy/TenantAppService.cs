﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Events.Bus;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Security;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.TemplateEngines;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Core.TemplateEngines;
using DinePlan.DineConnect.Editions.Dto;
using DinePlan.DineConnect.Framework;
using DinePlan.DineConnect.MultiTenancy.Dto;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using DinePlan.DineConnect.Url;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.MultiTenancy
{
    
    public class TenantAppService : DineConnectAppServiceBase, ITenantAppService
    {
        private readonly IRepository<TemplateEngine> _templateEngineRepository;
        private readonly IRepository<TenantDatabase> _tenantDatabaseRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<TransactionType> _transactionTypeRepository;

        public TenantAppService(IRepository<TenantDatabase> tenantDatabaseRepository,
            IRepository<Tenant> tenantRepository
            , IRepository<TemplateEngine> templateEngineRepository,
            IRepository<TransactionType> transactionTypeRepository,
            IRepository<PaymentType> paymentTypeRepository)
        {
            AppUrlService = NullAppUrlService.Instance;
            EventBus = NullEventBus.Instance;
            _tenantDatabaseRepository = tenantDatabaseRepository;
            _tenantRepository = tenantRepository;
            _templateEngineRepository = templateEngineRepository;
            _transactionTypeRepository = transactionTypeRepository;
            _paymentTypeRepository = paymentTypeRepository;
        }

        public IAppUrlService AppUrlService { get; set; }
        public IEventBus EventBus { get; set; }

        public async Task<PagedResultDto<TenantListDto>> GetTenants(GetTenantsInput input)
        {
            var query = TenantManager.Tenants
                .Include(t => t.Edition)
                .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                    t => t.Name.Contains(input.Filter) || t.TenancyName.Contains(input.Filter))
                .WhereIf(input.CreationDateStart.HasValue, t => t.CreationTime >= input.CreationDateStart.Value)
                .WhereIf(input.CreationDateEnd.HasValue, t => t.CreationTime <= input.CreationDateEnd.Value)
                .WhereIf(input.SubscriptionEndDateStart.HasValue,
                    t => t.SubscriptionEndDateUtc >= input.SubscriptionEndDateStart.Value.ToUniversalTime())
                .WhereIf(input.SubscriptionEndDateEnd.HasValue,
                    t => t.SubscriptionEndDateUtc <= input.SubscriptionEndDateEnd.Value.ToUniversalTime())
                .WhereIf(input.EditionIdSpecified, t => t.EditionId == input.EditionId);

            var tenantCount = await query.CountAsync();
            var tenants = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<TenantListDto>(
                tenantCount,
                ObjectMapper.Map<List<TenantListDto>>(tenants)
            );
        }

        
        [UnitOfWork(IsDisabled = true)]
        public async Task CreateTenant(CreateTenantInput input)
        {
            TenantDatabase tenantDatabase = null;
            string connectionString = null;
            if (input.TenantDatabaseId > 0)
            {
                tenantDatabase = await _tenantDatabaseRepository.GetAsync(input.TenantDatabaseId);

                connectionString = $"Server={tenantDatabase.ServerIp}," +
                                       $"{tenantDatabase.ServerPort};" +
                                       $"Initial Catalog={input.DBName};" +
                                       $"Persist Security Info={tenantDatabase.PersistSecurityInfo};" +
                                       $"User ID={tenantDatabase.UserName};Password={tenantDatabase.Password};" +
                                       $"MultipleActiveResultSets={tenantDatabase.MultipleActiveResultSets};" +
                                       $"Encrypt={tenantDatabase.Encrypt};" +
                                       $"TrustServerCertificate={tenantDatabase.TrustServerCertificate};" +
                                       $"Connection Timeout={tenantDatabase.ConnectionTimeout};";
            }
            var newTenantId = await TenantManager.CreateWithAdminUserAsync(input.TenancyName,
                input.Name,
                input.AdminPassword,
                input.AdminEmailAddress,
                connectionString,
                input.IsActive,
                input.EditionId,
                input.ShouldChangePasswordOnNextLogin,
                input.SendActivationEmail,
                input.SubscriptionEndDateUtc?.ToUniversalTime(),
                input.IsInTrialPeriod,
                AppUrlService.CreateEmailActivationUrlFormat(input.TenancyName),
                tenantDatabase?.Id ?? null
            );

            await CreateTemplateEngineForNewTenant(newTenantId);
            await CreateTransactionTypeForTenant(newTenantId);
            await CreatePaymentForTenant(newTenantId);

        }

        

        
        public async Task<TenantEditDto> GetTenantForEdit(EntityDto input)
        {
            var tenant = await _tenantRepository
                .GetAllIncluding(e => e.TenantDatabase)
                .FirstOrDefaultAsync(t=>t.Id == input.Id);

            var tenantEditDto = ObjectMapper.Map<TenantEditDto>(tenant);
            tenantEditDto.ConnectionString = SimpleStringCipher.Instance.Decrypt(tenantEditDto.ConnectionString);

            return tenantEditDto;
        }

        
        public async Task UpdateTenant(TenantEditDto input)
        {
            await TenantManager.CheckEditionAsync(input.EditionId, input.IsInTrialPeriod);

            input.ConnectionString = SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

            var tenant = await TenantManager.GetByIdAsync(input.Id);

            if (tenant.EditionId != input.EditionId)
                EventBus.Trigger(new TenantEditionChangedEventData
                {
                    TenantId = input.Id,
                    OldEditionId = tenant.EditionId,
                    NewEditionId = input.EditionId
                });

            ObjectMapper.Map(input, tenant);
            tenant.SubscriptionEndDateUtc = tenant.SubscriptionEndDateUtc?.ToUniversalTime();

            await TenantManager.UpdateAsync(tenant);

            if (!input.ConnectionString.IsNullOrEmpty() && input.TenantDatabase != null)
            {
                var tenantDatabase = await _tenantDatabaseRepository.GetAsync((int)input.TenantDatabaseId);
                ObjectMapper.Map(input.TenantDatabase, tenantDatabase);

                await _tenantDatabaseRepository.UpdateAsync(tenantDatabase);
            }
        }

        
        public async Task DeleteTenant(EntityDto input)
        {
            var tenant = await TenantManager.GetByIdAsync(input.Id);
            await TenantManager.DeleteAsync(tenant);
        }

        
        public async Task<GetTenantFeaturesEditOutput> GetTenantFeaturesForEdit(EntityDto input)
        {
            var features = FeatureManager.GetAll()
                .Where(f => f.Scope.HasFlag(FeatureScopes.Tenant));
            var featureValues = await TenantManager.GetFeatureValuesAsync(input.Id);

            return new GetTenantFeaturesEditOutput
            {
                Features = ObjectMapper.Map<List<FlatFeatureDto>>(features).OrderBy(f => f.DisplayName).ToList(),
                FeatureValues = featureValues.Select(fv => new NameValueDto(fv)).ToList()
            };
        }

        
        public async Task UpdateTenantFeatures(UpdateTenantFeaturesInput input)
        {
            await TenantManager.SetFeatureValuesAsync(input.Id,
                input.FeatureValues.Select(fv => new NameValue(fv.Name, fv.Value)).ToArray());
        }

        
        public async Task ResetTenantSpecificFeatures(EntityDto input)
        {
            await TenantManager.ResetAllFeaturesAsync(input.Id);
        }

        public async Task UnlockTenantAdmin(EntityDto input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.Id))
            {
                var tenantAdmin = await UserManager.GetAdminAsync();
                if (tenantAdmin != null) tenantAdmin.Unlock();
            }
        }

        private async Task CreateTemplateEngineForNewTenant(int tenantId)
        {
            var templates = GetInitialTemplates(tenantId);

            foreach (var item in templates) await _templateEngineRepository.InsertAsync(item);
        }

        private List<TemplateEngine> GetInitialTemplates(int tenantId)
        {
            var variables = Enum.GetNames(typeof(TemplateVariable));
            var registerVar = Enum.GetNames(typeof(RegisterVariable));
            var mealPlanVar = Enum.GetNames(typeof(BuyMealPlanVariable));
            var orderVar = Enum.GetNames(typeof(OrderVariable));
            var minQuantityVar = Enum.GetNames(typeof(MinQuantityVariable));
            var minExpiryDateVar = Enum.GetNames(typeof(MinExpiryDateVariable));
            var customerPromotionCodeVar = Enum.GetNames(typeof(CustomerPromotionCodeVariable));
            var referralEarnProductOfferVar = Enum.GetNames(typeof(ReferralEarningProductOfferVariable));
            var buyMealPlanReminderVar = Enum.GetNames(typeof(BuyMealPlanReminderVariable));
            var pickUpOrderReminderVar = Enum.GetNames(typeof(PickUpOrderReminderVariable));
            var pickUpOrderSuccessVar = Enum.GetNames(typeof(PickUpOrderSuccessVariable));

            return new List<TemplateEngine>
            {
                new TemplateEngine(tenantId, "Name of the Template", "Subject of the Template", "Customer",
                    string.Join(", ", variables)),
                new TemplateEngine(tenantId, "Welcome", "Welcome to DineConnect", "Register",
                    string.Join(", ", registerVar)),
                new TemplateEngine(tenantId, "BuyMealPlan", "Detail Buy Meal Plan", "BuyMealPlan",
                    string.Join(", ", mealPlanVar)),
                new TemplateEngine(tenantId, "Order", "Detail Order", "Order", string.Join(", ", orderVar)),
                new TemplateEngine(tenantId, "MinQuantity", "Minimum Quantity", "MinQuantity",
                    string.Join(", ", minQuantityVar)),
                new TemplateEngine(tenantId, "MinExpiryDay", "Minimum Expiry Days", "MinExpiryDay",
                    string.Join(", ", minExpiryDateVar)),
                new TemplateEngine(tenantId, "Customer Promotion Code", "Promotion Code", "CustomerPromotionCode",
                    string.Join(", ", customerPromotionCodeVar)),
                new TemplateEngine(tenantId, "Referral Earning Product Offer", "Referral Earning Product Offer",
                    "ReferralEarningProductOffer", string.Join(", ", referralEarnProductOfferVar)),
                new TemplateEngine(tenantId, "Buy Meal Plan Reminder", "Buy Meal Plan Reminder", "BuyMealPlanReminder",
                    string.Join(", ", buyMealPlanReminderVar)),
                new TemplateEngine(tenantId, "PickUp Order Reminder", "PickUp Order Reminder", "PickUpOrderReminder",
                    string.Join(", ", pickUpOrderReminderVar)),
                new TemplateEngine(tenantId, "PickUp Order Success", "PickUp Order Success", "PickUpOrderSuccess",
                    string.Join(", ", pickUpOrderSuccessVar))
            };
        }

        public async Task CreatePaymentForTenant(int tenantId)
        {
            if (await _paymentTypeRepository.CountAsync(a => a.TenantId.Equals(tenantId)) <= 0)
            {
                var paymentType = new PaymentType { Name = "CASH", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "VISA", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "MASTER", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "VOUCHER", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "MEMBER", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "CARD", TenantId = tenantId };
                await _paymentTypeRepository.InsertOrUpdateAndGetIdAsync(paymentType);
            }
        }

        private async Task CreateTransactionTypeForTenant(int tenantId)
        {
            if (await _transactionTypeRepository.CountAsync(a => a.TenantId.Equals(tenantId)) <= 0)
            {
                TransactionType transactionType = new TransactionType { Name = "PAYMENT", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "SALE", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "SERVICE CHARGE", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "DISCOUNT", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUNDING", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUND+", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUND-", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "TAX", TenantId = tenantId };
                await _transactionTypeRepository.InsertOrUpdateAndGetIdAsync(transactionType);
            }
        }
    }
}
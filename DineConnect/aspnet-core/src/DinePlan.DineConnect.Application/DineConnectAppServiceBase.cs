﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.Threading.Timers;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.MultiTenancy;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class DineConnectAppServiceBase : ApplicationService
    {
        private AbpTimer timer;

        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected DineConnectAppServiceBase()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }

        protected DineConnectAppServiceBase(AbpTimer timer)
        {
            this.timer = timer;
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual User GetCurrentUser()
        {
            return AsyncHelper.RunSync(GetCurrentUserAsync);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
            }
        }

        protected virtual Tenant GetCurrentTenant()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetById(AbpSession.GetTenantId());
            }
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected virtual void GetLocationsForEdit(ConnectOrgLocEditDto editDto, CommonLocationGroupDto output)
        {
            /*Start Group*/
            output.Group = editDto.Group;
            output.LocationTag = editDto.LocationTag;
            if (!string.IsNullOrEmpty(editDto.Locations))
            {
                if (editDto.Group)
                {
                    output.Groups =
                        JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(editDto.Locations);
                }
                else if (editDto.LocationTag)
                {
                    output.LocationTags =
                        JsonConvert.DeserializeObject<List<SimpleLocationTagDto>>(editDto.Locations);
                }
                else
                {
                    output.Locations =
                        JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.Locations);
                }
            }

            if (!string.IsNullOrEmpty(editDto.NonLocations))
            {
                output.NonLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.NonLocations);
            }
            /*End Group*/
        }

        protected virtual CommonLocationGroupDto GetLocationsFromEntity(ConnectOrgLocFullMultiTenantAuditEntity entity)
        {
            var output = new CommonLocationGroupDto();

            /*Start Group*/
            output.Group = entity.Group;
            output.LocationTag = entity.LocationTag;
            if (!string.IsNullOrEmpty(entity.Locations))
            {
                if (entity.Group)
                {
                    output.Groups =
                        JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(entity.Locations);
                }
                else if (entity.LocationTag)
                {
                    output.LocationTags =
                        JsonConvert.DeserializeObject<List<SimpleLocationTagDto>>(entity.Locations);
                }
                else
                {
                    output.Locations =
                        JsonConvert.DeserializeObject<List<SimpleLocationDto>>(entity.Locations);
                }
            }

            if (!string.IsNullOrEmpty(entity.NonLocations))
            {
                output.NonLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(entity.NonLocations);
            }
            /*End Group*/

            return output;
        }

        protected virtual void UpdateLocations(ConnectLocFullMultiTenantAuditEntity item, CommonLocationGroupDto lgroup)
        {
            var allLocations = "";
            var allNonLocation = "";

            if (lgroup != null)
            {
                if (lgroup.Group && lgroup.Groups.Any())
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Groups);
                }
                else if (!lgroup.Group && lgroup.LocationTag && lgroup.LocationTags.Any())
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.LocationTags);
                }
                else if (!lgroup.Group && !lgroup.LocationTag && lgroup.Locations.Any())
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Locations);
                }

                if (lgroup.NonLocations != null && lgroup.NonLocations.Any())
                {
                    allNonLocation = JsonConvert.SerializeObject(lgroup.NonLocations);
                }
                item.Group = lgroup.Group;
                item.LocationTag = lgroup.LocationTag;
            }

            item.Locations = allLocations;
            item.NonLocations = allNonLocation;
        }

        protected virtual IQueryable<T> FilterByLocations<T>(IQueryable<T> query, CommonLocationGroupFilterInputDto input) where T : ConnectLocFullMultiTenantAuditEntity
        {
            if (!string.IsNullOrWhiteSpace(input.LocationIds))
            {
                var locationIds = input.ParseLocationIds();
                query = query.Where(x => x.Locations != null && x.Group == input.LocationGroup && x.LocationTag == input.LocationTag);
                var locationIdPrefixes = locationIds.Select(id => $"{{\"Id\":{id},").ToList();

                if (input.NonLocation)
                    locationIdPrefixes.ForEach(prefix =>
                    {
                        query = query.Where(x => x.NonLocations.Contains(prefix));
                    });
                else
                    locationIdPrefixes.ForEach(prefix =>
                    {
                        query = query.Where(x => x.Locations.Contains(prefix));
                    });
            }

            return query;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Dependency;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Spire.Pdf.Security;
using Spire.Xls;

namespace DinePlan.DineConnect.DataExporting.Excel.NPOI
{
    public abstract class NpoiExcelExporterBase : DineConnectServiceBase, ITransientDependency
    {
        private readonly ITempFileCacheManager _tempFileCacheManager;

        protected NpoiExcelExporterBase(ITempFileCacheManager tempFileCacheManager)
        {
            _tempFileCacheManager = tempFileCacheManager;
        }

        protected FileDto CreateExcelPackage(string fileName, Action<XSSFWorkbook> creator, ExportType type = ExportType.Excel)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            if (type == ExportType.HTML)
            {
                file.FileType = MimeTypeNames.TextHtml;
            }

            var workbook = new XSSFWorkbook();

            creator(workbook);

            Save(workbook, file, type);

            return file;
        }

        protected FileDto CreateExcelPackage(string fileName)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            return file;
        }

        protected void AddHeader(ISheet sheet, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            sheet.CreateRow(0);

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i, headerTexts[i]);
            }
        }

        protected void AddHeader(ISheet sheet, int columnIndex, string headerText)
        {
            var cell = sheet.GetRow(0).CreateCell(columnIndex);
            cell.SetCellValue(headerText);
            var cellStyle = sheet.Workbook.CreateCellStyle();
            var font = sheet.Workbook.CreateFont();
            font.IsBold = true;
            font.FontHeightInPoints = 12;
            cellStyle.SetFont(font);
            cell.CellStyle = cellStyle;
        }

        protected ICellStyle GetCellStyle(XSSFWorkbook workBook, int size, bool isBold, bool isItalic)
        {
            var myFont = workBook.CreateFont();
            myFont.FontHeightInPoints = size;
            myFont.IsBold = isBold;
            myFont.IsItalic = isItalic;

            var myStyle = workBook.CreateCellStyle();
            myStyle.VerticalAlignment = VerticalAlignment.Center;
            myStyle.Alignment = HorizontalAlignment.Center;
            myStyle.SetFont(myFont);

            return myStyle;

        }
        protected void AddObjects<T>(ISheet sheet, int startRowIndex, IList<T> items, params Func<T, object>[] propertySelectors)
        {
            if (items.IsNullOrEmpty() || propertySelectors.IsNullOrEmpty())
            {
                return;
            }


            for (var i = 1; i <= items.Count; i++)
            {
                var row = sheet.CreateRow(startRowIndex + i - 2);

                for (var j = 0; j < propertySelectors.Length; j++)
                {
                    var cell = row.CreateCell(j);
                    var value = propertySelectors[j](items[i - 1]);
                    if (value != null)
                    {
                        switch (value.GetType().Name)
                        {
                            case "String":
                            case "DateTime":
                                cell.SetCellValue(value.ToString());
                                break;
                            case "Boolean":
                                cell.SetCellValue((bool)value);
                                break;
                            case "Double":
                            case "Int32":
                                cell.SetCellValue(double.Parse(value.ToString()));
                                break;
                            default:
                                cell.SetCellValue(value.ToString());
                                break;
                        }
                    }
                }
            }
        }

        protected void AddRow(XSSFWorkbook workBook, ISheet sheet, int rowIndex, params object[] values)
        {
            if (values.IsNullOrEmpty())
            {
                return;
            }

            var row = sheet.CreateRow(rowIndex);

            for (var j = 0; j < values.Length; j++)
            {
                var cell = row.CreateCell(j);
                var value = values[j];
                if (value != null)
                {
                    switch (value.GetType().Name)
                    {
                        case "String":
                        case "DateTime":
                            cell.SetCellValue(value.ToString());
                            break;
                        case "Boolean":
                            cell.SetCellValue((bool)value);
                            break;
                        case "Double":
                        case "Int32":
                        case "Decimal":

                            ICellStyle middleSty = workBook.CreateCellStyle();
                            middleSty.Alignment = HorizontalAlignment.Right;
                            middleSty.VerticalAlignment = VerticalAlignment.Center;
                            middleSty.WrapText = true; //wrap the text in the cell
                            cell.CellStyle = middleSty;
                            cell.SetCellValue(value.ToString());
                            break;
                        default:
                            cell.SetCellValue(value.ToString());
                            break;
                    }
                }
            }
        }

        protected void Save(XSSFWorkbook excelPackage, FileDto file, ExportType type = ExportType.Excel)
        {
            var stream = new MemoryStream();

            excelPackage.Write(stream, true);

            if (type == ExportType.HTML)
            {
                // Convert Excel stream to HTML

                var spireExcelWorkbook = new Workbook();

                spireExcelWorkbook.LoadFromStream(stream);

                var spireExcelSheet = spireExcelWorkbook.Worksheets.FirstOrDefault();

                var htmlSteam = new MemoryStream();

                spireExcelSheet?.SaveToHtml(htmlSteam);

                _tempFileCacheManager.SetFile(file.FileToken, htmlSteam.ToArray());
            }
            else
            {
                _tempFileCacheManager.SetFile(file.FileToken, stream.ToArray());
            }
        }

        protected void SetCellDataFormat(ICell cell, string dataFormat)
        {
            if (cell == null)
            {
                return;
            }

            var dateStyle = cell.Sheet.Workbook.CreateCellStyle();
            var format = cell.Sheet.Workbook.CreateDataFormat();
            dateStyle.DataFormat = format.GetFormat(dataFormat);
            cell.CellStyle = dateStyle;
            if (DateTime.TryParse(cell.StringCellValue, out var datetime))
            {
                cell.SetCellValue(datetime);
            }
        }
    }
}

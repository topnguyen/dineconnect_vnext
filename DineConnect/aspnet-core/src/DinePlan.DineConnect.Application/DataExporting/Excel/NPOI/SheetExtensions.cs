﻿using System;
using System.Collections.Generic;
using System.Text;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.DataExporting.Excel.NPOI
{
    public static class SheetExtensions
    {
        public static IRow GetSheetRow(this ISheet sheet, int rowCount)
        {
            if (sheet.GetRow(rowCount) != null)
            {
                return sheet.GetRow(rowCount);
            }

            IRow row = sheet.CreateRow(rowCount);
            return row;
        }

        public static ICell GetSheetCell(this IRow sheet, int colCount, ICellStyle myStyle = null)
        {
            if (sheet.GetCell(colCount) != null)
            {
                return sheet.GetCell(colCount);
            }
            ICell myCell = sheet.CreateCell(colCount);
            if (myStyle != null)
            {
                myCell.CellStyle = myStyle;
            }
            return myCell;
        }
    }
}

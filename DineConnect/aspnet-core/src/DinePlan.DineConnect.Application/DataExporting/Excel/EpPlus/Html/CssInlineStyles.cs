﻿using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.EppPlusHtml.Html
{
    public class CssInlineStyles : CssDeclaration, RenderElement
    {
        public void Render(StringBuilder html)
        {
            html.Append("style=\"");
            html.Append
            (
                string.Join("", this
                    .Where(x => x.Value != null)
                    .Select(x => x.Key + ":" + x.Value + ";"))
            );
            html.Append('\"');
        }
    }
}

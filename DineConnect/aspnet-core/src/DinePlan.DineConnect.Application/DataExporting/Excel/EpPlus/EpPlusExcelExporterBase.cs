﻿//*https://github.com/EPPlusSoftware/EPPlus
// *From version 5 EPPlus changes the licence model using a dual license, Polyform Non Commercial/Commercial license.
// * This applies to EPPlus version 5 and later.
// * Previous versions are still licensed LGPL
// *
// * If you use the commercial version of EPPlus, please take the code below.
// */
///*
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Extensions;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Spire.Xls;

namespace DinePlan.DineConnect.DataExporting.Excel.EpPlus
{
    public abstract class EpPlusExcelExporterBase : DineConnectServiceBase, ITransientDependency
    {
        private readonly ITempFileCacheManager _tempFileCacheManager;
        public IAppFolders AppFolders { get; set; }
        public string _dateTimeFormat = "dd-MM-yyyy HH:mm:ss";
        public int _roundDecimals = 2;
        public string _simpleDateFormat = "dd-MM-yyyy";
        public string _timeFormat = "HH:mm:ss";

        protected EpPlusExcelExporterBase(ITempFileCacheManager tempFileCacheManager)
        {
            _tempFileCacheManager = tempFileCacheManager;
        }

        private void WriteHtml(FileDto file)
        {
            var package = new ExcelPackage(new FileInfo(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken)));
            if (package?.Workbook?.Worksheets != null && package.Workbook.Worksheets.Any())
            {
                StringBuilder builder = new StringBuilder();
                foreach (var worksheet in package.Workbook.Worksheets)
                {
                    var html = worksheet.ToHtml();
                    builder.Append(html);
                }
                if (builder.Length > 0)
                {
                    File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), builder.ToString());
                }
            }
        }

        public FileDto ProcessFile(ExportType input, FileDto file)
        {
            if (input == ExportType.HTML)
            {
                WriteHtml(file);
                //file.FileType = MimeTypeNames.ApplicationXhtmlXml;
                file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            }
            return file;
        }

        private void WriteHtmlSpire(FileDto file)
        {
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken));

            ////convert Excel to HTML
            Worksheet sheet = workbook.Worksheets[0];
            sheet.SaveToHtml(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken));
        }

        protected FileDto CreateExcelPackage(string fileName, Action<ExcelPackage> creator)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = GetExcelPackage())
            {
                creator(excelPackage);
                SaveToPath(excelPackage, file);
            }

            return file;
        }

        private static ExcelPackage GetExcelPackage()
        {
            return new ExcelPackage();
        }

        protected void AddHeader(ExcelWorksheet sheet, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, headerTexts[i]);
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, bool upper, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, upper ? headerTexts[i].ToUpper() : headerTexts[i]);
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, int startingPoint, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, startingPoint, i + 1, headerTexts[i]);
            }
        }

        protected void AddHeaderWithColor(ExcelWorksheet sheet, int startingPoint, Color background, Color foreground, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeaderWithColor(sheet, startingPoint, i + 1, background, foreground, headerTexts[i]);
            }
        }

        protected void AddRow(ExcelWorksheet sheet, int rowIndex, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                sheet.Cells[rowIndex, i + 1].Value = headerTexts[i];
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, int columnIndex, string headerText)
        {
            sheet.Cells[1, columnIndex].Value = headerText;
            sheet.Cells[1, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeader(ExcelWorksheet sheet, int rowIndex, int columnIndex, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithColor(ExcelWorksheet sheet, int rowIndex, int columnIndex, Color background, Color foreground, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Color.SetColor(foreground);
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(background);
        }

        protected void AddHeader(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMerge(ExcelWorksheet sheet, int rowIndex, int columnIndex, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + 1].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMergeWithColumnCountColor(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText, Color background, Color foreground)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Color.SetColor(foreground);
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(background);
        }

        protected void AddHeaderWithMergeWithColumnCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMergeWithColumnCountAndAlignment(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText, ExcelHorizontalAlignment alignment)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = alignment;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMergeWithColumnCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, decimal headerText)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMerge(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = Math.Round(headerText, 2, MidpointRounding.AwayFromZero);
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + 1].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText, ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        }

        protected void AddDetailWithNumberFormat(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText, string digitFormat)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            if (digitFormat.IsNullOrEmpty() || digitFormat.IsNullOrWhiteSpace())
                digitFormat = "0.00";
            sheet.Cells[rowIndex, columnIndex].Style.Numberformat.Format = digitFormat;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, int detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText,
             OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
        }

        protected void AddFooter(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText,
            OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Size = 20;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
        }

        protected void AddFooter(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText,
            OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Size = 20;
        }

        protected void AddObjects<T>(ExcelWorksheet sheet, int startRowIndex, IList<T> items, params Func<T, object>[] propertySelectors)
        {
            if (items.IsNullOrEmpty() || propertySelectors.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < items.Count; i++)
            {
                for (var j = 0; j < propertySelectors.Length; j++)
                {
                    sheet.Cells[i + startRowIndex, j + 1].Value = propertySelectors[j](items[i]);
                }
            }
        }

        protected void SaveToPath(ExcelPackage excelPackage, FileDto file)
        {
            var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
            excelPackage.SaveAs(new FileInfo(filePath));
        }

        protected void SaveToFile(ExcelPackage excelPackage, FileDto file)
        {
            _tempFileCacheManager.SetFile(file.FileToken, excelPackage.GetAsByteArray());
        }

        protected void SetBorder(ExcelWorksheet sheet, int rowIndex, int columnIndex, ExcelRange excelRange, ExcelBorderStyle excelBorderStyle)
        {
            excelRange.Style.Border.BorderAround(excelBorderStyle);
        }

        public void AutoFilterRangeGiven(ExcelWorksheet sheet, int startingRowIndex, int startingColIndex, int endingRowIndex, int endingColumnIndex)
        {
            using (ExcelRange autoFilterCells = sheet.Cells[
                           startingRowIndex, startingColIndex,
                           endingRowIndex, endingColumnIndex])
            {
                autoFilterCells.AutoFilter = true;
            }
        }

        protected void ChangeText(ExcelWorksheet sheet, string cellAddress, string tagName, string tagValue)
        {
            string temp = sheet.Cells[cellAddress].Value.ToString();
            temp = temp.Replace(tagName, tagValue);
            sheet.Cells[cellAddress].Value = temp;
        }

        protected void AddText(ExcelWorksheet sheet, string cellAddress, string cellText)
        {
            sheet.Cells[cellAddress].Value = cellText;
        }

        protected void AddText(ExcelWorksheet sheet, string cellAddress, decimal cellValue)
        {
            sheet.Cells[cellAddress].Value = cellValue;
        }

        public string RemoveRightSideFromGivenChar(string input, char searchChar)
        {
            var idx = input.LastIndexOf(searchChar);
            if (idx > 0)
                input = input.Substring(0, idx - 1);
            return input.Trim();
        }

        public void LinesForEmptyRow(ExcelWorksheet worksheet, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            var emptyRows = toRow - fromRow;
            if (emptyRows >= 0)
            {
                if (emptyRows < 3)
                {
                    for (int i = fromRow; i <= emptyRows + fromRow; i++)
                    {
                        var straightLine = worksheet.Drawings.AddShape("StraightLine_" + worksheet.Name + "_" + i, eShapeStyle.Line);
                        //to draw a straightline
                        straightLine.From.Row = i - 1;
                        straightLine.From.Column = fromColumn;
                        straightLine.To.Row = i - 1;
                        straightLine.To.Column = toColumn;
                        //to set the straightline with desired position
                        straightLine.SetPosition(i - 1, 12, 0, 4);
                        //To Set the border of the line
                        straightLine.Border.Width = 1;
                        straightLine.Border.Fill.Color = Color.Black;
                        //row++;
                    }
                }
                else
                {
                    //to draw a straightline
                    var straightLine = worksheet.Drawings.AddShape("StraightLine_" + worksheet.Name + "_" + fromRow, eShapeStyle.Line);
                    straightLine.From.Row = fromRow - 1;
                    straightLine.From.Column = fromColumn;
                    straightLine.To.Row = fromRow - 1;
                    straightLine.To.Column = toColumn;
                    //to set the straightline with desired position
                    straightLine.SetPosition(fromRow - 1, 12, 0, 4);
                    //To Set the border of the line
                    straightLine.Border.Width = 1;
                    straightLine.Border.Fill.Color = Color.Black;
                    fromRow++;

                    //to draw a cross line
                    var crossedLine = worksheet.Drawings.AddShape("CrossLine_" + worksheet.Name + "_" + fromRow + "_To_" + toColumn, eShapeStyle.Line);
                    crossedLine.From.Row = fromRow - 1;
                    crossedLine.From.Column = fromColumn;
                    crossedLine.To.Row = toRow;
                    crossedLine.To.Column = toColumn;
                    crossedLine.Border.Fill.Color = Color.Black;
                    crossedLine.SetPosition(fromRow - 1, 3, 0, 2);
                    crossedLine.Border.Width = 1;
                }
            }
        }

        protected void AddHeaderWithHorizontalMergeWithRowCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int rowcount, string headerText)
        {
            if (rowcount == 0)
                rowcount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex + rowcount - 1, columnIndex].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected string GetDecimalFormat(decimal number)
        {
            var format = $"{{0:F{_roundDecimals}}}";
            return string.Format(format, number);
        }
        protected FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            //file.FileType = MimeTypeNames.TextHtml;
            file.FileName = file.FileName.Replace(".xlsx",".html");
            return file;
        }
        protected string GetStyleForTable()
        {
            var stype = @"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }
            .text-right{
               text-align: right;          
            }
            .w-18 {
                width: 18%;
            }

            .w-20 {
                width: 20%;
            }

            .w-5 {
                width: 5%;
            }

            .w-15 {
                width: 15%;
            }

            .w-25 {
                width: 25%;
            }

            .w-30 {
                width: 30%;
            }

            .w-35 {
                width: 35%;
            }

            .w-50 {
                width: 50%;
            }

            .w-70 {
                width: 70%;
            }

            .w-85 {
                width: 85%;
            }
            .margin-0{
               margin: 0;
            }
            table tr:hover {background-color: #ddd;}
            </style>";
            return stype;
        }
    }
}
﻿using System.Threading.Tasks;
using Abp.Net.Mail;
using Microsoft.Extensions.Configuration;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.Configuration.Host.Dto;
using DinePlan.DineConnect.EmailConfiguration;

namespace DinePlan.DineConnect.Configuration
{
    public abstract class SettingsAppServiceBase : DineConnectAppServiceBase
    {
        private readonly ITenantEmailSendAppService _emailSender;
        private readonly IAppConfigurationAccessor _configurationAccessor;

        protected SettingsAppServiceBase(
            ITenantEmailSendAppService emailSender, 
            IAppConfigurationAccessor configurationAccessor)
        {
            _emailSender = emailSender;
            _configurationAccessor = configurationAccessor;
        }

        #region Send Test Email

        public async Task SendTestEmail(SendTestEmailInput input)
        {
            await _emailSender.SendAsyc(
                input.EmailAddress,
                L("TestEmail_Subject"),
                L("TestEmail_Body"),
                true,
                null
            );
        }

        public ExternalLoginSettingsDto GetEnabledSocialLoginSettings()
        {
            var dto = new ExternalLoginSettingsDto();


            var allowSocialSetting = _configurationAccessor.Configuration["Authentication:AllowSocialLoginSettingsPerTenant"];
            if (allowSocialSetting != null)
            {
                if (!bool.Parse(allowSocialSetting))
                {
                    return dto;
                }
            }

            if (IsSocialLoginEnabled("Facebook"))
            {
                dto.EnabledSocialLoginSettings.Add("Facebook");
            }

            if (IsSocialLoginEnabled("Google"))
            {
                dto.EnabledSocialLoginSettings.Add("Google");
            }

            if (IsSocialLoginEnabled("Twitter"))
            {
                dto.EnabledSocialLoginSettings.Add("Twitter");
            }

            if (IsSocialLoginEnabled("Microsoft"))
            {
                dto.EnabledSocialLoginSettings.Add("Microsoft");
            }
            
            if (IsSocialLoginEnabled("WsFederation"))
            {
                dto.EnabledSocialLoginSettings.Add("WsFederation");
            }
            
            if (IsSocialLoginEnabled("OpenId"))
            {
                dto.EnabledSocialLoginSettings.Add("OpenId");
            }

            return dto;
        }

        private bool IsSocialLoginEnabled(string name)
        {
            return _configurationAccessor.Configuration.GetSection("Authentication:" + name).Exists() &&
                   bool.Parse(_configurationAccessor.Configuration["Authentication:" + name + ":IsEnabled"]);
        }

        #endregion
    }
}

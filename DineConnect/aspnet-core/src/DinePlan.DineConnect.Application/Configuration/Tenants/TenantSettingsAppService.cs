using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Extensions;
using Abp.Json;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.Zero.Configuration;
using Abp.Zero.Ldap.Configuration;
using DinePlan.DineConnect.Authentication;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.Configuration.Host.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Security;
using DinePlan.DineConnect.Settings;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Timing;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Configuration.Tenants
{
    public class TenantSettingsAppService : SettingsAppServiceBase, ITenantSettingsAppService
    {
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ICloudImageUploader _cloudImageUploader;
        private readonly IAbpZeroLdapModuleConfig _ldapModuleConfig;

        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ITimeZoneService _timeZoneService;

        public TenantSettingsAppService(
            IAbpZeroLdapModuleConfig ldapModuleConfig,
            IMultiTenancyConfig multiTenancyConfig,
            ITimeZoneService timeZoneService,
            ITenantEmailSendAppService emailSender,
            ICloudImageUploader cloudImageUploader,
            ITempFileCacheManager tempFileCacheManager,
            IAppConfigurationAccessor configurationAccessor,
            IBinaryObjectManager binaryObjectManager,
            IWebHostEnvironment env) : base(emailSender, configurationAccessor)
        {
            ExternalLoginOptionsCacheManager = NullExternalLoginOptionsCacheManager.Instance;

            _multiTenancyConfig = multiTenancyConfig;
            _ldapModuleConfig = ldapModuleConfig;
            _timeZoneService = timeZoneService;
            _cloudImageUploader = cloudImageUploader;
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            ConfigPath = env.ContentRootPath;
            EnvironmentName = env.EnvironmentName;
        }

        private string ConfigPath { get; }
        private string EnvironmentName { get; }

        public IExternalLoginOptionsCacheManager ExternalLoginOptionsCacheManager { get; set; }


        #region Touch

        public async Task<TouchSettingDto> GetCompactSetting()
        {
            var retS = new TouchSettingDto
            {
                Currency = await FeatureChecker.GetValueAsync(AppFeatures.ConnectCurrency),
                Rounding = await SettingManager.GetSettingValueAsync<decimal>(AppSettings.TouchSaleSettings.Rounding),
                TotalDecimal = await SettingManager.GetSettingValueAsync<int>(AppSettings.TouchSaleSettings.Decimals)
            };
            return retS;
        }

        #endregion

        #region Get Settings

        public async Task<TenantSettingsEditDto> GetAllSettings()
        {
            if (AbpSession.TenantId == null)
            {
                return default;
            }
            
            var settings = new TenantSettingsEditDto
            {
                UserManagement = await GetUserManagementSettingsAsync(),
                Security = await GetSecuritySettingsAsync(),
                Billing = await GetBillingSettingsAsync(),
                OtherSettings = await GetOtherSettingsAsync(),
                Email = await GetEmailSettingsAsync(),
                ExternalLoginProviderSettings = await GetExternalLoginProviderSettings(),
                TenantMealSetting = await GetMealSettingAsync(),
                CompanySetting = await GetCompanySettingAsync(),
                CompanyLogoForCustomerSetting = await GetCompanyLogoForCustomerSettingAsync(),
                Theme = new TenantThemeSettingsEditDto
                {
                    Color = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ThemeSetting.Color,
                        AbpSession.GetTenantId())
                },
                TermsAndCondition = new TermsAndConditionsDto
                {
                    Content = await GetTermsAndConditions()
                },
                ReferralEarning = await GetReferralEarningSetting(),
                ShippingProvider = await GetShippingProviderSetting(),
                FileDateTimeFormat = await GetFileDateTimeFormatSetting(),
                TiffinPopup = await GetTiffinPopup(),
                TiffinAllowOrderInFutureDays = await GetTiffinAllowOrderInFutureDays(),
                TiffinCutOffDays = await GetTiffinCutOffDays(),
                TiffinIsEnableDelivery = await GetTiffinIsEnableDelivery(),
                IsTiffinEnableStudentInformation = await GetIsTiffinEnableStudentInformation(),
                SignUpForm = await GetSignUpFormSetting(),
                Connect = await GetConnectSetting(),
                TouchSaleSettings = await GetTouchSaleSettings(),
                CliqueCutOffDays = await GetCliqueCutOffDays(),
                TenantCliqueMealSetting = await GetCliqueMealSettingAsync(),
                CliqueAllowOrderInFutureDays = await GetCliqueAllowOrderInFutureDays(),
                CliqueAddressObligatory = await GetCliqueAddressObligatory(),
                General = await GetGeneralSettingsAsync()
            };

            if (_ldapModuleConfig.IsEnabled)
            {
                settings.Ldap = await GetLdapSettingsAsync();
            }
            else
            {
                settings.Ldap = new LdapSettingsEditDto { IsModuleEnabled = false };
            }

            return settings;
        }

        private async Task<LdapSettingsEditDto> GetLdapSettingsAsync()
        {
            return new LdapSettingsEditDto
            {
                IsModuleEnabled = true,
                IsEnabled = await SettingManager.GetSettingValueForTenantAsync<bool>(LdapSettingNames.IsEnabled,
                    AbpSession.GetTenantId()),
                Domain = await SettingManager.GetSettingValueForTenantAsync(LdapSettingNames.Domain,
                    AbpSession.GetTenantId()),
                UserName = await SettingManager.GetSettingValueForTenantAsync(LdapSettingNames.UserName,
                    AbpSession.GetTenantId()),
                Password = await SettingManager.GetSettingValueForTenantAsync(LdapSettingNames.Password,
                    AbpSession.GetTenantId())
            };
        }

        private async Task<TenantEmailSettingsEditDto> GetEmailSettingsAsync()
        {
            var useHostDefaultEmailSettings =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.Email.UseHostDefaultEmailSettings,
                    AbpSession.GetTenantId());

            if (useHostDefaultEmailSettings)
                return new TenantEmailSettingsEditDto
                {
                    UseHostDefaultEmailSettings = true
                };

            var smtpPassword =
                await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.Smtp.Password,
                    AbpSession.GetTenantId());

            return new TenantEmailSettingsEditDto
            {
                UseHostDefaultEmailSettings = false,
                DefaultFromAddress =
                    await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.DefaultFromAddress,
                        AbpSession.GetTenantId()),
                DefaultFromDisplayName =
                    await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.DefaultFromDisplayName,
                        AbpSession.GetTenantId()),
                SmtpHost = await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.Smtp.Host,
                    AbpSession.GetTenantId()),
                SmtpPort = await SettingManager.GetSettingValueForTenantAsync<int>(EmailSettingNames.Smtp.Port,
                    AbpSession.GetTenantId()),
                SmtpUserName =
                    await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.Smtp.UserName,
                        AbpSession.GetTenantId()),
                SmtpPassword = SimpleStringCipher.Instance.Decrypt(smtpPassword),
                SmtpDomain =
                    await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.Smtp.Domain,
                        AbpSession.GetTenantId()),
                SmtpEnableSsl =
                    await SettingManager.GetSettingValueForTenantAsync<bool>(EmailSettingNames.Smtp.EnableSsl,
                        AbpSession.GetTenantId()),
                SmtpUseDefaultCredentials =
                    await SettingManager.GetSettingValueForTenantAsync<bool>(
                        EmailSettingNames.Smtp.UseDefaultCredentials, AbpSession.GetTenantId()),
                SendGridApiKey = await SettingManager.GetSettingValueForTenantAsync(AppSettings.Email.SendGridApiKey,
                    AbpSession.GetTenantId())
            };
        }

        private async Task<GeneralSettingsEditDto> GetGeneralSettingsAsync()
        {
            var settings = new GeneralSettingsEditDto();

            if (Clock.SupportsMultipleTimezone)
            {
                var timezone =
                    await SettingManager.GetSettingValueForTenantAsync(TimingSettingNames.TimeZone,
                        AbpSession.GetTenantId());

                settings.Timezone = timezone;
                settings.TimezoneForComparison = timezone;
            }

            var defaultTimeZoneId = await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, AbpSession.TenantId);

            if (settings.Timezone == defaultTimeZoneId)
            {
                settings.Timezone = string.Empty;
            }

            settings.AdminWhatappNumber = await SettingManager.GetSettingValueForTenantAsync(AppSettings.AdminWhatappNumber, AbpSession.GetTenantId());
            settings.WhatappAccountSid = await SettingManager.GetSettingValueForTenantAsync(AppSettings.WhatappAccountSid, AbpSession.GetTenantId());
            settings.WhatappAuthToken = await SettingManager.GetSettingValueForTenantAsync(AppSettings.WhatappAuthToken, AbpSession.GetTenantId());
            settings.WhatappSenderNumber = await SettingManager.GetSettingValueForTenantAsync(AppSettings.WhatappSenderNumber, AbpSession.GetTenantId());


            return settings;
        }

        private async Task<TenantUserManagementSettingsEditDto> GetUserManagementSettingsAsync()
        {
            return new TenantUserManagementSettingsEditDto
            {
                AllowSelfRegistration =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.AllowSelfRegistration),
                IsNewRegisteredUserActiveByDefault =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                        .IsNewRegisteredUserActiveByDefault),
                IsEmailConfirmationRequiredForLogin =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                        .IsEmailConfirmationRequiredForLogin),
                UseCaptchaOnRegistration =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                        .UseCaptchaOnRegistration),
                UseCaptchaOnLogin =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.UseCaptchaOnLogin),
                IsCookieConsentEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.IsCookieConsentEnabled),
                IsQuickThemeSelectEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(
                        AppSettings.UserManagement.IsQuickThemeSelectEnabled),
                AllowUsingGravatarProfilePicture =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                        .AllowUsingGravatarProfilePicture),
                SessionTimeOutSettings = new SessionTimeOutSettingsEditDto
                {
                    IsEnabled = await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                        .SessionTimeOut.IsEnabled),
                    TimeOutSecond =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.UserManagement.SessionTimeOut
                            .TimeOutSecond),
                    ShowTimeOutNotificationSecond =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.UserManagement.SessionTimeOut
                            .ShowTimeOutNotificationSecond),
                    ShowLockScreenWhenTimedOut =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.SessionTimeOut
                            .ShowLockScreenWhenTimedOut)
                }
            };
        }

        private async Task<SecuritySettingsEditDto> GetSecuritySettingsAsync()
        {
            var passwordComplexitySetting = new PasswordComplexitySetting
            {
                RequireDigit =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireDigit),
                RequireLowercase =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireLowercase),
                RequireNonAlphanumeric =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireNonAlphanumeric),
                RequireUppercase =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireUppercase),
                RequiredLength =
                    await SettingManager.GetSettingValueAsync<int>(AbpZeroSettingNames.UserManagement.PasswordComplexity
                        .RequiredLength)
            };

            var defaultPasswordComplexitySetting = new PasswordComplexitySetting
            {
                RequireDigit =
                    await SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireDigit),
                RequireLowercase =
                    await SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireLowercase),
                RequireNonAlphanumeric =
                    await SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireNonAlphanumeric),
                RequireUppercase =
                    await SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequireUppercase),
                RequiredLength =
                    await SettingManager.GetSettingValueForApplicationAsync<int>(AbpZeroSettingNames.UserManagement
                        .PasswordComplexity.RequiredLength)
            };

            return new SecuritySettingsEditDto
            {
                UseDefaultPasswordComplexitySettings =
                    passwordComplexitySetting.Equals(defaultPasswordComplexitySetting),
                PasswordComplexity = passwordComplexitySetting,
                DefaultPasswordComplexity = defaultPasswordComplexitySetting,
                UserLockOut = await GetUserLockOutSettingsAsync(),
                TwoFactorLogin = await GetTwoFactorLoginSettingsAsync(),
                AllowOneConcurrentLoginPerUser = await GetOneConcurrentLoginPerUserSetting()
            };
        }

        private async Task<TenantBillingSettingsEditDto> GetBillingSettingsAsync()
        {
            return new TenantBillingSettingsEditDto
            {
                LegalName = await SettingManager.GetSettingValueAsync(AppSettings.TenantManagement.BillingLegalName),
                Address = await SettingManager.GetSettingValueAsync(AppSettings.TenantManagement.BillingAddress),
                TaxVatNo = await SettingManager.GetSettingValueAsync(AppSettings.TenantManagement.BillingTaxVatNo)
            };
        }

        private async Task<TenantOtherSettingsEditDto> GetOtherSettingsAsync()
        {
            return new TenantOtherSettingsEditDto
            {
                IsQuickThemeSelectEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(
                        AppSettings.UserManagement.IsQuickThemeSelectEnabled)
            };
        }

        private async Task<UserLockOutSettingsEditDto> GetUserLockOutSettingsAsync()
        {
            return new UserLockOutSettingsEditDto
            {
                IsEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement
                    .UserLockOut.IsEnabled),
                MaxFailedAccessAttemptsBeforeLockout =
                    await SettingManager.GetSettingValueAsync<int>(AbpZeroSettingNames.UserManagement.UserLockOut
                        .MaxFailedAccessAttemptsBeforeLockout),
                DefaultAccountLockoutSeconds =
                    await SettingManager.GetSettingValueAsync<int>(AbpZeroSettingNames.UserManagement.UserLockOut
                        .DefaultAccountLockoutSeconds)
            };
        }

        private Task<bool> IsTwoFactorLoginEnabledForApplicationAsync()
        {
            return SettingManager.GetSettingValueForApplicationAsync<bool>(AbpZeroSettingNames.UserManagement
                .TwoFactorLogin.IsEnabled);
        }

        private async Task<TwoFactorLoginSettingsEditDto> GetTwoFactorLoginSettingsAsync()
        {
            var settings = new TwoFactorLoginSettingsEditDto
            {
                IsEnabledForApplication = await IsTwoFactorLoginEnabledForApplicationAsync()
            };

            if (_multiTenancyConfig.IsEnabled && !settings.IsEnabledForApplication) return settings;

            settings.IsEnabled =
                await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                    .IsEnabled);
            settings.IsRememberBrowserEnabled =
                await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                    .IsRememberBrowserEnabled);

            if (!_multiTenancyConfig.IsEnabled)
            {
                settings.IsEmailProviderEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                        .IsEmailProviderEnabled);
                settings.IsSmsProviderEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                        .IsSmsProviderEnabled);
                settings.IsGoogleAuthenticatorEnabled =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.TwoFactorLogin
                        .IsGoogleAuthenticatorEnabled);
            }

            return settings;
        }

        private async Task<bool> GetOneConcurrentLoginPerUserSetting()
        {
            return await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                .AllowOneConcurrentLoginPerUser);
        }

        private async Task<ExternalLoginProviderSettingsEditDto> GetExternalLoginProviderSettings()
        {
            var facebookSettings = new FacebookExternalLoginProviderSettings
            {
                AppId = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.Facebook.AppId, AbpSession.GetTenantId()),
                AppSecret = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.Facebook.AppSecret, AbpSession.GetTenantId())
            };

            var googleSettings = new GoogleExternalLoginProviderSettings
            {
                ClientId = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.Google.ClientId, AbpSession.GetTenantId()),
                ClientSecret = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.Google.ClientSecret, AbpSession.GetTenantId()),
                UserInfoEndpoint = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.Google.UserInfoEndpoint, AbpSession.GetTenantId())
            };
            var twitterSettings =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.ExternalLoginProvider.Tenant.Twitter,
                    AbpSession.GetTenantId());
            var microsoftSettings =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.ExternalLoginProvider.Tenant.Microsoft,
                    AbpSession.GetTenantId());

            var openIdConnectSettings =
                await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.OpenIdConnect, AbpSession.GetTenantId());
            var openIdConnectMappedClaims =
                await SettingManager.GetSettingValueAsync(AppSettings.ExternalLoginProvider.OpenIdConnectMappedClaims);

            var wsFederationSettings =
                await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.ExternalLoginProvider.Tenant.WsFederation, AbpSession.GetTenantId());
            var wsFederationMappedClaims =
                await SettingManager.GetSettingValueAsync(AppSettings.ExternalLoginProvider.WsFederationMappedClaims);

            return new ExternalLoginProviderSettingsEditDto
            {
                Facebook = facebookSettings,
                Google = googleSettings,
                Twitter = twitterSettings.IsNullOrWhiteSpace()
                    ? new TwitterExternalLoginProviderSettings()
                    : twitterSettings.FromJsonString<TwitterExternalLoginProviderSettings>(),
                Microsoft = microsoftSettings.IsNullOrWhiteSpace()
                    ? new MicrosoftExternalLoginProviderSettings()
                    : microsoftSettings.FromJsonString<MicrosoftExternalLoginProviderSettings>(),

                OpenIdConnect = openIdConnectSettings.IsNullOrWhiteSpace()
                    ? new OpenIdConnectExternalLoginProviderSettings()
                    : openIdConnectSettings.FromJsonString<OpenIdConnectExternalLoginProviderSettings>(),
                OpenIdConnectClaimsMapping = openIdConnectMappedClaims.IsNullOrWhiteSpace()
                    ? new List<JsonClaimMapDto>()
                    : openIdConnectMappedClaims.FromJsonString<List<JsonClaimMapDto>>(),

                WsFederation = wsFederationSettings.IsNullOrWhiteSpace()
                    ? new WsFederationExternalLoginProviderSettings()
                    : wsFederationSettings.FromJsonString<WsFederationExternalLoginProviderSettings>(),
                WsFederationClaimsMapping = wsFederationMappedClaims.IsNullOrWhiteSpace()
                    ? new List<JsonClaimMapDto>()
                    : wsFederationMappedClaims.FromJsonString<List<JsonClaimMapDto>>()
            };
        }

        private async Task<TenantMealSettingEditDto> GetMealSettingAsync()
        {
            var minMealCountJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.MealSetting.MinMealCount,
                AbpSession.GetTenantId());

            var minExpiryDayJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.MealSetting.MinExpiryDay,
                    AbpSession.GetTenantId());

            var orderCutOffTimeJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.MealSetting.OrderCutOffTime,
                    AbpSession.GetTenantId());

            int? minMealCount = null;
            if (!minMealCountJson.IsNullOrEmpty() && minMealCountJson != "null")
                minMealCount = JsonConvert.DeserializeObject<int>(minMealCountJson);

            int? minExpiryDay = null;
            if (!minExpiryDayJson.IsNullOrEmpty() && minExpiryDayJson != "null")
                minExpiryDay = JsonConvert.DeserializeObject<int>(minExpiryDayJson);

            DateTime? orderCutOffTime = null;
            if (!orderCutOffTimeJson.IsNullOrEmpty() && orderCutOffTimeJson != "null")
            {
                var orderCutOffTimeString = JsonConvert.DeserializeObject<string>(orderCutOffTimeJson);
                orderCutOffTime = DateTime.Parse(orderCutOffTimeString);
            }

            return new TenantMealSettingEditDto
            {
                MinMealCount = minMealCount,
                MinExpiryDay = minExpiryDay,
                OrderCutOffTime = orderCutOffTime
            };
        }

        private async Task<TenantMealSettingEditDto> GetCliqueMealSettingAsync()
        {
            var minMealCountJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.CliqueMealSetting.CliqueMinMealCount,
                AbpSession.GetTenantId());

            var minExpiryDayJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CliqueMealSetting.CliqueMinExpiryDay,
                    AbpSession.GetTenantId());

            var orderCutOffTimeJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CliqueMealSetting.CliqueOrderCutOffTime,
                    AbpSession.GetTenantId());

            int? minMealCount = null;
            if (!minMealCountJson.IsNullOrEmpty() && minMealCountJson != "null")
                minMealCount = JsonConvert.DeserializeObject<int>(minMealCountJson);

            int? minExpiryDay = null;
            if (!minExpiryDayJson.IsNullOrEmpty() && minExpiryDayJson != "null")
                minExpiryDay = JsonConvert.DeserializeObject<int>(minExpiryDayJson);

            DateTime? orderCutOffTime = null;
            if (!orderCutOffTimeJson.IsNullOrEmpty() && orderCutOffTimeJson != "null")
            {
                var orderCutOffTimeString = JsonConvert.DeserializeObject<string>(orderCutOffTimeJson);
                orderCutOffTime = DateTime.Parse(orderCutOffTimeString);
            }

            return new TenantMealSettingEditDto
            {
                MinMealCount = minMealCount,
                MinExpiryDay = minExpiryDay,
                OrderCutOffTime = orderCutOffTime
            };
        }

        public async Task<CompanySettingDto> GetCompanySettingAsync()
        {
            var companyNameJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.CompanySetting.CompanyName,
                AbpSession.GetTenantId());

            var companyLogoUrlJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CompanySetting.CompanyLogoUrl,
                    AbpSession.GetTenantId());

            var companyName = string.Empty;
            if (!companyNameJson.IsNullOrEmpty()) companyName = JsonConvert.DeserializeObject<string>(companyNameJson);

            var companyLogoUrl = string.Empty;
            if (!companyLogoUrlJson.IsNullOrEmpty())
                companyLogoUrl = JsonConvert.DeserializeObject<string>(companyLogoUrlJson);

            return new CompanySettingDto
            {
                CompanyName = companyName,
                CompanyLogoUrl = companyLogoUrl
            };
        }

        public async Task<CompanyLogoForCustomerSettingDto> GetCompanyLogoForCustomerSettingAsync()
        {
            if (!AbpSession.TenantId.HasValue)
                return new CompanyLogoForCustomerSettingDto
                {
                    CompanyLogoUrl = string.Empty
                };

            var companyLogoUrlJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CompanySetting.CompanyLogoUrlForCustomer,
                    AbpSession.GetTenantId());

            var companyLogoUrl = string.Empty;
            if (!companyLogoUrlJson.IsNullOrEmpty())
                companyLogoUrl = JsonConvert.DeserializeObject<string>(companyLogoUrlJson);

            return new CompanyLogoForCustomerSettingDto
            {
                CompanyLogoUrl = companyLogoUrl
            };
        }

        #endregion Get Settings

        #region Update Settings

        public async Task UpdateAllSettings(TenantSettingsEditDto input)
        {
            await UpdateUserManagementSettingsAsync(input.UserManagement);
            await UpdateSecuritySettingsAsync(input.Security);
            await UpdateBillingSettingsAsync(input.Billing);
            await UpdateEmailSettingsAsync(input.Email);
            await UpdateExternalLoginSettingsAsync(input.ExternalLoginProviderSettings);
            await UpdateMealSettingAsync(input.TenantMealSetting);
            await UpdateCompanySettingAsync(input.CompanySetting);
            await UpdateLogoCompanySettingAsync(input.CompanyLogoForCustomerSetting);

            //theme
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.ThemeSetting.Color,
                input.Theme.Color);

            //T&C
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TermAndConditionSetting.TermAndCondition, input.TermsAndCondition.Content);

            //Time Zone
            if (Clock.SupportsMultipleTimezone)
            {
                if (input.General.Timezone.IsNullOrEmpty())
                {
                    var defaultValue =
                        await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, AbpSession.TenantId);
                    await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        TimingSettingNames.TimeZone, defaultValue);
                }
                else
                {
                    await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        TimingSettingNames.TimeZone, input.General.Timezone);
                }
            }

            // Excel DateTime Format Setting

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.FileDateTimeFormatSetting.FileDateTimeFormat, input.FileDateTimeFormat.FileDateTimeFormat);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.FileDateTimeFormatSetting.FileDateFormat, input.FileDateTimeFormat.FileDateFormat);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.FileDateTimeFormatSetting.FileDayMonthFormat, input.FileDateTimeFormat.FileDayMonthFormat);

            #region Wheel

            // Shipping Provider Setting

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ShippingProviderSetting.ShippingProvider,
                ((int)input.ShippingProvider.ShippingProvider).ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ShippingProviderSetting.ShippingSetting, input.ShippingProvider.ShippingProviderSetting);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ShippingProviderSetting.ShippingGoGoVan, input.ShippingProvider.ShippingGoGoVanSetting);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ShippingProviderSetting.ShippingLalaMove, input.ShippingProvider.ShippingLaLaMoveSetting);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ShippingProviderSetting.ShippingDunzo, input.ShippingProvider.ShippingDunzoSetting);

            #endregion Wheel

            #region Tiffins

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.TiffinPopup,
                input.TiffinPopup);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TiffinAllowOrderInFutureDays, input.TiffinAllowOrderInFutureDays.ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.TiffinCutOffDays,
                input.TiffinCutOffDays.ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TiffinIsEnableDelivery, input.TiffinIsEnableDelivery.ToString());

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.SignUpFormSetting.HintImageUrl, input.SignUpForm.HintImageUrl);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.IsTiffinEnableStudentInformation, input.IsTiffinEnableStudentInformation.ToString());
            // Referral Earning Setting

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ReferralEarningSetting.ReferralEarningType,
                ((int)input.ReferralEarning.ReferralEarnType).ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ReferralEarningSetting.ReferralEarningValue,
                input.ReferralEarning.ReferralEarnValue.ToString());

            #endregion Tiffins

            #region Clique

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.CliqueCutOffDays, input.CliqueCutOffDays.ToString());
            await UpdateCliqueMealSettingAsync(input.TenantCliqueMealSetting); 
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.CliqueAllowOrderInFutureDays, input.CliqueAllowOrderInFutureDays.ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.CliqueAddressObligatory, input.CliqueAddressObligatory.ToString());

            #endregion Clique

            #region Connect

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.WeekDay, input.Connect.WeekDay);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.WeekEnd, input.Connect.WeekEnd);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.Schedules, input.Connect.Schedules);

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.SyncUrl, input.Connect.SyncUrl);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.SyncTenantId, input.Connect.SyncTenantId);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.SyncTenantName, input.Connect.SyncTenantName);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.SyncUser, input.Connect.SyncUser);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.SyncPassword, input.Connect.SyncPassword);

            #endregion Connect

            #region Touch

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TouchSaleSettings.Currency, input.TouchSaleSettings.Currency);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TouchSaleSettings.Decimals, input.TouchSaleSettings.Decimals);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TouchSaleSettings.Rounding, input.TouchSaleSettings.Rounding);

            #endregion Touch

            // Whatapp
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.AdminWhatappNumber, input.General.AdminWhatappNumber);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.WhatappAccountSid, input.General.WhatappAccountSid);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.WhatappAuthToken, input.General.WhatappAuthToken);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.WhatappSenderNumber, input.General.WhatappSenderNumber);

            if (!_multiTenancyConfig.IsEnabled)
            {
                await UpdateOtherSettingsAsync(input.OtherSettings);

                input.ValidateHostSettings();

                if (_ldapModuleConfig.IsEnabled) await UpdateLdapSettingsAsync(input.Ldap);
            }
        }

        private async Task UpdateMealSettingAsync(TenantMealSettingEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.MealSetting.MinExpiryDay, JsonConvert.SerializeObject(input.MinExpiryDay));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.MealSetting.MinMealCount, JsonConvert.SerializeObject(input.MinMealCount));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.MealSetting.OrderCutOffTime, JsonConvert.SerializeObject(input.OrderCutOffTime));
        }

        private async Task UpdateCliqueMealSettingAsync(TenantMealSettingEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CliqueMealSetting.CliqueMinExpiryDay, JsonConvert.SerializeObject(input.MinExpiryDay));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CliqueMealSetting.CliqueMinMealCount, JsonConvert.SerializeObject(input.MinMealCount));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CliqueMealSetting.CliqueOrderCutOffTime, JsonConvert.SerializeObject(input.OrderCutOffTime));
        }

        private async Task UpdateCompanySettingAsync(CompanySettingDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CompanySetting.CompanyName, JsonConvert.SerializeObject(input.CompanyName));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CompanySetting.CompanyLogoUrl, JsonConvert.SerializeObject(input.CompanyLogoUrl));
        }

        private async Task UpdateLogoCompanySettingAsync(CompanyLogoForCustomerSettingDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.CompanySetting.CompanyLogoUrlForCustomer,
                JsonConvert.SerializeObject(input.CompanyLogoUrl));
        }

        private async Task UpdateOtherSettingsAsync(TenantOtherSettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.IsQuickThemeSelectEnabled,
                input.IsQuickThemeSelectEnabled.ToString().ToLowerInvariant()
            );
        }

        private async Task UpdateBillingSettingsAsync(TenantBillingSettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TenantManagement.BillingLegalName, input.LegalName);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TenantManagement.BillingAddress, input.Address);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.TenantManagement.BillingTaxVatNo, input.TaxVatNo);
        }

        private async Task UpdateLdapSettingsAsync(LdapSettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.IsEnabled,
                input.IsEnabled.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.Domain,
                input.Domain.IsNullOrWhiteSpace() ? null : input.Domain);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.UserName,
                input.UserName.IsNullOrWhiteSpace() ? null : input.UserName);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.Password,
                input.Password.IsNullOrWhiteSpace() ? null : input.Password);
        }

        private async Task UpdateEmailSettingsAsync(TenantEmailSettingsEditDto input)
        {
            var useHostDefaultEmailSettings = _multiTenancyConfig.IsEnabled && input.UseHostDefaultEmailSettings;

            if (useHostDefaultEmailSettings)
            {
                var smtpPassword =
                    await SettingManager.GetSettingValueForApplicationAsync(EmailSettingNames.Smtp.Password);

                input = new TenantEmailSettingsEditDto
                {
                    UseHostDefaultEmailSettings = true,
                    DefaultFromAddress =
                        await SettingManager.GetSettingValueForApplicationAsync(EmailSettingNames.DefaultFromAddress),
                    DefaultFromDisplayName =
                        await SettingManager.GetSettingValueForApplicationAsync(
                            EmailSettingNames.DefaultFromDisplayName),
                    SmtpHost = await SettingManager.GetSettingValueForApplicationAsync(EmailSettingNames.Smtp.Host),
                    SmtpPort =
                        await SettingManager.GetSettingValueForApplicationAsync<int>(EmailSettingNames.Smtp.Port),
                    SmtpUserName =
                        await SettingManager.GetSettingValueForApplicationAsync(EmailSettingNames.Smtp.UserName),
                    SmtpPassword = SimpleStringCipher.Instance.Decrypt(smtpPassword),
                    SmtpDomain = await SettingManager.GetSettingValueForApplicationAsync(EmailSettingNames.Smtp.Domain),
                    SmtpEnableSsl =
                        await SettingManager.GetSettingValueForApplicationAsync<bool>(EmailSettingNames.Smtp.EnableSsl),
                    SmtpUseDefaultCredentials =
                        await SettingManager.GetSettingValueForApplicationAsync<bool>(EmailSettingNames.Smtp
                            .UseDefaultCredentials),
                    SendGridApiKey =
                        await SettingManager.GetSettingValueForApplicationAsync(AppSettings.Email.SendGridApiKey)
                };
            }

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.Email.UseHostDefaultEmailSettings,
                useHostDefaultEmailSettings.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                EmailSettingNames.DefaultFromAddress, input.DefaultFromAddress);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                EmailSettingNames.DefaultFromDisplayName, input.DefaultFromDisplayName);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.Host,
                input.SmtpHost);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.Port,
                input.SmtpPort.ToString(CultureInfo.InvariantCulture));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.UserName,
                input.SmtpUserName);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.Password,
                SimpleStringCipher.Instance.Encrypt(input.SmtpPassword));
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.Domain,
                input.SmtpDomain);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), EmailSettingNames.Smtp.EnableSsl,
                input.SmtpEnableSsl.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                EmailSettingNames.Smtp.UseDefaultCredentials,
                input.SmtpUseDefaultCredentials.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.Email.SendGridApiKey,
                input.SendGridApiKey);
        }

        private async Task UpdateUserManagementSettingsAsync(TenantUserManagementSettingsEditDto settings)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.AllowSelfRegistration,
                settings.AllowSelfRegistration.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault,
                settings.IsNewRegisteredUserActiveByDefault.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin,
                settings.IsEmailConfirmationRequiredForLogin.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.UseCaptchaOnRegistration,
                settings.UseCaptchaOnRegistration.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.UseCaptchaOnLogin,
                settings.UseCaptchaOnLogin.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.IsCookieConsentEnabled,
                settings.IsCookieConsentEnabled.ToString().ToLowerInvariant()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.AllowUsingGravatarProfilePicture,
                settings.AllowUsingGravatarProfilePicture.ToString().ToLowerInvariant()
            );

            await UpdateUserManagementSessionTimeOutSettingsAsync(settings.SessionTimeOutSettings);
        }

        private async Task UpdateUserManagementSessionTimeOutSettingsAsync(SessionTimeOutSettingsEditDto settings)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.SessionTimeOut.IsEnabled,
                settings.IsEnabled.ToString().ToLowerInvariant()
            );
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.SessionTimeOut.TimeOutSecond,
                settings.TimeOutSecond.ToString()
            );
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.SessionTimeOut.ShowTimeOutNotificationSecond,
                settings.ShowTimeOutNotificationSecond.ToString()
            );
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.UserManagement.SessionTimeOut.ShowLockScreenWhenTimedOut,
                settings.ShowLockScreenWhenTimedOut.ToString()
            );
        }

        private async Task UpdateSecuritySettingsAsync(SecuritySettingsEditDto settings)
        {
            if (settings.UseDefaultPasswordComplexitySettings)
                await UpdatePasswordComplexitySettingsAsync(settings.DefaultPasswordComplexity);
            else
                await UpdatePasswordComplexitySettingsAsync(settings.PasswordComplexity);

            await UpdateUserLockOutSettingsAsync(settings.UserLockOut);
            await UpdateTwoFactorLoginSettingsAsync(settings.TwoFactorLogin);
            await UpdateOneConcurrentLoginPerUserSettingAsync(settings.AllowOneConcurrentLoginPerUser);
        }

        private async Task UpdatePasswordComplexitySettingsAsync(PasswordComplexitySetting settings)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.PasswordComplexity.RequireDigit,
                settings.RequireDigit.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.PasswordComplexity.RequireLowercase,
                settings.RequireLowercase.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.PasswordComplexity.RequireNonAlphanumeric,
                settings.RequireNonAlphanumeric.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.PasswordComplexity.RequireUppercase,
                settings.RequireUppercase.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.PasswordComplexity.RequiredLength,
                settings.RequiredLength.ToString()
            );
        }

        private async Task UpdateUserLockOutSettingsAsync(UserLockOutSettingsEditDto settings)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled,
                settings.IsEnabled.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.UserLockOut.DefaultAccountLockoutSeconds,
                settings.DefaultAccountLockoutSeconds.ToString());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.UserLockOut.MaxFailedAccessAttemptsBeforeLockout,
                settings.MaxFailedAccessAttemptsBeforeLockout.ToString());
        }

        private async Task UpdateTwoFactorLoginSettingsAsync(TwoFactorLoginSettingsEditDto settings)
        {
            if (_multiTenancyConfig.IsEnabled &&
                !await IsTwoFactorLoginEnabledForApplicationAsync()) //Two factor login can not be used by tenants if disabled by the host
                return;

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled,
                settings.IsEnabled.ToString().ToLowerInvariant());
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsRememberBrowserEnabled,
                settings.IsRememberBrowserEnabled.ToString().ToLowerInvariant());

            if (!_multiTenancyConfig.IsEnabled)
            {
                //These settings can only be changed by host, in a multitenant application.
                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEmailProviderEnabled,
                    settings.IsEmailProviderEnabled.ToString().ToLowerInvariant());
                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsSmsProviderEnabled,
                    settings.IsSmsProviderEnabled.ToString().ToLowerInvariant());
                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.UserManagement.TwoFactorLogin.IsGoogleAuthenticatorEnabled,
                    settings.IsGoogleAuthenticatorEnabled.ToString().ToLowerInvariant());
            }
        }

        private async Task UpdateOneConcurrentLoginPerUserSettingAsync(bool allowOneConcurrentLoginPerUser)
        {
            if (_multiTenancyConfig.IsEnabled) return;
            await SettingManager.ChangeSettingForApplicationAsync(
                AppSettings.UserManagement.AllowOneConcurrentLoginPerUser, allowOneConcurrentLoginPerUser.ToString());
        }

        private async Task UpdateExternalLoginSettingsAsync(ExternalLoginProviderSettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Facebook.AppId, input.Facebook.AppId);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Facebook.AppSecret, input.Facebook.AppSecret);

            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Google.ClientId, input.Google.ClientId);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Google.ClientSecret, input.Google.ClientSecret);
            await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Google.UserInfoEndpoint, input.Google.UserInfoEndpoint);

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Twitter,
                input.Twitter == null || !input.Twitter.IsValid() ? "" : input.Twitter.ToJsonString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.Microsoft,
                input.Microsoft == null || !input.Microsoft.IsValid() ? "" : input.Microsoft.ToJsonString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.OpenIdConnect,
                input.OpenIdConnect == null || !input.OpenIdConnect.IsValid() ? "" : input.OpenIdConnect.ToJsonString()
            );

            string openIdConnectMappedClaimsValue;
            if (input.OpenIdConnect == null || !input.OpenIdConnect.IsValid() ||
                input.OpenIdConnectClaimsMapping.IsNullOrEmpty())
                openIdConnectMappedClaimsValue =
                    await SettingManager.GetSettingValueForApplicationAsync(AppSettings.ExternalLoginProvider
                        .OpenIdConnectMappedClaims); //set default value
            else
                openIdConnectMappedClaimsValue = input.OpenIdConnectClaimsMapping.ToJsonString();

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.OpenIdConnectMappedClaims,
                openIdConnectMappedClaimsValue
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.Tenant.WsFederation,
                input.WsFederation == null || !input.WsFederation.IsValid() ? "" : input.WsFederation.ToJsonString()
            );

            string wsFederationMappedClaimsValue;
            if (input.WsFederation == null || !input.WsFederation.IsValid() ||
                input.WsFederationClaimsMapping.IsNullOrEmpty())
                wsFederationMappedClaimsValue =
                    await SettingManager.GetSettingValueForApplicationAsync(AppSettings.ExternalLoginProvider
                        .WsFederationMappedClaims); //set default value
            else
                wsFederationMappedClaimsValue = input.WsFederationClaimsMapping.ToJsonString();

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.ExternalLoginProvider.WsFederationMappedClaims,
                wsFederationMappedClaimsValue
            );

            ExternalLoginOptionsCacheManager.ClearCache();
        }

        #endregion Update Settings

        #region Others

        public async Task ClearLogo()
        {
            var tenant = await GetCurrentTenantAsync();

            if (!tenant.HasLogo()) return;

            if (tenant.LogoId != null)
            {
                var logoObject = await _binaryObjectManager.GetOrNullAsync(tenant.LogoId.Value);
                if (logoObject != null) await _binaryObjectManager.DeleteAsync(tenant.LogoId.Value);
            }

            tenant.ClearLogo();
        }

        public async Task ClearFavicon()
        {
            var tenant = await GetCurrentTenantAsync();

            if (!tenant.HasFavicon()) return;

            if (tenant.FaviconId != null)
            {
                var logoObject = await _binaryObjectManager.GetOrNullAsync(tenant.FaviconId.Value);
                if (logoObject != null) await _binaryObjectManager.DeleteAsync(tenant.FaviconId.Value);
            }

            tenant.ClearFavicon();
        }

        public async Task ClearMenuItemDefault()
        {
            var tenant = await GetCurrentTenantAsync();

            if (!tenant.HasMenuItemDefault()) return;

            if (tenant.MenuItemDefaultID != null)
            {
                var menuItemObject = await _binaryObjectManager.GetOrNullAsync(tenant.MenuItemDefaultID.Value);
                if (menuItemObject != null) await _binaryObjectManager.DeleteAsync(tenant.MenuItemDefaultID.Value);
            }

            tenant.ClearMenuItemDefault();
        }

        public async Task ClearCustomCss()
        {
            var tenant = await GetCurrentTenantAsync();

            if (!tenant.CustomCssId.HasValue) return;

            var cssObject = await _binaryObjectManager.GetOrNullAsync(tenant.CustomCssId.Value);
            if (cssObject != null) await _binaryObjectManager.DeleteAsync(tenant.CustomCssId.Value);

            tenant.CustomCssId = null;
        }

        public async Task<string> CreateCompanyLogo(CompanyLogoInfo input)
        {
            if (!input.UploadedImageToken.IsNullOrWhiteSpace())
            {
                var image = _tempFileCacheManager.GetFile(input.UploadedImageToken);

                if (image != null)
                {
                    var result = await _cloudImageUploader.UploadImage(input.Name, image);

                    if (result != null)
                    {
                    }
                }
            }

            return string.Empty;
        }

        private async Task<string> GetTermsAndConditions()
        {
            var result = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.TermAndConditionSetting.TermAndCondition,
                AbpSession.GetTenantId());

            return result;
        }

        private async Task<ReferralEarningDto> GetReferralEarningSetting()
        {
            try
            {
                var referralEarningDto = new ReferralEarningDto();

                var referralEarningTypeStr =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ReferralEarningSetting.ReferralEarningType, AbpSession.GetTenantId());
                int.TryParse(referralEarningTypeStr, out var referralEarningType);
                referralEarningDto.ReferralEarnType = (ReferralEarningType)referralEarningType;

                var referralEarningValueStr =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ReferralEarningSetting.ReferralEarningValue, AbpSession.GetTenantId());
                int.TryParse(referralEarningValueStr, out var referralEarningValue);
                referralEarningDto.ReferralEarnValue = referralEarningValue;

                return referralEarningDto;
            }
            catch
            {
                return default;
            }
        }

        public async Task<ShippingProviderDto> GetShippingProviderSetting()
        {
            try
            {
                var shippingProviderDto = new ShippingProviderDto();

                var shippingProviderStr =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ShippingProviderSetting.ShippingProvider, AbpSession.GetTenantId());
                int.TryParse(shippingProviderStr, out var shippingProvider);
                shippingProviderDto.ShippingProvider = (ShippingProvider)shippingProvider;

                shippingProviderDto.ShippingProviderSetting =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ShippingProviderSetting.ShippingSetting, AbpSession.GetTenantId());
                shippingProviderDto.ShippingGoGoVanSetting =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ShippingProviderSetting.ShippingGoGoVan, AbpSession.GetTenantId());
                shippingProviderDto.ShippingLaLaMoveSetting =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ShippingProviderSetting.ShippingLalaMove, AbpSession.GetTenantId());
                shippingProviderDto.ShippingDunzoSetting =
                    await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ShippingProviderSetting.ShippingDunzo, AbpSession.GetTenantId());

                return shippingProviderDto;
            }
            catch
            {
                return default;
            }
        }

        private async Task<FileDateTimeFormatDto> GetFileDateTimeFormatSetting()
        {
            try
            {
                var excelDateTimeFormatSetting = new FileDateTimeFormatDto
                {
                    FileDateTimeFormat = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.FileDateTimeFormatSetting.FileDateTimeFormat, AbpSession.GetTenantId()),
                    FileDateFormat = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.FileDateTimeFormatSetting.FileDateFormat, AbpSession.GetTenantId()),
                    FileDayMonthFormat = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.FileDateTimeFormatSetting.FileDayMonthFormat, AbpSession.GetTenantId())
                };

                return excelDateTimeFormatSetting;
            }
            catch
            {
                return default;
            }
        }

        private async Task<string> GetTiffinPopup()
        {
            try
            {
                var tiffinPopup =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.TiffinPopup,
                        AbpSession.GetTenantId());

                return tiffinPopup;
            }
            catch
            {
                return default;
            }
        }

        private async Task<int> GetTiffinAllowOrderInFutureDays()
        {
            try
            {
                var tiffinAllowOrderInFutureDays =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.TiffinAllowOrderInFutureDays,
                        AbpSession.GetTenantId());

                if (int.TryParse(tiffinAllowOrderInFutureDays, out var tiffinAllowOrderInFutureDaysInt))
                    return tiffinAllowOrderInFutureDaysInt;

                return -1;
            }
            catch
            {
                return -1;
            }
        }

        private async Task<int> GetTiffinCutOffDays()
        {
            try
            {
                var tiffinCutOffDays =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.TiffinCutOffDays,
                        AbpSession.GetTenantId());

                if (int.TryParse(tiffinCutOffDays, out var tiffinCutOffDaysInt)) return tiffinCutOffDaysInt;

                return 0;
            }
            catch
            {
                return 0;
            }
        }

        private async Task<int> GetCliqueCutOffDays()
        {
            try
            {
                var cliqueCutOffDays =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.CliqueCutOffDays,
                        AbpSession.GetTenantId());

                if (int.TryParse(cliqueCutOffDays, out var cliqueCutOffDaysInt)) return cliqueCutOffDaysInt;

                return 0;
            }
            catch
            {
                return 0;
            }
        }

        private async Task<int> GetCliqueAllowOrderInFutureDays()
        {
            try
            {
                var cliqueAllowOrderInFutureDays =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.CliqueAllowOrderInFutureDays,
                        AbpSession.GetTenantId());

                if (int.TryParse(cliqueAllowOrderInFutureDays, out var cliqueAllowOrderInFutureDaysInt))
                    return cliqueAllowOrderInFutureDaysInt;

                return -1;
            }
            catch
            {
                return -1;
            }
        }

        private async Task<bool> GetTiffinIsEnableDelivery()
        {
            try
            {
                var tiffinIsEnableDelivery =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.TiffinIsEnableDelivery,
                        AbpSession.GetTenantId());

                if (bool.TryParse(tiffinIsEnableDelivery, out var tiffinIsEnableDeliveryBool))
                    return tiffinIsEnableDeliveryBool;

                return false;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> GetIsTiffinEnableStudentInformation()
        {
            try
            {
                var isTiffinEnableStudentInformation =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.IsTiffinEnableStudentInformation,
                        AbpSession.GetTenantId());

                if (bool.TryParse(isTiffinEnableStudentInformation, out var isTiffinEnableStudentInformationBool))
                    return isTiffinEnableStudentInformationBool;

                return false;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> GetCliqueAddressObligatory()
        {
            try
            {
                var cliqueAddressObligatory =
                    await SettingManager.GetSettingValueForTenantAsync(AppSettings.CliqueAddressObligatory, AbpSession.GetTenantId());

                if (bool.TryParse(cliqueAddressObligatory, out var cliqueAddressObligatoryBool))
                    return cliqueAddressObligatoryBool;

                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<OrderSettingDto> GetOrderSetting()
        {
            var result = new OrderSettingDto();
            var orderSetting =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.Accepted,
                    AbpSession.GetTenantId());
            var signUp =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.Signup,
                    AbpSession.GetTenantId());
            var autoCallDevelir =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.AutoRequestDeliver,
                    AbpSession.GetTenantId());
            var width = await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Width,
                AbpSession.GetTenantId());
            var height =
                await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Height,
                    AbpSession.GetTenantId());
            var askTheMobileNumberFirst =
                await SettingManager.GetSettingValueForTenantAsync<bool>(
                    AppSettings.OrderSetting.AskTheMobileNumberFirst, AbpSession.GetTenantId());
            result.Accepted = orderSetting;
            result.Width = width;
            result.Height = height;
            result.AutoRequestDeliver = autoCallDevelir;
            result.AskTheMobileNumberFirst = askTheMobileNumberFirst;
            result.SignUp = signUp;
            return result;
        }

        public async Task<SignUpFormSettingDto> GetSignUpFormSetting()
        {
            try
            {
                var result = new SignUpFormSettingDto
                {
                    HintImageUrl = await SettingManager.GetSettingValueForTenantAsync(AppSettings.SignUpFormSetting.HintImageUrl,
                        AbpSession.GetTenantId())
                };
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<ConnectSettingsDto> GetConnectSetting()
        {
            try
            {
                var result = new ConnectSettingsDto
                {
                    WeekDay = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.WeekDay,
                        AbpSession.GetTenantId()),
                    WeekEnd = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.WeekEnd,
                        AbpSession.GetTenantId()),
                    Schedules = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.ConnectSettings.Schedules, AbpSession.GetTenantId()),
                    SyncUrl = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.SyncUrl,
                        AbpSession.GetTenantId()),
                    SyncTenantId =
                        await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.SyncTenantId,
                            AbpSession.GetTenantId()),
                    SyncTenantName =
                        await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.SyncTenantName,
                            AbpSession.GetTenantId()),
                    SyncUser = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.SyncUser,
                        AbpSession.GetTenantId()),
                    SyncPassword =
                        await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.SyncPassword,
                            AbpSession.GetTenantId())
                };
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<TouchSaleSettingsDto> GetTouchSaleSettings()
        {
            try
            {
                var result = new TouchSaleSettingsDto
                {
                    Currency = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.TouchSaleSettings.Currency,
                        AbpSession.GetTenantId()),
                    Decimals = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.TouchSaleSettings.Decimals,
                        AbpSession.GetTenantId()),
                    Rounding = await SettingManager.GetSettingValueForTenantAsync(
                        AppSettings.TouchSaleSettings.Rounding,
                        AbpSession.GetTenantId())
                };

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task UpdateOrderSetting(OrderSettingDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.Accepted,
                input.Accepted.ToString()
            );
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.AutoRequestDeliver,
                input.AutoRequestDeliver.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.AskTheMobileNumberFirst,
                input.AskTheMobileNumberFirst.ToString()
            );

            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.Signup,
                input.SignUp.ToString()
            );
        }

        public async Task UpdateStandImageSize(OrderSettingDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.Width,
                input.Width.ToString()
            );
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.GetTenantId(),
                AppSettings.OrderSetting.Height,
                input.Height.ToString()
            );
        }

        #endregion Others
    }
}
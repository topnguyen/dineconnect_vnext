﻿namespace DinePlan.DineConnect.Utility.Rest
{
    namespace DinePlan.Common.Model.Sync
    {
        public class AuthRequest
        {
            public string tenancyName { get; set; }
            public string usernameOrEmailAddress { get; set; }
            public string password { get; set; }
        }
    }

}

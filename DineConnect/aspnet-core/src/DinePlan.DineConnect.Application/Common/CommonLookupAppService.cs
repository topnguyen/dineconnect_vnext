﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.Editions.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Common
{
	[AbpAuthorize]
	public class CommonLookupAppService : DineConnectAppServiceBase, ICommonLookupAppService
	{
		private readonly EditionManager _editionManager;
		private readonly IRepository<ProductGroup> _productGroupRepository;
		private readonly IRepository<Department> _departmentRepository;

		public CommonLookupAppService(EditionManager editionManager, IRepository<ProductGroup> productGroupRepository, IRepository<Department> departmentRepository)
		{
			_editionManager = editionManager;
			_productGroupRepository = productGroupRepository;
			_departmentRepository = departmentRepository;
		}

		public async Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false)
		{
			var subscribableEditions = (await _editionManager.Editions.Cast<SubscribableEdition>().ToListAsync())
				.WhereIf(onlyFreeItems, e => e.IsFree)
				.OrderBy(e => e.MonthlyPrice);

			return new ListResultDto<SubscribableEditionComboboxItemDto>(
				subscribableEditions.Select(e => new SubscribableEditionComboboxItemDto(e.Id.ToString(), e.DisplayName, e.IsFree)).ToList()
			);
		}

		public async Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input)
		{
			if (AbpSession.TenantId != null)
			{
				//Prevent tenants to get other tenant's users.
				input.TenantId = AbpSession.TenantId;
			}

			using (CurrentUnitOfWork.SetTenantId(input.TenantId))
			{
				var query = UserManager.Users
				.WhereIf(
					!input.Filter.IsNullOrWhiteSpace(),
					u =>
						u.Name.Contains(input.Filter) ||
						u.Surname.Contains(input.Filter) ||
						u.UserName.Contains(input.Filter) ||
						u.EmailAddress.Contains(input.Filter)
				);

				var userCount = await query.CountAsync();
				var users = await query
					.OrderBy(u => u.Name)
					.ThenBy(u => u.Surname)
					.PageBy(input)
					.ToListAsync();

				return new PagedResultDto<NameValueDto>(
					userCount,
					users.Select(u =>
						new NameValueDto(
							u.FullName + " (" + u.EmailAddress + ")",
							u.Id.ToString()
							)
						).ToList()
					);
			}
		}

		public GetDefaultEditionNameOutput GetDefaultEditionName()
		{
			return new GetDefaultEditionNameOutput
			{
				Name = EditionManager.DefaultEditionName
			};
		}

		//get combobox
		public async Task<ListResultDto<ComboboxItemDto>> GetProductGroupsForCombobox(string input)
		{
			var result = await _productGroupRepository.GetAll()
						//.WhereIf(!string.IsNullOrWhiteSpace(input), e => e.Code.Contains(input) || e.Name.Contains(input))
						.Select(g => new ComboboxItemDto(g.Id.ToString(), g.Name))
						.ToListAsync();

			return new ListResultDto<ComboboxItemDto>(result);
		}

		public async Task<ListResultDto<ComboboxItemDto>> GetDepartmentForLookupTable()
		{
			var returnList = await _departmentRepository.GetAll()
				.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
				.ToListAsync();

			return new ListResultDto<ComboboxItemDto>(returnList);
		}

		public string GetConditionFilter()
		{
			var rfilter = new
			{
				Total = new
				{
					field = "Total",
					id = "Total",
					name = "Total",
					type = "number",
					operators = new[] { "equal", "less", "less_or_equal", "greater", "greater_or_equal" },
					validation = new
					{
						value = 0,
						min = 0,
						step = 0.1
					}
				},
				Department = new
				{
					field = "Department",
					id = "Department",
					name = "Department",
					type = "string"
				},
				DepartmentGroup = new
				{
					field = "DepartmentGroup",
					id = "DepartmentGroup",
					name = "DepartmentGroup",
					type = "string"
				},

				Member = new
				{
					field = "Member",
					id = "Member",
					name = "IsMember",
					type = "boolean"
				}
			};

			return JsonConvert.SerializeObject(rfilter);
		}
	}
}
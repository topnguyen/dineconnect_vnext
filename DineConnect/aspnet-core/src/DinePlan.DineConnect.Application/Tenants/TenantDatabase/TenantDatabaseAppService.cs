﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Framework;
using DinePlan.DineConnect.Tenants.TenantDatabases;
using DinePlan.DineConnect.Tenants.TenantDatabases.Dtos;
using Microsoft.EntityFrameworkCore;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tenants.TenantDatabases
{
    [AbpAuthorize(AppPermissions.Pages_Administration_Host_TenantDatabase)]
    public class TenantDatabaseAppService : ApplicationService, ITenantDatabaseAppService
    {
        private readonly IRepository<TenantDatabase> _tenantDatabaseRepository;

        public TenantDatabaseAppService(IRepository<TenantDatabase> tenantDatabaseRepository)
        {
            _tenantDatabaseRepository = tenantDatabaseRepository;
        }

        public async Task<PagedResultDto<TenantDatabaseListDto>> GetTenantDatabases(GetTenantDatabaseInput input)
        {
            var query = _tenantDatabaseRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    t => t.Name.Contains(input.Filter)
                );

            if (!input.Sorting.IsNullOrEmpty())
                query = query.OrderBy(input.Sorting);

            var tenantDatabaseCount = await query.CountAsync();
            var tenantDatabases = await query.PageBy(input).ToListAsync();

            return new PagedResultDto<TenantDatabaseListDto>(
                tenantDatabaseCount,
                ObjectMapper.Map<List<TenantDatabaseListDto>>(tenantDatabases)
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_TenantDatabase_Create, AppPermissions.Pages_Administration_Host_TenantDatabase_Edit)]
        public async Task CreateOrUpdateTenantDatabase(CreateOrUpdateTenantDatabaseInput input)
        {
            var tenantDatabase = ObjectMapper.Map<TenantDatabase>(input);
            await _tenantDatabaseRepository.InsertOrUpdateAsync(tenantDatabase);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_TenantDatabase_Edit)]
        public async Task<TenantDatabaseEditDto> GetTenantDatabaseForEdit(GetTenantDatabaseForEditInput input)
        {
            var tenantDatabase = await _tenantDatabaseRepository.GetAsync(input.Id);
            return ObjectMapper.Map<TenantDatabaseEditDto>(tenantDatabase);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_TenantDatabase_Delete)]
        public async Task DeleteTenantDatabase(EntityDto input)
        {
            await _tenantDatabaseRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants)]
        public async Task<List<TenantDatabaseComboboxItemDto>> GetTenantDatabaseForCombobox(int? selectedEditionId = null)
        {
            var tenantDatabases = await _tenantDatabaseRepository.GetAllListAsync();

            var tenantDatabasesItems = new ListResultDto<TenantDatabaseComboboxItemDto>(tenantDatabases.Select(
                t => new TenantDatabaseComboboxItemDto(t.Id.ToString(), t.Name, false)).ToList()).Items.ToList();

            if (tenantDatabasesItems.Count > 0)
            {
                if (selectedEditionId.HasValue)
                {
                    var selectedEdition = tenantDatabasesItems.FirstOrDefault(e => e.Value == selectedEditionId.Value.ToString());
                    if (selectedEdition != null)
                    {
                        selectedEdition.IsSelected = true;
                    }
                }
                else
                {
                    tenantDatabasesItems[0].IsSelected = true;
                }
            }

            return tenantDatabasesItems;
        }
    }
}

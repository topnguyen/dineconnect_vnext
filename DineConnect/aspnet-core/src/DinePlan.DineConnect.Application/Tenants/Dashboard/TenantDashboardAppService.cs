﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Tenants.Dashboard.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tenants.Dashboard
{
    [DisableAuditing]
    
    public class TenantDashboardAppService : DineConnectAppServiceBase, ITenantDashboardAppService
    {
        private readonly IRepository<Ticket> _ticketRepository;
        private readonly ILocationAppService _locService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public TenantDashboardAppService(
             IRepository<Ticket> ticketRepository
            ,ILocationAppService locService
            ,IUnitOfWorkManager unitOfWorkManager
            )
        {
            _ticketRepository = ticketRepository;
            _locService = locService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        public GetMemberActivityOutput GetMemberActivity()
        {
            return new GetMemberActivityOutput
            (
                DashboardRandomDataGenerator.GenerateMemberActivities()
            );
        }

        public GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input)
        {
            var output = new GetDashboardDataOutput
            {
                TotalProfit = DashboardRandomDataGenerator.GetRandomInt(5000, 9000),
                NewFeedbacks = DashboardRandomDataGenerator.GetRandomInt(1000, 5000),
                NewOrders = DashboardRandomDataGenerator.GetRandomInt(100, 900),
                NewUsers = DashboardRandomDataGenerator.GetRandomInt(50, 500),
                SalesSummary = DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod),
                Expenses = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Growth = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Revenue = DashboardRandomDataGenerator.GetRandomInt(1000, 9000),
                TotalSales = DashboardRandomDataGenerator.GetRandomInt(10000, 90000),
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                DailySales = DashboardRandomDataGenerator.GetRandomArray(30, 10, 50),
                ProfitShares = DashboardRandomDataGenerator.GetRandomPercentageArray(3)
            };

            return output;
        }

        public async Task<GetTopStatsOutput> GetTopStats(GetChartInput input)
        {
            var result = new GetTopStatsOutput();
            var allTickets = GetAllTickets(input).Include(x=>x.Orders);
            if (!allTickets.Any()) 
                 return await Task.FromResult(result);
           
            result.TotalTicketCount =await allTickets.CountAsync();
            result.TotalAmount = await allTickets.DefaultIfEmpty().SumAsync(a => a.TotalAmount);
            result.AverageTicketAmount = result.TotalAmount / result.TotalTicketCount;
            var totalOrder = await allTickets.SelectMany(x => x.Orders).CountAsync();
            result.TotalOrderCount = totalOrder;
            var myOrders = allTickets.SelectMany(a => a.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory);
            result.TotalItemSold =await myOrders.SumAsync(b => b.Quantity);
            return result;
        }

        public GetProfitShareOutput GetProfitShare()
        {
            return new GetProfitShareOutput
            {
                ProfitShares = DashboardRandomDataGenerator.GetRandomPercentageArray(3)
            };
        }

        public GetDailySalesOutput GetDailySales()
        {
            return new GetDailySalesOutput
            {
                DailySales = DashboardRandomDataGenerator.GetRandomArray(30, 10, 50)
            };
        }

        public GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input)
        {
            var salesSummary = DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod);
            return new GetSalesSummaryOutput(salesSummary)
            {
                Expenses = DashboardRandomDataGenerator.GetRandomInt(0, 3000),
                Growth = DashboardRandomDataGenerator.GetRandomInt(0, 3000),
                Revenue = DashboardRandomDataGenerator.GetRandomInt(0, 3000),
                TotalSales = DashboardRandomDataGenerator.GetRandomInt(0, 3000)
            };
        }

        public GetRegionalStatsOutput GetRegionalStats()
        {
            return new GetRegionalStatsOutput(
                DashboardRandomDataGenerator.GenerateRegionalStat()
            );
        }

        public GetGeneralStatsOutput GetGeneralStats()
        {
            return new GetGeneralStatsOutput
            {
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100)
            };
        }

        public async Task<List<ChartOutputDto>> GetTransactionChart(GetChartInput input)
        {
            var result =await GetChartTickets(input).SelectMany(a => a.Transactions)
                .GroupBy(a => a.TransactionType.Name).Select(a1 => new ChartOutputDto
                {
                    name = a1.Key,
                    y = a1.Sum(a => a.Amount)
                }).ToListAsync();

            return result;
        }

        public async Task<List<ChartOutputDto>> GetPaymentChart(GetChartInput input)
        {
            var result =
               await GetChartTickets(input).SelectMany(a => a.Payments)
                    .GroupBy(a => a.PaymentType.Name).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Amount)
                    }).ToListAsync();

            return result;
        }

        public async Task<List<ChartOutputDto>> GetDepartmentChart(GetChartInput input)
        {
            var result =await GetChartTickets(input).GroupBy(l => l.DepartmentName)
               .Select(cl => new ChartOutputDto
               {
                   name = cl.Key,
                   y = cl.Sum(c => c.TotalAmount)
               }).ToListAsync();

            return result;
        }

        public async Task<List<ChartOutputDto>> GetItemChart(GetChartInput input)
        {
            var result =
               await GetChartTickets(input)
                    .SelectMany(a => a.Orders)
                    .Where(a => a.CalculatePrice && a.DecreaseInventory)
                    .GroupBy(a => a.MenuItemName).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Quantity)
                    }).OrderByDescending(a => a.y).Take(10).ToListAsync();

            return result;
        }
        private IQueryable<Ticket> GetChartTickets(GetChartInput input)
        {
            var tickets = GetAllTickets(input);
            var outTickets = tickets.Where(a => a.TenantId == AbpSession.TenantId
                                                && input.Location > 0
                ? a.LocationId == input.Location
                : true && a.IsClosed);

            return outTickets;
        }
        public IQueryable<Ticket> GetAllTickets(IDateInput input)
        {
            IQueryable<Ticket> tickets;

            if (!string.IsNullOrEmpty(input.TicketNo))
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    tickets = _ticketRepository.GetAll()
                        .Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
                    return tickets;
                }

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                tickets = _ticketRepository.GetAll();
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate.Date;

                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTimeTruc >= mySt
                                    &&
                                    a.LastPaymentTimeTruc <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }
            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            tickets = tickets.Where(a => a.Credit == input.Credit);

            return tickets;
        }
        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate)
            {
                return true;
            }

            var returnStatus = true;

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                input.EndDate = input.EndDate.AddDays(1);
            }
            else
            {
                returnStatus = false;
            }

            return returnStatus;
        }
    }
}
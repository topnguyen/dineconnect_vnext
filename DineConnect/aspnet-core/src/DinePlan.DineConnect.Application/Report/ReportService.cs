

using Microsoft.Extensions.Configuration;
using DinePlan.DineConnect.ReportService;
using DinePlan.DineConnect.Configuration;
using MySql.Data.MySqlClient;
using Microsoft.Data.SqlClient;
using System.Data;
using System;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.ReportService
{
    public class ReportService : DineConnectAppServiceBase, IReportService
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ReportService(
            IAppConfigurationAccessor configurationAccessor)
        {
            _appConfiguration = configurationAccessor.Configuration;
        }

        public async Task<DataTable> WheelPayments(string fromDate,string toDate)
        {
            try
            {
                var connectionString = _appConfiguration[$"ConnectionStrings:{DineConnectConsts.ConnectionStringName}"];
                MySqlConnectionStringBuilder sqlConStrBuilder = new MySqlConnectionStringBuilder(connectionString);
                DataTable dt = new DataTable();
                string sql = "SELECT * FROM WheelPayments";
                if (!(fromDate == "" && toDate == "" || fromDate == null && toDate == null))
                {
                    DateTime from = DateTime.Parse(fromDate);
                    DateTime to = DateTime.Parse(toDate);
                    sql += " WHERE CreationTime BETWEEN '" + from + "' and '" + to + "'";
                }
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sql))
                    {
                        cmd.Connection = conn;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            //sda.Fill(dt);
                            await Task.Run(() =>
                            {
                                sda.Fill(dt);
                                string dir = "D:/eDine/DineConnect_VNext/DineConnect/file-report/" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                                using (var sw = new System.IO.StreamWriter(dir))
                                {
                                    sw.WriteLine(string.Format("DataTable:  WheelPayment.\n"));
                                    sw.WriteLine(string.Format("-------------------------------------------------\n"));
                                    foreach (DataRow dataRow in dt.Rows)
                                    {
                                        sw.WriteLine(string.Format("-------------------------------------------------\n"));
                                        foreach (var item in dataRow.ItemArray)
                                        {
                                            Console.WriteLine(item);
                                            sw.WriteLine(string.Format("Col:  {0}", item.ToString()));
                                        }
                                        sw.WriteLine(string.Format("-------------------------------------------------\n"));
                                    }
                                }
                            });
                        }
                    }
                }
                return dt;
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Shipment.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.TrackDrivers
{
	public class ShipmentAnalyticAppService : DineConnectAppServiceBase, IShipmentAnalyticAppService
	{
		private readonly IRepository<ShipmentRoute> _routeRepository;
		private readonly IRepository<ShipmentOrder> _shipmentOrderRepository;
		private readonly IRepository<ShipmentItem> _shipmentItemRepository;
		private readonly IRepository<ShipmentRoutePoint> _routePointRepository;
		private readonly IRepository<ShipmentAddress> _shipmentAddressRepository;
		private readonly IRepository<ShipmentClientAddress, long> _shipmentClientAddressRepository;
		private readonly IRepository<ShipmentClient> _shipmentClientRepository;
		private readonly IRepository<ShipmentRoutePointOrder, long> _routePointOrderRepository;
		private readonly IRepository<ShipmentDepot> _shipmentDepotRepository;
		private readonly IRepository<ShipmentDriver> _shipmentDriverRepository;

		public ShipmentAnalyticAppService(
			IRepository<ShipmentRoute> routeRepository,
			IRepository<ShipmentRoutePoint> routePointRepository,
			IRepository<ShipmentOrder> shipmentOrderRepository,
			IRepository<ShipmentItem> shipmentItemRepository,
			IRepository<ShipmentRoutePointOrder, long> routePointOrderRepository,
			IRepository<ShipmentAddress> shipmentAddressRepository,
			IRepository<ShipmentClientAddress, long> shipmentClientAddressRepository,
			IRepository<ShipmentClient> shipmentClientRepository,
			IRepository<ShipmentDepot> shipmentDepotRepository,
			IRepository<ShipmentDriver> shipmentDriverRepository
			)
		{
			_routeRepository = routeRepository;
			_routePointRepository = routePointRepository;
			_shipmentOrderRepository = shipmentOrderRepository;
			_shipmentItemRepository = shipmentItemRepository;
			_shipmentAddressRepository = shipmentAddressRepository;
			_shipmentClientAddressRepository = shipmentClientAddressRepository;
			_shipmentClientRepository = shipmentClientRepository;
			_routePointOrderRepository = routePointOrderRepository;
			_shipmentDepotRepository = shipmentDepotRepository;
			_shipmentDriverRepository = shipmentDriverRepository;
		}

		public async Task<PagedResultDto<ShipmentAnalyticDto>> GetAll(GetAllShipmentAnalyticInput input)
		{
			var orderList = _routePointOrderRepository
						 .GetAll()
						 .Include(t => t.RoutePoint)
						 .ThenInclude(t => t.Route)
						 .ThenInclude(t=>t.Driver)
						 .Include(t => t.Order)
						 .ThenInclude(t=>t.ShipmentDepot)
						 .Where(x => x.Order.Date >= input.StartDate && x.Order.Date <= input.EndDate)
						 .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
						   e => e.Order.Type.Contains(input.Filter) ||
							e.Order.Number.Contains(input.Filter) ||
							e.RoutePoint.Route.Driver.Driver.Contains(input.Filter) ||
							e.RoutePoint.Route.Code.Contains(input.Filter)
						   );

			var pagedAndFiltered = orderList
				   .OrderBy(input.Sorting ?? "id asc")
				   .PageBy(input).ToList();

			var ouput = pagedAndFiltered.Select(t =>
				 {
					 var item = ObjectMapper.Map<ShipmentAnalyticDto>(t.Order);
					 item.TrackKey = t.TrackKey;
					 item.ArrivalDate = t.RoutePoint.ArrivalDate;
					 item.RouteCode = t.RoutePoint.Route.Code;
					 item.Driver = t.RoutePoint.Route?.Driver?.Driver;
					 item.DriverPhone = t.RoutePoint.Route?.Driver?.Phone;
					 item.DepotAddress = t.Order.ShipmentDepot?.FullAddress;
					 return item;
				 }).ToList();

			var totalCount = await orderList.CountAsync();

			return new PagedResultDto<ShipmentAnalyticDto>(
				totalCount,
				ouput
			);

		}

	}
}
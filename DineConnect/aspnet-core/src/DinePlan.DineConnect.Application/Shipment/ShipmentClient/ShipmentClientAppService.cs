﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Shipment.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentClientAppService : DineConnectAppServiceBase, IShipmentClientAppService
    {
        private readonly IRepository<ShipmentClient> _shipmentClientRepository;

        public ShipmentClientAppService(IRepository<ShipmentClient> shipmentClientRepository)
        {
            _shipmentClientRepository = shipmentClientRepository;
        }

        public async Task<PagedResultDto<ShipmentClientDto>> GetAll(GetAllShipmentClientInput input)
        {
            var clients = _shipmentClientRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        e=> e.Name.Contains(input.Filter)
                         || e.ExtCode.Contains(input.Filter)
                         || e.Phone.Contains(input.Filter)
                         || e.Email.Contains(input.Filter)
                        );

            var pagedAndFiltered = clients
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var output = pagedAndFiltered
                                 .Select(o => ObjectMapper.Map<ShipmentClientDto>(o));

            var totalCount = await clients.CountAsync();

            return new PagedResultDto<ShipmentClientDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        public async Task<CreateOrEditShipmentClientDto> GetShipmentClientEdit(EntityDto input)
        {
            var client = await _shipmentClientRepository.FirstOrDefaultAsync(input.Id);
            var output = ObjectMapper.Map<CreateOrEditShipmentClientDto>(client);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShipmentClientDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditShipmentClientDto input)
        {
            var Client = ObjectMapper.Map<ShipmentClient>(input);
            if (AbpSession.TenantId != null)
            {
                Client.TenantId = (int)AbpSession.TenantId;
            }

            await _shipmentClientRepository.InsertAsync(Client);
        }

        protected virtual async Task Update(CreateOrEditShipmentClientDto input)
        {
            var client = await _shipmentClientRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, client);
        }

        public async Task Delete(EntityDto input)
        {
            await _shipmentClientRepository.DeleteAsync(input.Id);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditShipmentClientDto input)
        {
            var exists = _shipmentClientRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.ExtCode.Equals(input.ExtCode)))
            {
                throw new UserFriendlyException($"Client code {input.ExtCode} aleady exists");
            }
            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException($"Client name {input.Name} aleady exists");
            }
        }
    }
}
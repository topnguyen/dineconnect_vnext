﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Shipment.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.TrackDrivers
{
    public class ShipmentDriverAppService : DineConnectAppServiceBase, IShipmentDriverAppService
    {
        private readonly IRepository<ShipmentDriver> _trackDepotRepository;

        public ShipmentDriverAppService(IRepository<ShipmentDriver> trackDepotRepository)
        {
            _trackDepotRepository = trackDepotRepository;
        }

        public async Task<PagedResultDto<TrackShipmentDto>> GetAll(GetAllShipmentDriverInput input)
        {
            var trackDepots = _trackDepotRepository.GetAll()
                        .Include(x=>x.TrackDepot)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                         e => e.Driver.Contains(input.Filter)
                         || e.Vehicle.Contains(input.Filter)
                         || e.Phone.Contains(input.Filter)
                         || e.CompanyId.Contains(input.Filter)
                         || e.UserName.Contains(input.Filter)
                         || e.HomeAddress.Contains(input.Filter)
                         || e.Zone.Contains(input.Filter)
                         );

            var pagedAndFiltered = trackDepots
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var output = pagedAndFiltered
                                 .Select(o => ObjectMapper.Map<TrackShipmentDto>(o));

            var totalCount = await trackDepots.CountAsync();

            return new PagedResultDto<TrackShipmentDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        public async Task<CreateOrEditShipmentDriverDto> GetDriverForEdit(EntityDto input)
        {
            var trackDepot = await _trackDepotRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditShipmentDriverDto>(trackDepot);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShipmentDriverDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditShipmentDriverDto input)
        {
            var trackDepot = ObjectMapper.Map<ShipmentDriver>(input);

            if (AbpSession.TenantId != null)
            {
                trackDepot.TenantId = (int)AbpSession.TenantId;
            }

            await _trackDepotRepository.InsertAsync(trackDepot);
        }

        protected virtual async Task Update(CreateOrEditShipmentDriverDto input)
        {
            var trackDepot = await _trackDepotRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, trackDepot);
        }

        public async Task Delete(EntityDto input)
        {
            await _trackDepotRepository.DeleteAsync(input.Id);
        }
    }
}
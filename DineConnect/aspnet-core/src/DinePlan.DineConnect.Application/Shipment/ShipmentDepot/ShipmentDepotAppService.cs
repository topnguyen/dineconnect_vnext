﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Shipment.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentDepotAppService : DineConnectAppServiceBase, IShipmentDepotAppService
    {
        private readonly IRepository<ShipmentDepot> _shipmentDepotRepository;

        public ShipmentDepotAppService(IRepository<ShipmentDepot> trackDepotRepository)
        {
            _shipmentDepotRepository = trackDepotRepository;
        }

        public async Task<PagedResultDto<ShipmentDepotDto>> GetAll(GetAllShipmentDepotInput input)
        {
            var trackDepots = _shipmentDepotRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        e=> e.Code.Contains(input.Filter)
                         || e.Street.Contains(input.Filter)
                         || e.City.Contains(input.Filter)
                         || e.StateProvinceCountry.Contains(input.Filter)
                         || e.PostalCode.Contains(input.Filter)
                         || e.Latitude.ToString().Contains(input.Filter)
                         || e.Longitude.ToString().Contains(input.Filter)
                        );

            var pagedAndFiltered = trackDepots
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var output = pagedAndFiltered
                                 .Select(o => ObjectMapper.Map<ShipmentDepotDto>(o));

            var totalCount = await trackDepots.CountAsync();

            return new PagedResultDto<ShipmentDepotDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        public async Task<CreateOrEditShipmentDepotDto> GetTrackDepotEdit(EntityDto input)
        {
            var trackDepot = await _shipmentDepotRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditShipmentDepotDto>(trackDepot);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShipmentDepotDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditShipmentDepotDto input)
        {
            var depot = ObjectMapper.Map<ShipmentDepot>(input);

            if (AbpSession.TenantId != null)
            {
                depot.TenantId = (int)AbpSession.TenantId;
            }

            await _shipmentDepotRepository.InsertAsync(depot);
        }

        protected virtual async Task Update(CreateOrEditShipmentDepotDto input)
        {
            var depot = await _shipmentDepotRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, depot);
        }
        protected virtual void ValidateForCreateOrUpdate(CreateOrEditShipmentDepotDto input)
        {
            var exists = _shipmentDepotRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException($"Depot code {input.Code} aleady exists");
            }
        }

        public async Task Delete(EntityDto input)
        {
            await _shipmentDepotRepository.DeleteAsync(input.Id);
        }
    }
}
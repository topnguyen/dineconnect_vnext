﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
	public class ShipmentClientAddressAppService : DineConnectAppServiceBase, IShipmentClientAddressAppService
	{
		private readonly IRepository<ShipmentClient> _shipmentClientRepository;
		private readonly IRepository<ShipmentAddress> _shipmentAddressRepository;
		private readonly IRepository<ShipmentClientAddress, long> _shipmentClientAddressRepository;

		public ShipmentClientAddressAppService(
			IRepository<ShipmentClient> shipmentClientRepository,
			IRepository<ShipmentClientAddress, long> shipmentClientAddressRepository,
			IRepository<ShipmentAddress> shipmentAddressRepository)
		{
			_shipmentClientRepository = shipmentClientRepository;
			_shipmentClientAddressRepository = shipmentClientAddressRepository;
			_shipmentAddressRepository = shipmentAddressRepository;
		}

		public async Task Delete(EntityDto input)
		{
			await _shipmentClientAddressRepository.DeleteAsync(input.Id);
		}

		public async Task<List<LinkedAddressDto>> GetLinkedAddressByClientId(int clientId)
		{
			var addresses = await _shipmentClientAddressRepository.GetAll()
						   .Include(x => x.Address)
						   .Include(x => x.Client)
						   .Where(x => x.ClientId == clientId)
						   .Select(x => new LinkedAddressDto
						   {
							   Id = x.Id,
							   Name = x.Address.Name,
							   ExtCode = x.Address.ExtCode
						   }).ToListAsync();
			return addresses;
		}

		public async Task<List<LinkedClientDto>> GetLinkedClientByAddressId(int addressId)
		{
			var clients = await _shipmentClientAddressRepository.GetAll()
					   .Include(x => x.Address)
					   .Include(x => x.Client)
					   .Where(x => x.AddressId == addressId)
					   .Select(x => new LinkedClientDto
					   {
						   Id = x.Id,
						   Name = x.Client.Name,
						   ExtCode = x.Client.ExtCode
					   }).ToListAsync();
			return clients;
		}

		public async Task<long> Create(CreateOrEditShipmentClientAddressDto input)
		{
			var clientAddress = ObjectMapper.Map<ShipmentClientAddress>(input);
		    var id=	await _shipmentClientAddressRepository.InsertAndGetIdAsync(clientAddress);
			await CurrentUnitOfWork.SaveChangesAsync();
			return id;
		}

		public async Task<List<LinkedClientDto>> GetClientesIsNotInAddressId(int addressId)
		{
			var linkedClientes = await _shipmentClientAddressRepository.GetAll().Where(x => x.AddressId == addressId).Select(x => x.ClientId).ToListAsync();
			var clientesIsNotLinked = await _shipmentClientRepository.GetAll()
								   .Where(x => !linkedClientes.Any(i => i == x.Id))
								   .Select(x => new LinkedClientDto
								   {
									   Id = x.Id,
									   ExtCode = x.ExtCode,
									   Name = x.Name
								   }).ToListAsync();
			return clientesIsNotLinked;
		}

		public async Task<List<LinkedAddressDto>> GetAddressesIsNotInClientId(int clientId)
		{
			var linkedAddresses = await _shipmentClientAddressRepository.GetAll().Where(x => x.ClientId == clientId).Select(x => x.AddressId).ToListAsync();
			var addressesIsNotLinked = await _shipmentAddressRepository.GetAll()
								   .Where(x => !linkedAddresses.Any(i => i == x.Id))
								   .Select(x => new LinkedAddressDto
								   {
									   Id = x.Id,
									   ExtCode = x.ExtCode,
									   Name = x.Name
								   }).ToListAsync();
			return addressesIsNotLinked;
		}
	}
}
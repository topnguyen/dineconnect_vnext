﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Exporting
{
    public interface IShipmentRouteExcelExporter
    {
        FileDto ExportOrdersToFileForImportTemplate();
    }
}

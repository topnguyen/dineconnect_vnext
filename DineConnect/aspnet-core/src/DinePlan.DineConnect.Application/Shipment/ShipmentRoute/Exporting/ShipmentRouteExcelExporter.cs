﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Exporting
{
    public class ShipmentRouteExcelExporter: NpoiExcelExporterBase,IShipmentRouteExcelExporter
	{
		private readonly IAbpSession _abpSession;
		private readonly ITimeZoneConverter _timeZoneConverter;

		public ShipmentRouteExcelExporter(
			ITimeZoneConverter timeZoneConverter,
			IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :
			base(tempFileCacheManager)
		{
			_timeZoneConverter = timeZoneConverter;
			_abpSession = abpSession;
		}
		public FileDto ExportOrdersToFileForImportTemplate()
		{
			return CreateExcelPackage(
				"ImportOrders.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("Orders"));

					AddHeader(
						sheet,
						L("Number"),
						L("Date"),
						L("Client"),
						L("Tel"),
						L("Email"),
						L("Address"),
						L("TimeFrom"),
						L("TimeTo"),
						L("DepartFrom"),
						L("Goods"),
						L("Qty"),
						L("Cost"),
						L("Shipper")
					);
					var values = new List<object>
						{
							1,
							"2021-08-23",
							"Alice",
							"+99549110214",
							"alice@example.com",
							"145 LORONG 2 TOA PAYOH TOA PAYOH TOWERS SINGAPORE 310145",
							"09:00",
							"10:00",
							"134 BEDOK NORTH STREET 2 SINGAPORE",
							"Eggs",
							1,
							3.14,
							"eShop"
						};

					AddRow(excelPackage, sheet, 1, values.ToArray());

					for (var i = 1; i <= 13 ; i++)
					{
						sheet.AutoSizeColumn(i);
					}
				});
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Importing.Dto
{
    public class ImportOrderDto
    {
        public Double Number { get; set; }
        public string Date { get; set; }
        public string Client { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string DeliveryTimeFrom { get; set; }
        public string DeliveryTimeTo { get; set; }
        public string DepartFrom { get; set; }
        public string Goods { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Cost { get; set; }
        public string Shipper { get; set; }
        public string Exception { get; set; }
        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}

﻿using Abp.Dependency;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Importing
{
   public interface IOrderListExcelDataReader : ITransientDependency
   {
        List<ImportOrderDto> GetOrdersFromExcel(byte[] fileBytes);
   }
}
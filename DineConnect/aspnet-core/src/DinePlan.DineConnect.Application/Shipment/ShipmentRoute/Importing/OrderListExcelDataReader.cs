﻿using Abp.Localization;
using Abp.Localization.Sources;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Importing.Dto;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Importing
{
    public class OrderListExcelDataReader : NpoiExcelImporterBase<ImportOrderDto>, IOrderListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public OrderListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(DineConnectConsts.LocalizationSourceName);
        }

        public List<ImportOrderDto> GetOrdersFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportOrderDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }
            var exceptionMessage = new StringBuilder();
            var order = new ImportOrderDto();
            try
            {
                order.Number = worksheet.GetRow(row).GetCell(0).NumericCellValue;
                order.Date = worksheet.GetRow(row).GetCell(1).StringCellValue ?? string.Empty;
                order.Client = worksheet.GetRow(row).GetCell(2).StringCellValue ?? string.Empty;
                order.Phone = worksheet.GetRow(row).GetCell(3).StringCellValue ?? string.Empty;
                order.Email = worksheet.GetRow(row).GetCell(4).StringCellValue ?? string.Empty;
                order.Address = worksheet.GetRow(row).GetCell(5).StringCellValue ?? string.Empty;
                order.DeliveryTimeFrom = worksheet.GetRow(row).GetCell(6).StringCellValue ?? string.Empty;
                order.DeliveryTimeTo = worksheet.GetRow(row).GetCell(7).StringCellValue ?? string.Empty;
                order.DepartFrom = worksheet.GetRow(row).GetCell(8).StringCellValue ?? string.Empty;
                order.Goods = worksheet.GetRow(row).GetCell(9).StringCellValue ?? string.Empty;
                order.Amount =Convert.ToInt64(worksheet.GetRow(row).GetCell(10).NumericCellValue);
                order.Cost = Convert.ToDecimal(worksheet.GetRow(row).GetCell(11).NumericCellValue);
                order.Shipper = worksheet.GetRow(row).GetCell(12).StringCellValue ?? string.Empty;
            }
            catch (System.Exception exception)
            {
                order.Exception = exception.Message;
            }

            return order;
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null ;
        }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.MultiTenancy.OneMap;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Shipment.Dtos;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Dtos;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Exporting;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Importing;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Importing.Dto;
using DinePlan.DineConnect.Tiffins.TrackPod.TrackRoute;
using Elect.Core.ObjUtils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.TrackDrivers
{
	public class ShipmentRouteAppService : DineConnectAppServiceBase, IShipmentRouteAppService
	{
		private readonly IRepository<ShipmentRoute> _routeRepository;
		private readonly IRepository<ShipmentOrder> _shipmentOrderRepository;
		private readonly IRepository<ShipmentItem> _shipmentItemRepository;
		private readonly IRepository<ShipmentRoutePoint> _routePointRepository;
		private readonly IRepository<ShipmentAddress> _shipmentAddressRepository;
		private readonly IRepository<ShipmentClientAddress, long> _shipmentClientAddressRepository;
		private readonly IRepository<ShipmentClient> _shipmentClientRepository;
		private readonly IRepository<ShipmentRoutePointOrder, long> _routePointOrderRepository;
		private readonly IRepository<ShipmentDepot> _shipmentDepotRepository;
		private readonly IOrderListExcelDataReader _orderListExcelDataReader;
		private readonly IShipmentRouteExcelExporter _shipmentRouteExcelExporter;
		private readonly OneMapGatewayManager _oneMapGatewayManager;

		public ShipmentRouteAppService(
			IRepository<ShipmentRoute> routeRepository,
			IRepository<ShipmentRoutePoint> routePointRepository,
			IRepository<ShipmentOrder> shipmentOrderRepository,
			IRepository<ShipmentItem> shipmentItemRepository,
			IRepository<ShipmentRoutePointOrder, long> routePointOrderRepository,
			IRepository<ShipmentAddress> shipmentAddressRepository,
			IRepository<ShipmentClientAddress, long> shipmentClientAddressRepository,
			IRepository<ShipmentClient> shipmentClientRepository,
			IRepository<ShipmentDepot> shipmentDepotRepository,
			IOrderListExcelDataReader orderListExcelDataReader,
			IShipmentRouteExcelExporter shipmentRouteExcelExporter,
			OneMapGatewayManager oneMapGatewayManager
			)
		{
			_routeRepository = routeRepository;
			_routePointRepository = routePointRepository;
			_shipmentOrderRepository = shipmentOrderRepository;
			_shipmentItemRepository = shipmentItemRepository;
			_shipmentAddressRepository = shipmentAddressRepository;
			_shipmentClientAddressRepository = shipmentClientAddressRepository;
			_shipmentClientRepository = shipmentClientRepository;
			_routePointOrderRepository = routePointOrderRepository;
			_shipmentDepotRepository = shipmentDepotRepository;
			_orderListExcelDataReader = orderListExcelDataReader;
			_shipmentRouteExcelExporter = shipmentRouteExcelExporter;
			_oneMapGatewayManager = oneMapGatewayManager;
		}

		public async Task<List<ShipmentRouteDto>> GetRouteList(GetShipmentRouteListInput input)
		{
			var startDate = input.StartDate.Date;

			var endDate = input.EndDate.Date.AddDays(1).AddTicks(-1);

			var routes = await _routeRepository.GetAll()
						.Include(x => x.ShipFrom)
						.Include(x => x.Driver)
						.Where(x => x.Date >= startDate && x.Date <= endDate)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Code.Contains(input.Filter))
						.OrderBy(input.Sorting)
						.ToListAsync();

			return ObjectMapper.Map<List<ShipmentRouteDto>>(routes);
		}

		public async Task<CreateOrEditShipmentRouteDto> GetRouteForEdit(EntityDto input)
		{
			var trackRoute = await _routeRepository.FirstOrDefaultAsync(input.Id);

			var output = ObjectMapper.Map<CreateOrEditShipmentRouteDto>(trackRoute);

			output.DateInput = trackRoute.Date;
			output.Time = trackRoute.Date;

			return output;
		}

		public async Task CreateOrEditRoute(CreateOrEditShipmentRouteDto input)
		{
			ValidateForCreateOrUpdateRoute(input);
			if (input.Id == null)
			{
				await CreateRoute(input);
			}
			else
			{
				await UpdateRoute(input);
			}
		}

		public async Task DeleteRoute(EntityDto input)
		{
			await _routeRepository.DeleteAsync(input.Id);
		}

		public async Task<List<ShipmentRoutePointDto>> GetRoutePointList(GetShipmentRoutePointListInput input)
		{
			var route = await _routeRepository.GetAllIncluding(t => t.ShipFrom).FirstOrDefaultAsync(x => x.Id == input.RouteId);

			var data = await _routePointRepository
						.GetAll()
						.Include(x => x.Route)
						.Include(x => x.Orders).ThenInclude(o => o.Order)
						.Where(e => e.RouteId == input.RouteId)
						.OrderBy(input.Sorting)
						.ToListAsync();

			var result = data.Select(t =>
			{
				var item = ObjectMapper.Map<ShipmentRoutePointDto>(t);
				var firstOrder = t.Orders.FirstOrDefault()?.Order;

				if (item.Type == (int)RoutePointType.Delivery)
				{
					item.Clients = string.Join(", ", t.Orders.Select(o => o.Order.Client).ToArray());
					item.TimeSlot = $"{firstOrder.DeliveryTimeFrom} - {firstOrder.DeliveryTimeTo}";
				}

				return item;
			}).ToList();

			return result;
		}

		public async Task<List<ShipmentRoutePointOrderListDto>> GetOrderList(GetShipmentRoutePointOrderListInput input)
		{
			var routerPoint = await _routePointRepository.FirstOrDefaultAsync(input.RoutePointId);

			if (routerPoint != null)
			{
				var orderList = await
					_routePointOrderRepository
					.GetAllIncluding(t => t.RoutePoint)
					.Include(t => t.Order).ThenInclude(t => t.Items)
					.WhereIf(routerPoint.Type == (int)RoutePointType.Delivery, x => x.RoutePointId == routerPoint.Id)
					.WhereIf(routerPoint.Type != (int)RoutePointType.Delivery, x => x.RoutePoint.RouteId == routerPoint.RouteId)
					.ToListAsync();

				return orderList.Select(t =>
				{
					var item = ObjectMapper.Map<ShipmentRoutePointOrderListDto>(t.Order);
					item.TrackKey = t.TrackKey;
					item.ArrivalDate = t.RoutePoint.ArrivalDate;
					return item;
				}).ToList();
			}

			return new List<ShipmentRoutePointOrderListDto>();
		}

		public async Task<List<AddressShipmentDto>> GetOrderListOnMap(GetShipmentRoutePointListInput input)
		{
			var listDelivery = new List<AddressShipmentDto>();
			var route = await _routeRepository.GetAllIncluding(t => t.ShipFrom).FirstOrDefaultAsync(x => x.Id == input.RouteId);
			var data = await _routePointRepository
						.GetAll()
						.Include(x => x.Route)
						.Include(x => x.Orders).ThenInclude(o => o.Order)
						.Where(e => e.RouteId == input.RouteId)
						.OrderBy(x => x.Number)
						.ToListAsync();

			foreach (var item in data)
			{
				var firstOrder = item.Orders.Select(x => x.Order).FirstOrDefault();
				if (firstOrder != null)
				{
					if (firstOrder.GPSLon.HasValue && firstOrder.GPSLat.HasValue)
					{
						listDelivery.Add(new AddressShipmentDto
						{
							Address = firstOrder.Address,
							Latitude = firstOrder.GPSLat.ToString(),
							Longitude = firstOrder.GPSLon.ToString(),
							Label = item.Number.ToString(),
						});
					}
					else
					{
						var lstAddress = await _oneMapGatewayManager.GetAddressByPostCode(firstOrder.Address);
						if (lstAddress.Count > 0)
						{
							var address = lstAddress.Select(x => new AddressShipmentDto()
							{
								Address = x.Address,
								Label = item.Number.ToString(),
								Latitude = x.Latitude,
								Longitude = x.Longitude
							}).FirstOrDefault();

							listDelivery.Add(address);
							firstOrder.GPSLat = Convert.ToDouble(address.Latitude);
							firstOrder.GPSLon = Convert.ToDouble(address.Longitude);
						}
					}
				}
			}

			if (route != null && route.ShipFromId.HasValue)
			{
				var shipFrom = route.ShipFrom;
				var label = "Start";
				if (!route.IsStartFromDepot)
				{
					label = "End";
				}
				var addrressShipFrom = new AddressShipmentDto()
				{
					Label = label,
					Address = shipFrom.FullAddress,
					Latitude = shipFrom.Latitude.ToString(),
					Longitude = shipFrom.Longitude.ToString()
				};


				if (route.IsStartFromDepot)
				{
					listDelivery.Insert(0, addrressShipFrom);
				}
				if (route.IsEndAtDepot)
				{
					listDelivery.Add(addrressShipFrom);
				}
			}

			return listDelivery;
		}

		public async Task CreateOrUpdateOrder(CreateOrUpdateOrderDto input)
		{
			input.TenantId = AbpSession.TenantId;
			if (input.Id > 0)
			{
				await UpdateOrder(input);
			}
			else
			{
				await CreateOrder(input);
			}
		}

		private async Task CreateOrder(CreateOrUpdateOrderDto input)
		{
			var order = ObjectMapper.Map<ShipmentOrder>(input);
			var orderId = await _shipmentOrderRepository.InsertAndGetIdAsync(order);

			foreach (var item in input.Items)
			{
				var orderItem = ObjectMapper.Map<ShipmentItem>(item);
				orderItem.OrderId = orderId;
				await _shipmentItemRepository.InsertAsync(orderItem);
			}
			await CurrentUnitOfWork.SaveChangesAsync();
		}

		private async Task UpdateOrder(CreateOrUpdateOrderDto input)
		{
			var order = await _shipmentOrderRepository.GetAll().Include(x => x.Items).FirstOrDefaultAsync(x => x.Id == input.Id);
			ObjectMapper.Map(input, order);
			var orderItemsIdInput = input.Items.Select(x => x.Id).ToList();

			// create order item 
			var newOrderItems = input.Items.Where(x => x.Id == 0).ToList();
			foreach (var item in newOrderItems)
			{
				var orderItem = ObjectMapper.Map<ShipmentItem>(item);
				orderItem.OrderId = input.Id;
				await _shipmentItemRepository.InsertAsync(orderItem);
			}
			// remove orderItem 
			var orderItemDelete = order.Items.Where(x => !orderItemsIdInput.Any(i => i == x.Id)).ToList();
			foreach (var item in orderItemDelete)
			{
				await _shipmentItemRepository.DeleteAsync(item.Id);
			}
			// update orderItem
			var orderItemUpdate = order.Items.Where(x => orderItemsIdInput.Any(i => i == x.Id)).ToList();
			foreach (var item in orderItemUpdate)
			{
				var oldItem = await _shipmentItemRepository.FirstOrDefaultAsync(x => x.Id == item.Id);
				var newItem = input.Items.Where(x => x.Id == item.Id).FirstOrDefault();
				ObjectMapper.Map(newItem, oldItem);
			}

		}

		public async Task<CreateOrUpdateOrderDto> GetOrderById(int id)
		{
			var order = await _shipmentOrderRepository.GetAll().Include(x => x.Items).Where(x => x.Id == id).FirstOrDefaultAsync();
			var result = ObjectMapper.Map<CreateOrUpdateOrderDto>(order);
			return result;
		}

		#region Import Orders
		public async Task CreateOrdersFromExcel(byte[] fileBytes)
		{
			var ordersFromExcel = _orderListExcelDataReader.GetOrdersFromExcel(fileBytes);
			var orders = ordersFromExcel.GroupBy(x => new { x.Number, x.Client, x.Address, x.DepartFrom })
				.Select(x => new
				{
					Number = x.Key.Number,
					Address = x.Key.Address,
					Client = x.Key.Client,
					Depot = x.Key.DepartFrom,
					Items = x.ToList()
				});

			foreach (var item in orders)
			{
				var clientId = await CreateOrGetShipmentClientId(item.Client);
				var addressId = await CreateOrGetShipmentAddressId(item.Address);
				var depotId = await CreateOrGetShipmentDepotId(item.Depot);
				await CreateShipmentClientAddress(clientId, addressId);
				var order = new CreateOrUpdateOrderDto();
				order.ClientId = clientId;
				order.AddressId = addressId;
				order.ShipmentDepotId = depotId;
				order.Client = item.Client;
				order.Address = item.Address;
				order.Number = item.Number.ToString();
				order.Customer = item.Items?.First()?.Shipper;
				order.Email = item.Items?.First()?.Email;
				order.Tel = item.Items?.First()?.Phone;
				order.Warehouse = item.Items?.First().DepartFrom;
				order.DeliveryTimeFrom = item.Items?.First().DeliveryTimeFrom;
				order.DeliveryTimeTo = item.Items?.First().DeliveryTimeTo;
				order.Date = Convert.ToDateTime(item.Items?.First()?.Date);
				order.Type = (ShipmentType.Delivery).ToString();
				var orderItems = item.Items.Select(x => new ShipmentItemListDto()
				{
					Id = 0,
					Amount = x.Amount,
					Cost = x.Cost,
					Name = x.Goods,
				}).ToList();
				order.Items = orderItems;
				await CreateOrder(order);
			}
		}

		private async Task<int> CreateOrGetShipmentAddressId(string fullAddress)
		{
			var addressId = 0;
			var address = await _shipmentAddressRepository.FirstOrDefaultAsync(x => x.Address == fullAddress);
			if (address != null)
			{
				return address.Id;
			}
			else
			{
				address = new ShipmentAddress();
				address.Address = fullAddress;
				address.TenantId = AbpSession.TenantId ?? 1;
				addressId = await _shipmentAddressRepository.InsertAndGetIdAsync(address);
			}
			return addressId;
		}

		private async Task CreateShipmentClientAddress(int clientId, int addressId)
		{
			var clientAdress = new ShipmentClientAddress();
			clientAdress.AddressId = addressId;
			clientAdress.ClientId = clientId;
			clientAdress.TenantId = AbpSession.TenantId ?? 1;
			await _shipmentClientAddressRepository.InsertAsync(clientAdress);
			await CurrentUnitOfWork.SaveChangesAsync();

		}
		private async Task<int> CreateOrGetShipmentClientId(string clientName)
		{
			var clientId = 0;
			var client = await _shipmentClientRepository.FirstOrDefaultAsync(x => x.Name == clientName);
			if (client != null)
			{
				return client.Id;
			}
			else
			{
				client = new ShipmentClient();
				client.Name = clientName;
				client.TenantId = AbpSession.TenantId ?? 1;
				clientId = await _shipmentClientRepository.InsertAndGetIdAsync(client);
			}
			return clientId;
		}
		private async Task<int> CreateOrGetShipmentDepotId(string departFrom)
		{
			var depotId = 0;
			var depot = await _shipmentDepotRepository.FirstOrDefaultAsync(x => x.FullAddress == departFrom);
			if (depot != null)
			{
				return depot.Id;
			}
			else
			{
				depot = new ShipmentDepot();
				depot.FullAddress = departFrom;
				depot.TenantId = AbpSession.TenantId ?? 1;
				depotId = await _shipmentDepotRepository.InsertAndGetIdAsync(depot);
			}
			return depotId;
		}

		public FileDto GetImportTemplateToExcel()
		{
			return _shipmentRouteExcelExporter.ExportOrdersToFileForImportTemplate();
		}

		#endregion Import

		public async Task DeleteOrder(int id)
		{
			var lstItem = await _shipmentItemRepository.GetAll().Where(x => x.OrderId == id).Select(x => x.Id).ToListAsync();
			foreach (int item in lstItem)
			{
				await _shipmentItemRepository.DeleteAsync(item);
			}
			await _shipmentOrderRepository.DeleteAsync(id);
		}

		public async Task<List<ShipmentRoutePointDeliveryDto>> GetFreeOrderList(GetShipmentRoutePointListInput input)
		{
			var feeOrderLists = await _shipmentOrderRepository
				.GetAll()
				.Include(x => x.ShipmentDepot)
				.Where(x => x.TrackKey == null)
				.OrderBy(input.Sorting).ToListAsync();

			var result = ObjectMapper.Map<List<ShipmentRoutePointDeliveryDto>>(feeOrderLists);

			return result;
		}

		public async Task<List<ShipmentItemListDto>> GetOrderItemsList(GetShipmentItemInput input)
		{
			var orderItems =
				await _shipmentItemRepository
				.GetAll()
				.Where(x => x.OrderId == input.OrderId)
				.OrderBy(input.Sorting)
				.ToListAsync();

			var result = ObjectMapper.Map<List<ShipmentItemListDto>>(orderItems);

			return result;
		}

		public async Task UpdateRoutePointsOrder(UpdateRoutePointsOrderInput input)
		{
			var allRouteRoints = _routePointRepository.GetAll().Where(p => input.RoutePointIds.Contains(p.Id));

			foreach (var p in allRouteRoints)
			{
				p.Number = input.RoutePointIds.IndexOf(p.Id) + 1;
			}

			await CurrentUnitOfWork.SaveChangesAsync();
		}

		public async Task RemoveOrdersFromRoute(RemoveOrdersFromRouteInput input)
		{
			await _routePointOrderRepository
				.DeleteAsync(x => x.RoutePoint.RouteId == input.RouteId && input.OrderIds.Contains(x.OrderId));

			await CurrentUnitOfWork.SaveChangesAsync();

			await _routePointRepository.DeleteAsync(x => x.RouteId == input.RouteId && x.Type == 0 && x.Orders.Count() == 0);

			await CurrentUnitOfWork.SaveChangesAsync();

			var allRouteRoints = await _routePointRepository.GetAll().Where(p => p.RouteId == input.RouteId && p.Type == 0).OrderBy(x => x.Number).ToListAsync();

			for (var i = 0; i < allRouteRoints.Count; i++)
			{
				allRouteRoints[i].Number = i + 1;
			}

			var updatedOrders = await _shipmentOrderRepository.GetAll().Where(t => input.OrderIds.Contains(t.Id)).ToListAsync();

			foreach (var order in updatedOrders)
			{
				order.TrackKey = null;
			}
		}

		public async Task EmptyRoute(EntityDto input)
		{
			var orderIds = await _routePointOrderRepository
				.GetAll()
				.Where(t => t.RoutePoint.Route.Id == input.Id)
				.Select(x => x.OrderId).ToListAsync();

			await _routePointOrderRepository.DeleteAsync(x => x.RoutePoint.RouteId == input.Id);

			await CurrentUnitOfWork.SaveChangesAsync();

			await _routePointRepository.DeleteAsync(x => x.RouteId == input.Id && x.Type == 0);

			var updatedOrders = await _shipmentOrderRepository.GetAll().Where(t => orderIds.Contains(t.Id)).ToListAsync();

			foreach (var order in updatedOrders)
			{
				order.TrackKey = null;
			}
		}

		public async Task<int> AddOrdersFromRoute(AddOrdersToRouteInputDto input)
		{
			var orders = await _shipmentOrderRepository.GetAll().Where(t => input.OrderIds.Contains(t.Id)).ToListAsync();

			var newRoutePoints = orders.GroupBy(t => new { t.Warehouse, t.Date.Date, t.Address, t.DeliveryTimeFrom, t.DeliveryTimeTo });

			var currentRoutePoints = await _routePointRepository
				.GetAll()
				.Where(t => t.RouteId == input.RouteId && t.Type == 0).ToListAsync();

			var currentNumber = currentRoutePoints.Any() ? currentRoutePoints.Max(t => t.Number) : 0;

			int lastId = 0;

			foreach (var r in newRoutePoints)
			{
				var current = currentRoutePoints.FirstOrDefault(t => t.Address == r.Key.Address);

				if (current == null)
				{
					current = new ShipmentRoutePoint
					{
						Number = ++currentNumber,
						RouteId = input.RouteId,
						Type = (int)(int)RoutePointType.Delivery,
						Address = r.Key.Address
					};
				}

				foreach (var i in r)
				{
					var newTrackId = new System.Guid();

					current.Orders.Add(new ShipmentRoutePointOrder
					{
						OrderId = i.Id,
						TrackKey = newTrackId
					});

					i.TrackKey = newTrackId;
				}

				lastId = await _routePointRepository.InsertOrUpdateAndGetIdAsync(current);
			}

			return lastId;
		}
		public async Task CloseRoute(int routeId)
		{
			var route = await _routeRepository.FirstOrDefaultAsync(x => x.Id == routeId);
			route.Status = true;
		}

		public async Task DuplicateRoute(CreateOrEditShipmentRouteDto input)
		{
			// check xem teen dda ton tai chua, neu roi thu thay ten moi
			var exists = _routeRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);
			if (exists.Any(o => o.Code.Equals(input.Code)))
			{
				input.Code = RamdomRouteCode();
			}
			var routeDuplicate = new CreateOrEditShipmentRouteDto();
			routeDuplicate.Code = input.Code;
			routeDuplicate.DateInput = input.DateInput;
			routeDuplicate.Time = input.Time;
			routeDuplicate.ShipFromId = input.ShipFromId;
			routeDuplicate.DriverId = input.DriverId;
			routeDuplicate.IsStartFromDepot = input.IsStartFromDepot;
			routeDuplicate.DriverId = input.DriverId;
			var routeId = await CreateRoute(routeDuplicate);
			var inputRoutePoint = new GetShipmentRoutePointListInput();
			inputRoutePoint.RouteId = input.Id ?? 1;

			var oldOrders = await _routePointRepository
				  .GetAll()
				  .Include(x => x.Route)
				  .Include(x => x.Orders)
				  .ThenInclude(o => o.Order)
				  .ThenInclude(x => x.Items)
				  .Where(e => e.RouteId == input.Id)
				  .SelectMany(x => x.Orders)
				  .ToListAsync();
			var addOrdersToRoute = new AddOrdersToRouteInputDto();
			addOrdersToRoute.RouteId = routeId;
			foreach (var oldOrder in oldOrders)
			{
				var newOrder = oldOrder.Order;
				newOrder.Items = null;
				newOrder.Id = 0;
				newOrder.TrackKey = null;
				var newOrderId = await _shipmentOrderRepository.InsertAndGetIdAsync(newOrder);
				var newOrderItems = oldOrder.Order.Items;
				foreach (var item in newOrderItems)
				{
					var newOrderItem = item;
					newOrderItem.Id = 0;
					newOrderItem.OrderId = newOrderId;
					await _shipmentItemRepository.InsertAsync(newOrderItem);
				}
				addOrdersToRoute.OrderIds.Add(newOrderId);
			}
			await AddOrdersFromRoute(addOrdersToRoute);
		}

		private string RamdomRouteCode()
		{
			var result = Guid.NewGuid();
			return result.ToString();
		}


		#region Privates
		private async Task<int> CreateRoute(CreateOrEditShipmentRouteDto input)
		{
			var route = ObjectMapper.Map<ShipmentRoute>(input);

			if (AbpSession.TenantId != null)
			{
				route.TenantId = (int)AbpSession.TenantId;
			}

			route.RoutePoints = new List<ShipmentRoutePoint>();

			var adddress = await _shipmentDepotRepository.FirstOrDefaultAsync(input.ShipFromId ?? 0);

			route.RoutePoints.Add(new ShipmentRoutePoint()
			{
				Address = adddress?.FullAddress,
				Type = (int)RoutePointType.DepartFromDepot,
				Number = -1000,
			});

			if (route.IsEndAtDepot)
			{
				route.RoutePoints.Add(new ShipmentRoutePoint()
				{
					Address = adddress?.FullAddress,
					Type = (int)RoutePointType.BackToDepot,
					Number = 1000,
				});
			}

			var routeId = await _routeRepository.InsertAndGetIdAsync(route);
			return routeId;
		}

		private async Task UpdateRoute(CreateOrEditShipmentRouteDto input)
		{
			var route = await _routeRepository.GetAllIncluding(t => t.RoutePoints).FirstOrDefaultAsync(t => t.Id == input.Id);

			ObjectMapper.Map(input, route);

			var departRoutePoint = route.RoutePoints.FirstOrDefault(x => x.Type == (int)RoutePointType.DepartFromDepot);

			var adddress = await _shipmentDepotRepository.FirstOrDefaultAsync(input.ShipFromId ?? 0);

			if (departRoutePoint == null)
			{
				route.RoutePoints.Add(new ShipmentRoutePoint()
				{
					Address = adddress?.FullAddress,
					Type = (int)RoutePointType.DepartFromDepot,
					Number = -1000,
				});
			}
			else
			{
				departRoutePoint.Address = adddress?.FullAddress;
			}

			var backToDeportRouterPoint = route.RoutePoints.FirstOrDefault(x => x.Type == (int)RoutePointType.BackToDepot);

			if (route.IsEndAtDepot && backToDeportRouterPoint == null)
			{
				route.RoutePoints.Add(new ShipmentRoutePoint()
				{
					Address = adddress?.FullAddress,
					Type = (int)RoutePointType.BackToDepot,
					Number = 1000,
				});
			}
			else if (route.IsEndAtDepot && backToDeportRouterPoint != null)
			{
				backToDeportRouterPoint.Address = adddress?.FullAddress;
			}
			else if (!route.IsEndAtDepot && backToDeportRouterPoint != null)
			{
				route.RoutePoints.Remove(backToDeportRouterPoint);
			}
		}
		protected virtual void ValidateForCreateOrUpdateRoute(CreateOrEditShipmentRouteDto input)
		{
			var exists = _routeRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

			if (exists.Any(o => o.Code.Equals(input.Code)))
			{
				throw new UserFriendlyException($"Route code {input.Code} aleady exists");
			}
		}

		#endregion
	}
}
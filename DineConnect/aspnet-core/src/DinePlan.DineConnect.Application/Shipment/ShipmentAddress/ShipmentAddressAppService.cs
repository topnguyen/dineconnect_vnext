﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Shipment.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentAddressAppService : DineConnectAppServiceBase, IShipmentAddressAppService
    {
        private readonly IRepository<ShipmentAddress> _shipmentAddressRepository;

        public ShipmentAddressAppService(IRepository<ShipmentAddress> shipmentAddressRepository)
        {
            _shipmentAddressRepository = shipmentAddressRepository;
        }

        public async Task<PagedResultDto<ShipmentAddressDto>> GetAll(GetAllShipmentAddressInput input)
        {
            var clients = _shipmentAddressRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        e=> e.Name.Contains(input.Filter)
                         || e.ExtCode.Contains(input.Filter)
                         || e.Address.Contains(input.Filter)
                         || e.Zone.Contains(input.Filter)
                        );

            var pagedAndFiltered = clients
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var output = pagedAndFiltered
                                 .Select(o => ObjectMapper.Map<ShipmentAddressDto>(o));

            var totalCount = await clients.CountAsync();

            return new PagedResultDto<ShipmentAddressDto>(
                totalCount,
                await output.ToListAsync()
            );
        }

        public async Task<CreateOrEditShipmentAddressDto> GetShipmentAddressEdit(EntityDto input)
        {
            var address = await _shipmentAddressRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditShipmentAddressDto>(address);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShipmentAddressDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditShipmentAddressDto input)
        {
            var address = ObjectMapper.Map<ShipmentAddress>(input);

            if (AbpSession.TenantId != null)
            {
                address.TenantId = (int)AbpSession.TenantId;
            }

            await _shipmentAddressRepository.InsertAsync(address);
        }

        protected virtual async Task Update(CreateOrEditShipmentAddressDto input)
        {
            var address = await _shipmentAddressRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, address);
        }

        public async Task Delete(EntityDto input)
        {
            await _shipmentAddressRepository.DeleteAsync(input.Id);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditShipmentAddressDto input)
        {
            var exists = _shipmentAddressRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.ExtCode.Equals(input.ExtCode)))
            {
                throw new UserFriendlyException($"Address code {input.ExtCode} aleady exists");
            }
            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException($"Address name {input.Name} aleady exists");
            }
        }
    }
}
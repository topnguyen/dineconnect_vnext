﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos;
using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Exporting;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates
{
    
    public class ProgramSettingTemplatesAppService : DineConnectAppServiceBase, IProgramSettingTemplatesAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;
        private readonly IRepository<ProgramSettingValue> _programSettingValueRepository;

        private readonly IProgramSettingTemplatesExcelExporter _programSettingTemplatesExcelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Location> _locationRepository;

        public ProgramSettingTemplatesAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<ProgramSettingTemplate> programSettingTemplateRepository,
            IRepository<ProgramSettingValue> programSettingValueRepository,
            IProgramSettingTemplatesExcelExporter programSettingTemplatesExcelExporter,
            ILocationAppService locationAppService,
            IRepository<Location> locationRepository
            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _programSettingTemplateRepository = programSettingTemplateRepository;
            _programSettingValueRepository = programSettingValueRepository;

            _programSettingTemplatesExcelExporter = programSettingTemplatesExcelExporter;
            _locationAppService = locationAppService;
            _locationRepository = locationRepository;
        }

        public async Task<PagedResultDto<GetAllProgramSettingTemplateDto>> GetAll(GetAllProgramSettingTemplateInput input)
        {
            var query = _programSettingTemplateRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

            if (!string.IsNullOrWhiteSpace(input.LocationIds))
            {
                var templateIds = await FilterByLocations(_programSettingValueRepository.GetAll(), input)
                    .Select(x => x.ProgramSettingTemplateId)
                    .Distinct().ToListAsync();

                query = query.Where(x => templateIds.Contains(x.Id));
            }

            var sortedItems = await query
                 .OrderBy(input.Sorting ?? "Id desc")
                 .PageBy(input)
                 .Select(x => ObjectMapper.Map<GetAllProgramSettingTemplateDto>(x))
                 .ToListAsync();

            return new PagedResultDto<GetAllProgramSettingTemplateDto>(await query.CountAsync(), sortedItems);
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _programSettingTemplateRepository.GetAll();
            var items = await query.Select(x => ObjectMapper.Map<GetAllProgramSettingTemplateDto>(x)).ToListAsync();

            return _programSettingTemplatesExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<EditProgramSettingTemplateInput> GetForEdit(int id)
        {
            var template = await _programSettingTemplateRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (template == null)
                throw new UserFriendlyException(L("ProgramSettingTemplateNotFound"));

            return ObjectMapper.Map<EditProgramSettingTemplateInput>(template);
        }

        // --

        
        public async Task<int> Create(CreateProgramSettingTemplateInput input)
        {
            var count = await _programSettingTemplateRepository.CountAsync(x => x.Name == input.Name);
            if (count > 0)
                throw new UserFriendlyException(L("NameAlreadyExists"));

            var template = ObjectMapper.Map<ProgramSettingTemplate>(input);
            template.TenantId = AbpSession.TenantId ?? 0;

            return await _programSettingTemplateRepository.InsertOrUpdateAndGetIdAsync(template);
        }

        
        public async Task<int> Edit(EditProgramSettingTemplateInput input)
        {
            var template = await _programSettingTemplateRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (template == null)
                throw new UserFriendlyException(L("ProgramSettingTemplateNotFound"));

            var count = await _programSettingTemplateRepository.CountAsync(x => x.Name == input.Name && x.Id != template.Id);
            if (count > 0)
                throw new UserFriendlyException(L("NameAlreadyExists"));

            ObjectMapper.Map(input, template);
            template.TenantId = AbpSession.TenantId ?? 0;

            return await _programSettingTemplateRepository.InsertOrUpdateAndGetIdAsync(template);
        }

        // --

        
        public async Task Delete(int id)
        {
            var valueIds = await _programSettingValueRepository.GetAll().Where(x => x.ProgramSettingTemplateId == id)
                .Select(x => x.Id)
                .ToListAsync();

            if (valueIds.Any())
                await _programSettingValueRepository.DeleteAsync(x => valueIds.Contains(x.Id));

            await _programSettingTemplateRepository.DeleteAsync(id);
        }

        // --

        
        public async Task<PagedResultDto<GetAllProgramSettingValueDto>> GetAllValues(GetAllProgramSettingValueInput input)
        {
            var count = await _programSettingTemplateRepository.CountAsync(x => x.Id == input.ProgramSettingTemplateId);
            if (count == 0)
                throw new UserFriendlyException(L("ProgramSettingTemplateNotFound"));

            var query = _programSettingValueRepository.GetAll()
                .Where(x => x.ProgramSettingTemplateId == input.ProgramSettingTemplateId);
            query = FilterByLocations(query, input);

            var sortedItems = await query
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input)
                .Select(x => ObjectMapper.Map<GetAllProgramSettingValueDto>(x))
                .ToListAsync();

            return new PagedResultDto<GetAllProgramSettingValueDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<CreateOrEditProgramSettingValueInput> GetValueForEdit(int id)
        {
            var value = await _programSettingValueRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null)
                throw new UserFriendlyException(L("ProgramSettingValueNotFound"));

            var valueDto = ObjectMapper.Map<CreateOrEditProgramSettingValueInput>(value);
            GetLocationsForEdit(valueDto, valueDto.LocationGroup);

            return valueDto;
        }

        
        public async Task<int> CreateOrUpdateValue(CreateOrEditProgramSettingValueInput input)
        {
            var template = await _programSettingTemplateRepository.FirstOrDefaultAsync(x => x.Id == input.ProgramSettingTemplateId);
            if (template == null)
                throw new UserFriendlyException(L("ProgramSettingTemplateNotFound"));

            await CheckValueBusinessRules(input);

            if ((input.Id ?? -1) > 0)
            {
                var value = await _programSettingValueRepository.FirstOrDefaultAsync(x => x.Id == input.Id.Value);
                if (value == null)
                    throw new UserFriendlyException(L("ProgramSettingValueNotFound"));

                value.ActualValue = input.ActualValue;
                UpdateLocations(value, input.LocationGroup);

                await _programSettingValueRepository.UpdateAsync(value);

                return value.Id;
            }
            else
            {
                var value = ObjectMapper.Map<ProgramSettingValue>(input);
                value.TenantId = AbpSession.TenantId ?? 0;
                value.OrganizationId = _connectSession.OrgId;

                UpdateLocations(value, input.LocationGroup);

                return await _programSettingValueRepository.InsertOrUpdateAndGetIdAsync(value);
            }
        }

        private async Task CheckValueBusinessRules(CreateOrEditProgramSettingValueInput input)
        {
            var locationGroupDto = input.LocationGroup;
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedValuesQuery = _programSettingValueRepository.GetAll()
                .Where(x => x.ProgramSettingTemplateId == input.ProgramSettingTemplateId)
                .WhereIf((input.Id ?? -1) > 0, x => x.Id != input.Id);

            var existedValues = await existedValuesQuery.ToListAsync();
            existedValues.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);

                if (x.ActualValue == input.ActualValue)
                {
                    var message = string.Format(L("DuplcatedValueError"), x.ActualValue);
                    throw new UserFriendlyException(message);
                }
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        
        public async Task DeleteValue(int id)
        {
            await _programSettingValueRepository.DeleteAsync(id);
        }

        public async Task<ListResultDto<ApiProgramSettingValue>> ApiGetProgramSettings(ApiLocationInput input)
        {
            var loc = await _locationRepository.GetAsync(input.LocationId);
            if (loc.OrganizationId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, loc.OrganizationId);

            var deptOutput = new List<ApiProgramSettingValue>();

            var lstTemplates =
                await _programSettingTemplateRepository.GetAllListAsync(i => i.TenantId == loc.TenantId);

            foreach (var oneTemplate in lstTemplates)
            {
                var nullSettings = _programSettingValueRepository
                    .GetAll().Where(a => a.ProgramSettingTemplateId == oneTemplate.Id && a.Locations == null);

                if (nullSettings.Any())
                {
                    var nullSetting = nullSettings.ToList().LastOrDefault();
                    if (nullSetting != null)
                    {
                        deptOutput.Add(new ApiProgramSettingValue
                        {
                            Name = oneTemplate.Name,
                            DefaultValue = nullSetting.ActualValue
                        });
                        continue;
                    }
                }

                var notNullLocations = _programSettingValueRepository
                    .GetAll().Where(a => a.ProgramSettingTemplateId == oneTemplate.Id && (a.Locations != null || a.NonLocations != null));

                bool add = false;
                foreach (var myValue in notNullLocations)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = myValue.Locations,
                        Group = myValue.Group,
                        NonLocations = myValue.NonLocations,
                        LocationTag = myValue.LocationTag
                    }))
                    {
                        add = true;
                        deptOutput.Add(new ApiProgramSettingValue
                        {
                            Name = oneTemplate.Name,
                            DefaultValue = myValue.ActualValue
                        });
                        break;
                    }
                }

                if (add)
                {
                    continue;
                }
                deptOutput.Add(new ApiProgramSettingValue
                {
                    Name = oneTemplate.Name,
                    DefaultValue = oneTemplate.DefaultValue
                });
            }

            return new ListResultDto<ApiProgramSettingValue>(deptOutput);
        }
    }
}
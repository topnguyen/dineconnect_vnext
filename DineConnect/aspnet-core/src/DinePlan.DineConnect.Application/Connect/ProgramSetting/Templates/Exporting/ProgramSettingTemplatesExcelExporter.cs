﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Exporting
{
    public class ProgramSettingTemplatesExcelExporter : NpoiExcelExporterBase, IProgramSettingTemplatesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProgramSettingTemplatesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllProgramSettingTemplateDto> items)
        {
            return CreateExcelPackage(
                "ProgramSettingList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProgramSetting"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("DefaultValue"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.DefaultValue,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(AppConsts.FileDateTimeFormat)
                        );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
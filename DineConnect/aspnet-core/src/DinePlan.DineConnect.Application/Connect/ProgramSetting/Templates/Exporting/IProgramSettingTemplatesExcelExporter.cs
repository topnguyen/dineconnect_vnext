﻿using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Exporting
{
    public interface IProgramSettingTemplatesExcelExporter
    {
        FileDto ExportToFile(List<GetAllProgramSettingTemplateDto> items);
    }
}
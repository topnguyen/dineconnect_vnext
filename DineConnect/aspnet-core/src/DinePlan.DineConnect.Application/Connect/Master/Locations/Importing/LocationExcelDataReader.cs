﻿using Abp.Dependency;
using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Locations.Importing
{
    public class LocationExcelDataReader : NpoiExcelImporterBase<ImportLocationDto>, ILocationExcelDataReader, ITransientDependency
    {
        public List<ImportLocationDto> GetLocationsFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportLocationDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var location = new ImportLocationDto();

            try
            {
                location.OrganizationCode = worksheet.GetRow(row).GetCell(0)?.StringCellValue.Trim();
                location.Code = worksheet.GetRow(row).GetCell(1)?.StringCellValue.Trim();
                location.Name = worksheet.GetRow(row).GetCell(2)?.StringCellValue.Trim();
                location.Address1 = worksheet.GetRow(row).GetCell(3)?.StringCellValue.Trim();
                location.Address2 = worksheet.GetRow(row).GetCell(4)?.StringCellValue.Trim();
                location.Address3 = worksheet.GetRow(row).GetCell(5)?.StringCellValue.Trim();
                location.CityName = worksheet.GetRow(row).GetCell(6)?.StringCellValue.Trim();
                location.StateName = worksheet.GetRow(row).GetCell(7)?.StringCellValue.Trim();
                location.CountryName = worksheet.GetRow(row).GetCell(8)?.StringCellValue.Trim();
                location.PhoneNumber = worksheet.GetRow(row).GetCell(9)?.StringCellValue.Trim();
                location.Fax = worksheet.GetRow(row).GetCell(10)?.StringCellValue.Trim();
                location.Website = worksheet.GetRow(row).GetCell(11)?.StringCellValue.Trim();
                location.Email = worksheet.GetRow(row).GetCell(12)?.StringCellValue.Trim();
            }
            catch (Exception exception)
            {
                location.Exception = exception.Message;
            }

            return location;
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            return string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(0)?.StringCellValue) 
                || string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(1)?.StringCellValue) 
                || string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(2)?.StringCellValue);
        }
    }
}

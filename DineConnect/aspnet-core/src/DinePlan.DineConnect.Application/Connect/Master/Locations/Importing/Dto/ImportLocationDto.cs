﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto
{
    public class ImportLocationDto
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int OrganizationId { get; set; }

        public string OrganizationCode { get; set; }
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostalCode { get; set; }

        public int? CityId { get; set; }

        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string Exception { get; set; }
        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}

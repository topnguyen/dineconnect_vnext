﻿using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Threading;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations.Exporting;
using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Locations.Importing
{
    public class ImportLocationsToExcelJob : BackgroundJob<ImportLocationsFromExcelJobArgs>, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ILocationExcelDataReader _locationExcelDataReader;
        private readonly IAppNotifier _appNotifier;
        private readonly IObjectMapper _objectMapper;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<Brand> _organizationRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly ILocationExcelExporter _locationExcelExporter;
        private readonly ILocalizationContext _localizationContext;

        public ImportLocationsToExcelJob(IUnitOfWorkManager unitOfWorkManager,
            IBinaryObjectManager binaryObjectManager, IAppNotifier appNotifier,
            IObjectMapper objectMapper, IRepository<City> cityRepository,
            IRepository<Country> countryRepository, IRepository<State> stateRepository, 
            IRepository<Brand> organizationRepository, IRepository<Location> locationRepository, 
            ILocationExcelDataReader locationExcelDataReader, ILocationExcelExporter locationExcelExporter, 
            ILocalizationContext localizationContext)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _binaryObjectManager = binaryObjectManager;
            _appNotifier = appNotifier;
            _objectMapper = objectMapper;
            _cityRepository = cityRepository;
            _countryRepository = countryRepository;
            _stateRepository = stateRepository;
            _organizationRepository = organizationRepository;
            _locationRepository = locationRepository;
            _locationExcelDataReader = locationExcelDataReader;
            _locationExcelExporter = locationExcelExporter;
            _localizationContext = localizationContext;
        }

        public override void Execute(ImportLocationsFromExcelJobArgs args)
        {
            var users = GetLocationsListFromExcelOrNull(args);
            if (users == null || !users.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }

            CreateLocations(args, users);
        }

        private void CreateLocations(ImportLocationsFromExcelJobArgs args, List<ImportLocationDto> locations)
        {
            var invalidUsers = new List<ImportLocationDto>();
            var listDuplicate = CheckDuplicate(locations);
            invalidUsers.AddRange(listDuplicate);
            locations = locations.Except(listDuplicate).ToList();

            foreach (var location in locations)
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        if (location.CanBeImported())
                        {
                            try
                            {
                                AsyncHelper.RunSync(() => CreateLocationAsync(location));
                            }
                            catch (UserFriendlyException exception)
                            {
                                location.Exception = exception.Message;
                                invalidUsers.Add(location);
                            }
                            catch (Exception exception)
                            {
                                location.Exception = exception.ToString();
                                invalidUsers.Add(location);
                            }
                        }
                        else
                        {
                            invalidUsers.Add(location);
                        }
                    }

                    uow.Complete();
                }
            }

            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => ProcessImportLocationsResultAsync(args, invalidUsers));
                }

                uow.Complete();
            }
        }

        private async Task CreateLocationAsync(ImportLocationDto input)
        {
            var tenantId = CurrentUnitOfWork.GetTenantId();
            var location = _objectMapper.Map<Location>(input);

            var isExistName = await _locationRepository.FirstOrDefaultAsync(x => x.Name == input.Name);
            if (isExistName != null)
            {
                throw new UserFriendlyException($"{input.Name} {new LocalizableString("IsExist", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext)}");
            }

            var isExistCode = await _locationRepository.FirstOrDefaultAsync(x => x.Code == input.Code);
            if (isExistCode != null)
            {
                throw new UserFriendlyException($"{input.Code} {new LocalizableString("IsExist", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext)}");
            }

            var organization = await _organizationRepository.FirstOrDefaultAsync(x => x.Code == input.OrganizationCode);
            if (organization == null)
            {
                throw new UserFriendlyException(new LocalizableString("OrganizationNotFound", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext));
            }

            if (!string.IsNullOrWhiteSpace(input.CountryName) || !string.IsNullOrWhiteSpace(input.StateName) || !string.IsNullOrWhiteSpace(input.CityName))
            {
                var country = await _countryRepository.FirstOrDefaultAsync(x => x.Name == input.CountryName);
                if (country == null)
                {
                    throw new UserFriendlyException(new LocalizableString("CountryNotFound", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext));
                }

                var state = await _stateRepository.FirstOrDefaultAsync(x => x.Name == input.StateName && x.CountryId == country.Id);
                if (state == null)
                {
                    throw new UserFriendlyException(new LocalizableString("StateNotFoundIn", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext));
                }

                var city = await _cityRepository.FirstOrDefaultAsync(x => x.Name == input.CityName && x.CountryId == country.Id && x.StateId == state.Id);
                if (city == null)
                {
                    throw new UserFriendlyException(new LocalizableString("CityNotFoundIn", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext));
                }

                location.CityId = city.Id;
            }

            location.OrganizationId = organization.Id;
            location.TenantId = tenantId.Value;

            await _locationRepository.InsertAsync(location);
        }

        private List<ImportLocationDto> CheckDuplicate(List<ImportLocationDto> locations)
        {
            var listDuplication = new List<ImportLocationDto>();

            var anyDuplicateName = locations.GroupBy(x => new { x.Name }).Where(g => g.Count() > 1);

            if (anyDuplicateName.Any())
            {
                var builder = new StringBuilder();
            
                builder.Append(string.Format(new LocalizableString("DuplicateExistsInFile_f", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext), 
                    new LocalizableString("Name", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext)));
                
                builder.Append(anyDuplicateName.Select(e => e.Key.Name).JoinAsString(", "));

                listDuplication.AddRange(anyDuplicateName.SelectMany(x => x.Select(y =>
                {
                    y.Exception = builder.ToString();
                    return y; 
                })));
            }

            var anyDuplicateCode = locations.GroupBy(x => new { x.Code }).Where(g => g.Count() > 1);
            
            if (anyDuplicateName.Any())
            {
                var builder = new StringBuilder();
            
                builder.Append(string.Format(new LocalizableString("DuplicateExistsInFile_f", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext),
                    new LocalizableString("Code", DineConnectConsts.LocalizationSourceName).Localize(_localizationContext)));
                
                builder.Append(anyDuplicateName.Select(e => e.Key.Name).JoinAsString(", "));

                listDuplication.AddRange(anyDuplicateCode.SelectMany(x => x.Select(y =>
                {
                    y.Exception = builder.ToString();
                    return y;
                })));
            }

            return listDuplication;
        }

        private List<ImportLocationDto> GetLocationsListFromExcelOrNull(ImportLocationsFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _locationExcelDataReader.GetLocationsFromExcel(file.Bytes);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task ProcessImportLocationsResultAsync(ImportLocationsFromExcelJobArgs args,
           List<ImportLocationDto> invalidUsers)
        {
            if (invalidUsers.Any())
            {
                var file = _locationExcelExporter.ExportToFile(invalidUsers);
                await _appNotifier.SomeLocationsCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllLocationsSuccessfullyImportedFromExcel", DineConnectConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }

        private void SendInvalidExcelNotification(ImportLocationsFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToLocationList", DineConnectConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }
    }
}

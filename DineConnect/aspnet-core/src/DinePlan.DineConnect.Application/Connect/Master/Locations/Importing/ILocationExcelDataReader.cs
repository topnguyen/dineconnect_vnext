﻿using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Locations.Importing
{
    public interface ILocationExcelDataReader
    {
        List<ImportLocationDto> GetLocationsFromExcel(byte[] fileBytes);
    }
}

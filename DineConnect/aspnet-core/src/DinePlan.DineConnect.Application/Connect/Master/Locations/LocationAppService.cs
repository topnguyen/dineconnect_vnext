﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos.Api;
using DinePlan.DineConnect.Connect.Master.Locations.Exporting;
using DinePlan.DineConnect.Connect.Master.Locations.Importing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Master.Locations
{
    public class LocationAppService : DineConnectAppServiceBase, ILocationAppService
    {
        private readonly ILocationManager _locationManager;
        private readonly ConnectSession _connectSession;
        private readonly ISettingManager _settingManager;
        private readonly ILocationExcelExporter _excelExporter;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Group> _locationGroupRepository;
        private readonly IRepository<LocationSchedule> _locationScheduleRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<Brand> _brandRepository;
        private readonly IRepository<LocationBranch> _branchRepository;

        public LocationAppService(
            ConnectSession connectSession,
            ILocationManager locationManager,
            IAddressesManager addressManager,
            ISettingManager settingManager,
            ILocationExcelExporter excelExporter,
            ITempFileCacheManager tempFileCacheManager,
            ILocationExcelDataReader locationExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager,
            IRepository<Location> locationRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<Brand> brandRepository,
            IRepository<Group> locationGroupRepository,
            IRepository<LocationSchedule> locationScheduleRepo,
            IRepository<LocationBranch> branchRepository)
        {
            _locationManager = locationManager;
            _connectSession = connectSession;
            _settingManager = settingManager;
            _excelExporter = excelExporter;
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            _backgroundJobManager = backgroundJobManager;
            _locationScheduleRepo = locationScheduleRepo;
            _locationRepository = locationRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _brandRepository = brandRepository;
            _branchRepository = branchRepository;
            _locationGroupRepository = locationGroupRepository;
        }

        
        public async Task<int> CreateOrUpdteAsync(LocationDto input)
        {
            //input.LocationGroups = ObjectMapper
            var location = ObjectMapper.Map<Location>(input);
            location.TenantId = AbpSession.TenantId ?? 0;
            location.LocationMenuItems = null;
            location.Organization = null;
            location.City = null;

            var id = await _locationManager.CreateOrUpdteAsync(location);

            return id;
        }

        public async Task<bool> IsLocationExists(CheckLocationInput input)
        {
            if (!string.IsNullOrEmpty(input.NonLocations))
            {
                var allLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(input.NonLocations);
                var allIds = allLocations.Select(a => a.Id).ToList();
                return !allIds.Contains(input.LocationId);
            }

            if (input.Group && !string.IsNullOrEmpty(input.Locations))
            {
                var myG = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(input.Locations);
                var allIds = myG.Select(a => a.Id).ToList();

                var location = await _locationRepository.GetAsync(input.LocationId);

                if (location != null && location.LocationGroups.Any())
                {
                    return allIds.Any(a => location.LocationGroups.Any(lg => a == lg.Id));
                }
                return false;
            }
            if (input.LocationTag && !string.IsNullOrEmpty(input.Locations))
            {
                var myG = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(input.Locations);
                var allIds = myG.Select(a => a.Id).ToList();

                var location = await _locationRepository.GetAsync(input.LocationId);

                if (location != null && location.LocationTags.Any())
                {
                    return allIds.Any(a => location.LocationTags.Any(lg => a == lg.Id));
                }
                return false;
            }
            if (!input.Group && !string.IsNullOrEmpty(input.Locations))
            {
                var allLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(input.Locations);

                var allIds = allLocations.Select(a => a.Id).ToList();
                return allIds.Any(a => a == input.LocationId);
            }
            return true;
        }

        
        public async Task DeleteAsync(int id)
        {
            try
            {
                var users = await UserManager.Users.ToListAsync();

                foreach (var item in users)
                {
                    bool result = Int32.TryParse(await _settingManager.GetSettingValueForUserAsync("LocationId", _connectSession.TenantId, item.Id), out int userDefaultLocationId);
                    if (result)
                    {
                        if (userDefaultLocationId == id)
                        {
                            throw new UserFriendlyException("Can not delete, this is the default location for some user");
                        }
                    }
                }

                await _locationManager.DeleteAsync(id);
            }
            catch (DbUpdateException ex)
            {
                HandleException(ex);
            }
        }
        public async Task<List<ScheduleSetting>> GetLocationSchedulesByLocation(int locationId)
        {
            var list = await _locationScheduleRepo.GetAllListAsync(e => e.LocationId == locationId);

            return ObjectMapper.Map<List<ScheduleSetting>>(list);
        }
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _locationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _locationRepository.UpdateAsync(item);
            }
        }

        private void HandleException(DbUpdateException ex)
        {
            if (ex.Message.Contains("REFERENCE constraint"))
                throw new UserFriendlyException("Can not delete, delete related information and try again");

            if (ex.InnerException != null)
            {
                if (ex.InnerException.Message.Contains("REFERENCE constraint"))
                    throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            throw new UserFriendlyException("Something went wrong");
        }

        public async Task<LocationDto> GetAsync(int id)
        {
            var company = await _locationManager.GetAsync(id);

            var dto = ObjectMapper.Map<LocationDto>(company);
            return dto;
        }

        public async Task<PagedResultDto<LocationDto>> GetList(GetLocationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var locations = _locationManager.GetAllLocationIgnoringBrandFilter(input.OrganizationId, input.Filter, input.IsDeleted); //GetList(input.OrganizationId,input.Filter);
                var sortedOrganization = await locations
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

                var dtos = ObjectMapper.Map<List<LocationDto>>(sortedOrganization);

                return new PagedResultDto<LocationDto>(await locations.CountAsync(), dtos);
            }
        }

        public async Task<PagedResultDto<SimpleLocationDto>> GetSimpleLocations(GetLocationInput input)
        {
            var locations = _locationManager.GetAllLocationIgnoringBrandFilter(input.OrganizationId, input.Filter);
            var sortedLocation = await locations
            .OrderBy(input.Sorting ?? "id asc")
            .PageBy(input)
            .ToListAsync();

            return new PagedResultDto<SimpleLocationDto>(await locations.CountAsync(), ObjectMapper.Map<List<SimpleLocationDto>>(sortedLocation));
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var filteredOrganization = await _locationManager.GetAllLocationIgnoringBrandFilter(0, string.Empty)
                .ToListAsync();
            var allListDtos = filteredOrganization
                .Select(l =>
                {
                    var dto = ObjectMapper.Map<LocationDto>(l);
                    return dto;
                }).ToList();

            return _excelExporter.ExportToFile(allListDtos);
        }

        public Task<FileDto> GetTemplateExcelToImport()
        {
            return Task.FromResult(_excelExporter.ExportTemplateToFile());
        }

        
        public async Task ImportLocationFromExcel(string fileToken)
        {
            if (!fileToken.IsNullOrWhiteSpace())
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();
                        List<string> headers = new List<string>
                        {
                            "OrganizationCode",
                            "Code",
                            "Name",
                            "Address1",
                            "Address2",
                            "Address3",
                            "City",
                            "State",
                            "Fax",
                            "Website",
                            "Email"
                        };

                        var columns = worksheet.Dimension.End.Column;
                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (int i = 1; i < columns + 1; i++)
                            {
                                sheetHeaders.Add(worksheet.GetValue(1, i).ToString());
                            }

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") + " colum to template. If not applicable please leave it blank.");
                        }
                    }
                }

                var tenantId = AbpSession.TenantId;

                var fileObject = new BinaryObject(tenantId, fileBytes);

                await _binaryObjectManager.SaveAsync(fileObject);

                await _backgroundJobManager.EnqueueAsync<ImportLocationsToExcelJob, ImportLocationsFromExcelJobArgs>(new ImportLocationsFromExcelJobArgs
                {
                    TenantId = tenantId.Value,
                    BinaryObjectId = fileObject.Id,
                    User = AbpSession.ToUserIdentifier()
                });
            }
        }

        public async Task<int> GetOrgnizationIdByLocationId(int locationId)
        {
            var location = await _locationRepository.GetAll().FirstOrDefaultAsync(e => e.Id == locationId);
            if (location != null)
            {
                return location.OrganizationId;
            }
            return 0;
        }
   
        
        public async Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUser(ApiUserInput input)
        {
            IQueryable<Location> allItems;

            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null)
            {
                return null;
            }

            var userorg = await _userOrganizationUnitRepository.GetAllListAsync(a => a.UserId == input.UserId);
            if (userorg.Count == 0)
            {
                allItems = _locationRepository.GetAll();
            }
            else
            {
                allItems = from loc in _locationRepository.GetAll()
                    join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
                        on loc.Id equals ou.OrganizationUnitId
                    select loc;
            }
            var sortLocationOutput = await allItems.ToListAsync();

            var locationOuputs = sortLocationOutput.Select(sortL => new ApiLocationOutput
            {
                Id = sortL.Id,
                Code = sortL.Code,
                Name = sortL.Name
            }).ToList();

            return new ListResultDto<ApiLocationOutput>(locationOuputs);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCode(string code)
        {
            /*
            var locGroup = await _locationGroupRepository.FirstOrDefaultAsync(a => a.Code!= null &&
                                                                             a.Code.Equals(code));
            if (locGroup != null)
            {
                var locationGroup = await _locationGroupRepository.GetAll()
                    .Include(lg => lg.LocationGroups)
                    .WhereIf(code != null, lg => lg.Id == locGroup.Id)
                    .FirstOrDefaultAsync();

                if (locationGroup != null)
                {
                    var query = locationGroup.LocationGroups.Select(l => new ComboboxItemDto
                    {
                        DisplayText = l.Name,
                        Value = l.Id.ToString(),
                    }).ToList();

                    return new ListResultDto<ComboboxItemDto>(query);
                }
            }*/
            return null;
        }

        public List<int> GetLocationForUserAndGroup(CommonLocationGroupDto dto)
        {
            var rLocations = new List<int>();
            if (!dto.Group && !dto.Locations.Any())
            {
                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId });
                    if (allLocations != null && allLocations.Any())
                    {
                        rLocations = allLocations.Select(a => a.Id).ToList();
                    }
                }
            }
            else if (!dto.Group)
            {
                rLocations = dto.Locations.Select(a => a.Id).ToList();
            }
            else
            {
                var allLgids = dto.Groups.Select(a => a.Id).ToList();
                var allLoca =
                    _locationRepository.GetAll()
                        .Where(a => a.LocationGroups.Where(lg => allLgids.Contains(lg.Id)).Any());

                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId }).Select(a => a.Id).ToList();
                    rLocations = allLoca.Where(a => allLocations.Contains(a.Id)).Select(a => a.Id).ToList();
                }
            }
            return rLocations;
        }

        public List<SimpleLocationDto> GetLocationsForUser(GetLocationInputBasedOnUser input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var allItems = GetLocationByUserId(input.UserId);

                if (!input.LocationName.IsNullOrEmpty())
                {
                    allItems =
                        allItems.Where(t => t.Name.Contains(input.LocationName) || t.Code.Contains(input.LocationName));
                }

                var sortMenuItems = allItems.ToList();

                var allListDtos = ObjectMapper.Map<List<SimpleLocationDto>>(sortMenuItems);
                return allListDtos;
            }
        }

        private IQueryable<Location> GetLocationByUserId(long userId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                IQueryable<Location> allItems;

                var userorg = _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == userId);
                if (!userorg.Any())
                {
                    allItems = _locationRepository.GetAll();
                }
                else
                {
                    allItems = from loc in _locationRepository.GetAll().Where(a => !a.IsDeleted)
                               join ou in userorg
                                   on loc.Id equals ou.OrganizationUnitId
                               select loc;
                }
                return allItems;
            }
        }

        public async Task<ApiLocationSetting> ApiGetLocationSettings(ApiLocationInput locationInput)
        {
            var api = new ApiLocationSetting();

            var location = await _locationRepository.GetAll()
                .Include(l => l.Branch)
                .Include(l => l.Organization)
                .FirstOrDefaultAsync(l => l.Id == locationInput.LocationId);
            if (location != null)
            {
                api.ChangeTax = location.ChangeTax;
                api.TaxInclusive = location.TaxInclusive;

                var myCompany = location.Organization;

                if (myCompany == null)
                {
                    myCompany = await _brandRepository.GetAsync(location.OrganizationId);
                }

                if (myCompany != null)
                {
                    api.CompanyCode = myCompany.Code;
                    api.CompanyName = myCompany.Name;
                    api.CompanyAddress1 = myCompany.Address1;
                    api.CompanyAddress2 = myCompany.Address2;
                    api.CompanyAddress3 = myCompany.Address3;
                    api.CompanyCity = myCompany.City;
                    api.CompanyState = myCompany.State;
                    api.CompanyCountry = myCompany.Country;
                    api.CompanyPhoneNumber = myCompany.PhoneNumber;
                    api.CompanyWebsite = myCompany.Website;
                    api.CompanyEmail = myCompany.Email;
                    api.CompanyTaxCode = myCompany.GovtApprovalId;
                }

                api.Id = location.Id;
                api.Code = location.Code;
                api.Name = location.Name;
                api.Address1 = location.Address1;
                api.Address2 = location.Address2;
                api.Address3 = location.Address3;
                api.District = location.District;
                api.City = location.City?.Name;

                api.State = location.State;
                api.PostalCode = location.PostalCode;
                api.Country = location.Country;
                api.Website = location.Website;
                api.Email = location.Email;
                api.PhoneNumber = location.PhoneNumber;
                api.LocationTaxCode = location.LocationTaxCode;
                api.AddOn = location.AddOn;

                var myBranch = location.Branch;

                if (myBranch == null && location.BranchId != null)
                {
                    myBranch = await _branchRepository.GetAsync(location.BranchId.Value);
                }

                if (myBranch != null)
                {
                    api.BranchId = myBranch.Id.ToString();
                    api.BranchCode = myBranch.Code;
                    api.BranchName = myBranch.Name;
                    api.BranchAddress1 = myBranch.Address1;
                    api.BranchAddress2 = myBranch.Address2;
                    api.BranchAddress3 = myBranch.Address3;
                    api.BranchDistrict = myBranch.District;
                    api.BranchCity = myBranch.City;
                    api.BranchState = myBranch.State;
                    api.BranchPostalCode = myBranch.PostalCode;
                    api.BranchCountry = myBranch.Country;
                    api.BranchWebsite = myBranch.Website;
                    api.BranchEmail = myBranch.Email;
                    api.BranchPhoneNumber = myBranch.PhoneNumber;
                    api.BranchLocationTaxCode = myBranch.BranchTaxCode;
                }
            }
            return api;
        }
    }
}
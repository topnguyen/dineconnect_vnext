﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Locations.Exporting
{
    public class LocationExcelExporter : NpoiExcelExporterBase, ILocationExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LocationExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<LocationDto> items)
        {
            return CreateExcelPackage(
                "Locations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                        L("Brand"),
                        L("Code"),
                        L("Name"),
                        L("Address1"),
                        L("Address2"),
                        L("Address3"),
                        L("Country"),
                        L("State"),
                        L("City"),
                        L("PhoneNumber"),
                        L("Fax"),
                        L("Website"),
                        L("Email"),
                        L("UpdatedDate")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.OrganizationName,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.Address1,
                        _ => _.Address2,
                        _ => _.Address3,
                        _ => _.CountryName,
                        _ => _.StateName,
                        _ => _.CityName,
                        _ => _.PhoneNumber,
                        _ => _.Fax,
                        _ => _.Website,
                        _ => _.Email,
                        _ => _.LastModificationTime
                        );

                    for (var i = 1; i <= 15; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportTemplateToFile()
        {
            return CreateExcelPackage(
                "LocationsTemplate.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                        L("OrganizationCode"),
                        L("Code"),
                        L("Name"),
                        L("Address1"),
                        L("Address2"),
                        L("Address3"),
                        L("City"),
                        L("State"),
                        L("Country"),
                        L("PhoneNumber"),
                        L("Fax"),
                        L("Website"),
                        L("Email")
                        );

                    for (var i = 1; i <= 13; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportToFile(List<ImportLocationDto> importLocationDtos)
        {
            return CreateExcelPackage(
                "InvalidLocationsImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                        L("OrganizationCode"),
                        L("Code"),
                        L("Name"),
                        L("Address1"),
                        L("Address2"),
                        L("Address3"),
                        L("City"),
                        L("State"),
                        L("Country"),
                        L("PhoneNumber"),
                        L("Fax"),
                        L("Website"),
                        L("Email"),
                        L("RefuseReason")
                        );

                    AddObjects(
                        sheet, 2, importLocationDtos,
                        _ => _.OrganizationCode,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.Address1,
                        _ => _.Address2,
                        _ => _.Address3,
                        _ => _.CityName,
                        _ => _.StateName,
                        _ => _.CountryName,
                        _ => _.PhoneNumber,
                        _ => _.Fax,
                        _ => _.Website,
                        _ => _.Email,
                        _ => _.Exception
                        );

                    for (var i = 1; i <= 15; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
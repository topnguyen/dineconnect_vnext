﻿using DinePlan.DineConnect.Connect.Master.Locations.Importing.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Locations.Exporting
{
    public interface ILocationExcelExporter
    {
        FileDto ExportToFile(List<LocationDto> items);
        FileDto ExportTemplateToFile();
        FileDto ExportToFile(List<ImportLocationDto> importLocationDtos);
    }
} 
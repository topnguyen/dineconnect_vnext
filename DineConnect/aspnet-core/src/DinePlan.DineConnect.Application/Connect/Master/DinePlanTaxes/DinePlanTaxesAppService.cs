﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Exporting;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes
{
    
    public class DinePlanTaxesAppService : DineConnectAppServiceBase, IDinePlanTaxesAppService
    {
        private readonly IRepository<DinePlanTax> _dinePlanTaxRepository;
        private readonly IDinePlanTaxesExcelExporter _dinePlanTaxesExcelExporter;
        private readonly IRepository<DinePlanTaxLocation> _dinePlanTaxLocationRepository;
        private readonly IRepository<DinePlanTaxMapping> _dinePlanTaxMappingRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<TransactionType> _transactiontypeRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Department> _departmentRepository;
        private readonly IRepository<FutureDateInformation> _futureDateRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DinePlanTaxesAppService(IRepository<DinePlanTax> dinePlanTaxRepository
            , IDinePlanTaxesExcelExporter dinePlanTaxesExcelExporter
            , IRepository<DinePlanTaxLocation> dinePlanTaxLocationRepository
            , IRepository<Location> locationRepository
            , IRepository<DinePlanTaxMapping> dinePlanTaxMappingRepository
            , ConnectSession connectSession
            , ILocationAppService locationAppService
            , IRepository<TransactionType> transactiontypeRepository
            , IRepository<MenuItem> menuItemRepository
            , IRepository<Category> categoryRepository
            , IRepository<Department> departmentRepository
            , IRepository<FutureDateInformation> futureDateRepository
            , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _dinePlanTaxRepository = dinePlanTaxRepository;
            _dinePlanTaxesExcelExporter = dinePlanTaxesExcelExporter;
            _dinePlanTaxLocationRepository = dinePlanTaxLocationRepository;
            _locationRepository = locationRepository;
            _dinePlanTaxMappingRepository = dinePlanTaxMappingRepository;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
            _transactiontypeRepository = transactiontypeRepository;
            _menuItemRepository = menuItemRepository;
            _categoryRepository = categoryRepository;
            _departmentRepository = departmentRepository;
            _futureDateRepository = futureDateRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<DinePlanTaxDto>> GetAll(GetAllDinePlanTaxesInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredDinePlanTaxes = _dinePlanTaxRepository.GetAll()
                        .Include(e => e.TransactionType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => EF.Functions.Like(e.TaxName, input.Filter.ToILikeString()) || EF.Functions.Like(e.TransactionType.Name, input.Filter.ToILikeString()))
                        .Where(x => x.IsDeleted == input.IsDeleted);

                if (!input.LocationIds.IsNullOrWhiteSpace())
                {
                    var locationIds = input.ParseLocationIds();
                    var dinePlanTaxLocationQuery = _dinePlanTaxLocationRepository.GetAll().Where(x => x.Locations != null &&
                    x.Group == input.LocationGroup && x.LocationTag == input.LocationTag);

                    var locationIdPrefixes = locationIds.Select(id => $"{{\"Id\":{id},").ToList();

                    if (input.NonLocation)
                        locationIdPrefixes.ForEach(prefix =>
                        {
                            dinePlanTaxLocationQuery = dinePlanTaxLocationQuery.Where(x => x.NonLocations.Contains(prefix));
                        });
                    else
                        locationIdPrefixes.ForEach(prefix =>
                        {
                            dinePlanTaxLocationQuery = dinePlanTaxLocationQuery.Where(x => x.Locations.Contains(prefix));
                        });

                    var dinePlanTaxIds = await dinePlanTaxLocationQuery.Select(x => x.DinePlanTaxRefId).Distinct().ToListAsync();
                    filteredDinePlanTaxes = filteredDinePlanTaxes.Where(x => dinePlanTaxIds.Contains(x.Id));
                }

                var pagedAndFilteredDinePlanTaxes = await filteredDinePlanTaxes
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var dinePlanTaxes = ObjectMapper.Map<List<DinePlanTaxDto>>(pagedAndFilteredDinePlanTaxes);

                var totalCount = await filteredDinePlanTaxes.CountAsync();

                return new PagedResultDto<DinePlanTaxDto>(totalCount, dinePlanTaxes);
            }
        }

        
        public async Task<GetDinePlanTaxForEditOutput> GetDinePlanTaxForEdit(EntityDto input)
        {
            var dinePlanTax = await _dinePlanTaxRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetDinePlanTaxForEditOutput { DinePlanTax = ObjectMapper.Map<CreateOrEditDinePlanTaxDto>(dinePlanTax) };

            return output;
        }

        public async Task<int> CreateOrEdit(CreateOrEditDinePlanTaxDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            if (input.Id == null)
            {
                return await Create(input);
            }
            return await Update(input);
        }

        
        protected virtual async Task<int> Create(CreateOrEditDinePlanTaxDto input)
        {
            var dinePlanTax = ObjectMapper.Map<DinePlanTax>(input);

            if (AbpSession.TenantId != null)
            {
                dinePlanTax.TenantId = (int)AbpSession.TenantId;
            }

            var count = await _dinePlanTaxRepository.CountAsync(x => x.TaxName == dinePlanTax.TaxName);
            if (count > 0)
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            var id = await _dinePlanTaxRepository.InsertAndGetIdAsync(dinePlanTax);

            return id;
        }

        
        protected virtual async Task<int> Update(CreateOrEditDinePlanTaxDto input)
        {
            var dinePlanTax = await _dinePlanTaxRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, dinePlanTax);

            return await _dinePlanTaxRepository.InsertOrUpdateAndGetIdAsync(dinePlanTax);
        }

        
        public async Task Delete(EntityDto input)
        {
            var taxLoctionsNeedRemove = await _dinePlanTaxLocationRepository.GetAll().Where(e => e.DinePlanTaxRefId == input.Id).ToListAsync();
            foreach (var item in taxLoctionsNeedRemove)
            {
                await DeleteDinePlanTaxLocation(new DeleteDinePlanTaxLocationInput() { Id = item.Id });
            }

            await _dinePlanTaxRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dinePlanTaxRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _dinePlanTaxRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetDinePlanTaxesToExcel(GetAllDinePlanTaxesForExcelInput input)
        {
            var filteredDinePlanTaxes = _dinePlanTaxRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.TaxName, input.Filter.ToILikeString()));

            var query = filteredDinePlanTaxes
                         .Select(o => new GetDinePlanTaxForViewDto()
                         {
                             DinePlanTax = ObjectMapper.Map<DinePlanTaxDto>(o)
                         });

            var dinePlanTaxListDtos = await query.ToListAsync();

            return _dinePlanTaxesExcelExporter.ExportToFile(dinePlanTaxListDtos);
        }

        public async Task<PagedResultDto<DinePlanTaxReturnDto>> GetLocationTaxesForGivenTax(GetLocationTaxesForGivenTaxInput input, CommonLocationGroupFilterInputDto locationInput)
        {
            var entriesQuery = _dinePlanTaxLocationRepository.GetAll()
                .Where(x => x.DinePlanTaxRefId == input.DinePlanTaxRefId);

            if (locationInput != null && !locationInput.LocationIds.IsNullOrWhiteSpace())
            {
                var locationIds = locationInput.ParseLocationIds();
                entriesQuery = entriesQuery.Where(x => x.Locations != null &&
                    x.Group == locationInput.LocationGroup && x.LocationTag == locationInput.LocationTag);

                var locationIdPrefixes = locationIds.Select(id => $"{{\"Id\":{id},").ToList();

                if (locationInput.NonLocation)
                    locationIdPrefixes.ForEach(prefix =>
                    {
                        entriesQuery = entriesQuery.Where(x => x.NonLocations.Contains(prefix));
                    });
                else
                    locationIdPrefixes.ForEach(prefix =>
                    {
                        entriesQuery = entriesQuery.Where(x => x.Locations.Contains(prefix));
                    });
            }

            var items = entriesQuery
                .OrderBy(input.Sorting ?? "Id asc")
                .PageBy(input).Select(x => new DinePlanTaxReturnDto()
                {
                    Id = x.Id,
                    TaxPercentage = x.TaxPercentage,
                }).ToList();

            return new PagedResultDto<DinePlanTaxReturnDto>(await entriesQuery.CountAsync(), items);
        }

        public async Task CreateOrUpdateTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            if (input.DinePlanTaxLocation.Id.HasValue)
                await UpdateTaxLocation(input);
            else
                await CreateTaxLocation(input);
        }

        protected virtual async Task CreateTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            if (!input.TaxTemplateMapping.Any())
                throw new UserFriendlyException(L("MinimumOneDetail"));

            var taxlocationId = await CreateUpdateTaxLocationSync(input);

            // --

            foreach (var taxMappingDto in input.TaxTemplateMapping)
            {
                var taxMapping = ObjectMapper.Map<DinePlanTaxMapping>(taxMappingDto);
                taxMapping.DinePlanTaxLocationId = taxlocationId;

                await _dinePlanTaxMappingRepository.InsertAndGetIdAsync(taxMapping);
            }
        }

        protected virtual async Task UpdateTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            var taxLocationId = await CreateUpdateTaxLocationSync(input);
            var taxMappingDtos = input.TaxTemplateMapping;

            if (taxMappingDtos != null)
            {
                var taxMappings = (await _dinePlanTaxMappingRepository
                    .GetAllListAsync(x => x.DinePlanTaxLocationId == taxLocationId))
                    .ToDictionary(x => x.Id, x => x);

                var updatedTaxMappingIds = new List<int>();
                foreach (var taxMappingDto in taxMappingDtos)
                {
                    if (taxMappingDto.Id.HasValue && taxMappingDto.Id > 0 && taxMappings.ContainsKey(taxMappingDto.Id.Value))
                    {
                        var taxMappingId = taxMappingDto.Id.Value;
                        var taxMapping = taxMappings[taxMappingId];

                        taxMapping.DepartmentId = taxMappingDto.DepartmentId;
                        taxMapping.CategoryId = taxMappingDto.CategoryId;
                        taxMapping.MenuItemId = taxMappingDto.MenuItemId;

                        await _dinePlanTaxMappingRepository.UpdateAsync(taxMapping);

                        updatedTaxMappingIds.Add(taxMappingId);
                    }
                    else
                    {
                        var taxMapping = ObjectMapper.Map<DinePlanTaxMapping>(taxMappingDto);
                        taxMapping.DinePlanTaxLocationId = taxLocationId;

                        await _dinePlanTaxMappingRepository.InsertAndGetIdAsync(taxMapping);
                    }
                }

                var deletedTaxMappingIds = taxMappings.Keys.Where(x => !updatedTaxMappingIds.Contains(x)).ToList();
                foreach (var id in deletedTaxMappingIds)
                    await _dinePlanTaxMappingRepository.DeleteAsync(id);
            }
        }

        public async Task<CreateOrUpdateDinePlanTaxLocationInput> GetDinePlanTaxLocationForEdit(GetDinePlanTaxLocationInput input)
        {
            var taxLocationEditDto = new DinePlanTaxLocationEditDto();
            var taxMappingEditDtos = new List<DinePlanTaxMappingEditDto>();

            var taxLocation = _dinePlanTaxLocationRepository.FirstOrDefault(x => x.Id == input.Id);
            if (taxLocation != null)
            {
                taxLocationEditDto = ObjectMapper.Map<DinePlanTaxLocationEditDto>(taxLocation);
                GetLocationsForEdit(taxLocationEditDto, taxLocationEditDto.LocationGroup);
                taxLocationEditDto.Locations = null;
                taxLocationEditDto.NonLocations = null;

                taxMappingEditDtos = await _dinePlanTaxMappingRepository.GetAll()
                    .Where(x => x.DinePlanTaxLocationId == taxLocationEditDto.Id)
                    .Select(x => ObjectMapper.Map<DinePlanTaxMappingEditDto>(x))
                    .ToListAsync();
            }
            else throw new UserFriendlyException(L("TaxLocationNotFound"));

            return new CreateOrUpdateDinePlanTaxLocationInput
            {
                DinePlanTaxLocation = taxLocationEditDto,
                TaxTemplateMapping = taxMappingEditDtos
            };
        }

        public async Task<int> AddOrEditDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            await BusinessRulesForDinePlanTaxLocation(input);
            Collection<DinePlanTaxLocationEditDto> valuesOutput = new Collection<DinePlanTaxLocationEditDto>();
            if (input.FutureDataId.HasValue && input.FutureDataId > 0)
            {
                var dineplantaxlocDto = input.DinePlanTaxLocation;
                var dto = ObjectMapper.Map<DinePlanTaxLocation>(dineplantaxlocDto);
                UpdateLocations(dto, dineplantaxlocDto.LocationGroup);

                var futureData = await _futureDateRepository.FirstOrDefaultAsync(t => t.Id == input.FutureDataId);
                GetDinePlanTaxForEditOutput existData = new GetDinePlanTaxForEditOutput();
                if (!string.IsNullOrEmpty(futureData.ObjectContents))
                {
                    existData = JsonConvert.DeserializeObject<GetDinePlanTaxForEditOutput>(futureData.ObjectContents);
                    existData.DinePlanTax = input.DinePlanTax;
                    if (existData.DinePlanTax.DinePlanTaxLocations.Count > 0)
                    {
                        Collection<DinePlanTaxLocationEditDto> dineplantaxlocationDto = ObjectMapper.Map<Collection<DinePlanTaxLocationEditDto>>(existData.DinePlanTax.DinePlanTaxLocations);
                        DinePlanTaxLocationEditDto cloneRecord = null;
                        if (input.DinePlanTaxLocation.CloneFlag == true)
                        {
                            foreach (var lst in dineplantaxlocationDto)
                            {
                                GetLocationsForEdit(lst, lst.LocationGroup);
                                valuesOutput.Add(lst);
                            }
                            cloneRecord = ObjectMapper.Map<DinePlanTaxLocationEditDto>(dto);
                            if (valuesOutput.Count > 0)
                                cloneRecord.FutureDataSno = valuesOutput.Max(t => t.FutureDataSno) + 1;
                            else
                                cloneRecord.FutureDataSno = 1;
                            valuesOutput.Add(cloneRecord);
                            existData.DinePlanTax.DinePlanTaxLocations = valuesOutput;
                        }
                        else
                        {
                            foreach (var lst in dineplantaxlocationDto)
                            {
                                if (lst.FutureDataSno == input.DinePlanTaxLocation.FutureDataSno)
                                {
                                    ObjectMapper.Map(dto, lst);
                                    GetLocationsForEdit(lst, input.DinePlanTaxLocation.LocationGroup);
                                }
                                else
                                {
                                    GetLocationsForEdit(lst, lst.LocationGroup);
                                    valuesOutput.Add(lst);
                                }
                            }
                            if (input.DinePlanTaxLocation.FutureDataSno == 0)
                            {
                                cloneRecord = ObjectMapper.Map<DinePlanTaxLocationEditDto>(dto);
                                if (valuesOutput.Count > 0)
                                    cloneRecord.FutureDataSno = valuesOutput.Max(t => t.FutureDataSno) + 1;
                                else
                                    cloneRecord.FutureDataSno = 1;
                                valuesOutput.Add(cloneRecord);
                                existData.DinePlanTax.DinePlanTaxLocations = valuesOutput;
                            }
                        }
                    }
                    else
                    {
                        var dineplantaxlocation = ObjectMapper.Map<DinePlanTaxLocationEditDto>(dto);
                        dineplantaxlocation.FutureDataSno = 1;
                        valuesOutput.Add(dineplantaxlocation);
                        existData.DinePlanTax.DinePlanTaxLocations = valuesOutput;
                    }
                }

                GetDinePlanTaxForEditOutput dtoJson = new GetDinePlanTaxForEditOutput
                {
                    DinePlanTax = existData.DinePlanTax,
                    //DinePlanTaxLocation = new DinePlanTaxLocationEditDto()
                };
                var objectJson = JsonConvert.SerializeObject(dtoJson);
                futureData.ObjectContents = objectJson;
                await _futureDateRepository.UpdateAsync(futureData);
                return 1;
            }
            return 0;
            //else if (input.DinePlanTaxLocation.Id.HasValue && input.DinePlanTaxLocation.Id > 0)
            //{
            //    var existEntry = await _dinePlanTaxLocationRepository.FirstOrDefaultAsync(t => t.Id == input.DinePlanTaxLocation.Id.Value);
            //    existEntry.TaxPercentage = input.DinePlanTaxLocation.TaxPercentage;
            //    UpdateLocations(existEntry, input.DinePlanTaxLocation.LocationGroup);

            //    if (input.DinePlanTaxLocation.DinePlanTaxMappings != null && input.DinePlanTaxLocation.DinePlanTaxMappings.Any())
            //    {
            //        var oids = new List<int>();
            //        foreach (var otdto in input.DinePlanTaxLocation.DinePlanTaxMappings)
            //        {
            //            if (otdto.Id != null && otdto.Id > 0)
            //            {
            //                oids.Add(otdto.Id.Value);
            //                var fromUpdation = existEntry.DinePlanTaxMappings.SingleOrDefault(a => a.Id.Equals(otdto.Id));
            //                UpdateTaxMappingPortion(fromUpdation, otdto);
            //            }
            //            else
            //            {
            //                existEntry.DinePlanTaxMappings.Add(Mapper.Map<DinePlanTaxMappingEditDto, DinePlanTaxMapping>(otdto));
            //            }
            //        }
            //        var tobeRemoved = existEntry.DinePlanTaxMappings.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
            //        foreach (var dinePlanTaxMapping in tobeRemoved)
            //            try
            //            {
            //                await _dineplantaxmappingRepo.DeleteAsync(dinePlanTaxMapping.Id);
            //            }
            //            catch (Exception exception)
            //            {
            //                throw new UserFriendlyException(exception.Message);
            //            }
            //    }
            //    else
            //    {
            //        foreach (var dineplantaxmapping in existEntry.DinePlanTaxMappings) await _dineplantaxmappingRepo.DeleteAsync(dineplantaxmapping.Id);
            //    }

            //    return new IdInput { Id = existEntry.Id };
            //}
            //else
            //{
            //    var dto = input.DinePlanTaxLocation.MapTo<DinePlanTaxLocation>();
            //    UpdateLocationAndNonLocation(dto, input.DinePlanTaxLocation.LocationGroup);
            //    await _dineplantaxlocationRepo.InsertOrUpdateAndGetIdAsync(dto);
            //    return new IdInput { Id = dto.Id };
            //}
        }

        public async Task DeleteDinePlanTaxLocation(DeleteDinePlanTaxLocationInput input)
        {
            var dinetaxlocation = await _dinePlanTaxLocationRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

            if (dinetaxlocation == null)
                throw new UserFriendlyException(L("TaxLocationNotFound"));

            var taxMappingIds = await _dinePlanTaxMappingRepository.GetAllIncluding()
                        .Where(x => x.DinePlanTaxLocationId == dinetaxlocation.Id)
                        .Select(x => x.Id)
                        .ToListAsync();

            foreach (var id in taxMappingIds)
                await _dinePlanTaxMappingRepository.DeleteAsync(id);

            await _dinePlanTaxLocationRepository.DeleteAsync(dinetaxlocation.Id);
            //await _syncAppService.UpdateSync(SyncConsts.TAX);
        }

        protected virtual async Task<int> CreateUpdateTaxLocationSync(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            var locationGroupDto = input.DinePlanTaxLocation.LocationGroup;
            DinePlanTaxLocation taxLocation;

            if (input.DinePlanTaxLocation.Id.HasValue && input.DinePlanTaxLocation.Id > 0)
            {
                taxLocation = await _dinePlanTaxLocationRepository.GetAsync(input.DinePlanTaxLocation.Id.Value);
                ObjectMapper.Map(input.DinePlanTaxLocation, taxLocation);
            }
            else
                taxLocation = ObjectMapper.Map<DinePlanTaxLocation>(input.DinePlanTaxLocation);

            await CheckTaxLocationBusinessRules(taxLocation, locationGroupDto);
            UpdateLocations(taxLocation, locationGroupDto);

            taxLocation.OrganizationId = _connectSession.OrgId;
            taxLocation.TenantId = _connectSession.TenantId ?? 0;

            return await _dinePlanTaxLocationRepository.InsertOrUpdateAndGetIdAsync(taxLocation);
        }

        private async Task CheckTaxLocationBusinessRules(DinePlanTaxLocation dinePlanTaxlocation, CommonLocationGroupDto locationGroupDto)
        {
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedTaxLocationQuery = _dinePlanTaxLocationRepository.GetAll()
                .Where(x => x.DinePlanTaxRefId == dinePlanTaxlocation.DinePlanTaxRefId)
                .WhereIf(dinePlanTaxlocation.Id > 0, x => x.Id != dinePlanTaxlocation.Id);

            var existedTaxLocations = await existedTaxLocationQuery.ToListAsync();
            existedTaxLocations.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        public async Task<decimal> GetTaxPercentageForTiffin()
        {
            var locationIdPrefix = $"{{\"Id\":{_connectSession.LocationId},";
            var percentage = await _dinePlanTaxLocationRepository.GetAll()
                .Where(e => e.Locations != null && e.Locations.Contains(locationIdPrefix))
                .SumAsync(e => e.TaxPercentage);

            return percentage;
        }

        public async Task<List<GetDinePlanTaxForComboboxDto>> GetDinePlanTaxForCombobox(int? selectedValue)
        {
            var dinePlanTax = await _dinePlanTaxRepository
              .GetAllListAsync();

            var dinePlanTaxItems = new List<GetDinePlanTaxForComboboxDto>(dinePlanTax.Select(x =>
                new GetDinePlanTaxForComboboxDto(x.Id.ToString(), x.TaxName, false)));

            if (dinePlanTaxItems.Count > 0)
            {
                if (selectedValue.HasValue)
                {
                    var selectedItem = dinePlanTaxItems.FirstOrDefault(x => x.Value == selectedValue.Value.ToString());
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        dinePlanTaxItems[0].IsSelected = true;
                    }
                }
            }

            return dinePlanTaxItems;
        }

        private async Task BusinessRulesForDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            Collection<DinePlanTaxLocationEditDto> dinePlanTaxLocationDtos = new Collection<DinePlanTaxLocationEditDto>();
            var locationGroupDto = input.DinePlanTaxLocation.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }
            if (input.FutureDataId.HasValue && input.FutureDataId > 0)
            {
                var futureData = await _futureDateRepository.FirstOrDefaultAsync(t => t.Id == input.FutureDataId);
                if (!string.IsNullOrEmpty(futureData.ObjectContents))
                {
                    var rsAlreadyExists = JsonConvert.DeserializeObject<GetDinePlanTaxForEditOutput>(futureData.ObjectContents);
                    dinePlanTaxLocationDtos = ObjectMapper.Map<Collection<DinePlanTaxLocationEditDto>>(rsAlreadyExists.DinePlanTax.DinePlanTaxLocations);
                    if (dinePlanTaxLocationDtos.Count > 0)
                    {
                        foreach (var lst in dinePlanTaxLocationDtos)
                        {
                            if (lst.FutureDataSno == input.DinePlanTaxLocation.FutureDataSno)
                            {
                                dinePlanTaxLocationDtos.Remove(lst);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                var iQPv = _dinePlanTaxLocationRepository.GetAll().Where(t => t.DinePlanTaxRefId == input.DinePlanTaxLocation.DinePlanTaxRefId);
                if (input.DinePlanTaxLocation.Id.HasValue)
                    iQPv = iQPv.Where(t => t.Id != input.DinePlanTaxLocation.Id);
                var otherProgramValues = await iQPv.ToListAsync();
                dinePlanTaxLocationDtos = ObjectMapper.Map<Collection<DinePlanTaxLocationEditDto>>(otherProgramValues);
            }
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in dinePlanTaxLocationDtos)
            {
                GetLocationsForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);
            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task<List<ApiTaxOutput>> ApiGetTaxes(ApiLocationInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var output = new List<ApiTaxOutput>();
            if (input.TenantId == 0)
            {
                var location = await _locationRepository.GetAsync(input.LocationId);
                if (location != null)
                {
                    input.TenantId = location.TenantId;
                }
            }

            var taxLocations = await _dinePlanTaxLocationRepository.GetAllListAsync(a => a.TenantId == input.TenantId);

            var myTaxLocations = new List<DinePlanTaxLocation>();

            if (input.LocationId != 0)
            {
                foreach (var taxLocation in taxLocations)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = taxLocation.Locations,
                        Group = taxLocation.Group,
                        NonLocations = taxLocation.NonLocations,
                        LocationTag = taxLocation.LocationTag
                    }))
                    {
                        myTaxLocations.Add(taxLocation);
                    }
                }
            }
            else
            {
                myTaxLocations = taxLocations.ToList();
            }

            foreach (var myTax in myTaxLocations)
            {
                var dinePlantax = _dinePlanTaxRepository.Get(myTax.DinePlanTaxRefId);
                TransactionType tType = null;
                if (dinePlantax.TransactionTypeId != null)
                {
                    tType = await _transactiontypeRepository.GetAsync(dinePlantax.TransactionTypeId.Value);
                }
                if (tType == null)
                    continue;

                foreach (var mapping in _dinePlanTaxMappingRepository.GetAll().Where(a => a.DinePlanTaxLocationId == myTax.Id))
                {
                    var detail = new ApiTaxOutput
                    {
                        Id = myTax.Id,
                        Name = dinePlantax.TaxName,
                        Percentage = myTax.TaxPercentage,
                        TransactionTypeId = tType.Id,
                        TransactionTypeName = tType.Name
                    };
                    MenuItem mItem = null;
                    Category cate = null;
                    Department dept = null;

                    if (mapping.MenuItemId != null)
                    {
                        mItem = await _menuItemRepository.FirstOrDefaultAsync(t => t.Id == mapping.MenuItemId);
                    }
                    if (mapping.CategoryId != null)
                    {
                        cate = await _categoryRepository.FirstOrDefaultAsync(t => t.Id == mapping.CategoryId);
                    }
                    if (mapping.DepartmentId != null)
                    {
                        dept = await _departmentRepository.FirstOrDefaultAsync(t => t.Id == mapping.DepartmentId);
                    }

                    if (mItem != null)
                    {
                        detail.MenuItemId = mItem.Id;
                        detail.MenuItemName = mItem.Name;
                    }
                    if (cate != null)
                    {
                        detail.CategoryId = cate.Id;
                        detail.CategoryName = cate.Name;
                    }
                    if (dept != null)
                    {
                        detail.DepartmentId = dept.Id;
                        detail.DepartmentName = dept.Name;
                    }
                    output.Add(detail);
                }
            }
            return output;
        }
    }
}
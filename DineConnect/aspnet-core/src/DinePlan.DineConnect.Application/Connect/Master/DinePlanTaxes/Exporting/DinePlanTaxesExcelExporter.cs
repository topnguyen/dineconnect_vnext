﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Exporting
{
    public class DinePlanTaxesExcelExporter : NpoiExcelExporterBase, IDinePlanTaxesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DinePlanTaxesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDinePlanTaxForViewDto> dinePlanTaxes)
        {
            return CreateExcelPackage(
                "DinePlanTaxes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DinePlanTaxes"));

                    AddHeader(
                        sheet,
                        L("TaxName")
                        );

                    AddObjects(
                        sheet, 2, dinePlanTaxes,
                        _ => _.DinePlanTax.TaxName
                        );

					

                });
        }
    }
}

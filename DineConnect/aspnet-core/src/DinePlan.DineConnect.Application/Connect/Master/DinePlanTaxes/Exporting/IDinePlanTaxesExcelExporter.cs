﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Exporting
{
    public interface IDinePlanTaxesExcelExporter
    {
        FileDto ExportToFile(List<GetDinePlanTaxForViewDto> dinePlanTaxes);
    }
}
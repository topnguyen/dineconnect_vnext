﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Connect.Master.OrderTags.Exporting;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Session;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Master.OrderTags
{
    
    public class OrderTagGroupAppService : DineConnectReportAppServiceBase, IOrderTagGroupAppService
    {
        private readonly IRepository<Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<PaymentTypes.PaymentType> _paymentTypeRepo;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<OrderTagMap> _orderMapManager;
        private readonly IRepository<OrderTagGroup> _orderTagGroupRepo;
        private readonly IRepository<OrderTag> _orderTagManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MenuItem> _menuItemRepo;
        private readonly ConnectSession _connectSession;
        private readonly IOrderTagGroupExcelExporter _excelExporter;
        private readonly IOrderTagAndExchangeExcelExporter _excelOrderTagExchangeExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<OrderTagLocationPrice> _tagLocationPriceRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly ILogger _logger;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public OrderTagGroupAppService(
            ILocationAppService locService,
            IRepository<Department> drepo,
            IRepository<Location> lRepo,
            IRepository<PaymentTypes.PaymentType> paymentTypeRepo,
            IRepository<OrderTagGroup> orderTagGroupRepo,
            IRepository<OrderTag> orderTag,
            IRepository<OrderTagMap> orderTagMap, IRepository<Category> categoryRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MenuItem> menuItemRepo,
            ILogger logger,
            ITenantSettingsAppService tenantSettingsService,
            IOrderTagAndExchangeExcelExporter excelOrderTagExchangeExporter,
            IRepository<Transaction.Ticket> ticketManager,
            ConnectSession connectSession,
            IOrderTagGroupExcelExporter excelExporter,
            ILocationAppService locationAppService,
            IRepository<OrderTagLocationPrice> tagLocationPriceRepository,
            IRepository<Location> locationRepository) : base(locService, ticketManager, unitOfWorkManager, drepo)
        {
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _paymentTypeRepo = paymentTypeRepo;
            _ticketManager = ticketManager;
            _orderTagGroupRepo = orderTagGroupRepo;
            _orderMapManager = orderTagMap;
            _orderTagManager = orderTag;
            _tenantSettingsService = tenantSettingsService;
            _categoryRepository = categoryRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _menuItemRepo = menuItemRepo;
            _connectSession = connectSession;
            _excelExporter = excelExporter;
            _locationAppService = locationAppService;
            _excelOrderTagExchangeExporter = excelOrderTagExchangeExporter;
            _tagLocationPriceRepository = tagLocationPriceRepository;
            _locationRepository = locationRepository;
            _logger = logger;
        }
        public async Task<PagedResultDto<OrderTagGroupListDto>> GetAll(GetOrderTagGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<OrderTagGroup> allItems =
                    _orderTagGroupRepo
                        .GetAll()
                        .Include(x => x.Tags)
                        .Include(x => x.Maps)
                        .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !string.IsNullOrWhiteSpace(input.Filter),
                            p => p.Id.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    allItems = FilterByLocations(allItems, input);
                }

                var allItemCount = allItems.Count();

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = ObjectMapper.Map<List<OrderTagGroupListDto>>(sortMenuItems);

                return new PagedResultDto<OrderTagGroupListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<FileDto> GetToExcel(GetOrderTagGroupInput inputDto)
        {
            var paged = await GetAll(inputDto);

            return await _excelExporter.ExportToFile(paged.Items.ToList());
        }

        
        public async Task<GetOrderTagGroupForEditOutput> GetTagForEdit(NullableIdDto input)
        {
            var output = new GetOrderTagGroupForEditOutput();

            OrderTagGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto =
                    await _orderTagGroupRepo
                        .GetAll()
                        .Where(x => x.Id == input.Id.Value)
                        .Include(x => x.Tags)
                        .Include(x => x.Maps)
                        .FirstOrDefaultAsync();

                editDto = ObjectMapper.Map<OrderTagGroupEditDto>(hDto);

                var rscategory = await _categoryRepository.GetAllListAsync();

                var rsmenuItem = await _menuItemRepo.GetAllListAsync();

                if (editDto.Tags != null && editDto.Tags.Count > 0)
                {
                    foreach (var lst in editDto.Tags)
                    {
                        if (lst.MenuItemId != null)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                    }
                }

                if (editDto.Maps != null && editDto.Maps.Count > 0)
                {
                    foreach (var lst in editDto.Maps)
                    {
                        if (lst.CategoryId != null)
                        {
                            var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                            if (category != null)
                            {
                                lst.CategoryName = category.Name;
                            }
                        }
                        if (lst.MenuItemId != null)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                    }
                }

                UpdateLocations(hDto, output.LocationGroup);
            }
            else
            {
                editDto = new OrderTagGroupEditDto();
            }

            output.OrderTagGroup = editDto;

            if (!string.IsNullOrEmpty(editDto.Departments))
            {
                output.Departments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
            }

            return output;
        }

        public async Task CreateOrUpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            if (input?.LocationGroup == null)
            {
                return;
            }

            if (input.OrderTagGroup.Id.HasValue)
            {
                await UpdateOrderTagGroup(input);
            }
            else
            {
                await CreateOrderTagGroup(input);
            }
        }

        
        public async Task DeleteOrderTagGroup(EntityDto input)
        {
            await _orderTagGroupRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<OrderTagGroupListDto>> GetIds()
        {
            var lstOrderTagGroup = await _orderTagGroupRepo.GetAll().OrderBy(x => x.Name).ToListAsync();

            var result = ObjectMapper.Map<List<OrderTagGroupListDto>>(lstOrderTagGroup);

            return new ListResultDto<OrderTagGroupListDto>(result);
        }

        public async Task<ListResultDto<OrderTagGroupEditDto>> GetTagForCategoryAndItem(OrderTagInput input)
        {
            var allItems = await (from mas in _orderTagGroupRepo.GetAll()
                                  where mas.Maps.Any(a => a.CategoryId == input.CategoryId && a.MenuItemId == input.MenuItemId)
                                  select mas).ToListAsync();

            var allListDtos = ObjectMapper.Map<List<OrderTagGroupEditDto>>(allItems);

            return new ListResultDto<OrderTagGroupEditDto>(allListDtos);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _orderTagGroupRepo.GetAsync(input.Id);

                item.IsDeleted = false;

                await _orderTagGroupRepo.UpdateAsync(item);
            }
        }
        [HttpPost]
        public async Task<OrderTagReportOutput> BuildOrderTagReport(GetOrderTagReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = GetAllTickets(cInput);
            try
            {
                var ordersTags = tickets.SelectMany(x => x.Orders.SelectMany(o => o.TransactionOrderTags))
                    .WhereIf(input.OrderTagGroupIds != null && input.OrderTagGroupIds.Any(),
                        x => input.OrderTagGroupIds.Contains(x.OrderTagGroupId));
                var order = tickets.SelectMany(x => x.Orders);
                var allTags = await ordersTags.OrderBy(input.Sorting).ToListAsync();

                var dtos = new List<OrderTagReportDto>();
                var orderTags = new List<OrderTagReportDto>();
                foreach (var tag in allTags)
                {
                    var findOrder = order.FirstOrDefault(s => s.Id == tag.OrderId);
                    var findTicket = tickets.FirstOrDefault(s => s.Id == findOrder.TicketId);
                    var findLoc = _locationRepository.FirstOrDefault(s => s.Id == findTicket.LocationId);
                    if (!orderTags.Any())
                    {
                        orderTags.Add(new OrderTagReportDto
                        {
                            TicketId = findOrder.TicketId,
                            TicketNo = findTicket?.TicketNumber,
                            LocationCode = findLoc.Code,
                            OrderId = findOrder.OrderId,
                            OrderTagGroup = tag.TagName,
                            OrderTagName = tag.TagValue,
                            Quantity = tag.Quantity,
                            Price = tag.Price,
                            Total = tag.Price * tag.Quantity,
                            User = findTicket?.LastModifiedUserName
                        });
                        continue;
                    }

                    var existTag = orderTags.FirstOrDefault(x =>
                        x.OrderId == tag.Order.OrderId && x.OrderTagGroup == tag.TagName &&
                        x.OrderTagName == tag.TagValue && x.Price == tag.Price);
                    if (existTag != null)
                    {
                        existTag.Quantity += tag.Quantity;
                    }
                    else
                    {
                        dtos.AddRange(orderTags);
                        orderTags = new List<OrderTagReportDto>
                        {
                            new OrderTagReportDto
                            {
                                TicketId = findOrder.TicketId,
                                TicketNo = findTicket?.TicketNumber,
                                LocationCode = findLoc.Code,
                                OrderId = findOrder.OrderId,
                                OrderTagGroup = tag.TagName,
                                OrderTagName = tag.TagValue,
                                Quantity = tag.Quantity,
                                Price = tag.Price,
                                Total = tag.Price * tag.Quantity,
                                User =findTicket?.LastModifiedUserName
                            }
                        };
                    }
                }
                dtos.AddRange(orderTags);

                // filter by query builder
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var total = dataAsQueryable.Count();
                var pageBy = dataAsQueryable.PageBy(input).ToList();

                return new OrderTagReportOutput
                {
                    Data = new PagedResultDto<OrderTagReportDto>(total, pageBy),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = ordersTags.Sum(x => x.Price * x.Quantity),
                        TotalOrderCount = ordersTags.Select(x => x.Order.Id).Distinct().Count(),
                        TotalItemSold = ordersTags.Count()
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetOrderTagReport", ex);
            }

            return new OrderTagReportOutput
            {
                Data = new PagedResultDto<OrderTagReportDto>(0, new List<OrderTagReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }
        public async Task<FileDto> BuildOrderTagsExcel(GetOrderTagReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
                input.MaxResultCount = 10000;
            return await _excelOrderTagExchangeExporter.ExportOrderTag(input, this);
        }
        public async Task<OrderExchangeReportOutput> BuildOrderExchangeReport(GetOrderExchangeReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = GetAllTickets(cInput);
            try
            {
                var exchangeOrders = tickets.SelectMany(x => x.Orders).Where(x => x.OrderStates.Contains("Exchange"));
                var filterOrders = await exchangeOrders.OrderBy(input.Sorting).ToListAsync();
                var dtos = new List<OrderExchangeReportDto>();
                foreach (var order in filterOrders)
                {
                    var findTicket = tickets.FirstOrDefault(s => s.Id == order.TicketId);
                    var findLoc = _locationRepository.FirstOrDefault(s => s.Id == findTicket.LocationId);
                    dtos.Add(new OrderExchangeReportDto
                    {
                        TicketId = order.TicketId,
                        TicketNo = findTicket?.TicketNumber,
                        LocationCode = findLoc?.Code,
                        OrderId = order.OrderId,
                        MenuItemName = order.MenuItemName,
                        OrderExchange = order.OrderLog?.Replace("\r\n", ", "),
                        OrderExchange2 = order.OrderLog,
                        Quantity = order.Quantity,
                        Price = order.Price,
                        Total = order.Price * order.Quantity,
                        User = findTicket?.LastModifiedUserName
                    });
                }

                // filter by query builder
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var total = dataAsQueryable.Count();
                var result = dataAsQueryable.PageBy(input).ToList();

                return new OrderExchangeReportOutput
                {
                    Data = new PagedResultDto<OrderExchangeReportDto>(total, result),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = exchangeOrders.Sum(x => x.Price * x.Quantity),
                        TotalOrderCount = exchangeOrders.Select(x => x.Id).Distinct().Count(),
                        TotalItemSold = exchangeOrders.Count()
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetOrderExchangeReport", ex);
            }

            return new OrderExchangeReportOutput
            {
                Data = new PagedResultDto<OrderExchangeReportDto>(0, new List<OrderExchangeReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }
        [HttpPost]
        public async Task<FileDto> BuildOrderExchangesExcel(GetOrderExchangeReportInput input)
        {
            return await _excelOrderTagExchangeExporter.ExportOrderExchange(input, this);
        }
        [HttpPost]
        public async Task<ReturnProductReportOutput> BuildReturnProductReport(GetOrderExchangeReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = GetAllTickets(cInput).Where(x => x.TotalAmount > 0);
            var allPaymentType = _paymentTypeRepo.GetAll();
            try
            {
                var returnProduceOrders = tickets.SelectMany(x => x.Orders);
                var filterOrders = new  List<Order>();
                if (input.IsReport)
                {
                    filterOrders = await returnProduceOrders.OrderBy(input.Sorting).ToListAsync();
                }
                else
                {
                    filterOrders = await returnProduceOrders.OrderBy(input.Sorting).PageBy(input).ToListAsync();
                }
                 
                var dtos = new List<ReturnProductReportDto>();
                var totalAmount = 0M;
                var totalOrderCount = 0;
                
                foreach (var order in filterOrders)
                {
                    var ticket = tickets.FirstOrDefault(a => a.Id.Equals(order.TicketId));
                    if (ticket == null) continue;
                    var menu = new MenuItem();
                    menu = _menuItemRepo.GetAll()?.FirstOrDefault(x => x.Id == order.MenuItemId);
                    if (menu == null) continue;

                    var ticketReference = new Transaction.Ticket();

                    if (!String.IsNullOrWhiteSpace(ticket.ReferenceNumber))
                        ticketReference = tickets.FirstOrDefault(t => t.ReferenceNumber == ticket.ReferenceNumber);
                    else
                        continue;

                    if (ticketReference == null)
                        continue;
                    if (ticketReference.Payments == null)
                        continue;
                    var payment = ticketReference.Payments.FirstOrDefault(x => x.TicketId == ticketReference.Id);
                    if (payment == null)
                        continue;

                    var ticketEntities = JsonConvert.DeserializeObject<IList<TicketEntityMap>>(ticketReference.TicketEntities);
                    var entity = ticketEntities.SingleOrDefault(a => a.EntityTypeId.Equals(1));

                    var ticketStateValue = ticketReference.GetTicketStateValues().FirstOrDefault();
                    var returnProduct = JsonHelper.Deserialize<ReturnProductReport>(ticketStateValue.StateValue);
                    var findPaymentType = allPaymentType.FirstOrDefault(s => s.Id == payment.PaymentTypeId);
                    string paymentName = findPaymentType != null ? findPaymentType.Name : "";

                    var setting = await _tenantSettingsService.GetAllSettings();
                    var dateTimeFormat = setting.FileDateTimeFormat.FileDateTimeFormat;
                    decimal amount = 0;
                    if (order != null)
                        amount = order.Price * Math.Abs(order.Quantity);
                    totalAmount += amount;
                    totalOrderCount++;

                    // get location 
                    var location = await _lRepo.FirstOrDefaultAsync(ticket.LocationId);

                    dtos.Add(new ReturnProductReportDto
                    {
                        DateTimeString = ticketReference.LastPaymentTime.ToString(dateTimeFormat),
                        Cashier = payment.PaymentUserName,
                        TicketNumber = ticket.TicketNumber,
                        CustomerName = entity != null ? entity.EntityName : "",
                        MenuCode = menu?.AliasCode,// order.MenuItemId.ToString(),
                        MenuName = order.MenuItemName,
                        PaymentType = paymentName,
                        Quantity = Math.Abs(order.Quantity),
                        RefundType = returnProduct?.RefundType == ReturnProductReportConsts.Payment ? paymentName : returnProduct?.RefundType,
                        ReturnProductDateString = ticketStateValue.LastUpdateTime.ToString(dateTimeFormat),
                        ReturnProductBy = returnProduct?.ReturnProductBy,
                        Reason = order?.Note,
                        Detail = "",
                        Amount = amount,
                        LocationCode = location?.Code,
                        LocationName = location?.Name,
                    });
                }

                // filter query builder 
                var dataAsQueryable = dtos.AsQueryable();

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                var total = dataAsQueryable.Count();
                var result = dataAsQueryable.ToList();

                var totalAmountOrder = result.Sum(x => x.Amount);

                return new ReturnProductReportOutput
                {
                    Data = new PagedResultDto<ReturnProductReportDto>(total, result),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = totalAmountOrder,
                        TotalOrderCount = total,
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetReturnProductReport", ex);
            }

            return new ReturnProductReportOutput
            {
                Data = new PagedResultDto<ReturnProductReportDto>(0, new List<ReturnProductReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }
        [HttpPost]
        public async Task<FileDto> BuildReturnProductExcel(GetOrderExchangeReportInput input)
        {
            return await _excelOrderTagExchangeExporter.ExportReturnProduct(input, this);
        }
        
        protected virtual async Task UpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            var dto = input.OrderTagGroup;

            var item = await
                _orderTagGroupRepo.GetAll()
                    .Include(x => x.Tags)
                    .Include(x => x.Maps)
                .FirstOrDefaultAsync(x => x.Id == input.OrderTagGroup.Id.Value);

            item.Name = dto.Name;
            item.Prefix = dto.Prefix;
            item.MaxSelectedItems = dto.MaxSelectedItems;
            item.MinSelectedItems = dto.MinSelectedItems;
            item.SortOrder = dto.SortOrder;
            item.AddTagPriceToOrderPrice = dto.AddTagPriceToOrderPrice;
            item.SaveFreeTags = dto.SaveFreeTags;
            item.FreeTagging = dto.FreeTagging;
            item.TaxFree = dto.TaxFree;
            item.Departments = JsonConvert.SerializeObject(input.Departments);

            UpdateLocations(item, input.LocationGroup);

            if (dto?.Tags != null && dto.Tags.Any())
            {
                var oids = new List<int>();

                foreach (var otdto in dto.Tags)
                {
                    if (otdto.Id == null)
                    {
                        item.Tags.Add(ObjectMapper.Map<OrderTag>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);

                        var fromUpdation = item.Tags.FirstOrDefault(a => a.Id.Equals(otdto.Id));

                        if (fromUpdation != null)
                        {
                            UpdateTag(fromUpdation, otdto);
                        }
                    }
                }

                var tobeRemoved = item.Tags.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();

                foreach (var tagItem in tobeRemoved) await _orderTagManager.DeleteAsync(tagItem.Id);
            }

            if (dto?.Maps != null && dto.Maps.Any())
            {
                var oids = new List<int>();

                foreach (var otdto in dto.Maps)
                {
                    if (otdto.Id == null)
                    {
                        item.Maps.Add(ObjectMapper.Map<OrderTagMap>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);

                        var fromUpdation = item.Maps.SingleOrDefault(a => a.Id.Equals(otdto.Id));

                        UpdateMap(fromUpdation, otdto);
                    }
                }

                var tobeRemoved = item.Maps.Where(a => a.Id != null && !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();

                foreach (var mapItem in tobeRemoved)
                {
                    await _orderMapManager.DeleteAsync(mapItem.Id);
                }
            }
            else
            {
                var allList = _orderMapManager.GetAll().Where(a => a.OrderTagGroupId == dto.Id).ToList();

                foreach (var orderMap in allList)
                {
                    await _orderMapManager.DeleteAsync(orderMap.Id);
                }
            }

            CheckErrors(await CreateOrderTagSync(item));
        }

        private void UpdateMap(OrderTagMap fromUpdation, OrderMapDto otdto)
        {
            fromUpdation.CategoryId = otdto.CategoryId;

            fromUpdation.MenuItemId = otdto.MenuItemId;
        }

        private void UpdateTag(OrderTag fromUpdation, OrderTagDto otdto)
        {
            fromUpdation.Name = otdto.Name;
            fromUpdation.AlternateName = otdto.AlternateName;
            fromUpdation.MenuItemId = otdto.MenuItemId;
            fromUpdation.MaxQuantity = otdto.MaxQuantity;
            fromUpdation.Price = otdto.Price;
            fromUpdation.SortOrder = otdto.SortOrder;
        }

        
        protected virtual async Task CreateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            var dto = ObjectMapper.Map<OrderTagGroup>(input.OrderTagGroup);

            if (input.Departments != null && input.Departments.Any())
            {
                dto.Departments = JsonConvert.SerializeObject(input.Departments);
            }

            UpdateLocations(dto, input.LocationGroup);

            CheckErrors(await CreateOrderTagSync(dto));
        }

        public async Task<IdentityResult> CreateOrderTagSync(OrderTagGroup orderTagGroup)
        {
            if (orderTagGroup.Id == 0)
            {
                if (_orderTagGroupRepo.GetAll().Any(a => a.Name.Equals(orderTagGroup.Name)))
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }

                orderTagGroup.OrganizationId = _connectSession.OrgId;

                await _orderTagGroupRepo.InsertAndGetIdAsync(orderTagGroup);

                return IdentityResult.Success;
            }
            else
            {
                List<OrderTagGroup> lst = _orderTagGroupRepo.GetAll().Where(a => a.Name.Equals(orderTagGroup.Name) && a.Id != orderTagGroup.Id).ToList();

                if (lst.Count > 0)
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                return IdentityResult.Success;
            }
        }

        public async Task<string> GetFilesOrderTagGroupById(int id)
        {
            var entity = await _orderTagGroupRepo.FirstOrDefaultAsync(id);
            return entity?.Files;
        }

        public async Task UpdateFilesOrderTagGroup(int id, string files)
        {
            var entity = await _orderTagGroupRepo.FirstOrDefaultAsync(id);

            entity.Files = files;
            await _orderTagGroupRepo.UpdateAsync(entity);
        }

        public async Task<string> GetFilesOrderTagById(int id)
        {
            var entity = await _orderTagManager.FirstOrDefaultAsync(id);
            return entity?.Files;
        }

        public async Task UpdateFilesOrderTag(int id, string files)
        {
            var entity = await _orderTagManager.FirstOrDefaultAsync(id);

            entity.Files = files;
            await _orderTagManager.UpdateAsync(entity);
        }

        public async Task<ListResultDto<OrderTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput)
        {
            var returnDto = new List<OrderTagGroupEditDto>();
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var allItems = _orderTagGroupRepo.GetAll()
                .Include(a => a.Tags).Include(a => a.Maps).Where(a => a.TenantId == locationInput.TenantId);

            var allListOutputs = ObjectMapper.Map<List<OrderTagGroupEditDto>>(allItems);

            foreach (var map in allListOutputs.SelectMany(a => a.Maps))
                if (map.CategoryId != null)
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        map.CategoryName = _categoryRepository.Get(map.CategoryId.Value).Name;
                    }

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var orderTagList in allListOutputs)
                        if (!orderTagList.Group && !string.IsNullOrEmpty(orderTagList.Locations) &&
                            !orderTagList.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(orderTagList.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        returnDto.Add(orderTagList);
                        }
                        else
                        {
                            returnDto.Add(orderTagList);
                        }
            }
            else if (locationInput.LocationId > 0)
            {
                foreach (var myDto in allListOutputs)
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = myDto.Locations,
                        Group = myDto.Group,
                        LocationTag = myDto.LocationTag,
                        NonLocations = myDto.NonLocations
                    }))
                        returnDto.Add(myDto);
            }
            else
            {
                returnDto = allListOutputs;
            }

            var myLocationTags = _tagLocationPriceRepository.GetAll()
                .Where(a => a.LocationId == locationInput.LocationId);
            var myTagPriceCount = myLocationTags.Count(a => a.LocationId == locationInput.LocationId);
            if (myTagPriceCount > 0)
            {
                foreach (var orderTagDto in allListOutputs.SelectMany(a => a.Tags))
                {
                    var myTag = myLocationTags.Where(a => a.OrderTagId == orderTagDto.Id).ToList();
                    if (myTag.Any())
                    {
                        orderTagDto.Price = myTag.Last().Price;
                    }
                }
            }

            return new ListResultDto<OrderTagGroupEditDto>(returnDto);
        }

        public async Task<PagedResultDto<OrderTagLocationPriceListDto>> GetAllOrderTagLocationPrice(GetOrderTagLocationPriceInput input)
        {
            var allItems = _tagLocationPriceRepository.GetAllIncluding(t => t.Location)
                .Where(t => t.OrderTagId == input.OrderTagId)
                .Select(t => new OrderTagLocationPriceListDto
                {
                    Id = t.Id,
                    OrderTagId = t.OrderTagId,
                    LocationId = t.LocationId,
                    LocationName = t.Location.Name,
                    CreationTime = t.CreationTime,
                    CreatorUserId = t.CreatorUserId,
                    Price = t.Price
                });

            var allItemCount = await allItems.CountAsync();

            var sortMenuItems = await allItems.OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

            return new PagedResultDto<OrderTagLocationPriceListDto>(
                allItemCount,
                sortMenuItems
            );
        }

        public async Task<GetOrderTagLocationPriceForEditOutput> GetOrderTagLocationPriceForEdit(NullableIdDto input)
        {
            var output = new GetOrderTagLocationPriceForEditOutput();
            OrderTagLocationPriceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _tagLocationPriceRepository.GetAllListAsync(t => t.OrderTagId == input.Id.Value);
                editDto = ObjectMapper.Map<OrderTagLocationPriceEditDto>(hDto);
            }
            else
            {
                editDto = new OrderTagLocationPriceEditDto();
            }
            output.OrderTagLocationPrice = editDto;
            return output;
        }

        public async Task CreateOrderTagLocationPrice(OrderTagLocationPriceEditDto input)
        {
            if (input.Id.HasValue)
            {
                var existEntry = await _tagLocationPriceRepository.FirstOrDefaultAsync(t => t.Id == input.Id.Value);
                ObjectMapper.Map(input, existEntry);
                await _tagLocationPriceRepository.UpdateAsync(existEntry);
            }
            else
            {
                var entity = ObjectMapper.Map<OrderTagLocationPrice>(input);
                await _tagLocationPriceRepository.InsertAndGetIdAsync(entity);
            }
        }

        public async Task DeleteOrderTagLocationPrice(EntityDto input)
        {
            await _tagLocationPriceRepository.DeleteAsync(input.Id);
        }

        public async Task<bool> CheckExistOrderTagLocationPrice(OrderTagLocationPriceEditDto input)
        {
            var existEntry = _tagLocationPriceRepository.GetAll()
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id)
                .Where(t => t.LocationId == input.LocationId && t.OrderTagId == input.OrderTagId);

            return await existEntry.AnyAsync();
        }

        public async Task<ListResultDto<OrderTagGroupEditDto>> GetOrderTagGroupNames()
        {
            var lstOrderTagGroup = await _orderTagGroupRepo.GetAll().ToListAsync();
            return new ListResultDto<OrderTagGroupEditDto>(ObjectMapper.Map<List<OrderTagGroupEditDto>>(lstOrderTagGroup));
        }
        public async Task<ListResultDto<OrderTagDto>> GetOrderTagNames()
        {
            var lstOrderTag = await _orderTagManager.GetAll().ToListAsync();
            return new ListResultDto<OrderTagDto>(ObjectMapper.Map<List<OrderTagDto>>(lstOrderTag));
        }
    }
}
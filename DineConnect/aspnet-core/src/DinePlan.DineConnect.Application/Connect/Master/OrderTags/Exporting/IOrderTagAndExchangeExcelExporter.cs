﻿using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.OrderTags.Exporting
{
    public interface IOrderTagAndExchangeExcelExporter
    {
        Task<FileDto> ExportOrderTag(GetOrderTagReportInput input, IOrderTagGroupAppService connectReportService);
        Task<FileDto> ExportOrderExchange(GetOrderExchangeReportInput input, IOrderTagGroupAppService connectReportService);
        Task<FileDto> ExportReturnProduct(GetOrderExchangeReportInput input, IOrderTagGroupAppService connectReportService);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetZeroCore.Net;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Master.OrderTags.Exporting
{
    public class OrderTagGroupExcelExporter : NpoiExcelExporterBase, IOrderTagGroupExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<MenuItem> _menuitemRepo;
        public OrderTagGroupExcelExporter(
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService,
            IRepository<MenuItem> menuitemRepo) :
            base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _menuitemRepo = menuitemRepo;
        }     
        public async Task<FileDto> ExportToFile(List<OrderTagGroupListDto> dtos)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            var allMenuItem = _menuitemRepo.GetAll();
            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "OrderTagGroups.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("OrderTagGroups"));
                    var listExport = new List<ImportOrderTagGroupDto>();
                    foreach (var group in dtos)
                    {
                        var allMap = group.Maps;
                        var allTag = group.Tags;
                        if (!allMap.Any() && !allTag.Any())
                        {
                            listExport.Add(new ImportOrderTagGroupDto
                            {
                                Id = group?.Id,
                                TagGroupName = group.Name,
                                Departments = group.Departments,
                                MinimumEx = group?.MinSelectedItems,
                                MaximumEx = group?.MaxSelectedItems,
                                FreeTagging = group.FreeTagging,
                                SaveFreeTags = group.SaveFreeTags,
                                AddTagPriceToOrder = group.AddTagPriceToOrderPrice,
                                TaxFree = group.TaxFree
                            });
                        }
                        var listExportMaps = new List<ImportOrderTagGroupDto>();
                        var idMap = 0;
                        foreach (var map in group.Maps)
                        {
                            listExportMaps.Add(new ImportOrderTagGroupDto
                            {
                                IndexEx = idMap,
                                Category = map.CategoryName,
                                Menu = allMenuItem.FirstOrDefault(s => s.Id == map.MenuItemId)?.Name
                            });
                            idMap++;
                        }
                        var listExportTags = new List<ImportOrderTagGroupDto>();
                        var idTag = 0;
                        foreach (var tag in group.Tags)
                        {

                            listExportTags.Add(new ImportOrderTagGroupDto
                            {
                                IndexEx = idTag,
                                Tag = tag.Name,
                                TagAliasName = tag.AlternateName,
                                TagPrice = tag.Price,
                                TagQuantity = tag.MaxQuantity,
                                MenuItem = allMenuItem.FirstOrDefault(s => s.Id == tag.MenuItemId)?.Name
                            });
                            idTag++;
                        }
                        var listMerge = new List<ImportOrderTagGroupDto>();
                        var depart = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(group.Departments);
                        var count = listExportMaps.Count >= listExportTags.Count ? listExportMaps.Count : listExportTags.Count;
                        for (int i = 0; i < count; i++)
                        {
                            var findMap = listExportMaps.Find(s => s.IndexEx == i);
                            var findTag = listExportTags.Find(s => s.IndexEx == i);

                            listMerge.Add(new ImportOrderTagGroupDto
                            {
                                Id = group?.Id,
                                TagGroupName = group.Name,
                                Departments = depart != null ? (depart.FirstOrDefault()?.DisplayText ?? "*") : "*",
                                MinimumEx = group?.MinSelectedItems,
                                MaximumEx = group?.MaxSelectedItems,
                                FreeTagging = group.FreeTagging,
                                SaveFreeTags = group.SaveFreeTags,
                                AddTagPriceToOrder = group.AddTagPriceToOrderPrice,
                                TaxFree = group.TaxFree,
                                Tag = findTag != null ? findTag.Tag : "",
                                TagAliasName = findTag != null ? findTag.TagAliasName : "",
                                TagPrice = findTag != null ? findTag.TagPrice : 0,
                                TagQuantity = findTag != null ? findTag.TagQuantity : 0,
                                MenuItem = findTag != null ? findTag.MenuItem : "",
                                Category = findMap != null ? findMap.Category : "",
                                Menu = findMap != null ? findMap.Menu : "",
                            });
                        }
                        listExport.AddRange(listMerge);
                    }
                    listExport = listExport.OrderBy(x => x.Id).ToList();
                    AddHeader(
                        sheet,
                        "Id",
                        "Tag Group Name",
                        "Departments",
                        "Minimum",
                        "Maximum",
                        "Free Tagging",
                        "Save Free Tags",
                        "Add Tag Price To Order",
                        "Tax Free",
                        "Tag",
                        "Tag Alias Name",
                        "Menu Item",
                        "Tag Quantity",
                        "Tag Price",
                        "Category",
                        "Menu"
                        );
                    AddObjects(
                        sheet,
                        2,
                        listExport,
                        _ => _.Id,
                        _ => _.TagGroupName,
                        _ => _.Departments,
                        _ => _.MinimumEx,
                        _ => _.MaximumEx,
                        _ => _.FreeTagging == false ? "0" : "1",
                        _ => _.SaveFreeTags == false ? "0" : "1",
                        _ => _.AddTagPriceToOrder == false ? "0" : "1",
                        _ => _.TaxFree == false ? "0" : "1",
                        _ => _.Tag,
                        _ => _.TagAliasName,
                        _ => _.MenuItem,
                        _ => _.TagQuantity,
                        _ => _.TagPrice,
                        _ => _.Category,
                        _ => _.Menu
                    );

                    //Formatting cells

                    for (var i = 1; i <= 16; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
    public class OrderTagAndExchangeExcelExporter : EpPlusExcelExporterBase, IOrderTagAndExchangeExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Ticket> _ticketRepo;
        public OrderTagAndExchangeExcelExporter(ITempFileCacheManager tempFileCacheManager, IRepository<Ticket> ticketRepo, ILocationAppService locService, IUnitOfWorkManager unitOfWorkManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
            _ticketRepo = ticketRepo;
        }
        public async Task<FileDto> ExportOrderExchange(GetOrderExchangeReportInput input, IOrderTagGroupAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            var file = new FileDto($"Order_Exchange_Report_{start}-{end}.{(input.ExportOutputType == ExportType.Excel ? "xlsx" : "html")}", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Order Exchange Report_{start}-{end}");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("TicketNo"),
                        L("LocationCode"),
                        L("OrderId"),
                        L("MenuItemName"),
                        L("OrderExchange"),
                        L("Quantity"),
                        L("Price"),
                        L("Total"),
                        L("User"));

                var cInput = new GetChartInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup
                };
                var tickets = GetAllOrderTagTickets(cInput).SelectMany(x => x.Orders).Where(x => x.OrderStates.Contains("Exchange"));
                var dtos = tickets.Select(order => new OrderExchangeReportDto
                {
                    TicketId = order.TicketId,
                    TicketNo = order.Ticket.TicketNumber,
                    LocationCode = order.Ticket.Location.Code,
                    OrderId = order.OrderId,
                    MenuItemName = order.MenuItemName,
                    OrderExchange = order.OrderLog.Replace("\r\n", ", "),
                    Quantity = order.Quantity,
                    Price = order.Price,
                    Total = order.Price * order.Quantity,
                    User = order.Ticket.LastModifiedUserName
                }).ToList();

                AddObjects(sheet, 2, dtos,
                            _ => _.TicketNo,
                            _ => _.LocationCode,
                            _ => _.OrderId,
                            _ => _.MenuItemName,
                            _ => _.OrderExchange,
                            _ => _.Quantity,
                            _ => _.Price,
                            _ => _.Total,
                            _ => _.User
                            );

                //Formatting cells

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        public async Task<FileDto> ExportOrderTag(GetOrderTagReportInput input, IOrderTagGroupAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            var file = new FileDto($"Order_Tag_Report_{start}-{end}.{(input.ExportOutputType == ExportType.Excel ? "xlsx" : "html")}", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Order Tag Report_{start}-{end}");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("TicketNo"),
                        L("LocationCode"),
                        L("OrderId"),
                        L("OrderTagGroup"),
                        L("OrderTag"),
                        L("Quantity"),
                        L("Price"),
                        L("Total"),
                        L("User"));

                var cInput = new GetChartInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup
                };
                var tickets = GetAllOrderTagTickets(cInput).SelectMany(x => x.Orders.SelectMany(o => o.TransactionOrderTags));
                var dtos = tickets.Select(tag => new OrderTagReportDto
                {
                    TicketId = tag.Order.TicketId,
                    TicketNo = tag.Order.Ticket.TicketNumber,
                    LocationCode = tag.Order.Ticket.Location.Code,
                    OrderId = tag.Order.OrderId,
                    OrderTagGroup = tag.TagName,
                    OrderTagName = tag.TagValue,
                    Quantity = tag.Quantity,
                    Price = tag.Price,
                    Total = tag.Price * tag.Quantity,
                    User = tag.Order.Ticket.LastModifiedUserName
                }).ToList();

                AddObjects(sheet, 2, dtos,
                            _ => _.TicketNo,
                            _ => _.LocationCode,
                            _ => _.OrderId,
                            _ => _.OrderTagGroup,
                            _ => _.OrderTagName,
                            _ => _.Quantity,
                            _ => _.Price,
                            _ => _.Total,
                            _ => _.User
                            );

                //Formatting cells

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
        public IQueryable<Ticket> GetAllOrderTagTickets(IDateInput input)
        {
            IQueryable<Ticket> tickets;

            if (!string.IsNullOrEmpty(input.TicketNo))
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    tickets = _ticketRepo.GetAll()
                        .Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
                    return tickets;
                }

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                tickets = _ticketRepo.GetAllIncluding(a => a.Orders, a => a.Payments, a => a.Transactions);
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate.Date;
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTimeTruc >= mySt
                                    &&
                                    a.LastPaymentTimeTruc <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }
            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            tickets = tickets.Where(a => a.Credit == input.Credit);
            return tickets;
        }
        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;
            var returnStatus = !(input.StartDate.Equals(DateTime.MinValue) && input.EndDate.Equals(DateTime.MinValue));

            return returnStatus;
        }

        public async Task<FileDto> ExportReturnProduct(GetOrderExchangeReportInput input, IOrderTagGroupAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            input.IsReport = true;
            var file = new FileDto($"Return_Product_Report_{start}-{end}.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Return Product Report");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("LocationCode"),
                        L("LocationName"),
                        L("DateTime"),
                        L("Cashier"),
                        L("TicketNumber"),
                        L("CustomerName"),
                        L("MenuCode"),
                        L("MenuName"),
                        L("PaymentType"),
                        L("Quantity"),
                        L("RefundType"),
                        L("ReturnProductDate"),
                        L("ReturnProductBy"),
                        L("Reason"),
                        L("Detail"),
                        L("Amount")
                        );

                var output = await connectReportService.BuildReturnProductReport(input);
                // filter query builder 


                var result = output.Data.Items.ToList();
                AddObjects(sheet, 2, result,
                            _ => _.LocationCode,
                            _ => _.LocationName,
                            _ => _.DateTimeString,
                            _ => _.Cashier,
                            _ => _.TicketNumber,
                            _ => _.CustomerName,
                            _ => _.MenuCode,
                            _ => _.MenuName,
                            _ => _.PaymentType,
                            _ => Math.Abs(_.Quantity),
                            _ => _.RefundType,
                            _ => _.ReturnProductDateString,
                            _ => _.ReturnProductBy,
                            _ => _.Reason,
                            _ => "",
                            _ => _.Amount
                            );

                //Formatting cells

                for (var i = 1; i <= 16; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.OrderTags.Exporting
{
  public interface IOrderTagGroupExcelExporter
    {
        Task<FileDto> ExportToFile(List<OrderTagGroupListDto> list);
    }
}

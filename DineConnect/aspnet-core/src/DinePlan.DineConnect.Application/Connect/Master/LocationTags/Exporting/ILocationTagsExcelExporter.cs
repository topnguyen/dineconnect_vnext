﻿using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Exporting
{
    public interface ILocationTagsExcelExporter
    {
        FileDto ExportToFile(List<LocationTagDto> locationTags);
    }
}
﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Exporting
{
    public class LocationTagsExcelExporter : NpoiExcelExporterBase, ILocationTagsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LocationTagsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<LocationTagDto> locationTags)
        {
            return CreateExcelPackage(
                "LocationTags.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LocationTags"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Code")
                        );

                    AddObjects(
                        sheet, 2, locationTags,
                        _ => _.Name,
                        _ => _.Code
                        );
                });
        }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.LocationTags
{
    public class LocationTagsAppService : DineConnectAppServiceBase, ILocationTagsAppService
    {
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<LocationTag> _locationTagRepository;
        private readonly IRepository<LocationTagLocation> _locationTagLocationRepository;
        private readonly ILocationTagsExcelExporter _locationTagsExcelExporter;

        public LocationTagsAppService(IRepository<LocationTag> locationTagRepository, ILocationTagsExcelExporter locationTagsExcelExporter,
            IRepository<LocationTagLocation> locationTagLocationRepository,
             IRepository<Location> locationRepository)
        {
            _locationTagRepository = locationTagRepository;
            _locationTagsExcelExporter = locationTagsExcelExporter;
            _locationTagLocationRepository = locationTagLocationRepository;
            _locationRepository = locationRepository;
        }

        public async Task<PagedResultDto<LocationTagDto>> GetAllTags(GetAllLocationTagsInput input)
        {
            var filteredLocationTags = _locationTagRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Code, input.Filter.ToILikeString()));

            var pagedAndFilteredLocationTags = filteredLocationTags
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var locationTags = ObjectMapper.Map<List<LocationTagDto>>(pagedAndFilteredLocationTags);

            var totalCount = await filteredLocationTags.CountAsync();

            return new PagedResultDto<LocationTagDto>(
                totalCount,
                locationTags
            );
        }

        public async Task<PagedResultDto<LocationTagLocationDto>> GetAllLocationOfTag(GetLocationTagLocationsInput input)
        {
            var locationTag = _locationTagLocationRepository.GetAll()
                .Where(l => l.LocationTagId == input.LocationTagId)
                .Select(l => new LocationTagLocationDto
                {
                    Id = l.Id,
                    LocationId = l.LocationId,
                    LocationTagId = l.LocationTagId,
                    Code = l.Location.Code,
                    Name = l.Location.Name
                });

            var sortedlocationTag = await locationTag
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            return new PagedResultDto<LocationTagLocationDto>(await locationTag.CountAsync(), sortedlocationTag);
        }

        public async Task<PagedResultDto<LocationDto>> GetLocationNotInTag(GetLocationTagLocationsInput input)
        {
            var locationTag = _locationTagLocationRepository.GetAll()
                .Where(l => l.LocationTagId == input.LocationTagId)
                .Select(l => l.LocationId)
                .ToList();

            var location = _locationRepository.GetAll()
                .Where(l => !locationTag.Contains(l.Id))
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                EF.Functions.Like(e.Code, input.Filter.ToILikeString()));

            var sortedlocation = await location
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToListAsync();

            var locationDtoList = ObjectMapper.Map<List<LocationDto>>(sortedlocation);

            return new PagedResultDto<LocationDto>(await location.CountAsync(), locationDtoList);
        }

        public async Task<CreateOrEditLocationTagDto> GetLocationTagForEdit(EntityDto input)
        {
            var locationTag = await _locationTagRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditLocationTagDto>(locationTag);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLocationTagDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditLocationTagDto input)
        {
            var locationTag = ObjectMapper.Map<LocationTag>(input);

            if (AbpSession.TenantId != null)
            {
                locationTag.TenantId = (int)AbpSession.TenantId;
            }

            await _locationTagRepository.InsertAsync(locationTag);
        }

        protected virtual async Task Update(CreateOrEditLocationTagDto input)
        {
            var locationTag = await _locationTagRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, locationTag);
        }

        public async Task AddLocationToTag(AddLocationToTagDto input)
        {
            foreach (var item in input.LocationIds)
            {
                var locationTag = new LocationTagLocation
                {
                    LocationId = item,
                    LocationTagId = input.LocationTagId,
                    TenantId = (int)AbpSession.TenantId
                };

                await _locationTagLocationRepository.InsertAsync(locationTag);
            }
        }

        public async Task DeleteTag(EntityDto input)
        {
            await _locationTagRepository.DeleteAsync(input.Id);
        }

        public async Task DeleteLoctionTagLocation(EntityDto input)
        {
            await _locationTagLocationRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetLocationTagsToExcel(GetAllLocationTagsForExcelInput input)
        {
            var filteredLocationTags = await _locationTagRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Code, input.Filter.ToILikeString()))
                        .ToListAsync();

            var locationTagListDtos = ObjectMapper.Map<List<LocationTagDto>>(filteredLocationTags);

            return _locationTagsExcelExporter.ExportToFile(locationTagListDtos);
        }

        public async Task<PagedResultDto<IdNameDto>> GetSimpleLocationTags(GetAllLocationTagsInput input)
        {
            var filteredLocationTags = _locationTagRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
            EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
            EF.Functions.Like(e.Code, input.Filter.ToILikeString()));

            var pagedAndFilteredLocationTags = filteredLocationTags
                .OrderBy(input.Sorting ?? "name asc")
                .PageBy(input);

            var locationTags = ObjectMapper.Map<List<IdNameDto>>(pagedAndFilteredLocationTags);

            var totalCount = await filteredLocationTags.CountAsync();

            return new PagedResultDto<IdNameDto>(
                totalCount,
                locationTags
            );
        }
    }
}
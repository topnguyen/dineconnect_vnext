﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.UserLocations;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.DefaultLocations
{
    public class DefaultLocationAppService : DineConnectAppServiceBase, IDefaultLocationAppService
    {
        private ConnectSession _connectSession;
        private readonly ILocationManager _locationManager;
        private readonly ICompanyManager _companyManager;
        private readonly IUserLocationManger _userLocationManger;
        private readonly ISettingManager _settingManager;
        private readonly IRepository<User, long> _userRepository;

        public DefaultLocationAppService(ConnectSession connectSession, ILocationManager locationManager, ICompanyManager companyManager, IUserLocationManger userLocationManger, ISettingManager settingManager
            , IRepository<User, long> userRepository)
        {
            _connectSession = connectSession;
            _locationManager = locationManager;
            _companyManager = companyManager;
            _userLocationManger = userLocationManger;
            _settingManager = settingManager;
            _userRepository = userRepository;
        }

        public async Task<UserDefaultLocationDto> GetUserDefaultLocation()
        {
            if (await _userLocationManger.HasAlloedLocaionConfigured((long)AbpSession.UserId) && _connectSession.OrgId <= 0)
            {
                var firstAlloedLocation = await _userLocationManger.GetFirstAllowedLocation((long)AbpSession.UserId);
                _connectSession.OrgId = firstAlloedLocation.Location.OrganizationId;
                _connectSession.LocationId = firstAlloedLocation.LocationId;
            }
            var userDefaultLocation = new UserDefaultLocationDto();

            if (_connectSession.OrgId > 0)
            {
                var defaultOranization = await _companyManager.GetAsync(_connectSession.OrgId);
                userDefaultLocation.DefaultOrganizationName = defaultOranization.Name;
                userDefaultLocation.DefaultOrganizationId = defaultOranization.Id;

                var defaultLocation = await _locationManager.GetAsync(_connectSession.LocationId);
                userDefaultLocation.DefaultLocationName = defaultLocation.Name;
                userDefaultLocation.DefaultLocationId = defaultLocation.Id;

                userDefaultLocation.CityId = defaultLocation?.CityId;
                userDefaultLocation.CountryId = defaultLocation?.City?.CountryId;
                userDefaultLocation.StateId = defaultLocation?.City?.StateId;
            }

            return userDefaultLocation;
        }

        public async Task SetDefaultLocation(int locationId)
        {
            var location = await _locationManager.GetLocationWithoutBrandFilter(AbpSession.TenantId ?? 0, locationId);
            if (location != null)
            {
                _connectSession.LocationId = location.Id;
                _connectSession.OrgId = location.OrganizationId;

                await SetConnectSessionForAllUser(location);
            }
        }

        private async Task SetConnectSessionForAllUser(Location location)
        {
            var users = await _userRepository.GetAll().Where(e => e.Id != AbpSession.UserId).ToListAsync();
            foreach (var user in users)
            {
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, user.Id), "OrganizationId", location.OrganizationId.ToString());
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, user.Id), "LocationId", location.Id.ToString());
            }
        }

        public async Task<PagedResultDto<LocationDto>> GetLocationBasedOnUser(GetUserDefaultLocationInput input)
        {
            var defaultLocation = new List<Location>().AsQueryable();
            var qurableDefaultLocation = new List<Location>().AsQueryable();
            if (!await _userLocationManger.HasAlloedLocaionConfigured((long)AbpSession.UserId))
            {
                defaultLocation = _locationManager.GetAllLocation(input.OrganizationId, input.CountryId, input.StateId, input.CityId, input.Filter);
                qurableDefaultLocation = defaultLocation;
            }
            else
            {
                defaultLocation = _locationManager.GetListOfAllowedLocationForUser(AbpSession.TenantId ?? 0, AbpSession.UserId ?? 0, input.OrganizationId, input.CountryId, input.StateId, input.CityId, input.Filter);
                qurableDefaultLocation = defaultLocation;
            }

            qurableDefaultLocation = defaultLocation.WhereIf(input.OrganizationId > 0, o => o.OrganizationId == input.OrganizationId);

            var sortedDefaultLocation = await qurableDefaultLocation
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<LocationDto>
            {
                TotalCount = await qurableDefaultLocation.CountAsync(),
                Items = ObjectMapper.Map<List<LocationDto>>(sortedDefaultLocation)
            };
        }

        public async Task<List<BrandDto>> GetBrandBasedOnUser()
        {
            List<Brand> defaultLocation = new List<Brand>();
            if (!await _userLocationManger.HasAlloedLocaionConfigured((long)AbpSession.UserId))
            {
                var allBrand = _companyManager.GetList("");//GetAll(0, "");//.GetListOfAllowedBrandForUser();
                defaultLocation = await allBrand.ToListAsync();
                return ObjectMapper.Map<List<BrandDto>>(defaultLocation);
            }
            defaultLocation = await _locationManager.GetListOfAllowedBrandForUser(AbpSession.UserId ?? 0);
            return ObjectMapper.Map<List<BrandDto>>(defaultLocation);
        }
    }
}
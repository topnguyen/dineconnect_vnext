﻿using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;

namespace DinePlan.DineConnect.Connect.Master.DineDevices
{
    public class DineDeviceTypes
    {
        public static readonly Dictionary<string, List<FormlyType>> AllDeviceTypes
            = new Dictionary<string, List<FormlyType>>
            {
                {
                    "DineChef", new List<FormlyType>
                    {

                        new FormlyType
                        {
                            Key = "Name",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Name"
                            }

                        },
                        new FormlyType
                        {
                            Key = "TicketSettingData.PageSize",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "PageSize"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.SyncTicketStatusWithOrders",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Sync Ticket Status With Orders"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.ShowPartialCompleted",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show Partial Completed"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.ShowPageSpecificOrders",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show Page Specific Orders"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.AddNewFirst",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Add New First"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.ViewOption",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "View Option"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.Font",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Font"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.Header1",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Header1"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.Header2",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Header2"
                            }
                        },
                       new FormlyType
                        {
                            Key = "TicketStates",
                            Type = "repeatSection",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                BtnText="Add another Ticket States",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        ClassName="row",
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Key = "duration",
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Duration"
                            }
                                         },
                                                   new FormlyType
                        {

                                        Type = "input",
                                        Key = "durationMinutes",
                                        ClassName = "col-xs-3",
                                        TemplateOptions= new FormlyTemplateOption
                                        {
                                            Label= "DurationMinutes"
                                        }
                        },
                        new FormlyType
                        {
                                        Type = "input",
                                        Key = "durationSeconds",
                                        ClassName = "col-xs-3",
                                        TemplateOptions=new FormlyTemplateOption {
                                            Label= "Duration Seconds"
                                        }
},
                        new FormlyType
                             {
ClassName = "col-xs-3",
                                        Type = "input",
                                        Key = "levelCaption",
                                         TemplateOptions = new FormlyTemplateOption
    {
    Label = "Level Caption"
                                        }
}
                                        }
                                     },
                                     new Field
                                    {
                                        ClassName="row",
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                        {
        Type = "input",
                                        Key = "level",
                                        ClassName = "col-xs-3",
                                         TemplateOptions = new FormlyTemplateOption
        {
        Label = "Level"
                                        }
    },
                                                   new FormlyType
                       {
    Type = "input",
                                        Key = "background",
                                        ClassName = "col-xs-3",
                                         TemplateOptions = new FormlyTemplateOption
    {
    Label = "Background"
                                        }
},
                                                   new FormlyType{
    Type = "input",
                                        Key = "foreground",
                                        ClassName = "col-xs-3",
                                         TemplateOptions = new FormlyTemplateOption
    {
    Label = "Foreground"
                                        }
},
                                                   new FormlyType
                       {
    Type = "checkbox",
                                        Key = "blink",
                                        ClassName = "col-xs-3",
                                         TemplateOptions = new FormlyTemplateOption
    {
    Label = "Blink"
                                        }
}
                                                }
                                        }

                                }

                            }

                        },
                        new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.Duration",
                            Type="input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                 Label = "Duration",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Duration"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                        new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.DurationMinutes",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                 Label = "Duration Minutes",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Duration Minutes"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                        new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.DurationSeconds",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                 Label = "Duration Seconds",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Duration Seconds"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                        new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.LevelCaption",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Level Caption",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Level Caption"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                         new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.Level",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Level",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Level"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                         new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.Background",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                 Label = "Background",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Background"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                          new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.Foreground",
                            Type = "input",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Foreground",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "input",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Foreground"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                           new FormlyType
                        {
                            Key = "TicketSettingData.CompletedState.Blink",
                            Type = "checkbox",
                             ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Blink",
                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Type = "checkbox",
                            ClassName= "col-xs-3",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Blink"
                            }
                                         },

                                        }
                                     }

                                }
                            }

                        },
                         new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.SendTheQueueStatusToDinePlan",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Send The Queue Status To DinePlan"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.ShowQueueNumber",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show Queue Number"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "TicketSettingData.AdditionalOrderOnTop",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Additional Order On Top"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "TicketSettingData.TableNameFontSize",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Table Name Font Size"
                            }
                        },
                        new FormlyType
                        {
                            Key = "OrderStates",
                            Type = "repeatSection",
                            TemplateOptions = new FormlyTemplateOption
                            {

                                Fields= new List<Field>()
                                {
                                     new Field
                                    {
                                        ClassName="row",
                                        FieldGroup=new List<FormlyType>()
                                               {
                                                   new FormlyType
                             {
                            Key = "StateName",
                            Type = "input",
                            ClassName= "col-xs-6",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "State Name"
                            }
                                         },
                                                   new FormlyType
                        {

                                        Type = "input",
                                        Key = "Background",
                                        ClassName = "col-xs-6",
                                        TemplateOptions= new FormlyTemplateOption
                                        {
                                            Label= "Background"
                                        }
                        }
                                        }
                                     }

                                },
                                BtnText="Add another Order States"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "Theme",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Theme"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "Culture",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Culture"
                            }

                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "ClearData",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Clear Data"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "ShowDoneButton",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show Done Button"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "ShowMenuItemName",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show MenuItem Name"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "ShowAliasName",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Show Alias Name"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "Printer.Print",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Print"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "Printer.ShareName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Share Name"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "Printer.CodePage",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "CodePage"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "Printer.CharsPerLine",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "CharsPerLine"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "Printer.PrintFontFamily",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Print Font Family"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-4",
                            Key = "DepartmentFiltersPosition",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Department Filters Position"
                            }
                        }

                        }
                    },
                {
    "DineQueue", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "CalculationName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "CalculationName"
                            }

}
                }
}
            };


        public static readonly string[] DeviceTypes = AllDeviceTypes.Keys.ToArray();
    }
}

﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.DineDevices.Exporting
{
    public class DineDeviceExcelExporter : NpoiExcelExporterBase, IDineDeviceExcelExporter
    {
        public DineDeviceExcelExporter(
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<DineDeviceListDto> list)
        {
            return CreateExcelPackage(
                "DineDeviceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DineDevice"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Type")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ =>
                            _.Name,
                        _ => _.DineDeviceTypeName
                    );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
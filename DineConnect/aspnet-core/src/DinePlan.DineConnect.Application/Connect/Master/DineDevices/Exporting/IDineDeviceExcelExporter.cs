﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.DineDevices.Exporting
{
  public interface IDineDeviceExcelExporter
    {
        FileDto ExportToFile(List<DineDeviceListDto> list);
    }
}

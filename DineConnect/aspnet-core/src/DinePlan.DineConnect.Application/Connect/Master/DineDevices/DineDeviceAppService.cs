﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.DineDevices.Exporting;
using DinePlan.DineConnect.Connect.Print.DineDevice;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.DineDevices
{
    
    public class DineDeviceAppService : DineConnectAppServiceBase, IDineDeviceAppService
    {
        private readonly IDineDeviceExcelExporter _dineDeviceExporter;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<DineDevice> _dineDeviceRepo;

        public DineDeviceAppService(IRepository<DineDevice> dineDeviceRepo, IDineDeviceExcelExporter dineDeviceExporter, ConnectSession connectSession)
        {
            _dineDeviceRepo = dineDeviceRepo;
            _dineDeviceExporter = dineDeviceExporter;
            _connectSession = connectSession;
        }

        public async Task<PagedResultDto<DineDeviceListDto>> GetAll(GetDineDeviceInput input)
        {
            var allItems = _dineDeviceRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DineDeviceListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DineDeviceListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineDeviceInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<DineDeviceListDto>>(allList.Items);

            return _dineDeviceExporter.ExportToFile(allListDtos);
        }

        public async Task<CreateOrUpdateDineDeviceInput> GetDineDeviceForEdit(NullableIdDto input)
        {
            var output = new CreateOrUpdateDineDeviceInput();

            var editDto = new DineDeviceEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _dineDeviceRepo.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<DineDeviceEditDto>(hDto);

                UpdateLocations(hDto, output.LocationGroup);
            }

            output.DineDevice = editDto;

            return output;
        }

        public async Task CreateOrUpdateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.DineDevice.OrganizationId = _connectSession.OrgId;
            if (input.DineDevice.Id.HasValue)
            {
                await UpdateDineDevice(input);
            }
            else
            {
                await CreateDineDevice(input);
            }
        }

        
        public async Task DeleteDineDevice(EntityDto input)
        {
            await _dineDeviceRepo.DeleteAsync(input.Id);
        }

        
        protected virtual async Task UpdateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            var dto = input.DineDevice;

            var item = await _dineDeviceRepo.GetAsync(input.DineDevice.Id.Value);
           
            ObjectMapper.Map(dto, item);


            UpdateLocations(item, input.LocationGroup);
            
            await _dineDeviceRepo.UpdateAsync(item);
        }

        
        protected virtual async Task CreateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            var dto = ObjectMapper.Map<DineDevice>(input.DineDevice);


            UpdateLocations(dto, input.LocationGroup);
            
            await _dineDeviceRepo.InsertAsync(dto);
        }
        
        public ListResultDto<ComboboxItemDto> GetDineDeviceTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(DineDeviceType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public Task<List<FormlyType>> GetDineDeviceTypeSetting(EntityDto input)
        {
            var myKey = DineDeviceTypes.DeviceTypes[input.Id];

            var output = DineDeviceTypes.AllDeviceTypes[myKey];
            
            return Task.FromResult(output);
        }
    }
}

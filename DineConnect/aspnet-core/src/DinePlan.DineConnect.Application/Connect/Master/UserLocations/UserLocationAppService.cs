﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master.UserLocations.Dtos;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations
{
    public class UserLocationAppService : DineConnectAppServiceBase, IUserLocationAppService
    {
        private readonly IUserLocationManger _userLocationManger;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<UserLocation> _userLocationRepositoy;
        private readonly ISettingManager _settingManager;

        public UserLocationAppService(IUserLocationManger userLocationManger
            , ConnectSession connectSession
            , IRepository<UserLocation> userLocationRepositoy,
             ISettingManager settingManager
            )
        {
            _userLocationManger = userLocationManger;
            _connectSession = connectSession;
            _userLocationRepositoy = userLocationRepositoy;
            _settingManager = settingManager;
        }

        public async Task<int> AddUserToLocationAsync(UserLocationDto userLocationDetails)
        {
            await _userLocationManger.CreateOrUpdateAsync(new UserLocation
            {
                TenantId = AbpSession.TenantId ?? 0,
                UserId = userLocationDetails.UserId,
                LocationId = userLocationDetails.LocationId,
                Id = userLocationDetails.Id
            });

            var alloedLocaion = (await _userLocationManger.GetAllLocationByUserId(userLocationDetails.UserId)).Count;
            bool result = Int32.TryParse(await _settingManager.GetSettingValueForUserAsync("LocationId", _connectSession.TenantId, userLocationDetails.UserId), out int userDefaultLocationId);
            if (result && alloedLocaion == 1)
            {
                var getFirstAlloedLocation = await _userLocationManger.GetFirstAllowedLocation(userLocationDetails.UserId);
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(_connectSession.TenantId, userLocationDetails.UserId), "LocationId", getFirstAlloedLocation.LocationId.ToString());
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(_connectSession.TenantId, userLocationDetails.UserId), "OrganizationId", getFirstAlloedLocation.Location.OrganizationId.ToString());
            }
            return userLocationDetails.LocationId;
        }

        public async Task<PagedResultDto<UserNameIdDto>> FindUsers(FindUserLocationUsersInput input)
        {
            var userIdsInLocation = _userLocationRepositoy.GetAll()
               .Where(uou => uou.LocationId == input.LocationId)
               .Select(uou => uou.UserId);

            var userid = new List<long>();

            foreach (var item in userIdsInLocation)
            {
                userid.Add(item);
            }

            var useridQu = userid.AsQueryable().ToList();

            var a = userIdsInLocation.Count();

            var query = UserManager.Users
               .Where(u => !useridQu.Contains(u.Id))
               .WhereIf(
                   !input.Filter.IsNullOrWhiteSpace(),
                   u =>
                       EF.Functions.Like(u.Name, input.Filter.ToILikeString()) ||
                       EF.Functions.Like(u.Surname, input.Filter.ToILikeString()) ||
                       EF.Functions.Like(u.UserName, input.Filter.ToILikeString()) ||
                       EF.Functions.Like(u.EmailAddress, input.Filter.ToILikeString())
               );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.Name)
                .ThenBy(u => u.Surname)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<UserNameIdDto>(
                userCount,
                users.Select(u =>
                    new UserNameIdDto
                    {
                        UserName = $" {u.UserName} ( {u.EmailAddress})",
                        Id = u.Id
                    }
                ).ToList()
            );
        }

        public async Task<PagedResultDto<UserLocationDto>> GetUsersOfLocation(GetUsersOfLocationInput input)
        {
            try
            {
                var userLocationList = from userLocation in _userLocationRepositoy.GetAll()
                                       join user in UserManager.Users on userLocation.UserId equals user.Id
                                       where userLocation.LocationId == input.LocationId
                                       select new UserLocationDto
                                       {
                                           Id = userLocation.Id,
                                           LocationId = userLocation.LocationId,
                                           UserId = (int)user.Id,
                                           UserName = user.UserName,
                                           CreationTime = userLocation.CreationTime
                                       };

                var sortedUserLocationList = await userLocationList
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

                return new PagedResultDto<UserLocationDto>(
           await userLocationList.CountAsync(),
               sortedUserLocationList
               );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<int> RemoveUserFromLocation(int userLocationId)
        {
            var userLocation = _userLocationRepositoy.Get(userLocationId);
            bool result = Int32.TryParse(await _settingManager.GetSettingValueForUserAsync("LocationId", _connectSession.TenantId, userLocation.UserId), out int userDefaultLocationId);
            if (result)
            {
                if (userLocation.LocationId == Convert.ToInt32(userDefaultLocationId))
                {
                    await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(_connectSession.TenantId, userLocation.UserId), "LocationId", "-1");
                    await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(_connectSession.TenantId, userLocation.UserId), "OrganizationId", "-1");
                }
            }
            await _userLocationManger.DeleteAsync(userLocationId);
            return 1;
        }
    }
}
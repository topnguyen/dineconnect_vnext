﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Exporting;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies
{
    
    public class ForeignCurrenciesAppService : DineConnectAppServiceBase, IForeignCurrenciesAppService
    {
        private readonly IRepository<ForeignCurrency> _foreignCurrencyRepository;
        private readonly IForeignCurrenciesExcelExporter _foreignCurrenciesExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<PaymentTypes.PaymentType> _paymentTypeRepository;
        private readonly ConnectSession _connectSession;

        public ForeignCurrenciesAppService(IRepository<ForeignCurrency> foreignCurrencyRepository
            , IForeignCurrenciesExcelExporter foreignCurrenciesExcelExporter
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<PaymentTypes.PaymentType> paymentTypeRepository
            , ConnectSession connectSession)
        {
            _foreignCurrencyRepository = foreignCurrencyRepository;
            _foreignCurrenciesExcelExporter = foreignCurrenciesExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _paymentTypeRepository = paymentTypeRepository;
            _connectSession = connectSession;
        }

        public async Task<PagedResultDto<ForeignCurrencyDto>> GetAll(GetAllForeignCurrenciesInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredForeignCurrencies = _foreignCurrencyRepository.GetAll()
                        .Where(x => x.IsDeleted == input.IsDeleted)
                        .Include(e => e.PaymentType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.CurrencySymbol, input.Filter.ToILikeString()))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentTypeNameFilter), e => e.PaymentType != null && e.PaymentType.Name == input.PaymentTypeNameFilter);

                var pagedAndFilteredForeignCurrencies = await filteredForeignCurrencies
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var foreignCurrencies = ObjectMapper.Map<List<ForeignCurrencyDto>>(pagedAndFilteredForeignCurrencies);

                var totalCount = await filteredForeignCurrencies.CountAsync();

                return new PagedResultDto<ForeignCurrencyDto>(
                    totalCount,
                    foreignCurrencies
                );
            }
        }

        
        public async Task<GetForeignCurrencyForEditOutput> GetForeignCurrencyForEdit(EntityDto input)
        {
            var foreignCurrency = await _foreignCurrencyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetForeignCurrencyForEditOutput
            {
                ForeignCurrency = ObjectMapper.Map<CreateOrEditForeignCurrencyDto>(foreignCurrency)
            };

            //GetLocationsForEdit(output.ForeignCurrency, output.ForeignCurrency.LocationGroup);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditForeignCurrencyDto input)
        {
            ValidateForCreateOrUpdate(input);

            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditForeignCurrencyDto input)
        {
            var foreignCurrency = ObjectMapper.Map<ForeignCurrency>(input);
            foreignCurrency.LastModificationTime = Clock.Now;
            if (AbpSession.TenantId != null)
            {
                foreignCurrency.TenantId = (int)AbpSession.TenantId;
            }

            //UpdateLocations(foreignCurrency, input.LocationGroup);
            await _foreignCurrencyRepository.InsertAsync(foreignCurrency);
        }

        
        protected virtual async Task Update(CreateOrEditForeignCurrencyDto input)
        {
            var foreignCurrency = await _foreignCurrencyRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, foreignCurrency);
            //UpdateLocations(foreignCurrency, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _foreignCurrencyRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _foreignCurrencyRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _foreignCurrencyRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetForeignCurrenciesToExcel(GetAllForeignCurrenciesForExcelInput input)
        {
            var filteredForeignCurrencies = await _foreignCurrencyRepository.GetAll()
                        .Include(e => e.PaymentType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.CurrencySymbol, input.Filter.ToILikeString()))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentTypeNameFilter), e => e.PaymentType != null && e.PaymentType.Name == input.PaymentTypeNameFilter)
                        .ToListAsync();

            var foreignCurrencyListDtos = ObjectMapper.Map<List<ForeignCurrencyDto>>(filteredForeignCurrencies);

            return _foreignCurrenciesExcelExporter.ExportToFile(foreignCurrencyListDtos);
        }

        
        public async Task<ListResultDto<ComboboxItemDto>> GetAllPaymentTypeForLookupTable(string input)
        {
            var result = await _paymentTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input),
                  e => EF.Functions.Like(e.Name, input.ToILikeString()))
                .Select(g => new ComboboxItemDto(g.Id.ToString(), g.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(result);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditForeignCurrencyDto input)
        {
            var exists = _foreignCurrencyRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<ListResultDto<ForeignCurrencyDto>> ApiGetAll(ApiLocationInput locationInput)
        {
            var lstForeignCurrency = await _foreignCurrencyRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            return new ListResultDto<ForeignCurrencyDto>(ObjectMapper.Map<List<ForeignCurrencyDto>>(lstForeignCurrency));
        }
    }
}
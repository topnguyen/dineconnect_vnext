﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Exporting
{
    public class ForeignCurrenciesExcelExporter : NpoiExcelExporterBase, IForeignCurrenciesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ForeignCurrenciesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<ForeignCurrencyDto> foreignCurrencies)
        {
            return CreateExcelPackage(
                "ForeignCurrencies.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ForeignCurrencies"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("PaymentTypeName"),
                        L("CurrencySymbol"),
                        L("ExchangeRate"),
                        L("UpdatedDate")
                        );

                    AddObjects(
                        sheet, 2, foreignCurrencies,
                        _ => _.Name,
                        _ => _.PaymentTypeName,
                        _ => _.CurrencySymbol,
                        _ => _.ExchangeRate,
                        _ => _timeZoneConverter.Convert(_.LastModificationTime, _abpSession.TenantId, _abpSession.GetUserId())

                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
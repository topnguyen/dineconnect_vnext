﻿using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Exporting
{
    public interface IForeignCurrenciesExcelExporter
    {
        FileDto ExportToFile(List<ForeignCurrencyDto> foreignCurrencies);
    }
}
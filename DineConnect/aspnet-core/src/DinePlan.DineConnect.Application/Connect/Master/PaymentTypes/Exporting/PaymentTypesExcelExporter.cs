﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Exporting
{
    public class PaymentTypesExcelExporter : NpoiExcelExporterBase, IPaymentTypesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPaymentTypeForViewDto> paymentTypes)
        {
            return CreateExcelPackage(
                "PaymentTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PaymentTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("AcceptChange"),
                        L("DisplayInShift"),
                        L("NoRefund"),
                        L("AccountCode"),
                        L("PaymentGroup"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, paymentTypes,
                        _ => _.PaymentType.Name,
                        _ => _.PaymentType.AcceptChange,
                        _ => _.PaymentType.DisplayInShift,
                        _ => _.PaymentType.NoRefund,
                        _ => _.PaymentType.AccountCode,
                        _ => _.PaymentType.PaymentGroup,
                        _ => _.PaymentType.CreationTime.ToShortDateString()
                        );

                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Exporting
{
    public interface IPaymentTypesExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentTypeForViewDto> paymentTypes);
    }
}
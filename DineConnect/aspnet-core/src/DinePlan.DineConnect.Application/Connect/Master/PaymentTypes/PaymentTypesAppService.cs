﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Exporting;
using DinePlan.DineConnect.Connect.PaymentType;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes
{
    public class PaymentTypesAppService : DineConnectAppServiceBase, IPaymentTypesAppService
    {
        private const int MaxImageBytes = 5242880; //5MB
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPaymentTypesExcelExporter _paymentTypesExcelExporter;
        private readonly IRepository<Location, int> _lookupLocationRepository;
        private readonly ConnectSession _connectSession;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ICloudImageUploader _cloudImageUploader;
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncersAppService _syncersAppService;

        public PaymentTypesAppService(
            IRepository<PaymentType> paymentTypeRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IPaymentTypesExcelExporter paymentTypesExcelExporter,
            IRepository<Location, int> locationRepository,
            ConnectSession connectSession,
            ITempFileCacheManager tempFileCacheManager,
            IBinaryObjectManager binaryObjectManager,
            ICloudImageUploader cloudImageUploader, ILocationAppService locationAppService, ISyncersAppService syncersAppService
            )
        {
            _paymentTypeRepository = paymentTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _paymentTypesExcelExporter = paymentTypesExcelExporter;
            _lookupLocationRepository = locationRepository;
            _connectSession = connectSession;
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            _cloudImageUploader = cloudImageUploader;
            _locationAppService = locationAppService;
            _syncersAppService = syncersAppService;
        }

        public async Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAll(GetAllPaymentTypesInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredPaymentTypes = _paymentTypeRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .Include(e => e.Location)
                    .WhereIf(input.IsAcceptChange == true, e => e.AcceptChange == input.IsAcceptChange)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                                                                            EF.Functions.Like(e.Name,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.AccountCode,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Processors,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.ButtonColor,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.PaymentGroup,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Files,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.PaymentTag,
                                                                                input.Filter.ToILikeString()))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.LocationNameFilter),
                        e => e.Location != null && e.Location.Name == input.LocationNameFilter);

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredPaymentTypes = FilterByLocations(filteredPaymentTypes, input);
                }

                filteredPaymentTypes = filteredPaymentTypes
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input);

                var paymentTypes = filteredPaymentTypes.Select(o => new GetPaymentTypeForViewDto()
                {
                    PaymentType = ObjectMapper.Map<PaymentTypeDto>(o),
                    LocationName = o.Location == null ? "" : o.Location.Name
                });

                var totalCount = await filteredPaymentTypes.CountAsync();

                return new PagedResultDto<GetPaymentTypeForViewDto>(
                    totalCount,
                    await paymentTypes.ToListAsync()
                );
            }
        }

        
        public async Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(EntityDto input)
        {
            var paymentType = await _paymentTypeRepository.GetAll().FirstOrDefaultAsync(x => x.Id == input.Id);

            var output = new GetPaymentTypeForEditOutput { PaymentType = ObjectMapper.Map<CreateOrEditPaymentTypeDto>(paymentType) };

            if (output.PaymentType.LocationId != null)
            {
                var _lookupLocation = await _lookupLocationRepository.FirstOrDefaultAsync((int)output.PaymentType.LocationId);
                output.LocationName = _lookupLocation.Name.ToString();
            }
            GetLocationsForEdit(output.PaymentType, output.PaymentType.LocationGroup);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPaymentTypeDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            if (!input.UploadedImageToken.IsNullOrWhiteSpace())
            {
                var image = _tempFileCacheManager.GetFile(input.UploadedImageToken);

                if (image != null)
                {
                    var result = await _cloudImageUploader.UploadImage(input.Name, image);

                    if (result != null)
                    {
                        input.Files = result.Url;
                    }
                }

                input.DownloadImage = Guid.Parse(input.UploadedImageToken);
            }

            ValidateForCreateOrUpdate(input);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditPaymentTypeDto input)
        {
            var paymentType = ObjectMapper.Map<PaymentType>(input);

            if (AbpSession.TenantId != null)
            {
                paymentType.TenantId = AbpSession.TenantId.Value;
            }
            UpdateLocations(paymentType, input.LocationGroup);

            var paymentId = await _paymentTypeRepository.InsertAndGetIdAsync(paymentType);
        }

        
        protected virtual async Task Update(CreateOrEditPaymentTypeDto input)
        {
            var paymentType = await _paymentTypeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, paymentType);
            UpdateLocations(paymentType, input.LocationGroup);

            await _paymentTypeRepository.UpdateAsync(paymentType);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _paymentTypeRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _paymentTypeRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _paymentTypeRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetPaymentTypesToExcel(GetAllPaymentTypesForExcelInput input)
        {
            var filteredPaymentTypes = _paymentTypeRepository.GetAll()
                        .Include(e => e.Location)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.AccountCode, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Processors, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.ButtonColor, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.PaymentGroup, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Files, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.PaymentTag, input.Filter.ToILikeString()))
                        .WhereIf(input.IsAcceptChange.HasValue, e => e.AcceptChange == input.IsAcceptChange)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LocationNameFilter), e => e.Location != null && e.Location.Name == input.LocationNameFilter);

            var query = filteredPaymentTypes
                .Select(o => new GetPaymentTypeForViewDto()
                {
                    PaymentType = ObjectMapper.Map<PaymentTypeDto>(o),
                    LocationName = o.Location == null ? "" : o.Location.Name
                });

            var paymentTypeListDtos = await query.ToListAsync();

            return _paymentTypesExcelExporter.ExportToFile(paymentTypeListDtos);
        }

        
        public async Task<PagedResultDto<PaymentTypeLocationLookupTableDto>> GetAllLocationForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _lookupLocationRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => EF.Functions.Like(e.Name, input.Filter.ToILikeString())
               );

            var totalCount = await query.CountAsync();

            var locationList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<PaymentTypeLocationLookupTableDto>();
            foreach (var location in locationList)
            {
                lookupTableDtoList.Add(new PaymentTypeLocationLookupTableDto
                {
                    Id = location.Id,
                    DisplayName = location.Name?.ToString()
                });
            }

            return new PagedResultDto<PaymentTypeLocationLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPaymentProcessors()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in PaymentTypeProcessors.Processors)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public ListResultDto<ComboboxItemDto> GetPaymentTenderType()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PaymentTenderType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task UpdatePaymentTypeImage(UpdatePaymentTypeImageInput input)
        {
            byte[] byteArray;

            var imageBytes = _tempFileCacheManager.GetFile(input.FileToken);

            if (imageBytes == null)
            {
                throw new UserFriendlyException("There is no such image file with the token: " + input.FileToken);
            }

            using (var bmpImage = new Bitmap(new MemoryStream(imageBytes)))
            {
                var width = (input.Width == 0 || input.Width > bmpImage.Width) ? bmpImage.Width : input.Width;
                var height = (input.Height == 0 || input.Height > bmpImage.Height) ? bmpImage.Height : input.Height;
                var bmCrop = bmpImage.Clone(new Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                using (var stream = new MemoryStream())
                {
                    bmCrop.Save(stream, bmpImage.RawFormat);
                    byteArray = stream.ToArray();
                }
            }

            if (byteArray.Length > MaxImageBytes)
            {
                throw new UserFriendlyException(L("ResizedProfilePicture_Warn_SizeLimit", AppConsts.ResizedMaxProfilPictureBytesUserFriendlyValue));
            }

            var payment = await _paymentTypeRepository.GetAsync(input.PaymentTypeId);

            if (payment.DownloadImage.HasValue)
            {
                await _binaryObjectManager.DeleteAsync(payment.DownloadImage.Value);
            }

            var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            await _binaryObjectManager.SaveAsync(storedFile);

            payment.DownloadImage = storedFile.Id;
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditPaymentTypeDto input)
        {
            var exists = _paymentTypeRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<PagedResultDto<NameValueDto>> GetPaymentTypeForLookupTable(GetAllForLookupTableInput input)
        {
            var filteredDepartments = _paymentTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "name asc")
                .PageBy(input);

            var departments = await pagedAndFilteredDepartments.Select(d => new NameValueDto(d.Name, d.Id.ToString())).ToListAsync();

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<NameValueDto>(
                totalCount,
                departments
            );
        }

        public async Task<ListResultDto<ApiPaymentTypeOutput>> ApiGetAll(ApiTenantInputDto input)
        {
            var localPaymentTypes = await _paymentTypeRepository.GetAllListAsync(a => a.TenantId == input.TenantId);

            List<ApiPaymentTypeOutput> paymentTypeOutputs = new List<ApiPaymentTypeOutput>();
            foreach (var sortL in localPaymentTypes)
            {
                if (await _locationAppService.IsLocationExists(new CheckLocationInput
                {
                    LocationId = input.LocationId,
                    Locations = sortL.Locations,
                    Group = sortL.Group,
                    NonLocations = sortL.NonLocations,
                    LocationTag = sortL.LocationTag
                }))
                {
                    var myPt = new ApiPaymentTypeOutput
                    {
                        Id = sortL.Id,
                        AcceptChange = sortL.AcceptChange,
                        SortOrder = sortL.SortOrder,
                        AccountCode = sortL.AccountCode,
                        PaymentProcessor = sortL.PaymentProcessor,
                        Name = sortL.Name,
                        Processors = sortL.Processors,
                        ButtonColor = sortL.ButtonColor,
                        Group = sortL.PaymentGroup,
                        NoRefund = sortL.NoRefund,
                        AutoPayment = sortL.AutoPayment,
                        DisplayInShift = sortL.DisplayInShift,
                        Files = sortL.Files,
                        DownloadImage = sortL.DownloadImage,
                        Departments = sortL.Departments,
                        PaymentTenderType = sortL.PaymentTenderType,
                        PaymentTag = sortL.PaymentTag
                    };

                    if (!string.IsNullOrEmpty(sortL.ProcessorName))
                    {
                        myPt.PaymentProcessorName = sortL.ProcessorName;
                    }
                    else
                    {
                        myPt.PaymentProcessorName = sortL.PaymentProcessor > 0
                            ? PaymentTypeProcessors.Processors[sortL.PaymentProcessor - 1]
                            : "";
                    }

                    paymentTypeOutputs.Add(myPt);
                }
            }
            return new ListResultDto<ApiPaymentTypeOutput>(paymentTypeOutputs);
        }


        public async Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAllItems()
        {
            var allItems = _paymentTypeRepository.GetAll();

            var allListDtos = await allItems.Select(o => new GetPaymentTypeForViewDto()
            {
                PaymentType = ObjectMapper.Map<PaymentTypeDto>(o),
                LocationName = o.Location == null ? "" : o.Location.Name
            }).ToListAsync();

            var allItemCount = await allItems.CountAsync();
            return new PagedResultDto<GetPaymentTypeForViewDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
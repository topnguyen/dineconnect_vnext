﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.TicketType.Exporting;
using DinePlan.DineConnect.Connect.Master.TicketTypes;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.TicketType
{
    
    public class TicketTypeAppService : DineConnectAppServiceBase, ITicketTypeAppService
    {
        private readonly IRepository<TicketTypes.TicketType> _ticketTypeManager;
        private readonly IRepository<TicketTypeConfiguration> _ticketTypeConfigurationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITicketTypeExcelExporter _ticketTypeExcelExporter;
        private readonly ConnectSession _connectSession;

        public TicketTypeAppService(
            IRepository<TicketTypes.TicketType> ticketTypeManager,
            IRepository<TicketTypeConfiguration> ticketTypeConfigurationManager,
            IUnitOfWorkManager unitOfWorkManager,
            ITicketTypeExcelExporter ticketTypeExcelExporter,
            ConnectSession connectSession)
        {
            _ticketTypeManager = ticketTypeManager;
            _ticketTypeConfigurationManager = ticketTypeConfigurationManager;
            _unitOfWorkManager = unitOfWorkManager;
            _ticketTypeExcelExporter = ticketTypeExcelExporter;
            _connectSession = connectSession;
        }

        public async Task<PagedResultDto<TicketTypeConfigurationListDto>> GetAll(GetTicketTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<TicketTypeConfiguration> allItems = _ticketTypeConfigurationManager.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter));

                if (!input.Location.IsNullOrWhiteSpace())
                {
                    var tickettype = _ticketTypeManager.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                        !input.Location.IsNullOrWhiteSpace(),
                        p => p.Locations.Contains(input.Location));

                    var tickettypeIds = tickettype.Select(t => t.TicketTypeConfigurationId).ToList();

                    allItems = allItems.Where(t => tickettypeIds.Contains(t.Id));
                }

                var allItemCount = allItems.Count();

                var sortMenuItems = await allItems.AsQueryable().ToListAsync();

                var allListDtos = ObjectMapper.Map<List<TicketTypeConfigurationListDto>>(sortMenuItems);

                return new PagedResultDto<TicketTypeConfigurationListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetAllTicketTypeForLookupTable()
        {
            var all = await _ticketTypeConfigurationManager.GetAll().Select(t => new ComboboxItemDto(t.Id.ToString(), t.Name)).ToListAsync();

            return new ListResultDto<ComboboxItemDto>(all);
        }

        public async Task<FileDto> GetAllToExcel(GetTicketTypeInput input)
        {
            var allList = await GetAll(input);

            var allListDtos = ObjectMapper.Map<List<TicketTypeConfigurationListDto>>(allList.Items);

            return await _ticketTypeExcelExporter.ExportToFile(allListDtos);
        }

        public async Task<GetTicketTypeOutputDto> GetTicketTypeForEdit(TicketTypeFilterInputDto input)
        {
            TicketTypeConfigurationEditDto editDto;

            try
            {
                if (input.Id.HasValue)
                {
                    List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                    List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                    List<int> filterLocationRefIds = new List<int>();
                    List<int> filterLocationGroupRefIds = new List<int>();
                    List<int> filterLocationTagRefIds = new List<int>();
                    if (input.LocationGroupToBeFiltered != null)
                    {
                        filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                        filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                        filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                    }

                    var ticketTypes = await _ticketTypeManager.GetAllListAsync(t => t.TicketTypeConfigurationId == input.Id.Value);
                    var tempValuesFilters = ObjectMapper.Map<Collection<TicketTypeEditDto>>(ticketTypes);

                    var ticketTypeConfiguration = await _ticketTypeConfigurationManager.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                    editDto = ObjectMapper.Map<TicketTypeConfigurationEditDto>(ticketTypeConfiguration);

                    Collection<TicketTypeEditDto> valuesOutput = new Collection<TicketTypeEditDto>();
                    foreach (var lst in tempValuesFilters)
                    {
                        var ticketType = ticketTypes.FirstOrDefault(x => x.Id == lst.Id);

                        GetLocationsForEdit(lst, lst.LocationGroup);
                        //get(ticketType, lst.LocationGroup);

                        if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                        {
                            if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                        }
                        else
                        {
                            valuesOutput.Add(lst);
                        }
                    }

                    editDto.TicketTypes = valuesOutput;
                }
                else
                {
                    editDto = new TicketTypeConfigurationEditDto();
                }

                return new GetTicketTypeOutputDto
                {
                    TicketTypeConfiguration = editDto,
                    TicketType = new TicketTypeEditDto()
                };
            }
            catch (Exception ex)
            {
                // Ignore
            }

            return null;
        }

        public async Task<EntityDto> AddOrEditTicketType(CreateOrUpdateTicketTypeInput input)
        {
            var ticketType = input.TicketType;

            if (ticketType.Id.HasValue)
            {
                var existEntry = await _ticketTypeManager.FirstOrDefaultAsync(t => t.Id == ticketType.Id.Value);
                //existEntry.OrganizationId = _connectSession.OrgId;

                ObjectMapper.Map(ticketType, existEntry);
                UpdateLocations(existEntry, ticketType.LocationGroup);
                await _ticketTypeManager.UpdateAsync(existEntry);
                return new EntityDto { Id = existEntry.Id };
            }
            else
            {
                var dto = ObjectMapper.Map<TicketTypes.TicketType>(ticketType);
                //dto.OrganizationId = _connectSession.OrgId;
                UpdateLocations(dto, ticketType.LocationGroup);
                await _ticketTypeManager.InsertOrUpdateAndGetIdAsync(dto);
                return new EntityDto { Id = dto.Id };
            }
        }

        
        public async Task DeleteTicketType(EntityDto input)
        {
            var rstickettype = await _ticketTypeManager.GetAllListAsync(t => t.TicketTypeConfigurationId == input.Id);
            if (rstickettype == null)
            {
                throw new UserFriendlyException("TICKETTYPENOTFOUND");
            }

            foreach (var tickettype in rstickettype)
            {
                await _ticketTypeManager.DeleteAsync(tickettype.Id);
            }
            await _ticketTypeConfigurationManager.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var rstickettypelist = await _ticketTypeManager.GetAllListAsync(t => t.TicketTypeConfigurationId == input.Id);

                foreach (var tickettype in rstickettypelist)
                {
                    var rstickettype = rstickettypelist.FirstOrDefault(t => t.Id == tickettype.Id);
                    if (rstickettype != null)
                    {
                        rstickettype.IsDeleted = false;
                        rstickettype.DeleterUserId = null;
                        rstickettype.DeletionTime = null;
                        await _ticketTypeManager.UpdateAsync(rstickettype);
                    }
                }
                var item = await _ticketTypeConfigurationManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _ticketTypeConfigurationManager.UpdateAsync(item);
            }
        }

        public async Task DeleteTicketTypeMapping(EntityDto input)
        {
            await _ticketTypeManager.DeleteAsync(t => t.Id == input.Id);
        }
    }
}
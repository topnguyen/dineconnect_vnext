﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.TicketType.Exporting
{
    public class TicketTypeExcelExporter : NpoiExcelExporterBase, ITicketTypeExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public TicketTypeExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<TicketTypeConfigurationListDto> items)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "TicketType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TicketType"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name")
                    );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Id,
                        _ => _.Name
                    );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
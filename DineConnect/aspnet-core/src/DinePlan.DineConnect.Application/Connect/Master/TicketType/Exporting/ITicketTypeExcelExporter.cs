﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.TicketType.Exporting
{
    public interface ITicketTypeExcelExporter
    {
        Task<FileDto> ExportToFile(List<TicketTypeConfigurationListDto> items);
    }
}
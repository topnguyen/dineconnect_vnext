﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master.Departments.Exporting;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Departments
{
    
    public class DepartmentsAppService : DineConnectAppServiceBase, IDepartmentsAppService
    {
        private readonly IRepository<Department> _departmentRepository;
        private readonly IRepository<DepartmentGroup> _departmentGroupRepository;
        private readonly IDepartmentsExcelExporter _departmentsExcelExporter;
        private readonly IRepository<PriceTag> _priceTagRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;

        public DepartmentsAppService(
            IRepository<Department> departmentRepository,
            IRepository<DepartmentGroup> departmentGroupRepository,
            IDepartmentsExcelExporter departmentsExcelExporter,
            IRepository<PriceTag> priceTagRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ConnectSession connectSession,
            ILocationAppService locationAppService)
        {
            _departmentRepository = departmentRepository;
            _departmentGroupRepository = departmentGroupRepository;
            _departmentsExcelExporter = departmentsExcelExporter;
            _priceTagRepository = priceTagRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
        }

        public async Task<PagedResultDto<DepartmentDto>> GetAll(GetAllDepartmentsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredDepartments = _departmentRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .Include(e => e.PriceTag)
                    .Include(e => e.DepartmentGroup)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                                p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter)
                                || p.DepartmentGroup.Name.Contains(input.Filter) || p.DepartmentGroup.Code.Contains(input.Filter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PriceTagNameFilter),
                        e => e.PriceTag != null && e.PriceTag.Name == input.PriceTagNameFilter);

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredDepartments = FilterByLocations(filteredDepartments, input);
                }

                var pagedAndFilteredDepartments = filteredDepartments
                    .OrderBy(input.Sorting ?? $"{nameof(Department.SortOrder)} asc")
                    .PageBy(input);

                var departments = ObjectMapper.Map<List<DepartmentDto>>(pagedAndFilteredDepartments);

                var totalCount = await filteredDepartments.CountAsync();

                return new PagedResultDto<DepartmentDto>(
                    totalCount,
                    departments
                );
            }
        }

        public async Task<ListResultDto<DepartmentDto>> GetListAll()
        {
            var filteredDepartments = await _departmentRepository.GetAll()
                .Include(e => e.PriceTag)
                .Include(e => e.DepartmentGroup)
                .OrderBy($"{nameof(Department.SortOrder)} asc")
                .ToListAsync();

            var departments = ObjectMapper.Map<List<DepartmentDto>>(filteredDepartments);

            return new ListResultDto<DepartmentDto>(departments);
        }

        public async Task SaveSortSortItems(int[] menuItems)
        {
            var i = 1;
            foreach (var item in menuItems)
            {
                var mItem = await _departmentRepository.GetAsync(item);
                mItem.SortOrder = i++;
                await _departmentRepository.UpdateAsync(mItem);
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        
        public async Task<CreateOrEditDepartmentDto> GetDepartmentForEdit(EntityDto input)
        {
            var department = await _departmentRepository
                .GetAllIncluding(x => x.DepartmentGroup, x => x.PriceTag)
                .FirstOrDefaultAsync(x => x.Id == input.Id);

            var output = ObjectMapper.Map<CreateOrEditDepartmentDto>(department);

            GetLocationsForEdit(output, output.LocationGroup);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDepartmentDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditDepartmentDto input)
        {
            var department = ObjectMapper.Map<Department>(input);

            if (AbpSession.TenantId != null)
            {
                department.TenantId = (int)AbpSession.TenantId;
            }

            UpdateLocations(department, input.LocationGroup);
            await _departmentRepository.InsertAsync(department);
        }

        
        protected virtual async Task Update(CreateOrEditDepartmentDto input)
        {
            var department = await _departmentRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, department);
            UpdateLocations(department, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _departmentRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _departmentRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _departmentRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetDepartmentsToExcel(GetAllDepartmentsForExcelInput input)
        {
            var filteredDepartments = await _departmentRepository.GetAll()
                .Include(e => e.PriceTag)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                                                                        EF.Functions.Like(e.Name,
                                                                            input.Filter.ToILikeString()))
                .ToListAsync();

            var departmentListDtos = ObjectMapper.Map<List<DepartmentDto>>(filteredDepartments);

            return await _departmentsExcelExporter.ExportToFile(departmentListDtos);
        }

        
        public async Task<ListResultDto<ComboboxItemDto>> GetAllPriceTagForLookupTable(string input)
        {
            var lookupTableDtoList = _priceTagRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input), e =>
                        EF.Functions.Like(e.Name, input.ToILikeString()))
                .Select(t => new ComboboxItemDto(t.Id.ToString(), t.Name));

            return new ListResultDto<ComboboxItemDto>(await lookupTableDtoList.ToListAsync());
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditDepartmentDto input)
        {
            var exists = _departmentRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetDepartmentsForCombobox()
        {
            var dtos = await _departmentRepository.GetAll()
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(dtos);
        }

        // Department Group

        public async Task<PagedResultDto<DepartmentGroupListDto>> GetAllDepartmentGroup(GetDepartmentGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _departmentGroupRepository.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                allItems = allItems
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter));

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = ObjectMapper.Map<List<DepartmentGroupListDto>>(sortMenuItems);

                var allItemCount = await allItems.CountAsync();

                return new PagedResultDto<DepartmentGroupListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllDepartmentGroupToExcel()
        {
            var allList = await _departmentGroupRepository.GetAll().ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DepartmentGroupListDto>>(allList);

            return await _departmentsExcelExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDepartmentGroupForEditOutput> GetDepartmentGroupForEdit(NullableIdDto input)
        {
            DepartmentGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _departmentGroupRepository.GetAsync(input.Id.Value);
                editDto = ObjectMapper.Map<DepartmentGroupEditDto>(hDto);
            }
            else
            {
                editDto = new DepartmentGroupEditDto();
            }

            return new GetDepartmentGroupForEditOutput
            {
                DepartmentGroup = editDto
            };
        }

        public async Task CreateOrUpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            if (input.DepartmentGroup.Id.HasValue)
            {
                await UpdateDepartmentGroup(input);
            }
            else
            {
                await CreateDepartmentGroup(input);
            }
        }

        
        protected virtual async Task UpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            var item = await _departmentGroupRepository.GetAsync(input.DepartmentGroup.Id.Value);
            var dto = input.DepartmentGroup;
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.AddOns = dto.AddOns;

            CheckErrors(await CreateDepartmentGroupSync(item));
        }

        
        protected virtual async Task CreateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            var dto = ObjectMapper.Map<DepartmentGroup>(input.DepartmentGroup);

            CheckErrors(await CreateDepartmentGroupSync(dto));
        }

        
        public async Task<bool> DeleteDepartmentGroup(EntityDto input)
        {
            bool isExists = false;

            var recordExists = await _departmentRepository.GetAllListAsync(t => t.DepartmentGroupId == input.Id);

            if (recordExists != null && recordExists.Count > 0)
            {
                isExists = true;
                return isExists;
            }
            else
            {
                await _departmentGroupRepository.DeleteAsync(input.Id);

                return isExists;
            }
        }

        public async Task<ListResultDto<DepartmentGroupListDto>> GetDepartmentGroupIds()
        {
            var lstDepartmentGroup = await _departmentGroupRepository.GetAll().ToListAsync();
            return new ListResultDto<DepartmentGroupListDto>(
                ObjectMapper.Map<List<DepartmentGroupListDto>>(lstDepartmentGroup));
        }

        public async Task ActivateDepartmentGroupItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _departmentGroupRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _departmentGroupRepository.UpdateAsync(item);
            }
        }

        public async Task<ListResultDto<DepartmentGroupListDto>> GetDepartmentGroups()
        {
            var departmentGroup = await _departmentGroupRepository.GetAllListAsync();
            return new ListResultDto<DepartmentGroupListDto>(
                ObjectMapper.Map<List<DepartmentGroupListDto>>(departmentGroup));
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetComboBoxDepartmentGroups()
        {
            var groups = await _departmentGroupRepository.GetAllListAsync();
            return new ListResultDto<ComboboxItemDto>(groups.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToList());
        }

        public async Task<IdentityResult> CreateDepartmentGroupSync(DepartmentGroup departmentGroup)
        {
            var codeExists = _departmentGroupRepository.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(departmentGroup.Code))
                .WhereIf(departmentGroup.Id != 0, a => a.Id != departmentGroup.Id);

            var nameExists = _departmentGroupRepository.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(departmentGroup.Name))
                .WhereIf(departmentGroup.Id != 0, a => a.Id != departmentGroup.Id);

            //  if the New Addition
            if (codeExists.Any())
            {
                var errors = new List<IdentityError>
                {
                    new IdentityError
                    {
                        Code = "AlreadyExists",
                        Description = $"{L("DepartmentGroup")} {L("Code")} {departmentGroup.Code} {L("AlreadyExists")}"
                    }
                };

                var success = IdentityResult.Failed(errors.ToArray());

                return success;
            }

            if (nameExists.Any())
            {
                var errors = new List<IdentityError>
                {
                    new IdentityError
                    {
                        Code = "AlreadyExists",
                        Description = $"{L("DepartmentGroup")} {L("Code")} {departmentGroup.Code} {L("AlreadyExists")}"
                    }
                };

                var success = IdentityResult.Failed(errors.ToArray());

                return success;
            }

            await _departmentGroupRepository.InsertOrUpdateAndGetIdAsync(departmentGroup);

            return IdentityResult.Success;
        }

        public async Task<PagedResultDto<NameValueDto>> GetDepartmentsForLookup(GetDepartmentsForLoockup input)
        {
            var filteredDepartments = _departmentRepository.GetAll()
                .Include(e => e.PriceTag)
                .Include(e => e.DepartmentGroup)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? $"{nameof(Department.SortOrder)} asc")
                .PageBy(input);

            var departments = await pagedAndFilteredDepartments.Select(d => new NameValueDto(d.Name, d.Id.ToString())).ToListAsync();

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<NameValueDto>(
                totalCount,
                departments
            );
        }

        public async Task<ListResultDto<ApiDepartmentOutput>> ApiGetAll(ApiLocationInput input)
        {
            var lstDept = _departmentRepository.GetAll()
                .Include(a => a.DepartmentGroup)
                .Include(a => a.TicketType)
                .Include(a => a.PriceTag)
                .Where(a => a.TenantId == input.TenantId);

            List<ApiDepartmentOutput> deptOutput = new List<ApiDepartmentOutput>();
            if (input.LocationId > 0)
            {
                foreach (var mydept in lstDept)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = mydept.Locations,
                        Group = mydept.Group,
                        NonLocations = mydept.NonLocations,
                        LocationTag = mydept.LocationTag
                    }))
                    {
                        var deGroup = mydept.DepartmentGroup;
                        if (deGroup == null && mydept.DepartmentGroupId != null)
                        {
                            deGroup = await _departmentGroupRepository.GetAsync(mydept.DepartmentGroupId.Value);
                        }
                        deptOutput.Add(new ApiDepartmentOutput
                        {
                            Id = mydept.Id,
                            Name = mydept.Name,
                            TagId = mydept.PriceTagId ?? 0,
                            SortOrder = mydept.SortOrder,
                            DepartmentGroupCode = deGroup != null ? deGroup.Code : "",
                            DepartmentGroup = deGroup != null ? deGroup.Name : "",
                            TicketTypeId = mydept.TicketTypeId,
                            Code = mydept.Code
                        });
                    }
                }
            }
            else
            {
                foreach (var mydept in lstDept)
                {
                    var deGroup = mydept.DepartmentGroup;
                    if (deGroup == null && mydept.DepartmentGroupId != null)
                    {
                        deGroup = await _departmentGroupRepository.GetAsync(mydept.DepartmentGroupId.Value);
                    }
                    deptOutput.Add(new ApiDepartmentOutput
                    {
                        Id = mydept.Id,
                        Name = mydept.Name,
                        TagId = mydept.PriceTagId ?? 0,
                        SortOrder = mydept.SortOrder,
                        DepartmentGroupCode = deGroup != null ? deGroup.Code : "",
                        DepartmentGroup = deGroup != null ? deGroup.Name : "",
                        TicketTypeId = mydept.TicketTypeId,
                        Code = mydept.Code
                    });
                }
            }
            return new ListResultDto<ApiDepartmentOutput>(deptOutput);
        }
    }
}
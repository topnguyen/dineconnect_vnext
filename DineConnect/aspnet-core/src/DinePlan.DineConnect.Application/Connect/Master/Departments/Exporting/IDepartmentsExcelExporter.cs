﻿using DinePlan.DineConnect.Connect.Master.Departments.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Departments.Exporting
{
    public interface IDepartmentsExcelExporter
    {
        Task<FileDto> ExportToFile(List<DepartmentDto> departments);

        Task<FileDto> ExportToFile(List<DepartmentGroupListDto> departments);
    }
}
﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.Departments.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.Master.Departments.Exporting
{
    public class DepartmentsExcelExporter : NpoiExcelExporterBase, IDepartmentsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public DepartmentsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<DepartmentDto> departments)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;
            
            return CreateExcelPackage(
                "Departments.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Departments"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        (L("PriceTag")) + L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, departments,
                        _ => _.Name,
                        _ => _.PriceTagName,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateTimeSetting)
                        );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public async Task<FileDto> ExportToFile(List<DepartmentGroupListDto> departments)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;

            return CreateExcelPackage(
                "DepartmentGroup.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DepartmentGroup"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name")
                    );

                    AddObjects(
                        sheet, 2, departments,
                        _ => _.Id,
                        _ => _.Code,
                        _ => _.Name
                    );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Companies.Exporting
{
    public class BrandExcelExporter : NpoiExcelExporterBase, IBrandExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BrandExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<BrandDto> items)
        {
            return CreateExcelPackage(
                "Brands.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Brands"));

                    AddHeader(
                        sheet,
                        L("Code"),
                        L("Name"),
                        L("Address1"),
                        L("Address2"),
                        L("Address3"),
                        L("Country"),
                        L("State"),
                        L("City"),
                        L("PhoneNumber"),
                        L("S.No"),
                        L("FaxNumber"),
                        L("GovernmentApprovalCode"),
                        L("UpdatedDate")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.Address?.Address1,
                        _ => _.Address?.Address2,
                        _ => _.Address?.Address3,
                        _ => _.Address?.CountryName,
                        _ => _.Address?.StateName,
                        _ => _.Address?.CityName,
                        _ => _.PhoneNumber,
                        _ => _.UserSerialNumber,
                        _ => _.FaxNumber,
                        _ => _.GovtApprovalId,
                        _ => _.LastModificationTime
                        );

                    for (var i = 1; i <= 12; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
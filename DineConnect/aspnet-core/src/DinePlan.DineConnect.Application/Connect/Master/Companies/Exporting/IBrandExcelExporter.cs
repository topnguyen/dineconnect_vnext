﻿using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Companies.Exporting
{
    public interface IBrandExcelExporter
    {
        FileDto ExportToFile(List<BrandDto> items);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Companies.Exporting;
using DinePlan.DineConnect.CustomGridFilter;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    public class CompanyAppService : DineConnectAppServiceBase, ICompanyAppService
    {
        private readonly ICompanyManager _companyManager;
        private readonly ConnectSession _connectSession;
        private readonly IAddressesManager _addressManager;
        private readonly ISettingManager _settingManager;
        private readonly IRepository<Brand> _organizationRepository;
        private readonly IBrandExcelExporter _brandExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CompanyAppService(ICompanyManager companyManager,
            ConnectSession connectSession,
            IAddressesManager addressManager,
            ISettingManager settingManager,
            IRepository<Brand> organizationRepository,
            IBrandExcelExporter brandExcelExporter,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _companyManager = companyManager;
            _connectSession = connectSession;
            _addressManager = addressManager;
            _settingManager = settingManager;
            _organizationRepository = organizationRepository;
            _brandExcelExporter = brandExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<BrandDto>> GetAll(GetCompanyInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var sortedBrand = new List<Brand>();
                var organization = _organizationRepository.GetAll()
                    .Include(a => a.Address)
                    .ThenInclude(c => c.City)
                    .ThenInclude(s => s.State)
                    .Include(a => a.Address.City)
                    .ThenInclude(co => co.Country)
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), i => EF.Functions.Like(i.Name, input.Filter.ToILikeString()));

                organization = organization.GetGenaricFilterQuery(input.CustomFilters);
                sortedBrand = await organization
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

                var organizationDtos = sortedBrand.Select(o =>
                {
                    var dto = ObjectMapper.Map<BrandDto>(o);

                    dto.Address = ObjectMapper.Map<AddressDto>(o.Address);
                    return dto;
                }).ToList();

                return new PagedResultDto<BrandDto>(
                   await organization.CountAsync(),
                     organizationDtos
                     );
            }
        }

        public async Task<BrandDto> GetCompanyForEdit(int? id)
        {
            var editDto = new BrandDto();
            if (id.HasValue)
            {
                var company = await _companyManager.GetAsync(id.Value);
                editDto = ObjectMapper.Map<BrandDto>(company);
                editDto.Address = ObjectMapper.Map<AddressDto>(company.Address);
            }

            return editDto;
        }

        
        [UnitOfWork]
        public async Task CreateOrUpdateAsync(BrandDto input)
        {
            ValidateForCreateOrUpdate(input);

            var organization = ObjectMapper.Map<Brand>(input);
            input.TenantId = AbpSession.TenantId ?? 0;
            organization.Locations = null;

            var address = organization.Address;
            address.TenantId = AbpSession.TenantId ?? 0;
            address.City = null;
            if (address.CityId > 0)
            {
                organization.AddressId = await _addressManager.CreateorUpdateAddressAsync(address);
            }
            else
            {
                organization.AddressId = null;
            }

            organization.Address = null;

            if (input.Id > 0)
            {
                await Update(organization);
            }
            else await Create(organization);
        }

        
        public async Task DeleteAsync(int id)
        {
            try
            {
                var users = await UserManager.Users.ToListAsync();

                foreach (var item in users)
                {
                    bool result = Int32.TryParse(await _settingManager.GetSettingValueForUserAsync("OrganizationId", _connectSession.TenantId, item.Id), out int userDefaultBrandId);
                    if (result)
                    {
                        if (userDefaultBrandId == id)
                        {
                            throw new UserFriendlyException("Can not delete, this is the default organization for some user");
                        }
                    }
                }
                await _companyManager.DeleteAsync(id);
            }
            catch (DbUpdateException ex)
            {
                HandleException(ex);
            }
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _organizationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _organizationRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetBrandsToExcel()
        {
            var filteredDepartments = await _organizationRepository.GetAll()
                .Include(a => a.Address)
                .ThenInclude(c => c.City)
                .ThenInclude(s => s.State)
                .Include(a => a.Address.City)
                .ThenInclude(co => co.Country).ToListAsync();

            var listDtos = filteredDepartments.Select(o =>
            {
                var dto = ObjectMapper.Map<BrandDto>(o);

                dto.Address = ObjectMapper.Map<AddressDto>(o.Address);
                return dto;
            }).ToList();

            return _brandExcelExporter.ExportToFile(listDtos);
        }

        private void HandleException(DbUpdateException ex)
        {
            if (ex.Message.Contains("REFERENCE constraint"))
                throw new UserFriendlyException("Can not delete, delete related information and try again");

            if (ex.InnerException != null)
            {
                if (ex.InnerException.Message.Contains("REFERENCE constraint"))
                    throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            throw new UserFriendlyException("Something went wrong");
        }

        
        protected virtual async Task Create(Brand input)
        {
            if (AbpSession.TenantId != null)
            {
                input.TenantId = AbpSession.TenantId.Value;
            }

            await _organizationRepository.InsertAsync(input);
        }

        
        protected virtual async Task Update(Brand input)
        {
            var organization = await _organizationRepository.GetAsync(input.Id);
            organization.Name = input.Name;
            organization.Code = input.Code;
            organization.PhoneNumber = input.PhoneNumber;
            organization.FaxNumber = input.FaxNumber;
            organization.UserSerialNumber = input.UserSerialNumber;
            organization.GovtApprovalId = input.GovtApprovalId;
            organization.AddressId = input.AddressId;

            await _organizationRepository.UpdateAsync(organization);
        }

        protected virtual void ValidateForCreateOrUpdate(BrandDto input)
        {
            var exists = _organizationRepository.GetAll().WhereIf(input.Id > 0, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
            if (exists.Any(o => o.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }
        }
    }
}
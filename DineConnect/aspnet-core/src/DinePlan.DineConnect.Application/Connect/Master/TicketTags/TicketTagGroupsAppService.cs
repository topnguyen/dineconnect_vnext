﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.TicketTags.Dtos;
using DinePlan.DineConnect.Connect.Master.TicketTags.Exporting;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.TicketTags
{
    
    public class TicketTagGroupsAppService : DineConnectAppServiceBase, ITicketTagGroupsAppService
    {
        private readonly IRepository<TicketTagGroup> _ticketTagGroupRepository;
        private readonly IRepository<TicketTag> _ticketTagRepository;
        private readonly ITicketTagGroupsExcelExporter _ticketTagGroupsExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<Department> _departmentRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TicketTagGroupsAppService(IRepository<TicketTagGroup> ticketTagGroupRepository
            , IRepository<TicketTag> ticketTagRepository
            , ITicketTagGroupsExcelExporter ticketTagGroupsExcelExporter
            , ConnectSession connectSession
            , IRepository<Department> departmentRepository
            , ILocationAppService locationAppService
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _ticketTagGroupRepository = ticketTagGroupRepository;
            _ticketTagRepository = ticketTagRepository;
            _ticketTagGroupsExcelExporter = ticketTagGroupsExcelExporter;
            _connectSession = connectSession;
            _departmentRepository = departmentRepository;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<TicketTagGroupDto>> GetAll(GetAllTicketTagGroupsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredTicketTagGroups = _ticketTagGroupRepository.GetAll()
                        .Where(x => x.IsDeleted == input.IsDeleted)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredTicketTagGroups = FilterByLocations(filteredTicketTagGroups, input);
                }

                var pagedAndFilteredTicketTagGroups = filteredTicketTagGroups
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input);

                var ticketTagGroups = ObjectMapper.Map<List<TicketTagGroupDto>>(pagedAndFilteredTicketTagGroups);

                foreach (var item in ticketTagGroups)
                {
                    if (!item.Departments.IsNullOrEmpty())
                    {
                        try
                        {
                            var departments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(item.Departments);
                            var names = departments.Select(e => e.DisplayText).ToList();
                            item.DepartmentNames = names.JoinAsString(", ");
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                var totalCount = await filteredTicketTagGroups.CountAsync();

                return new PagedResultDto<TicketTagGroupDto>(
                    totalCount,
                    ticketTagGroups
                );
            }
        }

        
        public async Task<CreateOrEditTicketTagGroupDto> GetTicketTagGroupForEdit(EntityDto input)
        {
            var ticketTagGroup = await _ticketTagGroupRepository.GetAllIncluding(e => e.Tags).FirstOrDefaultAsync(e => e.Id == input.Id);

            var output = ObjectMapper.Map<CreateOrEditTicketTagGroupDto>(ticketTagGroup);

            GetLocationsForEdit(output, output.LocationGroup);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTicketTagGroupDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTicketTagGroupDto input)
        {
            var ticketTagGroup = ObjectMapper.Map<TicketTagGroup>(input);
            ticketTagGroup.LastModificationTime = Clock.Now;
            if (AbpSession.TenantId != null)
            {
                ticketTagGroup.TenantId = (int)AbpSession.TenantId;
            }

            foreach (var otdto in input.Tags)
            {
                if (ticketTagGroup.Tags.Any(a => a.TicketTagGroupId != otdto.Id))
                {
                    var obj = ObjectMapper.Map<TicketTag>(otdto);
                    ticketTagGroup.Tags.Add(obj);
                }
            }
            UpdateLocations(ticketTagGroup, input.LocationGroup);

            await _ticketTagGroupRepository.InsertAsync(ticketTagGroup);
        }

        
        protected virtual async Task Update(CreateOrEditTicketTagGroupDto input)
        {
            var ticketTagGroup = await _ticketTagGroupRepository.FirstOrDefaultAsync((int)input.Id);

            var toRemoveTags = _ticketTagRepository.GetAll().Where(a => a.TicketTagGroupId == input.Id && !a.Id.Equals(0) && !input.Tags.Select(u => u.Id).Contains(a.Id)).ToList();
            toRemoveTags.ForEach(e => _ticketTagRepository.DeleteAsync(e));

            ObjectMapper.Map(input, ticketTagGroup);
            UpdateLocations(ticketTagGroup, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _ticketTagGroupRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _ticketTagGroupRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _ticketTagGroupRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetTicketTagGroupsToExcel(GetAllTicketTagGroupsForExcelInput input)
        {
            var filteredTicketTagGroups = await _ticketTagGroupRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()))
                        .ToListAsync();

            var ticketTagGroupListDtos = ObjectMapper.Map<List<TicketTagGroupDto>>(filteredTicketTagGroups);

            foreach (var item in ticketTagGroupListDtos)
            {
                if (!item.Departments.IsNullOrEmpty())
                {
                    try
                    {
                        var departments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(item.Departments);
                        var names = departments.Select(e => e.DisplayText).ToList();
                        item.DepartmentNames = names.JoinAsString(", ");
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return _ticketTagGroupsExcelExporter.ExportToFile(ticketTagGroupListDtos);
        }

        public ListResultDto<ComboboxItemDto> GetTicketTagGroupTypeForLookupTable()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var name in Enum.GetNames(typeof(TicketTagGroupType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetDepartmentForCombobox()
        {
            var returnList = await _departmentRepository.GetAll()
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditTicketTagGroupDto input)
        {
            var exists = _ticketTagGroupRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<ListResultDto<TicketTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var allItems = _ticketTagGroupRepository.GetAll().Include(a => a.Tags).
                Where(a => a.TenantId == locationInput.TenantId);

            var allListDtos = ObjectMapper.Map<List<TicketTagGroupEditDto>>(allItems);
            var returnDto = new List<TicketTagGroupEditDto>();

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var ticketTagList in allListDtos)
                        if (!ticketTagList.Group && !string.IsNullOrEmpty(ticketTagList.Locations) &&
                            !ticketTagList.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(ticketTagList.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        returnDto.Add(ticketTagList);
                        }
                        else
                        {
                            returnDto.Add(ticketTagList);
                        }
            }
            else if (locationInput.LocationId > 0)
            {
                foreach (var myDto in allListDtos)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = myDto.Locations,
                        Group = myDto.Group,
                        NonLocations = myDto.NonLocations,
                        LocationTag = myDto.LocationTag
                    }))
                    {
                        returnDto.Add(myDto);
                    }
                }
            }
            else
            {
                returnDto = allListDtos;
            }
            return new ListResultDto<TicketTagGroupEditDto>(returnDto);
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Master.TicketTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Exporting
{
    public interface ITicketTagGroupsExcelExporter
    {
        FileDto ExportToFile(List<TicketTagGroupDto> ticketTagGroups);
    }
}
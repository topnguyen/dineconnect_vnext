﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.TicketTags.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Exporting
{
    public class TicketTagGroupsExcelExporter : NpoiExcelExporterBase, ITicketTagGroupsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TicketTagGroupsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<TicketTagGroupDto> ticketTagGroups)
        {
            return CreateExcelPackage(
                "TicketTagGroups.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TicketTagGroups"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DataType"),
                        L("SaveFreeTags"),
                        L("ForceValue"),
                        L("Departments"),
                        L("Locations"),
                        L("UpdatedDate")
                        );

                    AddObjects(
                        sheet, 2, ticketTagGroups,
                        _ => _.Name,
                        _ => _.DataTypeName,
                        _ => _.SaveFreeTags,
                        _ => _.ForceValue,
                        _ => _.DepartmentNames,
                        _ => _.LocationNames,
                        _ => _timeZoneConverter.Convert(_.LastModificationTime, _abpSession.TenantId, _abpSession.GetUserId())

                        );

                    for (var i = 1; i <= 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
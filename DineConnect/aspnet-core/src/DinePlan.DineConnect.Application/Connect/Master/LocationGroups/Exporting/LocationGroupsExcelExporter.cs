﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups.Exporting
{
    public class LocationGroupsExcelExporter : NpoiExcelExporterBase, ILocationGroupsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LocationGroupsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GroupDto> locationGroups)
        {
            return CreateExcelPackage(
                "LocationGroups.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LocationGroups"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Code")
                        );

                    AddObjects(
                        sheet, 2, locationGroups,
                        _ => _.Name,
                        _ => _.Code
                        );
                });
        }
    }
}
﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups.Exporting
{
    public interface ILocationGroupsExcelExporter
    {
        FileDto ExportToFile(List<GroupDto> locationGroups);
    }
}
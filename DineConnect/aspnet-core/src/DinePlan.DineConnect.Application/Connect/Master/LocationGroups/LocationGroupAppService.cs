﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.LocationGroups.Exporting;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups
{
    public class LocationGroupAppService : DineConnectAppServiceBase, ILocationGroupAppService
    {
        private readonly ILocationGroupManager _locationGroupManager;
        private readonly ConnectSession _connectSession;
        private readonly ILocationGroupsExcelExporter _locationGroupsExcelExporter;

        public LocationGroupAppService(ILocationGroupManager locationGroupManager, ConnectSession connectSession, ILocationGroupsExcelExporter locationGroupsExcelExporter)
        {
            _locationGroupManager = locationGroupManager;
            _connectSession = connectSession;
            _locationGroupsExcelExporter = locationGroupsExcelExporter;
        }

        public async Task AddLocationToGroup(AddLocationGroupInput locationGroup)
        {
            foreach (var item in locationGroup.Locations)
            {
                await _locationGroupManager.AddLocationToGroup(new LocationGroup
                {
                    LocationId = item,
                    TenantId = _connectSession.TenantId ?? 0,
                    GroupId = locationGroup.GroupId,
                    Group = null,
                    Location = null
                });
            }
        }

        public async Task CreateOrUpdateGroup(GroupDto group)
        {
            await _locationGroupManager.CreateOrUpdateGroup(new Groups.Group
            {
                TenantId = AbpSession.TenantId ?? 0,
                Name = group.Name,
                Code = group.Code,
                Id = group.Id
            });
        }

        public async Task DeleteGroup(int groupId)
        {
            await _locationGroupManager.DeleteGroup(groupId);
        }

        public async Task<PagedResultDto<GroupDto>> GetAllGroup(GetGroupInput input)
        {
            var group = _locationGroupManager.GetAllGroup(input.Filter);

            var sortedGroup = await group
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            var groupDtoList = sortedGroup.Select(g => new GroupDto
            {
                Id = g.Id,
                Name = g.Name,
                Code = g.Code
            }).ToList();

            return new PagedResultDto<GroupDto>(await group.CountAsync(), groupDtoList);
        }

        public async Task<PagedResultDto<LocationGroupDto>> GetAllLocationGroup(GetLocationsOfGroupInput input)
        {
            var locationGroup = _locationGroupManager.GetAllLocationGroup(input.GroupId);

            var locationGroupDto = locationGroup.Select(lg => new LocationGroupDto
            {
                Id = lg.Id,
                Code = lg.Location.Code,
                Name = lg.Location.Name
            }).AsQueryable();

            var sortedlocationGrroup = await locationGroupDto
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            return new PagedResultDto<LocationGroupDto>(await locationGroupDto.CountAsync(), sortedlocationGrroup);
        }

        public async Task<GroupDto> GetGroupAsync(int groupId)
        {
            var group = await _locationGroupManager.GetGroupAsync(groupId);

            return new GroupDto
            {
                Id = group.Id,
                Name = group.Name,
                Code = group.Code
            };
        }

        public async Task<PagedResultDto<LocationDto>> GetLocationNotInGroup(GetLocationNotInGroupInput input)
        {
            var location = _locationGroupManager.GetLocationNotInGroup(input.Filter, input.OrganizationId, input.GroupId);

            var sortedlocation = await location
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToListAsync();

            var locationDtoList = ObjectMapper.Map<List<LocationDto>>(sortedlocation);

            return new PagedResultDto<LocationDto>(await location.CountAsync(), locationDtoList);
        }

        public async Task RemoveLocationFromGroup(int locationGroupId)
        {
            await _locationGroupManager.RemoveLocationFromGroup(locationGroupId);
        }

        public async Task<PagedResultDto<IdNameDto>> GetSimpleLocationGroups(GetGroupInput input)
        {
            var group = _locationGroupManager.GetAllGroup(input.Filter);

            var sortedGroup = await group
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            var groupDtoList = sortedGroup.Select(g => new IdNameDto
            {
                Id = g.Id,
                Name = g.Name,
            }).ToList();

            return new PagedResultDto<IdNameDto>(await group.CountAsync(), groupDtoList);
        }

        public async Task<FileDto> GetLocationGroupsToExcel(GetGroupInput input)
        {
            var locationGroups = await GetAllGroup(input);

            var items = locationGroups.Items.ToList();

            return _locationGroupsExcelExporter.ExportToFile(items);
        }
    }
}
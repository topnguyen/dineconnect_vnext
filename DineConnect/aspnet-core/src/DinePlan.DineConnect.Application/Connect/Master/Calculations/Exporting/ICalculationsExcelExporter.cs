﻿using DinePlan.DineConnect.Connect.Master.Calculations.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Exporting
{
    public interface ICalculationsExcelExporter
    {
        FileDto ExportToFile(List<CalculationDto> calculations);
    }
}
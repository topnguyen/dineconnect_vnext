﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.Calculations.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Exporting
{
    public class CalculationsExcelExporter : NpoiExcelExporterBase, ICalculationsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CalculationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<CalculationDto> calculations)
        {
            return CreateExcelPackage(
                "Calculations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Calculations"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("CalculationType"),
                        L("TransactionType"),
                        L("Amount"),
                        L("IncludeTax"),
                        L("DecreaseAmount"),
                        L("Departments")
                        );

                    AddObjects(
                        sheet, 2, calculations,
                        _ => _.Name,
                        _ => _.CalculationTypeName,
                        _ => _.TransactionTypeName,
                        _ => _.Amount,
                        _ => _.IncludeTax,
                        _ => _.DecreaseAmount,
                        _ => _.DepartmentNames
                        );

                    for (var i = 1; i <= 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
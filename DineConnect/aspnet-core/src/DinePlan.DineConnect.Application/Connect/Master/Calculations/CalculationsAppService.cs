﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Calculations.Dtos;
using DinePlan.DineConnect.Connect.Master.Calculations.Exporting;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.Calculations
{
    
    public class CalculationsAppService : DineConnectAppServiceBase, ICalculationsAppService
    {
        private readonly IRepository<Calculation> _calculationRepository;
        private readonly ICalculationsExcelExporter _calculationsExcelExporter;
        private readonly IRepository<TransactionType, int> _transactionTypeRepository;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<Department> _departmentRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locationAppService;

        public CalculationsAppService(
            IRepository<Calculation> calculationRepository,
            ICalculationsExcelExporter calculationsExcelExporter,
            IRepository<TransactionType> transactionTypeRepository,
            ConnectSession connectSession,
            IRepository<Department> departmentRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ILocationAppService locationAppService)
        {
            _calculationRepository = calculationRepository;
            _calculationsExcelExporter = calculationsExcelExporter;
            _transactionTypeRepository = transactionTypeRepository;
            _connectSession = connectSession;
            _departmentRepository = departmentRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _locationAppService = locationAppService;
        }

        public async Task<PagedResultDto<CalculationDto>> GetAll(GetAllCalculationsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredCalculations = _calculationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted)
                        .Include(e => e.TransactionType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.ButtonHeader, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionType != null && e.TransactionType.Name == input.TransactionTypeNameFilter);

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredCalculations = FilterByLocations(filteredCalculations, input);
                }

                var pagedAndFilteredCalculations = await filteredCalculations
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var calculations = ObjectMapper.Map<List<CalculationDto>>(pagedAndFilteredCalculations);

                var totalCount = await filteredCalculations.CountAsync();

                return new PagedResultDto<CalculationDto>(totalCount, calculations);
            }
        }

        
        public async Task<CreateOrEditCalculationDto> GetCalculationForEdit(EntityDto input)
        {
            var calculation = await _calculationRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditCalculationDto>(calculation);
            GetLocationsForEdit(output, output.LocationGroup);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditCalculationDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditCalculationDto input)
        {
            var calculation = ObjectMapper.Map<Calculation>(input);

            if (AbpSession.TenantId != null)
            {
                calculation.TenantId = (int)AbpSession.TenantId;
            }
            UpdateLocations(calculation, input.LocationGroup);

            await _calculationRepository.InsertAsync(calculation);
        }

        
        protected virtual async Task Update(CreateOrEditCalculationDto input)
        {
            var calculation = await _calculationRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, calculation);
            UpdateLocations(calculation, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _calculationRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _calculationRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _calculationRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetCalculationsToExcel(GetAllCalculationsForExcelInput input)
        {
            var filteredCalculations = await _calculationRepository.GetAll()
                        .Include(e => e.TransactionType)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.ButtonHeader, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()))
                        .ToListAsync();

            var calculationListDtos = ObjectMapper.Map<List<CalculationDto>>(filteredCalculations);

            calculationListDtos.ForEach(c =>
            {
                var departmentIds = c.Departments.Split(",");
                var departments = _departmentRepository.GetAll().Where(d => departmentIds.Contains(d.Id.ToString())).Select(d => d.Name).ToList();

                c.DepartmentNames = departments.JoinAsString(", ");
            });

            return _calculationsExcelExporter.ExportToFile(calculationListDtos);
        }

        
        public async Task<ListResultDto<ComboboxItemDto>> GetAllTransactionTypeForLookupTable(string input)
        {
            var result = await _transactionTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input),
                  e => EF.Functions.Like(e.Name, input.ToILikeString()))
                .Select(g => new ComboboxItemDto(g.Id.ToString(), g.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(result);
        }

        
        public ListResultDto<ComboboxItemDto> GetCalculationTypeForLookupTable(string input)
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var name in Enum.GetNames(typeof(CalculationType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        
        public ListResultDto<ComboboxItemDto> GetConfirmationTypeForLookupTable(string input)
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var name in Enum.GetNames(typeof(ConfirmationType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetDepartmentForCombobox()
        {
            var returnList = await _departmentRepository.GetAll()
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditCalculationDto input)
        {
            var exists = _calculationRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<ApiCalculationOutput> ApiGetCalculations(ApiLocationInput locationInput)
        {
            ApiCalculationOutput output = new ApiCalculationOutput();

            try
            {
                var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
                }

                var allItems = await _calculationRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

                var returnDto = new List<ApiCalculation>();
                if (locationInput.LocationId > 0)
                {
                    foreach (var myDto in allItems)
                    {
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = locationInput.LocationId,
                            Locations = myDto.Locations,
                            Group = myDto.Group,
                            NonLocations = myDto.NonLocations,
                            LocationTag = myDto.LocationTag,
                        }))
                        {
                            returnDto.Add(ObjectMapper.Map<ApiCalculation>(myDto));
                        }
                    }
                }
                else
                {
                    returnDto = ObjectMapper.Map<List<ApiCalculation>>(allItems);
                }
                output.Calculations = returnDto;
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }
            return output;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos;
using DinePlan.DineConnect.Connect.Master.TillAccounts.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts
{
    
    public class TillAccountsAppService : DineConnectAppServiceBase, ITillAccountsAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<TillAccount> _tillAccountRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly ITillAccountsExcelExporter _tillAccountsExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<TillTransaction> _transManager;
        private readonly ILogger _logger;

        public TillAccountsAppService(IRepository<TillAccount> tillAccountRepository,
            ILogger logger,
            IRepository<Location> locationRepository,
            IRepository<TillTransaction> transManager,
            ITillAccountsExcelExporter tillAccountsExcelExporter, ConnectSession connectSession,
            IUnitOfWorkManager unitOfWorkManager, ILocationAppService locationAppService)
        {
            _tillAccountRepository = tillAccountRepository;
            _tillAccountsExcelExporter = tillAccountsExcelExporter;
            _connectSession = connectSession;
            _unitOfWorkManager = unitOfWorkManager;
            _locationAppService = locationAppService;
            _transManager = transManager;
            _locationRepository = locationRepository;
            _logger = logger;

        }

        public async Task<PagedResultDto<TillAccountDto>> GetAll(GetAllTillAccountsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredTillAccounts = _tillAccountRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(input.TillAccountTypeId.HasValue, e => e.TillAccountTypeId == input.TillAccountTypeId)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        e => false || EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                    filteredTillAccounts = FilterByLocations(filteredTillAccounts, input);

                var pagedAndFilteredTillAccounts = await filteredTillAccounts
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var tillAccounts = ObjectMapper.Map<List<TillAccountDto>>(pagedAndFilteredTillAccounts);

                var totalCount = await filteredTillAccounts.CountAsync();

                return new PagedResultDto<TillAccountDto>(
                    totalCount,
                    tillAccounts
                );
            }
        }
        public async Task<FileDto> BuildTillTransactionReportExcel(GetTillAccountInput input)
        {  
            return await _tillAccountsExcelExporter.ExportToFileTillTransaction(input, this);
        }
        
        public async Task<PagedResultDto<TillTransactionListDto>> BuildTillTransactionReport(GetTillAccountInput input)
        {
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue) && !input.NotCorrectDate)
            {
                input.EndDate = input.EndDate.AddDays(1);
            }
            IQueryable<TillTransaction> trans = _transManager.GetAllIncluding().Include(s => s.TillAccount)
                .WhereIf(!input.UserName.IsNullOrEmpty(), a => a.User.Contains(input.UserName));

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                trans = trans.Where(a => a.TransactionTime >= input.StartDate && a.TransactionTime <= input.EndDate);
            }

            var locations = new List<int>();
            if (input.LocationGroup.Locations != null && input.LocationGroup.Locations.Any())
            {
                locations = input.LocationGroup.Locations.Select(s=>s.Id).ToList();
            }
            if (locations.Any())
            {
                trans = trans.Where(a => locations.Contains(a.LocationId));
            }

            var accounts = new List<int>();
            if (input.Accounts != null && input.Accounts.Any())
            {
                accounts = input.Accounts.ToList();
            }
            if (accounts.Any())
            {
                trans = trans.Where(a => accounts.Contains(a.TillAccountId));
            }

            if (input.AccountType > 0)
            {
                trans = trans.Where(a => a.TillAccount.TillAccountTypeId == input.AccountType);
            }

            // filter by dynamic filter
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    trans = trans.BuildQuery(filRule);
                }
            }


            List<TillTransactionListDto> outputDtos = null;

            var sortTickets = await trans.OrderBy(input.Sorting)
                  .PageBy(input)
                  .ToListAsync();
            var result = new List<TillTransactionListDto>();
            sortTickets.ForEach(t => result.Add(new TillTransactionListDto
            {
                TillAccountId = t.TillAccountId,
                LocationId = t.LocationId,
                LocationName = _locationRepository.FirstOrDefault(s=>s.Id == t.LocationId)?.Name,
                TillAccountName = t.TillAccount.Name,
                TotalAmount = t.TotalAmount,
                Status = t.Status,
                User = t.User,
                Remarks = t.Remarks,
                TransactionTime = t.TransactionTime,               
            }));
            outputDtos = result;
            var menuItemCount = await trans.CountAsync();
            return new PagedResultDto<TillTransactionListDto>(
                menuItemCount,
                outputDtos
                );
        }

        
        public async Task<CreateOrEditTillAccountDto> GetTillAccountForEdit(EntityDto input)
        {
            var tillAccount = await _tillAccountRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTillAccountDto>(tillAccount);

            GetLocationsForEdit(output, output.LocationGroup);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTillAccountDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _tillAccountRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tillAccountRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _tillAccountRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetTillAccountsToExcel(GetAllTillAccountsForExcelInput input)
        {
            var filteredTillAccounts = await _tillAccountRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    e => false || EF.Functions.Like(e.Name, input.Filter.ToILikeString()))
                .ToListAsync();

            var tillAccountListDtos = ObjectMapper.Map<List<TillAccountDto>>(filteredTillAccounts);

            return _tillAccountsExcelExporter.ExportToFile(tillAccountListDtos);
        }

        public ListResultDto<ComboboxItemDto> GetAccountTypeForLookupTable()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var name in Enum.GetNames(typeof(TillAccounTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ApiTillAccountOutput>> ApiGetAll(ApiLocationInput input)
        {
            var tillAccounts = await _tillAccountRepository.GetAllListAsync(a => a.TenantId == input.TenantId);

            var apiOuputs = new List<ApiTillAccountOutput>();
            if (input.LocationId > 0)
            {
                foreach (var myaccount in tillAccounts)
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = myaccount.Locations,
                        Group = myaccount.Group,
                        LocationTag = myaccount.LocationTag,
                        NonLocations = myaccount.NonLocations
                    }))
                        apiOuputs.Add(new ApiTillAccountOutput
                        {
                            Id = myaccount.Id,
                            Name = myaccount.Name,
                            TillAccountTypeId = myaccount.TillAccountTypeId
                        });
            }
            else
            {
                apiOuputs = tillAccounts.Select(dept => new ApiTillAccountOutput
                {
                    Id = dept.Id,
                    Name = dept.Name,
                    TillAccountTypeId = dept.TillAccountTypeId
                }).ToList();
            }

            return new ListResultDto<ApiTillAccountOutput>(apiOuputs);
        }

        public async Task<CreateOrUpdateTillOutput> CreateOrUpdateTillTrans(CreateOrUpdateTillInput input)
        {
            var output = new CreateOrUpdateTillOutput();
            try
            {
                var updateTrans = false;
                TillTransaction trans = null;
                if (input.Transaction.Id.HasValue && input.Transaction.Id > 0)
                {
                    updateTrans = true;
                    trans = await _transManager.GetAsync(input.Transaction.Id.Value);
                }
                else
                {
                    trans =
                        await
                            _transManager.FirstOrDefaultAsync(
                                a =>
                                    a.LocalId == input.Transaction.LocalId &&
                                    a.LocationId == input.Transaction.LocationId);

                    if (trans != null) updateTrans = true;
                }

                if (trans == null)
                    trans = new TillTransaction();

                trans.TillAccountId = input.Transaction.AccountId;
                trans.TotalAmount = input.Transaction.TotalAmount;
                trans.TransactionTime = input.Transaction.TransactionTime;
                trans.WorkPeriodId = input.Transaction.WorkPeriodId;
                trans.LocalWorkPeriodId = input.Transaction.LocalWorkPeriodId;
                trans.LocationId = input.Transaction.LocationId;
                trans.User = input.Transaction.User;
                trans.Status = input.Transaction.Status;
                trans.LocalId = input.Transaction.LocalId;
                trans.Remarks = input.Transaction.Remarks;

                var id = 0;
                if (updateTrans)
                    id = trans.Id;
                else
                    id = await _transManager.InsertAndGetIdAsync(trans);
                output.Trans = id;
            }
            catch (Exception exception)
            {
                _logger.Fatal("Till Transaction Sync Failed");
                _logger.Fatal("-------------------");
                _logger.Fatal("Location : " + input.Transaction.LocationId);
                _logger.Fatal("Till Transaction Id : " + input.Transaction.LocalId);
                _logger.Fatal(exception.StackTrace);
                output.Trans = 0;
            }

            return output;
        }

        
        protected virtual async Task Create(CreateOrEditTillAccountDto input)
        {
            var tillAccount = ObjectMapper.Map<TillAccount>(input);

            if (AbpSession.TenantId != null) tillAccount.TenantId = (int) AbpSession.TenantId;

            UpdateLocations(tillAccount, input.LocationGroup);
            await _tillAccountRepository.InsertAsync(tillAccount);
        }

        
        protected virtual async Task Update(CreateOrEditTillAccountDto input)
        {
            var tillAccount = await _tillAccountRepository.FirstOrDefaultAsync((int) input.Id);
            ObjectMapper.Map(input, tillAccount);
            UpdateLocations(tillAccount, input.LocationGroup);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditTillAccountDto input)
        {
            var exists = _tillAccountRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name))) throw new UserFriendlyException(L("NameAlreadyExists"));
        }

        public virtual async Task<List<TillAccountListDto>> GetTillAccounts()
        {
            var lst = await _tillAccountRepository.GetAllListAsync();
            var allListDtos = new List<TillAccountListDto>();
            lst.ForEach(x => allListDtos.Add(new TillAccountListDto 
            { 
                TillAccountTypeId = x.TillAccountTypeId,
                Name = x.Name,
                Locations = x.Locations,
                Id = x.Id,
            }));
            return allListDtos;
        }

    }
}
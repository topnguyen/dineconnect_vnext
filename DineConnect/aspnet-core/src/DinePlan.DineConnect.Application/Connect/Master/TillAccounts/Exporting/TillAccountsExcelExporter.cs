﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Exporting
{
    public class TillAccountsExcelExporter : NpoiExcelExporterBase, ITillAccountsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        public TillAccountsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) :
    base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<TillAccountDto> tillAccounts)
        {
            return CreateExcelPackage(
                "TillAccounts.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TillAccounts"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("TillAccountType"),
                        L("CreationTime")

                        );

                    AddObjects(
                        sheet, 2, tillAccounts,
                        _ => _.Name,
                        _ => _.TillAccountType,
                        _ => _.CreationTime.ToString("dd/MM/yyyy")
                        );

                    for (var i = 1; i < 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public async Task<FileDto> ExportToFileTillTransaction(GetTillAccountInput input, TillAccountsAppService tillAccountAppService)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            input.MaxResultCount = 1000;

            var output = await tillAccountAppService.BuildTillTransactionReport(input);
            var fileName = input.ExportOutputType == ExportType.Excel ? "TillTransaction.xlsx" : "TillTransaction.html";         
            return await ExportTillTransaction(fileName, output, input);
        }
        private Task<FileDto> ExportTillTransaction(string fileName, PagedResultDto<TillTransactionListDto> output, GetTillAccountInput input)
        {
            var headers = new List<string>
            {
                L("Location"),
                    L("Date"),
                    L("Account"),
                    L("Remarks"),
                    L("User"),
                    L("Amount")
            };
            var allTot = 0M;

            return Task.FromResult(CreateExcelPackage(
                fileName, excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("Till");

                    AddHeader(sheet, headers.ToArray());

                    var rowIndex = 2;

                    foreach (var order in output.Items)
                    {
                        allTot += order.TotalAmount;

                        var values = new List<object>
                               {
                                    order.LocationName,
                                    order.TransactionTime,
                                    order.TillAccountName,
                                    order.Remarks,
                                    order.User,
                                    order.TotalAmount,
                               };

                        // Add Row to excel
                        AddRow(excelPackage, sheet, rowIndex, values.ToArray());
                        // Next Row
                        rowIndex++;

                    }

                    // Total Row
                    sheet.GetSheetRow(rowIndex).GetSheetCell(0).SetCellValue("Total");
                    sheet.GetSheetRow(rowIndex).GetSheetCell(5).SetCellValue((double)allTot);

                    for (var i = 1; i <= headers.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    
                }, input.ExportOutputType));
        }
    }
}
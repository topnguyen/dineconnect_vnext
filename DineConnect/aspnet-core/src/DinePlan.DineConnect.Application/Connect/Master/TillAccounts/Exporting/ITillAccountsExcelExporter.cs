﻿using DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Exporting
{
    public interface ITillAccountsExcelExporter
    {
        FileDto ExportToFile(List<TillAccountDto> tillAccounts);
        Task<FileDto> ExportToFileTillTransaction(GetTillAccountInput input, TillAccountsAppService tillAccountAppService);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Transactions;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Logging;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Tickets.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Promotions;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketReports;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Core.QueryScheduler.Interfaces;
using DinePlan.DineConnect.Core.Report;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Helper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PromotionDetailValue = DinePlan.DineConnect.Connect.Transaction.PromotionDetailValue;

namespace DinePlan.DineConnect.Connect.Master.Tickets
{
    public class TicketAppService : DineConnectAppServiceBase, ITicketAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Location> _locRepo;

        private readonly ILogger _logger;
        private readonly IRepository<MenuItem> _miRepository;
        private readonly IRepository<Order> _orRepository;
        private readonly IRepository<Transaction.Payment> _paymentRepository;
        private readonly IRepository<PaymentTypes.PaymentType> _paymentTypeRepository;

        private readonly IPromotionsAppService _promotionAppService;
        private readonly SettingManager _settingManager;
        private readonly IRepository<TicketTransaction> _taRepository;
        private readonly ITicketReportAppService _ticketReportAppService;
        private readonly IRepository<Ticket> _ticketRepository;
        private readonly IRepository<TransactionType> _ticketTransactionRepository;

        private readonly IQuerySchedularService<string> _querySchedularService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConfigurationRoot _appConfiguration;

        public TicketAppService(IRepository<Ticket> ticketRepository,
            IRepository<Transaction.Payment> paRepository,
            IRepository<Location> locRepository,
            IRepository<TicketTransaction> taRepository,
            IRepository<Location> locationRepository,
            IRepository<Order> orRepository,
            IRepository<TransactionType> ticketTransactionRepository,
            FeatureChecker featureChecker,
            ILogger logger,
            SettingManager settingManager,
            ITicketReportAppService ticketReportAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MenuItem> miRepository,
            IBackgroundJobManager backgroundJobManager,
            IRepository<PaymentTypes.PaymentType> paymentTypeRepository,
            IPromotionsAppService promotionAppService,
            IQuerySchedularService<string> querySchedularService,
            IAppConfigurationAccessor configurationAccessor
        )
        {
            if (featureChecker == null) throw new ArgumentNullException(nameof(featureChecker));
            _ticketReportAppService = ticketReportAppService;
            _ticketTransactionRepository = ticketTransactionRepository;
            _ticketRepository = ticketRepository;
            _orRepository = orRepository;
            _paymentRepository = paRepository;
            _taRepository = taRepository;
            _locationRepository = locationRepository;
            _logger = logger;
            _settingManager = settingManager;
            _unitOfWorkManager = unitOfWorkManager;
            _bgm = backgroundJobManager;
            _paymentTypeRepository = paymentTypeRepository;
            _miRepository = miRepository;
            _promotionAppService = promotionAppService;
            _locRepo = locRepository;
            _querySchedularService = querySchedularService;
            _appConfiguration = configurationAccessor.Configuration;
        }

        public IAppFolders AppFolders { get; set; }

        public async Task<CreateOrUpdateTicketOutput> CreateOrUpdateTicket(CreateOrUpdateTicketInput input)
        {
            try
            {
                if (input == null || input.Ticket == null)
                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = 0
                    };
                if (input.Ticket.LocationId == 0 && input.Ticket.TenantId == 0)
                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = 0
                    };

                using (_unitOfWorkManager.Current.SetTenantId(input.Ticket.TenantId))
                {
                    if (input.Ticket.LocationId > 0 && input.Ticket.TenantId > 0)
                    {
                        var myLocation = await _locRepo.FirstOrDefaultAsync(a =>
                            a.TenantId == input.Ticket.TenantId && a.Id == input.Ticket.LocationId);
                        if (myLocation != null)
                        {
                            if (myLocation.RestrictSync)
                                return new CreateOrUpdateTicketOutput
                                {
                                    Ticket = 100
                                };
                        }
                        else
                        {
                            return new CreateOrUpdateTicketOutput
                            {
                                Ticket = 0
                            };
                        }
                    }

                    var updateTicket = false;
                    var list = 0;
                    if (input.Ticket.Id.HasValue && input.Ticket.Id > 0) updateTicket = true;

                    if (!string.IsNullOrEmpty(input.Ticket.TicketNumber))
                    {
                        var ticket =
                            await _ticketRepository.FirstOrDefaultAsync(
                                a =>
                                    a.TicketNumber.Equals(input.Ticket.TicketNumber) &&
                                    a.LocationId.Equals(input.Ticket.LocationId));

                        if (ticket != null)
                        {
                            input.Ticket.Id = ticket.Id;
                            updateTicket = true;
                        }
                        else
                        {
                            updateTicket = false;
                        }
                    }

                    if (input.Ticket != null)
                    {
                        var dName = input.Ticket.DepartmentName;
                        if (string.IsNullOrEmpty(dName))
                        {
                            input.Ticket.DepartmentName = "";
                            dName = "";
                        }

                        input.Ticket.LastPaymentTimeTruc = input.Ticket.LastPaymentTime.Date;

                        if (input.Ticket.Orders.Any())
                            foreach (var order in input.Ticket.Orders)
                            {
                                order.DepartmentName = dName;
                                order.Location_Id = input.Ticket.LocationId;

                                if (order.TTaxes != null && order.TTaxes.Any())
                                    order.Taxes = JsonConvert.SerializeObject(order.TTaxes);
                                if (order.OTags != null && order.OTags.Any())
                                    order.OrderTags = JsonConvert.SerializeObject(order.OTags);
                                if (order.OTags != null && order.OStates.Any())
                                    order.OrderStates = JsonConvert.SerializeObject(order.OStates);
                            }

                        if (input.Ticket.TStates != null && input.Ticket.TStates.Any())
                            input.Ticket.TicketStates = JsonConvert.SerializeObject(input.Ticket.TStates);
                    }

                    if (input.Ticket.TotalAmount == 0M)
                    {
                        foreach (var ticketTransactionDto in input.Ticket.Transactions)
                            ticketTransactionDto.Amount = 0M;

                        foreach (var paymentAmount in input.Ticket.Payments)
                            paymentAmount.Amount = 0M;
                    }

                    foreach (var ticketTransaction in input.Ticket.Transactions.Where(a => a.TransactionTypeId == 0))
                        if (!string.IsNullOrEmpty(ticketTransaction.TransactionTypeName))
                        {
                            var myTrans = _ticketTransactionRepository.GetAll().Where(a =>
                                a.Name.ToUpper().Equals(ticketTransaction.TransactionTypeName.ToUpper()) &&
                                a.TenantId == input.Ticket.TenantId).ToList();

                            if (myTrans.Any())
                                ticketTransaction.TransactionTypeId = myTrans.Last().Id;
                            else
                                throw new UserFriendlyException("Ticket Transaction Type is coming as Empty " +
                                                                input.Ticket.TicketNumber);
                        }
                        else
                        {
                            throw new UserFriendlyException("Ticket Transaction Type is coming as Empty " +
                                                            input.Ticket.TicketNumber);
                        }

                    foreach (var paymentTrans in input.Ticket.Payments)
                        if (paymentTrans.PaymentTypeId > 0)
                        {
                            var myTrans = _paymentTypeRepository.GetAll().Where(a =>
                                a.Id == paymentTrans.PaymentTypeId &&
                                a.TenantId == input.Ticket.TenantId).ToList();
                            if (!myTrans.Any())
                                paymentTrans.PaymentTypeId =
                                    (await _paymentTypeRepository.FirstOrDefaultAsync(a => a.Id > 0)).Id;
                        }
                        else if (!string.IsNullOrEmpty(paymentTrans.PaymentTypeName))
                        {
                            var myTrans = _paymentTypeRepository.GetAll().Where(a =>
                                a.Name.ToUpper().Equals(paymentTrans.PaymentTypeName.ToUpper()) &&
                                a.TenantId == input.Ticket.TenantId).ToList();

                            paymentTrans.PaymentTypeId = myTrans.Any()
                                ? myTrans.Last().Id
                                : (await _paymentTypeRepository.FirstOrDefaultAsync(a => a.Id > 0)).Id;
                        }
                        else
                        {
                            var errorMessage =
                                $"Payment Type Type is coming as Empty -> Ticket Number : {input.Ticket.TicketNumber} and Location Id : {input.Ticket.LocationId} and TenantId : {input.Ticket.TenantId}";
                            throw new UserFriendlyException(errorMessage);
                        }

                    if (updateTicket)
                    {
                        list = await UpdateTicketAsync(input.Ticket);
                    }
                    else
                    {
                        list = await CreateTicketAsync(input.Ticket);
                    }

                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = list
                    };
                }
            }
            catch (Exception exception)
            {
                _logger.Log(LogSeverity.Error, "TicketCreation", exception);
                return new CreateOrUpdateTicketOutput
                {
                    Ticket = 0
                };
            }
        }

        public async Task<TicketListDto> GetInputTicket(NullableIdDto input)
        {
            TicketListDto returnList;
            if (input.Id.HasValue)
            {
                var ticket = await _ticketRepository.GetAllIncluding(t => t.Location)
                    .Include(t => t.Orders)
                    .Include(t => t.Transactions).ThenInclude(t => t.TransactionType)
                    .Include(t => t.Payments).ThenInclude(t => t.PaymentType)
                    .FirstOrDefaultAsync(t => t.Id == input.Id.Value);
                
                returnList = ObjectMapper.Map<TicketListDto>(ticket);
                var ticket2 = await _ticketRepository.GetAllIncluding(t => t.Location)
                    .Include(t => t.Orders).ThenInclude(t => t.TransactionOrderTags)
                    .Include(t => t.Transactions).ThenInclude(t => t.TransactionType)
                    .Include(t => t.Payments).ThenInclude(t => t.PaymentType)
                    .FirstOrDefaultAsync(t => t.Id == input.Id.Value);
                var transactionOrdertag = new List<TransactionOrderTag>();
                foreach (var od in ticket2.Orders)
                {
                    var ordertag = od.TransactionOrderTags;
                    transactionOrdertag.AddRange(ordertag);
                }
                foreach (var lst in returnList.Orders)
                {
                    var promotionDetailValuelst = new List<PromotionDetailValue>();
                    if (!string.IsNullOrEmpty(lst.OrderPromotionDetails))
                        promotionDetailValuelst =
                            JsonHelper.Deserialize<List<PromotionDetailValue>>(lst.OrderPromotionDetails);
                    if (promotionDetailValuelst != null && promotionDetailValuelst.Count > 0)
                    {
                        var orderPromotionList = " ";
                        foreach (var promotionList in promotionDetailValuelst)
                            if (promotionList != null)
                                orderPromotionList = orderPromotionList + promotionList.PromotionName + " , ";
                        if (orderPromotionList.Length > 0)
                            orderPromotionList = orderPromotionList.Left(orderPromotionList.Length - 2);
                        lst.PromotionName = orderPromotionList;
                    }
                    var findOrderTag = transactionOrdertag.Where(s => s.OrderId == lst.Id);
                    foreach(var od in findOrderTag)
                    {
                        lst.TransactionOrderTags.Add(new TransactionOrderTagReportDto
                        {
                            TagName = od.TagName,
                            TagValue = od.TagValue,
                            Price = od.Price,
                            AddTagPriceToOrderPrice = od.AddTagPriceToOrderPrice,
                            Quantity = od.Quantity,
                            Id = od.Id,
                            MenuItemPortionId = od.MenuItemPortionId,
                            OrderId = od.OrderId,
                            OrderTagGroupId = od.OrderTagGroupId,
                            OrderTagId = od.OrderTagId,
                            TagNote = od.TagNote,
                            TaxFree = od.TaxFree
                        });
                    }
                    
                }

                if (!string.IsNullOrEmpty(returnList.TicketPromotionDetails))
                {
                    var ticketPromList =
                        JsonHelper.Deserialize<List<PromotionDetailValue>>(returnList.TicketPromotionDetails);
                    if (ticketPromList != null && ticketPromList.Count > 0)
                    {
                        decimal totalPromoAmount = 0;
                        foreach (var ticketPromotionList in ticketPromList)
                            if (ticketPromotionList != null)
                                totalPromoAmount = totalPromoAmount + ticketPromotionList.PromotionAmount;
                        returnList.TicketPromotionAmount = totalPromoAmount;
                    }
                }
            }
            else
            {
                returnList = new TicketListDto();
            }

            // Payment in transaction  = sum payment
            foreach (var item in returnList.Transactions)
                if (!item.TransactionTypeName.IsNullOrEmpty() && item.TransactionTypeName.ToUpper() == "PAYMENT")
                    item.Amount = returnList.Payments.Sum(x => x.Amount);
            var tenderAmount = returnList.Payments.Sum(s => s.TenderedAmount);
            if (tenderAmount > returnList.TotalAmount)
            {
                returnList.RemainingAmount = tenderAmount - returnList.TotalAmount;
            }
            return returnList;
        }

        [AbpAuthorize]
        public async Task<TicketStatsDto> GetDashboardChart(GetChartInput input)
        {
            if (input.Location > 0)
            {
                var loc = await _locationRepository.GetAsync(input.Location);
                if (loc != null)
                {
                    var myLd = ObjectMapper.Map<SimpleLocationDto>(loc);
                    input.Locations = new List<SimpleLocationDto> { myLd };
                }
            }

           var  x = _locationRepository.Query(q => q.Select(w => w.Address1));
            _querySchedularService.ExcuteQueryJob("", "", "", false, "",x, _appConfiguration[$"ConnectionStrings:{DineConnectConsts.ConnectionStringName}"]);


            return await _ticketReportAppService.GetTickets(new GetTicketInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                Locations = input.Locations
            }, true);
        }

        public List<ChartOutputDto> GetTransactionChart(GetChartInput input)
        {
            var result = GetChartTickets(input).SelectMany(a => a.Transactions)
                .GroupBy(a => a.TransactionType.Name).Select(a1 => new ChartOutputDto
                {
                    name = a1.Key,
                    y = a1.Sum(a => a.Amount)
                }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetPaymentChart(GetChartInput input)
        {
            var result =
                GetChartTickets(input).SelectMany(a => a.Payments)
                    .GroupBy(a => a.PaymentType.Name).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Amount)
                    }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetDepartmentChart(GetChartInput input)
        {
            var result = GetChartTickets(input).GroupBy(l => l.DepartmentName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.TotalAmount)
                }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetItemChart(GetChartInput input)
        {
            var result =
                GetChartTickets(input)
                    .SelectMany(a => a.Orders)
                    .Where(a => a.CalculatePrice && a.DecreaseInventory)
                    .GroupBy(a => a.MenuItemName).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Quantity)
                    }).OrderByDescending(a => a.y).Take(10).ToList();
            return result;
        }

        public async Task<DashboardTicketDto> GetDashboard()
        {
            return GetStatics(_ticketRepository.GetAll());
        }

        private IQueryable<Ticket> GetChartTickets(GetChartInput input)
        {
            var tickets = _ticketReportAppService.GetAllTickets(input);
            var outTickets = tickets.Where(a => a.TenantId == AbpSession.TenantId
                                                && input.Location > 0
                ? a.LocationId == input.Location
                : true && a.IsClosed);

            return outTickets;
        }

        private async Task<PagedResultDto<TicketListDto>> GetTickets(GetTicketInput input)
        {
            var tickets = _ticketReportAppService.GetAllTicketsForTicketInputQueryable(input);
            var sortTickets = await tickets.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var categoryListDtos = ObjectMapper.Map<List<TicketListDto>>(sortTickets);
            var menuItemCount = await tickets.CountAsync();

            return new PagedResultDto<TicketListDto>(
                menuItemCount,
                categoryListDtos
            );
        }

        private DashboardTicketDto GetStatics(IQueryable<Ticket> allTickets)
        {
            var ticketDto = new DashboardTicketDto();

            if (!allTickets.Any()) return ticketDto;
            try
            {
                ticketDto.TotalTicketCount = allTickets.Count();
                ticketDto.TotalAmount = allTickets.DefaultIfEmpty().Sum(a => a.TotalAmount);
                ticketDto.AverageTicketAmount = ticketDto.TotalAmount / ticketDto.TotalTicketCount;
                ticketDto.TotalOrderCount = allTickets.Sum(a => a.Orders.Count);

                var tickets = from p in allTickets.Include(a => a.Orders)
                    where p.Orders.Any(pt => pt.DecreaseInventory.Equals(true))
                    select p.Orders;
                ticketDto.TotalItemSold = tickets.Sum(a => a.Sum(b => b.Quantity));
            }
            catch (Exception)
            {
                // ignored
            }

            return ticketDto;
        }

        private async Task<int> CreateTicketAsync(TicketEditDto input)
        {
            var ticket = ObjectMapper.Map<Ticket>(input);
            ticket.Id = 0;
            foreach (var order in ticket.Orders)
            {
                order.Location_Id = ticket.LocationId;
                if (!string.IsNullOrEmpty(order.OrderTags))
                {
                    var tTake = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(order.OrderTags);

                    if (order.TransactionOrderTags == null)
                        order.TransactionOrderTags = new List<TransactionOrderTag>();
                    foreach (var otv in tTake) order.TransactionOrderTags.Add(await GetTransactionTag(otv));
                }
            }

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                {
                    var ticketId = await _ticketRepository.InsertAndGetIdAsync(ticket);
                    await unitOfWork.CompleteAsync();
                    return ticketId;
                }
            }
            catch (Exception exception)
            {
                if (input != null && !string.IsNullOrEmpty(input.TicketNumber) && input.TenantId > 0 &&
                    input.LocationId > 0)
                {
                    var turnedTicket =
                        await _ticketRepository.FirstOrDefaultAsync(
                            a =>
                                a.TicketNumber.Equals(input.TicketNumber) &&
                                a.LocationId.Equals(input.LocationId) && a.TenantId == input.TenantId);

                    if (turnedTicket != null) return turnedTicket.Id;
                }


                _logger.Fatal("Ticket Sync Failed");
                _logger.Fatal("-------------------");
                _logger.Fatal("Location : " + ticket.LocationId);
                _logger.Fatal("Ticket Number: " + ticket.TicketNumber);
                _logger.Fatal("Tenant Id : " + ticket.TenantId);
                _logger.Fatal("Ticket Details : " + JsonConvert.SerializeObject(input));
                _logger.Fatal("Exception : ", exception);
            }

            return 0;
        }

        private async Task<int> UpdateTicketAsync(TicketEditDto input)
        {
            if (input.Id == null)
                return 0;

            var inputDto = input;
            Ticket dbTicket;
            try
            {
                var dbQueryable = _ticketRepository
                    .GetAll()
                    .Include(a => a.Orders)
                    .Include(a => a.Payments).Include(a => a.Transactions).Where(a => a.Id == input.Id.Value);

                dbTicket = dbQueryable.ToList().LastOrDefault();

                if (dbTicket == null) return 0;

                dbTicket.InvoiceNo = inputDto.InvoiceNo;
                dbTicket.LocationId = inputDto.LocationId;
                dbTicket.TaxIncluded = inputDto.TaxIncluded;
                dbTicket.TicketId = inputDto.TicketId;
                dbTicket.TicketNumber = inputDto.TicketNumber;
                dbTicket.RemainingAmount = inputDto.RemainingAmount;
                dbTicket.TotalAmount = inputDto.TotalAmount;
                dbTicket.TicketTypeName = inputDto.TicketTypeName;
                dbTicket.LastModifiedUserName = inputDto.LastModifiedUserName;
                dbTicket.Note = inputDto.Note;
                dbTicket.TerminalName = inputDto.TerminalName;
                dbTicket.Credit = inputDto.Credit;
                dbTicket.ReferenceNumber = inputDto.ReferenceNumber;
                dbTicket.DepartmentName = inputDto.DepartmentName;
                dbTicket.DepartmentGroup = inputDto.DepartmentGroup;
                dbTicket.TicketTags = inputDto.TicketTags;
                dbTicket.TicketStates = inputDto.TicketStates;
                dbTicket.TicketLogs = inputDto.TicketLogs;
                dbTicket.LastUpdateTime = inputDto.LastUpdateTime;
                dbTicket.TicketCreatedTime = inputDto.TicketCreatedTime;
                dbTicket.LastOrderTime = inputDto.LastOrderTime;
                dbTicket.LastPaymentTime = inputDto.LastPaymentTime;
                dbTicket.LastPaymentTimeTruc = inputDto.LastPaymentTime.Date;
                dbTicket.IsClosed = inputDto.IsClosed;
                dbTicket.IsLocked = inputDto.IsLocked;
                dbTicket.PreOrder = inputDto.PreOrder;
                dbTicket.TicketEntities = inputDto.TicketEntities;
                dbTicket.WorkPeriodId = inputDto.WorkPeriodId;
                dbTicket.TicketPromotionDetails = inputDto.TicketPromotionDetails;
                dbTicket.TicketStatus = inputDto.TicketStatus;

                //Updating Order
                if (inputDto.Orders.Any())
                {
                    var oIds = new List<int>();

                    foreach (var orderDto in inputDto.Orders)
                    {
                        Order getOrder = null;
                        if (orderDto.OrderId > 0)
                            getOrder = dbTicket.Orders.SingleOrDefault(a => a.OrderId == orderDto.OrderId);

                        if (getOrder == null && !orderDto.OrderNumber.Equals("0"))
                            getOrder =
                                dbTicket.Orders.FirstOrDefault(
                                    a => a.OrderNumber.Equals(orderDto.OrderNumber) &&
                                         a.MenuItemId == orderDto.MenuItemId
                                         && orderDto.PortionName.Equals(a.PortionName)
                                         && orderDto.Quantity == a.Quantity
                                         && orderDto.CalculatePrice == a.CalculatePrice
                                         && orderDto.OrderCreatedTime ==
                                         a.OrderCreatedTime
                                         && orderDto.IncreaseInventory ==
                                         a.IncreaseInventory
                                         && orderDto.DecreaseInventory ==
                                         a.DecreaseInventory &&
                                         a.OrderTags.Equals(orderDto.OrderTags));
                        if (getOrder == null)
                        {
                            if (string.IsNullOrEmpty(orderDto.OrderTags)) orderDto.OrderTags = "";

                            getOrder =
                                dbTicket.Orders.FirstOrDefault(
                                    a => a.MenuItemId == orderDto.MenuItemId
                                         && orderDto.PortionName.Equals(a.PortionName)
                                         && orderDto.Quantity == a.Quantity
                                         && orderDto.CalculatePrice == a.CalculatePrice
                                         && orderDto.OrderCreatedTime == a.OrderCreatedTime
                                         && orderDto.IncreaseInventory == a.IncreaseInventory
                                         && orderDto.DecreaseInventory == a.DecreaseInventory &&
                                         orderDto.DepartmentName == a.DepartmentName &&
                                         a.OrderTags != null && a.OrderTags.Equals(orderDto.OrderTags));
                        }

                        if (getOrder == null)
                        {
                            var order = ObjectMapper.Map<Order>(orderDto);
                            if (!string.IsNullOrEmpty(orderDto.OrderTags))
                            {
                                var tTake = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(orderDto.OrderTags);
                                if (order.TransactionOrderTags == null)
                                    order.TransactionOrderTags = new List<TransactionOrderTag>();
                                foreach (var otv in tTake) order.TransactionOrderTags.Add(await GetTransactionTag(otv));
                            }

                            order.Location_Id = inputDto.LocationId;
                            dbTicket.Orders.Add(order);
                        }
                        else
                        {
                            oIds.Add(getOrder.Id);
                            getOrder.Location_Id = inputDto.LocationId;
                            UpdateOrder(getOrder, orderDto);
                        }
                    }

                    var tobeRemoved =
                        dbTicket.Orders.Where(a => !a.Id.Equals(0) && !oIds.Contains(a.Id)).Select(a => a.Id).ToArray();
                    foreach (var order in tobeRemoved) await _orRepository.DeleteAsync(order);
                }
                else
                {
                    var tobeRemoved = dbTicket.Orders.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id).ToArray();
                    foreach (var payment in tobeRemoved) await _orRepository.DeleteAsync(payment);
                }

                //Updating Payments
                if (inputDto.Payments.Any())
                {
                    var pids = new List<int>();
                    foreach (var pto in inputDto.Payments)
                    {
                        var getPayment =
                            dbTicket.Payments.SingleOrDefault(
                                a => a.PaymentTypeId == pto.PaymentTypeId && a.Amount == pto.Amount &&
                                     a.PaymentCreatedTime == pto.PaymentCreatedTime);

                        if (getPayment == null)
                        {
                            dbTicket.Payments.Add(ObjectMapper.Map<Transaction.Payment>(pto));
                        }
                        else
                        {
                            pids.Add(getPayment.Id);
                            UpdatePayment(getPayment, pto);
                        }
                    }

                    var tobeRemoved = dbTicket.Payments.Where(a => !a.Id.Equals(0) && !pids.Contains(a.Id)).ToList();
                    foreach (var order in tobeRemoved) await _paymentRepository.DeleteAsync(order.Id);
                }
                else
                {
                    var tobeRemoved = dbTicket.Payments.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id)
                        .ToArray();
                    foreach (var payment in tobeRemoved) await _paymentRepository.DeleteAsync(payment);
                }

                //Updating Transaction
                if (inputDto.Transactions.Any())
                {
                    var tids = new List<int>();
                    foreach (var pto in inputDto.Transactions)
                    {
                        var getPayment =
                            dbTicket.Transactions.SingleOrDefault(
                                a => a.TransactionTypeId == pto.TransactionTypeId && a.Amount == pto.Amount);

                        if (getPayment == null)
                        {
                            dbTicket.Transactions.Add(ObjectMapper.Map<TicketTransaction>(pto));
                        }
                        else
                        {
                            tids.Add(getPayment.Id);
                            UpdateTicketTransaction(getPayment, pto);

                            if (input.TotalAmount == 0M) getPayment.Amount = 0;
                        }
                    }

                    var tobeRemoved = dbTicket.Transactions.Where(a => !a.Id.Equals(0) && !tids.Contains(a.Id))
                        .ToList();
                    foreach (var order in tobeRemoved) await _taRepository.DeleteAsync(order.Id);
                }
                else
                {
                    var tobeRemoved =
                        dbTicket.Transactions.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id).ToArray();
                    foreach (var payment in tobeRemoved) await _taRepository.DeleteAsync(payment);
                }

                await _ticketRepository.InsertOrUpdateAndGetIdAsync(dbTicket);
            }
            catch (Exception exception)
            {
                _logger.Fatal("Ticket Update Error " + input.Id.Value);
                _logger.Fatal("Ticket Update Error", exception);
            }

            return input.Id.Value;
        }

        private void UpdateTicketTransaction(TicketTransaction to, TicketTransactionDto from)
        {
            to.TransactionTypeId = from.TransactionTypeId;
            to.Amount = from.Amount;
            to.TransactionNote = from.TransactionNote;
        }

        private void UpdatePayment(Transaction.Payment to, PaymentEditDto from)
        {
            to.PaymentTypeId = from.PaymentTypeId;
            to.PaymentCreatedTime = from.PaymentCreatedTime;
            to.TenderedAmount = from.TenderedAmount;
            to.TerminalName = from.TerminalName;
            to.Amount = from.Amount;
            to.PaymentUserName = from.PaymentUserName;
            to.PaymentTags = from.PaymentTags;
        }

        private void UpdateOrder(Order to, OrderEditDto from)
        {
            to.OrderId = from.OrderId;
            to.DepartmentName = from.DepartmentName;
            to.MenuItemId = from.MenuItemId;
            to.MenuItemName = from.MenuItemName;
            to.PortionName = from.PortionName;
            to.Price = from.Price;
            to.CostPrice = from.CostPrice;
            to.Quantity = from.Quantity;
            to.PortionCount = from.PortionCount;
            to.Note = from.Note;
            to.Locked = from.Locked;
            to.CalculatePrice = from.CalculatePrice;
            to.IncreaseInventory = from.IncreaseInventory;
            to.DecreaseInventory = from.DecreaseInventory;
            to.OrderNumber = from.OrderNumber;
            to.CreatingUserName = from.CreatingUserName;
            to.OrderCreatedTime = from.OrderCreatedTime;
            to.PriceTag = from.PriceTag;
            to.Taxes = from.Taxes;
            to.OrderTags = from.OrderTags;
            to.OrderStates = from.OrderStates;
            to.IsPromotionOrder = from.IsPromotionOrder;
            to.PromotionSyncId = from.PromotionSyncId;
            to.PromotionAmount = from.PromotionAmount;
            to.MenuItemPortionId = from.MenuItemPortionId;
            to.OrderPromotionDetails = from.OrderPromotionDetails;

            to.MenuItemType = from.MenuItemType;
            to.OrderRef = from.OrderRef != null ? from.OrderRef : "";
            to.IsParent = from.IsParent;
            to.IsUpSelling = from.IsUpSelling;
            to.UpSellingOrderRef = from.UpSellingOrderRef;
            to.OriginalPrice = from.OriginalPrice;

            to.OrderLog = from.OrderLog;
            to.OrderStatus = from.OrderStatus;
        }

        private async Task<TransactionOrderTag> GetTransactionTag(ReOrderTagValue otv)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization,
                       AbpDataFilters.SoftDelete))
            {
                var portionId = 0;
                if (otv.MI > 0)
                {
                    var allItems = _miRepository.GetAll().Include(a => a.Portions)
                        .Where(a => a.Id == otv.MI);
                    var menuItem = allItems.ToList().LastOrDefault();
                    if (menuItem != null && menuItem.Portions.Any())
                    {
                        var myPortion = menuItem.Portions.First();
                        portionId = myPortion.Id;
                    }
                }

                return new TransactionOrderTag
                {
                    MenuItemPortionId = portionId,
                    OrderTagGroupId = otv.GID,
                    OrderTagId = otv.TID,
                    Quantity = otv.Q,
                    Price = otv.PR,
                    TagName = otv.TN,
                    TagNote = otv.TO,
                    TagValue = otv.TV,
                    TaxFree = otv.TF,
                    AddTagPriceToOrderPrice = otv.AP
                };
            }
        }
    }
}
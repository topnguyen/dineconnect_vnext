﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Exporting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes
{
    public class TransactionTypesAppService : DineConnectAppServiceBase, ITransactionTypesAppService
    {
        private readonly IRepository<TransactionType> _transactionTypeRepository;
        private readonly ITransactionTypesExcelExporter _transactionTypesExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ISyncersAppService _syncersAppService;

        public TransactionTypesAppService(IRepository<TransactionType> transactionTypeRepository, 
            ITransactionTypesExcelExporter transactionTypesExcelExporter,ISyncersAppService syncersAppService,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _transactionTypeRepository = transactionTypeRepository;
            _transactionTypesExcelExporter = transactionTypesExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _syncersAppService = syncersAppService;

        }

        public async Task<PagedResultDto<TransactionTypeDto>> GetAll(GetAllTransactionTypesInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredTransactionTypes = _transactionTypeRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.AccountCode, input.Filter.ToILikeString()));

                var pagedAndFilteredTransactionTypes = await filteredTransactionTypes
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var transactionTypes = ObjectMapper.Map<List<TransactionTypeDto>>(pagedAndFilteredTransactionTypes);

                var totalCount = await filteredTransactionTypes.CountAsync();

                return new PagedResultDto<TransactionTypeDto>(
                    totalCount,
                    transactionTypes
                );
            }
        }

        
        public async Task<CreateOrEditTransactionTypeDto> GetTransactionTypeForEdit(EntityDto input)
        {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTransactionTypeDto>(transactionType);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTransactionTypeDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTransactionTypeDto input)
        {
            var transactionType = ObjectMapper.Map<TransactionType>(input);

            if (AbpSession.TenantId != null)
            {
                transactionType.TenantId = (int)AbpSession.TenantId;
            }

            await _transactionTypeRepository.InsertAsync(transactionType);
        }

        
        protected virtual async Task Update(CreateOrEditTransactionTypeDto input)
        {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, transactionType);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _transactionTypeRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _transactionTypeRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _transactionTypeRepository.UpdateAsync(item);
            }
        }

       

        public async Task<FileDto> GetTransactionTypesToExcel(GetAllTransactionTypesForExcelInput input)
        {
            var filteredTransactionTypes = await _transactionTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.AccountCode, input.Filter.ToILikeString()))
                        .ToListAsync();

            var transactionTypeListDtos = ObjectMapper.Map<List<TransactionTypeDto>>(filteredTransactionTypes);

            return _transactionTypesExcelExporter.ExportToFile(transactionTypeListDtos);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditTransactionTypeDto input)
        {
            var exists = _transactionTypeRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetTransactionTypeForLookupTable()
        {
            var query = await _transactionTypeRepository.GetAll()
                .Select(o => new ComboboxItemDto(o.Id.ToString(), o.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(query);
        }

        public async Task<PagedResultDto<NameValueDto>> GetTransactionTypeForLookupTablePaging(GetAllForLookupTableInput input)
        {
            var filteredDepartments = _transactionTypeRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "name asc")
                .PageBy(input);

            var departments = await pagedAndFilteredDepartments.Select(d => new NameValueDto(d.Name, d.Id.ToString())).ToListAsync();

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<NameValueDto>(
                totalCount,
                departments
            );
        }

        public async Task<ListResultDto<ApiTransactionTypeOutput>> ApiGetAll(ApiTenantInputDto inputDto)
        {
            var lstTransTypes = await _transactionTypeRepository.GetAll().ToListAsync();

            var allOutputs = lstTransTypes.Select(sortL => new ApiTransactionTypeOutput
            {
                Id = sortL.Id,
                Name = sortL.Name,
                AccountCode = sortL.AccountCode,
                Tax = sortL.Tax
            }).ToList();

            return new ListResultDto<ApiTransactionTypeOutput>(allOutputs);
        }

        public async Task<PagedResultDto<TransactionTypeDto>> GetAllItems()
        {
            var allItems = _transactionTypeRepository.GetAll();
            var allListDtos = ObjectMapper.Map<List<TransactionTypeDto>>(allItems);
            var allItemCount = await allItems.CountAsync();
            return new PagedResultDto<TransactionTypeDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
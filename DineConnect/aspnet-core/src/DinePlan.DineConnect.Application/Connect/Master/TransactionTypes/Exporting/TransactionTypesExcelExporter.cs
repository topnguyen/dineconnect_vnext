﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Exporting
{
    public class TransactionTypesExcelExporter : NpoiExcelExporterBase, ITransactionTypesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TransactionTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<TransactionTypeDto> transactionTypes)
        {
            return CreateExcelPackage(
                "TransactionTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TransactionTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Tax"),
                        L("Discount"),
                        L("AccountCode"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, transactionTypes,
                        _ => _.Name,
                        _ => _.Tax,
                        _ => _.Discount,
                        _ => _.AccountCode,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())

                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
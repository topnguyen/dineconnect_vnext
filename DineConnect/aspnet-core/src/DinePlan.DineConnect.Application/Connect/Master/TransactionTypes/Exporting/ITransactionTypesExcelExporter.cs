﻿using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Exporting
{
    public interface ITransactionTypesExcelExporter
    {
        FileDto ExportToFile(List<TransactionTypeDto> transactionTypes);
    }
}
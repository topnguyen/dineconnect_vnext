﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardType.Exporting
{
  public interface IConnectCardTypeExcelExporter
    {
        FileDto ExportToFile(List<ConnectCardTypeListDto> list);
        
        FileDto ExportToFileForImportCardsTemplate();
    }
}

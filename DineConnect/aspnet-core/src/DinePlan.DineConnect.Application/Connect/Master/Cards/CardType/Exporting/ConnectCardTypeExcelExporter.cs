﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardType.Exporting
{
    public class ConnectCardTypeExcelExporter : NpoiExcelExporterBase, IConnectCardTypeExcelExporter
    {
        public ConnectCardTypeExcelExporter(
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ConnectCardTypeListDto> list)
        {
            return CreateExcelPackage(
                "ConnectCardTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ConnectCardType"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Length"),
                        L("UseOneTime"),
                        L("Oid")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.Length,
                        _ => _.UseOneTime,
                        _ => _.Oid
                    );

                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportToFileForImportCardsTemplate()
        {
            return CreateExcelPackage(
                "ImportConnectCards.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ConnectCard"));

                    AddHeader(
                        sheet,
                        L("CardNo"),
                        L("Redeemed")
                    );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Filter;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardType.Importing
{
    public class ConnectCardTypeListExcelDataReader : NpoiExcelImporterBase<ConnectCardEditDto>, IConnectCardTypeListExcelDataReader
    {
        private readonly IRepository<ConnectCardType> _cardTypeRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ConnectCardTypeListExcelDataReader(
            IRepository<ConnectCardType> cardTypeRepo,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _cardTypeRepo = cardTypeRepo;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public List<ConnectCardEditDto> GetConnectCardFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ConnectCardEditDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            try
            {
                using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
                {
                    if (IsRowEmpty(worksheet, row))
                    {
                        return null;
                    }

                    var cardEditDto = new ConnectCardEditDto
                    {
                        CardNo = worksheet.GetRow(row).GetCell(0).StringCellValue?.Trim(),
                        
                        Redeemed =  worksheet.GetRow(row).GetCell(1).StringCellValue?.Trim()
                            .Equals("YES", StringComparison.OrdinalIgnoreCase) == true
                    };

                    return cardEditDto;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var rowData = worksheet.GetRow(row);

            try
            {
                return string.IsNullOrWhiteSpace(rowData.GetCell(0)?.StringCellValue)
                       || string.IsNullOrWhiteSpace(rowData.GetCell(1)?.StringCellValue);
            }
            catch
            {
                return true;
            }
        }
    }
}
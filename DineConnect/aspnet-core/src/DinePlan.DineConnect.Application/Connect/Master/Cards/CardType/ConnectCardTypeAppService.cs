﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Card.Card;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.Connect.Card.CardType;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Connect.Master.Cards.CardType.Exporting;
using DinePlan.DineConnect.Connect.Master.Cards.CardType.Importing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardType
{
    
    public class ConnectCardTypeAppService : DineConnectAppServiceBase, IConnectCardTypeAppService
    {
        private readonly IRepository<ConnectCardType> _cardTypeRepository;
        private readonly IRepository<ConnectCard> _connectCardRepository;
        private readonly IConnectCardTypeExcelExporter _connectCardTypeExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ConnectSession _connectSession;
        private readonly IConnectCardTypeListExcelDataReader _cardTypeListExcelDataReader;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IConnectCardAppService _connectCardAppService;

        public ConnectCardTypeAppService(
            IRepository<ConnectCardType> cardTypeRepository
            , IRepository<ConnectCard> connectCardRepository
            , IConnectCardTypeExcelExporter connectCardTypeExporter
            , IUnitOfWorkManager unitOfWorkManager
            , ConnectSession connectSession
            , IConnectCardTypeListExcelDataReader cardTypeListExcelDataReader
            , ITempFileCacheManager tempFileCacheManager
            , IConnectCardAppService connectCardAppService
            )
        {
            _cardTypeRepository = cardTypeRepository;
            _connectCardRepository = connectCardRepository;
            _connectCardTypeExporter = connectCardTypeExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _connectSession = connectSession;
            _cardTypeListExcelDataReader = cardTypeListExcelDataReader;
            _tempFileCacheManager = tempFileCacheManager;
            _connectCardAppService = connectCardAppService;
        }

        public async Task<PagedResultDto<ConnectCardTypeListDto>> GetCardTypes(GetConnectCardTypesInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (!input.Filter.IsNullOrEmpty())
                {
                    input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
                }

                var query = _cardTypeRepository.GetAll()
                    .OrderBy(input.Sorting)
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(!input.Filter.IsNullOrEmpty(), c => c.Name.Contains(input.Filter));

                //if (!string.IsNullOrWhiteSpace(input.LocationIds))
                //{
                //    query = FilterByLocations(query, input);
                //}

                var pagedList = query
                    .PageBy(input)
                    .ToList();

                var result = pagedList.Select(c =>
                {
                    var dto = ObjectMapper.Map<ConnectCardTypeListDto>(c);

                    dto.CardCount = _connectCardRepository
                        .GetAll()
                        .Count(t => t.ConnectCardTypeId == dto.Id);

                    return dto;
                }).ToList();

                return new PagedResultDto<ConnectCardTypeListDto>(await query.CountAsync(), result);
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _cardTypeRepository.GetAll().ToListAsync();

            var allListDtos = ObjectMapper.Map<List<ConnectCardTypeListDto>>(allList);

            return _connectCardTypeExporter.ExportToFile(allListDtos);
        }

        public FileDto GetImportCardsTemplateToExcel()
        {
            return _connectCardTypeExporter.ExportToFileForImportCardsTemplate();
        }

        public async Task ImportCardsFromExcel(int cardTypeId, string fileToken)
        {
            if (!string.IsNullOrWhiteSpace(fileToken))
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                        List<string> headers = new List<string>
                        {
                            "CardNumber",
                            "CardType"
                        };

                        var columns = worksheet.Dimension.End.Column;

                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (int i = 1; i < columns + 1; i++)
                            {
                                sheetHeaders.Add(worksheet.GetValue(1, i).ToString());
                            }

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }

                var cards = _cardTypeListExcelDataReader.GetConnectCardFromExcel(fileBytes);

                foreach (var card in cards)
                {
                    card.ConnectCardTypeId = cardTypeId;

                    await _connectCardAppService.CreateOrUpdateConnectCard(card);
                }
            }
        }

        
        public async Task<GetConnectCardTypeForEditOutput> GetConnectCardTypeForEdit(int? id)
        {
            GetConnectCardTypeForEditOutput output = new GetConnectCardTypeForEditOutput();

            if (id.HasValue)
            {
                var connectCardType = await _cardTypeRepository.GetAll().FirstOrDefaultAsync(c => c.Id == id.Value);

                output.ConnectCardType = ObjectMapper.Map<ConnectCardTypeEditDto>(connectCardType);

                var connectCards = await _connectCardRepository.GetAll()
                    .Where(t => t.ConnectCardTypeId == id.Value)
                    .OrderByDescending(t => t.CreationTime)
                    .ToListAsync();

                output.CardList = ObjectMapper.Map<List<ConnectCardListDto>>(connectCards);

                //if (connectCardType != null && output.LocationGroup != null)
                //{
                //    UpdateLocations(connectCardType, output.LocationGroup);
                //}
            }

            return output;
        }

        public async Task<GetConnectCardTypeForEditOutput> CreateOrUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
        {
            var id = input.ConnectCardType.Id;

            input.ConnectCardType.TenantId = AbpSession.TenantId;

            ValidateForUpdateConnectCardType(input);

            if (input.ConnectCardType.Id.HasValue)
            {
                id = await UpdateConnectCardType(input);
            }
            else
            {
                id = await CreateConnectCardType(input);
            }

            return await GetConnectCardTypeForEdit(id);
        }

        
        public async Task DeleteConnectCardType(NullableIdDto input)
        {
            if (input.Id.HasValue)
            {
                await _cardTypeRepository.DeleteAsync(input.Id.Value);
            }
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _cardTypeRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _cardTypeRepository.UpdateAsync(item);
            }
        }

        public async Task<List<ConnectCardTypeListDto>> GetAllCardTypes()
        {
            var myTags = await _cardTypeRepository.GetAll().ToListAsync();

            return ObjectMapper.Map<List<ConnectCardTypeListDto>>(myTags);
        }

        
        private async Task<int?> UpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
        {
            var connectCardType = await _cardTypeRepository.GetAsync(input.ConnectCardType.Id.Value);

            var dto = input.ConnectCardType;

            ObjectMapper.Map(dto, connectCardType);

            connectCardType.OrganizationId = _connectSession.OrgId;

            //UpdateLocations(connectCardType, input.LocationGroup);

            await _cardTypeRepository.UpdateAsync(connectCardType);

            return connectCardType.Id;
        }

        
        private async Task<int?> CreateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
        {
            var dto = ObjectMapper.Map<ConnectCardType>(input.ConnectCardType);

            dto.OrganizationId = _connectSession.OrgId;

            //UpdateLocations(dto, input.LocationGroup);

            return await _cardTypeRepository.InsertAndGetIdAsync(dto);
        }

        private void ValidateForUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
        {
            var exists = _cardTypeRepository
                .GetAll()
                .WhereIf(input.ConnectCardType.Id.HasValue, c => c.Id != input.ConnectCardType.Id).Where(a => a.Name.Equals(input.ConnectCardType.Name))
                .ToList();

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists", input.ConnectCardType.Name));
            }
        }
    }
}
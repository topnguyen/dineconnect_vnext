﻿using System.Collections.Generic;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardType.Importing
{
    public interface IConnectCardTypeListExcelDataReader : IApplicationService
    {
        List<ConnectCardEditDto> GetConnectCardFromExcel(byte[] fileBytes);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.Jobs.ImportConnectCardJob;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Card.Card;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Connect.Master.Cards.Card.Exporting;
using DinePlan.DineConnect.Connect.Master.Cards.Card.Importing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Master.Cards.Card
{
    
    public class ConnectCardAppService : DineConnectAppServiceBase, IConnectCardAppService
    {
        private readonly IRepository<ConnectCardType> _connectCardTypeRepository;
        private readonly IRepository<ConnectCard> _connectCardRepository;
        private readonly IRepository<ConnectCardRedemption> _connectCardRedeemRepository;
        private readonly IConnectCardExcelExporter _connectCardExporter;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IConnectCardListExcelDataReader _cardListExcelDataReader;
        private readonly IRepository<ImportCardDataDetail> _importCardDataDetailRepository;
        private readonly ConnectSession _connectSession;

        public ConnectCardAppService(
            IRepository<ConnectCardType> connectCardTypeRepository
            , IRepository<ConnectCard> connectCardRepository
            , IRepository<ConnectCardRedemption> connectCardRedeemRepository
            , IConnectCardExcelExporter connectCardExporter
            , IBackgroundJobManager backgroundJobManager
            , IUnitOfWorkManager unitOfWorkManager
            , ITempFileCacheManager tempFileCacheManager
            , IConnectCardListExcelDataReader cardListExcelDataReader
            , IRepository<ImportCardDataDetail> importCardDataDetailRepository
            , ConnectSession connectSession)
        {
            _connectCardTypeRepository = connectCardTypeRepository;
            _connectCardRepository = connectCardRepository;
            _connectCardRedeemRepository = connectCardRedeemRepository;
            _connectCardExporter = connectCardExporter;
            _backgroundJobManager = backgroundJobManager;
            _unitOfWorkManager = unitOfWorkManager;
            _tempFileCacheManager = tempFileCacheManager;
            _cardListExcelDataReader = cardListExcelDataReader;
            _importCardDataDetailRepository = importCardDataDetailRepository;
            _connectSession = connectSession;
        }

        public async Task<PagedResultDto<ConnectCardListDto>> GetAllConnectCard(GetConnectCardsInput input)
        {
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var allItems = _connectCardRepository
               .GetAll()
               .Include(x => x.ConnectCardType)
               .Where(c => c.TenantId == AbpSession.TenantId)
               .WhereIf(input.Redeemed.HasValue, p => p.Redeemed == input.Redeemed)
               .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.CardNo.Contains(input.Filter) || p.ConnectCardType.Name.Contains(input.Filter))
               .WhereIf(input.ConnectCardTypeId.HasValue, p => p.ConnectCardTypeId == input.ConnectCardTypeId);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<ConnectCardListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<ConnectCardListDto>(allItemCount, allListDtos);
        }

        public FileDto GetImportTemplateToExcel()
        {
            return _connectCardExporter.ExportToFileForImportTemplate();
        }

        public async Task ImportFromExcel(string fileToken)
        {
            if (!string.IsNullOrWhiteSpace(fileToken))
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                        List<string> headers = new List<string>
                        {
                            "CardNumber",
                            "CardType"
                        };

                        var columns = worksheet.Dimension.End.Column;

                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (int i = 1; i < columns + 1; i++)
                            {
                                sheetHeaders.Add(worksheet.GetValue(1, i).ToString());
                            }

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }

                var cards = _cardListExcelDataReader.GetConnectCardFromExcel(fileBytes);

                foreach (var card in cards)
                {
                    await CreateOrUpdateConnectCard(card);
                }
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _connectCardRepository.GetAllListAsync(c => c.TenantId == AbpSession.TenantId);

            var allListDtos = ObjectMapper.Map<List<ConnectCardListDto>>(allList);

            return _connectCardExporter.ExportToFile(allListDtos);
        }

        public async Task<List<ConnectCardTypeListDto>> GetConnectCardTypes()
        {
            var lst = await _connectCardTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ConnectCardTypeListDto>>(lst);
        }

        
        public async Task<ConnectCardEditDto> GetConnectCardForEdit(NullableIdDto input)
        {
            var editDto = new ConnectCardEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _connectCardRepository.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<ConnectCardEditDto>(hDto);
            }
            else
            {
                editDto = new ConnectCardEditDto();
            }

            return editDto;
        }

        public async Task<ConnectCardEditDto> CreateOrUpdateConnectCard(ConnectCardEditDto input)
        {
            var cardType = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == input.ConnectCardTypeId);

            input.Redeemed = cardType.UseOneTime && input.Redeemed;
            if (AbpSession.TenantId.HasValue)
                input.TenantId = AbpSession.TenantId;

            if (input.Id.HasValue)
            {
                return await UpdateConnectCard(input);
            }
            else
            {
                return await CreateConnectCard(input);
            }
        }

        public async Task GenerateConnectCards(GenerateCardsInputDto input)
        {
            input.TenantId = AbpSession.TenantId;

            input.UserId = AbpSession.UserId;

            var ct = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == input.ConnectCardTypeId && t.TenantId == input.TenantId);
            if (ct == null)
            {
                throw new UserFriendlyException(L("CardTypeIdNotExist"));
            }

            if (input.CardNumberOfDigits + (input.Prefix.IsNullOrEmpty() ? 0 : input.Prefix.Length) + (input.Suffix.IsNullOrEmpty() ? 0 : input.Suffix.Length) > ct.Length)
            {
                throw new UserFriendlyException(L("CardDigitsShouldNotBeGreaterThanNDigits", ct.Length));
            }

            await GenerateConnectCardsInBackground(input);
            // await _backgroundJobManager.EnqueueAsync<GenerateConnectCardJob, GenerateCardsInputDto>(input);
        }

        [UnitOfWork]
        public async Task GenerateConnectCardsInBackground(GenerateCardsInputDto input)
        {
            var ct = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == input.ConnectCardTypeId && t.TenantId == input.TenantId);
            if (ct == null)
            {
                throw new UserFriendlyException(L("CardTypeIdNotExist"));
            }
            for (var i = 0; i < input.CardCount; i++)
            {
                var myStr = input.CardStartingNumber + i;
                var cardNumber = myStr.ToString().Trim();
                var digitsDiff = input.CardNumberOfDigits - cardNumber.Trim().Length;

                if (digitsDiff < 0)
                {
                    throw new UserFriendlyException(L("CardDigitsLessThanCardNumberLength", cardNumber, cardNumber.Trim().Length, input.CardNumberOfDigits));
                }

                cardNumber = cardNumber.Trim().PadLeft(input.CardNumberOfDigits, '0');

                var cardNumberInSpecifiedDigits = $"{input.Prefix}{cardNumber}{input.Suffix}";

                var connectCard = new ConnectCard
                {
                    ConnectCardTypeId = input.ConnectCardTypeId,
                    CardNo = cardNumberInSpecifiedDigits,
                    Redeemed = false,
                    TenantId = ct.TenantId,
                    CreatorUserId = input.UserId
                };

                CheckErrors(await ValidateForCreateSync(connectCard));
            }
        }

        
        public async Task DeleteConnectCard(NullableIdDto input)
        {
            if (input.Id.HasValue)
            {
                await _connectCardRepository.DeleteAsync(input.Id.Value);
            }
        }

        
        public async Task DeleteMultiConnectCard(List<int> input)
        {
            if (input.Any())
            {
                var connectCards = await _connectCardRepository.GetAll().Where(c => input.Contains(c.Id)).ToListAsync();
                foreach (var rsconnectCards in connectCards)
                {
                    if (rsconnectCards != null)
                        if (!rsconnectCards.Redeemed)
                            await _connectCardRepository.DeleteAsync(rsconnectCards);
                }
            }
        }

        public async Task ActiveOrInactiveConnectCard(EntityDto input)
        {
            var card = await _connectCardRepository.GetAsync(input.Id);
            card.Active = !card.Active;
        }

        public async Task<ValidateCardOutput> ApiValidateCard(ValidateCardInput input)
        {
            ValidateCardOutput output = new ValidateCardOutput();

            if (input != null && !string.IsNullOrEmpty(input.CardNumber))
            {
                var myCards = _connectCardRepository.GetAll().Where(a => a.CardNo.Equals(input.CardNumber) && a.TenantId == input.TenantId);

                if (myCards.Any())
                {
                    var myLast = myCards.ToList().LastOrDefault();
                    if (!myLast.Redeemed)
                    {
                        output.ConnectCardTypeId = myLast.ConnectCardTypeId;
                        output.ErrorMessage = "";
                    }
                    else
                    {
                        output.ErrorMessage = "Card has been redeemed";
                    }
                }
                else
                {
                    output.ErrorMessage = "No Card Available";
                }
            }
            else
            {
                output.ErrorMessage = "Empty Card Number";
            }

            return output;
        }

        public async Task<ApplyCardOutput> ApiApplyCard(ApplyCardInput input)
        {
            ApplyCardOutput output = new ApplyCardOutput();

            if (input != null && !string.IsNullOrEmpty(input.CardNumber))
            {
                var myCards = _connectCardRepository.GetAll().Where(a => a.CardNo.Equals(input.CardNumber) && a.TenantId == input.TenantId);

                if (myCards.Any())
                {
                    var myLast = myCards.LastOrDefault();

                    if (!myLast.Redeemed)
                    {
                        bool addRedeem = false;
                        if (myLast.ConnectCardType.UseOneTime)
                        {
                            myLast.Redeemed = true;
                            addRedeem = true;
                        }
                        else
                        {
                            var allRedeemAva = _connectCardRedeemRepository.GetAll().Where(a => a.ConnectCardId == myLast.Id && a.TicketNumber.Equals(input.TicketNumber));

                            if (allRedeemAva.Any())
                            {
                                output.ErrorMessage = "Already Redeemed";
                            }
                            else
                            {
                                addRedeem = true;
                            }
                        }

                        if (addRedeem)
                        {
                            var myId = await _connectCardRedeemRepository.InsertAndGetIdAsync(new ConnectCardRedemption
                            {
                                ConnectCardId = myLast.Id,
                                LocationId = input.LocationId,
                                RedemptionDate = DateTime.Now,
                                RedemptionReferences = input.PromotionId.ToString(),
                            });
                        }
                    }
                    else
                    {
                        output.ErrorMessage = "Card has been redeemed";
                    }
                }
                else
                {
                    output.ErrorMessage = "No Card Available";
                }
            }
            else
            {
                output.ErrorMessage = "Empty Card Number";
            }

            return output;
        }

        private async Task<IdentityResult> ValidateForCreateSync(ConnectCard connectCard)
        {
            var cardType = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == connectCard.ConnectCardTypeId);

            if (connectCard.CardNo.Length > cardType.Length)
            {
                throw new UserFriendlyException(L("CardDigitsLessThanCardNumberLength", connectCard.CardNo, connectCard.CardNo.Length, cardType.Length));
            }

            var exists = _connectCardRepository.GetAll()
                .Where(c => c.TenantId == AbpSession.TenantId)
                .Where(a => a.CardNo.ToUpper().Equals(connectCard.CardNo.ToUpper()))
                .WhereIf(connectCard.Id != 0, a => a.Id != connectCard.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("CardNumberAlreadyExists", connectCard.CardNo));
            }
            if (connectCard.Id == 0)
            {
                await _connectCardRepository.InsertOrUpdateAsync(connectCard);
            }
            return IdentityResult.Success;
        }

        
        private async Task<ConnectCardEditDto> CreateConnectCard(ConnectCardEditDto input)
        {
            var dto = ObjectMapper.Map<ConnectCard>(input);

            CheckErrors(await ValidateForCreateSync(dto));

            return ObjectMapper.Map<ConnectCardEditDto>(dto);
        }

        
        private async Task<ConnectCardEditDto> UpdateConnectCard(ConnectCardEditDto input)
        {
            var item = await _connectCardRepository.GetAsync(input.Id.Value);

            ObjectMapper.Map(input, item);

            CheckErrors(await ValidateForCreateSync(item));

            return ObjectMapper.Map<ConnectCardEditDto>(item);
        }

        public async Task ImportConnectCards(ImportConnectCardsInBackgroundInputDto input)
        {
            #region Insert ImportCard Data Detail

            DateTime? startDateTime = DateTime.Now;
            var importedfileName = Path.GetFileName(input.FileName);
            var currentPath = Path.GetFullPath(input.FileName);
            currentPath = Directory.GetParent(currentPath).FullName;
            var importedfilePath = currentPath;

            ImportCardDataDetailEditDto importCardDataDetailDto = new ImportCardDataDetailEditDto
            {
                JobName = input.JobName,
                FileName = importedfileName,
                FileLocation = importedfilePath,
                StartTime = startDateTime,
                EndTime = null,
                OutputJsonDto = null,
                TenantId = input.TenantId
            };
            input.ImportCardDataDetail = importCardDataDetailDto;

            var dto = ObjectMapper.Map<ImportCardDataDetail>(input.ImportCardDataDetail);
            var retId = await _importCardDataDetailRepository.InsertAndGetIdAsync(dto);
            input.ImportCardDataDetail.Id = retId;

            #endregion Insert ImportCard Data Detail

            await _backgroundJobManager.EnqueueAsync<ImportConnectCardJob, ImportConnectCardJobArgs>(new ImportConnectCardJobArgs
            {
                ConnectCard = input.ConnectCard,
                FileName = input.FileName,
                Active = input.Active,
                TenantId = input.TenantId,
                TenantName = input.TenantName,
                JobName = input.JobName,
                ImportCardDataDetail = input.ImportCardDataDetail
            });
        }

        public async Task<GetConnectCardDataFromFileOutputDto> ImportConnectCardInBackGround(ImportConnectCardsInBackgroundInputDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var duplicateExistsInDB = new List<ConnectCardEditDto>();
                var duplicateExistsInExcel = new List<ConnectCardEditDto>();
                var importedCardsList = new List<ConnectCardEditDto>();

                FileInfo localfileName = new FileInfo(input.FileName);
                var data = new ExcelData(input.FileName, localfileName.OpenRead());
                var dtos = await GetConnectCardDataFromFile(data, input.Active, input.TenantId);
                var allItems = ObjectMapper.Map<List<ConnectCardEditDto>>(dtos.ConnectCards);

                var rsconnectcard = await _connectCardRepository.GetAll().Where(c => c.TenantId == input.TenantId).ToListAsync();
                foreach (var lst in allItems)
                {
                    var alreadyExists = rsconnectcard.FirstOrDefault(t => t.CardNo == lst.CardNo);
                    if (alreadyExists != null)
                    {
                        var exists = duplicateExistsInDB.Any(t => t.CardNo == lst.CardNo);
                        if (!exists)
                        {
                            lst.Remarks = L("RecordAlreadyExistInDb");
                            duplicateExistsInDB.Add(lst);
                        }
                    }
                }

                var duplicates = allItems.GroupBy(i => i.CardNo).Where(g => g.Count() > 1).ToList();
                if (duplicates.Count() > 0)
                {
                    foreach (var lst in duplicates)
                    {
                        var firstRec = lst.LastOrDefault();
                        firstRec.Remarks = L("RecordAlreadyExistInExcel");
                        duplicateExistsInExcel.Add(firstRec);
                    }
                }
                var connectCard = new PagedResultDto<ConnectCardEditDto>()
                {
                    Items = duplicateExistsInExcel,
                    TotalCount = duplicateExistsInExcel.Count
                };
                foreach (var existRec in connectCard.Items)
                {
                    allItems.Remove(existRec);
                }
                foreach (var lst in allItems)
                {
                    if (lst.Id == null)
                    {
                        if (lst.ConnectCardTypeId > 0)
                        {
                            var result = await CreateOrUpdateConnectCard(lst);
                            importedCardsList.Add(lst);
                        }
                    }
                }
                ExportConnectCardImportStatusDto exportConnectCardImportStatusDto = new ExportConnectCardImportStatusDto
                {
                    DuplicateExistInExcel = duplicateExistsInExcel,
                    DuplicateExistInDB = duplicateExistsInDB,
                    ImportedCardsList = importedCardsList,
                    ErrorList = dtos.MessageOutput.ErrorMessageList
                };

                var objectJson = JsonConvert.SerializeObject(exportConnectCardImportStatusDto);
                input.ImportCardDataDetail.OutputJsonDto = objectJson;
                DateTime? endDateTime = DateTime.Now;
                input.ImportCardDataDetail.EndTime = endDateTime;

                var dto = ObjectMapper.Map<ImportCardDataDetail>(input.ImportCardDataDetail);
                await _importCardDataDetailRepository.UpdateAsync(dto);
                return dtos;
            }
        }

        private async Task<GetConnectCardDataFromFileOutputDto> GetConnectCardDataFromFile(ExcelData data, bool active, int? tenantId)
        {
            string errorMessage = "";
            MessageOutputDto messageOutput = new MessageOutputDto();
            List<string> errorList = new List<string>();
            var currentRow = 1;
            try
            {
                var dtos = new List<ConnectCardEditDto>();
                foreach (var row in data.GetData())
                {
                    currentRow++;

                    if (row["Card Number"].ToString().Length == 0)
                    {
                        continue;
                    }

                    var cardNumber = row["Card Number"]?.ToString().Trim() ?? "";
                    if (cardNumber.IsNullOrEmpty())
                    {
                        errorMessage = L("ErrRow", currentRow) + L("Card Number");
                        errorList.Add(errorMessage);
                    }

                    string connectCardTypeName = row["Card Type"]?.ToString() ?? "";
                    connectCardTypeName = connectCardTypeName.Trim().ToUpper();
                    if (connectCardTypeName.Length == 0)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists");
                        errorList.Add(errorMessage);
                    }

                    int? id = null, cardTypeId = null; bool redeemedValue = false;
                    var exists = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Name == connectCardTypeName && tenantId == t.TenantId);
                    if (exists != null)
                    {
                        cardTypeId = exists.Id;
                        connectCardTypeName = exists.Name;
                        if (exists.UseOneTime == true)
                        {
                            redeemedValue = false;
                        }
                    }
                    else
                    {
                        cardTypeId = null;
                    }

                    if (cardTypeId == null)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists");
                        errorList.Add(errorMessage);
                    }

                    var alreadyExists = await _connectCardRepository.FirstOrDefaultAsync(t => t.CardNo == cardNumber);
                    if (alreadyExists != null)
                    {
                        id = alreadyExists.Id;
                    }

                    var connectCard = new ConnectCardEditDto
                    {
                        Id = id,
                        ConnectCardTypeId = cardTypeId != null ? cardTypeId.Value : 0,
                        ConnectCardTypeName = connectCardTypeName,
                        CardNo = cardNumber,
                        Redeemed = redeemedValue,
                        Active = active
                    };

                    dtos.Add(connectCard);
                }
                messageOutput.ErrorMessageList = errorList;
                return new GetConnectCardDataFromFileOutputDto { ConnectCards = dtos, MessageOutput = messageOutput };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<ImportCardDataDetailListDto>> GetImportCardDataDetails(GetConnectCardsInput input)
        {
            var allItems = _importCardDataDetailRepository.GetAll();
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<ImportCardDataDetailListDto>>(sortMenuItems);
            GetRemarksForImportDetail(allListDtos);
            return new PagedResultDto<ImportCardDataDetailListDto>(await allItems.CountAsync(), allListDtos);
        }

        private static void GetRemarksForImportDetail(List<ImportCardDataDetailListDto> allListDtos)
        {
            foreach (var lst in allListDtos)
            {
                string remarks = " ";
                if (lst.OutputJsonDto != null)
                {
                    var jsonOutput = JsonConvert.DeserializeObject<ExportConnectCardImportStatusDto>(lst.OutputJsonDto);
                    if (jsonOutput != null && jsonOutput.ErrorList.Count > 0)
                    {
                        remarks = remarks + "Error" + "   ";
                    }
                }
                if (lst.EndTime.HasValue)
                {
                    var jsonOutput = JsonConvert.DeserializeObject<ExportConnectCardImportStatusDto>(lst.OutputJsonDto);
                    remarks = remarks + "Inserted Cards : " + jsonOutput.ImportedCardsList.Count + " , " + "Duplicate Exists in Excel : " + jsonOutput.DuplicateExistInExcel.Count + " , " + "Duplicate Exists in DB : " + jsonOutput.DuplicateExistInDB.Count;
                }
                else
                {
                    remarks = remarks + "In Process";
                }

                lst.Remarks = remarks;
            }
        }

        public async Task<FileDto> GetImportCardDataDetailToExport()
        {
            var allItems = _importCardDataDetailRepository.GetAll();
            var sortMenuItems = await allItems
                .OrderByDescending(c => c.CreationTime)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<ImportCardDataDetailListDto>>(sortMenuItems);
            GetRemarksForImportDetail(allListDtos);

            return _connectCardExporter.ExportImportCardDataDetails(allListDtos);
        }
    }
}
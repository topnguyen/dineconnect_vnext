﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Cards.Card.Exporting
{
  public interface IConnectCardExcelExporter
    {
		FileDto ExportImportCardDataDetails(List<ImportCardDataDetailListDto> dtos);
		FileDto ExportToFile(List<ConnectCardListDto> list);
        
        FileDto ExportToFileForImportTemplate();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Filter;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.Connect.Master.Cards.Card.Importing
{
    public class ConnectCardListExcelDataReader : NpoiExcelImporterBase<ConnectCardEditDto>, IConnectCardListExcelDataReader
    {
        private readonly IRepository<ConnectCardType> _cardTypeRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ConnectCardListExcelDataReader(
            IRepository<ConnectCardType> cardTypeRepo,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _cardTypeRepo = cardTypeRepo;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public List<ConnectCardEditDto> GetConnectCardFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ConnectCardEditDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            try
            {
                using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
                {
                    if (IsRowEmpty(worksheet, row))
                    {
                        return null;
                    }

                    var cardEditDto = new ConnectCardEditDto
                    {
                        CardNo = worksheet.GetRow(row).GetCell(0).StringCellValue?.Trim()
                    };

                    // Card Type Id
                    var cardTypeName = worksheet.GetRow(row).GetCell(1).StringCellValue.Trim();
                    var cardTypeId = _cardTypeRepo.GetAll().FirstOrDefault(x => x.Name == cardTypeName)?.Id;
                    if (!cardTypeId.HasValue)
                    {
                        return null;
                    }

                    cardEditDto.ConnectCardTypeId = cardTypeId.Value;

                    return cardEditDto;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var rowData = worksheet.GetRow(row);

            try
            {
                return string.IsNullOrWhiteSpace(rowData.GetCell(0)?.StringCellValue)
                       || string.IsNullOrWhiteSpace(rowData.GetCell(1)?.StringCellValue);
            }
            catch
            {
                return true;
            }
        }
    }
}
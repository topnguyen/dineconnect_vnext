﻿using System.Collections.Generic;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Cards.Card.Importing
{
    public interface IConnectCardListExcelDataReader : IApplicationService
    {
        List<ConnectCardEditDto> GetConnectCardFromExcel(byte[] fileBytes);
    }
}
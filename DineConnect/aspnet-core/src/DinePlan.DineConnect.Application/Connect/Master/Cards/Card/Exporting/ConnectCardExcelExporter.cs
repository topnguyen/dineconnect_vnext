﻿using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Cards.Card.Exporting
{
	public class ConnectCardExcelExporter : NpoiExcelExporterBase, IConnectCardExcelExporter
	{
		public ConnectCardExcelExporter(
			ITempFileCacheManager tempFileCacheManager) :
			base(tempFileCacheManager)
		{
		}

		public FileDto ExportToFile(List<ConnectCardListDto> list)
		{
			return CreateExcelPackage(
				"ConnectCardList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("ConnectCard"));

					AddHeader(
						sheet,
						L("Id"),
						L("CardNo"),
						L("ConnectCardTypeName"),
						L("Redeemed"),
						L("CreationTime")
					);

					AddObjects(
						sheet, 2, list,
						_ => _.Id,
						_ => _.CardNo,
						_ => _.ConnectCardTypeName,
						_ => _.Redeemed,
						_ => _.CreationTime
					);

					for (var i = 1; i <= 5; i++)
					{
						sheet.AutoSizeColumn(i);
					}
				});
		}

		public FileDto ExportToFileForImportTemplate()
		{
			return CreateExcelPackage(
				"ImportConnectCards.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("ConnectCard"));

					AddHeader(
						sheet,
						L("CardNo"),
						L("CardType")
					);

					for (var i = 1; i <= 2; i++)
					{
						sheet.AutoSizeColumn(i);
					}
				});
		}

		public FileDto ExportImportCardDataDetails(List<ImportCardDataDetailListDto> dtos)
		{
			return CreateExcelPackage(
				"ImportConnectCards.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("ImportCardDataDetail"));

					AddHeader(
						sheet,
						L("JobName"),
						L("FileName"),
						L("StartDate"),
						L("EndDate"),
						L("Remarks")
						);

					AddObjects(
						sheet, 2, dtos,
						_ => _.JobName,
						_ => _.FileName,
						_ => _.StartTime?.ToString("dd/MM/yyyy hh:mm:ss"),
						_ => _.EndTime?.ToString("dd/MM/yyyy hh:mm:ss"),
						_ => _.Remarks
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.AutoSizeColumn(i);
					}
				});
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos;
using DinePlan.DineConnect.Connect.Master.Cards.CardTypeCategory.Exporting;
using DinePlan.DineConnect.Dto;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardTypeCategory
{
    
    public class ConnectCardTypeCategoryAppService : DineConnectAppServiceBase, IConnectCardTypeCategoryAppService
    {
        private readonly IRepository<ConnectCardTypeCategory> _connectCardTypeCategoryRepo;
        private readonly IConnectCardTypeCategoryExcelExporter _cardTypeCategoryExcelExporter;

        public ConnectCardTypeCategoryAppService(IRepository<ConnectCardTypeCategory> connectCardTypeCategoryRepo,
            IConnectCardTypeCategoryExcelExporter cardTypeCategoryExcelExporter)
        {
            _connectCardTypeCategoryRepo = connectCardTypeCategoryRepo;
            _cardTypeCategoryExcelExporter = cardTypeCategoryExcelExporter;
        }

        public async Task<PagedResultDto<ConnectCardTypeCategoryListDto>> GetAll(GetConnectCardTypeCategoryInput input)
        {
            var allItems = _connectCardTypeCategoryRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                     p => p.Name.ToUpper().Contains(input.Filter.ToUpper()) || p.Code.ToUpper().Contains(input.Filter.ToUpper())
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<ConnectCardTypeCategoryListDto>>(sortMenuItems);

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<ConnectCardTypeCategoryListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetConnectCardTypeCategoryInput input)
        {
            var allList = await GetAll(input);
            
            var allListDtos = ObjectMapper.Map<List<ConnectCardTypeCategoryListDto>>(allList.Items);

            return _cardTypeCategoryExcelExporter.ExportToFile(allListDtos);
        }

        
        public async Task<GetConnectCardTypeCategoryForEditOutput> GetConnectCardTypeCategoryForEdit(NullableIdDto input)
        {
            ConnectCardTypeCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _connectCardTypeCategoryRepo.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<ConnectCardTypeCategoryEditDto>(hDto);
            }
            else
            {
                editDto = new ConnectCardTypeCategoryEditDto();
            }

            return new GetConnectCardTypeCategoryForEditOutput
            {
                ConnectCardTypeCategory = editDto
            };
        }

        public async Task CreateOrUpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            if (input.ConnectCardTypeCategory.Id.HasValue)
            {
                await UpdateConnectCardTypeCategory(input);
            }
            else
            {
                await CreateConnectCardTypeCategory(input);
            }
        }

        
        public async Task DeleteConnectCardTypeCategory(EntityDto input)
        {
            await _connectCardTypeCategoryRepo.DeleteAsync(input.Id);
        }

        
        protected virtual async Task UpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            var item = await _connectCardTypeCategoryRepo.GetAsync(input.ConnectCardTypeCategory.Id.Value);
            
            var dto = input.ConnectCardTypeCategory;
            
            item.Name = dto.Name;
            
            item.Code = dto.Code;

            CheckErrors(await CreateSync(item));
        }

        
        protected virtual async Task CreateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            var dto = ObjectMapper.Map<ConnectCardTypeCategory>(input.ConnectCardTypeCategory);

            CheckErrors(await CreateSync(dto));
        }

        public async Task<ListResultDto<ConnectCardTypeCategoryListDto>> GetConnectCardTypeCategorys()
        {
            var cardTypeCategories = await _connectCardTypeCategoryRepo.GetAllListAsync();

            return new ListResultDto<ConnectCardTypeCategoryListDto>(ObjectMapper.Map<List<ConnectCardTypeCategoryListDto>>(cardTypeCategories));
        }

        public async Task<IdentityResult> CreateSync(ConnectCardTypeCategory connectCardTypeCategory)
        {
            var nameExists = _connectCardTypeCategoryRepo.GetAll()
                .Where(a => a.Name != null && a.Name == connectCardTypeCategory.Name)
                .WhereIf(connectCardTypeCategory.Id != 0, a => a.Id != connectCardTypeCategory.Id);

            var codeExists = _connectCardTypeCategoryRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(connectCardTypeCategory.Code))
                .WhereIf(connectCardTypeCategory.Id != 0, a => a.Id != connectCardTypeCategory.Id);

            //  if the New Addition
            if (nameExists.Any())
            {
                var errors = new List<IdentityError>
                {
                    new IdentityError {Code = "AlreadyExists", Description = $"{L("Category")} {L("Name")} {connectCardTypeCategory.Name} {L("AlreadyExists")}"}
                };

                var success = IdentityResult.Failed(errors.ToArray());

                return success;
            }
            if (codeExists.Any())
            {
                var errors = new List<IdentityError>
                {
                    new IdentityError {Code = "AlreadyExists", Description = $"{L("Category")} {L("Code")} {connectCardTypeCategory.Name} {L("AlreadyExists")}"}
                };

                var success = IdentityResult.Failed(errors.ToArray());

                return success;
            }

            await _connectCardTypeCategoryRepo.InsertOrUpdateAndGetIdAsync(connectCardTypeCategory);

            return IdentityResult.Success;
        }
    }
}

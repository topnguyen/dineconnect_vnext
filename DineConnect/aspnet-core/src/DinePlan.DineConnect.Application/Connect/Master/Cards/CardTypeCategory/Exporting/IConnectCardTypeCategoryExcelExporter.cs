﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardTypeCategory.Exporting
{
  public interface IConnectCardTypeCategoryExcelExporter
    {
        FileDto ExportToFile(List<ConnectCardTypeCategoryListDto> list);
    }
}

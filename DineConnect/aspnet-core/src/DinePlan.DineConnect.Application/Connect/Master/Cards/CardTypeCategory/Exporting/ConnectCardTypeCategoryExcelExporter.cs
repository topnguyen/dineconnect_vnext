﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.Cards.CardTypeCategory.Exporting
{
    public class ConnectCardTypeCategoryExcelExporter : NpoiExcelExporterBase, IConnectCardTypeCategoryExcelExporter
    {
        public ConnectCardTypeCategoryExcelExporter(
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ConnectCardTypeCategoryListDto> list)
        {
            return CreateExcelPackage(
                "ConnectCardTypeCategoryList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ConnectCardTypeCategory"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Code,
                        _ => _.Name
                    );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Exporting
{
    public interface IFutureDateInformationsExcelExporter
    {
        FileDto ExportToFile(List<FutureDateInformationDto> futureDateInformations);
    }
}
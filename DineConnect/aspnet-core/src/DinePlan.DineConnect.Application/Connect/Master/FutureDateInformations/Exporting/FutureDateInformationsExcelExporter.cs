﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Exporting
{
    public class FutureDateInformationsExcelExporter : NpoiExcelExporterBase, IFutureDateInformationsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public FutureDateInformationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<FutureDateInformationDto> futureDateInformations)
        {
            return CreateExcelPackage(
                "FutureDateInformations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("FutureDateInformations"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("FutureDateType"),
                        L("FutureDate"),
                        L("UpdatedDate")
                        );

                    AddObjects(
                        sheet, 2, futureDateInformations,
                        _ => _.Name,
                        _ => _.FutureDateTypeName,
                        _ => _timeZoneConverter.Convert(_.FutureDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _timeZoneConverter.Convert(_.LastModificationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );

                    sheet.AutoSizeColumn(3);
                    sheet.AutoSizeColumn(4);

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
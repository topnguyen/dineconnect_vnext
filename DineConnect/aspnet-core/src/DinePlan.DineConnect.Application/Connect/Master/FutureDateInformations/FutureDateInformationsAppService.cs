﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations
{
    
    public class FutureDateInformationsAppService : DineConnectAppServiceBase, IFutureDateInformationsAppService
    {
        private readonly IRepository<FutureDateInformation> _futureDateInformationRepository;
        private readonly IFutureDateInformationsExcelExporter _futureDateInformationsExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FutureDateInformationsAppService(ConnectSession connectSession,
            IRepository<FutureDateInformation> futureDateInformationRepository,
            IFutureDateInformationsExcelExporter futureDateInformationsExcelExporter,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _connectSession = connectSession;
            _futureDateInformationRepository = futureDateInformationRepository;
            _futureDateInformationsExcelExporter = futureDateInformationsExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<FutureDateInformationDto>> GetAll(GetAllFutureDateInformationsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredFutureDateInformations = _futureDateInformationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDelete)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Contents, input.Filter.ToILikeString()))
                .WhereIf(input.Type.HasValue, p => p.FutureDateType.Equals(input.Type.Value))
                .WhereIf(input.FutureDate.HasValue, p => p.FutureDate == input.FutureDate);

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredFutureDateInformations = FilterByLocations(filteredFutureDateInformations, input);
                }

                var pagedAndFilteredFutureDateInformations = filteredFutureDateInformations
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input);

                var futureDateInformations = ObjectMapper.Map<List<FutureDateInformationDto>>(pagedAndFilteredFutureDateInformations);

                var totalCount = await filteredFutureDateInformations.CountAsync();

                return new PagedResultDto<FutureDateInformationDto>(
                    totalCount,
                    futureDateInformations
                );
            }
        }

        
        public async Task<CreateOrEditFutureDateInformationDto> GetFutureDateInformationForEdit(EntityDto input)
        {
            var futureDateInformation = await _futureDateInformationRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditFutureDateInformationDto>(futureDateInformation);

            GetLocationsForEdit(output, output.LocationGroup);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditFutureDateInformationDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditFutureDateInformationDto input)
        {
            var futureDateInformation = ObjectMapper.Map<FutureDateInformation>(input);

            if (AbpSession.TenantId != null)
            {
                futureDateInformation.TenantId = (int)AbpSession.TenantId;
            }

            await _futureDateInformationRepository.InsertAsync(futureDateInformation);
            UpdateLocations(futureDateInformation, input.LocationGroup);
        }

        
        protected virtual async Task Update(CreateOrEditFutureDateInformationDto input)
        {
            var futureDateInformation = await _futureDateInformationRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, futureDateInformation);
            UpdateLocations(futureDateInformation, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _futureDateInformationRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _futureDateInformationRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _futureDateInformationRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetFutureDateInformationsToExcel(GetAllFutureDateInformationsForExcelInput input)
        {
            var filteredFutureDateInformations = await _futureDateInformationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Contents, input.Filter.ToILikeString()))
                        .ToListAsync();

            var futureDateInformationListDtos = ObjectMapper.Map<List<FutureDateInformationDto>>(filteredFutureDateInformations);

            return _futureDateInformationsExcelExporter.ExportToFile(futureDateInformationListDtos);
        }

        public ListResultDto<ComboboxItemDto> GetFutureDateTypeForLookupTable()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in Enum.GetNames(typeof(FutureDateType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditFutureDateInformationDto input)
        {
            var exists = _futureDateInformationRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }
    }
}
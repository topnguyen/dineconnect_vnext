﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.Terminals.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Terminals.Exporting
{
    public class TerminalsExcelExporter : NpoiExcelExporterBase, ITerminalsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TerminalsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<TerminalDto> terminals)
        {
            return CreateExcelPackage(
                "Terminals.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Terminals"));

                    AddHeader(
                        sheet,
                        L("TerminalCode"),
                        L("Name"),
                        L("Float"),
                        L("Tolerance"),
                        L("Denominations"),
                        L("IsDefault"),
                        L("AutoLogout"),
                        L("IsEndWorkTimeMoneyShown"),
                        L("IsCollectionTerminal"),
                        L("WorkTimeTotalInFloat")
                        );

                    AddObjects(
                        sheet, 2, terminals,
                        _ => _.TerminalCode,
                        _ => _.Name,
                        _ => _.Float,
                        _ => _.Tolerance,
                        _ => _.Denominations,
                        _ => _.IsDefault,
                        _ => _.AutoLogout,
                        _ => _.IsEndWorkTimeMoneyShown,
                        _ => _.IsCollectionTerminal,
                        _ => _.WorkTimeTotalInFloat
                        );

                    for (var i = 1; i <= 12; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Master.Terminals.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Terminals.Exporting
{
    public interface ITerminalsExcelExporter
    {
        FileDto ExportToFile(List<TerminalDto> terminals);
    }
}
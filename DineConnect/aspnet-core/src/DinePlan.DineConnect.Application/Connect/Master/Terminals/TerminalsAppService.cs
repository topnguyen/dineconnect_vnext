﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.Terminals.Dtos;
using DinePlan.DineConnect.Connect.Master.Terminals.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Master.Terminals
{
    
    public class TerminalsAppService : DineConnectAppServiceBase, ITerminalsAppService
    {
        private readonly IRepository<Terminal> _terminalRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITerminalsExcelExporter _terminalsExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;

        public TerminalsAppService(IRepository<Terminal> terminalRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ITerminalsExcelExporter terminalsExcelExporter,
            ConnectSession connectSession,
            ILocationAppService locationAppService)
        {
            _terminalRepository = terminalRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _terminalsExcelExporter = terminalsExcelExporter;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
        }

        public async Task<PagedResultDto<TerminalDto>> GetAll(GetAllTerminalsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredTerminals = _terminalRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter.ToILikeString()), e => false ||
                    EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                    EF.Functions.Like(e.TerminalCode, input.Filter.ToILikeString()) ||
                    EF.Functions.Like(e.Denominations, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredTerminals = FilterByLocations(filteredTerminals, input);
                }

                var pagedAndFilteredTerminals = await filteredTerminals
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var terminals = ObjectMapper.Map<List<TerminalDto>>(pagedAndFilteredTerminals);

                var totalCount = await filteredTerminals.CountAsync();

                return new PagedResultDto<TerminalDto>(
                    totalCount,
                    terminals
                );
            }
        }

        
        public async Task<CreateOrEditTerminalDto> GetTerminalForEdit(EntityDto input)
        {
            var terminal = await _terminalRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditTerminalDto>(terminal);

            GetLocationsForEdit(output, output.LocationGroup);

            if (string.IsNullOrEmpty(terminal.AutoDayCloseSettings))
                output.AutoDayCloses = new List<AutoDayCloseDto>();
            else
                output.AutoDayCloses = JsonConvert.DeserializeObject<List<AutoDayCloseDto>>(terminal.AutoDayCloseSettings);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTerminalDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditTerminalDto input)
        {
            var terminal = ObjectMapper.Map<Terminal>(input);

            if (AbpSession.TenantId != null)
            {
                terminal.TenantId = (int)AbpSession.TenantId;
            }

            terminal.TicketType = null;
            terminal.AutoDayCloseSettings = JsonConvert.SerializeObject(input.AutoDayCloses);

            UpdateLocations(terminal, input.LocationGroup);
            await _terminalRepository.InsertAsync(terminal);
        }

        
        protected virtual async Task Update(CreateOrEditTerminalDto input)
        {
            var terminal = await _terminalRepository.FirstOrDefaultAsync((int)input.Id);

            ObjectMapper.Map(input, terminal);
            terminal.TicketType = null;
            terminal.AutoDayCloseSettings = JsonConvert.SerializeObject(input.AutoDayCloses);
            UpdateLocations(terminal, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _terminalRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _terminalRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _terminalRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetTerminalsToExcel(GetAllTerminalsForExcelInput input)
        {
            var filteredTerminals = await _terminalRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.TerminalCode, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Denominations, input.Filter.ToILikeString()))
                        .ToListAsync();

            var terminalListDtos = ObjectMapper.Map<List<TerminalDto>>(filteredTerminals);

            return _terminalsExcelExporter.ExportToFile(terminalListDtos);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditTerminalDto input)
        {
            var exists = _terminalRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<List<ApiTerminalOutput>> ApiGetTerminals(ApiLocationInput locationInput)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
                if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);

                var repoTerminals = await _terminalRepository.GetAll().Where(a => a.TenantId == locationInput.TenantId && !a.IsDeleted).ToListAsync();

                var allTerminalOutputs = new List<ApiTerminalOutput>();

                if (!string.IsNullOrEmpty(locationInput.Tag))
                {
                    var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                    if (allLocations != null && allLocations.Items.Any())
                        foreach (var terminal in repoTerminals)
                            if (!terminal.Group && !string.IsNullOrEmpty(terminal.Locations) && !terminal.LocationTag)
                            {
                                var scLocations =
                                    JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(terminal.Locations);
                                if (scLocations.Any())
                                {
                                    foreach (var i in scLocations.Select(a => a.Id))
                                        if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                            allTerminalOutputs.Add(new ApiTerminalOutput
                                            {
                                                Id = terminal.Id,
                                                Name = terminal.Name,
                                                TerminalCode = terminal.TerminalCode,
                                                Tid = terminal.Tid,
                                                SerialNumber = terminal.SerialNumber,
                                                IsDefault = terminal.IsDefault,
                                                AutoLogout = terminal.AutoLogout,
                                                Float = terminal.Float,
                                                Tolerance = terminal.Tolerance,
                                                IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                                IsCollectionTerminal = terminal.IsCollectionTerminal,
                                                Denominations = terminal.Denominations,
                                                WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                                Settings = terminal.Settings,
                                                TicketTypeId = terminal.TicketTypeId,
                                                AutoDayClose = terminal.AutoDayClose,
                                                AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                                AcceptTolerance = terminal.AcceptTolerance
                                            });
                                }
                                else
                                {
                                    allTerminalOutputs.Add(new ApiTerminalOutput
                                    {
                                        Id = terminal.Id,
                                        Name = terminal.Name,
                                        TerminalCode = terminal.TerminalCode,
                                        IsDefault = terminal.IsDefault,
                                        Tid = terminal.Tid,
                                        SerialNumber = terminal.SerialNumber,
                                        AutoLogout = terminal.AutoLogout,
                                        Float = terminal.Float,
                                        Tolerance = terminal.Tolerance,
                                        IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                        IsCollectionTerminal = terminal.IsCollectionTerminal,
                                        Denominations = terminal.Denominations,
                                        WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                        Settings = terminal.Settings,
                                        TicketTypeId = terminal.TicketTypeId,
                                        AutoDayClose = terminal.AutoDayClose,
                                        AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                        AcceptTolerance = terminal.AcceptTolerance
                                    });
                                }
                            }
                }
                else if (locationInput.LocationId > 0)
                {
                    foreach (var terminal in repoTerminals)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = locationInput.LocationId,
                            Locations = terminal.Locations,
                            Group = terminal.Group,
                            LocationTag = terminal.LocationTag,
                            NonLocations = terminal.NonLocations
                        }))
                            allTerminalOutputs.Add(new ApiTerminalOutput
                            {
                                Id = terminal.Id,
                                Name = terminal.Name,
                                TerminalCode = terminal.TerminalCode,
                                IsDefault = terminal.IsDefault,
                                AutoLogout = terminal.AutoLogout,
                                Float = terminal.Float,
                                Tid = terminal.Tid,
                                SerialNumber = terminal.SerialNumber,
                                Tolerance = terminal.Tolerance,
                                IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                IsCollectionTerminal = terminal.IsCollectionTerminal,
                                Denominations = terminal.Denominations,
                                WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                Settings = terminal.Settings,
                                TicketTypeId = terminal.TicketTypeId,
                                AutoDayClose = terminal.AutoDayClose,
                                AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                AcceptTolerance = terminal.AcceptTolerance
                            });
                }
                return new List<ApiTerminalOutput>(allTerminalOutputs);
            }
        }
    }
}
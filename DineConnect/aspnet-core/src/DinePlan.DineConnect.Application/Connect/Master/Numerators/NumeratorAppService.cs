﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Numberators;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.Connect.Master.Numerators.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Numerators
{
    
    public class NumeratorAppService : DineConnectAppServiceBase, INumeratorAppService
    {
        private readonly IRepository<Numerator> _numeratorManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly INumeratorExcelExporter _numeratorExcelExporter;
        private readonly ConnectSession _connectSession;

        public NumeratorAppService(IRepository<Numerator> numeratorManager,
            IUnitOfWorkManager unitOfWorkManager,
            INumeratorExcelExporter numeratorExcelExporter,
            ConnectSession connectSession)
        {
            _numeratorManager = numeratorManager;
            _unitOfWorkManager = unitOfWorkManager;
            _numeratorExcelExporter = numeratorExcelExporter;
            _connectSession = connectSession;
        }

        public PagedResultDto<NumeratorListDto> GetAll(NumeratorInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<Numerator> allItems = _numeratorManager.GetAll().Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter));

                var allItemCount = allItems.Count();

                var sortMenuItems = allItems.AsQueryable().ToList();

                var allListDtos = ObjectMapper.Map<List<NumeratorListDto>>(sortMenuItems);

                return new PagedResultDto<NumeratorListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _numeratorManager.GetAll().ToListAsync();

            var allListDtos = ObjectMapper.Map<List<NumeratorListDto>>(allList);

            return await _numeratorExcelExporter.ExportToFile(allListDtos);
        }

        public async Task<GetNumeratorForEditOutput> GetNumeratorForEdit(NullableIdDto input)
        {
            GetNumeratorForEditOutput output = new GetNumeratorForEditOutput();

            var allLocations = new List<ComboboxItemDto>();

            NumeratorEditDto editDto;

            if (input.Id.HasValue)
            {
                var numerator = await _numeratorManager.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<NumeratorEditDto>(numerator);

                //UpdateLocations(numerator, output.LocationGroup);
            }
            else
            {
                editDto = new NumeratorEditDto();
            }

            output.Numerator = editDto;

            if (input.Id == null)
            {
                output.Numerator.NumberFormat = "#";
                output.Numerator.OutputFormat = "[Number]";
            }

            return output;
        }

        public async Task<int> CreateOrUpdateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var output = 0;

            if (input.Numerator.Id.HasValue && input.Numerator.Id > 0)
            {
                await UpdateNumerator(input);
                output = input.Numerator.Id.Value;
            }
            else
            {
                output = await CreateNumerator(input);
            }

            return output;
        }

        
        protected virtual async Task UpdateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var item = await _numeratorManager.GetAsync(input.Numerator.Id.Value);

            var dto = input.Numerator;

            ObjectMapper.Map(dto, item);

            //item.OrganizationId = _connectSession.OrgId;

            item.Name = input.Numerator.Name;

            item.NumberFormat = input.Numerator.NumberFormat;

            item.OutputFormat = input.Numerator.OutputFormat;

            item.ResetNumerator = input.Numerator.ResetNumerator;

            item.ResetNumeratorType = input.Numerator.ResetNumeratorType;
        }

        
        protected virtual async Task<int> CreateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var item = ObjectMapper.Map<Numerator>(input.Numerator);

            //item.OrganizationId = _connectSession.OrgId;

            item.Name = input.Numerator.Name;

            item.NumberFormat = input.Numerator.NumberFormat;

            item.OutputFormat = input.Numerator.OutputFormat;

            return await _numeratorManager.InsertAndGetIdAsync(item);
        }

        
        public async Task DeleteNumerator(EntityDto input)
        {
            await _numeratorManager.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _numeratorManager.GetAsync(input.Id);

                item.IsDeleted = false;

                await _numeratorManager.UpdateAsync(item);
            }
        }

        public ListResultDto<ComboboxItemDto> GetResetNumeratorTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;

            foreach (var name in Enum.GetNames(typeof(ResetNumeratorType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Numerators.Exporting
{
    public interface INumeratorExcelExporter
    {
        Task<FileDto> ExportToFile(List<NumeratorListDto> promotions);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Master.Numerators.Exporting
{
    public class NumeratorExcelExporter : NpoiExcelExporterBase, INumeratorExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public NumeratorExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<NumeratorListDto> list)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "Numerator.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Numerator"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("NumberFormat"),
                        L("OutputFormat")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.NumberFormat,
                        _ => _.OutputFormat
                    );

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
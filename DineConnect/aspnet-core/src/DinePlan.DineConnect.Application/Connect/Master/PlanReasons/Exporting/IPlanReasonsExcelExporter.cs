﻿using DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Exporting
{
    public interface IPlanReasonsExcelExporter
    {
        FileDto ExportToFile(List<PlanReasonDto> planReasons);
    }
}
﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Exporting
{
    public class PlanReasonsExcelExporter : NpoiExcelExporterBase, IPlanReasonsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PlanReasonsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<PlanReasonDto> planReasons)
        {
            return CreateExcelPackage(
                "PlanReasons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PlanReasons"));

                    AddHeader(
                        sheet,
                        L("Reason"),
                        L("AliasReason"),
                        L("ReasonGroup"),
                        L("UpdatedDate")

                        );

                    AddObjects(
                        sheet, 2, planReasons,
                        _ => _.Reason,
                        _ => _.AliasReason,
                        _ => _.ReasonGroup,
                        _ => _.LastModificationTime?.ToString("dd-MM-yyy")
                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
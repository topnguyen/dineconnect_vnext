﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos;
using DinePlan.DineConnect.Connect.Master.PlanReasons.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons
{
    
    public class PlanReasonsAppService : DineConnectAppServiceBase, IPlanReasonsAppService
    {
        private readonly IRepository<PlanReason> _planReasonRepository;
        private readonly IPlanReasonsExcelExporter _planReasonsExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PlanReasonsAppService(IRepository<PlanReason> planReasonRepository, IPlanReasonsExcelExporter planReasonsExcelExporter, ConnectSession connectSession,
            ILocationAppService locationAppService, IUnitOfWorkManager unitOfWorkManager)
        {
            _planReasonRepository = planReasonRepository;
            _planReasonsExcelExporter = planReasonsExcelExporter;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<PlanReasonDto>> GetAll(GetAllPlanReasonsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredPlanReasons = _planReasonRepository.GetAll()
                        .Where(x => x.IsDeleted == input.IsDeleted)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false
                        || EF.Functions.Like(e.Reason, input.Filter.ToILikeString())
                        || EF.Functions.Like(e.AliasReason, input.Filter.ToILikeString())
                        || EF.Functions.Like(e.ReasonGroup, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredPlanReasons = FilterByLocations(filteredPlanReasons, input);
                }

                var pagedAndFilteredPlanReasons = await filteredPlanReasons
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                    .ToListAsync();

                var planReasons = ObjectMapper.Map<List<PlanReasonDto>>(pagedAndFilteredPlanReasons);

                var totalCount = await filteredPlanReasons.CountAsync();

                return new PagedResultDto<PlanReasonDto>(
                    totalCount,
                    planReasons
                );
            }
        }

        
        public async Task<CreateOrEditPlanReasonDto> GetPlanReasonForEdit(EntityDto input)
        {
            var planReason = await _planReasonRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditPlanReasonDto>(planReason);

            var recordExists = ReasonGroup.ReasonGroups.Contains(output.ReasonGroup);
            if (recordExists)
            {
                output.ReasonGroupCheckBox = false;
            }
            else
            {
                output.ReasonGroupCheckBox = true;
            }

            GetLocationsForEdit(output, output.LocationGroup);
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPlanReasonDto input)
        {
            ValidateForCreateOrUpdate(input);

            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditPlanReasonDto input)
        {
            var planReason = ObjectMapper.Map<PlanReason>(input);
            planReason.LastModificationTime = Clock.Now;

            if (AbpSession.TenantId != null)
            {
                planReason.TenantId = (int)AbpSession.TenantId;
            }

            UpdateLocations(planReason, input.LocationGroup);
            await _planReasonRepository.InsertAsync(planReason);
        }

        
        protected virtual async Task Update(CreateOrEditPlanReasonDto input)
        {
            var planReason = await _planReasonRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, planReason);

            UpdateLocations(planReason, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _planReasonRepository.DeleteAsync(input.Id);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _planReasonRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _planReasonRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetPlanReasonsToExcel(GetAllPlanReasonsForExcelInput input)
        {
            var filteredPlanReasons = await _planReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Reason, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.AliasReason, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.ReasonGroup, input.Filter.ToILikeString()))
                        .ToListAsync();

            var planReasonListDtos = ObjectMapper.Map<List<PlanReasonDto>>(filteredPlanReasons);

            return _planReasonsExcelExporter.ExportToFile(planReasonListDtos);
        }

        public ListResultDto<ComboboxItemDto> GetReasonGroupForLookupTable()
        {
            var returnList = new List<ComboboxItemDto>();
            foreach (var name in ReasonGroup.ReasonGroups)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = name
                });
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditPlanReasonDto input)
        {
            var exists = _planReasonRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Reason.Equals(input.Reason)))
            {
                throw new UserFriendlyException("ReasonAlreadyExists");
            }
        }

        public async Task<List<ApiPlanReasonOutput>> ApiGetPlanReasons(ApiLocationInput locationInput)
        {
            //var lastSyncTime = locationInput.TimeZone != null
            //     ? TimeZoneInfo.ConvertTime(locationInput.LastSyncTime, locationInput.TimeZone, TimeZoneInfo.Local)
            //     : locationInput.LastSyncTime;

            //var localRepoTypes = await _planReasonRepo.GetAll()
            //    .Where(i =>  (i.LastModificationTime != null && i.LastModificationTime > lastSyncTime) ||
            //    (i.LastModificationTime == null && i.CreationTime > lastSyncTime))
            //    .ToListAsync();

            var localRepoTypes = await _planReasonRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            List<ApiPlanReasonOutput> allPlanReasonOutputs = new List<ApiPlanReasonOutput>();
            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var sortL in localRepoTypes)
                        if (!sortL.Group && !string.IsNullOrEmpty(sortL.Locations) &&
                            !sortL.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(sortL.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                                        {
                                            Reason = sortL.Reason,
                                            AliasReason = sortL.AliasReason,
                                            ReasonGroup = sortL.ReasonGroup,
                                            Id = sortL.Id
                                        });
                        }
                        else
                        {
                            allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                            {
                                Reason = sortL.Reason,
                                AliasReason = sortL.AliasReason,
                                ReasonGroup = sortL.ReasonGroup,
                                Id = sortL.Id
                            });
                        }
            }
            else if (locationInput.LocationId > 0)
            {
                foreach (var sortL in localRepoTypes)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = sortL.Locations,
                        Group = sortL.Group,
                        NonLocations = sortL.NonLocations,
                        LocationTag = sortL.LocationTag
                    }))
                    {
                        allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                        {
                            Reason = sortL.Reason,
                            AliasReason = sortL.AliasReason,
                            ReasonGroup = sortL.ReasonGroup,
                            Id = sortL.Id
                        });
                    }
                }
            }

            return new List<ApiPlanReasonOutput>(allPlanReasonOutputs);
        }
    }
}
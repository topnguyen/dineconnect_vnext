﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Connect.Master.PriceTags.Exporting;
using DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.PriceTags
{
    
    public class PriceTagsAppService : DineConnectAppServiceBase, IPriceTagsAppService
    {
        private readonly IRepository<PriceTag> _priceTagRepository;
        private readonly IRepository<FutureDateInformation> _futureDateRepository;
        private readonly IRepository<PriceTagDefinition> _priceTagDefineRepository;
        private readonly IPriceTagsExcelExporter _priceTagsExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ITagPriceListExcelDataReader _tagPriceListExcelDataReader;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<PriceTagLocationPrice> _pricetagLocationPriceRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PriceTagsAppService(
            IRepository<PriceTag> priceTagRepository,
            IRepository<FutureDateInformation> futureDateRepository,
            IRepository<PriceTagDefinition> priceTagDefineRepository
            , IPriceTagsExcelExporter priceTagsExcelExporter
            , ConnectSession connectSession
            , IRepository<MenuItem> menuItemRepository
            , ITempFileCacheManager tempFileCacheManager
            , IRepository<MenuItemPortion> menuItemPortionRepository
            , ITagPriceListExcelDataReader tagPriceListExcelDataReader
            , IAppNotifier appNotifier
            , IRepository<PriceTagLocationPrice> pricetagLocationPriceRepository
            , ILocationAppService locationAppService
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _priceTagRepository = priceTagRepository;
            _futureDateRepository = futureDateRepository;
            _priceTagDefineRepository = priceTagDefineRepository;
            _priceTagsExcelExporter = priceTagsExcelExporter;
            _connectSession = connectSession;
            _menuItemRepository = menuItemRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _menuItemPortionRepository = menuItemPortionRepository;
            _tagPriceListExcelDataReader = tagPriceListExcelDataReader;
            _appNotifier = appNotifier;
            _pricetagLocationPriceRepository = pricetagLocationPriceRepository;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<PriceTagDto>> GetAll(GetAllPriceTagsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredPriceTags = _priceTagRepository.GetAll()
                        .Where(x => x.IsDeleted == input.IsDeleted)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    filteredPriceTags = FilterByLocations(filteredPriceTags, input);
                }

                var pagedAndFilteredPriceTags = filteredPriceTags
                    .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input);

                var priceTags = ObjectMapper.Map<List<PriceTagDto>>(pagedAndFilteredPriceTags);

                var totalCount = await filteredPriceTags.CountAsync();

                return new PagedResultDto<PriceTagDto>(
                    totalCount,
                    priceTags
                );
            }
        }

        
        public async Task<CreateOrEditPriceTagDto> GetPriceTagForEdit(EntityDto input)
        {
            var priceTag = await _priceTagRepository.FirstOrDefaultAsync(input.Id);

            var output = ObjectMapper.Map<CreateOrEditPriceTagDto>(priceTag);

            GetLocationsForEdit(output, output.LocationGroup);

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPriceTagDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditPriceTagDto input)
        {
            var priceTag = ObjectMapper.Map<PriceTag>(input);

            if (AbpSession.TenantId != null)
            {
                priceTag.TenantId = (int)AbpSession.TenantId;
            }
            UpdateLocations(priceTag, input.LocationGroup);

            await _priceTagRepository.InsertAsync(priceTag);
        }

        
        protected virtual async Task Update(CreateOrEditPriceTagDto input)
        {
            var priceTag = await _priceTagRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, priceTag);

            UpdateLocations(priceTag, input.LocationGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _priceTagRepository.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _priceTagRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _priceTagRepository.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetPriceTagsToExcel(GetAllPriceTagsForExcelInput input)
        {
            var filteredPriceTags = await _priceTagRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Departments, input.Filter.ToILikeString()))
                        .ToListAsync();

            var priceTagListDtos = ObjectMapper.Map<List<PriceTagDto>>(filteredPriceTags);

            return _priceTagsExcelExporter.ExportToFile(priceTagListDtos);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditPriceTagDto input)
        {
            var exists = _priceTagRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<PagedResultDto<TagMenuItemPortionDto>> GetMenuPricesForTag(GetPriceTagInput input)
        {
            if (input.TagId > 0)
            {
                var allItems = _menuItemRepository
                    .GetAll()
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter)
                    );

                var query = allItems.SelectMany(a => a.Portions)
                    .Select(mip => new TagMenuItemPortionDto
                    {
                        Id = mip.Id,
                        MenuName = mip.MenuItem.Name,
                        PortionName = mip.Name,
                        MenuItemId = mip.MenuItemId,
                        MultiPlier = mip.MultiPlier,
                        TagId = input.TagId,
                        Price = mip.Price,
                        TagPrice =
                        mip.PriceTags.Any(a => a.TagId.Equals(input.TagId))
                            ? mip.PriceTags.FirstOrDefault(a => a.TagId.Equals(input.TagId)).Price
                            : 0M
                    }).WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.MenuName.Contains(input.Filter) || p.PortionName.Contains(input.Filter)
                    );

                var allItemCount = await query.CountAsync();

                var result = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                if (input.FutureDataId.HasValue && input.FutureDataId > 0)
                {
                    var futureData = await _futureDateRepository.GetAsync(input.FutureDataId.Value);

                    var futureTagPrices = new List<TagPriceInput>();
                    if (!futureData.ObjectContents.IsNullOrEmpty())
                    {
                        futureTagPrices = JsonConvert.DeserializeObject<List<TagPriceInput>>(futureData.ObjectContents);
                    }
                    foreach (var futurelst in futureTagPrices)
                    {
                        var pricetaglst = result.FirstOrDefault(t => t.TagId == futurelst.TagId && t.Id == futurelst.PortionId);
                        if (pricetaglst != null)
                        {
                            pricetaglst.TagId = futurelst.TagId;
                            pricetaglst.Id = futurelst.PortionId;
                            pricetaglst.TagPrice = futurelst.Price;
                        }
                    }
                }

                return new PagedResultDto<TagMenuItemPortionDto>(
                    allItemCount,
                    result
                );
            }

            return null;
        }

        public async Task<FileDto> GetMenuPricesForTagToExcel(int tagId)
        {
            if (tagId > 0)
            {
                var priceTag = await _priceTagRepository.GetAsync(tagId);
                var allItems = _menuItemRepository
                    .GetAll();

                var query = allItems.SelectMany(a => a.Portions)
                    .Select(mip => new TagMenuItemPortionDto
                    {
                        Id = mip.Id,
                        MenuName = mip.MenuItem.Name,
                        PortionName = mip.Name,
                        MenuItemId = mip.MenuItemId,
                        MultiPlier = mip.MultiPlier,
                        TagId = tagId,
                        TagName = priceTag.Name,
                        Price = mip.Price,
                        TagPrice =
                        mip.PriceTags.Any(a => a.TagId.Equals(tagId))
                            ? mip.PriceTags.FirstOrDefault(a => a.TagId.Equals(tagId)).Price
                            : 0M
                    });

                var result = await query.ToListAsync();

                return _priceTagsExcelExporter.ExportMenuPricesForTagToFile(result);
            }

            return null;
        }

        private async Task UpdateTagPriceFromExcel(ImportPriceTagDto input)
        {
            var menu = await _menuItemRepository.GetAll().FirstOrDefaultAsync(e => e.Name == input.MenuName);

            if (menu == null)
            {
                throw new Exception(L("MenuItemNotFound"));
            }

            var portion = await _menuItemPortionRepository.GetAll().FirstOrDefaultAsync(x => x.Name == input.PortionName && x.MenuItemId == menu.Id);

            if (portion == null)
            {
                throw new Exception($"{L("PortionNotFoundIn")} {menu.Name}");
            }

            portion.Price = input.Price;

            await _menuItemPortionRepository.UpdateAsync(portion);

            var tag = await _priceTagRepository.FirstOrDefaultAsync(input.TagId);
            if (tag == null)
            {
                return;
            }

            var priceTag = await _priceTagDefineRepository.GetAll().Where(p => p.MenuPortionId == portion.Id).FirstOrDefaultAsync(a => a.TagId.Equals(tag.Id));

            if (priceTag != null)
            {
                priceTag.Price = input.TagPrice;

                await _priceTagDefineRepository.UpdateAsync(priceTag);
            }
            else
            {
                var priceTagDefine = new PriceTagDefinition
                {
                    TagId = input.TagId,
                    Price = input.TagPrice,
                    MenuPortionId = portion.Id
                };

                await _priceTagDefineRepository.InsertAsync(priceTagDefine);
            }
            await CurrentUnitOfWork.SaveChangesAsync();

            await UpdateTagPrice(new TagPriceInput
            {
                Price = input.TagPrice,
                FutureDataId = input.FutureDataId,
                PortionId = portion.Id,
                TagId = input.TagId
            });
        }

        public async Task UpdateTagPrice(TagPriceInput input)
        {
            if (input.FutureDataId.HasValue && input.FutureDataId > 0)
            {
                var futureData = await _futureDateRepository.GetAsync(input.FutureDataId.Value);

                var futureTagPrices = new List<TagPriceInput>();

                if (!futureData.ObjectContents.IsNullOrEmpty())
                {
                    futureTagPrices = JsonConvert.DeserializeObject<List<TagPriceInput>>(futureData.ObjectContents);
                }

                var i = 0;

                foreach (var item in futureTagPrices)
                {
                    if (item.PortionId == input.PortionId)
                    {
                        item.Price = input.Price;
                        item.TagId = input.TagId;
                        i++;
                    }
                }

                if (i == 0)
                {
                    futureTagPrices.Add(input);
                }

                futureData.ObjectContents = JsonConvert.SerializeObject(futureTagPrices);

                await _futureDateRepository.UpdateAsync(futureData);
            }
            else
            {
                var portion = await _menuItemPortionRepository.GetAllIncluding(e => e.PriceTags).FirstOrDefaultAsync(e => e.Id.Equals(input.PortionId));
                var locationPrice = portion.PriceTags?.SingleOrDefault(a => a.TagId.Equals(input.TagId));

                if (locationPrice != null)
                    locationPrice.Price = input.Price;
                else
                    await _priceTagDefineRepository.InsertAsync(new PriceTagDefinition
                    {
                        TagId = input.TagId,
                        Price = input.Price,
                        MenuPortionId = portion.Id
                    });

                //await _menuItemPortionRepository.UpdateAsync(portion);
            }
        }

        public async Task<FileDto> GetImportTemplate(int tagId)
        {
            if (tagId > 0)
            {
                return await GetMenuPricesForTagToExcel(tagId);
            }

            return null;
        }

        private int GetTagIdByName(string tagName)
        {
            var allList = _priceTagRepository.GetAllList(a => a.Name.Equals(tagName));
            if (allList.Any()) return allList.FirstOrDefault().Id;
            return 0;
        }

        public async Task ImportPriceTagToDatabase(string fileToken, int? futureDataId)
        {
            if (!fileToken.IsNullOrWhiteSpace())
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                var tagPrices = _tagPriceListExcelDataReader.GetTagPricesFromExcel(fileBytes);

                var invalidPriceTags = tagPrices.Where(x => !string.IsNullOrWhiteSpace(x.Exception)).ToList();

                tagPrices = tagPrices.Except(invalidPriceTags).ToList();

                foreach (var tagPrice in tagPrices)
                {
                    tagPrice.FutureDataId = futureDataId;

                    try
                    {
                        await UpdateTagPriceFromExcel(tagPrice);
                    }
                    catch (Exception ex)
                    {
                        tagPrice.Exception = ex.Message;
                        invalidPriceTags.Add(tagPrice);
                    }
                }

                await ProcessImportPriceTagsResultAsync(invalidPriceTags.ToList());
            }
        }

        public async Task<List<PriceTagsForComboboxDto>> GetPriceTagsForCombobox(int? selectedId)
        {
            var priceTags = await _priceTagRepository
               .GetAllListAsync();

            var priceTagsItems = new List<PriceTagsForComboboxDto>(priceTags.Select(x =>
                new PriceTagsForComboboxDto(x.Id.ToString(), x.Name, false)));

            if (priceTagsItems.Count > 0)
            {
                if (selectedId.HasValue)
                {
                    var selectedItem = priceTagsItems.FirstOrDefault(x => x.Value == selectedId.Value.ToString());
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        priceTagsItems[0].IsSelected = true;
                    }
                }
            }

            return priceTagsItems;
        }

        public async Task<PagedResultDto<PriceTagLocationPriceListDto>> GetAllPriceTagLocationPrice(GetPriceTagInput input)
        {
            var pricetagDefinition = _priceTagDefineRepository.GetAll().FirstOrDefault(x => x.TagId == input.TagId && x.MenuPortionId == input.MenuItemPortionId);
            int priceTagDefinitionId;
            if(pricetagDefinition == null)
            {
                priceTagDefinitionId = _priceTagDefineRepository.InsertAndGetId(new PriceTagDefinition()
                {
                    TagId = input.TagId,
                    MenuPortionId = input.MenuItemPortionId
                });
            } else
            {
                priceTagDefinitionId = pricetagDefinition.Id;
            }
            var allItems = _pricetagLocationPriceRepository.GetAll()
                .Include(p => p.Location)
                .Where(t => t.PriceTagDefinitionId == priceTagDefinitionId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Location.Name.Contains(input.Filter));

            var allItemCount = await allItems.CountAsync();

            var sortMenuItems = await allItems
                    .PageBy(input)
                    .OrderBy(input.Sorting)
                    .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<PriceTagLocationPriceListDto>>(sortMenuItems);

            return new PagedResultDto<PriceTagLocationPriceListDto>(
                allItemCount,
                allListDtos
            );
        }

        public async Task CreatePriceTagLocationPrice(PriceTagLocationPriceEditDto input)
        {
            if (input.Id.HasValue)
            {
                var pricetaglocationprice = input;
                var existEntry = await _pricetagLocationPriceRepository.FirstOrDefaultAsync(t => t.Id == input.Id.Value);
                ObjectMapper.Map(input, existEntry);
                existEntry.Location = null;
                await _pricetagLocationPriceRepository.UpdateAsync(existEntry);
            }
            else
            {
                var dto = ObjectMapper.Map<PriceTagLocationPrice>(input);
                dto.Location = null;
                await _pricetagLocationPriceRepository.InsertAndGetIdAsync(dto);
            }
        }

        public async Task<bool> CheckExistLocationPrice(PriceTagLocationPriceEditDto input)
        {
            var existEntry = _pricetagLocationPriceRepository.GetAll()
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id)
                .Where(t => t.LocationId == input.LocationId && t.PriceTagDefinitionId == input.PriceTagDefinitionId);

            return await existEntry.AnyAsync();
        }

        public async Task DeletePriceTagLocationPrice(EntityDto input)
        {
            await _pricetagLocationPriceRepository.DeleteAsync(input.Id);
        }

        private async Task ProcessImportPriceTagsResultAsync(List<ImportPriceTagDto> invalidUsers)
        {
            if (invalidUsers.Any())
            {
                var file = _priceTagsExcelExporter.ExportMenuPricesForTagInvalidToFile(invalidUsers);
                await _appNotifier.SomeLocationsCouldntBeImported(AbpSession.ToUserIdentifier(), file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    AbpSession.ToUserIdentifier(),
                    new LocalizableString("AllPriceTagsSuccessfullyImportedFromExcel", DineConnectConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }

        public async Task<TagPriceOutputDto> ApiGetTags(ApiLocationInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new List<TagPriceDto>();

            var rOutput = new TagPriceOutputDto
            {
                Tags = output
            };

            var listP = await _priceTagRepository.GetAllListAsync(a => a.TenantId == input.TenantId);

            if (!string.IsNullOrEmpty(input.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(input.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var repoTag in listP)
                        if (!repoTag.Group && !string.IsNullOrEmpty(repoTag.Locations) && !repoTag.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(repoTag.Locations);
                            if (scLocations.Any())
                            {
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                    {
                                        var tPrice = new TagPriceDto
                                        {
                                            Id = repoTag.Id,
                                            Name = repoTag.Name,
                                            Type = repoTag.Type,
                                            StartDate = repoTag.StartDate,
                                            EndDate = repoTag.EndDate
                                        };
                                        var pDefinitions = await _priceTagDefineRepository.GetAllListAsync(a => a.TagId == repoTag.Id);
                                        if (pDefinitions.Any())
                                        {
                                            tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                                            {
                                                TagName = a.PriceTag.Name,
                                                MenuItemPortionId = a.MenuPortionId,
                                                Price = a.Price,
                                                TagId = a.Id
                                            }).ToList();
                                            output.Add(tPrice);
                                        }
                                    }
                            }
                            else
                            {
                                var tPrice = new TagPriceDto
                                {
                                    Id = repoTag.Id,
                                    Name = repoTag.Name,
                                    Type = repoTag.Type,
                                    StartDate = repoTag.StartDate,
                                    EndDate = repoTag.EndDate
                                };
                                var pDefinitions = await _priceTagDefineRepository.GetAllListAsync(a => a.TagId == repoTag.Id);
                                if (pDefinitions.Any())
                                {
                                    tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                                    {
                                        TagName = a.PriceTag.Name,
                                        MenuItemPortionId = a.MenuPortionId,
                                        Price = a.Price,
                                        TagId = a.Id
                                    }).ToList();
                                    output.Add(tPrice);
                                }
                            }
                        }
            }
            else
            {
                foreach (var priceTag in listP)
                {
                    var fetch = await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = priceTag.Locations,
                        Group = priceTag.Group,
                        NonLocations = priceTag.NonLocations,
                        LocationTag = priceTag.LocationTag
                    });
                    if (fetch)
                    {
                        var tPrice = new TagPriceDto
                        {
                            Id = priceTag.Id,
                            Name = priceTag.Name,
                            Type = priceTag.Type,
                            StartDate = priceTag.StartDate,
                            EndDate = priceTag.EndDate
                        };
                        var pDefinitions = await _priceTagDefineRepository.GetAllListAsync(a => a.TagId == priceTag.Id);
                        if (pDefinitions.Any())
                        {
                            tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                            {
                                TagName = a.PriceTag.Name,
                                MenuItemPortionId = a.MenuPortionId,
                                Price = a.Price,
                                TagId = a.Id
                            }).ToList();
                            output.Add(tPrice);
                        }
                    }
                }
            }

            var myLocationTags = _pricetagLocationPriceRepository.GetAll()
                .Where(a => a.LocationId == input.LocationId);
            var myTagPriceCount = myLocationTags.Count(a => a.LocationId == input.LocationId);
            if (myTagPriceCount > 0)
            {
                foreach (var tagDto in rOutput.Tags.SelectMany(a => a.Definitions))
                {
                    var myTag = myLocationTags.Where(a => a.PriceTagDefinitionId == tagDto.TagId);
                    if (myTag.Any())
                    {
                        var myTagList = myTag.ToList();
                        if (myTagList.Any())
                            tagDto.Price = myTagList.Last().Price;
                    }
                }
            }

            return rOutput;
        }

        public PriceTagDefinitionDto GetPriceTagDefinition(GetPriceTagInput input)
        {
            var priceTagDefinition = _priceTagDefineRepository.GetAll().FirstOrDefault(x => x.TagId == input.TagId && x.MenuPortionId == input.MenuItemPortionId);
            return priceTagDefinition != null ? new PriceTagDefinitionDto() { 
                Id = priceTagDefinition.Id,
                MenuPortionId = priceTagDefinition.MenuPortionId,
                TagId = priceTagDefinition.TagId,
                Price = priceTagDefinition.Price
            } : null;
        }
    }
}
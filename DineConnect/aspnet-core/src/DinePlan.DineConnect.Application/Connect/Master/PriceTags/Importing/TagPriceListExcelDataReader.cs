﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PriceTags
{
	public class TagPriceListExcelDataReader : NpoiExcelImporterBase<ImportPriceTagDto>, ITagPriceListExcelDataReader
	{
		private readonly IRepository<PriceTag> _priceTagRepository;

		public TagPriceListExcelDataReader(IRepository<PriceTag> priceTagRepository)
		{
			_priceTagRepository = priceTagRepository;
		}

		public List<ImportPriceTagDto> GetTagPricesFromExcel(byte[] fileBytes)
		{
			return ProcessExcelFile(fileBytes, ProcessExcelRow);
		}

		private ImportPriceTagDto ProcessExcelRow(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var tagPrice = new ImportPriceTagDto();

			try
			{
				tagPrice.TagId = (int)worksheet.GetRow(row).GetCell(1).NumericCellValue;
				tagPrice.MenuName = worksheet.GetRow(row).GetCell(2).StringCellValue;
				tagPrice.PortionName = worksheet.GetRow(row).GetCell(3).StringCellValue;
				tagPrice.Price = decimal.Parse(worksheet.GetRow(row).GetCell(4).NumericCellValue.ToString());
				tagPrice.TagPrice = decimal.Parse(worksheet.GetRow(row).GetCell(5).NumericCellValue.ToString());
			}
			catch (Exception exception)
			{
				tagPrice.Exception = exception.Message;
			}

			return tagPrice;
		}

		private bool IsRowEmpty(ISheet worksheet, int row)
		{
			return worksheet.GetRow(row).GetCell(1).NumericCellValue <= 0;
		}

		private int GetTagIdByName(string tagName)
		{
			var allList = _priceTagRepository.GetAllList(a => a.Name.Equals(tagName));
			if (allList.Any()) return allList.FirstOrDefault().Id;
			return 0;
		}
	}
}
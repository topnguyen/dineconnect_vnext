﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto
{
    public class ImportPriceTagDto
    {
        public int Id { get; set; }
        public int TagId { get; set; }
        public string TagName { get; set; }
        public string MenuName { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public decimal TagPrice { get; set; }
        public string Exception { get; set; }
        public int? FutureDataId { get; set; }
    }
}

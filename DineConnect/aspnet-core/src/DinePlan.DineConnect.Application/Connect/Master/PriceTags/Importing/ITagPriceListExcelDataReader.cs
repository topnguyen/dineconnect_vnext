﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PriceTags
{
    public interface ITagPriceListExcelDataReader : IApplicationService
    {
        List<ImportPriceTagDto> GetTagPricesFromExcel(byte[] fileBytes);
    }
}
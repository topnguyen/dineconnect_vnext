﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Exporting
{
    public class PriceTagsExcelExporter : NpoiExcelExporterBase, IPriceTagsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PriceTagsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<PriceTagDto> priceTags)
        {
            return CreateExcelPackage(
                "PriceTags.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PriceTags"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, priceTags,
                        _ => _.Name
                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportMenuPricesForTagToFile(List<TagMenuItemPortionDto> priceTags)
        {
            return CreateExcelPackage(
                "PriceTags.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PriceTags"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("TagId"),
                        L("MenuName"),
                        L("PortionName"),
                        L("Price"),
                        L("TagPrice")
                        );

                    AddObjects(
                        sheet, 2, priceTags,
                         _ => _.Id,
                        _ => _.TagId,
                        _ => _.MenuName,
                        _ => _.PortionName,
                        _ => double.Parse(_.Price.ToString()),
                        _ => double.Parse(_.TagPrice.ToString())
                        );

                    for (var i = 1; i <= 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportMenuPricesForTagInvalidToFile(List<ImportPriceTagDto> importPriceTagDtos)
        {
            return CreateExcelPackage(
                "InvalidPriceTags.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PriceTags"));

                    AddHeader(
                       sheet,
                       L("Id"),
                       L("TagName"),
                       L("MenuName"),
                       L("PortionName"),
                       L("Price"),
                       L("TagPrice"),
                       L("RefuseReason")
                       );

                    AddObjects(
                        sheet, 2, importPriceTagDtos,
                         _ => _.Id,
                        _ => _.TagName,
                        _ => _.MenuName,
                        _ => _.PortionName,
                        _ => double.Parse(_.Price.ToString()),
                        _ => double.Parse(_.TagPrice.ToString()),
                        _ => _.Exception
                        );



                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
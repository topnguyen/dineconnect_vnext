﻿using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Connect.Master.PriceTags.Importing.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Exporting
{
    public interface IPriceTagsExcelExporter
    {
        FileDto ExportMenuPricesForTagToFile(List<TagMenuItemPortionDto> priceTags);
        FileDto ExportToFile(List<PriceTagDto> priceTags);
        FileDto ExportMenuPricesForTagInvalidToFile(List<ImportPriceTagDto> importPriceTagDtos);
    }
}
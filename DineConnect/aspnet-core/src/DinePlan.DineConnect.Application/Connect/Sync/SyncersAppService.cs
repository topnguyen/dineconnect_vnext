﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Logging;
using Castle.Core.Logging;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Sync
{
	[AbpAuthorize(AppPermissions.Pages_Syncers)]
	public class SyncersAppService : DineConnectAppServiceBase, ISyncersAppService
	{
		private readonly IRepository<Syncer> _syncerRepository;
		private readonly IRepository<LastSync> _lastSyncRepository;
        private readonly IRepository<LocationSyncer> _locationSyncerRepository;
		private readonly ILogger _logger;

		public SyncersAppService(IRepository<Syncer> syncerRepository,
			IRepository<LastSync> lastSyncRepository,
			ILogger logger,
			IRepository<LocationSyncer> locationSyncerRepository)
		{
			_syncerRepository = syncerRepository;
			_lastSyncRepository = lastSyncRepository;
			_logger = logger;
			_locationSyncerRepository = locationSyncerRepository;
		}

		public async Task<PagedResultDto<GetSyncerForViewDto>> GetAll(GetAllSyncersInput input)
		{
			var filteredSyncers = _syncerRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

			var pagedAndFilteredSyncers = filteredSyncers
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var syncers = from o in pagedAndFilteredSyncers
						  select new
						  {
							  o.Name,
							  o.SyncerGuid,
							  Id = o.Id
						  };

			var totalCount = await filteredSyncers.CountAsync();

			var dbList = await syncers.ToListAsync();
			var results = new List<GetSyncerForViewDto>();

			foreach (var o in dbList)
			{
				var res = new GetSyncerForViewDto()
				{
					Syncer = new SyncerDto
					{
						Name = o.Name,
						SyncerGuid = o.SyncerGuid,
						Id = o.Id,
					}
				};

				results.Add(res);
			}

			return new PagedResultDto<GetSyncerForViewDto>(
				totalCount,
				results
			);
		}

		public async Task<GetSyncerForViewDto> GetSyncerForView(int id)
		{
			var syncer = await _syncerRepository.GetAsync(id);

			var output = new GetSyncerForViewDto { Syncer = ObjectMapper.Map<SyncerDto>(syncer) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_Syncers_Edit)]
		public async Task<GetSyncerForEditOutput> GetSyncerForEdit(EntityDto input)
		{
			var syncer = await _syncerRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetSyncerForEditOutput { Syncer = ObjectMapper.Map<CreateOrEditSyncerDto>(syncer) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditSyncerDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		protected virtual async Task Create(CreateOrEditSyncerDto input)
		{
			var syncer = ObjectMapper.Map<Syncer>(input);

			if (AbpSession.TenantId != null)
			{
				syncer.TenantId = (int)AbpSession.TenantId;
			}

			await _syncerRepository.InsertAsync(syncer);
		}

		protected virtual async Task Update(CreateOrEditSyncerDto input)
		{
			var syncer = await _syncerRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, syncer);
		}

		[AbpAuthorize(AppPermissions.Pages_Syncers_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _syncerRepository.DeleteAsync(input.Id);
		}

		public async Task<ListResultDto<ApiSyncerOutput>> ApiGetAll(ApiLocationInput input)
		{
			if (input.LocationId > 0 && input.TenantId > 0)
			{
				await UpdatePullTime(new ApiLocationInput
				{
					LocationId = input.LocationId,
					TenantId = input.TenantId
				});
			}

			var allSyncs = await _syncerRepository.GetAllListAsync(a => a.TenantId == input.TenantId);
			List<ApiSyncerOutput> output = allSyncs.Select(dept => new ApiSyncerOutput()
			{
				Id = dept.Id,
				Name = dept.Name,
				SyncerGuid = dept.SyncerGuid
			}).ToList();

			return new ListResultDto<ApiSyncerOutput>(output);
		}

		public async Task ApiUpdateAllSyncer(ApiLocationInput input)
		{
			var myDate = DateTime.Now;
			var allDefaultSyncer = _syncerRepository.GetAll();
			var allLocationSyncer = _locationSyncerRepository.GetAll().Where(a => a.LocationId == input.LocationId);

			foreach (var rr in allDefaultSyncer)
			{
				var myLocationSyncer = await allLocationSyncer.FirstOrDefaultAsync(a => a.SyncerId == rr.Id);
				if (myLocationSyncer == null)
				{
					LocationSyncer lsy = new LocationSyncer()
					{
						LocationId = input.LocationId,
						LastSyncTime = myDate,
						SyncerGuid = rr.SyncerGuid,
						SyncerId = rr.Id
					};
					await _locationSyncerRepository.InsertAndGetIdAsync(lsy);
				}
				else
				{
					myLocationSyncer.LastSyncTime = myDate;
					myLocationSyncer.SyncerId = rr.Id;
					myLocationSyncer.SyncerGuid = rr.SyncerGuid;

					await _locationSyncerRepository.UpdateAsync(myLocationSyncer);
				}
			}
		}

		public async Task UpdatePullTime(ApiLocationInput input)
		{
			if (input.LocationId > 0 && input.TenantId > 0)
			{
				try
				{
					var mySyncer = await _lastSyncRepository.FirstOrDefaultAsync(a =>
						a.LocationId == input.LocationId && a.TenantId == input.TenantId);

					if (mySyncer != null)
					{
						mySyncer.LastPull = DateTime.Now;
					}
					else
					{
						mySyncer = new LastSync()
						{
							TenantId = input.TenantId,
							LocationId = input.LocationId,
							CreationTime = DateTime.Now,
							CreatorUserId = 1,
							LastPull = DateTime.Now,
							LastPush = DateTime.Now,
						};
						_lastSyncRepository.InsertAndGetId(mySyncer);
					}
				}
				catch (Exception e)
				{
					_logger.Log(LogSeverity.Fatal, "Update Pull Time : Location " + input.LocationId + "Tenant : " + input.TenantId);
				}
			}
		}

		public async Task UpdateSyncer(UpdateSyncerInput input)
		{
			var lSync = await _syncerRepository.FirstOrDefaultAsync(a => a.TenantId == input.TenantId
							&& a.Name.Equals(input.Name));

			if (lSync != null)
			{
				var locUpdate = lSync.LocationSyncers.SingleOrDefault(a => a.LocationId == input.LocationId);
				if (locUpdate != null)
				{
					locUpdate.LastSyncTime = DateTime.Now;
					locUpdate.SyncerGuid = input.SyncerGuid;
				}
				else
				{
					lSync.LocationSyncers.Add(new LocationSyncer()
					{
						LastSyncTime = DateTime.Now,
						LocationId = input.LocationId,
						SyncerGuid = input.SyncerGuid
					});
				}
				await _syncerRepository.UpdateAsync(lSync);
			}
		}

        public async Task UpdateSync(string input)
        {
            var syncer = await _syncerRepository.FirstOrDefaultAsync(a => a.Name.Equals(input));
            if (syncer != null)
            {
                syncer.SyncerGuid = Guid.NewGuid();
                syncer.LastModificationTime = DateTime.Now;
            }
            else
            {
                syncer = new Syncer()
                {
                    Name = input,
                    TenantId = (int)(AbpSession.TenantId ?? 0),
                    SyncerGuid = Guid.NewGuid()
                };
                _syncerRepository.InsertAndGetId(syncer);
            }
        }
	}
}
﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Card.Card;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Discount.Exporting;
using DinePlan.DineConnect.Connect.Master.Cards;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Promotions;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Session;
using Itenso.TimePeriod;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Days = DinePlan.DineConnect.Connect.Discount.Dtos.Days;

namespace DinePlan.DineConnect.Connect.Discount
{
    
    public class PromotionsAppService : DineConnectAppServiceBase, IPromotionsAppService
    {
        private readonly IRepository<BuyXAndYAtZValuePromotionExecution> _buyXAndYAtZValuePromotionExecutionRepository;
        private readonly IRepository<BuyXAndYAtZValuePromotion> _buyXAndYAtZValuePromotionRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<ConnectCardRedemption> _connectCardRedemptionRepository;
        private readonly IRepository<ConnectCard> _connectCardRepository;
        private readonly IRepository<ConnectCardType> _connectCardTypeRepository;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<DemandDiscountPromotion> _demandDiscountPromotionRepository;
        private readonly IRepository<DemandPromotionExecution> _demandPromotionExecutionPromoRepo;
        private readonly IDepartmentsAppService _departmentsAppService;
        private readonly IRepository<FixedPromotion> _fixedRepository;
        private readonly IRepository<FreeItemPromotionExecution> _freeItemPromotionExecutionRepository;
        private readonly IRepository<FreeItemPromotion> _freeItemPromotionRepository;
        private readonly IRepository<FreePromotionExecution> _freePromotionExecutionRepository;
        private readonly IRepository<FreePromotion> _freePromotionRepository;
        private readonly IRepository<FreeValuePromotionExecution> _freeValuePromotionExecutionRepository;
        private readonly IRepository<FreeValuePromotion> _freeValuePromotionRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<PriceTag> _priceTagRepository;
        private readonly IPromotionCategoryAppService _promotionCategoryAppService;
        private readonly IRepository<PromotionQuota> _promotionQuotaRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<PromotionRestrictItem> _promotionRestrictItemRepository;
        private readonly IRepository<PromotionSchedule> _promotionScheduleRepository;
        private readonly IPromotionsExcelExporter _promotionsExcelExporter;
        private readonly IRepository<StepDiscountPromotionExecution> _stepDiscountPromotionExecutionRepository;
        private readonly IRepository<StepDiscountPromotion> _stepDiscountPromotionRepository;
        private readonly IRepository<TicketDiscountPromotion> _ticketDiscountRepository;
        private readonly IRepository<Ticket> _ticketRepository;
        private readonly IRepository<TimerPromotion> _timerPromotionRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PromotionsAppService(
            IRepository<Category> categoryRepository
            , IRepository<Promotion> promotionRepository
            , IPromotionsExcelExporter promotionsExcelExporter
            , IRepository<PromotionSchedule> promotionScheduleRepository
            , IRepository<PromotionRestrictItem> promotionRestrictItemRepository
            , IRepository<FreeItemPromotionExecution> freeItemPromotionExecutionRepository
            , IRepository<FreeItemPromotion> freeItemPromotionRepository
            , IRepository<FixedPromotion> fixedRepository
            , IRepository<FreePromotionExecution> freePromotionExecutionRepository
            , IRepository<FreePromotion> freePromotionRepository
            , IRepository<FreeValuePromotion> freeValuePromotionRepository
            , IRepository<FreeValuePromotionExecution> freeValuePromotionExecutionRepository
            , IRepository<MenuItem> menuItemRepository
            , IRepository<PriceTag> priceTagRepository
            , IRepository<TicketDiscountPromotion> ticketDiscountRepository
            , IRepository<TimerPromotion> timerPromotionRepository
            , IRepository<DemandPromotionExecution> demandPromotionExecutionPromoRepo
            , IRepository<DemandDiscountPromotion> demandDiscountPromotionRepository
            , IRepository<MenuItemPortion> menuItemPortionRepository
            , IUnitOfWorkManager unitOfWorkManager
            , ConnectSession connectSession
            , ILocationAppService locationAppService
            , IRepository<PromotionQuota> promotionQuotaRepository
            , IRepository<Ticket> ticketRepository
            , IRepository<ConnectCardType> connectCardTypeRepository
            , IRepository<ConnectCard> connectCardRepository
            , IRepository<ConnectCardRedemption> connectCardRedemptionRepository
            , IRepository<Location> locationRepository
            , ICommonLookupAppService commonLookupAppService
            , IDepartmentsAppService departmentsAppService
            , IPromotionCategoryAppService promotionCategoryAppService
            , IConnectCardAppService connectCardAppService
            , IRepository<BuyXAndYAtZValuePromotion> buyXAndYAtZValuePromotionRepository
            , IRepository<BuyXAndYAtZValuePromotionExecution> buyXAndYAtZValuePromotionExecutionRepository
            , IRepository<StepDiscountPromotion> stepDiscountPromotionRepository
            , IRepository<StepDiscountPromotionMappingExecution> stepDiscountPromotionMappingExecutionRepository
            , IRepository<StepDiscountPromotionExecution> stepDiscountPromotionExecutionRepository
        )
        {
            _categoryRepository = categoryRepository;
            _promotionRepository = promotionRepository;
            _promotionsExcelExporter = promotionsExcelExporter;
            _promotionScheduleRepository = promotionScheduleRepository;
            _promotionRestrictItemRepository = promotionRestrictItemRepository;
            _freeItemPromotionExecutionRepository = freeItemPromotionExecutionRepository;
            _freeItemPromotionRepository = freeItemPromotionRepository;
            _fixedRepository = fixedRepository;
            _freePromotionExecutionRepository = freePromotionExecutionRepository;
            _freePromotionRepository = freePromotionRepository;
            _freeValuePromotionRepository = freeValuePromotionRepository;
            _freeValuePromotionExecutionRepository = freeValuePromotionExecutionRepository;
            _menuItemRepository = menuItemRepository;
            _priceTagRepository = priceTagRepository;
            _ticketDiscountRepository = ticketDiscountRepository;
            _timerPromotionRepository = timerPromotionRepository;
            _demandPromotionExecutionPromoRepo = demandPromotionExecutionPromoRepo;
            _demandDiscountPromotionRepository = demandDiscountPromotionRepository;
            _menuItemPortionRepository = menuItemPortionRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
            _promotionQuotaRepository = promotionQuotaRepository;
            _ticketRepository = ticketRepository;
            _connectCardTypeRepository = connectCardTypeRepository;
            _connectCardRedemptionRepository = connectCardRedemptionRepository;
            _locationRepository = locationRepository;
            _departmentsAppService = departmentsAppService;
            _promotionCategoryAppService = promotionCategoryAppService;
            _buyXAndYAtZValuePromotionRepository = buyXAndYAtZValuePromotionRepository;
            _buyXAndYAtZValuePromotionExecutionRepository = buyXAndYAtZValuePromotionExecutionRepository;
            _stepDiscountPromotionRepository = stepDiscountPromotionRepository;
            _stepDiscountPromotionExecutionRepository = stepDiscountPromotionExecutionRepository;
        }

        public async Task<PagedResultDto<PromotionsDto>> GetAll(GetAllPromotionsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredPromotions = _promotionRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                                                                            EF.Functions.Like(e.Name,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Code,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.RefCode,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Filter,
                                                                                input.Filter.ToILikeString()))
                    .WhereIf(input.PromotionTypeId.HasValue, a => a.PromotionTypeId == input.PromotionTypeId);

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                    filteredPromotions = FilterByLocations(filteredPromotions, input);

                var allMyEnItems = filteredPromotions.ToList();

                if (!input.IsDeleted && input.StartDate.HasValue && input.EndDate.HasValue)
                    allMyEnItems = allMyEnItems.Where(x =>
                    {
                        var firstRange = new TimeRange(
                            new DateTime(input.StartDate.Value.Year, input.StartDate.Value.Month,
                                input.StartDate.Value.Day),
                            new DateTime(input.EndDate.Value.Year, input.EndDate.Value.Month, input.EndDate.Value.Day)
                        );

                        var secondRange = new TimeRange(
                            new DateTime(x.StartDate.Year, x.StartDate.Month, x.StartDate.Day),
                            new DateTime(x.EndDate.Year, x.EndDate.Month, x.EndDate.Day)
                        );

                        var relation = firstRange.GetRelation(secondRange);
                        return relation != PeriodRelation.Before &&
                               relation != PeriodRelation.After;
                    }).ToList();

                var pagedAndFilteredPromotions = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting ?? "SortOrder asc")
                    .PageBy(input)
                    .ToList();

                var promotions = ObjectMapper.Map<List<PromotionsDto>>(pagedAndFilteredPromotions);

                var totalCount = allMyEnItems.Count();

                return new PagedResultDto<PromotionsDto>(
                    totalCount,
                    promotions
                );
            }
        }

        
        public async Task<GetPromotionForEditOutput> GetPromotionForEdit(NullableIdDto input)
        {
            var promotionEditDto = new CreateOrEditPromotionDto();
            if (input.Id.HasValue)
            {
                var promotion = await _promotionRepository.FirstOrDefaultAsync(e => e.Id == input.Id);
                promotionEditDto = ObjectMapper.Map<CreateOrEditPromotionDto>(promotion);
            }

            var output = new GetPromotionForEditOutput { Promotion = promotionEditDto };
            var schedules = await _promotionScheduleRepository.GetAll().Where(e => e.PromotionId == input.Id)
                .ToListAsync();
            var restrictItems = await _promotionRestrictItemRepository.GetAll().Where(e => e.PromotionId == input.Id)
                .ToListAsync();
            var quotas = await _promotionQuotaRepository.GetAll().Where(e => e.PromotionId == input.Id).ToListAsync();

            output.Promotion.PromotionSchedules = ObjectMapper.Map<List<PromotionScheduleEditDto>>(schedules);
            output.Promotion.PromotionRestrictItems =
                ObjectMapper.Map<List<PromotionRestrictItemEditDto>>(restrictItems);
            output.Promotion.PromotionQuotas = await GetPromotionQuotasByPromotionId(input);

            foreach (var item in output.Promotion.PromotionRestrictItems)
                item.Name = (await _menuItemRepository.GetAsync(item.MenuItemId))?.Name;

            output.TimerPromotion = await GetTimerPromotion(input);
            output.FixPromotions = await GetFixedPromotion(input, 2);
            output.FixPromotionsPer = await GetFixedPromotion(input, 1);
            output.Distributions = await GetFixedPromotion(input, 1);
            output.FreePromotion = await GetFreePromotion(input);
            output.FreeValuePromotion = await GetFreeValuePromotion(input);
            output.DemandPromotion = await GetDemandPromotion(input);
            output.FreeItemPromotion = await GetFreeItemPromotion(input);
            output.BuyXAndYAtZValuePromotion = await GetBuyXAndYAtZValuePromotion(input);
            output.StepDiscountPromotion = await GetStepDiscountPromotion(input);
            output.Filter = output.Promotion.Filter;
            output.TicketDiscountPromotion = await GetTicketDiscountPromotion(input);

            GetLocationsForEdit(output.Promotion, output.Promotion.LocationGroup);
            await GetLookup(output);
            return output;
        }

        public async Task CreateOrEdit(GetPromotionForEditOutput input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.Promotion.OrganizationId = _connectSession.OrgId;

            ValidateForCreate(input.Promotion);

            if (input.Promotion.Id.HasValue)
                await UpdatePromotion(input);
            else
                await CreatePromotion(input);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _promotionRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetPromotionsToExcel(GetAllPromotionsForExcelInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var filteredPromotions = _promotionRepository.GetAll()
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                                                                            EF.Functions.Like(e.Name,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Code,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.RefCode,
                                                                                input.Filter.ToILikeString()) ||
                                                                            EF.Functions.Like(e.Filter,
                                                                                input.Filter.ToILikeString()));

                var query = filteredPromotions
                    .Select(o => new GetPromotionForViewDto
                    {
                        Promotion = ObjectMapper.Map<PromotionsDto>(o)
                    });

                var promotionListDtos = await query.ToListAsync();

                return _promotionsExcelExporter.ExportToFile(promotionListDtos);
            }
        }

        public ListResultDto<ComboboxItemDto> GetPromotionTypesForLookupTable()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PromotionTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public ListResultDto<ComboboxItemDto> GetDaysForLookupTable()
        {
            return new ListResultDto<ComboboxItemDto>(new List<ComboboxItemDto>
            {
                new ComboboxItemDto{DisplayText = DayOfWeek.Monday.ToString(), Value = ((int)DayOfWeek.Monday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Tuesday.ToString(), Value = ((int)DayOfWeek.Tuesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Wednesday.ToString(), Value = ((int)DayOfWeek.Wednesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Thursday.ToString(), Value = ((int)DayOfWeek.Thursday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Friday.ToString(), Value = ((int)DayOfWeek.Friday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Saturday.ToString(), Value = ((int)DayOfWeek.Saturday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Sunday.ToString(), Value = ((int)DayOfWeek.Sunday).ToString()},
            });
        }

        public async Task<ListResultDto<ComboboxItemCustomizeDto>> GetCategoriesForCombobox()
        {
            var dtos = await _categoryRepository.GetAll()
                .Select(a => new ComboboxItemCustomizeDto(a.Id, a.Name, a.ProductGroupId))
                .ToListAsync();
            return new ListResultDto<ComboboxItemCustomizeDto>(dtos);
        }

        public ListResultDto<ComboboxItemDto> GetPromotionValueTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionValueTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetSimplePortionsForCombobox()
        {
            var menuItems = await _menuItemPortionRepository.GetAll()
                .Where(e => e.MenuItem.ProductType == 1)
                .Select(e => new ComboboxItemDto(e.Id.ToString(), string.Concat(e.MenuItem.Name, " - ", e.Name)))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(menuItems);
        }

        public async Task UpdateQuotaUsage(int ticketId)
        {
            var ticket = await _ticketRepository.FirstOrDefaultAsync(ticketId);

            var promotionDetails = new List<int>();
            if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                promotionDetails = JsonConvert
                    .DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails)
                    .Select(x => x.PromotionSyncId).ToList();
            foreach (var order in ticket.Orders)
            {
                if (!string.IsNullOrEmpty(order.OrderPromotionDetails))
                    promotionDetails.AddRange(order.GetOrderPromotionList().Select(x => x.PromotionSyncId));
                if (!string.IsNullOrEmpty(order.OrderTags))
                    promotionDetails.AddRange(order.GetOrderTagValues().Where(x => x.PromotionSyncId > 0)
                        .Select(x => x.PromotionSyncId));
            }

            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(ticket.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            foreach (var id in promotionDetails.Distinct())
            {
                var promotion = await _promotionRepository.GetAsync(id);
                if (promotion != null && promotion.ApplyOffline) continue;

                var quota = await _promotionQuotaRepository.FirstOrDefaultAsync(x => x.PromotionId == id);
                if (quota == null || string.IsNullOrEmpty(quota.LocationQuota)) return;

                var group = JsonConvert.DeserializeObject<CustomLocationGroupDto>(quota.LocationQuota);
                var lc = group?.Locations?.FirstOrDefault(x => x.Id == ticket.LocationId);
                if (lc != null)
                {
                    if (ticket.IsInState("*", "Refund") && lc.QuotaUsed > 0)
                    {
                        lc.QuotaUsed -= 1;
                        quota.QuotaUsed -= 1;
                    }
                    else
                    {
                        lc.QuotaUsed += 1;
                        quota.QuotaUsed += 1;
                    }

                    quota.LocationQuota = JsonConvert.SerializeObject(group);
                }

                await _promotionQuotaRepository.UpdateAsync(quota);
            }
        }

        public async Task<PagedResultDto<CombinePromotionDto>> GetPromotionsCombine(GetQuotaDetailInput input)
        {
            var allItems = await _promotionRepository.GetAll()
                .Where(x => x.Id != input.PromotionQuotaId)
                .Select(x => new CombinePromotionDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToListAsync();

            return new PagedResultDto<CombinePromotionDto>(
                allItems.Count(),
                allItems
            );
        }

        public ListResultDto<ComboboxItemDto> GetPromotionPositions()
        {
            var returnList = new List<ComboboxItemDto>
            {
                new ComboboxItemDto {Value = "1", DisplayText = L("LoginOfDinePlan")},
                new ComboboxItemDto {Value = "2", DisplayText = L("ClickOnPOS")},
                new ComboboxItemDto {Value = "3", DisplayText = L("EveryTransaction")},
                new ComboboxItemDto {Value = "4", DisplayText = L("PaymentClick")},
                new ComboboxItemDto {Value = "5", DisplayText = L("LogoutOfDinePlan")},
                new ComboboxItemDto {Value = "6", DisplayText = L("StartingWorkPeriod")},
                new ComboboxItemDto {Value = "7", DisplayText = L("EndWorkPeriod")},
                new ComboboxItemDto {Value = "8", DisplayText = L("StartShift")},
                new ComboboxItemDto {Value = "9", DisplayText = L("EndShift")}
            };

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        
        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _promotionRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _promotionRepository.UpdateAsync(item);
            }
        }

        #region Quota API

        public async Task<PromotionQuotaDto> ApiGetPromotionQuota(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var quotas = await _promotionQuotaRepository.GetAll()
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            var dto = new PromotionQuotaDto { QuotaAmount = 0, QuotaUsed = 0 };

            var locations = await _locationRepository.GetAllListAsync();
            foreach (var qt in quotas)
            {
                if (string.IsNullOrEmpty(qt.LocationQuota)) continue;

                dto.Id = qt.Id;
                dto.QuotaAmount = qt.QuotaAmount;
                var group = JsonHelper.Deserialize<CustomLocationGroupDto>(qt.LocationQuota);
                var qtlc = group?.Locations?.FirstOrDefault(x => x.Id == input.LocationId);
                if (qtlc != null)
                {
                    dto.QuotaUsed = qtlc.QuotaUsed;
                    break;
                }
            }

            return dto;
        }

        #endregion Quota API

        private async Task<StepDiscountPromotionEditDto> GetStepDiscountPromotion(NullableIdDto input)
        {
            var returnDto = new StepDiscountPromotionEditDto();
            if (input.Id.HasValue)
            {
                var output = await _stepDiscountPromotionRepository.GetAll()
                    .Include(e => e.StepDiscountPromotionExecutions)
                    .Include(e => e.StepDiscountPromotionMappingExecutions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);

                returnDto = ObjectMapper.Map<StepDiscountPromotionEditDto>(output);
                if (output != null && (output.StepDiscountPromotionExecutions.Count() > 0 ||
                                       output.StepDiscountPromotionMappingExecutions.Count() > 0))
                {
                    var excutions =
                        ObjectMapper.Map<List<StepDiscountPromotionExecutionEditDto>>(
                            output.StepDiscountPromotionExecutions);
                    var mappings =
                        ObjectMapper.Map<List<StepDiscountPromotionMappingExecutionEditDto>>(
                            output.StepDiscountPromotionMappingExecutions);
                    returnDto.StepDiscountPromotionExecutions =
                        new Collection<StepDiscountPromotionExecutionEditDto>(excutions);
                    returnDto.StepDiscountPromotionMappingExecutions =
                        new Collection<StepDiscountPromotionMappingExecutionEditDto>(mappings);
                    foreach (var item in returnDto.StepDiscountPromotionMappingExecutions)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.ProductGroupId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem?.Name} - {data?.Name}";
                    }
                }
            }

            return returnDto;
        }

        private async Task<BuyXAndYAtZValuePromotionEditDto> GetBuyXAndYAtZValuePromotion(NullableIdDto input)
        {
            var returnDto = new BuyXAndYAtZValuePromotionEditDto();
            if (input.Id.HasValue)
            {
                var output = await _buyXAndYAtZValuePromotionRepository
                    .GetAllIncluding(e => e.BuyXAndYAtZValuePromotionExecutions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);

                returnDto = ObjectMapper.Map<BuyXAndYAtZValuePromotionEditDto>(output);
                if (output != null && output.BuyXAndYAtZValuePromotionExecutions.Any())
                {
                    var excutions =
                        ObjectMapper.Map<List<BuyXAndYAtZValuePromotionExecutionEditDto>>(
                            output.BuyXAndYAtZValuePromotionExecutions);
                    returnDto.BuyXAndYAtZValueExecutions =
                        new Collection<BuyXAndYAtZValuePromotionExecutionEditDto>(excutions);

                    foreach (var item in returnDto.BuyXAndYAtZValueExecutions)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.ProductGroupId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                    }
                }
            }

            return returnDto;
        }

        private async Task<List<PromotionQuotaDto>> GetPromotionQuotasByPromotionId(NullableIdDto input)
        {
            var output = new List<PromotionQuotaDto>();
            if (input.Id.HasValue)
            {
                var quotas = await _promotionQuotaRepository.GetAll().Where(e => e.PromotionId == input.Id.Value)
                    .ToListAsync();

                if (quotas != null && quotas.Count() > 0)
                {
                    output = ObjectMapper.Map<List<PromotionQuotaDto>>(quotas);
                    foreach (var item in output)
                        item.LocationGroup = new CommonLocationGroupDto
                        {
                            Group = item.Group,
                            Locations = item.Locations != null ? JsonConvert.DeserializeObject<List<SimpleLocationDto>>(item.Locations) : null
                        };
                }
            }

            return output;
        }

        private async Task<List<FixedPromotionEditDto>> GetFixedPromotion(NullableIdDto input, int type)
        {
            var retuDto = new List<FixedPromotionEditDto>();
            if (input.Id.HasValue)
            {
                var fixedPromotions = await _fixedRepository.GetAllListAsync(a =>
                    a.PromotionId == input.Id.Value && a.PromotionValueType == type);
                if (fixedPromotions != null) retuDto = ObjectMapper.Map<List<FixedPromotionEditDto>>(fixedPromotions);
                foreach (var item in retuDto)
                {
                    var data = _menuItemPortionRepository.GetAll()
                        .Include(x => x.MenuItem)
                        .Include(x => x.MenuItem.Category)
                        .Include(x => x.MenuItem.Category.ProductGroup)
                        .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                        .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                        .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                        .WhereIf(item.ProductGroupId > 0,
                            x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                    item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                    item.MenuItemName = data?.MenuItem?.Name;
                    item.CategoryName = data?.MenuItem?.Category?.Name;
                    item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                }
            }

            return retuDto;
        }

        private async Task<FreeValuePromotionEditDto> GetFreeValuePromotion(NullableIdDto input)
        {
            var returnDto = new FreeValuePromotionEditDto();
            if (input.Id.HasValue)
            {
                var freeValue = await _freeValuePromotionRepository.GetAllIncluding(e => e.FreeValuePromotionExecutions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);

                returnDto = ObjectMapper.Map<FreeValuePromotionEditDto>(freeValue);
                if (returnDto != null && returnDto.FreeValuePromotionExecutions.Any())
                {
                    returnDto.From =
                        new Collection<FreeValuePromotionExecutionEditDto>(
                            returnDto.FreeValuePromotionExecutions.Where(a => a.From).ToList());
                    foreach (var item in returnDto.From)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.CategoryId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                    }

                    returnDto.To =
                        new Collection<FreeValuePromotionExecutionEditDto>(
                            returnDto.FreeValuePromotionExecutions.Where(a => !a.From).ToList());
                    foreach (var item in returnDto.To)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.ProductGroupId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                    }
                }
            }

            return returnDto;
        }

        private async Task<FreePromotionEditDto> GetFreePromotion(NullableIdDto input)
        {
            var returnDto = new FreePromotionEditDto();
            if (input.Id.HasValue)
            {
                var freePromotion = await _freePromotionRepository.GetAllIncluding(e => e.FreePromotionExecutions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);

                returnDto = ObjectMapper.Map<FreePromotionEditDto>(freePromotion);
                if (returnDto != null && returnDto.FreePromotionExecutions.Any())
                {
                    returnDto.From =
                        new Collection<FreePromotionExecutionEditDto>(
                            returnDto.FreePromotionExecutions.Where(a => a.From).ToList());
                    foreach (var item in returnDto.From)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.ProductGroupId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                    }

                    returnDto.To =
                        new Collection<FreePromotionExecutionEditDto>(
                            returnDto.FreePromotionExecutions.Where(a => !a.From).ToList());
                    foreach (var item in returnDto.To)
                    {
                        var data = _menuItemPortionRepository.GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Category)
                            .Include(x => x.MenuItem.Category.ProductGroup)
                            .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                            .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                            .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                            .WhereIf(item.ProductGroupId > 0,
                                x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                        item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                        item.MenuItemName = data?.MenuItem?.Name;
                        item.CategoryName = data?.MenuItem?.Category?.Name;
                        item.PortionName = $"{data?.MenuItem?.Name} - {data?.Name}";
                    }
                }
            }

            return returnDto;
        }

        private async Task<TimerPromotionEditDto> GetTimerPromotion(NullableIdDto input)
        {
            var retuDto = new TimerPromotionEditDto();
            if (input.Id.HasValue)
            {
                var timerPromotion =
                    await _timerPromotionRepository.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (timerPromotion != null) retuDto = ObjectMapper.Map<TimerPromotionEditDto>(timerPromotion);
            }

            return retuDto;
        }

        private async Task<DemandPromotionEditDto> GetDemandPromotion(NullableIdDto input)
        {
            var returnDto = new DemandPromotionEditDto();
            if (input.Id.HasValue)
            {
                var demandDiscount = await _demandDiscountPromotionRepository
                    .GetAllIncluding(e => e.DemandPromotionExecutions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (demandDiscount != null) returnDto = ObjectMapper.Map<DemandPromotionEditDto>(demandDiscount);

                foreach (var item in returnDto.DemandPromotionExecutions)
                {
                    var data = _menuItemPortionRepository.GetAll()
                        .Include(x => x.MenuItem)
                        .Include(x => x.MenuItem.Category)
                        .Include(x => x.MenuItem.Category.ProductGroup)
                        .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId)
                        .WhereIf(item.MenuItemId > 0, x => x.MenuItem.Id == item.MenuItemId)
                        .WhereIf(item.CategoryId > 0, x => x.MenuItem.Category.Id == item.CategoryId)
                        .WhereIf(item.ProductGroupId > 0,
                            x => x.MenuItem.Category.ProductGroup.Id == item.ProductGroupId).FirstOrDefault();
                    item.ProductGroupName = data?.MenuItem?.Category?.ProductGroup?.Name;
                    item.MenuItemName = data?.MenuItem?.Name;
                    item.CategoryName = data?.MenuItem?.Category?.Name;
                    item.PortionName = $"{data?.MenuItem.Name} - {data.Name}";
                }
            }

            return returnDto;
        }

        private async Task<TicketDiscountPromotionDto> GetTicketDiscountPromotion(NullableIdDto input)
        {
            var returnDto = new TicketDiscountPromotionDto();
            if (input.Id.HasValue)
            {
                var ticketDiscount =
                    await _ticketDiscountRepository.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (ticketDiscount != null) returnDto = ObjectMapper.Map<TicketDiscountPromotionDto>(ticketDiscount);
            }

            return returnDto;
        }

        private async Task<FreeItemPromotionEditDto> GetFreeItemPromotion(NullableIdDto input)
        {
            var retuDto = new FreeItemPromotionEditDto();
            if (input.Id.HasValue)
            {
                var freeItemPromotion = await _freeItemPromotionRepository.GetAllIncluding(e => e.Executions)
                    .FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (freeItemPromotion != null) retuDto = ObjectMapper.Map<FreeItemPromotionEditDto>(freeItemPromotion);
                foreach (var item in retuDto.Executions)
                {
                    var data = _menuItemPortionRepository.GetAll()
                        .Include(x => x.MenuItem)
                        .WhereIf(item.MenuItemPortionId > 0, x => x.Id == item.MenuItemPortionId).FirstOrDefault();
                    item.PortionName = $"{data?.MenuItem?.Name} - {data.Name}";
                }
            }

            return retuDto;
        }

        
        public async Task<int> CreatePromotion(GetPromotionForEditOutput input)
        {
            var promotion = ObjectMapper.Map<Promotion>(input.Promotion);
            promotion.PromotionSchedules = null;
            promotion.PromotionRestrictItems = null;

            if (AbpSession.TenantId != null) promotion.TenantId = (int)AbpSession.TenantId;

            UpdateLocations(promotion, input.Promotion.LocationGroup);

            var promotionId = await _promotionRepository.InsertAndGetIdAsync(promotion);

            var promotionSchedules =
                ObjectMapper.Map<ICollection<PromotionSchedule>>(input.Promotion.PromotionSchedules);

            foreach (var promotionSchedule in promotionSchedules)
            {
                promotionSchedule.PromotionId = promotionId;

                promotionSchedule.Id = await _promotionScheduleRepository.InsertAndGetIdAsync(promotionSchedule);
            }

            //promotion
            var promotionQuotas = ObjectMapper.Map<ICollection<PromotionQuota>>(input.Promotion.PromotionQuotas);

            foreach (var quota in promotionQuotas)
            {
                quota.PromotionId = promotionId;
                UpdateQuotaResetDate(quota);
                UpdateQuotaPlants(quota);
                quota.Id = await _promotionQuotaRepository.InsertAndGetIdAsync(quota);
            }

            var promotionRestrictItems =
                ObjectMapper.Map<ICollection<PromotionRestrictItem>>(input.Promotion.PromotionRestrictItems);

            foreach (var promotionRestrictItem in promotionRestrictItems)
            {
                promotionRestrictItem.PromotionId = promotionId;

                promotionRestrictItem.Id =
                    await _promotionRestrictItemRepository.InsertAndGetIdAsync(promotionRestrictItem);
            }

            switch (input.Promotion.PromotionTypeId)
            {
                case (int)PromotionTypes.HappyHour:
                    if (input.TimerPromotion != null)
                        await UpdateTimerPromotion(input.TimerPromotion, promotionId);
                    break;

                case (int)PromotionTypes.FixedDiscountPercentage:
                    if (input.FixPromotionsPer != null && input.FixPromotionsPer.Any())
                        await UpdateFixedPromotion(input.FixPromotionsPer, promotionId);
                    break;

                case (int)PromotionTypes.FixedDiscountValue:
                    if (input.FixPromotions != null && input.FixPromotions.Any())
                        await UpdateFixedPromotion(input.FixPromotions, promotionId);
                    break;

                case (int)PromotionTypes.BuyXItemGetYItemFree:
                    if (input.FreePromotion != null)
                        await UpdateFreePromotion(input.FreePromotion, promotionId);
                    break;

                case (int)PromotionTypes.BuyXItemGetYatZValue:
                    if (input.FreeValuePromotion != null)
                        await UpdateFreeValuePromotion(input.FreeValuePromotion, promotionId);
                    break;

                case (int)PromotionTypes.OrderDiscount:
                case (int)PromotionTypes.PaymentOrderDiscount:
                    if (input.DemandPromotion != null)
                        await UpdateDemandPromotion(input.DemandPromotion, promotionId);
                    break;

                case (int)PromotionTypes.FreeItems:
                    if (input.FreeItemPromotion != null)
                        await UpdateFreeItemPromotion(input.FreeItemPromotion, promotionId);

                    break;

                case (int)PromotionTypes.TicketDiscount:
                case (int)PromotionTypes.PaymentTicketDiscount:
                    if (input.TicketDiscountPromotion != null)
                        await UpdateTicketDiscountPromotion(input.TicketDiscountPromotion, promotionId);

                    if (input.Promotion.PromotionTypeId == (int)PromotionTypes.TicketDiscount &&
                        input.Distributions != null &&
                        input.Distributions.Count() > 0) await UpdateFixedPromotion(input.Distributions, promotionId);
                    break;

                case (int)PromotionTypes.BuyXAndYAtZValue:
                    if (input.BuyXAndYAtZValuePromotion != null)
                        await UpdateBuyXAndYAtZValuePromotion(input.BuyXAndYAtZValuePromotion, promotionId);
                    break;

                case (int)PromotionTypes.StepDiscount:
                    if (input.StepDiscountPromotion != null)
                        await UpdateStepDiscountPromotion(input.StepDiscountPromotion, promotionId);
                    break;
            }

            return promotionId;
        }

        private async Task UpdateStepDiscountPromotion(StepDiscountPromotionEditDto stepDiscountPromotion,
            int promotionId)
        {
            if (stepDiscountPromotion.Id.HasValue)
            {
                var dto = await _stepDiscountPromotionRepository.GetAsync(stepDiscountPromotion.Id.Value);
                ObjectMapper.Map(stepDiscountPromotion, dto);
                dto.PromotionId = promotionId;

                dto.StepDiscountPromotionExecutions =
                    ObjectMapper.Map<ICollection<StepDiscountPromotionExecution>>(stepDiscountPromotion
                        .StepDiscountPromotionExecutions);

                var tobeRemoved = await _stepDiscountPromotionExecutionRepository.GetAll()
                    .Where(e => e.StepDiscountPromotionId == stepDiscountPromotion.Id &&
                                !dto.StepDiscountPromotionExecutions.Select(s => s.Id).Contains(e.Id))
                    .ToListAsync();

                tobeRemoved.ForEach(e => _stepDiscountPromotionExecutionRepository.DeleteAsync(e));

                await _stepDiscountPromotionRepository.UpdateAsync(dto);
            }
            else
            {
                var dto = ObjectMapper.Map<StepDiscountPromotion>(stepDiscountPromotion);
                dto.PromotionId = promotionId;
                await _stepDiscountPromotionRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateBuyXAndYAtZValuePromotion(BuyXAndYAtZValuePromotionEditDto buyXAndYAtZValuePromotion,
            int promotionId)
        {
            if (buyXAndYAtZValuePromotion.Id.HasValue)
            {
                var dto = await _buyXAndYAtZValuePromotionRepository.GetAsync(buyXAndYAtZValuePromotion.Id.Value);
                ObjectMapper.Map(buyXAndYAtZValuePromotion, dto);
                dto.PromotionId = promotionId;

                dto.BuyXAndYAtZValuePromotionExecutions =
                    ObjectMapper.Map<ICollection<BuyXAndYAtZValuePromotionExecution>>(buyXAndYAtZValuePromotion
                        .BuyXAndYAtZValueExecutions);

                var tobeRemoved = await _buyXAndYAtZValuePromotionExecutionRepository.GetAll()
                    .Where(e => e.BuyXAndYAtZValuePromotionId == buyXAndYAtZValuePromotion.Id &&
                                !dto.BuyXAndYAtZValuePromotionExecutions.Select(s => s.Id).Contains(e.Id))
                    .ToListAsync();

                tobeRemoved.ForEach(e => _buyXAndYAtZValuePromotionExecutionRepository.DeleteAsync(e));

                await _buyXAndYAtZValuePromotionRepository.UpdateAsync(dto);
            }
            else
            {
                var dto = ObjectMapper.Map<BuyXAndYAtZValuePromotion>(buyXAndYAtZValuePromotion);
                dto.PromotionId = promotionId;
                await _buyXAndYAtZValuePromotionRepository.InsertAsync(dto);
            }
        }

        
        public async Task UpdatePromotion(GetPromotionForEditOutput input)
        {
            var promotion = await _promotionRepository.FirstOrDefaultAsync((int)input.Promotion.Id);

            ObjectMapper.Map(input.Promotion, promotion);

            promotion.PromotionSchedules =
                ObjectMapper.Map<ICollection<PromotionSchedule>>(input.Promotion.PromotionSchedules);

            promotion.PromotionRestrictItems =
                ObjectMapper.Map<ICollection<PromotionRestrictItem>>(input.Promotion.PromotionRestrictItems);

            promotion.PromotionQuotas = ObjectMapper.Map<ICollection<PromotionQuota>>(input.Promotion.PromotionQuotas);
            foreach (var quota in promotion.PromotionQuotas)
            {
                quota.LastModifierUserId = AbpSession.UserId;
                quota.TenantId = AbpSession.TenantId.Value;
                UpdateQuotaResetDate(quota);
                UpdateQuotaPlants(quota);
            }
            //promotion.PromotionQuotas = promtionQuotas;

            var schedulesNotinPro = await _promotionScheduleRepository.GetAll()
                .Where(e => e.PromotionId == input.Promotion.Id &&
                            !promotion.PromotionSchedules.Select(s => s.Id).Contains(e.Id))
                .ToListAsync();

            var restrictItemNotinPro = await _promotionRestrictItemRepository.GetAll()
                .Where(e => e.PromotionId == input.Promotion.Id &&
                            !promotion.PromotionRestrictItems.Select(s => s.Id).Contains(e.Id))
                .ToListAsync();

            var quotasNotinPro = await _promotionQuotaRepository.GetAll()
                .Where(e => e.PromotionId == input.Promotion.Id &&
                            !promotion.PromotionQuotas.Select(s => s.Id).Contains(e.Id))
                .ToListAsync();

            schedulesNotinPro.ForEach(e => _promotionScheduleRepository.DeleteAsync(e));

            restrictItemNotinPro.ForEach(e => _promotionRestrictItemRepository.DeleteAsync(e));

            quotasNotinPro.ForEach(e => _promotionQuotaRepository.DeleteAsync(e));

            UpdateLocations(promotion, input.Promotion.LocationGroup);
            await _promotionRepository.UpdateAsync(promotion);
            var promoId = promotion.Id;

            switch (input.Promotion.PromotionTypeId)
            {
                case (int)PromotionTypes.HappyHour:
                    if (input.TimerPromotion != null)
                        await UpdateTimerPromotion(input.TimerPromotion, promoId);
                    break;

                case (int)PromotionTypes.FixedDiscountPercentage:
                    if (input.FixPromotionsPer != null && input.FixPromotionsPer.Any())
                        await UpdateFixedPromotion(input.FixPromotionsPer, promoId);
                    break;

                case (int)PromotionTypes.FixedDiscountValue:
                    if (input.FixPromotions != null && input.FixPromotions.Any())
                        await UpdateFixedPromotion(input.FixPromotions, promoId);
                    break;

                case (int)PromotionTypes.BuyXItemGetYItemFree:
                    if (input.FreePromotion != null)
                        await UpdateFreePromotion(input.FreePromotion, promoId);
                    break;

                case (int)PromotionTypes.BuyXItemGetYatZValue:
                    if (input.FreeValuePromotion != null)
                        await UpdateFreeValuePromotion(input.FreeValuePromotion, promoId);
                    break;

                case (int)PromotionTypes.OrderDiscount:
                case (int)PromotionTypes.PaymentOrderDiscount:

                    if (input.DemandPromotion != null)
                        await UpdateDemandPromotion(input.DemandPromotion, promoId);
                    break;

                case (int)PromotionTypes.FreeItems:
                    if (input.FreeItemPromotion != null)
                        await UpdateFreeItemPromotion(input.FreeItemPromotion, promoId);

                    break;

                case (int)PromotionTypes.TicketDiscount:
                case (int)PromotionTypes.PaymentTicketDiscount:
                    if (input.TicketDiscountPromotion != null)
                        await UpdateTicketDiscountPromotion(input.TicketDiscountPromotion, promoId);
                    if (input.Promotion.PromotionTypeId == (int)PromotionTypes.TicketDiscount)
                        await UpdateFixedPromotion(input.Distributions, promoId);
                    break;

                case (int)PromotionTypes.BuyXAndYAtZValue:
                    if (input.BuyXAndYAtZValuePromotion != null)
                        await UpdateBuyXAndYAtZValuePromotion(input.BuyXAndYAtZValuePromotion, promoId);
                    break;

                case (int)PromotionTypes.StepDiscount:
                    if (input.StepDiscountPromotion != null)
                        await UpdateStepDiscountPromotion(input.StepDiscountPromotion, promoId);
                    break;
            }
        }

        private async Task UpdateFreePromotion(FreePromotionEditDto freePromotion, int promoId)
        {
            if (freePromotion.Id.HasValue)
            {
                await AddOrRemoveFreePromotionFromTo(freePromotion, promoId);
            }
            else
            {
                var dto = ObjectMapper.Map<FreePromotion>(freePromotion);

                dto.PromotionId = promoId;
                dto.FreePromotionExecutions = new List<FreePromotionExecution>();

                foreach (var promos in freePromotion.From)
                    dto.FreePromotionExecutions.Add(ObjectMapper.Map<FreePromotionExecution>(promos));
                foreach (var promos in freePromotion.To)
                    dto.FreePromotionExecutions.Add(ObjectMapper.Map<FreePromotionExecution>(promos));
                await _freePromotionRepository.InsertAsync(dto);
            }
        }

        private async Task AddOrRemoveFreePromotionFromTo(FreePromotionEditDto freePromotion, int promoId)
        {
            var dto = await _freePromotionRepository.GetAsync(freePromotion.Id.Value);
            dto.PromotionId = promoId;
            ObjectMapper.Map(freePromotion, dto);

            dto.FreePromotionExecutions = new List<FreePromotionExecution>();
            foreach (var promos in freePromotion.From)
                dto.FreePromotionExecutions.Add(ObjectMapper.Map<FreePromotionExecution>(promos));
            foreach (var promos in freePromotion.To)
                dto.FreePromotionExecutions.Add(ObjectMapper.Map<FreePromotionExecution>(promos));

            var tobeRemoved = await _freePromotionExecutionRepository.GetAll()
                .Where(e => e.FreePromotionId == freePromotion.Id &&
                            !dto.FreePromotionExecutions.Select(s => s.Id).Contains(e.Id))
                .ToListAsync();

            tobeRemoved.ForEach(e => _freePromotionExecutionRepository.DeleteAsync(e));

            await _freePromotionRepository.UpdateAsync(dto);
        }

        private async Task UpdateFreeValuePromotion(FreeValuePromotionEditDto freePromotion, int promoId)
        {
            if (freePromotion.Id.HasValue)
            {
                await AddOrRemoveFreeValuePromotion(freePromotion, promoId);
            }
            else
            {
                var dto = ObjectMapper.Map<FreeValuePromotion>(freePromotion);

                dto.PromotionId = promoId;
                dto.FreeValuePromotionExecutions = new List<FreeValuePromotionExecution>();

                foreach (var promos in freePromotion.From)
                    dto.FreeValuePromotionExecutions.Add(ObjectMapper.Map<FreeValuePromotionExecution>(promos));
                foreach (var promos in freePromotion.To)
                    dto.FreeValuePromotionExecutions.Add(ObjectMapper.Map<FreeValuePromotionExecution>(promos));

                await _freeValuePromotionRepository.InsertAsync(dto);
            }
        }

        private async Task AddOrRemoveFreeValuePromotion(FreeValuePromotionEditDto freePromotion, int promoId)
        {
            var dto = await _freeValuePromotionRepository.GetAsync(freePromotion.Id.Value);

            dto.PromotionId = promoId;
            ObjectMapper.Map(freePromotion, dto);

            dto.FreeValuePromotionExecutions = new List<FreeValuePromotionExecution>();
            foreach (var promos in freePromotion.From)
                dto.FreeValuePromotionExecutions.Add(ObjectMapper.Map<FreeValuePromotionExecution>(promos));
            foreach (var promos in freePromotion.To)
                dto.FreeValuePromotionExecutions.Add(ObjectMapper.Map<FreeValuePromotionExecution>(promos));

            var tobeRemoved = await _freeValuePromotionExecutionRepository.GetAll()
                .Where(e => e.FreeValuePromotionId == freePromotion.Id &&
                            !dto.FreeValuePromotionExecutions.Select(s => s.Id).Contains(e.Id))
                .ToListAsync();

            tobeRemoved.ForEach(e => _freeValuePromotionExecutionRepository.DeleteAsync(e));

            await _freeValuePromotionRepository.UpdateAsync(dto);
        }

        private async Task UpdateDemandPromotion(DemandPromotionEditDto demandPromotion, int promoId)
        {
            if (demandPromotion.Id.HasValue)
            {
                var dto = await _demandDiscountPromotionRepository.GetAsync(demandPromotion.Id.Value);
                ObjectMapper.Map(demandPromotion, dto);
                dto.PromotionId = promoId;

                dto.DemandPromotionExecutions =
                    ObjectMapper.Map<ICollection<DemandPromotionExecution>>(demandPromotion.DemandPromotionExecutions);

                var tobeRemoved = await _demandPromotionExecutionPromoRepo.GetAll()
                    .Where(e => e.DemandDiscountPromotionId == demandPromotion.Id &&
                                !dto.DemandPromotionExecutions.Select(s => s.Id).Contains(e.Id))
                    .ToListAsync();

                tobeRemoved.ForEach(e => _demandPromotionExecutionPromoRepo.DeleteAsync(e));

                await _demandDiscountPromotionRepository.UpdateAsync(dto);
            }
            else
            {
                var dto = ObjectMapper.Map<DemandDiscountPromotion>(demandPromotion);
                dto.PromotionId = promoId;
                await _demandDiscountPromotionRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateFreeItemPromotion(FreeItemPromotionEditDto fit, int promoId)
        {
            if (fit.Id.HasValue)
            {
                var dto = await _freeItemPromotionRepository.GetAsync(fit.Id.Value);
                ObjectMapper.Map(fit, dto);
                dto.PromotionId = promoId;

                dto.Executions = ObjectMapper.Map<ICollection<FreeItemPromotionExecution>>(fit.Executions);

                var tobeRemoved = await _freeItemPromotionExecutionRepository.GetAll()
                    .Where(e => e.FreeItemPromotionId == fit.Id && !dto.Executions.Select(s => s.Id).Contains(e.Id))
                    .ToListAsync();

                tobeRemoved.ForEach(e => _freeItemPromotionExecutionRepository.DeleteAsync(e));

                await _freeItemPromotionRepository.UpdateAsync(dto);
            }
            else
            {
                var dto = ObjectMapper.Map<FreeItemPromotion>(fit);
                dto.PromotionId = promoId;

                await _freeItemPromotionRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateTicketDiscountPromotion(TicketDiscountPromotionDto demandPromotion, int promoId)
        {
            if (demandPromotion.Id.HasValue)
            {
                var item = await _ticketDiscountRepository.GetAsync(demandPromotion.Id.Value);

                ObjectMapper.Map(demandPromotion, item);
                item.PromotionId = promoId;
                await _ticketDiscountRepository.UpdateAsync(item);
            }
            else
            {
                var dto = ObjectMapper.Map<TicketDiscountPromotion>(demandPromotion);
                dto.PromotionId = promoId;
                await _ticketDiscountRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateTimerPromotion(TimerPromotionEditDto timer, int prom)
        {
            if (timer.Id.HasValue)
            {
                var item = await _timerPromotionRepository.GetAsync(timer.Id.Value);
                item.PromotionId = prom;
                item.PriceTagId = timer.PriceTagId;
                await _timerPromotionRepository.UpdateAsync(item);
            }
            else
            {
                var dto = ObjectMapper.Map<TimerPromotion>(timer);
                dto.PromotionId = prom;
                await _timerPromotionRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateFixedPromotion(List<FixedPromotionEditDto> fixes, int prom)
        {
            await _fixedRepository.GetAll()
                .Where(f => f.PromotionValueType == fixes.First().PromotionValueType)
                .Where(f => !fixes.Select(p => p.Id).Contains(f.Id))
                .DeleteFromQueryAsync();

            foreach (var item in fixes)
            {
                var fixedEntity = ObjectMapper.Map<FixedPromotion>(item);
                fixedEntity.PromotionId = prom;
                if (fixedEntity.Id > 0)
                    await _fixedRepository.UpdateAsync(fixedEntity);
                else
                    await _fixedRepository.InsertAsync(fixedEntity);
            }
        }

        private void ValidateForCreate(CreateOrEditPromotionDto promotionDto)
        {
            var exists = _promotionRepository.GetAll()
                .Where(c => c.TenantId == AbpSession.TenantId)
                .Where(a => a.Name.ToUpper().Equals(promotionDto.Name.ToUpper()))
                .WhereIf(promotionDto.Id.HasValue, a => a.Id != promotionDto.Id);

            if (exists.Any()) throw new UserFriendlyException(L("NameAlreadyExists", promotionDto.Name));
        }

        public ListResultDto<ComboboxItemDto> GetPromotionResetType()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(ResetType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                ;
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        #region Api

        public async Task<TicketDiscountPromotionDto> ApiTicketDiscountPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new TicketDiscountPromotionDto();
            var promoDefs = await _ticketDiscountRepository.GetAll()
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            var ti = promoDefs.LastOrDefault();

            if (ti != null) output = ObjectMapper.Map<TicketDiscountPromotionDto>(ti);
            return output;
        }

        public async Task<FreeItemPromotionEditDto> ApiGetFreeItemPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);
            var returnDto = new FreeItemPromotionEditDto();

            var promoDefs = await _freeItemPromotionRepository.GetAll()
                .Include(x => x.Executions)
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            var ti = promoDefs.LastOrDefault();

            if (ti != null) returnDto = ObjectMapper.Map<FreeItemPromotionEditDto>(ti);
            return returnDto;
        }

        public async Task<ApiPromotionOutput> ApiGetPromotions(ApiLocationInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var listPromotions = await _promotionRepository
                .GetAll().Where(a => a.TenantId == input.TenantId && a.EndDate >= DateTime.Now)
                .Include(t => t.PromotionSchedules)
                .Include(t => t.PromotionRestrictItems).ToListAsync();

            var allOutputPromotions = new List<Promotion>();
            var allLocations = new List<int>();
            if (input.LocationId >= 0) allLocations.Add(input.LocationId);

            if (!string.IsNullOrEmpty(input.Tag))
            {
                var myLocs = await _locationAppService.GetLocationsByLocationGroupCode(input.Tag);
                if (myLocs != null && myLocs.Items.Any())
                    allLocations.AddRange(myLocs.Items.Select(allLocationsItem =>
                        Convert.ToInt32(allLocationsItem.Value)));
            }

            try
            {
                if (listPromotions.Any() && allLocations.Any())
                {
                    foreach (var myLocation in allLocations)
                        foreach (var myPromo in listPromotions)
                            if (await _locationAppService.IsLocationExists(new CheckLocationInput
                            {
                                LocationId = myLocation,
                                Locations = myPromo.Locations,
                                Group = myPromo.Group,
                                NonLocations = myPromo.NonLocations,
                                LocationTag = myPromo.LocationTag
                            }))
                                allOutputPromotions.Add(myPromo);
                }
                else
                {
                    allOutputPromotions = listPromotions;
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            if (allOutputPromotions.Any())
            {
                var returnPromotion = ObjectMapper.Map<List<CreateOrEditPromotionDto>>(allOutputPromotions);
                foreach (var item in returnPromotion)
                {
                    var quotas = await _promotionQuotaRepository.GetAllListAsync(x =>
                        x.PromotionId == item.Id && !string.IsNullOrEmpty(x.Plants));
                    item.HasQuota = quotas.Any();
                }

                return new ApiPromotionOutput
                {
                    Promotions = returnPromotion
                };
            }

            return null;
        }

        public async Task<ApiTimerPromotionOutput> ApiGetTimerPromotions(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new ApiTimerPromotionOutput();

            var promoDefs = await _timerPromotionRepository.GetAll()
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();
            var timer = promoDefs.LastOrDefault();

            if (timer != null && timer.PriceTagId > 0)
            {
                var pTag = await _priceTagRepository.FirstOrDefaultAsync(a => a.Id == timer.PriceTagId);
                if (pTag != null) output.PriceTag = pTag.Name;
            }

            return output;
        }

        public async Task<FreePromotionEditDto> ApiGetFreePromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var returnDto = new FreePromotionEditDto();

            var promoDefs = await _freePromotionRepository.GetAll()
                .Include(x => x.FreePromotionExecutions)
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();
            var ti = promoDefs.LastOrDefault();

            if (ti != null)
                returnDto = ObjectMapper.Map<FreePromotionEditDto>(ti);
            return returnDto;
        }

        public async Task<FreeValuePromotionEditDto> ApiGetFreeValuePromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var returnDto = new FreeValuePromotionEditDto();

            var promoDefs = await _freeItemPromotionRepository.GetAll()
                .Include(x => x.Executions)
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            var ti = promoDefs.LastOrDefault();

            if (ti != null) returnDto = ObjectMapper.Map<FreeValuePromotionEditDto>(ti);
            return returnDto;
        }

        public async Task<List<ApiFixedPromotionOutput>> ApiGetFixedPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new List<ApiFixedPromotionOutput>();

            var promos = await _fixedRepository.GetAll()
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            if (promos.Any())
                foreach (var fp in promos)
                {
                    var pro = new ApiFixedPromotionOutput
                    {
                        CategoryId = fp.CategoryId,
                        MenuItemId = fp.MenuItemId,
                        Value = fp.PromotionValue,
                        ValueType = fp.PromotionValueType,
                        ProductGroupId = fp.ProductGroupId,
                        MenuItemPortionId = fp.MenuItemPortionId
                    };
                    if (fp.CategoryId > 0)
                    {
                        var category = await _categoryRepository.GetAsync(fp.CategoryId);
                        if (category != null) pro.CategoryName = category.Name;
                    }

                    output.Add(pro);
                }

            return output;
        }

        public async Task<DemandPromotionEditDto> ApiGetDemandPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new DemandPromotionEditDto();

            var allMenu = await _demandDiscountPromotionRepository.GetAll()
                .Include(x => x.DemandPromotionExecutions)
                .Where(a => a.PromotionId == input.PromotionId).ToListAsync();

            var ti = allMenu.LastOrDefault();

            if (ti != null) output = ObjectMapper.Map<DemandPromotionEditDto>(ti);

            foreach (var all in output.DemandPromotionExecutions)
                if (all.CategoryId > 0)
                {
                    var category = await _categoryRepository.GetAsync(all.CategoryId);
                    if (category != null) all.CategoryName = category.Name;
                }

            return output;
        }

        #endregion Api

        #region Card

        public async Task<ApiCardTypeOutput> ApiValidateCard(ApiCardTypeInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            return ValidateCard(input);
        }

        
        public async Task<ApiPromotionStatusUpdate> ApiUpdatePromotionStatus(ApiPromotionStatusUpdate input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            if (input.PromotionId > 0)
            {
                var myPromo = await _promotionRepository.GetAsync(input.PromotionId);
                if (myPromo != null)
                {
                    myPromo.Active = input.Active;
                    return new ApiPromotionStatusUpdate
                    {
                        Active = myPromo.Active,
                        PromotionId = myPromo.Id,
                        TenantId = myPromo.TenantId
                    };
                }
            }

            return null;
        }

        public async Task<ApiCardTypeOutput> ApiUpdateCard(ApiCardTypeInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var myValidCard = ValidateCard(input);

            if (myValidCard.Valid)
            {
                var myCardTypeList = _connectCardTypeRepository.GetAll().Where(a => a.Id == input.CardTypeId
                    && a.TenantId == input.TenantId).ToList();

                var myCardType = myCardTypeList.LastOrDefault();
                if (myCardType != null &&
                    myCardType.UseOneTime && myCardType.ValidTill >= DateTime.Now)
                {
                    var myCardList = _connectCardRepository.GetAll().Where(a => a.TenantId.Equals(input.TenantId) &&
                        a.ConnectCardTypeId == myCardType.Id &&
                        a.CardNo.Equals(input.CardNumber)).ToList();
                    var myCard = myCardList.LastOrDefault();

                    if (myCard != null)
                    {
                        myCard.Redeemed = true;
                        await _connectCardRepository.UpdateAsync(myCard);

                        var cardRedemption = new ConnectCardRedemption
                        {
                            ConnectCardId = myCard.Id,
                            RedemptionDate = DateTime.Now,
                            LocationId = input.LocationId,
                            TicketNumber = input.TicketNumber
                        };

                        await _connectCardRedemptionRepository.InsertAsync(cardRedemption);
                    }
                }
            }

            return myValidCard;
        }

        public async Task<ApiCardTypeOutput> ApiReverseAppliedCard(ApiCardTypeInput input)
        {
            var cardOutput = new ApiCardTypeOutput();
            try
            {
                var orgId = await _locationAppService.GetOrgnizationIdByLocationId(input.LocationId);
                if (orgId > 0)
                    CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                        DineConnectDataFilters.Parameters.OrganizationId, orgId);

                var myCardTypeList = _connectCardRedemptionRepository.GetAll().Where(a =>
                    a.LocationId.Equals(input.LocationId) && a.TicketNumber.Equals(input.TicketNumber)).ToList();

                foreach (var cardInstance in myCardTypeList)
                {
                    var myCard = await _connectCardRepository.GetAsync(cardInstance.ConnectCardId);
                    if (myCard != null) myCard.Redeemed = false;
                }

                ;

                foreach (var connectCardRedemption in myCardTypeList.ToArray())
                    await _connectCardRedemptionRepository.DeleteAsync(connectCardRedemption);

                cardOutput.Valid = true;
                return cardOutput;
            }
            catch (Exception e)
            {
                cardOutput.Valid = false;
                cardOutput.Reason = e.Message;
            }

            return cardOutput;
        }

        private ApiCardTypeOutput ValidateCard(ApiCardTypeInput input)
        {
            var cardOutput = new ApiCardTypeOutput
            {
                Valid = false,
                Reason = "[Empty]"
            };

            if (string.IsNullOrEmpty(input.CardNumber))
            {
                cardOutput.Reason = "Empty Card Number";
                return cardOutput;
            }

            if (input.TenantId == 0)
            {
                cardOutput.Reason = "No Tenant";
                return cardOutput;
            }

            if (input.CardTypeId == 0)
            {
                cardOutput.Reason = "No Card Type";
                return cardOutput;
            }

            if (string.IsNullOrEmpty(input.TicketNumber))
            {
                cardOutput.Reason = "No Ticket Number";
                return cardOutput;
            }

            if (input.IsPromotion)
            {
                var myPromotionList = _promotionRepository.GetAll()
                    .Where(a => a.Id == input.PromotionId && a.TenantId == input.TenantId).ToList();

                var myPromotion = myPromotionList.LastOrDefault();
                if (myPromotion == null)
                {
                    cardOutput.Reason = "Empty Promotion";
                    return cardOutput;
                }

                if (!myPromotion.ConnectCardTypeId.HasValue)
                {
                    cardOutput.Reason = "No CardType for Promotion";
                    return cardOutput;
                }

                if (myPromotion.ConnectCardTypeId != input.CardTypeId)
                {
                    cardOutput.Reason = "Invalid Card Type";
                    return cardOutput;
                }
            }

            var myCardTypeList = _connectCardTypeRepository.GetAll()
                .Where(a => a.Id == input.CardTypeId && a.TenantId == input.TenantId)
                .ToList();
            var myCardType = myCardTypeList.LastOrDefault();

            if (myCardType == null)
            {
                cardOutput.Reason = "Invalid CardType";
                return cardOutput;
            }

            if (myCardType.ValidTill < DateTime.Now)
            {
                cardOutput.Reason = "Card is expired";
                return cardOutput;
            }

            var myCardList = _connectCardRepository.GetAll().Where(a =>
                a.TenantId.Equals(input.TenantId) && a.ConnectCardTypeId == myCardType.Id &&
                a.CardNo.Equals(input.CardNumber)).ToList();
            var myCard = myCardList.LastOrDefault();
            if (myCard == null)
            {
                cardOutput.Reason = "No Card";
                return cardOutput;
            }

            if (!myCard.Active)
            {
                cardOutput.Reason = "Card is not Active";
                return cardOutput;
            }

            if (myCard.Redeemed)
            {
                cardOutput.Reason = "Already Redeemed Card";
                return cardOutput;
            }

            cardOutput.Reason = "";
            cardOutput.Valid = true;

            return cardOutput;
        }

        #endregion Card

        #region PromotionQuota

        private void UpdateQuotaPlants(PromotionQuota quota)
        {
            try
            {
                var allLocations = _locationRepository.GetAllList();
                var locationIds = new List<int>();

                if (string.IsNullOrEmpty(quota.Locations))
                {
                    locationIds = allLocations.Select(x => x.Id).ToList();
                }
                else
                {
                    var lcs = JsonConvert.DeserializeObject<List<QuotaLocationDto>>(quota.Locations);
                    locationIds = lcs.Select(x => x.Id).ToList();
                }

                var group = new CustomLocationGroupDto
                {
                    Locations = new List<QuotaLocationDto>()
                };

                if (string.IsNullOrEmpty(quota.LocationQuota))
                {
                    foreach (var id in locationIds)
                    {
                        var lc = allLocations.FirstOrDefault(x => x.Id == id);
                        group.Locations.Add(new QuotaLocationDto
                        {
                            Id = id,
                            Code = lc.Code,
                            Name = lc.Name,
                            QuotaUsed = 0
                        });
                    }
                }
                else
                {
                    var existGroup = JsonConvert.DeserializeObject<CustomLocationGroupDto>(quota.LocationQuota);

                    foreach (var id in locationIds)
                    {
                        var lc = allLocations.FirstOrDefault(x => x.Id == id);
                        var existG = existGroup?.Locations.FirstOrDefault(x => x.Id == id);
                        if (existG == null)
                            group.Locations.Add(new QuotaLocationDto
                            {
                                Id = id,
                                Code = lc.Code,
                                Name = lc.Name,
                                QuotaUsed = 0
                            });
                        else
                            group.Locations.Add(new QuotaLocationDto
                            {
                                Id = id,
                                Code = lc.Code,
                                Name = lc.Name,
                                QuotaUsed = existG.QuotaUsed
                            });
                    }
                }

                quota.LocationQuota = JsonConvert.SerializeObject(group);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot Update Promotion Quota");
            }
        }

        private void UpdateQuotaResetDate(PromotionQuota quota)
        {
            var today = DateTime.Today;
            if (quota.ResetType == (int)ResetType.Daily)
            {
                quota.PlantResetDate = today.AddDays(1);
            }
            else if (quota.ResetType == (int)ResetType.Weekly)
            {
                var result = today.AddDays(1);
                while ((int)result.DayOfWeek != quota.ResetWeekValue)
                    result = result.AddDays(1);
                quota.PlantResetDate = result;
            }
            else if (quota.ResetType == (int)ResetType.Monthly && quota.ResetMonthValue.HasValue)
            {
                var next = quota.ResetMonthValue.Value.AddMonths(1);
                quota.PlantResetDate = new DateTime(next.Year, next.Month, next.Day);
            }
            else if (quota.ResetType == (int)ResetType.Yearly && quota.ResetMonthValue.HasValue)
            {
                var next = quota.ResetMonthValue.Value.AddYears(1);
                quota.PlantResetDate = new DateTime(next.Year, next.Month, next.Day);
            }
        }

        private async Task GetLookup(GetPromotionForEditOutput output)
        {
            output.PromotionTypes = GetPromotionTypesForLookupTable();
            output.PromotionValueTypes = GetPromotionValueTypes();
            output.DayOfWeeks = GetDaysForLookupTable();
            output.RestTypeList = GetPromotionResetType();
            output.PromotionPositions = GetPromotionPositions();
            output.PromotionCategories = await _promotionCategoryAppService.GetPromotionCategories();
            var cardTypes = await _connectCardTypeRepository.GetAll()
                .Select(a => new ComboboxItemDto(a.Id.ToString(), a.Name)).ToListAsync();
            output.CardTypes = new ListResultDto<ComboboxItemDto>(cardTypes);
            //for
        }

        private async Task<ListResultDto<ComboboxItemCustomizeDto>> GetMenuItemForCategoryCombobox()
        {
            var result = await _menuItemRepository.GetAll()
                .Select(a => new ComboboxItemCustomizeDto(a.Id, a.Name, a.CategoryId))
                .ToListAsync();

            return new ListResultDto<ComboboxItemCustomizeDto>(result);
        }

        private async Task<ListResultDto<ComboboxItemCustomizeDto>> GetPortionForCombobox()
        {
            var portions = await _menuItemPortionRepository.GetAll()
                .Select(e =>
                    new ComboboxItemCustomizeDto(e.Id, string.Concat(e.MenuItem.Name, " - ", e.Name), e.MenuItemId))
                .ToListAsync();

            return new ListResultDto<ComboboxItemCustomizeDto>(portions);
        }

        private ListResultDto<ComboboxItemDto> GetPromotionApplyTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionApplyTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        private async Task<ListResultDto<ComboboxItemDto>> GetMenuItemPortionsForCombobox()
        {
            var menuItemsPortions = await _menuItemRepository.GetAll().SelectMany(a => a.Portions)
                .Select(e => new ComboboxItemDto(e.Id.ToString(), string.Concat(e.MenuItem.Name, " - ", e.Name)))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(menuItemsPortions);
        }

        private ListResultDto<ComboboxItemDto> GetStepDiscountType()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(StepDiscountType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<GetComboboxForPromotionTypeOutput> GetComboboxByPromotionType(PromotionTypes input)
        {
            var output = new GetComboboxForPromotionTypeOutput();
            switch (input)
            {
                case PromotionTypes.HappyHour:
                    output.PriceTags = await _departmentsAppService.GetAllPriceTagForLookupTable("");
                    break;

                case PromotionTypes.OrderDiscount:
                case PromotionTypes.PaymentOrderDiscount:
                case PromotionTypes.StepDiscount:
                    if (input == PromotionTypes.OrderDiscount || input == PromotionTypes.PaymentOrderDiscount)
                        output.ApplyTypes = GetPromotionApplyTypes();

                    if (input == PromotionTypes.StepDiscount)
                    {
                        output.PromotionValueTypes = GetPromotionValueTypes();
                        output.StepDiscountTypes = GetStepDiscountType();
                    }

                    break;

                case PromotionTypes.TicketDiscount:
                    output.ApplyTypes = GetPromotionApplyTypes();
                    break;

                case PromotionTypes.PaymentTicketDiscount:
                    break;
            }

            return output;
        }

        public async Task SaveSortOrder(int[] all)
        {
            var i = 1;
            foreach (var promoId in all)
            {
                var cate = await _promotionRepository.FirstOrDefaultAsync(promoId);
                cate.SortOrder = i++;
                await _promotionRepository.UpdateAsync(cate);
            }
        }

        #endregion PromotionQuota
    }
}
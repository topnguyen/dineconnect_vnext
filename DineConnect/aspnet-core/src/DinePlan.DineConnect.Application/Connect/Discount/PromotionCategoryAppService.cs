﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount.Exporting;
using DinePlan.DineConnect.Connect.Promotions;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    
    public class PromotionCategoryAppService : DineConnectAppServiceBase, IPromotionCategoryAppService
    {
        private readonly IRepository<PromotionCategory> _promotionCategoryRepo;

        private readonly IPromotionCategoryExcelExporter _promotionCategoryExcelExporter;

        private readonly ConnectSession _connectSession;

        public PromotionCategoryAppService(IRepository<PromotionCategory> promotionCategoryRepo,
            IPromotionCategoryExcelExporter promotionCategoryExcelExporter,
            ConnectSession connectSession
            )
        {
            _promotionCategoryRepo = promotionCategoryRepo;
            _promotionCategoryExcelExporter = promotionCategoryExcelExporter;
            _connectSession = connectSession;
        }

        public async Task<PagedResultDto<PromotionCategoryListDto>> GetAll(GetPromotionCategoryInput input)
        {
            var allItems = _promotionCategoryRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );

            //if (!string.IsNullOrWhiteSpace(input.LocationIds))
            //{
            //    allItems = FilterByLocations(allItems, input);
            //}

            var allItemCount = allItems.Count();

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<PromotionCategoryListDto>>(sortMenuItems);

            return new PagedResultDto<PromotionCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetPromotionCategoryInput input)
        {
            var allItems = await _promotionCategoryRepo
              .GetAll()
              .WhereIf(
                  !input.Filter.IsNullOrEmpty(),
                  p => p.Name.Contains(input.Filter)
              )
              .ToListAsync();

            return await _promotionCategoryExcelExporter.ExportToFile(ObjectMapper.Map<List<PromotionCategoryListDto>>(allItems));
        }

        public async Task<GetPromotionCategoryForEditOutput> GetPromotionCategoryForEdit(NullableIdDto input)
        {
            var output = new GetPromotionCategoryForEditOutput();

            PromotionCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _promotionCategoryRepo.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<PromotionCategoryEditDto>(hDto);

                //UpdateLocations(hDto, output.LocationGroup);
            }
            else
            {
                editDto = new PromotionCategoryEditDto();
            }

            output.PromotionCategory = editDto;

            return output;
        }

        public async Task CreateOrUpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            if (input.PromotionCategory.Id.HasValue)
            {
                await UpdatePromotionCategory(input);
            }
            else
            {
                await CreatePromotionCategory(input);
            }
        }

        
        public async Task DeletePromotionCategory(EntityDto input)
        {
            await _promotionCategoryRepo.DeleteAsync(input.Id);
        }

        
        protected virtual async Task UpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            var item = await _promotionCategoryRepo.GetAsync(input.PromotionCategory.Id.Value);

            var dto = input.PromotionCategory;

            ObjectMapper.Map(dto, item);

            //UpdateLocations(item, input.LocationGroup);

            CheckErrors(await CreateOrUpdateSync(item));
        }

        
        protected virtual async Task CreatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            var entity = ObjectMapper.Map<PromotionCategory>(input.PromotionCategory);

            //UpdateLocations(dto, input.LocationGroup);

            CheckErrors(await CreateOrUpdateSync(entity));
        }

        public async Task<ListResultDto<PromotionCategoryListDto>> GetPromotionCategories()
        {
            var promotionCategory = await _promotionCategoryRepo.GetAllListAsync();

            return new ListResultDto<PromotionCategoryListDto>(ObjectMapper.Map<List<PromotionCategoryListDto>>(promotionCategory));
        }

        public async Task<IdentityResult> CreateOrUpdateSync(PromotionCategory promotionCategory)
        {
            //promotionCategory.OrganizationId = _connectSession.OrgId;

            //  if the New Addition
            if (promotionCategory.Id == 0)
            {
                if (_promotionCategoryRepo.GetAll().Any(a => a.Id.Equals(promotionCategory.Id)))
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                await _promotionCategoryRepo.InsertAndGetIdAsync(promotionCategory);
                return IdentityResult.Success;
            }
            else
            {
                List<PromotionCategory> lst = _promotionCategoryRepo.GetAll().Where(a => a.Id.Equals(promotionCategory.Id) && a.Id != promotionCategory.Id).ToList();
                if (lst.Count > 0)
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}
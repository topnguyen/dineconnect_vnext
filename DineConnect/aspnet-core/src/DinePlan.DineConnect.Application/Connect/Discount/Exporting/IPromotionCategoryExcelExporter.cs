﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Discount.Exporting
{
  public interface IPromotionCategoryExcelExporter
    {
        Task<FileDto> ExportToFile(List<PromotionCategoryListDto> list);
    }
}

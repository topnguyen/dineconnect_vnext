﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Discount.Exporting
{
    public class PromotionsExcelExporter : NpoiExcelExporterBase, IPromotionsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PromotionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPromotionForViewDto> promotions)
        {
            return CreateExcelPackage(
                "Promotions.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Promotions"));
                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("PromotionType"),
                        L("StartDate"),
                        L("EndDate"),
                        L("Locations"),
                        L("IsDeleted"),
                        L("Active")
                        );

                    AddObjects(
                        sheet, 2, promotions,
                        _ => _.Promotion.Id,
                        _ => _.Promotion.Name,
                        _ => _.Promotion.PromotionType,
                        _ => _timeZoneConverter.Convert(_.Promotion.StartDate, _abpSession.TenantId, _abpSession.GetUserId())?.ToString("yyyy-MM-dd"),
                        _ => _timeZoneConverter.Convert(_.Promotion.EndDate, _abpSession.TenantId, _abpSession.GetUserId())?.ToString("yyyy-MM-dd"),
                        _ => _.Promotion.LocationsName,
                        _ => _.Promotion.IsDeleted,
                        _ => _.Promotion.Active
                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Discount.Exporting
{
    public interface IPromotionsExcelExporter
    {
        FileDto ExportToFile(List<GetPromotionForViewDto> promotions);
    }
}
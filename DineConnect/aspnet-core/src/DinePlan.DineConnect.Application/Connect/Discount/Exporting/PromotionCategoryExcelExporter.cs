﻿using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Discount.Exporting
{
	public class PromotionCategoryExcelExporter : NpoiExcelExporterBase, IPromotionCategoryExcelExporter
	{
		private readonly ITenantSettingsAppService _tenantSettingsAppService;

		public PromotionCategoryExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
		{
			_tenantSettingsAppService = tenantSettingsAppService;
		}

		public async Task<FileDto> ExportToFile(List<PromotionCategoryListDto> list)
		{
			var allSettings = await _tenantSettingsAppService.GetAllSettings();

			var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
				? allSettings.FileDateTimeFormat.FileDateFormat
				: AppConsts.FileDateFormat;

			return CreateExcelPackage(
				"PromotionCategory.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.CreateSheet(L("PromotionCategory"));

					AddHeader(
						sheet,
						L("Id"),
						L("Code"),
						L("Name"),
						L("Tag")
					);

					AddObjects(
						sheet, 2, list,
						_ => _.Id,
						_ => _.Code,
						_ => _.Name,
						_ => _.Tag
					);

					for (var i = 1; i <= 3; i++)
					{
						sheet.AutoSizeColumn(i);
					}
				});
		}
	}
}
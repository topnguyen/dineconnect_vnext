using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Lookups
{
    public class ConnectLookupAppService : DineConnectAppServiceBase, IConnectLookupAppService
    {
        private readonly IRepository<TransactionType> _transactionTypeRepo;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _paymentTypeRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITillAccountsAppService _tillAccountService;
        public ConnectLookupAppService(
            ITillAccountsAppService tillAccountService,
        IRepository<TransactionType> transactionTypeRepo,
            IRepository<Master.PaymentTypes.PaymentType> paymentTypeRepo,
            IRepository<Department> departmentRepo,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _transactionTypeRepo = transactionTypeRepo;
            _paymentTypeRepo = paymentTypeRepo;
            _departmentRepo = departmentRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _tillAccountService = tillAccountService;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetTransactionTypesForCombobox()
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var items = await _transactionTypeRepo.GetAll().ToListAsync();

                var listCombobox = items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList();

                return new ListResultDto<ComboboxItemDto>(listCombobox);
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPaymentTypes()
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var items = await _paymentTypeRepo.GetAll().ToListAsync();

                var listCombobox = items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList();

                return new ListResultDto<ComboboxItemDto>(listCombobox);
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetDepartments()
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var items = await _departmentRepo.GetAll().ToListAsync();

                var listCombobox = items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList();

                return new ListResultDto<ComboboxItemDto>(listCombobox);
            }
        }

        public ListResultDto<ComboboxItemDto> GetScreenMenuCategoryKeyBoardTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(ScreenMenuNumerator)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i += 1;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }
        public async Task<ListResultDto<ComboboxItemDto>> GetTillAccounts()
        {
            var lst = await _tillAccountService.GetTillAccounts();
            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }
    }
}
﻿using Abp;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Timing;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace DinePlan.DineConnect.Connect.TimeSettings
{
    public class TimeSettingsAppService : DineConnectAppServiceBase, ITimeSettingsAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly ISettingManager _settingManager;
        private readonly IRepository<User, long> _userRepository;

        public TimeSettingsAppService(ConnectSession connectSession, ISettingManager settingManager, IRepository<User, long> userRepository)
        {
            _settingManager = settingManager;
            _userRepository = userRepository;
            _connectSession = connectSession;
        }

        public async Task<TimeSettingDto> GetTimeSettings()
        {
            return new TimeSettingDto
            {
                TimeZone = await _settingManager.GetSettingValueForUserAsync("TimeZone", AbpSession.TenantId, (long)AbpSession.UserId),
                TimeFormat = await _settingManager.GetSettingValueForUserAsync("TimeFormat", AbpSession.TenantId, (long)AbpSession.UserId),
                CurrentDateTime = DateTime.UtcNow
            };
        }

        public async Task SetTimeSettings(TimeSettingDto timeSetting)
        {
            var user = await GetCurrentUserAsync();
            await _settingManager.ChangeSettingForUserAsync(user.ToUserIdentifier(), "TimeFormat", timeSetting.TimeFormat);
            await _settingManager.ChangeSettingForUserAsync(user.ToUserIdentifier(), "TimeZone", timeSetting.TimeZone);

            var windowTimezone = TZConvert.IanaToWindows(timeSetting.TimeZone);
            if (AbpSession.TenantId != null)
                await _settingManager.ChangeSettingForTenantAsync((int) AbpSession.TenantId,
                    TimingSettingNames.TimeZone, windowTimezone);

            await SetimeSettingsForAllUser(timeSetting);
        }

        private async Task SetimeSettingsForAllUser(TimeSettingDto timeSetting)
        {
            var users = await _userRepository.GetAll().Where(e => e.Id != AbpSession.UserId).ToListAsync();
            foreach (var user in users)
            {
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, user.Id), "TimeFormat", timeSetting.TimeFormat);
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, user.Id), "TimeZone", timeSetting.TimeZone);
            }
        }
    }
}
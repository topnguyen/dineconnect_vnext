﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Table.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Table
{
    public class ConnectTableGroupAppService : DineConnectAppServiceBase, IConnectTableGroupAppService
    {
        private readonly IRepository<ConnectTableGroup> _tableGroupRepo;
        private readonly IRepository<ConnectTable> _tableRepo;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ConnectTableGroupAppService(
            IRepository<ConnectTableGroup> tableGroupRepo, IRepository<ConnectTable> tableRepo,
        ILocationAppService locAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tableGroupRepo = tableGroupRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _tableRepo = tableRepo;
            _locAppService = locAppService;
        }

        public async Task<ListResultDto<ConnectTableGroupEditDto>> ApiGetAll(ApiConnectTableGroupInput input)
        {
            //var lastSyncTime = input.TimeZone != null
            //     ? TimeZoneInfo.ConvertTime(input.LastSyncTime, input.TimeZone, TimeZoneInfo.Local)
            //     : input.LastSyncTime;

            var lstDept =
                _tableGroupRepo
                    .GetAll()
                    .Where(a => a.TenantId == input.TenantId)
                    .Include(a => a.ConnectTableInGroups)
                    .ThenInclude(x => x.ConnectTable);

            List<ConnectTableGroupEditDto> deptOutput = new List<ConnectTableGroupEditDto>();

            if (input.LocationId > 0)
            {
                foreach (var mydept in lstDept)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = mydept.Locations,
                        Group = mydept.Group,
                        LocationTag = mydept.LocationTag,
                        NonLocations = mydept.NonLocations
                    }))
                    {
                        deptOutput.Add(ObjectMapper.Map<ConnectTableGroupEditDto>(mydept));
                    }
                }
            }
            else
            {
                deptOutput = ObjectMapper.Map<List<ConnectTableGroupEditDto>>(lstDept);
            }
            return new ListResultDto<ConnectTableGroupEditDto>(deptOutput);
        }

        public async Task<PagedResultDto<ConnectTableGroupListDto>> GetAll(GetConnectTableGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tableGroupRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    allItems = FilterByLocations(allItems, input);
                }

                var allItemCount = allItems.Count();

                var sortMenuItems = allItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = ObjectMapper.Map<List<ConnectTableGroupListDto>>(sortMenuItems);


                foreach (var loca in allListDtos)
                {
                    if (string.IsNullOrEmpty(loca.Locations))
                    {
                        continue;
                    }
                    var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Locations);
                    loca.DisplayLocation = string.Join(", ", allLo.Select(a => a.DisplayText));
                }


                return new PagedResultDto<ConnectTableGroupListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<GetConnectTableGroupForEditOutput> GetConnectTableGroupForEdit(NullableIdDto input)
        {
            ConnectTableGroupEditDto editDto;

            GetConnectTableGroupForEditOutput output = new GetConnectTableGroupForEditOutput();
            
            var allTables = ObjectMapper.Map<List<ConnectTableEditDto>>(_tableRepo.GetAll());

            if (input.Id.HasValue && input.Id.Value > 0)
            {
                var hDto =
                    await
                        _tableGroupRepo.GetAll()
                            .Include(a => a.ConnectTableInGroups)
                            .ThenInclude(x => x.ConnectTable)
                            .FirstAsync(a => a.Id == input.Id.Value);
                
                editDto = ObjectMapper.Map<ConnectTableGroupEditDto>(hDto);

                var myTaIds = editDto.ConnectTables.Select(a => a.Id).ToList();
                
                editDto.AllTables = allTables.Where(a => !myTaIds.Contains(a.Id)).ToList();

                UpdateLocations(hDto, output.LocationGroup);
            }
            else
            {
                editDto = new ConnectTableGroupEditDto { AllTables = allTables };
            }

            output.ConnectTableGroup = editDto;

            return output;
        }

        public async Task CreateOrUpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            if (input?.LocationGroup == null)
                return;
            if (input.ConnectTableGroup.Id.HasValue)
            {
                await UpdateConnectTableGroup(input);
            }
            else
            {
                await CreateConnectTableGroup(input);
            }
        }

        public async Task DeleteConnectTableGroup(EntityDto input)
        {
            await _tableGroupRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<ConnectTableGroupListDto>> GetIds()
        {
            var lstConnectTableGroup = await _tableGroupRepo.GetAll().ToListAsync();
            return new ListResultDto<ConnectTableGroupListDto>(ObjectMapper.Map<List<ConnectTableGroupListDto>>(lstConnectTableGroup));
        }

        public async Task<ListResultDto<ConnectTableGroupListDto>> GetConnectTableGroups()
        {
            var roles = await _tableGroupRepo.GetAll().ToListAsync();
            return new ListResultDto<ConnectTableGroupListDto>(ObjectMapper.Map<List<ConnectTableGroupListDto>>(roles));
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetConnectTableGroupsForCombobox()
        {
            var lstLocation = await _tableGroupRepo.GetAllListAsync(a => !a.IsDeleted);

            return
                new ListResultDto<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<PagedResultDto<ConnectTableListDto>> GetAllTables(GetConnectTableGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tableRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                       .WhereIf(
                           !input.Filter.IsNullOrEmpty(),
                           p => p.Name.Equals(input.Filter)
                       );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = ObjectMapper.Map<List<ConnectTableListDto>>(sortMenuItems);

                var allItemCount = await allItems.CountAsync();

                return new PagedResultDto<ConnectTableListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        protected virtual async Task UpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            var dto = input.ConnectTableGroup;

            var groupObject =
                   await
                       _tableGroupRepo.GetAll()
                           .Include(a => a.ConnectTableInGroups)
                           .ThenInclude(x => x.ConnectTable)
                           .FirstAsync(a => a.Id == dto.Id.Value);

            List<int> otherIds = dto.ConnectTables.Where(a => a.Id.HasValue).Select(a => a.Id.Value).ToList();

            List<int> tableIds =
                groupObject.ConnectTableInGroups.Select(x => x.ConnectTable).Where(a => !otherIds.Contains(a.Id)).Select(a => a.Id).ToList();

            foreach (var objec in tableIds.Select(allMyIds => groupObject.ConnectTableInGroups.Select(x => x.ConnectTable).SingleOrDefault(a => a.Id == allMyIds)).Where(objec => objec != null))
            {
                groupObject.ConnectTableInGroups.Select(x => x.ConnectTable).ToList().Remove(objec);
            }

            foreach (var allTab in dto.ConnectTables)
            {
                if (allTab.Id.HasValue)
                {
                    if (groupObject.ConnectTableInGroups.Select(x => x.ConnectTable).ToList().Any(a => a.Id != allTab.Id))
                    {
                        var tableRe = _tableRepo.Get(allTab.Id.Value);
                        if (tableRe != null)
                        {
                            groupObject.ConnectTableInGroups.Select(x => x.ConnectTable).ToList().Add(tableRe);
                        }
                    }
                }
            }
            groupObject.Name = dto.Name;

            UpdateLocations(groupObject, input.LocationGroup);

            CheckErrors(await CreateOrUpdateSync(groupObject));
        }

        protected virtual async Task CreateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            var dto = ObjectMapper.Map<ConnectTableGroup>(input.ConnectTableGroup);
            dto.ConnectTableInGroups.Select(x => x.ConnectTable).ToList().Clear();

            foreach (var table in input.ConnectTableGroup.ConnectTables)
            {
                if (table.Id.HasValue)
                {
                    var tableRe = _tableRepo.Get(table.Id.Value);
                    if (tableRe != null)
                    {
                        dto.ConnectTableInGroups.Select(x => x.ConnectTable).ToList().Add(tableRe);
                    }
                }
            }

            UpdateLocations(dto, input.LocationGroup);

            CheckErrors(await CreateOrUpdateSync(dto));
        }

        public async Task<int> CreateOrUpdateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            if (input.Table.Id.HasValue)
            {
                await UpdateConnectTable(input);
                return input.Table.Id.Value;
            }
            else
            {
                var output = await CreateConnectTable(input);
                if (output > 0)
                {
                    return output;
                }
            }
            return 0;
        }

        public async Task DeleteConnectTable(EntityDto input)
        {
            await _tableRepo.DeleteAsync(input.Id);
        }

        public async Task<GetConnectTableForEditOutput> GetConnectTableForEdit(NullableIdDto input)
        {
            ConnectTableEditDto editDto;
            var allLocations = new List<ComboboxItemDto>();

            if (input.Id.HasValue)
            {
                var hDto = 
                        await _tableRepo
                            .GetAll()
                            .Include(x => x.ConnectTableInGroups)
                            .ThenInclude(x => x.ConnectTableGroup)
                            .FirstOrDefaultAsync(x => x.Id == input.Id.Value)
                    ;

                editDto = ObjectMapper.Map<ConnectTableEditDto>(hDto);
            }
            else
            {
                editDto = new ConnectTableEditDto();
            }
            return new GetConnectTableForEditOutput
            {
                Table = editDto
            };
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tableRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _tableRepo.UpdateAsync(item);
            }
        }

        private async Task<int> CreateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            var dto = ObjectMapper.Map<ConnectTable>(input.Table);
            CheckErrors(await CreateOrUpdateSyncTable(dto));
            return dto.Id;
        }

        private async Task UpdateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            var item = await _tableRepo.GetAsync(input.Table.Id.Value);
            var dto = input.Table;
            item.Name = dto.Name;
            item.Pax = dto.Pax;
            CheckErrors(await CreateOrUpdateSyncTable(item));
        }

        public async Task<IdentityResult> CreateOrUpdateSync(ConnectTableGroup department)
        {
            if (department.Id == 0)
            {
                if (_tableGroupRepo.GetAll().Any(a => a.Name.Equals(department.Name)))
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                await _tableGroupRepo.InsertAndGetIdAsync(department);
                return IdentityResult.Success;
            }
            else
            {
                List<ConnectTableGroup> lst = _tableGroupRepo.GetAll().Where(a => a.Name.Equals(department.Name) && a.Id != department.Id).ToList();
                if (lst.Count > 0)
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> CreateOrUpdateSyncTable(ConnectTable item)
        {
            if (item.Id == 0)
            {
                if (_tableRepo.GetAll().Any(a => a.Name.Equals(item.Name)))
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                var id = await _tableRepo.InsertAndGetIdAsync(item);
                item.Id = id;
                return IdentityResult.Success;
            }
            else
            {
                List<ConnectTable> lst = _tableRepo.GetAll().Where(a => a.Name.Equals(item.Name) && a.Id != item.Id).ToList();
                if (lst.Count > 0)
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}

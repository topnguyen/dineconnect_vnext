﻿using DinePlan.DineConnect.Connect.Print.Job.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Job.Exporting
{
    public interface IPrintJobExcelExporter
    {
        FileDto ExportToFile(List<GetAllPrintJobDto> items);
    }
}
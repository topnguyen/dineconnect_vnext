﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Print.Job.Dtos;
using DinePlan.DineConnect.Connect.Print.Job.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Print.Job
{
    
    public class PrintJobAppService : DineConnectAppServiceBase, IPrintJobAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly IRepository<PrintJob> _printJobRepository;
        private readonly IRepository<PrinterMap> _printerMapRepository;

        private readonly IPrintJobExcelExporter _printTemplateExcelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrintJobAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<PrintConfiguration> printConfigurationRepository,
            IRepository<PrintJob> printJobRepository,
            IRepository<PrinterMap> printerMapRepository,
            IPrintJobExcelExporter printJobExcelExporter,
            ILocationAppService locationAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _printConfigurationRepository = printConfigurationRepository;
            _printJobRepository = printJobRepository;
            _printerMapRepository = printerMapRepository;

            _printTemplateExcelExporter = printJobExcelExporter;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<GetAllPrintJobDto>> GetAll(GetAllPrintJobInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = _printConfigurationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted && x.PrintConfigurationType == PrintConfigurationType.PrintJob)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    var printConfigurationIds = await FilterByLocations(_printJobRepository.GetAll(), input)
                        .Select(x => x.PrintConfigurationId)
                        .Distinct().ToListAsync();

                    query = query.Where(x => printConfigurationIds.Contains(x.Id));
                }

                var sortedItems = await query
                     .OrderBy(input.Sorting ?? "Id desc")
                     .PageBy(input)
                     .Select(x => ObjectMapper.Map<GetAllPrintJobDto>(x))
                     .ToListAsync();

                return new PagedResultDto<GetAllPrintJobDto>(await query.CountAsync(), sortedItems);
            }
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _printConfigurationRepository.GetAll().Where(x => x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            var items = await query.Select(x => ObjectMapper.Map<GetAllPrintJobDto>(x)).ToListAsync();

            return _printTemplateExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<EditPrintJobInput> GetForEdit(int id)
        {
            var printTemplate = await _printConfigurationRepository
                .FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            return ObjectMapper.Map<EditPrintJobInput>(printTemplate);
        }

        // --

        
        public async Task<int> Create(CreatePrintJobInput input)
        {
            var printTemplate = ObjectMapper.Map<PrintConfiguration>(input);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;
            printTemplate.PrintConfigurationType = PrintConfigurationType.PrintJob;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        
        public async Task<int> Edit(EditPrintJobInput input)
        {
            var printTemplate = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.Id && x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            ObjectMapper.Map(input, printTemplate);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        // --

        
        public async Task Delete(int id)
        {
            var PrintJob = _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            if (PrintJob == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var printJobIds = await _printJobRepository.GetAll()
                .Where(x => x.PrintConfigurationId == x.Id)
                .Select(x => x.Id)
                .ToListAsync();
            await _printerMapRepository.DeleteAsync(x => x.PrintJobId != null && printJobIds.Contains(x.PrintJobId.Value));
            await _printJobRepository.DeleteAsync(x => x.PrintConfigurationId == id);
            await _printConfigurationRepository.DeleteAsync(x => x.Id == id);
        }

        // --

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printConfigurationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _printConfigurationRepository.UpdateAsync(item);
            }
        }

        
        public async Task<PagedResultDto<GetAllPrintJobMappingDto>> GetAllMappings(GetAllPrintJobMappingInput input)
        {
            var count = await _printConfigurationRepository.CountAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            if (count == 0)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var query = _printJobRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId);
            query = FilterByLocations(query, input);

            var sortedItemsQuery = query
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input);
            var sortedItems = await sortedItemsQuery
                .Select(x => ObjectMapper.Map<GetAllPrintJobMappingDto>(x))
                .ToListAsync();

            return new PagedResultDto<GetAllPrintJobMappingDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<CreateOrEditPrintJobMappingInput> GetMappingForEdit(int id)
        {
            var value = await _printJobRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null)
                throw new UserFriendlyException(L("PrintJobNotFound"));

            var valueDto = ObjectMapper.Map<CreateOrEditPrintJobMappingInput>(value);
            GetLocationsForEdit(valueDto, valueDto.LocationGroup);

            valueDto.PrinterMapInputs = await _printerMapRepository.GetAll()
                .Include(e => e.Department)
                .Where(x => x.PrintJobId == id)
                .Select(x => ObjectMapper.Map<CreateOrEditPrinterMapInput>(x))
                .ToListAsync();

            return valueDto;
        }

        
        public async Task<int> CreateOrUpdateMapping(CreateOrEditPrintJobMappingInput input)
        {
            var printJob = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintJob);
            if (printJob == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await CheckPrintJobBusinessRules(input);

            var printerMapInputs = input.PrinterMapInputs;
            if ((input.Id ?? -1) > 0)
            {
                var value = await _printJobRepository.FirstOrDefaultAsync(x => x.Id == input.Id.Value);
                if (value == null)
                    throw new UserFriendlyException(L("PrintJobNotFound"));

                ObjectMapper.Map(input, value);
                UpdateLocations(value, input.LocationGroup);

                await _printJobRepository.UpdateAsync(value);

                if (printerMapInputs != null && printerMapInputs.Any())
                {
                    var printerMaps = (await _printerMapRepository
                        .GetAllListAsync(x => x.PrintJobId == value.Id))
                        .ToDictionary(x => x.Id, x => x);

                    var updatedTaxMappingIds = new List<int>();
                    foreach (var printerMapInput in printerMapInputs)
                    {
                        if (printerMapInput.Id.HasValue && printerMapInput.Id > 0 && printerMaps.ContainsKey(printerMapInput.Id.Value))
                        {
                            var printerMapId = printerMapInput.Id.Value;
                            var printerMap = printerMaps[printerMapId];

                            ObjectMapper.Map(printerMapInput, printerMap);
                            printerMap.Department = null;
                            await _printerMapRepository.UpdateAsync(printerMap);

                            updatedTaxMappingIds.Add(printerMapId);
                        }
                        else
                        {
                            var printerMap = ObjectMapper.Map<PrinterMap>(printerMapInput);
                            printerMap.PrintJobId = value.Id;
                            printerMap.Department = null;

                            await _printerMapRepository.InsertAndGetIdAsync(printerMap);
                        }
                    }

                    var deletedIds = printerMaps.Keys.Where(x => !updatedTaxMappingIds.Contains(x)).ToList();
                    foreach (var id in deletedIds)
                        await _printerMapRepository.DeleteAsync(id);
                }

                return value.Id;
            }
            else
            {
                var value = ObjectMapper.Map<PrintJob>(input);
                value.TenantId = AbpSession.TenantId ?? 0;
                value.OrganizationId = _connectSession.OrgId;

                UpdateLocations(value, input.LocationGroup);

                await _printJobRepository.InsertOrUpdateAndGetIdAsync(value);
                if (printerMapInputs != null && printerMapInputs.Any())
                {
                    var printerMaps = printerMapInputs.Select(x => ObjectMapper.Map<PrinterMap>(x));
                    foreach (var printerMap in printerMaps)
                    {
                        printerMap.Id = 0;
                        printerMap.PrintJobId = value.Id;
                        printerMap.Department = null;

                        await _printerMapRepository.InsertAndGetIdAsync(printerMap);
                    }
                }

                return value.Id;
            }
        }

        private async Task CheckPrintJobBusinessRules(CreateOrEditPrintJobMappingInput input)
        {
            var locationGroupDto = input.LocationGroup;
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedValuesQuery = _printJobRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId)
                .WhereIf((input.Id ?? -1) > 0, x => x.Id != input.Id);

            var existedValues = await existedValuesQuery.ToListAsync();
            existedValues.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        
        public async Task DeleteMapping(int id)
        {
            await _printerMapRepository.DeleteAsync(x => x.PrintJobId == id);
            await _printJobRepository.DeleteAsync(id);
        }

        // --

        public ListResultDto<ComboboxItemDto> GetWhatToPrints()
        {
            var returnList = new List<ComboboxItemDto>();
            var names = Enum.GetNames(typeof(WhatToPrintTypes));
            var values = Enum.GetValues(typeof(WhatToPrintTypes));

            for (int i = 0; i < names.Length; i++)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = names[i],
                    Value = Convert.ToInt32(values.GetValue(i)).ToString()
                });
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ApiPrintJobOutput> ApiPrintJob(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var output = new ApiPrintJobOutput();

            var printJobs = await _printJobRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            var myPJs = new List<PrintJob>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pj in printJobs)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pj.Locations,
                        Group = pj.Group,
                        LocationTag = pj.LocationTag,
                        NonLocations = pj.NonLocations
                    }))
                    {
                        myPJs.Add(pj);
                    }
                }
            }
            else
            {
                myPJs = printJobs.ToList();
            }

            foreach (var pt in myPJs)
            {
                var myPrinter = ObjectMapper.Map<ApiPrintJob>(pt);
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a => a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pC.Id;
                        myPrinter.Name = pC.Name;
                        output.PrintJobs.Add(myPrinter);
                    }
                }
            }

            return output;
        }
    }
}
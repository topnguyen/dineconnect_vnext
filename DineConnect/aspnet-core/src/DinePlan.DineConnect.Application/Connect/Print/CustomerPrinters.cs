﻿using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;

namespace DinePlan.DineConnect.Connect.Print
{
    public class CustomerPrinterProcessors
    {
        private const string IPPattern = @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
        public static readonly Dictionary<string, List<FormlyType>> AllProcessors
            = new Dictionary<string, List<FormlyType>>
            {
                    {
                        "Dine Queue", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "IpAddress",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Ip Address",
                                    Placeholder = "Ip Address",
                                    Pattern = IPPattern
                                }
                            },
                            new FormlyType
                            {
                                Key = "Port",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "number",
                                    Label = "Port",
                                    Placeholder = "Port"
                                }
                            },
                            new FormlyType
                            {
                                Key = "AliasName",
                                Type = "checkbox",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Label = "Alias Name"
                                }
                            },
                            new FormlyType
                            {
                                Key = "Numerator",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "number",
                                    Label = "Numerator",
                                    Placeholder = "Numerator"
                                }
                            }
                        }
                    },
                    {
                        "Second Monitor", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "WriteFile",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Write File",
                                    Placeholder = "Write File"
                                }
                            },
                            new FormlyType
                            {
                                Key = "OpeningFile",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Opening File",
                                    Placeholder = "Opening File"
                                }
                            },
                            new FormlyType
                            {
                                Key = "SecondMonitorPath",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Second Monitor Path",
                                    Placeholder = "Second Monitor Path"
                                }
                            }
                        }
                    },
                    {
                        "Kitchen Display", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "IpAddress",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Ip Address",
                                    Placeholder = "IpAddress Name",
                                    Pattern = IPPattern
                                }
                            },
                            new FormlyType
                            {
                                Key = "Port",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "number",
                                    Label = "Port",
                                    Placeholder = "Port"
                                }
                            },
                            new FormlyType
                            {
                                Key = "AliasName",
                                Type = "checkbox",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Label = "Alias Name"
                                }
                            },
                            new FormlyType
                            {
                                Key = "Numerator",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "number",
                                    Label = "Numerator",
                                    Placeholder = "Numerator"
                                }
                            }
                        }
                    },
                    {
                        "Browser Printer", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "UpdateFile",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Update File",
                                    Placeholder = "Update File"
                                }
                            },
                            new FormlyType
                            {
                                Key = "OpeningFile",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Opening File",
                                    Placeholder = "Opening File"
                                }
                            },
                            new FormlyType
                            {
                                Key = "StartArguments",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Start Arguments",
                                    Placeholder = "Start Arguments"
                                }
                            },
                            new FormlyType
                            {
                                Key = "KioskMode",
                                Type = "checkbox",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Label = "Kiosk Mode"
                                }
                            }
                        }
                    },
                    {
                        "SaveFile Printer", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "FileName",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "File Name",
                                    Placeholder = "File Name"
                                }
                            }
                        }
                    },
                    {
                        "Setting Printer", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "IsLocal",
                                Type = "checkbox",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Label = "Is Local"
                                }
                            },
                            new FormlyType
                            {
                                Key = "SettingName",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Setting Name",
                                    Placeholder = "Setting Name"
                                }
                            }
                        }
                    },
                    {
                        "Url Printer", new List<FormlyType>
                        {
                            new FormlyType
                            {
                                Key = "UrlFormat",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Url Format",
                                    Placeholder = "Url Format"
                                }
                            },
                            new FormlyType
                            {
                                Key = "TokenChar",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Token Char",
                                    Placeholder = "Token Char"
                                }
                            },
                            new FormlyType
                            {
                                Key = "LiveMode",
                                Type = "checkbox",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Label = "Live Mode"
                                }
                            },
                            new FormlyType
                            {
                                Key = "DataFormat",
                                Type = "input",
                                TemplateOptions = new FormlyTemplateOption
                                {
                                    Type = "string",
                                    Label = "Data Format",
                                    Placeholder = "Data Format"
                                }
                            }
                        }
                    }
            };


        public static readonly string[] Printers = AllProcessors.Keys.ToArray();
    }
}

﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Print.Printers.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.Print.Printers.Exporting
{
    public class PrinterExcelExporter : NpoiExcelExporterBase, IPrinterExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public PrinterExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<GetAllPrinterDto> items)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;
            
            return CreateExcelPackage(
                "PrinterList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PrintTemplate"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateTimeSetting)
                        );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
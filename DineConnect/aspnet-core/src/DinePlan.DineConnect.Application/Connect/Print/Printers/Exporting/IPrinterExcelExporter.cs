﻿using DinePlan.DineConnect.Connect.Print.Printers.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Print.Printers.Exporting
{
    public interface IPrinterExcelExporter
    {
        Task<FileDto> ExportToFile(List<GetAllPrinterDto> items);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Connect.Print.Printers.Dtos;
using DinePlan.DineConnect.Connect.Print.Printers.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Print.Printers
{
    
    public class PrintersAppService : DineConnectAppServiceBase, IPrinterAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly IRepository<Printer> _printerRepository;

        private readonly IPrinterExcelExporter _printerExcelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrintersAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<PrintConfiguration> printConfigurationRepository,
            IRepository<Printer> printerRepository,
            IPrinterExcelExporter printerExcelExporter,
            ILocationAppService locationAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _printConfigurationRepository = printConfigurationRepository;
            _printerRepository = printerRepository;

            _printerExcelExporter = printerExcelExporter;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<GetAllPrinterDto>> GetAll(GetAllPrinterInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = _printConfigurationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted && x.PrintConfigurationType == PrintConfigurationType.Printer)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    var PrintConfigurationIds = await FilterByLocations(_printerRepository.GetAll(), input)
                        .Select(x => x.PrintConfigurationId)
                        .Distinct().ToListAsync();

                    query = query.Where(x => PrintConfigurationIds.Contains(x.Id));
                }

                var sortedItems = await query
                     .OrderBy(input.Sorting ?? "Id desc")
                     .PageBy(input)
                     .Select(x => ObjectMapper.Map<GetAllPrinterDto>(x))
                     .ToListAsync();

                return new PagedResultDto<GetAllPrinterDto>(await query.CountAsync(), sortedItems);
            }
        }

        public Task<ListResultDto<ComboboxItemDto>> GetPrinterProcessors()
        {
            var returnList = new List<ComboboxItemDto>();

            foreach (var name in CustomerPrinterProcessors.Printers)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = name.ToString()
                });
            }

            return Task.FromResult(new ListResultDto<ComboboxItemDto>(returnList));
        }

        public Task<List<FormlyType>> GetPrinterProcessorSetting(string stringId)
        {
            return Task.FromResult(CustomerPrinterProcessors.AllProcessors[stringId]);
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _printConfigurationRepository.GetAll().Where(x => x.PrintConfigurationType == PrintConfigurationType.Printer);
            var items = await query.Select(x => ObjectMapper.Map<GetAllPrinterDto>(x)).ToListAsync();

            return await _printerExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<EditPrinterInput> GetForEdit(int id)
        {
            var printer = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.Printer);
            if (printer == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            return ObjectMapper.Map<EditPrinterInput>(printer);
        }

        // --

        
        public async Task<int> Create(CreatePrinterInput input)
        {
            var printer = ObjectMapper.Map<PrintConfiguration>(input);
            printer.TenantId = AbpSession.TenantId ?? 0;
            printer.PrintConfigurationType = PrintConfigurationType.Printer;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printer);
        }

        
        public async Task<int> Edit(EditPrinterInput input)
        {
            var printer = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.Id && x.PrintConfigurationType == PrintConfigurationType.Printer);
            if (printer == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            ObjectMapper.Map(input, printer);
            printer.TenantId = AbpSession.TenantId ?? 0;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printer);
        }

        // --

        
        public async Task Delete(int id)
        {
            var printer = _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.Printer);
            if (printer == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await _printerRepository.DeleteAsync(x => x.PrintConfigurationId == id);
            await _printConfigurationRepository.DeleteAsync(x => x.Id == id);
        }

        // --

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printConfigurationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _printConfigurationRepository.UpdateAsync(item);
            }
        }

        
        public async Task<PagedResultDto<GetAllPrinterMappingDto>> GetAllPrinterTemplates(GetAllPrinterTemplateInput input)
        {
            var count = await _printConfigurationRepository.CountAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.Printer);
            if (count == 0)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var query = _printerRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId);
            query = FilterByLocations(query, input);

            var sortedItemsQuery = query
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input);

            var sortedItems = await sortedItemsQuery
                .Select(x => ObjectMapper.Map<GetAllPrinterMappingDto>(x))
                .ToListAsync();

            var fallBackPrintConfigurationIds = await sortedItemsQuery
                .Where(x => x.FallBackPrinter > 0)
                .Select(x => x.FallBackPrinter)
                .Distinct().ToListAsync();
            var fallBackPrintConfigurations = await _printConfigurationRepository.GetAll()
                 .Where(x => fallBackPrintConfigurationIds.Contains(x.Id) && x.PrintConfigurationType == PrintConfigurationType.Printer)
                 .Select(x => new { x.Id, x.Name })
                 .ToDictionaryAsync(x => x.Id, x => x.Name);

            var printerTypeNames = Enum.GetNames(typeof(PrinterTypes));
            var printerTypeValues = Enum.GetValues(typeof(PrinterTypes));
            var printerTypes = new Dictionary<int, string>();

            for (int i = 0; i < printerTypeNames.Length; i++)
                printerTypes.Add(Convert.ToInt32(printerTypeValues.GetValue(i)), L(printerTypeNames[i]));

            sortedItems.ForEach(x =>
            {
                if (fallBackPrintConfigurations.ContainsKey(x.FallBackPrinter))
                    x.FallBackPrinterName = fallBackPrintConfigurations[x.FallBackPrinter];

                if (printerTypes.ContainsKey(x.PrinterType))
                    x.PrinterTypeName = printerTypes[x.PrinterType];
            });

            return new PagedResultDto<GetAllPrinterMappingDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<CreateOrEditPrinterMappingInput> GetPrinterTemplateForEdit(int id)
        {
            var value = await _printerRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null)
                throw new UserFriendlyException(L("PrinterNotFound"));

            var valueDto = ObjectMapper.Map<CreateOrEditPrinterMappingInput>(value);
            GetLocationsForEdit(valueDto, valueDto.LocationGroup);

            return valueDto;
        }

        
        public async Task<int> CreateOrUpdatePrinterTemplate(CreateOrEditPrinterMappingInput input)
        {
            var printer = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.Printer);
            if (printer == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await CheckPrinterBusinessRules(input);

            input.FallBackPrinter = input.FallBackPrinter ?? 0;

            if ((input.Id ?? -1) > 0)
            {
                var value = await _printerRepository.FirstOrDefaultAsync(x => x.Id == input.Id.Value);
                if (value == null)
                    throw new UserFriendlyException(L("PrinterNotFound"));

                ObjectMapper.Map(input, value);
                UpdateLocations(value, input.LocationGroup);

                await _printerRepository.UpdateAsync(value);

                return value.Id;
            }
            else
            {
                var value = ObjectMapper.Map<Printer>(input);
                value.TenantId = AbpSession.TenantId ?? 0;
                value.OrganizationId = _connectSession.OrgId;

                UpdateLocations(value, input.LocationGroup);

                return await _printerRepository.InsertOrUpdateAndGetIdAsync(value);
            }
        }

        private async Task CheckPrinterBusinessRules(CreateOrEditPrinterMappingInput input)
        {
            var locationGroupDto = input.LocationGroup;
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedValuesQuery = _printerRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId)
                .WhereIf((input.Id ?? -1) > 0, x => x.Id != input.Id);

            var existedValues = await existedValuesQuery.ToListAsync();
            existedValues.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        
        public async Task DeletePrinterTemplate(int id)
        {
            await _printerRepository.DeleteAsync(id);
        }

        // --

        public ListResultDto<ComboboxItemDto> GetPrinterTypes()
        {
            var data = new List<ComboboxItemDto>();
            var printerTypes = Enum.GetNames(typeof(PrinterTypes));

            var i = 0;
            foreach (var name in printerTypes)
            {
                data.Add(new ComboboxItemDto
                {
                    DisplayText = L(name),
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(data);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetFallBackPrinters(int? id)
        {
            var returnList = await _printConfigurationRepository.GetAll()
                .WhereIf(id.HasValue && id.Value > 0, x => x.Id != id.Value && x.PrintConfigurationType == PrintConfigurationType.Printer)
                .Select(x => new ComboboxItemDto
                {
                    Value = x.Id.ToString(),
                    DisplayText = x.Name
                })
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPrintersForComboBox()
        {
            var items = await _printConfigurationRepository.GetAll()
                .Where(x => x.PrintConfigurationType == PrintConfigurationType.Printer)
                .Select(x => new ComboboxItemDto
                {
                    DisplayText = x.Name.ToString(),
                    Value = x.Id.ToString(),
                })
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(items);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPrinterTemplatesForComboBox()
        {
            var items = await _printConfigurationRepository.GetAll()
                .Where(x => x.PrintConfigurationType == PrintConfigurationType.PrintTemplate)
                .Select(x => new ComboboxItemDto
                {
                    DisplayText = x.Name.ToString(),
                    Value = x.Id.ToString(),
                })
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(items);
        }

        public async Task<ApiPrinterOutput> ApiPrinter(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var output = new ApiPrinterOutput();
            var printers = await _printerRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);
            var myPrinters = new List<Printer>();
            if (locationInput.LocationId != 0)
            {
                foreach (var printer in printers)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = printer.Locations,
                        Group = printer.Group,
                        LocationTag = printer.LocationTag,
                        NonLocations = printer.NonLocations
                    }))
                    {
                        myPrinters.Add(printer);
                    }
                }
            }
            else
            {
                myPrinters = printers.ToList();
            }

            foreach (var printer in myPrinters)
            {
                var myPrinter = ObjectMapper.Map<ApiPrinterDto>(printer);
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a => a.Id.Equals(printer.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = printer.PrintConfigurationId;
                        myPrinter.Name = pC.Name;
                        output.Printers.Add(myPrinter);
                    }
                }
            }

            return output;
        }
    }
}
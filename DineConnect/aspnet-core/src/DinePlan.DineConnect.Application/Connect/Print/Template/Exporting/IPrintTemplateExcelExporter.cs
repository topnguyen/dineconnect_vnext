﻿using DinePlan.DineConnect.Connect.Print.Template.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Template.Exporting
{
    public interface IPrintTemplateExcelExporter
    {
        FileDto ExportToFile(List<GetAllPrintTemplateDto> items);
    }
}
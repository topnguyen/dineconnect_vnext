﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Print.Template.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Template.Exporting
{
    public class PrintTemplateExcelExporter : NpoiExcelExporterBase, IPrintTemplateExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PrintTemplateExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllPrintTemplateDto> items)
        {
            return CreateExcelPackage(
                "PrintTemplateList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PrintTemplate"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(AppConsts.FileDateTimeFormat)
                        );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Print.Template.Dtos;
using DinePlan.DineConnect.Connect.Print.Template.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Print.Template
{
    
    public class PrintTemplateAppService : DineConnectAppServiceBase, IPrintTemplateAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly IRepository<PrintTemplate> _printTemplateRepository;

        private readonly IPrintTemplateExcelExporter _printTemplateExcelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrintTemplateAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<PrintConfiguration> printConfigurationRepository,
            IRepository<PrintTemplate> printTemplateRepository,
            IPrintTemplateExcelExporter printTemplateExcelExporter,
            ILocationAppService locationAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _printConfigurationRepository = printConfigurationRepository;
            _printTemplateRepository = printTemplateRepository;

            _printTemplateExcelExporter = printTemplateExcelExporter;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<GetAllPrintTemplateDto>> GetAll(GetAllPrintTemplateInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = _printConfigurationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    var printConfigurationIds = await FilterByLocations(_printTemplateRepository.GetAll(), input)
                        .Select(x => x.PrintConfigurationId)
                        .Distinct().ToListAsync();

                    query = query.Where(x => printConfigurationIds.Contains(x.Id));
                }

                var sortedItems = await query
                     .OrderBy(input.Sorting ?? "Id desc")
                     .PageBy(input)
                     .Select(x => ObjectMapper.Map<GetAllPrintTemplateDto>(x))
                     .ToListAsync();

                return new PagedResultDto<GetAllPrintTemplateDto>(await query.CountAsync(), sortedItems);
            }
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _printConfigurationRepository.GetAll().Where(x => x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            var items = await query.Select(x => ObjectMapper.Map<GetAllPrintTemplateDto>(x)).ToListAsync();

            return _printTemplateExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<EditPrintTemplateInput> GetForEdit(int id)
        {
            var printTemplate = await _printConfigurationRepository
                .FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            return ObjectMapper.Map<EditPrintTemplateInput>(printTemplate);
        }

        // --

        
        public async Task<int> Create(CreatePrintTemplateInput input)
        {
            var printTemplate = ObjectMapper.Map<PrintConfiguration>(input);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;
            printTemplate.PrintConfigurationType = PrintConfigurationType.PrintTemplate;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        
        public async Task<int> Edit(EditPrintTemplateInput input)
        {
            var printTemplate = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.Id && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            ObjectMapper.Map(input, printTemplate);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        // --

        
        public async Task Delete(int id)
        {
            var PrintTemplate = _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            if (PrintTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await _printTemplateRepository.DeleteAsync(x => x.PrintConfigurationId == id);
            await _printConfigurationRepository.DeleteAsync(x => x.Id == id);
        }

        // --

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printConfigurationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _printConfigurationRepository.UpdateAsync(item);
            }
        }

        
        public async Task<PagedResultDto<GetAllPrintTemplateMappingDto>> GetAllMappings(GetAllPrintTemplateMappingInput input)
        {
            var count = await _printConfigurationRepository.CountAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            if (count == 0)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var query = _printTemplateRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId);
            query = FilterByLocations(query, input);

            var sortedItemsQuery = query
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input);
            var sortedItems = await sortedItemsQuery
                .Select(x => ObjectMapper.Map<GetAllPrintTemplateMappingDto>(x))
                .ToListAsync();

            return new PagedResultDto<GetAllPrintTemplateMappingDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<CreateOrEditPrintTemplateMappingInput> GetMappingForEdit(int id)
        {
            var value = await _printTemplateRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null)
                throw new UserFriendlyException(L("PrintTemplateNotFound"));

            var valueDto = ObjectMapper.Map<CreateOrEditPrintTemplateMappingInput>(value);
            GetLocationsForEdit(valueDto, valueDto.LocationGroup);

            return valueDto;
        }

        
        public async Task<int> CreateOrUpdateMapping(CreateOrEditPrintTemplateMappingInput input)
        {
            var PrintTemplate = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            if (PrintTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await CheckPrintTemplateBusinessRules(input);

            if ((input.Id ?? -1) > 0)
            {
                var value = await _printTemplateRepository.FirstOrDefaultAsync(x => x.Id == input.Id.Value);
                if (value == null)
                    throw new UserFriendlyException(L("PrintTemplateNotFound"));

                ObjectMapper.Map(input, value);
                UpdateLocations(value, input.LocationGroup);

                await _printTemplateRepository.UpdateAsync(value);

                return value.Id;
            }
            else
            {
                var value = ObjectMapper.Map<PrintTemplate>(input);
                value.TenantId = AbpSession.TenantId ?? 0;
                value.OrganizationId = _connectSession.OrgId;

                UpdateLocations(value, input.LocationGroup);

                return await _printTemplateRepository.InsertOrUpdateAndGetIdAsync(value);
            }
        }

        private async Task CheckPrintTemplateBusinessRules(CreateOrEditPrintTemplateMappingInput input)
        {
            var locationGroupDto = input.LocationGroup;
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedValuesQuery = _printTemplateRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId)
                .WhereIf((input.Id ?? -1) > 0, x => x.Id != input.Id);

            var existedValues = await existedValuesQuery.ToListAsync();
            existedValues.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        
        public async Task DeleteMapping(int id)
        {
            await _printTemplateRepository.DeleteAsync(id);
        }

        public async Task<ApiPrinterTemplateOutput> ApiPrinterTemplate(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var output = new ApiPrinterTemplateOutput();
            var printTemplates = await _printTemplateRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);
            var myPTs = new List<PrintTemplate>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pt in printTemplates)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pt.Locations,
                        Group = pt.Group,
                        LocationTag = pt.LocationTag,
                        NonLocations = pt.NonLocations
                    }))
                    {
                        myPTs.Add(pt);
                    }
                }
            }
            else
            {
                myPTs = printTemplates.ToList();
            }

            foreach (var pt in myPTs)
            {
                var myPrinter = ObjectMapper.Map<ApiPrinterTemplateDto>(pt);
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a => a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pt.PrintConfigurationId;
                        myPrinter.Name = pC.Name;
                        output.PrinterTemplates.Add(myPrinter);
                    }
                }
            }

            return output;
        }
    }
}
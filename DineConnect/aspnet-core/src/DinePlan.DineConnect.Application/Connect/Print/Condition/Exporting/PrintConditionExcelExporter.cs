﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Print.Condition.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Condition.Exporting
{
    public class PrintConditionExcelExporter : NpoiExcelExporterBase, IPrintConditionExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PrintConditionExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllPrintConditionDto> items)
        {
            return CreateExcelPackage(
                "PrintTemplateList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PrintTemplate"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(AppConsts.FileDateTimeFormat)
                        );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
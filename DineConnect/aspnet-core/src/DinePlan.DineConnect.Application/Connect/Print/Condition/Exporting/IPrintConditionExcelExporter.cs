﻿using DinePlan.DineConnect.Connect.Print.Condition.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Condition.Exporting
{
    public interface IPrintConditionExcelExporter
    {
        FileDto ExportToFile(List<GetAllPrintConditionDto> items);
    }
}
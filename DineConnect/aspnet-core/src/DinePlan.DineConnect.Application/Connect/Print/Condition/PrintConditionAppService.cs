﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Print.Condition.Dtos;
using DinePlan.DineConnect.Connect.Print.Condition.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Print.Condition
{
    
    public class PrintConditionAppService : DineConnectAppServiceBase, IPrintConditionAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly IRepository<PrintTemplateCondition> _printTemplateConditionRepository;

        private readonly IPrintConditionExcelExporter _printTemplateExcelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrintConditionAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<PrintConfiguration> printConfigurationRepository,
            IRepository<PrintTemplateCondition> printTemplateConditionRepository,
            IPrintConditionExcelExporter printConditionExcelExporter,
            ILocationAppService locationAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _printConfigurationRepository = printConfigurationRepository;
            _printTemplateConditionRepository = printTemplateConditionRepository;

            _printTemplateExcelExporter = printConditionExcelExporter;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<GetAllPrintConditionDto>> GetAll(GetAllPrintConditionInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = _printConfigurationRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted && x.PrintConfigurationType == PrintConfigurationType.PrintCondition)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    var printConfigurationIds = await FilterByLocations(_printTemplateConditionRepository.GetAll(), input)
                        .Select(x => x.PrintConfigurationId)
                        .Distinct().ToListAsync();

                    query = query.Where(x => printConfigurationIds.Contains(x.Id));
                }

                var sortedItems = await query
                     .OrderBy(input.Sorting ?? "Id desc")
                     .PageBy(input)
                     .Select(x => ObjectMapper.Map<GetAllPrintConditionDto>(x))
                     .ToListAsync();

                return new PagedResultDto<GetAllPrintConditionDto>(await query.CountAsync(), sortedItems);
            }
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _printConfigurationRepository.GetAll().Where(x => x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            var items = await query.Select(x => ObjectMapper.Map<GetAllPrintConditionDto>(x)).ToListAsync();

            return _printTemplateExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<EditPrintConditionInput> GetForEdit(int id)
        {
            var printTemplate = await _printConfigurationRepository
                .FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            return ObjectMapper.Map<EditPrintConditionInput>(printTemplate);
        }

        // --

        
        public async Task<int> Create(CreatePrintConditionInput input)
        {
            var printTemplate = ObjectMapper.Map<PrintConfiguration>(input);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;
            printTemplate.PrintConfigurationType = PrintConfigurationType.PrintCondition;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        
        public async Task<int> Edit(EditPrintConditionInput input)
        {
            var printTemplate = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.Id && x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            if (printTemplate == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            ObjectMapper.Map(input, printTemplate);
            printTemplate.TenantId = AbpSession.TenantId ?? 0;

            return await _printConfigurationRepository.InsertOrUpdateAndGetIdAsync(printTemplate);
        }

        // --

        
        public async Task Delete(int id)
        {
            var PrintTemplateCondition = _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == id && x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            if (PrintTemplateCondition == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var PrintTemplateConditionIds = await _printTemplateConditionRepository.GetAll()
                .Where(x => x.PrintConfigurationId == x.Id)
                .Select(x => x.Id)
                .ToListAsync();

            await _printTemplateConditionRepository.DeleteAsync(x => x.PrintConfigurationId == id);
            await _printConfigurationRepository.DeleteAsync(x => x.Id == id);
        }

        // --

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printConfigurationRepository.GetAsync(input.Id);

                item.IsDeleted = false;
                await _printConfigurationRepository.UpdateAsync(item);
            }
        }

        
        public async Task<PagedResultDto<GetAllPrintConditionMappingDto>> GetAllMappings(GetAllPrintConditionMappingInput input)
        {
            var count = await _printConfigurationRepository.CountAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            if (count == 0)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            var query = _printTemplateConditionRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId);
            query = FilterByLocations(query, input);

            var sortedItemsQuery = query
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input);
            var sortedItems = await sortedItemsQuery
                .Select(x => ObjectMapper.Map<GetAllPrintConditionMappingDto>(x))
                .ToListAsync();

            return new PagedResultDto<GetAllPrintConditionMappingDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<CreateOrEditPrintConditionMappingInput> GetMappingForEdit(int id)
        {
            var value = await _printTemplateConditionRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (value == null)
                throw new UserFriendlyException(L("PrintTemplateConditionNotFound"));

            var valueDto = ObjectMapper.Map<CreateOrEditPrintConditionMappingInput>(value);
            GetLocationsForEdit(valueDto, valueDto.LocationGroup);

            valueDto.ConditionScheduleList = JsonConvert.DeserializeObject<List<PrintConditionMappingScheduleEditDto>>(valueDto.ConditionSchedules);

            return valueDto;
        }

        
        public async Task<int> CreateOrUpdateMapping(CreateOrEditPrintConditionMappingInput input)
        {
            var PrintTemplateCondition = await _printConfigurationRepository.FirstOrDefaultAsync(x => x.Id == input.PrintConfigurationId && x.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            if (PrintTemplateCondition == null)
                throw new UserFriendlyException(L("PrintConfigurationNotFound"));

            await CheckPrintTemplateConditionBusinessRules(input);

            if ((input.Id ?? -1) > 0)
            {
                var value = await _printTemplateConditionRepository.FirstOrDefaultAsync(x => x.Id == input.Id.Value);
                if (value == null)
                    throw new UserFriendlyException(L("PrintTemplateConditionNotFound"));

                ObjectMapper.Map(input, value);
                UpdateLocations(value, input.LocationGroup);

                await _printTemplateConditionRepository.UpdateAsync(value);

                return value.Id;
            }
            else
            {
                var value = ObjectMapper.Map<PrintTemplateCondition>(input);
                value.TenantId = AbpSession.TenantId ?? 0;
                value.OrganizationId = _connectSession.OrgId;

                UpdateLocations(value, input.LocationGroup);

                await _printTemplateConditionRepository.InsertOrUpdateAndGetIdAsync(value);

                return value.Id;
            }
        }

        private async Task CheckPrintTemplateConditionBusinessRules(CreateOrEditPrintConditionMappingInput input)
        {
            var locationGroupDto = input.LocationGroup;
            var existedLocationDtos = new List<SimpleLocationDto>();
            var existedLocationGroupDtos = new List<SimpleLocationGroupDto>();
            var existedLocationTagDtos = new List<SimpleLocationTagDto>();

            var existedQuery = _printTemplateConditionRepository.GetAll()
                .Where(x => x.PrintConfigurationId == input.PrintConfigurationId)
                .WhereIf((input.Id ?? -1) > 0, x => x.Id != input.Id);

            var existedEntries = await existedQuery.ToListAsync();
            existedEntries.ForEach(x =>
            {
                var existedLocationGroup = GetLocationsFromEntity(x);

                if (existedLocationGroup.Locations.Any())
                    existedLocationDtos.AddRange(existedLocationGroup.Locations);

                if (existedLocationGroup.Groups.Any())
                    existedLocationGroupDtos.AddRange(existedLocationGroup.Groups);

                if (existedLocationGroup.LocationTags.Any())
                    existedLocationTagDtos.AddRange(existedLocationGroup.LocationTags);
            });

            if (existedLocationDtos.Any())
            {
                var ids = locationGroupDto.Locations.Select(x => x.Id);
                var duplicatedLocations = existedLocationDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocations.Any())
                {
                    var names = string.Join(", ", duplicatedLocations.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocations.Count() == 1 ? "DuplicatedLocation" : "DuplicatedLocations"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationGroupDtos.Any())
            {
                var ids = locationGroupDto.Groups.Select(x => x.Id);
                var duplicatedLocationGroups = existedLocationGroupDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationGroups.Any())
                {
                    var names = string.Join(", ", duplicatedLocationGroups.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationGroups.Count() == 1 ? "DuplicatedLocationGroup" : "DuplicatedLocationGroups"), names);
                    throw new UserFriendlyException(message);
                }
            }

            if (existedLocationTagDtos.Any())
            {
                var ids = locationGroupDto.LocationTags.Select(x => x.Id);
                var duplicatedLocationTags = existedLocationTagDtos.Where(x => ids.Contains(x.Id));

                if (duplicatedLocationTags.Any())
                {
                    var names = string.Join(", ", duplicatedLocationTags.Select(x => x.Name));
                    var message = string.Format(L(duplicatedLocationTags.Count() == 1 ? "DuplicatedLocationTag" : "DuplicatedLocationTags"), names);
                    throw new UserFriendlyException(message);
                }
            }
        }

        
        public async Task DeleteMapping(int id)
        {
            await _printTemplateConditionRepository.DeleteAsync(id);
        }

        // --

        public ListResultDto<ComboboxItemDto> GetWhatToPrints()
        {
            var returnList = new List<ComboboxItemDto>();
            var names = Enum.GetNames(typeof(WhatToPrintTypes));
            var values = Enum.GetValues(typeof(WhatToPrintTypes));

            for (int i = 0; i < names.Length; i++)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = names[i],
                    Value = Convert.ToInt32(values.GetValue(i)).ToString()
                });
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ApiPrinterConditionOutput> ApiPrinterTemplateCondition(ApiLocationInput locationInput)
        {
            var output = new ApiPrinterConditionOutput();

            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization, DineConnectDataFilters.Parameters.OrganizationId, orgId);
            }

            var printTemplateConditions = await _printTemplateConditionRepository.GetAllListAsync(a =>
                a.TenantId == locationInput.TenantId && a.EndDate >= DateTime.Now);

            var myPJs = new List<PrintTemplateCondition>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pj in printTemplateConditions)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pj.Locations,
                        Group = pj.Group,
                        LocationTag = pj.LocationTag,
                        NonLocations = pj.NonLocations
                    }))
                    {
                        myPJs.Add(pj);
                    }
                }
            }
            else
            {
                myPJs = printTemplateConditions.ToList();
            }

            foreach (var pt in myPJs)
            {
                var myPrinter = ObjectMapper.Map<ApiPrinterTemplateConditionDto>(pt);
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a => a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pC.Id;
                        myPrinter.Name = pC.Name;
                        output.PrintTemplateConditions.Add(myPrinter);
                    }
                }
            }

            return output;
        }
    }
}
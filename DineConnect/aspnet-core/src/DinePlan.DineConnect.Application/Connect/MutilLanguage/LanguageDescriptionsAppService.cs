﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.MutilLanguage.Dtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.MutilLanguage
{
	public class LanguageDescriptionsAppService : DineConnectAppServiceBase, ILanguageDescriptionsAppService
	{
		private readonly IRepository<LanguageDescription> _languageDescriptionRepository;

		public LanguageDescriptionsAppService(IRepository<LanguageDescription> languageDescriptionRepository)
		{
			_languageDescriptionRepository = languageDescriptionRepository;
		}

		public async Task<PagedResultDto<LanguageDescriptionDto>> GetAll(GetAllLanguageDescriptionsInput input)
		{
			LanguageDescriptionType type = 0;
			if (input.LanguageDescriptionType.IsNullOrEmpty()) { type = 0; }
			else
			{
				type = (LanguageDescriptionType)Enum.Parse(typeof(LanguageDescriptionType), input.LanguageDescriptionType);
			}
			var filteredLanguageDescriptions = _languageDescriptionRepository.GetAll()
						.Where(t => t.ReferenceId == input.ReferenceId && t.LanguageDescriptionType == type)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LanguageCode.Contains(input.Filter) || e.Name.Contains(input.Filter));

			var pagedAndFilteredLanguageDescriptions = filteredLanguageDescriptions
				.OrderBy(input.Sorting ?? "id asc")
				.PageBy(input);

			var totalCount = await filteredLanguageDescriptions.CountAsync();

			var dbList = await pagedAndFilteredLanguageDescriptions.ToListAsync();
			var results = ObjectMapper.Map<List<LanguageDescriptionDto>>(dbList);

			return new PagedResultDto<LanguageDescriptionDto>(
				totalCount,
				results
			);
		}

		public async Task<GetLanguageDescriptionForEditOutput> GetLanguageDescriptionForEdit(NullableIdDto input)
		{
			CreateOrEditLanguageDescriptionDto editDto;

			if (input.Id.HasValue)
			{
				var entity = await _languageDescriptionRepository.GetAsync(input.Id.Value);
				editDto = ObjectMapper.Map<CreateOrEditLanguageDescriptionDto>(entity);
			}
			else
			{
				editDto = new CreateOrEditLanguageDescriptionDto();
			}

			return new GetLanguageDescriptionForEditOutput
			{
				LanguageDescription = editDto
			};
		}

		public async Task CreateOrEdit(CreateOrEditLanguageDescriptionDto input)
		{
			input.Name = input.Name.Trim().ToUpper();

			if (input.Type.IsNullOrEmpty()) { input.LanguageDescriptionType = 0; }
			else
			{
				input.LanguageDescriptionType = (int)Enum.Parse(typeof(LanguageDescriptionType), input.Type);
			}

			bool updateDesc = input.Id.HasValue;

			var myDesc = await _languageDescriptionRepository
			.FirstOrDefaultAsync(t => t.LanguageCode.Equals(input.LanguageCode) && t.ReferenceId == input.ReferenceId && t.LanguageDescriptionType == (LanguageDescriptionType)input.LanguageDescriptionType);

			if (myDesc != null)
			{
				input.Id = myDesc.Id;
				updateDesc = true;
			}

			if (updateDesc)
			{
				await Update(input);
			}
			else
			{
				await Create(input);
			}
		}

		protected virtual async Task Create(CreateOrEditLanguageDescriptionDto input)
		{
			var languageDescription = ObjectMapper.Map<LanguageDescription>(input);

			if (AbpSession.TenantId != null)
			{
				languageDescription.TenantId = (int)AbpSession.TenantId;
			}

			await _languageDescriptionRepository.InsertAsync(languageDescription);
		}

		protected virtual async Task Update(CreateOrEditLanguageDescriptionDto input)
		{
			var languageDescription = await _languageDescriptionRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, languageDescription);
		}

		public async Task Delete(EntityDto input)
		{
			await _languageDescriptionRepository.DeleteAsync(input.Id);
		}
	}
}
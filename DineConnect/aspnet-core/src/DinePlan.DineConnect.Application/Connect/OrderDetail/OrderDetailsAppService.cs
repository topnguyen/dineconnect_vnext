﻿

using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.OrderDetail.Exporting;
using DinePlan.DineConnect.Connect.OrderDetail.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.Configuration;
using Microsoft.Extensions.Configuration;
using Omise;
using Omise.Models;
using Abp.UI;
using DinePlan.DineConnect.Tiffins.Payment;
using DinePlan.DineConnect.Authorization.Users;
using Abp.Authorization.Users;
using DinePlan.DineConnect.Authorization.Roles;

namespace DinePlan.DineConnect.Connect.OrderDetail
{
    [AbpAuthorize]
    public class OrderDetailsAppService : DineConnectAppServiceBase, IOrderDetailsAppService
    {
        private readonly IRepository<OrderDetail, long> _orderDetailRepository;
        private readonly IOrderDetailsExcelExporter _orderDetailsExcelExporter;
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IOmiseConfiguration _omiseConfiguration;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly RoleManager _roleManager;

        public OrderDetailsAppService(IRepository<UserRole, long> userRoleRepository, RoleManager roleManager, IAppConfigurationAccessor configurationAccessor, IOmiseConfiguration omiseConfiguration, IRepository<OrderDetail, long> orderDetailRepository, IOrderDetailsExcelExporter orderDetailsExcelExporter)
        {
            _orderDetailRepository = orderDetailRepository;
            _orderDetailsExcelExporter = orderDetailsExcelExporter;
            _appConfiguration = configurationAccessor.Configuration;
            _omiseConfiguration = omiseConfiguration;
            _userRoleRepository = userRoleRepository;
            _roleManager = roleManager;
        }

        public async Task<PagedResultDto<GetOrderDetailForViewDto>> GetAll(GetAllOrderDetailsInput input)
        {
           

            var filteredOrderDetails = _orderDetailRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.OrderNotes.Contains(input.Filter) || e.OrderDetails.Contains(input.Filter) || e.Building.Contains(input.Filter) || e.Street.Contains(input.Filter) || e.FlatNo.Contains(input.Filter) || e.Landmark.Contains(input.Filter) || e.Currency.Contains(input.Filter))
                        .WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
                        .WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
                        .WhereIf(input.MinCreatorUserIdFilter != null, e => e.CreatorUserId >= input.MinCreatorUserIdFilter)
                        .WhereIf(input.MaxCreatorUserIdFilter != null, e => e.CreatorUserId <= input.MaxCreatorUserIdFilter)
                        .WhereIf(input.MinCheckoutStatusFilter != null, e => e.CheckoutStatus >= input.MinCheckoutStatusFilter)
                        .WhereIf(input.MaxCheckoutStatusFilter != null, e => e.CheckoutStatus <= input.MaxCheckoutStatusFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinDeliveryLocationIdFilter != null, e => e.DeliveryLocationId >= input.MinDeliveryLocationIdFilter)
                        .WhereIf(input.MaxDeliveryLocationIdFilter != null, e => e.DeliveryLocationId <= input.MaxDeliveryLocationIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BuildingFilter), e => e.Building == input.BuildingFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetFilter), e => e.Street == input.StreetFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FlatNoFilter), e => e.FlatNo == input.FlatNoFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LandmarkFilter), e => e.Landmark == input.LandmarkFilter)
                        .WhereIf(input.MinOrderPaymentMethodFilter != null, e => e.OrderPaymentMethod >= input.MinOrderPaymentMethodFilter)
                        .WhereIf(input.MaxOrderPaymentMethodFilter != null, e => e.OrderPaymentMethod <= input.MaxOrderPaymentMethodFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CurrencyFilter), e => e.Currency == input.CurrencyFilter);
            var currentUser = GetCurrentUser();
            var userRoles = await _userRoleRepository.GetAll()
                .Where(userRole => userRole.UserId == currentUser.Id)
                .Select(userRole => userRole).ToListAsync();
            var distinctRoleIds = userRoles.Select(userRole => userRole.RoleId).Distinct();
            var isAdmin = false;
            foreach (var roleId in distinctRoleIds)
            {
                var role = await _roleManager.FindByIdAsync(roleId.ToString());
                if (role != null && role.DisplayName.Equals(StaticRoleNames.Tenants.Admin))
                {
                    isAdmin = true;
                }
            }

            if (!isAdmin)
            {
                filteredOrderDetails = filteredOrderDetails.Where(x => x.CreatorUserId == currentUser.Id);
            }


            var pagedAndFilteredOrderDetails = filteredOrderDetails
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var orderDetails = from o in pagedAndFilteredOrderDetails
                               select new GetOrderDetailForViewDto()
                               {
                                   OrderDetail = new OrderDetailDto
                                   {
                                       CreationTime = o.CreationTime,
                                       CheckoutStatus = o.CheckoutStatus,
                                       Amount = o.Amount,
                                       OrderNotes = o.OrderNotes,
                                       OrderDetails = o.OrderDetails,
                                       DeliveryLocationId = o.DeliveryLocationId,
                                       Building = o.Building,
                                       Street = o.Street,
                                       FlatNo = o.FlatNo,
                                       Landmark = o.Landmark,
                                       OrderPaymentMethod = o.OrderPaymentMethod,
                                       Currency = o.Currency,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredOrderDetails.CountAsync();

            return new PagedResultDto<GetOrderDetailForViewDto>(
                totalCount,
                await orderDetails.ToListAsync()
            );
        }

        public async Task<GetOrderDetailForViewDto> GetOrderDetailForView(long id)
        {
            var orderDetail = await _orderDetailRepository.GetAsync(id);

            var output = new GetOrderDetailForViewDto { OrderDetail = ObjectMapper.Map<OrderDetailDto>(orderDetail) };

            return output;
        }

        [AbpAuthorize]
        public async Task<GetOrderDetailForEditOutput> GetOrderDetailForEdit(EntityDto<long> input)
        {
            var orderDetail = await _orderDetailRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOrderDetailForEditOutput { OrderDetail = ObjectMapper.Map<CreateOrEditOrderDetailDto>(orderDetail) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOrderDetailDto input)
        {
            if (input.PaymentOption == "omise")
            {

                if (string.IsNullOrEmpty(input.ChargedToken))
                {
                    throw new UserFriendlyException("The payment token/source is not valid.");
                }
                // charged before save details
                var pKey = _appConfiguration["Omise:OMISE_PUBLIC_KEY"];
                var sKey = _appConfiguration["Omise:OMISE_SECRET_KEY"];
                var ratioCurrency = 1;
                int.TryParse(_appConfiguration["Omise:RATIO_CURRENCY"], out ratioCurrency);
                var client = new Client(_omiseConfiguration.PublishableKey, _omiseConfiguration.SecretKey);
                var createChargeRequest = new CreateChargeRequest
                {
                    Amount = input.Amount * ratioCurrency,
                    Currency = input.Currency,
                    Card = input.ChargedToken,
                };
                try
                {
                    var charge = await client.Charges.Create(createChargeRequest);
                    input.CheckoutStatus = (int)charge.Status;
                    if (input.Id == null)
                    {
                        var curUser = GetCurrentUser();
                        input.CreatorUserId = curUser.Id;

                        await Create(input);


                    }
                    else
                    {
                        await Update(input);
                    }
                }
                catch
                {
                    throw new UserFriendlyException("Can not Charged, Please re-enter the charge information and try again");
                }
            }
        }

        [AbpAuthorize]
        protected virtual async Task Create(CreateOrEditOrderDetailDto input)
        {
            var orderDetail = ObjectMapper.Map<OrderDetail>(input);


            if (AbpSession.TenantId != null)
            {
                orderDetail.TenantId = (int)AbpSession.TenantId;
            }


            await _orderDetailRepository.InsertAsync(orderDetail);
        }

        [AbpAuthorize]
        protected virtual async Task Update(CreateOrEditOrderDetailDto input)
        {
            var orderDetail = await _orderDetailRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, orderDetail);
        }

        [AbpAuthorize]
        public async Task Delete(EntityDto<long> input)
        {
            await _orderDetailRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetOrderDetailsToExcel(GetAllOrderDetailsForExcelInput input)
        {

            var filteredOrderDetails = _orderDetailRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.OrderNotes.Contains(input.Filter) || e.OrderDetails.Contains(input.Filter) || e.Building.Contains(input.Filter) || e.Street.Contains(input.Filter) || e.FlatNo.Contains(input.Filter) || e.Landmark.Contains(input.Filter) || e.Currency.Contains(input.Filter))
                        .WhereIf(input.MinCreationTimeFilter != null, e => e.CreationTime >= input.MinCreationTimeFilter)
                        .WhereIf(input.MaxCreationTimeFilter != null, e => e.CreationTime <= input.MaxCreationTimeFilter)
                        .WhereIf(input.MinCreatorUserIdFilter != null, e => e.CreatorUserId >= input.MinCreatorUserIdFilter)
                        .WhereIf(input.MaxCreatorUserIdFilter != null, e => e.CreatorUserId <= input.MaxCreatorUserIdFilter)
                        .WhereIf(input.MinCheckoutStatusFilter != null, e => e.CheckoutStatus >= input.MinCheckoutStatusFilter)
                        .WhereIf(input.MaxCheckoutStatusFilter != null, e => e.CheckoutStatus <= input.MaxCheckoutStatusFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinDeliveryLocationIdFilter != null, e => e.DeliveryLocationId >= input.MinDeliveryLocationIdFilter)
                        .WhereIf(input.MaxDeliveryLocationIdFilter != null, e => e.DeliveryLocationId <= input.MaxDeliveryLocationIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BuildingFilter), e => e.Building == input.BuildingFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StreetFilter), e => e.Street == input.StreetFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FlatNoFilter), e => e.FlatNo == input.FlatNoFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LandmarkFilter), e => e.Landmark == input.LandmarkFilter)
                        .WhereIf(input.MinOrderPaymentMethodFilter != null, e => e.OrderPaymentMethod >= input.MinOrderPaymentMethodFilter)
                        .WhereIf(input.MaxOrderPaymentMethodFilter != null, e => e.OrderPaymentMethod <= input.MaxOrderPaymentMethodFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CurrencyFilter), e => e.Currency == input.CurrencyFilter);
            
            var currentUser = GetCurrentUser();
            var userRoles = await _userRoleRepository.GetAll()
                .Where(userRole => userRole.UserId == currentUser.Id)
                .Select(userRole => userRole).ToListAsync();
            var distinctRoleIds = userRoles.Select(userRole => userRole.RoleId).Distinct();
            var isAdmin = false;
            foreach (var roleId in distinctRoleIds)
            {
                var role = await _roleManager.FindByIdAsync(roleId.ToString());
                if (role != null && role.DisplayName.Equals(StaticRoleNames.Tenants.Admin))
                {
                    isAdmin = true;
                }
            }

            if (!isAdmin)
            {
                filteredOrderDetails = filteredOrderDetails.Where(x => x.CreatorUserId == currentUser.Id);
            }


            var query = (from o in filteredOrderDetails
                         select new GetOrderDetailForViewDto()
                         {
                             OrderDetail = new OrderDetailDto
                             {
                                 CreationTime = o.CreationTime,
                                 CheckoutStatus = o.CheckoutStatus,
                                 Amount = o.Amount,
                                 OrderNotes = o.OrderNotes,
                                 DeliveryLocationId = o.DeliveryLocationId,
                                 Building = o.Building,
                                 Street = o.Street,
                                 FlatNo = o.FlatNo,
                                 Landmark = o.Landmark,
                                 OrderPaymentMethod = o.OrderPaymentMethod,
                                 Currency = o.Currency,
                                 Id = o.Id
                             }
                         });


            var orderDetailListDtos = await query.ToListAsync();

            return _orderDetailsExcelExporter.ExportToFile(orderDetailListDtos);
        }


    }
}
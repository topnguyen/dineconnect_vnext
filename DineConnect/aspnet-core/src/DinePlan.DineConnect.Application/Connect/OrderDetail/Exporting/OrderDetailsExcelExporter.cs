﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.OrderDetail.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.OrderDetail.Exporting
{
    public class OrderDetailsExcelExporter : NpoiExcelExporterBase, IOrderDetailsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public OrderDetailsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetOrderDetailForViewDto> orderDetails)
        {
            return CreateExcelPackage(
                "OrderDetails.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("OrderDetails"));

                    AddHeader(
                        sheet,
                        L("CreationTime"),
                        L("CheckoutStatus"),
                        L("Amount"),
                        L("OrderNotes"),
                        L("Building"),
                        L("Street"),
                        L("FlatNo"),
                        L("Landmark"),
                        L("OrderPaymentMethod"),
                        L("Currency")
                        );

                    AddObjects(
                        sheet, 2, orderDetails,
                        _ => _timeZoneConverter.Convert(_.OrderDetail.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.OrderDetail.CheckoutStatus,
                        _ => _.OrderDetail.Amount,
                        _ => _.OrderDetail.OrderNotes,
                        _ => _.OrderDetail.Building,
                        _ => _.OrderDetail.Street,
                        _ => _.OrderDetail.FlatNo,
                        _ => _.OrderDetail.Landmark,
                        _ => _.OrderDetail.OrderPaymentMethod,
                        _ => _.OrderDetail.Currency
                        );

                    sheet.AutoSizeColumn(1);
                });
        }
    }
}

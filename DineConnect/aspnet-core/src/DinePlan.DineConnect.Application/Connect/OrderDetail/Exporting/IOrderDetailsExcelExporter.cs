﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.OrderDetail.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.OrderDetail.Exporting
{
    public interface IOrderDetailsExcelExporter
    {
        FileDto ExportToFile(List<GetOrderDetailForViewDto> orderDetails);
    }
}
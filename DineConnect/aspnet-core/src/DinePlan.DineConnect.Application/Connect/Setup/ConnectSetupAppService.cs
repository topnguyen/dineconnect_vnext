﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Logging;
using DinePlan.DineConnect.Connect.Job.Dto;
using DinePlan.DineConnect.Utility.Rest;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;

namespace DinePlan.DineConnect.Connect.Setup
{
    public class ConnectSetupAppService  :  DineConnectAppServiceBase, IConnectSetupAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILogger _logger;
        private readonly IRepository<Brand> _brandRepository;
        private readonly IRepository<Location> _locRepository;
        private readonly IRepository<ProductGroup> _productGroupRepository;
        private readonly IRepository<Category> _catRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;


        public ConnectSetupAppService(IUnitOfWorkManager unitOfWorkManager, 
            ILogger logger,
            IRepository<Brand> brandRepository,
            IRepository<Location> locRepository,
            IRepository<ProductGroup> productGroupRepository,
            IRepository<Category> catRepository,
            IRepository<MenuItem> menuItemRepository
        )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _logger = logger;
            _locRepository = locRepository;
            _productGroupRepository = productGroupRepository;
            _catRepository = catRepository;
            _menuItemRepository = menuItemRepository;
        }
        public async Task SyncDineConnect(ConnectSyncJobArgs args)
        {
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(args.LocalTenantId))
                {
                    if (!string.IsNullOrEmpty(args.TenantId) && !string.IsNullOrEmpty(args.TenantName) &&
                        !string.IsNullOrEmpty(args.TenantPassword) && !string.IsNullOrEmpty(args.TenantUrl) &&
                        !string.IsNullOrEmpty(args.TenantUser))
                    {

                        #region Token

                        RestUtils.Url = args.TenantUrl;
                        RestUtils.Tenant = args.TenantName;
                        RestUtils.User = args.TenantUser;
                        RestUtils.Password = args.TenantPassword;
                        RestUtils.TenantId = args.TenantId;



                        var token = RestUtils.GetToken(true);
                        if (string.IsNullOrEmpty(token))
                        {
                            throw new Exception("Token is empty");
                        }
                        #endregion

                        #region RetrieveLocations
                        var allLocations = RestUtils.GetResponse(ConnectRequests.SyncLocations,
                            new 
                            {
                                TenantId = args.TenantId
                            });

                        var myTest = "";

                        #endregion


                    }
                }
            }
            catch (Exception exception)
            {
                _logger.Log(LogSeverity.Error, "TicketCreation", exception);
            }
        }
    }
}

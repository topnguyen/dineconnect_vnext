﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.DinePlanUser.Exporting
{
    public class DinePlanUserRoleExcelExporter : NpoiExcelExporterBase, IDinePlanUserRoleExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public DinePlanUserRoleExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<DinePlanUserRoleListDto> list)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "DinePlanUserRoleList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DinePlanUserRole"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("IsAdmin"),
                        L("CreationTime"),
                        L("Permissions")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.IsAdmin,
                        _ => _.CreationTime.ToString(fileDateSetting),
                        _ => _.Permission
                    );

                    sheet.GetColumnStyle(5).WrapText = true;
                    sheet.SetColumnWidth(5, 100);

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
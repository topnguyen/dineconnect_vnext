﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.DinePlanUser.Exporting
{
  public interface IDinePlanUserRoleExcelExporter
    {
        Task<FileDto> ExportToFile(List<DinePlanUserRoleListDto> list);
    }
}

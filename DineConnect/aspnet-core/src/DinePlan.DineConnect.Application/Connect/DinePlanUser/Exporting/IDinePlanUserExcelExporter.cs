﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.DinePlanUser.Exporting
{
  public interface IDinePlanUserExcelExporter
    {
        Task<FileDto> ExportToFile(List<DinePlanUserListDto> list);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.DinePlanUser.Exporting
{
    public class DinePlanUserExcelExporter : NpoiExcelExporterBase, IDinePlanUserExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public DinePlanUserExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<DinePlanUserListDto> list)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            return CreateExcelPackage(
                "DinePlanUserList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DinePlanUser"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name"),
                        L("Role"),
                        L("PinCode"),
                        L("CreationTime")
                    );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.DinePlanUserRoleName,
                        _ => _.PinCode,
                        _ => _.CreationTime.ToString(fileDateSetting)
                    );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
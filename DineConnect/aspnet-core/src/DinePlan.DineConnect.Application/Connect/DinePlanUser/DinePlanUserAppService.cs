﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.DinePlanUser.Exporting;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.DinePlanUser
{
    public class DinePlanUserAppService : DineConnectAppServiceBase, IDinePlanUserAppService
    {
        private readonly IRepository<Users.DinePlanUser> _dinePlanUserRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDinePlanUserExcelExporter _dinePlanUserExcelExporter;
        private readonly ConnectSession _connectSession;
        private readonly ILocationAppService _locationAppService;
        private readonly IUserAppService _userAppService;

        public DinePlanUserAppService(
            IRepository<Users.DinePlanUser> dinePlanUserRepo,
            ILocationAppService locAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IUserAppService userAppService,
            IDinePlanUserExcelExporter dinePlanUserExcelExporter,
            ConnectSession connectSession,
            ILocationAppService locationAppService
        )
        {
            _dinePlanUserRepo = dinePlanUserRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _dinePlanUserExcelExporter = dinePlanUserExcelExporter;
            _connectSession = connectSession;
            _locationAppService = locationAppService;
            _userAppService = userAppService;
        }

        public async Task<PagedResultDto<DinePlanUserListDto>> GetAll(GetDinePlanUserInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _dinePlanUserRepo.GetAll()
                    .Include(x => x.DinePlanUserRole)
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name != null && p.Name.Contains(input.Filter) ||
                             p.PinCode != null && p.PinCode.Contains(input.Filter) ||
                             p.DinePlanUserRole != null && p.DinePlanUserRole.Name.Contains(input.Filter)
                    );

                if (!string.IsNullOrWhiteSpace(input.LocationIds))
                {
                    allItems = FilterByLocations(allItems, input);
                }

                var allItemCount = allItems.Count();

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = ObjectMapper.Map<List<DinePlanUserListDto>>(sortMenuItems);

                return new PagedResultDto<DinePlanUserListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dinePlanUserRepo.GetAll().ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DinePlanUserListDto>>(allList);

            return await _dinePlanUserExcelExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDinePlanUserForEditOutput> GetDinePlanUserForEdit(EntityDto input)
        {
            var output = new GetDinePlanUserForEditOutput();

            var hDto = await _dinePlanUserRepo.GetAll()
                .Include(x => x.DinePlanUserRole)
                .Where(x => x.Id == input.Id)
                .FirstOrDefaultAsync();

            var editDto = ObjectMapper.Map<DinePlanUserEditDto>(hDto);

            UpdateLocations(hDto, output.LocationGroup);

            output.DinePlanUser = editDto;

            return output;
        }

        public async Task CreateOrUpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            if (input?.LocationGroup == null)
            {
                return;
            }

            if (input.DinePlanUser.Id.HasValue)
            {
                await UpdateDinePlanUser(input);
            }
            else
            {
                await CreateDinePlanUser(input);
            }
        }

        public async Task DeleteDinePlanUser(EntityDto input)
        {
            await _dinePlanUserRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<DinePlanUserListDto>> GetIds()
        {
            var lstDinePlanUser = await _dinePlanUserRepo.GetAll().ToListAsync();

            return new ListResultDto<DinePlanUserListDto>(ObjectMapper.Map<List<DinePlanUserListDto>>(lstDinePlanUser));
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dinePlanUserRepo.GetAsync(input.Id);

                item.IsDeleted = false;

                await _dinePlanUserRepo.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            var dto = input.DinePlanUser;

            if (input.DinePlanUser.Id != null)
            {
                var item = await _dinePlanUserRepo.GetAsync(input.DinePlanUser.Id.Value);

                ObjectMapper.Map(dto, item);

                UpdateLocations(item, input.LocationGroup);

                item.DinePlanUserRole = null;
                await CreateSync(item);
            }
        }

        protected virtual async Task CreateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            var dto = ObjectMapper.Map<Users.DinePlanUser>(input.DinePlanUser);
            dto.DinePlanUserRole = null;

            UpdateLocations(dto, input.LocationGroup);

            await CreateSync(dto);
        }

        public async Task CreateSync(Users.DinePlanUser user)
        {
            user.OrganizationId = _connectSession.OrgId;

            if (user.Id == 0)
            {
                if (_dinePlanUserRepo.GetAll().Any(a => a.Name.Equals(user.Name)))
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                if (_dinePlanUserRepo.GetAll().Any(a => a.Code != null && a.Code.Equals(user.Code)))
                {
                    throw new UserFriendlyException(L("CodeAlreadyExists"));
                }
                if (!string.IsNullOrEmpty(user.PinCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.PinCode != null && a.PinCode.Equals(user.PinCode)))
                    {
                        throw new UserFriendlyException(L("PinCodeAlreadyExists"));
                    }

                    if (!string.IsNullOrEmpty(user.SecurityCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.PinCode != null && a.PinCode.Equals(user.SecurityCode)))
                        {
                            throw new UserFriendlyException(L("PinCodeAlreadyExists"));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(user.SecurityCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.SecurityCode != null && a.SecurityCode.Equals(user.SecurityCode)))
                    {
                        throw new UserFriendlyException(L("SecurityCodeAlreadyExists"));
                    }
                    if (!string.IsNullOrEmpty(user.PinCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.SecurityCode != null && a.SecurityCode.Equals(user.PinCode)))
                        {
                            throw new UserFriendlyException(L("SecurityCodeAlreadyExists"));
                        }
                    }
                }
                await _dinePlanUserRepo.InsertAsync(user);
            }
            else
            {
                List<Users.DinePlanUser> lst = _dinePlanUserRepo.GetAll().Where(a => a.Name.Equals(user.Name) && a.Id != user.Id).ToList();
                if (lst.Count > 0)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }

                lst = _dinePlanUserRepo.GetAll().Where(a => a.Code != null && a.Code.Equals(user.Code) && a.Id != user.Id).ToList();
                if (lst.Count > 0)
                {
                    throw new UserFriendlyException(L("CodeAlreadyExists"));
                }

                if (!string.IsNullOrEmpty(user.PinCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.PinCode != null && a.PinCode.Equals(user.PinCode)))
                    {
                        throw new UserFriendlyException(L("PinCodeAlreadyExists"));
                    }
                    if (!string.IsNullOrEmpty(user.SecurityCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.PinCode != null && a.PinCode.Equals(user.SecurityCode)))
                        {
                            throw new UserFriendlyException(L("PinCodeAlreadyExists"));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(user.SecurityCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.SecurityCode != null && a.SecurityCode.Equals(user.SecurityCode)))
                    {
                        throw new UserFriendlyException(L("SecurityCodeAlreadyExists"));
                    }
                    if (!string.IsNullOrEmpty(user.PinCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.SecurityCode != null && a.SecurityCode.Equals(user.PinCode)))
                        {
                            throw new UserFriendlyException(L("SecurityCodeAlreadyExists"));
                        }
                    }
                }

                await _dinePlanUserRepo.UpdateAsync(user);
            }
        }

        public async Task<PagedResultDto<ApiDinePlanUserListDto>> ApiUsersByLocation(ApiLocationInput input)
        {
            var outputUsers = new List<Users.DinePlanUser>();

            try
            {
                var allUsers = _dinePlanUserRepo.GetAll()
                    .Include(a => a.DinePlanUserRole)
                    .Where(a => a.TenantId == input.TenantId);

                if (!string.IsNullOrEmpty(input.Tag))
                {
                    var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(input.Tag);
                    if (allLocations != null && allLocations.Items.Any())
                        foreach (var dinePlanUser in allUsers)
                            if (!dinePlanUser.Group && !string.IsNullOrEmpty(dinePlanUser.Locations) &&
                                !dinePlanUser.LocationTag)
                            {
                                var scLocations =
                                    JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(dinePlanUser.Locations);
                                if (scLocations.Any())
                                    foreach (var i in scLocations.Select(a => a.Id))
                                        if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                            outputUsers.Add(dinePlanUser);
                            }
                            else
                            {
                                outputUsers.Add(dinePlanUser);
                            }
                }
                else if (input.LocationId > 0)
                {
                    foreach (var dinePlanUser in allUsers)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = input.LocationId,
                            Locations = dinePlanUser.Locations,
                            Group = dinePlanUser.Group,
                            LocationTag = dinePlanUser.LocationTag,
                            NonLocations = dinePlanUser.NonLocations
                        }))
                            outputUsers.Add(dinePlanUser);
                }
                else
                {
                    foreach (var dinePlanUser in allUsers) outputUsers.Add(dinePlanUser);
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            List<ApiDinePlanUserListDto> allPlanUsers = new List<ApiDinePlanUserListDto>();

            foreach (var dinePlanUser in outputUsers)
            {
                var apiDi = ObjectMapper.Map<ApiDinePlanUserListDto>(dinePlanUser);
                if (dinePlanUser.UserId.HasValue)
                {
                    var output = await
                        _userAppService.ApiGetUser(new ApiLocationInput()
                        {
                            TenantId = input.TenantId,
                            UserId = dinePlanUser.UserId.Value
                        });

                    if (output != null)
                    {
                        apiDi.IsActive = output.IsActive;
                        apiDi.ConnectUser = output.UserName;
                        apiDi.ConnectPassword = output.Password;
                        apiDi.Name = output.Name;
                    }
                }
                allPlanUsers.Add(apiDi);
            }
            return new PagedResultDto<ApiDinePlanUserListDto>(allPlanUsers.Count, allPlanUsers);
        }
    }
}
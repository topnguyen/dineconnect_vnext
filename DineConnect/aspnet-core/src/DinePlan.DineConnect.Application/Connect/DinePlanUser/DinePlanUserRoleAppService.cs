﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Permissions.Dto;
using DinePlan.DineConnect.Connect.DinePlanUser.Exporting;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;
using DinePlan.DineConnect.Dto;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.DinePlanUser
{
    public class DinePlanUserRoleAppService : DineConnectAppServiceBase, IDinePlanUserRoleAppService
    {
        private readonly IRepository<Department> _departmentRepo;

        private readonly IRepository<DinePlanPermission> _dinePlanPermission;

        private readonly IRepository<DinePlanUserRole> _dinePlanUseRoleRepo;

        private readonly IUnitOfWorkManager _unitOfWorkManager;

        private readonly IDinePlanUserRoleExcelExporter _dinePlanUserRoleExcelExporter;

        public DinePlanUserRoleAppService(
            IRepository<DinePlanUserRole> dinePlanUserRoleRepo,
            IRepository<DinePlanPermission> dinePlanPermission,
            IRepository<Department> dRepo
            , IUnitOfWorkManager unitOfWorkManager,
            IDinePlanUserRoleExcelExporter dinePlanUserRoleExcelExporter
            )
        {
            _dinePlanUseRoleRepo = dinePlanUserRoleRepo;
            _dinePlanPermission = dinePlanPermission;
            _departmentRepo = dRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _dinePlanUserRoleExcelExporter = dinePlanUserRoleExcelExporter;
        }

        public async Task<PagedResultDto<DinePlanUserRoleListDto>> GetAll(GetDinePlanUserRoleInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _dinePlanUseRoleRepo
                .GetAll()
                .Where(i => i.IsDeleted == input.IsDeleted)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Id.Equals(input.Filter) || (p.Name != null && p.Name.Contains(input.Filter))
                );

                if (!input.Location.IsNullOrEmpty())
                {
                    allItems = allItems.Where(r => r.Department.Locations.Contains(input.Location));
                }

                var allDtos = from a in allItems
                              select new DinePlanUserRoleListDto
                              {
                                  Name = a.Name,
                                  IsAdmin = a.IsAdmin,
                                  CreationTime = a.CreationTime,
                                  Id = a.Id
                              };

                var sortMenuItems = await allDtos
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = ObjectMapper.Map<List<DinePlanUserRoleListDto>>(sortMenuItems);

                var allItemCount = await allItems.CountAsync();

                return new PagedResultDto<DinePlanUserRoleListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dinePlanUseRoleRepo.GetAll().ToListAsync();

            var allListDtos = ObjectMapper.Map<List<DinePlanUserRoleListDto>>(allList);

            foreach (var item in allListDtos)
            {
                var listPermission = GetGrantedPermission(new NullableIdDto(item.Id));

                item.Permission = listPermission.JoinAsString(", ");
            }

            return await _dinePlanUserRoleExcelExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDinePlanUserRoleForEditOutput> GetDinePlanUserRoleForEdit(NullableIdDto input)
        {
            DinePlanUserRoleEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dinePlanUseRoleRepo.GetAsync(input.Id.Value);

                editDto = ObjectMapper.Map<DinePlanUserRoleEditDto>(hDto);
            }
            else
            {
                editDto = new DinePlanUserRoleEditDto();
            }

            return new GetDinePlanUserRoleForEditOutput
            {
                DinePlanUserRole = editDto,
                Permissions = GetPermissions(),
                GrantedPermissionNames = GetGrantedPermission(input)
            };
        }

        public async Task CreateOrUpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            if (input.DinePlanUserRole.Id.HasValue)
            {
                await UpdateDinePlanUserRole(input);
            }
            else
            {
                await CreateDinePlanUserRole(input);
            }
        }

        public async Task DeleteDinePlanUserRole(EntityDto input)
        {
            await _dinePlanUseRoleRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<DinePlanUserRoleListDto>> GetIds()
        {
            var lstDinePlanUserRole = await _dinePlanUseRoleRepo.GetAll().ToListAsync();

            return new ListResultDto<DinePlanUserRoleListDto>(ObjectMapper.Map<List<DinePlanUserRoleListDto>>(lstDinePlanUserRole));
        }

        private List<string> GetGrantedPermission(NullableIdDto input)
        {
            var allRolesS = new List<string>();

            if (input.Id.HasValue)
            {
                var allRoles =
                    _dinePlanPermission.GetAll()
                        .Where(
                            a =>
                                a.DinePlanUserRoleId != null && a.DinePlanUserRoleId == input.Id.Value &&
                                a.IsGrantedByDefault);

                if (allRoles.Any())
                {
                    allRolesS.AddRange(allRoles.ToList().Select(dinePlanPermission => dinePlanPermission.Name));
                }
            }
            return allRolesS;
        }

        private List<FlatPermissionDto> GetPermissions()
        {
            var permissions = new List<FlatPermissionDto>();

            permissions.Add(new FlatPermissionDto
            {
                Description = null,
                DisplayName = "Permissions",
                IsGrantedByDefault = false,
                Name = "Permissions",
                ParentName = null
            });

            var category = "";
            foreach (var item in PermissionList.Permissions)
            {
                if (string.IsNullOrWhiteSpace(item))
                {
                    continue;
                }

                if (item.StartsWith("#"))
                {
                    category = item.Substring(1);
                    permissions.Add(new FlatPermissionDto
                    {
                        ParentName = "Permissions",
                        Name = category,
                        DisplayName = category,
                        IsGrantedByDefault = false,
                        Description = null
                    });
                }
                else if (!string.IsNullOrEmpty(category))
                {
                    permissions.Add(new FlatPermissionDto
                    {
                        ParentName = category,
                        Name = item,
                        DisplayName = item
                    });
                }
            }

            foreach (var department in _departmentRepo.GetAll())
            {
                permissions.Add(new FlatPermissionDto
                {
                    ParentName = PermissionList.DepartmentPermission,
                    Name = "UseDepartment_" + department.Id,
                    DisplayName = department.Name
                });
            }
            return permissions;
        }

        protected virtual async Task UpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            var item = await _dinePlanUseRoleRepo.GetAsync(input.DinePlanUserRole.Id.Value);

            var dto = input.DinePlanUserRole;

            item.Name = dto.Name;

            item.IsAdmin = dto.IsAdmin;

            item.DepartmentId = dto.DepartmentId;

            await SetGrantedPermissionsAsync(dto.Id.Value, input.GrantedPermissionNames);
        }

        protected virtual async Task CreateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            var dto = ObjectMapper.Map<DinePlanUserRole>(input.DinePlanUserRole);

            dto.TenantId = AbpSession.TenantId ?? 0;

            CheckErrors(await CreateSync(dto));

            await CurrentUnitOfWork.SaveChangesAsync(); //It's done to get Id of the role.

            await SetGrantedPermissionsAsync(dto.Id, input.GrantedPermissionNames);
        }

        public virtual async Task SetGrantedPermissionsAsync(int role, IEnumerable<string> permissions)
        {
            var oldPermissions = await GetGrantedPermissionsAsync(role);

            var oldPermissionsNames = new List<string>();

            if (oldPermissions != null)
            {
                oldPermissionsNames = oldPermissions.Select(a => a.Name).ToList();
            }

            var newPermissions = permissions.ToArray();

            var addList = newPermissions.Except(oldPermissionsNames).ToList();

            var removeList = oldPermissionsNames.Except(newPermissions).ToList();

            foreach (var myAddList in addList)
            {
                var perm =
                    await
                        _dinePlanPermission.FirstOrDefaultAsync(
                            a => a.Name.Equals(myAddList) && a.DinePlanUserRoleId == role);

                if (perm != null)
                {
                    perm.IsGrantedByDefault = true;
                }
                else
                {
                    await _dinePlanPermission.InsertAsync(new DinePlanPermission
                    {
                        TenantId = AbpSession.TenantId ?? 0,
                        DinePlanUserRoleId = role,
                        IsGrantedByDefault = true,
                        Name = myAddList
                    });
                }
            }

            foreach (var myAddList in removeList)
            {
                var perm =
                    await
                        _dinePlanPermission.FirstOrDefaultAsync(
                            a => a.Name.Equals(myAddList) && a.DinePlanUserRoleId == role);
                perm.IsGrantedByDefault = false;

                await _dinePlanPermission.UpdateAsync(perm);
            }
        }

        public virtual async Task<IReadOnlyList<DinePlanPermission>> GetGrantedPermissionsAsync(int roleId)
        {
            if (_dinePlanPermission.GetAll().Any())
            {
                return await _dinePlanPermission.GetAllListAsync(a => a.DinePlanUserRoleId != null
                                                                      && a.DinePlanUserRoleId == roleId &&
                                                                      a.IsGrantedByDefault);
            }
            return null;
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dinePlanUseRoleRepo.GetAsync(input.Id);

                item.IsDeleted = false;

                await _dinePlanUseRoleRepo.UpdateAsync(item);
            }
        }

        public async Task<IdentityResult> CreateSync(DinePlanUserRole dinePlanUserRole)
        {
            //  if the New Addition
            if (dinePlanUserRole.Id == 0)
            {
                if (_dinePlanUseRoleRepo.GetAll().Any(a => a.Id.Equals(dinePlanUserRole.Id)))
                {
                    var errors = new List<IdentityError>
                    {
                        new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                    };

                    var success = IdentityResult.Failed(errors.ToArray());

                    return success;
                }

                await _dinePlanUseRoleRepo.InsertAndGetIdAsync(dinePlanUserRole);

                return IdentityResult.Success;
            }

            List<DinePlanUserRole> lst = _dinePlanUseRoleRepo.GetAll().Where(a => a.Id.Equals(dinePlanUserRole.Id) && a.Id != dinePlanUserRole.Id).ToList();

            if (lst.Count > 0)
            {
                var errors = new List<IdentityError>
                {
                    new IdentityError {Code = "NameAlreadyExists", Description = L("NameAlreadyExists")}
                };

                var success = IdentityResult.Failed(errors.ToArray());

                return success;
            }

            return IdentityResult.Success;
        }

        public async Task<GetDinePlanUserRoleOutputList> ApiGetUserRoles(GetUserRolesNameInput input)
        {
            if (input.TenantId <= 0) return null;
            var lstDinePlanUserRole =
                await
                    _dinePlanUseRoleRepo.GetAllListAsync(a => a.TenantId == input.TenantId && a.Name.Equals(input.Name));

            var lst = lstDinePlanUserRole.Select(dto => new GetDinePlanUserRoleForEditOutput
            {
                DinePlanUserRole = dto.MapTo<DinePlanUserRoleEditDto>(),
                GrantedPermissionNames = GetGrantedPermission(new NullableIdDto(dto.Id))
            }).ToList();

            return new GetDinePlanUserRoleOutputList
            {
                Items = lst
            };
        }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Castle.Core.Logging;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Period.Exporter;
using DinePlan.DineConnect.Connect.Reports;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using Abp.Runtime.Session;

namespace DinePlan.DineConnect.Connect.Period
{
	public class WorkPeriodAppService : DineConnectReportAppServiceBase, IWorkPeriodAppService
	{
		private readonly IRepository<Period.WorkPeriod> _workperiodRepository;
		private readonly ILogger _logger;
		private readonly IRepository<Location> _lRepo;
		private readonly IPaymentTypesAppService _ptService;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly ILocationAppService _locService;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _paymentRepository;
        private readonly IWorkPeriodExporter _workPeriodExporter;

        private readonly IRepository<Ticket> _ticketRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<MenuItemPortion> _poRepository;
        private readonly ISettingManager _settingManager;
        public WorkPeriodAppService(
			ILocationAppService locService,
			IRepository<Period.WorkPeriod> workPeriodRepository,
			IRepository<Location> lrepo,
			ILogger logger,
			IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.PaymentTypes.PaymentType> paymentRepository,
            IWorkPeriodExporter workPeriodExporter,
            IRepository<Ticket> ticketRepo,
            IRepository<Department> departmentRepo,
            IRepository<MenuItemPortion> poRepository,
            ISettingManager settingManager
            ) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
		{
			_lRepo = lrepo;
			_logger = logger;
			_workperiodRepository = workPeriodRepository;
			_lRepo = lrepo;
            _locService = locService;
            _paymentRepository = paymentRepository;
            _workPeriodExporter = workPeriodExporter;
            _poRepository = poRepository;
            _ticketRepo = ticketRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _settingManager = settingManager;
        }

		public async Task<CreateOrUpdatePeriodOutput> CreateOrUpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
		{
			var updatePeriod = false;
			CreateOrUpdatePeriodOutput output = new CreateOrUpdatePeriodOutput();

			if (input.WorkPeriod.Id.HasValue && input.WorkPeriod.Id > 0)
			{
				updatePeriod = true;
			}
			if (input.WorkPeriod.Wid > 0)
			{
				var count =
						await
							_workperiodRepository.LongCountAsync(
								a =>
									a.Wid == input.WorkPeriod.Wid && a.LocationId == input.WorkPeriod.LocationId);
				updatePeriod = count > 0;
				if (updatePeriod)
				{
					var period =
							_workperiodRepository.FirstOrDefault(
								a =>
								   a.Wid == input.WorkPeriod.Wid && a.LocationId == input.WorkPeriod.LocationId);

					if (period != null)
					{
						input.WorkPeriod.Id = period.Id;
					}
					else
					{
						updatePeriod = false;
					}
				}
			}
			if (updatePeriod)
			{
				await UpdateWorkPeriod(input);
				output.Wid = input.WorkPeriod.Id.Value;
			}
			else
			{
				var pId = await CreateWorkPeriod(input);
				output.Wid = pId;
			}
			if (input.WorkPeriod.TotalSales > 0M)
			{
				//await _bgm.EnqueueAsync<ConnectJob, ConnectJobArgs>(new ConnectJobArgs
				//{
				//	WorkPeriodId = output.Wid
				//});
			}
			return output;
		}

		protected virtual async Task UpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
		{
			var item = await _workperiodRepository.GetAsync(input.WorkPeriod.Id.Value);
			var dto = input.WorkPeriod;

			item.StartUser = dto.StartUser;
			item.EndUser = dto.EndUser;
			item.StartTime = dto.StartTime;
			item.EndTime = dto.EndTime;
			item.LocationId = dto.LocationId;
			item.Wid = dto.Wid;
			item.TotalSales = dto.TotalSales;
			item.TotalTicketCount = dto.TotalTicketCount;
			item.TenantId = dto.TenantId;
			item.WorkPeriodInformations = dto.WorkPeriodInformations;
			item.AutoClosed = dto.AutoClosed;

			await _workperiodRepository.UpdateAsync(item);
		}

		protected virtual async Task<int> CreateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
		{
			try
			{
				var dto = ObjectMapper.Map<Period.WorkPeriod>(input.WorkPeriod);
				var iResult = await _workperiodRepository.InsertAndGetIdAsync(dto);
				return iResult;
			}
			catch (Exception exception)
			{
				_logger.Fatal("---Error in Create WorkPeriod---");
				_logger.Fatal("Exception ", exception);
				_logger.Fatal("Location " + input.WorkPeriod.LocationId);
				_logger.Fatal("---End Error in Create WorkPeriod---");
			}
			return 0;
		}

        public async Task<WorkPeriodOutput> BuildSummaryReport(WorkPeriodSummaryInput input)
        {
            WorkPeriodOutput reOutput = new WorkPeriodOutput();
            List<WorkPeriodSummaryOutput> output = new List<WorkPeriodSummaryOutput>();
            var allW = _workperiodRepository.GetAll();

            var locations = new List<int>();

            if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                && !input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null && input.UserId > 0)
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
            }


            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));

            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where( a =>a.StartTime >= input.StartDate && a.EndTime < nextDay);
            decimal total = 0M;
            decimal totalTickets = 0M;
            decimal averVal = 0M;

            foreach (var workPeriod in allW)
            {
                WorkPeriodSummaryOutput sout;
                if (output.Any(a => a.LocationId == workPeriod.LocationId))
                {
                    sout = output.FirstOrDefault(a => a.LocationId == workPeriod.LocationId);
                    if (sout != null)
                    {
                        sout.Total += workPeriod.TotalSales;
                        sout.TotalTicketCount += workPeriod.TotalTicketCount;
                        if (sout.ReportEndDay < workPeriod.EndTime)
                            sout.ReportEndDay = workPeriod.EndTime;

                        if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                        {
                            var allShifts =
                                JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                            if (!string.IsNullOrEmpty(sout.DayInformation))
                            {
                                var soutShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(sout.DayInformation);
                                soutShifts.AddRange(allShifts);
                                sout.DayInformation = JsonConvert.SerializeObject(soutShifts);
                            }

                            if (allShifts.Any())
                            {
                                sout.Actual += allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                                sout.Entered += allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                            }
                        }
                    }
                }
                else
                {
                    sout = new WorkPeriodSummaryOutput();
                    Location loc = _lRepo.Get(workPeriod.LocationId);
                    if (loc != null)
                    {
                        sout.ReportStartDay = workPeriod.StartTime;
                        sout.LocationName = loc.Name;
                        sout.Wid = workPeriod.Id;
                        sout.ReportEndDay = workPeriod.EndTime;
                        sout.LocationCode = loc.Code;
                        sout.LocationCode = loc.Code;
                        sout.LocationId = loc.Id;
                        sout.DayInformation = workPeriod.WorkPeriodInformations;
                        sout.Total = workPeriod.TotalSales;
                        sout.TotalTicketCount = workPeriod.TotalTicketCount;

                        var allShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                        if (allShifts.Any())
                        {
                            sout.Actual = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                            sout.Entered = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                        }

                        output.Add(sout);
                    }
                }
                var allPay = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                var groupPayments = allPay.SelectMany(a => a.PaymentInfos).GroupBy(a => a.PaymentType);

                foreach (var pType in groupPayments)
                {
                    var totalValue = pType.Sum(a => a.Entered);
                    if (totalValue > 0M)
                    {
                        var pt =await _paymentRepository.FirstOrDefaultAsync(pType.Key);
                        if (pt != null)
                        {
                            var keyThere = reOutput.OtherPayments.ContainsKey(pt.Name);
                            if (keyThere)
                            {
                                var getObje = reOutput.OtherPayments[pt.Name];
                                getObje += totalValue;
                                reOutput.OtherPayments[pt.Name] = getObje;
                            }
                            else
                            {
                                reOutput.OtherPayments.Add(pt.Name, totalValue);
                            }
                        }
                    }
                }
            }

            totalTickets = output.Sum(a => a.TotalTicketCount);
            total = output.Sum(a => a.Total);

            if (total > 0M && totalTickets > 0M)
                averVal = total / totalTickets;

            if (output.Any())
                output = output.OrderBy(a => a.LocationCode).ToList();

            reOutput.Periods = output;
            reOutput.DashBoard.TotalSales = total;
            reOutput.DashBoard.TotalTickets = totalTickets;
            reOutput.DashBoard.Average = averVal;

            if (reOutput.Periods.Any())
            {
                var myLo = reOutput.Periods.OrderByDescending(a => a.ReportEndDay).First();
                reOutput.LastLocationClosed = myLo.LocationCode;
                reOutput.TimeOfClosed = myLo.ReportEndDay;
            }
            return reOutput;
        }

        public async Task<FileDto> BuildSummaryExcel(WorkPeriodSummaryInput input)
        {
            return await _workPeriodExporter.ExportWorkPeriodSummary(input, this);
        }

        public async Task<FileDto> BuildDetailExcel(WorkPeriodSummaryInput input)
        {
           return await _workPeriodExporter.ExportWorkPeriodDetails(input, this);
        }

        public async Task<WorkPeriodOutput> GetSummaryReportOnTickets(WorkPeriodSummaryInput input)
        {
            WorkPeriodOutput output = new WorkPeriodOutput();
            GetTicketInput tInput = new GetTicketInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate
            };
            var allLocs = input.Locations.Select(loc => new SimpleLocationDto
            {
                Id = loc.Id,
                Name = loc.Name
            }).ToList();

            tInput.Locations = allLocs;
            var allTickets = GetAllTickets(tInput);
            var dataGroupLocation = allTickets.ToList().GroupBy(a => a.LocationId);

            foreach (var allT in dataGroupLocation)
            {
                WorkPeriodSummaryOutput sout = new WorkPeriodSummaryOutput();
                Location loc = _lRepo.Get(allT.Key);
                if (loc != null)
                {
                    sout.LocationName = loc.Name;
                    sout.LocationCode = loc.Code;
                    sout.LocationId = loc.Id;
                    sout.Total = allT.Sum(a => a.TotalAmount);
                    sout.TotalTicketCount = allT.Count();
                    output.Periods.Add(sout);
                }
            }
            return await Task.FromResult(output);
        }
        public async Task<List<WorkPeriodItemSales>> GetItemSalesForWorkPeriod(GetItemInput input)
        {
            var output = new List<WorkPeriodItemSales>();

            if (input.WorkPeriodId > 0)
            {
                var wp = _workperiodRepository.Get(input.WorkPeriodId);

                var allTickets =
                    _ticketRepo.GetAll()
                        .Where(
                            a =>
                                a.WorkPeriodId == wp.Wid && a.LocationId == wp.LocationId &&
                                a.CreationTime >= wp.StartTime);
                var allorders = allTickets.SelectMany(a => a.Orders);
                var orders =
                    allorders.Where(a => a.CalculatePrice && a.DecreaseInventory).ToList().GroupBy(a => a.MenuItemPortionId);

                foreach (var order in orders)
                {
                    var itemS = new WorkPeriodItemSales
                    {
                        MenuItemPortionId = order.Key,
                        Quantity = order.Sum(a => a.Quantity),
                        Price = order.Sum(b => b.Quantity) == 0 ? 0 : order.Sum(a => a.Quantity * a.Price) / order.Sum(b => b.Quantity)
                    };
                    output.Add(itemS);
                }

                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    foreach (var item in output)
                    {
                        var mP = await _poRepository.GetAll().Include(x => x.MenuItem).ThenInclude(x => x.Category).Where(x => x.Id == item.MenuItemPortionId).FirstOrDefaultAsync();
                        item.PortionName = mP?.Name;
                        item.MenuItemName = mP?.MenuItem.Name;
                        item.Category = mP?.MenuItem?.Category.Name;
                    }
                }
                  
            }

            return output;
        }
        public async Task<List<WorkPeriodEditDto>> GetWorkPeriodByLocationForDates(WorkPeriodSummaryInput input)
        {
            var allW = _workperiodRepository.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
                locations = input.Locations.Select(a => a.Id).ToList();
            if (locations.Any())
            {
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any() &&
                     !input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    allW = allW.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }
            else if (input.LocationGroup == null)
            {
                locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    allW = allW.Where(a => locations.Contains(a.LocationId));
            }

            var nextDay = input.EndDate.AddDays(1);
            var result = await allW.Where(a => a.StartTime >= input.StartDate
                && a.EndTime < nextDay
            ).ToListAsync();

            return result.MapTo<List<WorkPeriodEditDto>>();
        }

        public async Task<FileDto> BuildItemSalesForWorkPeriodExcel(GetItemInput input)
        {
            var myLocation = await _locService.GetAsync(input.Location);
            var fileOutput = await _workPeriodExporter.ExportItemSalesForWorkPeriodExcel(input, this, myLocation.Code);
            return fileOutput;
        }

        public async Task<List<WorkPeriodSummaryOutput>> BuildListWorkPeriodsSummary(WorkPeriodSummaryInput input)
        {
            List<WorkPeriodSummaryOutput> output = new List<WorkPeriodSummaryOutput>();
            var allW = _workperiodRepository.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                locations = input.Locations.Select(a => a.Id).ToList();
            }
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));
            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where( a => a.StartTime >= input.StartDate && a.EndTime < nextDay);

            foreach (var workPeriod in allW)
            {
                WorkPeriodSummaryOutput sout = new WorkPeriodSummaryOutput();
                var loc = _lRepo.Get(workPeriod.LocationId);
                if (loc != null)
                {
                    sout.ReportStartDay = workPeriod.StartTime;
                    sout.ReportEndDay = workPeriod.EndTime;
                    sout.LocationName = loc.Name;
                    sout.Wid = workPeriod.Id;
                    sout.LocationCode = loc.Code;
                    sout.LocationCode = loc.Code;
                    sout.LocationId = loc.Id;
                    sout.DayInformation = workPeriod.WorkPeriodInformations;
                    sout.Total = workPeriod.TotalSales;
                    sout.TotalTicketCount = workPeriod.TotalTicketCount;

                    var allShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                    if (allShifts.Any())
                    {
                        sout.Actual = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                        sout.Entered = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                    }
                    output.Add(sout);
                }
            }

            if (output.Any())
                output = output.OrderBy(a => a.LocationId).ToList();

            return await Task.FromResult(output);
        }

        #region WeekDayReport
        public async Task<List<WorkDayDetailSummaryOutput>> BuildWeekDaySales(WorkPeriodSummaryInput input)
        {
            var output = new List<WorkDayDetailSummaryOutput>();
            var tenantId = input.TenantId;
            var weekDay =await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.WeekDay, AbpSession.GetTenantId());
            if (string.IsNullOrEmpty(weekDay))
                return null;
            List<ComboboxItemDto> alLDays = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(weekDay);

            if (alLDays.Any())
            {
                GetTicketInput tInput = new GetTicketInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    Locations = input.Locations.Select(loc => new SimpleLocationDto { Id = loc.Id, Name = loc.Name }).ToList()
                };

                var allTickets =await GetAllTickets(tInput).ToListAsync();

                DateTime firstSunday = new DateTime(1753, 1, 7);
                foreach (var day in alLDays)
                {
                    var myValue = Convert.ToInt32(day.Value);
                    var myTickets = allTickets.Where(a => (int)a.LastPaymentTime.DayOfWeek == myValue);
                    myValue = myValue == 7 ? 0 : myValue;
                    var myO = new WorkDayDetailSummaryOutput
                    {
                        Date = day.DisplayText,
                        Discount = 0,
                    };

                    if (myTickets.Any())
                    {
                        myO.Total = myTickets.Sum(a => a.TotalAmount);
                        myO.Tax = myTickets.Sum(a => a.TotalAmount) * 7 / 107;
                        myO.Discount = (myTickets.Where(t => t.TicketPromotionDetails != null))
                         .SelectMany(a => JsonConvert.DeserializeObject<List<DinePlan.DineConnect.Connect.Transaction.PromotionDetailValue>>(a.TicketPromotionDetails))
                         .Sum(e => e.PromotionAmount);
                        myO.TotalTickets = myTickets.Count();
                        myO.Sales = myO.Total - myO.Tax;
                    }
                    output.Add(myO);
                }
            }
            return output;
        }
        public async Task<FileDto> BuildWeekDaySalesExport(WorkPeriodSummaryInput input)
        {
           return await _workPeriodExporter.ExportWeekDaySales(input, this);
        }
        #endregion Week day report

        #region WeekEndReport
        public async Task<List<WorkDayDetailSummaryOutput>> BuildWeekEndSales(WorkPeriodSummaryInput input)
        {
             var output = new List<WorkDayDetailSummaryOutput>();
            var tenantId = input.TenantId;
            var weekEnd = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.WeekEnd, AbpSession.GetTenantId());
            if (string.IsNullOrEmpty(weekEnd))
                return null;
            List<ComboboxItemDto> alLDays = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(weekEnd);

            if (alLDays.Any())
            {
                GetTicketInput tInput = new GetTicketInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    Locations = input.Locations.Select(loc => new SimpleLocationDto { Id = loc.Id, Name = loc.Name }).ToList()
                };

                var allTickets = await GetAllTickets(tInput).ToListAsync();

                DateTime firstSunday = new DateTime(1753, 1, 7);
                foreach (var day in alLDays)
                {
                    var myValue = Convert.ToInt32(day.Value);
                    var myTickets = allTickets.Where(a => (int)a.LastPaymentTime.DayOfWeek == myValue);
                    myValue = myValue == 7 ? 0 : myValue;
                    var myO = new WorkDayDetailSummaryOutput
                    {
                        Date = day.DisplayText,
                        Discount = 0,
                    };

                    if (myTickets.Any())
                    {
                        myO.Total = myTickets.Sum(a => a.TotalAmount);
                        myO.Tax = myTickets.Sum(a => a.TotalAmount) * 7 / 107;
                        myO.Discount = (myTickets.Where(t => t.TicketPromotionDetails != null))
                         .SelectMany(a => JsonConvert.DeserializeObject<List<DinePlan.DineConnect.Connect.Transaction.PromotionDetailValue>>(a.TicketPromotionDetails))
                         .Sum(e => e.PromotionAmount);
                        myO.TotalTickets = myTickets.Count();
                        myO.Sales = myO.Total - myO.Tax;
                    }
                    output.Add(myO);
                }
            }
            return output;
        }
        public async Task<FileDto> BuildWeekEndSalesExport(WorkPeriodSummaryInput input)
        {
            return await _workPeriodExporter.ExportWeekEndSales(input, this);
        }
        #endregion Week end report

           public async Task<StartWorkPeriodOutput> StartWorkPeriod(StartWorkPeriodInput input)
        {
            StartWorkPeriodOutput output = new StartWorkPeriodOutput();

            Period.WorkPeriod period = new Period.WorkPeriod
            {
                StartTime = input.StartTime,
                EndTime = input.StartTime,
                StartUser = input.UserName,
                EndUser = input.UserName,
                TotalSales = 0M,
                TotalTicketCount = 0,
                LocationId = input.LocationId,
                TenantId = input.TenantId,
                Wid = input.Wid
            };
            List<WorkShiftDto> shiftInfo = new List<WorkShiftDto>
            {
                new WorkShiftDto()
                {
                    StartDate = input.StartTime,
                    EndDate = input.StartTime,
                    Float = input.Float,
                    StartUser = input.UserName,
                    StopUser = input.UserName
                }
            };

            period.WorkPeriodInformations = JsonConvert.SerializeObject(shiftInfo);

            var wp = await _workperiodRepository.InsertAndGetIdAsync(period);
            output.WorkPeriodId = wp;
            return output;
        }

        public async Task<EndWorkPeriodOutput> EndWorkPeriod(EndWorkPeriodInput input)
        {
            EndWorkPeriodOutput output = new EndWorkPeriodOutput();
            try
            {
                if (input.WorkPeriodId > 0)
                {
                    var period = await _workperiodRepository.GetAsync(input.WorkPeriodId);
                    if (period != null && period.StartTime == period.EndTime)
                    {
                        period.EndTime = input.EndTime;

                        
                        if (!string.IsNullOrEmpty(period.WorkPeriodInformations))
                        {
                            List<WorkShiftDto> shiftDto =
                                JsonConvert.DeserializeObject<List<WorkShiftDto>>(period.WorkPeriodInformations);
                            if (shiftDto != null && shiftDto.Any())
                            {
                                WorkShiftDto wS = shiftDto.First();
                                wS.EndDate = input.EndTime;
                                wS.PaymentInfos = new List<WorkTimePaymentInformationDto>();
                                var totalAmount = 0M;
                                foreach (var pInfo in input.Payments)
                                {
                                    wS.PaymentInfos.Add(new WorkTimePaymentInformationDto()
                                    {
                                        Actual = pInfo.Actual,
                                        Entered = pInfo.Entered,
                                        PaymentType = pInfo.PaymentTypeId
                                    });

                                    totalAmount += pInfo.Actual;
                                }

                                period.TotalSales = totalAmount;
                            }

                            period.WorkPeriodInformations = JsonConvert.SerializeObject(shiftDto);

                        }
                        await _workperiodRepository.UpdateAsync(period);
                        output.WorkPeriodId = period.Id;
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.Fatal("---Error in End WorkPeriod--");
                _logger.Fatal("Exception ", exception);
                _logger.Fatal("Input " + input.WorkPeriodId);
                _logger.Fatal("---Error in End WorkPeriod--");

            }
            return output;
        }

    }
}
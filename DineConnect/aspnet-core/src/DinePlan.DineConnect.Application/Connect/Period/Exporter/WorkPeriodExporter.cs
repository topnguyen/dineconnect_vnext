﻿using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Period.Exporter
{
    public class WorkPeriodExporter : EpPlusExcelExporterBase, IWorkPeriodExporter
    {
        private readonly IPaymentTypesAppService _payService;
        private readonly string _dateFormat = "dd-MM-yyyy";
        public WorkPeriodExporter(IPaymentTypesAppService payService, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _payService = payService;
        }

        #region Work-Day-Summary
        public async Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Summary"));
            builder.Append("-");
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));

            var file = new FileDto(builder + ".xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await ExportWorkPeriodSummaryToExcel(excelPackage, input, appService);
                    SaveToPath(excelPackage, file);
                }
                return ProcessFile(input.ExportOutputType, file);
            }
            else
            {
                var contentHTML = await ExportWorkPeriodSummaryToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }
        private async Task<string> ExportWorkPeriodSummaryToHtml(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var dtos = await appService.GetSummaryReportOnTickets(input);
            var output = dtos.Periods;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);

            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            var formatDateSetting = "dd/MM/yyyy";
            var dataRange = L("From") + " " + input.StartDate.ToString(formatDateSetting) + " " + L("to") + " " + input.EndDate.ToString(formatDateSetting);

            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");

            strHTMLBuilder.Append("<tr class='font_14'>"); 
            strHTMLBuilder.Append($"<td colspan='5' class='text-center font_weight_bold'> {L("DailySales").ToUpper()}</td> ");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='5'class='text-center font_weight_bold' > {dataRange}</td> ");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class=' border_bottom_none text-center font_weight_bold'>{L("LocationCode")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("LocationName")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Sales")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("TotalReceipts")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Average")}</th>");
            strHTMLBuilder.Append("</tr>");
           
            // add content
            foreach (var item in output)
            {
                strHTMLBuilder.Append("<tr class='font_14'>");
                strHTMLBuilder.Append($"<td> { item.LocationCode}</td> ");
                strHTMLBuilder.Append($"<td> { item.LocationName}</td> ");
                strHTMLBuilder.Append($"<td> { item.Total.ToString(GetDecimalFormat(2))}</td>");
                strHTMLBuilder.Append($"<td> { item.TotalTicketCount.ToString(GetDecimalFormat(2))}</td> ");
                strHTMLBuilder.Append($"<td> { item.Average.ToString(GetDecimalFormat(2))}</td> ");
                strHTMLBuilder.Append("</tr>");
            }
            // Footer 
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td></td> ");
            strHTMLBuilder.Append($"<td>{L("Total")}</td> ");
            strHTMLBuilder.Append($"<td> { output.Sum(x=>x.Total).ToString(GetDecimalFormat(2))}</td>");
            strHTMLBuilder.Append($"<td> { output.Sum(x=>x.TotalTicketCount).ToString(GetDecimalFormat(2))}</td>");
            strHTMLBuilder.Append($"<td> { output.Sum(x=>x.Average).ToString(GetDecimalFormat(2))}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        public async Task ExportWorkPeriodSummaryToExcel(ExcelPackage package, WorkPeriodSummaryInput input, IWorkPeriodAppService wpService)
        {
            var output = await wpService.BuildSummaryReport(input);
            if (output?.Periods != null && output.Periods.Any() && output.DashBoard != null)
            {
                var sheet = package.Workbook.Worksheets.Add(L("Sales"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                    {
                        L("Code").ToUpper(),
                        L("Name").ToUpper(),
                        L("TotalTickets").ToUpper(),
                        L("TotalSales").ToUpper()
                    };
                var payments = await _payService.GetAllItems();
                foreach (var pt in payments.Items)
                {
                    if (pt.PaymentType.Name.ToUpper().Equals("CASH"))
                    {
                        headers.Add((pt.PaymentType.Name + " " + L("Actual")).ToUpper());
                        headers.Add((pt.PaymentType.Name + " " + L("Entered")).ToUpper());
                        headers.Add((pt.PaymentType.Name + " " + L("Difference")).ToUpper());
                    }
                    else
                    {
                        headers.Add((pt.PaymentType.Name + " " + L("Sales")).ToUpper());
                    }
                }

                AddHeader(
                  sheet,
                  headers.ToArray()
                );
                var rowCount = 2;
                var i = 0;


                foreach (var wpe in output.Periods)
                {
                    int colCount = 1;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationCode;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationName;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.TotalTicketCount;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.Total;
                    var te = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wpe.DayInformation);

                    if (te.Any())
                    {
                        var infos = te.SelectMany(a => a.PaymentInfos);
                        foreach (var pt in payments.Items)
                        {
                            var actual = infos.Where(a => a.PaymentType == pt.PaymentType.Id).Sum(a => a.Actual);
                            var entered = infos.Where(a => a.PaymentType == pt.PaymentType.Id).Sum(a => a.Entered);
                            var difference = entered - actual;

                            if (pt.PaymentType.Name.ToUpper().Equals("CASH"))
                            {
                                sheet.Cells[i + rowCount, colCount++].Value =
                                    infos.Where(a => a.PaymentType == pt.PaymentType.Id).Sum(a => a.Actual);
                                sheet.Cells[i + rowCount, colCount++].Value =
                                    infos.Where(a => a.PaymentType == pt.PaymentType.Id).Sum(a => a.Entered);

                                var colCo = colCount++;
                                if (difference < 0M)
                                {
                                    sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                    sheet.Cells[i + rowCount, colCo].Value = difference;
                                }
                                else
                                {
                                    sheet.Cells[i + rowCount, colCo].Value = difference;
                                }
                            }
                            else
                            {
                                sheet.Cells[i + rowCount, colCount++].Value = actual;
                            }
                        }
                    }
                    rowCount++;
                }
                for (var ce = 1; ce <= 100; ce++)
                {
                    sheet.Column(ce).AutoFit();
                }
            }
        }
        #endregion Work-Day-Summary

        #region Work-Day-Detail
        public async Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Detail"));
            builder.Append("_");
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));

            var file = new FileDto(builder + ".xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await ExportWorkPeriodDetailToExcel(excelPackage, input, appService);
                    SaveToPath(excelPackage, file);
                }
                return ProcessFile(input.ExportOutputType, file);
            }
            else
            {
                var contentHTML = await ExportWorkPeriodDetailToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }
        public async Task ExportWorkPeriodDetailToExcel(ExcelPackage package, WorkPeriodSummaryInput input, IWorkPeriodAppService wpService)
        {
            var output = await wpService.BuildSummaryReport(input);
            if (output?.Periods != null && output.Periods.Any() && output.DashBoard != null)
            {
                var sheet = package.Workbook.Worksheets.Add(L("Sales"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                    {
                        L("Code").ToUpper(),
                        L("Name").ToUpper(),
                        L("Date").ToUpper(),
                        L("Shift").ToUpper(),
                    };
                var payments = await _payService.GetAllItems();
                var nonHiPT = payments.Items.Select(x => x.PaymentType);
                foreach (var pt in nonHiPT)
                {
                    if (pt.Name.ToUpper().Equals("CASH"))
                    {
                        headers.Add((pt.Name + " " + L("Actual")).ToUpper());
                        headers.Add((pt.Name + " " + L("Entered")).ToUpper());
                        headers.Add((pt.Name + " " + L("Difference")).ToUpper());
                    }
                    else
                    {
                        headers.Add((pt.Name + " " + L("Sales")).ToUpper());
                    }
                }

                AddHeader(
                  sheet,
                  headers.ToArray()
                );
                var rowCount = 2;
                var i = 0;
                Dictionary<int, decimal> alltotal = new Dictionary<int, decimal>();
                Dictionary<int, decimal> allentered = new Dictionary<int, decimal>();
                Dictionary<int, decimal> alldifference = new Dictionary<int, decimal>();

                foreach (var wpe in output.Periods)
                {
                    int colCount = 1;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationCode;
                    sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationName;
                    var allDayInfos = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wpe.DayInformation);
                    if (allDayInfos.Any())
                    {
                        var myInnerCount = 0;
                        foreach (var aldI in allDayInfos)
                        {
                            myInnerCount = colCount;
                            if (aldI != null)
                            {
                                sheet.Cells[i + rowCount, myInnerCount++].Value = aldI.StartDate.ToString("yyyy-MM-dd");
                                sheet.Cells[i + rowCount, myInnerCount++].Value = aldI.StartDate.ToString("HH:mm");
                                var infos = aldI.PaymentInfos;

                                foreach (var pt in nonHiPT)
                                {
                                    var actual = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                                    var entered = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);
                                    var difference = entered - actual;

                                    if (alltotal.ContainsKey(pt.Id))
                                    {
                                        alltotal[pt.Id] += actual;
                                    }
                                    else
                                    {
                                        alltotal[pt.Id] = actual;
                                    }

                                    if (allentered.ContainsKey(pt.Id))
                                    {
                                        allentered[pt.Id] += entered;
                                    }
                                    else
                                    {
                                        allentered[pt.Id] = entered;
                                    }

                                    if (alldifference.ContainsKey(pt.Id))
                                    {
                                        alldifference[pt.Id] += difference;
                                    }
                                    else
                                    {
                                        alldifference[pt.Id] = difference;
                                    }

                                    if (pt.Name.ToUpper().Equals("CASH"))
                                    {
                                        sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                        sheet.Cells[i + rowCount, myInnerCount++].Value = entered;

                                        var colCo = myInnerCount++;
                                        if (difference < 0M)
                                        {
                                            sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                            sheet.Cells[i + rowCount, colCo].Value = difference;
                                        }
                                        else
                                        {
                                            sheet.Cells[i + rowCount, colCo].Value = difference;
                                        }
                                    }
                                    else
                                    {
                                        sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                    }
                                }
                            }
                            rowCount++;
                        }
                        myInnerCount = colCount;
                        myInnerCount++;
                        var totC = myInnerCount++;

                        sheet.Cells[i + rowCount, totC].Value = L("Total").ToUpper();

                        var summInfo = allDayInfos.SelectMany(a => a.PaymentInfos);
                        foreach (var pt in nonHiPT)
                        {
                            var actual = summInfo.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                            var entered = summInfo.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);
                            var difference = entered - actual;

                            if (pt.Name.ToUpper().Equals("CASH"))
                            {
                                sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                sheet.Cells[i + rowCount, myInnerCount++].Value = entered;

                                var colCo = myInnerCount++;
                                if (difference < 0M)
                                {
                                    sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                    sheet.Cells[i + rowCount, colCo].Value = difference;
                                }
                                else
                                {
                                    sheet.Cells[i + rowCount, colCo].Value = difference;
                                }
                            }
                            else
                            {
                                sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                            }
                        }

                    }
                    rowCount++;
                }

                rowCount++;
                int mycolCount = 4;
                sheet.Cells[i + rowCount, mycolCount++].Value = L("GrandTotal").ToUpper();

                foreach (var pt in nonHiPT)
                {
                    sheet.Cells[i + rowCount, mycolCount++].Value = alltotal[pt.Id];
                    sheet.Cells[i + rowCount, mycolCount++].Value = allentered[pt.Id];

                    var col = mycolCount++;
                    if (alldifference[pt.Id] < 0M)
                    {
                        sheet.Cells[i + rowCount, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[i + rowCount, col].Style.Fill.BackgroundColor.SetColor(Color.Red);
                        sheet.Cells[i + rowCount, col].Value = alldifference[pt.Id];
                    }
                    else
                    {
                        sheet.Cells[i + rowCount, col].Value = alldifference[pt.Id];
                    }
                }
                for (var ce = 1; ce <= 100; ce++)
                {
                    sheet.Column(ce).AutoFit();
                }
            }
        }
        private async Task<string> ExportWorkPeriodDetailToHtml(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var salesSummary = await appService.BuildListWorkPeriodsSummary(input);
            var allPayItems = await _payService.GetAllItems();
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);

            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");
            var dataRange = L("From") + " " + input.StartDate.ToString(_dateFormat) + " " + L("to") + " " + input.EndDate.ToString(_dateFormat);
            strHTMLBuilder.Append($"<h3 style='text-align:center;'>{L("DailySummary").ToUpper()}</h3>");
            strHTMLBuilder.Append($"<h3 style='text-align:center;'>{dataRange}</h3>");
            foreach (var wp in salesSummary)
            {
                GenerateLocationTable(wp, strHTMLBuilder);
                if (!string.IsNullOrEmpty(wp.DayInformation))
                {
                    var wshiDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wp.DayInformation);
                    if (wshiDto.Any())
                    {
                        int Vshift = 1;
                        foreach (var shift in wshiDto)
                        {
                            GenerateShiftInfo(shift, allPayItems.Items, strHTMLBuilder, Vshift);
                            Vshift++;
                        }
                    }
                }
            }
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        private void GenerateLocationTable(WorkPeriodSummaryOutput  workPeriod, StringBuilder strHTMLBuilder)
        {
            strHTMLBuilder.Append("<br/><h3 style='text-align:center;'> Summary </h3><br/>");

            strHTMLBuilder.Append("<table class='table table-bordered'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td class='border_bottom_none text-center font_weight_bold w-30' >{L("Code")}</td>");
            strHTMLBuilder.Append($"<td class='border_bottom_none font_weight_bold w-70'>{workPeriod.LocationCode}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td class='border_bottom_none text-center font_weight_bold w-30'>{L("Name")}</td>");
            strHTMLBuilder.Append($"<td class='border_bottom_none font_weight_bold w-70' >{workPeriod.LocationName}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td class='border_bottom_none text-center font_weight_bold w-30'>{L("Date")}</td>");
            strHTMLBuilder.Append($"<td class='border_bottom_none font_weight_bold w-70'>{workPeriod.ReportStartDay.ToString(_dateFormat)}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td class='border_bottom_none text-center font_weight_bold w-30'>{L("TicketCount")}</td>");
            strHTMLBuilder.Append($"<td class='border_bottom_none font_weight_bold w-70'>{workPeriod.TotalTicketCount}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td class='text-center font_weight_bold w-30'>{L("Total")}</td>");
            strHTMLBuilder.Append($"<td class='font_weight_bold w-70'>{workPeriod.Total}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
        }
        private void GenerateShiftInfo(WorkShiftDto wshiDto, IReadOnlyList<GetPaymentTypeForViewDto> allPayItems, StringBuilder strHTMLBuilder, int shift)
        {
            strHTMLBuilder.Append($"<br/><h3 style='text-align:center;'> Shift - {shift} </h3><br/>");
            strHTMLBuilder.Append("<table class='table table-bordered'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PaymentType")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Actual")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Entered")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Difference")}</th>");
            strHTMLBuilder.Append("</tr>");


            foreach (var pInfo in wshiDto.PaymentInfos)
            {
                var mypType = allPayItems.FirstOrDefault(a => a.PaymentType.Id == pInfo.PaymentType);
                if (mypType != null)
                {
                    strHTMLBuilder.Append("<tr class='font_14'>");
                    strHTMLBuilder.Append($"<td class='text-center font_weight_bold'>{mypType.PaymentType.Name}</td>");
                    strHTMLBuilder.Append($"<td class='text-center font_weight_bold'>{pInfo.Actual}</td>");
                    strHTMLBuilder.Append($"<td class='text-center font_weight_bold'>{pInfo.Entered}</td>");
                    strHTMLBuilder.Append($"<td class='text-center font_weight_bold'>{pInfo.Difference}</td>");
                    strHTMLBuilder.Append("</tr>");
                }
            }
            strHTMLBuilder.Append("</table>");
        }
            public async Task<FileDto> ExportItemSalesForWorkPeriodExcel(GetItemInput input, IWorkPeriodAppService appService, string location)
        {
            var file =
                  new FileDto("WorkPeriod_Items-" + location + "-" + input.StartDate.ToString("yyyy-MMMM-dd") + ".xlsx",
                      MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var allPeriods = await appService.GetWorkPeriodByLocationForDates(new WorkPeriodSummaryInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationId = input.Location
            });

            if (allPeriods.Any())
                using (var excelPackage = new ExcelPackage())
                {
                    foreach (var period in allPeriods)
                    {
                        var sheet =
                            excelPackage.Workbook.Worksheets.Add(period.StartTime.ToShortTimeString() + "-" +
                                                                 period.Wid +
                                                                 ":");
                        sheet.OutLineApplyStyle = true;
                        var row = 1;

                        AddDetail(sheet, row, 1, L("StartTime"));
                        AddDetail(sheet, row, 2, period.StartTime.ToString("f"));

                        AddDetail(sheet, row, 3, L("EndTime"));
                        AddDetail(sheet, row, 4, period.EndTime.ToString("f"));

                        row++;

                        AddDetail(sheet, row, 1, L("TotalTickets"));
                        AddDetail(sheet, row, 2, period.TotalTicketCount);

                        AddDetail(sheet, row, 3, L("TotalSales"));
                        AddDetail(sheet, row, 4, Math.Round(period.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero));

                        row++;
                        row++;
                        row++;

                        AddHeader(
                            sheet,
                            row++,
                            L("Category"),
                            L("MenuItemName"),
                            L("Portion"),
                            L("Quantity"),
                            L("Price"),
                            L("Total")
                        );

                        if (period.Id != null) input.WorkPeriodId = period.Id.Value;

                        var outPut = await appService.GetItemSalesForWorkPeriod(input);
                        if (DynamicQueryable.Any(outPut))
                            foreach (var old in outPut)
                            {
                                var colCount = 1;
                                sheet.Cells[row, colCount++].Value = old.Category;
                                sheet.Cells[row, colCount++].Value = old.MenuItemName;
                                sheet.Cells[row, colCount++].Value = old.PortionName;
                                sheet.Cells[row, colCount++].Value = old.Quantity;
                                sheet.Cells[row, colCount++].Value = Math.Round(old.Price, _roundDecimals, MidpointRounding.AwayFromZero);
                                sheet.Cells[row, colCount++].Value = Math.Round(old.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                                row++;
                            }

                        for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();
                    }

                    SaveToPath(excelPackage, file);
                }

            return ProcessFile(input.ExportOutputType, file);
        }
        #endregion Work-Day-Detail


        #region Week day report
        public async Task<FileDto> ExportWeekDaySales(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("WeekDaySeles"));
            builder.Append("-");
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetWeekDaySales(excelPackage, input, appService);
                SaveToPath(excelPackage, file);
            }
            return ProcessFile(input.ExportOutputType, file);
        }
        private async Task GetWeekDaySales(ExcelPackage package, WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {

            var sheet = package.Workbook.Worksheets.Add(L("WeekDaySeles"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                L("Date"),
                L("TotalTickets"),
                L("Sales"),
                L("Average"),
                L("Tax"),
                L("Total")
            };

            int rowCount = 1;
            AddHeader(
                sheet, rowCount, headers.ToArray()
            );

            rowCount++;

            var dtos = await appService.BuildWeekDaySales(input);

            if (dtos != null)
            {
                foreach (var item in dtos)
                {
                    sheet.Cells[rowCount, 1].Value = item.Date;
                    sheet.Cells[rowCount, 2].Value = GetDecimalFormat(item.TotalTickets);
                    sheet.Cells[rowCount, 3].Value = GetDecimalFormat(item.Sales);
                    sheet.Cells[rowCount, 4].Value = GetDecimalFormat(item.Average);
                    sheet.Cells[rowCount, 5].Value = GetDecimalFormat(item.Tax);
                    sheet.Cells[rowCount, 6].Value = GetDecimalFormat(item.Total);
                    rowCount++;
                }
                // Footer
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 2].Value = GetDecimalFormat(dtos.Sum(x => x.TotalTickets));
                sheet.Cells[rowCount, 3].Value = GetDecimalFormat(dtos.Sum(x => x.Sales));
                sheet.Cells[rowCount, 4].Value = GetDecimalFormat(dtos.Sum(x => x.Average));
                sheet.Cells[rowCount, 5].Value = GetDecimalFormat(dtos.Sum(x => x.Tax));
                sheet.Cells[rowCount, 6].Value = GetDecimalFormat(dtos.Sum(x => x.Total));

                for (int i = 1; i <= 6; i++)
                {
                    sheet.Cells[rowCount, i].Style.Font.Bold = true;
                }
            }

            for (int i= 1;i<= 6; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }
        #endregion Week day report

        #region Week end report
        public async Task<FileDto> ExportWeekEndSales(WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("WeekEndSeles"));
            builder.Append("-");
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetWeekEndSales(excelPackage, input, appService);
                SaveToPath(excelPackage, file);
            }
            return ProcessFile(input.ExportOutputType, file);
        }
        private async Task GetWeekEndSales(ExcelPackage package, WorkPeriodSummaryInput input, IWorkPeriodAppService appService)
        {

            var sheet = package.Workbook.Worksheets.Add(L("WeekEndSeles"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                L("Date"),
                L("TotalTickets"),
                L("Sales"),
                L("Average"),
                L("Tax"),
                L("Total")
            };

            int rowCount = 1;
            AddHeader(
                sheet, rowCount, headers.ToArray()
            );

            rowCount++;

            var dtos = await appService.BuildWeekEndSales(input);

            if (dtos != null)
            {
                foreach (var item in dtos)
                {
                    sheet.Cells[rowCount, 1].Value = item.Date;
                    sheet.Cells[rowCount, 2].Value = GetDecimalFormat(item.TotalTickets);
                    sheet.Cells[rowCount, 3].Value = GetDecimalFormat(item.Sales);
                    sheet.Cells[rowCount, 4].Value = GetDecimalFormat(item.Average);
                    sheet.Cells[rowCount, 5].Value = GetDecimalFormat(item.Tax);
                    sheet.Cells[rowCount, 6].Value = GetDecimalFormat(item.Total);
                    rowCount++;
                }
                // Footer
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 2].Value = GetDecimalFormat(dtos.Sum(x => x.TotalTickets));
                sheet.Cells[rowCount, 3].Value = GetDecimalFormat(dtos.Sum(x => x.Sales));
                sheet.Cells[rowCount, 4].Value = GetDecimalFormat(dtos.Sum(x => x.Average));
                sheet.Cells[rowCount, 5].Value = GetDecimalFormat(dtos.Sum(x => x.Tax));
                sheet.Cells[rowCount, 6].Value = GetDecimalFormat(dtos.Sum(x => x.Total));

                for (int i = 1; i <= 6; i++)
                {
                    sheet.Cells[rowCount, i].Style.Font.Bold = true;
                }
            }
           
            for (int i = 1; i <= 6; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }
        #endregion Week end report
    }
}

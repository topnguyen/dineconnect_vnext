﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport
{
    public interface IPlanLoggerAppService: IApplicationService
    {
        Task<PagedResultDto<PlanLoggerListDto>> BuildGetAll(GetPlanLoggerInput inputDto);
        Task<TimeLineOutput> BuildTimelineLogs(int? input);
        Task<FileDto> BuildGetExcel(GetPlanLoggerInput inputDto);
        Task<ListResultDto<ComboboxItemDto>> GetLogTypes();
    }
}

﻿using DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Exporting
{
    public interface IPlanLoggerExporter
    {
        Task<FileDto> ExportLogs(IPlanLoggerAppService logService, GetPlanLoggerInput input);
    }
}

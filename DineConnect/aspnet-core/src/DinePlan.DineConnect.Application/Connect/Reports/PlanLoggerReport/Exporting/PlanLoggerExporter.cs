﻿using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Exporting
{
    internal class PlanLoggerExporter : EpPlusExcelExporterBase, IPlanLoggerExporter
    {
        private readonly string dateFormat = "dd-MM-yyyy HH:ss:mm";
        public PlanLoggerExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public async Task<FileDto> ExportLogs(IPlanLoggerAppService logService, GetPlanLoggerInput input)
        {
            var file = new FileDto("Log" + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var package = new ExcelPackage())
            {
                var sheet = package.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var headers = new List<string>
                {
                    L("EventTime"),
                    L("EventName"),
                    L("UserName"),
                    L("TicketNumber"),
                    L("TicketTotal"),
                    L("Location"),
                    L("EventLog"),
                };
                AddHeader(
                    sheet, true,
                    headers.ToArray()
                );

                var secondTime = false;
                int i = 0;
                var rowCount = 2;

                while (true)
                {
                    input.MaxResultCount = 100;
                    if (secondTime)
                    {
                        input.NotCorrectDate = true;
                    }
                    var outPut = await logService.BuildGetAll(input);
                    if (!secondTime)
                    {
                        secondTime = true;
                    }
                    if (outPut.Items.Any())
                    {
                        for (i = 0; i < outPut.Items.Count; i++)
                        {
                            var colCount = 1;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventTime.ToString(dateFormat);
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].UserName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].TicketNo;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].TicketTotal;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].LocationName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventLog;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }
                SaveToPath(package, file);
            }
            return ProcessFile(input.ExportOutputType, file);
        }
    }
}

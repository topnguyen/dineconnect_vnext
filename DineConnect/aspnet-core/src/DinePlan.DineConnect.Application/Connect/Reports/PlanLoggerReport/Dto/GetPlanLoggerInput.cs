﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto
{
    public class GetPlanLoggerInput: PagedAndSortedInputDto, IShouldNormalize, IDateInput
    {
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool Group { get; set; }
        public bool Unfinished { get; set; }
        public List<ComboboxItemDto> Events { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public ExportType ExportType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public bool Credit { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "EventTime";
            }
        }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
    }
}

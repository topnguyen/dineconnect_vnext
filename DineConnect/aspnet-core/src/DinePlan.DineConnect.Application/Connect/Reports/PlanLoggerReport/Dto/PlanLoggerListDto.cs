﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto
{
        [AutoMapFrom(typeof(PlanLogger))]
        public class PlanLoggerListDto 
        {
            public virtual int? Id { get; set; }
            public virtual string Terminal { get; set; }
            public virtual int TicketId { get; set; }
            public virtual string TicketNo { get; set; }
            public virtual decimal TicketTotal { get; set; }
            public virtual string UserName { get; set; }
            public virtual string EventLog { get; set; }
            public virtual string EventName { get; set; }
            public virtual DateTime EventTime { get; set; }
            public virtual int TenantId { get; set; }
            public virtual int LocationId { get; set; }
            public virtual string LocationName { get; set; }
        }

        [AutoMapTo(typeof(PlanLogger))]
        public class PlanLoggerEditDto
        {
            public virtual int? Id { get; set; }
            public virtual string Terminal { get; set; }
            public virtual int TicketId { get; set; }
            public virtual string TicketNo { get; set; }
            public virtual decimal TicketTotal { get; set; }
            public virtual string UserName { get; set; }
            public virtual string EventLog { get; set; }
            public virtual string EventName { get; set; }
            public virtual DateTime EventTime { get; set; }
            public virtual int TenantId { get; set; }
            public virtual int LocationId { get; set; }
        }

        public class GetPlanLoggerOutput : IOutputDto
        {
            public PlanLoggerEditDto PlanLogger { get; set; }
        }

        public class CreateOrUpdatePlanLoggerInput : IInputDto
        {
            [Required]
            public PlanLoggerEditDto PlanLogger { get; set; }
        }

        public class CreateOrUpdatePlanLoggerOutput : IOutputDto
        {
            [Required]
            public int LogId { get; set; }
        }

        public class EventOutput : IOutputDto
        {
            public string BadgeClass { get; set; }
            public string BadgeIconClass { get; set; }
            public string Title { get; set; }
            public string When { get; set; }
            public string Content { get; set; }
        }

        public class TimeLineOutput : IOutputDto
        {
            public TimeLineOutput()
            {
                EventOutputs = new List<EventOutput>();
                Descriptions = new List<NameValueDto>();
            }

            public List<EventOutput> EventOutputs { get; set; }
            public List<NameValueDto> Descriptions { get; set; }
        }

}

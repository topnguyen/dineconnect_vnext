﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Dto;
using DinePlan.DineConnect.Connect.Reports.PlanLoggerReport.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Reports.PlanLoggerReport
{
    
    public class PlanLoggerAppService : DineConnectReportAppServiceBase, IPlanLoggerAppService
    {
        private readonly ILogger _logger;
        private readonly IRepository<Location> _lService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locService;
        private readonly IPlanLoggerExporter _exporter;
        private readonly IRepository<PlanLogger> _planLogger;
        public PlanLoggerAppService(ILocationAppService locService, 
            IRepository<Ticket> ticketRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Department> departmentRepo,
            ILogger logger, IPlanLoggerExporter exporter,
        IRepository<PlanLogger> planLogger) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _planLogger = planLogger;
            _logger = logger;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
            _exporter = exporter;
        }

        public async Task<PagedResultDto<PlanLoggerListDto>> BuildGetAll(GetPlanLoggerInput input)
        {
            var correctDate = CorrectInputDate(input);
            var loggers = _planLogger.GetAll();
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (correctDate)
                {
                    loggers =
                        loggers.Where(
                            a =>
                                a.EventTime >=
                                input.StartDate
                                &&
                                a.EventTime <=
                                input.EndDate);
                }
                else
                {
                    loggers =
                        loggers.Where(
                            a =>
                                a.EventTime >=
                                input.StartDate
                                &&
                                a.EventTime <=
                                input.EndDate);
                }
            }
            if (input.Location > 0)
            {
                loggers = loggers.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new Common.Dto.CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }

            var ticketNumbers = new List<string>();
            if (input.Unfinished)
            {
                loggers = loggers.Where(a => !string.IsNullOrEmpty(a.TicketNo) && !a.TicketNo.Equals("0"));
                var allGroup = loggers.ToList().GroupBy(a => a.TicketNo);

                foreach (var tGroup in allGroup)
                {
                    if (!tGroup.Any(a => a.EventName.Equals("PAYMENT")))
                    {
                        ticketNumbers.Add(tGroup.Key);
                    }
                }
                if (ticketNumbers.Any())
                {
                    loggers = loggers.Where(a => ticketNumbers.Contains(a.TicketNo));
                }
            }
            else if (input.Events != null && input.Events.Any())
            {
                var events = input.Events.Select(a => a.DisplayText).ToList();
                loggers = loggers.Where(a => events.Contains(a.EventName));
            }

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    loggers = loggers.BuildQuery(filRule);
                }
            }

            var sortLoggers = await loggers
                .PageBy(input)
                .ToListAsync();
            var result = new List<PlanLoggerListDto>();
            sortLoggers.ForEach(a => result.Add(new PlanLoggerListDto
            {
                UserName = a.UserName,
                EventLog = a.EventLog,
                EventName = a.EventName,
                EventTime = a.EventTime,
                Id = a.Id,
                LocationId = a.LocationId,
                LocationName = a.Location.Name,
                TicketNo = a.TicketNo,
                TicketTotal = a.TicketTotal,
            }));
            var allListDtos = result;

            var allItemCount = loggers.Count();

            return new PagedResultDto<PlanLoggerListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> BuildGetExcel(GetPlanLoggerInput input)
        {
            return await _exporter.ExportLogs(this, input);
        }

        public async Task<TimeLineOutput> BuildTimelineLogs(int? input)
        {
            List<EventOutput> output = new List<EventOutput>();
            List<NameValueDto> nvDots = new List<NameValueDto>();

            TimeLineOutput returnOutput = new TimeLineOutput
            {
                EventOutputs = output,
                Descriptions = nvDots
            };

            if (input.HasValue && input.Value > 0)
            {
                var indLog = await _planLogger.GetAsync(input.Value);

                if (!string.IsNullOrEmpty(indLog.EventLog))
                {
                    var allString = indLog.EventLog.Split(';');
                    if (allString.Length > 0)
                    {
                        nvDots.AddRange(from allStr in allString
                                        select allStr.Split('=')
                            into valPair
                                        where valPair.Length > 1
                                        select new NameValueDto
                                        {
                                            Name = valPair[0],
                                            Value = valPair[1]
                                        });
                    }
                }
                if (!string.IsNullOrEmpty(indLog?.TicketNo))
                {
                    var allLogs = _planLogger.GetAll().
                        Where(a => a.TicketNo.Equals(indLog.TicketNo) && a.LocationId.Equals(indLog.LocationId));
                    if (allLogs.Any())
                    {
                        output.AddRange(allLogs.OrderBy(a => a.EventTime).Select(pll => new EventOutput()
                        {
                            Content = "Total : " + pll.TicketTotal + "  " + pll.EventLog,
                            Title = pll.EventName,
                            When = pll.EventTime.ToString(),
                            BadgeClass = "info",
                            BadgeIconClass = "glyphicon-check",
                        }));
                    }
                }
            }
            return returnOutput;
        }
        public async Task<ListResultDto<ComboboxItemDto>> GetLogTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;

            foreach (var name in DineConnectConsts.LogConsts)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }
    }
}

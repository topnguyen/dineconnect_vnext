﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.SaleSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleSummaryReport
{
    
    public class SaleSummaryReportAppService : DineConnectReportAppServiceBase, ISaleSummaryReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPromotionRepository;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payRe;
        private readonly IRepository<Master.TransactionTypes.TransactionType> _tRe;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<Promotion> _promoRepo;
        private ISaleSummaryReportExcelExporter _saleSummaryReportExcelExporter;
        private readonly ILocationAppService _locService;
        private readonly ITransactionTypesAppService _ttService;
        private readonly IPaymentTypesAppService _ptService;

        public SaleSummaryReportAppService(ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Location> locationRepository
            , IRepository<MenuItem> menuItemRepository
            , IRepository<Promotion> promotionRepository
            , IRepository<MenuItemPortion> menuItemPromotionRepository
            , ISaleSummaryReportExcelExporter saleSummaryReportExcelExporter
            , IRepository<Master.PaymentTypes.PaymentType> payRe
            , IRepository<Master.TransactionTypes.TransactionType> tRe
            , ITransactionTypesAppService ttService
            , IPaymentTypesAppService ptService
            , IRepository<Promotion> promoRepo
            )
            : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _locationRepository = locationRepository;
            _menuItemRepository = menuItemRepository;
            _promotionRepository = promotionRepository;
            _menuItemPromotionRepository = menuItemPromotionRepository;
            _saleSummaryReportExcelExporter = saleSummaryReportExcelExporter;
            _payRe = payRe;
            _tRe = tRe;
            _locService = locService;
            _departmentRepo = departmentRepo;
            _promoRepo = promoRepo;
            _ttService = ttService;
            _ptService = ptService;
            _promoRepo = promoRepo;
        }

        public async Task<SaleSummaryStatsDto> BuildSaleSummaryReport(GetTicketInput input)
        {
            var returnOutput = new SaleSummaryStatsDto();
            var mySDt = new List<SaleSummaryDto>();
            input.EndDate = input.EndDate.AddDays(1);
            var daysDifference = (input.EndDate - input.StartDate).Days;

            for (var i = 0; i < daysDifference; i++)
            {
                input.EndDate = input.StartDate.AddDays(1);
                var tickets = GetAllTickets(input);
                var output = tickets.Select(a => a.LocationId).Distinct();
                foreach (var dDto in output)
                {
                    var tiO = tickets.Where(a => a.LocationId.Equals(dDto));

                    var summaryDto = new SaleSummaryDto
                    {
                        Date = input.StartDate,
                        LocationId = dDto,
                        Total = tiO.Sum(a => a.TotalAmount),
                        TotalTicketCount = tiO.Count(),
                        TotalCovers = 0
                    };
                    mySDt.Add(summaryDto);

                    if (input.ReportDisplay)
                        continue;

                    try
                    {
                        var totalPaxCount = 0;
                        var checkForTags = tiO.Where(a => !string.IsNullOrEmpty(a.TicketTags)).Select(a => a.TicketTags);
                        foreach (var tag in checkForTags)
                            if (!string.IsNullOrEmpty(tag))
                            {
                                var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                                var lastTag = myTag?.LastOrDefault(a =>
                                    a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                                if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                            }

                        summaryDto.TotalCovers = totalPaxCount;
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    var tioP = tickets.Include(x => x.Payments).Where(a => a.LocationId.Equals(dDto))
                        .SelectMany(a => a.Payments)
                        .ToList()
                        .GroupBy(a => a.PaymentTypeId);

                    var pTypes = new Dictionary<int, string>();
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        foreach (var pName in tioP)
                        {
                            var paymentName = "";
                            if (pTypes.ContainsKey(pName.Key))
                            {
                                paymentName = pTypes[pName.Key];
                            }
                            else
                            {
                                var pType =await _payRe.FirstOrDefaultAsync(pName.Key);
                                if (pType != null)
                                {
                                    if (input.PaymentName)
                                    {
                                        paymentName = pType.Name;
                                    }
                                    else
                                    {
                                        paymentName = pType.AccountCode;
                                        if (string.IsNullOrEmpty(paymentName))
                                            paymentName = pType.Name;
                                    }

                                    pTypes.Add(pName.Key, paymentName);
                                }
                            }

                            if (summaryDto.Payments.ContainsKey(paymentName))
                                summaryDto.Payments[paymentName] += pName.Sum(a => a.Amount);
                            else
                                summaryDto.Payments[paymentName] = pName.Sum(a => a.Amount);
                        }
                    }

                    var tioT =
                          tickets.Include(x=>x.Transactions).Where(a => a.LocationId.Equals(dDto))
                            .SelectMany(a => a.Transactions)
                            .ToList()
                            .GroupBy(a => a.TransactionTypeId);

                    var tTypes = new Dictionary<int, string>();
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        foreach (var pName in tioT)
                        {
                            var tName = "";
                            if (tTypes.ContainsKey(pName.Key))
                            {
                                tName = tTypes[pName.Key];
                            }
                            else
                            {
                                var pType =await _tRe.FirstOrDefaultAsync(pName.Key);
                                if (pType != null)
                                {
                                    tName = pType.Name;
                                    tTypes.Add(pName.Key, tName);
                                }
                            }

                            if (summaryDto.Transactions.ContainsKey(tName))
                                summaryDto.Transactions[tName] += pName.Sum(a => a.Amount);
                            else
                                summaryDto.Transactions[tName] = pName.Sum(a => a.Amount);
                        }
                    }

                    // Promotion Discount                    
                    var tioPromotion =
                       tickets.Where(a => a.LocationId.Equals(dDto) && !string.IsNullOrEmpty(a.TicketPromotionDetails))
                           .Select(a => a.TicketPromotionDetails).ToList();

                    if (tioPromotion.Any())
                    {
                        List<Dtos.PromotionDetailValue> promotionsValues = new List<Dtos.PromotionDetailValue>(); ;
                        tioPromotion.ForEach(t => promotionsValues.AddRange(JsonConvert.DeserializeObject<List<Dtos.PromotionDetailValue>>(t)));
                        summaryDto.Promotions = promotionsValues
                                                .GroupBy(t => new { t.PromotionId, t.PromotionName })
                                                .Select(t => new { t.Key.PromotionName, Amount = t.Sum(p => p.PromotionAmount) })
                                                .ToDictionary(t => t.PromotionName, t => t.Amount);
                    }



                    var departmentReportDtos = new List<DepartmentReportDto>();
                    summaryDto.Departments = departmentReportDtos;
                    var lstDepDto = tickets.Where(a => a.LocationId.Equals(dDto)).ToList().GroupBy(a => a.DepartmentName);
                    foreach (var depDto in lstDepDto)
                        departmentReportDtos.Add(new DepartmentReportDto
                        {
                            DepartmentName = depDto.Key,
                            Total = depDto.Sum(a => a.TotalAmount),
                            TotalTicketCount = depDto.Count()
                        });
                }

                input.StartDate = input.StartDate.Date.AddDays(1);
            }

            foreach (var mySd in mySDt)
                if (mySd.LocationId > 0)
                {
                    var loc = await _locationRepository.FirstOrDefaultAsync(mySd.LocationId);

                    if (loc != null)
                    {
                        mySd.Location = loc.Name;
                        mySd.LocationCode = loc.Code;
                    }
                }

            var menuItemCount = mySDt.Count();
            var dashDto = new DashboardSaleDto();

            dashDto.Dates.AddRange(mySDt.Select(key => key.DateStr));
            dashDto.Totals.AddRange(mySDt.Select(key => key.Total));
            dashDto.TicketCounts.AddRange(mySDt.Select(key => key.TotalTicketCount));

            returnOutput.Dashboard = dashDto;
            returnOutput.Sale = new PagedResultDto<SaleSummaryDto>(
                menuItemCount,
                mySDt.OrderBy(a => a.Date).ToList()
            );
            return returnOutput;
        }

        public async Task<FileDto> BuildSaleSummaryExcel(GetTicketInput input)
        {
            var ttNames = await _ttService.GetAllItems();
            var ttList = ttNames.Items.Select(a => a.Name).ToList();
            var ptNames = await _ptService.GetAllItems();
            var ptList = ptNames.Items.Select(a => a.PaymentType.Name).ToList();
            var dtNames = await _departmentRepo.GetAll().ToListAsync();
            var dtList = dtNames.Select(a => a.Name).ToList();  
            var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
            input.ExportType = "EXPORT";
            return await _saleSummaryReportExcelExporter.ExportToSalesSummary(input, this, ttList, ptList, dtList, proList,
                                      (int)AbpSession.TenantId);
        }

        public async Task<FileDto> BuildLocationComparisonReportToExcel(GetTicketInput input)
        {
            var tickets = await GetAllTickets(input).ToListAsync();

            var dateGroup = tickets.GroupBy(t => t.LastPaymentTimeTruc);
            var locations = await _locationRepository.GetAllListAsync();

            var locationGroup = dateGroup.Select(g =>
            {
                var dto = new LocationComparisonReportOuptut
                {
                    Date = g.Key,
                    LocationDetails = g.GroupBy(l => new { l.LocationId, l.Location?.Name })
                        .Select(e => new LocationComparisonDetails
                        {
                            LocationId = e.Key.LocationId,
                            LocationName = e.Key.Name,
                            SubTotal = e.Sum(t => t.TotalAmount)
                        }).ToList()
                };
                var locationNotin = locations.Where(l => !dto.LocationDetails.Select(e => e.LocationId).Contains(l.Id))
                    .Select(e => new LocationComparisonDetails
                    {
                        LocationId = e.Id,
                        LocationName = e.Name,
                        GrossSales = 0,
                        SubTotal = 0
                    }).ToList();
                dto.LocationDetails.AddRange(locationNotin);
                dto.LocationDetails = dto.LocationDetails.OrderBy(e => e.LocationName).ToList();
                return dto;
            }).ToList();

            return _saleSummaryReportExcelExporter.ExportLocationComparison(locationGroup);
        }

        public async Task<ConsolidatedDto> GetConsolidated(GetTicketInput input)
        {
            var result = new ConsolidatedDto();
            var ticketsForFilter = GetAllTickets(input).ToList();

            var tickets = new List<ConsolidatedItem>();
            foreach (var ticket in ticketsForFilter)
            {
                var subTotal = ticket.GetPlainSum() + ticket.GetPreTaxServicesTotal();
                var tax = ticket.GetTaxTotal();
                if (ticket.TaxIncluded) subTotal = subTotal - tax;
                tickets.Add(new ConsolidatedItem
                {
                    LocationID = ticket.LocationId,
                    DepartmentName = ticket.DepartmentName,
                    LocationName = ticket.Location.Name,
                    WithOutTax = subTotal,
                    WithTax = ticket.TotalAmount,
                });
            }

            var ticketBy_Location_Deparment = tickets.GroupBy(x => new { x.LocationID, x.LocationName, x.DepartmentName, })
                 .Select(x => new ConsolidatedBy_Location_Department
                 {
                     LocationID = x.Key.LocationID,
                     LocationName = x.Key.LocationName,
                     DepartmentName = x.Key.DepartmentName,
                     ListItem = x.ToList()
                 });
            var ticketBy_Location = ticketBy_Location_Deparment.GroupBy(x => new { x.LocationID, x.LocationName, })
             .Select(x => new ConsolidatedBy_Location
             {
                 LocationID = x.Key.LocationID,
                 LocationName = x.Key.LocationName,
                 ListItem = x.ToList()
             });
            result.ListItem = ticketBy_Location.ToList();
            return await Task.FromResult(result);
        }
        public async Task<FileDto> BuildConsolidatedExport(GetTicketInput input)
        {
            return await _saleSummaryReportExcelExporter.ExportConsolidatedReportToFile(input, this);
        }

    }

}

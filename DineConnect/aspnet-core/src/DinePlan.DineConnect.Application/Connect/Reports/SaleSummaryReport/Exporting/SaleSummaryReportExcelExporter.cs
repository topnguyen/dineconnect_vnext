﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Connect.Reports.SaleSummaryReport.Exporting
{
    public class SaleSummaryReportExcelExporter : EpPlusExcelExporterBase, ISaleSummaryReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        public string[] restrictTypes = new string[] { "VAT", "VAT 7" };
        private IRepository<Master.Locations.Location> _locRepo;
        public IFeatureChecker FeatureChecker { protected get; set; }
        public SaleSummaryReportExcelExporter(
              ITempFileCacheManager tempFileCacheManager
            , ITenantSettingsAppService tenantSettingsAppService
            , IRepository<Master.Locations.Location> locRepo) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _locRepo = locRepo;
        }

        public async Task<FileDto> ExportToSalesSummary(GetTicketInput input, ISaleSummaryReportAppService appService, List<string> tt, List<string> pp, List<string> dts, List<string> proList, int tenantId)
        {
            var countryCode = FeatureChecker.GetValue(tenantId, AppFeatures.ConnectCountry);

            var file = new FileDto("Summary_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                int rowCount = AddReportHeader(sheet, L("SaleSummary_r"), input);

                input.PaymentName = true;

                var outPut = await appService.BuildSaleSummaryReport(input);
                input.Sorting = "LocationId";
                var headers = new List<string> { L("Date"), L("Location"), L("TotalTickets"), L("TotalCovers"), L("TotalAmount") };
                headers.AddRange(dts);
                headers.AddRange(pp);
                headers.Add("Total Promotion");
                headers.AddRange(tt);
                if (countryCode.Equals("TH"))
                    headers.Add(L("NetSales"));

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                decimal total = 0;
                var ticketCount = 0;
                var ticketCoverCount = 0;

                var i = 0;
                var pptotal = new Dictionary<string, decimal>();
                var dttotal = new Dictionary<string, decimal>();
                var ttotal = new Dictionary<string, decimal>();
                var proTotal = 0M;

                if (DynamicQueryable.Any(outPut.Sale.Items))
                    for (i = 0; i < outPut.Sale.TotalCount; i++)
                    {
                        sheet.Cells[i + rowCount, 1].Value = outPut.Sale.Items[i].DateStr;
                        sheet.Cells[i + rowCount, 2].Value = outPut.Sale.Items[i].Location;
                        sheet.Cells[i + rowCount, 3].Value = outPut.Sale.Items[i].TotalTicketCount;
                        sheet.Cells[i + rowCount, 4].Value = outPut.Sale.Items[i].TotalCovers;
                        sheet.Cells[i + rowCount, 5].Value = outPut.Sale.Items[i].Total;
                        total += outPut.Sale.Items[i].Total;
                        ticketCount += outPut.Sale.Items[i].TotalTicketCount;
                        ticketCoverCount += outPut.Sale.Items[i].TotalCovers;

                        var index = 6;
                        foreach (var ppt in dts)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Departments.Any(a => a.DepartmentName.Equals(ppt)))
                                ppAmount =
                                    outPut.Sale.Items[i].Departments.First(a => a.DepartmentName.Equals(ppt)).Total;

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            if (dttotal.ContainsKey(ppt))
                                dttotal[ppt] += ppAmount;
                            else
                                dttotal[ppt] = ppAmount;
                            index++;
                        }

                        foreach (var ppt in pp)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Payments.ContainsKey(ppt))
                                ppAmount = outPut.Sale.Items[i].Payments[ppt];

                            sheet.Cells[i + rowCount, index].Value = ppAmount;

                            if (pptotal.ContainsKey(ppt))
                                pptotal[ppt] += ppAmount;
                            else
                                pptotal[ppt] = ppAmount;

                            index++;
                        }

                       
                        var proAmount = 0M;
                        proAmount = outPut.Sale.Items[i].Promotions.Where(p => proList.Contains(p.Key)).Sum(p => p.Value);
                        sheet.Cells[i + rowCount, index].Value = proAmount;
                        proTotal += proAmount;
                        index++;


                        var mainTotal = outPut.Sale.Items[i].Total;

                        if (mainTotal > 0)
                        {
                            mainTotal = mainTotal - mainTotal * 7 / 107;
                            mainTotal = Math.Round(mainTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        var taxAmount = outPut.Sale.Items[i].Total - mainTotal;
                        taxAmount = Math.Round(taxAmount, _roundDecimals, MidpointRounding.AwayFromZero);

                        foreach (var ttt in tt)
                        {
                            var ttAmount = 0M;
                            if (outPut.Sale.Items[i].Transactions.ContainsKey(ttt))
                                ttAmount = outPut.Sale.Items[i].Transactions[ttt];

                            if (!string.IsNullOrEmpty(countryCode) && restrictTypes.Contains(ttt.ToUpper()))
                            {
                                if (countryCode.Equals("TH"))
                                    ttAmount = taxAmount;
                            }

                            sheet.Cells[i + rowCount, index].Value = ttAmount;

                            if (ttotal.ContainsKey(ttt))
                                ttotal[ttt] += ttAmount;
                            else
                                ttotal[ttt] = ttAmount;

                            index++;
                        }
                        if (mainTotal != 0M)
                        {
                            if (ttotal.ContainsKey("NETSALE"))
                            {
                                ttotal["NETSALE"] += mainTotal;
                            }
                            else
                            {
                                ttotal["NETSALE"] = mainTotal;
                            }
                            if (countryCode.Equals("TH"))
                                sheet.Cells[i + rowCount, index].Value = mainTotal;
                        }
                    }
                var tindex = 2;

                sheet.Cells[i + rowCount, tindex++].Value = L("Total");
                sheet.Cells[i + rowCount, tindex++].Value = ticketCount;
                sheet.Cells[i + rowCount, tindex++].Value = ticketCoverCount;
                sheet.Cells[i + rowCount, tindex++].Value = total;
                foreach (var ppt in dts)
                {
                    if (dttotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = dttotal[ppt];

                    tindex++;
                }
                foreach (var ppt in pp)
                {
                    if (pptotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = pptotal[ppt];
                    tindex++;
                }

                sheet.Cells[i + rowCount, tindex].Value = proTotal;
                tindex++;

                foreach (var ppt in tt)
                {
                    if (ttotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = ttotal[ppt];
                    tindex++;
                }
                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Bold = true;
                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Size = 16;

                for (i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }


        public FileDto ExportLocationComparison(List<LocationComparisonReportOuptut> dtos)
        {
            var file = new FileDto("LocationComparisonReport-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                 MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Collections"));
                sheet.OutLineApplyStyle = true;
                var locations = new List<string>();
                if(dtos.Count > 0)
                {
                    locations = dtos.FirstOrDefault()?.LocationDetails.Select(l => l.LocationName).ToList();
                }

                var headers = new List<string>
                {
                    L("Date"),
                    L("Day"),
                    L("Week")
                };
                headers.AddRange(locations);
                headers.Add(L("Total"));

                AddHeader(sheet, true, headers.ToArray());

                var row = 2;
                foreach (var item in dtos)
                {
                    var col = 1;
                    AddDetail(sheet, row, col++, item.Date.ToString("dd-MM-yyyy"));
                    AddDetail(sheet, row, col++, item.Date.ToString("ddd"));
                    AddDetail(sheet, row, col++, item.WeekNumber);
                    item.LocationDetails.ForEach(l =>
                    {
                        AddDetail(sheet, row, col++, Math.Round(l.SubTotal, _roundDecimals, MidpointRounding.AwayFromZero));
                    });

                    AddDetail(sheet, row, col++, Math.Round(item.LocationDetails.Sum(l => l.SubTotal), _roundDecimals, MidpointRounding.AwayFromZero));

                    row++;
                }

                for (var i = 1; i <= headers.Count() + 2; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                SaveToPath(excelPackage, file);
            }
            return file;
        }
        public async Task<FileDto> ExportConsolidatedReportToFile(GetTicketInput input, ISaleSummaryReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Consolidated"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetConsolidatedExport(excelPackage, input, appService);
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private async Task GetConsolidatedExport(ExcelPackage package, GetTicketInput ticketInput, ISaleSummaryReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("Consolidated"));
            worksheet.OutLineApplyStyle = true;

            var row = 1;
            worksheet.Cells[row, 2].Value = L("FromDate");
            worksheet.Cells[row, 3].Value = ticketInput.StartDate.ToString(_simpleDateFormat);
            worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

            row++;
            worksheet.Cells[row, 2].Value = L("EndDate");
            worksheet.Cells[row, 3].Value = ticketInput.EndDate.ToString(_simpleDateFormat);
            worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;


            row++;
            worksheet.Cells[row, 1].Value = L("LocationID");
            worksheet.Cells[row, 3].Value = L("WithOutTax");
            worksheet.Cells[row, 4].Value = L("WithTax");
            worksheet.Cells[row, 5].Value = L("Total");
            worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

            var dtos = await appService.GetConsolidated(ticketInput);

            row++;
            foreach (var dataBy_Location in dtos.ListItem)
            {
                worksheet.Cells[row, 1].Value = dataBy_Location.LocationID;
                worksheet.Cells[row, 2].Value = dataBy_Location.LocationName;
                worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

                row++;
                foreach (var dataBy_Department in dataBy_Location.ListItem)
                {
                    //set color date
                    worksheet.Cells[row, 2].Value = dataBy_Department.DepartmentName;
                    worksheet.Cells[row, 3].Value = dataBy_Department.TotalWithOutTax;
                    worksheet.Cells[row, 4].Value = dataBy_Department.TotalWithTax;
                    worksheet.Cells[row, 5].Value = dataBy_Department.TotalAmount;
                    row++;
                }
                row++;
            }
            worksheet.Cells[row, 2].Value = L("Total");
            worksheet.Cells[row, 3].Value = dtos.TotalWithOutTax;
            worksheet.Cells[row, 4].Value = dtos.TotalWithTax;
            worksheet.Cells[row, 5].Value = dtos.TotalAmount;
            worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

            for (var i = 1; i <= 5; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }


        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";

            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Location myLocation = null;

            //if (input.Location > 0)
            //    myLocation = _.Get(input.Location);

            //if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            //{
            //    var simpleL = input.LocationGroup.Locations.First();
            //    if (simpleL != null)
            //    {
            //        myLocation = _locRepo.Get(simpleL.Id);
            //    }
            //}

            //if (myLocation != null)
            //{
            //    Master.Company myCompany = _comRepo.Get(myLocation.CompanyRefId);
            //    if (myCompany != null)
            //    {
            //        brandName = myCompany.Name;
            //    }
            //    locationCode = myLocation.Code;
            //    locationName = myLocation.Name;
            //}

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;

            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(_simpleDateFormat) + " to " + input.EndDate.ToString(_simpleDateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(_dateTimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }

    }
}
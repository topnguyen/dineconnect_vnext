﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleSummaryReport.Exporting
{
	public interface ISaleSummaryReportExcelExporter
	{
		Task<FileDto> ExportToSalesSummary(GetTicketInput input, ISaleSummaryReportAppService connectReportAppService, List<string> ttList, List<string> ptList, List<string> dtList, List<string> proList, int tenantId);
		FileDto ExportLocationComparison(List<LocationComparisonReportOuptut> dtos);
		Task<FileDto> ExportConsolidatedReportToFile(GetTicketInput input, ISaleSummaryReportAppService appService);
	}
}

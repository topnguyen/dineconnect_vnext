﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport
{
    public interface ISaleTargetAppService: IApplicationService
    {
        Task<FileDto> BuildSaleTargetTemplate(SaleTargetTemplateInput input);
        Task<FileDto> BuildSaleTargetExcel(SaleTargetTicketInput input);
        Task<PagedResultDto<SaleTargetViewDto>> BuildAll(SaleTargetTicketInput input);
        Task Import(string fileToken);
    }
}

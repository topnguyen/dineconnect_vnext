﻿using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Importing
{
    public class SaleTargetListExcelDataReader : NpoiExcelImporterBase<SaleTargetImportDto>, ISaleTargetListExcelDataReader
    {
        public List<SaleTargetImportDto> GetSalesTargetFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }
        private SaleTargetImportDto ProcessExcelRow(ISheet worksheet, int row)
        {
            try
            {

                var salesTargetEditDto = new SaleTargetImportDto();
                salesTargetEditDto.LocationId = worksheet.GetRow(row).GetCell(0)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.LocationCode = worksheet.GetRow(row).GetCell(1)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.LocationName = worksheet.GetRow(row).GetCell(2)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Monday = worksheet.GetRow(row).GetCell(3)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Tuesday = worksheet.GetRow(row).GetCell(4)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Wednesday = worksheet.GetRow(row).GetCell(5)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Thursday = worksheet.GetRow(row).GetCell(6)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Friday = worksheet.GetRow(row).GetCell(7)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Saturday = worksheet.GetRow(row).GetCell(8)?.StringCellValue ?? string.Empty;
                salesTargetEditDto.Sunday = worksheet.GetRow(row).GetCell(9)?.StringCellValue ?? string.Empty;
                return salesTargetEditDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

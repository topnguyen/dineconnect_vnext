﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Importing
{
    public interface ISaleTargetListExcelDataReader: IApplicationService
    {
        List<SaleTargetImportDto> GetSalesTargetFromExcel(byte[] fileBytes);
    }
}

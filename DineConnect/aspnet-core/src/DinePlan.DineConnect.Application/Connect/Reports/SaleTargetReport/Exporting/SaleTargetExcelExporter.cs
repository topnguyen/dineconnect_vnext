﻿using Abp.AspNetZeroCore.Net;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Exporting
{
    public class SaleTargetExcelExporter : EpPlusExcelExporterBase, ISaleTargetExcelExporter
    {
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Location> _locationRepository;
        public SaleTargetExcelExporter(ITempFileCacheManager tempFileCacheManager, ILocationAppService locationAppService, IRepository<Location> locationRepository) : base(tempFileCacheManager)
        {
            _locationRepository = locationRepository;
            _locationAppService = locationAppService;
        }

        public async Task<FileDto> ExportSaleTargetAsync(SaleTargetTicketInput input, ISaleTargetAppService saleTargetAppService)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(L("SalesTarget"));
            input.MaxResultCount = 1000;
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var saleTargets = await saleTargetAppService.BuildAll(input);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Sheet"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                    L("Date"),
                    L("LocationCode"),
                    L("LocationNameTarget"),
                    L("Day"),
                    L("SalesTarget"),
                    L("SalesValue"),
                    L("Difference")
                };
                AddHeader(
                    sheet,
                    true, headers.ToArray()
                );


                var rowCount = 2;
                var colCount = 1;
                foreach (var saleTarget in saleTargets.Items)
                {
                    colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.Date.ToString("dd/MM/yyyy");
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.LocationCode;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.LocationName;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.Day;
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.SaleTarget, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.SaleValue, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.Difference, _roundDecimals, MidpointRounding.AwayFromZero);

                    rowCount++;
                }

                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 1, rowCount, 7].Style.Font.Bold = true;
                colCount = 2;
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.SaleTarget, _roundDecimals, MidpointRounding.AwayFromZero));
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.SaleValue, _roundDecimals, MidpointRounding.AwayFromZero));
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.Difference, _roundDecimals, MidpointRounding.AwayFromZero));
                var lastRow = rowCount;
                // sheet.Cells[1, 1, lastRow + 1, 10].Style.WrapText = true;
                for (var i = 1; i <= 7; i++) sheet.Column(i).AutoFit();
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        public async Task<FileDto> ExportSaleTargetTemplateAsync(SaleTargetTemplateInput input)
        {
            var builder = new StringBuilder();
            builder.Append(L("Target_Dates"));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);


            var allLocation = _locationRepository.GetAll();

            var listLocationId = new List<int>();
            if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                    && !input.LocationGroup.Locations.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });

            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }

            var locations = listLocationId.Count > 0 ? allLocation.Where(location => listLocationId.Contains(location.Id)).ToList() : allLocation.ToList();


            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Sheet"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                     "LOCATION ID",
                        "LOCATION CODE",
                        "LOCATION NAME",
                        "MONDAY",
                        "TUESDAY",
                        "WEDNESDAY",
                        "THURSDAY",
                        "FRIDAY",
                        "SATURDAY",
                        "SUNDAY"
                };
                AddHeader(
                    sheet,
                    true, headers.ToArray()
                );


                var rowCount = 2;

                foreach (var location in locations)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = location.Id;
                    sheet.Cells[rowCount, colCount++].Value = location.Code;
                    sheet.Cells[rowCount, colCount++].Value = location.Name;

                    rowCount++;
                }

                var lastRow = rowCount;
                // sheet.Cells[1, 1, lastRow + 1, 10].Style.WrapText = true;
                for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
    }
}

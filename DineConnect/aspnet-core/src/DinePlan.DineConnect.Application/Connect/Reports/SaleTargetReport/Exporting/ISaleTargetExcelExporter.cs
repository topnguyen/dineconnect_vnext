﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Exporting
{
    public interface ISaleTargetExcelExporter
    {
        Task<FileDto> ExportSaleTargetTemplateAsync(SaleTargetTemplateInput input);
        Task<FileDto> ExportSaleTargetAsync(SaleTargetTicketInput input, ISaleTargetAppService saleTargetAppService);
    }
}

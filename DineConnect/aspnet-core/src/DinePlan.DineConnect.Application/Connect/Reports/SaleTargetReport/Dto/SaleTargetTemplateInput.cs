﻿using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto
{
    public class SaleTargetTemplateInput
    {
        public GetLocationInput LocationInput { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
    }
    public class FileUploadViewModel
    {
        public string FileName { get; set; }
    }
}

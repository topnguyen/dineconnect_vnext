﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto
{
    public class SaleTargetViewDto
    {
        public DateTime Date { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string Day { get; set; }
        public decimal SaleTarget { get; set; }
        public decimal SaleValue { get; set; }
        public decimal Difference => SaleTarget - SaleValue;
    }
}

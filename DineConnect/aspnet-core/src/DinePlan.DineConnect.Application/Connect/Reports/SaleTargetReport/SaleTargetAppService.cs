﻿using Abp.Application.Services.Dto;
using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Dto;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Exporting;
using DinePlan.DineConnect.Connect.Reports.SaleTargetReport.Importing;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.SaleTargetReport
{
    public class SaleTargetAppService : DineConnectReportAppServiceBase, ISaleTargetAppService
    {
        public IAppFolders AppFolders { get; set; }
        private readonly ISaleTargetExcelExporter _exporter;
        private readonly ISaleTargetListExcelDataReader _readExcel;
        private readonly IAppFolders _appFolders;
        private readonly ISaleSummaryReportAppService _appSummaryReportAppService;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IHostEnvironment _env;
        public SaleTargetAppService(IHostEnvironment env,ISaleTargetListExcelDataReader readExcel, ISaleTargetExcelExporter exporter,ITempFileCacheManager tempFileCacheManager ,IAppFolders appFolders,ISaleSummaryReportAppService appSummaryReportAppService,ILocationAppService locService, IRepository<Ticket> ticketRepo, IUnitOfWorkManager unitOfWorkManager, IRepository<Department> departmentRepo) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _appSummaryReportAppService = appSummaryReportAppService;
            _appFolders = appFolders;
            _tempFileCacheManager = tempFileCacheManager;
            _exporter = exporter;
            _readExcel = readExcel;
            _env = env;
        }

        public async Task<PagedResultDto<SaleTargetViewDto>> BuildAll(SaleTargetTicketInput input)
        {
            var result = new List<SaleTargetViewDto>();

            var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, "Target_Dates.xlsx");
            if (!File.Exists(filePath))
            {
                return null;
            }
            var saleTargetTable = ReadFromExcelFile(filePath, "");
            if (saleTargetTable == null) return null;
            input.PaymentName = true;

            var saleSummary = await _appSummaryReportAppService.BuildSaleSummaryReport(input);


            foreach (var item in saleSummary.Sale.Items)
            {
                var locationCodeData = item.LocationCode;
                for (var i = 0; i < saleTargetTable.Rows.Count; i++)
                {
                    var locationCodeExcel = saleTargetTable.Rows[i].ItemArray[1].ToString();
                    if (locationCodeData == locationCodeExcel)
                    {
                        var targetView = new SaleTargetViewDto();
                        targetView.Date = item.Date;
                        targetView.LocationCode = item.LocationCode;
                        targetView.LocationName = item.Location;

                        var indexDay = ((int)item.Date.DayOfWeek == 0) ? 7 : (int)item.Date.DayOfWeek;

                        targetView.Day = item.Date.DayOfWeek.ToString();

                        if (decimal.TryParse(saleTargetTable.Rows[i].ItemArray[indexDay + 2].ToString(), out var saleTargetDecimal))
                        {
                            targetView.SaleTarget = saleTargetDecimal;
                        }
                        targetView.SaleValue = item.Total;

                        result.Add(targetView);
                    }
                }
            }


            // filter by dynamic filter
            var dataAsQueryable = result.AsQueryable();

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }
            result = dataAsQueryable.ToList();
            var totalCount = result.Count;

            if (!input.Sorting.Equals("Id"))
                result = result.OrderBy(s=>s.Date).ToList();

            var outputPageDtos = result
                .AsQueryable()
                .PageBy(input)
                .ToList();
            return new PagedResultDto<SaleTargetViewDto>(
                totalCount,
                outputPageDtos
            );
        }
        public async Task Import(string fileToken)
        {
            var fileName = fileToken;
            if (string.IsNullOrWhiteSpace(fileName)) return;

            var fileBytes = _tempFileCacheManager.GetFile(fileName);

            using (var stream = new MemoryStream(fileBytes))
            {
                using (var excelPackage = new ExcelPackage(stream))
                {
                    CheckTemplateExist();
                    SaveToPath(excelPackage, new FileDto { Date = DateTime.Now, FileName = "SaleTarget", FileToken = "Target_Dates.xlsx" });
                    var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                    var headers = new List<string>
                    {
                        "LOCATION ID",
                        "LOCATION CODE",
                        "LOCATION NAME",
                        "MONDAY",
                        "TUESDAY",
                        "WEDNESDAY",
                        "THURSDAY",
                        "FRIDAY",
                        "SATURDAY",
                        "SUNDAY"
                    };

                    if (worksheet != null)
                    {
                        var columns = worksheet.Dimension.End.Column;

                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (var i = 1; i < columns + 1; i++) sheetHeaders.Add(worksheet.GetValue(1, i).ToString());

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }
            }
        }
        private void CheckTemplateExist()
        {
            var myFileNameSub = "Target_Dates.xlsx";
            var fullPathSub = Path.Combine(AppFolders.TempFileDownloadFolder, myFileNameSub);
            if (System.IO.File.Exists(fullPathSub))
            {
                DeleteFile(myFileNameSub);
            }
        }
        protected void SaveToPath(ExcelPackage excelPackage, FileDto file)
        {
            var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
            excelPackage.SaveAs(new FileInfo(filePath));
        }
        protected void DeleteFile(string file)
        {
            var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file);
            File.Delete(filePath);
        }
        private DataTable ReadFromExcelFile(string path, string sheetName)
        {
            var dt = new DataTable();
            using (ExcelPackage package = new ExcelPackage(new FileInfo(path)))
            {
                if (package.Workbook.Worksheets.Count < 1)
                {
                    return null;
                }

                var workSheet = package.Workbook.Worksheets.FirstOrDefault(x => x.Name == sheetName) ??
                                           package.Workbook.Worksheets.FirstOrDefault();
                foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                {
                    dt.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                {
                    var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                    var newRow = dt.NewRow();
                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;

                    }

                    dt.Rows.Add(newRow);
                }
            }

            var columnNameList = new List<string>();
            for (var i = 0; i < dt.Columns.Count; i++)
            {
                columnNameList.Add(dt.Columns[i].ColumnName);

            }
            var targetColumnName = new List<string>(new[] {"LOCATION ID", "LOCATION CODE", "LOCATION NAME",
            "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"});

            return !columnNameList.SequenceEqual(targetColumnName) ? null : dt;
        }
        public async Task<FileDto> BuildSaleTargetExcel(SaleTargetTicketInput input)
        {
            return await _exporter.ExportSaleTargetAsync(input,this);
        }
        public async Task<FileDto> BuildSaleTargetTemplate(SaleTargetTemplateInput input)
        {
            return await _exporter.ExportSaleTargetTemplateAsync(input);
        }
    }
}

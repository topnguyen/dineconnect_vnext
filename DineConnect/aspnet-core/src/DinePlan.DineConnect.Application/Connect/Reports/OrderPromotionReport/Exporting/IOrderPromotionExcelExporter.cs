﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.OrderPromotionReport.Exporting
{
	public interface IOrderPromotionReportExcelExporter
	{
		Task<FileDto> ExportToFileOrderPromotion(GetItemInput input, IOrderPromotionReportAppService appService);
	}
}

﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.GroupReports.Exporting;
using DinePlan.DineConnect.Connect.Reports.OrderPromotionReport.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.OrderPromotionReport
{
	[AbpAuthorize(AppPermissions.Pages_Tenant_Connect_Report_Promotion)]
	public class OrderPromotionReportAppService : DineConnectReportAppServiceBase, IOrderPromotionReportAppService
	{
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IOrderPromotionReportExcelExporter _exporter;

        public OrderPromotionReportAppService(
              IRepository<Promotion> promotionRepository,
              IOrderPromotionReportExcelExporter exporter,
              ILocationAppService locService,
              IRepository<Ticket> ticketRepository,
              IRepository<Department> departmentRepository,
              IUnitOfWorkManager unitOfWorkManager
            ) : base(locService,ticketRepository, unitOfWorkManager, departmentRepository)
        {
            _promotionRepository = promotionRepository;
            _exporter = exporter;
        }

        public async Task<OrderStatsDto> BuildOrderPromotionReport(GetItemInput input)
        {
            var returnOutput = new OrderStatsDto();
            var allOrders = GetAllOrders(input);
            var orders = allOrders.Where(a => a.IsPromotionOrder && !string.IsNullOrEmpty(a.OrderPromotionDetails));

            if (input.Promotions != null && input.Promotions.Any())
            {
                var allIds = input.Promotions.Select(a => a.Id);
                var searchedList = (await orders.ToListAsync()).Where(a => allIds.Any(p => a.GetOrderPromotionList().Select(e => e.PromotionSyncId).Contains(p)));

                orders = searchedList.AsQueryable();
            }

            returnOutput.DashBoardDto = GetPromoStatics(orders);
            var sortTickets = orders.OrderBy(input.Sorting).PageBy(input);
            List<OrderListDto> outputDtos = sortTickets.ToList().MapTo<List<OrderListDto>>();

            var menuItemCount = orders.Count();
            returnOutput.OrderList = new PagedResultDto<OrderListDto>(
                menuItemCount,
                outputDtos
            );
            return returnOutput;
        }

        public async Task<FileDto> BuildOrderPromotionExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            return await _exporter.ExportToFileOrderPromotion(input, this);
        }

        private DashboardOrderDto GetPromoStatics(IQueryable<Order> orders)
        {
            var orderDto = new DashboardOrderDto();
            try
            {
                if (!orders.Any()) return orderDto;
                orderDto.TotalAmount = orders.DefaultIfEmpty().Sum(a => a.PromotionAmount * a.Quantity);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
                throw ex;
            }

            return orderDto;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Reports
{
    public abstract class DineConnectReportAppServiceBase : DineConnectAppServiceBase
    {
        protected readonly IRepository<Department> _departmentRepo;
        protected readonly ILocationAppService _locService;
        protected readonly IRepository<Ticket> _ticketRepo;
        protected readonly IUnitOfWorkManager _unitOfWorkManager;


        protected DineConnectReportAppServiceBase(
            ILocationAppService locService,
            IRepository<Ticket> ticketRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Department> departmentRepo)
        {
            _locService = locService;
            _ticketRepo = ticketRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _departmentRepo = departmentRepo;
        }

        public IQueryable<Ticket> GetAllTickets(IDateInput input)
        {
            IQueryable<Ticket> tickets;

            if (!string.IsNullOrEmpty(input.TicketNo))
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    tickets = _ticketRepo.GetAllIncluding().Include(s => s.Location)
                        .Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
                    return tickets;
                }

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                tickets = _ticketRepo.GetAllIncluding(a => a.Orders, a => a.Payments, a => a.Transactions);
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate.Date;
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTimeTruc >= mySt
                                    &&
                                    a.LastPaymentTimeTruc <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }
            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            tickets = tickets.Where(a => a.Credit == input.Credit);
            return tickets;
        }


        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.StartDate.Equals(DateTime.MaxValue)
                                                           && !input.EndDate.Equals(DateTime.MinValue) &&
                                                           !input.EndDate.Equals(DateTime.MaxValue)
               )
                if (input.EndDate > input.StartDate)
                {
                    var days = input.EndDate - input.StartDate;
                    if (days.Days == 0)
                        return false;
                    return true;
                }

            var returnStatus = !(input.StartDate.Equals(DateTime.MinValue) && input.EndDate.Equals(DateTime.MinValue));

            return returnStatus;
        }

        protected IQueryable<Order> GetAllOrders(GetItemInput input)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input)
                    .WhereIf(!string.IsNullOrEmpty(input.TerminalName),
                        e => e.TerminalName.Contains(input.TerminalName))
                ;

            var orders = tickets.SelectMany(a => a.Orders)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            if (!input.VoidParam && !input.Gift && !input.Comp)
                orders = orders.Where(a => a.CalculatePrice && a.DecreaseInventory);

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));

            if (input.VoidParam)
                orders = orders.Where(a =>
                    !a.CalculatePrice && !a.IncreaseInventory && !a.DecreaseInventory &&
                    !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Void"));
            else if (input.Gift)
                orders =
                    orders.Where(a =>
                        !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                        !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Gift"));
            else if (input.Comp)
                orders =
                    orders.Where(
                        a =>
                            !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                            !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Comp"));

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    foreach (var lst in filRule.Rules)
                        if (lst.Field == "Void")
                        {
                            orders = orders.Where(a =>
                                !a.CalculatePrice && !a.IncreaseInventory && !a.DecreaseInventory &&
                                !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Void"));
                        }
                        else if (lst.Field == "Gift")
                        {
                            orders =
                                orders.Where(a =>
                                    !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                                    !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Gift"));
                        }
                        else if (lst.Field == "Comp")
                        {
                            orders =
                                orders.Where(
                                    a =>
                                        !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                                        !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Comp"));
                        }
                        else if (lst.Field == "DepartmentName")
                        {
                            var deptId = int.Parse(lst.Value);
                            var rsdepartment = _departmentRepo.FirstOrDefault(t => t.Id == deptId);
                            if (rsdepartment != null)
                                orders =
                                    orders.Where(e => e.DepartmentName.Contains(rsdepartment.Name));
                        }
                        else if (lst.Field == "TicketNo")
                        {
                            orders =
                                orders.Where(t => t.Ticket.TicketNumber == lst.Value);
                        }

                    filRule.Rules = filRule.Rules.Where(t =>
                        t.Value != null && t.Field != "Void" && t.Field != "Comp" && t.Field != "Gift" &&
                        t.Field != "DepartmentName" && t.Field != "TicketNo").ToList();
                    if (filRule.Rules != null && filRule.Rules.Count > 0)
                        orders = orders.BuildQuery(filRule);
                }
            }

            return orders;
        }

        public Ticket GetTicketByNumber(string ticketNumber)
        {
            var ticket = _ticketRepo.GetAllIncluding(t => t.Payments).Where(x => x.TicketNumber.Equals(ticketNumber))
                .FirstOrDefault();
            return ticket;
        }

        protected IQueryable<Order> GetAllOrdersNoDynamicFilter(GetItemInput input)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input)
                    .WhereIf(!string.IsNullOrEmpty(input.TerminalName),
                        e => e.TerminalName.Contains(input.TerminalName))
                ;

            var orders = tickets.SelectMany(a => a.Orders)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            if (!input.VoidParam && !input.Gift && !input.Comp)
                orders = orders.Where(a => a.CalculatePrice && a.DecreaseInventory);

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));

            return orders;
        }

        public IQueryable<Ticket> GetAllTicketsForTicketInputNoFilterByDynamicFilter(GetTicketInput input)
        {
            try
            {
                var tickets = GetAllTickets(input).Include(s => s.Transactions.Select(a => a.TransactionType)).Include(s=>s.Payments);

                if (input.Payments != null && input.Payments.Any())
                    tickets = tickets.Include(a => a.Payments).Include(p => p.Payments.Select(t => t.Ticket))
                        .WhereIf(input.Payments.Any(),
                            p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

                if (input.Transactions != null && input.Transactions.Any())
                {
                    var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                    tickets = from p in tickets.Include(a => a.Transactions)
                        where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                        select p;
                }

                if (!string.IsNullOrEmpty(input.TerminalName))
                    tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

                if (!string.IsNullOrEmpty(input.Department))
                    tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
                if (input.Departments != null && input.Departments.Any())
                {
                    var allDeparments = input.Departments.Select(a => a.DisplayText).ToArray();
                    tickets = tickets.Where(a => a.DepartmentName != null && allDeparments.Contains(a.DepartmentName));
                }

                if (input.TicketTags != null && input.TicketTags.Any())
                {
                    var allDeparments = input.TicketTags.Select(a => a.DisplayText).ToArray();
                    tickets = from c in tickets
                        where allDeparments.Any(i => c.TicketTags.Contains(i))
                        select c;
                }

                if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                    tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

                if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

                return tickets;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
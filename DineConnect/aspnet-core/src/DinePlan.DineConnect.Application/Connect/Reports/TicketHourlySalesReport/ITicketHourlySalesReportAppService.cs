﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport.Exporting;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport
{
    public interface ITicketHourlySalesReportAppService : IApplicationService
    {
        Task<TicketHourListDto> GetHourlySalesSummary(GetTicketInput input);

        Task<HourDto> GetHourlySales(GetTicketInput input);

        Task<IdHourListDto> GetHourlySalesByRange(GetTicketInput input);

        Task<List<IdLocationHourListDto>> GetLocationHourlySalesByRange(GetTicketInput input);

        Task<IdHourListDto> GetLocationHourySales(GetTicketInput input);

        Task<FileDto> GetAllToExcel(GetHourlySaleTicketExportInput input);
    }
}

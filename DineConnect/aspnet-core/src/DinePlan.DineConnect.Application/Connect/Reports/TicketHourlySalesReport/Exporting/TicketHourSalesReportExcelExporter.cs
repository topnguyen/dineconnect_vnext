﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport.Exporting;

namespace DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport
{
    public class TicketHourSalesReportExcelExporter : EpPlusExcelExporterBase, ITicketHourSalesReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<Location> _locRepo;
        private readonly IRepository<Brand> _comRepo;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TicketHourSalesReportExcelExporter(ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService,
            ITimeZoneConverter timeZoneConverter,
            IRepository<Location> locRepo,
            IAbpSession abpSession,

            IRepository<Brand> comRepo
            ) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _locRepo = locRepo;
            _timeZoneConverter = timeZoneConverter;
            _comRepo = comRepo;
            _abpSession = abpSession;
        }

        public async Task<FileDto> ExportHourlySummary(ITicketHourlySalesReportAppService appService, GetHourlySaleTicketExportInput input)
        {
            var file = new FileDto("Hourly-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            bool dti = input.ExportOutput.Equals("I") || input.ExportOutput.Equals("B");
            bool dta = input.ExportOutput.Equals("T") || input.ExportOutput.Equals("B");


            try
            {
                using (var excelPackage = new ExcelPackage())
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Hours-Summary"));
                    sheet.OutLineApplyStyle = true;
                    if (!input.ByLocation)
                    {
                        var serviceOutput = await appService.GetHourlySales(input);
                        if (serviceOutput == null || !serviceOutput.Hours.Any())
                        {
                            SaveToFile(excelPackage, file);
                            return file;
                        }

                        List<string> headS = new List<string> { L("TimeRange") };
                        if (dti)
                            headS.Add(L("TotalTickets"));

                        if (dta)
                            headS.Add(L("TotalSales"));
                        headS.Add(L("Average"));
                        headS.Add(L("Discount"));
                        headS.Add(L("Taxable"));
                        headS.Add(L("TotalTax"));
                        headS.Add(L("Cash"));
                        headS.Add(L("Credit"));


                        AddHeader(
                            sheet,
                            headS.ToArray()
                            );

                        var rowCount = 2;

                        var colCount = 1;

                        foreach (var output in serviceOutput.Hours)
                        {
                            colCount = 1;
                            sheet.Cells[rowCount, colCount++].Value = output.Hour;
                            if (dti)
                                sheet.Cells[rowCount, colCount++].Value = output.Tickets;
                            if (dta)
                                sheet.Cells[rowCount, colCount++].Value = output.Total;
                            sheet.Cells[rowCount, colCount++].Value = output.Average;
                            sheet.Cells[rowCount, colCount++].Value = output.Discount;
                            sheet.Cells[rowCount, colCount++].Value = output.Taxable;
                            sheet.Cells[rowCount, colCount++].Value = output.TotalTax;
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";

                            rowCount++;
                        }
                        rowCount++;
                        colCount = 1;
                        sheet.Cells[rowCount, colCount++].Value = L("Total");
                        if (dti)
                            sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.Tickets);

                        if (dta)
                            sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.Total);
                        sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.Average);
                        sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.Discount);
                        sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.Taxable);
                        sheet.Cells[rowCount, colCount++].Value = serviceOutput.Hours.Sum(a => a.TotalTax);
                        sheet.Cells[rowCount, colCount++].Value = "";
                        sheet.Cells[rowCount, colCount++].Value = "";

                        for (var i = 1; i <= 1; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                        SaveToFile(excelPackage, file);
                    }
                    else
                    {
                        var serviceOutput = await appService.GetLocationHourySales(input);
                        if (serviceOutput == null || !serviceOutput.HourList.Any())
                        {
                            SaveToFile(excelPackage, file);
                            return file;
                        }

                        var headers = new List<string>
                        {
                            L("TimeRange")
                        };
                        var sHeaders = new List<string>
                        {
                            ""
                        };
                        var colHead = 6;
                        if (dti)
                            colHead++;
                        if (dta)
                            colHead++;
                        var startCol = 1;
                        var counter = 0;
                        foreach (var allOut in serviceOutput.HourList)
                        {
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Merge = true;
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Value = allOut.Name;
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            counter++;

                            if (dti)
                                sHeaders.Add(L("TotalTickets"));
                            if (dta)
                                sHeaders.Add(L("TotalSales"));
                            sHeaders.Add(L("Average"));
                            sHeaders.Add(L("Discount"));
                            sHeaders.Add(L("Taxable"));
                            sHeaders.Add(L("TotalTax"));
                            sHeaders.Add(L("Cash"));
                            sHeaders.Add(L("Credit"));
                        }

                        AddHeader(
                            sheet,
                            headers.ToArray()
                            );

                        AddRow(
                            sheet, 2,
                            sHeaders.ToArray()
                            );

                        var firstColCount = 2;

                        var firstTime = true;
                        var rowCount = 3;
                        var secondColumn = 2;

                        foreach (var output in serviceOutput.HourList)
                        {
                            rowCount = 3;

                            foreach (var allH in output.Hours)
                            {

                                if (firstTime)
                                {
                                    sheet.Cells[rowCount, 1].Value = allH.Hour;
                                }
                                if (dti)
                                    sheet.Cells[rowCount, firstColCount++].Value = allH.Tickets;
                                if (dta)
                                    sheet.Cells[rowCount, firstColCount++].Value = allH.Total;
                                sheet.Cells[rowCount, firstColCount++].Value = allH.Average;
                                sheet.Cells[rowCount, firstColCount++].Value = allH.Discount;
                                sheet.Cells[rowCount, firstColCount++].Value = allH.Taxable;
                                sheet.Cells[rowCount, firstColCount++].Value = allH.TotalTax;
                                sheet.Cells[rowCount, firstColCount++].Value = "";
                                sheet.Cells[rowCount, firstColCount++].Value = "";
                                rowCount++;
                                firstColCount = secondColumn;

                            }
                            secondColumn += colHead;
                            firstTime = false;
                        }
                        for (var i = 1; i <= 1; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }

                        sheet.Cells[rowCount, 1].Value = L("Total");
                        var colCount = 2;
                        foreach (var output in serviceOutput.HourList)
                        {
                            if (dti)
                                sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.Tickets);
                            if (dta)
                                sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.Total);
                            sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.Average);
                            sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.Discount);
                            sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.Taxable);
                            sheet.Cells[rowCount, colCount++].Value = output.Hours.Sum(a => a.TotalTax);
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                        }
                        SaveToFile(excelPackage, file);
                    }
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        public async Task<FileDto> ExportHourlySummaryByDate(ITicketHourlySalesReportAppService appService, GetHourlySaleTicketExportInput input)
        {
            var file = new FileDto("Hourly-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                            MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            bool dti = input.ExportOutput.Equals("I") || input.ExportOutput.Equals("B");
            bool dta = input.ExportOutput.Equals("T") || input.ExportOutput.Equals("B");
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            var dateTimeFormat = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;
            var dateFormat = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;
            try
            {
                using (var excelPackage = new ExcelPackage())
                {


                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Hours-Date-Range"));
                    sheet.OutLineApplyStyle = true;
                    if (!input.ByLocation)
                    {
                        var serviceOutput = await appService.GetHourlySalesByRange(input);
                        if (serviceOutput == null || !serviceOutput.HourList.Any())
                        {
                            SaveToFile(excelPackage, file);
                            return file;
                        }

                        List<string> headers = new List<string> { L("Hour/Date") };
                        AddHeader(
                            sheet,
                            headers.ToArray()
                            );
                        var colHead = 6;
                        if (dti)
                            colHead++;
                        if (dta)
                            colHead++;
                        var startCol = 1;
                        for (int counter = 0; counter <= 23; counter++)
                        {
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Merge = true;
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Value = counter.ToString();
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        }
                        headers = new List<string> { "" };
                        for (int counter = 0; counter <= 23; counter++)
                        {
                            if (dti)
                                headers.Add(L("TotalTickets"));
                            if (dta)
                                headers.Add(L("TotalSales"));

                            headers.Add(L("Average"));
                            headers.Add(L("Discount"));
                            headers.Add(L("Taxable"));
                            headers.Add(L("TotalTax"));
                            headers.Add(L("Cash"));
                            headers.Add(L("Credit"));
                        }
                        AddRow(
                           sheet, 2,
                           headers.ToArray()
                           );
                        int rowCount = 3;
                        int colCount;
                        foreach (var output in serviceOutput.HourList)
                        {
                            colCount = 1;
                            sheet.Cells[rowCount, colCount++].Value = Convert.ToDateTime(output.Name).ToString(dateFormat);
                            foreach (var hourS in output.Hours)
                            {
                                if (dti)
                                    sheet.Cells[rowCount, colCount++].Value = hourS.Tickets;
                                if (dta)
                                    sheet.Cells[rowCount, colCount++].Value = hourS.Total;
                                sheet.Cells[rowCount, colCount++].Value = hourS.Average;
                                sheet.Cells[rowCount, colCount++].Value = hourS.Discount;
                                sheet.Cells[rowCount, colCount++].Value = hourS.Taxable;
                                sheet.Cells[rowCount, colCount++].Value = hourS.TotalTax;
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = "";
                            }
                            rowCount++;
                        }
                        for (var i = 1; i <= 1; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                        rowCount++;

                        sheet.Cells[rowCount, 1].Value = L("Total");
                        colCount = 2;
                        foreach (var output in serviceOutput.HourList.SelectMany(a => a.Hours).GroupBy(a => a.Hour))
                        {
                            if (dti)
                                sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Tickets);
                            if (dta)
                                sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Total);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Average);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Discount);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Taxable);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.TotalTax);
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                        }
                        SaveToFile(excelPackage, file);
                    }
                    else
                    {
                        var serviceOutput = await appService.GetLocationHourlySalesByRange(input);
                        if (serviceOutput == null || !serviceOutput.Any())
                        {
                            SaveToFile(excelPackage, file);
                            return file;
                        }


                        List<string> headers = new List<string> { L("Hour/Date"), "" };
                        AddHeader(
                            sheet,
                            headers.ToArray()
                            );
                        var colHead = 6;
                        if (dti)
                            colHead++;
                        if (dta)
                            colHead++;
                        var startCol = 2;
                        for (int counter = 0; counter <= 23; counter++)
                        {
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Merge = true;
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Value = counter.ToString();
                            sheet.Cells[1, counter * colHead + startCol + 1, 1, (counter + 1) * colHead + startCol].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        }
                        headers = new List<string> { "", "" };
                        for (int counter = 0; counter <= 23; counter++)
                        {
                            if (dti)
                                headers.Add(L("TotalTickets"));
                            if (dta)
                                headers.Add(L("TotalSales"));
                            headers.Add(L("Average"));
                            headers.Add(L("Discount"));
                            headers.Add(L("Taxable"));
                            headers.Add(L("TotalTax"));
                            headers.Add(L("Cash"));
                            headers.Add(L("Credit"));
                        }
                        AddRow(
                           sheet, 2,
                           headers.ToArray()
                           );
                        int rowCount = 3;
                        int colCount = 1;
                        foreach (var output in serviceOutput)
                        {
                            colCount = 1;
                            sheet.Cells[rowCount, colCount].Value = output.Name;
                            foreach (var dayL in output.HourList)
                            {
                                colCount = 2;
                                sheet.Cells[rowCount, colCount++].Value =
                                    Convert.ToDateTime(dayL.Name).ToString(dateFormat);
                                foreach (var hourl in dayL.Hours)
                                {
                                    if (dti)
                                        sheet.Cells[rowCount, colCount++].Value = hourl.Tickets;
                                    if (dta)
                                        sheet.Cells[rowCount, colCount++].Value = hourl.Total;
                                    sheet.Cells[rowCount, colCount++].Value = hourl.Average;
                                    sheet.Cells[rowCount, colCount++].Value = hourl.Discount;
                                    sheet.Cells[rowCount, colCount++].Value = hourl.Taxable;
                                    sheet.Cells[rowCount, colCount++].Value = hourl.TotalTax;
                                    sheet.Cells[rowCount, colCount++].Value = "";
                                    sheet.Cells[rowCount, colCount++].Value = "";
                                }
                                rowCount++;
                            }
                        }
                        for (var i = 1; i <= 1; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                        rowCount++;
                        sheet.Cells[rowCount, 1].Value = L("Total");
                        colCount = 3;
                        foreach (var output in serviceOutput.SelectMany(a => a.HourList).SelectMany(a => a.Hours).GroupBy(a => a.Hour))
                        {
                            if (dti)
                                sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Tickets);
                            if (dta)
                                sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Total);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Average);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Discount);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.Taxable);
                            sheet.Cells[rowCount, colCount++].Value = output.Sum(a => a.TotalTax);
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                        }
                        SaveToFile(excelPackage, file);
                    }
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            return ProcessFile(input.ExportOutputType, file);
        }

    }
}
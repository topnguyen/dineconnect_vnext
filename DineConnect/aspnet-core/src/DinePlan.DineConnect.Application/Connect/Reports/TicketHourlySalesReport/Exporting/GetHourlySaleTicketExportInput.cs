﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport.Exporting
{
    public class GetHourlySaleTicketExportInput: GetTicketInput
    {
        public string ExportOutput { get; set; }

        public string Duration { get; set; }

        public bool ByLocation { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport.Exporting;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport
{
  public interface ITicketHourSalesReportExcelExporter
    {
        Task<FileDto> ExportHourlySummary(ITicketHourlySalesReportAppService ticketReportAppService, GetHourlySaleTicketExportInput input);

        Task<FileDto> ExportHourlySummaryByDate(ITicketHourlySalesReportAppService ticketReportAppService, GetHourlySaleTicketExportInput input);
    }
}

﻿using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.Timing;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Timing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TicketHourlySalesReport
{
    
    
    public class TicketHourlySalesReportAppService : DineConnectReportAppServiceBase, ITicketHourlySalesReportAppService
    {
        private readonly ITicketHourSalesReportExcelExporter _exporter;
        private readonly ITimeZoneService _timeZoneService;
        public TicketHourlySalesReportAppService(
            IRepository<Ticket> ticketRepo,
            IUnitOfWorkManager unitOfWorkManager,
            ILocationAppService locService,
            IRepository<Department> departmentRepo,
            ITicketHourSalesReportExcelExporter exporter,
            ITimeZoneService timeZoneService
        ) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _exporter = exporter;
            _timeZoneService = timeZoneService;
        }
        #region Hourly Sales
        public async Task<TicketHourListDto> GetHourlySalesSummary(GetTicketInput input)
        {
            var tickets  = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();
            var returnOutput = new TicketHourListDto { HourList = new List<TicketHourDto>() };
            var startDate = input.StartDate.Date;
            var myHourList = new TicketHourDto
            {
                Date = startDate
            };
            BuildHour(myHourList, tickets);
            returnOutput.HourList.Add(myHourList);
            if (returnOutput.HourList.Any())
            {
                returnOutput.ChartTotal = returnOutput.HourList[0].Hours != null
                    ? returnOutput.HourList[0].Hours.Select(a => a.Total).ToList()
                    : null;
                returnOutput.ChartTicket = returnOutput.HourList[0].Hours != null
                    ? returnOutput.HourList[0].Hours.Select(a => a.Tickets).ToList()
                    : null;
            }

            return returnOutput;
        }

        public async Task<HourDto> GetHourlySales(GetTicketInput input)
        {
            var tickets = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();
            var hourDto = new HourDto();
            BuildHour (hourDto, tickets);
            return hourDto;
        }


        public async Task<IdHourListDto> GetHourlySalesByRange(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto { HourList = new List<IdHourDto>() };
            var tickets = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();

            var data = tickets.GroupBy(o => o.LastPaymentTimeTruc)
                .Select(g => new { Day = g.Key, Tickets = g });

            foreach (var allTi in data)
            {
                var myHourList = new IdHourDto
                {
                    Name = allTi.Day.ToString()
                };
                returnOutput.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets.ToList());
            }

            return returnOutput;
        }

        public async Task<List<IdLocationHourListDto>> GetLocationHourlySalesByRange(GetTicketInput input)
        {
            var returnOutput = new List<IdLocationHourListDto>();
            var tickets = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();

            var data = tickets.GroupBy(
                    o => new { o.LastPaymentTimeTruc, o.LocationId, o.Location.Name })
                .Select(g => new { g.Key.LastPaymentTimeTruc, g.Key.LocationId, g.Key.Name, Tickets = g });

            foreach (var allTi in data)
            {
                var oo = new IdLocationHourListDto { Id = allTi.LocationId, Name = allTi.Name };
                var myHourList = new IdHourDto
                {
                    Name = allTi.LastPaymentTimeTruc.ToString()
                };
                oo.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets.ToList());
                returnOutput.Add(oo);
            }

            return returnOutput;
        }

        public async Task<IdHourListDto> GetLocationHourySales(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto { HourList = new List<IdHourDto>() };
            var tickets = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();
            var lc = _locService.GetLocationForUserAndGroup(new CommonLocationGroupDto
            {
                Locations = input.LocationGroup.Locations,
                Group = false
            });

            foreach (var location in lc)
            {
                var localTickets = tickets.Where(a => a.LocationId == location);
                if (localTickets.Any())
                {
                    var myHourList = new IdHourDto
                    {
                        Id = location,
                        Name = localTickets.First().Location?.Name
                    };
                    returnOutput.HourList.Add(myHourList);
                    BuildHour(myHourList, localTickets.ToList());
                }
            }

            

            return returnOutput;
        }

        public async Task<FileDto> GetAllToExcel(GetHourlySaleTicketExportInput input)
        {
            if (input.Duration.Equals("S")) return await _exporter.ExportHourlySummary(this, input);

            return await _exporter.ExportHourlySummaryByDate(this, input);
        }

        private void BuildHour(HourDto myHourList, List<Transaction.Ticket> myTickets)
        {
            var timezoneInfo = AsyncHelper.RunSync(() => GetTimezone());
            if (myTickets.Any())
            {
                myHourList.Total = myTickets.Sum(a => a.TotalAmount);
                myHourList.Tickets = myTickets.Count();
                myHourList.Hours = myTickets
                    .GroupBy(x => TimeZoneInfo.ConvertTimeFromUtc(x.LastPaymentTime, timezoneInfo).Hour)
                    .Select(g => new HourTotalDto
                    {
                        Hour = g.Key,
                        Tickets = g.Count(),
                        Total = g.Sum(x => x.TotalAmount)
                    }).ToList();

                var allHours = myHourList.Hours.Select(a => a.Hour);

                for (var i = 0; i < 24; i++)
                {
                    var hh = allHours.Contains(i);
                    if (!hh)
                        myHourList.Hours.Add(new HourTotalDto
                        {
                            Hour = i,
                            Total = 0M,
                            Tickets = 0
                        });
                }

                myHourList.Hours = myHourList.Hours.OrderBy(a => a.Hour).ToList();
            }
        }
        #endregion

        #region Queryable

        public IQueryable<Ticket> GetAllTicketsForTicketInputQueryable(GetTicketInput input)
        {
            var tickets = GetAllTickets(input)
                .Include(t => t.Location)
                .Include(t => t.Payments).ThenInclude(x => x.PaymentType)
                .Include(t => t.Transactions).ThenInclude(x => x.TransactionType)
                .AsQueryable(); ;

            if (input.Payments != null && input.Payments.Any())
                tickets = tickets.Include(a => a.Payments)
                    .WhereIf(input.Payments.Any(),
                        p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

            if (input.Transactions != null && input.Transactions.Any())
            {
                var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                tickets = from p in tickets.Include(a => a.Transactions)
                          where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                          select p;
            }

            if (!string.IsNullOrEmpty(input.TerminalName))
                tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

            if (!string.IsNullOrEmpty(input.Department))
                tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
            if (input.Departments != null && input.Departments.Any())
            {
                var allDepartments = input.Departments.Select(a => a.DisplayText).ToList();
                tickets = tickets.Where(a =>
                    a.DepartmentName != null &&
                    allDepartments.Contains(a.DepartmentName));
            }

            if (input.TicketTags != null && input.TicketTags.Any())
            {
                var allTags = input.TicketTags.Select(a => a.DisplayText).ToArray();
                tickets = from c in tickets
                          where allTags.Any(i => c.TicketTags.Contains(i))
                          select c;
            }

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

            if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

            return tickets;
        }

        private DashboardTicketDto GetStatics(IQueryable<Ticket> allTickets)
        {
            var ticketDto = new DashboardTicketDto();

            if (!allTickets.Any()) return ticketDto;
            try
            {
                ticketDto.TotalTicketCount = allTickets.Count();
                ticketDto.TotalAmount = allTickets.DefaultIfEmpty().Sum(a => a.TotalAmount);
                ticketDto.AverageTicketAmount = ticketDto.TotalAmount / ticketDto.TotalTicketCount;
                ticketDto.TotalOrderCount = allTickets.Sum(a => a.Orders.Count);
                var myOrders = allTickets.SelectMany(a => a.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory);
                ticketDto.TotalItemSold = myOrders.Sum(b => b.Quantity);
            }
            catch (Exception)
            {
                // ignored
            }

            return ticketDto;
        }

        private async Task<TimeZoneInfo> GetTimezone()
        {
            var timezone = await SettingManager.GetSettingValueAsync(TimingSettingNames.TimeZone);
            var defaultTimeZoneId =
                await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, AbpSession.TenantId);

            timezone = timezone ?? defaultTimeZoneId;
            return _timeZoneService.FindTimeZoneById(timezone);
        }

        #endregion Queryable
    }
}

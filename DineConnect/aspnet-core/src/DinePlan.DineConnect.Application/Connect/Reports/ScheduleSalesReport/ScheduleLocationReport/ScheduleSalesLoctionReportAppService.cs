﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Dto;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleItemReport;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleLoctionReport;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleLocationReport
{
    
    public class ScheduleSalesLocationReportAppService : DineConnectReportAppServiceBase, IScheduleSalesLocationReportAppService
    {
        private IScheduleSalesItemsReportAppService _scheduleSalesItemsReportAppService;
        public ScheduleSalesLocationReportAppService(ILocationAppService locService, IRepository<Ticket> ticketRepo, IUnitOfWorkManager unitOfWorkManager, IRepository<Department> departmentRepo, IScheduleSalesItemsReportAppService scheduleSalesItemsReportAppService) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _scheduleSalesItemsReportAppService = scheduleSalesItemsReportAppService;
        }

        public async Task<FileDto> BuildLocationScheduleReportExcel(GetItemInput input)
        {
            return await _scheduleSalesItemsReportAppService.BuildScheduleReportExcel(input);
        }

        public async Task<ScheduleAllList> BuildScheduleLocationReport(GetItemInput input)
        {
            return await _scheduleSalesItemsReportAppService.BuildScheduleReport(input);
        }
    }
}
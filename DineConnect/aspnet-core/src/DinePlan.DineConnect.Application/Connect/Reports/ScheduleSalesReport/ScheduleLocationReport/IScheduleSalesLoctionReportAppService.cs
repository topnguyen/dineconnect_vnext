﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleLoctionReport
{
    public interface IScheduleSalesLocationReportAppService : IApplicationService
    {
        Task<ScheduleAllList> BuildScheduleLocationReport(GetItemInput input);
        Task<FileDto> BuildLocationScheduleReportExcel(GetItemInput input);
    }
}


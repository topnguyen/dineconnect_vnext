﻿using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Dto
{
    public class ScheduleAllList
    {
        public List<ScheduleTotal> ListSchedule { get; set; }
        public List<ScheduleReportOutput> ListResult { get; set; }
    }

    public class ScheduleTotal
    {
        public string ScheduleName { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }

        public decimal TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }

    }

    public class ScheduleReportOutput
    {
        public ScheduleReportOutput()
        {
            ScheduleReportDatas = new List<ScheduleReportData>();
        }

        public int MenuItemId { get; set; }

        public string MenuItemName { get; set; }

        public List<ScheduleReportData> ScheduleReportDatas { get; set; }
    }
}


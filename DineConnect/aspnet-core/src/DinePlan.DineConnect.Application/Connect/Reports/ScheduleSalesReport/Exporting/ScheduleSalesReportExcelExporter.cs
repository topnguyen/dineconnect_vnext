﻿using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleItemReport;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Exporting
{
    public class ScheduleSalesReportExcelExporter : EpPlusExcelExporterBase, IScheduleSalesReportExcelExporter
    {
        public ScheduleSalesReportExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
        }

        public async Task<FileDto> ExportScheduleReportToFile(GetItemInput input, IScheduleSalesItemsReportAppService appService)
        {
            var file = new FileDto("Schedule-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;
                input.MaxResultCount = 999;
                var rowCount = 2;
                var data = await appService.BuildScheduleReport(input);
                var outPut = data.ListResult;
                var schedules = await appService.GetScheduleSetting(input.Location);

                sheet.Cells[rowCount, 1, rowCount, schedules.Count() * 2 + 1].Style.Font.Bold = true;
                sheet.Cells[rowCount + 1, 1, rowCount + 1, schedules.Count() * 2 + 1].Style.Font.Bold = true;

                sheet.Cells[rowCount, 1].Value = L("MenuItem");

                var col = 2;
                schedules.ForEach(s =>
                {
                    sheet.Cells[rowCount, col].Value = s.ScheduleName;
                    sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                    sheet.Cells[rowCount + 1, col].Value = L("Quantity");
                    sheet.Cells[rowCount + 1, col + 1].Value = L("Amount");

                    col = col + 2;
                });
                rowCount += 2;

                foreach (var item in outPut)
                {
                    var colInList = 1;
                    sheet.Cells[rowCount, colInList].Value = item.MenuItemName;

                    item.ScheduleReportDatas.ForEach(s =>
                    {
                        colInList++;
                        sheet.Cells[rowCount, colInList].Value = s.Quantity;
                        sheet.Cells[rowCount, colInList + 1].Value = s.Amount;

                        colInList++;
                        col = col + 2;
                    });

                    rowCount++;
                }

                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 1, rowCount, schedules.Count() * 2 + 1].Style.Font.Bold = true;

                for (var i = 1; i <= schedules.Count(); i++)
                {
                    sheet.Cells[rowCount, i * 2].Value = outPut.Select(e => e.ScheduleReportDatas[i - 1]).Sum(e => e.Quantity);
                    sheet.Cells[rowCount, i * 2 + 1].Value = outPut.Select(e => e.ScheduleReportDatas[i - 1]).Sum(e => e.Amount);
                }

                for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();

                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
    }
}





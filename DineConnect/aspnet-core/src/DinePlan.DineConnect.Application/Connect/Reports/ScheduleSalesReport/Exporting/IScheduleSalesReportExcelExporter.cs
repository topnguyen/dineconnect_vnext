﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleItemReport;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Exporting
{
    public interface IScheduleSalesReportExcelExporter
    {
        Task<FileDto> ExportScheduleReportToFile(GetItemInput input, IScheduleSalesItemsReportAppService appService);
    }
}


﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleItemReport
{
    public interface IScheduleSalesItemsReportAppService : IApplicationService
    {
        Task<ScheduleAllList> BuildScheduleReport(GetItemInput input);
        Task<FileDto> BuildScheduleReportExcel(GetItemInput input);
        Task<List<ScheduleReportData>> GetScheduleSetting(int? locationId = null);
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Connect.Reports.SaleSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Dto;
using DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ScheduleSalesReport.ScheduleItemReport
{
    
    public class ScheduleSalesItemsReportAppService : DineConnectReportAppServiceBase, IScheduleSalesItemsReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPromotionRepository;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payRe;
        private readonly IRepository<Master.TransactionTypes.TransactionType> _tRe;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<Promotion> _promoRepo;
        private IScheduleSalesReportExcelExporter _exporter;
        private readonly ILocationAppService _locService;
        private readonly ITransactionTypesAppService _ttService;
        private readonly IPaymentTypesAppService _ptService;
        public ScheduleSalesItemsReportAppService(ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Location> locationRepository
            , IRepository<MenuItem> menuItemRepository
            , IRepository<Promotion> promotionRepository
            , IRepository<MenuItemPortion> menuItemPromotionRepository
            , IScheduleSalesReportExcelExporter exporter
            , IRepository<Master.PaymentTypes.PaymentType> payRe
            , IRepository<Master.TransactionTypes.TransactionType> tRe
            , ITransactionTypesAppService ttService
            , IPaymentTypesAppService ptService
            , IRepository<Promotion> promoRepo) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _locationRepository = locationRepository;
            _menuItemRepository = menuItemRepository;
            _promotionRepository = promotionRepository;
            _menuItemPromotionRepository = menuItemPromotionRepository;
            _exporter = exporter;
            _payRe = payRe;
            _tRe = tRe;
            _locService = locService;
            _departmentRepo = departmentRepo;
            _promoRepo = promoRepo;
            _ttService = ttService;
            _ptService = ptService;
            _promoRepo = promoRepo;
        }

        public async Task<ScheduleAllList> BuildScheduleReport(GetItemInput input)
        {
            var resultFinal = new ScheduleAllList();
            var listTotalSchedule = new List<ScheduleReportData>();
            var returnOutput = new List<ScheduleReportOutput>();

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var schedules = await GetScheduleSetting(input.Location);
                var orders = GetAllOrders(input).Include(s=>s.Ticket);

                var output = from c in orders.ToList()
                             group c by new { c.MenuItemId }
                    into g
                             select new { g.Key.MenuItemId, Items = g };

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var scheduleReportDto = new ScheduleReportOutput
                            {
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName
                            };

                            scheduleReportDto.ScheduleReportDatas = schedules.Select(s =>
                            {
                                var data = new ScheduleReportData
                                {
                                    ScheduleName = s.ScheduleName,
                                    FromTime = s.FromTime,
                                    ToTime = s.ToTime
                                };

                                var ticketBySchedule = dto.Items.Where(i =>
                                    i.Ticket.LastPaymentTime.TimeOfDay >= s.FromTime &&
                                    i.Ticket.LastPaymentTime.TimeOfDay < s.ToTime);
                                data.Quantity = ticketBySchedule.Sum(a => a.Quantity);
                                data.Price = data.Quantity == 0
                                    ? 0M
                                    : ticketBySchedule.Sum(a => a.Quantity * a.Price) / data.Quantity;
                                listTotalSchedule.Add(data);
                                return data;
                            }).ToList();

                            returnOutput.Add(scheduleReportDto);
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                returnOutput = returnOutput.OrderBy(e => e.MenuItemName).ToList();
            }

            if (!input.Sorting.Equals("Id"))
                returnOutput = returnOutput.OrderBy(input.Sorting).ToList();

            // var outputDtosNew = outputDtos.GroupBy(c => c.Hour).ToList();

            var outputPageDtos = returnOutput;
            if (input.IsSummary)
                outputPageDtos = returnOutput.Skip(input.SkipCount)
                    .Take(input.MaxResultCount)
                    .ToList();

            var groupBySchedule = from d in listTotalSchedule
                                  group d by new { d.ScheduleName }
                                  into g
                                  select new { Items = g, ScheduleName = g.Key.ScheduleName };
            var listSchedule = new List<ScheduleTotal>();
            foreach (var sche in groupBySchedule)
            {
                var first = sche.Items.FirstOrDefault();
                listSchedule.Add(new ScheduleTotal
                {
                    ScheduleName = sche.ScheduleName,
                    FromTime = first.FromTime,
                    ToTime = first.ToTime,
                    TotalAmount = sche.Items.Sum(s => s.Amount),
                    TotalQuantity = sche.Items.Sum(s => s.Quantity)
                });
            }
            resultFinal.ListResult = outputPageDtos;
            resultFinal.ListSchedule = listSchedule;
            return resultFinal;
        }

        public async Task<FileDto> BuildScheduleReportExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
                input.MaxResultCount = 10000;
            var output = await _exporter.ExportScheduleReportToFile(input, this);
            return output;
        }
        public async Task<List<ScheduleReportData>> GetScheduleSetting(int? locationId = null)
        {
            var schedules = new List<Master.Locations.Dtos.ScheduleSetting>();
            if (locationId.HasValue && locationId > 0)
            {
                schedules = await _locService.GetLocationSchedulesByLocation(locationId.Value);
            }
            else
            {
                var scheduleSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
                schedules = JsonConvert.DeserializeObject<List<Master.Locations.Dtos.ScheduleSetting>>(scheduleSetting);
            }
            if (schedules.Count == 0) schedules = new List<Master.Locations.Dtos.ScheduleSetting>();
            var listScheduleReport = schedules.Select(e =>
            {
                var dto = new ScheduleReportData
                {
                    FromTime = new TimeSpan(e.StartHour, e.StartMinute, 0),
                    ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0)
                };

                dto.ScheduleName = $@"{e.Name} ({dto.FromTime.ToString(@"hh\:mm")} - {dto.ToTime.ToString(@"hh\:mm")})";
                return dto;
            }).ToList();

            return listScheduleReport;
        }
    }
}







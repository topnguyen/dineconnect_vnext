﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.CategoryReports.Exporting;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.CategoryReports
{
    
    public class CategoryReportAppService : DineConnectReportAppServiceBase, ICategoryReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPromotionRepository;
        private ICategoryReportExcelExporter _categoryReportExcelExporter;

        public CategoryReportAppService(ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Location> locationRepository
            , IRepository<MenuItem> menuItemRepository
            , IRepository<Promotion> promotionRepository
            , IRepository<MenuItemPortion> menuItemPromotionRepository
            , ICategoryReportExcelExporter categoryReportExcelExporter
            )
            : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _locationRepository = locationRepository;
            _menuItemRepository = menuItemRepository;
            _promotionRepository = promotionRepository;
            _menuItemPromotionRepository = menuItemPromotionRepository;
            _categoryReportExcelExporter = categoryReportExcelExporter;
        }

        public async Task<CategoryStatsDto> BuildCategorySalesReport(GetItemInput input)
        {
            var returnOutput = new CategoryStatsDto();
            var returnOrders = GetAllOrdersNoDynamicFilter(input);

            // Exclude void order
            if (!input.VoidParam)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Void"));
            }

            if (!input.Gift)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || !x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Gift"));
            }

            if (!input.Comp)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || !x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Comp"));
            }

            if (!input.Refund)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Refund"));
            }

            var categoryGroupings = returnOrders.Include(x => x.MenuItem)
                .ThenInclude(x => x.Category)
                .ThenInclude(x => x.ProductGroup)
                .ToList()
                .GroupBy(o => new
                {
                    CategoryId = o.MenuItem.CategoryId,
                    CategoryName = o.MenuItem.Category?.Name,
                    ProductGroupName = o.MenuItem.Category?.ProductGroup?.Name
                });

            returnOutput.Categories = categoryGroupings.Select(c => c.Key.CategoryName).ToList();

            returnOutput.ChartOutput = categoryGroupings
                .Select(c => new BarChartOutputDto
                {
                    name = c.Key.CategoryName,
                    data = new List<decimal> { c.Sum(e => e.GetTotal()) }
                }).ToList();

            var items = categoryGroupings.Select(g =>
                new CategoryReportDto
                {
                    Id = (int)g.Key.CategoryId,
                    CategoryId = (int)g.Key.CategoryId,
                    CategoryName = g.Key.CategoryName,
                    ProductGroupName = g.Key.ProductGroupName,
                    Quantity = g.Sum(c => c.Quantity),
                    Total = g.Sum(e => e.GetTotal()),
                    MenuItems = g.GroupBy(c => c.MenuItem).Select(e => new MenuReportView
                    {
                        AliasCode = e.Key.AliasCode,
                        MenuItemName = e.Key.Name,
                        Quantity = e.Sum(a => a.Quantity),
                        Price = e.Sum(a => a.Quantity * a.Price)
                    }).ToList(),
                    LocationReports = g.GroupBy(c => new { c.Location.Id, c.Location.Code }).Select(e => new LocationReportDto
                    {
                        LocationId = e.Key.Id,
                        LocationName = e.Key.Code,
                        Quantity = e.Sum(a => a.Quantity),
                        TotalAmount = e.Sum(a => a.GetTotal())
                    }).ToList()
                });

            var dataAsQueryable = items.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }
            var menuItemCount = dataAsQueryable.Count();

            var result = new List<CategoryReportDto>();
            result = input.IsExport ? dataAsQueryable.OrderBy(input.Sorting).ToList() : dataAsQueryable.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            await FillDataForLoactions(input, result);
            returnOutput.CategoryList = new PagedResultDto<CategoryReportDto>(
                menuItemCount,
                result
            );
            return returnOutput;
        }

        private async Task FillDataForLoactions(GetItemInput input, List<CategoryReportDto> items)
        {
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locationIds = input.LocationGroup.Locations.Select(e => e.Id).ToList();
                var locations = await _locationRepository.GetAll()
                    .Where(l => locationIds.Contains(l.Id))
                    .Select(loc => new SimpleLocationDto
                    {
                        Id = loc.Id,
                        Code = loc.Code,
                        Name = loc.Name,
                    })
                    .ToListAsync();
                items.ForEach(item =>
                {
                    var locationNoData = locations.Where(c => !item.LocationReports.Select(l => l.LocationId).Contains(c.Id))
                       .Select(e => new LocationReportDto
                       {
                           LocationId = e.Id,
                           LocationName = e.Name,
                           Quantity = 0M,
                           TotalAmount = 0M
                       }).ToList();
                    item.LocationReports.AddRange(locationNoData);
                    item.LocationReports = item.LocationReports.OrderBy(l => l.LocationName).ToList();
                });
            }
        }

        public async Task<FileDto> BuildCategoryExcel(GetItemInput input)
        {
            input.MaxResultCount = 10000;
            var output = await _categoryReportExcelExporter.ExportToFileCategory(input, this);
            return output;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Reports.CategoryReports.Exporting
{
    public class CategoryReportExcelExporter : EpPlusExcelExporterBase, ICategoryReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public CategoryReportExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFileCategory(GetItemInput input, ICategoryReportAppService appService)
        {
            Dictionary<string, int> colCountDics = new Dictionary<string, int>();

            var file = new FileDto("Category-" + input.StartDate.ToString("yyyy-MM-dd") + "-" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;
                var rowCount = 1;

                var outPut = await appService.BuildCategorySalesReport(input);
                if (outPut?.CategoryList?.Items != null)
                {
                    var col = 3;
                    AddDetail(sheet, 2, 1, L("Group"));
                    AddDetail(sheet, 2, 2, L("Category"));

                    var allCate = outPut.CategoryList.Items.ToList();
                    var allLo = allCate.OrderByDescending(a => a.LocationReports.Count());
                    int colCount = 1;
                    foreach (var location in allLo.SelectMany(a => a.LocationReports.Select(b => b.LocationName)))
                    {
                        if (!colCountDics.ContainsKey(location))
                        {
                            colCountDics.Add(location, colCount++);
                            AddDetail(sheet, rowCount, col, location);
                            sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                            AddDetail(sheet, rowCount + 1, col++, L("Quantity"));
                            AddDetail(sheet, rowCount + 1, col++, L("Total"));
                        }
                    }

                    AddDetail(sheet, rowCount, col, L("Total"));
                    sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                    AddDetail(sheet, rowCount + 1, col++, L("Quantity"));
                    AddDetail(sheet, rowCount + 1, col++, L("Total"));

                    sheet.Row(2).Style.Font.Bold = true;

                    rowCount += 2;
                    var cates = outPut.CategoryList.Items.ToList();
                    BindingValueForRowInCategoryReport(sheet, rowCount, cates, allLo.First().LocationReports.Count(), colCountDics);
                }

                for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void BindingValueForRowInCategoryReport(ExcelWorksheet sheet, int rowCount, IList<CategoryReportDto> cates, int colCount, Dictionary<string, int> colDics)
        {
            foreach (var item in cates)
            {
                AddDetail(sheet, rowCount, 1, item.ProductGroupName);
                AddDetail(sheet, rowCount, 2, item.CategoryName);

                foreach (var location in item.LocationReports.OrderBy(a => a.LocationName))
                {
                    int col = colDics[location.LocationName] * 2 + 1;
                    AddDetail(sheet, rowCount, col++, location.Quantity);
                    AddDetail(sheet, rowCount, col++, location.TotalAmount);
                }

                var finalCount = colDics.Count() * 2 + 3;
                AddDetail(sheet, rowCount, finalCount++, item.Quantity);
                AddDetail(sheet, rowCount, finalCount++, item.Total);

                rowCount++;
            }
            sheet.Row(rowCount).Style.Font.Bold = true;
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.CategoryReports.Exporting
{
	public interface ICategoryReportExcelExporter
	{
		Task<FileDto> ExportToFileCategory(GetItemInput input, ICategoryReportAppService appService);
	}
}

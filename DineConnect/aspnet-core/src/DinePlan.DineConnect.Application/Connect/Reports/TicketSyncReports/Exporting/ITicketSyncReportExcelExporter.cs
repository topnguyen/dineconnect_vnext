﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.TicketSyncReports.Exporting
{
  public interface ITicketSyncReportExcelExporter
    {
        Task<FileDto> ExportTicketSyncs(ITicketSyncReportAppService ticketSyncReportAppService, GetTicketSyncInput input);
    }
}

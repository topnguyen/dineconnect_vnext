﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Reports.TicketSyncReports.Exporting
{
    public class TicketSyncReportExcelExporter : NpoiExcelExporterBase, ITicketSyncReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public TicketSyncReportExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }


        public async Task<FileDto> ExportTicketSyncs(ITicketSyncReportAppService ticketSyncReportAppService, GetTicketSyncInput input)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var startDate = !input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var endDate = !input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var fileName = $"TicketSyncs-{startDate}-{endDate}.{(input.ExportOutputType == ExportType.Excel ? "xlsx" : "html")}";

            var headers = new List<string>
            {
                L("LocationCode"),
                L("TicketSyncCount"),
                L("TicketTotalAmount"),
                L("WorkStartDate"),
                L("WorkCloseDate"),
                L("TotalTickets"),
                L("WorkTotalAmount"),
                L("Difference"),
            };

            var ticketSyncs = await ticketSyncReportAppService.GetTicketSyncs(input);

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TicketSyncs"));

                    AddHeader(sheet, headers.ToArray());

                    var rowIndex = 2;

                    foreach (var ticket in ticketSyncs.Items)
                    {
                        var values = new List<object>
                        {
                            ticket.LocationCode,
                            ticket.TicketSyncCount,
                            ticket.TicketTotalAmount,
                            ticket.WorkStartDate.ToString(fileDateSetting),
                            ticket.WorkCloseDate.ToString(fileDateSetting),
                            ticket.TotalTickets,
                            ticket.WorkTotalAmount,
                            ticket.IsDifference ? "True" : "False"
                        };

                        // Add Row to excel

                        AddRow(excelPackage, sheet, rowIndex, values.ToArray());

                        // Next Row
                        rowIndex++;
                    }


                    for (var i = 1; i <= 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                }, input.ExportOutputType);
        }
    }
}
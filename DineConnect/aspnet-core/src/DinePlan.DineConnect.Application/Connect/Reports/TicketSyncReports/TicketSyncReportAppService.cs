﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketSyncReports.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.TicketSyncReports
{
    
    public class TicketSyncReportAppService : DineConnectAppServiceBase, ITicketSyncReportAppService
    {
        private readonly IRepository<Ticket> _ticketRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<WorkPeriod> _workPeriodRepo;
        private readonly ITicketSyncReportExcelExporter _ticketSyncReportExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TicketSyncReportAppService(
            IRepository<Ticket> ticketRepo,
            IRepository<Location> locationRepo,
            IRepository<WorkPeriod> workPeriodRepo,
            ITicketSyncReportExcelExporter ticketSyncReportExcelExporter,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _ticketRepo = ticketRepo;
            _locationRepo = locationRepo;
            _workPeriodRepo = workPeriodRepo;
            _ticketSyncReportExcelExporter = ticketSyncReportExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<GetTicketSyncOutput>> GetTicketSyncs(GetTicketSyncInput input)
        {
            var result = new PagedResultDto<GetTicketSyncOutput>();

            var listTicketListDto = new List<GetTicketSyncOutput>();

            try
            {
                var workPeriods = new List<WorkPeriod>();

                var allWorkPeriods = (await _workPeriodRepo.GetAllListAsync())
                    .Where(x => x.StartTime >= input.StartDate && x.EndTime <= input.EndDate && x.EndUser != null)
                    .ToList();

                foreach (var workPeriod in allWorkPeriods)
                {
                    var myLocation = await _locationRepo.GetAsync(workPeriod.LocationId);

                    var startTime = workPeriod.StartTime.Date;

                    var endDate = workPeriod.EndTime.Date + new TimeSpan(23, 23, 59);

                    var tickets = await _ticketRepo.GetAllListAsync(x => x.LastPaymentTimeTruc >= startTime && x.LastPaymentTimeTruc <= endDate);

                    if (tickets == null)
                    {
                        continue;
                    }

                    var ticketSyncCount = tickets.Count();

                    var ticketTotalAmount = tickets.Sum(x => x.TotalAmount);

                    listTicketListDto.Add(new GetTicketSyncOutput
                    {
                        LocationCode = myLocation?.Code,
                        LocationName = myLocation?.Name,
                        TicketSyncCount = ticketSyncCount,
                        TicketTotalAmount = ticketTotalAmount,
                        WorkStartDate = workPeriod.StartTime,
                        WorkCloseDate = workPeriod.EndTime,
                        TotalTickets = workPeriod.TotalTicketCount,
                        WorkTotalAmount = workPeriod.TotalSales,
                        IsDifference = ticketSyncCount != workPeriod.TotalTicketCount || ticketTotalAmount != workPeriod.TotalSales
                    });
                }


                if (input.Sorting == null)
                {
                    input.Sorting = "locationCode";
                }

                listTicketListDto = listTicketListDto.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();

                result = new PagedResultDto<GetTicketSyncOutput>()
                {
                    Items = listTicketListDto,
                    TotalCount = workPeriods.Count
                };

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public async Task<FileDto> GetTicketSyncToExcel(GetTicketSyncInput input)
        {
            input.MaxResultCount = 10000;

            var output = await _ticketSyncReportExcelExporter.ExportTicketSyncs(this, input);
            
            return output;
        }
    }
}

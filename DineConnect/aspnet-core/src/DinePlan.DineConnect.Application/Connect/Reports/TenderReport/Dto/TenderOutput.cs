﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.TenderReport.Dto
{

        public class TenderViewDto
        {
            public TenderViewDto()
            {
                TenderDetails = new List<TenderDetail>();
            }

            public string PaymentTypeName => PaymentType != null ? PaymentType.Name : string.Empty;

            public Master.PaymentTypes.PaymentType PaymentType { get; set; }

            public List<TenderDetail> TenderDetails { get; set; }
        }

        public class TenderDetail
        {
            public string TenderCode { get; set; }
            public string TenderName { get; set; }
            public string TicketNo { get; set; }
            public string Terminal { get; set; }

            public string InvoiceNo { get; set; }
            public DateTime DateTime { get; set; }
            public string Table { get; set; }
            public string Employee { get; set; }
            public string CardNumber { get; set; }
            public string ReferenceNo { get; set; }
            public decimal SubTotal => (Total - Vat - ServiceCharge + Discount);
            public decimal Discount { get; set; }
            public decimal ServiceCharge { get; set; }
            public decimal Vat { get; set; }
            public decimal Total { get; set; }
        }
    public class PaymentFrame
    {
        public string ResponseCode { get; set; }
        public string AuthorizationCode { get; set; }
        public string TypeOfCard { get; set; }
        public string AccountNumber { get; set; }
        public string Last4DigitsCard { get; set; }
        public string Description { get; set; }
        public string ApprovalCode { get; set; }
        public string TerminalId { get; set; }
        public string MerchantId { get; set; }
        public string CardIssuerName { get; set; }
        public string CardNo { get; set; }
        public string CardExpiry { get; set; }
        public string BatchNo { get; set; }
        public string ReferenceNo { get; set; }
        public string TransactionNo { get; set; }
    }
}



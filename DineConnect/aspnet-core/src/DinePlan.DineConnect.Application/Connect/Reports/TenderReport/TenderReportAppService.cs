﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TenderReport.Dto;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Reports.TenderReport.Exporting;

namespace DinePlan.DineConnect.Connect.Reports.TenderReport
{
    public class TenderReportAppService : DineConnectReportAppServiceBase, ITenderReportAppService
    {
        private readonly ITenderReportExcelExporter _exporter;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payTypeRe;
        public TenderReportAppService(IRepository<Master.PaymentTypes.PaymentType> payTypeRe ,ITenderReportExcelExporter exporter,ILocationAppService locService, IRepository<Ticket> ticketRepo, IUnitOfWorkManager unitOfWorkManager, IRepository<Department> departmentRepo) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _exporter = exporter;
            _payTypeRe = payTypeRe;
        }

        public async Task<PagedResultDto<TenderDetail>> BuildTenderDetails(GetTicketInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var tenderViewDtos = await BuildTenderDetailsForBackground(input);

                var result = tenderViewDtos.SelectMany(c => c.TenderDetails).ToList();

                var countTotal = result.Count();
                if (input.Sorting == "Id") input.Sorting = "TenderCode";
                var resultPage = result.AsQueryable()
                    .OrderBy(s=>s.TenderCode)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<TenderDetail>(countTotal, resultPage);
            }
        }

        public async Task<FileDto> BuildTenderDetailsExport(GetTicketInput input)
        {
            return await _exporter.ExportToFileTenderDetail(input, this);
        }

        public async Task<List<TenderViewDto>> BuildTenderDetailsForBackground(GetTicketInput input)
        {
            var allReturnList = new List<TenderViewDto>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var currentTickets = GetAllTickets(input)
                    .Where(a => a.TotalAmount > 0M);

                var allPayments = currentTickets.SelectMany(x => x.Payments);
                if (input.Payments != null && input.Payments.Any())
                {
                    allPayments = allPayments.Where(s => input.Payments.Contains(s.PaymentTypeId));
                }
                foreach(var pay in allPayments)
                {
                    pay.PaymentType = _payTypeRe.FirstOrDefault(s => s.Id == pay.PaymentTypeId);
                }    
                var paymentGroup = from d in allPayments.ToList()
                                   group d by new { d.PaymentTypeId }
                                   into g
                                   select new { PaymentTypeIdKey = g.Key.PaymentTypeId, Items = g.ToList() };
                
                foreach (var pay in paymentGroup)
                {
                    var allTicke = currentTickets
                        .Where(t => t.Payments.Any(x => x.PaymentTypeId == pay.PaymentTypeIdKey));

                    var tenderView = new TenderViewDto
                    {
                        PaymentType = pay.Items.FirstOrDefault()?.PaymentType
                    };
                    allReturnList.Add(tenderView);
                    foreach (var c in allTicke)
                    {
                        var payments = c.Payments.Where(a => a.PaymentTypeId == pay.PaymentTypeIdKey);

                        if (payments != null)
                        {
                            var paymentDic = new Dictionary<string, List<Connect.Transaction.Payment>>();
                            foreach (var payment in payments)
                            {
                                if (!string.IsNullOrEmpty(payment.PaymentTags))
                                {
                                    try
                                    {
                                        var conFrame =
                                            JsonConvert.DeserializeObject<PaymentFrame>(payment.PaymentTags);
                                        if (conFrame != null)
                                        {
                                            if (!paymentDic.ContainsKey(conFrame.Description))
                                            {
                                                paymentDic.Add(conFrame.Description, new List<Connect.Transaction.Payment>());
                                            }
                                            paymentDic[conFrame.Description].Add(payment);
                                        }
                                        else if (!paymentDic.ContainsKey(""))
                                        {
                                            paymentDic.Add("", new List<Connect.Transaction.Payment>() { payment });
                                        }
                                        else
                                        {
                                            paymentDic[""].Add(payment);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //
                                    }
                                }
                                else
                                {
                                    if (!paymentDic.ContainsKey(""))
                                    {
                                        paymentDic.Add("", new List<Connect.Transaction.Payment>() { payment });
                                    }
                                    else
                                    {
                                        paymentDic[""].Add(payment);
                                    }
                                }

                            }
                            foreach (var payment in paymentDic)
                            {
                                var detail = new TenderDetail
                                {
                                    DateTime = c.LastPaymentTime,
                                    TicketNo = c.TicketNumber,
                                    Terminal = c.TerminalName,
                                    InvoiceNo = c.InvoiceNo,
                                    Table = "",
                                    Employee = payment.Value.Last().PaymentUserName,
                                    CardNumber = "",
                                    ReferenceNo = "",
                                    Discount = 0M,
                                    ServiceCharge = 0,
                                    Vat = payment.Value.Sum(a => a.Amount) * 7 / 107,
                                    Total = payment.Value.Sum(a => a.Amount),
                                    TenderCode = tenderView.PaymentType?.AccountCode,
                                    TenderName = tenderView.PaymentType?.Name
                                };

                                if (!string.IsNullOrEmpty(c.TicketEntities))
                                {
                                    var allEntities =
                                        JsonConvert.DeserializeObject<IList<TicketEntityMap>>(c.TicketEntities);
                                    var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);

                                    if (lastEntity != null) detail.Table = lastEntity.EntityName;
                                }

                                if (!string.IsNullOrEmpty(c.TicketPromotionDetails))
                                {
                                    var allPromoDetails = JsonConvert
                                        .DeserializeObject<List<Dtos.PromotionDetailValue>>(c.TicketPromotionDetails);

                                    if (allPromoDetails.Any())
                                    {
                                        detail.Discount = allPromoDetails.Sum(a => a.PromotionAmount);
                                        ;
                                    }
                                }

                                if (!string.IsNullOrEmpty(payment.Key))
                                    try
                                    {
                                        var conFrame =
                                            JsonConvert.DeserializeObject<PaymentFrame>(payment.Value.Last().PaymentTags);
                                        if (conFrame != null)
                                        {
                                            detail.CardNumber = conFrame.Last4DigitsCard;
                                            detail.ReferenceNo = conFrame.Description;
                                        }

                                    }
                                    catch (Exception)
                                    {
                                        //
                                    }

                                tenderView.TenderDetails.Add(detail);
                            }


                        }
                        // filter by dynamic filter
                    }

                    var dataAsQueryable = tenderView.TenderDetails.AsQueryable();
                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }

                    tenderView.TenderDetails = dataAsQueryable.ToList();
                }
            }

            return allReturnList;
        }
    }
}

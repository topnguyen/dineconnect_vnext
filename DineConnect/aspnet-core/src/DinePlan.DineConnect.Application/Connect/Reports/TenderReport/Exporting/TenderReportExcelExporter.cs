﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TenderReport.Exporting
{
    public class TenderReportExcelExporter : EpPlusExcelExporterBase, ITenderReportExcelExporter
    {
        private readonly IRepository<Location> _locRepo;
        public TenderReportExcelExporter(IRepository<Location> locRepo,ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _locRepo = locRepo;
        }

        public async Task<FileDto> ExportToFileTenderDetail(GetTicketInput input, ITenderReportAppService connectReportAppService)
        {
            var file = new FileDto(
                "Tender_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Discount"));
                sheet.OutLineApplyStyle = true;
                var rowHeader = AddReportHeader(sheet, "Tender Report", input);
                input.MaxResultCount = 1000000000;
                var tenderItems = await connectReportAppService.BuildTenderDetailsForBackground(input);

                var row = rowHeader;
                row++;
                foreach (var tender in tenderItems)
                {
                    var colCount = 1;
                    row += 2;
                    sheet.Cells[row, 1, row, 12].Merge = true;
                    sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                    sheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
                    sheet.Cells[row, colCount].Value = "Settled Under " + tender.PaymentTypeName;
                    var headers = new List<string>
                    {
                        L("Date"),
                        L("Time"),
                        L("TicketNo"),
                        L("InvoiceNo"),
                        L("Terminal"),

                        L("Table"),
                        L("Employee"),
                        L("CardNumber"),
                        L("ReferenceNo"),
                        L("SubTotal"),
                        L("Discount"),
                        L("ServiceCharge"),
                        L("Vat"),
                        L("Total")
                    };

                    row++;
                    AddHeader(sheet, row, headers.ToArray());
                    foreach (var item in tender.TenderDetails)
                    {
                        row++;
                        colCount = 1;
                        sheet.Cells[row, colCount++].Value = item.DateTime.ToString("dd/MM/yyyy");
                        sheet.Cells[row, colCount++].Value = item.DateTime.ToString(_timeFormat);
                        sheet.Cells[row, colCount++].Value = item.TicketNo;
                        sheet.Cells[row, colCount++].Value = item.InvoiceNo;
                        sheet.Cells[row, colCount++].Value = item.Terminal;

                        sheet.Cells[row, colCount++].Value = item.Table;
                        sheet.Cells[row, colCount++].Value = item.Employee;
                        sheet.Cells[row, colCount++].Value = item.CardNumber;
                        sheet.Cells[row, colCount++].Value = item.ReferenceNo;
                        sheet.Cells[row, colCount++].Value = Math.Round(item.SubTotal, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value = Math.Round(item.Discount, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value = Math.Round(item.ServiceCharge, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value =
                            Math.Round(item.Vat, _roundDecimals, MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value =
                            Math.Round(item.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                    }

                    row++;
                    sheet.Cells[row, 1].Value = L("Total");
                    sheet.Cells[row, 10].Value = Math.Round(tender.TenderDetails.Sum(t => t.SubTotal), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 11].Value = Math.Round(tender.TenderDetails.Sum(t => t.Discount), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 12].Value = Math.Round(tender.TenderDetails.Sum(t => t.ServiceCharge),
                        _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 13].Value = Math.Round(tender.TenderDetails.Sum(t => t.Vat), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 14].Value = Math.Round(tender.TenderDetails.Sum(t => t.Total), _roundDecimals,
                        MidpointRounding.AwayFromZero);

                    sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                }

                for (var i = 1; i <= 12; i++) sheet.Column(i).AutoFit();
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";
            var isAllLocation =
                input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() +
                input.LocationGroup?.LocationTags?.Count() == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
            }

            Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null) myLocation = _locRepo.Get(simpleL.Id);
            }

            if (myLocation != null)
            {
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;
            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString("dd/MM/yyyy") + " to " +
                                      input.EndDate.ToString("dd/MM/yyyy");
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString("dd/MM/yyyy");

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }

    }
}

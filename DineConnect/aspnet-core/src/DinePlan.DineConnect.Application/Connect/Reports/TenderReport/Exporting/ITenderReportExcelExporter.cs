﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TenderReport.Exporting
{
    public interface ITenderReportExcelExporter
    {
        Task<FileDto> ExportToFileTenderDetail(GetTicketInput input, ITenderReportAppService connectReportAppService);
    }
}

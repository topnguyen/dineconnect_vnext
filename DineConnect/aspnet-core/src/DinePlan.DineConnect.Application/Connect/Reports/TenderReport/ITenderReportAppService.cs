﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TenderReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TenderReport
{
    public interface ITenderReportAppService: IApplicationService
    {
        Task<PagedResultDto<TenderDetail>> BuildTenderDetails(GetTicketInput input);
        Task<List<TenderViewDto>> BuildTenderDetailsForBackground(GetTicketInput input);
        Task<FileDto> BuildTenderDetailsExport(GetTicketInput input);
    }
}

﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.GroupReports.Exporting
{
	public interface IGroupReportExcelExporter
	{
		Task<FileDto> ExportToFileGroup(GetItemInput input, IGroupReportReportAppService appService);
	}
}

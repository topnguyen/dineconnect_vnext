﻿using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.GroupReports.Exporting
{
	public class GroupReportExcelExporter : EpPlusExcelExporterBase, IGroupReportExcelExporter
	{
		private readonly ITenantSettingsAppService _tenantSettingsAppService;
		public GroupReportExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
		{
			_tenantSettingsAppService = tenantSettingsAppService;
		}

        public async Task<FileDto> ExportToFileGroup(GetItemInput input, IGroupReportReportAppService appService)
        {
            var file = new FileDto("Group-" + input.StartDate.ToString("yyyy-MM-dd") + "-" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
             MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Group"),
                    L("Quantity"),
                    L("Total")
                );

                var rowCount = 2;
				var output = await appService.BuildGroupSalesReport(input);
				if (output?.GroupList?.Items != null)
                {
                    var groups = output.GroupList.Items.ToList();
                    AddObjects(
                        sheet, rowCount, groups,
                        _ => _.GroupName,
                        _ => _.Quantity,
                        _ => _.Total
                    );

                    var totalRow = output.GroupList.Items.Count() + 2;

                    sheet.Cells[totalRow, 1, totalRow, 5].Style.Font.Bold = true;
                    sheet.Cells[totalRow, 1].Value = "Total";
                    sheet.Cells[totalRow, 2].Value = groups.Select(g => g.Quantity).Sum();
                    sheet.Cells[totalRow, 3].Value = groups.Select(g => g.Total).Sum();

                    input.SkipCount = input.SkipCount + input.MaxResultCount;
                }

                for (var i = 1; i <= 3; i++) sheet.Column(i).AutoFit();

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);

        }
	}
}

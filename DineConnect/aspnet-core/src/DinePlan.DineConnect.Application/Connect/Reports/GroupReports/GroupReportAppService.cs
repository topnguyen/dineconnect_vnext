﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.GroupReports.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.GroupReports
{
    
    public class GroupReportAppService : DineConnectReportAppServiceBase, IGroupReportReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<MenuItem> _mrRepo;
        private readonly IRepository<Promotion> _promoRepo;
        private readonly IRepository<MenuItemPortion> _menuItemPromotionRepository;
        private IGroupReportExcelExporter _groupReportExcelExporter;

        public GroupReportAppService(ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Location> lRepo
            , IRepository<MenuItem> mrRepo
            , IRepository<Promotion> promoRepo
            , IRepository<MenuItemPortion> menuItemPromotionRepository,
            IGroupReportExcelExporter groupReportExcelExporter
            )
            : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _lRepo = lRepo;
            _mrRepo = mrRepo;
            _promoRepo = promoRepo;
            _menuItemPromotionRepository = menuItemPromotionRepository;
            _groupReportExcelExporter = groupReportExcelExporter;
        }

        public async Task<GroupStatsDto> BuildGroupSalesReport(GetItemInput input)
        {
            var returnOutput = new GroupStatsDto();
            var groups = new List<string>();
            var chartOutput = new List<BarChartOutputDto>();
            var oDto = new List<GroupReportDto>();
            var returnOrders = GetAllOrdersNoDynamicFilter(input);

            // Exclude void order
            if (!input.VoidParam)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Void"));
            }

            if (!input.Gift)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || !x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Gift"));
            }

            if (!input.Comp)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || !x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Comp"));
            }

            if (!input.Refund)
            {
                returnOrders = returnOrders.Where(x =>
                                    x.CalculatePrice
                                    || x.IncreaseInventory
                                    || x.DecreaseInventory
                                    || string.IsNullOrEmpty(x.OrderStates)
                                    || !x.OrderStates.Contains("Refund"));
            }

            if (returnOrders.Any())
            {
                var myItems = returnOrders.ToList().GroupBy(a => a.MenuItemId);
                foreach (var myItem in myItems)
                {
                    var menuItem = await _mrRepo.GetAll()
                        .Include(x => x.Category)
                        .ThenInclude(x => x.ProductGroup)
                        .FirstOrDefaultAsync(x => x.Id == myItem.Key);
                    var myCategory = oDto.LastOrDefault(a => a.GroupId == menuItem.Category.ProductGroupId);

                    if (myCategory == null)
                    {
                        myCategory = new GroupReportDto
                        {
                            GroupId = menuItem?.Category?.ProductGroupId ?? 0,
                            GroupName = menuItem?.Category?.ProductGroup?.Name,
                            Quantity = myItem.Sum(a => a.Quantity),
                            Total = myItem.Sum(a => a.GetTotal())
                        };
                        oDto.Add(myCategory);
                    }
                    else
                    {
                        myCategory.Quantity += myItem.Sum(a => a.Quantity);
                        myCategory.Total += myItem.Sum(a => a.GetTotal());
                    }
                    myCategory.MenuItems.Add(new MenuReportView
                    {
                        AliasCode = menuItem.AliasCode,
                        MenuItemName = menuItem.Name,
                        Quantity = myItem.Sum(a => a.Quantity),
                        Price = myItem.Sum(a => a.Quantity * a.Price)
                    });
                }
            }

            // filter by dynamic filter
            var dataAsQueryable = oDto.AsQueryable();

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
            }

            oDto = dataAsQueryable.ToList();
            var menuItemCount = oDto.Count();
            var items = oDto.AsQueryable().PageBy(input).ToList();
            returnOutput.GroupList = new PagedResultDto<GroupReportDto>(
                menuItemCount,
                items
            );

            groups = oDto.Select(x => x.GroupName).ToList();

            foreach (var item in oDto)
                chartOutput.Add(new BarChartOutputDto
                {
                    name = item.GroupName,
                    data = new List<decimal> { item.Total }
                });

            returnOutput.Groups = groups;
            returnOutput.ChartOutput = chartOutput;
            return await Task.FromResult(returnOutput);
        }

        public async Task<FileDto> BuildGroupExcel(GetItemInput input)
        {
            input.MaxResultCount = 10000;
            var output = await _groupReportExcelExporter.ExportToFileGroup(input, this);
            return output;
        }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Web.Http;

namespace DinePlan.DineConnect.Connect.Reports.OrderReports
{
    
    public class OrderReportAppService : DineConnectReportAppServiceBase, IOrderReportAppService
    {
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Transaction.Payment> _paymentRepo;
        private readonly IOrderReportExcelExporter _orderReportExcelExporter;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<Department> _departmentRepo;

        public OrderReportAppService(
            IRepository<Location> locationRepo,
            IRepository<Transaction.Payment> paymentRepo,
            IOrderReportExcelExporter OrderReportExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            ITenantSettingsAppService tenantSettingsAppService,
            ILocationAppService locService,
            IRepository<Ticket> ticketRepo,
            IRepository<Department> departmentRepo

        ) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _locationRepo = locationRepo;
            _paymentRepo = paymentRepo;
            _orderReportExcelExporter = OrderReportExcelExporter;
            _tenantSettingsAppService = tenantSettingsAppService;
            _departmentRepo = departmentRepo;
        }

        [HttpPost]
        public async Task<OrderStatsDto> BuildOrderReport(GetItemInput input)
        {
            var returnOutput = new OrderStatsDto();

            var orders = GetAllOrders(input);

            var outputDtos = new List<OrderListDto>();

            List<OrderViewDto> orderViewDtos = null;

            if (!string.IsNullOrEmpty(input.OutputType) && input.OutputType.Equals("EXPORT"))
            {
                var sortTickets = await orders.OrderBy(input.Sorting)
                .ToListAsync();

                outputDtos = ObjectMapper.Map<List<OrderListDto>>(sortTickets);
            }
            else
            {
                var sortTickets = await orders.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
                orderViewDtos = ObjectMapper.Map<List<OrderViewDto>>(sortTickets);
            }

            var menuItemCount = await orders.CountAsync();

            returnOutput.OrderList = new PagedResultDto<OrderListDto>(
                menuItemCount,
                outputDtos
            );

            returnOutput.OrderViewList = new PagedResultDto<OrderViewDto>(
                menuItemCount,
                orderViewDtos
            );

            returnOutput.DashBoardDto = GetStatics(orders);

            return returnOutput;
        }

        [HttpPost]
        public async Task<FileDto> BuildOrderReportExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var orders = GetAllOrders(input);
                var sortTickets = await orders.OrderBy(input.Sorting)
                .ToListAsync();

                var outputDtos = ObjectMapper.Map<List<OrderListDto>>(sortTickets);

                if (!input.RunInBackground)
                {
                    return await _orderReportExcelExporter.ExportOrders(input, outputDtos);
                }
            }

            return null;
        }

        private DashboardOrderDto GetStatics(IQueryable<Order> orders)
        {
            var orderDto = new DashboardOrderDto();

            try
            {
                if (!orders.Any())
                {
                    return orderDto;
                }

                orderDto.TotalAmount = orders.DefaultIfEmpty().Sum(a => a.Price * a.Quantity);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
            }

            return orderDto;
        }
    }
}
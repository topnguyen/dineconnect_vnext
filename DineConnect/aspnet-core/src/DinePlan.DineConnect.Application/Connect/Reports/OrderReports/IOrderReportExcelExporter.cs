﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.OrderReports
{
    public interface IOrderReportExcelExporter
    {
        Task<FileDto> ExportOrders(GetItemInput input, List<OrderListDto> listDtos);
    }
}
﻿using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.Connect.Reports.OrderReports.Exporting
{
    public class OrderReportExcelExporter : NpoiExcelExporterBase, IOrderReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public OrderReportExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportOrders(GetItemInput input, List<OrderListDto> listDtos)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var startDate = !input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var endDate = !input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var fileName = $"Orders-{startDate}-{endDate}.{(input.ExportOutputType == ExportType.Excel ? "xlsx" : "html")}";


            if (input.VoidParam)
            {
                return await ExportVoidOrders(fileName, listDtos, input);
            }

            return await ExportOrdersSummary(fileName, listDtos, input);
        }

        private Task<FileDto> ExportVoidOrders(string fileName, List<OrderListDto> orders, GetItemInput input)
        {
            var headers = new List<string>
            {
                L("Date"),
                L("Time"),
                L("TicketNumber"),
                L("Department"),
                L("Table"),
                L("MenuName"),
                L("Qty"),
                L("Note"),
                L("OrderUser"),
                L("VoidUser"),
                L("Terminal"),
                L("SubTotal")
            };

            return Task.FromResult(CreateExcelPackage(
                fileName, excelPackage =>
               {
                   var sheet = excelPackage.CreateSheet(L("VoidOrders"));

                   AddHeader(sheet, headers.ToArray());

                   var rowIndex = 2;

                   foreach (var order in orders)
                   {
                       var values = new List<object>
                               {
                                    order.OrderCreatedTime.ToString("dd-MM-yyyy"),
                                    order.OrderCreatedTime.ToString("HH:mm:ss"),
                                    order.TicketNumber,
                                    order.DepartmentName,
                                    string.Empty,
                                    order.MenuItemName,
                                    order.Quantity,
                                    order.Note,
                                    order.CreatingUserName,
                                    order.OperationUser,
                                    order.TerminalName,
                                    Math.Round(order.LineTotal, 2, MidpointRounding.AwayFromZero),
                               };

                       // Add Row to excel

                       AddRow(excelPackage, sheet, rowIndex, values.ToArray());

                       // Next Row
                       rowIndex++;
                   }

                   // Total Row
                   sheet.GetSheetRow(rowIndex).GetSheetCell(0).SetCellValue("Total");

                   // col 7 sum
                   var col7SumData = orders.Sum(o => o.Quantity);
                   sheet.GetSheetRow(rowIndex).GetSheetCell(6).SetCellValue(col7SumData.ToString());

                   // col 12 sum
                   var col12SumData = orders.Sum(o => o.LineTotal);
                   var myCell = sheet.GetSheetRow(rowIndex).GetSheetCell(11);
                   myCell.SetCellValue(col12SumData.ToString());

                   ICellStyle styleMiddle2 = excelPackage.CreateCellStyle();
                   styleMiddle2.Alignment = HorizontalAlignment.Right;
                   styleMiddle2.VerticalAlignment = VerticalAlignment.Center;
                   styleMiddle2.WrapText = true; //wrap the text in the cell
                   myCell.CellStyle = styleMiddle2;

                   for (var i = 1; i <= headers.Count; i++)
                   {
                       sheet.AutoSizeColumn(i);
                   }
               }, input.ExportOutputType));
        }

        private Task<FileDto> ExportOrdersSummary(string fileName, List<OrderListDto> orders, GetItemInput input)
        {
            var headers = new List<string>
            {
                L("Location"),
                L("TicketId"),
                L("Order"),
                L("MenuItem"),
                L("TagValue"),
                L("Portion"),
                L("Quantity"),
                L("Price"),
                L("LineTotal"),
                L("TaxPrice"),
                L("User"),
                L("Date"),
                L("Time"),
                L("Department"),
                L("Note")
            };

            bool addOperationUser = false;

            if (input.VoidParam || input.Comp || input.Gift)
            {
                addOperationUser = true;
                headers.Add(L("OperationUser"));
            }

            var allQuan = 0M;
            var allTot = 0M;

            return Task.FromResult(CreateExcelPackage(
                fileName, excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Orders"));

                    AddHeader(sheet, headers.ToArray());

                    var rowIndex = 2;

                    foreach (var order in orders)
                    {
                        allQuan += order.Quantity;
                        var total = order.Price * order.Quantity;
                        allTot += total;

                        var values = new List<object>
                               {
                                    order.LocationName,
                                    order.TicketId,
                                    order.OrderNumber,
                                    order.MenuItemName,
                                    string.Empty,
                                    order.PortionName,
                                    order.Quantity,
                                    order.Price,
                                    order.Quantity * order.Price,
                                    order.TaxPrice,
                                    order.CreatingUserName,
                                    order.OrderCreatedTime.ToString("dd-MM-yyyy"),
                                    order.OrderCreatedTime.ToString("HH:mm:ss"),
                                    order.DepartmentName,
                                    order.Note,
                               };

                        if (addOperationUser)
                        {
                            values.Add(order.OperationUser);
                        }

                        // Add Row to excel
                        AddRow(excelPackage, sheet, rowIndex, values.ToArray());

                        // Next Row
                        rowIndex++;

                        foreach (var totd in order.TransactionOrderTags)
                        {
                            allQuan += order.Quantity;
                            total = totd.Price * order.Quantity;
                            allTot += total;

                            var transactionValues = new List<object>
                            {
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                string.Empty,
                                totd.TagValue,
                                string.Empty,
                                order.Quantity,
                                totd.Price,
                                order.Quantity * totd.Price,
                                string.Empty,
                                string.Empty
                            };

                            // Add Row to excel
                            AddRow(excelPackage, sheet, rowIndex, transactionValues.ToArray());

                            // Next Row
                            rowIndex++;
                        }
                    }

                    // Total Row
                    sheet.GetSheetRow(rowIndex).GetSheetCell(1).SetCellValue("Total");
                    sheet.GetSheetRow(rowIndex).GetSheetCell(7).SetCellValue((double)allQuan);
                    sheet.GetSheetRow(rowIndex).GetSheetCell(9).SetCellValue((double)allTot);

                    for (var i = 1; i <= headers.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                }, input.ExportOutputType));
        }
    }
}
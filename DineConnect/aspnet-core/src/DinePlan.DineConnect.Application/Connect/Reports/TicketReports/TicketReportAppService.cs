﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.TicketReports
{
    
    public class TicketReportAppService : DineConnectReportAppServiceBase, ITicketReportAppService
    {
        private readonly IRepository<Master.PaymentTypes.PaymentType> _paymentTypeRepo;
        private readonly IRepository<TicketTagGroup> _ticketGroupRepo;
        private readonly IRepository<Ticket> _ticketRepo;
        private readonly ITicketReportExcelExporter _ticketReportExcelExporter;
        private readonly IRepository<TransactionType> _tractionTypeRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locService;

        public TicketReportAppService(
            IRepository<Ticket> ticketRepo,
            IRepository<TransactionType> tractionTypeRepo,
            IRepository<Master.PaymentTypes.PaymentType> paymentTypeRepo,
            IRepository<TicketTagGroup> ticketGroupRepo,
            ITicketReportExcelExporter ticketReportExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            ILocationAppService locService,
            IRepository<Department> departmentRepo
        ) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _ticketRepo = ticketRepo;
            _tractionTypeRepo = tractionTypeRepo;
            _paymentTypeRepo = paymentTypeRepo;
            _ticketGroupRepo = ticketGroupRepo;
            _ticketReportExcelExporter = ticketReportExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
        }

        public async Task<TicketStatsDto> GetTickets(GetTicketInput input, bool onlystatus = false)
        {
            var tickets = GetAllTicketsForTicketInputQueryable(input);

            var returnOutput = new TicketStatsDto();

            if (!onlystatus)
            {
                var outputDtos = new List<TicketListDto>();
                var outputViewDtos = new List<TicketViewDto>();

                var sortTickets = await tickets.OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var menuItemCount = await tickets.CountAsync();

                if (!string.IsNullOrEmpty(input.OutputType) && input.OutputType.Equals("EXPORT"))
                {
                    sortTickets = await tickets.OrderBy(input.Sorting)
                    .ToListAsync();
                    outputDtos = ObjectMapper.Map<List<TicketListDto>>(sortTickets);
                }
                else
                {
                    outputViewDtos = ObjectMapper.Map<List<TicketViewDto>>(sortTickets);
                }

                returnOutput.TicketList = new PagedResultDto<TicketListDto>(menuItemCount, outputDtos);
                returnOutput.TicketViewList = new PagedResultDto<TicketViewDto>(menuItemCount, outputViewDtos);
            }

            returnOutput.DashBoardDto = GetStatics(tickets);

            return returnOutput;
        }

        public async Task<List<TicketListDto>> GetAllTicketsForTicketInput(GetTicketInput input)
        {
            var tickets = await GetAllTicketsForTicketInputQueryable(input).ToListAsync();

            var outPut = ObjectMapper.Map<List<TicketListDto>>(tickets);

            return outPut;
        }

        public async Task<FileDto> GetAllToExcel(GetTicketInput input)
        {
            List<string> transactionItems;
            List<string> paymentTypeItems;
            List<string> ticketGroupItems;

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                transactionItems = await _tractionTypeRepo.GetAll().Select(a => a.Name).ToListAsync();
                paymentTypeItems = await _paymentTypeRepo.GetAll().Select(a => a.Name).ToListAsync();
                ticketGroupItems = await _ticketGroupRepo.GetAll().Select(a => a.Name).ToListAsync();
            }
            input.OutputType = "EXPORT";

            var output = await _ticketReportExcelExporter.ExportTickets(this, input, transactionItems, paymentTypeItems,
                ticketGroupItems, AbpSession.TenantId.Value);

            return output;
        }

        #region Queryable

        public IQueryable<Ticket> GetAllTicketsForTicketInputQueryable(GetTicketInput input)
        {
            var tickets = GetAllTickets(input)
                .Include(t => t.Payments).ThenInclude(x => x.PaymentType)
                .Include(t => t.Transactions).ThenInclude(x => x.TransactionType)
                .AsQueryable(); ;

            if (input.Payments != null && input.Payments.Any())
                tickets = tickets.Include(a => a.Payments)
                    .WhereIf(input.Payments.Any(),
                        p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

            if (input.Transactions != null && input.Transactions.Any())
            {
                var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                tickets = from p in tickets.Include(a => a.Transactions)
                          where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                          select p;
            }

            if (!string.IsNullOrEmpty(input.TerminalName))
                tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

            if (!string.IsNullOrEmpty(input.Department))
                tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
            if (input.Departments != null && input.Departments.Any())
            {
                var allDepartments = input.Departments.Select(a => a.DisplayText).ToList();
                tickets = tickets.Where(a =>
                    a.DepartmentName != null &&
                    allDepartments.Contains(a.DepartmentName));
            }

            if (input.TicketTags != null && input.TicketTags.Any())
            {
                var allTags = input.TicketTags.Select(a => a.DisplayText).ToArray();
                tickets = from c in tickets
                          where allTags.Any(i => c.TicketTags.Contains(i))
                          select c;
            }

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

            if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

            return tickets;
        }

        private DashboardTicketDto GetStatics(IQueryable<Ticket> allTickets)
        {
            var ticketDto = new DashboardTicketDto();

            if (!allTickets.Any()) return ticketDto;
            try
            {
                ticketDto.TotalTicketCount = allTickets.Count();
                ticketDto.TotalAmount = allTickets.DefaultIfEmpty().Sum(a => a.TotalAmount);
                ticketDto.AverageTicketAmount = ticketDto.TotalAmount / ticketDto.TotalTicketCount;
                ticketDto.TotalOrderCount = allTickets.SelectMany(s=>s.Orders).Count();
                var myOrders = allTickets.SelectMany(a => a.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory);
                ticketDto.TotalItemSold = myOrders.Sum(b => b.Quantity);
            }
            catch (Exception)
            {
                // ignored
            }

            return ticketDto;
        }

        #endregion Queryable
    }
}
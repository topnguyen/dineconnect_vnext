﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;

namespace DinePlan.DineConnect.Connect.Reports.TicketReports.Exporting
{
    public class TicketReportExcelExporter : EpPlusExcelExporterBase, ITicketReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<Location> _locRepo;
        private readonly IRepository<Brand> _comRepo;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TicketReportExcelExporter(ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService,
            ITimeZoneConverter timeZoneConverter,
            IRepository<Location> locRepo,
            IAbpSession abpSession,

            IRepository<Brand> comRepo
            ) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _locRepo = locRepo;
            _timeZoneConverter = timeZoneConverter;
            _comRepo = comRepo;
            _abpSession = abpSession;
        }

        public async Task<FileDto> ExportTickets(ITicketReportAppService ticketReportAppService, GetTicketInput input, List<string> transactionTypes, List<string> paymentTypes, List<string> ticketGroups, int tenantId)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var startDate = !input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var endDate = !input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var fileName = $"Tickets-{startDate}-{endDate}";

            if (input.Refund)
            {
                return await ExportRefunds(fileName, ticketReportAppService, input, transactionTypes, paymentTypes, ticketGroups, tenantId);
            }

            return await ExportTickets(fileName, ticketReportAppService, input, transactionTypes, paymentTypes, ticketGroups, tenantId);
        }

        private async Task<FileDto> ExportTickets(string fileName, ITicketReportAppService ticketReportAppService, GetTicketInput input, List<string> transactionTypes, List<string> paymentTypes, List<string> ticketGroups, int tenantId)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
               ? allSettings.FileDateTimeFormat.FileDateTimeFormat
               : AppConsts.FileDateTimeFormat;

            var file = new FileDto(fileName + ".xlsx",
              MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var headers = new List<string>
            {
                L("Location"),
                L("Id"),
                L("TicketNo"),
                L("RefTicket"),
                L("InvoiceNo"),
                L("Covers"),
                L("RefNo"),
                L("Credit"),
                L("TicketCreatedTime"),
                L("LastOrderTime"),
                L("LastPaymentTime"),
                L("DepartmentName"),
                L("Note"),
                L("LastModifiedUserName"),
                L("TerminalName"),
                L("TaxIncluded"),
                L("TotalAmount")
            };

            headers.AddRange(paymentTypes);

            headers.AddRange(transactionTypes);

            var tickets = await ticketReportAppService.GetAllTicketsForTicketInput(input);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));

                int myRerowCount = AddReportHeader(sheet, "Tickets Report", input);
                myRerowCount++;

                AddHeader(sheet, myRerowCount, headers.ToArray());

                myRerowCount++;

                foreach (var ticket in tickets)
                {
                    var values = new List<string>
                        {
                            ticket.LocationName,
                            ticket.Id.ToString(),
                            ticket.TicketNumber,
                            ticket.ReferenceTicket,
                            ticket.InvoiceNo,
                            ticket.Covers,
                            ticket.ReferenceNumber,
                            ticket.Credit.ToString(),
                            ticket.TicketCreatedTime.ToString(fileDateTimeSetting),
                            ticket.LastOrderTime.ToString(fileDateTimeSetting),
                            ticket.LastPaymentTime.ToString(fileDateTimeSetting),
                            ticket.DepartmentName,
                            ticket.Note,
                            ticket.LastModifiedUserName,
                            ticket.TerminalName,
                            ticket.TaxIncluded.ToString(),
                            ticket.TotalAmount.ToString()
                        };

                    foreach (var paymentType in paymentTypes)
                    {
                        var payment = ticket.Payments.Where(a => a.PaymentTypeName.Equals(paymentType));

                        var paymentTotalAmount = payment.Select(a => a.Amount).Sum();

                        values.Add(paymentTotalAmount.ToString());
                    }

                    foreach (var transactionType in transactionTypes)
                    {
                        var transaction = ticket.Transactions.Where(a => a.TransactionTypeName != null && a.TransactionTypeName.Equals(transactionType));

                        var transactionTotalAmount = transaction.Select(a => a.Amount).Sum();

                        values.Add(transactionTotalAmount.ToString());
                    }

                    // Add Row to excel

                    AddRow(sheet, myRerowCount, values.ToArray());

                    // Next Row
                    myRerowCount++;
                }

                for (var i = 1; i <= 17; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private async Task<FileDto> ExportRefunds(string fileName, ITicketReportAppService ticketReportAppService, GetTicketInput input, List<string> transactionTypes, List<string> paymentTypes, List<string> ticketGroups, int tenantId)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var file = new FileDto(fileName + ".xlsx",
             MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var ticketData = await ticketReportAppService.GetTickets(input);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("RefundTickets"));

                int myRerowCount = AddReportHeader(sheet, "Tickets Report", input);
                myRerowCount++;

                AddHeader(
                    sheet,
                    myRerowCount,
                    L("Date"),
                    L("Location"),
                    L("TicketNo"),
                    L("InvoiceNo"),
                    L("Department"),
                    L("Note"),
                    L("Employee"),
                    L("Terminal"),
                    L("TotalAmount"),
                    L("Payments")
                );

                myRerowCount++;

                AddObjects(
                    sheet, myRerowCount, ticketData.TicketList.Items.ToList(),
                    _ => _.LastPaymentTime.ToString(fileDateSetting),
                    _ => _.LocationName,
                    _ => _.TicketNumber,
                    _ => _.InvoiceNo,
                    _ => _.DepartmentName,
                    _ => _.Note,
                    _ => _.LastModifiedUserName,
                    _ => _.TerminalName,
                    _ =>
                    {
                        var lastRefund = _.TicketTagValues.LastOrDefault(a => a.TagName.Equals("Refund"));
                        if (lastRefund != null)
                        {
                            return lastRefund.TagValue;
                        }

                        return string.Empty;
                    },
                    _ =>
                    {
                        var lastRefund = _.TicketTagValues.LastOrDefault(a => a.TagName.Equals("CancelPayments"));
                        if (lastRefund != null)
                        {
                            return lastRefund.TagValue.Replace("\u000d\u000a", Environment.NewLine);
                        }
                        return string.Empty;
                    }
                );

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        protected int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";

            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                Brand myCompany = _comRepo.Get(myLocation.OrganizationId);
                if (myCompany != null)
                {
                    brandName = myCompany.Name;
                }
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }
            var row = 1;
            sheet.Cells[row, 1].Value = nameOfReport;
            sheet.Cells[row, 1, 1, 12].Merge = true;
            sheet.Cells[row, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[row, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            row++;
            sheet.Cells[row, 1].Value = L("Brand");
            sheet.Cells[row, 2].Value = brandName;

            sheet.Cells[row, 6].Value = "Business Date:";
            sheet.Cells[row, 7].Value = input.StartDate.ToString(_simpleDateFormat) + " to " + input.EndDate.ToString(_simpleDateFormat);

            row++;
            sheet.Cells[row, 1].Value = "Branch Name:";
            sheet.Cells[row, 2].Value = branch;
            sheet.Cells[row, 6].Value = "Printed On:";
            sheet.Cells[row, 7].Value = DateTime.Now.ToString(_dateTimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;
            row++;

            return row;
        }
    }
}
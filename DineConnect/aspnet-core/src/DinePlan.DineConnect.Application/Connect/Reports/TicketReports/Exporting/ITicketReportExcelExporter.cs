﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.TicketReports
{
  public interface ITicketReportExcelExporter
    {
        Task<FileDto> ExportTickets(ITicketReportAppService ticketReportAppService, GetTicketInput input, List<string> transactionTypes, List<string> paymentTypes, List<string> ticketGroups, int tenantId);
    }
}

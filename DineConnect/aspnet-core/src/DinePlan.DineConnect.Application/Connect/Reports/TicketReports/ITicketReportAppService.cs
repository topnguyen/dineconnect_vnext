﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports.TicketReports
{
	public interface ITicketReportAppService : IApplicationService
	{
		Task<TicketStatsDto> GetTickets(GetTicketInput input, bool onlystatus = false);

		Task<List<TicketListDto>> GetAllTicketsForTicketInput(GetTicketInput input);

		Task<FileDto> GetAllToExcel(GetTicketInput input);

		IQueryable<Ticket> GetAllTicketsForTicketInputQueryable(GetTicketInput input);

		IQueryable<Ticket> GetAllTickets(IDateInput input);
	}
}
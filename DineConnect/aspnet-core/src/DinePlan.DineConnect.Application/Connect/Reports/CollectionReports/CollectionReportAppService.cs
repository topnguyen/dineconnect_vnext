﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;

using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;

using DinePlan.DineConnect.Dto;
using System.Data.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;


using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Authorization;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DbFunctions = System.Data.Entity.DbFunctions;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Exporting;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Configuration;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.CollectionReports.Exporting;
using System.Linq;
using System.Linq.Dynamic;

namespace DinePlan.DineConnect.Connect.Reports.CollectionReports
{
    
    public class CollectionReportAppService : DineConnectReportAppServiceBase, ICollectionReportAppService
    {
        private string[] DISCOUNTNAMES = { "DISCOUNT" };
        private string[] ROUNDTOTALS = { "ROUND+", "ROUND-", "ROUNDING" };
        private readonly IRepository<Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payRe;
        private readonly ICollectionReportExporter _exporter;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<TicketTagGroup> _tagGroupRepo;
        private readonly IRepository<TransactionType> _tRe;
        private readonly IRepository<TillTransaction> _tillTransactionRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<TicketTagGroup> _ticketTagGroupRepository;
        public CollectionReportAppService(ILocationAppService locService, IRepository<Ticket> ticketRepo, IUnitOfWorkManager unitOfWorkManager, IRepository<Department> departmentRepo, IRepository<Transaction.Ticket> ticketManager,
            IRepository<Department> drepo,
            IRepository<Location> lRepo,
            IRepository<TicketTagGroup> tagGroupRepo,
            IRepository<Transaction.Order> orderManager,
             IReportBackgroundAppService rbas,
             IBackgroundJobManager bgm,
             ICollectionReportExporter exporter,
             IRepository<Period.WorkPeriod> workPeriod,
             IRepository<Master.PaymentTypes.PaymentType> payRe,
             ITenantSettingsAppService tenantSettingsAppService,
             IRepository<TransactionType> tRe,
             IRepository<TillTransaction> tillTransactionRepository,
              IRepository<Promotion> promotionRepository,
            IRepository<TicketTagGroup> ticketTagGroupRepository) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _ticketManager = ticketManager;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _tagGroupRepo = tagGroupRepo;
            _payRe = payRe;
            _tenantSettingsAppService = tenantSettingsAppService;
            _tRe = tRe;
            _tillTransactionRepository = tillTransactionRepository;
            _promotionRepository = promotionRepository;
            _ticketTagGroupRepository = ticketTagGroupRepository;
        }

        public async Task<PagedResultDto<TicketTypeReportData>> BuildCollectionReport(GetTicketInput input)
        {
            var orderByLocation = await GetCollectionObjects(input);
            var resultList = orderByLocation.AsQueryable().PageBy(input).ToList();
            return new PagedResultDto<TicketTypeReportData>(orderByLocation.Count(), resultList);
        }

        public async Task<FileDto> BuildCollectionReportForExport(GetTicketInput input)
        {
            var orderByLocation = await GetCollectionObjects(input);

            var resultList = orderByLocation.ToList();

            return _exporter.ExportCollectionReport(resultList, input);
        }

        public async Task<FileDto> BuildCollectionReportForExportInternal(GetTicketInput input)
        {
            var RESULT = new FileDto();
            return RESULT;

        }

        private async Task<IEnumerable<TicketTypeReportData>> GetCollectionObjects(GetTicketInput input)
        {
            var allTranType = _tRe.GetAll();
               var allReportOutput = new List<TicketTypeReportData>();
            var allTickets = GetAllTicketsForTicketInputNoFilterByDynamicFilter(input).ToList();
           foreach(var transac in allTickets)
            {
                foreach(var trantype in transac.Transactions)
                {
                    trantype.TransactionType = allTranType.FirstOrDefault(s =>s.Id == trantype.TransactionTypeId);
                }
            }
            var groupTicket = allTickets.GroupBy(o => new { o.LocationId, o.Location.Name });
            foreach (var tickets in groupTicket)
            {
                var returnObject = new TicketTypeReportData
                {
                    Id = tickets.Key.LocationId,
                    LocationId = tickets.Key.LocationId,
                    LocationName = tickets.Key.Name,
                    TotalSales = tickets.Sum(a => a.TotalAmount),
                    Tax = tickets.Sum(a => a.CalculateTax(a.GetPlainSum(),
                        a.Transactions.Where(x => DISCOUNTNAMES.Contains(x.TransactionType.Name.ToUpper()))
                            .Sum(b => b.Amount))),
                    OrderTotal = tickets.SelectMany(a => a.Orders).Sum(a => a.GetTotal()),

                    TicketCount = tickets.Count(),
                    ItemCount = tickets.SelectMany(t => t.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory)
                        .Sum(o => o.Quantity),
                    PaxCount = GetPaxCount(tickets.ToList()),
                };

                returnObject.TicketCount = tickets.Count(a => a.TotalAmount > 0);
                returnObject.ItemDiscountTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                    .SelectMany(a => a.GetOrderPromotionList()).Sum(a => a.PromotionAmount);
                returnObject.ItemSales = tickets.SelectMany(a => a.Orders).Sum(b => b.GetTotal());

                returnObject.TicketDiscountTotal = tickets.Where(a => a.TotalAmount > 0)
                    .SelectMany(a => a.GetTicketPromotionList()).Sum(a => a.PromotionAmount);

                var tranAmount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                    .Where(a => DISCOUNTNAMES.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
                returnObject.TicketDiscountTotal += tranAmount;

                var roundTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                    .Where(a => ROUNDTOTALS.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
                returnObject.RoundingTotal += roundTotal;

                returnObject.ItemCount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                    .Where(a => a.CalculatePrice && a.DecreaseInventory).Sum(a => a.Quantity);

                returnObject.PaxCount = GetPaxCount(tickets.Where(a => a.TotalAmount > 0));

                allReportOutput.Add(returnObject);
            }
            // filter by DynamicFilter

            var dataAsQueryable = allReportOutput.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }
            return dataAsQueryable.ToList();
        }

        private decimal GetPaxCount(IEnumerable<Transaction.Ticket> dDto)
        {
            if (_tagGroupRepo.Count() > 1)
            {
                var totalPaxCount = 0M;
                var toaalll = dDto.Where(a => !string.IsNullOrEmpty(a.TicketTags))
                    .Select(a => a.TicketTags);
                foreach (var tag in toaalll)
                    if (!string.IsNullOrEmpty(tag))
                    {
                        var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                        var lastTag = myTag?.LastOrDefault(a =>
                            a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                        if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                    }
                // Gosei Hoan Update pax calculation
                if (totalPaxCount == 0)
                {
                    totalPaxCount = dDto.SelectMany(t => t.Orders).Sum(o => o.Quantity);
                }
                return totalPaxCount;
            }

            return 0;
        }

    }
}


﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.CollectionReports.Exporting
{
    public interface ICollectionReportExporter
    {
        FileDto ExportCollectionReport(List<TicketTypeReportData> dtos, GetTicketInput input);
    }
}

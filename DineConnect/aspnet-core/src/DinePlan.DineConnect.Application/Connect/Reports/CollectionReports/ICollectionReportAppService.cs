﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.CollectionReports
{
    public interface ICollectionReportAppService : IApplicationService
    {
        Task<PagedResultDto<TicketTypeReportData>> BuildCollectionReport(GetTicketInput input);

        Task<FileDto> BuildCollectionReportForExport(GetTicketInput input);

        Task<FileDto> BuildCollectionReportForExportInternal(GetTicketInput input);
    }
}

﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.DepartmentReports.Exporting
{
    public interface IDepartmentReportExporter
    {            
        Task<FileDto> GetDepartmentExcel(GetItemInput input, IDepartmentReportAppService departmentReportAppService);
    }
}

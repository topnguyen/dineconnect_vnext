﻿using Abp.AspNetZeroCore.Net;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.DepartmentReports.Exporting
{
    public class DepartmentReportExporter : EpPlusExcelExporterBase, IDepartmentReportExporter
    {
        public readonly IRepository<Master.PaymentTypes.PaymentType> _paymentTypesRepository;
        public readonly IRepository<Master.TransactionTypes.TransactionType> _transactionTypesRepository;
        public DepartmentReportExporter(ITempFileCacheManager tempFileCacheManager
            , IRepository<Master.PaymentTypes.PaymentType> paymentTypesRepository
            , IRepository<Master.TransactionTypes.TransactionType> transactionTypesRepository) : base(tempFileCacheManager)
        {
            _paymentTypesRepository = paymentTypesRepository;
            _transactionTypesRepository = transactionTypesRepository;
        }
        public async Task<FileDto> GetDepartmentExcel(GetItemInput input, IDepartmentReportAppService departmentReportAppService)
        {
            var transactionTypes = _transactionTypesRepository.GetAll().ToList();
            var paymentTypes = _paymentTypesRepository.GetAll().ToList();
            var file = new FileDto("Group-" + input.StartDate.ToString("yyyy-MM-dd") + "-" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
             MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var output = await departmentReportAppService.GetDepartmentSalesSummary(input);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                    L("LocationName"),
                    L("DepartmentName")
                };

                headers.AddRange(paymentTypes.Select(x=> x.Name));
                headers.AddRange(transactionTypes.Select(x => x.Name));

                AddHeader(
                    sheet,
                    headers.ToArray()
                );

                var rowCount = 2;
                foreach (var dept in output)
                {
                    var colCount = 1;

                    sheet.Cells[rowCount, colCount++].Value = dept.Location;
                    sheet.Cells[rowCount, colCount++].Value = dept.DepartmentName;
                    foreach (var p in paymentTypes)
                    {
                        var pValue = 0M;
                        if (dept.Payments.ContainsKey(p.Id)) pValue = dept.Payments[p.Id];
                        sheet.Cells[rowCount, colCount++].Value = pValue;
                    }

                    foreach (var t in transactionTypes)
                    {
                        var tValue = 0M;
                        if (dept.Transactions.ContainsKey(t.Id)) tValue = dept.Transactions[t.Id];
                        sheet.Cells[rowCount, colCount++].Value = tValue;
                    }

                    rowCount++;
                }

                for (var i = 1; i <= headers.Count; i++) sheet.Column(i).AutoFit();

                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Tickets.Dtos;
using DinePlan.DineConnect.Connect.Reports.DepartmentReports.Exporting;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.TicketReports;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.DepartmentReports
{
    
    public class DepartmentSummaryAppService : DineConnectReportAppServiceBase, IDepartmentReportAppService
    {
        private IDepartmentReportExporter _exporter;
        public DepartmentSummaryAppService(ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IDepartmentReportExporter exporter) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _exporter = exporter;
        }
        public async Task<DepartmentStatsDto> BuildDepartmentSales(GetItemInput input)
        {
            var returnOutput = new DepartmentStatsDto();

            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var tickets = GetAllTickets(input).ToList();
                var sortTickets = tickets.GroupBy(a => a.DepartmentName);
                var reportDto = new List<DepartmentReportDto>();

                foreach (var dto in sortTickets)
                {
                    var depatId = 0;
                    var depts = _departmentRepo.FirstOrDefault(a => a.Name.Equals(dto.Key));
                    if (depts != null) depatId = depts.Id;
                    reportDto.Add(new DepartmentReportDto
                    {
                        Id = depatId,
                        DepartmentName = dto.Key,
                        TotalTicketCount = dto.Count(),
                        Total = dto.Sum(a => a.TotalAmount)
                    });
                }

                //filter by builder query
                var dataAsQueryable = reportDto.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                var menuItemCount = dataAsQueryable.Count();
                returnOutput.DepartmentList = new PagedResultDto<DepartmentReportDto>(
                    menuItemCount,
                    dataAsQueryable.ToList()
                );
            }

            return returnOutput;
        }

        public Task<List<DepartmentSummaryDto>> GetDepartmentSalesSummary(GetItemInput input)
        {
            var summary = new List<DepartmentSummaryDto>();
            var tickets = GetAllTickets(input).ToList();

            if (tickets.Any())
            {
                var deptS = tickets.GroupBy(a => new { a.Location.Name, a.DepartmentName });
                foreach (var dept in deptS)
                {
                    var su = new DepartmentSummaryDto
                    {
                        DepartmentName = dept.Key.DepartmentName,
                        Location = dept.Key.Name,
                        AllPayments = dept.Where(a => a.Payments != null).SelectMany(a => a.Payments).Select(x => ObjectMapper.Map<PaymentEditDto>(x)).GroupBy(a => a.PaymentTypeId).ToList(),
                        AllTransactions = dept.Where(a => a.Transactions != null).SelectMany(a => a.Transactions).Select(x => ObjectMapper.Map<TicketTransactionListDto>(x)).GroupBy(a => a.TransactionTypeId).ToList()
                    };
                    summary.Add(su);
                }

                foreach (var sum in summary)
                {
                    foreach (var payment in sum.AllPayments) sum.Payments[payment.Key] = payment.Sum(a => a.Amount);
                    foreach (var trns in sum.AllTransactions) sum.Transactions[trns.Key] = trns.Sum(a => a.Amount);
                }
            }

            return Task.FromResult(summary);
        }

        public async Task<FileDto> BuildDepartmentSummaryExport(GetItemInput input)
        {
            return await _exporter.GetDepartmentExcel(input, this);
        }
    }
}

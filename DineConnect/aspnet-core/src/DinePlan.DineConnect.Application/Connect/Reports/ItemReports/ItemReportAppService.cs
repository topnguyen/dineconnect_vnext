﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.ItemReports.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Web.Http;

namespace DinePlan.DineConnect.Connect.Reports.ItemReports
{
    
    public class ItemReportAppService : DineConnectReportAppServiceBase, IItemReportAppService
    {
        private readonly IRepository<Department> _departmentRepo;
        private readonly ILocationAppService _locationService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<MenuItem> _menuItemRepo;
        private readonly IRepository<Promotion> _promotionRepo;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<Category> _categoryRepository;

        private IItemReportExcelExporter _itemReportExcelExporter;

        public ItemReportAppService(ILocationAppService locationService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Location> lRepo
            , IRepository<MenuItem> mrRepo
            , IRepository<Promotion> promoRepo
            , IRepository<MenuItemPortion> menuItemPortionRepository
            , IItemReportExcelExporter itemReportExcelExporter
            , IRepository<Category> categoryRepository
            )
            : base(locationService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _locationService = locationService;
            _unitOfWorkManager = unitOfWorkManager;
            _locationRepo = lRepo;
            _menuItemRepo = mrRepo;
            _promotionRepo = promoRepo;
            _menuItemPortionRepository = menuItemPortionRepository;
            _itemReportExcelExporter = itemReportExcelExporter;
            _categoryRepository = categoryRepository;
        }

        public async Task<ItemSummaryReportOutput> BuildItemSalesReport(GetItemInput input)
        {
            var returnOutput = new ItemSummaryReportOutput();
            using (var unitOfWork = _unitOfWorkManager.Begin(new UnitOfWorkOptions()
            {
                Scope = System.Transactions.TransactionScopeOption.RequiresNew,
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
                IsTransactional = false
            }))
            {
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete, DineConnectDataFilters.MustHaveOrganization))
                {
                    var orders = GetAllOrdersNotFilterByDynamicFilter(input);

                    if (!input.VoidParam && !input.Gift && !input.Comp && !input.Refund)
                    {
                        orders = orders.Where(x => x.CalculatePrice
                        || x.DecreaseInventory
                        || string.IsNullOrEmpty(x.OrderStates)
                        || (!x.OrderStates.Contains("Void")
                            && !x.OrderStates.Contains("Gift")
                            && !x.OrderStates.Contains("Comp")
                            && !x.OrderStates.Contains("Refund")));
                    }

                    var output = (await orders.ToListAsync()).GroupBy(c => new { c.MenuItemPortionId, Location = c.Location_Id })
                        .Select(g => new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });

                    var outputOrderTemps = output.ToList().Select(o => new
                    {
                        Location = o.Location,
                        MenuItemPortion = o.MenuItemPortionId,
                        Items = ObjectMapper.Map<List<OrderListDto>>(o.Items)
                    });

                    var menuListOutputDots = new List<MenuListDto>();

                    var dicLocations = new Dictionary<int, string>();
                    var dicCategory = new Dictionary<int, string>();
                    var dicGroups = new Dictionary<int, string>();
                    var aliasCodes = new Dictionary<int, string>();
                    var portionIds = new Dictionary<int, string>();

                    foreach (var dto in outputOrderTemps)
                    {
                        var firObj = dto.Items.First();
                        if (firObj != null)
                            try
                            {
                                var sumQuantity = dto.Items.Sum(b => b.Quantity);
                                var lname = "";
                                if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                                {
                                    lname = dicLocations[dto.Location];
                                }
                                else
                                {
                                    var lSer = await _locationRepo.GetAsync(dto.Location);
                                    if (lSer == null) continue;

                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }

                                var menuItem = firObj.MenuItemId;
                                var cateDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, menuItem);
                                var menL = new MenuListDto
                                {
                                    AliasCode = cateDto.AliasCode,
                                    LocationName = lname,
                                    MenuItemPortionId = dto.MenuItemPortion,
                                    MenuItemPortionName = firObj.PortionName,
                                    MenuItemId = firObj.MenuItemId,
                                    MenuItemName = firObj.MenuItemName,
                                    CategoryId = cateDto.CatId,
                                    CategoryName = cateDto.CatgoryName,
                                    GroupId = cateDto.GroupId,
                                    GroupName = cateDto.GroupName.IsNullOrEmpty() ? null : cateDto.GroupName,
                                    Quantity = dto.Items.Sum(a => a.Quantity),
                                    Tags = firObj.Tags,
                                    Price = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.Price) / sumQuantity) : 0,
                                    TaxPrice = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0
                                };

                                var allTags = dto.Items.SelectMany(t => t.TransactionOrderTags)
                                    .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId == 0);

                                if (allTags.Any())
                                {
                                    menL.OrderTagPrice = sumQuantity != 0 ? (allTags.Sum(a => a.Price * a.Quantity) / sumQuantity) : 0;
                                    menL.IsOrderTag = false;

                                    if (input.IsItemSalesReport)
                                    {
                                        var menuOrderTag = new MenuListDto
                                        {
                                            IsOrderTag = true,
                                            AliasCode = cateDto.AliasCode,
                                            LocationName = lname,
                                            MenuItemPortionId = dto.MenuItemPortion,
                                            MenuItemPortionName = firObj.PortionName,
                                            MenuItemId = firObj.MenuItemId,
                                            MenuItemName = firObj.MenuItemName,
                                            CategoryId = cateDto.CatId,
                                            CategoryName = cateDto.CatgoryName,
                                            GroupId = cateDto.GroupId,
                                            GroupName = cateDto.GroupName.IsNullOrEmpty() ? null : cateDto.GroupName,
                                            Tags = firObj.Tags,
                                            Quantity = dto.Items
                                                .Where(e => allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity),
                                            OrderTagPrice = menL.OrderTagPrice
                                        };
                                        menuOrderTag.Price =
                                         menuOrderTag.Quantity != 0 ? (dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                                .Sum(a => a.Quantity * a.Price) / menuOrderTag.Quantity) : 0 +
                                            menuOrderTag.OrderTagPrice;
                                        menuOrderTag.TaxValue =
                                            dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                                .Sum(c => c.GetTaxablePrice()) - menuOrderTag.Price * menuOrderTag.Quantity;
                                        menuOrderTag.TaxPrice =
                                          sumQuantity != 0 ? (dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                                .Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0;

                                        menuListOutputDots.Add(menuOrderTag);

                                        menL.OrderTagPrice = 0;
                                        menL.Quantity = dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity);
                                        menL.Price =
                                            menL.Quantity != 0 ? (dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity * a.Price) / menL.Quantity) : 0
                                                + menL.OrderTagPrice;
                                        menL.TaxValue =
                                            dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                                .Sum(c => c.GetTaxablePrice()) - menL.Price * menL.Quantity;
                                    }
                                }

                                if (!input.IsItemSalesReport || !allTags.Any())
                                {
                                    var finalPrice = menL.Price + menL.OrderTagPrice;
                                    menL.Price = finalPrice;

                                    var taxvalue = dto.Items.Sum(c => c.GetTaxablePrice()) - finalPrice * menL.Quantity;
                                    menL.TaxValue = taxvalue;
                                }

                                menuListOutputDots.Add(menL);

                                var itemTags = dto.Items.SelectMany(t => t.TransactionOrderTags)
                                    .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId > 0);
                                foreach (var transactionOrderTag in itemTags)
                                {
                                    var portionId = transactionOrderTag.MenuItemPortionId;

                                    var lastMenuListDto =
                                        menuListOutputDots.LastOrDefault(a => a.MenuItemPortionId == portionId);
                                    if (lastMenuListDto == null)
                                    {
                                        var myItemid = 0;
                                        var portionName = "";

                                        if (portionIds.Any() && portionIds.ContainsKey(portionId))
                                        {
                                            var portionNameSplit = portionIds[menuItem];
                                            var splitName = portionNameSplit.Split('@');
                                            if (splitName.Any())
                                            {
                                                myItemid = Convert.ToInt32(splitName[0]);
                                                portionName = splitName[1];
                                            }
                                        }
                                        else
                                        {
                                            var lSer = await _menuItemPortionRepository.GetAsync(portionId);
                                            if (lSer != null)
                                            {
                                                myItemid = lSer.MenuItemId;
                                                portionName = lSer.Name;
                                                portionIds.Add(lSer.Id, myItemid + "@" + portionName);
                                            }
                                        }

                                        var myCategoryDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, myItemid);
                                        var myTagList = new MenuListDto
                                        {
                                            IsOrderTag = true,
                                            AliasCode = myCategoryDto.AliasCode,
                                            LocationName = lname,
                                            MenuItemPortionId = portionId,
                                            MenuItemPortionName = portionName,
                                            MenuItemId = myItemid,
                                            MenuItemName = transactionOrderTag.TagValue,
                                            CategoryId = myCategoryDto.CatId,
                                            CategoryName = myCategoryDto.CatgoryName,
                                            GroupId = myCategoryDto.GroupId,
                                            GroupName = myCategoryDto.GroupName.IsNullOrEmpty() ? null : myCategoryDto.GroupName,
                                            Quantity = transactionOrderTag.Quantity,
                                            Tags = "",
                                            Price = transactionOrderTag.Price,
                                            TaxPrice = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0
                                        };
                                        menuListOutputDots.Add(myTagList);
                                    }
                                    else
                                    {
                                        lastMenuListDto.Quantity += transactionOrderTag.Quantity;
                                        lastMenuListDto.Price =
                                            (lastMenuListDto.Quantity + transactionOrderTag.Quantity) != 0 ?
                                            (lastMenuListDto.Price * lastMenuListDto.Quantity +
                                             transactionOrderTag.Price * transactionOrderTag.Quantity) /
                                            (lastMenuListDto.Quantity + transactionOrderTag.Quantity)
                                            : 0;
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                var mess = exception.Message;
                            }
                    }
                    // filter by dynamic filter
                    var dataAsQueryable = menuListOutputDots.AsQueryable();
                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        if (filRule?.Rules != null)
                        {
                            dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                        }
                    }

                    menuListOutputDots = dataAsQueryable.ToList();

                    if (input.Stats)
                    {
                        returnOutput.Quantities = GetItemByQuantity(dataAsQueryable);
                        returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                        returnOutput.Totals = GetItemByTotal(dataAsQueryable);
                        returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                    }

                    if (input.NoSales) menuListOutputDots = GetNoSales(menuListOutputDots);

                    if (!input.Sorting.Equals("Id"))
                        menuListOutputDots = menuListOutputDots.AsQueryable().OrderBy(input.Sorting).ToList();

                    var totalCount = menuListOutputDots.Count();
                    menuListOutputDots = menuListOutputDots.AsQueryable()
                            .OrderBy(i => i.LocationName).ThenBy(i => i.GroupName).ThenBy(i => i.CategoryName)
                            .ThenBy(input.Sorting).ToList();

                    if (input.IsSummary)
                    {
                        var result = menuListOutputDots.GroupBy(m => m.LocationName)
                              .Select(g =>
                              {
                                  var p = new ItemReportTreeData
                                  {
                                      Data = new MenuListDto
                                      {
                                          LocationName = $"{g.Key} ({g.Count()})",
                                          Quantity = g.Sum(m => m.Quantity),
                                          TotalAmount = g.Sum(m => m.Total),
                                      },
                                      Children = g.GroupBy(m => m.GroupName)
                                      .Select(a =>
                                      {
                                          var b = new ItemReportTreeData
                                          {
                                              Data = new MenuListDto
                                              {
                                                  GroupName = $"{a.Key} ({a.Count()})",
                                                  Quantity = a.Sum(m => m.Quantity),
                                                  TotalAmount = a.Sum(m => m.Total),
                                              },
                                              Children = a.GroupBy(m => m.CategoryName)
                                              .Select(c =>
                                              {
                                                  var d = new ItemReportTreeData
                                                  {
                                                      Data = new MenuListDto
                                                      {
                                                          CategoryName = $"{c.Key} ({c.Count()})",
                                                          Quantity = c.Sum(m => m.Quantity),
                                                          TotalAmount = c.Sum(m => m.Total),
                                                      },
                                                      Children = c.Select(ch => new ItemReportTreeData { Data = ch }).ToList()
                                                  };

                                                  return d;
                                              }).ToList()
                                          };

                                          return b;
                                      }).ToList()
                                  };

                                  return p;
                              }
                            );

                        returnOutput.MenuList = new PagedResultDto<ItemReportTreeData>(
                            result.Count(),
                            result.AsQueryable()
                            .PageBy(input).ToList());
                    }

                    returnOutput.TotalQuantity = menuListOutputDots.Sum(m => m.Quantity);
                    returnOutput.TotalResult = menuListOutputDots.Sum(m => m.Total);
                }
            }

            return returnOutput;
        }

        public IQueryable<Order> GetAllOrdersNotFilterByDynamicFilter(GetItemInput input)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input)
                .WhereIf(!string.IsNullOrEmpty(input.TerminalName), x => x.TerminalName.Contains(input.TerminalName));
            var orders = tickets.SelectMany(a => a.Orders).Include(a=>a.TransactionOrderTags)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            if (input.VoidParam || input.Gift || input.Comp || input.Refund)
                orders = orders.Where(x =>
                !x.CalculatePrice && !x.IncreaseInventory &&
                (!string.IsNullOrEmpty(x.OrderStates)
                && ((input.VoidParam && x.OrderStates.Contains("Void") && !x.DecreaseInventory) ||
                (input.Gift && x.OrderStates.Contains("Gift") && x.DecreaseInventory) ||
                (input.Comp && x.OrderStates.Contains("Comp") && x.DecreaseInventory) ||
                (input.Refund && x.OrderStates.Contains("Refund") && !x.DecreaseInventory)
                )));

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));
            return orders;
        }

        public async Task<ItemStatsDto> BuildItemSalesByDepartment(GetItemInput input)
        {
            var orders = GetAllOrdersNotFilterByDynamicFilter(input);

            var returnOutput = new ItemStatsDto();

            returnOutput.Quantities = GetItemByQuantity(orders);
            returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

            returnOutput.Totals = GetItemByTotal(orders);
            returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;

            var sortTickets = await orders.ToListAsync();
            var categoryListDtos = ObjectMapper.Map<List<OrderListDto>>(sortTickets);
            var output = from c in categoryListDtos
                         group c by new { c.MenuItemPortionId, Location = c.LocationName, Department = c.DepartmentName }
                into g
                         select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, g.Key.Department, Items = g };

            var outputDtos = new List<MenuListDto>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete, DineConnectDataFilters.MustHaveOrganization))
            {
                foreach (var dto in output)
                {
                    var portion = new MenuItemPortion();
                    portion = await _menuItemPortionRepository
                        .GetAll()
                        .Include(menu => menu.MenuItem)
                        .ThenInclude(x => x.Category)
                        .Where(i => i.Id == dto.MenuItemPortion)
                        .FirstOrDefaultAsync();

                    if (portion != null)
                        try
                        {
                            outputDtos.Add(new MenuListDto
                            {
                                DepartmentName = dto.Department,
                                LocationName = dto.Location,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = portion.Name,
                                MenuItemId = portion.MenuItemId,
                                MenuItemName = portion.MenuItem.Name,
                                CategoryId = portion.MenuItem.CategoryId,
                                CategoryName = portion.MenuItem.Category.Name ?? "",
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity)
                            });
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }
            }

            if (input.NoSales) outputDtos = GetNoSales(outputDtos);

            // add filter by query builder
            var dataAsQueryable = outputDtos.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
            }

            outputDtos = dataAsQueryable.ToList();

            outputDtos = input.Sorting == "Id"
                ? outputDtos.AsQueryable().OrderBy("CategoryId").ToList()
                : outputDtos.AsQueryable().OrderBy(input.Sorting).ToList();
            var outputPageDtos = outputDtos
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToList();

            returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                outputDtos.Count(),
                outputPageDtos
            );
            returnOutput.TotalQuantity = 0;
            returnOutput.TotalResult = 0;
            foreach (var item in outputDtos)
            {
                returnOutput.TotalQuantity += item.Quantity;
                returnOutput.TotalResult += item.Total;
            }

            returnOutput.TotalQuantity = returnOutput.TotalQuantity;
            returnOutput.TotalResult = returnOutput.TotalResult;

            return returnOutput;
        }

        public async Task<FileDto> ExportItemSalesByDepartmentExcel(GetItemInput input)
        {
            input.MaxResultCount = int.MaxValue;
            var items = await BuildItemSalesByDepartment(input);
            return _itemReportExcelExporter.ExportItemSalesForDepartmentExcel(input, items);
        }

        private async Task<TCategoryDto> GetCategoryDto(Dictionary<int, string> dicCategory, Dictionary<int, string> dicGroups, Dictionary<int, string> aliasCodes, int menuItem)
        {
            var returnD = new TCategoryDto();

            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
            {
                var catSplitname = dicCategory[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.CatId = Convert.ToInt32(splitName[0]);
                    returnD.CatgoryName = splitName[1];
                }
            }
            else
            {
                var lSer = await _menuItemRepo.GetAllIncluding(m => m.Category).FirstOrDefaultAsync(m => m.Id == menuItem);
                if (lSer != null)
                {
                    returnD.CatId = lSer.CategoryId;
                    returnD.CatgoryName = lSer.Category.Name;
                    dicCategory.Add(menuItem, returnD.CatId + "@" + returnD.CatgoryName);
                }
            }

            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
            {
                var catSplitname = dicGroups[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.GroupId = Convert.ToInt32(splitName[0]);
                    returnD.GroupName = splitName[1];
                }
            }
            else
            {
                var lSer = await _menuItemRepo.GetAllIncluding(m => m.Category).FirstOrDefaultAsync(m => m.Id == menuItem);
                if (lSer != null)
                {
                    returnD.GroupId = lSer.Category?.ProductGroupId ?? 0;
                    returnD.GroupName = lSer.Category?.ProductGroup?.Name;
                    dicGroups.Add(menuItem, returnD.GroupId + "@" + returnD.GroupName);
                }
            }

            if (aliasCodes.Any() && aliasCodes.ContainsKey(menuItem))
            {
                returnD.AliasCode = aliasCodes[menuItem];
            }
            else
            {
                var alisCodeSet = _menuItemRepo.Get(menuItem);
                aliasCodes.Add(menuItem, alisCodeSet.AliasCode);
                returnD.AliasCode = alisCodeSet.AliasCode;
            }

            return returnD;
        }

        public List<ChartOutputDto> GetItemByQuantity(IQueryable<MenuListDto> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        public List<ChartOutputDto> GetItemByTotal(IQueryable<MenuListDto> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Price * c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        private List<MenuListDto> GetNoSales(List<MenuListDto> outputDtos)
        {
            var allPortions = _menuItemPortionRepository.GetAll().ToList();
            var missinPortion = allPortions.Where(e => outputDtos.All(a => a.MenuItemPortionId != e.Id)).ToList();

            if (missinPortion.Any()) outputDtos.Clear();

            try
            {
                foreach (var missP in missinPortion)
                    if (missP?.MenuItem?.Category != null)
                        outputDtos.Add(new MenuListDto
                        {
                            LocationName = L("All"),
                            MenuItemPortionId = missP.Id,
                            MenuItemPortionName = missP.Name,
                            MenuItemId = missP.MenuItemId,
                            MenuItemName = missP.MenuItem.Name,
                            CategoryId = missP.MenuItem.CategoryId,
                            CategoryName = missP.MenuItem.Category.Name ?? "",
                            Quantity = 0,
                            Price = 0,
                            Hour = 0
                        });
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            outputDtos = outputDtos.OrderBy(a => a.LocationName).ToList();
            return outputDtos;
        }

        public async Task<ItemStatsDto> BuildItemHourlySales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (var unitOfWork = _unitOfWorkManager.Begin(new UnitOfWorkOptions()
            {
                Scope = System.Transactions.TransactionScopeOption.RequiresNew,
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
                IsTransactional = false
            }))
            {
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete, DineConnectDataFilters.MustHaveOrganization))
                {
                    var orders = GetAllOrdersNoDynamicFilter(input);
                    var output = (await orders.Include(x => x.Ticket).ToListAsync()).GroupBy(x => new { x.Ticket.LastPaymentTime.Hour, x.MenuItemPortionId, x.Location_Id, x.MenuItemId })
                                  .Select(x => new
                                  {
                                      Location = x.Key.Location_Id,
                                      MenuItemPortion = x.Key.MenuItemPortionId,
                                      Items = x
                                  }).ToList();

                    var outputDtos = new List<MenuListDto>();
                    var dicLocations = new Dictionary<int, string>();
                    var dicCategory = new Dictionary<int, string>();

                    foreach (var dto in output)
                    {
                        var firObj = dto.Items.First();

                        if (firObj != null)
                            try
                            {
                                var lname = "";
                                if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                                {
                                    lname = dicLocations[dto.Location];
                                }
                                else
                                {
                                    var lSer = _locationRepo.Get(dto.Location);
                                    if (lSer != null)
                                    {
                                        lname = lSer.Name;
                                        dicLocations.Add(dto.Location, lname);
                                    }
                                }

                                var catId = 0;
                                var catgoryName = "";

                                var menuItem = firObj.MenuItemId;
                                if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                                {
                                    var catSplitname = dicCategory[menuItem];
                                    var splitName = catSplitname.Split('@');
                                    if (splitName != null && splitName.Any())
                                    {
                                        catId = Convert.ToInt32(splitName[0]);
                                        catgoryName = splitName[1];
                                    }
                                }
                                else
                                {
                                    var lSer = await _menuItemRepo.GetAll().Include(x => x.Category).Where(x => x.Id == menuItem).FirstOrDefaultAsync();
                                    if (lSer != null)
                                    {
                                        catId = lSer.CategoryId;
                                        catgoryName = lSer.Category.Name;
                                        dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                    }
                                }

                                outputDtos.Add(new MenuListDto
                                {
                                    LocationName = lname,
                                    MenuItemPortionId = dto.MenuItemPortion,
                                    MenuItemPortionName = firObj.PortionName,
                                    MenuItemId = firObj.MenuItemId,
                                    MenuItemName = firObj.MenuItemName,
                                    CategoryId = catId,
                                    CategoryName = catgoryName,
                                    Quantity = dto.Items.Sum(a => a.Quantity),
                                    Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                    Hour = dto.Items.Key.Hour
                                });
                            }
                            catch (Exception exception)
                            {
                                var mess = exception.Message;
                            }
                    }

                    if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                    var outputAsQueryable = outputDtos.AsQueryable();
                    if (!input.Sorting.Equals("Id"))
                        outputDtos = outputAsQueryable.OrderBy(input.Sorting).ToList();
                    // var outputDtosNew = outputDtos.GroupBy(c => c.Hour).ToList();

                    // filter by dynamic
                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        filRule.Rules = filRule.Rules.Where(x => x.Value != null && x.Value != "").ToList();
                        if (filRule.Rules != null && filRule.Rules.Count > 0)
                            outputAsQueryable = outputAsQueryable.BuildQuery(filRule);
                    }
                    var outputPageDtos = new List<MenuListDto>();

                    if (input.OutputType == "EXPORT")
                        outputPageDtos = outputAsQueryable
                        .ToList();
                    else outputPageDtos = outputAsQueryable
                        .Skip(input.SkipCount)
                        .Take(input.MaxResultCount)
                        .ToList();

                    returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                        outputAsQueryable.Count(),
                        outputPageDtos
                    );
                }
                return await Task.FromResult(returnOutput);
            }
        }

        [HttpPost]
        public async Task<FileDto> BuildItemHourlyReportExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            var list = await BuildItemHourlySales(input);

            var output = await _itemReportExcelExporter.ExportToFileItemsHours(input, list.MenuList.Items.ToList());
            return output;
        }

        #region ComboItem

        public async Task<FileDto> ExportItemComboSalesToExcel(GetItemInput input)
        {
            input.ExportOutputType = ExportType.Excel;

            if (!input.RunInBackground)
            {
                var combos = await GetItemComboSales(input);
                return _itemReportExcelExporter.ExportComboItemsSaleToFile(combos, input);
            }
            return null;
        }

        public async Task<List<ItemSale>> GetItemComboSales(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete, DineConnectDataFilters.MustHaveOrganization))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var comboOrders = await orders.Where(a => a.MenuItemType == 1 && a.OrderRef != null).ToListAsync();
                var comboOrdersWithItems = await orders
                    .Where(t => t.OrderRef != null && comboOrders.Select(o => o.OrderRef).Contains(t.OrderRef))
                    .ToListAsync();

                var result = comboOrders.GroupBy(o => new { o.MenuItemId, o.MenuItemName })
                    .Select(g => new ItemSale
                    {
                        Location = g.FirstOrDefault()?.Location?.Name,
                        LocationCode = g.FirstOrDefault()?.Location?.Code,
                        Name = g.Key.MenuItemName,
                        Quantity = g.Sum(o => o.Quantity),
                        Amount = g.Sum(o => o.Quantity * o.Price),
                        Price = g.FirstOrDefault().Price,
                        OrderItems = g.SelectMany(o => GetOrderItems(o, comboOrdersWithItems))
                            .GroupBy(t => new { t.Id, t.Name }).Select(g2 => new ItemSale
                            {
                                Location = g2.FirstOrDefault().Location,
                                LocationCode = g2.FirstOrDefault().LocationCode,
                                GroupName = g2.FirstOrDefault().GroupName,
                                Name = g2.Key.Name,
                                Id = g2.Key.Id,
                                Quantity = g2.Sum(i => i.Quantity),
                                Price = g2.FirstOrDefault().Price,
                                Amount = g2.Sum(i => i.Amount)
                            }).OrderBy(t => t.Name).ToList(),
                        OrderTags = g.SelectMany(o => GetOrderTags(o)).GroupBy(t => new { t.Id, t.Name }).Select(g2 =>
                              new ItemSale
                              {
                                  Location = g2.FirstOrDefault().Location,
                                  LocationCode = g2.FirstOrDefault().LocationCode,
                                  Name = g2.Key.Name,
                                  Id = g2.Key.Id,
                                  Quantity = g2.Sum(i => i.Quantity),
                                  Price = g2.FirstOrDefault().Price,
                                  Amount = g2.Sum(i => i.Amount)
                              }).OrderBy(t => t.Name).ToList()
                    }).OrderBy(t => t.Name)
                    .ToList();
                return result;
            }
        }

        private List<ItemSale> GetOrderItems(Order comboOrder, List<Order> allComboOrdersWithItems)
        {
            var refOrders = allComboOrdersWithItems
                .Where(t => t.OrderRef == comboOrder.OrderRef && t.Id != comboOrder.Id)
                .OrderBy(o => o.Id).ToList();

            var result = refOrders.Select(t => new ItemSale
            {
                Id = t.MenuItemId,
                Name = t.MenuItemName,
                Quantity = t.Quantity,
                Amount = t.Quantity * t.Price,
                GroupName = ""
            }).ToList();
            return result;
        }

        private List<ItemSale> GetOrderTags(Order order)
        {
            if (string.IsNullOrEmpty(order.OrderTags)) return new List<ItemSale>();

            var orderTagIems = JsonConvert.DeserializeObject<List<OrderTagValue>>(order.OrderTags);

            return orderTagIems.Select(t => new ItemSale
            {
                Id = t.TagSync,
                Name = t.TagValue,
                Quantity = t.Quantity,
                Amount = t.Quantity * t.Price
            }).ToList();
        }

        #endregion ComboItem

        #region Item

        private async Task<List<MenuListDto>> GetAllMenuItemSales(GetItemInput input)
        {
            var orders = GetAllOrdersNotFilterByDynamicFilter(input);

            if (!input.VoidParam && !input.Gift && !input.Comp && !input.Refund)
            {
                orders = orders.Where(x => x.CalculatePrice && x.DecreaseInventory &&  (
                    string.IsNullOrEmpty(x.OrderStates)
                    || (!x.OrderStates.Contains("Void")
                        && !x.OrderStates.Contains("Gift")
                        && !x.OrderStates.Contains("Comp")
                        && !x.OrderStates.Contains("Refund"))));
            }
            var orderList = await orders.ToListAsync();

            var output = orderList.GroupBy(c => new { c.MenuItemPortionId, Location = c.Location_Id })
                .Select(g => new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });

            var outputOrderTemps = output.Select(o => new
            {
                Location = o.Location,
                MenuItemPortion = o.MenuItemPortionId,
                Items = ObjectMapper.Map<List<OrderListDto>>(o.Items.ToList())
            });

            var menuListOutputDots = new List<MenuListDto>();

            var dicLocations = new Dictionary<int, string>();
            var dicCategory = new Dictionary<int, string>();
            var dicGroups = new Dictionary<int, string>();
            var aliasCodes = new Dictionary<int, string>();
            var portionIds = new Dictionary<int, string>();

            foreach (var dto in outputOrderTemps)
            {
                var firObj = dto.Items.First();
                if (firObj != null)
                    try
                    {
                        var sumQuantity = dto.Items.Sum(b => b.Quantity);
                        var lname = "";
                        if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                        {
                            lname = dicLocations[dto.Location];
                        }
                        else
                        {
                            var lSer = _locationRepo.Get(dto.Location);
                            if (lSer == null) continue;

                            lname = lSer.Name;
                            dicLocations.Add(dto.Location, lname);
                        }

                        var menuItem = firObj.MenuItemId;
                        var cateDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, menuItem);
                        var menL = new MenuListDto
                        {
                            AliasCode = cateDto.AliasCode,
                            LocationName = lname,
                            MenuItemPortionId = dto.MenuItemPortion,
                            MenuItemPortionName = firObj.PortionName,
                            MenuItemId = firObj.MenuItemId,
                            MenuItemName = firObj.MenuItemName,
                            CategoryId = cateDto.CatId,
                            CategoryName = cateDto.CatgoryName,
                            GroupId = cateDto.GroupId,
                            GroupName = cateDto.GroupName,
                            Quantity = dto.Items.Sum(a => a.Quantity),
                            Tags = firObj.Tags,
                            Price = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.Price) / sumQuantity) : 0,
                            TaxPrice = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0
                        };

                        var allTags = dto.Items.SelectMany(t => t.TransactionOrderTags)
                            .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId == 0);

                        if (allTags.Any())
                        {
                            menL.OrderTagPrice = sumQuantity != 0 ? (allTags.Sum(a => a.Price * a.Quantity) / sumQuantity) : 0;
                            menL.IsOrderTag = false;

                            if (input.IsItemSalesReport)
                            {
                                var menuOrderTag = new MenuListDto
                                {
                                    IsOrderTag = true,
                                    AliasCode = cateDto.AliasCode,
                                    LocationName = lname,
                                    MenuItemPortionId = dto.MenuItemPortion,
                                    MenuItemPortionName = firObj.PortionName,
                                    MenuItemId = firObj.MenuItemId,
                                    MenuItemName = firObj.MenuItemName,
                                    CategoryId = cateDto.CatId,
                                    CategoryName = cateDto.CatgoryName,
                                    GroupId = cateDto.GroupId,
                                    GroupName = cateDto.GroupName,
                                    Tags = firObj.Tags
                                };
                                menuOrderTag.Quantity = dto.Items
                                    .Where(e => allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity);
                                menuOrderTag.OrderTagPrice = menL.OrderTagPrice;
                                menuOrderTag.Price =
                                 menuOrderTag.Quantity != 0 ? (dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(a => a.Quantity * a.Price) / menuOrderTag.Quantity) : 0 +
                                    menuOrderTag.OrderTagPrice;
                                menuOrderTag.TaxValue =
                                    dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(c => c.GetTaxablePrice()) - menuOrderTag.Price * menuOrderTag.Quantity;
                                menuOrderTag.TaxPrice =
                                  sumQuantity != 0 ? (dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0;

                                menuListOutputDots.Add(menuOrderTag);

                                menL.OrderTagPrice = 0;
                                menL.Quantity = dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                    .Sum(a => a.Quantity);
                                menL.Price =
                                    menL.Quantity != 0 ? (dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity * a.Price) / menL.Quantity) : 0
                                        + menL.OrderTagPrice;
                                menL.TaxValue =
                                    dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(c => c.GetTaxablePrice()) - menL.Price * menL.Quantity;
                            }
                        }

                        if (!input.IsItemSalesReport || !allTags.Any())
                        {
                            var finalPrice = menL.Price + menL.OrderTagPrice;
                            menL.Price = finalPrice;

                            var taxvalue = dto.Items.Sum(c => c.GetTaxablePrice()) - finalPrice * menL.Quantity;
                            menL.TaxValue = taxvalue;
                        }

                        menuListOutputDots.Add(menL);

                        var itemTags = dto.Items.SelectMany(t => t.TransactionOrderTags)
                            .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId > 0);
                        foreach (var transactionOrderTag in itemTags)
                        {
                            var portionId = transactionOrderTag.MenuItemPortionId;

                            var lastMenuListDto =
                                menuListOutputDots.LastOrDefault(a => a.MenuItemPortionId == portionId);
                            if (lastMenuListDto == null)
                            {
                                var myItemid = 0;
                                var portionName = "";

                                if (portionIds.Any() && portionIds.ContainsKey(portionId))
                                {
                                    var portionNameSplit = portionIds[menuItem];
                                    var splitName = portionNameSplit.Split('@');
                                    if (splitName.Any())
                                    {
                                        myItemid = Convert.ToInt32(splitName[0]);
                                        portionName = splitName[1];
                                    }
                                }
                                else
                                {
                                    var lSer = _menuItemPortionRepository.Get(portionId);
                                    if (lSer != null)
                                    {
                                        myItemid = lSer.MenuItemId;
                                        portionName = lSer.Name;
                                        portionIds.Add(lSer.Id, myItemid + "@" + portionName);
                                    }
                                }

                                var myCategoryDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, myItemid);
                                var myTagList = new MenuListDto
                                {
                                    IsOrderTag = true,
                                    AliasCode = myCategoryDto.AliasCode,
                                    LocationName = lname,
                                    MenuItemPortionId = portionId,
                                    MenuItemPortionName = portionName,
                                    MenuItemId = myItemid,
                                    MenuItemName = transactionOrderTag.TagValue,
                                    CategoryId = myCategoryDto.CatId,
                                    CategoryName = myCategoryDto.CatgoryName,
                                    GroupId = myCategoryDto.GroupId,
                                    GroupName = myCategoryDto.GroupName,
                                    Quantity = transactionOrderTag.Quantity,
                                    Tags = "",
                                    Price = transactionOrderTag.Price,
                                    TaxPrice = sumQuantity != 0 ? (dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) / sumQuantity) : 0
                                };
                                menuListOutputDots.Add(myTagList);
                            }
                            else
                            {
                                var tagQuantity = transactionOrderTag.Quantity;
                                var myOrder = dto.Items.LastOrDefault(a => a.Id == transactionOrderTag.OrderId);

                                if (myOrder != null)
                                {
                                    tagQuantity = myOrder.Quantity * transactionOrderTag.Quantity;
                                }

                                lastMenuListDto.Price =
                                    (lastMenuListDto.Quantity + tagQuantity) != 0 ?
                                    (lastMenuListDto.Price * lastMenuListDto.Quantity +
                                     transactionOrderTag.Price * tagQuantity) /
                                    (lastMenuListDto.Quantity + tagQuantity)
                                    : 0;
                                lastMenuListDto.Quantity += tagQuantity;

                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        var mess = exception.Message;
                    }
            }
            // filter by dynamic filter
            var dataAsQueryable = menuListOutputDots.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }

            menuListOutputDots = dataAsQueryable.ToList();
            return menuListOutputDots;
        }

        public async Task<FileDto> ExportItemsAndOrderTagToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.ExportOutputType = ExportType.Excel;
                input.IsItemSalesReport = true;
                if (!input.RunInBackground)
                {
                    var output = await GetItemSales(input);
                    return _itemReportExcelExporter.ExportToFileItemAndOrderTagSales(input, output);
                }
            }

            return null;
        }

        public async Task<ItemStatsDto> GetItemSales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (var unitOfWork = _unitOfWorkManager.Begin(new UnitOfWorkOptions()
            {
                Scope = System.Transactions.TransactionScopeOption.RequiresNew,
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
                IsTransactional = false
            }))
            {
                using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
                {
                    var menuListOutputDots = await GetAllMenuItemSales(input);

                    if (input.Stats)
                    {
                        returnOutput.Quantities = GetItemByQuantity(menuListOutputDots.AsQueryable());
                        returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                        returnOutput.Totals = GetItemByTotal(menuListOutputDots.AsQueryable());
                        returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                    }

                    if (input.NoSales) menuListOutputDots = GetNoSales(menuListOutputDots);

                    menuListOutputDots = menuListOutputDots.AsQueryable().OrderBy(input.Sorting).ToList();

                    var totalCount = menuListOutputDots.Count();

                    if (input.IsSummary)
                        menuListOutputDots = menuListOutputDots.AsQueryable().PageBy(input).ToList();

                    returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                        totalCount,
                        menuListOutputDots
                    );
                }
            }

            return returnOutput;
        }

        #endregion Item

        #region Price

        public async Task<FileDto> ExportItemSalesByPriceToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.ExportOutputType = ExportType.Excel;

                if (!input.RunInBackground)
                {
                    input.Sorting = "CategoryName, MenuItemName";
                    var output = await GetItemSalesByPrice(input);
                    return _itemReportExcelExporter.ExportToFileItemsByPrice(input, output);
                }
            }

            return null;
        }

        public async Task<ItemStatsDto> GetItemSalesByPrice(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var orderList = await orders.ToListAsync();

                var output = orderList.GroupBy(c => new { c.MenuItemPortionId, c.Price })
                    .Select(g => new { MenuItemPortion = g.Key.MenuItemPortionId, g.Key.Price, Items = g });

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();
                var dicGroups = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var catId = 0;
                            var catgoryName = "";
                            var groupName = "";
                            var groupId = 0;
                            var menuItem = firObj.MenuItemId;
                            var lSer = await _menuItemRepo.GetAllIncluding(m => m.Category.ProductGroup).FirstOrDefaultAsync(m => m.Id == menuItem);

                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId;
                                    catgoryName = lSer.Category?.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
                            {
                                var catSplitname = dicGroups[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    groupId = Convert.ToInt32(splitName[0]);
                                    groupName = splitName[1];
                                }
                            }
                            else
                            {
                                if (lSer != null)
                                {
                                    groupId = lSer.Category?.ProductGroupId ?? 0;
                                    groupName = lSer.Category?.ProductGroup?.Name;
                                    dicGroups.Add(menuItem, groupId + "@" + groupName);
                                }
                            }

                            outputDtos.Add(new MenuListDto
                            {
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                GroupName = groupName
                            });
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                // filter by dynamic
                var dataAsQueryable = outputDtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                outputDtos = dataAsQueryable.ToList();

                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(dataAsQueryable);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(dataAsQueryable);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                if (!input.Sorting.Equals("Id"))
                    outputDtos = dataAsQueryable.OrderBy(input.Sorting).ToList();

                returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                    outputDtos.Count(),
                    outputDtos
                );
            }

            return returnOutput;
        }

        #endregion Price

        #region portion

        public async Task<FileDto> ExportItemsPortionToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (!input.RunInBackground)
                {
                    input.ExportOutputType = ExportType.Excel;

                    var output = await GetPortionSales(input);
                    return _itemReportExcelExporter.ExportToFileItemsPortion(input, output);
                }
            }

            return null;
        }

        private async Task<PortionSalesOutput> GetPortionSales(GetItemInput input)
        {
            var rOutput = new PortionSalesOutput();

            var output = await _categoryRepository.GetAll().OrderBy(a => a.Id)
                .SelectMany(c => c.MenuItems.OrderBy(a => a.Id).Select(mi => new PortionSales
                {
                    CategoryId = c.Id,
                    Category = c.Name,
                    AliasCode = mi.AliasCode,
                    AliasName = mi.AliasName,
                    MenuItemId = mi.Id,
                    MenuItemName = mi.Name
                }))
                .ToListAsync();

            var getAllItemSales = input;
            getAllItemSales.MaxResultCount = 0;

            var itemSales = await GetAllMenuItemSales(getAllItemSales);

            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
            {
                foreach (var iSale in itemSales.GroupBy(a => new { a.MenuItemPortionId }))
                {
                    var mportion = _menuItemPortionRepository.Get(iSale.Key.MenuItemPortionId);
                    var menuId = mportion.MenuItemId;
                    var menuItem = mportion.MenuItem;

                    var cObje = output.FirstOrDefault(a => a.MenuItemId == menuId);
                    if (cObje == null)
                    {
                        cObje = new PortionSales
                        {
                            AliasCode = menuItem.AliasCode,
                            AliasName = menuItem.AliasName,
                            Category = menuItem.Category.Name,
                            MenuItemId = menuItem.Id,
                            MenuItemName = menuItem.Name
                        };
                        output.Add(cObje);
                    }

                    cObje.AddToPortion(mportion.Id, iSale.Sum(a => a.Quantity),
                       iSale.Sum(a => a.Quantity) == 0 ? 0 : iSale.Sum(a => a.Price * a.Quantity) / iSale.Sum(a => a.Quantity), mportion.Price, mportion.Name);
                    rOutput.PortionNames.Add(mportion.Name);
                }
            }

            rOutput.Portions = output.OrderBy(a => a.Category).ToList();
            rOutput.PortionNames = _menuItemPortionRepository.GetAll().Select(a => a.Name).Distinct().ToList();
            return rOutput;
        }

        #endregion portion

        #region Export to excel

        public async Task<FileDto> ExportItemsExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;

                if (!input.RunInBackground)
                {
                    input.ExportOutputType = ExportType.Excel;

                    var startOfDate = input.StartDate;
                    var endOfDate = input.EndDate;

                    if (input.Duration != null && input.Duration.Equals("M"))
                    {
                        var myDate = input.StartDate;
                        startOfDate = new DateTime(myDate.Year, myDate.Month, 1);
                        endOfDate = startOfDate.AddMonths(1);
                    }

                    var exportInput = new GetDayItemInput()
                    {
                        StartDate = startOfDate,
                        EndDate = endOfDate,
                        Void = input.VoidParam,
                        Gift = input.Gift,
                        Comp = input.Comp,
                        Locations = input.Locations,
                        Sorting = "CategoryName",
                        Stats = false,
                        FilterByCategory = input.FilterByCategory,
                        FilterByDepartment = input.FilterByDepartment,
                        FilterByGroup = input.FilterByGroup,
                        LocationGroup = input.LocationGroup,
                        FilterByTag = input.FilterByTag,
                        MenuItemIds = input.MenuItemIds,
                        MenuItemPortionIds = input.MenuItemPortionIds,
                        DynamicFilter = input.DynamicFilter,
                        TenantId = AbpSession.TenantId.Value
                    };

                    if (input.Locations == null || !input.Locations.Any())
                        if (input.LocationGroup != null && !input.LocationGroup.Group)
                            input.Locations = input.LocationGroup.Locations;
                    if (input.ByLocation)
                    {
                        var locationsByUser = _locationService.GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = input.UserId });

                        exportInput.ByLocation = true;
                        exportInput.LocationsByUser = locationsByUser;
                        exportInput.TenantId = input.TenantId.Value;
                        exportInput.UserId = input.UserId;
                    }

                    var output = await GetDaysItemSales(exportInput);
                    var total = output.Items.SelectMany(i => i.Items).Sum(c => c.Quantity);

                    return _itemReportExcelExporter.ExportToFileItems(input, output);
                }
            }

            return null;
        }

        private async Task<IdDayItemOutput> GetDaysItemSales(GetDayItemInput input)
        {
            var returnOutput = new IdDayItemOutput();

            var retrDates = new List<DateTime>();
            var output = new List<IdDayItemOutputDto>();
            returnOutput.Items = output;

            if (input.AllDates != null && input.AllDates.Any())
            {
                retrDates.AddRange(input.AllDates);
            }
            else if (input.StartDate > DateTime.MinValue)
            {
                var sDate = input.StartDate;
                var eDate = input.EndDate;
                while (true)
                {
                    retrDates.Add(sDate);
                    sDate = sDate.AddDays(1);
                    if (sDate > eDate) break;
                }
            }

            if (retrDates.Any())
                foreach (var retrDate in retrDates)
                {
                    var getItemInput = new GetItemInput
                    {
                        StartDate = retrDate.Date,
                        EndDate = retrDate.Date.AddDays(1).AddTicks(-1),
                        Comp = input.Comp,
                        VoidParam = input.Void,
                        Gift = input.Gift,
                        Locations = input.Locations,
                        Location = input.Location,
                        Sorting = "Id",
                        Stats = input.Stats,
                        TenantId = input.TenantId,
                        UserId = input.UserId,
                        FilterByCategory = input.FilterByCategory,
                        FilterByDepartment = input.FilterByDepartment,
                        FilterByGroup = input.FilterByGroup,
                        LocationGroup = input.LocationGroup,
                        FilterByTag = input.FilterByTag,
                        MenuItemIds = input.MenuItemIds,
                        MenuItemPortionIds = input.MenuItemPortionIds,
                        DynamicFilter = input.DynamicFilter
                    };
                    var allSales = await GetItemSales(getItemInput);

                    ProcessSales(output, input, allSales, retrDate.ToString("yyyy-MM-dd"));
                }

            foreach (var retrDate in retrDates) returnOutput.AllDates.Add(retrDate.ToString("yyyy-MM-dd"));

            if (input.ByLocation && input.LocationsByUser != null)
                returnOutput.AllLocations = input.LocationsByUser.ToDictionary(e => e.Code, e => e.Name);
            else
            {
                returnOutput.AllLocations = await _locationRepo.GetAll().ToDictionaryAsync(e => e.Code, e => e.Name);
            }

            foreach (var allI in returnOutput.Items)
                foreach (var datee in returnOutput.AllDates)
                {
                    var aa = allI.Items.SingleOrDefault(a => a.Date.Equals(datee));
                    if (aa == null)
                    {
                        aa = new DayWiseItemDto
                        {
                            Date = datee,
                            Price = 0M,
                            Quantity = 0M
                        };
                        allI.Items.Add(aa);
                    }
                }

            if (returnOutput.Items.Any())
                returnOutput.Items = returnOutput.Items
                    .WhereIf(input.MenuItemIds.Any(), a => input.MenuItemIds.Contains(a.MenuItemId))
                    .OrderBy(a => a.MenuItemId).ToList();
            return returnOutput;
        }

        private void ProcessSales(List<IdDayItemOutputDto> output, GetDayItemInput input, ItemStatsDto allSales, string StartDate)
        {
            foreach (var allSale in allSales.MenuList.Items)
            {
                IdDayItemOutputDto por = null;

                if (input != null && input.ByLocation)
                    por =
                        output.SingleOrDefault(a =>
                            a.PortionId == allSale.MenuItemPortionId && a.LocationName != null &&
                            a.LocationName.Equals(allSale.LocationName));
                else
                    por = output.SingleOrDefault(
                        a => a.PortionId == allSale.MenuItemPortionId);

                if (por == null)
                {
                    por = new IdDayItemOutputDto
                    {
                        IsOrderTag = allSale.IsOrderTag,
                        MenuItemId = allSale.MenuItemId,
                        MenuItemName = allSale.MenuItemName,
                        PortionId = allSale.MenuItemPortionId,
                        PortionName = allSale.MenuItemPortionName,
                        CategoryId = allSale.CategoryId,
                        CategoryName = allSale.CategoryName,
                        AliasCode = allSale.AliasCode
                    };
                    if (input.ByLocation)
                        por.LocationName = allSale.LocationName;

                    output.Add(por);
                }

                var dd = por.Items.SingleOrDefault(a => a.Date.Equals(StartDate));
                if (dd == null)
                {
                    por.Items.Add(new DayWiseItemDto
                    {
                        Date = StartDate,
                        Quantity = allSale.Quantity,
                        Price = allSale.Price
                    });
                }
                else
                {
                    dd.Price = (dd.Quantity * dd.Price +
                                allSale.Quantity * allSale.Price) / (dd.Quantity + allSale.Quantity);
                    dd.Quantity += allSale.Quantity;
                }
            }
        }

        #endregion Export to excel

        #region Item Tag

        public async Task<ItemTagStatsDto> BuildItemTagSalesReport(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
            {
                var itemSales = await GetItemSalesNotFilterByDynamicFilter(input);

                var menuListDtos = itemSales.MenuList.Items
                    .SelectMany(
                        mi => mi.TagList.Select(t => new MenuListDto
                        {
                            MenuItemId = mi.MenuItemId,
                            AliasCode = mi.AliasCode,
                            MenuItemName = mi.MenuItemName,
                            CategoryId = mi.CategoryId,
                            DepartmentName = mi.DepartmentName,
                            GroupId = mi.GroupId,
                            MenuItemPortionId = mi.MenuItemPortionId,
                            MenuItemPortionName = mi.MenuItemPortionName,
                            Tag = t,
                            Price = mi.Price,
                            Quantity = mi.Quantity,
                            OrderTagPrice = mi.OrderTagPrice,
                            LocationName = mi.LocationName,
                            Hour = mi.Hour
                        })).Distinct()
                    .ToList();

                return GetItemByTag(input, menuListDtos);
            }
        }

        public async Task<FileDto> ExportItemTagSalesToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
                input.IsExport = true;

                if (!input.RunInBackground)
                {
                    var output = await BuildItemTagSalesReport(input);

                    return _itemReportExcelExporter.ExportToFileItemTagSales(input, output);
                }
            }

            return null;
        }

        private async Task<ItemStatsDto> GetItemSalesNotFilterByDynamicFilter(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var orderList = await orders.ToListAsync();

                var output = orderList.GroupBy(c => new { c.MenuItemPortionId, Location = c.Location_Id })
                    .Select(g => new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });

                var menuListOutputDots = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();
                var dicGroups = new Dictionary<int, string>();
                var aliasCodes = new Dictionary<int, string>();
                var portionIds = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();
                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _locationRepo.Get(dto.Location);
                                if (lSer == null) continue;

                                lname = lSer.Name;
                                dicLocations.Add(dto.Location, lname);
                            }

                            var menuItem = firObj.MenuItemId;
                            var cateDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, menuItem);
                            var menL = new MenuListDto
                            {
                                AliasCode = cateDto.AliasCode,
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortionId,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = cateDto.CatId,
                                CategoryName = cateDto.CatgoryName,
                                GroupId = cateDto.GroupId,
                                GroupName = cateDto.GroupName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Tags = firObj.MenuItem?.Tag,
                                TotalAmount = dto.Items.Sum(a => a.GetTotal()),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                TaxPrice = dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) /
                                           dto.Items.Sum(b => b.Quantity)
                            };

                            var allTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId == 0);

                            if (allTags.Any())
                            {
                                menL.OrderTagPrice =
                                    allTags.Sum(a => a.Price * (a.Order?.Quantity ?? 1)) /
                                    dto.Items.Sum(b => b.Quantity);
                                menL.IsOrderTag = false;

                                if (input.IsItemSalesReport)
                                {
                                    var menuOrderTag = new MenuListDto
                                    {
                                        IsOrderTag = true,
                                        AliasCode = cateDto.AliasCode,
                                        LocationName = lname,
                                        MenuItemPortionId = dto.MenuItemPortionId,
                                        MenuItemPortionName = firObj.PortionName,
                                        MenuItemId = firObj.MenuItemId,
                                        MenuItemName = firObj.MenuItemName,
                                        CategoryId = cateDto.CatId,
                                        CategoryName = cateDto.CatgoryName,
                                        GroupId = cateDto.GroupId,
                                        GroupName = cateDto.GroupName,
                                        Tags = firObj.MenuItem?.Tag
                                    };
                                    menuOrderTag.Quantity = dto.Items
                                        .Where(e => allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity);
                                    menuOrderTag.OrderTagPrice = menL.OrderTagPrice;
                                    menuOrderTag.Price =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.Price) / menuOrderTag.Quantity +
                                        menuOrderTag.OrderTagPrice;
                                    menuOrderTag.TaxValue =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(c => c.GetTaxablePrice()) - menuOrderTag.Price * menuOrderTag.Quantity;
                                    menuOrderTag.TaxPrice =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.GetTaxTotal()) / dto.Items.Sum(b => b.Quantity);

                                    menuListOutputDots.Add(menuOrderTag);

                                    menL.OrderTagPrice = 0;
                                    menL.Quantity = dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(a => a.Quantity);
                                    menL.Price =
                                        dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.Price) / menL.Quantity + menL.OrderTagPrice;
                                    menL.TaxValue =
                                        dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(c => c.GetTaxablePrice()) - menL.Price * menL.Quantity;
                                }
                            }

                            if (!input.IsItemSalesReport || !allTags.Any())
                            {
                                var finalPrice = menL.Price + menL.OrderTagPrice;
                                menL.Price = finalPrice;

                                var taxvalue = dto.Items.Sum(c => c.GetTaxablePrice()) - finalPrice * menL.Quantity;
                                menL.TaxValue = taxvalue;
                            }

                            menuListOutputDots.Add(menL);

                            var itemTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId > 0);
                            foreach (var transactionOrderTag in itemTags)
                            {
                                var portionId = transactionOrderTag.MenuItemPortionId;

                                var lastMenuListDto =
                                    menuListOutputDots.LastOrDefault(a => a.MenuItemPortionId == portionId);
                                if (lastMenuListDto == null)
                                {
                                    var myItemid = 0;
                                    var portionName = "";

                                    if (portionIds.Any() && portionIds.ContainsKey(portionId))
                                    {
                                        var portionNameSplit = portionIds[menuItem];
                                        var splitName = portionNameSplit.Split('@');
                                        if (splitName.Any())
                                        {
                                            myItemid = Convert.ToInt32(splitName[0]);
                                            portionName = splitName[1];
                                        }
                                    }
                                    else
                                    {
                                        var lSer = _menuItemPortionRepository.Get(portionId);
                                        if (lSer != null)
                                        {
                                            myItemid = lSer.MenuItemId;
                                            portionName = lSer.Name;
                                            portionIds.Add(lSer.Id, myItemid + "@" + portionName);
                                        }
                                    }

                                    var myCategoryDto = await GetCategoryDto(dicCategory, dicGroups, aliasCodes, myItemid);
                                    var myTagList = new MenuListDto
                                    {
                                        IsOrderTag = true,
                                        AliasCode = myCategoryDto.AliasCode,
                                        LocationName = lname,
                                        MenuItemPortionId = portionId,
                                        MenuItemPortionName = portionName,
                                        MenuItemId = myItemid,
                                        MenuItemName = transactionOrderTag.TagValue,
                                        CategoryId = myCategoryDto.CatId,
                                        CategoryName = myCategoryDto.CatgoryName,
                                        GroupId = myCategoryDto.GroupId,
                                        GroupName = myCategoryDto.GroupName,
                                        Quantity = transactionOrderTag.Quantity,
                                        Tags = "",
                                        Price = transactionOrderTag.Price,
                                        TaxPrice = dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) /
                                                   dto.Items.Sum(b => b.Quantity)
                                    };
                                    menuListOutputDots.Add(myTagList);
                                }
                                else
                                {
                                    lastMenuListDto.Quantity += transactionOrderTag.Quantity;
                                    lastMenuListDto.Price =
                                        (lastMenuListDto.Price * lastMenuListDto.Quantity +
                                         transactionOrderTag.Price * transactionOrderTag.Quantity) /
                                        (lastMenuListDto.Quantity + transactionOrderTag.Quantity);
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) menuListOutputDots = GetNoSales(menuListOutputDots);

                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                if (!input.Sorting.Equals("Id"))
                    menuListOutputDots = menuListOutputDots.AsQueryable().OrderBy(input.Sorting).ToList();

                var totalCount = menuListOutputDots.Count();

                if (input.IsSummary)
                    menuListOutputDots = menuListOutputDots.AsQueryable().PageBy(input).ToList();

                returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                    totalCount,
                    menuListOutputDots
                );
            }

            return returnOutput;
        }

        private List<ChartOutputDto> GetItemByQuantity(IQueryable<Order> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        private List<ChartOutputDto> GetItemByTotal(IQueryable<Order> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Price * c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        private ItemTagStatsDto GetItemByTag(GetItemInput input, List<MenuListDto> menuListDtos)
        {
            var groups = new List<string>();
            var chartOutput = new List<BarChartOutputDto>();
            var oDto = new List<ItemTagReportDto>();

            if (menuListDtos != null && menuListDtos.Any())
            {
                var groupDto = menuListDtos.GroupBy(i => i.Tag).Select(g => new { Tag = g.Key, Items = g });

                foreach (var dto in groupDto)
                    if (dto.Tag != null)
                    {
                        var quantity = dto.Items.Sum(a => a.Quantity);
                        var total = dto.Items.Sum(a => a.Quantity * a.Price);
                        oDto.Add(new ItemTagReportDto
                        {
                            TagName = dto.Tag,
                            Quantity = quantity,
                            Total = total,
                            TotalItems = dto.Items.ToList()
                        });
                    }
            }

            var dataAsQueryable = oDto.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
            }

            oDto = dataAsQueryable.ToList();
            groups = oDto.Select(x => x.TagName).ToList();
            foreach (var item in oDto)
                chartOutput.Add(new BarChartOutputDto
                {
                    name = item.TagName,
                    data = new List<decimal> { item.Total }
                });

            var menuItemCount = oDto.Count();
            var itemTags = oDto.ToList();

            if (!input.IsExport) itemTags = oDto.AsQueryable().PageBy(input).ToList();

            var result = new ItemTagStatsDto
            {
                ItemTagList = new PagedResultDto<ItemTagReportDto>(menuItemCount, itemTags),
                ItemTags = groups,
                ChartOutput = chartOutput
            };
            return result;
        }

        #endregion Item Tag

        #region Top down

        public async Task<ItemStatsDto> BuldTopOrBottomItemSales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization, AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                var orderList = await orders.ToListAsync();

                var output = orderList.GroupBy(c => new { c.MenuItemPortionId, Location = c.Location_Id })
                    .Select(g => new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });
                //var rsMenuItems = await _mrRepo.GetAllListAsync();
                //var rsCategory = await _catRepository.GetAllListAsync();

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();

                var totRecordCount = output.Count();
                var loopCnt = 0;

                foreach (var dto in output)
                {
                    loopCnt++;
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = await _locationRepo.GetAsync(dto.Location);
                                if (lSer != null)
                                {
                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }
                            }

                            var catId = 0;
                            var catgoryName = "";

                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = await _menuItemRepo.GetAllIncluding(m => m.Category.ProductGroup).FirstOrDefaultAsync(m => m.Id == menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            var menL = new MenuListDto
                            {
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortionId,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity)
                            };
                            menL.Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity);
                            var allTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice);
                            if (allTags.Any())
                                menL.OrderTagPrice =
                                    allTags.Sum(a => a.Price * (a.Order != null ? a.Order.Quantity : 1)) /
                                    dto.Items.Sum(b => b.Quantity);

                            var finalPrice = menL.Price + menL.OrderTagPrice;
                            menL.Price = finalPrice;
                            outputDtos.Add(menL);
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                // filter by DynamicFilter

                var dataAsQueryable = outputDtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                var totalCount = dataAsQueryable.Count() >= input.TakeNumber ? input.TakeNumber : dataAsQueryable.Count();
                dataAsQueryable = dataAsQueryable.OrderBy(input.TakeString).ThenBy(input.Sorting).Take(input.TakeNumber);

                var outputPage = dataAsQueryable.ToList();
                if (!input.IsExport)
                    outputPage = dataAsQueryable.PageBy(input).ToList();

                returnOutput.MenuList = new PagedResultDto<MenuListDto>(
                    totalCount,
                    outputPage
                );
            }

            return returnOutput;
        }

        public async Task<FileDto> ExportTopOrBottomItemSalesToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
                input.IsExport = true;
                input.OutputType = "EXPORT";

                if (!input.RunInBackground)
                {
                    var output = await BuldTopOrBottomItemSales(input);

                    return _itemReportExcelExporter.ExportTopOrBottomItemSales(input, output.MenuList.Items.ToList());
                }
            }

            return null;
        }

        #endregion Top down
    }
}
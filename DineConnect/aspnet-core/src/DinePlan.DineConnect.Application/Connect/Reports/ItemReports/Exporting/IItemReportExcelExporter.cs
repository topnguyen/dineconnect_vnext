﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ItemReports.Exporting
{
    public interface IItemReportExcelExporter
    {
        FileDto ExportComboItemsSaleToFile(List<ItemSale> items, GetItemInput input);

        FileDto ExportToFileItemAndOrderTagSales(GetItemInput input, ItemStatsDto output);

        FileDto ExportToFileItems(GetItemInput input, IdDayItemOutput outPut);

        FileDto ExportToFileItemsByPrice(GetItemInput input, ItemStatsDto output);

        Task<FileDto> ExportToFileItemsHours(GetItemInput input, List<MenuListDto> dtos);

        FileDto ExportToFileItemsPortion(GetItemInput input, PortionSalesOutput output);

        FileDto ExportItemSalesForDepartmentExcel(GetItemInput input, ItemStatsDto items);

        FileDto ExportToFileItemTagSales(GetItemInput input, ItemTagStatsDto output);

        FileDto ExportTopOrBottomItemSales(GetItemInput input, List<MenuListDto> dtos);
    }
}
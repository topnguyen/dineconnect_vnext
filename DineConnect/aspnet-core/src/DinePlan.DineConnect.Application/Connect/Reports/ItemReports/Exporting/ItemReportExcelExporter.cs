﻿using Abp.AspNetZeroCore.Net;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.ItemReports.Exporting
{
    public class ItemReportExcelExporter : EpPlusExcelExporterBase, IItemReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        public const string NumberFormat = "0.00";

        private IRepository<Location> _locRepo;
        private IRepository<Brand> _comRepo;

        public ItemReportExcelExporter(ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService,
            IRepository<Location> locRepo,
            IRepository<Brand> comRepo
            ) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
            _locRepo = locRepo;
            _comRepo = comRepo;
        }

        public async Task<FileDto> ExportToFileItemsHours(GetItemInput input, List<MenuListDto> dtos)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();

            var fileDateSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateFormat)
                ? allSettings.FileDateTimeFormat.FileDateFormat
                : AppConsts.FileDateFormat;

            var startDate = !input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var endDate = !input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(fileDateSetting)
                : DateTime.Now.ToString(fileDateSetting);

            var fileName = $"ItemHourlySalesExcel-{startDate}-{endDate}";
            var file = new FileDto(fileName + ".xlsx",
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ItemHourlySale"));
                sheet.OutLineApplyStyle = true;

                ExportOrdersSummary(sheet, input, dtos);
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void ExportOrdersSummary(ExcelWorksheet sheet, GetItemInput input, List<MenuListDto> dtos)
        {
            var outItemHours = dtos.GroupBy(c => c.Hour).ToList();

            var hoursList = dtos.GroupBy(c => c.Hour)
                .Select(c => c.Key).Distinct().ToList();

            var outItem = dtos.GroupBy(c => new
            {
                c.MenuItemName,
                c.MenuItemPortionName,
                c.CategoryName
            }).Select(gcs => new
            {
                gcs.Key.CategoryName,
                gcs.Key.MenuItemName,
                gcs.Key.MenuItemPortionName,
                Children = gcs.ToList(),
            });

            var headers = new List<string>
                    {
                        L("Category"),
                        L("MenuItem"),
                        L("Portion"),
                    };
            foreach (var pos in hoursList)
            {
                headers.Add(pos.ToString());
                headers.Add("");
                headers.Add("");
            }

            AddHeader(sheet, headers.ToArray());

            var rowIndex = 2;

            var rowTitel = new List<string>()
                    {
                        "",
                        "",
                        ""
                    };
            foreach (var pos in hoursList)
            {
                rowTitel.Add("Quantity");
                rowTitel.Add("Price");
                rowTitel.Add("Amount");
            }
            AddRow(sheet, rowIndex, rowTitel.ToArray());
            rowIndex++;

            foreach (var pos in outItem)
            {
                var item = new List<string>();
                item.Add(pos.CategoryName);
                item.Add(pos.MenuItemName);
                item.Add(pos.MenuItemPortionName);
                foreach (var child in pos.Children.GroupBy(a => a.Hour))
                {
                    var colCount = (hoursList.IndexOf(child.Key) + 1) * 3;
                    item.Add(child.Sum(a => a.Quantity).ToString(NumberFormat));
                    item.Add(child.Sum(a => a.Price).ToString(NumberFormat));
                    item.Add(child.Sum(a => a.Quantity).ToString(NumberFormat));
                }
                AddRow(sheet, rowIndex, item.ToArray());
                rowIndex++;
            }

            // Footer
            var rowTotal = new List<string>();
            rowTotal.Add("Total");
            rowTotal.Add("");
            rowTotal.Add("");

            foreach (var old in outItemHours)
            {
                var colCount = (hoursList.IndexOf(old.Key) + 1) * 3 + 1;
                rowTotal.Add(old.Sum(a => a.Quantity).ToString(NumberFormat));
                rowTotal.Add("");
                rowTotal.Add(old.Sum(a => a.Total).ToString(NumberFormat));
            }
            AddRow(sheet, rowIndex, rowTotal.ToArray());

            for (var i = 1; i <= headers.Count; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }

        #region ComboItem

        public FileDto ExportComboItemsSaleToFile(List<ItemSale> items, GetItemInput input)
        {
            var file = new FileDto("ItemCombos_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ItemCombo"));
                sheet.OutLineApplyStyle = true;
                GetItemComboSales(sheet, input, items);
                SaveToFile(excelPackage, file);
            }

            return file;
        }

        private void GetItemComboSales(ExcelWorksheet sheet, GetItemInput input, List<ItemSale> itemSales)
        {
            int row = AddReportHeader(sheet, "Item Combo Sales Report", input);

            var headers = new List<string>
            {
                L("ItemCombo"),
                "",
                L("Group"),
                L("MenuItem"),
                L("Quantity"),
                L("Amount")
            };

            var output = itemSales.GroupBy(i => new { i.Location, i.LocationCode })
                .Select(g => new ItemComboSalesDto
                {
                    Location = g.Key.Location,
                    LocationCode = g.Key.LocationCode,
                    ItemSales = g.ToList()
                }).ToList();

            //var row = 5;

            foreach (var item in output)
            {
                var colCount = 1;
                row += 2;
                sheet.Cells[row, 1, row, 12].Merge = true;
                sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                sheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
                sheet.Cells[row, colCount].Value = $"Branch: {item.LocationCode} {item.Location} ";

                row++;
                AddHeader(sheet, row, headers.ToArray());
                foreach (var itemSale in item.ItemSales)
                {
                    row++;
                    colCount = 1;
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = itemSale.Name;
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = itemSale.Quantity;
                    sheet.Cells[row, colCount++].Value = Math.Round(itemSale.Amount, _roundDecimals, MidpointRounding.AwayFromZero);

                    var group = itemSale.OrderItems.GroupBy(o => o.GroupName)
                        .Select(g => new
                        {
                            GroupName = g.Key,
                            OrderItems = g.ToList()
                        }).ToList();
                    foreach (var g in group)
                    {
                        row++;

                        colCount = 1;
                        sheet.Cells[row, colCount++].Value = "";
                        sheet.Cells[row, colCount++].Value = "";
                        sheet.Cells[row, colCount++].Value = g.GroupName;

                        foreach (var orderItem in g.OrderItems)
                        {
                            row++;

                            colCount = 1;
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = orderItem.Name;
                            sheet.Cells[row, colCount++].Value = orderItem.Quantity;
                            sheet.Cells[row, colCount++].Value = Math.Round(orderItem.Amount, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                }

                row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 5].Value = "";
                sheet.Cells[row, 6].Value = Math.Round(item.ItemSales.Sum(t => t.Amount + t.OrderItems.Sum(o => o.Amount)), _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
            }

            for (var i = 1; i <= 11; i++) sheet.Column(i).AutoFit();
        }

        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";

            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                Brand myCompany = _comRepo.Get(myLocation.OrganizationId);
                if (myCompany != null)
                {
                    brandName = myCompany.Name;
                }
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }
            var row = 1;
            sheet.Cells[row, 1].Value = nameOfReport;
            sheet.Cells[row, 1, 1, 12].Merge = true;
            sheet.Cells[row, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[row, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            row++;
            sheet.Cells[row, 1].Value = L("Brand");
            sheet.Cells[row, 2].Value = brandName;

            sheet.Cells[row, 6].Value = "Business Date:";
            sheet.Cells[row, 7].Value = input.StartDate.ToString(_simpleDateFormat) + " to " + input.EndDate.ToString(_simpleDateFormat);

            row++;
            sheet.Cells[row, 1].Value = "Branch Name:";
            sheet.Cells[row, 2].Value = branch;
            sheet.Cells[row, 6].Value = "Printed On:";
            sheet.Cells[row, 7].Value = DateTime.Now.ToString(_dateTimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;
            row++;

            return row;
        }

        #endregion ComboItem

        #region Item

        public FileDto ExportToFileItemAndOrderTagSales(GetItemInput input, ItemStatsDto output)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ItemCombo"));
                sheet.OutLineApplyStyle = true;

                CreateFileForItemAndOrderTagSales(sheet, input, output);
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void CreateFileForItemAndOrderTagSales(ExcelWorksheet sheet, GetItemInput input, ItemStatsDto output)
        {
            var headers = new List<string>
            {
                L("AliasCode"),
                L("MenuItem"),
                L("Portion"),
                L("Quantity"),
                L("PriceExcludeTax"),
                L("AmountExcludeTax"),
                L("Tax"),
                L("AmountIncludeTax")
            };

            int rowCount = AddReportHeader(sheet, "Item Sales Report", input);

            rowCount++;
            AddHeader(sheet, rowCount, headers.ToArray());

            rowCount = CreateRowsForItemAndOrderTag(sheet, output.MenuList.Items.Where(i => !i.IsOrderTag).ToList(), rowCount);

            rowCount++;
            sheet.Cells[rowCount, 1].Value = "Order Tag Sales";
            sheet.Cells[rowCount, 1, rowCount, 5].Merge = true;
            sheet.Cells[rowCount, 1, rowCount, 30].Style.Font.Bold = true;
            rowCount++;

            AddHeader(sheet, rowCount, headers.ToArray());

            rowCount = CreateRowsForItemAndOrderTag(sheet, output.MenuList.Items.Where(i => i.IsOrderTag).ToList(), rowCount);

            for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
        }

        private int CreateRowsForItemAndOrderTag(ExcelWorksheet sheet, List<MenuListDto> output, int rowCount)
        {
            rowCount++;
            decimal amountET = 0M;
            decimal totalTax = 0M;
            decimal amountIT = 0M;

            foreach (var pos in output.GroupBy(e => new { e.MenuItemId, e.MenuItemPortionId }).Select(g => g.ToList()))
            {
                var colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = pos.FirstOrDefault().AliasCode;
                sheet.Cells[rowCount, colCount++].Value = pos.FirstOrDefault().MenuItemName;
                sheet.Cells[rowCount, colCount++].Value = pos.FirstOrDefault().MenuItemPortionName;
                sheet.Cells[rowCount, colCount++].Value = pos.Sum(p => p.Quantity);
                var priceExcludeTax = pos.Sum(p => p.Quantity) != 0 ? (pos.Sum(p => p.Price - p.TaxPrice)) * pos.Sum(p => p.Quantity) / pos.Sum(p => p.Quantity) : 0;
                var amountExcludeTax = priceExcludeTax * pos.Sum(p => p.Quantity);

                var tax = pos.Sum(p => p.Quantity) != 0 ? (pos.Sum(p => p.TaxPrice)) * pos.Sum(p => p.Quantity) / pos.Sum(p => p.Quantity) : 0;
                var amountIncludeTax = pos.FirstOrDefault().Price * pos.Sum(p => p.Quantity);

                amountET += amountExcludeTax;
                totalTax += tax;
                amountIT += amountIncludeTax;

                sheet.Cells[rowCount, colCount++].Value = Math.Round(priceExcludeTax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value = Math.Round(amountExcludeTax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value = Math.Round(tax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value = Math.Round(amountIncludeTax, _roundDecimals, MidpointRounding.AwayFromZero);

                rowCount++;
            }
            sheet.Cells[rowCount, 1].Value = "Total";
            sheet.Cells[rowCount, 4].Value = output.Sum(i => i.Quantity);
            sheet.Cells[rowCount, 6].Value = Math.Round(amountET, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 7].Value = Math.Round(totalTax, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 8].Value = Math.Round(amountIT, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 1, rowCount, 30].Style.Font.Bold = true;

            return rowCount + 1;
        }

        #endregion Item

        #region Price

        public FileDto ExportToFileItemsByPrice(GetItemInput input, ItemStatsDto output)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Summary"));
                sheet.OutLineApplyStyle = true;

                GetItemsSummaryByPrice(sheet, input, output);
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void GetItemsSummaryByPrice(ExcelWorksheet sheet, GetItemInput input, ItemStatsDto output)
        {
            input.Sorting = "MenuItemId";

            var headers = new List<string>
            {
                L("Category"),
                L("AliasCode"),
                L("MenuItem"),
                L("Portion"),
                L("Price"),
                L("Quantity"),
                L("Total")
            };

            int rowCount = AddReportHeader(sheet, "Item Price Summary Report", input);
            rowCount++;

            AddHeader(
                sheet, rowCount, headers.ToArray()
            );
            rowCount++;

            if (DynamicQueryable.Any(output.MenuList.Items))
            {
                MenuListDto myListDto = null;

                foreach (var pos in output.MenuList.Items)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.CategoryName;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasCode;

                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemPortionName;
                    sheet.Cells[rowCount, colCount++].Value = pos.Price;
                    sheet.Cells[rowCount, colCount++].Value = pos.Quantity;
                    sheet.Cells[rowCount, colCount++].Value = pos.Total;

                    if (myListDto != null && myListDto.MenuItemName.Equals(pos.MenuItemName))
                    {
                        sheet.Row(rowCount).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Row(rowCount).Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                    }

                    myListDto = pos;

                    rowCount++;
                }
            }

            for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();
        }

        #endregion Price

        #region Portion

        public FileDto ExportToFileItemsPortion(GetItemInput input, PortionSalesOutput output)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Summary"));
                sheet.OutLineApplyStyle = true;

                GetItemsSummary(sheet, input, output);
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void GetItemsSummary(ExcelWorksheet sheet, GetItemInput input, PortionSalesOutput output)
        {
            var headers = new List<string>
            {
                L("Category"),
                L("AliasCode"),
                L("AliasName"),
                L("MenuItem")
            };
            if (output != null && output.PortionNames.Any() && output.Portions.Any())
            {
                foreach (var pname in output.PortionNames)
                {
                    headers.Add(pname);
                    headers.Add("");
                    headers.Add("");
                }
                int rowCount = AddReportHeader(sheet, "Item Summary Report", input);
                rowCount++;

                AddHeader(sheet, rowCount, headers.ToArray());

                rowCount++;

                foreach (var pos in output.Portions)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.Category;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasCode;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    foreach (var portionName in output.PortionNames)
                    {
                        var dePos = pos.Details.SingleOrDefault(a => a.PortionName.Equals(portionName));
                        if (dePos != null)
                        {
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.TotalQuantity, _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.Price, _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.TotalQuantity * dePos.Price,
                                _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                        }
                    }

                    rowCount++;
                }

                for (var i = 1; i <= 100; i++) sheet.Column(i).AutoFit();
            }
        }

        #endregion Portion

        #region export to excel

        public FileDto ExportToFileItems(GetItemInput input, IdDayItemOutput output)
        {
            var builder = new StringBuilder();

            if (input.VoidParam)
                builder.Append(L("Void"));
            else if (input.Gift)
                builder.Append(L("Gift"));
            else if (input.Comp)
                builder.Append(L("Comp"));
            else
                builder.Append(L("Item"));
            if (input.ByLocation)
            {
                builder.Append("-");
                builder.Append(L("Location"));
                builder.Append("-");
                builder.Append(input.StartDate.Month);
            }
            else
            {
                if (input?.Duration != null && input.Duration.Equals("M"))
                {
                    builder.Append(L("Summary"));
                    builder.Append("-");
                    builder.Append(input.StartDate.Month);
                }
                else
                {
                    builder.Append("-");
                    builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                        ? input.StartDate.ToString(_simpleDateFormat)
                        : DateTime.Now.ToString(_simpleDateFormat));
                    builder.Append(" - ");
                    builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                        ? input.EndDate.ToString(_simpleDateFormat)
                        : DateTime.Now.ToString(_simpleDateFormat));
                }
            }

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                if (!input.ByLocation)
                    GetItemsForDate(excelPackage, input, output);
                else
                    GetItemsForLocation(excelPackage, input, output);
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void GetItemsForDate(ExcelPackage package, GetItemInput input, IdDayItemOutput output)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items"));
            sheet.OutLineApplyStyle = true;

            int rowCount = AddReportHeader(sheet, "Item Sales Report", input);
            rowCount = AddHeaderForDate(sheet, rowCount++, false, input, output);
            rowCount = CreateItemsForDateRows(input, sheet, output, rowCount, output.Items);

            for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
        }

        private int CreateItemsForDateRows(GetItemInput input, ExcelWorksheet sheet, IdDayItemOutput output, int rowCount, List<IdDayItemOutputDto> items)
        {
            var allDates = output.AllDates.ToArray();
            if (input.Duration.Equals("S"))
                foreach (var pos in items.GroupBy(a => a.CategoryName))
                    foreach (var tets in pos)
                    {
                        var colCount = 1;

                        sheet.Cells[rowCount, colCount++].Value = tets.CategoryName;
                        sheet.Cells[rowCount, colCount++].Value = tets.MenuItemName;
                        sheet.Cells[rowCount, colCount++].Value = tets.PortionName;

                        if (input.ExportOutput.Equals("T"))
                        {
                            sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Total);
                        }
                        else if (input.ExportOutput.Equals("Q"))
                        {
                            sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Quantity);
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Quantity);
                            sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Total);
                        }

                        rowCount++;
                    }
            else
                foreach (var pos in items)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.CategoryName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    sheet.Cells[rowCount, colCount++].Value = pos.PortionName;

                    foreach (var myDate in allDates)
                    {
                        var outputD = pos.Items.FirstOrDefault(a => a.Date.Equals(myDate));
                        if (outputD != null)
                        {
                            if (input.ExportOutput.Equals("T"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                            }
                            else if (input.ExportOutput.Equals("Q"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Quantity, _roundDecimals, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                if (outputD.Quantity > 0M)
                                {
                                    sheet.Cells[rowCount, colCount++].Value =
                                        Math.Round(outputD.Quantity, _roundDecimals, MidpointRounding.AwayFromZero);
                                    sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                                }
                                else
                                {
                                    sheet.Cells[rowCount, colCount++].Value = 0;
                                    sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                                }
                            }
                        }
                        else
                        {
                            if (input.ExportOutput.Equals("B"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = 0;
                                sheet.Cells[rowCount, colCount++].Value = 0;
                            }
                            else
                            {
                                sheet.Cells[rowCount, colCount++].Value = 0;
                            }
                        }
                    }

                    rowCount++;
                }

            var col = 1;
            sheet.Cells[rowCount, col++].Value = L("Total");
            sheet.Cells[rowCount, col++].Value = "";
            sheet.Cells[rowCount, col++].Value = "";
            var allSal = items.SelectMany(a => a.Items);
            if (!input.Duration.Equals("S"))
            {
                foreach (var allDate in output.AllDates)
                {
                    var sumQuantity = allSal.Where(a => a.Date.Equals(allDate));

                    if (input.ExportOutput.Equals("B"))
                    {
                        if (sumQuantity.Any())
                        {
                            var quA = sumQuantity.Sum(a => a.Quantity);
                            var toT = sumQuantity.Sum(a => a.Total);

                            sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, col++].Value = Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                    else
                    {
                        if (input.ExportOutput.Equals("T"))
                        {
                            var quA = sumQuantity.Sum(a => a.Total);
                            sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            var quA = sumQuantity.Sum(a => a.Quantity);
                            sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                }
            }
            else
            {
                var quA = allSal.Sum(a => a.Quantity);
                var toT = allSal.Sum(a => a.Total);
                if (input.ExportOutput.Equals("T"))
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                }
                else if (input.ExportOutput.Equals("Q"))
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, col++].Value = Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                }
            }

            return rowCount++;
        }

        private void GetItemsForLocation(ExcelPackage package, GetItemInput input, IdDayItemOutput output)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items"));
            sheet.OutLineApplyStyle = true;

            var rowCount = AddReportHeader(sheet, "Item Sales", input);

            rowCount = AddHeaderForLocation(sheet, rowCount++, false, input, output);

            CreateItemForLocationRows(input, sheet, output.Items, output, rowCount);

            for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();
        }

        private int AddHeaderForDate(ExcelWorksheet sheet, int rowCount, bool isOrderTag, GetItemInput input, IdDayItemOutput output)
        {
            var headers = new List<string>
            {
                L("Category"),
                L("MenuItem"),
                L("Portion")
            };

            if (isOrderTag)
            {
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
                rowCount += 2;

                sheet.Cells[rowCount, 1].Value = "Order Tag Item";
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Merge = true;
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
            }

            if (input.Duration.Equals("S"))
            {
                if (input.ExportOutput.Equals("T"))
                {
                    headers.Add(L("Total"));
                }
                else if (input.ExportOutput.Equals("Q"))
                {
                    headers.Add(L("Quantity"));
                }
                else
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                rowCount++;
                AddHeader(
                    sheet, rowCount, headers.ToArray()
                );

                rowCount++;
            }
            else
            {
                if (input.ExportOutput.Equals("B"))
                {
                    foreach (var allDate in output.AllDates)
                    {
                        headers.Add(allDate);
                        headers.Add("");
                    }

                    rowCount++;
                    AddHeader(
                        sheet, rowCount, headers.ToArray()
                    );
                    rowCount++;
                    headers = new List<string>
                    {
                        "",
                        "",
                        ""
                    };

                    foreach (var allDate in output.AllDates)
                    {
                        headers.Add(L("Quantity"));
                        headers.Add(L("Total"));
                    }

                    var colCount = 1;
                    foreach (var t in headers) sheet.Cells[rowCount, colCount++].Value = t;

                    rowCount++;
                }
                else
                {
                    headers.AddRange(output.AllDates);
                    rowCount++;
                    AddHeader(
                        sheet, rowCount, headers.ToArray()
                    );
                    rowCount++;
                }
            }
            return rowCount++;
        }

        private int AddHeaderForLocation(ExcelWorksheet sheet, int rowCount, bool isOrderTag, GetItemInput input, IdDayItemOutput output)
        {
            var headers = new List<string>
            {
                L("Category"),
                L("Code"),
                L("MenuItem"),
                L("Portion")
            };

            if (isOrderTag)
            {
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
                rowCount += 2;

                sheet.Cells[rowCount, 1].Value = "Order Tag Item";
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Merge = true;
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
            }

            if (input.ExportOutput.Equals("B"))
            {
                foreach (var locationT in output.AllLocations.Keys)
                {
                    headers.Add(locationT);
                    headers.Add("");
                }
                headers.Add(L("QtyTotal"));
                headers.Add(L("SumTotal"));
            }
            else
            {
                headers.AddRange(output.AllLocations.Keys);
                headers.Add(L("Grand"));
            }

            int colCount = 1;
            rowCount++;
            AddHeader(
                sheet, rowCount, headers.ToArray()
            );

            rowCount++;
            if (input.ExportOutput.Equals("B"))
            {
                colCount = 1;
                headers = new List<string>
                {
                    "",
                    "",
                    "",
                    ""
                };

                foreach (var allDate in output.AllLocations)
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                foreach (var t in headers)
                    sheet.Cells[rowCount, colCount++].Value = t;

                rowCount++;
            }

            return rowCount;
        }

        private int CreateItemForLocationRows(GetItemInput input, ExcelWorksheet sheet, List<IdDayItemOutputDto> items, IdDayItemOutput output, int rowCount)
        {
            int colCount;

            Dictionary<string, decimal> sumDc = new Dictionary<string, decimal>();
            Dictionary<string, decimal> quaDc = new Dictionary<string, decimal>();

            var grandQuantity = 0M;
            var grandTotal = 0M;
            foreach (var itemGrouped in items.GroupBy(e => new { e.PortionId, e.MenuItemId }))
            {
                var itemT = itemGrouped.FirstOrDefault();

                colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = itemT.CategoryName;
                sheet.Cells[rowCount, colCount++].Value = itemT.AliasCode;
                sheet.Cells[rowCount, colCount++].Value = itemT.MenuItemName;
                sheet.Cells[rowCount, colCount++].Value = itemT.PortionName;

                foreach (var locationT in output.AllLocations.Values)
                {
                    var myLocItems = itemGrouped.Where(a => a.LocationName.Equals(locationT));

                    var quantity = 0M;
                    var total = 0M;
                    if (myLocItems.Any())
                    {
                        quantity = myLocItems.SelectMany(a => a.Items).Sum(a => a.Quantity);
                        total = myLocItems.SelectMany(a => a.Items).Sum(a => a.Total);

                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                        {
                            var sumC = sumDc[locationT];
                            sumC += total;
                            sumDc[locationT] = sumC;

                            var quav = quaDc[locationT];
                            quav += quantity;
                            quaDc[locationT] = quav;
                        }
                        else
                        {
                            sumDc[locationT] = total;
                            quaDc[locationT] = quantity;
                        }
                    }

                    switch (input.ExportOutput)
                    {
                        case "B":
                            sheet.Cells[rowCount, colCount++].Value = quantity;
                            sheet.Cells[rowCount, colCount++].Value = total;
                            break;

                        case "T":
                            sheet.Cells[rowCount, colCount++].Value = total;
                            break;

                        case "Q":
                            sheet.Cells[rowCount, colCount++].Value = quantity;
                            break;

                        default:
                            break;
                    }
                }

                grandQuantity += itemGrouped.SelectMany(c => c.Items).Sum(a => a.Quantity);
                grandTotal += itemGrouped.SelectMany(c => c.Items).Sum(a => a.Total);
                switch (input.ExportOutput)
                {
                    case "B":
                        sheet.Cells[rowCount, colCount++].Value = itemGrouped.SelectMany(c => c.Items).Sum(a => a.Quantity);
                        sheet.Cells[rowCount, colCount++].Value = itemGrouped.SelectMany(c => c.Items).Sum(a => a.Total);
                        break;

                    case "T":
                        sheet.Cells[rowCount, colCount++].Value = itemGrouped.SelectMany(c => c.Items).Sum(a => a.Total);
                        break;

                    case "Q":
                        sheet.Cells[rowCount, colCount++].Value = itemGrouped.SelectMany(c => c.Items).Sum(a => a.Quantity);
                        break;

                    default:
                        break;
                }

                rowCount++;
            }
            sheet.Cells[rowCount, 1].Value = L("Total");
            colCount = 5;
            foreach (var locationT in output.AllLocations.Values)
            {
                switch (input.ExportOutput)
                {
                    case "B":
                        if (quaDc.Any() && quaDc.ContainsKey(locationT))
                        {
                            sheet.Cells[rowCount, colCount++].Value = quaDc[locationT];
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        }

                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                        {
                            sheet.Cells[rowCount, colCount++].Value = sumDc[locationT];
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        }
                        break;

                    case "T":
                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                        {
                            sheet.Cells[rowCount, colCount++].Value = sumDc[locationT];
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        }
                        break;

                    default:
                        if (quaDc.Any() && quaDc.ContainsKey(locationT))
                        {
                            sheet.Cells[rowCount, colCount++].Value = quaDc[locationT];
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        }
                        break;
                }
            }

            switch (input.ExportOutput)
            {
                case "B":
                    sheet.Cells[rowCount, colCount++].Value = grandQuantity;
                    sheet.Cells[rowCount, colCount++].Value = grandTotal;
                    break;

                case "T":
                    sheet.Cells[rowCount, colCount++].Value = grandTotal;
                    break;

                default:
                    sheet.Cells[rowCount, colCount++].Value = grandQuantity;
                    break;
            }

            sheet.Cells[rowCount, 1, rowCount, colCount].Style.Font.Bold = true;

            return rowCount++;
        }

        #endregion export to excel

        #region Items Tag

        public FileDto ExportToFileItemTagSales(GetItemInput input, ItemTagStatsDto output)
        {
            var builder = new StringBuilder();
            builder.Append(L("ItemTagSales"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                CreateFileForItemTagSales(excelPackage, input, output);
                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        private void CreateFileForItemTagSales(ExcelPackage package, GetItemInput input, ItemTagStatsDto output)
        {
            var sheet = package.Workbook.Worksheets.Add(L("ItemTagSales"));
            sheet.OutLineApplyStyle = true;

            int rowCount = AddReportHeader(sheet, "Item Tag Sales Report", input);
            rowCount++;

            AddHeader(
                sheet, rowCount,
                 L("Tag"),
                L("Quantity"),
                L("Amount")
            );
            rowCount++;
            if (DynamicQueryable.Any(output.ItemTagList.Items))
            {
                AddObjects(
                    sheet, rowCount, output.ItemTagList.Items.ToList(),
                    _ => _.TagName,
                    _ => _.Quantity,
                    _ => Math.Round(_.Total, _roundDecimals, MidpointRounding.AwayFromZero)
                );

                rowCount = rowCount + output.ItemTagList.TotalCount + 1;
            }
            sheet.Cells[rowCount, 1].Value = "Total";
            sheet.Cells[rowCount, 2].Value = output.ItemTagList.Items.Sum(i => i.Quantity);
            sheet.Cells[rowCount, 3].Value = Math.Round(output.ItemTagList.Items.Sum(i => i.Total), _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 1, rowCount, 5].Style.Font.Bold = true;

            for (var i = 1; i <= 5; i++) sheet.Column(i).AutoFit();
        }

        #endregion Items Tag

        public FileDto ExportTopOrBottomItemSales(GetItemInput input, List<MenuListDto> dtos)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopOrBottomItemSales"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("TopOrBottomItemSales"));
                sheet.OutLineApplyStyle = true;

                var row = 2;

                AddHeader(sheet,
                        L("Location"),
                        L("Category"),
                        L("MenuItemName"),
                        L("Portion"),
                        L("Price"),
                        L("Quantity"),
                        L("TotalAmount")
                        );

                AddObjects(sheet, row, dtos,
                            _ => _.LocationName,
                            _ => _.CategoryName,
                            _ => _.MenuItemName,
                            _ => _.MenuItemPortionName,
                            _ => Math.Round(_.Price, _roundDecimals, MidpointRounding.AwayFromZero),
                            _ => _.Quantity,
                            _ => Math.Round(_.Total, _roundDecimals, MidpointRounding.AwayFromZero)
                            );

                row = dtos.Count + row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 6].Value = dtos.Sum(t => t.Quantity);
                //sheet.Cells[row, 16].Value = dtos.Sum(t => t.Quantity);
                sheet.Cells[row, 7].Value = Math.Round(dtos.Sum(t => t.Total), _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, 1, row, 20].Style.Font.Bold = true;
                for (var i = 1; i <= 20; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                SaveToFile(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        public FileDto ExportItemSalesForDepartmentExcel(GetItemInput input, ItemStatsDto items)
        {
            var file =
                new FileDto("Department" + "-" + input.StartDate.ToString("yyyy-MMMM-dd") + ".xlsx",
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var row = 1;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;
                var itemSales = items;
                var departmentNameList = itemSales.MenuList.Items
                    .GroupBy(i => i.DepartmentName)
                    .Select(c => c.Key)
                    .Distinct()
                    .ToList();

                var outPut = itemSales.MenuList.Items
                    .GroupBy(c => new
                    {
                        c.CategoryName,
                        c.MenuItemPortionName,
                        c.MenuItemName
                    })
                    .ToList();

                var colCount = 1;
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";

                foreach (var departmentName in departmentNameList)
                {
                    sheet.Cells[row, colCount++].Value = departmentName;
                    colCount++;
                }

                row = 2;
                var headers = new List<string> { L("Category"), L("MenuItemName"), L("Portion") };

                for (var i = 0; i < departmentNameList.Count; i++)
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                AddHeader(
                    sheet,
                    row++,
                    headers.ToArray()
                );

                foreach (var old in outPut)
                {
                    colCount = 1;
                    sheet.Cells[row, colCount++].Value = old.Key.CategoryName;
                    sheet.Cells[row, colCount++].Value = old.Key.MenuItemName;
                    sheet.Cells[row, colCount++].Value = old.Key.MenuItemPortionName;
                    foreach (var item in old)
                    {
                        var index = 0;
                        foreach (var departmentName in departmentNameList)
                        {
                            index++;
                            if (item.DepartmentName == departmentName)
                            {
                                colCount = (index - 1) * 2 + 4;
                                sheet.Cells[row, colCount++].Value = Math.Round(item.Quantity, _roundDecimals, MidpointRounding.AwayFromZero);
                                sheet.Cells[row, colCount++].Value = Math.Round(item.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                            }
                        }
                    }
                    row++;
                }
                sheet.Cells[row, 1, row, 3 + 2 * departmentNameList.Count].Style.Font.Bold = true;
                sheet.Cells[row, 1].Value = L("Total");
                colCount = 2;
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";

                var groupByDepartment = itemSales.MenuList.Items
                    .GroupBy(c => c.DepartmentName)
                    .ToList();
                foreach (var old in groupByDepartment)
                {
                    var key = old.Key;
                    var index = 0;
                    foreach (var departmentName in departmentNameList)
                    {
                        index++;
                        if (key == departmentName)
                        {
                            colCount = (index - 1) * 2 + 4;
                            sheet.Cells[row, colCount++].Value = Math.Round(old.Sum(a => a.Quantity), _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[row, colCount++].Value = Math.Round(old.Sum(a => a.Total), _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                }

                for (var i = 1; i <= 3 + 2 * departmentNameList.Count; i++) sheet.Column(i).AutoFit();
                SaveToFile(excelPackage, file); 
            }

            return ProcessFile(input.ExportOutputType, file);
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Dto
{
    public class PaymentExcessReportOutput
    {
        public int Id { get; set; }

        public string PaymentType { get; set; }

        public decimal Actual { get; set; }

        public decimal Tendered { get; set; }

        public decimal Excess => Tendered - Actual;

        public string PaymentTags { get; set; }

        public List<string> PaymentTagValues
        {
            get
            {
                if (PaymentTags == null)
                    return new List<string>();
                return PaymentTags.Split(",").ToList();
            }
        }

        [JsonIgnore]
        public List<Transaction.Payment> Payments { get; set; } = new List<Transaction.Payment>();
    }
}

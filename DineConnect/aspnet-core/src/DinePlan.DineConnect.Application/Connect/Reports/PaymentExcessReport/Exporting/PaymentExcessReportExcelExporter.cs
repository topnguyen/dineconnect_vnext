﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Exporting
{
    public class PaymentExcessReportExcelExporter : EpPlusExcelExporterBase, IPaymentExcessReportExcelExporter
    {
        private readonly IRepository<Location> _locRepo;
        public PaymentExcessReportExcelExporter(IRepository<Location> locRepo, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _locRepo = locRepo;
        }

        public async Task<FileDto> ExportPaymentExcessDetail(GetTicketInput input, IPaymentExcessReportAppService appService)
        {
            var file = new FileDto("PaymentExcess_" + input.StartDate.ToString(_simpleDateFormat) + "_" + input.EndDate.ToString(_simpleDateFormat) + ".xlsx",
                 MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetPaymentExcessDetail(excelPackage, input, appService);
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }

        public async Task<FileDto> ExportPaymentExcessSummary(GetTicketInput input, IPaymentExcessReportAppService appService)
        {
            var file = new FileDto("PaymentExcess_" + input.StartDate.ToString(_simpleDateFormat) + "_" + input.EndDate.ToString(_simpleDateFormat) + ".xlsx",
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetPaymentExcessSummary(excelPackage, input, appService);
                SaveToPath(excelPackage, file);
            }

            return ProcessFile(input.ExportOutputType, file);
        }
        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input, string dateFormat, string datetimeFormat)
        {

            var brandName = "";
            var locationCode = "";
            var locationName = "";

            Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }


            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 16].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = "Brand:";
            sheet.Cells[2, 2].Value = brandName;


            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(dateFormat) + " to " + input.EndDate.ToString(dateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = locationCode + " - " + locationName;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(datetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }
        private async Task GetPaymentExcessSummary(ExcelPackage package, GetTicketInput input, IPaymentExcessReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Summary"));
            var row = AddReportHeader(sheet, "Payment Excess Summary", input, "dd/MM/yyyy", "dd-MM-yyyy HH:ss:mm");
            row++;

            var headers = new List<string>
            {
                L("Location"),
                L("PaymentTypeName"),
                L("Qty"),
                L("%Qty"),
                L("TotalAmount"),
                L("%Ttl")
            };

            var listDtos = await appService.BuildPaymentExcessExportInternal(input);
            AddHeader(sheet, row++, headers.ToArray());

            foreach (var item in listDtos)
            {
                var colCount = 1;
                row++;
                colCount = 1;
                sheet.Cells[row, colCount++].Value = item.LocationName;
                sheet.Cells[row, colCount++].Value = item.PaymentTypeName;
                sheet.Cells[row, colCount++].Value = item.Quantity;
                sheet.Cells[row, colCount++].Value = Math.Round((double)item.Quantity * 100 / listDtos.Sum(t => t.Quantity), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                sheet.Cells[row, colCount++].Value = item.TotalAmount;
                sheet.Cells[row, colCount++].Value = Math.Round(item.TotalAmount * 100 / listDtos.Sum(t => t.TotalAmount), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
            }

            row++;
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 3].Value = listDtos.Sum(t => t.Quantity);
            sheet.Cells[row, 4].Value = "100%";
            sheet.Cells[row, 5].Value = Math.Round(listDtos.Sum(t => t.TotalAmount), _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[row, 6].Value = "100%";

            sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;

            for (var i = 1; i <= 11; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetPaymentExcessDetail(ExcelPackage package, GetTicketInput input, IPaymentExcessReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Detail"));
            var row = AddReportHeader(sheet, "Payment Excess Detail", input, "dd/MM/yyyy", "dd-MM-yyyy HH:ss:mm");
            row++;

            var headers = new List<string>
            {
                L("Date"),
                L("Location"),
                L("TicketNumber"),
                L("InvoiceNo"),
                L("RefNo"),
                L("Department"),
                L("PaymentTypeName"),
                L("SubTotal"),
                L("Amount"),
                L("Excess")
            };

            var listDtos = await appService.BuildPaymentExcessDetaiInternal(input);
            AddHeader(sheet, row++, headers.ToArray());

            foreach (var item in listDtos)
            {
                var colCount = 1;
                row++;
                colCount = 1;
                sheet.Cells[row, colCount++].Value = item.Date.ToString(_simpleDateFormat);
                sheet.Cells[row, colCount++].Value = item.LocationName;
                sheet.Cells[row, colCount++].Value = item.TicketNumber;
                sheet.Cells[row, colCount++].Value = item.InvoiceNo;
                sheet.Cells[row, colCount++].Value = item.ReferenceNumber;
                sheet.Cells[row, colCount++].Value = item.DepartmentName;
                sheet.Cells[row, colCount++].Value = item.PaymentTypeName;
                sheet.Cells[row, colCount++].Value = Math.Round(item.TotalAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[row, colCount++].Value = Math.Round(item.TendedAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[row, colCount++].Value = Math.Round(item.Excess, _roundDecimals, MidpointRounding.AwayFromZero);
            }

            row++;
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 8].Value = Math.Round(listDtos.Sum(t => t.TotalAmount), _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[row, 9].Value = Math.Round(listDtos.Sum(t => t.TendedAmount), _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[row, 10].Value = Math.Round(listDtos.Sum(t => t.Excess), _roundDecimals, MidpointRounding.AwayFromZero);

            sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;

            for (var i = 1; i <= 15; i++) sheet.Column(i).AutoFit();
        }
    }
}

﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Exporting
{
    public interface IPaymentExcessReportExcelExporter
    {
        Task<FileDto> ExportPaymentExcessSummary(GetTicketInput input, IPaymentExcessReportAppService appService);

        Task<FileDto> ExportPaymentExcessDetail(GetTicketInput input, IPaymentExcessReportAppService appService);
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PaymentExcessReport
{
    public interface IPaymentExcessReportAppService: IApplicationService
    {
        Task<PagedResultDto<PaymentExcessReportOutput>> BuildPaymentExcessSummary(GetTicketInput input);

        Task<List<PaymentExcessSummaryDto>> BuildPaymentExcessExportInternal(GetTicketInput input);

        Task<FileDto> BuildPaymentExcessExport(GetTicketInput input);

        Task<FileDto> BuildPaymentExcessDetailExport(GetTicketInput input);

        Task<List<PaymentExcessDetailDto>> BuildPaymentExcessDetaiInternal(GetTicketInput input);
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Dto;
using DinePlan.DineConnect.Connect.Reports.PaymentExcessReport.Exporting;
using DinePlan.DineConnect.Connect.Reports.TenderReport.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PaymentExcessReport
{
    public class PaymentExcessReportAppService : DineConnectReportAppServiceBase, IPaymentExcessReportAppService
    {
        private readonly IPaymentExcessReportExcelExporter _exporter;
        private readonly IRepository<Transaction.Payment> _payRepository;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _paymentTypeRepository;
        public PaymentExcessReportAppService(
            ILocationAppService locService, IRepository<Master.PaymentTypes.PaymentType> paymentTypeRepository, IRepository<Transaction.Ticket> ticketRepository,
        IRepository<Ticket> ticketRepo, 
            IUnitOfWorkManager unitOfWorkManager, IPaymentExcessReportExcelExporter exporter, IRepository<Transaction.Payment> payRepository,
        IRepository<Department> departmentRepo) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _exporter = exporter;
            _payRepository = payRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _ticketRepository = ticketRepository;
        }

        public async Task<List<PaymentExcessDetailDto>> BuildPaymentExcessDetaiInternal(GetTicketInput input)
        {
            var groupByPayment = GetPaymentExcess(input);
            var allTicket = _ticketRepository.GetAll();
            foreach(var item in groupByPayment.SelectMany(g => g.Payments).ToList())
            {
                item.Ticket = _ticketRepository.Get(item.TicketId);
            }
            var result = groupByPayment.SelectMany(g => g.Payments.GroupBy(t => t.TicketId)
                    .Select(t =>
                    {
                        var dto =
                        new PaymentExcessDetailDto
                        {
                            Id = t.Key,
                            PaymentTypeName = g.PaymentType,
                            LocationName = t.First().Ticket.Location.Name,
                            TicketNumber = t.First().Ticket.TicketNumber,
                            Date = t.First().Ticket.LastPaymentTime,
                            InvoiceNo = t.First().Ticket.InvoiceNo,
                            DepartmentName = t.First().Ticket.DepartmentName
                        };
                        dto.TotalAmount = t.Sum(x => x.Amount);
                        dto.TendedAmount = t.Sum(x => x.TenderedAmount);
                        var pFrame = t.First().PaymentTags;

                        if (pFrame != null)
                        {
                            try
                            {
                                PaymentFrame frame = JsonConvert.DeserializeObject<PaymentFrame>(pFrame);
                                dto.ReferenceNumber = frame != null ? frame.Description : "";
                            }
                            catch (Exception)
                            {
                                dto.ReferenceNumber = "";
                            }

                        }
                        return dto;
                    }).ToList()
                ).ToList();
            return result.Where(a => a.Excess > 0M).ToList();
        }

        public async Task<FileDto> BuildPaymentExcessDetailExport(GetTicketInput input)
        {
            return await _exporter.ExportPaymentExcessDetail(input, this);
        }

        public async Task<FileDto> BuildPaymentExcessExport(GetTicketInput input)
        {
            return await _exporter.ExportPaymentExcessSummary(input, this);
        }
        private List<PaymentExcessReportOutput> GetPaymentExcess(GetTicketInput input)
        {
            var currentTickets = GetAllTicketsForTicketInputNoFilterByDynamicFilter(input)
                            .Where(a => a.TotalAmount > 0M);
            var allPaymentType = _paymentTypeRepository.GetAll();
            var allPayments = currentTickets.SelectMany(x => x.Payments).ToList();
            foreach(var item in allPayments)
            {
                item.PaymentType = allPaymentType.FirstOrDefault(a => a.Id == item.PaymentTypeId);
            }
            var allPaymentGroup = allPayments.GroupBy(p => p.PaymentType)
                .Select(g => new PaymentExcessReportOutput
                {
                    Id = g.Key.Id,
                    PaymentType = g.Key.Name,
                    Actual = g.Sum(a => a.Amount),
                    Tendered = g.Sum(a => a.TenderedAmount),
                    PaymentTags = g.Key.PaymentTag,
                    Payments = g.ToList()
                });


            List<PaymentExcessReportOutput> returnOutpu = new List<PaymentExcessReportOutput>();
            if (input.PaymentTags.Any())
            {
                allPaymentGroup = allPaymentGroup.Where(g => input.PaymentTags.Contains(g.PaymentTags));
            }

            foreach (var summaryDto in allPaymentGroup.ToList())
            {
                if (summaryDto.Excess > 0M)
                {
                    returnOutpu.Add(summaryDto);
                }
            }

            // filter by dynamic Filter
            var dataAsQueryable = returnOutpu.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }

            returnOutpu = dataAsQueryable.ToList();
            return returnOutpu;
        }
        public async Task<List<PaymentExcessSummaryDto>> BuildPaymentExcessExportInternal(GetTicketInput input)
        {
            var currentTickets = GetAllTicketsForTicketInputNoFilterByDynamicFilter(input)
               .Where(a => a.TotalAmount > 0M);

            List<int> myPaymentTypes = new List<int>();

            if (input.PaymentTags != null && input.PaymentTags.Any())
            {
                myPaymentTypes = _payRepository.GetAll().Where(a => input.PaymentTags.Contains(a.PaymentTags)).Select(a => a.Id).ToList();
            }

            var allTickets = currentTickets.ToList().GroupBy(p => new { p.Location });
            List<PaymentExcessSummaryDto> firstList = new List<PaymentExcessSummaryDto>();

            foreach (var myTicket in allTickets)
            {
                var allPayment = myTicket.SelectMany(a => a.Payments).Where(a => (a.TenderedAmount - a.Amount) > 0);

                if (myPaymentTypes.Any())
                {
                    allPayment = allPayment.Where(a => myPaymentTypes.Contains(a.PaymentTypeId));
                }

                if (allPayment.Any())
                {
                    firstList.Add(new PaymentExcessSummaryDto
                    {
                        Id = 1,
                        PaymentTypeName = "Excess",
                        Actual = allPayment.Sum(a => a.Amount),
                        Quantity = allPayment.Count(),
                        Tendered = allPayment.Sum(a => a.TenderedAmount),
                        PaymentTags = "",
                        LocationName = myTicket.Key.Location.Name
                    });
                }
            }

            return firstList;
        }

        public async Task<PagedResultDto<PaymentExcessReportOutput>> BuildPaymentExcessSummary(GetTicketInput input)
        {
            List<PaymentExcessReportOutput> returnOutpu = GetPaymentExcess(input);

            var finalResult = returnOutpu
               .Skip(input.SkipCount)
               .Take(input.MaxResultCount)
               .ToList();

            return new PagedResultDto<PaymentExcessReportOutput>(returnOutpu.Count(), finalResult);
        }
    }
}

﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;

using DinePlan.DineConnect.Dto;
using System.Data.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;


using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Authorization;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DbFunctions = System.Data.Entity.DbFunctions;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Exporting;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Configuration;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Tag.TicketTags;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport
{
    
    public class PlanDayReportAppService : DineConnectAppServiceBase, IPlanDayReportAppService
    {
        private const string PAYMENT_TYPE_CASH = "CASH";
        private string[] DISCOUNTNAMES = { "DISCOUNT" };
        private string[] ROUNDTOTALS = { "ROUND+", "ROUND-", "ROUNDING" };
        private readonly IRepository<Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payRe;
        private readonly IPlanDayReportExcelExporter _exporter;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        private readonly IRepository<TransactionType> _tRe;
        private readonly IRepository<TillTransaction> _tillTransactionRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<TicketTagGroup> _ticketTagGroupRepository;
        public PlanDayReportAppService(IRepository<Transaction.Ticket> ticketManager,
            ILocationAppService locService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Department> drepo,
            IRepository<Location> lRepo,
            IRepository<Transaction.Order> orderManager,
             IReportBackgroundAppService rbas,
             IBackgroundJobManager bgm,
             IPlanDayReportExcelExporter exporter,
             IRepository<Period.WorkPeriod> workPeriod,
             IRepository<Master.PaymentTypes.PaymentType> payRe,
             ITenantSettingsAppService tenantSettingsAppService,
             IRepository<TransactionType> tRe,
             IRepository<TillTransaction> tillTransactionRepository,
              IRepository<Promotion> promotionRepository,
            IRepository<TicketTagGroup> ticketTagGroupRepository)
        {
            _ticketManager = ticketManager;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _tenantSettingsAppService = tenantSettingsAppService;
            _tRe = tRe;
            _tillTransactionRepository = tillTransactionRepository;
            _promotionRepository = promotionRepository;
            _ticketTagGroupRepository = ticketTagGroupRepository;
        }
        public async Task<List<WorkPeriodSummaryOutput>> BuildWorkPeriodByLocationForDatesWithPaging(WorkPeriodSummaryInputWithPaging input)
        {
            var allW = _workPeriod.GetAll();

            var locations = new List<int>();
            if (input.LocationGroup.Locations != null && input.LocationGroup.Locations.Any())
                locations = input.LocationGroup.Locations.Select(a => a.Id).ToList();
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));

            var nextDay = input.EndDate.AddDays(1);

            allW = allW.Where(
                a =>
                    a.StartTime.Date >=
                    input.StartDate.Date
                    &&
                    a.EndTime.Date <
                    nextDay.Date);

            var locLIst = await _lRepo.GetAllListAsync();

            var output = allW.ToList().OrderBy(t => t.Id).Select(workPeriod => new WorkPeriodSummaryOutput
            {
                Wid = workPeriod.Id,
                ReportStartDay = workPeriod.StartTime,
                ReportEndDay = workPeriod.EndTime,
                LocationId = workPeriod.LocationId,
                LocationName = locLIst.First(t => t.Id == workPeriod.LocationId).Name,
                LocationCode = locLIst.First(t => t.Id == workPeriod.LocationId).Code,
                DayInformation = workPeriod.WorkPeriodInformations,
                Total = workPeriod.TotalSales,
                TotalTicketCount = workPeriod.TotalTicketCount
            }).ToList();

            if (!input.Sorting.Equals("Id"))
                output = output.OrderBy(input.Sorting).ToList();

            var outputPage = output.AsQueryable().PageBy(input).ToList();

            return outputPage;
        }

        public async Task<FileDto> BuildWorkPeriodByLocationForDatesWithPagingToExcel(WorkPeriodSummaryInputWithPaging input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
                input.MaxResultCount = 10000;
            var output = await _exporter.ExportToFilePlanWorkDay(input, this);
            return output;
        }


        public async Task<CentralDayReportData> BuildCentralPlanDayReport(int workPeriodId)
        {
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == workPeriodId);
            var allTickets = GetWorkPeriodTickets(workPeriod);
            var result = new CentralDayReportData();
            if (workPeriod != null)
            {
                var locationId = workPeriod.LocationId;
                var findLoc = _lRepo.FirstOrDefault(s => s.Id == locationId);
                result.LocationDetails = findLoc;
            }
            else
            {
                result.LocationDetails = new Location();
            }
            await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
            return result;
        }
        private async Task<TicketTypeReportData> CreateTicketTypeInfo(IEnumerable<Transaction.Ticket> tickets)
        {
            var allTaxValues = _tRe.GetAll().Where(a => a.Tax).Select(a => a.Id).ToList();

            var returnObject = new TicketTypeReportData
            {
                TotalSales = tickets.Sum(a => a.TotalAmount),
                Tax = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions).
                    Where(a => allTaxValues.Contains(a.TransactionTypeId)).Sum(a => a.Amount),
                Transactions = new Dictionary<string, decimal>(),
                OrderTotal = tickets.SelectMany(a => a.Orders).Sum(a => a.GetTotal())
            };

            var allTransactions = tickets.SelectMany(x => x.Transactions);
            allTransactions.GroupBy(x => x.TransactionType).ToList().ForEach(
                x => { returnObject.Transactions.Add(x.Key.Name, x.Sum(y => y.Amount)); });

            returnObject.TicketCount = tickets.Count(a => a.TotalAmount > 0);
            returnObject.ItemDiscountTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders).SelectMany(a => a.GetOrderPromotionList()).Sum(a => a.PromotionAmount);
            returnObject.ItemSales = tickets.SelectMany(a => a.Orders).Sum(b => b.GetTotal());

            returnObject.TicketDiscountTotal = tickets.Where(a => a.TotalAmount > 0)
                .SelectMany(a => a.GetTicketPromotionList()).Sum(a => a.PromotionAmount);

            var tranAmount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                .Where(a => DISCOUNTNAMES.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
            returnObject.TicketDiscountTotal += tranAmount;

            var roundTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                .Where(a => ROUNDTOTALS.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
            returnObject.RoundingTotal += roundTotal;

            returnObject.ItemCount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                .Where(a => a.CalculatePrice && a.DecreaseInventory).Sum(a => a.Quantity);

            returnObject.PaxCount = GetPaxCount(tickets.Where(a => a.TotalAmount > 0));
            returnObject.TotalVoid = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && !a.DecreaseInventory && a.OrderStates.Contains("Void")).Sum(a => a.Quantity);

            returnObject.TotalComp = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && a.DecreaseInventory && a.OrderStates.Contains("Comp")).Sum(a => a.Quantity);

            returnObject.TotalComp = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && a.DecreaseInventory && a.OrderStates.Contains("Gift")).Sum(a => a.Quantity);

            returnObject.TotalRefund = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && !a.DecreaseInventory && a.OrderStates.Contains("Refund")).Sum(a => a.Quantity);

            return returnObject;
        }
        private decimal GetPaxCount(IEnumerable<Transaction.Ticket> dDto)
        {
            var totalPaxCount = 0M;
            var toaalll = dDto.Where(a => !string.IsNullOrEmpty(a.TicketTags))
                .Select(a => a.TicketTags);
            foreach (var tag in toaalll)
                if (!string.IsNullOrEmpty(tag))
                {
                    var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                    var lastTag = myTag?.LastOrDefault(a =>
                        a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                    if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                }
            // Gosei Hoan Update pax calculation
            if (totalPaxCount == 0)
            {
                totalPaxCount = dDto.SelectMany(t => t.Orders).Sum(o => o.Price * o.Quantity);
            }
            return totalPaxCount;
        }
        private IEnumerable<DepartmentSales> GetDepartmentSales(IEnumerable<Transaction.Ticket> AllTickets,
            IEnumerable<Department> AllDepartments)
        {
            var result = AllTickets
                .GroupBy(l => l.DepartmentName)
                .Select(csLine => new DepartmentSales
                {
                    Name = csLine.Key,
                    TotalTicketCount = csLine.Count().ToString(),
                    Amount = csLine.Sum(c => c.TotalAmount),
                    ItemCount = csLine.SelectMany(c => c.Orders).Sum(o => o.Quantity),
                    TaxTotal = csLine.Sum(a => a.GetTaxTotal())
                });

            foreach (var departmentSalese in result)
            {
                var depat = AllDepartments.SingleOrDefault(a => a.Name.Equals(departmentSalese.Name));
                departmentSalese.Name = "NO_DEPT";
                if (depat != null) departmentSalese.Name = depat.Name;
            }

            return result;
        }
        private IEnumerable<TillTransaction> GetTillTransactionsByWorkPeriod(Period.WorkPeriod workPeriod)
        {
            return
                _tillTransactionRepository.GetAll().Where(
                        x => x.TransactionTime >= workPeriod.StartTime && x.TransactionTime <= workPeriod.EndTime &&
                             !x.Status)
                    .Include(x => x.TillAccount);
        }
        private string GetPaymentTypeName(int paymentTypeId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var pt = _payRe.FirstOrDefault(x => x.Id == paymentTypeId);
                return pt != null ? pt.Name : "";
            }

            return "";
        }

        private AmountCalculator GetTicketsIncomeCalculator(IEnumerable<Transaction.Ticket> allTickets)
        {
            var groups = allTickets
                .SelectMany(x => x.Payments)
                .GroupBy(x => x.PaymentTypeId)
                .Select(x => new TenderedAmount
                {
                    PaymentName = GetPaymentTypeName(x.Key),
                    Amount = x.Sum(y => y.Amount),
                    ActualAmount = x.Sum(y => y.TenderedAmount),
                    PaymentCount = x.Count(),
                });

            return new AmountCalculator(groups)
            {
            };
        }
        public async Task<CentralDayReportData> BuildCentralTerminalReport(CentralTerminalReportInput input)
        {
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == input.workPeriodId);

            List<WorkShiftDto> allWorkTimes = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
            {
                allWorkTimes = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                allWorkTimes = allWorkTimes.Where(t => input.terminals.Contains(t.TerminalName)).ToList();
            }

            var result = new CentralDayReportData();
            var allTickets = GetWorkPeriodTerminalTickets(workPeriod, input.terminals);
            if (workPeriod != null)
            {
                var locationId = workPeriod.LocationId;
                var findLoc = _lRepo.FirstOrDefault(s => s.Id == locationId);
                result.LocationDetails = findLoc;
            }

            await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
            return result;
        }
        public IQueryable<Transaction.Ticket> GetWorkPeriodTerminalTickets(Period.WorkPeriod workPeriod, List<string> terminals)
        {
            var allTickets = GetTicketsForWorkPeriod(workPeriod);

            return allTickets.Where(
                x => x.LastPaymentTime >= workPeriod.StartTime && x.LastPaymentTime <= workPeriod.EndTime &&
                     !x.PreOrder && !x.Credit &&
                     (x.Payments.Any(a => terminals.Contains(a.TerminalName)) ||
                      x.TotalAmount == 0M && terminals.Contains(x.TerminalName)));
        }

        public async Task<CentralDayReportData> BuildCentralShiftReport(CentralShiftReportInput input)
        {
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == input.workPeriodId);
            List<WorkShiftDto> shiftDto = null;
            WorkShiftDto currentWorkShiftDto = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
            {
                shiftDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                currentWorkShiftDto = shiftDto.FirstOrDefault(t => t.UniqueId == input.workShiftId);
            }

            var allTickets = GetCurrentWorkTimeTicketsForTerminal(currentWorkShiftDto, workPeriod);
            var result = new CentralDayReportData();
            if (workPeriod != null)
            {
                var locationId = workPeriod.LocationId;
                var findLoc = _lRepo.FirstOrDefault(s => s.Id == locationId);
                result.LocationDetails = findLoc;
            }

            await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
            return result;
        }
        private IQueryable<Transaction.Ticket> GetCurrentWorkTimeTicketsForTerminal(WorkShiftDto workShift,
           Period.WorkPeriod workPeriod)
        {
            var allTickets = GetTicketsForWorkPeriod(workPeriod);

            return allTickets.Where(
                x => x.LastPaymentTime >= workShift.StartDate && x.LastPaymentTime <= workShift.EndDate &&
                     !x.PreOrder && !x.Credit
                     && (x.Payments.Any(a => a.TerminalName.Equals(workShift.TerminalName))) || x.TerminalName.Equals(workShift.TerminalName));
        }
        private IEnumerable<SalesItemSumaryDto> GetGroupSales(IEnumerable<Transaction.Ticket> allTickets, int type)
        {
            IEnumerable<IGrouping<string, Order>> groups = null;


            switch (type)
            {
                case 1:
                    groups = allTickets.SelectMany(e => e.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory)
                        .GroupBy(e => e.MenuItem.Category.ProductGroup?.Name);
                    break;

                case 2:
                    groups = allTickets.SelectMany(e => e.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory).GroupBy(e => e.MenuItem.Category.Name);
                    break;

                default:
                    break;
            }

            return groups.Select(
                g => new SalesItemSumaryDto
                {
                    Name = g.Key,
                    Amount = g.Sum(e => e.GetTotal()),
                    TaxAmount = g.Sum(e => e.GetTaxTotal() * e.Quantity),
                    ItemCount = g.Sum(e => e.Quantity),
                    Discount = g.SelectMany(e => e.GetOrderPromotionList()).Sum(a => a.PromotionAmount)
                });
        }
        private CashReportData RenderCashTransactions(decimal cashAmount, IEnumerable<TillTransaction> tillTransAll,
            decimal floatAmount, int floatCount)
        {
            var cashReprortData = new CashReportData();
            cashReprortData.cashAmount = cashAmount;

            var allIncome = tillTransAll.Where(a => a.TillAccount.TillAccountTypeId == (int)TillAccounTypes.Income);
            var allIncomeAmount = allIncome.Sum(a => a.TotalAmount);
            allIncomeAmount += floatAmount;

            var allExp = tillTransAll.Where(a => a.TillAccount.TillAccountTypeId == (int)TillAccounTypes.Expense);
            var allExpAmount = allExp.Sum(a => a.TotalAmount);

            var differ = cashAmount + allIncomeAmount - allExpAmount;

            cashReprortData.cashIn = allIncome.Count() + floatCount;
            cashReprortData.allIncomeAmount = allIncomeAmount;
            cashReprortData.cashOut = allExp.Count();
            cashReprortData.allExpAmount = allExpAmount;
            cashReprortData.cashInDrawer = differ;

            return cashReprortData;
        }
        private List<SalesItemSumaryDto> GetDiscountData(IEnumerable<Transaction.Ticket> tickets)
        {
            var ticketPros = tickets.Where(t => t.TicketPromotionDetails != null)
                .SelectMany(
                    a => JsonConvert.DeserializeObject<List<Dtos.PromotionDetailValue>>(a.TicketPromotionDetails))
                .ToList();

            var orderPros = tickets.SelectMany(t => t.Orders)
                .Where(t => t.OrderPromotionDetails != null)
                .SelectMany(a => JsonConvert.DeserializeObject<List<Dtos.PromotionDetailValue>>(a.OrderPromotionDetails))
                .ToList();

            var allPromos = ticketPros.Concat(orderPros).ToList();

            var result = allPromos.GroupBy(p => p.PromotionName).Select(s =>
            {
                var data = new SalesItemSumaryDto
                {
                    Name = s.Key,
                    Amount = s.Sum(a => a.PromotionAmount),
                    TicketCount = s.Count()
                };
                return data;
            }).ToList();
            return result;
        }
        private async Task<List<ScheduleReportData>> GetScheduleSetting(int locationId)
        {
            var location = _lRepo.Get(locationId);
            var allSchedules = location.Schedules;

            if (allSchedules != null && allSchedules.Any())
            {
                List<ScheduleReportData> sch = new List<ScheduleReportData>();

                foreach (var e in allSchedules)
                {
                    ScheduleReportData s1 = new ScheduleReportData()
                    {
                        FromTime = new TimeSpan(e.StartHour, e.StartMinute - 1, 59),
                        ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0),
                    };
                    s1.ScheduleName = $@"{e.Name}";
                    sch.Add(s1);
                }
                return sch;
            }
            else
            {

                var scheduleSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);

                if (!string.IsNullOrEmpty(scheduleSetting))
                {
                    List<ScheduleSetting> schedules;
                    schedules = JsonConvert.DeserializeObject<List<ScheduleSetting>>(scheduleSetting);
                    var listScheduleReport = schedules.Select(e =>
                    {
                        var dto = new ScheduleReportData
                        {
                            FromTime = new TimeSpan(e.StartHour, e.StartMinute - 1, 59),
                            ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0),
                        };

                        dto.ScheduleName = $@"{e.Name}";
                        return dto;
                    }).OrderBy(e => e.FromTime).ToList();

                    return listScheduleReport;
                }
            }

            return null;
        }
        private async Task<List<SalesItemSumaryDto>> GetPeriodData(IEnumerable<Transaction.Ticket> tickets, Period.WorkPeriod period)
        {
            var schedules = await GetScheduleSetting(period.LocationId);

            if (schedules != null)
            {
                var result = schedules.Select(s =>
                {
                    var ticketBySchedule = tickets.Where(i =>
                        i.LastPaymentTime.TimeOfDay > s.FromTime && i.LastPaymentTime.TimeOfDay < s.ToTime);
                    var data = new SalesItemSumaryDto
                    {
                        Name = s.ScheduleName,
                        ItemCount = ticketBySchedule.SelectMany(t => t.Orders).Distinct().Sum(a => a.Quantity),
                        Amount = ticketBySchedule.SelectMany(t => t.Orders).Distinct().Sum(a => a.Quantity * a.Price),
                        TicketCount = ticketBySchedule.Count()
                    };
                    return data;
                }).ToList();
                return result;
            }

            return null;
        }
        private async Task RenderDayAndTimeReport(IEnumerable<Transaction.Ticket> allTickets,
            CentralDayReportData result, Period.WorkPeriod workPeriod)
        {
            //ticket types
            result.TicketTypeReportData = await CreateTicketTypeInfo(allTickets);

            var allDepartments = await _dRepo.GetAllListAsync();
            var departmentSales = GetDepartmentSales(allTickets.Where(a => a.TotalAmount > 0), allDepartments).OrderBy(t => t.Name).ToList();
            result.Departments = new List<object>();
            departmentSales.ForEach(dept => result.Departments.Add(new
            {
                depatName = dept.Name,
                count = dept.TotalTicketCount,
                amount = dept.Amount,
                itemCount = dept.ItemCount
            }));

            result.SalesGroups = GetGroupSales(allTickets.Where(a => a.TotalAmount > 0), 1).OrderBy(t => t.Name).ToList();
            result.SalesCategories = GetGroupSales(allTickets.Where(a => a.TotalAmount > 0), 2).OrderBy(t => t.Name).ToList();
            result.PeriodData = await GetPeriodData(allTickets.Where(a => a.TotalAmount > 0), workPeriod);
            result.DiscountData = GetDiscountData(allTickets.Where(a => a.TotalAmount > 0));
            //payments
            var incomeCalculator = GetTicketsIncomeCalculator(allTickets);

            var cashAmount = 0M;
            var totalSales = 0M;

            var paymentTypeRows = new List<PaymentTypeRowData>();
            foreach (var paymentName in incomeCalculator.PaymentNames)
            {
                var actualSales = incomeCalculator.GetAmount(paymentName);
                if (paymentName.ToUpper().Equals(PAYMENT_TYPE_CASH)) cashAmount = actualSales;

                totalSales += actualSales;
                paymentTypeRows.Add(new PaymentTypeRowData
                {
                    paymentName = paymentName,
                    count = incomeCalculator.GetCount(paymentName),
                    actualSales = actualSales
                });
            }

            result.PaymentTypeReportData = new PaymentTypeReportData
            {
                totalSales = totalSales,
                details = paymentTypeRows
            };

            // Cash Transactions
            var tillTransAll = GetTillTransactionsByWorkPeriod(workPeriod);

            List<WorkShiftDto> shiftDto = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                shiftDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);

            result.CashReportData = RenderCashTransactions(cashAmount, tillTransAll, shiftDto.Sum(a => a.Float),
                shiftDto.Count());

            //WorkTimePaTable
            if (shiftDto != null && shiftDto.Any())
            {
                var allPaymentInfos = shiftDto.SelectMany(a => a.PaymentInfos);

                if (allPaymentInfos.Any())
                {
                    var winSalesTotal = 0M;
                    var wdifferenceTotal = 0M;
                    var wactualTotal = 0M;

                    var pt = await _payRe.GetAllListAsync();

                    var details = new List<PaymentTypeByWorkTimeRowData>();

                    foreach (var infos in allPaymentInfos.GroupBy(a => a.PaymentType))
                    {
                        var actualTotal = infos.Sum(a => a.Actual);
                        var inSalesTotal = infos.Sum(a => a.Entered);
                        var differenceTotal = inSalesTotal - actualTotal;

                        var pType = pt.LastOrDefault(a => a.Id == infos.Key);

                        if (pType != null && (actualTotal > 0M || inSalesTotal > 0M))
                            details.Add(new PaymentTypeByWorkTimeRowData
                            {
                                paymentName = pType.Name,
                                actualTotal = actualTotal,
                                inSalesTotal = inSalesTotal,
                                differenceTotal = differenceTotal
                            });

                        wactualTotal += actualTotal;
                        winSalesTotal += inSalesTotal;
                        wdifferenceTotal += differenceTotal;
                    }

                    result.WorkTimePaTable = new WorkTimePaTable
                    {
                        wactualTotal = wactualTotal,
                        winSalesTotal = winSalesTotal,
                        wdifferenceTotal = wdifferenceTotal,
                        details = details
                    };
                }
            }

            //tickets promotions
            if (true)
            {
                var allPromotionOrders = allTickets
                    .Where(a => a.TotalAmount > 0M && !string.IsNullOrEmpty(a.TicketPromotionDetails))
                    .SelectMany(a => JsonHelper.Deserialize<List<Dtos.PromotionDetailValue>>(a.TicketPromotionDetails));

                if (allPromotionOrders.Any())
                {
                    var promotionData = new PromotionReportData();

                    var totalCount = 0M;
                    var totalSum = 0M;

                    foreach (var allPro in allPromotionOrders.GroupBy(a => a.PromotionId))
                    {
                        var quantity = allPro.Count();
                        var tAmount = allPro.Sum(a => Math.Abs(a.PromotionAmount));

                        var keyName = allPro.First().PromotionName;

                        promotionData.details.Add(new PromotionReportRowData
                        {
                            promotionName = keyName,
                            quantity = quantity,
                            amount = tAmount
                        });

                        totalCount += quantity;
                        totalSum += tAmount;
                    }

                    promotionData.totalCount = totalCount;
                    promotionData.totalSum = totalSum;

                    result.TicketsPromotionReportData = promotionData;
                }
            }

            //orders promotions
            if (true)
            {
                var allPromotionOrders = allTickets.SelectMany(x => x.Orders)
                    .Where(a => a.CalculatePrice && a.IsPromotionOrder && a.PromotionSyncId > 0)
                    .GroupBy(x => x.PromotionSyncId);

                if (allPromotionOrders.Any())
                {
                    var orderPromotionData = new PromotionReportData();

                    var allPromotions = await _promotionRepository.GetAllListAsync();

                    var totalCount = 0M;
                    var totalSum = 0M;

                    foreach (var allPro in allPromotionOrders)
                    {
                        var promotion = allPromotions.FirstOrDefault(t => t.Id == allPro.Key);

                        if (promotion != null)
                        {
                            var quantity = allPro.Sum(a => a.Quantity);
                            var tAmount = allPro.Sum(a => a.PromotionAmount * a.Quantity);

                            orderPromotionData.details.Add(new PromotionReportRowData
                            {
                                promotionName = promotion.Name,
                                quantity = quantity,
                                amount = tAmount
                            });

                            totalCount += quantity;
                            totalSum += tAmount;
                        }
                    }

                    orderPromotionData.totalCount = totalCount;
                    orderPromotionData.totalSum = totalSum;

                    result.OrdersPromotionReportData = orderPromotionData;
                }
            }

            //tickets count by types
            var ticketGroups = allTickets.GroupBy(x => new { x.TicketTypeName })
                .Select(x => new TicketTypeInfo
                {
                    TicketTypeName = x.Key.TicketTypeName,
                    TicketCount = x.Count(),
                });

            result.TicketCountByTicketTypeData = new List<TicketCountByTicketTypeData>();

            if (ticketGroups.Count() > 1)
                foreach (var ticketTypeInfo in ticketGroups)
                    result.TicketCountByTicketTypeData.Add(new TicketCountByTicketTypeData
                    {
                        ticketTypeName = ticketTypeInfo.TicketTypeName,
                        count = ticketTypeInfo.TicketCount
                    });

            //items
            var totalPaidOrders = allTickets.SelectMany(x => x.Orders)
                .Where(a => a.CalculatePrice && a.DecreaseInventory);
            var totalPaidItemsitemCount = totalPaidOrders.Sum(a => a.Quantity);
            var totalPaidItemsitemValue = totalPaidOrders.Sum(x => x.GetValue());

            result.ItemsData = new ItemsData();

            if (totalPaidOrders.Any())
            {
                result.ItemsData.totalPaidItems = totalPaidItemsitemCount;
                result.ItemsData.totalPaidItemTotal = totalPaidItemsitemValue;
            }

            var itemCount = allTickets.Sum(x => x.Orders.Sum(a => a.Quantity));
            result.ItemsData.itemCount = itemCount;

            var orderStates = allTickets
                .SelectMany(x => x.Orders)
                .SelectMany(x => x.GetOrderStateValues()).Distinct();

            result.OrdersReportData = new OrdersReportData();

            if (orderStates.Any())
            {
                result.OrdersReportData.orderStateReports = new List<StateReportDataRow>();

                foreach (var orderState in orderStates
                    .OrderBy(x => x.OrderKey)
                    .ThenBy(x => x.StateValue))
                {
                    var items = allTickets.SelectMany(x => x.Orders)
                        .Where(x => x.IsInState(orderState.StateName, orderState.State, orderState.StateValue));
                    var amount = items.Sum(x => x.GetValue());
                    var quantity = items.Sum(a => a.Quantity);
                    var count = items.Count();

                    result.OrdersReportData.orderStateReports.Add(new StateReportDataRow
                    {
                        state = orderState.State,
                        stateValue = orderState.StateValue,
                        quantity = quantity,
                        amount = amount,
                        count = count
                    });
                }
            }

            result.OrdersReportData.orderCount = allTickets.Sum(x => x.Orders.Count);

            var totalPaidItemsorderCount = totalPaidOrders.Count();
            var totalPaidItemsorderValue = totalPaidOrders.Sum(x => x.GetValue());

            if (totalPaidItemsorderCount > 0)
            {
                result.OrdersReportData.totalPaidItemsorderCount = totalPaidItemsorderCount;
                result.OrdersReportData.totalPaidItemsorderValue = totalPaidItemsorderValue;
            }

            var ticketStates = allTickets
                .SelectMany(x => x.GetTicketStateValues()).Distinct();

            var ticketCount = 0;

            result.TicketsReportData = new TicketsReportData();
            if (ticketStates.Any())
            {
                result.TicketsReportData.ticketStateReports = new List<StateReportDataRow>();

                foreach (var ticketStateValue in ticketStates)
                {
                    var value = ticketStateValue;
                    var items = allTickets.Where(x => x.IsInState(value.StateName, value.State));
                    var amount = items.Sum(x => x.GetPlainSum());

                    var discount = items
                        .SelectMany(x => x.Transactions)
                        .Where(x => x.TransactionType.Name == "DISCOUNT")
                        .Sum(e => e.Amount);

                    var count = items.Count();

                    if (amount > 0) ticketCount += count;

                    result.TicketsReportData.ticketStateReports.Add(new StateReportDataRow
                    {
                        state = ticketStateValue.State,
                        count = count,
                        amount = amount + discount
                    });
                }
            }

            var firstTicket = allTickets.Any() ? allTickets.First() : null;
            result.TicketsReportData.firstTicketNumber = firstTicket != null ? firstTicket.TicketNumber : "0";

            var lastTicket = allTickets.Any() ? allTickets.Last() : null;
            result.TicketsReportData.lastTicketNumber = lastTicket != null ? lastTicket.TicketNumber : "0";

            if (true)
                if (ticketGroups.Count() > 1)
                {
                    var data = new List<TicketTypeInfoByPaymentTypeReportData>();
                    foreach (var ticketTypeInfo in ticketGroups)
                    {
                        var dinfo = ticketTypeInfo;

                        var groups = allTickets
                            .Where(x => x.TicketTypeName == dinfo.TicketTypeName)
                            .SelectMany(x => x.Payments)
                            .GroupBy(x => new { x.PaymentType.Name })
                            .Select(x => new TenderedAmount { PaymentName = x.Key.Name, Amount = x.Sum(y => y.Amount) });

                        var ticketTypeAmountCalculator = new AmountCalculator(groups);

                        var item = new TicketTypeInfoByPaymentTypeReportData
                        {
                            ticketType = ticketTypeInfo.TicketTypeName,
                            paymentTypes = new List<PaymentTypeRowData>(),
                        };

                        foreach (var paymentName in ticketTypeAmountCalculator.PaymentNames)
                            item.paymentTypes.Add(new PaymentTypeRowData
                            {
                                paymentName = paymentName,
                                percent = ticketTypeAmountCalculator.GetPercent(paymentName),
                                amount = ticketTypeAmountCalculator.GetAmount(paymentName)
                            });

                        item.totalAmount = ticketTypeInfo.Amount;

                        var ddiscounts = allTickets
                            .Where(x => x.TicketTypeName == dinfo.TicketTypeName)
                            .Sum(x => x.GetPreTaxServicesTotal());

                        item.ddiscounts = Math.Abs(ddiscounts);

                        item.tax = ticketTypeInfo.Tax;
                    }
                }

            if (true)
            {
                if (allTickets.Where(a => a.TotalAmount > 0M).Select(x => x.GetTagData())
                    .Where(x => !string.IsNullOrEmpty(x)).Distinct().Any())
                {
                    var dict = new Dictionary<string, List<Transaction.Ticket>>();

                    foreach (var ticket in allTickets.Where(x => x.IsTagged))
                        foreach (var tag in ticket.GetTicketTagValues().Select(x => x.TagName + ":" + x.TagValue))
                        {
                            if (!dict.ContainsKey(tag))
                                dict.Add(tag, new List<Transaction.Ticket>());
                            dict[tag].Add(ticket);
                        }

                    var TicketTagGroups = await _ticketTagGroupRepository.GetAllListAsync();
                    var tagGroups =
                        dict.Select(
                            x =>
                                new TicketTagInfo
                                {
                                    Amount = x.Value.Sum(y => y.TotalAmount),
                                    TicketCount = x.Value.Count,
                                    TagName = x.Key
                                }).OrderBy(x => x.TagName);

                    var tagGrp = tagGroups.GroupBy(x => x.TagName.Split(':')[0]);

                    if (tagGrp.Any())
                    {
                        var allKeys = tagGrp.Select(a => a.Key);
                        if (TicketTagGroups.Any(a => allKeys.Contains(a.Name)))
                        {
                        }
                    }

                    var ticketTags = new List<TicketTagGroupData>();
                    foreach (var grp in tagGrp)
                    {
                        var tag = TicketTagGroups.FirstOrDefault(x => x.Name == grp.Key);
                        if (tag == null) continue;

                        var groupData = new TicketTagGroupData()
                        {
                            groupName = grp.Key,
                            subRows = new List<TicketTagInfo>()
                        };

                        groupData.isDecimal = tag.IsDecimal;
                        if (tag.IsDecimal)
                        {
                            groupData.tCount = grp.Sum(x => x.TicketCount);
                            groupData.tSum = grp.Sum(x => Convert.ToDecimal(x.TagName.Split(':')[1]) * x.TicketCount);
                            groupData.amnt = grp.Sum(x => x.Amount);
                            groupData.rate = groupData.tSum / groupData.amnt;

                            continue;
                        }

                        foreach (var ticketTagInfo in grp)
                            groupData.subRows.Add(new TicketTagInfo
                            {
                                TagName = ticketTagInfo.TagName.Split(':')[1],
                                TicketCount = ticketTagInfo.TicketCount,
                                Amount = ticketTagInfo.Amount
                            });

                        groupData.totalAmount = grp.Sum(x => x.Amount);

                        var sum = 0m;

                        groupData.isInterger = tag.IsInteger;
                        if (tag.IsInteger)
                            try
                            {
                                foreach (var my in grp.Where(a => a.Amount > 0M))
                                {
                                    var allSplit = my.TagName.Split(':');

                                    if (allSplit != null && allSplit.Length > 1)
                                        if (DineConnectUtil.IsDecimal(allSplit[1]))
                                            sum += Convert.ToDecimal(allSplit[1]) * my.TicketCount;
                                }

                                groupData.sum = sum;
                            }
                            catch (FormatException)
                            {
                            }
                        else
                            sum = grp.Sum(x => x.TicketCount);

                        if (sum > 0) groupData.average = groupData.totalAmount / sum;

                        ticketTags.Add(groupData);
                    }

                    result.TicketTagsReportData = ticketTags;
                }

                var owners = allTickets.SelectMany(ticket => ticket.Orders.Where(x => !x.IncreaseInventory).ToList())
                    .GroupBy(x => new { x.CreatingUserName })
                    .Select(
                        x =>
                            new UserInfo
                            {
                                UserName = x.Key.CreatingUserName,
                                Amount = x.Sum(y => y.GetTotal())
                            });

                if (owners.Any())
                {
                    var totalOwnersAmount = 0M;
                    foreach (var ownerInfo in owners) totalOwnersAmount += ownerInfo.Amount;

                    result.OwnersReportData = new UserInfoReportData
                    {
                        details = owners.ToList(),
                        totalOwnersAmount = totalOwnersAmount
                    };
                }
            }

            //#region RefundOwners
            var refundOwners =
                allTickets.SelectMany(ticket => ticket.Orders.Where(x => x.IncreaseInventory).ToList())
                    .GroupBy(x => new { x.CreatingUserName })
                    .Select(
                        x =>
                            new UserInfo
                            {
                                UserName = x.Key.CreatingUserName,
                                Amount = x.Sum(y => y.GetTotal())
                            });

            if (refundOwners.Any())
            {
                var totalRefundAmount = 0M;
                foreach (var ownerInfo in refundOwners) totalRefundAmount += ownerInfo.Amount;
                result.RefundOwnersReportData = new UserInfoReportData
                {
                    details = refundOwners.ToList(),
                    totalOwnersAmount = totalRefundAmount
                };
            }

            //#endregion
            if (true)
            {
                var uInfo =
                    allTickets.SelectMany(x => x.Payments)
                        .Select(x => x.PaymentUserName)
                        .Distinct()
                        .Select(x => new UserInfo { UserName = x });

                if (uInfo.Count() > 1)
                {
                    var co = 0;
                    var paymentByUsersReportData = new List<UserNameByPaymentTypeReportData>();
                    foreach (var userInfo in uInfo)
                    {
                        var userIncomeCalculator = GetIncomeCalculatorByUser(allTickets, userInfo.UserName);
                        if (userIncomeCalculator.TotalAmount > 0M)
                        {
                            var item = new UserNameByPaymentTypeReportData
                            {
                                paymentUserName = userInfo.UserName,
                                paymentTypes = new List<PaymentTypeRowData>(),
                                totalAmount = userIncomeCalculator.TotalAmount
                            };

                            foreach (var paymentName in userIncomeCalculator.PaymentNames)
                                item.paymentTypes.Add(new PaymentTypeRowData
                                {
                                    paymentName = paymentName,
                                    percent = userIncomeCalculator.GetPercent(paymentName),
                                    amount = userIncomeCalculator.GetAmount(paymentName)
                                });

                            paymentByUsersReportData.Add(item);
                        }
                    }

                    result.UsersIncomeReportData = paymentByUsersReportData;
                }
            }
        }
        private IQueryable<Transaction.Ticket> GetWorkPeriodTickets(Period.WorkPeriod workPeriod)
        {
            return GetTicketsForWorkPeriod(workPeriod);
        }
        private AmountCalculator GetIncomeCalculatorByUser(IEnumerable<Transaction.Ticket> tickets,
            string paymentUserName)
        {
            var groups = tickets
                .SelectMany(x => x.Payments.Where(y => y.PaymentUserName == paymentUserName))
                .Where(x => x.Amount >= 0)
                .GroupBy(x => x.PaymentTypeId)
                .Select(x => new TenderedAmount
                { PaymentName = GetPaymentTypeName(x.Key), Amount = x.Sum(y => y.Amount) });

            return new AmountCalculator(groups);
        }
        private IQueryable<Transaction.Ticket> GetTicketsForWorkPeriod(Period.WorkPeriod workPeriod)
        {
            var allTickets = _ticketManager.GetAll()
                .Include(e => e.Orders.Select(o => o.MenuItem))
                .Where(x => !x.PreOrder && !x.Credit && x.LocationId == workPeriod.LocationId);

            var wpStartDay = workPeriod.StartTime.AddDays(-2);
            var wpendDay = workPeriod.EndTime.AddDays(2);

            allTickets = allTickets.Where(a =>
                a.LastPaymentTimeTruc >= wpStartDay && a.LastPaymentTimeTruc <= wpendDay);

            allTickets = allTickets.Where(
                x => x.LastPaymentTime >= workPeriod.StartTime && x.LastPaymentTime <= workPeriod.EndTime);

            return allTickets;
        }

        public async Task<FileDto> BuildCentralPlanDayExport(CentralExportInput input)
        {
            var output = await _exporter.CentralPlanDayExport(input, this);
            return output;
        }

        public async Task<FileDto> BuildCentralShiftExport(CentralShiftExportInput input)
        {
            return await _exporter.CentralShiftDayExport(input, this);
        }

        public async Task<FileDto> BuildCentralTerminalExport(CentralTerminalExportInput input)
        {
            return await _exporter.CentralTerminalDayExport(input, this);
        }
    }
}

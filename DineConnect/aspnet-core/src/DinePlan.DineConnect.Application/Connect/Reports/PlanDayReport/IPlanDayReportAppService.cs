﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport
{
    public interface IPlanDayReportAppService: IApplicationService
    {
        Task<List<WorkPeriodSummaryOutput>> BuildWorkPeriodByLocationForDatesWithPaging(WorkPeriodSummaryInputWithPaging input);
        Task<FileDto> BuildWorkPeriodByLocationForDatesWithPagingToExcel(WorkPeriodSummaryInputWithPaging input);
        Task<CentralDayReportData> BuildCentralPlanDayReport(int workPeriodId);
        Task<CentralDayReportData> BuildCentralShiftReport(CentralShiftReportInput input);
        Task<CentralDayReportData> BuildCentralTerminalReport(CentralTerminalReportInput input);
        Task<FileDto> BuildCentralPlanDayExport(CentralExportInput input);
        Task<FileDto> BuildCentralShiftExport(CentralShiftExportInput input);
        Task<FileDto> BuildCentralTerminalExport(CentralTerminalExportInput input);
    }
}

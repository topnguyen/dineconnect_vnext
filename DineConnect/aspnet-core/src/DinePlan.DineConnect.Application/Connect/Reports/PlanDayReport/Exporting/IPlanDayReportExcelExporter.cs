﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Exporting
{
    public interface IPlanDayReportExcelExporter
    {
        Task<FileDto> ExportToFilePlanWorkDay(WorkPeriodSummaryInputWithPaging input, IPlanDayReportAppService appService);
        Task<FileDto> CentralPlanDayExport(CentralExportInput input, IPlanDayReportAppService centralAppService);
        Task<FileDto> CentralShiftDayExport(CentralShiftExportInput input, IPlanDayReportAppService centralAppService);
        Task<FileDto> CentralTerminalDayExport(CentralTerminalExportInput input, IPlanDayReportAppService centralAppService);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class PaymentTypeReportData
    {
        public decimal totalSales { get; set; }

        public List<PaymentTypeRowData> details { get; set; }
    }
}

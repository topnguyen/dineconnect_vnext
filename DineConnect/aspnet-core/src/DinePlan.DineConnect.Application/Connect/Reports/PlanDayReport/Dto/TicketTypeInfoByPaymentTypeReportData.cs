﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TicketTypeInfoByPaymentTypeReportData
    {
        internal object ddiscounts;
        internal decimal tax;

        public string ticketType { get; set; }

        public List<PaymentTypeRowData> paymentTypes { get; set; }

        public decimal totalAmount { get; set; }
    }
}

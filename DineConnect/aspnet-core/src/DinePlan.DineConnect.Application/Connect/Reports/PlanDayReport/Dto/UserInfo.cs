﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class UserInfo
    {
        public string UserName { get; set; }
        public decimal Amount { get; set; }
        public int UserId { get; set; }
    }
}

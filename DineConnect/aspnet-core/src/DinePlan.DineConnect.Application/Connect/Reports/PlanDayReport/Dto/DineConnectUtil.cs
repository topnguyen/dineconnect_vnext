﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class DineConnectUtil
    {
        public static bool IsInteger(string value)
        {
            int num;
            return int.TryParse(value, out num);
        }

        public static bool IsDecimal(string value)
        {
            decimal num;
            return decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out num);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TicketsReportData
    {
        public List<StateReportDataRow> ticketStateReports;

        public string firstTicketNumber;

        public string lastTicketNumber;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TicketTypeReportData
    {
        public decimal TotalSales { get; set; }
        public int TicketCount { get; set; }
        public decimal OrderTotal { get; set; }

        public decimal Tax { get; set; }

        public decimal TotalVoid;
        public decimal TotalComp;
        public decimal TotalGift;
        public decimal TotalRefund;
        public decimal ItemSales { get; set; }
        public decimal ItemDiscountTotal { get; set; }
        public decimal TicketDiscountTotal { get; set; }

        public decimal RoundingTotal { get; set; }

        public Dictionary<string, decimal> Transactions { get; set; }
        public decimal ItemCount { get; set; }
        public decimal PaxCount { get; set; }
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal SubTotalSum { get; set; }

        #region Get Methods
        public decimal AvePaxCount
        {
            get
            {
                if (PaxCount > 0) return TotalSales / PaxCount;

                return 0;
            }
        }
        public decimal AvgItemCount
        {
            get
            {
                if (ItemCount > 0) return TotalSales / ItemCount;

                return 0;
            }
        }
        public decimal AvgItemCountReceipt
        {
            get
            {
                if (TicketCount > 0) return ItemCount / TicketCount;

                return 0;
            }
        }
        public decimal Average
        {
            get
            {
                if (TicketCount > 0) return TotalSales / TicketCount;

                return 0;
            }
        }
        public decimal SubTotal => TotalSales - Tax;
        public decimal GrossTotal => TotalSales + TicketDiscountTotal + RoundingTotal;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class UserNameByPaymentTypeReportData
    {
        public string paymentUserName { get; set; }

        public List<PaymentTypeRowData> paymentTypes { get; set; }

        public decimal totalAmount { get; set; }
    }
}

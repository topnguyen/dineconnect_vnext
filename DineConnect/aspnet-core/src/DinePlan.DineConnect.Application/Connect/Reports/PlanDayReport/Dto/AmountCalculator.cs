﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class AmountCalculator
    {
        private readonly IEnumerable<TenderedAmount> _amounts;

        public AmountCalculator(IEnumerable<TenderedAmount> amounts)
        {
            _amounts = amounts;
        }

        public IEnumerable<string> PaymentNames
        {
            get { return _amounts.Select(x => x.PaymentName).Distinct(); }
        }

        public decimal TotalAmount
        {
            get { return _amounts.Sum(x => x.Amount); }
        }

        public decimal GetAmount(string paymentName)
        {
            var r = _amounts.Where(x => x.PaymentName == paymentName);
            if (r != null && r.Any())
            {
                return r.Sum(a => a.Amount);
            }
            return 0;
        }

        public decimal GetActualAmount(string paymentName)
        {
            var r = _amounts.Where(x => x.PaymentName == paymentName);
            if (r != null && r.Any())
            {
                return r.Sum(a => a.Amount);
            }
            return 0;
        }

        public decimal GetPercent(string paymentName)
        {
            return TotalAmount > 0 ? (GetAmount(paymentName) * 100) / TotalAmount : 0;
        }

        public int GetCount(string paymentName)
        {
            var r = _amounts.Where(x => x.PaymentName == paymentName);
            if (r != null && r.Any())
            {
                return r.Sum(a => a.PaymentCount);
            }
            return 0;

        }
    }
}

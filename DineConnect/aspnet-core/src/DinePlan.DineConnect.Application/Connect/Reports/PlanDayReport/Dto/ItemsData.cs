﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class ItemsData
    {
        public decimal totalPaidItems { get; set; }

        public decimal totalPaidItemTotal { get; set; }

        public decimal itemCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class ScheduleReportData
    {
        public string ScheduleName { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }

        public decimal Quantity { get; set; }
        public decimal Amount => Price * Quantity;
        public decimal Price { get; set; }
    }
}

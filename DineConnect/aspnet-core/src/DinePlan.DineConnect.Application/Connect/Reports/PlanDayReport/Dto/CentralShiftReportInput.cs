﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class CentralShiftReportInput 
    {
        public int workPeriodId { get; set; }
        public string workShiftId { get; set; }
        public DateTime DateReport { get; set; }
        public bool RunInBackground { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public string ReportDescription { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }
    }

    public class CentralTerminalReportInput
    {
        public int workPeriodId { get; set; }

        public List<string> terminals { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class DepartmentSales
    {
        public string Name { get; set; }
        public string TotalTicketCount { get; set; }
        public decimal Amount { get; set; }
        public decimal ItemCount { get; set; }
        public decimal NetSales => Amount - TaxTotal;

        public decimal TaxTotal { get; set; }


    }
}

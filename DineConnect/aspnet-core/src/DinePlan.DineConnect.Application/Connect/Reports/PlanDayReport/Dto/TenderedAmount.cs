﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TenderedAmount
    {
        public string PaymentName { get; set; }
        public decimal Amount { get; set; }
        public decimal ActualAmount { get; set; }
        public int PaymentCount { get; set; }
    }
}

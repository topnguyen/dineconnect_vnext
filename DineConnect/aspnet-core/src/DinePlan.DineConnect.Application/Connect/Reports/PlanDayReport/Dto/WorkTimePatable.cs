﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class WorkTimePaTable
    {
        public decimal wactualTotal { get; set; }
        public decimal winSalesTotal { get; set; }
        public decimal wdifferenceTotal { get; set; }
        public List<PaymentTypeByWorkTimeRowData> details { get; set; }
    }
}

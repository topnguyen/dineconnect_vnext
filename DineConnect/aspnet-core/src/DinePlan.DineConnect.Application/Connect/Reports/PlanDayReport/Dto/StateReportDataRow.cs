﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class StateReportDataRow
    {
        public int count;

        public string state { get; set; }

        public string stateValue { get; set; }

        public decimal quantity { get; set; }

        public decimal amount { get; set; }
    }
}

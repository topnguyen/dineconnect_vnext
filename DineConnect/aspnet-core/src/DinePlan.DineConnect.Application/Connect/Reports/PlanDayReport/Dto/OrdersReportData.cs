﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class OrdersReportData
    {
        public List<StateReportDataRow> orderStateReports { get; set; }

        public int orderCount { get; set; }

        public int totalPaidItemsorderCount { get; set; }

        public decimal totalPaidItemsorderValue { get; set; }
    }
}

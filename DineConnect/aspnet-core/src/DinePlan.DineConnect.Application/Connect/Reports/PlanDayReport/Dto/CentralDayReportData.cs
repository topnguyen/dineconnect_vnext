﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class CentralDayReportData
    {
        public DinePlan.DineConnect.Connect.Master.Locations.Location LocationDetails { get; set; }

        public TicketTypeReportData TicketTypeReportData { get; set; }

        public List<object> Departments { get; set; }

        public PaymentTypeReportData PaymentTypeReportData { get; set; }

        public CashReportData CashReportData { get; internal set; }

        public WorkTimePaTable WorkTimePaTable { get; internal set; }

        public PromotionReportData TicketsPromotionReportData { get; set; }

        public PromotionReportData OrdersPromotionReportData { get; set; }

        public List<TicketCountByTicketTypeData> TicketCountByTicketTypeData { get; set; }

        public ItemsData ItemsData { get; set; }

        public OrdersReportData OrdersReportData { get; set; }

        public TicketsReportData TicketsReportData { get; set; }

        public UserInfoReportData OwnersReportData { get; set; }

        public UserInfoReportData RefundOwnersReportData { get; set; }

        public List<UserNameByPaymentTypeReportData> UsersIncomeReportData { get; internal set; }

        public List<TicketTagGroupData> TicketTagsReportData { get; set; }

        public List<SalesItemSumaryDto> SalesGroups { get; set; }

        public List<SalesItemSumaryDto> SalesCategories { get; set; }
        public List<SalesItemSumaryDto> PeriodData { get; set; }
        public List<SalesItemSumaryDto> DiscountData { get; set; }

        public CentralDayReportData()
        {
            SalesGroups = new List<SalesItemSumaryDto>();
            SalesCategories = new List<SalesItemSumaryDto>();
            PeriodData = new List<SalesItemSumaryDto>();
            DiscountData = new List<SalesItemSumaryDto>();
            UsersIncomeReportData = new List<UserNameByPaymentTypeReportData>();
            TicketTagsReportData = new List<TicketTagGroupData>();
            TicketCountByTicketTypeData = new List<TicketCountByTicketTypeData>();
            Departments = new List<object>();
            LocationDetails = new Master.Locations.Location();
            OrdersPromotionReportData = new PromotionReportData();
            OwnersReportData = new UserInfoReportData();
            RefundOwnersReportData = new UserInfoReportData();
            TicketsPromotionReportData = new PromotionReportData();
            WorkTimePaTable = new WorkTimePaTable();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class UserInfoReportData
    {
        public List<UserInfo> details { get; set; }
        public decimal totalOwnersAmount { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class ScheduleSetting
    {
        [JsonProperty("startHour")]
        public int StartHour { get; set; }

        [JsonProperty("endHour")]
        public int EndHour { get; set; }

        [JsonProperty("startMinute")]
        public int StartMinute { get; set; }

        [JsonProperty("endMinute")]
        public int EndMinute { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

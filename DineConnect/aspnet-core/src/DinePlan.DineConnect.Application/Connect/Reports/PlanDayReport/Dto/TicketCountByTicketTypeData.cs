﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TicketCountByTicketTypeData
    {
        public string ticketTypeName { get; set; }

        public int count { get; set; }
    }
}

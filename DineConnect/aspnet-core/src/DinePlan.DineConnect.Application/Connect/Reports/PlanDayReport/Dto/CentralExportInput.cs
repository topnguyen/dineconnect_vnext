﻿using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class CentralExportInput : IDateCentralExport
    {
        public int WorkPeriodId { get; set; }
        public ExportType ExportOutputType { get; set; }

        public DateTime DateReport { get; set; }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }

        public int TenantId { get; set; }
        public long UserId { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

    public class CentralShiftExportInput : IDateCentralExport
    {
        public CentralShiftReportInput CentralShiftReportInput { get; set; }
        public ExportType ExportOutputType { get; set; }
        public WorkPeriodSummaryOutput WorkPeriod { get; set; }

        public DateTime DateReport { get; set; }
    }

    public class CentralTerminalExportInput : IDateCentralExport
    {
        public CentralTerminalReportInput CentralTerminalReportInput { get; set; }
        public ExportType ExportOutputType { get; set; }

        public DateTime DateReport { get; set; }
    }

    public interface IDateCentralExport
    {
        DateTime DateReport { get; set; }
    }
}

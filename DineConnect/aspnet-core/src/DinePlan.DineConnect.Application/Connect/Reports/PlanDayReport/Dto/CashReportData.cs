﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class CashReportData
    {
        public int cashIn { get; set; }
        public decimal allIncomeAmount { get; set; }
        public int cashOut { get; set; }
        public decimal allExpAmount { get; set; }
        public decimal cashInDrawer { get; set; }
        public decimal cashAmount { get; set; }
    }
}

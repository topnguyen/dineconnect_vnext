﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class TicketTypeInfo
    {
        public decimal Amount { get; set; }
        public decimal PreTaxCalculationTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal PlainSum { get; set; }
        public decimal PostTaxCalculationTotal { get; set; }
        public Dictionary<string, decimal> ServicesTotal { get; set; }
        public int TicketCount { get; set; }
        public string TicketTypeName { get; set; }
        public bool TaxIncluded { get; set; }
    }
}

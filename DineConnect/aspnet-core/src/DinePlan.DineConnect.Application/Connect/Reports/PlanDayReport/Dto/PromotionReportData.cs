﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.PlanDayReport.Dto
{
    public class PromotionReportData
    {
        public decimal totalCount { get; set; }

        public decimal totalSum { get; set; }

        public List<PromotionReportRowData> details { get; set; }

        public PromotionReportData()
        {
            details = new List<PromotionReportRowData>();
        }
    }

    public class PromotionReportRowData
    {
        public string promotionName { get; set; }

        public decimal quantity { get; set; }

        public decimal amount { get; set; }
    }
}

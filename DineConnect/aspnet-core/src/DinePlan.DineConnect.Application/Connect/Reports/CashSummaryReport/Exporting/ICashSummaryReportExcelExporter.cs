﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.CashSummaryReport.Exporting
{
    public interface ICashSummaryReportExcelExporter
    {
        Task<FileDto> ExportCashSummaryReport(GetCashSummaryReportInput input, ICashSummaryReportAppService appService);
    }
}

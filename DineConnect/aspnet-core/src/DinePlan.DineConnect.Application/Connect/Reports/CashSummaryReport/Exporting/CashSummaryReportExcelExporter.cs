﻿using Abp.AspNetZeroCore.Net;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports.CashSummaryReport.Exporting
{
    public class CashSummaryReportExcelExporter : EpPlusExcelExporterBase, ICashSummaryReportExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        public CashSummaryReportExcelExporter(ITempFileCacheManager tempFileCacheManager, ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportCashSummaryReport(GetCashSummaryReportInput input, ICashSummaryReportAppService appService)
        {
            Dictionary<string, int> colCountDics = new Dictionary<string, int>();

            var file = new FileDto("Cash-Summary" + input.StartDate.ToString("yyyy-MM-dd") + "-" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var worksheet = excelPackage.Workbook.Worksheets.Add(L("CashSummaryReport"));
                worksheet.OutLineApplyStyle = true;
                var setting = await _tenantSettingsAppService.GetAllSettings();
                var dateTimeFormat = setting.FileDateTimeFormat;
                var dateFormat = "dd-MM-yyyy";
                AddHeader(
                    worksheet,
                    L("Plant"),
                    L("PlantName"),
                    L("SoldTo"),
                    L("DateOfSale"),
                    L("Shift"),
                    L("CloseShiftBy"),
                    L("StartTimeOfEachShift"),
                    L("EndTimeOfEachShift"),
                    L("SaleDetail"),
                    L("SaleAmount(ExcludedWHLDTaxAmount)"),
                    L("TaxID")
                    );

                worksheet.Cells[$"A{1}:K{1}"].Style.Font.Name = "Tahoma";
                worksheet.Cells[$"A{1}:K{1}"].Style.Font.Size = 14;

                var dtos = await appService.BuildCashSummaryReport(input);

                //Add  report data
                var row = 2;
                //location
                foreach (var dataByPlant in dtos.ListItem)
                {
                    foreach (var dataByDate in dataByPlant.ListItem)
                    {
                        var indexShift = 1;
                        foreach (var dataByShift in dataByDate.ListItem)
                        {
                            foreach (var item in dataByShift.ListItem)
                            {
                                worksheet.Cells[row, 1].Value = dataByPlant.PlantCode;
                                worksheet.Cells[row, 2].Value = dataByPlant.PlantName;
                                worksheet.Cells[row, 3].Value = dataByPlant.SoldToID;
                                worksheet.Cells[row, 4].Value = dataByDate.Date.ToString(dateFormat);
                                worksheet.Cells[row, 5].Value = L("Shift") + " " + indexShift + $"( {dataByShift.StartUser} )";
                                worksheet.Cells[row, 6].Value = item.StopUser;
                                worksheet.Cells[row, 7].Value = dataByShift.StartShift;
                                worksheet.Cells[row, 8].Value = dataByShift.EndShift;
                                worksheet.Cells[row, 9].Value = item.Detail;
                                worksheet.Cells[row, 10].Value = item.Amount.ToString();
                                worksheet.Cells[row, 11].Value = item.TaxId;
                                worksheet.Cells[$"A{row}:K{row}"].Style.Font.Name = "Tahoma";
                                worksheet.Cells[$"A{row}:K{row}"].Style.Font.Size = 12;
                                row++;
                            }
                            indexShift++;
                        }
                    }
                }
                for (var i = 1; i <= 11; i++)
                {
                    worksheet.Column(i).AutoFit();
                }
                SaveToPath(excelPackage, file);
            }
            return ProcessFile(input.ExportOutputType, file);
        }
    }
}

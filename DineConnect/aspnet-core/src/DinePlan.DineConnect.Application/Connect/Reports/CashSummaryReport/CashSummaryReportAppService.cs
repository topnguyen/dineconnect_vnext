﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;

using DinePlan.DineConnect.Dto;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;


using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.BaseCore.Report;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Reports.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Transaction;



using Microsoft.EntityFrameworkCore;

using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Authorization;
using DinePlan.DineConnect.Authorization;

namespace DinePlan.DineConnect.Connect.Reports.CashSummaryReport
{
    
    public class CashSummaryReportAppService : DineConnectReportAppServiceBase, ICashSummaryReportAppService
    {
        private readonly IRepository<Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Master.PaymentTypes.PaymentType> _payRe;
        private readonly ICashSummaryReportExcelExporter _exporter;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        public CashSummaryReportAppService(
            ILocationAppService locService
            , IRepository<Ticket> ticketRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Department> departmentRepo
            , IRepository<Transaction.Ticket> ticketManager
            , IRepository<Department> drepo
            , IRepository<Location> lRepo
            , IRepository<Transaction.Order> orderManager
            , IReportBackgroundAppService rbas
            , IBackgroundJobManager bgm
            , ICashSummaryReportExcelExporter exporter
            , IRepository<Period.WorkPeriod> workPeriod
            , IRepository<Master.PaymentTypes.PaymentType> payRe
            , ITenantSettingsAppService tenantSettingsAppService
            ) : base(locService, ticketRepo, unitOfWorkManager, departmentRepo)
        {
            _ticketManager = ticketManager;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _tenantSettingsAppService = tenantSettingsAppService;
        }



        #region CashSummaryReport

        public async Task<GetCashSummaryReportOutput> BuildCashSummaryReport(GetCashSummaryReportInput input)
        {
            var allsetting = await _tenantSettingsAppService.GetAllSettings();
            var formatDate = allsetting.FileDateTimeFormat.FileDateFormat;
            var result = new GetCashSummaryReportOutput();
            var listTickets = new List<GetCashSummaryReportByItem>();

            var fromDate = input.StartDate.Date;
            var endDate = input.EndDate.Date.AddDays(1).AddMilliseconds(-1);

            var cash = await _payRe.FirstOrDefaultAsync(x => x.Name.ToUpper() == AppConsts.Cash.ToUpper());
            var wht = _payRe.GetAll().Where(x => x.ProcessorName.ToUpper().Contains(AppConsts.WithHoldTax.ToUpper()));
            var _cashPaymentTypeId = cash?.Id ?? 0;
            var _whtPaymentTypeId = wht.Select(a => a.Id).ToList();

            while (fromDate < endDate)
            {
                var lstWP = await GetWorkTimessOnWorkPeriod(fromDate);
                foreach (var wp in lstWP)
                {
                    if (wp.StartDate.Date == fromDate.Date)
                    {
                        var listGroupsOfPayments = await GetWorkTimeTickets(wp, input);
                        if (listGroupsOfPayments == null)
                            continue;

                        foreach (var myTicket in listGroupsOfPayments)
                        {

                            foreach (var grpPayment in myTicket.Payments)
                            {
                                if (_whtPaymentTypeId.Any() && _whtPaymentTypeId.Contains(grpPayment.PaymentTypeId))
                                {
                                    var allCashPayments = myTicket.Payments
                                        .Where(a => a.PaymentTypeId == _cashPaymentTypeId).Sum(a => a.Amount);

                                    if (allCashPayments > 0M)
                                    {
                                        string taxId = GetTaxId(myTicket.TicketTags);
                                        listTickets.Add(new GetCashSummaryReportByItem

                                        {
                                            Detail = L(AppConsts.SalesWithHoldingTax),
                                            TaxId = taxId,
                                            Amount = allCashPayments,
                                            WithTWH = true,
                                            AddOn = myTicket.Location.AddOn,
                                            PlantCode = myTicket.Location.Code,
                                            PlantName = myTicket.Location.Name,
                                            Date = fromDate,
                                            StartUser = wp.StartUser,
                                            StopUser = wp.StopUser,
                                            StartShift = wp.StartDate.ToString("HH:mm"),
                                            EndShift = wp.EndDate.ToString("HH:mm"),
                                            LocationID = myTicket.LocationId,
                                        });
                                        break;
                                    }

                                }
                                else
                                {
                                    if (grpPayment.PaymentTypeId == _cashPaymentTypeId)
                                    {
                                        var ticket = listTickets.FirstOrDefault(l => !l.WithTWH && l.Date == fromDate && l.LocationID == wp.LocationId);
                                        if (ticket == null)
                                        {
                                            ticket = new GetCashSummaryReportByItem
                                            {
                                                Detail = L(AppConsts.CashSales),
                                                TaxId = null,
                                                WithTWH = false,
                                                AddOn = myTicket.Location.AddOn,
                                                PlantCode = myTicket.Location.Code,
                                                PlantName = myTicket.Location.Name,
                                                Date = fromDate,
                                                StartUser = wp.StartUser,
                                                StopUser = wp.StopUser,
                                                StartShift = wp.StartDate.ToString("HH:mm"),
                                                EndShift = wp.EndDate.ToString("HH:mm"),
                                                LocationID = wp.LocationId,
                                            };
                                            listTickets.Add(ticket);
                                        }
                                        ticket.Amount += grpPayment.Amount;
                                    }
                                }
                            }

                        }
                    }
                }
                fromDate = fromDate.AddDays(1);
            }



            var mapdataAsQueryable = listTickets.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
                }
            }

            var getCashSummaryBy_Plant_Date_Shift = mapdataAsQueryable.OrderBy(x => x.Date).GroupBy(x => new
            {
                x.PlantCode,
                x.PlantName,
                x.SoldToID,
                x.Date,
                x.StartShift,
                x.EndShift,
                x.StartUser
            }).Select(x => new GetCashSummaryReportByShift
            {
                PlantCode = x.Key.PlantCode,
                PlantName = x.Key.PlantName,
                Date = x.Key.Date,
                EndShift = x.Key.EndShift,
                StartShift = x.Key.StartShift,
                StartUser = x.Key.StartUser,
                SoldToID = x.Key.SoldToID,
                ListItem = x.ToList()
            }).OrderBy(x => x.Date).ToList();
            var getCashSummaryBy_Plant_Date = getCashSummaryBy_Plant_Date_Shift.GroupBy(x => new
            {
                x.PlantCode,
                x.PlantName,
                x.Date,
                x.SoldToID,
                x.StartUser
            }).Select(x => new GetCashSummaryReportByDate
            {
                PlantCode = x.Key.PlantCode,
                PlantName = x.Key.PlantName,
                Date = x.Key.Date,
                SoldToID = x.Key.SoldToID,
                ListItem = x.ToList()
            }).OrderBy(x => x.Date).ToList();

            var getCashSummaryBy_Plant = getCashSummaryBy_Plant_Date.GroupBy(x => new
            {
                x.PlantCode,
                x.PlantName,
                x.SoldToID
            }).Select(x => new GetCashSummaryReportByPlant
            {
                PlantCode = x.Key.PlantCode,
                PlantName = x.Key.PlantName,
                SoldToID = x.Key.SoldToID,

                ListItem = x.ToList()
            }).ToList();
            result.ListItem = getCashSummaryBy_Plant;
            return result;
        }

        private string GetTaxId(string ticketTags)
        {
            var result = "";
            if (!string.IsNullOrEmpty(ticketTags))
            {
                var listTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(ticketTags);
                var tax = listTag.Find(x => x.TagName == AppConsts.TaxIds);
                if (tax != null)
                {
                    result = tax.TagValue;
                }
            }
            return result;
        }
        private async Task<List<Transaction.Ticket>> GetWorkTimeTickets(WorkShiftByLocationDto wt, GetCashSummaryReportInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var defWorkTime = wt;
                if (defWorkTime == null)
                    return null;
                if (wt.StartDate.Equals(wt.EndDate))
                {
                    var ticketByLocation = GetAllTickets(input);
                    var tickets = await ticketByLocation.Where(x => x.LastPaymentTime == defWorkTime.StartDate && !x.PreOrder && !x.Credit && x.LocationId == wt.LocationId).ToListAsync();
                    return tickets;
                }
                else
                {
                    var ticketByLocation = GetAllTicketsForTicketInputQueryable(input);
                    var tickets = await ticketByLocation.Where(x => x.LastPaymentTime >= defWorkTime.StartDate && x.LastPaymentTime <= defWorkTime.EndDate && !x.PreOrder && !x.Credit && x.LocationId == wt.LocationId).ToListAsync();
                    return tickets;
                }
            }
        }
        private async Task<List<WorkShiftByLocationDto>> GetWorkTimessOnWorkPeriod(DateTime startDate)
        {
            var lstShiftDto = new List<WorkShiftByLocationDto>();

            var lstwt = await _workPeriod.GetAll().Where(x => x.StartTime.Year == startDate.Year &&
                                x.StartTime.Month == startDate.Month &&
                                x.StartTime.Day == startDate.Day).ToListAsync();

            foreach (var wt in lstwt)
            {
                if (wt != null && !string.IsNullOrEmpty(wt.WorkPeriodInformations))
                {
                    var wf = JsonConvert.DeserializeObject<List<WorkShiftByLocationDto>>(wt.WorkPeriodInformations);
                    foreach (var item in wf)
                    {
                        item.LocationId = wt.LocationId;
                    }
                    lstShiftDto.AddRange(wf);
                }
            }


            return lstShiftDto;
        }

        public async Task<FileDto> BuildCashSummaryReportToExport(GetCashSummaryReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
                input.MaxResultCount = 10000;
            var output = await _exporter.ExportCashSummaryReport(input, this);
            return output;
        }
        public IQueryable<Ticket> GetAllTicketsForTicketInputQueryable(GetCashSummaryReportInput input)
        {
            var tickets = GetAllTickets(input)
                .Include(t => t.Payments).ThenInclude(x => x.PaymentType)
                .Include(t => t.Transactions).ThenInclude(x => x.TransactionType)
                .AsQueryable(); ;

            if (input.Payments != null && input.Payments.Any())
                tickets = tickets.Include(a => a.Payments)
                    .WhereIf(input.Payments.Any(),
                        p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

            if (input.Transactions != null && input.Transactions.Any())
            {
                var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                tickets = from p in tickets.Include(a => a.Transactions)
                          where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                          select p;
            }

            if (!string.IsNullOrEmpty(input.TerminalName))
                tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

            if (!string.IsNullOrEmpty(input.Department))
                tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
            if (input.Departments != null && input.Departments.Any())
            {
                var allDepartments = input.Departments.Select(a => a.DisplayText).ToList();
                tickets = tickets.Where(a =>
                    a.DepartmentName != null &&
                    allDepartments.Contains(a.DepartmentName));
            }

            if (input.TicketTags != null && input.TicketTags.Any())
            {
                var allTags = input.TicketTags.Select(a => a.DisplayText).ToArray();
                tickets = from c in tickets
                          where allTags.Any(i => c.TicketTags.Contains(i))
                          select c;
            }

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

            if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

            return tickets;
        }

        #endregion  CashSummaryReport
    }
}

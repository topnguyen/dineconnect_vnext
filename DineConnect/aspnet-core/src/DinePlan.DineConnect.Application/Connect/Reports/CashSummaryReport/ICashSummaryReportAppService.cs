﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace DinePlan.DineConnect.Connect.Reports.CashSummaryReport
{  
    public interface ICashSummaryReportAppService : IApplicationService
    {
        Task<GetCashSummaryReportOutput> BuildCashSummaryReport(GetCashSummaryReportInput input);
        Task<FileDto> BuildCashSummaryReportToExport(GetCashSummaryReportInput input);

    }
}

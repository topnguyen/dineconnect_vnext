﻿using System;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using DinePlan.DineConnect.Connect.Job.Dto;
using DinePlan.DineConnect.Connect.Setup;
using DinePlan.DineConnect.Utility.Rest;

namespace DinePlan.DineConnect.Connect.Job
{
    public class ConnectSetupJobAppService  :  DineConnectAppServiceBase, IConnectSetupJobAppService
    {
        private readonly IBackgroundJobManager _backgroundJobManager;

        public ConnectSetupJobAppService(IBackgroundJobManager  backgroundJobManager)
        {
            _backgroundJobManager = backgroundJobManager;
        }
        public async Task ApiSyncDineConnect(ConnectSyncJobArgs args)
        {
            if (AbpSession.TenantId != null)
            {
                args.LocalTenantId = AbpSession.TenantId.Value;
                await _backgroundJobManager.EnqueueAsync<ConnectSyncJob, ConnectSyncJobArgs>(args);
            }
        }
    }
}

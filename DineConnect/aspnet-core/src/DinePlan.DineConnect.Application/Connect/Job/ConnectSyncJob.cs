﻿using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using DinePlan.DineConnect.Connect.Job.Dto;
using DinePlan.DineConnect.Connect.Setup;

namespace DinePlan.DineConnect.Connect.Job
{
    public class ConnectSyncJob : AsyncBackgroundJob<ConnectSyncJobArgs>, ITransientDependency
    {
        private readonly IConnectSetupAppService _connectSetupAppService;

        public ConnectSyncJob(IConnectSetupAppService connectSetupAppService)
        {
            _connectSetupAppService = connectSetupAppService;
        }

        protected override async Task ExecuteAsync(ConnectSyncJobArgs args)
        {
            await _connectSetupAppService.SyncDineConnect(args);
        }
    }
}
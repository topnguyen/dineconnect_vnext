﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups
{
    
    public class ProductGroupsAppService : DineConnectAppServiceBase, IProductGroupsAppService
    {
        private readonly IRepository<ProductGroup> _productGroupRepository;
        private readonly IProductGroupsExcelExporter _productGroupsExcelExporter;
        private readonly ConnectSession _connectSession;

        public ProductGroupsAppService(
            IRepository<ProductGroup> productGroupRepository,
            IProductGroupsExcelExporter productGroupsExcelExporter,
            ConnectSession connectSession)
        {
            _productGroupRepository = productGroupRepository;
            _productGroupsExcelExporter = productGroupsExcelExporter;
            _connectSession = connectSession;
        }

        public async Task<ListResultDto<ProductGroupDto>> GetAll()
        {
            //System.Linq.Expressions.Expression<System.Func<ProductGroup, bool>> p = e =>
            //              EF.Functions.Like(e.Code, input.Filter.ToILikeString()) ||
            //              EF.Functions.Like(e.Name, input.Filter.ToILikeString());
            var filteredProductGroups = _productGroupRepository.GetAll()
                        .Include(lg => lg.Categories);

            var productGroups = filteredProductGroups.Select(g => ObjectMapper.Map<ProductGroupDto>(g));

            return new ListResultDto<ProductGroupDto>(await productGroups.ToListAsync());
        }

        public async Task<GetProductGroupForViewDto> GetProductGroupForView(int id)
        {
            var productGroup = await _productGroupRepository.GetAsync(id);

            var output = new GetProductGroupForViewDto { ProductGroup = ObjectMapper.Map<ProductGroupDto>(productGroup) };

            return output;
        }

        
        public async Task<GetProductGroupForEditOutput> GetProductGroupForEdit(EntityDto input)
        {
            var productGroup = await _productGroupRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProductGroupForEditOutput { ProductGroup = ObjectMapper.Map<CreateOrEditProductGroupDto>(productGroup) };

            return output;
        }

        public async Task<int> CreateOrEdit(CreateOrEditProductGroupDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            ValidateForCreateOrUpdate(input);
            if (!input.Id.HasValue)
            {
                return await Create(input);
            }
            else
            {
                await Update(input);
                return input.Id.Value;
            }
        }

        
        protected virtual async Task<int> Create(CreateOrEditProductGroupDto input)
        {
            var productGroup = ObjectMapper.Map<ProductGroup>(input);

            if (AbpSession.TenantId != null)
            {
                productGroup.TenantId = AbpSession.TenantId.Value;
            }

            return await _productGroupRepository.InsertAndGetIdAsync(productGroup);
        }

        
        protected virtual async Task Update(CreateOrEditProductGroupDto input)
        {
            var productGroup = await _productGroupRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, productGroup);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _productGroupRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetProductGroupsToExcel(GetAllProductGroupsInput input)
        {
            var filteredProductGroups = _productGroupRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                       EF.Functions.Like(e.Code, input.Filter.ToILikeString()) ||
                       EF.Functions.Like(e.Name, input.Filter.ToILikeString()));

            var query = filteredProductGroups.Select(g => new GetProductGroupForViewDto { ProductGroup = ObjectMapper.Map<ProductGroupDto>(g) });

            var productGroupListDtos = await query.ToListAsync();

            return _productGroupsExcelExporter.ExportToFile(productGroupListDtos);
        }

        protected virtual void ValidateForCreateOrUpdate(CreateOrEditProductGroupDto input)
        {
            var exists = _productGroupRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }
            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<CreateOrEditProductGroupDto> GetOrCreateByName(string groupname, string code)
        {
            CreateOrEditProductGroupDto editDto;

            var allItems = _productGroupRepository
               .GetAll()
               .WhereIf(!groupname.IsNullOrEmpty(), p => p.Name.Contains(groupname));

            if (allItems.Any())
            {
                editDto = ObjectMapper.Map<CreateOrEditProductGroupDto>(allItems.First());
            }
            else
            {
                editDto = new CreateOrEditProductGroupDto()
                {
                    Name = groupname,
                    Code = code
                };
                var id = await CreateOrEdit(editDto);

                editDto.Id = id;
            }
            return editDto;
        }
    }
}
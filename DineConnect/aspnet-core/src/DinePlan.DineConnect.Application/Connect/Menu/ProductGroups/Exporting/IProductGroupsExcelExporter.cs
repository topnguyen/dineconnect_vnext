﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Exporting
{
    public interface IProductGroupsExcelExporter
    {
        FileDto ExportToFile(List<GetProductGroupForViewDto> productGroups);
    }
}
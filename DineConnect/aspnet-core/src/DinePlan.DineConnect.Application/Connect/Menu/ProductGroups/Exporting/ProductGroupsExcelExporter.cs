﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Exporting
{
    public class ProductGroupsExcelExporter : NpoiExcelExporterBase, IProductGroupsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductGroupsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProductGroupForViewDto> productGroups)
        {
            return CreateExcelPackage(
                "ProductGroups.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProductGroups"));

                    AddHeader(
                        sheet,
                        L("Code"),
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, productGroups,
                        _ => _.ProductGroup.Code,
                        _ => _.ProductGroup.Name,
                        _ => _.ProductGroup.CreationTime.ToShortDateString()
                        );

                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
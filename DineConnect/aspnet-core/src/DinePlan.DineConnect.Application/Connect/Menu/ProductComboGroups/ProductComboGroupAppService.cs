﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups
{
    
    public class ProductComboGroupAppService : DineConnectAppServiceBase, IProductComboGroupAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRedisCacheService _redisCacheService;

        private readonly IRepository<ProductComboGroup> _productComboGroupRepository;
        private readonly IRepository<ProductComboItem> _productComboItemRepository;

        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;

        private readonly IProductComboGroupExcelExporter _printTemplateExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ProductComboGroupAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IRepository<ProductComboGroup> productComboGroupRepository,
            IRepository<ProductComboItem> productComboItemRepository,
            IProductComboGroupExcelExporter productComboGroupExcelExporter,
            IRepository<MenuItemPortion> menuItemPortionRepository,
            IUnitOfWorkManager unitOfWorkManager

            )
        {
            _connectSession = connectSession;
            _redisCacheService = redisCacheService;

            _productComboGroupRepository = productComboGroupRepository;

            _productComboItemRepository = productComboItemRepository;
            _printTemplateExcelExporter = productComboGroupExcelExporter;
            _menuItemPortionRepository = menuItemPortionRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<ProductComboGroupEditDto>> GetAll(GetAllProductComboGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = _productComboGroupRepository.GetAll()
                .Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.ComboGroupSelectionIds))
                {
                    var comboGroupSelectionIdArr = input.ComboGroupSelectionIds.Split(',');
                    var comboGroupSelectionIds = new List<int>();

                    foreach (var comboGroupSelectionIdString in comboGroupSelectionIdArr)
                    {
                        if (int.TryParse(comboGroupSelectionIdString, out var temp) && temp > 0)
                        {
                            comboGroupSelectionIds.Add(temp);
                        }
                    }

                    query = query.WhereIf(comboGroupSelectionIds.Any(), x => comboGroupSelectionIds.Contains(x.Id));
                }

                var sortedItems = await query
                     .OrderBy(e => e.SortOrder)
                     .OrderBy(input.Sorting)
                     .PageBy(input)
                     .Select(x => ObjectMapper.Map<ProductComboGroupEditDto>(x))
                     .ToListAsync();

                return new PagedResultDto<ProductComboGroupEditDto>(await query.CountAsync(), sortedItems);
            }
        }

        // --

        public async Task<FileDto> ExportToExcel()
        {
            var query = _productComboGroupRepository.GetAll();
            var items = await query.Select(x => ObjectMapper.Map<GetAllProductComboGroupDto>(x)).ToListAsync();

            return _printTemplateExcelExporter.ExportToFile(items);
        }

        // --

        public async Task<CreateEditProductComboGroupInput> GetForEdit(int id)
        {
            var entry = await _productComboGroupRepository
                .GetAllIncluding(x => x.ComboItems)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (entry == null)
                throw new UserFriendlyException(L("ComboGroupNotFound"));

            return ObjectMapper.Map<CreateEditProductComboGroupInput>(entry);
        }

        // --

        public async Task<List<CreateEditProductComboItemDto>> GetComboItemsByComboGroupId(int combogroupId)
        {
            var items = await _productComboItemRepository
                .GetAll()
                .Where(x => x.ProductComboGroupId == combogroupId)
                .ToListAsync();

            return ObjectMapper.Map<List<CreateEditProductComboItemDto>>(items);
        }

        
        public async Task<int> Create(CreateEditProductComboGroupInput input)
        {
            var exists = _productComboGroupRepository.GetAll()
                .Where(x => x.Name.Equals(input.Name))
                .WhereIf(input.Id.HasValue && input.Id > 0, x => x.Id != input.Id);
            if (exists.Any())
            {
                var message = string.Format(L("DuplicateExists_F"), L("Group"), L("Combo"));
                throw new UserFriendlyException(message);
            }

            if (input.ComboItems != null && input.ComboItems.Count > 0)
            {
                foreach (var item in input.ComboItems)
                {
                    if (item.MenuItemPortionId > 0)
                    {
                        var portion = await _menuItemPortionRepository.GetAsync(item.MenuItemPortionId);
                        if (portion != null)
                        {
                            item.MenuItemId = portion.MenuItemId;
                            item.MenuItemPortionId = portion.Id;
                        }
                    }
                }
            }

            var productComboGroup = ObjectMapper.Map<ProductComboGroup>(input);

            return await _productComboGroupRepository.InsertOrUpdateAndGetIdAsync(productComboGroup);
        }

        
        public async Task<int> Edit(CreateEditProductComboGroupInput input)
        {
            var comboGroup = await _productComboGroupRepository
                .GetAllIncluding(x => x.ComboItems)
                .Include(x => x.ProductComboGroupDetails)
                .FirstOrDefaultAsync(x => x.Id == input.Id.Value);

            comboGroup.Name = input.Name;
            comboGroup.SortOrder = input.SortOrder;
            comboGroup.Maximum = input.Maximum;
            comboGroup.Minimum = input.Minimum;

            if (input.ComboItems != null && input.ComboItems.Count > 0)
            {
                var updatedComboItems = input.ComboItems.Where(x => x.Id.HasValue && x.Id > 0);
                foreach (var comboItem in updatedComboItems)
                {
                    var productComboItem = _productComboItemRepository.Get(comboItem.Id.Value);
                    productComboItem.Name = comboItem.Name;
                    productComboItem.AutoSelect = comboItem.AutoSelect;
                    productComboItem.MenuItemId = comboItem.MenuItemId;
                    productComboItem.MenuItemPortionId = comboItem.MenuItemPortionId;
                    productComboItem.SortOrder = comboItem.SortOrder;
                    productComboItem.AddSeperately = comboItem.AddSeperately;
                    productComboItem.ButtonColor = comboItem.ButtonColor;
                    productComboItem.GroupTag = comboItem.GroupTag;
                    productComboItem.Price = comboItem.Price;
                    productComboItem.Count = comboItem.Count == 0 && comboItem.AutoSelect ? 1 : comboItem.Count;

                    await _productComboItemRepository.UpdateAsync(productComboItem);
                }
            }

            var newComboItems = input.ComboItems.Where(a => a.Id == null || a.Id == 0).ToList();
            foreach (var comboItem in newComboItems)
                comboGroup.ComboItems.Add(ObjectMapper.Map<ProductComboItem>(comboItem));

            await _productComboGroupRepository.UpdateAsync(comboGroup);

            var updatedComboItemIds = input.ComboItems.Where(x => x.Id.HasValue && x.Id > 0).Select(x => x.Id.Value).ToList();
            var deletedIds = comboGroup.ComboItems
                .Where(x => x.Id != 0 && !updatedComboItemIds.Contains(x.Id))
                .Select(x => x.Id)
                .ToList();

            foreach (var id in deletedIds)
                await _productComboItemRepository.DeleteAsync(id);

            return comboGroup.Id;
        }

        // --

        
        public async Task Delete(int id)
        {
            await _productComboGroupRepository.DeleteAsync(id);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _productComboGroupRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _productComboGroupRepository.UpdateAsync(item);
            }
        }

        public async Task SortComboGroups(List<ProductComboGroupEditDto> input)
        {
            foreach (var item in input)
            {
                var combo = await _productComboGroupRepository.FirstOrDefaultAsync(t => t.Id == item.Id);
                combo.SortOrder = item.SortOrder;
                await _productComboGroupRepository.UpdateAsync(combo);
            }
        }
    }
}
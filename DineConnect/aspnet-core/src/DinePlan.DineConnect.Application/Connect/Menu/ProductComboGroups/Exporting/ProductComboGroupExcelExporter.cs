﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Exporting
{
    public class ProductComboGroupExcelExporter : NpoiExcelExporterBase, IProductComboGroupExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductComboGroupExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllProductComboGroupDto> items)
        {
            return CreateExcelPackage(
                L("ComboGroups") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ComboGroup"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("SortOrder"),
                        L("Minimum"),
                        L("Maximum")
                        );

                    AddObjects(
                        sheet, 2, items,
                        _ => _.Name,
                        _ => _.SortOrder,
                        _ => _.Minimum,
                        _ => _.Maximum
                        );

                    for (var i = 1; i <= 2; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Exporting
{
    public interface IProductComboGroupExcelExporter
    {
        FileDto ExportToFile(List<GetAllProductComboGroupDto> items);
    }
}
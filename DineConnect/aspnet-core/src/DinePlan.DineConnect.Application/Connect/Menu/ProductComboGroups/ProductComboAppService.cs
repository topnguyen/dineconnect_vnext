using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups
{
    
    public class ProductComboAppService : DineConnectAppServiceBase, IProductComboAppService
    {
        private readonly IRepository<ProductComboGroupDetail> _productComboGroupDetailRepository;
        private readonly IRepository<ProductCombo> _productComboRepository;

        public ProductComboAppService(
            IRepository<ProductComboGroupDetail> productComboGroupDetailRepository,
            IRepository<ProductCombo> productComboRepository
        )
        {
            _productComboGroupDetailRepository = productComboGroupDetailRepository;
            _productComboRepository = productComboRepository;
        }

        public async Task<PagedResultDto<ProductComboDto>> GetAll(GetAllProductComboInput input)
        {
            var query = _productComboRepository.GetAll()
                .Include(x => x.ProductComboGroupDetails).ThenInclude(x => x.ProductComboGroup)
                //.Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

            if (!string.IsNullOrWhiteSpace(input.ProductComboSelectionIds))
            {
                var productComboSelectionIdArr = input.ProductComboSelectionIds.Split(',');
                var productComboSelectionIds = new List<int>();

                foreach (var productComnoSelectionIdString in productComboSelectionIdArr)
                {
                    if (int.TryParse(productComnoSelectionIdString, out var temp) && temp > 0)
                    {
                        productComboSelectionIds.Add(temp);
                    }
                }

                query = query.WhereIf(productComboSelectionIds.Any(), x => productComboSelectionIds.Contains(x.Id));
            }

            var sortedItems = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .Select(x => ObjectMapper.Map<ProductComboDto>(x))
                .ToListAsync();

            return new PagedResultDto<ProductComboDto>(await query.CountAsync(), sortedItems);
        }

        public async Task<ProductComboDto> GetForEdit(int id)
        {
            var entry = await _productComboRepository.GetAll()
                .Include(x => x.ProductComboGroupDetails).ThenInclude(x => x.ProductComboGroup)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (entry == null)
                throw new UserFriendlyException(L("ComboNotFound"));

            return ObjectMapper.Map<ProductComboDto>(entry);
        }

        public async Task<int> CreateOrEdit(CreateOrEditComboInputDto input)
        {
            if (input.Id.HasValue)
            {
                var combo = await _productComboRepository
                    .GetAllIncluding(x => x.ProductComboGroupDetails)
                    .FirstOrDefaultAsync(x => x.Id == input.Id);

                if (combo == null)
                    throw new UserFriendlyException(L("ComboNotFound"));

                var comboDetails = await _productComboGroupDetailRepository.GetAll()
                    .Where(x => x.ProductComboId == input.Id)
                    .ToListAsync();
                foreach (var comboGroupDetail in comboDetails)
                    await _productComboGroupDetailRepository.DeleteAsync(comboGroupDetail.Id);

                combo.Name = input.Name;
                combo.AddPriceToOrderPrice = input.AddPriceToOrderPrice;
                combo.ProductComboGroupDetails = input.ProductComboGroupIds.Select(x => new ProductComboGroupDetail
                {
                    ProductComboGroupId = x
                }).ToList();
                await _productComboRepository.UpdateAsync(combo);
                return combo.Id;
            }
            else
            {
                var combo = new ProductCombo
                {
                    Name = input.Name,
                    AddPriceToOrderPrice = input.AddPriceToOrderPrice,
                    ProductComboGroupDetails = input.ProductComboGroupIds.Select(x => new ProductComboGroupDetail
                    {
                        ProductComboGroupId = x
                    }).ToList()
                };
                var comboId = await _productComboRepository.InsertAndGetIdAsync(combo);
                return comboId;
            }
        }

        public async Task Delete(int id)
        {
            await _productComboRepository.DeleteAsync(id);
        }
    }
}
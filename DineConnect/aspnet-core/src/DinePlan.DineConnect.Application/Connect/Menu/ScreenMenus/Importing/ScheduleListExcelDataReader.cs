﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Importing
{
    public class ScreenMenuListExcelDataReader : NpoiExcelImporterBase<MenuItemImportDto>, IScreenMenuListExcelDataReader
    {
        public List<MenuItemImportDto> GetScreenMenusFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private MenuItemImportDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.SheetName.ToLower() == "sample")
            {
                return null;
            }

            try
            {
                if (IsRowEmpty(worksheet, row))
                {
                    return null;
                }

                var screenMenuEditDto = new MenuItemImportDto();
                screenMenuEditDto.Location = worksheet.GetRow(row).GetCell(0)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.CategoryName = worksheet.GetRow(row).GetCell(1)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.AliasCode = worksheet.GetRow(row).GetCell(2)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.Name = worksheet.GetRow(row).GetCell(3)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.AliasName = worksheet.GetRow(row).GetCell(4)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.ItemDescription = worksheet.GetRow(row).GetCell(5)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.BarCode = worksheet.GetRow(row).GetCell(6)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.TPName = worksheet.GetRow(row).GetCell(7)?.StringCellValue ?? string.Empty;
                screenMenuEditDto.TPPrice = Convert.ToDecimal(worksheet.GetRow(row)?.GetCell(8).StringCellValue);
                screenMenuEditDto.Tag = worksheet.GetRow(row).GetCell(9)?.StringCellValue ?? string.Empty;
                return screenMenuEditDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            return string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(0)?.StringCellValue)
                   || string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(1)?.StringCellValue);
        }
    }
}
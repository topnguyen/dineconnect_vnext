﻿using System.Collections.Generic;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Importing
{
    public interface IScreenMenuListExcelDataReader : IApplicationService
    {
        List<MenuItemImportDto> GetScreenMenusFromExcel(byte[] fileBytes);
    }
}
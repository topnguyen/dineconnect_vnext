﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Exporting
{
    public class ScreenMenuExcelExporter : NpoiExcelExporterBase, IScreenMenuExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        private class Template
        {
            public string Location { get; set; }

            public string Category { get; set; }

            public string AliasCode { get; set; }

            public string MenuName { get; set; }

            public string AliasName { get; set; }

            public string Description { get; set; }

            public string Barcode { get; set; }

            public string Portion { get; set; }

            public string Price { get; set; }

            public string Tag { get; set; }
        }

        public ScreenMenuExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportImportTemplate()
        {
            return CreateExcelPackage(
                $"{L("ScreenMenuTemplate")}.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Template"));

                    AddHeader(
                        sheet,
                        L("Location"),
                        L("Category"),
                        L("AliasCode"),
                        L("MenuName"),
                        L("AliasName"),
                        L("Description"),
                        L("Barcode"),
                        L("Portion"),
                        L("Price"),
                        L("Tag")
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    var sheet2 = excelPackage.CreateSheet("Sample");
                    

                    AddHeader(
                        sheet2,
                        L("Location"),
                        L("Category"),
                        L("AliasCode"),
                        L("MenuName"),
                        L("AliasName"),
                        L("Description"),
                        L("Barcode"),
                        L("Portion"),
                        L("Price"),
                        L("Tag")
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet2.AutoSizeColumn(i);
                    }

                    AddObjects(sheet2, 2, new List<Template>()
                    {
                        new Template()
                        {
                            Location = "KALIFAS",
                            Category = "MAINCOURSE",
                            AliasCode = "KAL",
                            MenuName = "BAKED BREAD",
                            AliasName = "水果杯",
                            Portion = "NORMAL",
                            Price = "19.00"
                        }
                    },
                    _ => _.Location,
                    _ => _.Category,
                    _ => _.AliasCode,
                    _ => _.MenuName,
                    _ => _.AliasName,
                    _ => _.Description,
                    _ => _.Barcode,
                    _ => _.Portion,
                    _ => _.Price,
                    _ => _.Tag);
                });

        }
    }
}
﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Exporting
{
    public interface IScreenMenuExcelExporter
    {
        FileDto ExportImportTemplate();
    }
}
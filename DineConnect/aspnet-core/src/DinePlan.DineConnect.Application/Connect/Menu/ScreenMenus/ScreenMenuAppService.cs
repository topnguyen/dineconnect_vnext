﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Exporting;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Importing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus
{
    
    public class ScreenMenuAppService : DineConnectAppServiceBase, IScreenMenuAppService
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly ConnectSession _connectSession;
        private readonly IDepartmentsAppService _departmentsAppService;
        private readonly IRepository<FutureDateInformation> _futureDateInformationRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Location> _locationRepository;

        private readonly IMenuItemAppService _menuItemAppService;
        private readonly IRepository<MenuItem> _menuItemRepository;

        private readonly IRepository<ScreenMenuCategory> _screenMenuCategoryRepository;
        private readonly IScreenMenuExcelExporter _screenMenuExcelExporter;
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IScreenMenuListExcelDataReader _screenMenuListExcelDataReader;

        private readonly IRepository<ScreenMenu> _screenMenuRepository;

        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ScreenMenuAppService(
            ConnectSession connectSession,
            IRedisCacheService redisCacheService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<ScreenMenu> screenMenuRepository,
            IScreenMenuExcelExporter screenMenuExcelExporter,
            IRepository<ScreenMenuCategory> screenMenuCategoryRepository,
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IRepository<Category> categoryRepository,
            IRepository<Location> locationRepository,
            IRepository<MenuItem> menuItemRepository,
            IRepository<FutureDateInformation> futureDateInformationRepository,
            IMenuItemAppService menuItemAppService,
            ITempFileCacheManager tempFileCacheManager,
            IScreenMenuListExcelDataReader screenMenuListExcelDataReader,
            IDepartmentsAppService departmentsAppService,
            ILocationAppService locationAppService
        )
        {
            _connectSession = connectSession;
            _unitOfWorkManager = unitOfWorkManager;

            _screenMenuRepository = screenMenuRepository;
            _screenMenuExcelExporter = screenMenuExcelExporter;

            _screenMenuCategoryRepository = screenMenuCategoryRepository;
            _screenMenuItemRepository = screenMenuItemRepository;

            _categoryRepository = categoryRepository;
            _locationRepository = locationRepository;
            _menuItemRepository = menuItemRepository;
            _futureDateInformationRepository = futureDateInformationRepository;

            _menuItemAppService = menuItemAppService;

            _tempFileCacheManager = tempFileCacheManager;
            _screenMenuListExcelDataReader = screenMenuListExcelDataReader;
            _departmentsAppService = departmentsAppService;
            _locationAppService = locationAppService;
        }

        // --

        public async Task<PagedResultDto<GetAllScreenMenuDto>> GetAll(GetAllScreenMenuInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var entryQuery = _screenMenuRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .Where(x => x.Type == ScreenMenuType.ConnectScreenMenu)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds)) entryQuery = FilterByLocations(entryQuery, input);

                if (!string.IsNullOrWhiteSpace(input.Sorting)) entryQuery = entryQuery.OrderBy(input.Sorting);

                var sortEntries = await entryQuery
                    .PageBy(input)
                    .ToListAsync();

                var dtos = new List<GetAllScreenMenuDto>();

                foreach (var screenMenu in sortEntries)
                {
                    var screenMenuDto = ObjectMapper.Map<GetAllScreenMenuDto>(screenMenu);

                    if (screenMenu.Dynamic != null)
                    {
                        dynamic screenMenuDtoDynamic = JObject.Parse(screenMenu.Dynamic);

                        screenMenuDto.CategoryColumnCount = screenMenuDtoDynamic.CategoryColumnCount;

                        screenMenuDto.CategoryColumnWidthRate = screenMenuDtoDynamic.CategoryColumnWidthRate;
                    }

                    dtos.Add(screenMenuDto);
                }

                foreach (var ltd in dtos)
                    if (!string.IsNullOrEmpty(ltd.Locations))
                    {
                        if (!ltd.Group)
                        {
                            var myL = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(ltd.Locations);

                            if (myL.Any() && myL.Count() == 1) ltd.RefLocationId = Convert.ToInt32(myL[0].Id);
                        }
                        else
                        {
                            ltd.RefLocationId = 0;
                        }
                    }

                return new PagedResultDto<GetAllScreenMenuDto>(await entryQuery.CountAsync(), dtos);
            }
        }

        // --

        public async Task<CreateOrUpdateScreenMenuInput> GetForEdit(int id)
        {
            var result = new CreateOrUpdateScreenMenuInput();

            var screenMenu = await _screenMenuRepository.GetAsync(id);

            result.ScreenMenu = ObjectMapper.Map<ScreenMenuEditDto>(screenMenu);

            if (screenMenu.Dynamic != null)
            {
                dynamic dynamic = JObject.Parse(screenMenu.Dynamic);

                if (dynamic.Departments != null)
                {
                    result.Departments =
                        JsonConvert.DeserializeObject<List<ComboboxItemDto>>(dynamic.Departments.ToString());

                    result.ScreenMenu.CategoryColumnCount = dynamic.CategoryColumnCount;

                    result.ScreenMenu.CategoryColumnWidthRate = dynamic.CategoryColumnWidthRate;
                }
            }

            GetLocationsForEdit(result.ScreenMenu, result.LocationGroup);

            return result;
        }

        public async Task CreateOrUpdateScreenMenu(CreateOrUpdateScreenMenuInput input)
        {
            if (input == null) return;

            var allDepartments = "";

            if (input.Departments.Any()) allDepartments = JsonConvert.SerializeObject(input.Departments);

            if (input.ScreenMenu.Id.HasValue)
                await UpdateScreenMenu(input, allDepartments);
            else
                await CreateScreenMenu(input, allDepartments);
        }


        public async Task Delete(int id)
        {
            await _screenMenuRepository.DeleteAsync(id);
        }


        public async Task Generate(int? id, bool locationInput)
        {
            var locationId = 0;
            var locationGroup = false;

            ScreenMenu menu = null;
            if (id.HasValue && id > 0)
            {
                if (!locationInput)
                {
                    menu = await _screenMenuRepository
                        .GetAllIncluding(x => x.Categories).Where(a => a.Type == ScreenMenuType.ConnectScreenMenu)
                        .FirstOrDefaultAsync(x => x.Id == id.Value);
                    if (menu.Group)
                        locationGroup = true;

                    if (!string.IsNullOrEmpty(menu.Locations))
                    {
                        var locationDtos = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(menu.Locations);
                        if (locationDtos.Any() && locationDtos.Count() == 1) locationId = locationDtos.First().Id;
                    }
                }
                else
                {
                    locationId = id.Value;
                }
            }

            var dynamiCategory = JsonConvert.SerializeObject(new
            {
                MostUsedItemsCategory = false,
                ImagePath = string.Empty,
                MainButtonHeight = 60,
                MainButtonColor = "#78ea86",
                MainFontSize = 15,
                ColumnCount = 0,
                MenuItemButtonHeight = 80,
                MaxItems = 0,
                PageCount = 1,
                WrapText = false,
                SortOrder = 0,
                SubButtonHeight = 60,
                SubButtonRows = 1,
                SubButtonColorDef = string.Empty,
                NumeratorType = 0,
                FromHour = 0,
                FromMinute = 0,
                ToHour = 0,
                ToMinute = 0,
                Files = string.Empty,
                DownloadImage = Guid.Empty
            });


            var dynamicMenuItem = JsonConvert.SerializeObject(new
            {
                ImagePath = string.Empty,
                ShowAliasName = false,
                AutoSelect = false,
                ButtonColor = "#feff72",
                FontSize = 20,
                SubMenuTag = "",
                OrderTags = "",
                ItemPortion = "",
                DownloadImage = Guid.Empty,
                RefreshImage = false,
                Files = string.Empty,
                ConfirmationType = 0,
                WeightedItem = false
            });

            if (locationId == 0 && !locationGroup)
            {
                if (menu != null && menu?.Categories.Count == 0)
                {
                    menu?.Categories.Clear();
                    var categories = _categoryRepository.GetAll().Include(a => a.MenuItems).ToList();
                    foreach (var category in categories)
                    {
                        var screenMenuCategory = new ScreenMenuCategory
                        {
                            Name = category.Name,
                            MenuItems = new List<ScreenMenuItem>(),
                            Dynamic = dynamiCategory
                        };

                        foreach (var menuItem in category.MenuItems)
                        {
                            var screenMenuItem = new ScreenMenuItem
                            {
                                Name = menuItem.Name,
                                MenuItemId = menuItem.Id,
                                Dynamic = dynamicMenuItem
                            };

                            screenMenuCategory.MenuItems.Add(screenMenuItem);
                        }

                        menu.Categories.Add(screenMenuCategory);
                    }
                }

                await _screenMenuRepository.UpdateAsync(menu);
            }
            else if (locationId > 0 && !locationGroup)
            {
                var allLo = await _locationRepository.FirstOrDefaultAsync(x => x.Id == locationId);
                if (await CheckScreenMenuExists(allLo))
                    return;

                var allList = await _menuItemAppService.GetAll(new GetMenuItemInput
                {
                    LocationIds = locationId.ToString(),
                    MaxResultCount = int.MaxValue
                });
                if (locationInput)
                {
                    var comL = new List<SimpleLocationDto>
                    {
                        new SimpleLocationDto
                        {
                            Name = allLo.Name,
                            Id = allLo.Id,
                            Code = allLo.Code
                        }
                    };

                    menu = new ScreenMenu
                    {
                        Locations = JsonConvert.SerializeObject(comL),
                        Name = allLo.Name,
                        Categories = new Collection<ScreenMenuCategory>()
                    };
                }

                if (menu == null)
                    return;

                foreach (var ci in allList.Items.GroupBy(a => a.CategoryId))
                {
                    var cat = await _categoryRepository.FirstOrDefaultAsync(a => a.Id == ci.Key);
                    if (cat != null)
                    {
                        var screenCategory = new ScreenMenuCategory
                        {
                            Name = cat.Name,
                            MenuItems = new List<ScreenMenuItem>(),
                            Dynamic = dynamiCategory
                        };

                        foreach (var menuItem in ci)
                        {
                            var screenMenuItem = new ScreenMenuItem
                            {
                                Name = menuItem.Name,
                                MenuItemId = menuItem.Id,
                                Dynamic = JsonConvert.SerializeObject(dynamicMenuItem)
                            };



                            screenCategory.MenuItems.Add(screenMenuItem);
                        }

                        menu.Categories.Add(screenCategory);
                    }
                }

                await _screenMenuRepository.InsertOrUpdateAndGetIdAsync(menu);
            }
            else if (locationGroup)
            {
                if (menu.Categories.Any()) await _screenMenuRepository.InsertOrUpdateAndGetIdAsync(menu);
            }
        }

        public async Task Clone(int id)
        {
            if (id > 0)
            {
                var srcScreenMenu = await _screenMenuRepository
                    .GetAll()
                    .Include(x => x.Categories)
                    .ThenInclude(x => x.MenuItems)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (srcScreenMenu == null) return;

                var screenMenu = new ScreenMenu
                {
                    TenantId = srcScreenMenu.TenantId,
                    Name = srcScreenMenu.Name,
                    Locations = srcScreenMenu.Locations,
                    Dynamic = srcScreenMenu.Dynamic,
                    Categories = new Collection<ScreenMenuCategory>(),
                    IsDeleted = false,
                    Group = srcScreenMenu.Group,
                    LocationTag = srcScreenMenu.LocationTag,
                    Type = srcScreenMenu.Type,
                    OrganizationId = srcScreenMenu.OrganizationId
                };

                if (srcScreenMenu.Categories?.Any() != true)
                {
                    await _screenMenuRepository.InsertAndGetIdAsync(screenMenu);

                    return;
                }

                foreach (var category in srcScreenMenu.Categories)
                {
                    var newCategory = new ScreenMenuCategory
                    {
                        Name = category.Name,
                        Dynamic = category.Dynamic
                    };

                    screenMenu.Categories.Add(newCategory);

                    if (category.MenuItems?.Any() != true) continue;

                    newCategory.MenuItems = new Collection<ScreenMenuItem>();

                    foreach (var menuItem in category.MenuItems)
                    {
                        var screenMenuItem = new ScreenMenuItem
                        {
                            Name = menuItem.Name,
                            MenuItemId = menuItem.MenuItemId,
                            Dynamic = menuItem.Dynamic
                        };

                        newCategory.MenuItems.Add(screenMenuItem);
                    }
                }

                await _screenMenuRepository.InsertAndGetIdAsync(screenMenu);
            }
        }

        // --

        public FileDto GetImportTemplate()
        {
            return _screenMenuExcelExporter.ExportImportTemplate();
        }

        // --

        public async Task<PagedResultDto<ScreenMenuCategoryListDto>> GetCategories(GetScreenMenuCategoriesInput input)
        {
            var screenMenuCategoriesQuery = _screenMenuCategoryRepository.GetAll()
                .WhereIf(input.ScreenMenuId > 0, p => p.ScreenMenuId == input.ScreenMenuId);

            if (!string.IsNullOrEmpty(input.Filter))
                screenMenuCategoriesQuery = screenMenuCategoriesQuery.WhereIf(!string.IsNullOrEmpty(input.Filter),
                    x => x.Name.Contains(input.Filter));

            if (!string.IsNullOrWhiteSpace(input.Sorting))
                screenMenuCategoriesQuery = screenMenuCategoriesQuery.OrderBy(input.Sorting);

            if (input.MaxResultCount >= 1) screenMenuCategoriesQuery = screenMenuCategoriesQuery.PageBy(input);

            List<int> screenmenuCategoryRefIds = new List<int>();

            if (input.DateFilterApplied)
            {
                foreach (var lst in screenMenuCategoriesQuery)
                {
                    List<ScreenMenuCategoryScheduleEditDto> screenMenuCategorySchedule = new List<ScreenMenuCategoryScheduleEditDto>();

                    if (!string.IsNullOrEmpty(lst.ScreenMenuCategorySchedule))
                    {
                        screenMenuCategorySchedule = JsonConvert.DeserializeObject<List<ScreenMenuCategoryScheduleEditDto>>(lst.ScreenMenuCategorySchedule);

                        var rsscreenmenucategorySchedules = screenMenuCategorySchedule.ToList();

                        if (rsscreenmenucategorySchedules != null && rsscreenmenucategorySchedules.Count > 0)
                        {
                            screenmenuCategoryRefIds.Add(lst.Id);
                        }
                    }

                }
                if (screenmenuCategoryRefIds != null && screenmenuCategoryRefIds.Count > 0)
                {
                    screenMenuCategoriesQuery = screenMenuCategoriesQuery.Where(t => screenmenuCategoryRefIds.Contains(t.Id));
                }
            }

            var screenMenuCategories = await screenMenuCategoriesQuery.ToListAsync();

            var dtos = new List<ScreenMenuCategoryListDto>();

            foreach (var screenMenuCategory in screenMenuCategories)
            {
                var screenMenuCategoryDto = ObjectMapper.Map<ScreenMenuCategoryListDto>(screenMenuCategory);

                if (screenMenuCategory.Dynamic != null)
                {
                    dynamic screenMenuCategoryDynamic = JsonConvert.DeserializeObject(screenMenuCategory.Dynamic);
                    if (screenMenuCategoryDynamic != null)
                    {
                        screenMenuCategoryDto.MostUsedItemsCategory =
                            screenMenuCategoryDynamic.MostUsedItemsCategory ?? false;
                        screenMenuCategoryDto.MenuItemButtonColor = screenMenuCategoryDynamic.MenuItemButtonColor;
                        screenMenuCategoryDto.MainButtonColor = screenMenuCategoryDynamic.MainButtonColor;
                        screenMenuCategoryDto.MainFontSize = screenMenuCategoryDynamic.MainFontSize ?? 15;
                        screenMenuCategoryDto.ColumnCount = screenMenuCategoryDynamic.ColumnCount ?? 0;
                        screenMenuCategoryDto.MainButtonHeight = screenMenuCategoryDynamic.MainButtonHeight ?? 60;
                        screenMenuCategoryDto.WrapText = screenMenuCategoryDynamic.WrapText ?? false;
                        screenMenuCategoryDto.MenuItemButtonHeight =
                            screenMenuCategoryDynamic.MenuItemButtonHeight ?? 60;
                        screenMenuCategoryDto.SubButtonHeight = screenMenuCategoryDynamic.SubButtonHeight ?? 60;
                        screenMenuCategoryDto.SubButtonColorDef = screenMenuCategoryDynamic.SubButtonColorDef;
                        screenMenuCategoryDto.SubButtonRows = screenMenuCategoryDynamic.SubButtonRows ?? 1;
                        screenMenuCategoryDto.SortOrder = screenMenuCategoryDynamic.SortOrder ?? 0;
                    }
                }

                if (screenMenuCategory.ScreenMenuCategorySchedule != null)
                {
                    screenMenuCategoryDto.ScreenMenuCategorySchedules = JsonConvert.DeserializeObject<List<ScreenMenuCategoryScheduleEditDto>>(screenMenuCategory.ScreenMenuCategorySchedule);
                }

                dtos.Add(screenMenuCategoryDto);
            }

            if (string.IsNullOrWhiteSpace(input.Sorting)) dtos = dtos.OrderBy(x => x.SortOrder).ToList();

            return new PagedResultDto<ScreenMenuCategoryListDto>(await screenMenuCategoriesQuery.CountAsync(), dtos);
        }

        public async Task CreateCategory(CreateCategoryInput input)
        {
            if (!string.IsNullOrEmpty(input.Name))
            {
                var category = await _categoryRepository
                    .GetAllIncluding(x => x.MenuItems)
                    .FirstOrDefaultAsync(x => x.Name.Equals(input.Name));
                var screenMenuItem = new List<ScreenMenuItem>();

                if (category != null && category.MenuItems.Any())
                    screenMenuItem.AddRange(category.MenuItems.Select(x => new ScreenMenuItem
                    {
                        MenuItemId = x.Id,
                        Name = x.Name,
                        Dynamic = JsonConvert.SerializeObject(new
                        {
                            ConfirmationType = 0,
                            WeightedItem = false,
                            ImagePath = string.Empty,
                            ShowAliasName = true,
                            AutoSelect = false,
                            ButtonColor = "#feff72",
                            FontSize = 20,
                            SubMenuTag = string.Empty,
                            OrderTags = string.Empty,
                            ItemPortion = string.Empty,
                            SortOrder = 0,
                            Files = string.Empty,
                            DownloadImage = Guid.Empty
                        })
                    }));

                var screenMenuCategory = new ScreenMenuCategory
                {
                    Name = input.Name,
                    ScreenMenuId = input.ScreenMenuId,
                    MenuItems = screenMenuItem,
                    Dynamic = JsonConvert.SerializeObject(new
                    {
                        MostUsedItemsCategory = false,
                        ImagePath = string.Empty,
                        MainButtonHeight = 60,
                        MainButtonColor = "#78ea86",
                        MainFontSize = 15,
                        ColumnCount = 0,
                        MenuItemButtonHeight = 80,
                        MaxItems = 0,
                        PageCount = 1,
                        WrapText = false,
                        SortOrder = 0,
                        SubButtonHeight = 60,
                        SubButtonRows = 1,
                        SubButtonColorDef = string.Empty,
                        NumeratorType = 0,
                        FromHour = 0,
                        FromMinute = 0,
                        ToHour = 0,
                        ToMinute = 0,
                        Files = string.Empty,
                        DownloadImage = Guid.Empty
                    })
                };

                await _screenMenuCategoryRepository.InsertAsync(screenMenuCategory);
            }
        }

        public async Task<ScreenMenuCategoryEditInput> GetCategoryForEdit(int id)
        {
            var screenMenuCategory = await _screenMenuCategoryRepository.GetAsync(id);

            var editInput = new ScreenMenuCategoryEditInput();

            if (!string.IsNullOrWhiteSpace(screenMenuCategory.Dynamic))
            {
                dynamic screenMenuCategoryDynamic = JObject.Parse(screenMenuCategory.Dynamic);

                editInput = ObjectMapper.Map<ScreenMenuCategoryEditInput>(screenMenuCategory);

                editInput.MostUsedItemsCategory = screenMenuCategoryDynamic.MostUsedItemsCategory ?? false;
                editInput.ImagePath = screenMenuCategoryDynamic.ImagePath;
                editInput.MainButtonHeight = screenMenuCategoryDynamic.MainButtonHeight ?? 0;
                editInput.MainButtonColor = screenMenuCategoryDynamic.MainButtonColor;
                editInput.MainFontSize = screenMenuCategoryDynamic.MainFontSize ?? 15;
                editInput.ColumnCount = screenMenuCategoryDynamic.ColumnCount ?? 0;
                editInput.MenuItemButtonHeight = screenMenuCategoryDynamic.MenuItemButtonHeight ?? 60;
                editInput.MaxItems = screenMenuCategoryDynamic.MaxItems ?? 0;
                editInput.PageCount = screenMenuCategoryDynamic.PageCount ?? 0;
                editInput.WrapText = screenMenuCategoryDynamic.WrapText ?? false;
                editInput.SubButtonHeight = screenMenuCategoryDynamic.SubButtonHeight ?? 60;
                editInput.SubButtonRows = screenMenuCategoryDynamic.SubButtonRows ?? 1;
                editInput.SubButtonColorDef = screenMenuCategoryDynamic.SubButtonColorDef;
                editInput.NumeratorType = screenMenuCategoryDynamic.NumeratorType ?? 0;
                editInput.FromHour = screenMenuCategoryDynamic.FromHour ?? 0;
                editInput.FromMinute = screenMenuCategoryDynamic.FromMinute ?? 0;
                editInput.ToHour = screenMenuCategoryDynamic.ToHour ?? 0;
                editInput.ToMinute = screenMenuCategoryDynamic.ToMinute ?? 0;
                editInput.Files = screenMenuCategoryDynamic.Files;
                editInput.DownloadImage = screenMenuCategoryDynamic.DownloadImage;

                return editInput;
            }

            editInput = ObjectMapper.Map<ScreenMenuCategoryEditInput>(screenMenuCategory);

            return editInput;
        }

        public async Task UpdateCategory(ScreenMenuCategoryEditInput input)
        {
            var screenMenuCategory = new ScreenMenuCategory(); ;
            if (input.Id != 0)
            {
                screenMenuCategory = _screenMenuCategoryRepository.Get(input.Id);
            }
            else
            {
                screenMenuCategory.ScreenMenuId = input.ScreenMenuId;
            }

            dynamic screenMenuCategoryDynamic = new ExpandoObject();
            screenMenuCategoryDynamic.Files = "";

            if (!string.IsNullOrWhiteSpace(screenMenuCategory.Dynamic))
                screenMenuCategoryDynamic = JObject.Parse(screenMenuCategory.Dynamic);

            if (screenMenuCategoryDynamic.Files != null && screenMenuCategoryDynamic.Files != input.Files)
                input.DownloadImage = Guid.NewGuid();

            if (input.RefreshImage) input.DownloadImage = Guid.NewGuid();

            ObjectMapper.Map(input, screenMenuCategory);

            screenMenuCategoryDynamic.MostUsedItemsCategory = input.MostUsedItemsCategory;
            screenMenuCategoryDynamic.ImagePath = input.ImagePath;
            screenMenuCategoryDynamic.MainButtonHeight = input.MainButtonHeight;
            screenMenuCategoryDynamic.MainButtonColor = input.MainButtonColor;
            screenMenuCategoryDynamic.MainFontSize = input.MainFontSize;
            screenMenuCategoryDynamic.ColumnCount = input.ColumnCount;
            screenMenuCategoryDynamic.MenuItemButtonHeight = input.MenuItemButtonHeight;
            screenMenuCategoryDynamic.MaxItems = input.MaxItems;
            screenMenuCategoryDynamic.PageCount = input.PageCount;
            screenMenuCategoryDynamic.WrapText = input.WrapText;
            screenMenuCategoryDynamic.SubButtonHeight = input.SubButtonHeight;
            screenMenuCategoryDynamic.SubButtonRows = input.SubButtonRows;
            screenMenuCategoryDynamic.SubButtonColorDef = input.SubButtonColorDef;
            screenMenuCategoryDynamic.NumeratorType = input.NumeratorType;
            screenMenuCategoryDynamic.FromHour = input.FromHour;
            screenMenuCategoryDynamic.FromMinute = input.FromMinute;
            screenMenuCategoryDynamic.ToHour = input.ToHour;
            screenMenuCategoryDynamic.ToMinute = input.ToMinute;
            screenMenuCategoryDynamic.Files = input.Files;
            screenMenuCategoryDynamic.DownloadImage = input.DownloadImage;

            screenMenuCategory.Dynamic = JsonConvert.SerializeObject(screenMenuCategoryDynamic);
            if (input.Id != 0)
            {
                await _screenMenuCategoryRepository.UpdateAsync(screenMenuCategory);
            }
            else
            {
                await _screenMenuCategoryRepository.InsertAsync(screenMenuCategory);
            }

            if (input.CopyToAll)
            {
                var screenMenuCategories =
                    await _screenMenuCategoryRepository.GetAllListAsync(x =>
                        x.ScreenMenuId == screenMenuCategory.ScreenMenuId);

                foreach (var item in screenMenuCategories)
                {
                    item.Dynamic = screenMenuCategory.Dynamic;

                    await _screenMenuCategoryRepository.UpdateAsync(item);
                }
            }
        }

        public async Task DeleteCategory(int id)
        {
            await _screenMenuCategoryRepository.DeleteAsync(id);
        }

        public async Task SaveSortCategories(int[] categories)
        {
            var i = 1;

            foreach (var category in categories)
            {
                var cate = await _screenMenuCategoryRepository.GetAsync(category);

                if (cate.Dynamic == null)
                {
                    dynamic dynamic = new
                    {
                        SortOrder = i++
                    };

                    cate.Dynamic = JsonConvert.SerializeObject(dynamic);
                }
                else
                {
                    dynamic dynamic = JObject.Parse(cate.Dynamic);

                    dynamic.SortOrder = i++;

                    cate.Dynamic = JsonConvert.SerializeObject(dynamic);
                }

                await _screenMenuCategoryRepository.UpdateAsync(cate);
            }
        }

        // --

        public async Task SaveSortSortItems(int[] menuItems)
        {
            var i = 1;
            foreach (var id in menuItems)
            {
                var menuItem = await _screenMenuItemRepository.GetAsync(id);

                dynamic dynamic = JObject.Parse(menuItem.Dynamic);

                dynamic.SortOrder = i++;

                menuItem.Dynamic = JsonConvert.SerializeObject(dynamic);

                await _screenMenuItemRepository.UpdateAsync(menuItem);
            }
        }

        // -------------------------------------------

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _screenMenuRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _screenMenuRepository.UpdateAsync(item);
            }
        }

        public async Task ClearScreenMenu(EntityDto input)
        {
            var menu = await _screenMenuRepository.GetAsync(input.Id);

            try
            {
                if (menu != null && menu.Categories.Count >= 0)
                {
                    foreach (var smc in menu.Categories)
                        await _screenMenuItemRepository.DeleteAsync(a => a.ScreenCategoryId == smc.Id);
                    await _screenMenuCategoryRepository.DeleteAsync(a => a.ScreenMenuId == input.Id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task CreateMenuItem(CreateScreenMenuItemInput input)
        {
            if (input.MenuItemId > 0)
            {
                var menuName = _menuItemRepository.Get(input.MenuItemId).Name;

                if (string.IsNullOrEmpty(input.Name)) input.Name = menuName;
                if (!string.IsNullOrEmpty(input.Name) && input.MenuItemId > 0)
                {
                    int.TryParse(input.MenuItem.ConfirmationType, out var confirmationType);

                    dynamic itemDynamic = new ExpandoObject();
                    itemDynamic.ConfirmationType = confirmationType;
                    itemDynamic.WeightedItem = input.MenuItem.WeightedItem;
                    itemDynamic.VoucherItem = input.MenuItem.VoucherItem;
                    itemDynamic.ImagePath = input.MenuItem.ImagePath;
                    itemDynamic.ShowAliasName = true;
                    itemDynamic.AutoSelect = input.MenuItem.AutoSelect;
                    itemDynamic.ButtonColor = input.MenuItem.ButtonColor;
                    itemDynamic.FontSize = input.MenuItem.FontSize;
                    itemDynamic.SubMenuTag = input.MenuItem.SubMenuTag;
                    itemDynamic.OrderTags = input.MenuItem.OrderTags;
                    itemDynamic.ItemPortion = input.MenuItem.ItemPortion;
                    itemDynamic.Files = input.MenuItem.Files;
                    itemDynamic.DownloadImage = input.MenuItem.DownloadImage;
                    itemDynamic.RefreshImage = false;

                    var item = new ScreenMenuItem
                    {
                        Name = input.Name,
                        ScreenCategoryId = input.CategoryId,
                        MenuItemId = input.MenuItemId
                    };

                    if (input.MenuItem != null)
                    {
                        if (!string.IsNullOrEmpty(input.MenuItem.SubMenuTag))
                            itemDynamic.SubMenuTag = input.MenuItem.SubMenuTag;
                        if (!string.IsNullOrEmpty(input.MenuItem.OrderTags))
                            itemDynamic.OrderTags = input.MenuItem.OrderTags;
                        if (!string.IsNullOrEmpty(input.MenuItem.ButtonColor))
                            itemDynamic.ButtonColor = input.MenuItem.ButtonColor;
                        if (input.MenuItem.FontSize > 0) itemDynamic.FontSize = input.MenuItem.FontSize;
                    }

                    item.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                    input.Files = input.Files;

                    input.DownloadImage = input.DownloadImage;

                    await _screenMenuItemRepository.InsertAsync(item);
                }
            }
            else if (input.Items.Any())
            {
                var allIds = input.Items.Select(a => a.Id).ToList();

                var removalList = _screenMenuItemRepository.GetAll().Where(a => a.ScreenCategoryId == input.CategoryId
                    && !allIds.Contains(a.MenuItemId.Value)).ToList();

                foreach (var myItem in removalList) _screenMenuItemRepository.Delete(myItem.Id);

                foreach (var myItem in input.Items)
                    if (_screenMenuItemRepository.FirstOrDefault(a =>
                        a.MenuItemId == myItem.Id && a.ScreenCategoryId == input.CategoryId) == null)
                    {
                        var myName = myItem.Name;
                        if (myName.Contains("@"))
                        {
                            var splitStr = myName.Split("@");
                            if (splitStr.Any()) myName = splitStr[0];
                        }

                        int.TryParse(input.MenuItem.ConfirmationType, out var confirmationType);

                        dynamic itemDynamic = new ExpandoObject();
                        itemDynamic.ConfirmationType = confirmationType;
                        itemDynamic.WeightedItem = input.MenuItem.WeightedItem;
                        itemDynamic.VoucherItem = input.MenuItem.VoucherItem;

                        itemDynamic.ImagePath = input.MenuItem.ImagePath;
                        itemDynamic.ShowAliasName = true;
                        itemDynamic.AutoSelect = input.MenuItem.AutoSelect;
                        itemDynamic.ButtonColor = input.MenuItem.ButtonColor;
                        itemDynamic.FontSize = input.MenuItem.FontSize;
                        itemDynamic.SubMenuTag = input.MenuItem.SubMenuTag;
                        itemDynamic.OrderTags = input.MenuItem.OrderTags;
                        itemDynamic.ItemPortion = input.MenuItem.ItemPortion;
                        itemDynamic.Files = input.MenuItem.Files;
                        itemDynamic.DownloadImage = input.MenuItem.DownloadImage;

                        var item = new ScreenMenuItem
                        {
                            Name = myName,
                            ScreenCategoryId = input.CategoryId,
                            MenuItemId = myItem.Id
                        };

                        if (input.MenuItem != null)
                        {
                            if (!string.IsNullOrEmpty(input.MenuItem.SubMenuTag))
                                itemDynamic.SubMenuTag = input.MenuItem.SubMenuTag;
                            if (!string.IsNullOrEmpty(input.MenuItem.OrderTags))
                                itemDynamic.OrderTags = input.MenuItem.OrderTags;
                            if (!string.IsNullOrEmpty(input.MenuItem.ButtonColor))
                                itemDynamic.ButtonColor = input.MenuItem.ButtonColor;
                            if (input.MenuItem.FontSize > 0) itemDynamic.FontSize = input.MenuItem.FontSize;

                            item.Dynamic = JsonConvert.SerializeObject(itemDynamic);
                        }

                        input.Files = input.Files;

                        input.DownloadImage = input.DownloadImage;

                        await _screenMenuItemRepository.InsertAsync(item);
                    }
            }
        }

        public async Task DeleteScreenMenuItem(EntityDto input)
        {
            await _screenMenuItemRepository.DeleteAsync(input.Id);
        }

        public ListResultDto<ComboboxItemDto> GetConfirmationTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var value in Enum.GetValues(typeof(ScreenMenuItemConfirmationTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = value.ToString(),
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<GetScreenMenuItemForEditOutput> GetMenuItemForEdit(EntityDto input)
        {
            var hDto = await _screenMenuItemRepository.GetAsync(input.Id);

            var menuIt = hDto.MenuItemId;

            var editDto = ObjectMapper.Map<ScreenMenuItemEditDto>(hDto);

            dynamic menuItemDynamic = JObject.Parse(hDto.Dynamic);
            editDto.ImagePath = menuItemDynamic.ImagePath;
            editDto.ShowAliasName = menuItemDynamic.ShowAliasName;
            editDto.AutoSelect = menuItemDynamic.AutoSelect;
            editDto.ButtonColor = menuItemDynamic.ButtonColor;
            editDto.FontSize = menuItemDynamic.FontSize;
            editDto.SubMenuTag = menuItemDynamic.SubMenuTag;
            editDto.OrderTags = menuItemDynamic.OrderTags;
            editDto.ItemPortion = menuItemDynamic.ItemPortion;
            editDto.DownloadImage = menuItemDynamic.DownloadImage;
            editDto.RefreshImage = menuItemDynamic.RefreshImage ?? false;
            editDto.Files = menuItemDynamic.Files;
            editDto.ConfirmationType = menuItemDynamic.ConfirmationType;
            editDto.WeightedItem = menuItemDynamic.WeightedItem;
            editDto.VoucherItem = menuItemDynamic.VoucherItem != null && (bool)menuItemDynamic.VoucherItem;


            if (menuIt != null)
                return new GetScreenMenuItemForEditOutput
                {
                    MenuItem = editDto,
                    MenuItemId = menuIt.Value
                };
            return null;
        }

        public async Task<GetScreenMenuItemBySubMenuOuput> GetMenuItemForVisualScreen(GetScreenMenuInput input)
        {
            var allItems = await _screenMenuItemRepository.GetAll()
                .WhereIf(input.CategoryId > 0, x => x.ScreenCategoryId == input.CategoryId)
                .ToListAsync();

            var allItemModels = new List<ScreenMenuItemEditDto>();

            foreach (var item in allItems)
            {
                var itemDto = ObjectMapper.Map<ScreenMenuItemEditDto>(item);

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                itemDto.ImagePath = itemDynamic.ImagePath;
                itemDto.ShowAliasName = itemDynamic.ShowAliasName;
                itemDto.AutoSelect = itemDynamic.AutoSelect;
                itemDto.ButtonColor = itemDynamic.ButtonColor;
                itemDto.FontSize = itemDynamic.FontSize;
                itemDto.SubMenuTag = itemDynamic.SubMenuTag;
                itemDto.OrderTags = itemDynamic.OrderTags;
                itemDto.DownloadImage = itemDynamic.DownloadImage;
                itemDto.RefreshImage = itemDynamic.RefreshImage ?? false;
                itemDto.Files = itemDynamic.Files;
                itemDto.ConfirmationType = itemDynamic.ConfirmationType;
                itemDto.WeightedItem = itemDynamic.WeightedItem;
                itemDto.VoucherItem = itemDynamic.VoucherItem != null && (bool)itemDynamic.VoucherItem;


                allItemModels.Add(itemDto);
            }

            //Get MenuItems
            var allTags = input.CurrentTag?.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();

            var tag = input.CurrentTag;

            //if (!string.IsNullOrEmpty(tag) && tag.Contains(','))
            //    tag = tag.Split(',').Last().Trim();

            var menuItems = allItemModels.Where(x =>
            {
                if ((string.IsNullOrEmpty(tag) && string.IsNullOrEmpty(x.SubMenuTag)) || x.SubMenuTag == tag)
                {
                    return true;
                }

                return false;
            });

            //Get Tags
            var parentTag = input.CurrentTag;

            var cateTags = allItemModels
                .Where(x => !string.IsNullOrEmpty(x.SubMenuTag))
                .Select(x => x.SubMenuTag)
                .Distinct()
                .Where(x => string.IsNullOrEmpty(parentTag) || (x.StartsWith(parentTag) && x != parentTag))
                .Select(x => Regex.Replace(x, "^" + parentTag + ",", ""))
                .Select(x =>
                {
                    if (x.Contains(','))
                        return x.Split(',').First();
                    return x;
                })
                .Select(x => !string.IsNullOrEmpty(parentTag) ? parentTag + "," + x : x).Distinct();

            return new GetScreenMenuItemBySubMenuOuput
            {
                CategoryTags = cateTags,
                MenuItems = ObjectMapper.Map<List<ScreenMenuItemListDto>>(menuItems)
            };
        }

        public async Task<PagedResultDto<ScreenMenuItemListDto>> GetMenuItems(GetAllScreenMenuInput input)
        {
            var allItems = _screenMenuItemRepository
                .GetAll()
                .WhereIf(
                    input.CategoryId > 0,
                    p => p.ScreenCategoryId == input.CategoryId)
                .WhereIf(!string.IsNullOrEmpty(input.Filter), p => p.Name.Contains(input.Filter));

            if (!string.IsNullOrWhiteSpace(input.Sorting)) allItems = allItems.OrderBy(input.Sorting);

            var menuItems = await allItems
                .PageBy(input)
                .ToListAsync();

            var allItemCount = await allItems.CountAsync();

            var screenListDto = new List<ScreenMenuItemListDto>();

            foreach (var menuItem in menuItems)
            {
                var menuItemDto = ObjectMapper.Map<ScreenMenuItemListDto>(menuItem);

                if (menuItem.Dynamic != null)
                {
                    dynamic screenMenuItemDynamic = JsonConvert.DeserializeObject(menuItem.Dynamic);
                    if (screenMenuItemDynamic != null)
                    {
                        menuItemDto.ButtonColor = screenMenuItemDynamic.ButtonColor;
                        menuItemDto.AutoSelect = screenMenuItemDynamic.AutoSelect ?? false;
                        menuItemDto.CategoryName = screenMenuItemDynamic.CategoryName;
                        menuItemDto.SubMenuTag = screenMenuItemDynamic.SubMenuTag;
                        menuItemDto.SortOrder = screenMenuItemDynamic.SortOrder ?? 0;
                    }
                }

                screenListDto.Add(menuItemDto);
            }

            if (string.IsNullOrWhiteSpace(input.Sorting)) screenListDto = screenListDto.OrderBy(x => x.SortOrder).ToList();

            return new PagedResultDto<ScreenMenuItemListDto>(allItemCount, screenListDto);
        }

        public async Task<GetScreenItemsOutput> GetScreenItemsForLocation(GetScreenMenuInput input)
        {
            var output = new GetScreenItemsOutput();

            var allItems = await _menuItemAppService.GetAllItems(new GetMenuItemLocationInput
            {
                LocationId = input.LocationId
            });

            var allCatItems =
                _screenMenuItemRepository.GetAll()
                    .Where(a => a.ScreenCategoryId == input.CategoryId)
                    .Select(a => a.MenuItemId)
                    .ToList();

            var allI = allItems.Items.Where(a => a?.Id != null && !allCatItems.Contains(a.Id.Value));
            var allList = allI.ToList();
            foreach (var allM in allList)
                if (allM.Id != null)
                    output.Items.Add(new IdNameDto(allM.Id.Value, allM.Name + "@" + allM.CategoryName));

            output.SelectedItems =
                _screenMenuItemRepository
                    .GetAll()
                    .Where(a => a.ScreenCategoryId == input.CategoryId)
                    .Select(a => new IdNameDto
                    {
                        Id = a.MenuItemId.Value,
                        Name = a.Name + "@" + a.ScreenMenuCategory.Name
                    }).ToList();

            return output;
        }

        public async Task CreateOrUpdateMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            if (input.MenuItem.Id.HasValue)
                await UpdateScreenMenuItem(input);
            else
                await CreateScreenMenuItem(input);
        }

        public async Task<ApiScreenMenuOutput> ApiScreenMenu(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);

            var output = new ApiScreenMenuOutput();

            var allMenu = _screenMenuRepository.GetAll()
                .Include(x => x.Categories).ThenInclude(a => a.MenuItems)
                .Where(a => a.TenantId == locationInput.TenantId && a.Type == ScreenMenuType.ConnectScreenMenu);

            var myMenu = new List<ScreenMenu>();
            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var screenMenu in allMenu)
                        if (!screenMenu.Group && !string.IsNullOrEmpty(screenMenu.Locations) && !screenMenu.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(screenMenu.Locations);
                            if (scLocations.Any())
                            {
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        myMenu.Add(screenMenu);
                            }
                            else
                            {
                                myMenu.Add(screenMenu);
                            }
                        }
            }
            else if (locationInput.LocationId != 0)
            {
                foreach (var screenMenu in allMenu)
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = screenMenu.Locations,
                        Group = screenMenu.Group,
                        NonLocations = screenMenu.NonLocations
                    }))
                        myMenu.Add(screenMenu);
            }
            else
            {
                myMenu = allMenu.ToList();
            }

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                output.Menu.Clear();

                var oScMenu = new ApiScreenMenu();

                foreach (var screenMenu in myMenu)
                {
                    var menu = ObjectMapper.Map<ApiScreenMenu>(screenMenu);
                    foreach (var apiScreenCategory in menu.Categories)
                        if (oScMenu.Categories.Any())
                        {
                            var lasCat = oScMenu.Categories.LastOrDefault(a =>
                                a.Name.Equals(apiScreenCategory.Name));
                            if (lasCat != null)
                                foreach (var apiScreenMenuItem in apiScreenCategory.MenuItems)
                                    lasCat.MenuItems.Add(apiScreenMenuItem);
                            else
                                oScMenu.Categories.Add(apiScreenCategory);
                        }
                        else
                        {
                            oScMenu.Categories.Add(apiScreenCategory);
                        }

                    oScMenu.Id = screenMenu.Id;
                }

                output.Menu.Add(oScMenu);
                oScMenu.Name = "Menu";
            }
            else
            {
                foreach (var screenMenu in myMenu)
                {
                    var menu = ObjectMapper.Map<ApiScreenMenu>(screenMenu);
                    if (!string.IsNullOrEmpty(screenMenu.Departments))
                    {
                        var allDep = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(menu.Departments);
                        menu.Departments = string.Join(",", allDep.Select(a => a.DisplayText));
                    }

                    output.Menu.Add(menu);
                }
            }

            return output;
        }

        protected virtual async Task UpdateScreenMenu(CreateOrUpdateScreenMenuInput input, string allDepartments)
        {
            var screenMenuDto = input.ScreenMenu;

            if (input.ScreenMenu.Id != null)
            {
                var screenMenu = await _screenMenuRepository.GetAsync(input.ScreenMenu.Id.Value);

                var dynamic = new
                {
                    Departments = allDepartments,
                    screenMenuDto.CategoryColumnCount,
                    screenMenuDto.CategoryColumnWidthRate
                };

                screenMenu.Dynamic = JsonConvert.SerializeObject(dynamic);

                screenMenu.Name = screenMenuDto.Name;

                UpdateLocations(screenMenu, input.LocationGroup);

                await _screenMenuRepository.UpdateAsync(screenMenu);
            }
        }

        protected virtual async Task CreateScreenMenu(CreateOrUpdateScreenMenuInput input, string allDepartments)
        {
            var screenMenu = ObjectMapper.Map<ScreenMenu>(input.ScreenMenu);

            var dynamic = new
            {
                Departments = allDepartments,
                input.ScreenMenu.CategoryColumnCount,
                input.ScreenMenu.CategoryColumnWidthRate
            };
            screenMenu.Type = input.ScreenMenu.Type == 2 ? ScreenMenuType.CliqueMenu : ScreenMenuType.ConnectScreenMenu;

            screenMenu.Dynamic = JsonConvert.SerializeObject(dynamic);

            screenMenu.TenantId = AbpSession.TenantId ?? 0;

            screenMenu.OrganizationId = _connectSession.OrgId;

            UpdateLocations(screenMenu, input.LocationGroup);

            await _screenMenuRepository.InsertAndGetIdAsync(screenMenu);
        }

        private async Task<bool> CheckScreenMenuExists(Location location)
        {
            var screenMenus = await _screenMenuRepository.GetAllIncluding(x => x.Categories).Where(a => a.Type == ScreenMenuType.ConnectScreenMenu).ToListAsync();
            foreach (var screenMenu in screenMenus)
                if (!string.IsNullOrEmpty(screenMenu.Locations))
                {
                    var locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(screenMenu.Locations);
                    if (locations.Find(x => x.Id == location.Id) != null)
                    {
                        var totalCount = screenMenu.Categories.Count();
                        if (totalCount != 0) return true;
                    }
                }

            return false;
        }

        public async Task<ScreenMenuEditDto> GetScreenMenuByNameAndLocation(string name, int locationId)
        {
            var allSm = await _screenMenuRepository.GetAllListAsync(a => a.Name.Equals(name));
            var sm = allSm.FirstOrDefault(a => !string.IsNullOrEmpty(a.Locations) || JsonConvert.DeserializeObject<List<SimpleLocationDto>>(a.Locations).Any(x => x.Id == locationId));
            if (sm == null)
                return null;

            return ObjectMapper.Map<ScreenMenuEditDto>(sm);
        }

        public async Task Import(string fileToken)
        {
            if (string.IsNullOrWhiteSpace(fileToken)) return;

            var fileBytes = _tempFileCacheManager.GetFile(fileToken);

            using (var stream = new MemoryStream(fileBytes))
            {
                using (var excelPackage = new ExcelPackage(stream))
                {
                    var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                    var headers = new List<string>
                    {
                        "Location Name",
                        "Category",
                        "Alias Code",
                        "MenuName",
                        "AliasName",
                        "Description",
                        "Barcode",
                        "Portion",
                        "Price",
                        "Tag"
                    };

                    if (worksheet != null)
                    {
                        var columns = worksheet.Dimension.End.Column;

                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (var i = 1; i < columns + 1; i++) sheetHeaders.Add(worksheet.GetValue(1, i).ToString());

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }
            }

            // Get Excel File

            var dtos = _screenMenuListExcelDataReader.GetScreenMenusFromExcel(fileBytes);

            var departmentCombobox = await _departmentsAppService.GetDepartmentsForCombobox();

            var locationSimples = await _locationAppService.GetSimpleLocations(new GetLocationInput
            {
                MaxResultCount = 100
            });
            var locations = new Dictionary<string, SimpleLocationDto>();
            foreach (var loc in dtos.GroupBy(a => a.Location))
            {
                var lIds = _locationAppService.GetLocationsForUser(new GetLocationInputBasedOnUser()
                {
                    UserId = AbpSession.UserId.GetValueOrDefault(),
                    LocationName = loc.Key
                });
                if (lIds == null || !lIds.Any())
                {
                    throw new UserFriendlyException(L("LocationErr", loc.Key) + " Row: " + loc.FirstOrDefault()?.RowIndex);
                }
                locations.Add(loc.Key, lIds.First());
            }

            foreach (var group in dtos.GroupBy(a => a.CategoryName))
            {
                var category = _categoryRepository.GetAll().FirstOrDefault(x => x.Name == group.Key);
                if (category == null) continue;

                foreach (var menuItemGroup in group.GroupBy(x => x.Name))
                {
                    CreateOrUpdateMenuItemInput menuItemInput = null;
                    var menuItem = menuItemGroup.First();
                    var myLocation = locations[menuItem.Location];
                    var allLocations = new List<SimpleLocationDto>
                        {
                            new SimpleLocationDto
                            {
                                Id = myLocation.Id,
                                Name = myLocation.Name,
                                Code = myLocation.Code
                            }
                        };
                    var existMenuItem = _menuItemRepository.GetAll().Include(x => x.Portions).FirstOrDefault(a => a.Name.ToUpper() == menuItem.Name.ToUpper());
                    var meD = new MenuItemEditDto
                    {
                        AliasCode = menuItem.AliasCode,
                        AliasName = menuItem.AliasName,
                        BarCode = menuItem.BarCode,
                        CategoryId = category.Id,
                        ForceChangePrice = menuItem.ForceChangePrice,
                        ForceQuantity = menuItem.ForceQuantity,
                        RestrictPromotion = menuItem.RestrictPromotion,
                        NoTax = menuItem.NoTax,
                        ItemDescription = menuItem.ItemDescription,
                        Name = menuItem.Name,
                        Locations = JsonConvert.SerializeObject(allLocations),
                        Tag = menuItem.Tag
                    };
                    if (existMenuItem != null)
                    {
                        meD.Id = existMenuItem.Id;

                    }

                    menuItemInput = new CreateOrUpdateMenuItemInput
                    {
                        MenuItem = meD,
                    };

                    foreach (var item in menuItemGroup)
                    {
                        var portion = new MenuItemPortionEditDto
                        {
                            Name = menuItem.TPName,
                            MultiPlier = 1,
                        };                        
                        var exPortion = existMenuItem?.Portions.FirstOrDefault(x => x.Name == menuItem.TPName);
                        portion.Id = exPortion?.Id;
                        portion.Price = menuItem.TPPrice;
                        menuItem.Portions.Add(portion);
                    }

                    if (menuItemInput != null)
                    {
                        await _menuItemAppService.CreateOrUpdateMenuItem(menuItemInput);
                    }
                }
            }
            var allMenuitem = _menuItemRepository.GetAll();
            CreateOrUpdateScreenMenuInput screenMenuInput = null;
            for (int i = 0; i < locations.Values.Count; i++)
            {
                var location = locations.Values.ElementAt(i);

                var existSM = await GetScreenMenuByNameAndLocation(location.Name, location.Id);
                if (existSM != null)
                {
                    throw new UserFriendlyException(string.Format(L("ScreenMenuErr"), location.Name) + "\n Row: " + i + 1);
                }


                var allLocations = new List<SimpleLocationDto>
                    {
                        new SimpleLocationDto
                        {
                            Id = location.Id,
                            Name = location.Name,
                            Code = location.Code
                        }
                    };
                var sm = new ScreenMenuEditDto
                {
                    Name = location.Name
                };
                screenMenuInput = new CreateOrUpdateScreenMenuInput
                {
                    ScreenMenu = sm,
                    LocationGroup = { Locations = allLocations }
                };

                var dynamic = new
                {
                    Departments = string.Empty,
                    CategoryColumnCount = 2,
                    CategoryColumnWidthRate = 45,
                };

                var menu = new ScreenMenu
                {
                    Name = location.Name,
                    Departments = null,
                    Locations = JsonHelper.Serialize<List<SimpleLocationDto>>(allLocations),
                    CategoryColumnCount = 2,
                    CategoryColumnWidthRate = 45,
                    OrganizationId = _connectSession.OrgId,
                    Dynamic = JsonConvert.SerializeObject(dynamic),
                    Type = ScreenMenuType.ConnectScreenMenu
                };

                menu.Categories = new Collection<ScreenMenuCategory>();
                var dtoGroup = from d in dtos
                               where d.Location == location.Name
                               group d by new { d.CategoryName }
                               into g                              
                               select new { CategoryName = g.Key.CategoryName, Items = g };

                var dynamiCategory = JsonConvert.SerializeObject(new
                {
                    MostUsedItemsCategory = false,
                    ImagePath = string.Empty,
                    MainButtonHeight = 60,
                    MainButtonColor = "#78ea86",
                    MainFontSize = 15,
                    ColumnCount = 0,
                    MenuItemButtonHeight = 80,
                    MaxItems = 0,
                    PageCount = 1,
                    WrapText = false,
                    SortOrder = 0,
                    SubButtonHeight = 60,
                    SubButtonRows = 1,
                    SubButtonColorDef = string.Empty,
                    NumeratorType = 0,
                    FromHour = 0,
                    FromMinute = 0,
                    ToHour = 0,
                    ToMinute = 0,
                    Files = string.Empty,
                    DownloadImage = Guid.Empty
                });

                var dynamicMenuItem = JsonConvert.SerializeObject(new
                {
                    ImagePath = string.Empty,
                    ShowAliasName = false,
                    AutoSelect = false,
                    ButtonColor = "#feff72",
                    FontSize = 20,
                    SubMenuTag = "",
                    OrderTags = "",
                    ItemPortion = "",
                    DownloadImage = Guid.Empty,
                    RefreshImage = false,
                    Files = string.Empty,
                    ConfirmationType = 0,
                    WeightedItem = false
                });

                foreach (var smc in dtoGroup)
                {
                    var smcNew = new ScreenMenuCategory
                    {
                        Name = smc.CategoryName,
                        Dynamic = dynamiCategory
                    };
                    menu.Categories.Add(smcNew);
                    smcNew.MenuItems = new Collection<ScreenMenuItem>();

                    foreach (var smi in smc.Items)
                    {
                        var findMenuItem = allMenuitem.FirstOrDefault(s => s.Name == smi.Name);
                        if (findMenuItem != null 
                            && !smcNew.MenuItems.Any(x => x.MenuItemId == findMenuItem.Id))
                        {
                            var mi = new ScreenMenuItem
                            {
                                Name = smi.Name,
                                MenuItemId = findMenuItem.Id,
                                Dynamic = dynamicMenuItem
                            };
                            smcNew.MenuItems.Add(mi);
                        }
                    }
                }
                if (menu.Categories.Any())
                {
                    await _screenMenuRepository.InsertOrUpdateAndGetIdAsync(menu);
                }

                //foreach (var screenMenuEditDto in screenMenus)
                //{
                //    var createOrUpdateScreenMenuInput = new CreateOrUpdateScreenMenuInput
                //    {
                //        ScreenMenu = screenMenuEditDto
                //    };

                //    // Location

                //    var allLocationName = screenMenuEditDto.Locations?.Split(',').Select(x => x.Trim()).ToList();

                //    if (allLocationName != null)
                //    {
                //        foreach (var locationName in allLocationName)
                //            if (locationSimples.Items.All(x => x.Name != locationName))
                //                throw new UserFriendlyException(string.Format(L("LocationInCorrect"), locationName));

                //        var screenMenuLocations =
                //            locationSimples.Items.Where(x => allLocationName.Contains(x.Name)).ToList();

                //        createOrUpdateScreenMenuInput.LocationGroup = new CommonLocationGroupDto
                //        {
                //            Group = false,
                //            Locations = screenMenuLocations
                //        };
                //    }

                //    // Department

                //    var allDepartmentName = screenMenuEditDto.Departments?.Split(',').Select(x => x.Trim()).ToList();

                //    if (allDepartmentName != null)
                //    {
                //        createOrUpdateScreenMenuInput.Departments = departmentCombobox.Items
                //            .Where(x => allDepartmentName.Contains(x.DisplayText)).ToList();

                //        foreach (var departmentComboboxItemDto in createOrUpdateScreenMenuInput.Departments)
                //            departmentComboboxItemDto.IsSelected = true;
                //    }

                //    await CreateOrUpdateScreenMenu(createOrUpdateScreenMenuInput);
                //}
            }
        }

        public async Task<GetScreenMenuForEditOutput> GetScreenMenuForEdit(FilterInputDto input)
        {
            var output = new GetScreenMenuForEditOutput();
            ScreenMenuEditDto editDto;
            var allDepartments = new List<ComboboxItemDto>();
            if (input.Id.HasValue || input.FutureDataId.HasValue)
            {
                if (input.Id.HasValue && input.FutureDataId == null)
                {
                    var hDto = await _screenMenuRepository.GetAsync(input.Id.Value);
                    editDto = ObjectMapper.Map<ScreenMenuEditDto>(hDto);

                    if (!string.IsNullOrWhiteSpace(hDto.Dynamic))
                    {
                        dynamic screenMenuDtoDynamic = JObject.Parse(hDto.Dynamic);

                        editDto.CategoryColumnCount = screenMenuDtoDynamic.CategoryColumnCount;

                        editDto.CategoryColumnWidthRate = screenMenuDtoDynamic.CategoryColumnWidthRate;

                        editDto.Departments = screenMenuDtoDynamic.Departments.ToString();
                    }

                    if (!string.IsNullOrEmpty(editDto.Departments))
                        allDepartments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
                }
                else
                {
                    var futureData =
                        await _futureDateInformationRepository.FirstOrDefaultAsync(t => t.Id == input.FutureDataId);
                    if (string.IsNullOrEmpty(futureData.Contents))
                    {
                        editDto = new ScreenMenuEditDto();
                        allDepartments = new List<ComboboxItemDto>();
                    }
                    else
                    {
                        var jsonOutput = JsonConvert.DeserializeObject<GetScreenMenuForEditOutput>(futureData.Contents);
                        editDto = ObjectMapper.Map<ScreenMenuEditDto>(jsonOutput.ScreenMenu);
                        if (jsonOutput.Departments != null && jsonOutput.Departments.Count > 0)
                            allDepartments = ObjectMapper.Map<List<ComboboxItemDto>>(jsonOutput.Departments);
                        else
                            allDepartments = new List<ComboboxItemDto>();
                        return new GetScreenMenuForEditOutput
                        {
                            ScreenMenu = editDto,
                            Departments = allDepartments
                        };
                    }
                }
            }
            else
            {
                editDto = new ScreenMenuEditDto();
            }

            return new GetScreenMenuForEditOutput
            {
                ScreenMenu = editDto,
                Departments = allDepartments
            };
        }

        private async Task CreateScreenMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            await CreateMenuItem(new CreateScreenMenuItemInput
            {
                Name = input.MenuItem.Name,
                MenuItemId = input.MenuItemId,
                CategoryId = input.CategoryId,
                Files = input.MenuItem.Files,
                DownloadImage = Guid.NewGuid(),
                MenuItem = input.MenuItem
            });
        }

        private async Task UpdateScreenMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            var from = input.MenuItem;

            if (from.Id.HasValue && from.Id > 0)
            {
                var myMenuItem = await _screenMenuItemRepository.GetAsync(from.Id.Value);

                dynamic itemDynamic = JObject.Parse(myMenuItem.Dynamic);

                if (itemDynamic.Files != null && itemDynamic.Files.ToString() != from.Files)
                    from.DownloadImage = Guid.NewGuid();

                if (input.MenuItem.RefreshImage) from.DownloadImage = Guid.NewGuid();

                ObjectMapper.Map(from, myMenuItem);

                itemDynamic.ImagePath = from.ImagePath;
                itemDynamic.ShowAliasName = from.ShowAliasName;
                itemDynamic.AutoSelect = from.AutoSelect;
                itemDynamic.ButtonColor = from.ButtonColor;
                itemDynamic.FontSize = from.FontSize;
                itemDynamic.SubMenuTag = from.SubMenuTag;
                itemDynamic.OrderTags = from.OrderTags;
                itemDynamic.ItemPortion = from.ItemPortion;
                itemDynamic.DownloadImage = from.DownloadImage;
                itemDynamic.RefreshImage = from.RefreshImage;
                itemDynamic.Files = from.Files;
                itemDynamic.ConfirmationType = from.ConfirmationType;
                itemDynamic.WeightedItem = from.WeightedItem;
                itemDynamic.VoucherItem = (bool)from.VoucherItem;


                myMenuItem.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                if (input.MenuItemId > 0) myMenuItem.MenuItemId = input.MenuItemId;

                await _screenMenuItemRepository.UpdateAsync(myMenuItem);
            }

            if (input.CopyToAll)
                foreach (var smc in await _screenMenuItemRepository.GetAllListAsync(a =>
                    a.ScreenCategoryId == input.CategoryId))
                {
                    smc.ScreenCategoryId = input.CategoryId;

                    dynamic smcDynamic = JObject.Parse(smc.Dynamic);

                    smcDynamic.ImagePath = from.ImagePath;
                    smcDynamic.ShowAliasName = from.ShowAliasName;
                    smcDynamic.AutoSelect = from.AutoSelect;
                    smcDynamic.ButtonColor = from.ButtonColor;
                    smcDynamic.FontSize = from.FontSize;
                    smcDynamic.SubMenuTag = from.SubMenuTag;
                    smcDynamic.OrderTags = from.OrderTags;
                    smcDynamic.ItemPortion = from.ItemPortion;
                    smcDynamic.ConfirmationType = from.ConfirmationType;
                    smcDynamic.WeightedItem = from.WeightedItem;
                    smcDynamic.VoucherItem = from.VoucherItem;
                    smc.Dynamic = JsonConvert.SerializeObject(smcDynamic);

                    //if (input.MenuItemId > 0) smc.MenuItemId = input.MenuItemId;

                    await _screenMenuItemRepository.UpdateAsync(smc);
                }
        }

        public async Task<PagedResultDto<GetAllScreenMenuDto>> GetAllScreenMenuCategory(GetScreenMenuCategoryInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var entryQuery = _screenMenuRepository.GetAll()
                    .Include(x => x.Categories)
                    .Where(x => x.Type == ScreenMenuType.ConnectScreenMenu)
                    .SelectMany(x => x.Categories)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()))
                    ;

                var sortEntries = await entryQuery
                    .PageBy(input)
                    .ToListAsync();

                var output = sortEntries.Select(x => new GetAllScreenMenuDto
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToList();
                return new PagedResultDto<GetAllScreenMenuDto>(await entryQuery.CountAsync(), output);
            }
        }

        public async Task<PagedResultDto<GetAllScreenMenuDto>> GetAllScreenMenuForImport(GetScreenMenuCategoryInput input)
        {

            var entryQuery = _screenMenuRepository.GetAll()
                    .Where(x => x.Type == ScreenMenuType.ConnectScreenMenu)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

            var sortEntries = await entryQuery
                .PageBy(input)
                .ToListAsync();

            var output = sortEntries.Select(x => new GetAllScreenMenuDto
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();
            return new PagedResultDto<GetAllScreenMenuDto>(await entryQuery.CountAsync(), output);
        }
    }
}
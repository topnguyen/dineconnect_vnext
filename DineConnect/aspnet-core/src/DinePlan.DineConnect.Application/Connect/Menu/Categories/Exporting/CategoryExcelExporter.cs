﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.Menu.Categories.Exporting
{
    public class CategoryExcelExporter : NpoiExcelExporterBase, ICategoryExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public CategoryExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public async Task<FileDto> ExportToFile(List<CategoryDto> categories)
        {
            var allSettings = await _tenantSettingsAppService.GetAllSettings();
            var fileDateTimeSetting = !string.IsNullOrWhiteSpace(allSettings.FileDateTimeFormat.FileDateTimeFormat)
                ? allSettings.FileDateTimeFormat.FileDateTimeFormat
                : AppConsts.FileDateTimeFormat;
            
            return CreateExcelPackage(
                "Category.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Category"));

                    AddHeader(
                        sheet,
                        L("Code"),
                        L("Name"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, categories,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())?.ToString(fileDateTimeSetting)

                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
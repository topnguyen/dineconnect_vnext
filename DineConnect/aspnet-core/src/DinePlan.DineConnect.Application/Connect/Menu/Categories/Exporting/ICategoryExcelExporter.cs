﻿using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Categories.Exporting
{
    public interface ICategoryExcelExporter
    {
        Task<FileDto> ExportToFile(List<CategoryDto> categories);
    }
}
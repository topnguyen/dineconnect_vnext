﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Menu.Categories.Exporting;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
    public class CategoryAppService : DineConnectAppServiceBase, ICategoryAppService
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly ICategoryManager _categoryManager;
        private readonly ConnectSession _connectSession;
        private readonly ICategoryExcelExporter _excelExporter;
        private readonly IRepository<ProductGroup> _productGroupRepository;
        private readonly IProductGroupsAppService _productGroupAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CategoryAppService(ICategoryManager categoryManager
            , ConnectSession connectSession
            , IRepository<Category> categoryRepository
            , ICategoryExcelExporter excelExporter
            , IRepository<ProductGroup> productGroupRepository
            , IProductGroupsAppService productGroupAppService
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _categoryManager = categoryManager;
            _connectSession = connectSession;
            _categoryRepository = categoryRepository;
            _excelExporter = excelExporter;
            _productGroupRepository = productGroupRepository;
            _productGroupAppService = productGroupAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<int> CreateOrUpdateAsync(CategoryDto input)
        {
            ValidateForCreateOrUpdate(input);
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            var exists = _categoryRepository.GetAll().WhereIf(input.Id > 0, m => m.Id != input.Id).Where(o => o.Name.Equals(input.Name));

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            if (input.Id.HasValue && input.Id > 0)
            {
                var category = await _categoryRepository.FirstOrDefaultAsync(input.Id.Value);
                ObjectMapper.Map(input, category);
                category.ProductGroup = null;
                return input.Id.Value;
            }

            var newCategory = ObjectMapper.Map<Category>(input);
            newCategory.TenantId = _connectSession.TenantId ?? 0;
            newCategory.OrganizationId = _connectSession.OrgId;
            //category.MenuItems = null;

            await _categoryRepository.InsertAsync(newCategory);
            await CurrentUnitOfWork.SaveChangesAsync();
            return newCategory.Id;

            //return await _categoryManager.CreateOrUpdateAsync(category);
        }

        public async Task DeleteAsync(int input)
        {
            await _categoryManager.DeleteAsync(input);
        }

        public async Task<PagedResultDto<CategoryDto>> GetAll(GetCategoryInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var category = _categoryManager.GetAll(input.Filter, input.IsDeleted);
                var sortedCategories = await category
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var categoryDtos = ObjectMapper.Map<List<CategoryDto>>(sortedCategories);
                return new PagedResultDto<CategoryDto>(
                    await category.CountAsync(),
                    categoryDtos
                     );
            }
        }

        public async Task<FileDto> GetCategoryToExcel(GetCategoryInput input)
        {
            var filteredCategory = await _categoryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
                        EF.Functions.Like(e.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(e.Code, input.Filter.ToILikeString()))
                        .ToListAsync();

            var categories = ObjectMapper.Map<List<CategoryDto>>(filteredCategory);

            return await _excelExporter.ExportToFile(categories);
        }

        public async Task<CategoryDto> GetCategoryForEdit(NullableIdDto input)
        {
            if (!input.Id.HasValue)
                return new CategoryDto();

            var category = await _categoryManager.GetAsync(input.Id.Value);
            return ObjectMapper.Map<CategoryDto>(category);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetCategoriesForCombobox()
        {
            var dtos = await _categoryRepository.GetAll()
                .Select(a => new ComboboxItemDto(a.Id.ToString(), a.Name)).ToListAsync();

            return new ListResultDto<ComboboxItemDto>(dtos);
        }

        protected virtual void ValidateForCreateOrUpdate(CategoryDto input)
        {
            var exists = _categoryRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any(o => o.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException($"Category Code {input.Code} aleady exists");
            }
            if (exists.Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException($"Category Name {input.Name} aleady exists");
            }
        }

        public async Task<CategoryDto> GetOrCreateCategory(CategoryDto input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            var allItems = _categoryRepository
               .GetAll()
               .WhereIf(!input.Name.IsNullOrEmpty(), p => p.Name.Equals(input.Name));

            if (allItems.Any())
            {
                var cateDto = ObjectMapper.Map<CategoryDto>(allItems.First());
                cateDto.Code = input.Code;
                cateDto.GroupName = input.GroupName;
                cateDto.GroupCode = input.GroupCode;

                await GetOrCreateGroup(cateDto);
                await CreateOrUpdateAsync(cateDto);
                input = cateDto;
            }
            else
            {
                await GetOrCreateGroup(input);
                var output = await CreateOrUpdateAsync(input);
                input.Id = output;
            }

            return input;
        }

        private async Task GetOrCreateGroup(CategoryDto input)
        {
            CreateOrEditProductGroupDto groupeditDto;

            var groups = _productGroupRepository.GetAll().Where(t => t.Name.Equals(input.GroupName));

            if (groups.Any())
            {
                groupeditDto = ObjectMapper.Map<CreateOrEditProductGroupDto>(groups.First());
            }
            else
            {
                groupeditDto = await _productGroupAppService.GetOrCreateByName(input.GroupName, input.GroupCode);
            }

            input.ProductGroupId = groupeditDto.Id;
        }

        public async Task<PagedResultDto<CategoryDto>> GetCategoryByPgroductGroupId(GetCategoryByProductGroupIdInput input)
        {
            var category = _categoryManager.GetAll(input.Filter)
                .Where(c => c.ProductGroupId == input.ProductGroupId);
                

            var sortedCategories = await category
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var categoryDtos = ObjectMapper.Map<List<CategoryDto>>(sortedCategories);
            return new PagedResultDto<CategoryDto>(
                await category.CountAsync(),
                categoryDtos
                 );
        }

        public async Task UpdateCategoryProductgroupId(UpdateProductgroupCategoryInput input)
        {
            if (input.CategoryIds != null)
            {
                var needRemoveCates = await _categoryRepository.GetAll().Where(c => !input.CategoryIds.Contains(c.Id) && c.ProductGroupId == input.ProductGroupId).ToListAsync();

                needRemoveCates.ForEach(c =>
                {
                    c.ProductGroupId = null;
                });
                foreach (var catId in input.CategoryIds)
                {
                    var category = await _categoryRepository.GetAsync(catId);
                    if (category != null)
                    {
                        category.ProductGroupId = input.ProductGroupId;
                        await _categoryRepository.UpdateAsync(category);
                    }
                }
            }
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _categoryRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _categoryRepository.UpdateAsync(item);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Exporting;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Importing;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using GetAllForLookupTableInput = DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos.GetAllForLookupTableInput;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public class MenuItemAppService : DineConnectAppServiceBase, IMenuItemAppService
    {
        private readonly ICategoryAppService _categoryAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<LocationMenuItemPrice> _locationItemPriceRepository;
        private readonly IRepository<LocationMenuItem> _locationItemRepository;
        private readonly ILocationMenuItemCustomRepository _locationMenuItemCustomRepository;
        private readonly LocationMenuItemExcelDataReader _locationMenuItemExcelDataReader;
        private readonly ILocationMenuItemExcelExporter _locationMenuItemExcelExporter;
        private readonly ILocationMenuItemManager _locationMenuItemManager;
        private readonly IRepository<LocationMenuItemPrice> _locationMenuItemPriceRepository;
        private readonly ILocationMenuItemPricesExcelDataReader _locationMenuItemPricesExcelDataReader;
        private readonly IRepository<MenuBarCode> _menuBarCodeRepository;
        private readonly IMenuItemExcelExporter _menuitemExporter;
        private readonly IMenuItemListExcelDataReader _menuItemListExcelDataReader;
        private readonly IMenuItemManager _menuItemManager;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<ProductComboGroupDetail> _productComboGroupDetailRepository;
        private readonly IRepository<ProductComboGroup> _productComboGroupRepository;
        private readonly IRepository<ProductComboItem> _productComboItemRepository;
        private readonly IRepository<ProductCombo> _productComboRepository;
        private readonly IRedisCacheService _redisCacheService;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<TransactionType> _transactionTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UpMenuItemLocationPrice> _upMenuItemLocationPriceRepository;
        private readonly IRepository<UpMenuItem> _upMenuItemRepository;

        public MenuItemAppService(IMenuItemManager menuItemManager,
            IRepository<MenuItem> menuItemRepository,
            ConnectSession connectSession,
            ILocationMenuItemCustomRepository locationMenuItemCustomRepository,
            ILocationMenuItemManager locationMenuItemManager,
            IRepository<ProductCombo> productComboRepository,
            IRepository<ProductComboGroup> productComboGroupRepository,
            IRepository<ProductComboItem> productComboItemRepository,
            IRepository<TransactionType> transactionTypeRepository,
            IRepository<MenuBarCode> menuBarCodeRepository,
            IRepository<LocationMenuItem> locationItemRepository,
            IRepository<LocationMenuItemPrice> locationItemPriceRepository,
            IRepository<UpMenuItem> upMenuItemRepository,
            IRepository<MenuItemPortion> menuItemPortionRepository,
            IMenuItemExcelExporter menuitemExporter,
            IMenuItemListExcelDataReader menuItemListExcelDataReader,
            ITempFileCacheManager tempFileCacheManager,
            ICategoryAppService categoryAppService,
            ILocationAppService locationAppService,
            IRedisCacheService redisCacheService,
            IApplicationLanguageManager applicationLanguageManager,
            IRepository<LocationMenuItemPrice> locationMenuItemPriceRepository,
            ILocationMenuItemPricesExcelDataReader locationMenuItemPricesExcelDataReader,
            ILocationMenuItemExcelExporter locationMenuItemExcelExporter,
            LocationMenuItemExcelDataReader locationMenuItemExcelDataReader,
            IRepository<ProductComboGroupDetail> productComboGroupDetailRepository,
            IRepository<UpMenuItemLocationPrice> upMenuItemLocationPriceRepository,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _menuItemManager = menuItemManager;
            _menuItemRepository = menuItemRepository;
            ConnectSession = connectSession;
            _locationMenuItemCustomRepository = locationMenuItemCustomRepository;
            _locationMenuItemManager = locationMenuItemManager;
            _productComboRepository = productComboRepository;
            _productComboGroupRepository = productComboGroupRepository;
            _productComboItemRepository = productComboItemRepository;
            _transactionTypeRepository = transactionTypeRepository;
            _menuBarCodeRepository = menuBarCodeRepository;
            _locationItemRepository = locationItemRepository;
            _locationItemPriceRepository = locationItemPriceRepository;
            _upMenuItemRepository = upMenuItemRepository;
            _menuItemPortionRepository = menuItemPortionRepository;
            _menuitemExporter = menuitemExporter;
            _menuItemListExcelDataReader = menuItemListExcelDataReader;
            _tempFileCacheManager = tempFileCacheManager;
            _categoryAppService = categoryAppService;
            _locationAppService = locationAppService;
            _redisCacheService = redisCacheService;

            _locationMenuItemPriceRepository = locationMenuItemPriceRepository;
            _locationMenuItemPricesExcelDataReader = locationMenuItemPricesExcelDataReader;

            _locationMenuItemExcelExporter = locationMenuItemExcelExporter;
            _locationMenuItemExcelDataReader = locationMenuItemExcelDataReader;

            _productComboGroupDetailRepository = productComboGroupDetailRepository;
            _upMenuItemLocationPriceRepository = upMenuItemLocationPriceRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public ConnectSession ConnectSession { get; set; }

        [HttpPost]
        public async Task<PagedResultDto<MenuItemListDto>> GetAll(GetMenuItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var menuItems = _menuItemRepository
                    .GetAllIncluding(e => e.Category, c => c.LocationMenuItems, x => x.Portions)
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(input.CategoryId.HasValue && input.CategoryId.Value>0, x => x.CategoryId == input.CategoryId)
                    .WhereIf(input.MenuItemType.HasValue, x => x.ProductType == input.MenuItemType)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        c => EF.Functions.Like(c.Name, input.Filter.ToILikeString())
                             || EF.Functions.Like(c.AliasName, input.Filter.ToILikeString())
                             || EF.Functions.Like(c.AliasCode, input.Filter.ToILikeString())
                             || EF.Functions.Like(c.Category.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds)) menuItems = FilterByLocations(menuItems, input);

                var sortedMenuItems = await menuItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var menuItemListDtos = ObjectMapper.Map<List<MenuItemListDto>>(sortedMenuItems);
                return new PagedResultDto<MenuItemListDto>(
                    await menuItems.CountAsync(),
                    menuItemListDtos
                );
            }
        }

        public async Task<PagedResultDto<MenuItemEditDto>> GetAllItems(GetMenuItemLocationInput input,
            bool restrictLocation = false)
        {
            List<MenuItemEditDto> outPut;
            var count = 0;

            if (input.LocationId > 0 &&
                await _locationItemRepository.CountAsync(a => a.LocationId.Equals(input.LocationId)) > 0)
            {
                var returnList =
                    await _locationItemRepository.GetAllListAsync(a => a.LocationId.Equals(input.LocationId));
                if (!string.IsNullOrEmpty(input.Filter))
                {
                    var menuItems = returnList.Select(a => a.MenuItem);
                    var allItems = menuItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.AliasName != null && p.AliasName.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.AliasCode != null && p.AliasCode.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.Category != null && p.Category.Name.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0)
                        );
                    outPut = ObjectMapper.Map<List<MenuItemEditDto>>(allItems);
                }
                else
                {
                    outPut = ObjectMapper.Map<List<MenuItemEditDto>>(returnList.Select(a => a.MenuItem));
                }
            }
            else
            {
                var allItems = new List<MenuItem>();
                if (!restrictLocation)
                    allItems = await _menuItemRepository
                        .GetAll()
                        .Include(x => x.Category)
                        .Include(x => x.Portions)
                        .Include(x => x.UpMenuItems)
                        .Include(x => x.Barcodes)
                        .Where(a => a.Locations != null)
                        .ToListAsync();

                if (input.LocationId > 0)
                {
                    var allMyLocations =
                        await _menuItemRepository.GetAllListAsync(a => a.Locations != null && !a.Locations.Equals(""));

                    foreach (var allLoc in allMyLocations)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                            {
                                LocationId = input.LocationId,
                                Locations = allLoc.Locations,
                                Group = allLoc.Group,
                                NonLocations = allLoc.NonLocations,
                                LocationTag = allLoc.LocationTag
                            }))
                            allItems.Add(allLoc);
                }

                var myAllItems = allItems.AsEnumerable();
                if (!string.IsNullOrEmpty(input.Filter))
                    myAllItems = myAllItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.AliasName != null && p.AliasName.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.AliasCode != null && p.AliasCode.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0 ||
                                               p.Category != null && p.Category.Name.IndexOf(input.Filter,
                                                   StringComparison.OrdinalIgnoreCase) >= 0)
                        );

                outPut = ObjectMapper.Map<List<MenuItemEditDto>>(myAllItems.ToList());

                count = allItems.Count();
            }

            var allPortions = outPut.SelectMany(a => a.Portions);
            var poritonIds = allPortions.Select(a => a.Id);

            if (await _locationMenuItemPriceRepository.CountAsync(a =>
                    a.LocationId.Equals(input.LocationId) && a.Price > 0M) > 0)
                foreach (
                    var lportion in
                    _locationMenuItemPriceRepository.GetAll()
                        .Where(
                            a =>
                                poritonIds.Contains(a.MenuPortionId) && a.LocationId.Equals(input.LocationId) &&
                                a.Price > 0M))
                    if (lportion.Price > 0M)
                    {
                        var singPors = allPortions.Where(a => a.Id == lportion.MenuPortionId);
                        if (singPors.Any())
                            foreach (var mied in singPors)
                                mied.Price = lportion.Price;
                    }

            return new PagedResultDto<MenuItemEditDto>(count, outPut);
        }

        public async Task<GetMenuItemForEditOutput> GetMenuItemForEdit(NullableIdDto input)
        {
            var output = new GetMenuItemForEditOutput();

            var editDto = new MenuItemEditDto();
            var productType = 1;
            var combs = new List<ProductComboGroupEditDto>();

            if (input.Id.HasValue)
            {
                var menuItem = await _menuItemRepository.GetAll()
                    .Include(e => e.Barcodes)
                    .Include(e => e.Portions)
                    .Include(e => e.UpMenuItems)
                    .FirstOrDefaultAsync(e => e.Id == input.Id.Value);

                editDto = ObjectMapper.Map<MenuItemEditDto>(menuItem);
                productType = menuItem.ProductType;

                if (editDto.UpMenuItems.Any())
                {
                    editDto.UpSingleMenuItems = editDto.UpMenuItems.Where(a => a.ProductType == 1).ToList();
                    editDto.UpMenuItems = editDto.UpMenuItems.Where(a => a.ProductType == 2).ToList();

                    foreach (var single in editDto.UpSingleMenuItems)
                        if (single.RefMenuItemId > 0)
                        {
                            var mItem = await _menuItemRepository.GetAsync(single.RefMenuItemId);
                            single.Name = mItem?.Name;
                        }
                }

                if (productType == 2)
                {
                    var productCombo = await _productComboRepository.GetAllIncluding(c => c.ProductComboGroupDetails)
                        .FirstOrDefaultAsync(a => a.MenuItemId == menuItem.Id);
                    if (productCombo != null)
                    {
                        combs = ObjectMapper.Map<List<ProductComboGroupEditDto>>(
                            await _productComboGroupDetailRepository.GetAll()
                                .Where(p => p.ProductComboId == productCombo.Id)
                                .Select(p => p.ProductComboGroup)
                                .OrderBy(e => e.SortOrder)
                                .ToListAsync());

                        if (!string.IsNullOrEmpty(productCombo.Sorting))
                        {
                            var sortedIds = productCombo.Sorting.Split(',').ToList();
                            if (sortedIds.Any())
                            {
                                foreach (var id in sortedIds)
                                {
                                    var idInt = Convert.ToInt32(id);
                                    var g = combs.FirstOrDefault(x => x.Id == idInt);
                                    if (g != null) g.SortOrder = sortedIds.IndexOf(id);
                                }

                                combs = combs.OrderBy(x => x.SortOrder).ToList();
                            }
                        }

                        combs.Select(c =>
                        {
                            c.ComboItems = ObjectMapper.Map<Collection<ProductComboItemEditDto>>(
                                _productComboItemRepository.GetAll().Where(i => i.ProductComboGroupId == c.Id)
                                    .ToList());
                            return c;
                        }).ToList();
                    }
                }
            }

            output.MenuItem = editDto;
            output.ProductType = productType;
            output.Combos = combs;

            GetLocationsForEdit(output.MenuItem, output.MenuItem.LocationGroup);
            return output;
        }

        public async Task<int> CreateOrUpdateAsync(MenuItemEditDto input)
        {
            if (ConnectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new MenuItem");

            var locationMenuItemsInDb = await _locationMenuItemManager.GetAllIncludingLocaions((int) input.Id);
            var menuItem = ObjectMapper.Map<MenuItem>(input);
            menuItem.OrganizationId = ConnectSession.OrgId;
            menuItem.TenantId = AbpSession.TenantId ?? 0;

            UpdateLocations(menuItem, input.LocationGroup);

            input.Id = await _menuItemManager.CreateOrUpdateAsync(menuItem);

            await _locationMenuItemCustomRepository.DeleteLocationMenuItems(locationMenuItemsInDb);

            var locationMenuItemList = new List<LocationMenuItem>();
            await _locationMenuItemCustomRepository.AddLocationMenuItems(locationMenuItemList);

            await _redisCacheService.RemoveAsync(AppConsts.GetMenuItemAndPriceForSelectionKey);
            return menuItem.Id;
        }

        public async Task CreateOrUpdateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            if (_menuItemRepository.GetAll().WhereIf(input.MenuItem.Id > 0, m => m.Id != input.MenuItem.Id)
                .Any(o => o.Name.ToUpper().Equals(input.MenuItem.Name.ToUpper())))
                throw new UserFriendlyException(
                    $"Menu Item with name {input.MenuItem.Name} is already added in another category, please check input file (Row: {input.MenuItem.RowIndex})");

            if (_menuItemRepository.GetAll().WhereIf(input.MenuItem.Id > 0, m => m.Id != input.MenuItem.Id)
                .Any(o => !string.IsNullOrWhiteSpace(input.MenuItem.AliasName) &&
                          o.AliasName.ToUpper().Equals(input.MenuItem.AliasName.ToUpper())))
                throw new UserFriendlyException(
                    $"Menu Item with alias name {input.MenuItem.AliasName} is already added in another category, please check input file (Row: {input.MenuItem.RowIndex})");

            if (ConnectSession.OrgId <= 0)
                throw new UserFriendlyException(
                    "Please set your default location/Organization before creating new MenuItem");

            input.MenuItem.OrganizationId = ConnectSession.OrgId;

            //input.MenuItem.ProductType = input.ProductType;

            if (input.ProductType == 2 && input.MenuItem.Portions.Any())
                input.MenuItem.Portions = new Collection<MenuItemPortionEditDto> {input.MenuItem.Portions[0]};

            if (input.ProductType == 1 && input.MenuItem.UpMenuItems.Any())
                input.MenuItem.UpMenuItems = input.MenuItem.UpMenuItems.Where(a => a.RefMenuItemId > 0).ToList();

            if (input.ProductType == 1 && input.MenuItem.UpSingleMenuItems.Any())
                input.MenuItem.UpSingleMenuItems =
                    input.MenuItem.UpSingleMenuItems.Where(a => a.RefMenuItemId > 0).ToList();

            if (input.MenuItem.Id.HasValue)
            {
                await UpdateMenuItem(input);
            }
            else
            {
                var menuName = input.MenuItem.Name.ToUpper();
                var exists = _menuItemRepository.GetAll().Where(a => a.Name.ToUpper().Equals(menuName));
                if (exists.Any())
                {
                    var firstItem = exists.First();
                    input.MenuItem.Id = firstItem.Id;

                    if (!input.NotOverride)
                        await UpdateMenuItem(input);
                    return;
                }

                input.MenuItem.Id = await CreateMenuItem(input);
            }

            //if (input.MenuItemLocationPrices.Any())
            //    await UpdateLocationPriceForMenu(input.MenuItemLocationPrices, input.MenuItem.Id);

            if (input.ProductType == 2)
                await CreateOrUpdateMenuCombo(input.Combos, input.MenuItem.Id.Value, input.MenuItem.Name);
            //await _syncAppService.UpdateSync(SyncConsts.MENU);

            await _redisCacheService.RemoveAsync(AppConsts.GetMenuItemAndPriceForSelectionKey);
        }

        public async Task<PagedResultDto<MenuItemAndPriceForSelection>> GetMenuItemAndPriceForSelection(
            GetMenuItemAndPriceForSelectionInput input)
        {
            //var cache = await _redisCacheService.GetAsync<List<MenuItemAndPriceForSelection>>(AppConsts
            //    .GetMenuItemAndPriceForSelectionKey);
            //if (cache != null)
            //{
            //    var itemsCache = cache.AsQueryable().WhereIf(
            //        !input.Filter.IsNullOrEmpty(),
            //        x => x.Name.Contains(input.Filter));

            //    if (!input.Sorting.IsNullOrEmpty())
            //        itemsCache = itemsCache.OrderBy(input.Sorting);

            //    return new PagedResultDto<MenuItemAndPriceForSelection>(
            //        itemsCache.Count(),
            //        itemsCache.PageBy(input).ToList()
            //    );
            //}

            var menuItems = _menuItemRepository.GetAllIncluding(e => e.Portions)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    c => EF.Functions.Like(c.Name, input.Filter.ToILikeString()));

            await _redisCacheService.SetAsync(AppConsts.GetMenuItemAndPriceForSelectionKey,
                ObjectMapper.Map<List<MenuItemAndPriceForSelection>>(await menuItems.ToListAsync()));

            if (!input.Sorting.IsNullOrEmpty())
                menuItems = menuItems.OrderBy(input.Sorting);

            var sortedMenuItems = await menuItems
                .PageBy(input)
                .ToListAsync();

            var menuItemListDtos = ObjectMapper.Map<List<MenuItemAndPriceForSelection>>(sortedMenuItems);

            menuItemListDtos.ForEach(t =>
            {
                if (t.ProductType == 2)
                    t.ProductComboId = _productComboRepository.FirstOrDefault(x => x.MenuItemId == t.Id)?.Id;
            });

            return new PagedResultDto<MenuItemAndPriceForSelection>(
                await menuItems.CountAsync(),
                menuItemListDtos
            );
        }

        public async Task UpdateLocationPrice(List<LocationPriceInput> inputs)
        {
            foreach (var input in inputs)
            {
                var portion = await _menuItemPortionRepository.GetAllIncluding(e => e.LocationPrices)
                    .FirstOrDefaultAsync(e => e.Id == input.PortionId);
                var locationPrice = portion.LocationPrices.FirstOrDefault(a => a.LocationId.Equals(input.LocationId));

                if (locationPrice != null)
                    locationPrice.Price = input.Price;
                else
                    await _locationItemPriceRepository.InsertAsync(new LocationMenuItemPrice
                    {
                        LocationId = input.LocationId,
                        Price = input.Price,
                        MenuPortionId = portion.Id
                    });
            }
        }

        public async Task DeleteAsync(int input)
        {
            var menuItemLocaion = await _locationMenuItemManager.GetAllByMenuItemId(input);
            await _locationMenuItemCustomRepository.DeleteLocationMenuItems(menuItemLocaion);
            await _menuItemManager.DeleteAsync(input);

            await _redisCacheService.RemoveAsync(AppConsts.GetMenuItemAndPriceForSelectionKey);
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _menuItemRepository.GetAsync(input.Id);

                item.IsDeleted = false;

                await _menuItemRepository.UpdateAsync(item);
            }
        }

        public async Task<PagedResultDto<IdNameDto>> GetSinglePortionItems(GetMenuItemForPromotionInput input)
        {
            var allItems = _menuItemRepository
                .GetAll()
                .Where(a => !input.ExceptIds.Contains(a.Id))
                .WhereIf(input.ProductType != null, x => x.ProductType == input.ProductType.Value)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.AliasName != null &&
                                       EF.Functions.Like(p.AliasName, input.Filter.ToILikeString()) ||
                                       p.AliasCode != null &&
                                       EF.Functions.Like(p.AliasCode, input.Filter.ToILikeString()) ||
                                       p.Category != null && EF.Functions.Like(p.Category.Name,
                                           input.Filter.ToILikeString()))
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = sortItems.Name,
                MenuItemId = sortItems.Id
            }).ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<IdNameDto>(
                allItemCount,
                namesDto
            );
        }

        public async Task<PagedResultDto<IdNameDto>> GetAllMenuItemIncludePortionItems(
            GetMenuItemForPromotionInput input)
        {
            var allItems = _menuItemRepository
                .GetAll()
                .Include(x => x.Portions)
                .Where(a => !input.ExceptIds.Contains(a.Id))
                .WhereIf(input.ProductType != null, x => x.ProductType == input.ProductType.Value)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.AliasName != null &&
                                       EF.Functions.Like(p.AliasName, input.Filter.ToILikeString()) ||
                                       p.AliasCode != null &&
                                       EF.Functions.Like(p.AliasCode, input.Filter.ToILikeString()) ||
                                       p.Category != null && EF.Functions.Like(p.Category.Name,
                                           input.Filter.ToILikeString()))
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.SelectMany(p => p.Portions).Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = sortItems.MenuItem.Name + "(" + sortItems.Name + ")",
                MenuItemId = sortItems.MenuItem.Id
            }).ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<IdNameDto>(
                allItemCount,
                namesDto
            );
        }


        public async Task<ListResultDto<MenuItemListDto>> GetMenuItemForCategoryCombobox(NullableIdDto input)
        {
            var result = await _menuItemRepository.GetAll()
                .WhereIf(input.Id.HasValue, t => t.CategoryId == input.Id)
                .ToListAsync();

            return new ListResultDto<MenuItemListDto>(ObjectMapper.Map<List<MenuItemListDto>>(result));
        }

        public ListResultDto<ComboboxItemDto> GetFoodTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in Enum.GetNames(typeof(ProductFoodType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public ListResultDto<ComboboxItemDto> GetProductTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in Enum.GetNames(typeof(ProductType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetTransactionTypesForCombobox()
        {
            var result = await _transactionTypeRepository.GetAll()
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(result);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetComboItems()
        {
            var lstMenu = await _menuItemRepository.GetAll()
                .Where(a => a.ProductType == 2)
                .Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name))
                .ToListAsync();

            return new ListResultDto<ComboboxItemDto>(lstMenu);
        }

        public async Task<bool> IsBarcodeExists(string barcode)
        {
            if (!string.IsNullOrEmpty(barcode))
            {
                var count = await _menuBarCodeRepository.CountAsync(a => a.Barcode.Equals(barcode));
                return count > 0;
            }

            return false;
        }

        public async Task<PagedResultDto<TiffinProductLookupTableDto>> GetAllTiffinProductSetForLookupTable(
            GetAllForLookupTableInput input)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var allList = _menuItemRepository.GetAll()
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        t => EF.Functions.Like(t.Name, input.Filter.ToILikeString()))
                    .Select(t => new TiffinProductLookupTableDto
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Value = t.Id.ToString()
                    });

                var result = await allList.OrderBy(t => t.Name).PageBy(input).ToListAsync();

                return new PagedResultDto<TiffinProductLookupTableDto>(allList.Count(), result);
            }
        }

        public async Task<List<MenuItemLocationPriceDto>> GetLocationPricesForMenu(GetMenuPortionPriceInput input)
        {
            var menuItemPortions = await _locationItemPriceRepository.GetAllIncluding(e => e.MenuPortion)
                .Where(m => m.MenuPortion.MenuItemId == input.MenuItemId)
                .Select(m => new MenuItemPortionDto
                {
                    LocationId = m.LocationId,
                    MenuItemId = m.MenuPortion.MenuItemId,
                    Id = m.MenuPortionId,
                    PortionName = m.MenuPortion.Name,
                    Price = m.MenuPortion.Price,
                    MultiPlier = m.MenuPortion.MultiPlier,
                    LocationPrice = m.Price,
                    MenuName = m.MenuPortion.MenuItem.Name,
                    LocationName = m.Location.Name
                })
                .ToListAsync();

            var result = menuItemPortions
                .GroupBy(t => new {t.LocationId, t.MenuItemId})
                .Select(g => new MenuItemLocationPriceDto
                {
                    LocationId = g.Key.LocationId,
                    MenuItemId = g.Key.MenuItemId,
                    LocationName = g.FirstOrDefault().LocationName,
                    MenuItemPortions = g.ToList()
                })
                .ToList();

            return result;
        }

        public async Task<FileDto> GetAllMenuItemToExcel()
        {
            var allList = await _menuItemRepository.GetAll()
                .Include(e => e.Portions)
                .Include(e => e.Category)
                .ThenInclude(e => e.ProductGroup)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<MenuItemListDto>>(allList);
            return _menuitemExporter.ExportToFile(allListDtos.OrderBy(a => a.CategoryName).ToList());
        }

        public async Task<FileDto> GetImportMenuItemTemplateToExcel()
        {
            var allList = await _menuItemRepository.GetAll()
                .Include(e => e.Portions)
                .Include(e => e.Category)
                .ThenInclude(e => e.ProductGroup)
                .ToListAsync();

            var allListDtos = ObjectMapper.Map<List<MenuItemListDto>>(allList);
            return _menuitemExporter.ExportToFileForImportTemplate(allListDtos.OrderBy(a => a.CategoryName).ToList());
        }

        public async Task ImportMenuItemToDatabase(string fileToken)
        {
            if (!fileToken.IsNullOrWhiteSpace())
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault(s =>
                            !s.Name.IsNullOrWhiteSpace() && !s.Name.ToLower().Contains("sample"));
                        var headers = new List<string>
                        {
                            "Id",
                            "GroupCode",
                            "GroupName",
                            "CategoryCode",
                            "CategoryName",
                            "AliasCode",
                            "MenuName",
                            "AliasName",
                            "Tag",
                            "Portion",
                            "Price",
                            "Barcode",
                            "Description",
                            "ForceQuantity",
                            "ForceChangePrice",
                            "RestrictPromotion",
                            "NoTax",
                            "ProductType"
                        };

                        if (worksheet != null)
                        {
                            var columns = worksheet.Dimension.End.Column;
                            if (columns < headers.Count)
                            {
                                var sheetHeaders = new List<string>();

                                for (var i = 1; i < columns + 1; i++)
                                    sheetHeaders.Add(worksheet.GetValue(1, i).ToString());

                                var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                                throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                                " column to template. If not applicable please leave it blank.");
                            }
                        }

                        worksheet = excelPackage.Workbook.Worksheets.SingleOrDefault(x =>
                            x.Name.ToLower().Contains("sample"));
                        if (worksheet != null)
                        {
                            excelPackage.Workbook.Worksheets.Delete(worksheet);
                            excelPackage.Save();

                            fileBytes = excelPackage.GetAsByteArray();
                        }

                        var menuItems = _menuItemListExcelDataReader.GetMenuItemsFromExcel(fileBytes);

                        CheckDuplicate(menuItems);

                        await ImportMenuItemInternal(menuItems);

                        await _redisCacheService.RemoveAsync(AppConsts.GetMenuItemAndPriceForSelectionKey);
                    }
                }
            }
        }

        
        public async Task<PagedResultDto<MenuItemPortionDto>> GetLocationMenuItemPrices(GetMenuPortionPriceInput input)
        {
            if (input.LocationId > 0)
            {
                var menuItemIds = _locationItemRepository
                    .GetAll()
                    .Where(x => x.LocationId == input.LocationId)
                    .Select(x => x.MenuItemId).ToList();

                var menuItemPortions = _menuItemPortionRepository
                    .GetAllIncluding(e => e.MenuItem)
                    .WhereIf(menuItemIds.Count > 0, x => menuItemIds.Contains(x.MenuItemId))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        c => EF.Functions.Like(c.Name, input.Filter.ToILikeString())
                             || EF.Functions.Like(c.MenuItem.Name, input.Filter.ToILikeString()))
                    .ToList();

                var returnList = menuItemPortions
                    .Select(mip => new MenuItemPortionDto
                    {
                        Id = mip.Id,
                        MenuName = _menuItemRepository.FirstOrDefault(mip.MenuItemId).Name,
                        PortionName = mip.Name,
                        MenuItemId = mip.MenuItemId,
                        MultiPlier = mip.MultiPlier,
                        LocationId = input.LocationId,
                        Price = mip.Price,
                        LocationPrice =
                            _locationMenuItemPriceRepository.FirstOrDefault(
                                a => a.MenuPortionId == mip.Id && a.LocationId == input.LocationId)
                            != null
                                ? _locationMenuItemPriceRepository.FirstOrDefault(
                                    a => a.MenuPortionId == mip.Id && a.LocationId == input.LocationId).Price
                                : 0M
                    }).AsQueryable();

                if (!input.Sorting.IsNullOrEmpty()) returnList = returnList.OrderBy(input.Sorting);
                var allItemCount = returnList.Count();
                if (input.SkipCount > 0) returnList = returnList.Skip(input.SkipCount);
                if (input.MaxResultCount > 0) returnList = returnList.Take(input.MaxResultCount);
                var finalList = returnList.ToList();

                return new PagedResultDto<MenuItemPortionDto>(allItemCount, finalList);
            }

            return null;
        }

        public async Task<ApiMenuOutput> ApiItemsForLocation(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdByLocationId(locationInput.LocationId);
            if (orgId > 0)
                CurrentUnitOfWork.SetFilterParameter(DineConnectDataFilters.MustHaveOrganization,
                    DineConnectDataFilters.Parameters.OrganizationId, orgId);
            var apiOutput = new ApiMenuOutput();

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                {
                    foreach (var allLocationsItem in allLocations.Items)
                    {
                        var output = await GetAllItems(new GetMenuItemLocationInput
                        {
                            LocationId = Convert.ToInt32(allLocationsItem.Value)
                        }, true);
                        FillMenuItems(output, apiOutput, Convert.ToInt32(allLocationsItem.Value));
                    }

                    if (!apiOutput.Categories.Any())
                    {
                        var myOutput = await GetAllItems(new GetMenuItemLocationInput
                        {
                            LocationId = 0
                        });
                        FillMenuItems(myOutput, apiOutput);
                    }
                }
            }
            else
            {
                var output = await GetAllItems(new GetMenuItemLocationInput
                {
                    LocationId = locationInput.LocationId
                });
                FillMenuItems(output, apiOutput);
            }

            var myLocationTags = _upMenuItemLocationPriceRepository.GetAll()
                .Where(a => a.LocationId == locationInput.LocationId);
            var myTagPriceCount = myLocationTags.Count(a => a.LocationId == locationInput.LocationId);
            if (myTagPriceCount > 0)
                foreach (var tagDto in apiOutput.Categories.SelectMany(a => a.MenuItems))
                    if (tagDto.UpMenuItems.Any())
                        foreach (var upItem in tagDto.UpMenuItems)
                        {
                            var myTag = myLocationTags.Where(a => a.UpMenuItemId == upItem.Id).ToList();
                            if (myTag.Any()) upItem.Price = myTag.Last().Price;
                        }

            return apiOutput;
        }

        public async Task<PagedResultDto<IdNameDto>> GetAllPortionItems(GetPortionItemInput input)
        {
            var portions = _menuItemPortionRepository
                .GetAllIncluding(x => x.MenuItem)
                .Where(x => x.IsDeleted == input.IsDeleted)
                .WhereIf(input.MenuItemId.HasValue, x => x.MenuItemId == input.MenuItemId);
            
            var sortMenuItems = await portions
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = $"{sortItems.MenuItem.Name} - {sortItems.Name}"
            }).ToList();

            var allItemCount = await portions.CountAsync();

            return new PagedResultDto<IdNameDto>(
                allItemCount,
                namesDto
            );
        }

        private async Task CreateOrUpdateMenuCombo(List<ProductComboGroupEditDto> comboGs, int mId, string name)
        {
            try
            {
                var onlyDuplicates = comboGs.GroupBy(x => x.Name)
                    .Where(x => x.Count() > 1);

                if (onlyDuplicates.Any())
                    throw new UserFriendlyException(string.Format(L("DuplicateExists_f"), L("Group"), L("Combo")));

                var exists = await _productComboRepository
                    .GetAll()
                    .Include(x => x.ProductComboGroupDetails)
                    .Where(a => a.MenuItemId == mId).ToListAsync();
                if (exists != null && exists.Any())
                {
                    var firstCom = exists.FirstOrDefault();
                    firstCom.Name = name;
                    await UpdateMenuCombo(comboGs, firstCom);
                }
                else
                {
                    var pCombo = new ProductCombo
                    {
                        Name = name,
                        MenuItemId = mId
                    };
                    var comboId = await _productComboRepository.InsertAndGetIdAsync(pCombo);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    await UpdateMenuCombo(comboGs, pCombo);
                }
            }
            catch (Exception exception)
            {
                //_logger.Fatal("CREATE OR UPDATE COMBO", exception);
            }
        }

        private async Task UpdateMenuCombo(List<ProductComboGroupEditDto> comboGs, ProductCombo firstCom)
        {
            var comboGrExists = comboGs.Where(a => a.Id.HasValue && a.Id > 0).ToList();

            var removeGroups = await _productComboGroupDetailRepository.GetAll()
                .Where(p => p.ProductComboId == firstCom.Id)
                .Where(a => !comboGrExists.Select(e => e.Id.Value).Contains(a.ProductComboGroupId))
                .ToListAsync();

            removeGroups.ForEach(i => { _productComboGroupDetailRepository.Delete(i); });

            var comboGroupNoIds = comboGs.Where(a => !a.Id.HasValue || a.Id == 0).ToList();
            comboGroupNoIds.ForEach(e =>
            {
                var nameExists = _productComboGroupRepository.FirstOrDefault(c => c.Name == e.Name);
                if (nameExists != null)
                {
                    ObjectMapper.Map(e, nameExists);
                }
                else
                {
                    var group = ObjectMapper.Map<ProductComboGroup>(e);
                    var cgId = _productComboGroupRepository.InsertAndGetId(group);
                    var existsDetails = _productComboGroupDetailRepository.GetAll()
                        .Where(p => p.ProductComboId == firstCom.Id && p.ProductComboGroupId == cgId).ToList();
                    if (!existsDetails.Any())
                        _productComboGroupDetailRepository.Insert(new ProductComboGroupDetail
                            {ProductComboId = firstCom.Id, ProductComboGroupId = cgId});
                }
            });

            foreach (var productComboGroup in comboGrExists)
                await UpdateProductComboGroup(productComboGroup, firstCom.Id);

            await _productComboRepository.UpdateAsync(firstCom);
        }

        private async Task UpdateProductComboGroup(ProductComboGroupEditDto dto, int productComboId)
        {
            var productComboGroup = await _productComboGroupRepository.GetAsync(dto.Id.Value);


            foreach (var cIdItem in dto.ComboItems.Where(a => a.Id.HasValue && a.Id > 0))
                //var ci = _productComboItemRepository.Get(cIdItem.Id.Value);
                cIdItem.Count = cIdItem.Count == 0 && cIdItem.AutoSelect ? 1 : cIdItem.Count;
            //ObjectMapper.Map(cIdItem, ci);

            if (dto.ComboItems.Any())
            {
                var toRemoveComboItems = _productComboItemRepository.GetAll()
                    .Where(a => a.ProductComboGroupId == dto.Id &&
                                !dto.ComboItems.Select(u => u.Id).Contains(a.Id))
                    .ToList();

                toRemoveComboItems.ForEach(e => _productComboItemRepository.DeleteAsync(e));
            }

            ObjectMapper.Map(dto, productComboGroup);
            var existsDetails = _productComboGroupDetailRepository.GetAll().Where(p =>
                p.ProductComboId == productComboId && p.ProductComboGroupId == productComboGroup.Id).ToList();
            if (!existsDetails.Any())
                await _productComboGroupDetailRepository.InsertAsync(new ProductComboGroupDetail
                    {ProductComboId = productComboId, ProductComboGroupId = productComboGroup.Id});

            await _productComboGroupRepository.UpdateAsync(productComboGroup);
        }

        protected virtual async Task<int> CreateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            try
            {
                var menuItem = ObjectMapper.Map<MenuItem>(input.MenuItem);
                menuItem.ProductType = input.ProductType;
                menuItem.CategoryId = input.MenuItem.CategoryId.GetValueOrDefault();
                menuItem.TenantId = AbpSession.TenantId ?? 0;
                if (!string.IsNullOrEmpty(input.MenuItem.AllergicInfo))
                    menuItem.AllergicInfo = input.MenuItem.AllergicInfo;
                if (menuItem.TransactionType != null)
                    if (menuItem.TransactionType?.Name == null)
                        menuItem.TransactionType.Name = string.Empty;

                //menuItem.Category = null;
                //menuItem.TransactionType = null;

                if (menuItem.ProductType == 0)
                    menuItem.ProductType = 1;
                var dto = input.MenuItem;

                if (dto?.UpMenuItems != null && dto.UpMenuItems.Any())
                    foreach (var otdto in dto.UpMenuItems)
                        if (menuItem.UpMenuItems.Any(a => a.RefMenuItemId != otdto.RefMenuItemId && a.ProductType == 2))
                        {
                            var obj = ObjectMapper.Map<UpMenuItem>(otdto);
                            obj.ProductType = 2;
                            menuItem.UpMenuItems.Add(obj);
                        }

                if (dto?.UpSingleMenuItems != null && dto.UpSingleMenuItems.Any())
                    foreach (var otdto in dto.UpSingleMenuItems)
                        if (menuItem.UpMenuItems.Any(a => a.RefMenuItemId != otdto.RefMenuItemId && a.ProductType == 1))
                        {
                            var obj = ObjectMapper.Map<UpMenuItem>(otdto);
                            obj.ProductType = 1;
                            menuItem.UpMenuItems.Add(obj);
                        }

                UpdateLocations(menuItem, dto.LocationGroup);

                //UpdateLocationAndNonLocation(menuItem, input.LocationGroup);
                //CheckErrors(await _miManager.CreateOrUpdateSync(menuItem));
                var output = await _menuItemRepository.InsertAndGetIdAsync(menuItem);
                //await _wheelScreenMenuItemsAppService.CreateWheelScreenMenuSectionFromMenuItem(output);
                await CurrentUnitOfWork.SaveChangesAsync();
                return output;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        protected virtual async Task UpdateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            var dto = input.MenuItem;
            var item = await _menuItemRepository.GetAll().FirstOrDefaultAsync(x => x.Id == input.MenuItem.Id.Value);
            dto.UpMenuItems.ForEach(e => { e.ProductType = 2; });
            dto.UpSingleMenuItems.ForEach(e => { e.ProductType = 1; });
            dto.UpMenuItems.AddRange(dto.UpSingleMenuItems);

            var toRemoveUps = _upMenuItemRepository.GetAll().Where(a =>
                    a.MenuItemId == dto.Id && !a.Id.Equals(0) && !dto.UpMenuItems.Select(u => u.Id).Contains(a.Id))
                .ToList();
            toRemoveUps.ForEach(e => _upMenuItemRepository.DeleteAsync(e));

            var toRemovePortions = _menuItemPortionRepository.GetAll().Where(a =>
                a.MenuItemId == dto.Id && !a.Id.Equals(0) && !dto.Portions.Select(u => u.Id).Contains(a.Id)).ToList();
            toRemovePortions.ForEach(e => _menuItemPortionRepository.DeleteAsync(e));

            var toRemoveBacodes = _menuBarCodeRepository.GetAll().Where(a =>
                a.MenuItemId == dto.Id && !a.Id.Equals(0) && !dto.Barcodes.Select(u => u.Id).Contains(a.Id)).ToList();
            toRemoveBacodes.ForEach(e => _menuBarCodeRepository.DeleteAsync(e));

            ObjectMapper.Map(dto, item);
            UpdateLocations(item, dto.LocationGroup);
           
            if (dto.CategoryId.HasValue && dto.CategoryId != item.CategoryId) item.CategoryId = dto.CategoryId.Value;

            await _menuItemRepository.UpdateAsync(item);

            if (input.MenuItemLocationPrices.Any())
                await UpdateLocationPriceForMenu(input.MenuItemLocationPrices, input.MenuItem.Id);
        }

        private async Task UpdateLocationPriceForMenu(List<MenuItemLocationPriceDto> inputs, int? menuItemId)
        {
            var locationIds = inputs.Select(l => l.LocationId).Distinct().ToList();

            var deleteList = await _locationItemPriceRepository
                .GetAll()
                .Where(l => !locationIds.Contains(l.LocationId) && l.MenuPortion.MenuItemId == menuItemId)
                .ToListAsync();

            deleteList.ForEach(l => _locationItemPriceRepository.DeleteAsync(l));

            var locationPriceInputs = inputs
                .SelectMany(m => m.MenuItemPortions)
                .Select(l => new LocationPriceInput
                {
                    LocationId = l.LocationId,
                    PortionId = l.Id.Value,
                    Price = l.LocationPrice
                }).ToList();

            await UpdateLocationPrice(locationPriceInputs);
        }

        public async Task<Dictionary<int, List<ComboboxItemDto>>> GetMenuItemForComboBox()
        {
            var result = (await _menuItemRepository.GetAll()
                    .Select(x => new
                    {
                        x.CategoryId,
                        Value = x.Id.ToString(),
                        DisplayText = x.Name.ToString()
                    })
                    .ToListAsync())
                .GroupBy(x => x.CategoryId)
                .ToDictionary(x => x.Key, x => x.Select(o => new ComboboxItemDto(o.Value, o.DisplayText)).ToList());

            return result;
        }

        private async Task ImportMenuItemInternal(List<MenuItemEditDto> menuItems)
        {
            var row = 0;
            var categories = menuItems.GroupBy(a => new {a.CategoryName, a.GroupName, a.CategoryCode, a.GroupCode});
            try
            {
                foreach (var category in categories)
                {
                    var cateInput = new CategoryDto
                    {
                        Name = category.Key.CategoryName,
                        GroupName = category.Key.GroupName,
                        Code = category.Key.CategoryCode,
                        GroupCode = category.Key.GroupCode
                    };

                    row = category.OrderBy(g => g.RowIndex).FirstOrDefault().RowIndex;

                    var cate = await _categoryAppService.GetOrCreateCategory(cateInput);

                    await CreateMenuItemForImport((int) cate.Id, category.ToList());
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException($"{exception.Message}");
            }
        }

        private async Task CreateMenuItemForImport(int categoryId, List<MenuItemEditDto> menuItems)
        {
            CreateOrUpdateMenuItemInput input = null;

            foreach (var item in menuItems.OrderBy(a => a.Name))
            {
                var menuItem = item;
                menuItem.CategoryId = categoryId;
                menuItem.AddPortion(item.TPName, item.TPPrice);

                if (input == null)
                {
                    input = new CreateOrUpdateMenuItemInput
                    {
                        MenuItem = menuItem,
                        NotOverride = true
                    };
                }
                else if (item.Name.Equals(input.MenuItem.Name))
                {
                    input.MenuItem.AddPortion(item.TPName, item.TPPrice);
                }
                else
                {
                    if (input.MenuItem.Id > 0)
                        input.UpdateItem = true;

                    await CreateOrUpdateMenuItem(input);

                    input = new CreateOrUpdateMenuItemInput
                    {
                        MenuItem = menuItem,
                        NotOverride = true
                    };
                }
            }

            if (input != null)
            {
                if (input.MenuItem.Id > 0)
                    input.UpdateItem = true;

                await CreateOrUpdateMenuItem(input);
            }
        }

        private void CheckDuplicate(List<MenuItemEditDto> menuItems)
        {
            var anyDuplicate = menuItems.GroupBy(x => new {x.Name, x.TPName}).Where(g => g.Count() > 1);
            if (anyDuplicate.Any())
            {
                var builder = new StringBuilder();
                builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("Name")));
                builder.Append(anyDuplicate.Select(e => e.Key.Name).JoinAsString(", "));

                throw new UserFriendlyException(builder.ToString());
            }

            var alaisDuplicates = menuItems.Where(a => !string.IsNullOrEmpty(a.AliasCode))
                .GroupBy(x => new {x.AliasCode, x.TPName}).Where(g => g.Count() > 1);
            if (alaisDuplicates.Any())
            {
                var builder = new StringBuilder();
                builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("AliasCode")));
                builder.Append(alaisDuplicates.Select(e => e.Key.AliasCode).JoinAsString(", "));

                throw new UserFriendlyException(builder.ToString());
            }
        }

        public async Task<FileDto> GetMenuPriceTemplate(int locationId)
        {
            var input = new GetMenuPortionPriceInput
            {
                LocationId = locationId,
                Sorting = "Id"
            };
            var locationMenuItemPrices = await GetLocationMenuItemPrices(input);

            return _menuitemExporter.ExportPriceForLocation(locationMenuItemPrices.Items.ToList());
        }

        public async Task ImportLocationMenuItemPricesToDatabase(string fileToken, int locationId)
        {
            if (!fileToken.IsNullOrWhiteSpace())
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();
                        var headers = new List<string>
                        {
                            L("Id"),
                            L("MenuName"),
                            L("Portion"),
                            L("Price"),
                            L("LocationPrice")
                        };

                        var columns = worksheet.Dimension.End.Column;
                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (var i = 1; i < columns + 1; i++) sheetHeaders.Add(worksheet.GetValue(1, i).ToString());

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }

                var locationMenuItemPrices =
                    _locationMenuItemPricesExcelDataReader.GetLocationMenuItemPricesFromExcel(fileBytes, locationId);

                foreach (var locationMenuItemPrice in locationMenuItemPrices)
                {
                    var portion = await _menuItemPortionRepository
                        .GetAllIncluding(x => x.LocationPrices)
                        .FirstOrDefaultAsync(x => x.Id == locationMenuItemPrice.PortionId);
                    var locationPrice =
                        portion.LocationPrices.FirstOrDefault(
                            a => a.LocationId.Equals(locationMenuItemPrice.LocationId));

                    if (locationPrice != null)
                    {
                        locationPrice.Price = locationMenuItemPrice.Price;
                    }
                    else
                    {
                        locationPrice = new LocationMenuItemPrice
                        {
                            LocationId = locationMenuItemPrice.LocationId,
                            Price = locationMenuItemPrice.Price,
                            MenuPortionId = portion.Id
                        };

                        portion.LocationPrices.Add(locationPrice);
                    }
                }
            }
        }

        // --

        public async Task<PagedResultDto<MenuItemListDto>> GetLocationMenuItems(GetLocationMenuItemsInput input)
        {
            var locationItemQuery = _locationItemRepository.GetAll().Where(x => x.LocationId == input.LocationId);
            var menuItemIds = await locationItemQuery.Select(x => x.MenuItemId).Distinct().ToListAsync();

            if (menuItemIds.Count > 0)
            {
                var menuItemsQuery = _menuItemRepository.GetAll()
                    .Include(x => x.Category)
                    .Where(x => menuItemIds.Contains(x.Id))
                    .WhereIf(!input.Filter.IsNullOrWhiteSpace(), x =>
                        EF.Functions.Like(x.Name, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(x.AliasName, input.Filter.ToILikeString()) ||
                        EF.Functions.Like(x.AliasCode, input.Filter.ToILikeString()) ||
                        x.Category != null && EF.Functions.Like(x.Category.Name, input.Filter.ToILikeString()));

                var sortedMenuItems = await menuItemsQuery
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .Select(x => ObjectMapper.Map<MenuItemListDto>(x))
                    .ToListAsync();
                ;

                return new PagedResultDto<MenuItemListDto>(
                    await menuItemsQuery.CountAsync(),
                    sortedMenuItems
                );
            }

            return new PagedResultDto<MenuItemListDto>(0, new List<MenuItemListDto>());
        }

        public FileDto GetLocationMenuItemTemplate()
        {
            var locationMenus = GetAll(new GetMenuItemInput
            {
                IsDeleted = false,
                Sorting = "Id",
                MaxResultCount = int.MaxValue
            }).GetAwaiter().GetResult();

            return _locationMenuItemExcelExporter.ExportToFile(locationMenus.Items.ToList());
        }

        public async Task ImportLocationMenuItems(string fileToken, int locationId)
        {
            if (!fileToken.IsNullOrWhiteSpace())
            {
                var fileBytes = _tempFileCacheManager.GetFile(fileToken);

                using (var stream = new MemoryStream(fileBytes))
                {
                    using (var excelPackage = new ExcelPackage(stream))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();
                        var headers = new List<string>
                        {
                            L("Id"),
                            L("Category"),
                            L("Name")
                        };

                        var columns = worksheet.Dimension.End.Column;
                        if (columns < headers.Count)
                        {
                            var sheetHeaders = new List<string>();

                            for (var i = 1; i < columns + 1; i++) sheetHeaders.Add(worksheet.GetValue(1, i).ToString());

                            var noExistCols = headers.Where(e => !sheetHeaders.Contains(e));

                            throw new UserFriendlyException("Please add " + noExistCols.JoinAsString(", ") +
                                                            " column to template. If not applicable please leave it blank.");
                        }
                    }
                }

                var locationMenuItemDtos = _locationMenuItemExcelDataReader.GetItemsFromExcel(fileBytes, locationId);
                var menuItemIds = locationMenuItemDtos.Select(x => x.MenuItemId).ToList();
                menuItemIds = await _menuItemRepository.GetAll().Where(x => menuItemIds.Contains(x.Id))
                    .Select(x => x.Id).ToListAsync();
                var input = new LocationMenuItemInput
                {
                    LocationId = locationId,
                    MenuItems = menuItemIds
                };

                await CreateLocationMenuItem(input);
            }
        }

        public async Task<FileDto> ExportLocationMenuItems(int locationId)
        {
            var locationItemQuery = _locationItemRepository.GetAll().Where(x => x.LocationId == locationId);
            var menuItemIds = await locationItemQuery.Select(x => x.MenuItemId).Distinct().ToListAsync();
            var menuItemsQuery = _menuItemRepository.GetAll()
                .Include(x => x.Category)
                .Where(x => menuItemIds.Contains(x.Id));
            var menuItemDtos = await menuItemsQuery.Select(x => ObjectMapper.Map<MenuItemListDto>(x)).ToListAsync();

            return _locationMenuItemExcelExporter.ExportToFile(menuItemDtos);
        }

        public async Task CreateLocationMenuItem(LocationMenuItemInput input)
        {
            if (input.MenuItemId > 0)
            {
                var checkItem = await _locationItemRepository.CountAsync(x =>
                    x.LocationId.Equals(input.LocationId) && x.MenuItemId.Equals(input.MenuItemId));
                if (checkItem <= 0)
                    await _locationItemRepository.InsertAsync(new LocationMenuItem
                    {
                        MenuItemId = input.MenuItemId,
                        LocationId = input.LocationId
                    });
            }

            if (input.MenuItems != null && input.MenuItems.Any())
            {
                input.DeleteMenuItems = true;
                await DeleteLocationMenuItem(input);

                foreach (var menuItem in input.MenuItems)
                    await _locationItemRepository.InsertAsync(new LocationMenuItem
                    {
                        MenuItemId = menuItem,
                        LocationId = input.LocationId
                    });
            }
        }

        public async Task DeleteLocationMenuItem(LocationMenuItemInput input)
        {
            await _locationItemRepository.DeleteAsync(x =>
                x.LocationId.Equals(input.LocationId) && x.MenuItemId.Equals(input.MenuItemId));
            if (input.DeleteMenuItems)
                _locationItemRepository.Delete(x => x.LocationId == input.LocationId);
        }

        private void FillMenuItems(PagedResultDto<MenuItemEditDto> output, ApiMenuOutput apiOutput, int locationId = 0)
        {
            foreach (var ou in output.Items)
            {
                if (ou?.CategoryId == null)
                    continue;
                var cat = apiOutput.Categories.SingleOrDefault(a => a.Id == ou.CategoryId);
                if (cat == null)
                {
                    cat = new ApiCategoryOutput
                    {
                        Name = ou.CategoryName,
                        Id = ou.CategoryId,
                        Code = ou.CategoryCode
                    };
                    if (ou.Category != null && ou.Category.ProductGroup != null)
                    {
                        cat.ProductGroup = new ApiProductGroup();
                        FillParentGroup(cat.ProductGroup, ou.Category.ProductGroup);
                    }

                    apiOutput.Categories.Add(cat);
                }

                var menuItem = new ApiMenuItemOutput
                {
                    Id = ou.Id ?? 0,
                    Name = ou.Name,
                    Files = ou.Files,
                    DownloadImage = ou.DownloadImage,
                    AliasName = ou.AliasName,
                    AliasCode = ou.AliasCode,
                    BarCode = ou.BarCode,
                    Description = ou.ItemDescription,
                    ForceQuantity = ou.ForceQuantity,
                    RestrictPromotion = ou.RestrictPromotion,
                    NoTax = ou.NoTax,
                    ForceChangePrice = ou.ForceChangePrice,
                    TransactionTypeId = ou.TransactionTypeId,
                    TransactionTypeName = ou.TransactionTypeName,
                    Tag = ou.Tag,
                    Uom = ou.Uom,
                    ProductType = ou.ProductType,
                    LocationId = locationId
                };

                foreach (var po in ou.Portions)
                    menuItem.MenuPortions.Add(new ApiMenuItemPortionOutput
                    {
                        Id = po.Id ?? 0,
                        PortionName = po.Name,
                        Barcode = po.Barcode,
                        Price = po.Price,
                        Multiplier = po.MultiPlier,
                        ChangePrice = po.ChangePrice
                    });
                foreach (var po in ou.Barcodes)
                    menuItem.Barcodes.Add(new ApiMenuBarcodeOutput
                    {
                        Id = po.Id ?? 0,
                        Barcode = po.Barcode
                    });
                foreach (var po in ou.UpMenuItems)
                    menuItem.UpMenuItems.Add(new ApiUpMenuItemOutput
                    {
                        Id = po.Id ?? 0,
                        AddBaseProductPrice = po.AddBaseProductPrice,
                        MenuItemId = po.MenuItemId,
                        RefMenuItemId = po.RefMenuItemId,
                        Price = po.Price,
                        MinimumQuantity = po.MinimumQuantity,
                        MaxQty = po.MaxQty,
                        AddAuto = po.AddAuto,
                        AddQuantity = po.AddQuantity,
                        ProductType = po.ProductType
                    });
                foreach (var po in ou.MenuItemSchedules)
                    menuItem.MenuItemSchedules.Add(new ApiMenuItemSchedule
                    {
                        Id = po.Id ?? 0,
                        StartHour = po.StartHour,
                        EndHour = po.EndHour,
                        StartMinute = po.StartMinute,
                        EndMinute = po.EndMinute,
                        Days = po.Days,
                        MenuItemId = menuItem.Id
                    });

                foreach (var po in ou.MenuItemDescriptions)
                    menuItem.MenuItemDescriptions.Add(new ApiMenuItemDescription
                    {
                        MenuItemId = menuItem.Id,
                        LanguageCode = po.LanguageCode,
                        ProductName = po.ProductName,
                        Description = po.Description
                    });

                cat.MenuItems.Add(menuItem);

                if (ou.Id > 0 && ou.ProductType == 2)
                {
                    var cItem = _productComboRepository.GetAll()
                        .Include(a => a.ProductComboGroupDetails).ThenInclude(a => a.ProductComboGroup)
                        .ThenInclude(a => a.ComboItems)
                        .Where(a => a.MenuItemId == ou.Id).ToList().LastOrDefault();
                    if (cItem != null)
                        try
                        {
                            menuItem.Combo = ObjectMapper.Map<ApiProductComboOutput>(cItem);
                        }
                        catch (Exception exception)
                        {
                            var myMess = exception.Message;
                        }
                }
            }
        }

        private void FillParentGroup(ApiProductGroup catProductGroup, ProductGroupOutputEditDto editDto)
        {
            if (editDto.Id.HasValue)
            {
                catProductGroup.Name = editDto.Name;
                catProductGroup.Code = editDto.Code;
                catProductGroup.Id = editDto.Id.Value;
                catProductGroup.ParentId = editDto.ParentId;
            }

            if (editDto.Parent != null)
            {
                catProductGroup.Parent = new ApiProductGroup();
                FillParentGroup(catProductGroup.Parent, editDto.Parent);
            }
        }
    }
}
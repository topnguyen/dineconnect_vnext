﻿using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public class MenuItemListExcelDataReader : NpoiExcelImporterBase<MenuItemEditDto>, IMenuItemListExcelDataReader
    {
        public List<MenuItemEditDto> GetMenuItemsFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private MenuItemEditDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var menuItem = new MenuItemEditDto();

            try
            {
                var id = 0;
                var idCell = worksheet.GetRow(row).GetCell(0);
                if (idCell?.CellType == CellType.Numeric)
                    id = Convert.ToInt32(idCell.NumericCellValue);
                else
                    id = Convert.ToInt32(idCell?.StringCellValue);

                menuItem.Id = null;
                if (id > 0)
                {
                    menuItem.Id = id;
                }
                menuItem.GroupCode = GetValue(worksheet.GetRow(row).GetCell(1))?.Trim();
                menuItem.GroupName = GetValue(worksheet.GetRow(row).GetCell(2)).Trim();
                menuItem.CategoryCode = GetValue(worksheet.GetRow(row).GetCell(3)).Trim();
                menuItem.CategoryName = GetValue(worksheet.GetRow(row).GetCell(4)).Trim();
                menuItem.AliasCode = GetValue(worksheet.GetRow(row).GetCell(5)).Trim();
                menuItem.Name = GetValue(worksheet.GetRow(row).GetCell(6)).Trim();
                menuItem.AliasName = GetValue(worksheet.GetRow(row).GetCell(7)).Trim();
                menuItem.Tag = GetValue(worksheet.GetRow(row).GetCell(8)).Trim();
                menuItem.TPName = GetValue(worksheet.GetRow(row).GetCell(9)).Trim() ?? "Normal";
                menuItem.TPPrice = Convert.ToDecimal(GetValue(worksheet.GetRow(row).GetCell(10)).Trim());
                menuItem.BarCode = GetValue(worksheet.GetRow(row).GetCell(11)).Trim();
                menuItem.ItemDescription = GetValue(worksheet.GetRow(row).GetCell(12)).Trim();
                menuItem.ForceQuantity = Convert.ToBoolean(GetValue(worksheet.GetRow(row).GetCell(13)).Trim().Equals("1"));
                menuItem.ForceChangePrice = Convert.ToBoolean(GetValue(worksheet.GetRow(row).GetCell(14)).Trim().Equals("1"));
                menuItem.RestrictPromotion = Convert.ToBoolean(GetValue(worksheet.GetRow(row).GetCell(15)).Trim().Equals("1"));
                menuItem.NoTax = Convert.ToBoolean(GetValue(worksheet.GetRow(row).GetCell(16)).Trim().Equals("1"));
                menuItem.ProductType = Convert.ToInt32(GetValue(worksheet.GetRow(row).GetCell(17)).Trim());
                menuItem.RowIndex = row + 1;
            }
            catch (Exception exception)
            {
                menuItem.Exception = exception.Message;
            }

            return menuItem;
        }

        private string GetValue(ICell cell)
        {
            if (cell == null)
            {
                return string.Empty;
            }

            if (cell.CellType == CellType.String)
            {
                return cell.StringCellValue;
            }

            if (cell.CellType == CellType.Numeric)
            {
                return cell.NumericCellValue.ToString();
            }

            if (cell.CellType == CellType.Boolean)
            {
                return cell.BooleanCellValue.ToString();
            }

            return string.Empty;
        }
        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            return string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(4)?.StringCellValue) ||
                string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(6)?.StringCellValue);
        }
    }
}
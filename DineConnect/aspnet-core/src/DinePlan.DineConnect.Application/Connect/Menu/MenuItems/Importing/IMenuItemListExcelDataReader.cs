﻿using Abp.Application.Services;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public interface IMenuItemListExcelDataReader : IApplicationService
    {
        List<MenuItemEditDto> GetMenuItemsFromExcel(byte[] fileBytes);
    }
}
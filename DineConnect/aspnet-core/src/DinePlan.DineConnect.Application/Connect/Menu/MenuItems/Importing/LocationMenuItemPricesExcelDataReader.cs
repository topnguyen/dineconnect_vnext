﻿using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public class LocationMenuItemPricesExcelDataReader : NpoiExcelImporterBase<LocationPriceInput>, ILocationMenuItemPricesExcelDataReader
    {
        public List<LocationPriceInput> GetLocationMenuItemPricesFromExcel(byte[] fileBytes, int locationId)
        {
            var result = ProcessExcelFile(fileBytes, ProcessExcelRow);
            result.ForEach(x =>
            {
                x.LocationId = locationId;
            });

            return result;
        }

        private LocationPriceInput ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            if (IsEmptyRow(worksheet, row))
            {
                return null;
            }

            var item = new LocationPriceInput();

            try
            {
                var id = 0;
                var idCell = worksheet.GetRow(row).GetCell(0);
                if (idCell?.CellType == CellType.Numeric)
                    id = Convert.ToInt32(idCell.NumericCellValue);
                else
                    id = Convert.ToInt32(idCell?.StringCellValue);
                var priceCell = worksheet.GetRow(row).GetCell(4);
                var price = 0M;
                if (priceCell?.CellType == CellType.Numeric)
                    price = Convert.ToDecimal(priceCell.NumericCellValue);
                else
                    price = Convert.ToDecimal(priceCell?.StringCellValue);

                item.PortionId = id;
                item.Price = price;
            }
            catch (Exception exception)
            {
            }

            return item;
        }

        private bool IsEmptyRow(ISheet worksheet, int row)
        {
            return string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(1)?.StringCellValue);
        }
    }
}
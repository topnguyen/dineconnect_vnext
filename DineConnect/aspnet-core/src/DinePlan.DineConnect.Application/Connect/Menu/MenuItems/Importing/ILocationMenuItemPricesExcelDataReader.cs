﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public interface ILocationMenuItemPricesExcelDataReader : IApplicationService
    {
        List<LocationPriceInput> GetLocationMenuItemPricesFromExcel(byte[] fileBytes, int locationId);
    }
}
﻿using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public class LocationMenuItemExcelDataReader : NpoiExcelImporterBase<LocationMenuItemDataReaderDto>, ILocationMenuItemExcelDataReader
    {
        public List<LocationMenuItemDataReaderDto> GetItemsFromExcel(byte[] fileBytes, int locationId)
        {
            var result = ProcessExcelFile(fileBytes, ProcessExcelRow);
            result.ForEach(x =>
            {
                x.LocationId = locationId;
            });

            return result;
        }

        private LocationMenuItemDataReaderDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (worksheet.Workbook.ActiveSheetIndex != 0)
            {
                return null;
            }

            if (IsEmptyRow(worksheet, row))
            {
                return null;
            }

            var item = new LocationMenuItemDataReaderDto();

            try
            {
                var id = 0;
                var idCell = worksheet.GetRow(row).GetCell(0);
                if (idCell?.CellType == CellType.Numeric)
                    id = Convert.ToInt32(idCell.NumericCellValue);
                else
                    id = Convert.ToInt32(idCell?.StringCellValue);

                item.MenuItemId = id;
                item.MenuItemName = worksheet.GetRow(row).GetCell(2)?.StringCellValue;
            }
            catch (Exception exception)
            {
            }

            return item;
        }

        private bool IsEmptyRow(ISheet worksheet, int row)
        {
            return string.IsNullOrWhiteSpace(worksheet.GetRow(row).GetCell(2)?.StringCellValue);
        }
    }
}
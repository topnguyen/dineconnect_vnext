﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Importing
{
    public interface ILocationMenuItemExcelDataReader : IApplicationService
    {
        List<LocationMenuItemDataReaderDto> GetItemsFromExcel(byte[] fileBytes, int locationId);
    }
}
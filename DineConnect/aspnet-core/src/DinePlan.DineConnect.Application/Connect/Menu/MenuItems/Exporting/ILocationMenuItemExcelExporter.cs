﻿using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Exporting
{
    public interface ILocationMenuItemExcelExporter
    {
        FileDto ExportToFile(List<MenuItemListDto> items);

        FileDto ExportToFileForImportTemplate();
    }
}
﻿using Abp.AspNetZeroCore.Net;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Exporting;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.LocationMenuItems.Exporting
{
    public class LocationMenuItemExcelExporter : NpoiExcelExporterBase, ILocationMenuItemExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LocationMenuItemExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<MenuItemListDto> menuItemListDtos)
        {
            return CreateExcelPackage(
                $"{L("LocationMenuItem")}.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LocationMenuItem"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("CategoryName"),
                        L("Name")
                        );

                    var rowIndex = 2;
                    AddObjects(
                        sheet, rowIndex, menuItemListDtos,
                        _ => _.Id,
                        _ => _.CategoryName,
                        _ => _.Name
                        );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportToFileForImportTemplate()
        {
            return CreateExcelPackage(
                $"{L("LocationMenuItem")}.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LocationMenuItem"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("CategoryName"),
                        L("Name")
                        );

                    for (var i = 1; i <= 3; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
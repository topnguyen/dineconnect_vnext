﻿using Abp.AspNetZeroCore.Net;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Exporting
{
    public class MenuItemExcelExporter : NpoiExcelExporterBase, IMenuItemExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MenuItemExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<MenuItemListDto> menuItemListDtos)
        {
            return CreateExcelPackage(
                @L("MenuItem") + "-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                       "Id",
                            "GroupCode",
                            "GroupName",
                            "CategoryCode",
                            "CategoryName",
                            "AliasCode",
                            "MenuName",
                            "AliasName",
                            "Tag",
                            "Portion",
                            "Price",
                            "Barcode",
                            "Description",
                            "ForceQuantity",
                            "ForceChangePrice",
                            "RestrictPromotion",
                            "NoTax",
                            "ProductType"
                        );

                    var rowIndex = 2;
                    foreach (var item in menuItemListDtos)
                    {
                        AddObjects(
                            sheet, rowIndex, item.Portions,
                            _ => item.Id,
                            _ => item.ProductGroupCode,
                            _ => item.ProductGroupName,
                            _ => item.CategoryCode,
                            _ => item.CategoryName,
                            _ => item.AliasCode,
                            _ => item.Name,
                            _ => item.AliasName,
                            _ => item.Tag,
                            _ => _.Name,
                            _ => _.Price,
                            _ => item.BarCode,
                            _ => item.ItemDescription,
                            _ => item.ForceQuantity ? "1" : "0",
                            _ => item.ForceChangePrice ? "1" : "0",
                            _ => item.RestrictPromotion ? "1" : "0",
                            _ => item.NoTax ? "1" : "0",
                            _ => item.ProductType
                            );

                        rowIndex += item.Portions.Count;
                    }

                    for (var i = 1; i <= 18; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportToFileForImportTemplate(List<MenuItemListDto> items)
        {
            return CreateExcelPackage(
                @L("MenuItem") + "-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("GroupCode"),
                        L("GroupName"),
                        L("CategoryCode"),
                        L("CategoryName"),
                        L("AliasCode"),
                        L("MenuName"),
                        L("AliasName"),
                        L("Tag"),
                        L("Portion"),
                        L("Price"),
                        L("Barcode"),
                        L("Description"),
                        L("ForceQuantity"),
                        L("ForceChangePrice"),
                        L("RestrictPromotion"),
                        L("NoTax")
                        );

                    for (var i = 1; i <= 17; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }


        public FileDto ExportPriceForLocation(List<MenuItemPortionDto> menuItemPortionDtos)
        {
            return CreateExcelPackage("Price-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx", excelPackage =>
            {
                var sheet = excelPackage.CreateSheet(L("Locations"));

                AddHeader(
                    sheet,
                    L("Id"),
                    L("MenuName"),
                    L("Portion"),
                    L("Price"),
                    L("LocationPrice")
                    );

                var row = 2;
                AddObjects(
                        sheet, row, menuItemPortionDtos,
                        _ => _.Id,
                        _ => _.MenuName,
                        _ => _.PortionName,
                        _ => _.Price,
                        _ => _.LocationPrice
                        );

                for (var i = 1; i <= 5; i++)
                {
                    sheet.AutoSizeColumn(i);
                }
            });
        }
    }
}
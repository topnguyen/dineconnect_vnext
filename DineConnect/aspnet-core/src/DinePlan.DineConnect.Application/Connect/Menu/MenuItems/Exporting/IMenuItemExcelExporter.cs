﻿using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Exporting
{
    public interface IMenuItemExcelExporter
    {
        FileDto ExportToFile(List<MenuItemListDto> items);

        FileDto ExportToFileForImportTemplate(List<MenuItemListDto> items);

        FileDto ExportPriceForLocation(List<MenuItemPortionDto> menuItemPortionDtos);
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Install.Dto;

namespace DinePlan.DineConnect.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Clique.CliqueScreenMenu.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;


namespace DinePlan.DineConnect.Clique.CliqueScreenMenu
{
    public class CliqueScreenMenuAppService : DineConnectAppServiceBase, ICliqueScreenMenuAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ScreenMenu> _screenMenuRepository;

        public CliqueScreenMenuAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<ScreenMenu> screenMenuRepository
        )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _screenMenuRepository = screenMenuRepository;
        }
        public async Task<PagedResultDto<CliqueScreenMenuDto>> GetAll(CliqueScreenMenuInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var entryQuery = _screenMenuRepository.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .Where(x => x.Type == ScreenMenuType.CliqueMenu)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                        x => EF.Functions.Like(x.Name, input.Filter.ToILikeString()));

                if (!string.IsNullOrWhiteSpace(input.LocationIds)) entryQuery = FilterByLocations(entryQuery, input);

                if (!string.IsNullOrWhiteSpace(input.Sorting)) entryQuery = entryQuery.OrderBy(input.Sorting);

                var sortEntries = await entryQuery
                    .PageBy(input)
                    .ToListAsync();

                var dtos = new List<CliqueScreenMenuDto>();

                foreach (var screenMenu in sortEntries)
                {
                    var screenMenuDto = ObjectMapper.Map<CliqueScreenMenuDto>(screenMenu);

                    if (screenMenu.Dynamic != null)
                    {
                        dynamic screenMenuDtoDynamic = JObject.Parse(screenMenu.Dynamic);

                        screenMenuDto.CategoryColumnCount = screenMenuDtoDynamic.CategoryColumnCount;

                        screenMenuDto.CategoryColumnWidthRate = screenMenuDtoDynamic.CategoryColumnWidthRate;
                    }

                    dtos.Add(screenMenuDto);
                }

                foreach (var ltd in dtos)
                    if (!string.IsNullOrEmpty(ltd.Locations))
                    {
                        if (!ltd.Group)
                        {
                            var myL = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(ltd.Locations);

                            if (myL.Any() && myL.Count() == 1) ltd.RefLocationId = Convert.ToInt32(myL[0].Id);
                        }
                        else
                        {
                            ltd.RefLocationId = 0;
                        }
                    }

                return new PagedResultDto<CliqueScreenMenuDto>(await entryQuery.CountAsync(), dtos);
            }
        }
    }
}

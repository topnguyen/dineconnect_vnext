﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using Abp.Timing.Timezone;
using Abp.UI;
using DinePlan.DineConnect.Clique.Payment.Dtos;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Core.Swipe;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;
using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.Payment
{
    public class CliquePaymentAppService : DineConnectAppServiceBase, ICliquePaymentAppService
    {
        private readonly IRepository<CliquePayment> _cliquePaymentRepository;
        private readonly IRepository<CliqueOrder> _cliqueOrderRepository;
        private readonly IRepository<CliqueOrderDetail> _cliqueOrderDetailRepository;
        private readonly IPaymentService _paymentService;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        private readonly IAdyenPaymentProcessor _adyenPaymentProcessor;
        private readonly IStripePaymentProcessor _stripePaymentProcessor;
        private readonly IPaypalPaymentProcessor _paypalPaymentProcessor;
        private readonly IGooglePaymentProcessor _googlePaymentProcessor;
        private readonly IRepository<SwipeTransactionDetail> _swipeTransactionDetailRepository;
        private readonly IRepository<SwipeCard> _swipeCardRepository;
        private readonly IRepository<CliqueMealTime> _cliqueMealTimeRepository;
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepository;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public CliquePaymentAppService(
            IRepository<SwipeCustomerCard> swipeCustomerCardRepository,
            IRepository<CliquePayment> cliquePaymentRepository,
            IRepository<CliqueOrder> cliqueOrderRepository,
            IPaymentService paymentService,
            IOmisePaymentProcessor omisePaymentProcessor,
            IAdyenPaymentProcessor adyenPaymentProcessor,
            IStripePaymentProcessor stripePaymentProcessor,
            IPaypalPaymentProcessor paypalPaymentProcessor,
            IGooglePaymentProcessor googlePaymentProcessor,
            IRepository<CliqueOrderDetail> cliqueOrderDetailRepository,
            IRepository<SwipeTransactionDetail> swipeTransactionDetailRepository,
            IRepository<SwipeCard> swipeCardRepository,
            IRepository<CliqueMealTime> cliqueMealTimeRepository,
            ITenantSettingsAppService tenantSettingsAppService,
            ITimeZoneConverter timeZoneConverter
        )
        {
            _cliquePaymentRepository = cliquePaymentRepository;
            _cliqueOrderRepository = cliqueOrderRepository;
            _paymentService = paymentService;
            _omisePaymentProcessor = omisePaymentProcessor;
            _adyenPaymentProcessor = adyenPaymentProcessor;
            _stripePaymentProcessor = stripePaymentProcessor;
            _paypalPaymentProcessor = paypalPaymentProcessor;
            _googlePaymentProcessor = googlePaymentProcessor;
            _cliqueOrderDetailRepository = cliqueOrderDetailRepository;
            _swipeTransactionDetailRepository = swipeTransactionDetailRepository;
            _swipeCardRepository = swipeCardRepository;
            _cliqueMealTimeRepository = cliqueMealTimeRepository;
            _swipeCustomerCardRepository = swipeCustomerCardRepository;
            _tenantSettingsAppService = tenantSettingsAppService;
            _timeZoneConverter = timeZoneConverter;
        }

        public async Task<CliquePaymentDto> CreatePaymentForOrder(CreateOrderCliquePaymentInput input)
        {
            var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == input.CustomerId && x.CardRefId == input.CardId && x.IsDeleted == false && x.Active == true).ToList();
            if (getCustomerCard != null && getCustomerCard.Count() > 0 && getCustomerCard[0].ExpiryDate >= DateTime.Now.Date)
            {

                bool paymentSuccess = false;
                CliquePaymentDto output = new CliquePaymentDto();
                // Omise
                if (input.PaymentOption == "omise" || input.PaymentOption == "paynow")
                {
                    OmisePaymentResponseDto omiseChargeResult;

                    if (input.PaymentOption == "omise")
                    {
                        omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                        {
                            OmiseToken = input.OmiseToken,
                            Amount = Convert.ToInt64(input.PaidAmount * 100),
                            Currency = DineConnectConsts.Currency,
                            ReturnUrl = input.CallbackUrl
                        });
                    }
                    else
                    {
                        omiseChargeResult = await _omisePaymentProcessor.GetStatus(input.OmiseToken);
                    }

                    if (omiseChargeResult != null)
                    {
                        //output.Message = omiseChargeResult.Message;
                        paymentSuccess = omiseChargeResult.IsSuccess;
                        //tiffPaymentRe.PaymentReference = output.OmiseId = omiseChargeResult.PaymentId;
                        //output.AuthenticateUri = omiseChargeResult.AuthorizeUri;
                    }
                }

                // Adyen
                if (input.PaymentOption == "adyen")
                {
                    var adyenChargeResult = await _adyenPaymentProcessor.ProcessPayment(new AdyenPaymentRequestDto
                    {
                        EncryptedCardNumber = input.AdyenEncryptedCardNumber,
                        EncryptedExpiryMonth = input.AdyenEncryptedExpiryMonth,
                        EncryptedExpiryYear = input.AdyenEncryptedExpiryYear,
                        EncryptedSecurityCode = input.AdyenEncryptedSecurityCode,
                        Amount = Convert.ToInt64(input.PaidAmount),
                        Currency = DineConnectConsts.Currency,
                        ReturnUrl = input.CallbackUrl
                    });

                    if (adyenChargeResult != null)
                    {
                        //output.Message = adyenChargeResult.Message;
                        paymentSuccess = adyenChargeResult.IsSuccess;
                        //tiffPaymentRe.PaymentReference = adyenChargeResult.PaymentId;
                    }
                }

                // Stripe
                if (input.PaymentOption == "stripe")
                {
                    var stripeChargeResult = await _stripePaymentProcessor.ProcessPayment(new StripePaymentRequestDto
                    {
                        StripeToken = input.StripeToken,
                        Amount = Convert.ToInt64(input.PaidAmount),
                        Currency = DineConnectConsts.Currency
                    });

                    if (stripeChargeResult != null)
                    {
                        //output.Message = stripeChargeResult.Message;
                        paymentSuccess = stripeChargeResult.IsSuccess;
                        //tiffPaymentRe.PaymentReference = stripeChargeResult.PaymentId;
                    }
                }

                // Paypal
                if (input.PaymentOption == "paypal")
                {
                    var paypalChargeResult = await _paypalPaymentProcessor.ProcessPayment(new PaypalPaymentRequestDto
                    {
                        OrderId = input.PaypalOrderId
                    });

                    if (paypalChargeResult != null)
                    {
                        //output.Message = paypalChargeResult.Message;
                        paymentSuccess = paypalChargeResult.IsSuccess;
                        //tiffPaymentRe.PaymentReference = paypalChargeResult.PaymentId;
                    }
                }

                // Google
                if (input.PaymentOption == "google")
                {
                    var googleChargeResult = await _googlePaymentProcessor.ProcessPayment(new GooglePaymentRequestDto
                    {
                        GooglePayToken = input.GooglePayToken,
                        Amount = Convert.ToInt64(input.PaidAmount),
                        Currency = DineConnectConsts.Currency
                    });

                    if (googleChargeResult != null)
                    {
                        //output.Message = googleChargeResult.Message;
                        paymentSuccess = googleChargeResult.IsSuccess;
                        //tiffPaymentRe.PaymentReference = googleChargeResult.PaymentId;
                    }
                }

                if (paymentSuccess || string.IsNullOrWhiteSpace(input.PaymentOption) || input.PaymentOption == "cash" || input.PaymentOption == "Stack Wallet")
                {
                    CliquePaymentInput inputData = new CliquePaymentInput();
                    inputData.PaymentMethod = input.PaymentOption;
                    inputData.Total = input.PaidAmount;
                    inputData.CliqueOrderId = input.CliqueOrderId;
                    inputData.CustomerId = input.CustomerId;
                    inputData.CardId = input.CardId;
                    output = await PaymentOrder(inputData);
                    return output;
                }
                else
                {
                    throw new InvalidOperationException("Payment failure.");
                }
            }
            else
            {
                throw new InvalidOperationException("Customer card not valid!");
            }
        }

        public async Task<CliquePaymentDto> PaymentOrder(CliquePaymentInput input)
        {
            List<CliquePayment> Payments = new List<CliquePayment>();
            var cliqueOrderList = _cliqueOrderRepository.GetAll().Where(x => x.Id == input.CliqueOrderId).ToList();
            if (cliqueOrderList != null && cliqueOrderList.Count() > 0)
            {
                var cliqueOrder = cliqueOrderList[0];
                CliquePaymentDto result = new CliquePaymentDto();
                Payments = _cliquePaymentRepository.GetAll().Where(x => x.IsDeleted == false && x.CliqueOrderId == input.CliqueOrderId).ToList();

                CliquePayment item = new CliquePayment();
                item.CreationTime = DateTime.Now;
                item.CreatorUserId = input.CustomerId;
                item.PaymentMethod = input.PaymentMethod;
                item.Total = input.Total;
                item.PaidTime = DateTime.Now;
                item.IsDeleted = false;
                Payments.Add(item);

                result.Total = item.Total;
                result.PaymentMethod = item.PaymentMethod;
                result.PaidTime = item.PaidTime;
                result.CliqueOrderId = input.CliqueOrderId;
                result.DatePaidTimeStr = item.PaidTime.ToString("dd MMM, yyyy");
                result.TimePaidTimeStr = item.PaidTime.ToString("HH:mm");


                cliqueOrder.Payments = Payments;
                cliqueOrder.LastModificationTime = DateTime.Now;
                cliqueOrder.LastModifierUserId = input.CustomerId;

                List<CliqueOrderDetail> OrderDetails = _cliqueOrderDetailRepository.GetAll().Where(x => x.IsDeleted == false && x.CliqueOrderId == input.CliqueOrderId).ToList();
                foreach (var itemOrder in OrderDetails)
                {
                    itemOrder.CliqueOrderStatus = CliqueOrderStatus.Prepared;
                    itemOrder.LastModificationTime = DateTime.Now;
                    itemOrder.LastModifierUserId = input.CustomerId;
                }
                cliqueOrder.OrderDetails = OrderDetails;

                var cliqueOrderDetailMax = OrderDetails.OrderByDescending(x => x.OrderDate).FirstOrDefault();

                await _cliqueOrderRepository.UpdateAsync(cliqueOrder);

                var getCard = _swipeCardRepository.GetAll().Where(x => x.Id == input.CardId).ToList();
                if (getCard != null && getCard.Count() > 0)
                {
                    var card = getCard[0];
                    card.Balance = card.Balance - input.Total;
                    card.LastModificationTime = DateTime.Now;
                    card.LastModifierUserId = input.CustomerId;
                    await _swipeCardRepository.UpdateAsync(card);

                    SwipeTransactionDetail transaction = new SwipeTransactionDetail();
                    transaction.CreationTime = DateTime.Now;
                    transaction.CreatorUserId = input.CustomerId;
                    transaction.TransactionTime = DateTime.Now;
                    transaction.TransactionType = (int)TransactionType.Payment;
                    transaction.CustomerId = input.CustomerId;
                    transaction.CardRefId = input.CardId;
                    transaction.Debit = input.Total;
                    transaction.Credit = 0;
                    transaction.PaymentType = "topup";
                    transaction.PaymentTransactionDetails = JsonConvert.SerializeObject(result);
                    var cliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == cliqueOrderDetailMax.CliqueMealTimeId).ToList();
                    if (cliqueMealTime != null && cliqueMealTime.Count() > 0)
                    {
                        transaction.Description = cliqueMealTime[0].Name + " menu";
                    }

                    await _swipeTransactionDetailRepository.InsertAsync(transaction);
                }


                return result;
            }
            else
            {
                throw new Exception("Order not found!");
            }
        }

        public async Task<ListResultDto<CliquePaymentMethodDto>> GetPaymentMethod(CliquePaymentMethodInput input)
        {
            var paymentProcessors = await _paymentService.GetAllPaymentProcessor();

            var listPaymentProcessors = new List<CliquePaymentMethodDto>();

            foreach (var item in paymentProcessors)
            {
                var data = await item.GetSetting();

                listPaymentProcessors.Add(ObjectMapper.Map<CliquePaymentMethodDto>(data));
            }

            listPaymentProcessors = listPaymentProcessors
                .WhereIf(!string.IsNullOrEmpty(input.Filter), x => x.SystemName.ToLower().Contains(input.Filter.ToLower()) || x.FriendlyName.ToLower().Contains(input.Filter.ToLower()))
                .WhereIf(!string.IsNullOrEmpty(input.SystemNameFilter), x => x.SystemName.ToLower().Contains(input.SystemNameFilter.ToLower()))
                .WhereIf(!string.IsNullOrEmpty(input.FriendlyNameFilter), x => x.FriendlyName.ToLower().Contains(input.FriendlyNameFilter.ToLower()))
                .WhereIf(input.SupportsCaptureFilter > -1, x => x.SupportsCapture == (input.SupportsCaptureFilter == 1))
                .WhereIf(input.RefundFilter > -1, x => x.Refund == (input.RefundFilter == 1))
                .WhereIf(input.PartialRefundFilter > -1, x => x.PartialRefund == (input.PartialRefundFilter == 1))
                .WhereIf(input.VoidFilter > -1, x => x.Void == (input.VoidFilter == 1))
                .WhereIf(input.ActiveFilter > -1, x => x.Active == (input.ActiveFilter == 1))
                .WhereIf(input.RecurringSupportFilter.HasValue, x => x.RecurringSupport == input.RecurringSupportFilter)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .AsQueryable()
                .PageBy(input)
                .ToList();

            return new ListResultDto<CliquePaymentMethodDto>(listPaymentProcessors);
        }

        public async Task ValidateCutOff(List<DateTime> orderDate)
        {
            var tenantSettings = await _tenantSettingsAppService.GetAllSettings();

            var allowOrderInFutureDate = Clock.Now.Date.AddDays(tenantSettings.CliqueAllowOrderInFutureDays);

            foreach (var dateTime in orderDate)
            {
                //if (dateTime.DayOfWeek == DayOfWeek.Sunday)
                //{
                //    throw new UserFriendlyException("Oops... Sunday, We do not accept Order");
                //}

                var curtOffTime = await GetCutOffProceed(dateTime);

                if (!curtOffTime)
                {
                    throw new UserFriendlyException($"Oops... Your requested date {dateTime.Date:d} error - Cut Off Time already Passed");
                }

                //if (tenantSettings.CliqueAllowOrderInFutureDays >= 0)
                //{
                //    if (dateTime > allowOrderInFutureDate)
                //    {
                //        throw new UserFriendlyException($"Cannot order for the date {dateTime:d} because it so far out");
                //    }
                //}
            }
        }

        private async Task<bool> GetCutOffProceed(DateTime orderDate)
        {
            DateTime? orderCutOffTime = await GetCutOffTime();
            if (orderCutOffTime.HasValue)
            {
                var nowDateTime = _timeZoneConverter.Convert(Clock.Now, AbpSession.TenantId.Value);
                var nowTime = nowDateTime.Value.TimeOfDay;
                var orderTime = orderDate.Date.Add(nowTime);
                if (orderTime.Date <= orderCutOffTime.Value.Date)
                {
                    return false;
                }
                var tomorrowDate = nowDateTime.Value.Date.AddDays(1);
                if (orderTime.Date == tomorrowDate)
                {
                    if (nowTime < orderCutOffTime.Value.TimeOfDay)
                    {
                        return true;
                    }
                    return false;
                }
            }
            return true;
        }

        private async Task<DateTime?> GetCutOffTime()
        {
            if (AbpSession.TenantId != null)
            {
                var cutOffTime = await SettingManager.GetSettingValueForTenantAsync(
                    AppSettings.CliqueMealSetting.CliqueOrderCutOffTime,
                    (int)AbpSession.TenantId);
                DateTime? orderCutOffTime = null;
                if (!cutOffTime.IsNullOrEmpty() && cutOffTime != "null")
                {
                    var orderCutOffTimeString = JsonConvert.DeserializeObject<string>(cutOffTime);
                    orderCutOffTime = DateTime.Parse(orderCutOffTimeString);
                    orderCutOffTime = _timeZoneConverter.Convert(orderCutOffTime, AbpSession.TenantId.Value);
                }
                if (orderCutOffTime.HasValue)
                {
                    var tenantSettings = await _tenantSettingsAppService.GetAllSettings();
                    var cutoffDate = Clock.Now;
                    cutoffDate = _timeZoneConverter.Convert(cutoffDate, AbpSession.TenantId.Value).Value;
                    cutoffDate = cutoffDate.AddDays(tenantSettings.CliqueCutOffDays).Date;
                    var cutoffTime = orderCutOffTime.Value.TimeOfDay;
                    return cutoffDate + cutoffTime;
                }
            }
            return null;
        }

        private enum TransactionType
        {
            Toptup = 0,
            Transfer = 1,
            Payment = 2
        }
    }
}

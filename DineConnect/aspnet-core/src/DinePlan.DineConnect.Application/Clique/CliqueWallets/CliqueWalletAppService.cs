﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Clique.CliqueWallets.Dtos;
using DinePlan.DineConnect.Core.Swipe;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Timing;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.PaymentProcessor;

namespace DinePlan.DineConnect.Clique.CliqueWallets
{
    public class CliqueWalletAppService : DineConnectAppServiceBase, ICliqueWalletAppService
    {
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepository;
        private readonly IRepository<SwipeCard> _swipeCardRepository;
        private readonly IRepository<SwipeTransactionDetail> _swipeTransactionDetailRepository;
        private readonly IRepository<SwipeCardType> _swipeCardTypeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        public CliqueWalletAppService(
            IRepository<SwipeCustomerCard> swipeCustomerCardRepository,
            IRepository<SwipeCard> swipeCardRepository,
            IRepository<SwipeTransactionDetail> swipeTransactionDetailRepository,
            IOmisePaymentProcessor omisePaymentProcessor,
            IRepository<Customer> customerRepository,
            IRepository<SwipeCardType> swipeCardTypeRepository
        )
        {
            _swipeCustomerCardRepository = swipeCustomerCardRepository;
            _swipeCardRepository = swipeCardRepository;
            _swipeTransactionDetailRepository = swipeTransactionDetailRepository;
            _customerRepository = customerRepository;
            _omisePaymentProcessor = omisePaymentProcessor;
            _swipeCardTypeRepository = swipeCardTypeRepository;
        }

        public CustomerWalletDto GetCustomerWallet(int customerId)
        {
            try
            {
                CustomerWalletDto data = new CustomerWalletDto();
                var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == customerId && x.IsDeleted == false && x.Active == true).ToList();
                if (getCustomerCard != null && getCustomerCard.Count() > 0)
                {
                    var getSwipeCard = _swipeCardRepository.GetAll().Where(x => x.Id == getCustomerCard[0].CardRefId && x.IsDeleted == false && x.Active == true).ToList();
                    if (getSwipeCard != null && getSwipeCard.Count() > 0)
                    {
                        data.CardId = getSwipeCard[0].Id;
                        data.CustomerId = customerId;
                        data.Balance = getSwipeCard[0].Balance;
                        data.CardNumber = getSwipeCard[0].CardNumber;
                    }
                }
                return data;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public List<CustomerTransactionDto> GetCustomerTransaction(int customerId)
        {
            try
            {
                List<CustomerTransactionDto> data = new List<CustomerTransactionDto>();
                var getTransaction = _swipeTransactionDetailRepository.GetAll().Where(x => x.CustomerId == customerId && x.IsDeleted == false).ToList();
                if (getTransaction != null && getTransaction.Count() > 0)
                {
                    getTransaction = getTransaction.OrderByDescending(x => x.CreationTime).ToList();
                    foreach (var item in getTransaction)
                    {
                        CustomerTransactionDto itemTransaction = new CustomerTransactionDto();
                        itemTransaction.TransactionId = item.Id;
                        itemTransaction.TransactionType = item.TransactionType;
                        itemTransaction.Description = item.Description;
                        itemTransaction.TransactionTime = item.TransactionTime.ToString("dd-MM-yyyy");
                        itemTransaction.Credit = item.Credit;
                        itemTransaction.Debit = item.Debit;
                        itemTransaction.TransactionStatus = 1;

                        data.Add(itemTransaction);
                    }
                }
                return data;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task ToptupRecharge(CustomerTopupInput input)
        {
            bool paymentSuccess = false;
            string errorMessage = "";
            string omiseId = "";
            string authenticateUri = "";
            // Omise
            if (input.PaymentType == "omise")
            {
                OmisePaymentResponseDto omiseChargeResult;

                omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                {
                    OmiseToken = input.OmiseToken,
                    Amount = Convert.ToInt64(input.Money * 100),
                    Currency = DineConnectConsts.Currency,
                    ReturnUrl = input.CallbackUrl
                });

                if (omiseChargeResult != null)
                {
                    errorMessage = omiseChargeResult.Message;
                    paymentSuccess = omiseChargeResult.IsSuccess;
                    omiseId = omiseChargeResult.PaymentId;
                    authenticateUri = omiseChargeResult.AuthorizeUri;
                }
            }

            if (paymentSuccess)
            {
                var getCard = _swipeCardRepository.GetAll().Where(x => x.Id == input.CardId).ToList();
                if (getCard != null && getCard.Count() > 0)
                {
                    var card = getCard[0];
                    card.Balance = card.Balance + input.Money;
                    card.LastModificationTime = DateTime.Now;
                    card.LastModifierUserId = input.CustomerId;
                    await _swipeCardRepository.UpdateAsync(card);
                }
                else
                {
                    int swipeCardTypeId = 0;
                    int dayExpiry = 100;
                    var getSwipeCardType = _swipeCardTypeRepository.GetAll().Where(x => x.IsDeleted == false).ToList();
                    if (getSwipeCardType != null && getSwipeCardType.Count() > 0)
                    {
                        swipeCardTypeId = getSwipeCardType[0].Id;
                        if (getSwipeCardType[0].ExpiryDaysFromIssue.HasValue)
                        {
                            dayExpiry = getSwipeCardType[0].ExpiryDaysFromIssue.Value;
                        }
                    }
                    else
                    {
                        var swipecardType = new SwipeCardType();
                        swipecardType.Name = "Stack Canteen";
                        swipecardType.DepositRequired = false;
                        swipecardType.DepositAmount = 10;
                        swipecardType.ExpiryDaysFromIssue = dayExpiry;
                        swipecardType.RefundAllowed = false;
                        swipecardType.CustomerRequired = true;
                        swipecardType.MinimumTopUpAmount = 100;
                        swipecardType.CreationTime = DateTime.Now;
                        swipecardType.CreatorUserId = input.CustomerId;
                        swipeCardTypeId = await _swipeCardTypeRepository.InsertAndGetIdAsync(swipecardType);
                    }
                    if(swipeCardTypeId > 0)
                    {
                        var card = new SwipeCard();
                        card.CardTypeRefId = swipeCardTypeId;
                        card.Balance = input.Money;
                        card.CreationTime = DateTime.Now;
                        card.CreatorUserId = input.CustomerId;
                        card.Active = true;
                        card.TenantId = AbpSession.TenantId.Value;
                        input.CardId = await _swipeCardRepository.InsertAndGetIdAsync(card);

                        var customerCard = new SwipeCustomerCard
                        {
                            Active = true,
                            CardRefId = input.CardId,
                            CustomerId = input.CustomerId,
                            IssueDate = Clock.Now,
                            DepositAmount = input.Money
                        };
                        customerCard.ExpiryDate = Clock.Now.AddDays(dayExpiry);

                        await _swipeCustomerCardRepository.InsertAsync(customerCard);
                    }
                    else
                    {
                        throw new InvalidOperationException(L("Swipe card type not found"));
                    }
                }

                if (input.CardId > 0)
                {
                    SwipeTransactionDetail transaction = new SwipeTransactionDetail();
                    transaction.CreationTime = DateTime.Now;
                    transaction.CreatorUserId = input.CustomerId;
                    transaction.TransactionTime = DateTime.Now;
                    transaction.TransactionType = (int)TransactionType.Toptup;
                    transaction.CustomerId = input.CustomerId;
                    transaction.CardRefId = input.CardId;
                    transaction.Credit = input.Money;
                    transaction.Debit = 0;
                    transaction.PaymentType = input.PaymentType;
                    dynamic dynamic = new
                    {
                        errorMessage = errorMessage,
                        paymentSuccess = paymentSuccess,
                        omiseId = omiseId,
                        authenticateUri = authenticateUri
                    };
                    transaction.PaymentTransactionDetails = JsonConvert.SerializeObject(dynamic);
                    transaction.Description = "From bank transfer";

                    await _swipeTransactionDetailRepository.InsertAsync(transaction);
                }
                else
                {
                    throw new InvalidOperationException(L("SwipeCardNotFound"));
                }
            }
            else
            {
                throw new InvalidOperationException("Topup fail, please check your payment information and try again! " + errorMessage);
            }
        }

        public async Task CustomerTransferTopup(CustomerTransferInput input)
        {
            var getFromCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == input.FromCustomerId && x.CardRefId == input.CardId && x.IsDeleted == false && x.Active == true).ToList();
            if (getFromCustomerCard != null && getFromCustomerCard.Count() > 0 && getFromCustomerCard[0].ExpiryDate >= DateTime.Now.Date)
            {
                var getCard = _swipeCardRepository.GetAll().Where(x => x.Id == input.CardId).ToList();
                if (getCard != null && getCard.Count() > 0)
                {
                    var card = getCard[0];
                    if (card.Balance < input.Money)
                    {
                        throw new UserFriendlyException("The amount transferred is greater than the balance in your wallet. Please check again.");
                    }
                    else
                    {
                        card.Balance = card.Balance - input.Money;
                        card.LastModificationTime = DateTime.Now;
                        card.LastModifierUserId = input.FromCustomerId;
                        await _swipeCardRepository.UpdateAsync(card);

                        SwipeTransactionDetail transaction = new SwipeTransactionDetail();
                        transaction.CreationTime = DateTime.Now;
                        transaction.CreatorUserId = input.FromCustomerId;
                        transaction.TransactionTime = DateTime.Now;
                        transaction.TransactionType = (int)TransactionType.Transfer;
                        transaction.CustomerId = input.FromCustomerId;
                        transaction.CardRefId = input.CardId;
                        transaction.Debit = input.Money;
                        transaction.Credit = 0;
                        transaction.PaymentType = "topup";
                        dynamic dynamic = new
                        {
                            ToCustomerId = input.ToCustomerId,
                            ToCustomerName = input.ToCustomerName
                        };
                        transaction.PaymentTransactionDetails = JsonConvert.SerializeObject(dynamic);
                        transaction.Description = "To " + input.ToCustomerName;

                        await _swipeTransactionDetailRepository.InsertAsync(transaction);

                        var getCardTo = _swipeCardRepository.GetAll().Where(x => x.Id == input.ToCustomerCardId).ToList();
                        if (getCardTo != null && getCardTo.Count() > 0)
                        {
                            var cardTo = getCardTo[0];
                            cardTo.Balance = cardTo.Balance + input.Money;
                            cardTo.LastModificationTime = DateTime.Now;
                            cardTo.LastModifierUserId = input.FromCustomerId;
                            await _swipeCardRepository.UpdateAsync(cardTo);

                            SwipeTransactionDetail transactionTo = new SwipeTransactionDetail();
                            transactionTo.CreationTime = DateTime.Now;
                            transactionTo.CreatorUserId = input.FromCustomerId;
                            transactionTo.TransactionTime = DateTime.Now;
                            transactionTo.TransactionType = (int)TransactionType.Transfer;
                            transactionTo.CustomerId = input.ToCustomerId;
                            transactionTo.CardRefId = input.ToCustomerCardId;
                            transactionTo.Credit = input.Money;
                            transactionTo.Debit = 0;
                            transactionTo.PaymentType = "topup";
                            var getCustomer = _customerRepository.GetAll().Where(x => x.Id == input.FromCustomerId).ToList();
                            if (getCustomer != null && getCustomer.Count() > 0)
                            {
                                input.FromCustomerName = getCustomer[0].Name;
                            }
                            dynamic dynamicTo = new
                            {
                                FromCustomerId = input.FromCustomerId,
                                FromCustomerName = input.FromCustomerName
                            };
                            transactionTo.PaymentTransactionDetails = JsonConvert.SerializeObject(dynamicTo);
                            transactionTo.Description = "From " + input.FromCustomerName;

                            await _swipeTransactionDetailRepository.InsertAsync(transactionTo);
                        }
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("Customer card not valid!");
            }
        }

        public List<CustomerForTransferDto> GetCustomerForTranfer(string filter)
        {
            try
            {
                List<CustomerForTransferDto> data = new List<CustomerForTransferDto>();

                var listCustomer = _customerRepository.GetAll().Where(x => x.IsDeleted == false && x.UserId != AbpSession.UserId).ToList();
                if(listCustomer != null && listCustomer.Count() > 0)
                {
                    if (!string.IsNullOrEmpty(filter))
                    {
                        var listCustomerTemp = new List<Customer>();
                        foreach (var customerItem in listCustomer)
                        {
                            if (!string.IsNullOrEmpty(customerItem.PhoneNumber))
                            {
                                if (customerItem.PhoneNumber.Contains(filter))
                                {
                                    listCustomerTemp.Add(customerItem);
                                    continue;
                                }
                            }
                            if (!string.IsNullOrEmpty(customerItem.EmailAddress))
                            {
                                if (customerItem.EmailAddress.Contains(filter))
                                {
                                    listCustomerTemp.Add(customerItem);
                                    continue;
                                }
                            }
                            if (!string.IsNullOrEmpty(customerItem.Name))
                            {
                                if (customerItem.Name.Contains(filter))
                                {
                                    listCustomerTemp.Add(customerItem);
                                    continue;
                                }
                            }
                        }
                        listCustomer = listCustomerTemp;
                    }

                    foreach (var item in listCustomer)
                    {
                        var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == item.Id && x.IsDeleted == false && x.Active == true).ToList();
                        if (getCustomerCard != null && getCustomerCard.Count() > 0)
                        {
                            var card = getCustomerCard[0];
                            CustomerForTransferDto itemTransfer = new CustomerForTransferDto();
                            itemTransfer.CustomerId = card.CustomerId;
                            itemTransfer.CustomerName = item.Name;
                            itemTransfer.PhoneNumber = item.PhoneNumber;
                            itemTransfer.CustomerCardId = card.Id;

                            data.Add(itemTransfer);
                        }
                    }
                }

                return data;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public PagedResultDto<CustomerTranferListDto> GetCustomerTransactionAdmin(CustomerTransactionListInput input)
        {
            List<CustomerTranferListDto> result = new List<CustomerTranferListDto>();
            int count = 0;
            DateTime? nullDateTime = null;
            var listTransaction = _swipeTransactionDetailRepository.GetAll()
                .Include(e => e.SwipeCard)
                .Include(e => e.Customer)
                .WhereIf(input.fromDate.HasValue, x => x.TransactionTime.Date >= input.fromDate)
                .WhereIf(input.toDate.HasValue, x => x.TransactionTime.Date <= input.toDate)
                .WhereIf(input.customerId != null, e => e.Customer.Id.Equals(input.customerId))
                .WhereIf(!input.paymentMethod.IsNullOrEmpty(), e => EF.Functions.Like(e.PaymentType, input.paymentMethod.ToILikeString()))
                .Select(e => new CustomerTranferListDto
                {
                    TransactionId = e.Id,
                    CustomerName = e.Customer.Name,
                    CustomerEmail = e.Customer.EmailAddress,
                    TransactionTime = e.TransactionTime,
                    Description = e.Description,
                    Credit = e.Credit,
                    Debit = e.Debit,
                    ExpiryDate = nullDateTime,
                    PaymentType = e.PaymentType,
                    TransactionType = e.TransactionType,
                    TransactionTypeName = "",
                    CardId = e.CardRefId
                }).ToList();
            if (listTransaction != null && listTransaction.Count() > 0)
            {
                foreach (var item in listTransaction)
                {
                    bool checkAdd = true;
                    var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.Id == item.CardId).ToList();
                    if (getCustomerCard != null && getCustomerCard.Count() > 0)
                    {
                        DateTime? expiryDate = getCustomerCard[0].ExpiryDate;
                        if (expiryDate.HasValue)
                        {
                            item.ExpiryDate = expiryDate;
                            if (input.dayExpiry.HasValue)
                            {
                                checkAdd = false;
                                if (expiryDate.Value.Date >= Clock.Now.Date.ToDayEnd() && expiryDate.Value.Date <= Clock.Now.Date.AddDays(input.dayExpiry.Value).ToDayEnd())
                                {
                                    checkAdd = true;
                                }
                            }
                        }
                    }
                    if (checkAdd)
                    {
                        if (item.TransactionType == (int)TransactionType.Toptup)
                        {
                            item.TransactionTypeName = "Top Up";
                        }
                        else if (item.TransactionType == (int)TransactionType.Transfer)
                        {
                            item.TransactionTypeName = "Transfer";
                        }
                        else if (item.TransactionType == (int)TransactionType.Payment)
                        {
                            item.TransactionTypeName = "Payment";
                        }
                        result.Add(item);
                    }
                }
            }
            if (result != null && result.Count() > 0)
            {
                count = result.Count();
                result = result.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            }
            return new PagedResultDto<CustomerTranferListDto>(
                count,
                result
            );
        }

        private enum TransactionType
        {
            Toptup = 0,
            Transfer = 1,
            Payment = 2
        }
    }
}

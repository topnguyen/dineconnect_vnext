﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Clique.CliqueMealTimes.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.Extension;
using System.Linq.Dynamic.Core;
using Abp.Collections.Extensions;
using Abp.UI;

namespace DinePlan.DineConnect.Clique.CliqueMealTimes
{
    public class CliqueMealTimeAppService : DineConnectAppServiceBase, ICliqueMealTimeAppService
    {
        private readonly IRepository<CliqueMealTime> _cliqueMealTimeRepository;
        public CliqueMealTimeAppService(
            IRepository<CliqueMealTime> cliqueMealTimeRepository
        )
        {
            _cliqueMealTimeRepository = cliqueMealTimeRepository;
        }

        public async Task<ListResultDto<CliqueMealTimeListDto>> GetCliqueMealTime(CliqueMealTimeInput input)
        {
            var mealTimes = _cliqueMealTimeRepository.GetAll()
                .Where(x => x.IsDeleted == false)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    c => EF.Functions.Like(c.Name, input.Filter.ToILikeString())
                         || EF.Functions.Like(c.Code, input.Filter.ToILikeString()));

            var sortedMenuItems = await mealTimes
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var mealTimeListDtos = ObjectMapper.Map<List<CliqueMealTimeListDto>>(sortedMenuItems);
            return new PagedResultDto<CliqueMealTimeListDto>(
                await mealTimes.CountAsync(),
                mealTimeListDtos
            );
        }

        public CliqueMealTimeListDto GetCliqueMealTimeById(int cliqueMealTimeId)
        {
            CliqueMealTimeListDto mealTimeListDtos = new CliqueMealTimeListDto();
            var mealTimes = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == cliqueMealTimeId).ToList();
            if(mealTimes != null && mealTimes.Count() > 0)
            {
                mealTimeListDtos = ObjectMapper.Map<CliqueMealTimeListDto>(mealTimes[0]);
            }
            return mealTimeListDtos;
        }

        public async Task CreateOrUpdateCliqueMealTime(CreateOrUpdateCliqueMealTimeInput input)
        {
            if (_cliqueMealTimeRepository.GetAll().Where(x => x.Name.ToLower() == input.Name.ToLower() && x.IsDeleted == false && x.Id != input.Id).Count() > 0)
                throw new UserFriendlyException(
                    $"Clique Meal Time with name {input.Name} is already added, please check input.");

            if (_cliqueMealTimeRepository.GetAll().Where(x => x.Code.ToLower() == input.Code.ToLower() && x.IsDeleted == false && x.Id != input.Id).Count() > 0)
                throw new UserFriendlyException(
                    $"Clique Meal Time with code {input.Code} is already added, please check input.");

            if (input.Id > 0)
            {
                await UpdateCliqueMealTime(input);
            }
            else
            {
                await CreateCliqueMealTime(input);
            }
        }

        protected virtual async Task CreateCliqueMealTime(CreateOrUpdateCliqueMealTimeInput input)
        {
            try
            {
                CliqueMealTime item = new CliqueMealTime();
                item.CreationTime = DateTime.Now;
                item.IsDeleted = false;
                item.Name = input.Name;
                item.Code = input.Code;

                await _cliqueMealTimeRepository.InsertAsync(item);
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        protected virtual async Task UpdateCliqueMealTime(CreateOrUpdateCliqueMealTimeInput input)
        {
            try
            {
                var items = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == input.Id).ToList();
                if(items != null && items.Count() > 0)
                {
                    var item = items[0];
                    item.LastModificationTime = DateTime.Now;
                    item.Name = input.Name;
                    item.Code = input.Code;

                    await _cliqueMealTimeRepository.UpdateAsync(item);
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task DeleteCliqueMealTime(int cliqueMealTimeId)
        {
            await _cliqueMealTimeRepository.DeleteAsync(x => x.Id == cliqueMealTimeId);
        }

        public async Task<List<CliqueMealTimeListDto>> GetCliqueMealTimeDropdown()
        {
            try
            {
                var data = await _cliqueMealTimeRepository.GetAll()
                    .Where(x => x.IsDeleted == false)
                    .Select(x => new CliqueMealTimeListDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code,
                        SortOrder = x.SortOrder,
                        CreationTime = x.CreationTime
                    })
                    .ToListAsync();
                return data;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }
    }
}

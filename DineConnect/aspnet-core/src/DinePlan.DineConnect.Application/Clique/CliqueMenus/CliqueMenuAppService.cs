﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Clique.CliqueMealTimes.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.Extension;
using System.Linq.Dynamic.Core;
using Abp.Collections.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Clique.CliqueMenus.Dtos;
using DinePlan.DineConnect.Tiffins.Schedule;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Clique.CliqueMenus
{
    public class CliqueMenuAppService : DineConnectAppServiceBase, ICliqueMenuAppService
    {
        private readonly IRepository<CliqueMenu> _cliqueMenuRepository;
        private readonly IRepository<ScreenMenu> _screenMenuRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<CliqueMealTime> _cliqueMealTimeRepository;
        public CliqueMenuAppService(
            IRepository<CliqueMenu> cliqueMenuRepository,
            IRepository<ScreenMenu> screenMenuRepository,
            IRepository<Location> locationRepository,
            IRepository<CliqueMealTime> cliqueMealTimeRepository
        )
        {
            _cliqueMenuRepository = cliqueMenuRepository;
            _screenMenuRepository = screenMenuRepository;
            _locationRepository = locationRepository;
            _cliqueMealTimeRepository = cliqueMealTimeRepository;
        }

        public async Task<ListResultDto<CliqueMenuDto>> GetCliqueMenu(CliqueMenuInput input)
        {
            var cliqueMenus = _cliqueMenuRepository.GetAll().Where(x => x.IsDeleted == false);

            if (input.FromDate.HasValue && input.FixedMenu.HasValue && !input.FixedMenu.Value)
            {
                cliqueMenus = cliqueMenus.Where(x => x.FromDate >= input.FromDate.Value);
            }

            if (input.ToDate.HasValue && input.FixedMenu.HasValue && !input.FixedMenu.Value)
            {
                cliqueMenus = cliqueMenus.Where(x => x.FromDate <= input.ToDate.Value);
            }

            if (input.FixedMenu.HasValue)
            {
                cliqueMenus = cliqueMenus.Where(x => x.FixedMenu == input.FixedMenu.Value);
            }

            if (input.Status.HasValue)
            {
                if(input.Status.Value == (int)ScheduleDetailStatus.Published)
                {
                    cliqueMenus = cliqueMenus.Where(x => x.PublishStatus == ScheduleDetailStatus.Published);
                }
                else if (input.Status.Value == (int)ScheduleDetailStatus.Draft)
                {
                    cliqueMenus = cliqueMenus.Where(x => x.PublishStatus == ScheduleDetailStatus.Draft);
                }
            }

            var sortedMenuItems = await cliqueMenus
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var cliqueMenuListDtos = new List<CliqueMenuDto>();
            foreach (var item in sortedMenuItems)
            {
                CliqueMenuDto itemMenu = new CliqueMenuDto();
                itemMenu.Id = item.Id;
                itemMenu.FixedMenu = item.FixedMenu;
                itemMenu.AllDay = item.AllDay;
                itemMenu.Days = item.Days;

                if (!string.IsNullOrEmpty(itemMenu.Days))
                {
                    var listDay = itemMenu.Days.Split(",");
                    foreach (var itemDay in listDay)
                    {
                        if (itemDay == "Monday")
                        {
                            itemMenu.Monday = true;
                        }
                        if (itemDay == "Tuesday")
                        {
                            itemMenu.Tuesday = true;
                        }
                        if (itemDay == "Wednesday")
                        {
                            itemMenu.Wednesday = true;
                        }
                        if (itemDay == "Thursday")
                        {
                            itemMenu.Thursday = true;
                        }
                        if (itemDay == "Friday")
                        {
                            itemMenu.Friday = true;
                        }
                        if (itemDay == "Saturday")
                        {
                            itemMenu.Saturday = true;
                        }
                        if (itemDay == "Sunday")
                        {
                            itemMenu.Sunday = true;
                        }
                    }
                }

                var getScreenMenu = _screenMenuRepository.GetAll().Where(x => x.Id == item.ScreenMenuId).ToList();
                if (getScreenMenu != null && getScreenMenu.Count() > 0)
                {
                    itemMenu.ScreenMenuName = getScreenMenu[0].Name;
                    itemMenu.ScreenMenuId = item.ScreenMenuId;
                }

                itemMenu.FromDate = item.FromDate;
                itemMenu.ToDate = item.ToDate;
                itemMenu.CutOffDate = item.CutOffDate;
                itemMenu.PublishStatus = (int)item.PublishStatus;

                var getLocation = _locationRepository.GetAll().Where(x => x.Id == item.LocationId).ToList();
                if (getLocation != null && getLocation.Count() > 0)
                {
                    itemMenu.LocationName = getLocation[0].Name;
                    itemMenu.LocationId = item.LocationId;
                }

                var getCliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == item.CliqueMealTimeId).ToList();
                if (getCliqueMealTime != null && getCliqueMealTime.Count() > 0)
                {
                    itemMenu.CliqueMealTimeName = getCliqueMealTime[0].Name;
                    itemMenu.CliqueMealTimeId = item.CliqueMealTimeId;
                }

                cliqueMenuListDtos.Add(itemMenu);
            }

            return new PagedResultDto<CliqueMenuDto>(
                await cliqueMenus.CountAsync(),
                cliqueMenuListDtos
            );
        }

        public CliqueMenuDto GetCliqueMenuById(int cliqueMenuId)
        {
            CliqueMenuDto itemMenu = new CliqueMenuDto();
            var menuItemList = _cliqueMenuRepository.GetAll().Where(x => x.Id == cliqueMenuId).ToList();
            if(menuItemList != null && menuItemList.Count() > 0)
            {
                var menuItems = menuItemList[0];
                itemMenu.Id = menuItems.Id;
                itemMenu.FixedMenu = menuItems.FixedMenu;
                itemMenu.AllDay = menuItems.AllDay;
                itemMenu.Days = menuItems.Days;

                var getScreenMenu = _screenMenuRepository.GetAll().Where(x => x.Id == menuItems.ScreenMenuId).ToList();
                if (getScreenMenu != null && getScreenMenu.Count() > 0)
                {
                    itemMenu.ScreenMenuName = getScreenMenu[0].Name;
                    itemMenu.ScreenMenuId = menuItems.ScreenMenuId;
                }

                itemMenu.FromDate = menuItems.FromDate;
                itemMenu.ToDate = menuItems.ToDate;
                itemMenu.CutOffDate = menuItems.CutOffDate;
                itemMenu.PublishStatus = (int)menuItems.PublishStatus;

                var getLocation = _locationRepository.GetAll().Where(x => x.Id == menuItems.LocationId).ToList();
                if (getLocation != null && getLocation.Count() > 0)
                {
                    itemMenu.LocationName = getLocation[0].Name;
                    itemMenu.LocationId = menuItems.LocationId;
                }

                var getCliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == menuItems.CliqueMealTimeId).ToList();
                if (getCliqueMealTime != null && getCliqueMealTime.Count() > 0)
                {
                    itemMenu.CliqueMealTimeName = getCliqueMealTime[0].Name;
                    itemMenu.CliqueMealTimeId = menuItems.CliqueMealTimeId;
                }
            }

            return itemMenu;
        }

        public async Task CreateOrUpdateCliqueMenu(CreateOrUpdateCliqueMenuInput input)
        {
            if (input.Id > 0)
            {
                await UpdateCliqueMenu(input);
            }
            else
            {
                await CreateCliqueMenu(input);
            }
        }

        protected virtual async Task CreateCliqueMenu(CreateOrUpdateCliqueMenuInput input)
        {
            try
            {
                CliqueMenu item = new CliqueMenu();
                item.CreationTime = DateTime.Now;
                item.IsDeleted = false;
                item.FixedMenu = input.FixedMenu;
                item.AllDay = input.AllDay;
                item.Days = input.Days;
                item.ScreenMenuId = input.ScreenMenuId;
                item.FromDate = input.FromDate;
                item.ToDate = input.ToDate;
                item.CutOffDate = input.CutOffDate;
                item.LocationId = input.LocationId;
                if (input.PublishStatus == (int)ScheduleDetailStatus.Draft)
                {
                    item.PublishStatus = ScheduleDetailStatus.Draft;
                }
                else if (input.PublishStatus == (int)ScheduleDetailStatus.Published)
                {
                    item.PublishStatus = ScheduleDetailStatus.Published;
                }
                item.CliqueMealTimeId = input.CliqueMealTimeId;
                item.TenantId = AbpSession.TenantId ?? 0;
                if (item.FixedMenu)
                {
                    item.FromDate = null;
                    item.ToDate = null;
                    item.CutOffDate = null;
                }

                await _cliqueMenuRepository.InsertAsync(item);
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        protected virtual async Task UpdateCliqueMenu(CreateOrUpdateCliqueMenuInput input)
        {
            try
            {
                var items = _cliqueMenuRepository.GetAll().Where(x => x.Id == input.Id).ToList();
                if(items != null && items.Count() > 0)
                {
                    var item = items[0];
                    item.LastModificationTime = DateTime.Now;
                    item.FixedMenu = input.FixedMenu;
                    item.AllDay = input.AllDay;
                    item.Days = input.Days;
                    item.ScreenMenuId = input.ScreenMenuId;
                    item.FromDate = input.FromDate;
                    item.ToDate = input.ToDate;
                    item.CutOffDate = input.CutOffDate;
                    item.LocationId = input.LocationId;
                    if (input.PublishStatus == (int)ScheduleDetailStatus.Draft)
                    {
                        item.PublishStatus = ScheduleDetailStatus.Draft;
                    }
                    else if (input.PublishStatus == (int)ScheduleDetailStatus.Published)
                    {
                        item.PublishStatus = ScheduleDetailStatus.Published;
                    }
                    item.CliqueMealTimeId = input.CliqueMealTimeId;
                    item.TenantId = AbpSession.TenantId ?? 0;
                    if (item.FixedMenu)
                    {
                        item.FromDate = null;
                        item.ToDate = null;
                        item.CutOffDate = null;
                    }

                    await _cliqueMenuRepository.UpdateAsync(item);
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task DeleteCliqueMenu(int cliqueMenuId)
        {
            await _cliqueMenuRepository.DeleteAsync(x => x.Id == cliqueMenuId);
        }

        public async Task<List<ScreenMenuListDto>> GetScreenMenuDropdown()
        {
            try
            {
                var data = await _screenMenuRepository.GetAll()
                    .Where(x => x.IsDeleted == false)
                    .Select(x => new ScreenMenuListDto
                    {
                        Id = x.Id,
                        Name = x.Name
                    })
                    .ToListAsync();
                return data;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }
    }
}

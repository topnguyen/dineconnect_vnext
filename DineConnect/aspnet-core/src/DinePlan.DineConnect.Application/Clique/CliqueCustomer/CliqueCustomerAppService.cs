﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Clique.CliqueCustomer.Dtos;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.Core.Swipe;

namespace DinePlan.DineConnect.Clique.CliqueCustomer
{
    public class CliqueCustomerAppService : DineConnectAppServiceBase, ICliqueCustomerAppService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ISettingManager _settingManager;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Brand> _organizationRepository;
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepository;
        private readonly IRepository<SwipeCard> _swipeCardRepository;
        public CliqueCustomerAppService(
            IRepository<Customer> customerRepository,
            ISettingManager settingManager,
            IRepository<City> cityRepository,
            IRepository<Location> locationRepository,
            IRepository<Brand> organizationRepository,
            IRepository<SwipeCustomerCard> swipeCustomerCardRepository,
            IRepository<SwipeCard> swipeCardRepository
        )
        {
            _customerRepository = customerRepository;
            _settingManager = settingManager;
            _cityRepository = cityRepository;
            _locationRepository = locationRepository;
            _organizationRepository = organizationRepository;
            _swipeCustomerCardRepository = swipeCustomerCardRepository;
            _swipeCardRepository = swipeCardRepository;
        }
         
        public async Task<CliqueCustomerDto> GetCustomerInfomation(int CustomerId)
        {
            CliqueCustomerDto result = new CliqueCustomerDto();

            var customer = _customerRepository
                .GetAll()
                .Where(x => x.IsDeleted == false && x.Id == CustomerId).FirstOrDefault();

            if(customer != null)
            {
                result.CustomerId = customer.Id;
                result.EmailAddress = customer.EmailAddress;
                result.CustomerName = customer.Name;
                result.PhoneNumber = customer.PhoneNumber;
                var user = await UserManager.FindByEmailAsync(customer.EmailAddress);
                if(user != null)
                {
                    result.UserName = user.UserName;
                }
            }

            return result;
        }

        public async Task UpdateCustomerInfomation(CliqueCustomerInput input)
        {
            var customer = await _customerRepository.FirstOrDefaultAsync(t => t.Id == input.CustomerId);

            customer.Name = input.CustomerName;
            customer.PhoneNumber = input.PhoneNumber;

            var user = await UserManager.FindByEmailAsync(customer.EmailAddress);

            if (user != null)
            {
                user.PhoneNumber = input.PhoneNumber;
            }

            CheckErrors(await UserManager.UpdateAsync(user));
            await _customerRepository.UpdateAsync(customer);
        }

        public async Task CliqueCustomerChangePassword(CliqueCustomerPasswordInput input)
        {
            await UserManager.InitializeOptionsAsync(AbpSession.TenantId);

            var user = await GetCurrentUserAsync();
            if(user != null)
            {
                await UserManager.ChangePasswordAsync(user, input.NewPassword);
            }
            else
            {
                CheckErrors(IdentityResult.Failed(new IdentityError
                {
                    Description = "User not found."
                }));
            }
            //if (await UserManager.CheckPasswordAsync(user, input.CurrentPassword))
            //{
            //    CheckErrors(await UserManager.ChangePasswordAsync(user, input.NewPassword));
            //}
            //else
            //{
            //    CheckErrors(IdentityResult.Failed(new IdentityError
            //    {
            //        Description = "Incorrect password."
            //    }));
            //}
        }

        public async Task UpdateLocationCustomer(CliqueCustomerLocationInput input)
        {
            int organizationId = 0;
            int locationId = 0;
            var listCity = _cityRepository.GetAll().Where(x => x.CountryId == input.CountryId && x.IsDeleted == false).ToList();
            if(listCity != null && listCity.Count() > 0)
            {
                List<Location> listLocation = new List<Location>();
                foreach(var itemCity in listCity)
                {
                    var getLocations = _locationRepository.GetAll().Where(x => x.CityId == itemCity.Id && x.IsDeleted == false).ToList();
                    if(getLocations != null && getLocations.Count() > 0)
                    {
                        listLocation.AddRange(getLocations);
                    }
                }
                if(listLocation != null && listLocation.Count() > 0)
                {
                    var listOrganizationId = listLocation.Select(x => x.OrganizationId).Distinct().ToList();
                    if(listOrganizationId != null && listOrganizationId.Count() > 0)
                    {
                        List<Brand> listOrganization = new List<Brand>();
                        foreach (var itemOrganizationId in listOrganizationId)
                        {
                            var getOrganizations = _organizationRepository.GetAll().Where(x => x.Id == itemOrganizationId && x.IsDeleted == false).ToList();
                            if (getOrganizations != null && getOrganizations.Count() > 0)
                            {
                                listOrganization.AddRange(getOrganizations);
                            }
                        }
                        if(listOrganization != null && listOrganization.Count() > 0)
                        {
                            var organization = listOrganization.OrderBy(x => x.Id).FirstOrDefault();
                            organizationId = organization.Id;
                        }
                    }
                }
            }
            if(organizationId == 0)
            {
                var getOrganizations = _organizationRepository.GetAll().Where(x => x.IsDeleted == false).ToList();
                if (getOrganizations != null && getOrganizations.Count() > 0)
                {
                    var organization = getOrganizations.OrderBy(x => x.Id).FirstOrDefault();
                    organizationId = organization.Id;
                }
            }
            if(organizationId > 0){
                var listLocation = _locationRepository.GetAll().Where(x => x.OrganizationId == organizationId && x.IsDeleted == false).ToList();
                if(listLocation != null && listLocation.Count() > 0)
                {
                    var location = listLocation.OrderBy(x => x.Id).FirstOrDefault();
                    locationId = location.Id;
                }
            }
            if(organizationId > 0 && locationId > 0)
            {
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, input.UserId), "OrganizationId", organizationId.ToString());
                await _settingManager.ChangeSettingForUserAsync(new UserIdentifier(AbpSession.TenantId, input.UserId), "LocationId", locationId.ToString());
            }
        }

        public async Task<PagedResultDto<CliqueCustomerListDto>> GetAllCustomer(CliqueCustomerListInput input)
        {
            var list = _customerRepository.GetAll()
                .Include(x => x.User)
                .Include(x => x.CustomerAddressDetails)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.PhoneNumber.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter));

            var totalCount = await list.CountAsync();
            var result = await list.ToListAsync();
            if(input.Sorting.Contains("lastLoginDate") == false && input.Sorting.Contains("balance") == false)
            {
                result = await list.OrderBy(input.Sorting).PageBy(input).ToListAsync();
            }
            var customers = ObjectMapper.Map<List<CliqueCustomerListDto>>(result);
            foreach (var item in customers)
            {
                item.LastLoginDate = item.CreationTime;
                item.Balance = 0;
                var customerLst = _customerRepository.GetAll().Where(x => x.UserId == item.UserId && x.IsDeleted == false).ToList();
                if(customerLst != null && customerLst.Count() > 0)
                {
                    var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == customerLst[0].Id && x.IsDeleted == false && x.Active == true).ToList();
                    if (getCustomerCard != null && getCustomerCard.Count() > 0)
                    {
                        var getSwipeCard = _swipeCardRepository.GetAll().Where(x => x.Id == getCustomerCard[0].CardRefId && x.IsDeleted == false && x.Active == true).ToList();
                        if (getSwipeCard != null && getSwipeCard.Count() > 0)
                        {
                            item.Balance = getSwipeCard[0].Balance;
                        }
                    }
                }
            }
            if(customers != null && customers.Count() > 0 && (input.Sorting.Contains("lastLoginDate") || input.Sorting.Contains("balance")))
            {
                customers = customers.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            }
            return new PagedResultDto<CliqueCustomerListDto>(totalCount, customers);
        }
    }
}

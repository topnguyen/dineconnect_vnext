﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Clique.MenuItemSchedules.Dtos;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using DinePlan.DineConnect.Clique.CliqueMenus.Dtos;

namespace DinePlan.DineConnect.Clique.MenuItemSchedules
{
    public class MenuItemScheduleAppService : DineConnectAppServiceBase, IMenuItemScheduleAppService
    {
        private readonly IRepository<MenuItemSchedule> _menuItemScheduleRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        public MenuItemScheduleAppService(
            IRepository<MenuItemSchedule> menuItemScheduleRepository,
            IRepository<MenuItem> menuItemRepository
        )
        {
            _menuItemScheduleRepository = menuItemScheduleRepository;
            _menuItemRepository = menuItemRepository;
        }

        public async Task<ListResultDto<ScreenMenuListDataDto>> GetMenuItemSchedule(ScreenMenuListInput input)
        {
            var menuItems = _menuItemScheduleRepository.GetAll().Where(x => x.IsDeleted == false);

            var sortedMenuItems = await menuItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var menuItemListDtos = new List<ScreenMenuListDataDto>();
            foreach (var item in sortedMenuItems)
            {
                ScreenMenuListDataDto itemMenu = new ScreenMenuListDataDto();
                itemMenu.Id = item.Id;
                itemMenu.StartHour = item.StartHour;
                itemMenu.StartMinute = item.StartMinute;
                itemMenu.EndHour = item.EndHour;
                itemMenu.EndMinute = item.EndMinute;
                itemMenu.Days = item.Days;

                if (!string.IsNullOrEmpty(itemMenu.Days))
                {
                    var listDay = itemMenu.Days.Split(",");
                    foreach(var itemDay in listDay)
                    {
                        if(itemDay == "Monday")
                        {
                            itemMenu.Monday = true;
                        }
                        if (itemDay == "Tuesday")
                        {
                            itemMenu.Tuesday = true;
                        }
                        if (itemDay == "Wednesday")
                        {
                            itemMenu.Wednesday = true;
                        }
                        if (itemDay == "Thursday")
                        {
                            itemMenu.Thursday = true;
                        }
                        if (itemDay == "Friday")
                        {
                            itemMenu.Friday = true;
                        }
                        if (itemDay == "Saturday")
                        {
                            itemMenu.Saturday = true;
                        }
                        if (itemDay == "Sunday")
                        {
                            itemMenu.Sunday = true;
                        }
                    }
                }

                var getMenu = _menuItemRepository.GetAll().Where(x => x.Id == item.MenuItemId).ToList();
                if (getMenu != null && getMenu.Count() > 0)
                {
                    itemMenu.MenuItemName = getMenu[0].Name;
                    itemMenu.MenuItemId = item.MenuItemId;
                }

                menuItemListDtos.Add(itemMenu);
            }

            return new PagedResultDto<ScreenMenuListDataDto>(
                await menuItems.CountAsync(),
                menuItemListDtos
            );
        }

        public ScreenMenuListDataDto GetMenuItemScheduleById(int menuItemSchedule)
        {
            ScreenMenuListDataDto itemMenu = new ScreenMenuListDataDto();
            var menuItemList = _menuItemScheduleRepository.GetAll().Where(x => x.Id == menuItemSchedule).ToList();
            if(menuItemList != null && menuItemList.Count() > 0)
            {
                var menuItems = menuItemList[0];
                itemMenu.Id = menuItems.Id;
                itemMenu.StartHour = menuItems.StartHour;
                itemMenu.StartMinute = menuItems.StartMinute;
                itemMenu.EndHour = menuItems.EndHour;
                itemMenu.EndMinute = menuItems.EndMinute;
                itemMenu.Days = menuItems.Days;

                var getMenu = _menuItemRepository.GetAll().Where(x => x.Id == menuItems.MenuItemId).ToList();
                if (getMenu != null && getMenu.Count() > 0)
                {
                    itemMenu.MenuItemName = getMenu[0].Name;
                    itemMenu.MenuItemId = menuItems.MenuItemId;
                }
            }
            return itemMenu;
        }

        public async Task CreateOrUpdateMenuItemSchedule(CreateOrUpdateScreenMenuCliqueInput input)
        {
            if (input.Id > 0)
            {
                await UpdateScreenMenu(input);
            }
            else
            {
                await CreateScreenMenu(input);
            }
        }

        protected virtual async Task CreateScreenMenu(CreateOrUpdateScreenMenuCliqueInput input)
        {
            try
            {
                if(CheckTimeValid(input.StartMinute, input.StartHour, input.EndHour, input.EndMinute))
                {
                    MenuItemSchedule item = new MenuItemSchedule();
                    item.CreationTime = DateTime.Now;
                    item.IsDeleted = false;
                    item.StartHour = input.StartHour;
                    item.StartMinute = input.StartMinute;
                    item.Days = input.Days;
                    item.EndHour = input.EndHour;
                    item.EndMinute = input.EndMinute;
                    item.MenuItemId = input.MenuItemId;

                    await _menuItemScheduleRepository.InsertAsync(item);
                }
                else
                {
                    throw new UserFriendlyException("Invalid input time. Please check again.");
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        protected virtual async Task UpdateScreenMenu(CreateOrUpdateScreenMenuCliqueInput input)
        {
            try
            {
                if (CheckTimeValid(input.StartMinute, input.StartHour, input.EndHour, input.EndMinute))
                {
                    var items = _menuItemScheduleRepository.GetAll().Where(x => x.Id == input.Id).ToList();
                    if(items != null && items.Count() > 0)
                    {
                        var item = items[0];
                        item.LastModificationTime = DateTime.Now;
                        item.StartHour = input.StartHour;
                        item.StartMinute = input.StartMinute;
                        item.Days = input.Days;
                        item.EndHour = input.EndHour;
                        item.EndMinute = input.EndMinute;
                        item.MenuItemId = input.MenuItemId;

                        await _menuItemScheduleRepository.UpdateAsync(item);
                    }
                }
                else
                {
                    throw new UserFriendlyException("Invalid input time. Please check again.");
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task DeleteMenuItemSchedule(int menuItemSchedule)
        {
            await _menuItemScheduleRepository.DeleteAsync(x => x.Id == menuItemSchedule);
        }

        private bool CheckTimeValid(int startMinute, int startHour, int endHour, int endMinute)
        {
            if(startHour > endHour)
            {
                return false;
            }
            else if(startHour == endHour && startMinute > endMinute)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Clique.CustomerFavoriteItem.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace DinePlan.DineConnect.Clique.CustomerFavoriteItem
{
    public class CliqueCustomerFavoriteItemAppService : DineConnectAppServiceBase, ICliqueCustomerFavoriteItemAppService
    {
        private readonly IRepository<CliqueCustomerFavoriteItem> _cliqueCustomerFavoriteItemRepository;
        public CliqueCustomerFavoriteItemAppService(
            IRepository<CliqueCustomerFavoriteItem> cliqueCustomerFavoriteItemRepository
        )
        {
            _cliqueCustomerFavoriteItemRepository = cliqueCustomerFavoriteItemRepository;
        }

        [HttpPost]

        public async Task CreateOrUpdateCliqueCustomerFavoriteItem(CliqueCustomerFavoriteInput input)
        {
            var existItem = _cliqueCustomerFavoriteItemRepository
                .GetAll()
                .Where(x => x.CustomerId == input.CustomerId && x.ScreenMenuItemId == input.ScreenMenuItemId)
                .FirstOrDefault();
            if (existItem != null)
            {
                existItem.IsDeleted = !input.IsFavorite;
                existItem.LastModificationTime = DateTime.Now;
                existItem.LastModifierUserId = input.CustomerId;
                if (!input.IsFavorite)
                {
                    existItem.DeletionTime = DateTime.Now;
                    existItem.DeleterUserId = input.CustomerId;
                }
                await _cliqueCustomerFavoriteItemRepository.UpdateAsync(existItem);
            }
            else if (input.IsFavorite)
            {
                var newItem = new CliqueCustomerFavoriteItem();
                newItem.CreationTime = DateTime.Now;
                newItem.CreatorUserId = input.CustomerId;
                newItem.CustomerId = input.CustomerId;
                newItem.MenuItemId = input.MenuItemId;
                newItem.ScreenMenuItemId = input.ScreenMenuItemId;
                newItem.IsDeleted = false;
                await _cliqueCustomerFavoriteItemRepository.InsertAsync(newItem);
            }
        }
    }
}

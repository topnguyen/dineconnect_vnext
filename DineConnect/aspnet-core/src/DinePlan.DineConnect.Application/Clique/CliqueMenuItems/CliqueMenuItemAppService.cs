﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos;
using DinePlan.DineConnect.Clique.Orders.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Linq.Dynamic;
using DinePlan.DineConnect.Tiffins.Schedule;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems
{
    public class CliqueMenuItemAppService : DineConnectAppServiceBase, ICliqueMenuItemAppService
    {
        private readonly IRepository<MenuItemSchedule> _menuItemScheduleRepository;
        private readonly IRepository<CliqueCustomerFavoriteItem> _cliqueCustomerFavoriteItemRepository;
        private readonly IRepository<CliqueOrder> _cliqueOrderRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IRepository<CliqueMealTime> _cliqueMealTimeRepository;
        private readonly IRepository<CliqueMenu> _cliqueMenuRepository;
        private readonly IRepository<ScreenMenu> _screenMenuRepository;
        private readonly IRepository<ScreenMenuCategory> _screenMenuCategoryRepository;
        private readonly IRepository<CliqueOrderDetail> _cliqueOrderDetailRepository;
        private readonly IRepository<CliquePayment> _cliquePaymentRepository;
        public CliqueMenuItemAppService(
            IRepository<MenuItemSchedule> menuItemScheduleRepository,
            IRepository<CliqueCustomerFavoriteItem> cliqueCustomerFavoriteItemRepository,
            IRepository<CliqueOrder> cliqueOrderRepository,
            IRepository<MenuItemPortion> menuItemPortionRepository,
            IRepository<MenuItem> menuItemRepository,
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IRepository<CliqueMealTime> cliqueMealTimeRepository,
            IRepository<CliqueMenu> cliqueMenuRepository,
            IRepository<ScreenMenu> screenMenuRepository,
            IRepository<ScreenMenuCategory> screenMenuCategoryRepository,
            IRepository<CliqueOrderDetail> cliqueOrderDetailRepository,
            IRepository<CliquePayment> cliquePaymentRepository
        )
        {
            _menuItemScheduleRepository = menuItemScheduleRepository;
            _cliqueCustomerFavoriteItemRepository = cliqueCustomerFavoriteItemRepository;
            _cliqueOrderRepository = cliqueOrderRepository;
            _menuItemPortionRepository = menuItemPortionRepository;
            _menuItemRepository = menuItemRepository;
            _screenMenuItemRepository = screenMenuItemRepository;
            _cliqueMealTimeRepository = cliqueMealTimeRepository;
            _cliqueMenuRepository = cliqueMenuRepository;
            _screenMenuRepository = screenMenuRepository;
            _screenMenuCategoryRepository = screenMenuCategoryRepository;
            _cliqueOrderDetailRepository = cliqueOrderDetailRepository;
            _cliquePaymentRepository = cliquePaymentRepository;
        }

        public ListResultDto<CliqueMealTimeDto> GetCliqueMealTime()
        {
            List<CliqueMealTimeDto> result = new List<CliqueMealTimeDto>();
            result = _cliqueMealTimeRepository
                .GetAll()
                .Where(x => x.IsDeleted == false)
                .Select(x => new CliqueMealTimeDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code,
                    SortOrder = x.SortOrder
                }).ToList();

            return new ListResultDto<CliqueMealTimeDto>(ObjectMapper.Map<List<CliqueMealTimeDto>>(result));
        }

        public ListResultDto<ScreenMenuCategoryDto> GetScreenMenuCategory(int CliqueMealTimeId, string Days, string DayOrder)
        {
             List<ScreenMenuCategoryDto> result = new List<ScreenMenuCategoryDto>();
            DateTime DateTemp = DateTime.Now;
            if (!string.IsNullOrEmpty(DayOrder) && DateTime.TryParse(DayOrder, out DateTemp))
            {
                DateTime DayOrderData = DateTime.Parse(DayOrder);
                var screenMenuIdList = _cliqueMenuRepository
                    .GetAll()
                    .Where(x => x.IsDeleted == false && x.CliqueMealTimeId == CliqueMealTimeId && x.PublishStatus == ScheduleDetailStatus.Published
                    && (x.FixedMenu || (x.CutOffDate.HasValue && x.CutOffDate.Value.Date == DayOrderData.Date)
                    || (!x.FixedMenu && (!x.FromDate.HasValue || (x.FromDate.HasValue && x.FromDate <= DayOrderData)) && (!x.ToDate.HasValue || (x.ToDate.HasValue && x.ToDate >= DayOrderData))
                    && (x.AllDay || (!x.AllDay && x.Days.Contains(Days))))))
                    .Select(x => x.ScreenMenuId).ToList();

                screenMenuIdList = _screenMenuRepository
                    .GetAll()
                    .Where(x => x.IsDeleted == false && screenMenuIdList.Contains(x.Id) && x.Type == ScreenMenuType.CliqueMenu)
                    .Select(x => x.Id).ToList();

                var screenMenuCategories = _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.IsDeleted == false && screenMenuIdList.Contains(x.ScreenMenuId))
                    .ToList();

                for (int i = 0; i < screenMenuCategories.Count(); i++)
                {
                    ScreenMenuCategoryDto item = new ScreenMenuCategoryDto
                    {
                        ScreenMenuCategoryId = screenMenuCategories[i].Id,
                        ScreenMenuId = screenMenuCategories[i].ScreenMenuId,
                        Name = screenMenuCategories[i].Name
                    };
                    var locationId = _cliqueMenuRepository
                        .GetAll()
                        .Where(x => x.ScreenMenuId == screenMenuCategories[i].ScreenMenuId && x.IsDeleted == false && x.CliqueMealTimeId == CliqueMealTimeId && x.PublishStatus == ScheduleDetailStatus.Published)
                        .Select(x => x.LocationId).FirstOrDefault();
                    item.LocationId = locationId;
                    if (!String.IsNullOrEmpty(screenMenuCategories[i].Dynamic))
                    {
                        dynamic dynamic = JObject.Parse(screenMenuCategories[i].Dynamic);
                        item.ImagePath = dynamic.ImagePath;
                        try
                        {
                            item.Order = dynamic.SortOrder;
                        }
                        catch (Exception e)
                        {
                            item.Order = 1;
                        }
                    }
                    result.Add(item);
                }
            }
            result = result.OrderBy(x => x.Order).ToList();
            return new ListResultDto<ScreenMenuCategoryDto>(ObjectMapper.Map<List<ScreenMenuCategoryDto>>(result));
        }

        public PagedResultDto<CliqueMenuItemDto> GetListMenuItemSchudule(CliqueMenuItemInput input)
        {
            var screenMenuItemList = _screenMenuItemRepository
                .GetAll()
                .Where(x => x.IsDeleted == false && x.ScreenCategoryId == input.ScreenMenuCategoryId)
                .ToList();
            var menuItemIdList = screenMenuItemList.Select(x => x.MenuItemId).Distinct().ToList();
            var menuItemPortionList = _menuItemPortionRepository
                .GetAll()
                .Where(x => menuItemIdList.Contains(x.MenuItemId) && x.IsDeleted == false)
                .ToList();

            var menuItemFavoriteList = _cliqueCustomerFavoriteItemRepository
                .GetAll()
                .Where(x => menuItemIdList.Contains(x.MenuItemId) && x.CustomerId == input.CustomerId && x.IsDeleted == false)
                .ToList();

            var menuItemList = _menuItemRepository
                .GetAll()
                .Where(x => menuItemIdList.Contains(x.Id) && x.IsDeleted == false)
                .ToList();

            var menuItemOrder = new List<CliqueOrderDetail>();

            var cliqueOrderList = _cliqueOrderRepository
                .GetAll()
                .Where(x => x.CustomerId == input.CustomerId && x.IsDeleted == false).ToList();

            foreach (var itemOrder in cliqueOrderList)
            {
                var checkPayment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).Count();
                var orderList = _cliqueOrderDetailRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).ToList();
                if (checkPayment == 0 && orderList != null && orderList.Count() > 0)
                {
                    menuItemOrder = orderList;
                    break;
                }
            }

            var returnList = (from a in screenMenuItemList
                              join c in menuItemPortionList on a.MenuItemId equals c.MenuItemId into mip
                              from portion in mip.DefaultIfEmpty()
                              join e in menuItemList on a.MenuItemId equals e.Id
                              select new CliqueMenuItemDto
                              {
                                  ScreenMenuItemId = a.Id,
                                  Name = a.Name,
                                  MenuItemId = e.Id,
                                  MenuItemPortionId = 0,
                                  AllergicInfo = e.AllergicInfo,
                                  Price = 0,
                                  FeedbackPoint = 4.5,
                                  FeedBackCount = 755,
                                  Mnt = 18,
                                  IsFavorite = false,
                                  IsAddToCart = false,
                                  Image = "",
                                  Description = "",
                                  Dynamic = a.Dynamic,
                                  LocationId = input.LocationId,
                                  ScreenMenuCategoryId = input.ScreenMenuCategoryId,
                                  Order = 0
                              }).AsQueryable();

            var allItemCount = returnList.Count();
            if (input.SkipCount > 0) returnList = returnList.Skip(input.SkipCount);
            if (input.MaxResultCount > 0) returnList = returnList.Take(input.MaxResultCount);
            var finalList = returnList.ToList();
            DateTime? DateTemp = null;
            DateTime DateCheck = DateTime.Now;
            if (!string.IsNullOrEmpty(input.DayOrder) && DateTime.TryParse(input.DayOrder, out DateCheck))
            {
                DateTemp = DateTime.Parse(input.DayOrder);
            }
            foreach (var item in finalList)
            {
                if (item != null)
                {
                    if (item.Dynamic != "")
                    {
                        dynamic dynamic = JObject.Parse(item.Dynamic);
                        if (dynamic != null)
                        {
                            item.Image = dynamic.ImagePath;
                            item.Description = dynamic.Description;
                            item.Order = dynamic.SortOrder ?? 0;
                        }
                    }
                    if (menuItemFavoriteList.Where(x => x.ScreenMenuItemId == item.ScreenMenuItemId).Count() > 0)
                    {
                        item.IsFavorite = true;
                    }
                    if (DateTemp != null && menuItemOrder.Where(x => x.ScreenMenuItemId == item.ScreenMenuItemId && x.OrderDate == DateTemp && x.CliqueMealTimeId == input.CliqueMealTimeId).Count() > 0)
                    {
                        item.IsAddToCart = true;
                    }
                    var Portion = menuItemPortionList.Where(x => x.MenuItemId == item.MenuItemId).FirstOrDefault();
                    if (Portion != null)
                    {
                        item.Price = Portion.Price;
                        item.MenuItemPortionId = Portion.Id;
                    }
                    var cliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == input.CliqueMealTimeId).ToList();
                    if (cliqueMealTime != null && cliqueMealTime.Count() > 0)
                    {
                        item.CliqueMealTimeId = cliqueMealTime[0].Id;
                        item.CliqueMealTimeName = cliqueMealTime[0].Name;
                    }
                }
            }
            finalList = finalList.OrderBy(x => x.Order).ToList();
            return new PagedResultDto<CliqueMenuItemDto>(allItemCount, finalList);
        }

        public PagedResultDto<CliqueMenuItemDto> GetListMenuItemFavorite(CliqueMenuItemFavoriteInput input)
        {
            var menuItemFavoriteList = _cliqueCustomerFavoriteItemRepository
                .GetAll()
                .Where(x => x.CustomerId == input.CustomerId && x.IsDeleted == false)
                .ToList();
            var menuItemIdList = menuItemFavoriteList.Select(x => x.MenuItemId).Distinct().ToList();
            var menuItemPortionList = _menuItemPortionRepository
                .GetAll()
                .Where(x => menuItemIdList.Contains(x.MenuItemId) && x.IsDeleted == false)
                .ToList();

            var menuItemList = _menuItemRepository
                .GetAll()
                .Where(x => menuItemIdList.Contains(x.Id) && x.IsDeleted == false)
                .ToList();

            var menuItemOrder = new List<CliqueOrderDetail>();

            var cliqueOrderList = _cliqueOrderRepository
                .GetAll()
                .Where(x => x.CustomerId == input.CustomerId && x.IsDeleted == false).ToList();

            foreach (var itemOrder in cliqueOrderList)
            {
                var checkPayment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).Count();
                var orderList = _cliqueOrderDetailRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).ToList();
                if (checkPayment == 0 && orderList != null && orderList.Count() > 0)
                {
                    menuItemOrder = orderList;
                    break;
                }
            }

            var returnList = (from a in menuItemFavoriteList
                              join e in menuItemList on a.MenuItemId equals e.Id
                              select new CliqueMenuItemDto
                              {
                                  ScreenMenuItemId = a.ScreenMenuItemId,
                                  Name = "",
                                  MenuItemId = e.Id,
                                  MenuItemPortionId = 0,
                                  AllergicInfo = e.AllergicInfo,
                                  Price = 0,
                                  FeedbackPoint = 4.5,
                                  FeedBackCount = 755,
                                  Mnt = 18,
                                  IsFavorite = true,
                                  IsAddToCart = false,
                                  Image = "",
                                  Description = "",
                                  Dynamic = "",
                                  LocationId = 0,
                                  ScreenMenuCategoryId = 0,
                                  Order = 0
                              }).AsQueryable();

            var allItemCount = returnList.Count();
            if (input.SkipCount > 0) returnList = returnList.Skip(input.SkipCount);
            if (input.MaxResultCount > 0) returnList = returnList.Take(input.MaxResultCount);
            var finalList = returnList.ToList();
            DateTime? DateTemp = null;
            DateTime DateCheck = DateTime.Now;
            if (!string.IsNullOrEmpty(input.DayOrder) && DateTime.TryParse(input.DayOrder, out DateCheck))
            {
                DateTemp = DateTime.Parse(input.DayOrder);
            }
            foreach (var item in finalList)
            {
                if (item != null)
                {
                    var screenMenuItemList = _screenMenuItemRepository
                        .GetAll()
                        .Where(x => x.Id == item.ScreenMenuItemId && x.IsDeleted == false)
                        .ToList();

                    if (screenMenuItemList != null && screenMenuItemList.Count() > 0)
                    {
                        item.Name = screenMenuItemList[0].Name;
                        if (screenMenuItemList[0].Dynamic != "")
                        {
                            item.Dynamic = screenMenuItemList[0].Dynamic;
                            dynamic dynamic = JObject.Parse(item.Dynamic);
                            if (dynamic != null)
                            {
                                item.Image = dynamic.ImagePath;
                                item.Description = dynamic.Description;
                                item.Order = dynamic.SortOrder ?? 0;
                            }
                        }
                    }

                    if (DateTemp != null && menuItemOrder.Where(x => x.ScreenMenuItemId == item.ScreenMenuItemId && x.OrderDate == DateTemp).Count() > 0)
                    {
                        item.IsAddToCart = true;
                    }
                    var Portion = menuItemPortionList.Where(x => x.MenuItemId == item.MenuItemId).ToList();
                    if (Portion != null && Portion.Count() > 0)
                    {
                        item.Price = Portion[0].Price;
                        item.MenuItemPortionId = Portion[0].Id;
                    }
                    var ScreenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == item.ScreenMenuItemId.Value).ToList();
                    if (ScreenMenuItem != null && ScreenMenuItem.Count() > 0)
                    {
                        item.ScreenMenuCategoryId = ScreenMenuItem[0].ScreenCategoryId;
                        var cliqueMenu = _cliqueMenuRepository.GetAll().Where(x => x.ScreenMenuId == ScreenMenuItem[0].ScreenMenuId && x.IsDeleted == false).ToList();
                        if (cliqueMenu != null && cliqueMenu.Count() > 0)
                        {
                            item.LocationId = cliqueMenu[0].LocationId;
                        }
                    }
                    var cliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == input.CliqueMealTimeId).ToList();
                    if (cliqueMealTime != null && cliqueMealTime.Count() > 0)
                    {
                        item.CliqueMealTimeId = cliqueMealTime[0].Id;
                        item.CliqueMealTimeName = cliqueMealTime[0].Name;
                    }
                }
            }
            finalList = finalList.OrderBy(x => x.Order).ToList();
            return new PagedResultDto<CliqueMenuItemDto>(allItemCount, finalList);
        }

        private bool CheckTimeValid(int startMinute, int startHour, int endHour, int endMinute)
        {
            int hourNow = DateTime.Now.Hour;
            int minuteNow = DateTime.Now.Minute;

            if (startHour > hourNow)
            {
                return false;
            }
            else if (startHour == hourNow && startMinute > minuteNow)
            {
                return false;
            }
            else if (endHour < hourNow)
            {
                return false;
            }
            else if (endHour == hourNow && endMinute < minuteNow)
            {
                return false;
            }
            return true;
        }
    }
}

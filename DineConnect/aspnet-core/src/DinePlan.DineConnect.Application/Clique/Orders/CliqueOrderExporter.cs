﻿using Abp.AspNetZeroCore.Net;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Storage;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Clique.Orders.Dtos;

namespace DinePlan.DineConnect.Clique.Orders
{
    public class CliqueOrderExporter : NpoiExcelExporterBase, ICliqueOrderExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;

        public CliqueOrderExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager,
            ITenantSettingsAppService tenantSettingsAppService)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
            _tempFileCacheManager = tempFileCacheManager;
            _tenantSettingsAppService = tenantSettingsAppService;
        }

        public FileDto PrintOrderDetail(CliqueOrderDetailForPrintDto input)
        {
            var pdffile = GenerateReportDetail(input);

            return pdffile;
        }

        private FileDto GenerateReportDetail(CliqueOrderDetailForPrintDto input)
        {
            Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
            Font NormalFont = FontFactory.GetFont("Arial", 13, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                PdfPCell cell = null;
                PdfPTable table = null;

                document.Open();

                table = new PdfPTable(1);
                table.TotalWidth = 400f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 1f });
                table.HorizontalAlignment = Element.ALIGN_RIGHT;

                InsertValueToCell(input.DateOrder.ToString(AppConsts.FileDateFormat), NormalFont, table, cell);
                InsertValueToCell(input.MealTime, NormalFont, table, cell);
                if(input.ProductOrders != null && input.ProductOrders.Count() > 0)
                {
                    foreach (var itemOrder in input.ProductOrders)
                    {
                        InsertValueToCell(itemOrder.Name + " x " + itemOrder.Quantity, NormalFont, table, cell);
                    }
                }
                InsertValueToCell(input.Total.ToString("#.##"), NormalFont, table, cell);

                document.Add(table);

                //Add border to page
                PdfContentByte content = writer.DirectContent;
                Rectangle rectangle = new Rectangle(document.PageSize);
                rectangle.Left += document.LeftMargin;
                rectangle.Right -= document.RightMargin;
                rectangle.Top -= document.TopMargin;
                rectangle.Bottom += document.BottomMargin;
                content.SetColorStroke(BaseColor.BLACK);
                content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                content.Stroke();

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                var file = new FileDto()
                {
                    FileName = $"Order-{input.DateOrder.ToString(AppConsts.FileDateFormat)}.pdf",
                    FileToken = new Guid().ToString("N"),
                    FileType = MimeTypeNames.ApplicationPdf,
                    Date = input.DateOrder
                };

                _tempFileCacheManager.SetFile(file.FileToken, bytes);

                return file;
            }
        }

        private static void InsertValueToCell(string value, Font NormalFont, PdfPTable table, PdfPCell cell)
        {
            table.AddCell(PhraseCell(new Phrase(value, NormalFont), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.PaddingBottom = 10f;
            table.AddCell(cell);
        }

        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderColor = BaseColor.WHITE;
            cell.VerticalAlignment = PdfPCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 5f;
            return cell;
        }
    }
}

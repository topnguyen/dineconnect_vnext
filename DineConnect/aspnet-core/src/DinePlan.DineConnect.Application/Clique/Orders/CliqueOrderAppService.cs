﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Clique.Orders.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Dto;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Tenants;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Configuration;
using Abp.Extensions;
using DinePlan.DineConnect.Core.Swipe;

namespace DinePlan.DineConnect.Clique.Orders
{
    public class CliqueOrderAppService : DineConnectAppServiceBase, ICliqueOrderAppService
    {
        private readonly IRepository<CliqueOrder> _cliqueOrderRepository;
        private readonly IRepository<CliqueOrderDetail> _cliqueOrderDetailRepository;
        private readonly IRepository<CliqueMealTime> _cliqueMealTimeRepository;
        private readonly IRepository<CliquePayment> _cliquePaymentRepository;
        private readonly IRepository<CliqueMenu> _cliqueMenuRepository;
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<ScreenMenuCategory> _screenMenuCategoryRepository;
        private readonly ICliqueOrderExporter _cliqueOrderExporter;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly ITimeZoneConverter _timeZoneConverter; 
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepository;
        private readonly IRepository<SwipeCard> _swipeCardRepository;
        private readonly IRepository<SwipeTransactionDetail> _swipeTransactionDetailRepository;
        public CliqueOrderAppService(
            IRepository<CliqueOrder> cliqueOrderRepository,
            IRepository<CliqueOrderDetail> cliqueOrderDetailRepository,
            IRepository<CliqueMealTime> cliqueMealTimeRepository,
            IRepository<CliquePayment> cliquePaymentRepository,
            IRepository<CliqueMenu> cliqueMenuRepository,
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IRepository<Customer> customerRepository,
            IRepository<ScreenMenuCategory> screenMenuCategoryRepository,
            ICliqueOrderExporter cliqueOrderExporter,
            ITenantSettingsAppService tenantSettingsAppService,
            ITimeZoneConverter timeZoneConverter,
            IRepository<SwipeCustomerCard> swipeCustomerCardRepository,
            IRepository<SwipeCard> swipeCardRepository,
            IRepository<SwipeTransactionDetail> swipeTransactionDetailRepository
        )
        {
            _cliqueOrderRepository = cliqueOrderRepository;
            _cliqueOrderDetailRepository = cliqueOrderDetailRepository;
            _cliqueMealTimeRepository = cliqueMealTimeRepository;
            _cliquePaymentRepository = cliquePaymentRepository;
            _cliqueMenuRepository = cliqueMenuRepository;
            _screenMenuItemRepository = screenMenuItemRepository;
            _customerRepository = customerRepository;
            _screenMenuCategoryRepository = screenMenuCategoryRepository;
            _cliqueOrderExporter = cliqueOrderExporter;
            _tenantSettingsAppService = tenantSettingsAppService;
            _timeZoneConverter = timeZoneConverter;
            _swipeCustomerCardRepository = swipeCustomerCardRepository;
            _swipeCardRepository = swipeCardRepository;
            _swipeTransactionDetailRepository = swipeTransactionDetailRepository;
        } 

        public async Task CreateOrUpdateCliqueOrder(CliqueOrderInput input)
        {
            //if (input.IsAddToCart && !CheckCutOfDate(input.ScreenMenuItemId, input.CliqueMealTimeId, input.OrderDate))
            //{
            //    throw new UserFriendlyException("Products that you are ordering during the day are not allowed to be order. Please check again.");
            //}
            List<CliqueOrderMenuInput> listMenu = new List<CliqueOrderMenuInput>();
            DateTime DateCheck = DateTime.Now;
            if (!string.IsNullOrEmpty(input.OrderDateStr) && DateTime.TryParse(input.OrderDateStr, out DateCheck))
            {
                input.OrderDate = DateTime.Parse(input.OrderDateStr);
            }
            await AddOrUpdateOrder(input, false, listMenu);
        }

        private async Task AddOrUpdateOrder(CliqueOrderInput input, bool isOrderAgain, List<CliqueOrderMenuInput> listMenu)
        {
            List<CliqueOrderDetail> OrderDetails = new List<CliqueOrderDetail>();
            var checkExistItem = false;
            var existItem = new CliqueOrder();
            var orderList = _cliqueOrderRepository
                .GetAll()
                .Where(x => x.CustomerId == input.CustomerId && x.IsDeleted == false)
                .ToList();

            foreach (var itemOrder in orderList)
            {
                var checkPayment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).Count();
                var orderDetailList = _cliqueOrderDetailRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).ToList();
                if (checkPayment == 0 && orderDetailList != null && orderDetailList.Count() > 0)
                {
                    OrderDetails = orderDetailList;
                    existItem = itemOrder;
                    checkExistItem = true;
                    break;
                }
            }

            if (checkExistItem && OrderDetails.Count() > 0)
            {
                if(listMenu != null && listMenu.Count() > 0)
                {
                    foreach(var itemMenuOrder in listMenu)
                    {
                        var indexOrderDetails = OrderDetails.FindIndex(x => x.MenuItemId == itemMenuOrder.MenuItemId && x.ScreenMenuItemId == itemMenuOrder.ScreenMenuItemId 
                        && x.CliqueOrderStatus == CliqueOrderStatus.Pending && x.OrderDate == itemMenuOrder.OrderDate && x.CliqueMealTimeId == itemMenuOrder.CliqueMealTimeId);
                        if (indexOrderDetails != -1)
                        {
                            if (itemMenuOrder.Quantity <= 0)
                            {
                                OrderDetails[indexOrderDetails].IsDeleted = true;
                            }
                            else
                            {
                                if (isOrderAgain)
                                {
                                    OrderDetails[indexOrderDetails].Quantity += itemMenuOrder.Quantity;
                                }
                                else
                                {
                                    OrderDetails[indexOrderDetails].Quantity = itemMenuOrder.Quantity;
                                }
                                OrderDetails[indexOrderDetails].OrderTags = itemMenuOrder.OrderTags;
                                OrderDetails[indexOrderDetails].Price = itemMenuOrder.Price;
                                OrderDetails[indexOrderDetails].MenuItemPortionId = itemMenuOrder.MenuItemPortionId;
                                OrderDetails[indexOrderDetails].OrderDate = itemMenuOrder.OrderDate;
                                OrderDetails[indexOrderDetails].CliqueMealTimeId = itemMenuOrder.CliqueMealTimeId;
                                OrderDetails[indexOrderDetails].LastModificationTime = DateTime.Now;
                                OrderDetails[indexOrderDetails].LastModifierUserId = input.CustomerId;
                                OrderDetails[indexOrderDetails].IsDeleted = !input.IsAddToCart;
                            }
                        }
                        else if (input.IsAddToCart && itemMenuOrder.Quantity > 0)
                        {
                            CliqueOrderDetail itemOrder = new CliqueOrderDetail
                            {
                                MenuItemId = itemMenuOrder.MenuItemId,
                                ScreenMenuItemId = itemMenuOrder.ScreenMenuItemId,
                                MenuItemPortionId = itemMenuOrder.MenuItemPortionId,
                                OrderTags = itemMenuOrder.OrderTags,
                                Quantity = itemMenuOrder.Quantity,
                                Price = itemMenuOrder.Price,
                                OrderDate = itemMenuOrder.OrderDate,
                                CliqueMealTimeId = itemMenuOrder.CliqueMealTimeId,
                                CliqueOrderStatus = CliqueOrderStatus.Pending,
                                IsDeleted = false
                            };
                            OrderDetails.Add(itemOrder);
                        }
                    }
                }
                else
                {
                    var indexOrderDetails = OrderDetails.FindIndex(x => x.MenuItemId == input.MenuItemId && x.ScreenMenuItemId == input.ScreenMenuItemId 
                    && x.CliqueOrderStatus == CliqueOrderStatus.Pending && x.OrderDate == input.OrderDate && x.CliqueMealTimeId == input.CliqueMealTimeId);
                    if (indexOrderDetails != -1)
                    {
                        if (input.Quantity <= 0)
                        {
                            OrderDetails[indexOrderDetails].IsDeleted = true;
                        }
                        else
                        {
                            if (isOrderAgain)
                            {
                                OrderDetails[indexOrderDetails].Quantity += input.Quantity;
                            }
                            else
                            {
                                OrderDetails[indexOrderDetails].Quantity = input.Quantity;
                            }
                            OrderDetails[indexOrderDetails].Price = input.Price;
                            OrderDetails[indexOrderDetails].MenuItemPortionId = input.MenuItemPortionId;
                            OrderDetails[indexOrderDetails].OrderDate = input.OrderDate;
                            OrderDetails[indexOrderDetails].CliqueMealTimeId = input.CliqueMealTimeId;
                            OrderDetails[indexOrderDetails].LastModificationTime = DateTime.Now;
                            OrderDetails[indexOrderDetails].LastModifierUserId = input.CustomerId;
                            OrderDetails[indexOrderDetails].IsDeleted = !input.IsAddToCart;
                        }
                    }
                    else if (input.IsAddToCart && input.Quantity > 0)
                    {
                        CliqueOrderDetail itemOrder = new CliqueOrderDetail
                        {
                            MenuItemId = input.MenuItemId,
                            ScreenMenuItemId = input.ScreenMenuItemId,
                            MenuItemPortionId = input.MenuItemPortionId,
                            OrderTags = input.OrderTags,
                            Quantity = input.Quantity,
                            Price = input.Price,
                            OrderDate = input.OrderDate,
                            CliqueMealTimeId = input.CliqueMealTimeId,
                            CliqueOrderStatus = CliqueOrderStatus.Pending,
                            IsDeleted = false
                        };
                        OrderDetails.Add(itemOrder);
                    }
                }

                existItem.OrderDetails = OrderDetails;
                existItem.Total = OrderDetails.Where(x => !x.IsDeleted).Sum(x => x.Quantity * x.Price);
                existItem.LastModificationTime = DateTime.Now;
                existItem.LastModifierUserId = input.CustomerId;

                await _cliqueOrderRepository.UpdateAsync(existItem);
            }
            else if (input.IsAddToCart && input.Quantity > 0)
            {
                CliqueOrder newItem = new CliqueOrder
                {
                    CreationTime = DateTime.Now,
                    CreatorUserId = input.CustomerId,
                    IsDeleted = false,
                    CustomerId = input.CustomerId,
                    LocationId = input.LocationId
                };
                if(listMenu != null && listMenu.Count() > 0)
                {
                    foreach(var itemMenuOrder in listMenu)
                    {
                        CliqueOrderDetail itemOrder = new CliqueOrderDetail
                        {
                            MenuItemId = itemMenuOrder.MenuItemId,
                            ScreenMenuItemId = itemMenuOrder.ScreenMenuItemId,
                            MenuItemPortionId = itemMenuOrder.MenuItemPortionId,
                            OrderTags = itemMenuOrder.OrderTags,
                            Quantity = itemMenuOrder.Quantity,
                            Price = itemMenuOrder.Price,
                            OrderDate = itemMenuOrder.OrderDate,
                            CliqueMealTimeId = itemMenuOrder.CliqueMealTimeId,
                            CliqueOrderStatus = CliqueOrderStatus.Pending,
                            IsDeleted = false
                        };
                        OrderDetails.Add(itemOrder);
                    }
                }
                else
                {
                    CliqueOrderDetail itemOrder = new CliqueOrderDetail
                    {
                        MenuItemId = input.MenuItemId,
                        ScreenMenuItemId = input.ScreenMenuItemId,
                        MenuItemPortionId = input.MenuItemPortionId,
                        OrderTags = input.OrderTags,
                        Quantity = input.Quantity,
                        Price = input.Price,
                        OrderDate = input.OrderDate,
                        CliqueMealTimeId = input.CliqueMealTimeId,
                        CliqueOrderStatus = CliqueOrderStatus.Pending,
                        IsDeleted = false
                    };
                    OrderDetails.Add(itemOrder);
                }
                newItem.OrderDetails = OrderDetails;
                newItem.Total = OrderDetails.Where(x => !x.IsDeleted).Sum(x => x.Quantity * x.Price);
                newItem.IsDeleted = false;
                await _cliqueOrderRepository.InsertAsync(newItem);
            }
        }

        public CliqueOrderDto GetCliqueOrderCustomer(int customerId)
        {
            decimal Total = 0;
            int CliqueOrderId = 0;
            int CountItems = 0;
            CliqueOrderDto data = new CliqueOrderDto();
            var CliqueOrderList = _cliqueOrderRepository
                .GetAll()
                .Where(x => x.CustomerId == customerId && x.IsDeleted == false).ToList();

            var OrderDetailList = new List<CliqueOrderDetail>();

            foreach (var itemOrder in CliqueOrderList)
            {
                var checkPayment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id).Count();
                var orderDetailList = _cliqueOrderDetailRepository.GetAll().Where(x => x.CliqueOrderId == itemOrder.Id && x.IsDeleted == false).ToList();
                if (checkPayment == 0 && orderDetailList != null && orderDetailList.Count() > 0)
                {
                    Total = itemOrder.Total;
                    CliqueOrderId = itemOrder.Id;
                    CountItems = orderDetailList.Count();
                    OrderDetailList.AddRange(orderDetailList);
                    break;
                }
            }

            data.Total = Total;
            data.CustomerId = customerId;
            data.CliqueOrderId = CliqueOrderId;
            data.CountItems = CountItems;
            List<CliqueProductOrderDto> ProductOrders = new List<CliqueProductOrderDto>();
            for(int i = 0; i < OrderDetailList.Count(); i++)
            {
                var item = OrderDetailList[i];
                CliqueProductOrderDto itemProduct = new CliqueProductOrderDto();
                itemProduct.CliqueOrderDetailId = item.Id;
                itemProduct.MenuItemId = item.MenuItemId;
                itemProduct.MenuItemPortionId = item.MenuItemPortionId;
                itemProduct.OrderTags = item.OrderTags;
                itemProduct.Quantity = item.Quantity;
                itemProduct.Price = item.Price;
                itemProduct.OrderDate = item.OrderDate;
                itemProduct.CliqueMealTimeId = item.CliqueMealTimeId;
                itemProduct.ScreenMenuItemId = item.ScreenMenuItemId;
                var screenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == item.ScreenMenuItemId).ToList();
                if(screenMenuItem != null && screenMenuItem.Count() > 0)
                {
                    var cliqueMenu = _cliqueMenuRepository.GetAll().Where(x => x.ScreenMenuId == screenMenuItem[0].ScreenMenuId && x.IsDeleted == false).ToList();
                    if (cliqueMenu != null && cliqueMenu.Count() > 0)
                    {
                        itemProduct.LocationId = cliqueMenu[0].LocationId;
                    }
                    itemProduct.Name = screenMenuItem[0].Name;
                    if (screenMenuItem[0].Dynamic != "")
                    {
                        dynamic dynamic = JObject.Parse(screenMenuItem[0].Dynamic);
                        if (dynamic != null)
                        {
                            itemProduct.Image = dynamic.ImagePath;
                        }
                    }
                }
                var CliqueMealTime = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == item.CliqueMealTimeId).ToList();
                if(CliqueMealTime != null && CliqueMealTime.Count() > 0)
                {
                    itemProduct.CliqueMealTimeName = CliqueMealTime[0].Name;
                    itemProduct.OrderDateStr = itemProduct.CliqueMealTimeName + " - " + itemProduct.OrderDate.ToString("ddd dd MMM, yyyy");
                }
                ProductOrders.Add(itemProduct);
            }
            data.ProductOrders = ProductOrders;

            return data;
        }

        public async Task<ListResultDto<CliqueOrderHistoryDto>> GetCliqueOrderHistory(CliqueOrderHistoryInput input)
        {
            var result = await GetCliqueOrderHistoryById(input.CustomerId);
            result = result.OrderByDescending(x => x.OrderTimeFor).ThenByDescending(x => x.CliqueOrderId).ToList();
            if (result != null && result.Count() > 0)
            {
                if (input.OrderStatus.HasValue)
                {
                    if(input.OrderStatus.Value == (int)CliqueOrderHistoryStatus.Complete)
                    {
                        result = result.Where(x => x.CliqueOrderStatus == CliqueOrderHistoryStatus.Complete).ToList();
                    }
                    else if (input.OrderStatus.Value == (int)CliqueOrderHistoryStatus.InQueue)
                    {
                        result = result.Where(x => x.CliqueOrderStatus == CliqueOrderHistoryStatus.InQueue).ToList();
                    }
                }
                if(input.SortResult == SortResult.ASC)
                {
                    result = result.OrderBy(x => x.OrderTimeFor).ThenBy(x => x.CliqueOrderId).ToList();
                }
            }

            return new ListResultDto<CliqueOrderHistoryDto>(ObjectMapper.Map<List<CliqueOrderHistoryDto>>(result));
        }

        public async Task<ListResultDto<CliqueOrderHistoryDto>> GetCliqueOrderForApp(string customerName)
        {
            List<CliqueOrderHistoryDto> result = new List<CliqueOrderHistoryDto>();
            List<int> customerList = _customerRepository.GetAll()
               .WhereIf(customerName != null && customerName != "", x => x.Name.Contains(customerName))
               .Where(x => x.IsDeleted == false).Select(x => x.Id).ToList();
            if (customerList != null && customerList.Count() > 0)
            {
                foreach(var customerId in customerList)
                {
                    var itemList = await GetCliqueOrderHistoryById(customerId);
                    if(itemList != null && itemList.Count() > 0)
                    {
                        result.AddRange(itemList);
                    }
                }
            }
            return new ListResultDto<CliqueOrderHistoryDto>(ObjectMapper.Map<List<CliqueOrderHistoryDto>>(result));
        }

        public async Task<PagedResultDto<CliqueOrderHistoryDto>> GetCustomerCliqueOrder(CliqueOrderHistoryAdminInput input)
        {
            List<CliqueOrderHistoryDto> result = new List<CliqueOrderHistoryDto>();
            int count = 0;
            List<int> customerList = _customerRepository.GetAll()
                .WhereIf(input.customerName != null && input.customerName != "", x => x.Name.Contains(input.customerName))
                .Where(x => x.IsDeleted == false).Select(x => x.Id).ToList();
            if (customerList != null && customerList.Count() > 0)
            {
                foreach (var customerId in customerList)
                {
                    var itemList = await GetCliqueOrderHistoryById(customerId);
                    if (itemList != null && itemList.Count() > 0)
                    {
                        if (input.orderDate.HasValue)
                        {
                            itemList = itemList.Where(x => x.OrderTimeFor.ToString("dd-MM-yyyy") == input.orderDate.Value.ToString("dd-MM-yyyy")).ToList();
                        }
                        result.AddRange(itemList);
                    }
                }
            }
            if(result != null && result.Count() > 0)
            {
                count = result.Count();
                result = result.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            }
            return new PagedResultDto<CliqueOrderHistoryDto>(
                count,
                result
            );
        }

        public async Task<CliqueOrderHistoryDto> GetCliqueOrderHistoryDetail(int cliqueOrderId)
        {
            CliqueOrderHistoryDto result = new CliqueOrderHistoryDto();
            var cliqueOrder = _cliqueOrderRepository.GetAll().Where(x => x.Id == cliqueOrderId).ToList();
            if(cliqueOrder != null && cliqueOrder.Count() > 0)
            {
                result = await GetCliqueOrderHistoryDetailById(cliqueOrder[0]);
            }
            return result;
        }

        public async Task DeleteCliqueOrderDetailById(int cliqueOrderDetailId)
        {
            var cliqueOrderDetail = _cliqueOrderDetailRepository.
                GetAll().Where(x=>x.Id == cliqueOrderDetailId).ToList();
            if(cliqueOrderDetail != null && cliqueOrderDetail.Count() > 0)
            {
                cliqueOrderDetail[0].IsDeleted = true;
                await _cliqueOrderDetailRepository.UpdateAsync(cliqueOrderDetail[0]);
            }
        }
        public async Task DeleteCliqueOrderById(int cliqueOrderId)
        {
            var cliqueOrderList = _cliqueOrderRepository.GetAll().Where(x => x.Id == cliqueOrderId).FirstOrDefault();
            var cliqueOrder = cliqueOrderList;
            List<CliqueOrderDetail> OrderDetails = _cliqueOrderDetailRepository.GetAll().Where(x => x.IsDeleted == false && x.CliqueOrderId == cliqueOrderId).ToList();
            foreach (var itemOrder in OrderDetails)
            {
                itemOrder.LastModificationTime = DateTime.Now;
                itemOrder.IsDeleted = true;
            }
            cliqueOrder.OrderDetails = OrderDetails;
            await _cliqueOrderRepository.UpdateAsync(cliqueOrder);
        }
        private async Task<List<CliqueOrderHistoryDto>> GetCliqueOrderHistoryById(int customerId)
        {
            List<CliqueOrderHistoryDto> data = new List<CliqueOrderHistoryDto>();
            var CliqueOrderList = _cliqueOrderRepository
                .GetAll()
                .Where(x => x.CustomerId == customerId && x.IsDeleted == false).ToList();
            foreach (var item in CliqueOrderList)
            {
                var checkPayment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == item.Id).Count();
                if (checkPayment > 0)
                {
                    var itemHistory = await GetCliqueOrderHistoryDetailById(item);
                    if(itemHistory != null)
                    {
                        data.Add(itemHistory);
                    }
                }
            }
            return data;
        }

        private async Task<CliqueOrderHistoryDto> GetCliqueOrderHistoryDetailById(CliqueOrder cliqueOrder)
        {
            var tenantSettings = await _tenantSettingsAppService.GetAllSettings();
            var cliqueOrderDetails = _cliqueOrderDetailRepository.GetAll().Where(x => x.IsDeleted == false && x.CliqueOrderId == cliqueOrder.Id).ToList();
            if(cliqueOrderDetails != null && cliqueOrderDetails.Count() > 0)
            {
                int NumberDayOfWeek = tenantSettings.CliqueCutOffDays;
                var cliqueOrderDetailMax = cliqueOrderDetails.OrderByDescending(x => x.OrderDate).FirstOrDefault();
                CliqueOrderHistoryDto data = new CliqueOrderHistoryDto();
                data.CliqueOrderId = cliqueOrder.Id;
                data.CliqueOrderStatus = CliqueOrderHistoryStatus.Complete;
                data.OrderTimeFor = cliqueOrderDetailMax.OrderDate.AddDays(NumberDayOfWeek);
                data.CliqueMealTimeId = cliqueOrderDetailMax.CliqueMealTimeId;
                var getCustomer = _customerRepository.GetAll().Where(x => x.Id == cliqueOrder.CustomerId).ToList();
                if(getCustomer != null && getCustomer.Count() > 0)
                {
                    data.CustomerName = getCustomer[0].Name;
                    data.CustomerId = getCustomer[0].Id;
                }
                var cliqueMealTime = new CliqueMealTime();
                var cliqueMealTimeist = _cliqueMealTimeRepository.GetAll().Where(x => x.Id == data.CliqueMealTimeId).ToList();
                if (cliqueMealTimeist != null && cliqueMealTimeist.Count() > 0)
                {
                    cliqueMealTime = cliqueMealTimeist[0];
                    data.CliqueMealTimeName = cliqueMealTime.Name;
                    data.OrderTimeForStr = cliqueMealTime.Name + " - " + data.OrderTimeFor.ToString("ddd dd MMM, yyyy");
                    data.OrderTimeForDetailStr = data.OrderTimeFor.ToString("dd MMM, yyyy | HH:mm") + " (" + NumberDayOfWeek.ToString() + " Days Left)";
                    data.OrderDateForDetailMobileStr = data.OrderTimeFor.ToString("dd MMM, yyyy");
                    data.OrderTimeForDetailMobileStr = data.OrderTimeFor.ToString("HH:mm");
                    data.OrderTimeStr = cliqueMealTime.Name + " - " + cliqueOrderDetailMax.OrderDate.ToString("ddd dd MMM, yyyy");
                }
                var payment = _cliquePaymentRepository.GetAll().Where(x => x.CliqueOrderId == cliqueOrder.Id && x.IsDeleted == false).FirstOrDefault();
                if (payment != null)
                {
                    data.PaymentMethod = payment.PaymentMethod;
                    data.PaidTime = payment.PaidTime;
                    data.PaidTimeStr = payment.PaidTime.ToString("dd MMM, yyyy | HH:mm");
                }
                List<CliqueProductOrderDto> ProductOrders = new List<CliqueProductOrderDto>();
                foreach (var itemOrder in cliqueOrderDetails)
                {
                    CliqueProductOrderDto product = new CliqueProductOrderDto();
                    var ScreenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == itemOrder.ScreenMenuItemId).ToList();
                    if (ScreenMenuItem != null && ScreenMenuItem.Count() > 0)
                    {
                        product.Name = ScreenMenuItem[0].Name;
                    }
                    product.MenuItemId = itemOrder.MenuItemId;
                    product.MenuItemPortionId = itemOrder.MenuItemPortionId;
                    product.OrderTags = itemOrder.OrderTags;
                    product.Quantity = itemOrder.Quantity;
                    product.Price = itemOrder.Price;
                    product.OrderDate = itemOrder.OrderDate;
                    product.CliqueMealTimeId = itemOrder.CliqueMealTimeId;
                    product.ScreenMenuItemId = itemOrder.ScreenMenuItemId;
                    if (cliqueMealTime != null && cliqueMealTime.Id > 0)
                    {
                        product.CliqueMealTimeName = cliqueMealTime.Name;
                        product.OrderDateStr = cliqueMealTime.Name + " - " + product.OrderDate.ToString("ddd dd MMM, yyyy");
                    }

                    var screenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == product.ScreenMenuItemId).ToList();
                    if (screenMenuItem != null && screenMenuItem.Count() > 0)
                    {
                        if (screenMenuItem[0].Dynamic != "")
                        {
                            dynamic dynamic = JObject.Parse(screenMenuItem[0].Dynamic);
                            if (dynamic != null)
                            {
                                product.Image = dynamic.ImagePath;
                            }
                        }
                    }

                    ProductOrders.Add(product);
                    if (itemOrder.CliqueOrderStatus != CliqueOrderStatus.Completed)
                    {
                        data.CliqueOrderStatus = CliqueOrderHistoryStatus.InQueue;
                    }
                }
                data.ProductOrders = ProductOrders;
                data.Total = ProductOrders.Sum(x => x.Price * x.Quantity);
                if (ProductOrders.Count() > 0)
                {
                    data.Name = ProductOrders[0].Name;
                    var screenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == ProductOrders[0].ScreenMenuItemId).ToList();
                    if (screenMenuItem != null && screenMenuItem.Count() > 0)
                    {
                        if (screenMenuItem[0].Dynamic != "")
                        {
                            dynamic dynamic = JObject.Parse(screenMenuItem[0].Dynamic);
                            if (dynamic != null)
                            {
                                data.Image = dynamic.ImagePath;
                            }
                        }
                    }
                    if (ProductOrders.Count() > 1)
                    {
                        data.Name += " and " + (ProductOrders.Count() - 1).ToString() + " others";
                    }
                }
                if(cliqueOrderDetailMax.CliqueOrderStatus == CliqueOrderStatus.Prepared)
                {
                    data.OrderStatus = "";
                }
                else if (cliqueOrderDetailMax.CliqueOrderStatus == CliqueOrderStatus.Delivered)
                {
                    data.OrderStatus = "Delivered";
                }
                else if (cliqueOrderDetailMax.CliqueOrderStatus == CliqueOrderStatus.Completed)
                {
                    data.OrderStatus = "Completed";
                }

                return data;
            }
            else
            {
                return null;
            }
        }

        public async Task UpdateStatusCliqueOrder(CliqueOrderStatusInput input)
        {
            var cliqueOrderList = _cliqueOrderRepository.GetAll().Where(x => x.Id == input.CliqueOrderId).ToList();
            if (cliqueOrderList != null && cliqueOrderList.Count() > 0)
            {
                var cliqueOrder = cliqueOrderList[0];
                if (input.OrderStatusId == (int)CliqueOrderStatus.Canceled)
                {
                    cliqueOrder.IsDeleted = true;
                    cliqueOrder.DeleterUserId = input.CustomerId;
                    cliqueOrder.DeletionTime = DateTime.Now;
                }
                cliqueOrder.LastModifierUserId = input.CustomerId;
                cliqueOrder.LastModificationTime = DateTime.Now;

                List<CliqueOrderDetail> orderDetails = _cliqueOrderDetailRepository.GetAll().Where(x=>x.IsDeleted == false && x.CliqueOrderId == input.CliqueOrderId).ToList();
                if(orderDetails != null && orderDetails.Count() > 0)
                {
                    foreach(var itemOrder in orderDetails)
                    {
                        itemOrder.LastModificationTime = DateTime.Now;
                        itemOrder.LastModifierUserId = input.CustomerId;
                        if(input.OrderStatusId == (int)CliqueOrderStatus.Canceled)
                        {
                            itemOrder.CliqueOrderStatus = CliqueOrderStatus.Canceled;
                        }
                        else if (input.OrderStatusId == (int)CliqueOrderStatus.Completed)
                        {
                            itemOrder.CliqueOrderStatus = CliqueOrderStatus.Completed;
                        }
                        else if (input.OrderStatusId == (int)CliqueOrderStatus.Delivered)
                        {
                            itemOrder.CliqueOrderStatus = CliqueOrderStatus.Delivered;
                        }
                        else if (input.OrderStatusId == (int)CliqueOrderStatus.Prepared)
                        {
                            itemOrder.CliqueOrderStatus = CliqueOrderStatus.Prepared;
                        }
                    }
                }
                cliqueOrder.OrderDetails = orderDetails;
                if(input.OrderStatusId == (int)CliqueOrderStatus.Canceled)
                {
                    await BackMoneyFromOrder(cliqueOrder.Total, input.CustomerId, cliqueOrder.Id, cliqueOrder.CreationTime);
                }

                await _cliqueOrderRepository.UpdateAsync(cliqueOrder);
            }
        }

        public async Task CopyCliqueOrder(int cliqueOrderId)
        {
            var cliqueOrderOld = _cliqueOrderRepository.GetAll().Where(x => x.Id == cliqueOrderId).ToList();
            if(cliqueOrderOld != null && cliqueOrderOld.Count() > 0)
            {
                var orderDetails = _cliqueOrderDetailRepository.GetAll().Where(x => x.IsDeleted == false && x.CliqueOrderId == cliqueOrderId).ToList();
                if(orderDetails !=  null && orderDetails.Count() > 0)
                {
                    List<CliqueOrderMenuInput> listMenu = orderDetails.Select(x => new CliqueOrderMenuInput {
                        MenuItemId = x.MenuItemId,
                        ScreenMenuItemId = x.ScreenMenuItemId,
                        CliqueMealTimeId = x.CliqueMealTimeId,
                        MenuItemPortionId = x.MenuItemPortionId,
                        Quantity = (int)x.Quantity,
                        Price = x.Price,
                        OrderDate = x.OrderDate
                    }).ToList();
                    CliqueOrderInput input = new CliqueOrderInput();
                    input.CustomerId = cliqueOrderOld[0].CustomerId;
                    input.IsAddToCart = true;
                    input.LocationId = cliqueOrderOld[0].LocationId;
                    await AddOrUpdateOrder(input, true, listMenu);
                }
            }
        }

        private bool CheckCutOfDate(int ScreenMenuItemId, int CliqueMealTimeId, DateTime OrderDate)
        {
            var getScreenMenuItem = _screenMenuItemRepository.GetAll().Where(x => x.Id == ScreenMenuItemId).ToList();
            if(getScreenMenuItem != null && getScreenMenuItem.Count() > 0)
            {
                var getScrennMenuCategory = _screenMenuCategoryRepository.GetAll().Where(x => x.Id == getScreenMenuItem[0].ScreenCategoryId).ToList();
                if(getScrennMenuCategory != null && getScrennMenuCategory.Count() > 0)
                {
                    var getCliqueMenu = _cliqueMenuRepository.GetAll().Where(x => x.CliqueMealTimeId == CliqueMealTimeId && x.ScreenMenuId == getScrennMenuCategory[0].ScreenMenuId && x.IsDeleted == false && x.CutOffDate.HasValue).ToList();
                    if(getCliqueMenu != null && getCliqueMenu.Count() > 0)
                    {
                        foreach(var cliqueMenu in getCliqueMenu)
                        {
                            if(cliqueMenu != null && cliqueMenu.CutOffDate.HasValue && cliqueMenu.CutOffDate.Value.Date == OrderDate.Date)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public FileDto PrintOrderDetail(CliqueOrderDetailForPrintDto input)
        {
            return _cliqueOrderExporter.PrintOrderDetail(input);
        }

        private async Task BackMoneyFromOrder(decimal money, int customerId, int orderId, DateTime orderDate)
        {
            var getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == customerId && x.IsDeleted == false && x.Active == true).ToList();
            if (getCustomerCard != null && getCustomerCard.Count() > 0)
            {
                var getSwipeCard = _swipeCardRepository.GetAll().Where(x => x.Id == getCustomerCard[0].CardRefId && x.IsDeleted == false && x.Active == true).ToList();
                if (getSwipeCard != null && getSwipeCard.Count() > 0)
                {
                    var card = getSwipeCard[0];
                    card.Balance = card.Balance + money;
                    card.LastModificationTime = DateTime.Now;
                    card.LastModifierUserId = customerId;
                    await _swipeCardRepository.UpdateAsync(card);

                    SwipeTransactionDetail transaction = new SwipeTransactionDetail();
                    transaction.CreationTime = DateTime.Now;
                    transaction.CreatorUserId = customerId;
                    transaction.TransactionTime = DateTime.Now;
                    transaction.TransactionType = (int)TransactionType.Transfer;
                    transaction.CustomerId = customerId;
                    transaction.CardRefId = card.Id;
                    transaction.Credit = money;
                    transaction.Debit = 0;
                    transaction.PaymentType = "tranfer";
                    dynamic dynamic = new
                    {
                        orderId = orderId,
                        orderDate = orderDate,
                        total = money,
                        cancelDate = DateTime.Now
                    };
                    transaction.PaymentTransactionDetails = JsonConvert.SerializeObject(dynamic);
                    transaction.Description = "Back money from Cancel Order";

                    await _swipeTransactionDetailRepository.InsertAsync(transaction);
                }
            }
        }

        private enum TransactionType
        {
            Toptup = 0,
            Transfer = 1,
            Payment = 2
        }
    }
}
 
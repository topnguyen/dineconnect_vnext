﻿using System;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.AddonSettings.Dtos;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.AddonSettings
{
    public class AddonSettingsAppService : DineConnectAppServiceBase, IAddonSettingsAppService
    {
        private readonly IRepository<Addon> _addonRepository;
        private readonly IRepository<TenantAddon> _tenantAddonRepository;

        public AddonSettingsAppService(IRepository<Addon> addonRepository, IRepository<TenantAddon> tenantAddonRepository)
        {
            _addonRepository = addonRepository;
            _tenantAddonRepository = tenantAddonRepository;
        }

        public async Task<List<CreateOrEditAddonDto>> GetPaymentSettingForEdit()
        {
            var entities = await _tenantAddonRepository.GetAll()
                            .Where(e => e.Addon.AddonType == AddonType.Payment)
                            .Select(e => e.Addon)
                            .ToListAsync();
            var result = new List<CreateOrEditAddonDto>();
            foreach (var entity in entities)
            {
                var dto = new CreateOrEditAddonDto();
                if (entity != null)
                {
                    dto = ObjectMapper.Map<CreateOrEditAddonDto>(entity);

                    if (entity.Name == "ServiceCharge")
                    {
                        dto.Setting = JsonConvert.DeserializeObject<List<ServiceChargeDto>>(entity.Settings);
                    }
                    else
                    {
                        dto.Setting = JsonConvert.DeserializeObject<TenantOmisePaymentGatewaySettingsEditDto>(entity.Settings);
                    }
                    result.Add(dto);
                }
            }

            return result;
        }

        public async Task<CreateOrEditAddonDto> GetAddonSettingForEdit(GetAddonForEditDto input)
        {
            var entity = await _tenantAddonRepository.GetAll()
                            .Select(e => e.Addon)
                            .FirstOrDefaultAsync(e => e.AddonType == (AddonType)input.AddonType);

            var dto = new CreateOrEditAddonDto();
            if (entity != null)
            {
                dto = ObjectMapper.Map<CreateOrEditAddonDto>(entity);

                switch (input.AddonType)
                {

                    case (int)AddonType.Image:
                        dto.Setting = JsonConvert.DeserializeObject<TenantCloudinarySettingsEditDto>(entity.Settings);
                        break;

                    default:
                        break;
                }
            }
            return dto;
        }

        public async Task CreateOrUpdateListAddon(List<CreateOrEditAddonDto> inputs)
        {
            foreach (var item in inputs)
            {
                await CreateOrUpdateAddonSetting(item);
            }
        }

        public async Task CreateOrUpdateAddonSetting(CreateOrEditAddonDto input)
        {
            ValidateForCreate(input);

            if (input.Id.HasValue)
            {
                await UpdateAddon(input);
            }
            else
            {
                await CreateAddon(input);
            }
        }

        protected async Task CreateAddon(CreateOrEditAddonDto input)
        {
            var addon = ObjectMapper.Map<Addon>(input);
            addon.Settings = JsonConvert.SerializeObject(input.Setting);
            if (addon.Name.IsNullOrEmpty())
                addon.Name = ((AddonType)input.AddonType).ToString();

            var id = await _addonRepository.InsertAndGetIdAsync(addon);

            await _tenantAddonRepository.InsertAsync(new TenantAddon { AddonId = id, TenantId = (int)AbpSession.TenantId });
        }

        protected async Task UpdateAddon(CreateOrEditAddonDto input)
        {
            var addon = await _addonRepository.GetAsync(input.Id.Value);
            ObjectMapper.Map(input, addon);

            addon.Settings = JsonConvert.SerializeObject(input.Setting);

            await _addonRepository.UpdateAsync(addon);
        }

        private void ValidateForCreate(CreateOrEditAddonDto input)
        {
            var exists = _addonRepository.GetAll().Where(e => e.Name == input.Name)
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id);
            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
        }

        public async Task<List<ServiceChargeDto>> GetServiceCharge(string paymentMode)
        {
            var serviceCharge = await _tenantAddonRepository.GetAll()
                .Where(e => e.Addon.AddonType == AddonType.Payment)
                .Select(e => e.Addon)
                .FirstOrDefaultAsync(e => e.Name == "ServiceCharge");

            if (serviceCharge == null || serviceCharge.Settings == null)
            {
                return null;
            }

            try
            {

                var listFees = JsonConvert.DeserializeObject<List<ServiceChargeDto>>(serviceCharge.Settings);

                switch (paymentMode)
                {
                    case "omise":
                        return listFees.Where(p => p.OmisePaymentUse).ToList();

                    case "stripe":
                        return listFees.Where(p => p.StripePaymentUse).ToList();

                    case "adyen":
                        return listFees.Where(p => p.AdyenPaymentUse).ToList();

                    case "paypal":
                        return listFees.Where(p => p.PaypalPaymentUse).ToList();

                    case "google":
                        return listFees.Where(p => p.GooglePayPaymentUse).ToList();

                    default:
                        return null;
                }

            }
            catch
            {
                return null;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.AddOn;
using DinePlan.DineConnect.AddonSettings.Grab.Dtos;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using Flurl.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Grab
{
    public class GrabAppService : DineConnectAppServiceBase, IGrabAppService
    {
        private const string AddOnName = "Grab";
        private readonly IRepository<ConnectAddOn> _addOn;

        private readonly string _ApiOAuthProduction = "https://api.grab.com/grabid/v1/oauth2/token";
        private readonly string _ApiOauthStaging = "https://api.stg-myteksi.com/grabid/v1/oauth2/token";
        private readonly string _ApiProduction = "http://developer.grab.com";
        private readonly string _ApiStaging = "https://developer-beta.stg-myteksi.com";

        private readonly IRepository<ExternalLog> _logRepository;
        private readonly IRepository<Location> _lRepo;
        private readonly IAbpSession _session;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly ISettingManager _settingManager;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<OrderTagMap> _orderMapManager;
        private readonly IRepository<Connect.Master.Locations.LocationSchedule> _locationScheduleRepo;

        public GrabAppService(IRepository<ConnectAddOn> addOn, ISettingManager settingManager,
            IRepository<WorkPeriod> wpRepo, IRepository<ExternalLog> logRepository,
            IRepository<Location> lRepo,
            IAbpSession session,
            IRepository<MenuItem> menuItemRepository,
            IRepository<ProductGroup> productGroupRepository,
            IRepository<Category> categoryRepository,
            IFeatureChecker featureManager,
            ITenantSettingsAppService tenantSettingsAppService,
            IRepository<OrderTagMap> orderMapManager,
            IRepository<Connect.Master.Locations.LocationSchedule> locationScheduleRepo)
        {
            _session = session;
            _menuItemRepository = menuItemRepository;
            _categoryRepository = categoryRepository;
            _tenantSettingsAppService = tenantSettingsAppService;
            _orderMapManager = orderMapManager;
            _locationScheduleRepo = locationScheduleRepo;
            _settingManager = settingManager;
            _addOn = addOn;
            _lRepo = lRepo;
            _logRepository = logRepository;
        }

        public async Task<bool> IsTokenExists()
        {
            try
            {
                var grabAddOn = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (grabAddOn != null)
                {
                    var token = JsonConvert.DeserializeObject<GrabToken>(grabAddOn.Settings);
                    var grabSetting = JsonConvert.DeserializeObject<GrabSetting>(grabAddOn.AddOnSettings);

                    if (token != null && token.IsExpired && !string.IsNullOrEmpty(token.access_token))
                    {
                        var myRefresh = await RefreshTokenAndSave(grabSetting.ClientId, grabSetting.ClientSecret);
                        return myRefresh != null;
                    }
                    return token != null && !string.IsNullOrEmpty(token.access_token);
                }

                return false;
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return false;
        }

        public bool RemoveToken()
        {
            var granAddOn = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (granAddOn != null) _addOn.Delete(granAddOn);
            return true;
        }

        public async Task<bool> Connect(string clientId, string clientSecret)
        {
            try
            {
                var myToken = await RefreshTokenAndSave(clientId, clientSecret);
                if (myToken != null)
                    return true;
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return false;
        }

        public async Task<string> GrabGetMenuItem()
        {
            var settings = await _tenantSettingsAppService.GetAllSettings();
            var schedules = new List<ServiceHours>();

            if (!settings.Connect.Schedules.IsNullOrEmpty())
            {
                schedules = JsonConvert.DeserializeObject<List<ServiceHours>>(settings.Connect.Schedules);
            }
            else
                schedules.Add(new ServiceHours
                {
                    name = "Meal Time",
                    startHour = "00",
                    startMinute = "00",
                    endHour = "23",
                    endMinute = "59"
                });

            var sections = new List<GrabMenuSection>();

            var categories = await GetCategoryAndItemForSection();

            var id = 1;
            foreach (var schedule in schedules)
            {
                var time = new
                {
                    startTime = schedule.startTime,
                    endTime = schedule.endTime
                };

                var period = new
                {
                    openPeriodType = "OpenPeriod",
                    periods = new List<object> { time }
                };

                var serviceHours = new
                {
                    mon = period,
                    tue = period,
                    wed = period,
                    thu = period,
                    fri = period,
                    sat = period,
                    sun = period,
                };

                var section = new GrabMenuSection
                {
                    Id = id.ToString(),
                    Name = schedule.name,
                    ServiceHours = serviceHours,
                    Categories = categories
                };

                sections.Add(section);
                id++;
            }

            var currency = new
            {
                code = "SGD",
                symbol = "S$",
                exponent = await _settingManager.GetSettingValueAsync<int>(AppSettings.TouchSaleSettings.Decimals),
            };
            var objectOut = new
            {
                merchantID = "",
                partnerMerchantID = "",
                currency = currency,
                sections = sections
            };

            return JsonConvert.SerializeObject(objectOut);
        }

        public async Task<string> GrabSyncData(string merchantID)
        {
            var grabAddOn = _addOn.GetAll().OrderByDescending(c => c.CreationTime).FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
            if (grabAddOn != null)
            {
                var token = JsonConvert.DeserializeObject<GrabToken>(grabAddOn.Settings);
                var grabSetting = JsonConvert.DeserializeObject<GrabSetting>(grabAddOn.AddOnSettings);

                if (token != null && token.IsExpired && !string.IsNullOrEmpty(token.access_token))
                {
                    token = await RefreshTokenAndSave(grabSetting.ClientId, grabSetting.ClientSecret); ;
                }

                if (!token.access_token.IsNullOrEmpty())
                {
                    var url = grabSetting.Production ? "https://partner-api.stg-myteksi.com/grabfood/partner/v1/merchant/menu/notification" : "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/merchant/menu/notification";

                    var request = new
                    {
                        merchantID = merchantID
                    };

                    var response = await url
                        .WithHeader("Content-Type", "application/json")
                        .WithHeader("Authorization", $"Bearer {token.access_token}")
                        .PostJsonAsync(request)
                        .ReceiveString();

                    return response;
                }
            }
            return null;
        }

        public async Task<string> GrabGetMenuItemForLocation(int locationid)
        {
            var locationschedules = await _locationScheduleRepo.GetAll().Where(s => s.LocationId == locationid).ToListAsync();

            var settings = await _tenantSettingsAppService.GetAllSettings();
            var schedules = new List<ServiceHours>();

            if (locationschedules.Any())
            {
                schedules = locationschedules.Select(s => new ServiceHours
                {
                    name = s.Name,
                    startHour = s.StartHour.ToString(),
                    startMinute = s.StartMinute.ToString(),
                    endHour = s.EndHour.ToString(),
                    endMinute = s.EndMinute.ToString(),
                }).ToList();
            }
            else
                schedules.Add(new ServiceHours
                {
                    name = "Meal Time",
                    startHour = "00",
                    startMinute = "00",
                    endHour = "23",
                    endMinute = "59"
                });

            var sections = new List<GrabMenuSection>();

            var categories = await GetCategoryAndItemForSection();

            var id = 1;
            foreach (var schedule in schedules)
            {
                var time = new
                {
                    startTime = schedule.startTime,
                    endTime = schedule.endTime
                };

                var period = new
                {
                    openPeriodType = "OpenPeriod",
                    periods = new List<object> { time }
                };

                var serviceHours = new
                {
                    mon = period,
                    tue = period,
                    wed = period,
                    thu = period,
                    fri = period,
                    sat = period,
                    sun = period,
                };

                var section = new GrabMenuSection
                {
                    Id = id.ToString(),
                    Name = schedule.name,
                    ServiceHours = serviceHours,
                    Categories = categories
                };

                sections.Add(section);
                id++;
            }

            var currency = new
            {
                code = "SGD",
                symbol = "S$",
                exponent = await _settingManager.GetSettingValueAsync<int>(AppSettings.TouchSaleSettings.Decimals),
            };
            var objectOut = new
            {
                merchantID = "",
                partnerMerchantID = "",
                currency = currency,
                sections = sections
            };

            return JsonConvert.SerializeObject(objectOut);
        }

        #region Privates

        private void LogGrab(Exception e)
        {
            var myLog = new ExternalLog
            {
                AuditType = (int)ExternalLogType.Grab,
                LogData = e.Message,
                LogDescription = FlattenException(e),
                LogTime = DateTime.Now,
                ExternalLogTrunc = DateTime.Now.Date
            };
            _logRepository.InsertAndGetId(myLog);
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        private async Task<List<GrabMenuCategory>> GetCategoryAndItemForSection()
        {
            var grabCategories = new List<GrabMenuCategory>();

            var categories = await _categoryRepository.GetAll().Where(c => c.MenuItems.Any()).ToListAsync();
            foreach (var category in categories)
            {
                var grabCategory = new GrabMenuCategory()
                {
                    Id = category.Id.ToString(),
                    AvailableStatus = AvailableStatus.AVAILABLE,
                    Name = category.Name,
                    Items = await GetGrabMenuItemsByCategory(category.Id)
                };

                grabCategories.Add(grabCategory);
            }
            return grabCategories;
        }

        private async Task<List<GrabMenuItem>> GetGrabMenuItemsByCategory(int categoryId)
        {
            List<GrabMenuItem> grabItems = new List<GrabMenuItem>();
            foreach (var mItem in _menuItemRepository.GetAll().Where(i => i.CategoryId == categoryId))
            {
                if (mItem.Portions != null)
                {
                    grabItems.Add(new GrabMenuItem()
                    {
                        Id = mItem.Id.ToString(),
                        Name = mItem.Name,
                        Description = mItem.ItemDescription,
                        AvailableStatus = AvailableStatus.AVAILABLE,
                        SpecialType = Enum.GetName(typeof(ProductFoodType), mItem.FoodType),
                        Price = mItem.Portions.First().Price * 100,
                        ModifierGroups = GetOrderTagByMenuItem(mItem.Id, categoryId)
                    });
                }
            }
            return grabItems;
        }

        private List<GrabModifierGroup> GetOrderTagByMenuItem(int menuItemId, int categoryId)
        {
            List<GrabModifierGroup> modifierGroups = _orderMapManager.GetAll()
                .Where(t => t.OrderTagGroup.MinSelectedItems <= t.OrderTagGroup.MaxSelectedItems && t.OrderTagGroup.MaxSelectedItems > 0)
                .Where(t => (t.CategoryId.HasValue && t.CategoryId == categoryId && !t.MenuItemId.HasValue) || (t.MenuItemId.HasValue && t.MenuItemId == menuItemId))
                .Include(t => t.OrderTagGroup.Tags)
                .ToList()
                .Select(t => new GrabModifierGroup
                {
                    Id = t.OrderTagGroup.Id.ToString(),
                    Name = t.OrderTagGroup.Name,
                    SelectionRangeMax = t.OrderTagGroup.MaxSelectedItems,
                    SelectionRangeMin = t.OrderTagGroup.MinSelectedItems,
                    AvailableStatus = AvailableStatus.AVAILABLE,
                    Modifiers = t.OrderTagGroup.Tags.Select(tg => new GrabModifier
                    {
                        Id = tg.Id.ToString(),
                        Name = tg.Name,
                        AvailableStatus = AvailableStatus.AVAILABLE,
                        Price = tg.Price * 100
                    }).ToList()
                })
                .ToList();

            return modifierGroups;
        }

        private async Task<GrabToken> RefreshTokenAndSave(string clientId, string clientSecret)
        {
            try
            {
                bool productionG = false;

                GrabSetting mySetting = new GrabSetting()
                {
                    Production = productionG,
                    ClientId = clientId,
                    ClientSecret = clientSecret
                };

                var body = new
                {
                    client_id = clientId,
                    client_secret = clientSecret,
                    grant_type = "client_credentials",
                    scope = "food.partner_api"
                };

                string url = productionG ? _ApiOAuthProduction : _ApiOauthStaging;

                var refreshedToken = await url
                    .WithHeader("Content-Type", "application/json")
                    .PostJsonAsync(body)
                    .ReceiveJson<GrabToken>();

                if (refreshedToken != null)
                {
                    var grabAddOn = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                    refreshedToken.ExpiresOn = DateTime.UtcNow.AddSeconds(refreshedToken.expires_in);

                    if (grabAddOn == null)
                    {
                        grabAddOn = new ConnectAddOn()
                        {
                            Name = AddOnName,
                        };
                    }

                    grabAddOn.AddOnSettings = JsonConvert.SerializeObject(mySetting);
                    grabAddOn.Settings = refreshedToken.ToJson();
                    await _addOn.InsertOrUpdateAsync(grabAddOn);

                    return refreshedToken;
                }
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return null;
        }

        #endregion
    }
}

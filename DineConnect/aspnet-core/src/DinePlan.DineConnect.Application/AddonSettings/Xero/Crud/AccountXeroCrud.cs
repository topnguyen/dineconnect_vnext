﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.AddonSettings.Xero.Crud.Token;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Xero.Crud
{
    public class AccountXeroCrud
    {
        public static async Task<List<XeroAccount>> GetAll(XeroToken token)
        {
            var response = await 
                TokenXeroCrud.CallXeroApi<string, AccountResponse<List<XeroAccount>>>("", XeroRequests.Account, token, SendGridClient.Method.GET);

            return response.Accounts;
        }
    }
}

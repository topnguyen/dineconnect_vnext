﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.AddonSettings.Xero.Crud.Token;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Xero.Crud
{
    public class ContactXeroCrud
    {
        public static async Task<List<XeroContact>> GetAll(XeroToken token)
        {
            var response = await
                TokenXeroCrud.CallXeroApi<string, ContactResponse<List<XeroContact>>>
                    ("", XeroRequests.Contact, token, SendGridClient.Method.GET);
            return response.Contacts;
        }
    }
}

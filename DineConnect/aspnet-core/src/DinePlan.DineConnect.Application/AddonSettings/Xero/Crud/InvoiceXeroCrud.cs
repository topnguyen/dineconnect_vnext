﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.AddonSettings.Xero.Crud.Token;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Xero.Crud
{
    public class InvoiceXeroCrud
    {
        public static async Task<string> CreateInvoice(XeroToken token, XeroInvoice invoice)
        {
            var response = await
                TokenXeroCrud.CallXeroApi<XeroInvoice, InvoiceResponse<List<XeroInvoice>>>
                    (invoice, XeroRequests.Invoice, token, SendGridClient.Method.POST);

            if (response?.Invoices != null && response.Invoices.Any())
            {
                return response.Invoices.First().InvoiceID;
            }
            return null;
        }

        public static string CreatePayment(XeroToken token, XeroPayment payment)
        {
            var response =
                TokenXeroCrud.CallXeroApi<XeroPayment, object>
                    (payment, XeroRequests.Payment, token, SendGridClient.Method.PUT);

            return response.ToString();

        }
    }
}

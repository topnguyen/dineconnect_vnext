﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.AddonSettings.Xero.Crud.Token;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Xero.Crud
{
    public class ItemXeroCrud
    {
        public static async Task<IEnumerable<XeroItem>> GetAll(XeroToken token)
        {
            var response = await
                TokenXeroCrud.CallXeroApi<string, ItemResponse<List<XeroItem>>>
                    ("", XeroRequests.Item, token, SendGridClient.Method.GET);
            return response.Items;
        }
    }
}

﻿using System.Threading.Tasks;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using Flurl.Http;
using Newtonsoft.Json;
using SendGrid;

namespace DinePlan.DineConnect.AddonSettings.Xero.Crud.Token
{
    public class TokenXeroCrud
    {

        public static async Task<SOutputObj> CallXeroApi<RequestObj, SOutputObj>(RequestObj input, string apiRoute,XeroToken token, SendGridClient.Method method)
        {
            SOutputObj result = default;

            var body = JsonConvert.SerializeObject(input, Formatting.None, new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            
           var response = await apiRoute.WithOAuthBearerToken(token.access_token)
                .WithHeader("Xero-tenant-id", token.xero_tenant_id)
                .WithHeader("content-type", "application/json")
                .PostJsonAsync(body)
                .ReceiveJson<SOutputObj>();

            return response;
        }
    }
}

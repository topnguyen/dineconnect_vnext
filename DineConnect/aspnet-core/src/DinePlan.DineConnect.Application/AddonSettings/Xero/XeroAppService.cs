﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.AddOn;
using DinePlan.DineConnect.AddonSettings.Xero.Crud;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Period;
using Flurl.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Xero
{
    public class XeroAppService : DineConnectAppServiceBase, IXeroAppService
    {
        private const string AddOnName = "Xero";
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly string _clientId;
        private readonly string _clientSecret;
        private readonly string _dineConnectApplicationUrl = "http://localhost:7301";
        private readonly IRepository<ExternalLog> _logRepository;
        private readonly IRepository<Location> _lRepo;
        private readonly IAbpSession _session;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<WorkPeriod> _wpRepo;
        private readonly IConfigurationRoot _appConfiguration;

        public XeroAppService(IRepository<ConnectAddOn> addOn, ISettingManager settingManager,
            IRepository<WorkPeriod> wpRepo, IRepository<ExternalLog> logRepository,
            IRepository<Location> lRepo,
            IAbpSession session,
            IWebHostEnvironment env)
        {
            _session = session;
            _env = env;
            _addOn = addOn;

            _appConfiguration = env.GetAppConfiguration();

            _clientId = _appConfiguration["Xero:XeroClientId"] ?? string.Empty;
            _clientSecret = _appConfiguration["Xero:XeroClientSecret"] ?? string.Empty;

            var siteRootFormat = _appConfiguration["App:ServerRootAddress"] ?? "https://localhost:44301/".EnsureEndsWith('/');

            if (!string.IsNullOrEmpty(siteRootFormat))
            {
                _dineConnectApplicationUrl = siteRootFormat;
            }
            _wpRepo = wpRepo;
            _lRepo = lRepo;
            _logRepository = logRepository;
        }

        public string GetLoginUrl()
        {
            var gatewayUrl = GetGatewayUrl();
            return
                $"{gatewayUrl}/dineconnect?applicationType=xero&dineConnectTenant={_session.TenantId}&applicationUrl={_dineConnectApplicationUrl}";
        }

        public bool RemoveToken()
        {
            var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (xeroAddon != null) _addOn.Delete(xeroAddon);
            return true;
        }

        public async Task<bool> IsTokenExists()
        {
            try
            {
                var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (xeroAddon != null)
                {
                    var settings = xeroAddon.Settings;
                    var token = JsonConvert.DeserializeObject<XeroToken>(settings);
                    if (token != null && token.IsExpired)
                    {
                        await RefreshTokenAndSave(token);
                    }
                    return token != null;
                }

                return false;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return false;
        }

        public async Task<bool> KeepTokenComingFromGateway(XeroToken token)
        {
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);

            var xeroAddOn = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
            if (xeroAddOn == null)
            {
                xeroAddOn = new ConnectAddOn
                {
                    TenantId = _session.GetTenantId(),
                    CreationTime = DateTime.UtcNow,
                    Name = AddOnName,
                    Settings = token.ToJson()
                };
                await _addOn.InsertAndGetIdAsync(xeroAddOn);
                return true;
            }

            return false;
        }

        public async Task<XeroToken> RefreshTokenAndSave(XeroToken token)
        {
            try
            {
                var url = "https://identity.xero.com/connect/token";

                var refreshedToken = await url
                    .WithBasicAuth(_clientId, _clientSecret)
                    .WithHeader("content-type", "application/x-www-form-urlencoded")
                    .PostJsonAsync(new
                    {
                        grant_type = "refresh_token",
                        refresh_token = token.refresh_token
                    })
                    .ReceiveJson<XeroToken>();

                refreshedToken.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);

                refreshedToken.xero_tenant_id = token.xero_tenant_id;

                var xeroAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);

                xeroAddon.Settings = refreshedToken.ToJson();

                await _addOn.UpdateAsync(xeroAddon);

                return token;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        public async Task<XeroSetting> GetXeroSetting()
        {
            var xeroSetting = new XeroSetting();

            var xeroAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);

            if (xeroAddon != null)
            {
                var settings = xeroAddon.AddOnSettings;
                if (!string.IsNullOrEmpty(settings)) return JsonConvert.DeserializeObject<XeroSetting>(settings);
            }

            return xeroSetting;
        }

        public async Task UpdateXeroSetting(XeroSetting xeroSetting)
        {
            var xeroAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);

            if (xeroAddon != null)
            {
                xeroAddon.AddOnSettings = JsonConvert.SerializeObject(xeroSetting);
            }
        }

        public async Task<List<ComboboxItemDto>> GetItems()
        {
            try
            {
                var output = new List<ComboboxItemDto>();

                var token = await GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in await ItemXeroCrud.GetAll(token))
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.Code,
                            DisplayText = accTye.Name
                        });
                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        public async Task<List<ComboboxItemDto>> GetAccounts()
        {
            try
            {
                var output = new List<ComboboxItemDto>();
                var token = await GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in await AccountXeroCrud.GetAll(token))
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.Code,
                            DisplayText = accTye.Code + "-" + accTye.Name
                        });
                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        public async Task<List<ComboboxItemDto>> GetCustomers()
        {
            try
            {
                var output = new List<ComboboxItemDto>();

                var token = await GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in await ContactXeroCrud.GetAll(token))
                    {
                        var dText = accTye.Name;
                        if (string.IsNullOrEmpty(dText)) dText = accTye.FirstName + " " + accTye.LastName;
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.ContactID,
                            DisplayText = dText
                        });
                    }

                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        #region Private Methods

        public string GetGatewayUrl()
        {
            return _appConfiguration["DineConnectGatewayUrl"];
        }

        private void LogXero(Exception e)
        {
            var myLog = new ExternalLog
            {
                AuditType = (int)ExternalLogType.Xero,
                LogData = e.Message,
                LogDescription = FlattenException(e),
                LogTime = DateTime.Now,
                ExternalLogTrunc = DateTime.Now.Date
            };
            _logRepository.InsertAndGetId(myLog);
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }


        /// <summary>
        ///     Gets the current xero token, refreshes if expired
        /// </summary>
        /// <returns></returns>
        private async Task<XeroToken> GetXeroTokenForTenant()
        {
            try
            {
                var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (xeroAddon != null)
                {
                    var settings = xeroAddon.Settings;
                    var token = JsonConvert.DeserializeObject<XeroToken>(settings);

                    //Refresh if expired
                    if (token.IsExpired) token = await RefreshTokenAndSave(token);

                    return token;
                }
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        #endregion
    }
}

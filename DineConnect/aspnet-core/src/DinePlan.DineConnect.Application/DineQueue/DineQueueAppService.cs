﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Timing;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.DineQueue.Dtos;
using DinePlan.DineConnect.Net.Sms;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.DineQueue
{
    
    public class DineQueueAppService : DineConnectAppServiceBase, IDineQueueAppService
    {
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<QueueLocation> _queueLocationRepo;
        private readonly IRepository<QueueLocationOption> _queueLocationOptionRepo;
        private readonly IRepository<CustomerQueue> _queueCustomerRepo;
        private readonly TwilioSmsSender _twilioSmsSender;

        public DineQueueAppService(
            IRepository<Location> locationRepo,
            IRepository<QueueLocation> queueLocationRepo,
            IRepository<QueueLocationOption> queueLocationOptionRepo,
            IRepository<CustomerQueue> queueCustomerRepo,
            TwilioSmsSender twilioSmsSender
            )
        {
            _locationRepo = locationRepo;
            _queueLocationRepo = queueLocationRepo;
            _queueLocationOptionRepo = queueLocationOptionRepo;
            _queueCustomerRepo = queueCustomerRepo;
            _twilioSmsSender = twilioSmsSender;
        }

        [AbpAllowAnonymous]
        public async Task<QueueLocationDto> GetQueueLocationFullDataByLocationId(int locationId)
        {
            var queueLocationId = await _locationRepo.GetAll().Where(x => x.Id == locationId).Select(x => x.QueueLocationId).FirstOrDefaultAsync();

            var queueLocation = await _queueLocationRepo.GetAll()
                .Include(x => x.QueueLocationOptions)
                .ThenInclude(x => x.CustomerQueues)
                .ThenInclude(x => x.Customer)
                .FirstOrDefaultAsync(x => x.Id == queueLocationId);

            var queueLocationDto = ObjectMapper.Map<QueueLocationDto>(queueLocation);

            // Customer Queue filter by Today
            if (queueLocationDto?.QueueLocationOptions != null)
            {
                foreach (var queueLocationQueueLocationOption in queueLocationDto.QueueLocationOptions)
                {
                    queueLocationQueueLocationOption.CustomerQueues = queueLocationQueueLocationOption.CustomerQueues
                        ?.Where(x => x.CreationTime.Date == Clock.Now.Date).ToList();
                }

                // Load location ids
                queueLocationDto.LocationIds = _locationRepo.GetAll()
                    .Where(x => x.QueueLocationId == queueLocationDto.Id)
                    .Select(x => x.Id).ToList();
            }

            return queueLocationDto;
        }

        public async Task<ListResultDto<QueueLocationDto>> GetQueueLocationList()
        {
            var queueLocations = await _queueLocationRepo.GetAll()
                .Include(x => x.QueueLocationOptions)
                .ThenInclude(x => x.CustomerQueues)
                .ThenInclude(x => x.Customer)
                .ToListAsync();

            var queueLocationDtoList = ObjectMapper.Map<List<QueueLocationDto>>(queueLocations);

            foreach (var queueLocationDto in queueLocationDtoList)
            {
                // Load location ids
                queueLocationDto.LocationIds = _locationRepo.GetAll().Where(x => x.QueueLocationId == queueLocationDto.Id)
                    .Select(x => x.Id).ToList();

                if (queueLocationDto.QueueLocationOptions != null)
                {
                    foreach (var queueLocationQueueLocationOption in queueLocationDto.QueueLocationOptions)
                    {
                        queueLocationQueueLocationOption.CustomerQueues = queueLocationQueueLocationOption.CustomerQueues?.Where(x => x.CreationTime.Date == Clock.Now.Date).ToList();
                    }
                }
            }

            return new ListResultDto<QueueLocationDto>(queueLocationDtoList);
        }

        public async Task<QueueLocationDto> GetQueueLocationFullDataById(int id)
        {
            var queueLocation = await _queueLocationRepo.GetAll()
                .Include(x => x.QueueLocationOptions)
                .ThenInclude(x => x.CustomerQueues)
                .ThenInclude(x => x.Customer)
                .FirstOrDefaultAsync(x => x.Id == id);

            var queueLocationDto = ObjectMapper.Map<QueueLocationDto>(queueLocation);

            if (queueLocationDto.QueueLocationOptions != null)
            {
                foreach (var queueLocationQueueLocationOption in queueLocationDto.QueueLocationOptions)
                {
                    queueLocationQueueLocationOption.CustomerQueues = queueLocationQueueLocationOption.CustomerQueues?.Where(x => x.CreationTime.Date == Clock.Now.Date).ToList();
                }
            }

            // Load location ids
            queueLocationDto.LocationIds = _locationRepo.GetAll().Where(x => x.QueueLocationId == queueLocationDto.Id)
                .Select(x => x.Id).ToList();

            return queueLocationDto;
        }

        public async Task CreateOrUpdateQueueLocation(QueueLocationDto input)
        {
            var queueLocation = ObjectMapper.Map<QueueLocation>(input);
            queueLocation.TenantId = AbpSession.TenantId ?? 0;

            if (queueLocation.TenantId == 0)
                return;

            var myQuId = await _queueLocationRepo.InsertOrUpdateAndGetIdAsync(queueLocation);

            var locationsUsedQueueLocation = _locationRepo.GetAll().Where(x => x.QueueLocationId == input.Id).ToList();
            foreach (var location in locationsUsedQueueLocation)
            {
                location.QueueLocationId = null;
                await _locationRepo.UpdateAsync(location);
            }
            await CurrentUnitOfWork.SaveChangesAsync();

            if (input.LocationIds?.Any() == true)
            {
                var locationsToAddQueueLocation = _locationRepo.GetAll().Where(x => input.LocationIds.Contains(x.Id)).ToList();
                foreach (var location in locationsToAddQueueLocation)
                {
                    location.QueueLocationId = myQuId;
                    await _locationRepo.UpdateAsync(location);
                }
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        public async Task DeleteQueueLocation(EntityDto input)
        {
            await _queueLocationRepo.DeleteAsync(input.Id);

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }

        public async Task CreateOrUpdateQueueOption(QueueLocationOptionDto input)
        {
            var queueLocationOption = ObjectMapper.Map<QueueLocationOption>(input);

            await _queueLocationOptionRepo.InsertOrUpdateAndGetIdAsync(queueLocationOption);

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }

        public async Task DeleteQueueOption(EntityDto input)
        {
            await _queueLocationOptionRepo.DeleteAsync(input.Id);

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }

        public async Task CreateOrUpdateCustomerQueue(CustomerQueueDto input)
        {
            var queueCustomer = ObjectMapper.Map<CustomerQueue>(input);

            queueCustomer.TenantId = AbpSession.TenantId ?? 0;

            if (queueCustomer.Id == default)
            {
                var queueOption = await _queueLocationOptionRepo.GetAll().FirstOrDefaultAsync(x => x.Id == input.QueueLocationOptionId);

                // Queue No

                var totalQueue = _queueCustomerRepo.GetAll().Count(x => x.QueueLocationOptionId == input.QueueLocationOptionId && x.CreationTime.Date == Clock.Now.Date);

                totalQueue++;

                queueCustomer.QueueNo = $"{queueOption.Prefix}{totalQueue:D3}";

                queueCustomer.QueueTime = Clock.Now;

                queueCustomer.Seated = false;

                queueCustomer.CallTime = null;

                queueCustomer.FinishTime = null;
            }
            else
            {
                if (queueCustomer.CallTime != null)
                {
                    try
                    {
                        await _twilioSmsSender.SendAsync(queueCustomer.CustomerMobile, L("YourTurn"));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);

                        // Ignore
                    }
                }
            }

            await _queueCustomerRepo.InsertOrUpdateAndGetIdAsync(queueCustomer);

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }

        public async Task DeleteCustomerQueue(EntityDto input)
        {
            await _queueCustomerRepo.DeleteAsync(input.Id);

            await UnitOfWorkManager.Current.SaveChangesAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.MultiTenancy.Payments;
using DinePlan.DineConnect.Sessions.Dto;
using DinePlan.DineConnect.UiCustomization;
using DinePlan.DineConnect.Authorization.Delegation;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Sessions
{
    public class SessionAppService : DineConnectAppServiceBase, ISessionAppService
    {
        private readonly IUiThemeCustomizerFactory _uiThemeCustomizerFactory;
        private readonly ISubscriptionPaymentRepository _subscriptionPaymentRepository;
        private readonly IUserDelegationConfiguration _userDelegationConfiguration;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<ConnectGuest> _guestRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public SessionAppService(
            IUiThemeCustomizerFactory uiThemeCustomizerFactory, IUnitOfWorkManager unitOfWorkManager,
            ISubscriptionPaymentRepository subscriptionPaymentRepository,
            IUserDelegationConfiguration userDelegationConfiguration,
            ITenantSettingsAppService tenantSettingsAppService,IRepository<ConnectGuest> guestRepo,
            IRepository<Customer> customerRepo)
        {
            _uiThemeCustomizerFactory = uiThemeCustomizerFactory;
            _subscriptionPaymentRepository = subscriptionPaymentRepository;
            _userDelegationConfiguration = userDelegationConfiguration;
            _tenantSettingsAppService = tenantSettingsAppService;
            _customerRepo = customerRepo;
            _guestRepo = guestRepo;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                Application = new ApplicationInfoDto
                {
                    Version = AppVersionHelper.Version,
                    ReleaseDate = AppVersionHelper.ReleaseDate,
                    Features = new Dictionary<string, bool>(),
                    Currency = DineConnectConsts.Currency,
                    CurrencySign = DineConnectConsts.CurrencySign,
                    AllowTenantsToChangeEmailSettings = DineConnectConsts.AllowTenantsToChangeEmailSettings,
                    UserDelegationIsEnabled = _userDelegationConfiguration.IsEnabled
                }
            };

            var uiCustomizer = await _uiThemeCustomizerFactory.GetCurrentUiCustomizer();
            output.Theme = await uiCustomizer.GetUiSettings();

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = ObjectMapper
                    .Map<TenantLoginInfoDto>(await TenantManager
                        .Tenants
                        .Include(t => t.Edition)
                        .FirstAsync(t => t.Id == AbpSession.GetTenantId()));
            }

            if (AbpSession.ImpersonatorTenantId.HasValue)
            {
                output.ImpersonatorTenant = ObjectMapper
                    .Map<TenantLoginInfoDto>(await TenantManager
                        .Tenants
                        .Include(t => t.Edition)
                        .FirstAsync(t => t.Id == AbpSession.ImpersonatorTenantId));
            }

            if (AbpSession.UserId.HasValue)
            {
                output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
            }

            if (AbpSession.ImpersonatorUserId.HasValue)
            {
                output.ImpersonatorUser = ObjectMapper.Map<UserLoginInfoDto>(await GetImpersonatorUserAsync());
            }

            if (output.Tenant == null)
            {
                return output;
            }

            if (output.Tenant.Edition != null)
            {
                var lastPayment = await _subscriptionPaymentRepository.GetLastCompletedPaymentOrDefaultAsync(output.Tenant.Id, null, null);
                if (lastPayment != null)
                {
                    output.Tenant.Edition.IsHighestEdition = IsEditionHighest(output.Tenant.Edition.Id, lastPayment.GetPaymentPeriodType());
                }
            }

            if (output.Tenant != null)
            {
                var settings = await _tenantSettingsAppService.GetAllSettings();

                output.Tenant.TiffinPopup = settings.TiffinPopup;
                output.Tenant.TiffinAllowOrderInFutureDays = settings.TiffinAllowOrderInFutureDays;
                output.Tenant.TiffinCutOffDays = settings.TiffinCutOffDays;
                output.Tenant.IsActiveDelivery  = settings.TiffinIsEnableDelivery;
            }

            output.Tenant.SubscriptionDateString = GetTenantSubscriptionDateString(output);
            output.Tenant.CreationTimeString = output.Tenant.CreationTime.ToString("d");

            // Customer Id

            if (output.User != null)
            {
                output.CustomerId = _customerRepo.GetAll().Where(x => x.UserId == output.User.Id).Select(x => x.Id)
                    .FirstOrDefault();
            }

            return output;
        }

        
        [DisableAuditing]
        public async Task<GetWheelCustomerLoginInformationsOutput> GetWheelLoginInformations(Guid? customerId)
        {
            var output = new GetWheelCustomerLoginInformationsOutput();
            var myTenant = AbpSession.TenantId ?? 1;
            using (_unitOfWorkManager.Current.SetTenantId(myTenant))
            {
                if (AbpSession.UserId.HasValue)
                {
                    output.UserId = AbpSession.UserId.Value;

                    var customer = await _customerRepo.GetAll().FirstOrDefaultAsync(t => t.UserId == output.UserId);
                    if (customer != null)
                    {
                        output.Name = customer.Name;
                        output.PhoneNumber = customer.PhoneNumber;
                        output.EmailAddress = customer.EmailAddress;
                    }
                }
                else
                {
                    if (customerId == null)
                    {
                        if (AbpSession.TenantId != null)
                        {
                            var currentCustomer = new ConnectGuest()
                            {
                                CustomerGuid = Guid.NewGuid(),
                                TenantId = myTenant,
                            };

                            var newCustomerId = await _guestRepo.InsertAndGetIdAsync(currentCustomer);
                            await CurrentUnitOfWork.SaveChangesAsync();
                            output.CustomerGuid = currentCustomer.CustomerGuid.ToString();
                        }
                        else
                        {
                            return output;
                        }
                    }
                    else
                    {
                        var guestCustomer = await _guestRepo.GetAll()
                            .FirstOrDefaultAsync(t => t.CustomerGuid == customerId);
                        if (guestCustomer != null)
                        {
                            output.Name = guestCustomer.Name;
                            output.PhoneNumber = guestCustomer.PhoneNumber;
                            output.EmailAddress = guestCustomer.EmailAddress;
                            output.CustomerGuid = customerId.Value.ToString();
                        }
                        else
                        {
                            var customer = await _customerRepo.GetAll()
                                .FirstOrDefaultAsync(t => t.CustomerGuid == customerId);
                            if (customer != null)
                            {
                                output.Name = customer.Name;
                                output.PhoneNumber = customer.PhoneNumber;
                                output.EmailAddress = customer.EmailAddress;
                                output.CustomerGuid = customerId.Value.ToString();
                            }
                            else
                            {
                                var currentCustomer = new ConnectGuest()
                                {
                                    CustomerGuid = customerId,
                                    TenantId = myTenant,
                                };
                                var newCustomerId = await _guestRepo.InsertAndGetIdAsync(currentCustomer);
                                await CurrentUnitOfWork.SaveChangesAsync();
                                output.CustomerGuid = currentCustomer.CustomerGuid.ToString();
                            }
                        }
                    }
                }
            }

            return output;
        }

        private bool IsEditionHighest(int editionId, PaymentPeriodType paymentPeriodType)
        {
            var topEdition = GetHighestEditionOrNullByPaymentPeriodType(paymentPeriodType);
            if (topEdition == null)
            {
                return false;
            }

            return editionId == topEdition.Id;
        }

        private SubscribableEdition GetHighestEditionOrNullByPaymentPeriodType(PaymentPeriodType paymentPeriodType)
        {
            var editions = TenantManager.EditionManager.Editions;
            if (editions == null || !editions.Any())
            {
                return null;
            }

            var query = editions.Cast<SubscribableEdition>();

            switch (paymentPeriodType)
            {
                case PaymentPeriodType.Daily:
                    query = query.OrderByDescending(e => e.DailyPrice ?? 0); break;
                case PaymentPeriodType.Weekly:
                    query = query.OrderByDescending(e => e.WeeklyPrice ?? 0); break;
                case PaymentPeriodType.Monthly:
                    query = query.OrderByDescending(e => e.MonthlyPrice ?? 0); break;
                case PaymentPeriodType.Annual:
                    query = query.OrderByDescending(e => e.AnnualPrice ?? 0); break;
            }

            return query.FirstOrDefault();
        }

        private string GetTenantSubscriptionDateString(GetCurrentLoginInformationsOutput output)
        {
            return output.Tenant.SubscriptionEndDateUtc == null
                ? L("Unlimited")
                : output.Tenant.SubscriptionEndDateUtc?.ToString("d");
        }

        public async Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken()
        {
            if (AbpSession.UserId <= 0)
            {
                throw new Exception(L("ThereIsNoLoggedInUser"));
            }

            var user = await UserManager.GetUserAsync(AbpSession.ToUserIdentifier());
            user.SetSignInToken();
            return new UpdateUserSignInTokenOutput
            {
                SignInToken = user.SignInToken,
                EncodedUserId = Convert.ToBase64String(Encoding.UTF8.GetBytes(user.Id.ToString())),
                EncodedTenantId = user.TenantId.HasValue
                    ? Convert.ToBase64String(Encoding.UTF8.GetBytes(user.TenantId.Value.ToString()))
                    : ""
            };
        }

        protected virtual async Task<User> GetImpersonatorUserAsync()
        {
            using (CurrentUnitOfWork.SetTenantId(AbpSession.ImpersonatorTenantId))
            {
                var user = await UserManager.FindByIdAsync(AbpSession.ImpersonatorUserId.ToString());
                if (user == null)
                {
                    throw new Exception("User not found!");
                }

                return user;
            }
        }
    }
}

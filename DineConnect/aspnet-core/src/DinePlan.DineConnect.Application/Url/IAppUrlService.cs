﻿namespace DinePlan.DineConnect.Url
{
    public interface IAppUrlService
    {
        string CreateEmailActivationUrlFormat(int? tenantId);

        string CreatePasswordResetUrlFormat(int? tenantId);

        string CreatePasswordResetUrlFormatTiffin(int? tenantId);

        string CreateEmailActivationUrlFormat(string tenancyName);

        string CreatePasswordResetUrlFormat(string tenancyName);

        string CreatePasswordResetUrlFormatTiffin(string tenancyName);
    }
}

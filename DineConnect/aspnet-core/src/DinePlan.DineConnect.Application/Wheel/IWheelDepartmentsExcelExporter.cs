﻿using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelDepartmentsExcelExporter
    {
        FileDto ExportToFile(List<GetWheelDepartmentForViewDto> wheelDepartments);
    }
}
﻿namespace DinePlan.DineConnect.Wheel.Dto
{
    public class CheckSlotOrderResult
    {
        public int Result { get; set; }
    }
}
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using System.Threading.Tasks;
using DinePlan.DineConnect.Wheel.ShippingProviders.GogoVan;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.GogoVan;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelTicketShippingOrderAppService : IApplicationService
    {
        Task LalaMoveWebhook(int? tenantId, LalaMoveResponWebhook inputRespon);

        Task GoGoVanWebhook(int? tenantId, GoGoVanResponWebhook inputRespon);

        Task DunzoWebhook(int? tenantId, DunzoTaskStatusRepsonse inputRespon);

        Task<string> CreateWheelTicketShippingOrder(CreateWheelTicketShippingOrderInputDto input);
    }
}
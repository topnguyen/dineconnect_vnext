﻿using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelPaymentMethodsExcelExporter
    {
        FileDto ExportToFile(List<GetWheelPaymentMethodForViewDto> wheelPaymentMethods);
    }
}
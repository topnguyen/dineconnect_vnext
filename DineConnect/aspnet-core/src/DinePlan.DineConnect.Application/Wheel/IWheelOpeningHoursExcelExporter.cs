﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelOpeningHoursExcelExporter
    {
        Task<FileDto> ExportToFile(List<WheelOpeningHourDto> wheelOpeningHours);
    }
}

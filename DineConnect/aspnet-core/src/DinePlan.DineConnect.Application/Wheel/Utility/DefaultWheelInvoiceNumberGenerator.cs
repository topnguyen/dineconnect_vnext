using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Timing;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Utility
{
    public class DefaultWheelInvoiceNumberGenerator : IWheelInvoiceNumberGenerator
    {
        private readonly IRepository<WheelPayment> _invoiceRepository;

        public DefaultWheelInvoiceNumberGenerator(IRepository<WheelPayment> invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        [UnitOfWork]
        public async Task<string> GetNewInvoiceNumber()
        {
            var lastInvoice = await _invoiceRepository.GetAll().OrderByDescending(i => i.Id).FirstOrDefaultAsync();
            if (lastInvoice == null)
            {
                return Clock.Now.Year + "" + (Clock.Now.Month).ToString("00") + "00001";
            }

            var nyear = lastInvoice.InvoiceNo.Substring(0, 4);
            var nmonth =lastInvoice.InvoiceNo.Substring(4, 2);

            int year;
            bool isYear =   int.TryParse(nyear, out year);

            int month;
            bool isMonth =   int.TryParse(nmonth, out month);

            if (!isYear)
            {
                return Guid.NewGuid().ToString("N");
            }

            var invoiceNumberToIncrease = lastInvoice.InvoiceNo.Substring(6, lastInvoice.InvoiceNo.Length - 6);
            if (year != Clock.Now.Year || month != Clock.Now.Month)
            {
                invoiceNumberToIncrease = "0";
            }

            var invoiceNumberPostfix = Convert.ToInt32(invoiceNumberToIncrease) + 1;
            return Clock.Now.Year + (Clock.Now.Month).ToString("00") + invoiceNumberPostfix.ToString("00000");
        }
    }
}
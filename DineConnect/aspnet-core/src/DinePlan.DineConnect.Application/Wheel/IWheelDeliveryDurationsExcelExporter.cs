﻿using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelDeliveryDurationsExcelExporter
    {
        FileDto ExportToFile(List<GetWheelDeliveryDurationForViewDto> wheelDeliveryDurations);
    }
}
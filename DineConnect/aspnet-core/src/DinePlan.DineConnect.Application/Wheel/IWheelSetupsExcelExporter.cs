﻿using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelSetupsExcelExporter
    {
        FileDto ExportToFile(List<GetWheelSetupForViewDto> wheelSetups);
    }
}
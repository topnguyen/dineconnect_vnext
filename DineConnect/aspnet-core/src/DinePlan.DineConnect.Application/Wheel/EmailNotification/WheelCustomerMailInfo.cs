﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.EmailNotification
{
    public class WheelCustomerMailInfo
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Wheel.EmailNotification
{
    public class WheelEmailNames
    {
        public const string BUY_ORDER = "Order";
        public const string LIST_ORDER = "List_Order";
    }
}

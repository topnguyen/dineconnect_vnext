using Abp.Application.Services;
using DinePlan.DineConnect.Wheel.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelOrderTrackingAppService : IApplicationService
    {
        Task CreateOrUpdateWheelOrderTracking(WheelOrderTrackingDto input);

        Task<WheelOrderTracking> GetWheelOrderTrackingByWheelDepartmentTimeSlotId(WheelOrderTrackingDto input);
    }
}
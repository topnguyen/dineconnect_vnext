﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Wheel.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelRecommendationMenusAppService : DineConnectAppServiceBase, IWheelRecommendationMenusAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly ILogger _logger;
        private readonly IRedisCacheService _redisCacheService;
        private readonly IRepository<WheelRecommendationMenu> _recommendationMenuRepository;
        private readonly IRepository<WheelRecommendationDayPeriod> _dayPeriodRepository;
        private readonly ISettingManager _settingManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public WheelRecommendationMenusAppService(
            ISettingManager settingManager,
            IRepository<WheelRecommendationMenu> recommendationMenuRepository,
            IRepository<WheelRecommendationDayPeriod> dayPeriodRepository,
            IRedisCacheService redisCacheService,
            ILogger logger,
            IUnitOfWorkManager unitOfWorkManager,
            ConnectSession connectSession)
        {
            _settingManager = settingManager;
            _recommendationMenuRepository = recommendationMenuRepository;
            _redisCacheService = redisCacheService;
            _logger = logger;
            _unitOfWorkManager = unitOfWorkManager;
            _connectSession = connectSession;
            _dayPeriodRepository = dayPeriodRepository;
        }

        public async Task<PagedResultDto<WheelRecommendationMenuDto>> GetAll(GetWheelScreenMenusInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var query = _recommendationMenuRepository.GetAll();

            var allRecommendationMenus = await query
                .Include(x => x.WheelRecommendationItems)
                .Include(x => x.WheelRecommendationDayPeriods)
                .ToListAsync();

            var allWheelProductItemList = ObjectMapper.Map<List<WheelRecommendationMenuDto>>(allRecommendationMenus);

            await _redisCacheService.SetAsync(AppConsts.GetAllWheelRecommendationMenuKey + "_" + tenantId, allWheelProductItemList);

            allWheelProductItemList = allWheelProductItemList
                .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                .OrderBy(x => x.Id)
                .ToList();

            var wheelScreenMenuCount = allWheelProductItemList.Count();

            var response = new PagedResultDto<WheelRecommendationMenuDto>(wheelScreenMenuCount, allWheelProductItemList);

            return response;
        }

        
        public async Task<int> CreateOrEditWheelRecommendationMenu(WheelRecommendationMenuDto input)
        {
            var tenantId = AbpSession.GetTenantId();
            var newRecommendationMenu = ObjectMapper.Map<WheelRecommendationMenu>(input);
            newRecommendationMenu.TenantId = tenantId;

            var newId = await _recommendationMenuRepository.InsertOrUpdateAndGetIdAsync(newRecommendationMenu);
            return newId;
        }

        public async Task<WheelRecommendationMenuDto> GetWheelRecommendationMenuForEdit(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            var data = await _recommendationMenuRepository
                .GetAll()
                .Include(x => x.WheelRecommendationDayPeriods)
                .Include(x => x.WheelRecommendationItems)
                .FirstOrDefaultAsync(s =>
                s.Id == input.Id && s.TenantId == tenantId);

            var wheelScreenMenuDto = ObjectMapper.Map<WheelRecommendationMenuDto>(data);

            await _redisCacheService.SetAsync(AppConsts.GetWheelRecommendationMenuForEditKey + "_" + input.Id, wheelScreenMenuDto);

            return wheelScreenMenuDto;
        }

        public async Task<WheelRecommendationDayPeriodDto> GetDayPeriodById(EntityDto input)
        {
            var data = await _dayPeriodRepository.FirstOrDefaultAsync(s => s.Id == input.Id);

            var result = ObjectMapper.Map<WheelRecommendationDayPeriodDto>(data);
            return result;
        }

        public async Task<int> CreateOrEditDayPeriod(WheelRecommendationDayPeriodDto input)
        {
            var entity = ObjectMapper.Map<WheelRecommendationDayPeriod>(input);
            var newId = await _dayPeriodRepository.InsertOrUpdateAndGetIdAsync(entity);
            return newId;
        }

        
        public async Task UpdateDisable(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            var entity = await _recommendationMenuRepository.SingleAsync(s => s.Id == input.Id && s.TenantId == tenantId);

            if (entity != null)
            {
                entity.Disable = !entity.Disable;
                await _recommendationMenuRepository.UpdateAsync(entity);
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            await _recommendationMenuRepository.DeleteAsync(s => s.Id == input.Id && s.TenantId == tenantId);

            await RemoveCache(input.Id);
        }

        private async Task RemoveCache(int id = 0)
        {
            var tenantId = AbpSession.GetTenantId();

            await _redisCacheService.RemoveAsync(AppConsts.GetAllWheelScreenMenuKey + "_" + tenantId,
                $"{AppConsts.GetWheelRecommendationMenuForEditKey}_{id}");
        }

    }
}
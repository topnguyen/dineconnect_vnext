﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    [AbpAllowAnonymous]
    public class WheelOrderTrackingAppService : DineConnectAppServiceBase, IWheelOrderTrackingAppService
    {
        private readonly IRepository<WheelOrderTracking> _wheelOrderTrackingRepository;
        private readonly IRepository<WheelDepartmentTimeSlot> _wheelDepartmentTimeSlotRepository;
        public WheelOrderTrackingAppService(
            IRepository<WheelOrderTracking> wheelOrderTrackingRepository,
            IRepository<WheelDepartmentTimeSlot> wheelDepartmentTimeSlotRepository

        )
        {
            _wheelOrderTrackingRepository = wheelOrderTrackingRepository;
            _wheelDepartmentTimeSlotRepository = wheelDepartmentTimeSlotRepository;
        }

        public async Task CreateOrUpdateWheelOrderTracking(WheelOrderTrackingDto input)
        {
            var orderTrackingExists = await GetWheelOrderTrackingByWheelDepartmentTimeSlotId(input);

            input.TenantId = AbpSession.TenantId;

            if (orderTrackingExists == null)
            {
                var orderTrackingInsert = ObjectMapper.Map<WheelOrderTracking>(input);
                orderTrackingInsert.CurrentCount = 1;
                await _wheelOrderTrackingRepository.InsertAsync(orderTrackingInsert);
            }
            else
            {
                orderTrackingExists.CurrentCount += 1;
                await _wheelOrderTrackingRepository.UpdateAsync(orderTrackingExists);
            }
        }

        public async Task<WheelOrderTracking> GetWheelOrderTrackingByWheelDepartmentTimeSlotId(WheelOrderTrackingDto input)
        {
            var result = await _wheelOrderTrackingRepository
                .FirstOrDefaultAsync(x => x.WheelDepartmentTimeSlotId == input.WheelDepartmentTimeSlotId && x.OrderDate == input.OrderDate);

            return result;
        }
    }
}
﻿using System;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using DinePlan.DineConnect.Wheel.EmailNotification.Dtos;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Email;

namespace DinePlan.DineConnect.Wheel.EmailNotification
{
    public class WheelEmailNotificationAppService : DineConnectAppServiceBase, IWheelEmailNotificationAppService
    {
        private readonly ITenantEmailSendAppService _emailAppService;
        private readonly IRepository<WheelLocation> _locationRepository;

        public WheelEmailNotificationAppService(
            ITenantEmailSendAppService emailAppService,
            IRepository<WheelLocation> locationRepository)
        {
            _emailAppService = emailAppService;
            _locationRepository = locationRepository;
        }

        public async Task SendEmailWhenCustomerOrder(WheelOrderSendEmailInput input)
        {
            try
            {
                var templateContent = await _emailAppService.GetEmailTemplateContentAsync(input.TenantId, WheelEmailNames.LIST_ORDER);

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(templateContent);

            var orderListTable = htmlDoc.DocumentNode.SelectSingleNode("//table");

            var body = orderListTable.SelectSingleNode("//tbody");
            if (body != null)
            {
                var fistTr = body.SelectSingleNode("//tr").OuterHtml;
                body.SelectSingleNode("//tr").Remove();
                var secondTr = body.SelectSingleNode("//tr").OuterHtml;

                orderListTable.FirstChild.RemoveAllChildren();

                var tableTotal = new StringBuilder(fistTr);

                foreach (var order in input.OrderItems)
                {
                    var sb = new StringBuilder(secondTr);

                    List<string> items = new List<string>(new[] { order.ItemName });

                    if (order.IsCombo)
                    {
                        foreach (var subItem in order.SubItems)
                        {
                            items.Add(
                                $"<span style='margin-left:10px'>{subItem.Quantity}x {subItem.ItemName}</span>");

                            if (subItem.Tags != null)
                            {
                                foreach (var tag in subItem.Tags)
                                {
                                    items.Add($"<span style='margin-left:20px'>{tag.Name}  x{tag.Quantity}</span>");
                                }
                            }
                        }
                    }

                    sb.Replace("{{Item_Data}}", string.Join("<br/>", items.ToArray()));
                    sb.Replace("{{Quantity_Data}}", order.Quantity.ToString());
                    sb.Replace("{{Price_Data}}", string.Format("{0:0.00}", order.SubTotal));
                    sb.Replace("{{PackSize_Data}}", order?.Portion?.Name.ToString());
                    sb.Replace("{{Note_Data}}", order?.Note);

                    var lstAddonString = new List<string>();

                    if (!order.IsCombo)
                    {
                        if (order.Tags != null)
                        {
                            foreach (var tag in order.Tags)
                            {
                                lstAddonString.Add(tag.Name + " x" + tag.Quantity.ToString());
                            }
                        }
                    }

                    sb.Replace("{{Addon_Data}}", string.Join("<br/>", lstAddonString));

                    tableTotal.Append(sb.ToString());
                }

                orderListTable.InnerHtml = tableTotal.ToString();

                //send to customer
                var toAddress = new List<string>(new[] { input.DeliveryAddress.EmailAddress });

                //also send to location email
                if (input.DeliveryAddress.WheelLocationId > 0)
                {
                    var location = await _locationRepository
                        .GetAllIncluding(t => t.Location)
                        .FirstOrDefaultAsync(t => t.Id == input.DeliveryAddress.WheelLocationId);

                    if (location != null && !string.IsNullOrEmpty(location.Email))
                    {
                        toAddress.Add(location.Email);
                    }
                    else if (location != null && !string.IsNullOrEmpty(location.Location.Email))
                    {
                        toAddress.Add(location.Location.Email);
                    }
                }

                var sendEmail = new SendEmailInput
                {
                    TenantId = input.TenantId,
                    EmailAddress = string.Join(",", toAddress.ToArray()),
                    Module = WheelEmailNames.BUY_ORDER,
                    Variables = new
                    {
                        First_name = input.DeliveryAddress.Name,
                        Delivery_Address = input.DeliveryAddress.FullAddress,
                        Mobile_No = input.DeliveryAddress.PhoneNumber,

                        Full_Name = input.DeliveryAddress.Name,
                        Email_address = input.DeliveryAddress.EmailAddress,
                        Phone_number = input.DeliveryAddress.PhoneNumber,
                        Address1 = input.DeliveryAddress.Address1,
                        Address2 = input.DeliveryAddress.Address2,
                        Address3 = input.DeliveryAddress.Address3,
                        City = input.DeliveryAddress.City,
                        Country = input.DeliveryAddress.Country,
                        Postcal_code = input.DeliveryAddress.PostcalCode,
                        Note = input.Order.Note,
                        Zone = input.DeliveryAddress.Zone?.ToString(),
                        Sub_Total = $"{input.Order.SubTotal:0.00}",
                        Total = $"{input.Order.Total:0.00}",
                        DeliveryFee = $"{input.DeliveryAddress.DeliveryFee:0.00}",
                        OrderTime = input.Order.DeliveryTime,
                        List_Order = orderListTable.OuterHtml
                    }
                };

                await _emailAppService.SendEmailByTemplateAsync(sendEmail);
            }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
    }
}
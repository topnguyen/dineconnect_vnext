﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.Timing;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class WheelDepartmentsAppService : DineConnectAppServiceBase, IWheelDepartmentsAppService
    {
        private readonly IRepository<DinePlanTaxLocation> _dinePlanTaxLocationRepository;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;
        private readonly IRepository<WheelDeliveryZone, int> _wheelDeliveryZoneRepository;
        private readonly IRepository<WheelServiceFee, int> _wheelServiceFeeRepository;
        private readonly IRepository<PriceTag, int> _lookupPriceTagRepository;
        private readonly IPaymentService _paymentService;
        private readonly IRepository<WheelDepartment> _wheelDepartmentRepository;
        private readonly IRepository<WheelDepartmentTimeSlot> _wheelDepartmentTimeSlotRepository;
        private readonly IRepository<WheelOpeningHour> _wheelOpeningHourRepository;
        private readonly IWheelDepartmentsExcelExporter _wheelDepartmentsExcelExporter;
        private readonly IRepository<WheelTax, int> _wheelTaxRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILocationAppService _locationAppService;


        public WheelDepartmentsAppService(IRepository<WheelDepartment> wheelDepartmentRepository,
            IRepository<DinePlanTaxLocation> dinePlanTaxLocationRepository,
            IWheelDepartmentsExcelExporter wheelDepartmentsExcelExporter,
            IRepository<PriceTag, int> lookupPriceTagRepository,
            IRepository<WheelLocation> wheelLocationRepository,
            IRepository<WheelDeliveryZone, int> wheelDeliveryZoneRepository,
            IRepository<WheelServiceFee, int> wheelServiceFeeRepository,
            IRepository<WheelDepartmentTimeSlot> wheelDepartmentTimeSlotRepository,
            IRepository<WheelOpeningHour> wheelOpeningHourRepository,
            IPaymentService paymentService,ILocationAppService locationAppService,
            IRepository<WheelTax, int> wheelTaxRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ITimeZoneService timeZoneService)
        {
            _wheelDepartmentRepository = wheelDepartmentRepository;
            _dinePlanTaxLocationRepository = dinePlanTaxLocationRepository;
            _wheelDepartmentsExcelExporter = wheelDepartmentsExcelExporter;
            _lookupPriceTagRepository = lookupPriceTagRepository;
            _wheelLocationRepository = wheelLocationRepository;
            _wheelDeliveryZoneRepository = wheelDeliveryZoneRepository;
            _wheelServiceFeeRepository = wheelServiceFeeRepository;
            _paymentService = paymentService;
            _wheelTaxRepository = wheelTaxRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _wheelDepartmentTimeSlotRepository = wheelDepartmentTimeSlotRepository;
            _wheelOpeningHourRepository = wheelOpeningHourRepository;
            _timeZoneService = timeZoneService;
            _locationAppService = locationAppService;
        }

        public async Task<PagedResultDto<GetWheelDepartmentForViewDto>> GetAll(GetAllWheelDepartmentsInput input)
        {
            var query = _wheelDepartmentRepository.GetAll()
                .Include(e => e.PriceTag)
                .Include(e => e.Menu)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                .WhereIf(input.WheelOrderTypeFilter != null, e => e.WheelOrderType == input.WheelOrderTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.PriceTagFilter),
                    e => e.PriceTag != null && e.PriceTag.Id.ToString() == input.PriceTagFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.MenuFilter),
                    e => e.Menu != null && e.Menu.Id.ToString() == input.MenuFilter)
                .WhereIf(input.Enable.HasValue, e => e.Enable == input.Enable.Value);

            if (!input.Sorting.IsNullOrEmpty())
                query = query.OrderBy(input.Sorting);

            var departmentsCount = await query.CountAsync();
            var departments = await query.PageBy(input).ToListAsync();
            var getWheelDepartmentForViewDtos = new List<GetWheelDepartmentForViewDto>();
            foreach (var wheelDepartment in departments)
            {
                var departmentForViewDto = ObjectMapper.Map<GetWheelDepartmentForViewDto>(wheelDepartment);
                if (string.IsNullOrEmpty(wheelDepartment.Dynamic) == false)
                {
                    dynamic itemDynamic = JObject.Parse(wheelDepartment.Dynamic);
                    departmentForViewDto.IsRequireOtp = itemDynamic.IsRequireOtp ?? 0;
                    departmentForViewDto.FutureOrderDaysLimit = itemDynamic.FutureOrderDaysLimit;
                    departmentForViewDto.OrderDeliveryTime = itemDynamic.OrderDeliveryTime;
                    departmentForViewDto.OrderPreparationTime = itemDynamic.OrderPreparationTime;
                }

                var openingHourIds = wheelDepartment.WheelOpeningHourIds?.Split(";");
                if (openingHourIds != null && openingHourIds.Length > 0)
                {
                    var openingHours = await _wheelOpeningHourRepository.GetAll()
                    .Where(x => openingHourIds.Contains(x.Id.ToString())).ToListAsync();
                    departmentForViewDto.OpeningHours = string.Join(",", openingHours.Select(r => r.Name));
                }

                getWheelDepartmentForViewDtos.Add(departmentForViewDto);
            }

            return new PagedResultDto<GetWheelDepartmentForViewDto>(
                departmentsCount,
                getWheelDepartmentForViewDtos
            );
        }

        
        public async Task<CreateOrEditWheelDepartmentDto> GetWheelDepartmentForEdit(EntityDto input)
        {
            var wheelDepartment = await _wheelDepartmentRepository.GetAll().FirstOrDefaultAsync(x => x.Id == input.Id);

            var output = ObjectMapper.Map<CreateOrEditWheelDepartmentDto>(wheelDepartment);
            if (string.IsNullOrEmpty(wheelDepartment.Dynamic) == false)
            {
                dynamic itemDynamic = JObject.Parse(wheelDepartment.Dynamic);
                output.IsRequireOtp = itemDynamic.IsRequireOtp ?? false;
                output.SendOtp = itemDynamic.SendOtp ?? false;
                output.FutureOrderDaysLimit = itemDynamic.FutureOrderDaysLimit;
                output.OrderDeliveryTime = itemDynamic.OrderDeliveryTime;
                output.OrderPreparationTime = itemDynamic.OrderPreparationTime;
            }

            output.WheelPaymentMethods = await GetListPaymentMethodFromSystemNames(wheelDepartment.WheelPaymentMethodIds);

            var serviceFeeIds = wheelDepartment.WheelServiceFeeIds?.Split(";");
            if (serviceFeeIds != null && serviceFeeIds.Length > 0)
            {
                output.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(await _wheelServiceFeeRepository.GetAll()
               .Where(x => serviceFeeIds.Contains(x.Id.ToString())).ToListAsync());
            }

            var openingHours = wheelDepartment.WheelOpeningHourIds?.Split(";");
            if (openingHours != null && openingHours.Length > 0)
            {
                output.WheelOpeningHours = ObjectMapper.Map<List<DinePlanOpeningHoursDto>>(await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHours.Contains(x.Id.ToString())).ToListAsync());
            }

            return output;
        }

        [AbpAllowAnonymous]
        public async Task<List<GetWheelDepartmentOutput>> GetWheelDepartments()
        {
            var tenantId = AbpSession.TenantId ?? 1;

            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);

            using (_unitOfWorkManager.Current.SetTenantId(tenantId))
            {
                var wheelDepartments = await _wheelDepartmentRepository.GetAll()
                 .Where(x => x.TenantId == tenantId)
                 .ToListAsync();
                var result = new List<GetWheelDepartmentOutput>();
                foreach (var wheelDepartment in wheelDepartments)
                {
                    var wheelLocations = await _wheelLocationRepository.GetAll()
                        .Include(x => x.Location)
                        .ThenInclude(x => x.City)
                        .ThenInclude(x => x.Country)
                        .Where(x => x.WheelDepartmentIds.Contains(wheelDepartment.Id.ToString()) && x.TenantId == tenantId)
                        .ToListAsync();

                    if (wheelLocations.Any() == false || wheelDepartment.Enable == false)
                    {
                        continue;
                    }

                    var output = ObjectMapper.Map<GetWheelDepartmentOutput>(wheelDepartment);
                    output.IsOpeningHour = await IsOpeningHour(wheelDepartment.Id);

                    if (string.IsNullOrEmpty(wheelDepartment.Dynamic) == false)
                    {
                        dynamic itemDynamic = JObject.Parse(wheelDepartment.Dynamic);
                        output.IsRequireOtp = itemDynamic.IsRequireOtp ?? false;
                        output.FutureOrderDaysLimit = itemDynamic.FutureOrderDaysLimit;
                    }

                    if ((output.IsOpeningHour == false || !output.IsEnableDelivery) && output.AllowPreOrder)
                    {
                        if (!output.FutureOrderDaysLimit.HasValue || output.FutureOrderDaysLimit == 0)
                        {
                            output.IsOpeningHour = false;
                        }
                        else
                        {
                            output.DateDefaultOrderFututer = now.Date;
                        }
                    }

                    output.CloseDayOfWeek = await GetCloseDayByDepartmentId(wheelDepartment.Id);

                    output.WheelPaymentMethods =
                        await GetListPaymentMethodFromSystemNames(wheelDepartment.WheelPaymentMethodIds);

                    output.WheelLocationOutputs = ObjectMapper.Map<List<GetWheelLocationOutput>>(wheelLocations);

                    var timeSlots = (await GetTimeSlotByDepartmentId(output.Id)).ToList().OrderBy(x => x.TimeFrom);
                    output.TimeSlots = ObjectMapper.Map<List<TimeSlotDto>>(timeSlots);
                    output.IsTimeSlotMode = output.TimeSlots.Any();

                    if (!output.WheelServiceFeeIds.IsNullOrWhiteSpace())
                    {
                        var wheelServiceFeeIds = output.WheelServiceFeeIds?.Split(";");
                        output.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(await _wheelServiceFeeRepository
                            .GetAll().Where(x => wheelServiceFeeIds.Contains(x.Id.ToString())).ToListAsync());

                        foreach (var fee in output.WheelServiceFees)
                        {
                            if (!fee.WheelTaxes.IsNullOrEmpty())
                            {
                                var taxes = JsonConvert.DeserializeObject<List<WheelTaxDto>>(fee.WheelTaxes);

                                var ids = taxes?.Select(t => t.DinePlanTaxId).ToList();

                                if (ids != null && ids.Any() && wheelLocations.Any())
                                {
                                    foreach (var wheelLocation in wheelLocations)
                                    {
                                        var orgId = await _locationAppService.GetOrgnizationIdByLocationId(wheelLocation.LocationId);
                                        if (orgId > 0)
                                        {
                                            CurrentUnitOfWork.SetFilterParameter(
                                                DineConnectDataFilters.MustHaveOrganization,
                                                DineConnectDataFilters.Parameters.OrganizationId, orgId);
                                            var allTaxes = _dinePlanTaxLocationRepository.GetAll()
                                                .Where(e => ids.Contains(e.DinePlanTaxRefId) && e.TenantId == tenantId).ToList();
                                            if (allTaxes.Any())
                                            {
                                                fee.ServiceTaxes.Add(wheelLocation.Id, allTaxes.Sum(a=>a.TaxPercentage));
                                            }
                                        }
                                    }
                                   
                                }
                            }
                        }
                    }
                    var openingHours = wheelDepartment.WheelOpeningHourIds?.Split(";");
                    if (openingHours != null && openingHours.Length > 0)
                    {
                        output.WheelOpeningHours = ObjectMapper.Map<List<DinePlanOpeningHoursDto>>(await _wheelOpeningHourRepository.GetAll()
                        .Where(x => openingHours.Contains(x.Id.ToString())).ToListAsync());
                    }

                    foreach (var wheelLocation in output.WheelLocationOutputs)
                    {
                        var wheelDeliveryZoneIds = wheelLocation.WheelDeliveryZoneIds?.Split(";");
                        wheelLocation.WheelDeliveryZones = ObjectMapper.Map<List<WheelDeliveryZoneDto>>(await _wheelDeliveryZoneRepository
                            .GetAll().Where(x => wheelDeliveryZoneIds.Contains(x.Id.ToString())).ToListAsync());

                        var wheelServiceFeeIds = wheelLocation.WheelServiceFeeIds?.Split(";");
                        wheelLocation.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(await _wheelServiceFeeRepository
                            .GetAll().Where(x => wheelServiceFeeIds.Contains(x.Id.ToString())).ToListAsync());

                        if (!wheelLocation.WheelTaxIds.IsNullOrEmpty())
                        {
                            var wheelTaxIds = wheelLocation.WheelTaxIds.Split(";");
                            wheelLocation.WheelTaxes = ObjectMapper.Map<List<WheelTaxDto>>(await _wheelTaxRepository
                                .GetAll().Where(x => wheelTaxIds.Contains(x.Id.ToString())).ToListAsync());

                            foreach (var item in wheelLocation.WheelTaxes)
                            {
                                item.TaxPercent = (await _dinePlanTaxLocationRepository.GetAll().FirstOrDefaultAsync(x => item.DinePlanTaxId == x.DinePlanTaxRefId))?.TaxPercentage ?? decimal.Zero;
                            }
                        }
                    }
                    result.Add(output);
                }

                return result;
            }
        }

        public async Task<GetWheelDepartmentOutput> GetWheelDepartment(int departmentId)
        {
            var tenantId = AbpSession.TenantId ?? 1;
            var output = new GetWheelDepartmentOutput();

            using (_unitOfWorkManager.Current.SetTenantId(tenantId))
            {
                var wheelDepartment = await _wheelDepartmentRepository.GetAll()
                    .Include(t => t.WheelDepartmentTimeSlot)
                    .Where(x => x.TenantId == tenantId)
                    .FirstOrDefaultAsync(t => t.Id == departmentId);

                if (wheelDepartment != null)
                {

                    var wheelLocations = await _wheelLocationRepository.GetAll()
                        .Include(x => x.Location)
                        .ThenInclude(x => x.City)
                        .ThenInclude(x => x.Country)
                        .Where(x => x.WheelDepartmentIds.Contains(wheelDepartment.Id.ToString()) &&
                                    x.TenantId == tenantId)
                        .ToListAsync();

                    var timeZone = await GetTimezone();
                    var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
                    var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);

                    if (wheelLocations.Any() == false || wheelDepartment.Enable == false)
                    {
                        return null;
                    }

                    output = ObjectMapper.Map<GetWheelDepartmentOutput>(wheelDepartment);
                    output.IsOpeningHour = await IsOpeningHour(wheelDepartment.Id);

                    if (string.IsNullOrEmpty(wheelDepartment.Dynamic) == false)
                    {
                        dynamic itemDynamic = JObject.Parse(wheelDepartment.Dynamic);
                        output.IsRequireOtp = itemDynamic.IsRequireOtp ?? false;
                        output.FutureOrderDaysLimit = itemDynamic.FutureOrderDaysLimit;
                    }

                    if ((output.IsOpeningHour == false || !output.IsEnableDelivery) && output.AllowPreOrder)
                    {
                        if (!output.FutureOrderDaysLimit.HasValue || output.FutureOrderDaysLimit == 0)
                        {
                            output.IsOpeningHour = false;
                        }
                        else
                        {
                            output.DateDefaultOrderFututer = now.Date;
                        }
                    }

                    output.CloseDayOfWeek = await GetCloseDayByDepartmentId(wheelDepartment.Id);

                    output.WheelPaymentMethods =
                        await GetListPaymentMethodFromSystemNames(wheelDepartment.WheelPaymentMethodIds);

                    output.WheelLocationOutputs = ObjectMapper.Map<List<GetWheelLocationOutput>>(wheelLocations);

                    if (wheelDepartment.WheelDepartmentTimeSlot != null &&
                        wheelDepartment.WheelDepartmentTimeSlot.Any())
                    {
                        output.IsTimeSlotMode = true;
                        var timeSlots = (await GetTimeSlotByDepartmentId(wheelDepartment.Id)).ToList().OrderBy(x => x.TimeFrom);
                        output.TimeSlots = ObjectMapper.Map<List<TimeSlotDto>>(timeSlots);
                    }

                    if (!output.WheelServiceFeeIds.IsNullOrWhiteSpace())
                    {
                        var wheelServiceFeeIds = output.WheelServiceFeeIds?.Split(";");
                        output.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(
                            await _wheelServiceFeeRepository
                                .GetAll().Where(x => wheelServiceFeeIds.Contains(x.Id.ToString())).ToListAsync());

                        foreach (var fee in output.WheelServiceFees)
                        {
                            if (!fee.WheelTaxes.IsNullOrEmpty())
                            {
                                var taxes = JsonConvert.DeserializeObject<List<WheelTaxDto>>(fee.WheelTaxes);

                                var ids = taxes?.Select(t => t.DinePlanTaxId).ToList();

                                if (ids != null && ids.Any() && wheelLocations.Any())
                                {
                                    foreach (var wheelLocation in wheelLocations)
                                    {
                                        var orgId = await _locationAppService.GetOrgnizationIdByLocationId(wheelLocation.LocationId);
                                        if (orgId > 0)
                                        {
                                            CurrentUnitOfWork.SetFilterParameter(
                                                DineConnectDataFilters.MustHaveOrganization,
                                                DineConnectDataFilters.Parameters.OrganizationId, orgId);
                                            var allTaxes = _dinePlanTaxLocationRepository.GetAll()
                                                .Where(e => ids.Contains(e.DinePlanTaxRefId) && e.TenantId == tenantId).ToList();
                                            if (allTaxes.Any())
                                            {
                                                fee.ServiceTaxes.Add(wheelLocation.Id, allTaxes.Sum(a=>a.TaxPercentage));
                                            }
                                        }
                                    }
                                   
                                }
                            }
                        }
                    }

                    var openingHours = wheelDepartment.WheelOpeningHourIds?.Split(";");
                    if (openingHours != null && openingHours.Length > 0)
                    {
                        output.WheelOpeningHours = ObjectMapper.Map<List<DinePlanOpeningHoursDto>>(
                            await _wheelOpeningHourRepository.GetAll()
                                .Where(x => openingHours.Contains(x.Id.ToString())).ToListAsync());
                    }

                    foreach (var wheelLocation in output.WheelLocationOutputs)
                    {
                        var wheelDeliveryZoneIds = wheelLocation.WheelDeliveryZoneIds?.Split(";");
                        wheelLocation.WheelDeliveryZones = ObjectMapper.Map<List<WheelDeliveryZoneDto>>(
                            await _wheelDeliveryZoneRepository
                                .GetAll().Where(x => wheelDeliveryZoneIds.Contains(x.Id.ToString())).ToListAsync());

                        var wheelServiceFeeIds = wheelLocation.WheelServiceFeeIds?.Split(";");
                        wheelLocation.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(
                            await _wheelServiceFeeRepository
                                .GetAll().Where(x => wheelServiceFeeIds.Contains(x.Id.ToString())).ToListAsync());

                        if (!wheelLocation.WheelTaxIds.IsNullOrEmpty())
                        {
                            var wheelTaxIds = wheelLocation.WheelTaxIds.Split(";");
                            wheelLocation.WheelTaxes = ObjectMapper.Map<List<WheelTaxDto>>(await _wheelTaxRepository
                                .GetAll().Where(x => wheelTaxIds.Contains(x.Id.ToString())).ToListAsync());

                            foreach (var item in wheelLocation.WheelTaxes)
                            {
                                item.TaxPercent =
                                    (await _dinePlanTaxLocationRepository.GetAll()
                                        .FirstOrDefaultAsync(x => item.DinePlanTaxId == x.DinePlanTaxRefId))
                                    ?.TaxPercentage ?? decimal.Zero;
                            }
                        }
                    }
                }

            }
            return output;

        }

        private async Task<List<DayOfWeek>> GetCloseDayByDepartmentId(int departmentId)
        {
            var closeDays = new List<DayOfWeek>();
            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == departmentId);
            if (string.IsNullOrEmpty(department.WheelOpeningHourIds))
            {
                return closeDays;
            }

            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll().Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();
            var openDays = new List<DayOfWeek>();
            foreach (var day in openingHours)
            {
                //var openDays = new List<DayOfWeek>();
                if ((day.OpenDays & 1 << 0) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Monday))
                    {
                        openDays.Add(DayOfWeek.Monday);
                    }
                }

                if ((day.OpenDays & 1 << 1) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Tuesday))
                    {
                        openDays.Add(DayOfWeek.Tuesday);
                    }
                }

                if ((day.OpenDays & 1 << 2) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Wednesday))
                    {
                        openDays.Add(DayOfWeek.Wednesday);
                    }
                }

                if ((day.OpenDays & 1 << 3) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Thursday))
                    {
                        openDays.Add(DayOfWeek.Thursday);
                    }
                }

                if ((day.OpenDays & 1 << 4) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Friday))
                    {
                        openDays.Add(DayOfWeek.Friday);
                    }
                }

                if ((day.OpenDays & 1 << 5) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Saturday))
                    {
                        openDays.Add(DayOfWeek.Saturday);
                    }
                }

                if ((day.OpenDays & 1 << 6) != 0)
                {
                    if (openDays.All(x => x != DayOfWeek.Sunday))
                    {
                        openDays.Add(DayOfWeek.Sunday);
                    }
                }
            }

            if (openDays.All(x => x != DayOfWeek.Monday))
            {
                closeDays.Add(DayOfWeek.Monday);
            }
            if (openDays.All(x => x != DayOfWeek.Tuesday))
            {
                closeDays.Add(DayOfWeek.Tuesday);
            }
            if (openDays.All(x => x != DayOfWeek.Wednesday))
            {
                closeDays.Add(DayOfWeek.Wednesday);
            }
            if (openDays.All(x => x != DayOfWeek.Thursday))
            {
                closeDays.Add(DayOfWeek.Thursday);
            }
            if (openDays.All(x => x != DayOfWeek.Friday))
            {
                closeDays.Add(DayOfWeek.Friday);
            }
            if (openDays.All(x => x != DayOfWeek.Saturday))
            {
                closeDays.Add(DayOfWeek.Saturday);
            }
            if (openDays.All(x => x != DayOfWeek.Sunday))
            {
                closeDays.Add(DayOfWeek.Sunday);

            }
            return closeDays;
        }

        public async Task CreateOrEdit(CreateOrEditWheelDepartmentDto input)
        {
            input.WheelPaymentMethodIds = input.WheelPaymentMethods != null ? 
                string.Join(";", input.WheelPaymentMethods.Select(x => x.SystemName)) : string.Empty;

            input.WheelServiceFeeIds = input.WheelServiceFees != null ? 
                string.Join(";", input.WheelServiceFees.Select(x => x.Id)) : string.Empty;

            input.WheelOpeningHourIds = input.WheelOpeningHours != null ? 
                string.Join(";", input.WheelOpeningHours.Select(x => x.Id)) : string.Empty;

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        public async Task UpdateOnlyOpeningHours(CreateOrEditWheelDepartmentDto input)
        {
            input.WheelOpeningHourIds = input.WheelOpeningHours != null ? 
                string.Join(";", input.WheelOpeningHours.Select(x => x.Id)) : string.Empty;
            if (input.Id != null)
            {
                var wheelDepartment = await _wheelDepartmentRepository.FirstOrDefaultAsync((int)input.Id);
                wheelDepartment.WheelOpeningHourIds = input.WheelOpeningHourIds;

                await _wheelDepartmentRepository.UpdateAsync(wheelDepartment);
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            await _wheelDepartmentRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetWheelDepartmentsToExcel(GetAllWheelDepartmentsForExcelInput input)
        {
            var query = _wheelDepartmentRepository.GetAll()
                .Include(e => e.PriceTag)
                .Include(e => e.Menu)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                .WhereIf(input.WheelOrderTypeFilter.HasValue, e => e.WheelOrderType == input.WheelOrderTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.PriceTagFilter),
                    e => e.PriceTag != null && e.PriceTag.Id.ToString() == input.PriceTagFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.MenuFilter),
                    e => e.Menu != null && e.Menu.Id.ToString() == input.MenuFilter)
                .WhereIf(input.Enable.HasValue, e => e.Enable == input.Enable.Value);

            var wheelDepartmentListDtos = await query.ToListAsync();

            return _wheelDepartmentsExcelExporter.ExportToFile(
                ObjectMapper.Map<List<GetWheelDepartmentForViewDto>>(wheelDepartmentListDtos));
        }

        
        public async Task<List<WheelDepartmentPriceTagLookupTableDto>> GetAllPriceTagForTableDropdown()
        {
            return await _lookupPriceTagRepository.GetAll()
                .Select(priceTag => new WheelDepartmentPriceTagLookupTableDto
                {
                    Id = priceTag.Id,
                    DisplayName = priceTag.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<GetWheelDepartmentForViewDto>> GetAllWheelDepartments()
        {
            var departments = await _wheelDepartmentRepository.GetAllListAsync();
            return ObjectMapper.Map<List<GetWheelDepartmentForViewDto>>(departments);
        }


        public async Task<List<TimeSlotDto>> GetTimeSlots(int departmentId, string date)
        {
            var listTimeSlot = await _wheelDepartmentTimeSlotRepository
                .GetAll()
                .Where(x => x.WheelDepartmentId == departmentId).ToListAsync();

            var result = ObjectMapper.Map<List<TimeSlotDto>>(listTimeSlot);

            CultureInfo provider = CultureInfo.InvariantCulture;

            DateTime orderDate = DateTime.ParseExact(date, "yyyy/MM/dd", provider);

            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == departmentId);
            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();

            if (openingHours.Count == 0) return null;

            var currentWeekDay = orderDate.DayOfWeek;
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

            foreach (var openingHour in openingHours)
            {
                if (TimeSpan.TryParse(openingHour.From, out var openAt) && TimeSpan.TryParse(openingHour.To, out var closeAt))
                {
                    var openTime = new DateTime(orderDate.Year, orderDate.Month, orderDate.Day, openAt.Hours, openAt.Minutes, 0);
                    var closeTime = new DateTime(orderDate.Year, orderDate.Month, orderDate.Day, closeAt.Hours, closeAt.Minutes, 0);

                    var openDays = new List<DayOfWeek>();
                    if ((openingHour.OpenDays & (1 << 0)) != 0) openDays.Add(DayOfWeek.Monday);

                    if ((openingHour.OpenDays & (1 << 1)) != 0) openDays.Add(DayOfWeek.Tuesday);

                    if ((openingHour.OpenDays & (1 << 2)) != 0) openDays.Add(DayOfWeek.Wednesday);

                    if ((openingHour.OpenDays & (1 << 3)) != 0) openDays.Add(DayOfWeek.Thursday);

                    if ((openingHour.OpenDays & (1 << 4)) != 0) openDays.Add(DayOfWeek.Friday);

                    if ((openingHour.OpenDays & (1 << 5)) != 0) openDays.Add(DayOfWeek.Saturday);

                    if ((openingHour.OpenDays & (1 << 6)) != 0) openDays.Add(DayOfWeek.Sunday);

                    if (openDays.Contains(currentWeekDay))
                    {
                        foreach (var timeSlot in result)
                        {
                            var timeFrom = DateTime.ParseExact(date + " " + timeSlot.TimeFrom, "yyyy/MM/dd HH:mm", provider);
                            var timeTo = DateTime.ParseExact(date + " " + timeSlot.TimeTo, "yyyy/MM/dd HH:mm", provider);

                            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);

                            if (timeFrom >= openTime && timeTo <= closeTime && (timeFrom.Date > now.Date || (timeFrom.Date == now.Date && timeFrom >= now)))
                            {
                                timeSlot.IsOpened = true;
                            }
                        }
                    }
                }
            }

            result.RemoveAll(t => !t.IsOpened);

            return result.OrderBy(t => t.TimeFrom).ToList();
        }


        
        protected virtual async Task Create(CreateOrEditWheelDepartmentDto input)
        {
            var wheelDepartment = ObjectMapper.Map<WheelDepartment>(input);

            dynamic itemDynamic = new ExpandoObject();

            itemDynamic.IsRequireOtp = input.IsRequireOtp;
            itemDynamic.FutureOrderDaysLimit = input.FutureOrderDaysLimit;
            itemDynamic.OrderDeliveryTime = input.OrderDeliveryTime;
            itemDynamic.OrderPreparationTime = input.OrderPreparationTime;
            itemDynamic.SendOtp = input.SendOtp;

            wheelDepartment.Dynamic = JsonConvert.SerializeObject(itemDynamic);

            if (AbpSession.TenantId != null) wheelDepartment.TenantId = (int)AbpSession.TenantId;

            await _wheelDepartmentRepository.InsertAsync(wheelDepartment);
        }

        
        protected virtual async Task Update(CreateOrEditWheelDepartmentDto input)
        {
            if (input.Id != null)
            {
                var wheelDepartment = await _wheelDepartmentRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, wheelDepartment);

                dynamic itemDynamic = new ExpandoObject();
                itemDynamic.IsRequireOtp = input.IsRequireOtp;
                itemDynamic.SendOtp = input.SendOtp;
                itemDynamic.FutureOrderDaysLimit = input.FutureOrderDaysLimit;
                itemDynamic.OrderDeliveryTime = input.OrderDeliveryTime;
                itemDynamic.OrderPreparationTime = input.OrderPreparationTime;
                wheelDepartment.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                await _wheelDepartmentRepository.UpdateAsync(wheelDepartment);
            }
        }

        
        public async Task<List<WheelDepartmentLookupTableDto>> GetAllDepartmentForTableDropdown()
        {
            return await _wheelDepartmentRepository.GetAll()
                .Select(wheelDepartment => new WheelDepartmentLookupTableDto
                {
                    Id = wheelDepartment.Id,
                    DisplayName = wheelDepartment.Name.ToString()
                }).ToListAsync();
        }

        public async Task<List<GetWheelPaymentMethodForViewDto>> GetListPaymentMethodFromSystemNames(string systemNames)
        {
            var listPaymentProcessors = new List<GetWheelPaymentMethodForViewDto>();

            if (!string.IsNullOrEmpty(systemNames))
            {
                var ids = systemNames.Split(";", StringSplitOptions.RemoveEmptyEntries);
                var paymentProcessors = await _paymentService.GetAllPaymentProcessor();

                foreach (var item in paymentProcessors)
                {
                    var data = await item.GetSetting();

                    if (data.DynamicFieldData != null)
                    {
                        data.DynamicFieldData.secretKey = null;
                    }

                    listPaymentProcessors.Add(ObjectMapper.Map<GetWheelPaymentMethodForViewDto>(data));
                }

                listPaymentProcessors = listPaymentProcessors.Where(x => ids.Contains(x.SystemName)).ToList();
            }

            return listPaymentProcessors;
        }

        public async Task CreateOrEditTimeSlot(TimeSlotDto input)
        {
            if (input.Id != 0)
            {
                await UpdateTimeSlot(input);
            }
            else
            {
                await CreateTimeSlot(input);
            }
        }

        private async Task CreateTimeSlot(TimeSlotDto input)
        {
            input.TenantId = AbpSession.TenantId;
            var timeSlot = ObjectMapper.Map<WheelDepartmentTimeSlot>(input);
            await _wheelDepartmentTimeSlotRepository.InsertAsync(timeSlot);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        private async Task UpdateTimeSlot(TimeSlotDto input)
        {
            var timeSlot = await _wheelDepartmentTimeSlotRepository.FirstOrDefaultAsync(input.Id);
            ObjectMapper.Map(input, timeSlot);
        }

        public async Task DeleteTimeSlot(int id)
        {
            await _wheelDepartmentTimeSlotRepository.DeleteAsync(id);
        }

        public async Task<List<TimeSlotDto>> GetTimeSlotByDepartmentId(int departmentId)
        {
            var listTimeSlot = await _wheelDepartmentTimeSlotRepository.GetAll()
                .Where(x => x.WheelDepartmentId == departmentId).ToListAsync();

            var result = ObjectMapper.Map<List<TimeSlotDto>>(listTimeSlot);
            return result;
        }

        public async Task<TimeSlotDto> GetTimeSlotById(int id)
        {
            var timeSlot = await _wheelDepartmentTimeSlotRepository.FirstOrDefaultAsync(x => x.Id == id);
            var result = ObjectMapper.Map<TimeSlotDto>(timeSlot);
            return result;
        }

        public async Task<bool> IsOpeningHour(long wheelDepartmentId)
        {
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == wheelDepartmentId);
            if (string.IsNullOrEmpty(department.WheelOpeningHourIds)) return false;
            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();

            if (openingHours.Count == 0) return false;

            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);

            var currentWeekDay = now.DayOfWeek;

            foreach (var openingHour in openingHours)
            {
                if (TimeSpan.TryParse(openingHour.From, out var openAt) && TimeSpan.TryParse(openingHour.To, out var closeAt))
                {
                    var todayOpenTime = new DateTime(now.Year, now.Month, now.Day, openAt.Hours, openAt.Minutes, 0);
                    var todayCloseTime = new DateTime(now.Year, now.Month, now.Day, closeAt.Hours, closeAt.Minutes, 0);

                    var openDays = new List<DayOfWeek>();
                    if ((openingHour.OpenDays & (1 << 0)) != 0) openDays.Add(DayOfWeek.Monday);

                    if ((openingHour.OpenDays & (1 << 1)) != 0) openDays.Add(DayOfWeek.Tuesday);

                    if ((openingHour.OpenDays & (1 << 2)) != 0) openDays.Add(DayOfWeek.Wednesday);

                    if ((openingHour.OpenDays & (1 << 3)) != 0) openDays.Add(DayOfWeek.Thursday);

                    if ((openingHour.OpenDays & (1 << 4)) != 0) openDays.Add(DayOfWeek.Friday);

                    if ((openingHour.OpenDays & (1 << 5)) != 0) openDays.Add(DayOfWeek.Saturday);

                    if ((openingHour.OpenDays & (1 << 6)) != 0) openDays.Add(DayOfWeek.Sunday);

                    if (openDays.Contains(currentWeekDay))
                        if (now >= todayOpenTime && now <= todayCloseTime)
                            return true;
                }
            }

            return false;
        }

        public async Task<DateTime> GetDefaultDateFuture(long wheelDepartmentId)
        {
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);
            var selectedDate = now.AddDays(1);

            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == wheelDepartmentId);
            if (string.IsNullOrEmpty(department.WheelOpeningHourIds)) return selectedDate;
            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();

            if (openingHours.Count == 0) return selectedDate;

            var isValid = false;
            var currentWeekDay = selectedDate.DayOfWeek;
            DateTime? dateDefault = null;
            var step = 1;
            while (step > 0)
            {
                foreach (var openingHour in openingHours)
                {
                    if (TimeSpan.TryParse(openingHour.From, out _) &&
                        TimeSpan.TryParse(openingHour.To, out _))
                    {
                        var openDays = new List<DayOfWeek>();
                        if ((openingHour.OpenDays & (1 << 0)) != 0) openDays.Add(DayOfWeek.Monday);

                        if ((openingHour.OpenDays & (1 << 1)) != 0) openDays.Add(DayOfWeek.Tuesday);

                        if ((openingHour.OpenDays & (1 << 2)) != 0) openDays.Add(DayOfWeek.Wednesday);

                        if ((openingHour.OpenDays & (1 << 3)) != 0) openDays.Add(DayOfWeek.Thursday);

                        if ((openingHour.OpenDays & (1 << 4)) != 0) openDays.Add(DayOfWeek.Friday);

                        if ((openingHour.OpenDays & (1 << 5)) != 0) openDays.Add(DayOfWeek.Saturday);

                        if ((openingHour.OpenDays & (1 << 6)) != 0) openDays.Add(DayOfWeek.Sunday);

                        if (openDays.Contains(currentWeekDay))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }

                if (isValid)
                {
                    dateDefault = selectedDate;
                    step = 0;
                }
                else
                {
                    selectedDate = selectedDate.AddDays(1);
                    currentWeekDay = selectedDate.DayOfWeek;
                }
            }

            return dateDefault ?? Clock.Now.AddDays(1);
        }

        private async Task<string> GetTimezone()
        {
            using (_unitOfWorkManager.Current.SetTenantId(AbpSession.TenantId ?? 1))
            {
                var timezone = string.Empty;
                if (Clock.SupportsMultipleTimezone)
                    timezone = await SettingManager.GetSettingValueForTenantAsync(TimingSettingNames.TimeZone,
                        AbpSession.TenantId ?? 1);

                var defaultTimeZoneId =
                    await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, AbpSession.TenantId ?? 1);

                timezone = timezone ?? defaultTimeZoneId;

                return timezone;
            }
        }

    }
}
﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelPaymentMethodsExcelExporter : NpoiExcelExporterBase, IWheelPaymentMethodsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelPaymentMethodsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelPaymentMethodForViewDto> wheelPaymentMethods)
        {
            return CreateExcelPackage(
                "WheelPaymentMethods.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelPaymentMethods"));

                    AddHeader(
                        sheet,
                        L("SystemName"),
                        L("FriendlyName"),
                        L("Logo"),
                        L("SupportsCapture"),
                        L("Refund"),
                        L("PartialRefund"),
                        L("Void"),
                        L("RecurringSupport"),
                        L("DisplayOrder"),
                        L("Active")
                        );

                    AddObjects(
                        sheet, 2, wheelPaymentMethods,
                        _ => _.SystemName,
                        _ => _.FriendlyName,
                        _ => _.Logo,
                        _ => _.SupportsCapture,
                        _ => _.Refund,
                        _ => _.PartialRefund,
                        _ => _.Void,
                        _ => L(_.RecurringSupport.ToString()),
                        _ => _.DisplayOrder,
                        _ => _.Active
                        );

					
					
                });
        }
    }
}

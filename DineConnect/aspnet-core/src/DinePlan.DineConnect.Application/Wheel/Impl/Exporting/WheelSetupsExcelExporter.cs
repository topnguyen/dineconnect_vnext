﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelSetupsExcelExporter : NpoiExcelExporterBase, IWheelSetupsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelSetupsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelSetupForViewDto> wheelSetups)
        {
            return CreateExcelPackage(
                "WheelSetups.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelSetups"));

                    AddHeader(
                        sheet,
                        L("HomePage"),
                        L("CheckOutPage"),
                        L("Theme")
                        );

                    AddObjects(
                        sheet, 2, wheelSetups,
                        _ => _.WheelSetup.HomePages,
                        _ => _.WheelSetup.CheckOutPage,
                        _ => _.WheelSetup.Theme
                        );

					
					
                });
        }
    }
}

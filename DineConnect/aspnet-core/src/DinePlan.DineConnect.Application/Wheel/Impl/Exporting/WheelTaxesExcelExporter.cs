﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelTaxesExcelExporter : NpoiExcelExporterBase, IWheelTaxesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelTaxesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelTaxForViewDto> wheelTaxes)
        {
            return CreateExcelPackage(
                "WheelTaxes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelTaxes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        (L("WheelTax")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, wheelTaxes,
                        _ => _.WheelTax.Name,
                        _ => _.WheelTaxName
                        );

					
					
                });
        }
    }
}

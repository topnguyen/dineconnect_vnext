﻿using Abp.Configuration;
using Abp.Runtime.Session;
using Abp.Timing;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Timing;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelOpeningHoursExcelExporter : NpoiExcelExporterBase, IWheelOpeningHoursExcelExporter
    {
        private readonly IAbpSession _abpSession;
        private readonly ITimeZoneService _timeZoneService;

        public WheelOpeningHoursExcelExporter(
           IAbpSession abpSession,
           ITempFileCacheManager tempFileCacheManager,
           ITimeZoneService timeZoneService) :
   base(tempFileCacheManager)
        {
            _abpSession = abpSession;
            _timeZoneService = timeZoneService;
        }

        public async Task<FileDto> ExportToFile(List<WheelOpeningHourDto> wheelOpeningHours)
        {
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

            return CreateExcelPackage(
                "WheelOpeningHours.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("WheelOpeningHours"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("From"),
                        L("To"),
                        L("Monday"),
                        L("Tuesday"),
                        L("Wednesday"),
                        L("Thursday"),
                        L("Friday"),
                        L("Saturday"),
                        L("Sunday")
                        );

                    AddObjects(
                        sheet, 2, wheelOpeningHours,
                        _ => _.Name,
                        _ => _.From,
                        _ => _.To,
                        _ => (_.OpenDays & 1 << 0) != 0,
                        _ => (_.OpenDays & 1 << 1) != 0,
                        _ => (_.OpenDays & 1 << 2) != 0,
                        _ => (_.OpenDays & 1 << 3) != 0,
                        _ => (_.OpenDays & 1 << 4) != 0,
                        _ => (_.OpenDays & 1 << 5) != 0,
                        _ => (_.OpenDays & 1 << 6) != 0
                        );

                });
        }

        private async Task<string> GetTimezone()
        {
            string timezone = string.Empty;
            if (Clock.SupportsMultipleTimezone)
            {
                timezone = await SettingManager.GetSettingValueForTenantAsync(TimingSettingNames.TimeZone, _abpSession.GetTenantId());
            }

            var defaultTimeZoneId = await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, _abpSession.TenantId);

            timezone = timezone ?? defaultTimeZoneId;

            return timezone;
        }
    }
}

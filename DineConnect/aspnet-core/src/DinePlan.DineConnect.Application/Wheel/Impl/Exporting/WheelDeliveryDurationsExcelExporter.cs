﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelDeliveryDurationsExcelExporter : NpoiExcelExporterBase, IWheelDeliveryDurationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelDeliveryDurationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelDeliveryDurationForViewDto> wheelDeliveryDurations)
        {
            return CreateExcelPackage(
                "WheelDeliveryDurations.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelDeliveryDurations"));

                    AddHeader(
                        sheet,
                        L("TenantId"),
                        L("DeliveryDistance"),
                        L("DeliveryTime")
                        );

                    AddObjects(
                        sheet, 2, wheelDeliveryDurations,
                        _ => _.WheelDeliveryDuration.TenantId,
                        _ => _.WheelDeliveryDuration.DeliveryDistance,
                        _ => _.WheelDeliveryDuration.DeliveryTime
                        );

					
					
                });
        }
    }
}

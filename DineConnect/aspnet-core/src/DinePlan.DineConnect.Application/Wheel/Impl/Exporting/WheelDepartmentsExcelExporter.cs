﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelDepartmentsExcelExporter : NpoiExcelExporterBase, IWheelDepartmentsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelDepartmentsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelDepartmentForViewDto> wheelDepartments)
        {
            return CreateExcelPackage(
                "WheelDepartments.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelDepartments"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("PriceTag"),
                        L("Type"),
                        L("OpeningHours"),
                        L("Menu"),
                        L("Enable")
                        );

                    AddObjects(
                        sheet, 2, wheelDepartments,
                        _ => _.Name,
                        _ => _.PriceTag.Name,
                        _ => _.WheelOrderType.ToString(),
                        _ => _.OpeningHour.Name,
                        _ => _.Menu.Name,
                        _ => _.Enable
                        );
                });
        }
    }
}

﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.DataExporting.Excel.NPOI;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public class WheelLocationsExcelExporter : NpoiExcelExporterBase, IWheelLocationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WheelLocationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWheelLocationForViewDto> wheelLocations)
        {
            return CreateExcelPackage(
                "WheelLocations.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("WheelLocations"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Enabled"),
                        L("TaxInclusive"),
                        L("OutsideDeliveryZone"),
                        (L("Location")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, wheelLocations,
                        _ => _.Name,
                        _ => _.Enabled,
                        _ => _.TaxInclusive,
                        _ => _.OutsideDeliveryZone,
                        _ => _.LocationName
                        );

                });
        }
    }
}

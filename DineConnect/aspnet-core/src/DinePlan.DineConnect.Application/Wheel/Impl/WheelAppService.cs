﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Elect.Location.Coordinate.DistanceUtils;
using Elect.Location.Coordinate.PolygonUtils;
using Elect.Location.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class WheelAppService : DineConnectAppServiceBase, IWheelAppService
    {
        private readonly IRepository<WheelDeliveryZone> _zoneRepository;
        private readonly IRepository<WheelOpeningHour> _openingHourRepository;
        private readonly IRepository<WheelServiceFee> _feeRepository;
        private readonly IRepository<WheelPaymentMethod> _paymentMethodRepository;
        private readonly IRepository<WheelDeliveryDuration> _deliveryDurationRepository;
        private readonly IWheelOpeningHoursExcelExporter _wheelOpeningHoursExcelExporter;

        public WheelAppService(IRepository<WheelDeliveryZone> zoneRepository, IRepository<WheelOpeningHour> openingHourRepository,
            IRepository<WheelServiceFee> feeRepository, IRepository<WheelPaymentMethod> paymentMethodRepository,
            IRepository<WheelDeliveryDuration> deliveryDurationRepository, IWheelOpeningHoursExcelExporter wheelOpeningHoursExcelExporter)
        {
            _zoneRepository = zoneRepository;
            _openingHourRepository = openingHourRepository;
            _feeRepository = feeRepository;
            _paymentMethodRepository = paymentMethodRepository;
            _deliveryDurationRepository = deliveryDurationRepository;
            _wheelOpeningHoursExcelExporter = wheelOpeningHoursExcelExporter;
        }

        public async Task<IEnumerable<WheelDeliveryZoneDto>> GetAllDeliveryZones()
        {
            var zones = await _zoneRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelDeliveryZoneDto>>(zones);
        }

        public async Task<WheelDeliveryZoneDto> AddZone(WheelDeliveryZoneDto zoneDto)
        {
            var zone = ObjectMapper.Map<WheelDeliveryZone>(zoneDto);
            zone.TenantId = AbpSession.TenantId ?? 1;
            zone = await _zoneRepository.InsertAsync(zone);
            await UnitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<WheelDeliveryZoneDto>(zone);
        }

        public async Task UpdateZone(WheelDeliveryZoneDto zoneDto)
        {
            var zone = await _zoneRepository.FirstOrDefaultAsync(zoneDto.Id);
            if (zone == null)
                return;

            zoneDto.TenantId = AbpSession.TenantId ?? -1;
            ObjectMapper.Map(zoneDto, zone);
        }

        public async Task DeleteZone(int zoneId)
        {
            await _zoneRepository.HardDeleteAsync(zone => zone.Id == zoneId);
        }
        public async Task<List<WheelDeliveryZoneDto>> GetWheelDeliveryZonesAsync(List<int> ids)
        {
            var delivelyZones = (await _zoneRepository.GetAllListAsync(x => ids.Contains(x.Id))).ToList();

            return ObjectMapper.Map<List<WheelDeliveryZoneDto>>(delivelyZones);
        }

        public async Task<List<WheelDeliveryZoneDto>> GetDeliveryZonesContainedPoint(double longitude, double latitude)
        {
            var zones = await _zoneRepository.GetAllListAsync();

            var deliveryZones = new List<WheelDeliveryZoneDto>();

            if (!zones.Any())
            {
                return deliveryZones;
            }

            var testCoordinate = new CoordinateModel(longitude, latitude);

            // Handle Circle Zones

            var circleDeliveryZones = zones.Where(x => x.ZoneType == DeliveryZoneType.Circle).ToList();

            foreach (var circleDeliveryZone in circleDeliveryZones)
            {
                dynamic circleDeliveryZoneData = JObject.Parse(circleDeliveryZone.ZoneCoordinates);

                dynamic centerPointData = circleDeliveryZoneData.center;

                var centerPoint = new CoordinateModel((double)centerPointData.lng, (double)centerPointData.lat);

                var distanceToCenterPoint = testCoordinate.DistanceTo(centerPoint, UnitOfLengthModel.Meter);

                if (distanceToCenterPoint < (double)circleDeliveryZoneData.radius)
                {
                    deliveryZones.Add(ObjectMapper.Map<WheelDeliveryZoneDto>(circleDeliveryZone));
                }
            }

            // Handle Polygon Zones

            var polygonDeliveryZones = zones.Where(x => x.ZoneType == DeliveryZoneType.Polygon).ToList();

            foreach (var polygonDeliveryZone in polygonDeliveryZones)
            {
                dynamic polygonJson = JObject.Parse(polygonDeliveryZone.ZoneCoordinates);

                var polygonPoints = JArray.Parse(polygonJson.paths.ToString());

                var polygon = new List<CoordinateModel>();

                foreach (var polygonPoint in polygonPoints)
                {
                    polygon.Add(new CoordinateModel((double)polygonPoint.lng, (double)polygonPoint.lat));
                }

                var isInPolygon = PolygonUtils.IsInPolygon(testCoordinate, polygon);

                if (isInPolygon)
                {
                    deliveryZones.Add(ObjectMapper.Map<WheelDeliveryZoneDto>(polygonDeliveryZone));
                }
            }


            return deliveryZones;
        }

        public async Task<PagedResultDto<WheelOpeningHourDto>> GetAllOpeningHours(GetAllWheelOpeningHoursInput input)
        {
            var query = _openingHourRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => x.Name.Contains(input.Filter));

            if (!input.Sorting.IsNullOrEmpty())
                query = query.OrderBy(input.Sorting);

            var count = await query.CountAsync();
            var openingHours = await query.PageBy(input).ToListAsync();

            return new PagedResultDto<WheelOpeningHourDto>(count, ObjectMapper.Map<List<WheelOpeningHourDto>>(openingHours));
        }

        public async Task<WheelOpeningHourDto> GetOpeningHour(EntityDto input)
        {
            var openingHour = await _openingHourRepository.FirstOrDefaultAsync(x => x.Id == input.Id);

            return ObjectMapper.Map<WheelOpeningHourDto>(openingHour);
        }

        public async Task CreateOrEditOpeningHour(WheelOpeningHourDto openingHourDto)
        {
            if (openingHourDto.Id > 0)
            {
                await UpdateOpeningHour(openingHourDto);
            }
            else
            {
                await AddOpeningHour(openingHourDto);
            }
        }

        private async Task<WheelOpeningHourDto> AddOpeningHour(WheelOpeningHourDto openingHourDto)
        {
            var openingHour = ObjectMapper.Map<WheelOpeningHour>(openingHourDto);
            openingHour.TenantId = AbpSession.TenantId ?? 1;
            await _openingHourRepository.InsertAsync(openingHour);
            await UnitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<WheelOpeningHourDto>(openingHour);
        }

        private async Task UpdateOpeningHour(WheelOpeningHourDto openingHourDto)
        {
            var openingHour = await _openingHourRepository.FirstOrDefaultAsync(oh => oh.Id == openingHourDto.Id);
            if (openingHour == null)
                return;

            openingHourDto.TenantId = AbpSession.TenantId ?? -1;
            ObjectMapper.Map(openingHourDto, openingHour);
        }

        public async Task DeleteOpeningHour(int id)
        {
            await _openingHourRepository.HardDeleteAsync(oh => oh.Id == id);
        }

        public async Task DeleteOpeningHoursByServiceType(WheelOpeningHourServiceType serviceType)
        {
            await _openingHourRepository.HardDeleteAsync(oh => oh.ServiceType == serviceType);
        }

        public async Task<List<WheelOpeningHourForComboboxItemDto>> GetOpeningHourForComboboxByServiceType(WheelOpeningHourServiceType serviceType, int? selectedId = null)
        {
            var openingHours = await _openingHourRepository.GetAll()
                .Where(x => x.ServiceType == serviceType)
                .ToListAsync();

            var openingHourItems = new List<WheelOpeningHourForComboboxItemDto>(openingHours.Select(x =>
                new WheelOpeningHourForComboboxItemDto(x.Id.ToString(), x.Name, false)));

            if (openingHourItems.Count > 0)
            {
                if (selectedId.HasValue)
                {
                    var selectedItem = openingHourItems.FirstOrDefault(x => x.Value == selectedId.Value.ToString());
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        openingHourItems[0].IsSelected = true;
                    }
                }
            }

            return openingHourItems;
        }

        public async Task<FileDto> GetWheelOpeningHoursToExcel(GetAllWheelOpeningHoursForExcelInput input)
        {
            var query = _openingHourRepository.GetAll()
               .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => x.Name.Contains(input.Filter));

            return await _wheelOpeningHoursExcelExporter.ExportToFile(ObjectMapper.Map<List<WheelOpeningHourDto>>(await query.ToListAsync()));
        }

        public async Task<IEnumerable<WheelServiceFeeDto>> GetAllWheelServiceFees()
        {
            var fees = await _feeRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelServiceFeeDto>>(fees.OrderBy(f => f.Id));
        }

        public async Task<WheelServiceFeeDto> AddWheelServiceFee(WheelServiceFeeDto feeDto)
        {
            var fee = ObjectMapper.Map<WheelServiceFee>(feeDto);
            fee.TenantId = AbpSession.TenantId ?? 1;
            await _feeRepository.InsertAsync(fee);
            await UnitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<WheelServiceFeeDto>(fee);
        }

        public async Task UpdateWheelServiceFee(WheelServiceFeeDto feeDto)
        {
            var fee = await _feeRepository.FirstOrDefaultAsync(f => f.Id == feeDto.Id);
            if (fee == null)
                return;

            feeDto.TenantId = AbpSession.TenantId ?? -1;
            ObjectMapper.Map(feeDto, fee);
        }

        public async Task DeleteWheelServiceFee(int feeId)
        {
            await _feeRepository.HardDeleteAsync(fee => fee.Id == feeId);
        }

        public async Task<List<WheelPaymentMethodDto>> GetAllPaymentMethodsAsync()
        {
            var paymentMethods = await _paymentMethodRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelPaymentMethodDto>>(paymentMethods);
        }

        public async Task<List<WheelDeliveryDurationDto>> GetAllDeliveryDurationsAsync()
        {
            var dds = await _deliveryDurationRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelDeliveryDurationDto>>(dds);
        }
    }
}

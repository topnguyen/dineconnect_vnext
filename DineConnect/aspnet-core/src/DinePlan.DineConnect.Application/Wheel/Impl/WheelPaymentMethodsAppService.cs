﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.PaymentProcessor.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelPaymentMethodsAppService : DineConnectAppServiceBase, IWheelPaymentMethodsAppService
    {
        private readonly IRepository<WheelPaymentMethod> _wheelPaymentMethodRepository;
        private readonly IWheelPaymentMethodsExcelExporter _wheelPaymentMethodsExcelExporter;
        private readonly IRepository<PaymentType, int> _lookup_paymentTypeRepository;
        private readonly IPaymentService _paymentService;

        public WheelPaymentMethodsAppService(IRepository<WheelPaymentMethod> wheelPaymentMethodRepository,
            IWheelPaymentMethodsExcelExporter wheelPaymentMethodsExcelExporter,
            IRepository<PaymentType, int> lookup_paymentTypeRepository,
            IPaymentService paymentService)
        {
            _wheelPaymentMethodRepository = wheelPaymentMethodRepository;
            _wheelPaymentMethodsExcelExporter = wheelPaymentMethodsExcelExporter;
            _lookup_paymentTypeRepository = lookup_paymentTypeRepository;
            _paymentService = paymentService;
        }

        public async Task<ListResultDto<GetWheelPaymentMethodForViewDto>> GetAll(GetAllWheelPaymentMethodsInput input)
        {
            var paymentProcessors = await _paymentService.GetAllPaymentProcessor();

            var listPaymentProcessors = new List<GetWheelPaymentMethodForViewDto>();

            foreach (var item in paymentProcessors)
            {
                var data = await item.GetSetting();

                listPaymentProcessors.Add(ObjectMapper.Map<GetWheelPaymentMethodForViewDto>(data));
            }

            listPaymentProcessors = listPaymentProcessors
                .WhereIf(!string.IsNullOrEmpty(input.Filter), x => x.SystemName.ToLower().Contains(input.Filter.ToLower()) || x.FriendlyName.ToLower().Contains(input.Filter.ToLower()))
                .WhereIf(!string.IsNullOrEmpty(input.SystemNameFilter), x => x.SystemName.ToLower().Contains(input.SystemNameFilter.ToLower()))
                .WhereIf(!string.IsNullOrEmpty(input.FriendlyNameFilter), x => x.FriendlyName.ToLower().Contains(input.FriendlyNameFilter.ToLower()))
                .WhereIf(input.SupportsCaptureFilter > -1, x => x.SupportsCapture == (input.SupportsCaptureFilter == 1))
                .WhereIf(input.RefundFilter > -1, x => x.Refund == (input.RefundFilter == 1))
                .WhereIf(input.PartialRefundFilter > -1, x => x.PartialRefund == (input.PartialRefundFilter == 1))
                .WhereIf(input.VoidFilter > -1, x => x.Void == (input.VoidFilter == 1))
                .WhereIf(input.ActiveFilter > -1, x => x.Active == (input.ActiveFilter == 1))
                .WhereIf(input.RecurringSupportFilter.HasValue, x => x.RecurringSupport == input.RecurringSupportFilter)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .AsQueryable()
                .PageBy(input)
                .ToList();

            return new ListResultDto<GetWheelPaymentMethodForViewDto>(listPaymentProcessors);
        }

        
        public async Task<GetWheelPaymentMethodForEditOutput> GetWheelPaymentMethodForEdit(string systemName)
        {
            var wheelPaymentMethod = await _paymentService.GetPaymentProcessorBySystemName(systemName);

            var data = await wheelPaymentMethod.GetSetting();

            return ObjectMapper.Map<GetWheelPaymentMethodForEditOutput>(data);
        }

        
        public async Task Edit(EditWheelPaymentMethodDto input)
        {
            var wheelPaymentMethod = await _paymentService.GetPaymentProcessorBySystemName(input.SystemName);

            if (wheelPaymentMethod != null)
            {
                await wheelPaymentMethod.UpdateSetting(ObjectMapper.Map<PaymentProcessorInfo>(input));
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            await _wheelPaymentMethodRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetWheelPaymentMethodsToExcel(GetAllWheelPaymentMethodsForExcelInput input)
        {
            var paymentProcessors = await _paymentService.GetAllPaymentProcessor();

            var listPaymentProcessors = new List<GetWheelPaymentMethodForViewDto>();

            foreach (var item in paymentProcessors)
            {
                var data = await item.GetSetting();

                listPaymentProcessors.Add(ObjectMapper.Map<GetWheelPaymentMethodForViewDto>(data));
            }

            listPaymentProcessors = listPaymentProcessors
                .WhereIf(!string.IsNullOrEmpty(input.Filter), x => x.SystemName.Contains(input.Filter) || x.FriendlyName.Contains(input.Filter))
                .WhereIf(!string.IsNullOrEmpty(input.SystemNameFilter), x => x.SystemName.Contains(input.SystemNameFilter))
                .WhereIf(!string.IsNullOrEmpty(input.FriendlyNameFilter), x => x.SystemName.Contains(input.FriendlyNameFilter))
                .ToList();

            return _wheelPaymentMethodsExcelExporter.ExportToFile(listPaymentProcessors);
        }

        
        public async Task<List<WheelPaymentMethodForComboboxDto>> GetAllPaymentMethodForCombobox(int? selectedId)
        {
            var paymentProcessors = await _paymentService.GetAllPaymentProcessor();

            var listPaymentProcessors = new List<GetWheelPaymentMethodForViewDto>();

            foreach (var item in paymentProcessors)
            {
                var data = await item.GetSetting();

                listPaymentProcessors.Add(ObjectMapper.Map<GetWheelPaymentMethodForViewDto>(data));
            }

            var paymentProcessorItems = new List<WheelPaymentMethodForComboboxDto>(listPaymentProcessors.Select(x =>
                new WheelPaymentMethodForComboboxDto(x.SystemName, x.FriendlyName, false)));

            if (paymentProcessorItems.Count > 0)
            {
                if (selectedId.HasValue)
                {
                    var selectedItem = paymentProcessorItems.FirstOrDefault(x => x.Value == selectedId.Value.ToString());
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        paymentProcessorItems[0].IsSelected = true;
                    }
                }
            }

            return paymentProcessorItems;
        }
    }
}
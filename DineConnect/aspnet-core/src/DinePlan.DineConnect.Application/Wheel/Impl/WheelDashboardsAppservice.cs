﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Wheel.V2;
using DinePlan.DineConnect.Wheel.WheelDashboard;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class WheelDashboardsAppservice : DineConnectAppServiceBase, IWheelDashboardsAppservice
    {
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<WheelTicket> _wheelTicketRepository;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;

        public WheelDashboardsAppservice(
            IRepository<WheelTicket> wheelTicketRepository
            , IRepository<WheelDepartment> wheelDepartmentRepository
            , IRepository<WheelLocation> wheelLocationRepository
            , IRepository<Location> locationRepository
            )
        {
            _wheelTicketRepository = wheelTicketRepository;
            _wheelLocationRepository = wheelLocationRepository;
            _locationRepository = locationRepository;
        }

        public async Task<ChartSaleByHourOutput> GetSaleByHoursDashboard(GetChartInput input)
        {
            var output = new List<SaleByHourDashboardOutputDto>();

            var outputChart = new ChartSaleByHourOutput();
            if (input.Location > 0)
            {
                input.Locations = GetLocation(input.Location);
            }
            var allTickets = await GetAllWheelTicketDashboard(input);
            var saleByHours = allTickets.AsEnumerable().GroupBy(row => new
            {
                Hours = row.DateDelivery.Value.Hour,

            })
            .Select(grp => new SaleByHourDashboardOutputDto()
            {
                Hours = grp.Key.Hours,
                Tickets = grp.Count(),
                TotalSale = grp.Sum(x => x.Amount).Value,
            }).ToList();
            var hours = SplitList<int>(Enumerable.Range(0, 24).ToList(), 1).ToList();

            hours.ForEach(h =>
            {
                var item = new SaleByHourDashboardOutputDto();
                item.Tickets = saleByHours.Where(x => h.Any(a => a == x.Hours)).Select(x => x.Tickets).FirstOrDefault();
                item.TotalSale = saleByHours.Where(x => h.Any(a => a == x.Hours)).Select(x => x.TotalSale).FirstOrDefault();

                output.Add(item);
            });
            //var data = await _wheelDashboardManager.GetSaleByHours(DateTime.Today.AddDays(-1), DateTime.Now);
            //outputChart.Labels = data.Select(x => x.Hour).ToList();
            //outputChart.Datasets.Add(new Datasets
            //{
            //    Label = "Total Tickets",
            //    Data = data.Select(x => x.TicketCount).ToList(),
            //    Fill = false,
            //    BorderColor = "#28a745"
            //});
            //outputChart.Datasets.Add(new Datasets
            //{
            //    Label = "Total Sales",
            //    Data = data.Select(x => (int)x.Amount).ToList(),
            //    Fill = false,
            //    BorderColor = "#dc3545"
            //});

            return outputChart;
        }

        public async Task<WheelOrderDashboardOutputDto> GetWheelOrderDashboard(GetChartInput input)
        {
            var output = new WheelOrderDashboardOutputDto();
            if (input.Location > 0)
            {
                input.Locations = GetLocation(input.Location);
            }
            var allTickets = await GetAllWheelTicketDashboard(input);

            output.TotalOrders = allTickets.Count();
            output.Totalsales = allTickets.Sum(x => x.Amount);
            return output;
        }

        private List<SimpleLocationDto> GetLocation(int locationId)
        {
            var locationList = new List<SimpleLocationDto>();
            if (locationId > 0)
            {
                var loc = _locationRepository.Get(locationId);
                if (loc != null)
                {
                    var myLd = ObjectMapper.Map<SimpleLocationDto>(loc);
                    locationList = new List<SimpleLocationDto> { myLd };
                }
            }
            return locationList;
        }

        private async Task<List<WheelTicket>> GetAllWheelTicketDashboard(GetChartInput input)
        {
            var query = _wheelTicketRepository
                .GetAllIncluding(t => t.Payment);
            var allTickets = (await query.ToListAsync())
                .Where(x => x.DateDelivery.HasValue && x.DateDelivery.Value.Date >= input.StartDate.Date)
                .Where(x => x.DateDelivery.Value.Date <= input.EndDate.Date).ToList();

            if (input.Locations?.Count() > 0)
            {
                var wheelLocationList = allTickets.Select(x => new { wheelLoctaionId = JsonConvert.DeserializeObject<WheelBillingInfo>(x.Payment.ExtraInformation)?.WheelLocationId, wheelTicketId = x.Id });

                var wheelLocationChoose = await _wheelLocationRepository.GetAllIncluding(x => x.Location).Where(x => input.Locations.Select(y => y.Id).Contains(x.Location.Id)).Select(x => x.Id).ToListAsync();

                var ticketsByLocation = wheelLocationList.Where(x => wheelLocationChoose.Contains((int)x.wheelLoctaionId)).Select(x => x.wheelTicketId).ToList();
                allTickets = allTickets.Where(x => ticketsByLocation.Contains(x.Id)).ToList();
            }

            return allTickets;
        }

        private static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 30, int overlap = 0)
        {
            for (int i = 0; i < locations.Count; i += nSize - overlap)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

    }
}
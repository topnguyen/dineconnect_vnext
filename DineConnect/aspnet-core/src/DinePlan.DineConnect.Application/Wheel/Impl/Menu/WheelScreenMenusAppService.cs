﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.Wheel.Impl.Menu
{
    
    public class WheelScreenMenusAppService : DineConnectAppServiceBase, IWheelScreenMenusAppService
    {
        private readonly ConnectSession _connectSession;
        private readonly IRepository<WheelItemSoldOut> _itemSoldOutRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<OrderTagGroup> _orderTagGroupRepository;
        private readonly IRepository<ProductComboGroupDetail> _productComboGroupDetailRepository;
        private readonly IRepository<ProductComboItem> _productComboItemRepository;
        private readonly IRepository<ProductCombo> _productComboRepository;
        private readonly IRepository<ScreenMenuCategory> _screenMenuCategoryRepository;
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IRepository<ScreenMenu> _screenMenuRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;

        public WheelScreenMenusAppService(
            IRepository<ScreenMenu> screenMenuRepository,
            IRepository<ScreenMenuCategory> screenMenuCategoryRepository,
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<OrderTagGroup> orderTagGroupRepository,
            IRepository<ProductCombo> productComboRepository,
            IRepository<ProductComboItem> productComboItemRepository,
            IRepository<MenuItem> menuItemRepository,
            IRepository<ProductComboGroupDetail> productComboGroupDetailRepository,
            IRepository<WheelItemSoldOut> itemSoldOutRepository,
            IRepository<WheelLocation> wheelLocationRepository, 
            ConnectSession connectSession)
        {
            _screenMenuRepository = screenMenuRepository;
            _screenMenuCategoryRepository = screenMenuCategoryRepository;
            _screenMenuItemRepository = screenMenuItemRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _orderTagGroupRepository = orderTagGroupRepository;
            _productComboRepository = productComboRepository;
            _productComboItemRepository = productComboItemRepository;
            _connectSession = connectSession;
            _menuItemRepository = menuItemRepository;
            _productComboGroupDetailRepository = productComboGroupDetailRepository;
            _itemSoldOutRepository = itemSoldOutRepository;
            _wheelLocationRepository = wheelLocationRepository;
        }

        
        public async Task<int> CreateOrEditWheelScreenMenu(CreateOrEditWheelScreenMenuInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var screenMenu = new ScreenMenu
            {
                Type = ScreenMenuType.WheelScreenMenu,
                Id = input.Id,
                Name = input.Name,
                TenantId = tenantId,
                OrganizationId = _connectSession.OrgId
            };

            dynamic screenMenuDynamic = new ExpandoObject();
            screenMenuDynamic.Description = input.Description;
            screenMenuDynamic.Published = input.Published;
            screenMenuDynamic.Order = 0;

            if (input.Id == 0)
            {
                var count = await _screenMenuRepository
                    .GetAll()
                    .Where(x => x.TenantId == tenantId && x.Type == ScreenMenuType.WheelScreenMenu)
                    .CountAsync();

                screenMenuDynamic.Order = count;
            }

            screenMenu.Dynamic = JsonConvert.SerializeObject(screenMenuDynamic);

            var screenMenuId = await _screenMenuRepository.InsertOrUpdateAndGetIdAsync(screenMenu);

            //await RemoveCache(input.Id);
            return screenMenuId;
        }

        
        public async Task DeleteWheelScreenMenu(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            await _screenMenuItemRepository.DeleteAsync(w => w.ScreenMenuCategory.ScreenMenuId == input.Id);

            await _screenMenuCategoryRepository.DeleteAsync(w => w.ScreenMenuId == input.Id);

            await _screenMenuRepository.DeleteAsync(s => s.Id == input.Id && s.TenantId == tenantId);

            //await RemoveCache(input.Id);
        }

        public async Task<PagedResultDto<WheelScreenMenusListDto>> GetAll(GetWheelScreenMenusInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var query = _screenMenuRepository
                .GetAll()
                .Where(w => w.TenantId == tenantId && w.Type == ScreenMenuType.WheelScreenMenu);

            var allWheelScreenMenus = await query.ToListAsync();

            var allWheelScreenMenuList = new List<WheelScreenMenusListDto>();

            foreach (var wheelScreenMenu in allWheelScreenMenus)
            {
                dynamic wheelScreenMenuDynamic = JObject.Parse(wheelScreenMenu.Dynamic);

                var itemCount = await _screenMenuItemRepository.GetAll()
                    .Where(x => x.ScreenMenuId == wheelScreenMenu.Id).CountAsync();

                var categoryCount = await _screenMenuItemRepository.GetAll()
                    .Where(x => x.ScreenMenuId == wheelScreenMenu.Id).CountAsync();

                var wheelScreenMenuDto = new WheelScreenMenusListDto
                {
                    Id = wheelScreenMenu.Id,
                    Name = wheelScreenMenu.Name,
                    Description = wheelScreenMenuDynamic.Description,
                    Published = wheelScreenMenuDynamic.Published,
                    Order = wheelScreenMenuDynamic.Order,
                    ItemCount = itemCount + categoryCount
                };

                allWheelScreenMenuList.Add(wheelScreenMenuDto);
            }

            allWheelScreenMenuList = allWheelScreenMenuList
                .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                .OrderBy(x => x.Order)
                .ToList();

            var wheelScreenMenuCount = allWheelScreenMenuList.Count();

            var response = new PagedResultDto<WheelScreenMenusListDto>(wheelScreenMenuCount, allWheelScreenMenuList);

            return response;
        }

        public async Task<WheelScreenMenuForEditDto> GetWheelScreenMenuForEdit(GetWheelScreenMenuForEditInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            //var cache = await _redisCacheService.GetAsync<WheelScreenMenuForEditDto>(
            //    AppConsts.GetWheelScreenMenuForEditKey + "_" + input.Id);

            //if (cache != null) return cache;

            var data = await _screenMenuRepository.SingleAsync(s =>
                s.Id == input.Id && s.TenantId == tenantId && s.Type == ScreenMenuType.WheelScreenMenu);

            dynamic dataDynamic = JObject.Parse(data.Dynamic);

            var wheelScreenMenuDto = new WheelScreenMenuForEditDto
            {
                Id = data.Id,
                Name = data.Name,
                Description = dataDynamic.Description,
                Published = dataDynamic.Published
            };

            //await _redisCacheService.SetAsync(AppConsts.GetWheelScreenMenuForEditKey + "_" + input.Id,
            //    wheelScreenMenuDto);

            return wheelScreenMenuDto;
        }

        [AbpAllowAnonymous]
        public async Task<List<WheelScreenMenuItemDto>> GetScreenCategories(GetWheelScreenMenuChildrenInput input)
        {
            var itemDos = new List<WheelScreenMenuItemDto>();
            var categories = await _screenMenuCategoryRepository
                .GetAll()
                .Where(x => x.ScreenMenuId == input.Id && x.ParentScreenMenuCategoryId == null)
                .ToListAsync();

            foreach (var category in categories)
            {
                dynamic categoryDynamic = JObject.Parse(category.Dynamic);
                if (input.IsGetClient && !(bool)categoryDynamic.Published) continue;
                var childrenDto = new WheelScreenMenuItemDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Published = categoryDynamic.Published,
                    Image = categoryDynamic.Image,
                    Price = categoryDynamic.Price ?? 0,
                    Order = categoryDynamic.Order,
                    Type = WheelScreenMenuItemType.Section,
                    DisplayAs = categoryDynamic.DisplayAs,
                    NumberOfColumns = categoryDynamic.NumberOfColumns,
                    ScreenMenuId = input.Id,
                    Tags = categoryDynamic.Tags == null
                        ? null
                        : JsonConvert.DeserializeObject<List<string>>(categoryDynamic.Tags.ToString())
                };

                itemDos.Add(childrenDto);
            }
            return itemDos.OrderBy(a=>a.Order).ToList();
        }


        [AbpAllowAnonymous]
        public async Task<List<WheelScreenMenuItemDto>> GetChildren(GetWheelScreenMenuChildrenInput input)
        {
            var itemDtos = new List<WheelScreenMenuItemDto>();

            var categories = await _screenMenuCategoryRepository
                .GetAll()
                .Where(x => x.ScreenMenuId == input.Id && x.ParentScreenMenuCategoryId == null)
                .ToListAsync();

            foreach (var category in categories)
            {
                dynamic categoryDynamic = JObject.Parse(category.Dynamic);
                if (input.IsGetClient && !(bool)categoryDynamic.Published) continue;
                var childrenDto = new WheelScreenMenuItemDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Published = categoryDynamic.Published,
                    Image = categoryDynamic.Image,
                    Price = categoryDynamic.Price ?? 0,
                    Order = categoryDynamic.Order,
                    Type = WheelScreenMenuItemType.Section,
                    DisplayAs = categoryDynamic.DisplayAs,
                    NumberOfColumns = categoryDynamic.NumberOfColumns,
                    ScreenMenuId = input.Id,
                    Tags = categoryDynamic.Tags == null
                        ? null
                        : JsonConvert.DeserializeObject<List<string>>(categoryDynamic.Tags.ToString())
                };

                itemDtos.Add(childrenDto);
            }
            return itemDtos.OrderBy(a=>a.Order).ToList();
        }

        public async Task Reorder(List<WheelScreenMenuReOrderInput> inputs)
        {
            var menus = await _screenMenuRepository
                .GetAll()
                .Where(w => inputs.Select(x => x.Id).Contains(w.Id))
                .ToListAsync();

            foreach (var menu in menus)
            {
                dynamic menuDynamic = JObject.Parse(menu.Dynamic);

                if (inputs != null)
                {
                    var inputOrder = inputs.FirstOrDefault(x => x.Id == menu.Id);
                    if(inputOrder!=null)
                        menuDynamic.Order = inputOrder.Order;
                }
                menu.Dynamic = JsonConvert.SerializeObject(menuDynamic);
                await _screenMenuRepository.UpdateAsync(menu);
            }
        }

        public async Task<List<WheelScreenMenuForComboboxItemDto>> GetScreenMenuForCombobox(int? selectedId = null)
        {

            var menus = await _screenMenuRepository
                .GetAll()
                .Where(x => x.Type == ScreenMenuType.WheelScreenMenu)
                .ToListAsync();

            var menuItems = new List<WheelScreenMenuForComboboxItemDto>(menus.Select(x =>
                new WheelScreenMenuForComboboxItemDto(x.Id.ToString(), x.Name, false)));

            if (menuItems.Count > 0)
                if (selectedId.HasValue)
                {
                    var selectedItem = menuItems.FirstOrDefault(x => x.Value == selectedId.Value.ToString());

                    if (selectedItem != null)
                        selectedItem.IsSelected = true;
                    else
                        menuItems[0].IsSelected = true;
                }

            return menuItems;
        }

        public async Task UpdatePublish(UpdatePublishInput input)
        {
            var menu = await _screenMenuRepository.GetAsync(input.Id);
            dynamic menuDynamic = JObject.Parse(menu.Dynamic);
            menuDynamic.Published = input.Publish;
            menu.Dynamic = JsonConvert.SerializeObject(menuDynamic);
            await _screenMenuRepository.UpdateAsync(menu);
        }

        public async Task<List<WheelScreenMenuItemDto>> GetSectionChildren(int menuId)
        {
            var categories = await _screenMenuCategoryRepository
                .GetAll()
                .Where(x => x.ScreenMenuId == menuId)
                
                .ToListAsync();

            var grouped = categories.GroupBy(x => x.ParentScreenMenuCategoryId)
                .Select(x => new GroupSectionCategory
                {
                    Key = x.Key,
                    Categories = x.ToList()
                }).ToList();

            var itemDtos = new List<WheelScreenMenuItemDto>();

            var rootCategories = grouped.FirstOrDefault(x => x.Key == null)?.Categories;
            if (rootCategories != null)
                foreach (var category in rootCategories)
                {
                    var childrenDto = GetChildrenOfSection(category.ParentScreenMenuCategoryId, category, grouped);
                    itemDtos.Add(childrenDto);
                }

            return itemDtos.OrderBy(a=>a.Order).ToList();
        }

        private WheelScreenMenuSectionDto GetChildrenOfSection(int? sectionId, ScreenMenuCategory category,
            List<GroupSectionCategory> screenMenuCategories)
        {
            dynamic categoryDynamic = JObject.Parse(category.Dynamic);
            var childrenDto = new WheelScreenMenuSectionDto
            {
                Id = category.Id,
                Name = category.Name,
                Published = categoryDynamic.Published,
                Image = categoryDynamic.Image,
                Price = categoryDynamic.Price ?? 0,
                Order = categoryDynamic.Order,
                Type = WheelScreenMenuItemType.Section,
                NumberOfColumns = categoryDynamic.NumberOfColumns,
                DisplayAs = categoryDynamic.DisplayAs,
                ScreenMenuId = category.ScreenMenuId,
                Tags = categoryDynamic.Tags == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(categoryDynamic.Tags.ToString())
            };

            return childrenDto;
        }


        private List<WheelMenuComboItemDto> GetWheelMenuItemsForCombo(
            ProductComboGroup productComboComboGroup,
            ScreenMenuCategory section, IQueryable<OrderTagGroup> allTagGroups
        )
        {
            var comboGroupItems = _productComboItemRepository.GetAll()
                .Where(x => x.ProductComboGroupId == productComboComboGroup.Id)
                .Include(x => x.MenuItem)
                .ThenInclude(t => t.Portions)
                .ToList();

            var menuItemIds = comboGroupItems.Select(x => x.MenuItem.Id).ToList();
            var menuItems = _menuItemRepository.GetAll()
                .Include(e => e.Portions)
                .Where(e => menuItemIds.Contains(e.Id));

            var itemMenu = new List<WheelScreenComboMenuItemDto>();

            foreach (var item in comboGroupItems.OrderBy(a=>a.SortOrder))
            {
                dynamic nutritionInfo = item.MenuItem?.NutritionInfo == null
                    ? null
                    : JObject.Parse(item.MenuItem.NutritionInfo);

                var menuItem = menuItems.FirstOrDefault(x => x.Id == item.MenuItemId);

                if (menuItem != null)
                {
                    var childrenItemDto = new WheelScreenComboMenuItemDto
                    {
                        MenuItemId = menuItem.Id,
                        Id = item.Id,
                        Name = item.Name,
                        Published = true,
                        Image = item.MenuItem?.Images,
                        Price = item.Price,
                        Order = 0,
                        Type = menuItem.ProductType == (int)WheelScreenMenuItemType.Combo
                            ? WheelScreenMenuItemType.Combo
                            : WheelScreenMenuItemType.Item,
                        ScreenMenuId = section?.ScreenMenuId ?? 0,
                        Portions = null,
                        OrderTags = FetchTagGroups(menuItem.Id, menuItem.CategoryId, allTagGroups),
                        Calories = nutritionInfo?.calories,
                        Description = item.MenuItem?.ItemDescription,
                        Quantity = item.Count,
                        AutoSelect = item.AutoSelect,
                        AddSeperately = item.AddSeperately
                    };

                    if (menuItem.ProductType == (int)WheelScreenMenuItemType.Combo)
                    {
                        var productCombo = _productComboRepository
                            .GetAll()
                            .Include(x => x.ProductComboGroupDetails)
                            .ThenInclude(x => x.ProductComboGroup.ComboItems)
                            .FirstOrDefault(a => a.MenuItemId == menuItem.Id);

                        var wheelMenuComboItemEditDto = new List<WheelMenuComboGroupDto>();

                        if (productCombo != null)
                        {
                            var combs = ObjectMapper.Map<List<ProductComboGroupEditDto>>(
                                _productComboGroupDetailRepository.GetAll()
                                    .Where(p => p.ProductComboId == productCombo.Id)
                                    .Select(p => p.ProductComboGroup)
                                    .OrderBy(e => e.SortOrder)
                                    .ToList());

                            if (!string.IsNullOrEmpty(productCombo.Sorting))
                            {
                                var sortedIds = productCombo.Sorting.Split(',').ToList();
                                if (sortedIds.Any())
                                {
                                    foreach (var id in sortedIds)
                                    {
                                        var idInt = Convert.ToInt32(id);
                                        var g = combs.FirstOrDefault(x => x.Id == idInt);
                                        if (g != null) g.SortOrder = sortedIds.IndexOf(id);
                                    }

                                    combs = combs.OrderBy(x => x.SortOrder).ToList();
                                }
                            }

                            combs.ForEach(c =>
                            {
                                c.ComboItems = ObjectMapper.Map<Collection<ProductComboItemEditDto>>(
                                    _productComboItemRepository.GetAll().Where(i => i.ProductComboGroupId == c.Id)
                                        .ToList());

                                wheelMenuComboItemEditDto.Add(new WheelMenuComboGroupDto
                                {
                                    Id = c.Id ?? 0,
                                    Name = c.Name,
                                    Maximum = c.Maximum,
                                    Minimum = c.Minimum,
                                    WheelComboItems = c.ComboItems.Select(x =>
                                        new WheelMenuComboItemDto
                                        {
                                            Id = x.Id ?? 0,
                                            Name = x.Name,
                                            AutoSelect = x.AutoSelect,
                                            WheelMenuComboItemId = c.Id,
                                            ScreenMenuItemId = null,
                                            Price = x.Price,
                                            Quantity = 1,
                                            MaxQuantity = x.Count,
                                            MenuItemId = x.MenuItemId,
                                            OrderTags = FetchTagGroups(x.MenuItemId, 0, allTagGroups),
                                            Type = WheelScreenMenuItemType.Item,
                                            Published = true
                                        }).ToList()
                                });
                            });

                            childrenItemDto.WheelComboGroups = wheelMenuComboItemEditDto;
                        }
                    }


                    itemMenu.Add(childrenItemDto);
                }
            }

            var comboGroupItemsMapping = comboGroupItems.Select(v =>
                new WheelMenuComboItemDto
                {
                    Id = v.Id,
                    Name = null,
                    Published = true,
                    WheelMenuComboItemId = productComboComboGroup.Id,
                    ScreenMenuItemId = null
                }).ToList();

            foreach (var menuItem in comboGroupItemsMapping)
            {
                var itemInside = itemMenu.FirstOrDefault(x => x.Id == menuItem.Id);

                if (itemInside != null)
                {
                    menuItem.Id = itemInside.Id;
                    menuItem.Name = itemInside.Name;
                    menuItem.Published = itemInside.Published;
                    menuItem.Image = itemInside.Image;
                    menuItem.Price = itemInside?.Price ?? 0;
                    menuItem.Order = itemInside.Order;
                    menuItem.Type = itemInside.Type;
                    menuItem.ScreenMenuId = itemInside.ScreenMenuId;
                    menuItem.Portions = itemInside.Portions;
                    menuItem.OrderTags = itemInside.OrderTags;
                    menuItem.Tags = itemInside.Tags;
                    menuItem.Description = itemInside.Description;
                    menuItem.MaxQuantity = itemInside.Quantity;
                    menuItem.Quantity = 1;
                    menuItem.AutoSelect = itemInside.AutoSelect;
                    menuItem.AddSeperately = itemInside.AddSeperately;
                    menuItem.MenuItemId = itemInside.MenuItemId;
                    menuItem.WheelComboGroups = itemInside.WheelComboGroups;
                }
            }

            return comboGroupItemsMapping;
        }

        //private async Task RemoveCache(int id = 0)
        //{
        //    var tenantId = AbpSession.GetTenantId();

        //    //await _redisCacheService.RemoveAsync(AppConsts.GetAllWheelScreenMenuKey + "_" + tenantId,
        //    //    $"{AppConsts.GetWheelScreenMenuForEditKey}_{id}",
        //    //    $"{AppConsts.GetChildrenScreenMenuKey}_{id}",
        //    //    AppConsts.GetWheelScreenMenuForComboboxKey);
        //}

        [AbpAllowAnonymous]
        public async Task<WheelScreenMenuSectionDto> GetSectionMenu(int menuId, int categoryId,
            string subCategory, int pageSize, int pageNumber, bool allowPreOrder, int? locationId = null)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var section = new ScreenMenuCategory();
                var output = new WheelScreenMenuSectionDto();
                var sections = await _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == menuId && x.Id == categoryId).ToListAsync();

                foreach (var item in sections)
                {
                    dynamic objectItem = JObject.Parse(item.Dynamic);
                    if ((bool)objectItem.Published)
                    {
                        section = item;
                        break;
                    }
                }

                if (section.Id > 0)
                    output = await GetSectionMenuForDisplay(section, subCategory, pageSize, pageNumber, allowPreOrder,
                        locationId);

                return output;
            }
        }


        [AbpAllowAnonymous]
        public async Task<WheelScreenMenuItemDto> ApiGetScreenMenuItemDetail(int screenMenuItemId,
            int locationId)
        {
            var output = new WheelScreenMenuItemDto();
            var wheelLocation = await _wheelLocationRepository.FirstOrDefaultAsync(x => x.Id == locationId);
            if (wheelLocation != null)
            {
                    using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
                    {

                        var allItems = _screenMenuItemRepository
                            .GetAll()
                            .Include(x => x.MenuItem)
                            .Include(x => x.MenuItem.Portions)
                            .Include(x => x.ProductCombo).ThenInclude(x => x.MenuItem.Portions)
                            .Where(x => x.Id == screenMenuItemId)
                            .ToList();

                        if (!allItems.Any())
                            return null;

                        var item = allItems.LastOrDefault();


                        var allTagGroups = _orderTagGroupRepository.GetAll()
                            .Include(t => t.Tags).Include(z => z.Maps);

                        var allSoldOutItems = _itemSoldOutRepository.GetAll()
                            .Where(x => x.LocationId == wheelLocation.LocationId && x.Disable);


                        if (item != null)
                        {
                            var itemMenu = new List<WheelScreenMenuSectionDto>();
                            if (item.MenuItemId.HasValue)
                            {
                                dynamic itemDynamic = JObject.Parse(item.Dynamic);
                                if ((bool)itemDynamic.Published)
                                {
                                    var prices = new List<WheelScreenMenuItemlPricesForEditDto>();
                                    if (item.MenuItem?.Portions != null && item.MenuItem.Portions.Any())
                                        foreach (var menuItemPortion in item.MenuItem.Portions)
                                            prices.Add(new WheelScreenMenuItemlPricesForEditDto
                                            {
                                                ItemId = menuItemPortion.MenuItemId,
                                                Name = menuItemPortion.Name,
                                                Price = menuItemPortion.Price,
                                                Multiplier = menuItemPortion.MultiPlier,
                                                Id = menuItemPortion.Id
                                            });
                                    var firstPrice = prices.FirstOrDefault();


                                    dynamic nutritionInfo = item.MenuItem?.NutritionInfo == null
                                        ? null
                                        : JObject.Parse(item.MenuItem.NutritionInfo);

                                    var childrenItemDto = new WheelScreenMenuSectionDto
                                    {
                                        Id = item.Id,
                                        Name = item.Name,
                                        MenuItemId = item.MenuItemId ?? 0,
                                        Published = item.Published,
                                        AllowPreOrder = itemDynamic.AllowPreOrder,
                                        Image = itemDynamic.Image,
                                        Price = firstPrice?.Price ?? 0,
                                        Order = item.Order,
                                        Type = WheelScreenMenuItemType.Item,
                                        Portions = prices,
                                        OrderTags = FetchTagGroups(item.MenuItemId.Value,
                                            item.MenuItem?.CategoryId ?? 0, allTagGroups),
                                        DisplayNutritionFacts = Convert.ToBoolean(itemDynamic.DisplayNutritionFacts)
                                    };

                                    childrenItemDto.Calories =
                                        childrenItemDto.DisplayNutritionFacts.HasValue &&
                                        childrenItemDto.DisplayNutritionFacts.Value
                                            ? nutritionInfo?.calories
                                            : "0";
                                    childrenItemDto.NutritionInfo =
                                        childrenItemDto.DisplayNutritionFacts.HasValue &&
                                        childrenItemDto.DisplayNutritionFacts.Value
                                            ? nutritionInfo?.ToString()
                                            : "";

                                    childrenItemDto.Tags = itemDynamic.Tags == null
                                        ? null
                                        : JsonConvert.DeserializeObject<List<string>>(itemDynamic.Tags.ToString());
                                    childrenItemDto.Description = itemDynamic.Description;
                                    childrenItemDto.Video = itemDynamic.Video;

                                    childrenItemDto.RecommendedItems = itemDynamic.RecommendedItems == null
                                        ? null
                                        : JsonConvert.DeserializeObject<List<WheelRecommendedForItemEditDto>>(
                                            itemDynamic
                                                .RecommendedItems
                                                .ToString());

                                    childrenItemDto.IsSoldOut =
                                        allSoldOutItems.Any(x => x.MenuItemId == item.MenuItemId);
                                    itemMenu.Add(childrenItemDto);
                                }
                            }
                            else if (item.ProductComboId.HasValue)
                            {
                                var comboIds = item.ProductComboId ?? 0;
                                var productCombos = await _productComboRepository
                                    .GetAll()
                                    .Include(x => x.ProductComboGroupDetails)
                                    .ThenInclude(x => x.ProductComboGroup)
                                    .Where(x => x.Id == comboIds)
                                    .ToListAsync();
                                var comboItem = item;
                                dynamic comboItemDynamic = JObject.Parse(comboItem.Dynamic);
                                if ((bool)comboItemDynamic.Published)
                                {
                                    List<WheelScreenMenuItemlPricesForEditDto> prices =
                                        comboItem.ProductCombo.MenuItem?.Portions != null
                                            ? comboItem.ProductCombo.MenuItem?.Portions.Select(x =>
                                                new WheelScreenMenuItemlPricesForEditDto
                                                {
                                                    Price = x.Price,
                                                    Name = x.Name,
                                                    Id = x.Id
                                                }).ToList()
                                            : comboItemDynamic.Prices == null
                                                ? null
                                                : JsonConvert
                                                    .DeserializeObject<List<WheelScreenMenuItemlPricesForEditDto>>(
                                                        comboItemDynamic.Prices.ToString());

                                    if (comboItem.ScreenMenuId != null)
                                        if (prices != null)
                                        {
                                            var combo = new WheelScreenMenuSectionDto
                                            {
                                                MenuItemId = (int)comboItem.ProductCombo?.MenuItemId,
                                                Id = comboItem.Id,
                                                Image = comboItemDynamic.Image,
                                                SubCategory = comboItem.SubCategory,
                                                Order = comboItem.Order,
                                                Name = comboItem.Name,
                                                Type = WheelScreenMenuItemType.Combo,
                                                Description = comboItemDynamic.Description,
                                                Dynamic = comboItem.Dynamic,
                                                Published = comboItemDynamic.Published,
                                                ScreenCategoryId = comboItem.ScreenCategoryId,
                                                ScreenMenuId = comboItem.ScreenMenuId.Value,
                                                BasePrice = prices.FirstOrDefault()?.Price ?? 0,
                                                Portions = prices,
                                                Tags = comboItemDynamic.Tags == null
                                                    ? null
                                                    : JsonConvert.DeserializeObject<List<string>>(
                                                        comboItemDynamic.Tags.ToString()),
                                                Price = prices.FirstOrDefault()?.Price ?? 0,
                                                OrderTags = FetchTagGroups((int)comboItem.ProductCombo?.MenuItemId,
                                                    0, allTagGroups)
                                            };

                                            var productCombo = productCombos.FirstOrDefault(x =>
                                                comboItem.ProductComboId != null &&
                                                x.Id ==
                                                comboItem.ProductComboId.Value);

                                            var wheelComboGroupDto = new List<WheelMenuComboGroupDto>();
                                            if (productCombo != null)
                                            {
                                                combo.Name = productCombo.Name;

                                                foreach (var productComboComboGroup in productCombo.ComboGroups.OrderBy(a=>a.SortOrder))
                                                    wheelComboGroupDto.Add(new WheelMenuComboGroupDto
                                                    {
                                                        Id = productComboComboGroup.Id,
                                                        Name = productComboComboGroup.Name,
                                                        Maximum = productComboComboGroup.Maximum,
                                                        Minimum = productComboComboGroup.Minimum,
                                                        WheelComboItems =
                                                            GetWheelMenuItemsForCombo(productComboComboGroup, null, allTagGroups)
                                                    });

                                                combo.WheelComboGroups = wheelComboGroupDto;

                                                if (combo.Dynamic != null)
                                                {
                                                    dynamic itemDynamic = JObject.Parse(combo.Dynamic);
                                                    combo.Image = itemDynamic.Image;
                                                    combo.Video = itemDynamic.Video;
                                                    combo.RecommendedItems = itemDynamic.RecommendedItems == null
                                                        ? null
                                                        : JsonConvert
                                                            .DeserializeObject<List<WheelRecommendedForItemEditDto>>(
                                                                itemDynamic
                                                                    .RecommendedItems
                                                                    .ToString());
                                                }

                                                combo.IsSoldOut =
                                                    allSoldOutItems.Any(x =>
                                                        x.MenuItemId == comboItem.ProductCombo.MenuItemId);
                                                itemMenu.Add(combo);
                                            }
                                        }
                                }
                            }

                            output = itemMenu.Last();
                            return output;
                        }
                    }

            }

            return output;
        }

        private List<WheelOrderTagGroupListDto> FetchTagGroups(int menuItemId, int menuItemCategoryId, IQueryable<OrderTagGroup> inputGroups)
        {
            var allTagGroups = inputGroups.ToList();
            if (menuItemCategoryId == 0)
            {
                var mItem = _menuItemRepository.Get(menuItemId);
                if (mItem != null) menuItemCategoryId = mItem.CategoryId;
            }
            var maps = allTagGroups.SelectMany(x => x.Maps)
                .Where(x => x.CategoryId == null || x.CategoryId == menuItemCategoryId)
                .Where(x => x.MenuItemId == null || x.MenuItemId == menuItemId);

            allTagGroups = allTagGroups.Where(x => maps.Any(y => y.OrderTagGroupId == x.Id))
                .OrderBy(x => x.SortOrder).ToList();

            var myOutput = ObjectMapper.Map<List<WheelOrderTagGroupListDto>>(allTagGroups);

            return myOutput;
        }

        private async Task<WheelScreenMenuSectionDto> GetSectionMenuForDisplay(ScreenMenuCategory section,
            string subCategory,
            int pageSize, int pageNumber, bool allowPreOrder, int? locationId = null)
        {
            dynamic categoryDynamic = JObject.Parse(section.Dynamic);

            var output = new WheelScreenMenuSectionDto
            {
                Id = section.Id,
                Name = section.Name,
                Published = categoryDynamic.Published,
                Image = categoryDynamic.Image,
                Price = categoryDynamic.Price ?? 0,
                Order = categoryDynamic.Order,
                Type = WheelScreenMenuItemType.Section,
                DisplayAs = categoryDynamic.DisplayAs,
                NumberOfColumns = categoryDynamic.NumberOfColumns,
                ScreenMenuId = section.ScreenMenuId,
                Tags = categoryDynamic.Tags == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(categoryDynamic.Tags.ToString())
            };

            var isMain = !string.IsNullOrEmpty(subCategory) && subCategory.Equals("All");

            var items = await _screenMenuItemRepository
                .GetAll()
                .Include(x => x.MenuItem.Portions)
                .Include(x => x.ProductCombo).ThenInclude(x => x.MenuItem.Portions)
                .Where(x => x.ScreenMenuId == section.ScreenMenuId && x.ScreenCategoryId == section.Id)
                .WhereIf(!isMain, x => x.SubCategory.Equals(subCategory))
                .OrderBy(t => t.Order).ThenBy(t => t.Name)
                .Skip(pageNumber * pageSize).Take(pageSize)
                .ToListAsync();

            var wheelLocation = await _wheelLocationRepository.FirstOrDefaultAsync(x => x.Id == locationId);
            var soldOutItems = wheelLocation != null
                ? await _itemSoldOutRepository.GetAll()
                    .Where(x => x.LocationId == wheelLocation.LocationId && x.Disable).ToListAsync()
                : new List<WheelItemSoldOut>();

            var itemMenu = new List<WheelScreenMenuSectionDto>();

            foreach (var item in items)
            {
                dynamic itemDynamic = JObject.Parse(item.Dynamic);
                if ((bool)itemDynamic.Published)
                {
                    var firstPrice = 0M;
                    if (item.MenuItem?.Portions != null && item.MenuItem.Portions.Any())
                        firstPrice = item.MenuItem.Portions.First().Price;

                    if (item.ProductComboId.HasValue && item.ProductCombo?.MenuItem?.Portions != null &&
                        item.ProductCombo.MenuItem.Portions.Any())
                        firstPrice = item.ProductCombo.MenuItem.Portions.First().Price;

                    var childrenItemDto = new WheelScreenMenuSectionDto
                    {
                        Id = item.Id,
                        Name = item.Name,
                        MenuItemId = item.MenuItemId ?? 0,
                        Published = item.Published,
                        AllowPreOrder = itemDynamic.AllowPreOrder,
                        Image = itemDynamic.Image,
                        Price = firstPrice,
                        Order = item.Order,
                        Type = item.ProductComboId.HasValue
                            ? WheelScreenMenuItemType.Combo
                            : WheelScreenMenuItemType.Item,
                        ScreenMenuId = section.ScreenMenuId,
                        DisplayNutritionFacts = Convert.ToBoolean(itemDynamic.DisplayNutritionFacts),
                        Tags = itemDynamic.Tags == null
                            ? null
                            : JsonConvert.DeserializeObject<List<string>>(itemDynamic.Tags.ToString()),
                        Description = itemDynamic.Description,
                        Video = itemDynamic.Video,
                        IsSoldOut = soldOutItems.Any(x => x.MenuItemId == item.MenuItemId)
                    };


                    itemMenu.Add(childrenItemDto);
                }
            }

            output.Items = itemMenu.OrderBy(a=>a.Order).ToList();
            return output;
        }


        [AbpAllowAnonymous]
        public async Task<WheelScreenMenuSectionDto> GetSubCategories(int menuId, int categoryId)
        {
            var section = await _screenMenuCategoryRepository
                .GetAll()
                .Where(x => x.ScreenMenuId == menuId && x.Id == categoryId)
                .FirstOrDefaultAsync();

            var allSubCategories = new List<string>();

            var subCategories = (await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == menuId && x.ScreenCategoryId == categoryId && x.Published == true &&
                                !string.IsNullOrEmpty(x.SubCategory))
                    .GroupBy(x => x.SubCategory)
                    .Select(x => new { SubCategory = x.Key, Order = x.Min(g => g.Order) })
                    .ToListAsync())
                .OrderBy(x => x.Order)
                .Select(x => x.SubCategory).ToList();

            allSubCategories.AddRange(subCategories);
            allSubCategories.Add("All");

            dynamic categoryDynamic = JObject.Parse(section.Dynamic);

            var output = new WheelScreenMenuSectionDto
            {
                Id = section.Id,
                Name = section.Name,
                Image = categoryDynamic.Image,
                Order = categoryDynamic.Order,
                Type = WheelScreenMenuItemType.Section,
                DisplayAs = categoryDynamic.DisplayAs,
                NumberOfColumns = categoryDynamic.NumberOfColumns,
                ScreenMenuId = section.ScreenMenuId,
                SubCategories = allSubCategories
            };

            return output;
        }

        [AbpAllowAnonymous]
        public async Task<SearchProductResult> GetSearchProduct(int menuId, string search)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var categories = await GetChildren(new GetWheelScreenMenuChildrenInput { Id = menuId });
                var section = new ScreenMenuCategory();

                foreach (var category in categories)
                {
                    var sections = await _screenMenuCategoryRepository
                        .GetAll()
                        .Where(x => x.ScreenMenuId == menuId && x.Id == category.Id).ToListAsync();

                    foreach (var item in sections)
                    {
                        dynamic objectItem = JObject.Parse(item.Dynamic);
                        if ((bool)objectItem.Published)
                        {
                            section = item;
                            break;
                        }
                    }

                    if (section.Id > 0)
                    {
                        var items = await _screenMenuItemRepository
                            .GetAll()
                            .Include(x => x.MenuItem.Portions)
                            .Include(x => x.ProductCombo)
                            .Where(x => section != null && x.ScreenMenuId == section.ScreenMenuId &&
                                        x.ScreenCategoryId == section.Id && x.Name.ToLower().Contains(search.ToLower()))
                            .OrderBy(t => t.Order).ThenBy(t => t.Name)
                            .ToListAsync();

                        if (items.Count > 0)
                            return new SearchProductResult
                            {
                                CategoryId = category.Id,
                                SubCategoryId = "All"
                            };
                    }
                }

                return null;
            }
        }
    }

    public class GroupSectionCategory
    {
        public int? Key { get; set; }
        public List<ScreenMenuCategory> Categories { get; set; }
    }
}
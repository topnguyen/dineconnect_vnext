﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.Wheel.Impl.Menu
{
    
    public class WheelScreenMenuItemsAppService : DineConnectAppServiceBase, IWheelScreenMenuItemsAppService
    {
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<OrderTagGroup> _orderTagGroupRepository;
        private readonly IRepository<OrderTag> _orderTagRepository;
        private readonly IRepository<ScreenMenuCategory> _screenMenuCategoryRepository;
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IRepository<ScreenMenu> _screenMenuRepository;
        private readonly IWheelScreenMenusAppService _wheelScreenMenusAppService;

        public WheelScreenMenuItemsAppService(
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IRepository<ScreenMenuCategory> screenMenuCategoryRepository,
            IRepository<OrderTag> orderTagRepository,
            IRepository<OrderTagGroup> orderTagGroupRepository,
            IRepository<MenuItem> menuItemRepository,
            IWheelScreenMenusAppService wheelScreenMenusAppService,
            IRepository<ScreenMenu> screenMenuRepository
        )
        {
            _screenMenuItemRepository = screenMenuItemRepository;
            _screenMenuCategoryRepository = screenMenuCategoryRepository;
            _orderTagRepository = orderTagRepository;
            _orderTagGroupRepository = orderTagGroupRepository;
            _menuItemRepository = menuItemRepository;
            _wheelScreenMenusAppService = wheelScreenMenusAppService;
            _screenMenuRepository = screenMenuRepository;
        }

        
        public async Task CreateOrEditWheelScreenMenuSection(CreateOrEditWheelScreenMenuItemInput input)
        {
            if (input.Id <= 0)
                await CreateScreenMenuItem(input);
            else
                await UpdateScreenMenuItem(input);
          
        }

        
        public async Task DeleteWheelScreenMenuSection(DeleteWheelScreenMenuSectionInput input)
        {
            var keys = new List<string>();

            switch (input.Type)
            {
                case WheelScreenMenuItemType.Item:
                case WheelScreenMenuItemType.Combo:
                    await _screenMenuItemRepository.DeleteAsync(w => w.Id == input.Id);
                   
                    break;
                case WheelScreenMenuItemType.Section:

                    await _screenMenuItemRepository.DeleteAsync(w => w.ScreenCategoryId == input.Id);

                    await _screenMenuCategoryRepository.DeleteAsync(w =>
                        w.Id == input.Id || w.ParentScreenMenuCategoryId == input.Id);

                    break;
            }

        }

        public async Task<List<WheelScreenMenuItemDto>> GetChildren(GetWheelScreenMenuChildrenInput input)
        {
            var returnDtos = new List<WheelScreenMenuItemDto>();

            var menuItems = _screenMenuItemRepository
                .GetAll()
                .Include(x => x.ScreenMenuCategory)
                .Where(x => x.ScreenCategoryId == input.Id)
                .ToList();

            foreach (var item in menuItems)
            {
                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                var children = new WheelScreenMenuItemDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    SubCategory = item.SubCategory,
                    Published = itemDynamic.Published,
                    Image = itemDynamic.Image,
                    Price = 0,
                    Order = itemDynamic.Order,
                    Type = WheelScreenMenuItemType.Item,
                    ParentId = item.ScreenCategoryId,
                    ScreenMenuId = item.ScreenMenuId ?? item.ScreenMenuCategory.ScreenMenuId,
                    Portions = null,
                    OrderTags = null
                };

                returnDtos.Add(children);
            }

            var categories = _screenMenuCategoryRepository
                .GetAll()
                .Where(x => x.ParentScreenMenuCategoryId == input.Id)
                .ToList();

            foreach (var category in categories)
            {
                dynamic categoryDynamic = JObject.Parse(category.Dynamic);

                var children = new WheelScreenMenuItemDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Published = categoryDynamic.Published,
                    Image = categoryDynamic.Image,
                    Price = categoryDynamic.Price ?? 0,
                    Order = categoryDynamic.Order,
                    Type = WheelScreenMenuItemType.Section,
                    ParentId = category.ParentScreenMenuCategoryId,
                    ScreenMenuId = category.ScreenMenuId
                };

                returnDtos.Add(children);
            }


            returnDtos = returnDtos.WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    x => x.Name.Contains(input.Filter))
                .OrderBy(x => x.Order)
                .ToList();
            return returnDtos;
        }


        public async Task<WheelScreenMenuItemForEditDto> GetWheelScreenMenuItemForEditSection(
            GetWheelScreenMenuItemForEditInput input)
        {
          
            var dataDto = new WheelScreenMenuItemForEditDto();
            if (input.Type == WheelScreenMenuItemType.Item)
            {
                var item = _screenMenuItemRepository
                    .Query(x => x
                        .Include(y => y.ScreenMenuCategory)
                        .Include(y => y.MenuItem)
                        .Where(y => y.Id == input.Id))
                    .FirstOrDefault();

                if (item == null) return null;

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                dataDto.Id = item.Id;

                dataDto.Name = item.Name;
                dataDto.SubCategory = item.SubCategory;

                dataDto.Description = itemDynamic.Description;
                dataDto.Published = itemDynamic.Published;
                dataDto.Image = itemDynamic.Image;
                dataDto.Video = itemDynamic.Video;
                dataDto.DisplayAs = itemDynamic.DisplayAs;
                dataDto.NumberOfColumns = itemDynamic.NumberOfColumns;
                dataDto.HideGridTitles = itemDynamic.HideGridTitles;
                dataDto.MarkAsNew = itemDynamic.MarkAsNew;
                dataDto.MarkAsSignature = itemDynamic.MarkAsSignature;
                dataDto.Note = itemDynamic.Note;
                dataDto.PreperationTime = itemDynamic.PreperationTime;
                dataDto.IngredientWarnings = itemDynamic.IngredientWarnings;
                dataDto.DisplayNutritionFacts = itemDynamic.DisplayNutritionFacts;
                dataDto.AllowPreOrder = itemDynamic.AllowPreOrder;
                dataDto.Order = itemDynamic.Order;
                dataDto.Type = WheelScreenMenuItemType.Item;
                dataDto.ScreenMenuId = item.ScreenMenuId ?? item.ScreenMenuCategory.ScreenMenuId;
                dataDto.ParentId = item.ScreenCategoryId;
                dataDto.SeletedMenuItemIdPrice = item.MenuItemId;
                dataDto.SelectedMenuIdPriceName = item.MenuItem?.Name;
                dataDto.Prices = itemDynamic.Prices == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelScreenMenuItemlPricesForEditDto>>(itemDynamic.Prices
                        .ToString());

                dataDto.RecommendedItems = itemDynamic.RecommendedItems == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelRecommendedForItemEditDto>>(itemDynamic.RecommendedItems
                        .ToString());

                dataDto.Tags = itemDynamic.Tags == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(itemDynamic.Tags.ToString());
                dataDto.TimingSettings = itemDynamic.TimingSettings == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(itemDynamic.TimingSettings.ToString());
            }
            else if (input.Type == WheelScreenMenuItemType.Section)
            {
                var category = _screenMenuCategoryRepository
                    .Query(x => x
                        .Where(y => y.Id == input.Id))
                    .FirstOrDefault();

                if (category == null) return null;

                dynamic categoryDynamic = JObject.Parse(category.Dynamic);

                dataDto.Id = category.Id;
                dataDto.Name = category.Name;
                dataDto.Description = categoryDynamic.Description;
                dataDto.Published = categoryDynamic.Published;
                dataDto.Image = categoryDynamic.Image;
                dataDto.Video = categoryDynamic.Video;
                dataDto.DisplayAs = categoryDynamic.DisplayAs;
                dataDto.NumberOfColumns = categoryDynamic.NumberOfColumns;
                dataDto.HideGridTitles = categoryDynamic.HideGridTitles;
                dataDto.MarkAsNew = categoryDynamic.MarkAsNew;
                dataDto.MarkAsSignature = categoryDynamic.MarkAsSignature;
                dataDto.Note = categoryDynamic.Note;
                dataDto.PreperationTime = categoryDynamic.PreperationTime;
                dataDto.IngredientWarnings = categoryDynamic.IngredientWarnings;
                dataDto.DisplayNutritionFacts = categoryDynamic.DisplayNutritionFacts;
                dataDto.Order = categoryDynamic.Order;
                dataDto.Type = WheelScreenMenuItemType.Section;
                dataDto.ScreenMenuId = category.ScreenMenuId;
                dataDto.ParentId = category.ParentScreenMenuCategoryId;
                dataDto.SeletedMenuItemIdPrice = categoryDynamic.SeletedMenuItemIdPrice;
                dataDto.SelectedMenuIdPriceName = string.Empty;
                dataDto.Prices = categoryDynamic.Prices == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelScreenMenuItemlPricesForEditDto>>(categoryDynamic.Prices
                        .ToString());

                dataDto.RecommendedItems = categoryDynamic.RecommendedItems == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelRecommendedForItemEditDto>>(
                        categoryDynamic.RecommendedItems.ToString());

                dataDto.Tags = categoryDynamic.Tags == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(
                        categoryDynamic.Tags.ToString());
                dataDto.TimingSettings = categoryDynamic.TimingSettings == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(categoryDynamic.TimingSettings.ToString());
            }
            else if (input.Type == WheelScreenMenuItemType.Combo)
            {
                var item = _screenMenuItemRepository
                    .Query(x => x
                        .Include(y => y.ScreenMenuCategory)
                        .Include(y => y.ProductCombo).Include(t => t.MenuItem)
                        .Where(y => y.Id == input.Id))
                    .FirstOrDefault();

                if (item == null) return null;

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                dataDto.Id = item.Id;

                dataDto.Name = item.Name;
                dataDto.SubCategory = item.SubCategory;

                dataDto.Description = itemDynamic.Description;
                dataDto.Published = itemDynamic.Published;
                dataDto.Image = itemDynamic.Image;
                dataDto.Video = itemDynamic.Video;
                dataDto.DisplayAs = itemDynamic.DisplayAs;
                dataDto.NumberOfColumns = itemDynamic.NumberOfColumns;
                dataDto.HideGridTitles = itemDynamic.HideGridTitles;
                dataDto.MarkAsNew = itemDynamic.MarkAsNew;
                dataDto.MarkAsSignature = itemDynamic.MarkAsSignature;
                dataDto.Note = itemDynamic.Note;
                dataDto.PreperationTime = itemDynamic.PreperationTime;
                dataDto.IngredientWarnings = itemDynamic.IngredientWarnings;
                dataDto.DisplayNutritionFacts = itemDynamic.DisplayNutritionFacts;
                dataDto.Order = itemDynamic.Order;
                dataDto.Type = WheelScreenMenuItemType.Combo;
                dataDto.ScreenMenuId = item.ScreenMenuId ?? item.ScreenMenuCategory.ScreenMenuId;
                dataDto.ParentId = item.ScreenCategoryId;
                dataDto.ProductComboId = item.ProductComboId;
                dataDto.AllowPreOrder = itemDynamic.AllowPreOrder;

                dataDto.ProductComboName = item.ProductCombo?.Name;
                dataDto.Prices = itemDynamic.Prices == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelScreenMenuItemlPricesForEditDto>>(itemDynamic.Prices
                        .ToString());

                dataDto.RecommendedItems = itemDynamic.RecommendedItems == null
                    ? null
                    : JsonConvert.DeserializeObject<List<WheelRecommendedForItemEditDto>>(itemDynamic.RecommendedItems
                        .ToString());

                dataDto.Tags = itemDynamic.Tags == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(itemDynamic.Tags
                        .ToString());
                dataDto.TimingSettings = itemDynamic.TimingSettings == null
                    ? null
                    : JsonConvert.DeserializeObject<List<string>>(itemDynamic.TimingSettings.ToString());
            }


            return dataDto;
        }

        public async Task<PagedResultDto<WheelScreenMenuItemForSection>> GetWheelScreenMenuItemForSelection(
            GetWheelScreenMenuItemForSelection input)
        {
         

            var items = _screenMenuItemRepository
                .GetAll()
                .Where(x => x.ScreenMenu.Type == ScreenMenuType.WheelScreenMenu ||
                            x.ScreenMenuCategory.ScreenMenu.Type == ScreenMenuType.WheelScreenMenu)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    x => x.Name.Contains(input.Filter)
                );


            if (!input.Sorting.IsNullOrEmpty()) items = items.OrderBy(input.Sorting);

            var itemsCount = await items.CountAsync();

            var itemsList = await items.PageBy(input).ToListAsync();

            var itemsDto = new List<WheelScreenMenuItemForSection>();

            foreach (var item in itemsList)
            {
                var itemDto = new WheelScreenMenuItemForSection
                {
                    Id = item.Id,
                    Name = item.Name
                };

                itemsDto.Add(itemDto);
            }

            return new PagedResultDto<WheelScreenMenuItemForSection>(itemsCount, itemsDto);
        }

        public async Task UpdatePublish(UpdatePublishInput input)
        {
            if (input.Type == WheelScreenMenuItemType.Item)
            {
                var item = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.Id == input.Id)
                    .Include(x => x.ScreenMenuCategory)
                    .FirstOrDefaultAsync();

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                itemDynamic.Published = input.Publish;

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);
                item.Published = input.Publish;

                await _screenMenuItemRepository.UpdateAsync(item);

            }
            else if (input.Type == WheelScreenMenuItemType.Section)
            {
                var category = await _screenMenuCategoryRepository.GetAsync(input.Id);

                dynamic categoryDynamic = JObject.Parse(category.Dynamic);

                categoryDynamic.Published = input.Publish;

                category.Dynamic = JsonConvert.SerializeObject(categoryDynamic);

                await _screenMenuCategoryRepository.UpdateAsync(category);

             
            }
            else if (input.Type == WheelScreenMenuItemType.Combo)
            {
                var item = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.Id == input.Id)
                    .Include(x => x.ScreenMenuCategory)
                    .FirstOrDefaultAsync();

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                itemDynamic.Published = input.Publish;

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);
                item.Published = input.Publish;

                await _screenMenuItemRepository.UpdateAsync(item);

            }
        }

        public async Task CreateWheelScreenMenuSectionFromMenuItem(long menuItemId)
        {
            var menuItem = await _menuItemRepository.GetAll().Include(x => x.Category).Include(x => x.Portions)
                .Where(x => x.Id == menuItemId).FirstOrDefaultAsync();
            var screenMenuItem = new CreateOrEditWheelScreenMenuItemInput();
            var screenMenuId = await GetMenuItemFromCategory(menuItem.Category.Name);
            screenMenuItem.Type =
                (WheelScreenMenuItemType)ConvertTypeMenuItemToTypeScreenMenuItem(menuItem.ProductType);
            screenMenuItem.Name = menuItem.Name;
            screenMenuItem.Description = menuItem.ItemDescription;
            screenMenuItem.Published = true;
            screenMenuItem.Image = string.Empty;
            screenMenuItem.Video = string.Empty;
            screenMenuItem.DisplayAs = WheelDisplayAs.List;
            screenMenuItem.HideGridTitles = true;
            screenMenuItem.MarkAsNew = true;
            screenMenuItem.MarkAsSignature = true;
            screenMenuItem.Note = string.Empty;
            screenMenuItem.DisplayNutritionFacts = true;
            screenMenuItem.Type = WheelScreenMenuItemType.Item;
            screenMenuItem.ScreenMenuId = screenMenuId;
            screenMenuItem.SeletedMenuItemIdPrice = menuItem.Id;
            screenMenuItem.Prices = menuItem.Portions.Select(x => new WheelScreenMenuItemlPricesDto
            {
                Multiplier = x.MultiPlier,
                Name = x.Name,
                Price = x.Price
            }).ToList();
            await CreateScreenMenuItem(screenMenuItem);
        }

        public async Task CreateWheelScreenMenuSectionFromScreenMenuCategory(int wheelScreenMenuId, int screenMenuId)
        {
            var screenMenu = await _screenMenuRepository.GetAll()
                .Include(x => x.Categories)
                .ThenInclude(x => x.MenuItems)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == screenMenuId);

            foreach (var screenMenuCategory in screenMenu.Categories)
            {
                var screenMenuItem = new CreateOrEditWheelScreenMenuItemInput();

                if (!string.IsNullOrEmpty(screenMenuCategory.Dynamic))
                {
                    dynamic screenMenuCategoryDynamic = JObject.Parse(screenMenuCategory.Dynamic);
                    screenMenuItem.Image = screenMenuCategoryDynamic.ImagePath;
                }
                else
                {
                    screenMenuItem.Image = string.Empty;
                }

                screenMenuItem.Type = WheelScreenMenuItemType.Section;
                screenMenuItem.Name = screenMenuCategory.Name;
                screenMenuItem.Published = true;
                screenMenuItem.Video = string.Empty;
                screenMenuItem.DisplayAs = WheelDisplayAs.List;
                screenMenuItem.HideGridTitles = true;
                screenMenuItem.MarkAsNew = true;
                screenMenuItem.MarkAsSignature = true;
                screenMenuItem.Note = string.Empty;
                screenMenuItem.DisplayNutritionFacts = true;
                screenMenuItem.ScreenMenuId = wheelScreenMenuId;

                var sectionId = await CreateScreenMenuItem(screenMenuItem);
                await CurrentUnitOfWork.SaveChangesAsync();

                // Get All MenuItem From Screen Category and Import
                var lstMenuItem = await GetMenuItemsFromCategoryId(screenMenuCategory.Id);

                foreach (var item in lstMenuItem)
                {
                    var menuItem = new CreateOrEditWheelScreenMenuItemInput();

                    menuItem.Type = (WheelScreenMenuItemType)ConvertTypeMenuItemToTypeScreenMenuItem(item.ProductType);

                    if (menuItem.Type == WheelScreenMenuItemType.Combo)
                        menuItem.ProductComboId = item.ProductComboId;
                    else
                        menuItem.SeletedMenuItemIdPrice = item.MenuItemId;
                    menuItem.Name = item.Name;
                    menuItem.SubCategory = item.SubCategory;

                    menuItem.Published = true;
                    menuItem.Image = item.ImagePath;
                    menuItem.Video = string.Empty;
                    menuItem.DisplayAs = WheelDisplayAs.List;
                    menuItem.HideGridTitles = true;
                    menuItem.MarkAsNew = true;
                    menuItem.MarkAsSignature = true;
                    menuItem.Note = string.Empty;
                    menuItem.DisplayNutritionFacts = true;
                    menuItem.ScreenMenuId = wheelScreenMenuId;
                    menuItem.ParentId = sectionId;
                    menuItem.Prices = item.Prices;

                    await CreateScreenMenuItem(menuItem);
                    await CurrentUnitOfWork.SaveChangesAsync();
                }
            }

           
        }

        public async Task<PagedResultDto<MenuItemAndPriceForSelection>> GetScreenMenuItemAndPriceForSelection(
            GetScreenMenuItemAndPriceForSelectionInput input)
        {
            var menuItems = _screenMenuItemRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    c => EF.Functions.Like(c.Name, input.Filter.ToILikeString()));

            if (!input.Sorting.IsNullOrEmpty())
                menuItems = menuItems.OrderBy(input.Sorting);

            var sortedMenuItems = await menuItems
                .PageBy(input)
                .ToListAsync();

            var menuItemListDtos = ObjectMapper.Map<List<MenuItemAndPriceForSelection>>(sortedMenuItems);
            return new PagedResultDto<MenuItemAndPriceForSelection>(
                await menuItems.CountAsync(),
                menuItemListDtos
            );
        }

        public async Task Reorder(List<WheelScreenMenuReOrderInput> inputs)
        {
            foreach (var input in inputs)
                if (input.Type != null && input.Type == WheelScreenMenuItemType.Section)
                {
                    var item = await _screenMenuCategoryRepository
                        .GetAll()
                        .Where(x => x.Id == input.Id)
                        .FirstOrDefaultAsync();
                    if (item != null)
                    {
                        dynamic itemDynamic = JObject.Parse(item.Dynamic);
                        itemDynamic.Order = input.Order;
                        item.Dynamic = JsonConvert.SerializeObject(itemDynamic);
                        await _screenMenuCategoryRepository.UpdateAsync(item);
                    }
                }
                else
                {
                    var item = await _screenMenuItemRepository
                        .GetAll()
                        .Where(x => x.Id == input.Id)
                        .FirstOrDefaultAsync();
                    if (item != null)
                    {
                        dynamic itemDynamic = JObject.Parse(item.Dynamic);

                        itemDynamic.Order = input.Order;
                        item.Order = input.Order;

                        item.Dynamic = JsonConvert.SerializeObject(itemDynamic);
                        await _screenMenuItemRepository.UpdateAsync(item);
                    }
                }
        }

        private async Task<int> CreateScreenMenuItem(CreateOrEditWheelScreenMenuItemInput input)
        {
            var screenMenuItemId = 0;
            if (input.Type == WheelScreenMenuItemType.Item)
            {
                var item = new ScreenMenuItem();

                dynamic itemDynamic = new ExpandoObject();

                item.Name = input.Name;
                item.SubCategory = input.SubCategory;
                item.Published = input.Published;

                itemDynamic.Description = input.Description;
                itemDynamic.Published = input.Published;
                itemDynamic.Image = input.Image;
                itemDynamic.Video = input.Video;
                itemDynamic.DisplayAs = input.DisplayAs;
                itemDynamic.NumberOfColumns = input.NumberOfColumns;
                itemDynamic.HideGridTitles = input.HideGridTitles;
                itemDynamic.MarkAsNew = input.MarkAsNew;
                itemDynamic.MarkAsSignature = input.MarkAsSignature;
                itemDynamic.Note = input.Note;
                itemDynamic.PreperationTime = input.PreperationTime;
                itemDynamic.IngredientWarnings = input.IngredientWarnings;
                itemDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                itemDynamic.AllowPreOrder = input.AllowPreOrder;
                itemDynamic.Order = 0;
                itemDynamic.Type = WheelScreenMenuItemType.Item;
                item.ScreenMenuId = input.ScreenMenuId;
                item.ScreenCategoryId = input.ParentId;
                item.MenuItemId = input.SeletedMenuItemIdPrice;
                itemDynamic.Prices = input.Prices;
                itemDynamic.RecommendedItems = input.RecommendedItems;
                itemDynamic.Tags = input.Tags;
                itemDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);

                var countItem = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId || x.ScreenCategoryId == input.ParentId)
                    .CountAsync();

                var countCategory = await _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId)
                    .CountAsync();

                itemDynamic.Order = countItem + countCategory;

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                screenMenuItemId = await _screenMenuItemRepository.InsertAndGetIdAsync(item);
            }
            else if (input.Type == WheelScreenMenuItemType.Section)
            {
                var category = new ScreenMenuCategory();

                dynamic categoryDynamic = new ExpandoObject();

                category.Name = input.Name;

                categoryDynamic.Description = input.Description;
                categoryDynamic.Published = input.Published;
                categoryDynamic.Image = input.Image;
                categoryDynamic.Video = input.Video;
                categoryDynamic.DisplayAs = input.DisplayAs;
                categoryDynamic.NumberOfColumns = input.NumberOfColumns;
                categoryDynamic.HideGridTitles = input.HideGridTitles;
                categoryDynamic.MarkAsNew = input.MarkAsNew;
                categoryDynamic.MarkAsSignature = input.MarkAsSignature;
                categoryDynamic.Note = input.Note;
                categoryDynamic.PreperationTime = input.PreperationTime;
                categoryDynamic.IngredientWarnings = input.IngredientWarnings;
                categoryDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                categoryDynamic.Order = 0;
                categoryDynamic.Type = WheelScreenMenuItemType.Section;
                category.ScreenMenuId = input.ScreenMenuId;
                category.ParentScreenMenuCategoryId = input.ParentId;
                categoryDynamic.Prices = input.Prices;
                categoryDynamic.RecommendedItems = input.RecommendedItems;
                categoryDynamic.Tags = input.Tags;
                categoryDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);

                var countItem = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId || x.ScreenCategoryId == input.ParentId)
                    .CountAsync();

                var countCategory = await _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId)
                    .CountAsync();

                categoryDynamic.Order = countItem + countCategory;

                category.Dynamic = JsonConvert.SerializeObject(categoryDynamic);

                screenMenuItemId = await _screenMenuCategoryRepository.InsertAndGetIdAsync(category);
            }
            else if (input.Type == WheelScreenMenuItemType.Combo)
            {
                var item = new ScreenMenuItem();

                dynamic itemDynamic = new ExpandoObject();

                item.Name = input.Name;
                item.SubCategory = input.SubCategory;
                item.Published = input.Published;

                itemDynamic.Description = input.Description;
                itemDynamic.Published = input.Published;
                itemDynamic.Image = input.Image;
                itemDynamic.Video = input.Video;
                itemDynamic.DisplayAs = input.DisplayAs;
                itemDynamic.NumberOfColumns = input.NumberOfColumns;
                itemDynamic.HideGridTitles = input.HideGridTitles;
                itemDynamic.MarkAsNew = input.MarkAsNew;
                itemDynamic.MarkAsSignature = input.MarkAsSignature;
                itemDynamic.Note = input.Note;
                itemDynamic.PreperationTime = input.PreperationTime;
                itemDynamic.IngredientWarnings = input.IngredientWarnings;
                itemDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                itemDynamic.Order = 0;
                itemDynamic.Type = WheelScreenMenuItemType.Combo;
                item.ScreenMenuId = input.ScreenMenuId;
                item.ScreenCategoryId = input.ParentId;
                item.ProductComboId = input.ProductComboId;
                itemDynamic.Prices = input.Prices;
                itemDynamic.RecommendedItems = input.RecommendedItems;
                itemDynamic.Tags = input.Tags;
                itemDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);
                itemDynamic.AllowPreOrder = input.AllowPreOrder;

                var countItem = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId || x.ScreenCategoryId == input.ParentId)
                    .CountAsync();

                var countCategory = await _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId)
                    .CountAsync();

                itemDynamic.Order = countItem + countCategory;

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                screenMenuItemId = await _screenMenuItemRepository.InsertAndGetIdAsync(item);
            }

            return screenMenuItemId;
        }

        private async Task UpdateScreenMenuItem(CreateOrEditWheelScreenMenuItemInput input)
        {
            if (input.Type == WheelScreenMenuItemType.Item)
            {
                var item = await _screenMenuItemRepository.GetAsync(input.Id);

                dynamic itemDynamic = JObject.Parse(item.Dynamic);

                item.Name = input.Name;
                item.SubCategory = input.SubCategory;

                itemDynamic.Description = input.Description;
                itemDynamic.Published = input.Published;
                itemDynamic.Image = input.Image;
                itemDynamic.Video = input.Video;
                itemDynamic.DisplayAs = input.DisplayAs;
                itemDynamic.NumberOfColumns = input.NumberOfColumns;
                itemDynamic.HideGridTitles = input.HideGridTitles;
                itemDynamic.MarkAsNew = input.MarkAsNew;
                itemDynamic.MarkAsSignature = input.MarkAsSignature;
                itemDynamic.Note = input.Note;
                itemDynamic.PreperationTime = input.PreperationTime;
                itemDynamic.IngredientWarnings = input.IngredientWarnings;
                itemDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                itemDynamic.AllowPreOrder = input.AllowPreOrder;
                itemDynamic.Type = WheelScreenMenuItemType.Item;
                item.ScreenMenuId = input.ScreenMenuId;
                item.ScreenCategoryId = input.ParentId;
                item.MenuItemId = input.SeletedMenuItemIdPrice;
                item.ProductComboId = null;

                itemDynamic.Prices = input.Prices?.Any() != true ? null : JsonConvert.SerializeObject(input.Prices);
                itemDynamic.RecommendedItems = input.RecommendedItems?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.RecommendedItems);

                itemDynamic.Tags = input.Tags?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.Tags);
                itemDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                await _screenMenuItemRepository.UpdateAsync(item);
            }
            else if (input.Type == WheelScreenMenuItemType.Section)
            {
                var category = await _screenMenuCategoryRepository.GetAsync(input.Id);

                dynamic categoryDynamic = JObject.Parse(category.Dynamic);

                category.Name = input.Name;
                categoryDynamic.Description = input.Description;
                categoryDynamic.Published = input.Published;
                categoryDynamic.Image = input.Image;
                categoryDynamic.Video = input.Video;
                categoryDynamic.DisplayAs = input.DisplayAs;
                categoryDynamic.NumberOfColumns = input.NumberOfColumns;
                categoryDynamic.HideGridTitles = input.HideGridTitles;
                categoryDynamic.MarkAsNew = input.MarkAsNew;
                categoryDynamic.MarkAsSignature = input.MarkAsSignature;
                categoryDynamic.Note = input.Note;
                categoryDynamic.PreperationTime = input.PreperationTime;
                categoryDynamic.IngredientWarnings = input.IngredientWarnings;
                categoryDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                categoryDynamic.Type = WheelScreenMenuItemType.Section;
                category.ScreenMenuId = input.ScreenMenuId;
                //category.ParentScreenMenuCategoryId = input.ParentId;
                categoryDynamic.Prices = input.Prices?.Any() != true ? null : JsonConvert.SerializeObject(input.Prices);
                categoryDynamic.RecommendedItems = input.RecommendedItems?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.RecommendedItems);

                categoryDynamic.Tags = input.Tags?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.Tags);
                categoryDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);

                category.Dynamic = JsonConvert.SerializeObject(categoryDynamic);

                await _screenMenuCategoryRepository.UpdateAsync(category);
            }
            else if (input.Type == WheelScreenMenuItemType.Combo)
            {
                var item = await _screenMenuItemRepository.GetAsync(input.Id);

                dynamic itemDynamic = new ExpandoObject();

                item.Name = input.Name;
                item.SubCategory = input.SubCategory;

                itemDynamic.Description = input.Description;
                itemDynamic.Published = input.Published;
                itemDynamic.Image = input.Image;
                itemDynamic.Video = input.Video;
                itemDynamic.DisplayAs = input.DisplayAs;
                itemDynamic.NumberOfColumns = input.NumberOfColumns;
                itemDynamic.HideGridTitles = input.HideGridTitles;
                itemDynamic.MarkAsNew = input.MarkAsNew;
                itemDynamic.MarkAsSignature = input.MarkAsSignature;
                itemDynamic.Note = input.Note;
                itemDynamic.PreperationTime = input.PreperationTime;
                itemDynamic.IngredientWarnings = input.IngredientWarnings;
                itemDynamic.DisplayNutritionFacts = input.DisplayNutritionFacts;
                itemDynamic.Order = 0;
                itemDynamic.Type = WheelScreenMenuItemType.Combo;
                item.ScreenMenuId = input.ScreenMenuId;
                item.ScreenCategoryId = input.ParentId;
                item.MenuItemId = null;
                item.ProductComboId = input.ProductComboId;
                itemDynamic.Prices = input.Prices;
                itemDynamic.RecommendedItems = input.RecommendedItems;
                itemDynamic.Tags = input.Tags;
                itemDynamic.TimingSettings = input.TimingSettings?.Any() != true
                    ? null
                    : JsonConvert.SerializeObject(input.TimingSettings);
                itemDynamic.AllowPreOrder = input.AllowPreOrder;

                var countItem = await _screenMenuItemRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId || x.ScreenCategoryId == input.ParentId)
                    .CountAsync();

                var countCategory = await _screenMenuCategoryRepository
                    .GetAll()
                    .Where(x => x.ScreenMenuId == input.ScreenMenuId)
                    .CountAsync();

                itemDynamic.Order = countItem + countCategory;

                item.Dynamic = JsonConvert.SerializeObject(itemDynamic);

                await _screenMenuItemRepository.UpdateAsync(item);
            }
        }

        private int ConvertTypeMenuItemToTypeScreenMenuItem(int input)
        {
            var type = new WheelScreenMenuItemType();
            if (input == 1)
                type = WheelScreenMenuItemType.Item;
            else
                type = WheelScreenMenuItemType.Combo;

            return (int)type;
        }

        private async Task<int> GetMenuItemFromCategory(string categoryName)
        {
            var result = 0;
            var menuItem = await _screenMenuRepository.FirstOrDefaultAsync(x => x.Name == categoryName);
            if (menuItem != null)
            {
                result = menuItem.Id;
            }
            else
            {
                var screenMenu = new CreateOrEditWheelScreenMenuInput
                {
                    Name = categoryName,
                    Published = true
                };
                result = await _wheelScreenMenusAppService.CreateOrEditWheelScreenMenu(screenMenu);
            }

            return result;
        }

        private async Task<List<GetMenuItemsOuputDto>> GetMenuItemsFromCategoryId(int categoryId)
        {
            var allItems = await _screenMenuItemRepository
                .GetAll()
                .Include(x => x.MenuItem)
                .ThenInclude(x => x.Portions)
                .Where(p => p.ScreenCategoryId == categoryId).AsNoTracking()
                .ToListAsync();

            var dtos = new List<GetMenuItemsOuputDto>();

            foreach (var menuItem in allItems)
            {
                var menuItemDto = ObjectMapper.Map<GetMenuItemsOuputDto>(menuItem);

                if (menuItem.Dynamic != null)
                {
                    dynamic screenMenuItemDynamic = JsonConvert.DeserializeObject(menuItem.Dynamic);
                    if (screenMenuItemDynamic != null)
                    {
                        menuItemDto.ButtonColor = screenMenuItemDynamic.ButtonColor;
                        menuItemDto.AutoSelect = screenMenuItemDynamic.AutoSelect ?? false;
                        menuItemDto.CategoryName = screenMenuItemDynamic.CategoryName;
                        menuItemDto.SubMenuTag = screenMenuItemDynamic.SubMenuTag;
                        menuItemDto.FontSize = screenMenuItemDynamic.FontSize ?? 0;
                        menuItemDto.ShowAliasName = screenMenuItemDynamic.ShowAliasName ?? false;
                        menuItemDto.MenuItemAliasName = screenMenuItemDynamic.MenuItemAliasName;
                        menuItemDto.ConfirmationType = screenMenuItemDynamic.ConfirmationType;
                        menuItemDto.WeightedItem = screenMenuItemDynamic.WeightedItem ?? 0;
                        menuItemDto.SortOrder = screenMenuItemDynamic.SortOrder ?? 0;
                        menuItemDto.MenuItemId = menuItem.MenuItemId;
                        menuItemDto.ImagePath = screenMenuItemDynamic.ImagePath;
                    }

                    menuItemDto.ProductType = menuItem.MenuItem.ProductType;
                    menuItemDto.ProductComboId = menuItem.ProductComboId;
                    menuItemDto.Prices = menuItem.MenuItem?.Portions?.Select(x => new WheelScreenMenuItemlPricesDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Price = x.Price
                    }).ToList();
                }

                dtos.Add(menuItemDto);
            }

            return await Task.FromResult(dtos);
        }
    }
}
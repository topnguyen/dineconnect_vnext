using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Net.Sms;
using DinePlan.DineConnect.Wheel.Dtos.WheelTable;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelTableAppService : DineConnectAppServiceBase, IWheelTableAppService
    {
        private readonly IRepository<WheelTable> _wheelTableRepository;
        private readonly IConfigurationRoot _configurationRoot;
        private readonly TwilioSmsSender _twilioSmsSender;
        private readonly IRepository<WheelSetup> _wheelSetupRepository;

        public WheelTableAppService(IRepository<WheelTable> wheelTableRepository,
            IAppConfigurationAccessor appConfigurationAccessor,
            TwilioSmsSender twilioSmsSender,
            IRepository<WheelSetup> wheelSetupRepository)
        {
            _wheelTableRepository = wheelTableRepository;
            _configurationRoot = appConfigurationAccessor.Configuration;
            _twilioSmsSender = twilioSmsSender;
            _wheelSetupRepository = wheelSetupRepository;
        }

        public async Task<ListResultDto<WheelTableViewModel>> GetAll(GetAllWheelTableInput input)
        {
            var wheelTable = _wheelTableRepository.GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    e => e.Name.Contains(input.Filter) ||
                         e.WheelLocation.Name.ToLower().Contains(input.LocationName.ToLower()) ||
                         e.WheelDepartment.Name.ToLower().Contains(input.DepartmentName.ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                .WhereIf(!string.IsNullOrEmpty(input.LocationName),
                    x => x.WheelLocation.Name.ToLower().Contains(input.LocationName.ToLower()))
                .WhereIf(!string.IsNullOrEmpty(input.DepartmentName),
                    x => x.WheelDepartment.Name.ToLower().Contains(input.DepartmentName.ToLower()))
                .WhereIf(input.WheelTableStatus.HasValue,
                    x => x.WheelTableStatus == input.WheelTableStatus.Value)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .AsQueryable()
                .PageBy(input);

            var totalCount = await wheelTable.CountAsync();

            var wheelTableViewModels = ObjectMapper.Map<List<WheelTableViewModel>>(await wheelTable.ToListAsync());

            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == AbpSession.TenantId);
            var clientRootAddress = wheelSetup.ClientUrl ?? _configurationRoot["App:ClientRootAddress"].TrimEnd('/'); 

            foreach (var wheelTableViewModel in wheelTableViewModels)
            {
                wheelTableViewModel.QrLink = $"{clientRootAddress}/verify/{wheelTableViewModel.WheelLocationId}/{wheelTableViewModel.WheelDepartmentId}/{wheelTableViewModel.Id}";
            }
            return new PagedResultDto<WheelTableViewModel>(
                totalCount,
                wheelTableViewModels
            );
        }

        
        public async Task<WheelTableBaseModel> GetWheelTableForEdit(int id)
        {
            var wheelPaymentMethod = await _wheelTableRepository.GetAsync(id);
            return ObjectMapper.Map<WheelTableBaseModel>(wheelPaymentMethod);
        }

        
        public async Task GenerateOtp(int id)
        {
            var wheelTable = await _wheelTableRepository.FirstOrDefaultAsync(id);
            wheelTable.OtpCode = GenerateOtpRandomNumber().ToString();
            await _wheelTableRepository.UpdateAsync(wheelTable);
        }

        public async Task CreateOrEdit(CreateOrEditWheelTableModel input)
        {
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditWheelTableModel input)
        {
            var wheelTable = ObjectMapper.Map<WheelTable>(input);

            if (AbpSession.TenantId != null) wheelTable.TenantId = (int)AbpSession.TenantId;
            wheelTable.OtpCode = GenerateOtpRandomNumber().ToString();
            await _wheelTableRepository.InsertAsync(wheelTable);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        
        protected virtual async Task Update(CreateOrEditWheelTableModel input)
        {
            var wheelLocation = await _wheelTableRepository.FirstOrDefaultAsync(input.Id);
            ObjectMapper.Map(input, wheelLocation);
            await _wheelTableRepository.UpdateAsync(wheelLocation);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _wheelTableRepository.DeleteAsync(input.Id);
        }

        
        public async Task<PagedResultDto<WheelTableLookupTableDto>> GetAllWheelTableForTableDropdown(int wheelLocationId, int wheelDepartmentId)
        {
            var data = await _wheelTableRepository.GetAll()
                .Where(x => x.WheelLocationId == wheelLocationId && x.WheelDepartmentId == wheelDepartmentId)
                .Select(location => new WheelTableLookupTableDto
                {
                    Id = location.Id,
                    Name = location.Name
                }).ToListAsync();

            return new PagedResultDto<WheelTableLookupTableDto>(
                data.Count,
                data
            );
        }

        
        public async Task<List<WheelTableViewModel>> GetAllWheelTables()
        {
            var wheelTableViewModels = await _wheelTableRepository
                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .Select(x => new WheelTableViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    WheelTableStatus = x.WheelTableStatus,
                    WheelLocationId = x.WheelLocationId,
                    WheelDepartmentId = x.WheelDepartmentId,
                    OtpCode = x.OtpCode,
                    Location = x.WheelLocation != null ? x.WheelLocation.Name : null,
                    Department = x.WheelDepartment != null ? x.WheelDepartment.Name : null
                }).ToListAsync();
            return wheelTableViewModels;
        }

        [AbpAllowAnonymous]
        public async Task<bool> SendOtp(int wheelLocationId, int wheelDepartmentId, int tableId, string phoneNumber)
        {
            var wheelTable = await _wheelTableRepository
                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .Where(x => x.WheelLocationId == wheelLocationId && x.WheelDepartmentId == wheelDepartmentId && x.Id == tableId)
                .FirstOrDefaultAsync();
            if (wheelTable == null)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(wheelTable.OtpCode))
            {
                wheelTable.OtpCode = GenerateOtpRandomNumber().ToString();
            }

            if (wheelTable.WheelDepartment != null && !string.IsNullOrEmpty(wheelTable.WheelDepartment.Dynamic))
            {
                if (string.IsNullOrEmpty(wheelTable.WheelDepartment.Dynamic) == false)
                {
                    dynamic itemDynamic = JObject.Parse(wheelTable.WheelDepartment.Dynamic);
                    bool sendOtp = itemDynamic.SendOtp ?? false;
                    if (sendOtp)
                    {
                        await _twilioSmsSender.SendAsync(phoneNumber, wheelTable.OtpCode);
                    }
                }

               
            }

            return true;
        }

        [AbpAllowAnonymous]
        public async Task<bool> VerifyOtp(int wheelLocationId, int wheelDepartmentId, int tableId, string phoneNumber, string verifyOtp)
        {
            var wheelTable = await _wheelTableRepository
                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .Where(x => x.WheelLocationId == wheelLocationId && x.WheelDepartmentId == wheelDepartmentId && x.Id == tableId)
                .FirstOrDefaultAsync();
           
            if (wheelTable != null)
            {
                if (wheelTable.OtpCode == verifyOtp)
                {
                    wheelTable.WheelTableStatus = WheelTableStatus.Booked;
                    wheelTable.BookedPhoneNumber = phoneNumber;
                    await _wheelTableRepository.UpdateAsync(wheelTable);
                    return true;
                }

                wheelTable.WheelTableStatus = WheelTableStatus.Vacant;
                wheelTable.BookedPhoneNumber = null;
                await _wheelTableRepository.UpdateAsync(wheelTable);
            }

            return false;
        }

        private int GenerateOtpRandomNumber()
        {
            var _min = 1000;
            var _max = 9999;
            var _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public async Task<DineUrlObject> TableUrl(DineUrlObjectInput entity)
        {
            var wheelTable = await _wheelTableRepository
                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .Where(x => x.WheelLocationId == entity.LocationId && x.Name.Equals(entity.TableName) && x.TenantId == entity.TenantId)
                .FirstOrDefaultAsync();

            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == entity.TenantId);
            var clientRootAddress = wheelSetup.ClientUrl ?? _configurationRoot["App:ClientRootAddress"].TrimEnd('/'); 
            DineUrlObject reObj = new DineUrlObject();
            if (wheelTable != null)
            {
                reObj.Pincode = wheelTable.OtpCode;
                reObj.Url= $"{clientRootAddress}/verify/{wheelTable.WheelLocationId}/{wheelTable.WheelDepartmentId}/{wheelTable.Id}";;
            }
            return reObj;

        }

        public async Task<DineUrlObject>  ChangeTableStatus(DineUrlObjectInput entity)
        {
            var wheelTable = await _wheelTableRepository
                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                .Where(x => x.WheelLocationId == entity.LocationId && x.Name.Equals(entity.TableName) && x.TenantId == entity.TenantId)
                .FirstOrDefaultAsync();
            DineUrlObject reObj = new DineUrlObject();
            if (wheelTable != null)
            {
                wheelTable.OtpCode = GenerateOtpRandomNumber().ToString();
                wheelTable.WheelTableStatus = WheelTableStatus.Vacant;
                await _wheelTableRepository.UpdateAsync(wheelTable);
                reObj.Pincode = wheelTable.OtpCode;
            }
           
            return reObj;
        }
    }
}
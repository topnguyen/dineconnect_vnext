﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.UiCustomization;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class WheelSetupsAppService : DineConnectAppServiceBase, IWheelSetupsAppService
    {
        private readonly IRepository<WheelSetup> _wheelSetupRepository;
        private readonly IWheelSetupsExcelExporter _wheelSetupsExcelExporter;
        private readonly IRepository<WheelLocation, int> _wheelLocationRepository;
        private readonly IUiThemeCustomizerFactory _uiThemeCustomizerFactory;
        private readonly EditionManager _editionManager;
        private readonly IRepository<WheelDynamicPages> _wheelDynamicPagesRepository;

        public WheelSetupsAppService(IRepository<WheelSetup> wheelSetupRepository,
            IWheelSetupsExcelExporter wheelSetupsExcelExporter,
            IRepository<WheelLocation, int> wheelLocationRepository,
            IUiThemeCustomizerFactory uiThemeCustomizerFactory,
            EditionManager editionManager,
            IRepository<WheelDynamicPages> wheelDynamicPagesRepository
            )
        {
            _wheelSetupRepository = wheelSetupRepository;
            _wheelSetupsExcelExporter = wheelSetupsExcelExporter;
            _wheelLocationRepository = wheelLocationRepository;
            _uiThemeCustomizerFactory = uiThemeCustomizerFactory;
            _editionManager = editionManager;
            _wheelDynamicPagesRepository = wheelDynamicPagesRepository;
        }

        [AbpAllowAnonymous]
        public async Task<GetWheelSetupForEditOutput> GetWheelSetupForEdit(GetWheelSetupsInputForEdit input)
        {
            var tenantId = AbpSession.TenantId ?? 1;
            var tenantCurrency = "";
            var wheelSetup = _wheelSetupRepository
                .GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.BaseUrl), x => x.ClientUrl.Contains(input.BaseUrl)).FirstOrDefault(x => x.Id == input.Id);

            if (input.Id == 0 || wheelSetup == null)
            {
                wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId);
            }

            if (wheelSetup != null)
            {
                await GetCollectionData(wheelSetup);
                var tenant = await TenantManager.GetByIdAsync(tenantId);
                if (tenant != null && tenant.EditionId.HasValue)
                {
                    tenantCurrency = await _editionManager.GetFeatureValueOrNullAsync(tenant.EditionId.Value,
                       "DinePlan.DineConnect.Connect.Currency");
                }
            }
            //var homePage
            if (wheelSetup != null && wheelSetup.HomePageId != null)
            {
                var tenantDatabase = await _wheelDynamicPagesRepository.GetAsync(wheelSetup.HomePageId.Value);
                wheelSetup.HomePage = tenantDatabase;
            }

            var output = new GetWheelSetupForEditOutput { WheelSetup = ObjectMapper.Map<CreateOrEditWheelSetupDto>(wheelSetup), Currency = tenantCurrency, OrderSetting = await GetOrderSetting()};

            if (wheelSetup != null && wheelSetup.HomePage != null)
            {
                output.WheelSetup.wheelDynamicPageEditDto = ObjectMapper.Map<WheelDynamicPageEditDto>(wheelSetup.HomePage);
            }

            return output;
        }

        [AbpAllowAnonymous]
        public async Task<ListResultDto<WheelLocationDto>> GetWheelSetupLocationsByTenant()
        {
            var tenantId = AbpSession.TenantId ?? 1;

            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId);
            if (wheelSetup != null)
            {
                await GetCollectionData(wheelSetup);
            }

            var output = new ListResultDto<WheelLocationDto> { Items = ObjectMapper.Map<List<WheelLocationDto>>(wheelSetup.WheelLocations.ToList()) };
            return output;
        }

        public async Task<PagedResultDto<GetWheelSetupForViewDto>> GetAll(GetAllWheelSetupsInput input)
        {
            var tenantId = AbpSession.TenantId ?? 1;
            var query = _wheelSetupRepository.GetAll()
                .Include(x => x.HomePage)
                //.Include(x=>x.WheelLocations)
                .Where(x => x.TenantId == tenantId);

            var pagedAndFilteredWheelSetups = query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var totalCount = await pagedAndFilteredWheelSetups.CountAsync();
            var listWheelsetups = await pagedAndFilteredWheelSetups.ToListAsync();
            var getWheelSetupForViewDtos = new List<GetWheelSetupForViewDto>();
            foreach (var item in listWheelsetups)
            {
                var wheelsetuptForViewDto = new GetWheelSetupForViewDto();
                wheelsetuptForViewDto.WheelSetup = ObjectMapper.Map<WheelSetupDto>(item);

                getWheelSetupForViewDtos.Add(wheelsetuptForViewDto);
            }
            var getWheelDepartmentForViewDtos = new List<GetWheelDepartmentForViewDto>();
            return new PagedResultDto<GetWheelSetupForViewDto>(
                totalCount,
               getWheelSetupForViewDtos
            );
        }

        public async Task CreateOrEdit(CreateOrEditWheelSetupDto input)
        {
            if (!string.IsNullOrWhiteSpace(input.ThemeName))
            {
                var theme = _uiThemeCustomizerFactory.GetWheelUiCustomizer(input.ThemeName);

                if (theme == null)
                    throw new UserFriendlyException("Theme could not be found.");
            }

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditWheelSetupDto input)
        {
            SetCollectionData(input);
            var wheelSetup = ObjectMapper.Map<WheelSetup>(input);

            if (AbpSession.TenantId != null)
            {
                wheelSetup.TenantId = (int)AbpSession.TenantId;
            }

            await _wheelSetupRepository.InsertAsync(wheelSetup);
        }

        
        protected virtual async Task Update(CreateOrEditWheelSetupDto input)
        {
            SetCollectionData(input);
            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, wheelSetup);
        }

        [AbpAllowAnonymous]
        public async Task<GetWheelSetupForEditOutput> GetWheelSetup()
        {
            var tenantId = AbpSession.GetTenantId();
            var tenantCurrency = "";
            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId);
            if (wheelSetup != null)
            {
                await GetCollectionData(wheelSetup);
                var tenant = await TenantManager.GetByIdAsync(tenantId);
                if (tenant != null && tenant.EditionId.HasValue)
                {
                    tenantCurrency = await _editionManager.GetFeatureValueOrNullAsync(tenant.EditionId.Value,
                       "DinePlan.DineConnect.Connect.Currency");
                }
            }

            var output = new GetWheelSetupForEditOutput
            {
                WheelSetup = ObjectMapper.Map<CreateOrEditWheelSetupDto>(wheelSetup),
                OrderSetting = await GetOrderSetting(),
                Currency = tenantCurrency
            };

            if (string.IsNullOrEmpty(output.WheelSetup.Currency))
            {
                output.WheelSetup.Currency = "S$";
            }
            if (string.IsNullOrEmpty(output.WheelSetup.Currency))
            {
                output.WheelSetup.Currency = output.Currency;
            }
            return output;
        }

        private async Task<OrderSettingDto> GetOrderSetting()
        {
            var result = new OrderSettingDto();
            var orderSetting =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.Accepted,
                    AbpSession.GetTenantId());
            var signUp =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.Signup,
                    AbpSession.GetTenantId());
            var autoCallDevelir =
                await SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.OrderSetting.AutoRequestDeliver,
                    AbpSession.GetTenantId());
            var width = await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Width,
                AbpSession.GetTenantId());
            var height =
                await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Height,
                    AbpSession.GetTenantId());
            var askTheMobileNumberFirst =
                await SettingManager.GetSettingValueForTenantAsync<bool>(
                    AppSettings.OrderSetting.AskTheMobileNumberFirst, AbpSession.GetTenantId());
            result.Accepted = orderSetting;
            result.Width = width;
            result.Height = height;
            result.AutoRequestDeliver = autoCallDevelir;
            result.AskTheMobileNumberFirst = askTheMobileNumberFirst;
            result.SignUp = signUp;
            return result;
        }

        private async Task GetCollectionData(WheelSetup wheelSetup)
        {
            wheelSetup.WheelLocations = new List<WheelLocation>();

            if (!string.IsNullOrEmpty(wheelSetup.WheelLocationIds))
            {
                string[] ids = wheelSetup.WheelLocationIds.Split(";", StringSplitOptions.RemoveEmptyEntries);
                wheelSetup.WheelLocations = await _wheelLocationRepository.GetAll()
                    .Where(x => ids.Contains(x.Id.ToString())).ToListAsync();
            }
        }

        private void SetCollectionData(CreateOrEditWheelSetupDto input)
        {
            if (input.WheelLocations != null)
            {
                input.WheelLocationIds = string.Join(";", input.WheelLocations.Select(x => x.Id));
            }
        }

        public async Task Delete(EntityDto input)
        {
            await _wheelSetupRepository.DeleteAsync(input.Id);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Cache;
using DinePlan.DineConnect.Wheel.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelProductSoldOutAppService : DineConnectAppServiceBase, IWheelProductSoldOutAppService
    {
        private readonly IRedisCacheService _redisCacheService;
        private readonly IRepository<WheelItemSoldOut> _itemSoldOutRepository;

        public WheelProductSoldOutAppService(IRepository<WheelItemSoldOut> itemSoldOutRepository, IRedisCacheService redisCacheService)
        {
            _itemSoldOutRepository = itemSoldOutRepository;
            _redisCacheService = redisCacheService;
        }

        public async Task<PagedResultDto<WheelProductSoldOutListDto>> GetAll(GetWheelScreenMenusInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var query = _itemSoldOutRepository.GetAll().WhereIf(input.LocationId > 0, x => x.LocationId == input.LocationId);

            var allProductSoldOuts = await query.Include(x => x.MenuItem).ToListAsync();

            var allWheelProductItemList = new List<WheelProductSoldOutListDto>();

            foreach (var item in allProductSoldOuts)
            {
                var wheelScreenMenuDto = new WheelProductSoldOutListDto
                {
                    Id = item.Id,
                    Name = item.MenuItem.Name,
                    Disable = item.Disable,
                };

                allWheelProductItemList.Add(wheelScreenMenuDto);
            }

            await _redisCacheService.SetAsync(AppConsts.GetAllWheelProductSoldOutKey + "_" + tenantId,
                allWheelProductItemList);

            allWheelProductItemList = allWheelProductItemList
                .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                .OrderBy(x => x.Id)
                .ToList();

            var wheelScreenMenuCount = allWheelProductItemList.Count();

            var response = new PagedResultDto<WheelProductSoldOutListDto>(wheelScreenMenuCount, allWheelProductItemList);

            return response;
        }

        
        public async Task<int> CreateOrEditWheelProductSoldOut(WheelProductSoldOutListDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            var newItemSoldOut = new WheelItemSoldOut
            {
                Id = input.Id,
                MenuItemId = input.MenuItemId,
                TenantId = tenantId,
                Disable = input.Disable,
                LocationId = input.LocationId
            };

            var newId = await _itemSoldOutRepository.InsertOrUpdateAndGetIdAsync(newItemSoldOut);

            return newId;
        }

        public async Task<WheelProductSoldOutListDto> GetWheelProductSoldOutForEdit(WheelProductSoldOutListDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            var data = await _itemSoldOutRepository
                .GetAll()
                .Include(x => x.MenuItem)
                .Include(x => x.Location)
                .FirstOrDefaultAsync(s =>
                s.Id == input.Id && s.TenantId == tenantId);

            var wheelScreenMenuDto = new WheelProductSoldOutListDto
            {
                Id = data.Id,
                Name = data.MenuItem.Name,
                MenuItemId = data.MenuItemId,
                LocationId = data.LocationId,
                Disable = data.Disable,
                LocationName = data.Location.Name
            };

            await _redisCacheService.SetAsync(AppConsts.GetWheelProductSoldOutForEditKey + "_" + input.Id,
                wheelScreenMenuDto);

            return wheelScreenMenuDto;
        }

        
        public async Task UpdateDisable(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            var entity = await _itemSoldOutRepository.SingleAsync(s => s.Id == input.Id && s.TenantId == tenantId);

            if (entity != null)
            {
                entity.Disable = !entity.Disable;
                await _itemSoldOutRepository.UpdateAsync(entity);
            }
        }

        
        public async Task Delete(EntityDto input)
        {
            var tenantId = AbpSession.GetTenantId();

            await _itemSoldOutRepository.DeleteAsync(s => s.Id == input.Id && s.TenantId == tenantId);

            await RemoveCache(input.Id);
        }

        private async Task RemoveCache(int id = 0)
        {
            var tenantId = AbpSession.GetTenantId();

            await _redisCacheService.RemoveAsync(AppConsts.GetAllWheelScreenMenuKey + "_" + tenantId,
                $"{AppConsts.GetWheelProductSoldOutForEditKey}_{id}");
        }

    }
}
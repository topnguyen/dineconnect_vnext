﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Syncs;
using DinePlan.DineConnect.Wheel.V2;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Wheel.Impl.Sync
{
    [AbpAuthorize]
    public class WheelOrderSyncAppService : DineConnectAppServiceBase
    {
        private readonly IRepository<ScreenMenuItem> _screenMenuItemRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<WheelDepartment> _wheelDepartmentRepository;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;
        private readonly IRepository<WheelTable> _wheelTableRepo;
        private readonly IRepository<WheelTicket> _wheelTicketRepo;

        public WheelOrderSyncAppService(
            IRepository<WheelDepartment> wheelDepartmentRepository,
            IRepository<ScreenMenuItem> screenMenuItemRepository,
            IRepository<WheelLocation> wheelLocationRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<WheelTicket> wheelTicketRepo,
            IRepository<WheelTable> wheelTableRepo)
        {
            _wheelTicketRepo = wheelTicketRepo;
            _wheelDepartmentRepository = wheelDepartmentRepository;
            _screenMenuItemRepository = screenMenuItemRepository;
            _wheelTableRepo = wheelTableRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _wheelLocationRepository = wheelLocationRepository;
        }

        public async Task<WheelTicketSyncOutputDto> PullNotSyncedTickets(ApiLocationInput input)
        {
            return await GetOutput(input);
        }

        public async Task<WheelTicketSyncOutputDto> PullNotNotifiedTickets(ApiLocationInput input)
        {
            return await GetOutput(input, 1);
        }

        public async Task<int> MarkTicketsAsSynced(WheelSyncInput input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var tickets = await _wheelTicketRepo.GetAll().Where(t => input.WheelTicketIds.Contains(t.Id))
                    .ToListAsync();

                foreach (var t in tickets) t.Synced = true;

                return tickets.Count();
            }
        }

        public async Task<int> MarkTicketsAsAccepted(WheelSyncInput input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var tickets = await _wheelTicketRepo.GetAll().Where(t => input.WheelTicketIds.Contains(t.Id))
                    .ToListAsync();

                foreach (var t in tickets)
                {
                    t.Synced = true;
                    t.OrderStatus = WheelOrderStatus.Accepted;
                }

                return tickets.Count();
            }
        }

        public async Task<int> MarkTicketsAsGatewayNotified(WheelSyncInput input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var tickets = await _wheelTicketRepo.GetAll().Where(t => input.WheelTicketIds.Contains(t.Id))
                    .ToListAsync();

                foreach (var t in tickets) t.GatewayNotified = true;

                return tickets.Count();
            }
        }

        public async Task<WheelTicketSyncOutputDto> ApiPreviewNonSyncTickets(ApiLocationInput input)
        {
            return await GetPreviewOutput(input);
        }

        public async Task<WheelTicketSyncOutputDto> ApiPreviewNonGatewayNotifiedTickets(ApiLocationInput input)
        {
            return await GetPreviewOutput(input, 1);
        }

        public async Task<WheelTicketSyncDto> ApiGetTicketById(ApiLocationInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var myLocations = await _wheelLocationRepository.GetAll()
                    .Where(a => a.LocationId == input.LocationId && a.TenantId == input.TenantId).ToListAsync();

                if (myLocations.Any())
                {
                    var myLastLocation = myLocations.LastOrDefault();
                    if (myLastLocation != null) input.LocationId = myLastLocation.Id;
                }
                else
                {
                    return null;
                }

                var myQueryable = _wheelTicketRepo
                    .GetAllIncluding(t => t.Payment)
                    .Where(t => t.TenantId == input.TenantId)
                    .WhereIf(input.LocationId > 0, x => x.WheelLocationId == input.LocationId);

                myQueryable = input.Id > 0
                    ? myQueryable.Where(a => a.Id == input.Id && !a.Synced)
                    : myQueryable.Where(a => !a.Synced);

                var sortedTicket = await GetWheelTicket(await myQueryable.LastOrDefaultAsync(), input);

                return sortedTicket;
            }
        }
        public async Task<WheelTicketSyncDto> ApiGetTicketByInvoice(ApiLocationInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var myLocations = await _wheelLocationRepository.GetAll()
                    .Where(a => a.LocationId == input.LocationId && a.TenantId == input.TenantId).ToListAsync();

                if (myLocations.Any())
                {
                    var myLastLocation = myLocations.LastOrDefault();
                    if (myLastLocation != null) input.LocationId = myLastLocation.Id;
                }
                else
                {
                    return null;
                }

                var myQueryable = _wheelTicketRepo
                    .GetAllIncluding(t => t.Payment)
                    .Where(t => t.TenantId == input.TenantId)
                    .WhereIf(input.LocationId > 0, x => x.WheelLocationId == input.LocationId);

                myQueryable = !string.IsNullOrEmpty(input.InvoiceNumber)
                    ? myQueryable.Where(a => a.Payment!=null && a.Payment.InvoiceNo.Equals(input.InvoiceNumber) && !a.Synced)
                    : myQueryable.Where(a => !a.Synced);

                var sortedTicket = await GetWheelTicket(await myQueryable.LastOrDefaultAsync(), input);

                return sortedTicket;
            }
        }

        private async Task<WheelTicketSyncDto> GetWheelTicket(WheelTicket sortedTicket, ApiLocationInput input)
        {

            var allTables = _wheelTableRepo.GetAll().Where(a => a.TenantId == input.TenantId);

            if (sortedTicket != null)
            {
                var wheelBillingInfo =
                    JsonConvert.DeserializeObject<WheelBillingInfo>(sortedTicket.Payment.ExtraInformation);

                var ticket = new WheelTicketSyncDto
                {
                    Id = sortedTicket.Id,
                    InvoiceNumber = sortedTicket.Payment.InvoiceNo,
                    WheelBillingInfo = wheelBillingInfo,
                    WheelOrderType = sortedTicket.WheelOrderType,
                    OrderStatus = sortedTicket.OrderStatus,
                    DateDelivery = sortedTicket.DateDelivery,
                    DeliveringTime = sortedTicket.DeliveringTime,
                    DeliveryTime = sortedTicket.DeliveryTime,
                    OrderCreatedTime = sortedTicket.OrderCreatedTime,
                    PaidAmount = sortedTicket.PaidAmount,
                    PaymentDate = sortedTicket.Payment?.PaymentDate,
                    PaymentOption = sortedTicket.Payment.PaymentOption,
                    TableId = sortedTicket.TableId
                };

                ticket.TableName = sortedTicket.TableId.HasValue
                    ? allTables.FirstOrDefault(t => t.Id == ticket.TableId.Value)?.Name
                    : "";

                if (wheelBillingInfo.WheelDepartmentId.HasValue)
                {
                    var deptId = Convert.ToInt32(wheelBillingInfo.WheelDepartmentId.Value);
                    var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(deptId);
                    ticket.DepartmentName = department.Name;
                }


                if (!string.IsNullOrEmpty(sortedTicket.MenuItemsDynamic))
                {
                    var myOrders =
                        JsonConvert.DeserializeObject<List<WheelCartItemDto>>(sortedTicket.MenuItemsDynamic);

                    if (myOrders != null && myOrders.Any()) ticket.Orders = myOrders;
                }

                if (!string.IsNullOrEmpty(sortedTicket.AmountCalculations))
                {
                    var myOrders =
                        JsonConvert.DeserializeObject<CartTotalDto>(sortedTicket.AmountCalculations);

                    if (myOrders != null) ticket.CartTotal = myOrders;
                }

                var shippingOrder = sortedTicket.WheelTicketShippingOrders.OrderByDescending(t => t.Id)
                    .FirstOrDefault();
                if (shippingOrder != null)
                {
                    ticket.ActiveShippingDeliveryOrder = shippingOrder?.ShippingOrderId;
                    ticket.ActiveShippingDeliveryOrderStatus = shippingOrder?.ShippingOrderStatus;
                    ticket.ShippingProvider = shippingOrder.ShippingProviderName.ToString();
                }

                return ticket;
            }

            return null;
        }


        private async Task<WheelTicketSyncOutputDto> GetOutput(ApiLocationInput input, int statusInput = 0)
        {
            var output = new WheelTicketSyncOutputDto();

            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var myLocations = await _wheelLocationRepository.GetAll()
                    .Where(a => a.LocationId == input.LocationId && a.TenantId == input.TenantId).ToListAsync();

                if (myLocations.Any())
                {
                    var myLastLocation = myLocations.LastOrDefault();
                    if (myLastLocation != null) input.LocationId = myLastLocation.Id;
                }
                else
                {
                    return null;
                }

                var myQueryable = _wheelTicketRepo
                    .GetAllIncluding(t => t.Payment)
                    .Where(t => t.TenantId == input.TenantId && t.OrderStatus != WheelOrderStatus.Rejected)
                    .WhereIf(input.LocationId > 0, x => x.WheelLocationId == input.LocationId);

                myQueryable = statusInput == 1
                    ? myQueryable.Where(a => !a.GatewayNotified)
                    : myQueryable.Where(a => !a.Synced);

                var allTickets = await myQueryable.ToListAsync();

                var allTables = _wheelTableRepo.GetAll().Where(a => a.TenantId == input.TenantId);

                foreach (var sortedTicket in allTickets)
                {
                    var wheelBillingInfo =
                        JsonConvert.DeserializeObject<WheelBillingInfo>(sortedTicket.Payment.ExtraInformation);

                    var ticket = new WheelTicketSyncDto
                    {
                        Id = sortedTicket.Id,
                        InvoiceNumber = sortedTicket.Payment.InvoiceNo,
                        WheelBillingInfo = wheelBillingInfo,
                        WheelOrderType = sortedTicket.WheelOrderType,
                        OrderStatus = sortedTicket.OrderStatus,
                        DateDelivery = sortedTicket.DateDelivery,
                        DeliveringTime = sortedTicket.DeliveringTime,
                        DeliveryTime = sortedTicket.DeliveryTime,
                        OrderCreatedTime = sortedTicket.OrderCreatedTime,
                        PaidAmount = sortedTicket.PaidAmount,
                        PaymentDate = sortedTicket.Payment?.PaymentDate,
                        PaymentOption = sortedTicket.Payment.PaymentOption,
                        TableId = sortedTicket.TableId
                    };

                    ticket.TableName = sortedTicket.TableId.HasValue
                        ? allTables.FirstOrDefault(t => t.Id == ticket.TableId.Value)?.Name
                        : "";

                    if (wheelBillingInfo.WheelDepartmentId.HasValue)
                    {
                        var deptId = Convert.ToInt32(wheelBillingInfo.WheelDepartmentId.Value);
                        var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(deptId);
                        ticket.DepartmentName = department.Name;
                    }


                    if (!string.IsNullOrEmpty(sortedTicket.MenuItemsDynamic))
                    {
                        var myOrders =
                            JsonConvert.DeserializeObject<List<WheelCartItemDto>>(sortedTicket.MenuItemsDynamic);

                        if (myOrders != null && myOrders.Any()) ticket.Orders = myOrders;
                    }

                    if (!string.IsNullOrEmpty(sortedTicket.AmountCalculations))
                    {
                        var myOrders =
                            JsonConvert.DeserializeObject<CartTotalDto>(sortedTicket.AmountCalculations);

                        if (myOrders != null) ticket.CartTotal = myOrders;
                    }

                    var shippingOrder = sortedTicket.WheelTicketShippingOrders.OrderByDescending(t => t.Id)
                        .FirstOrDefault();
                    if (shippingOrder != null)
                    {
                        ticket.ActiveShippingDeliveryOrder = shippingOrder?.ShippingOrderId;
                        ticket.ActiveShippingDeliveryOrderStatus = shippingOrder?.ShippingOrderStatus;
                        ticket.ShippingProvider = shippingOrder.ShippingProviderName.ToString();
                    }

                    output.Tickets.Add(ticket);
                }
            }

            return output;
        }

        private async Task<WheelTicketSyncOutputDto> GetPreviewOutput(ApiLocationInput input, int statusInput = 0)
        {
            var output = new WheelTicketSyncOutputDto();

            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var myLocations = await _wheelLocationRepository.GetAll()
                    .Where(a => a.LocationId == input.LocationId && a.TenantId == input.TenantId).ToListAsync();

                if (myLocations.Any())
                {
                    var myLastLocation = myLocations.LastOrDefault();
                    if (myLastLocation != null) input.LocationId = myLastLocation.Id;
                }
                else
                {
                    return null;
                }

                var myQueryable = _wheelTicketRepo
                    .GetAllIncluding(t => t.Payment)
                    .Where(t => t.TenantId == input.TenantId)
                    .WhereIf(input.LocationId > 0, x => x.WheelLocationId == input.LocationId);

                myQueryable = statusInput == 1
                    ? myQueryable.Where(a => !a.GatewayNotified)
                    : myQueryable.Where(a => !a.Synced);

                myQueryable = myQueryable.Skip(input.Skip).Take(input.PageSize);
                var allTickets = myQueryable.OrderByDescending(a => a.CreationTime).ToList();

                var allTables = _wheelTableRepo.GetAll().Where(a => a.TenantId == input.TenantId);

                foreach (var sortedTicket in allTickets)
                {
                    var wheelBillingInfo =
                        JsonConvert.DeserializeObject<WheelBillingInfo>(sortedTicket.Payment.ExtraInformation);

                    var ticket = new WheelTicketSyncDto
                    {
                        Id = sortedTicket.Id,
                        InvoiceNumber = sortedTicket.Payment.InvoiceNo,
                        WheelBillingInfo = wheelBillingInfo,
                        WheelOrderType = sortedTicket.WheelOrderType,
                        OrderStatus = sortedTicket.OrderStatus,
                        DateDelivery = sortedTicket.DateDelivery,
                        DeliveringTime = sortedTicket.DeliveringTime,
                        DeliveryTime = sortedTicket.DeliveryTime,
                        OrderCreatedTime = sortedTicket.OrderCreatedTime,
                        PaidAmount = sortedTicket.PaidAmount,
                        PaymentDate = sortedTicket.Payment?.PaymentDate,
                        PaymentOption = sortedTicket.Payment.PaymentOption,
                        TableId = sortedTicket.TableId
                    };

                    ticket.TableName = sortedTicket.TableId.HasValue
                        ? allTables.FirstOrDefault(t => t.Id == ticket.TableId.Value)?.Name
                        : "";

                    if (wheelBillingInfo.WheelDepartmentId.HasValue)
                    {
                        var deptId = Convert.ToInt32(wheelBillingInfo.WheelDepartmentId.Value);
                        var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(deptId);
                        ticket.DepartmentName = department.Name;
                    }


                    if (!string.IsNullOrEmpty(sortedTicket.MenuItemsDynamic))
                    {
                        var myOrders =
                            JsonConvert.DeserializeObject<List<WheelCartItemDto>>(sortedTicket.MenuItemsDynamic);

                        if (myOrders != null && myOrders.Any()) ticket.Orders = myOrders;
                    }

                    if (!string.IsNullOrEmpty(sortedTicket.AmountCalculations))
                    {
                        var myOrders =
                            JsonConvert.DeserializeObject<CartTotalDto>(sortedTicket.AmountCalculations);

                        if (myOrders != null) ticket.CartTotal = myOrders;
                    }

                    var shippingOrder = sortedTicket.WheelTicketShippingOrders.OrderByDescending(t => t.Id)
                        .FirstOrDefault();
                    if (shippingOrder != null)
                    {
                        ticket.ActiveShippingDeliveryOrder = shippingOrder?.ShippingOrderId;
                        ticket.ActiveShippingDeliveryOrderStatus = shippingOrder?.ShippingOrderStatus;
                        ticket.ShippingProvider = shippingOrder.ShippingProviderName.ToString();
                    }

                    output.Tickets.Add(ticket);
                }
            }

            return output;
        }
    }
}
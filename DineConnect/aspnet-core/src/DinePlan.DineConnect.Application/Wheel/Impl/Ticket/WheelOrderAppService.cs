using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.Timing;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.MultiTenancy.OneMap;
using DinePlan.DineConnect.PaymentProcessor;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;
using DinePlan.DineConnect.Settings;
using DinePlan.DineConnect.Shipping;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Timing;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.EmailNotification;
using DinePlan.DineConnect.Wheel.EmailNotification.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using DinePlan.DineConnect.Wheel.V2;
using Elect.Core.LinqUtils;
using Elect.Location.Coordinate.DistanceUtils;
using Elect.Location.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic.Core;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Impl.Ticket
{
    [AbpAllowAnonymous]
    public class WheelOrderAppService : DineConnectAppServiceBase, IWheelOrderAppService
    {
        private readonly CustomerManager _customerManager;

        private readonly IWheelEmailNotificationAppService _emailNotificationJobService;
        private readonly IOmisePaymentProcessor _omisePaymentProcessor;
        private readonly OneMapGatewayManager _oneMapGatewayManager;
        private readonly IShippingService _shippingService;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<TiffinPromotionCode> _tiffinPromotionCodeRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly WheelAppService _wheelAppService;
        private readonly IWheelCheckoutAppService _wheelCartAppService;
        private readonly IRepository<WheelDeliveryDuration> _wheelDeliveryDurationRepository;
        private readonly IRepository<WheelDepartment> _wheelDepartmentRepository;
        private readonly IRepository<WheelDepartmentTimeSlot> _wheelDepartmentTimeSlotRepository;
        private readonly IWheelInvoiceNumberGenerator _wheelInvoiceNumberGenerator;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;
        private readonly IRepository<WheelOpeningHour> _wheelOpeningHourRepository;
        private readonly IWheelOrderTrackingAppService _wheelOrderTrackingService;
        private readonly IRepository<WheelPayment> _wheelPaymentRepository;
        private readonly IRepository<WheelPaymentRequest> _wheelPaymentRequestRepo;
        private readonly IRepository<WheelTable> _wheelTableRepo;
        private readonly IRepository<WheelTicket> _wheelTicketRepo;
        private readonly IRepository<WheelDeliveryZone> _zoneRepository;

        public WheelOrderAppService(
            IRepository<WheelPaymentRequest> wheelPaymentRequestRepo,
            IRepository<WheelTicket> wheelTicketRepo,
            IRepository<WheelPayment> wheelPaymentRepository,
            IWheelEmailNotificationAppService emailNotificationJobService,
            IOmisePaymentProcessor omisePaymentProcessor,
            IWheelInvoiceNumberGenerator wheelInvoiceNumberGenerator,
            IRepository<WheelOpeningHour> wheelOpeningHourRepository,
            IRepository<WheelDepartment> wheelDepartmentRepository,
            ITenantSettingsAppService tenantSettingsAppService,
            ITimeZoneService timeZoneService,
            IRepository<WheelDepartmentTimeSlot> wheelDepartmentTimeSlotRepository,
            IWheelOrderTrackingAppService wheelOrderTrackingService,
            OneMapGatewayManager oneMapGatewayManager,
            WheelAppService wheelAppService,
            IRepository<WheelLocation> wheelLocationRepository,
            IRepository<WheelDeliveryDuration> wheelDeliveryDurationRepository
            , IRepository<WheelTax> wheelTaxRepository
            , IRepository<TiffinPromotionCode> tiffinPromotionCodeRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<WheelTable> wheelTableRepo
            , IRepository<WheelDeliveryZone> zoneRepository,
            IWheelCheckoutAppService wheelCartAppService, CustomerManager customerManager,
            IShippingService shippingService)
        {
            _wheelPaymentRepository = wheelPaymentRepository;
            _emailNotificationJobService = emailNotificationJobService;
            _wheelPaymentRequestRepo = wheelPaymentRequestRepo;
            _wheelTicketRepo = wheelTicketRepo;
            _omisePaymentProcessor = omisePaymentProcessor;
            _wheelInvoiceNumberGenerator = wheelInvoiceNumberGenerator;
            _wheelOpeningHourRepository = wheelOpeningHourRepository;
            _wheelDepartmentRepository = wheelDepartmentRepository;
            _timeZoneService = timeZoneService;
            _wheelDepartmentTimeSlotRepository = wheelDepartmentTimeSlotRepository;
            _wheelOrderTrackingService = wheelOrderTrackingService;
            _tenantSettingsAppService = tenantSettingsAppService;
            _oneMapGatewayManager = oneMapGatewayManager;
            _wheelAppService = wheelAppService;
            _wheelLocationRepository = wheelLocationRepository;
            _wheelDeliveryDurationRepository = wheelDeliveryDurationRepository;
            _tiffinPromotionCodeRepository = tiffinPromotionCodeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _wheelTableRepo = wheelTableRepo;
            _zoneRepository = zoneRepository;
            _wheelCartAppService = wheelCartAppService;
            _customerManager = customerManager;
            _shippingService = shippingService;
        }

        [AbpAllowAnonymous]
        public async Task<bool> IsOpeningHour(long wheelDepartmentId)
        {
            var result = false;
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == wheelDepartmentId);
            if (string.IsNullOrEmpty(department.WheelOpeningHourIds)) return false;
            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();

            if (openingHours.Count == 0) return false;

            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);

            var currentWeekDay = now.DayOfWeek;

            foreach (var openingHour in openingHours)
            {
                TimeSpan openAt;
                TimeSpan closeAt;

                if (TimeSpan.TryParse(openingHour.From, out openAt) && TimeSpan.TryParse(openingHour.To, out closeAt))
                {
                    var todayOpenTime = new DateTime(now.Year, now.Month, now.Day, openAt.Hours, openAt.Minutes, 0);
                    var todayCloseTime = new DateTime(now.Year, now.Month, now.Day, closeAt.Hours, closeAt.Minutes, 0);

                    var openDays = new List<DayOfWeek>();
                    if ((openingHour.OpenDays & (1 << 0)) != 0) openDays.Add(DayOfWeek.Monday);

                    if ((openingHour.OpenDays & (1 << 1)) != 0) openDays.Add(DayOfWeek.Tuesday);

                    if ((openingHour.OpenDays & (1 << 2)) != 0) openDays.Add(DayOfWeek.Wednesday);

                    if ((openingHour.OpenDays & (1 << 3)) != 0) openDays.Add(DayOfWeek.Thursday);

                    if ((openingHour.OpenDays & (1 << 4)) != 0) openDays.Add(DayOfWeek.Friday);

                    if ((openingHour.OpenDays & (1 << 5)) != 0) openDays.Add(DayOfWeek.Saturday);

                    if ((openingHour.OpenDays & (1 << 6)) != 0) openDays.Add(DayOfWeek.Sunday);

                    if (openDays.Contains(currentWeekDay))
                        if (now >= todayOpenTime && now <= todayCloseTime)
                            return true;
                }
            }

            return result;
        }

        public async Task<DateTime> GetDefaultDateFuture(long wheelDepartmentId)
        {
            var timeZone = await GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
            var now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timezoneInfo);
            var selectedDate = now.AddDays(1);

            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == wheelDepartmentId);
            if (string.IsNullOrEmpty(department.WheelOpeningHourIds)) return selectedDate;
            var openingHourIds = department.WheelOpeningHourIds.Split(';');
            var openingHours = await _wheelOpeningHourRepository.GetAll()
                .Where(x => openingHourIds.Any(o => o == x.Id.ToString())).ToListAsync();

            if (openingHours.Count == 0) return selectedDate;

            var isValid = false;
            var currentWeekDay = selectedDate.DayOfWeek;
            DateTime? dateDefault = null;
            var step = 1;
            while (step > 0)
            {
                foreach (var openingHour in openingHours)
                {
                    TimeSpan openAt;
                    TimeSpan closeAt;

                    if (TimeSpan.TryParse(openingHour.From, out openAt) &&
                        TimeSpan.TryParse(openingHour.To, out closeAt))
                    {
                        var openDays = new List<DayOfWeek>();
                        if ((openingHour.OpenDays & (1 << 0)) != 0) openDays.Add(DayOfWeek.Monday);

                        if ((openingHour.OpenDays & (1 << 1)) != 0) openDays.Add(DayOfWeek.Tuesday);

                        if ((openingHour.OpenDays & (1 << 2)) != 0) openDays.Add(DayOfWeek.Wednesday);

                        if ((openingHour.OpenDays & (1 << 3)) != 0) openDays.Add(DayOfWeek.Thursday);

                        if ((openingHour.OpenDays & (1 << 4)) != 0) openDays.Add(DayOfWeek.Friday);

                        if ((openingHour.OpenDays & (1 << 5)) != 0) openDays.Add(DayOfWeek.Saturday);

                        if ((openingHour.OpenDays & (1 << 6)) != 0) openDays.Add(DayOfWeek.Sunday);

                        if (openDays.Contains(currentWeekDay))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }

                if (isValid)
                {
                    dateDefault = selectedDate;
                    step = 0;
                }
                else
                {
                    selectedDate = selectedDate.AddDays(1);
                    currentWeekDay = selectedDate.DayOfWeek;
                    //step++;
                }
            }

            return dateDefault == null ? Clock.Now.AddDays(1) : dateDefault.Value;
        }


        public async Task<PaymentOutputDto> CreatePaymentForOrder(CreateWheelOrderPaymentInputDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(AbpSession.TenantId ?? 1))
            {
                var output = new PaymentOutputDto();
                var wheelServer = new WheelPaymentRequest
                {
                    TenantId = AbpSession.TenantId ?? 1,
                    PaymentOption = input.PaymentOption,
                    PaidAmount = input.PaidAmount ?? 0,
                    PaymentDate = Clock.Now,
                    Request = JsonConvert.SerializeObject(input)
                };

                var repoId = await _wheelPaymentRequestRepo.InsertAndGetIdAsync(wheelServer);

                if (input.CallbackUrl.EndsWith("/"))
                    input.CallbackUrl += repoId;
                else
                    input.CallbackUrl += "/" + repoId;


                input.PaymentOption = string.IsNullOrWhiteSpace(input.PaymentOption)
                    ? ""
                    : input.PaymentOption.ToLower().Trim();

                #region Omise

                if (input.PaymentOption == "omise" || input.PaymentOption == "paynow")
                {
                    OmisePaymentResponseDto omiseChargeResult;

                    if (input.PaymentOption == "omise")
                        omiseChargeResult = await _omisePaymentProcessor.ProcessPayment(new OmisePaymentRequestDto
                        {
                            OmiseToken = input.OmiseToken,
                            Amount = Convert.ToInt64(input.PaidAmount * 100),
                            Currency = DineConnectConsts.Currency,
                            ReturnUrl = input.CallbackUrl
                        });
                    else
                        omiseChargeResult = await _omisePaymentProcessor.ProcessPaymentNow(new OmisePaymentRequestDto
                        {
                            OmiseToken = input.OmiseToken,
                            Amount = Convert.ToInt64(input.PaidAmount * 100),
                            Currency = DineConnectConsts.Currency,
                            ReturnUrl = input.CallbackUrl
                        });

                    if (omiseChargeResult != null)
                    {
                        output.Message = omiseChargeResult.Message;
                        output.PurchasedSuccess = omiseChargeResult.IsSuccess;
                        wheelServer.PaymentReference = output.OmiseId = omiseChargeResult.PaymentId;
                        output.AuthenticateUri = omiseChargeResult.AuthorizeUri;
                    }
                }

                #endregion

                if (output.PurchasedSuccess || string.IsNullOrWhiteSpace(input.PaymentOption) ||
                    input.PaymentOption.ToLower() == "cash")
                {
                    var createResult = await CreateOrder(input);
                    output.InvoiceNo = createResult.InvoiceNo;
                }

                return output;
            }
        }

        public async Task<PaymentOutputDto> ValidatePayment(EntityDto<int> input)
        {
            var output = new PaymentOutputDto();
            if (input.Id > 0)
            {
                var paymentRequest = await _wheelPaymentRequestRepo.GetAsync(input.Id);
                if (paymentRequest != null)
                {
                    var changeResult =
                        await _omisePaymentProcessor.GetStatus(paymentRequest.PaymentReference);

                    output.Message = changeResult.Message;
                    output.AuthenticateUri = changeResult.AuthorizeUri;
                    output.OmiseId = changeResult.PaymentId;
                    output.PurchasedSuccess = changeResult.IsSuccess;

                    if (changeResult.IsSuccess)
                    {
                        var myReturn = await CreateOrder(
                            JsonConvert.DeserializeObject<CreateWheelOrderPaymentInputDto>(paymentRequest.Request));
                        output.PurchasedSuccess = true;
                        output.PaymentId = myReturn.PaymentId;
                        output.InvoiceNo = myReturn.InvoiceNo;
                        await _wheelPaymentRequestRepo.DeleteAsync(input.Id);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(output.Message)) output.Message = "The Payment is failed.";
                    }
                }
            }
            else
            {
                output.PurchasedSuccess = false;
                output.Message = "No Payment Request";
            }

            return output;
        }

        public async Task<bool> CheckSlotOrder(WheelOrderTrackingDto input)
        {
            var orderTracking =
                await _wheelOrderTrackingService.GetWheelOrderTrackingByWheelDepartmentTimeSlotId(input);

            var slotOrder = _wheelDepartmentTimeSlotRepository.FirstOrDefault(input.WheelDepartmentTimeSlotId.Value)
                .NumberOfOrder;

            if (slotOrder == 0 || orderTracking != null && orderTracking.CurrentCount >= slotOrder) return false;

            return true;
        }

        [AbpAllowAnonymous]
        public async Task<CheckDeliveryZoneAndFeeOuput> CheckDeliveryZone(CheckDeliveryZoneAndFeeInput input)
        {
            var result = new CheckDeliveryZoneAndFeeOuput();

            var location = await _wheelLocationRepository.FirstOrDefaultAsync(input.LocationId);

            if (location == null) return null;

            result.AllowOutOfDeliveryZones = location.OutsideDeliveryZone;

            if (string.IsNullOrEmpty(location.WheelDeliveryZoneIds))
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            var addressList = await _oneMapGatewayManager.GetAddressByPostCode(input.PostalCode);

            var address = addressList.FirstOrDefault();

            if (address == null)
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            var longitude = Convert.ToDouble(address.Longitude);
            var latitude = Convert.ToDouble(address.Latitude);

            var output = await _wheelAppService.GetDeliveryZonesContainedPoint(longitude, latitude);
            var deliveryZoneIds = location.WheelDeliveryZoneIds.Split(";", StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            var locationDeliveryZones = output.Where(zone => deliveryZoneIds.Contains(zone.Id.ToString())).ToList();

            if (locationDeliveryZones.Any())
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = locationDeliveryZones.Min(x => x.DeliveryFee);
            }
            else
            {
                result.OutOfAllDeliveryZones = false;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            return result;
        }

        [AbpAllowAnonymous]
        public async Task<decimal> CalculateDeliveryTime(int locationId, double customerAddressLng,
            double customerAddressLat)
        {
            var result = 0M;
            var location = await _wheelLocationRepository.FirstOrDefaultAsync(x => x.Id == locationId);
            if (string.IsNullOrWhiteSpace(location.PickupAddressLong) || string.IsNullOrWhiteSpace(
                                                                          location.PickupAddressLat)
                                                                      || !double.TryParse(
                                                                          location.PickupAddressLong.Replace(".", ","),
                                                                          out var pickupAddressLong) ||
                                                                      !double.TryParse(
                                                                          location.PickupAddressLat.Replace(".", ","),
                                                                          out var pickupAddressLat)
               )
                return 0M;
            var storeCoordinate = new CoordinateModel(pickupAddressLong, pickupAddressLat);
            var customerCoordinate = new CoordinateModel(customerAddressLng, customerAddressLat);
            var distanceToCenterPoint = storeCoordinate.DistanceTo(customerCoordinate, UnitOfLengthModel.Kilometer);
            var deliveryDurations = await _wheelDeliveryDurationRepository.GetAll().ToListAsync();
            if (deliveryDurations.Count == 0) return 0M;

            var nearest = deliveryDurations.OrderBy(x => Math.Abs(x.DeliveryDistance - (decimal)distanceToCenterPoint))
                .First();
            result = nearest.DeliveryTime;
            return result;
        }


        public async Task<int> GetPageGoToBack(GetPageInput input)
        {
            var wheelDepartment = await GetWheelDepartmentById(input.WheelDepatermentId);
            var value = 0;
            var locations = _wheelLocationRepository.GetAll()
                .Where(x => x.WheelDepartmentIds.Contains(wheelDepartment.Id.ToString())).ToList();
            var orderSetting = await _tenantSettingsAppService.GetOrderSetting();

            switch (input.CurrentPage)
            {
                case (int)Page.Department:
                    break;

                case (int)Page.PostalCode:
                    value = (int)Page.Department;
                    break;

                case (int)Page.Location:
                    if (wheelDepartment.WheelOrderType == WheelOrderType.Delivery)
                        value = (int)Page.PostalCode;
                    else
                        value = (int)Page.Department;
                    break;

                case (int)Page.MenuOrder:
                    if (input.IsGenerateQRCode)
                        value = (int)Page.PhoneNumber;
                    else
                        value = await GetPageBackFromMenuOrder(wheelDepartment, locations);

                    break;

                case (int)Page.PhoneNumber:
                    if (input.IsGenerateQRCode)
                    {
                        value = (int)Page.PhoneNumber;
                    }
                    else
                    {
                        if (locations.Count() > 1)
                            value = (int)Page.Location;
                        else
                            value = wheelDepartment.WheelOrderType == WheelOrderType.Delivery
                                ? (int)Page.PostalCode
                                : (int)Page.Department;
                    }

                    break;

                case (int)Page.DateOrderFuture:
                    if (orderSetting.AskTheMobileNumberFirst)
                        value = (int)Page.PhoneNumber;
                    else
                        value = locations.Count() > 1 ? (int)Page.Location : (int)Page.Department;
                    break;

                case (int)Page.Language:
                    if (input.IsGenerateQRCode)
                    {
                        value = (int)Page.PhoneNumber;
                    }
                    else if (orderSetting.AskTheMobileNumberFirst)
                    {
                        value = (int)Page.PhoneNumber;
                    }
                    else
                    {
                        if (wheelDepartment.WheelOrderType == WheelOrderType.Delivery)
                            value = locations.Count() > 1
                                ? (int)Page.Location
                                : (int)Page.PostalCode;
                        else
                            value = locations.Count() > 1
                                ? (int)Page.Location
                                : (int)Page.Department;
                    }

                    break;
            }

            return value;
        }

        public async Task<PagedResultDto<WheelOrderListDto>> GetAlllWheelOrders(GetWheelOrdersInputDto input)
        {
            var query = _wheelTicketRepo
                .GetAllIncluding(t => t.Payment)
                .Include(t => t.WheelTicketShippingOrders)
                .Where(t => t.OrderStatus != WheelOrderStatus.Delivered)
                .WhereIf(input.OrderStatus.HasValue, x => x.OrderStatus == input.OrderStatus);

            if (input.StartDate.HasValue)
            {
                var startdate = input.StartDate.Value.Date;
                query = query.Where(x => x.OrderCreatedTime >= startdate);
            }

            if (input.EndDate.HasValue)
            {
                var endDate = input.EndDate.Value.Date.AddDays(1);
                query = query.Where(x => x.OrderCreatedTime < endDate);
            }

            var resutls = query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            var resultCount = resutls.Count();

            var auditLogListDtos = ConvertToWheelOrderListDtos(resutls);

            return new PagedResultDto<WheelOrderListDto>(resultCount, auditLogListDtos);
        }

        private List<WheelOrderListDto> ConvertToWheelOrderListDtos(List<WheelTicket> results)
        {
            var shippingProviderStr =
                SettingManager.GetSettingValueForTenant(AppSettings.ShippingProviderSetting.ShippingProvider,
                    AbpSession.GetTenantId());

            var providerName = string.Empty;
            if (int.TryParse(shippingProviderStr, out var shippingProvider))
                providerName = ((ShippingProvider)shippingProvider).ToString();

            var allDepartments = _wheelDepartmentRepository.GetAll().ToList();
            var allTables = _wheelTableRepo.GetAll().ToList();

            return results.Select(
                ticket =>
                {
                    var orderItem = new WheelOrderListDtoOrderItem
                    {
                        WheelTicketId = ticket.Id,
                        Amount = ticket.Amount,
                        Quantity = ticket.Quantity,
                        AddOn = ticket.AddOn,
                        DeliveryCharge = 0M,
                        DeliveryTime = ticket.DeliveryTime,
                        DeliveringTime = ticket.DeliveringTime,
                        Note = ticket.Note,
                        WheelOrderType = ticket.WheelOrderType,
                        OrderDate = ticket.OrderCreatedTime,
                        TableId = ticket.TableId,
                        TableName = ticket.TableId.HasValue
                            ? allTables.FirstOrDefault(t => t.Id == ticket.TableId.Value)?.Name
                            : "Not Available"
                    };

                    var wheeOrders = JsonConvert.DeserializeObject<List<WheelCartItemDto>>(ticket.MenuItemsDynamic);

                    var menuItems = new List<WheelOrderListMenuItemData>();

                    wheeOrders.ForEach(order =>
                    {
                        var t = new WheelOrderListMenuItemData(order);

                        if (order.SubItems != null)
                            order.SubItems.ForEach(ot =>
                            {
                                var tSub = new WheelOrderListOrderItem(ot);
                                t.OrderItems.Add(tSub);
                            });

                        menuItems.Add(t);
                    });

                    orderItem.MenuItems = menuItems;

                    var shippingOrder = ticket.WheelTicketShippingOrders.OrderByDescending(t => t.Id).FirstOrDefault();

                    var result = new WheelOrderListDto
                    {
                        Order = orderItem,
                        WheelBillingInfo =
                            JsonConvert.DeserializeObject<WheelBillingInfo>(ticket.Payment.ExtraInformation),
                        PaymentOption = ticket.Payment.PaymentOption,
                        PaidAmount = ticket.Payment.PaidAmount,
                        InvoiceNo = ticket.Payment.InvoiceNo,
                        OrderTime = ticket.OrderCreatedTime,
                        OrderStatus = ticket.OrderStatus,
                        Note = ticket.Note,
                        DateDelivery = ticket.DateDelivery?.Date,
                        ActiveShippingDeliveryOrder = shippingOrder?.ShippingOrderId,
                        ActiveShippingDeliveryOrderStatus = shippingOrder?.ShippingOrderStatus,
                        ShippingProvider = providerName
                    };

                    if (result.WheelBillingInfo.WheelDepartmentId.HasValue)
                        result.WheelBillingInfo.WheelDepartmentName = allDepartments
                            .FirstOrDefault(d => d.Id == result.WheelBillingInfo.WheelDepartmentId.Value)?.Name;

                    return result;
                }).ToList();
        }


        public async Task UpdateWheelOrderStatus(UpdateWheelOrderStatusInput input)
        {
            var order = await _wheelTicketRepo
                .GetAllIncluding(t => t.Payment)
                .FirstOrDefaultAsync(t => t.Payment.InvoiceNo == input.InvoiceNo);

            if (order != null && (int)input.Status <= (int)WheelOrderStatus.Delivered)
                order.OrderStatus = input.Status;
        }


        private async Task<string> GetTimezone()
        {
            var timezone = string.Empty;
            if (Clock.SupportsMultipleTimezone)
                timezone = await SettingManager.GetSettingValueForTenantAsync(TimingSettingNames.TimeZone,
                    AbpSession.TenantId ?? 1);

            var defaultTimeZoneId =
                await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, AbpSession.TenantId ?? 1);

            timezone = timezone ?? defaultTimeZoneId;

            return timezone;
        }


        private async Task<CompanySettingDto> GetCompanyInfo()
        {
            var companyNameJson = await SettingManager.GetSettingValueForTenantAsync(
                AppSettings.CompanySetting.CompanyName,
                AbpSession.GetTenantId());

            var companyLogoUrlJson =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.CompanySetting.CompanyLogoUrl,
                    AbpSession.GetTenantId());

            var companyAddress =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingAddress,
                    AbpSession.GetTenantId());

            var taxNo =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.TenantManagement.BillingTaxVatNo,
                    AbpSession.GetTenantId());
            var companyName = string.Empty;
            if (!companyNameJson.IsNullOrEmpty()) companyName = JsonConvert.DeserializeObject<string>(companyNameJson);

            var companyLogoUrl = string.Empty;
            if (!companyLogoUrlJson.IsNullOrEmpty())
                companyLogoUrl = JsonConvert.DeserializeObject<string>(companyLogoUrlJson);

            return new CompanySettingDto
            {
                CompanyName = companyName,
                CompanyLogoUrl = companyLogoUrl,
                CompanyAddress = companyAddress,
                TaxNo = taxNo
            };
        }


        public async Task<CheckMinOrderByDeliveryZoneOutput> CheckMinOrderByDeliveryZone(
            CheckDeliveryZoneAndFeeInput input)
        {
            var result = new CheckMinOrderByDeliveryZoneOutput();

            var location = await _wheelLocationRepository.FirstOrDefaultAsync(input.LocationId);

            if (string.IsNullOrEmpty(location.WheelDeliveryZoneIds))
            {
                result.IsValid = true;
                return result;
            }

            var addressList = await _oneMapGatewayManager.GetAddressByPostCode(input.PostalCode);

            var address = addressList.FirstOrDefault();

            if (address == null)
            {
                result.IsValid = true;
                return result;
            }

            var longitude = Convert.ToDouble(address.Longitude);
            var latitude = Convert.ToDouble(address.Latitude);

            var output = await _wheelAppService.GetDeliveryZonesContainedPoint(longitude, latitude);

            var deliveryZoneIds = location.WheelDeliveryZoneIds.Split(";", StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            var locationDeliveryZones = output.Where(zone => deliveryZoneIds.Contains(zone.Id.ToString())).ToList();
            if (location.OutsideDeliveryZone)
            {
                var zone = await _zoneRepository.GetAll().Where(x => deliveryZoneIds.Contains(x.Id.ToString()))
                    .ToListAsync();

                locationDeliveryZones = ObjectMapper.Map<List<WheelDeliveryZoneDto>>(zone);
            }

            if (locationDeliveryZones.Any())
            {
                result.MinimuOrderTotal = locationDeliveryZones.Min(x => x.MinimumTotalAmount);
                result.IsValid = input.TotalPrice >= result.MinimuOrderTotal;
            }
            else
            {
                result.IsValid = true;
            }

            return result;
        }

        private async Task<int> GetPageBackFromMenuOrder(GetWheelDepartmentOutput wheelDepaterment,
            List<WheelLocation> locations)
        {
            int value;
            var orderSetting = await _tenantSettingsAppService.GetOrderSetting();
            if (orderSetting.AskTheMobileNumberFirst)
                switch (wheelDepaterment.WheelOrderType)
                {
                    case WheelOrderType.Delivery:
                        value = (int)Page.PhoneNumber;
                        break;

                    default:
                        if (wheelDepaterment.AllowPreOrder && !wheelDepaterment.IsOpeningHour &&
                            wheelDepaterment.DateDefaultOrderFututer != null)
                            value = (int)Page.DateOrderFuture;
                        else
                            value = (int)Page.PhoneNumber;
                        break;
                }
            else
                switch (wheelDepaterment.WheelOrderType)
                {
                    case WheelOrderType.Delivery:
                        value = locations != null && locations.Count() > 1
                            ? (int)Page.Location
                            : (int)Page.PostalCode;
                        break;

                    default:
                        if (wheelDepaterment.AllowPreOrder && !wheelDepaterment.IsOpeningHour &&
                            wheelDepaterment.DateDefaultOrderFututer != null)
                            value = (int)Page.DateOrderFuture;
                        else
                            value = locations != null && locations.Count() > 1
                                ? (int)Page.Location
                                : (int)Page.Department;
                        break;
                }

            return value;
        }

        private async Task<GetWheelDepartmentOutput> GetWheelDepartmentById(int id)
        {
            var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(id);
            var output = ObjectMapper.Map<GetWheelDepartmentOutput>(department);
            output.IsOpeningHour = await IsOpeningHour(id);
            if (string.IsNullOrEmpty(department.Dynamic) == false)
            {
                dynamic itemDynamic = JObject.Parse(department.Dynamic);
                output.IsRequireOtp = itemDynamic.IsRequireOtp ?? false;
                output.FutureOrderDaysLimit = itemDynamic.FutureOrderDaysLimit;
            }

            if (output.IsOpeningHour == false && output.AllowPreOrder)
            {
                if (!output.FutureOrderDaysLimit.HasValue || output.FutureOrderDaysLimit == 0)
                {
                    output.IsOpeningHour = false;
                }
                else
                {
                    var startDate = await GetDefaultDateFuture(id);
                    var endDate = Clock.Now.AddDays(output.FutureOrderDaysLimit.Value);
                    if (startDate.Date > endDate.Date)
                        output.IsOpeningHour = false;
                    else
                        output.DateDefaultOrderFututer = startDate.Date;
                    //output.IsOpeningHour = true;
                }
            }

            return output;
        }

        /* Order Display at the eDine*/

        public async Task<WheelTicketModel> GetWheelOrder(string invoiceNo)
        {
            var wheelTicket = await _wheelTicketRepo.GetAllIncluding(x => x.Payment)
                .FirstOrDefaultAsync(t => t.Payment.InvoiceNo == invoiceNo);

            var result = new WheelTicketModel
            {
                Order = new WheelOrderDto
                {
                    WheelTicketId = wheelTicket.Id,
                    DeliveryTime = wheelTicket.DeliveryTime,
                    Amount = wheelTicket.Amount,
                    Quantity = wheelTicket.Quantity,
                    Address = wheelTicket.Address,
                    AddOn = wheelTicket.AddOn,
                    CustomerAddressId = wheelTicket.CustomerAddressId,
                    OrderItems = JsonConvert.DeserializeObject<List<WheelCartItemDto>>(wheelTicket.MenuItemsDynamic),
                    WheelOrderType = wheelTicket.WheelOrderType,
                    PaymentId = wheelTicket.WheelPaymentId,
                    Note = wheelTicket.Note,
                    DiscountAmount = wheelTicket.DiscountAmount,
                    TableId = wheelTicket.TableId
                },
                WheelBillingInfo =
                    JsonConvert.DeserializeObject<WheelBillingInfo>(wheelTicket.Payment.ExtraInformation),
                PaymentOption = wheelTicket.Payment.PaymentOption,
                PaidAmount = wheelTicket.Payment.PaidAmount,
                InvoiceNo = wheelTicket.Payment.InvoiceNo,
                OrderTime = wheelTicket.Payment.CreationTime,
                ShippingProviderResponseJson = wheelTicket.ShippingProviderResponseJson
            };
            if (!string.IsNullOrEmpty(wheelTicket.AmountCalculations))
                result.AmountCalculations =
                    JsonConvert.DeserializeObject<CartTotalDto>(wheelTicket.AmountCalculations);

            if (result.Order.TableId.HasValue)
            {
                var myTable = await _wheelTableRepo.GetAsync(result.Order.TableId.Value);
                if (myTable != null) result.Order.TableName = myTable.Name;
            }

            var deliveryTime = 0M;
            if (wheelTicket.WheelOrderType == WheelOrderType.Delivery)
            {
                var wheelBillingInfo = result.WheelBillingInfo;
                if (wheelBillingInfo != null)
                    deliveryTime = AsyncHelper.RunSync(() =>
                        CalculateDeliveryTime(Convert.ToInt16(wheelBillingInfo.WheelLocationId),
                            Convert.ToDouble(wheelBillingInfo.Long), Convert.ToDouble(wheelBillingInfo.Lat)));
                ;
            }

            result.WheelBillingInfo.WheelDepartmentName = wheelTicket.WheelOrderType.ToString();
            result.DeliveryTime = deliveryTime;
            return result;
        }

        /*
         * Order Creates for eDine
         */

        private async Task<PaymentOutputDto> CreateOrder(CreateWheelOrderPaymentInputDto input)
        {
            var invoiceNo = await _wheelInvoiceNumberGenerator.GetNewInvoiceNumber();
            var customer = await _customerManager.GetCurrentCustomerAsync();
            var carts = await _wheelCartAppService.GetCartItems(true);

            var payment = new WheelPayment
            {
                TenantId = AbpSession.TenantId ?? 1,
                InvoiceNo = invoiceNo,
                PaymentDate = Clock.Now,
                CustomerId = customer?.Id ?? 0,
                CustomerName = customer?.Name,
                CustomerAddress = input.BillingInfo.GetFullAddress(),
                PaidAmount = input.PaidAmount,
                PaymentOption = input.PaymentOption,
                ExtraInformation = JsonConvert.SerializeObject(input.BillingInfo)
            };

            int? paymentId = await _wheelPaymentRepository.InsertAndGetIdAsync(payment);

            var wheelTicket = ObjectMapper.Map<WheelTicket>(input.Order);

            wheelTicket.OrderCreatedTime = Clock.Now;
            wheelTicket.CustomerId = customer?.Id ?? 0;
            wheelTicket.WheelPaymentId = paymentId.Value;
            wheelTicket.MenuItemsDynamic = JsonConvert.SerializeObject(carts.CartItems);
            wheelTicket.AmountCalculations = JsonConvert.SerializeObject(carts.CartTotal);

            var orderStatusSetting = await _tenantSettingsAppService.GetOrderSetting();
            wheelTicket.OrderStatus =
                orderStatusSetting.Accepted ? WheelOrderStatus.Accepted : WheelOrderStatus.OrderReceived;

            if (input.BillingInfo?.WheelDepartmentId != null)
            {
                var myDepartmentId = Convert.ToInt32(input.BillingInfo.WheelDepartmentId.Value);
                var department = await _wheelDepartmentRepository.FirstOrDefaultAsync(x => x.Id == myDepartmentId);
                if (department != null)
                    if (department.AutoAccept)
                        wheelTicket.OrderStatus = WheelOrderStatus.Accepted;
            }

            if (input.Order.WheelOrderType != WheelOrderType.Delivery)
                wheelTicket.CustomerAddressId = null;


            if (input.Order.WheelOrderType == WheelOrderType.Delivery)
            {
                var shippingProviderSetting = await _tenantSettingsAppService.GetShippingProviderSetting();

                if (orderStatusSetting.AutoRequestDeliver && shippingProviderSetting != null &&
                    orderStatusSetting.AutoRequestDeliver &&
                    shippingProviderSetting.ShippingProvider != ShippingProvider.None)
                    wheelTicket.ShippingProvider = shippingProviderSetting.ShippingProvider;
            }

            await _wheelTicketRepo.InsertAndGetIdAsync(wheelTicket);

            await CurrentUnitOfWork.SaveChangesAsync();

            if (wheelTicket.WheelOrderType == WheelOrderType.Delivery)
            {
                var autoCallDeliver =
                    await SettingManager.GetSettingValueForTenantAsync<bool>(
                        AppSettings.OrderSetting.AutoRequestDeliver, AbpSession.GetTenantId());

                if (autoCallDeliver)
                {
                    var shippingProviderResponse = await _shippingService.PlaceShippingOrder(wheelTicket, null);
                    wheelTicket.ShippingProviderResponseJson = shippingProviderResponse.PlaceOrderResponse;
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            if (input.WheelOrderTracking != null)
            {
                var deliveryTime = input.Order.DeliveryTime;
                var timeArr = deliveryTime.Split(" ");

                var scheduleAt = deliveryTime == "ASAP"
                    ? Clock.Now.Date.ToString("yyyy-MM-dd")
                    : deliveryTime.Substring(0, 10);

                if (string.IsNullOrEmpty(scheduleAt)) scheduleAt = Clock.Now.Date.ToString("yyyy-MM-dd");

                input.WheelOrderTracking.OrderDate = scheduleAt;
                await _wheelOrderTrackingService.CreateOrUpdateWheelOrderTracking(input.WheelOrderTracking);
            }

            // Mask Promotion Code Claimed if have
            if (input.DiscountDetails?.Any() == true)
            {
                var claimedTime = Clock.Now;

                input.DiscountDetails = input.DiscountDetails.DistinctBy(x => x.Id).ToList();

                var promotionCodeIds = input.DiscountDetails.Select(x => x.Id).ToList();

                var promotionCodes = await _tiffinPromotionCodeRepository.GetAll()
                    .Where(x => promotionCodeIds.Contains(x.Id) && x.IsClaimed == false)
                    .ToListAsync();

                foreach (var promotionCode in promotionCodes)
                {
                    promotionCode.ClaimedAmount =
                        input.DiscountDetails.First(x => x.Id == promotionCode.Id).ClaimAmount;

                    promotionCode.ClaimedTime = claimedTime;
                    promotionCode.IsClaimed = true;
                    promotionCode.WheelPaymentId = paymentId;
                    await _tiffinPromotionCodeRepository.UpdateAsync(promotionCode);
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            await _wheelCartAppService.ResetCheckout();

            try
            {
                if (AbpSession.TenantId != null)
                {
                    var emailSendingDto = new WheelOrderSendEmailInput
                    {
                        DeliveryAddress = input.BillingInfo,
                        OrderItems = carts.CartItems,
                        Order = new WheelTicketDto
                        {
                            SubTotal = input.Order.Amount,
                            Total = input.PaidAmount,
                            DeliveryTime = input.Order.DeliveryTime,
                            Note = input.Order.Note
                        },
                        TenantId = AbpSession.TenantId.Value
                    };

                    await _emailNotificationJobService.SendEmailWhenCustomerOrder(emailSendingDto);
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return new PaymentOutputDto
            {
                PurchasedSuccess = true,
                PaymentId = paymentId ?? -1,
                InvoiceNo = invoiceNo
            };
        }
    }
}
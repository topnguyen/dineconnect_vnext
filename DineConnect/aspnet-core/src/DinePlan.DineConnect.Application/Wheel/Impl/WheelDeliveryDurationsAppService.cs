﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelDeliveryDurationsAppService : DineConnectAppServiceBase, IWheelDeliveryDurationsAppService
    {
         private readonly IRepository<WheelDeliveryDuration> _wheelDeliveryDurationRepository;
         private readonly IWheelDeliveryDurationsExcelExporter _wheelDeliveryDurationsExcelExporter;
         

          public WheelDeliveryDurationsAppService(IRepository<WheelDeliveryDuration> wheelDeliveryDurationRepository, IWheelDeliveryDurationsExcelExporter wheelDeliveryDurationsExcelExporter ) 
          {
            _wheelDeliveryDurationRepository = wheelDeliveryDurationRepository;
            _wheelDeliveryDurationsExcelExporter = wheelDeliveryDurationsExcelExporter;
            
          }

         public async Task<PagedResultDto<GetWheelDeliveryDurationForViewDto>> GetAll(GetAllWheelDeliveryDurationsInput input)
         {
            
            var filteredWheelDeliveryDurations = _wheelDeliveryDurationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false )
                        .WhereIf(input.MinTenantIdFilter != null, e => e.TenantId >= input.MinTenantIdFilter)
                        .WhereIf(input.MaxTenantIdFilter != null, e => e.TenantId <= input.MaxTenantIdFilter)
                        .WhereIf(input.MinDeliveryDistanceFilter != null, e => e.DeliveryDistance >= input.MinDeliveryDistanceFilter)
                        .WhereIf(input.MaxDeliveryDistanceFilter != null, e => e.DeliveryDistance <= input.MaxDeliveryDistanceFilter)
                        .WhereIf(input.MinDeliveryTimeFilter != null, e => e.DeliveryTime >= input.MinDeliveryTimeFilter)
                        .WhereIf(input.MaxDeliveryTimeFilter != null, e => e.DeliveryTime <= input.MaxDeliveryTimeFilter);

            var pagedAndFilteredWheelDeliveryDurations = filteredWheelDeliveryDurations
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var wheelDeliveryDurations = from o in pagedAndFilteredWheelDeliveryDurations
                         select new GetWheelDeliveryDurationForViewDto() {
                            WheelDeliveryDuration = new WheelDeliveryDurationDto
                            {
                                TenantId = o.TenantId,
                                DeliveryDistance = o.DeliveryDistance,
                                DeliveryTime = o.DeliveryTime,
                                Id = o.Id
                            }
                        };

            var totalCount = await filteredWheelDeliveryDurations.CountAsync();

            return new PagedResultDto<GetWheelDeliveryDurationForViewDto>(
                totalCount,
                await wheelDeliveryDurations.ToListAsync()
            );
         }
         
         public async Task<GetWheelDeliveryDurationForViewDto> GetWheelDeliveryDurationForView(int id)
         {
            var wheelDeliveryDuration = await _wheelDeliveryDurationRepository.GetAsync(id);

            var output = new GetWheelDeliveryDurationForViewDto { WheelDeliveryDuration = ObjectMapper.Map<WheelDeliveryDurationDto>(wheelDeliveryDuration) };
            
            return output;
         }
         
         
         public async Task<GetWheelDeliveryDurationForEditOutput> GetWheelDeliveryDurationForEdit(EntityDto input)
         {
            var wheelDeliveryDuration = await _wheelDeliveryDurationRepository.FirstOrDefaultAsync(input.Id);
           
            var output = new GetWheelDeliveryDurationForEditOutput {WheelDeliveryDuration = ObjectMapper.Map<CreateOrEditWheelDeliveryDurationDto>(wheelDeliveryDuration)};
            
            return output;
         }

         public async Task CreateOrEdit(CreateOrEditWheelDeliveryDurationDto input)
         {
            if(input.Id == null){
                await Create(input);
            }
            else{
                await Update(input);
            }
         }

         
         protected virtual async Task Create(CreateOrEditWheelDeliveryDurationDto input)
         {
            var wheelDeliveryDuration = ObjectMapper.Map<WheelDeliveryDuration>(input);

            
            if (AbpSession.TenantId != null)
            {
                wheelDeliveryDuration.TenantId = AbpSession.TenantId ?? 1;
            }
        

            await _wheelDeliveryDurationRepository.InsertAsync(wheelDeliveryDuration);
         }

         
         protected virtual async Task Update(CreateOrEditWheelDeliveryDurationDto input)
         {
            var wheelDeliveryDuration = await _wheelDeliveryDurationRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, wheelDeliveryDuration);
         }

         
         public async Task Delete(EntityDto input)
         {
            await _wheelDeliveryDurationRepository.DeleteAsync(input.Id);
         } 

        public async Task<FileDto> GetWheelDeliveryDurationsToExcel(GetAllWheelDeliveryDurationsForExcelInput input)
         {
            
            var filteredWheelDeliveryDurations = _wheelDeliveryDurationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false )
                        .WhereIf(input.MinTenantIdFilter != null, e => e.TenantId >= input.MinTenantIdFilter)
                        .WhereIf(input.MaxTenantIdFilter != null, e => e.TenantId <= input.MaxTenantIdFilter)
                        .WhereIf(input.MinDeliveryDistanceFilter != null, e => e.DeliveryDistance >= input.MinDeliveryDistanceFilter)
                        .WhereIf(input.MaxDeliveryDistanceFilter != null, e => e.DeliveryDistance <= input.MaxDeliveryDistanceFilter)
                        .WhereIf(input.MinDeliveryTimeFilter != null, e => e.DeliveryTime >= input.MinDeliveryTimeFilter)
                        .WhereIf(input.MaxDeliveryTimeFilter != null, e => e.DeliveryTime <= input.MaxDeliveryTimeFilter);

            var query = (from o in filteredWheelDeliveryDurations
                         select new GetWheelDeliveryDurationForViewDto() { 
                            WheelDeliveryDuration = new WheelDeliveryDurationDto
                            {
                                TenantId = o.TenantId,
                                DeliveryDistance = o.DeliveryDistance,
                                DeliveryTime = o.DeliveryTime,
                                Id = o.Id
                            }
                         });


            var wheelDeliveryDurationListDtos = await query.ToListAsync();

            return _wheelDeliveryDurationsExcelExporter.ExportToFile(wheelDeliveryDurationListDtos);
         }


    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Wheel.Dtos;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelDynamicPagesAppService : DineConnectAppServiceBase, IWheelDynamicPagesAppService
    {
        private readonly IRepository<WheelDynamicPages> _wheelDynamicPagesRepository;
        private readonly IRepository<WheelSetup> _wheelSetupRepository;

        public WheelDynamicPagesAppService(IRepository<WheelDynamicPages> wheelDynamicPagesRepository, IRepository<WheelSetup> wheelSetupRepository)
        {
            this._wheelDynamicPagesRepository = wheelDynamicPagesRepository;
            _wheelSetupRepository = wheelSetupRepository;
        }

        
        public async Task CreateOrUpdateWheelDynamicPage(CreateOrEditWheelDynamicPagesInput input)
        {
            var tenantId = AbpSession.GetTenantId();

            var wheelDynamicPages = ObjectMapper.Map<WheelDynamicPages>(input);
            wheelDynamicPages.TenantId = tenantId;
            await _wheelDynamicPagesRepository.InsertOrUpdateAsync(wheelDynamicPages);
        }

        
        public async Task DeleteWheelDynamicPage(EntityDto input)
        {
            await _wheelDynamicPagesRepository.DeleteAsync(input.Id);
        }

        
        public async Task<WheelDynamicPageEditDto> GetWheelDynamicPageForEdit(GetWheelDynamicPageForEditInput input)
        {
            var tenantDatabase = await _wheelDynamicPagesRepository.GetAsync(input.Id);
            return ObjectMapper.Map<WheelDynamicPageEditDto>(tenantDatabase);
        }

        public async Task<PagedResultDto<WheelDynamicPagesListDto>> GetWheelDynamicPages(GetWheelDynamicPagesInput input)
        {
            var tenantId = AbpSession.GetTenantId();
            var query = _wheelDynamicPagesRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    t => t.Name.Contains(input.Filter) && t.TenantId == tenantId
                );

            if (!input.Sorting.IsNullOrEmpty())
                query = query.OrderBy(input.Sorting);

            var wheelDynamicPagesCount = await query.CountAsync();
            var wheelDynamicPages = await query.PageBy(input).ToListAsync();

            return new PagedResultDto<WheelDynamicPagesListDto>(
                wheelDynamicPagesCount,
                ObjectMapper.Map<List<WheelDynamicPagesListDto>>(wheelDynamicPages)
                );
        }

        public async Task<List<WheelDynamicPageForComboboxItemDto>> GetDynamicPageForCombobox(int? selectedId = null)
        {
            var dynamicPages = await _wheelDynamicPagesRepository
                 .GetAllListAsync();

            var dynamicPagesItems = new List<WheelDynamicPageForComboboxItemDto>(dynamicPages.Select(x =>
                new WheelDynamicPageForComboboxItemDto(x.Id.ToString(), x.Name, false)));

            if (dynamicPagesItems.Count > 0)
            {
                if (selectedId.HasValue)
                {
                    var selectedItem = dynamicPagesItems.FirstOrDefault(x => x.Value == selectedId.Value.ToString());
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        dynamicPagesItems[0].IsSelected = true;
                    }
                }
            }

            return dynamicPagesItems;
        }

        [AbpAllowAnonymous]
        public async Task<WheelDynamicPageEditDto> GetDynamicPageTermOrPrivacy(WheelDynamicPage key)
        {
            WheelDynamicPageEditDto result = null;
            //var query = _wheelDynamicPagesRepository
            //  .GetAll()
            switch (key)
            {
                case WheelDynamicPage.TermAndCondition:
                    var termAndCondtionsPage = await _wheelDynamicPagesRepository
                                                .GetAll()
                                                .Where(x => x.Name.Contains("Term") || x.Name.Contains("Condition"))
                                                .FirstOrDefaultAsync();
                    result = ObjectMapper.Map<WheelDynamicPageEditDto>(termAndCondtionsPage);
                    break;

                case WheelDynamicPage.Privacy:
                    var privacyPage = await _wheelDynamicPagesRepository
                                                .GetAll()
                                                .Where(x => x.Name.Contains("Privacy"))
                                                .FirstOrDefaultAsync();
                    result = ObjectMapper.Map<WheelDynamicPageEditDto>(privacyPage);
                    break;
            }

            if (result == null)
            {
                var homePage = await _wheelSetupRepository.GetAllIncluding(x => x.HomePage).Select(x => x.HomePage).FirstOrDefaultAsync();
                result = ObjectMapper.Map<WheelDynamicPageEditDto>(homePage);
            }
            return result;
        }
    }
}
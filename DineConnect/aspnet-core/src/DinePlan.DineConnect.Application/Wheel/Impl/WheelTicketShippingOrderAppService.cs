using System;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Settings;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Http;
using DinePlan.DineConnect.Wheel.ShippingProviders.GogoVan;
using DinePlan.DineConnect.Shipping;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Abp.UI;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.GogoVan;

namespace DinePlan.DineConnect.Wheel
{
    public class WheelTicketShippingOrderAppService : DineConnectAppServiceBase, IWheelTicketShippingOrderAppService
    {
        private readonly IRepository<WheelTicketShippingOrder> _wheelTicketShippingOrderRepository;
        private readonly IRepository<WheelTicket> _wheelTicketRepository;
        private readonly IWheelOrderAppService _wheelOrderAppService;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IShippingService _shippingService;

        public WheelTicketShippingOrderAppService(
           IRepository<WheelTicketShippingOrder> wheelTicketShippingOrderRepository,
           IWheelOrderAppService wheelOrderAppService,
           ITenantSettingsAppService tenantSettingsAppService,
           IRepository<WheelTicket> wheelTicketRepository, IShippingService shippingService)
        {
            _wheelTicketShippingOrderRepository = wheelTicketShippingOrderRepository;
            _wheelOrderAppService = wheelOrderAppService;
            _tenantSettingsAppService = tenantSettingsAppService;
            _wheelTicketRepository = wheelTicketRepository;
            _shippingService = shippingService;
        }

        public async Task<string> CreateWheelTicketShippingOrder(CreateWheelTicketShippingOrderInputDto input)
        {
            var ticket = await _wheelTicketRepository
                .GetAllIncluding(t => t.Payment)
                .FirstOrDefaultAsync(t => t.Id == input.TicketId);

            if (ticket.WheelOrderType != WheelDepartmentConsts.WheelOrderType.Delivery)
            {
                throw new UserFriendlyException($"Can not place shipping order for a {ticket.WheelOrderType} order");
            }

            ExtraPlaceShippingOrderInfo extra = null;

            if (input.IsManual)
            {
                extra = new ExtraPlaceShippingOrderInfo()
                {
                    ServiceType = input.ServiceType,
                    ManualScheduleAt = DateTime.ParseExact($"{input.ManualTime}", "yyyy/MM/dd HH:mm", System.Globalization.CultureInfo.InvariantCulture)
                };
            }

            var shippingProviderResponse = await _shippingService.PlaceShippingOrder(ticket, extra);

            if (shippingProviderResponse.ErrorMessages != null && shippingProviderResponse.ErrorMessages.Any())
            {
                throw new UserFriendlyException("Cannot place shipping order", string.Join("\n", shippingProviderResponse.ErrorMessages.ToArray()));
            }

            return shippingProviderResponse.CustomerOrderId;
        }

        public async Task<GetShippingOrderQuotationResult> GetWheelTicketShippingOrderQuotation(int ticketId)
        {
            var ticket = await _wheelTicketRepository
                .GetAllIncluding(t => t.Payment)
                .FirstOrDefaultAsync(t => t.Id == ticketId);

            if (ticket.WheelOrderType != WheelDepartmentConsts.WheelOrderType.Delivery)
            {
                throw new UserFriendlyException($"Can not place shipping order for a {ticket.WheelOrderType} order");
            }

            var shippingProviderResponse = await _shippingService.GetShippingOrderQuotation(ticket);

            if (shippingProviderResponse == null)
            {
                throw new UserFriendlyException("Can not get shipping order quotation");
            }

            return shippingProviderResponse;
        }

        public async Task CancelWheelTicketShippingOrder(int ticketId)
        {
            var ticket = await _wheelTicketRepository
                .FirstOrDefaultAsync(t => t.Id == ticketId);

            await _shippingService.CancelShippingOrder(ticket);
        }

        [AllowAnonymous]
        [HttpPost]
        [DisableValidation]
        public async Task LalaMoveWebhook(int? tenantId, LalaMoveResponWebhook input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                if (input == null || input.Data == null || input.Data.Order == null)
                {
                    return;
                }

                var shippingTicket = await _wheelTicketShippingOrderRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId && x.ShippingOrderId == input.Data.Order.Id);

                if (shippingTicket == null)
                {
                    return;
                }

                switch (input.EventType)
                {
                    case LalaMoveStatusConst.ORDER_STATUS_CHANGED:
                        shippingTicket.ShippingOrderStatus = input.Data.Order.Status;
                        break;

                    case LalaMoveStatusConst.ORDER_AMOUNT_CHANGED:
                        shippingTicket.ShippingOrderStatus = input.Data.Order.Status;
                        shippingTicket.ShippingOrderAmount = Newtonsoft.Json.JsonConvert.SerializeObject(new { Amount = input.Data.Order.Price.TotalPrice, Currency = input.Data.Order.Price.Currency });
                        break;
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [DisableValidation]
        public async Task GoGoVanWebhook(int? tenantId, [Microsoft.AspNetCore.Mvc.FromBody] GoGoVanResponWebhook input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                if (input == null || input.Data == null)
                {
                    return;
                }

                var shippingTicket = await _wheelTicketShippingOrderRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId && x.ShippingOrderId == input.Data.UUID);

                if (shippingTicket == null)
                {
                    return;
                }

                shippingTicket.ShippingOrderStatus = input.Data.Status;
                shippingTicket.ShippingOrderAmount = Newtonsoft.Json.JsonConvert.SerializeObject(new { Amount = input.Data.Price.Amount, Currency = input.Data.Price.Currency });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [DisableValidation]
        public async Task DunzoWebhook(int? tenantId, [Microsoft.AspNetCore.Mvc.FromBody] DunzoTaskStatusRepsonse input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                if (input == null || string.IsNullOrEmpty(input.TaskId))
                {
                    return;
                }

                var shippingTicket = await _wheelTicketShippingOrderRepository.FirstOrDefaultAsync(x => x.TenantId == tenantId && x.ShippingOrderId == input.TaskId);

                if (shippingTicket == null)
                {
                    return;
                }

                shippingTicket.ShippingOrderStatus = input.State;
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Tiffins.MemberPortal;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion.Exporting;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport;
using DinePlan.DineConnect.Whatapp;
using DinePlan.DineConnect.Wheel.Promotion;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl.Promotion
{
    
    public class WheelPromotionAppService : TiffinPromotionAppService, IWheelPromotionAppService
    {
        public WheelPromotionAppService(IRepository<TiffinPromotion> promotionRepository,
            IRepository<TiffinPromotionCode> promotionCodeRepository,
            IRepository<Customer> customerRepository,
            IPromotionExcelExporter promotionExcelExporter,
            ITiffinMemberPortalAppService tiffinMemberPortalAppService,
            ITenantEmailSendAppService tenantEmailSendAppService,
            ITenantSettingsAppService tenantSettingsAppService,
            IServiceProvider serviceProvider,
            ICustomerReportAppService customerReportAppService,
            ITenantWhatappSendAppService tenantWhatappSendAppService)
            : base(promotionRepository,
                promotionCodeRepository,
                customerRepository
                , promotionExcelExporter
                , tiffinMemberPortalAppService
                , tenantEmailSendAppService
                , tenantSettingsAppService
                , serviceProvider
                , customerReportAppService
                , tenantWhatappSendAppService)
        {
        }

        [AbpAllowAnonymous]
        public async Task<PromotionEditDto> GetWheelPromotionByCode(string code, bool isIncludeCodesDetail = false,
            bool? claimedStatus = false)
        {
            var tenantId = AbpSession?.TenantId ?? 0;
            if (tenantId > 0)
            {
                var promotionId = await _promotionCodeRepository.GetAll()
                    .Where(x => x.Code.Equals(code) && x.TenantId == tenantId)
                    .WhereIf(claimedStatus.HasValue, x => x.IsClaimed == claimedStatus)
                    .Select(x => x.PromotionId)
                    .FirstOrDefaultAsync();

                if (promotionId == default) return null;

                var promotion = await GetPromotionForEdit(new EntityDto
                {
                    Id = promotionId
                });

                if (!isIncludeCodesDetail) promotion.Codes = new List<PromotionCodeDto>();
                return promotion;

            }
            return null;
        }

        [AbpAllowAnonymous]
        public async Task<PromotionDiscountPaymentDto> CalculatePromotionDiscountPayment(
            CalculatePromotionDiscountPaymentInputDto input)
        {
            var promotionDiscountPaymentInfo = new PromotionDiscountPaymentDto();

            promotionDiscountPaymentInfo.OriginalAmount = input.PaidAmount;
            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.OriginalAmount;

            // Get Promotion Info

            input.Codes = input.Codes?.Select(c => c.ToLower()).Distinct().ToList();

            var appliedPromotion = await ValidateAndGetWheelPromotionInfo(input.Codes);

            if (appliedPromotion == null)
                throw new UserFriendlyException("The Coupon Code is wrong or expired. Please check it.");

            //return promotionDiscountPaymentInfo;

            if (appliedPromotion.MinimumOrderAmount > promotionDiscountPaymentInfo.OriginalAmount)
                throw new UserFriendlyException(string.Format(L("TiffinPromotionMinimumOrderAmountRequire"),
                    appliedPromotion.MinimumOrderAmount, DineConnectConsts.Currency));

            double onePromotionCodeClaimAmount;

            if (appliedPromotion.Type == TiffinPromotionType.Percentage)
                onePromotionCodeClaimAmount =
                    promotionDiscountPaymentInfo.OriginalAmount * appliedPromotion.VoucherValue / 100;
            else
                onePromotionCodeClaimAmount = appliedPromotion.VoucherValue;

            // Fill Detail Discount for each Code

            promotionDiscountPaymentInfo.DiscountDetails = new List<PromotionCodeDiscountDetailPaymentDto>();

            if (input.Codes != null)
                foreach (var code in input.Codes)
                {
                    var promotionCode = appliedPromotion.Codes.FirstOrDefault(x => x.Code.ToLower() == code);

                    if (promotionCode == null) continue;

                    var promotionCodeDiscountDetail = new PromotionCodeDiscountDetailPaymentDto
                    {
                        Id = promotionCode.Id,
                        Code = promotionCode.Code,
                        ClaimAmount = onePromotionCodeClaimAmount
                    };

                    promotionDiscountPaymentInfo.DiscountDetails.Add(promotionCodeDiscountDetail);
                }

            promotionDiscountPaymentInfo.TotalClaimAmount =
                promotionDiscountPaymentInfo.DiscountDetails.Sum(x => x.ClaimAmount);

            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.OriginalAmount -
                                                            promotionDiscountPaymentInfo.TotalClaimAmount;

            promotionDiscountPaymentInfo.DiscountedAmount = promotionDiscountPaymentInfo.DiscountedAmount > 0
                ? promotionDiscountPaymentInfo.DiscountedAmount
                : 0;

            return promotionDiscountPaymentInfo;
        }

        protected async Task<PromotionEditDto> ValidateAndGetWheelPromotionInfo(List<string> codes)
        {
            codes = codes?.Distinct().ToList();


            PromotionEditDto mainPromotion = null;

            if (codes?.Any() == true)
            {
                var promotions = new List<PromotionEditDto>();

                foreach (var code in codes)
                {
                    var promotion = await GetWheelPromotionByCode(code, true);

                    if (promotions.All(x => x.Id != promotion.Id)) promotions.Add(promotion);

                    var promotionCode = promotion.Codes.FirstOrDefault(x => x.Code == code);
                }

                if (promotions.Count > 1) throw new UserFriendlyException(L("PromotionMaximumOnePromotionPerPayment"));

                mainPromotion = promotions.Single();

                if (!mainPromotion.AllowMultipleRedemption && codes.Count > 1)
                    throw new UserFriendlyException(string.Format(L("PromotionMaximumPromotionCodeCanApply"), 1));

                mainPromotion.MultiRedemptionCount =
                    mainPromotion.MultiRedemptionCount < 1 ? 1 : mainPromotion.MultiRedemptionCount;

                if (codes.Count > mainPromotion.MultiRedemptionCount)
                    throw new UserFriendlyException(string.Format(L("PromotionMaximumPromotionCodeCanApply"),
                        mainPromotion.MultiRedemptionCount));
            }

            return mainPromotion;
        }
    }
}
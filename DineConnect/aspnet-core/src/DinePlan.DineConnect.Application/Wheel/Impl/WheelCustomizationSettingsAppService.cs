﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.UiCustomization;
using DinePlan.DineConnect.Wheel.Dtos;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class WheelCustomizationSettingsAppService : DineConnectAppServiceBase, IWheelCustomizationSettingsAppService
    {
        private readonly SettingManager _settingManager;
        private readonly IIocResolver _iocResolver;
        private readonly IUiThemeCustomizerFactory _uiThemeCustomizerFactory;

        public WheelCustomizationSettingsAppService(
            SettingManager settingManager,
            IIocResolver iocResolver,
            IUiThemeCustomizerFactory uiThemeCustomizerFactory)
        {
            _settingManager = settingManager;
            _iocResolver = iocResolver;
            _uiThemeCustomizerFactory = uiThemeCustomizerFactory;
        }

        public async Task<List<ThemeSettingsDto>> GetUiManagementSettings()
        {
            var settings = new List<ThemeSettingsDto>();
            var themeCustomizers = _iocResolver.ResolveAll<IWheelUiCustomizer>();

            foreach (var themeUiCustomizer in themeCustomizers)
            {
                var themeSettings = await themeUiCustomizer.GetUiSettings();
                settings.Add(themeSettings.BaseSettings);
            }

            return settings;
        }

        public async Task<ThemeSettingsDto> GetUiManagementSetting(string themeId)
        {
            var tenantId = AbpSession.TenantId ?? 1;
            var theme = _uiThemeCustomizerFactory.GetWheelUiCustomizer(themeId);
            return await theme.GetTenantUiCustomizationSettings(tenantId);
        }

        public async Task UpdateUiManagementSettings(ThemeSettingsDto settings)
        {
            var themeCustomizer = _uiThemeCustomizerFactory.GetWheelUiCustomizer(settings.Theme);
            await themeCustomizer.UpdateTenantUiManagementSettingsAsync(AbpSession.TenantId ?? 1, settings);
        }

        public async Task<List<WheelThemeForComboboxItemDto>> GetWheelThemeForCombobox(string selected)
        {
            var themes = await GetUiManagementSettings();

            var themesItems = new List<WheelThemeForComboboxItemDto>(themes.Select(x =>
                new WheelThemeForComboboxItemDto(x.Theme, x.Theme, false)));

            if (themesItems.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(selected))
                {
                    var selectedItem = themesItems.FirstOrDefault(x => x.Value == selected);
                    if (selectedItem != null)
                    {
                        selectedItem.IsSelected = true;
                    }
                    else
                    {
                        themesItems[0].IsSelected = true;
                    }
                }
            }

            return themesItems;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelTaxesAppService : DineConnectAppServiceBase, IWheelTaxesAppService
    {
        private readonly IRepository<WheelTax> _wheelTaxRepository;
        private readonly IWheelTaxesExcelExporter _wheelTaxesExcelExporter;
        private readonly IRepository<DinePlanTax, int> _dinePlanTaxRepository;

        public WheelTaxesAppService(IRepository<WheelTax> wheelTaxRepository, IWheelTaxesExcelExporter wheelTaxesExcelExporter,
            IRepository<DinePlanTax, int> dinePlanTaxRepository)
        {
            _wheelTaxRepository = wheelTaxRepository;
            _wheelTaxesExcelExporter = wheelTaxesExcelExporter;
            _dinePlanTaxRepository = dinePlanTaxRepository;
        }

        public async Task<PagedResultDto<GetWheelTaxForViewDto>> GetAll(GetAllWheelTaxesInput input)
        {
            var filteredWheelTaxes = _wheelTaxRepository.GetAll()
                        .Include(e => e.DinePlanTax)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.WheelTaxNameFilter), e => e.DinePlanTax != null && e.DinePlanTax.TaxName == input.WheelTaxNameFilter);

            var pagedAndFilteredWheelTaxes = filteredWheelTaxes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var wheelTaxes = from o in pagedAndFilteredWheelTaxes
                             join o1 in _dinePlanTaxRepository.GetAll() on o.DinePlanTaxId equals o1.Id into j1
                             from s1 in j1.DefaultIfEmpty()

                             select new GetWheelTaxForViewDto()
                             {
                                 WheelTax = new WheelTaxDto
                                 {
                                     Name = o.Name,
                                     Id = o.Id
                                 },
                                 WheelTaxName = s1 == null ? "" : s1.TaxName.ToString()
                             };

            var totalCount = await filteredWheelTaxes.CountAsync();

            return new PagedResultDto<GetWheelTaxForViewDto>(
                totalCount,
                await wheelTaxes.ToListAsync()
            );
        }

        public async Task<GetWheelTaxForViewDto> GetWheelTaxForView(int id)
        {
            var wheelTax = await _wheelTaxRepository.GetAsync(id);

            var output = new GetWheelTaxForViewDto { WheelTax = ObjectMapper.Map<WheelTaxDto>(wheelTax) };

            if (output.WheelTax.DinePlanTaxId != null)
            {
                var _lookupWheelTax = await _dinePlanTaxRepository.FirstOrDefaultAsync((int)output.WheelTax.DinePlanTaxId);
                output.WheelTaxName = _lookupWheelTax.TaxName;
            }

            return output;
        }

        
        public async Task<GetWheelTaxForEditOutput> GetWheelTaxForEdit(EntityDto input)
        {
            var wheelTax = await _wheelTaxRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWheelTaxForEditOutput { WheelTax = ObjectMapper.Map<CreateOrEditWheelTaxDto>(wheelTax) };

            if (output.WheelTax.DinePlanTaxId != null)
            {
                var _lookupWheelTax = await _dinePlanTaxRepository.FirstOrDefaultAsync((int)output.WheelTax.DinePlanTaxId);
                output.WheelTaxName = _lookupWheelTax.TaxName;
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWheelTaxDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        
        protected virtual async Task Create(CreateOrEditWheelTaxDto input)
        {
            var wheelTax = ObjectMapper.Map<WheelTax>(input);

            if (AbpSession.TenantId != null)
            {
                wheelTax.TenantId = (int)AbpSession.TenantId;
            }

            await _wheelTaxRepository.InsertAsync(wheelTax);
        }

        
        protected virtual async Task Update(CreateOrEditWheelTaxDto input)
        {
            var wheelTax = await _wheelTaxRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, wheelTax);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _wheelTaxRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetWheelTaxesToExcel(GetAllWheelTaxesForExcelInput input)
        {
            var filteredWheelTaxes = _wheelTaxRepository.GetAll()
                        .Include(e => e.DinePlanTax)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.WheelTaxNameFilter), e => e.DinePlanTax != null && e.DinePlanTax.TaxName == input.WheelTaxNameFilter);

            var query = (from o in filteredWheelTaxes
                         join o1 in _dinePlanTaxRepository.GetAll() on o.DinePlanTaxId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetWheelTaxForViewDto()
                         {
                             WheelTax = new WheelTaxDto
                             {
                                 Name = o.Name,
                                 Id = o.Id
                             },
                             WheelTaxName = s1 == null ? "" : s1.TaxName.ToString()
                         });

            var wheelTaxListDtos = await query.ToListAsync();

            return _wheelTaxesExcelExporter.ExportToFile(wheelTaxListDtos);
        }

        public async Task<List<WheelTaxDto>> GetAllWheelTaxes()
        {
            var taxes = await _wheelTaxRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelTaxDto>>(taxes);
        }

        public async Task<List<DinePlanTaxLookupDto>> GetAllDinePlanTaxForLookupTable()
        {
            var taxes = await _dinePlanTaxRepository.GetAll()
                .Select(tax => new DinePlanTaxLookupDto
                {
                    Id = tax.Id,
                    DisplayName = tax.TaxName
                }).ToListAsync();

            return taxes;
        }
    }
}
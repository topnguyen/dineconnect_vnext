﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.MultiTenancy.OneMap;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.V2;
using Elect.Location.Coordinate.DistanceUtils;
using Elect.Location.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Wheel.Impl.Cart
{
    [AbpAllowAnonymous]
    public class WheelCheckoutAppService : DineConnectAppServiceBase, IWheelCheckoutAppService
    {
        private readonly CustomerManager _customerManager;
        private readonly IRepository<DinePlanTaxLocation> _dinePlanTaxLocationRepository;
        private readonly IRepository<DinePlanTaxMapping> _dinePlanTaxMappingRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly OneMapGatewayManager _oneMapGatewayManager;
        private readonly IRepository<WheelCartItem> _repository;
        private readonly IRepository<WheelTable> _tableRepository;
        private readonly WheelAppService _wheelAppService;
        private readonly WheelCartManager _wheelCartManager;
        private readonly IRepository<WheelDeliveryDuration> _wheelDeliveryDurationRepository;
        private readonly IWheelDepartmentsAppService _wheelDepartmentsAppService;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;
        private readonly IRepository<WheelSetup> _wheelSetupRepository;
        private readonly IWheelSetupsAppService _wheelSetupsAppService;
        private readonly IRepository<WheelTable> _wheelTableRepository;
        private readonly IRepository<WheelTax> _wheelTaxRepository;
        private readonly IRepository<WheelTicket> _wheelTicketRepo;
        private readonly IRepository<WheelDeliveryZone> _zoneRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        private readonly List<string> _defaultPortionList = new List<string> { "NORMAL", "Normal", "normal" };

        public WheelCheckoutAppService(
            OneMapGatewayManager oneMapGatewayManager,
            WheelAppService wheelAppService,
            IRepository<WheelLocation> wheelLocationRepository,
            IRepository<WheelDeliveryDuration> wheelDeliveryDurationRepository
            , IRepository<DinePlanTaxMapping> dinePlanTaxMappingRepository
            , IRepository<DinePlanTaxLocation> dinePlanTaxLocationRepository
            , IRepository<WheelTax> wheelTaxRepository
            , IRepository<WheelDeliveryZone> zoneRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<MenuItem> menuItemRepository
            , CustomerManager customerManager
            , WheelCartManager wheelCartManager
            , IRepository<WheelSetup> wheelSetupRepository
            , IRepository<WheelCartItem> repository, IWheelDepartmentsAppService wheelDepartmentsAppService,
            IRepository<WheelTable> tableRepository, IWheelSetupsAppService wheelSetupsAppService,
            IRepository<WheelTable> wheelTableRepository, IRepository<WheelTicket> wheelTicketRepo)
        {
            _oneMapGatewayManager = oneMapGatewayManager;
            _wheelAppService = wheelAppService;
            _wheelLocationRepository = wheelLocationRepository;
            _wheelDeliveryDurationRepository = wheelDeliveryDurationRepository;
            _dinePlanTaxMappingRepository = dinePlanTaxMappingRepository;
            _dinePlanTaxLocationRepository = dinePlanTaxLocationRepository;
            _wheelTaxRepository = wheelTaxRepository;
            _zoneRepository = zoneRepository;
            _customerManager = customerManager;
            _wheelCartManager = wheelCartManager;
            _wheelSetupRepository = wheelSetupRepository;
            _repository = repository;
            _wheelDepartmentsAppService = wheelDepartmentsAppService;
            _tableRepository = tableRepository;
            _wheelSetupsAppService = wheelSetupsAppService;
            _wheelTableRepository = wheelTableRepository;
            _wheelTicketRepo = wheelTicketRepo;
            _menuItemRepository = menuItemRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<WheelGetCartOutputDto> GetCartItems(bool includeCartTotal = false)
        {
            using (_unitOfWorkManager.Current.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var model = new WheelGetCartOutputDto();
                var customer = await _customerManager.GetCurrentCustomerAsync();
                if (customer != null)
                {
                    var currentLocationStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_LOCATION);

                    var myConnectLocation = 0;
                    dynamic strLocation = JsonConvert.DeserializeObject(currentLocationStr);
                    if (strLocation != null) myConnectLocation = Convert.ToInt32(strLocation.locationId);
                    model.CustomerId = customer.Id;
                    var cartItems = await _wheelCartManager.GetCartItems(customer);
                    
                    if (cartItems != null && cartItems.Any())
                    {
                        var lastCartItem = cartItems.Last();
                        if (lastCartItem.CreationTime < Clock.Now.AddHours(-10))
                        {
                            await _wheelCartManager.ClearCartItems(customer);
                            return model;
                        }
                        var items = GetCartItemDto(cartItems, includeCartTotal);
                        model.CartItems = items;
                        if (includeCartTotal)
                        {
                            var itemTaxes = await GetCartTotalTax(items);

                            model.CartItems.ForEach(item =>
                            {
                                var tax = itemTaxes.FirstOrDefault(x => x.MenuItemId == item.MenuItemId);

                                if (tax != null)
                                {
                                    if (tax.TaxInclusive)
                                        item.Tax = item.SubTotal - item.SubTotal / (1 + tax.TaxPercentage / 100);
                                    else
                                        item.Tax = item.SubTotal * tax.TaxPercentage / 100;
                                }
                            });
                            
                            bool taxInclusive = true;

                            if (itemTaxes.Any())
                            {
                                var firstTax = itemTaxes.First();
                                if (firstTax != null)
                                {
                                    taxInclusive = firstTax.TaxInclusive;
                                }
                            }


                            model.CartTotal.SubTotal = model.CartItems.Sum(t => t.SubTotal);
                  
                            decimal taxSum = model.CartItems.Sum(t => t.Tax) ?? 0;
                            model.CartTotal.ItemsTaxTotal = decimal.Round(taxSum, 2, MidpointRounding.AwayFromZero);

                            var serviceFee = await GetCartServiceFee(model.CartTotal.SubTotal ?? 0, myConnectLocation);
                            
                            model.CartTotal.ServiceFeeTotal = serviceFee.serviceTotal;
                            model.CartTotal.ServiceFeeTaxTotal = decimal.Round(serviceFee.serviceTax, 2, MidpointRounding.AwayFromZero);

                            if (model.CartTotal != null)
                            {
                                if (model.CartTotal.SubTotal != null)
                                {
                                    model.CartTotal.TotalPrice += model.CartTotal.SubTotal.Value;
                                }

                                if (model.CartTotal.ServiceFeeTaxTotal != null)
                                {
                                    model.CartTotal.TotalPrice += model.CartTotal.ServiceFeeTaxTotal.Value;
                                }

                                if (model.CartTotal.ServiceFeeTaxTotal != null)
                                {
                                    model.CartTotal.TotalPrice += model.CartTotal.ServiceFeeTaxTotal.Value;
                                }

                                if (model.CartTotal.ItemsTaxTotal != null && !taxInclusive)
                                {
                                    model.CartTotal.TotalPrice += model.CartTotal.ItemsTaxTotal.Value;
                                }
                            }
                        }
                    }

                    //check if the QR Ordering then get previous items
                    var tableId =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_TABLE);
                    var currentDepartmentStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT);
                    currentLocationStr = await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.SELECTED_LOCATION);

                    var qrCode =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SCAN_QR_CODE);

                    if (!string.IsNullOrEmpty(tableId) && !string.IsNullOrEmpty(currentDepartmentStr) &&
                        !string.IsNullOrEmpty(currentLocationStr) && !string.IsNullOrEmpty(qrCode))
                    {
                        dynamic currentLocation = JsonConvert.DeserializeObject(currentLocationStr);
                        if (currentLocation != null)
                        {
                            int locationId = Convert.ToInt32(currentLocation.id);

                            var prevTickets = await _wheelTicketRepo
                                .GetAll()
                                .Where(t => t.CustomerId == customer.Id && t.TableId == Convert.ToInt32(tableId) &&
                                            t.WheelLocationId == locationId)
                                .OrderBy(t => t.CreationTime).ToListAsync();

                            model.OrderedItems = prevTickets.SelectMany(t =>
                                JsonConvert.DeserializeObject<List<WheelCartItemDto>>(t.MenuItemsDynamic)).ToList();
                        }
                    }
                }

                return model;
            }
        }

        private List<WheelCartItemDto> GetCartItemDto(List<WheelCartItem> cartItems ,bool cartTotal, int level = 0)
        {
            List<WheelCartItemDto> returnDto = new List<WheelCartItemDto>();
            foreach (var cartItem in cartItems)
            {
                if (cartItem != null)
                {
                    var itemDto = ObjectMapper.Map<WheelCartItemDto>(cartItem);
                    
                    if (itemDto != null )
                    {
                        if (level == 0)
                        {
                            if (cartItem.ScreenMenuItem != null)
                            {
                                dynamic dynamic = JsonConvert.DeserializeObject(cartItem.ScreenMenuItem.Dynamic);
                                if (dynamic != null)
                                {
                                    itemDto.ScreenMenuItem = new WheelScreenMenuItemSimpleDto()
                                    {
                                        Name = cartItem.ScreenMenuItem.Name,
                                        Image = dynamic.Image,
                                        ProductType = cartItem.ScreenMenuItem.MenuItemId.HasValue ? 1 : 2
                                    };
                                }
                            }
                        }
                        if (cartItem.MenuItemId!=null)
                        {
                            if (cartTotal)
                            {
                                if (cartItem.MenuItem == null)
                                {
                                    var menuItem = _menuItemRepository.Get(cartItem.MenuItemId.Value);
                                    if (menuItem != null)
                                    {
                                        cartItem.MenuItem = menuItem;
                                    }
                                }

                                if (cartItem.MenuItem != null)
                                {
                                    itemDto.UnitPrice = _wheelCartManager.GetUnitPrice(cartItem).Item1;
                                    itemDto.LineUnitPrice = _wheelCartManager.GetUnitPrice(cartItem).Item2;
                                }
                            }
                            else
                            {
                                itemDto.UnitPrice = _wheelCartManager.GetUnitPrice(cartItem).Item1;
                                itemDto.LineUnitPrice = _wheelCartManager.GetUnitPrice(cartItem).Item2;

                            }

                        }
                        if (!string.IsNullOrEmpty(cartItem.Tags) && !cartItem.Tags.Equals("[]"))
                        {
                            itemDto.Tags = JsonConvert.DeserializeObject<List<WheelCartItemOrderTagDto>>(cartItem.Tags);
                        }

                        if (itemDto.Portion != null && !string.IsNullOrEmpty(itemDto.Portion.Name))
                        {
                            if (_defaultPortionList.Contains(itemDto.Portion.Name))
                            {
                                itemDto.Portion.Name = "";
                            }
                        }

                        if (!string.IsNullOrEmpty(cartItem.ChildItems))
                        {
                            var myCartItems = JsonConvert.DeserializeObject<List<WheelCartItem>>(cartItem.ChildItems);
                            if (myCartItems != null && myCartItems.Any())
                            {
                                itemDto.SubItems = GetCartItemDto(myCartItems, cartTotal, 1);
                            }
                        }

                        returnDto.Add(itemDto);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            return returnDto;
        }

        public async Task<int> AddMenuItemToCart(AddItemToCartInputDto input)
        {
            try
            {
                if (AbpSession.TenantId != null)
                {
                    var customer = await _customerManager.GetCurrentCustomerAsync();

                    if (customer != null)
                    {
                        var cartItem = await _repository.GetAll()
                            .FirstOrDefaultAsync(itm =>
                                itm.CustomerId == customer.Id && itm.ScreenMenuItemId == input.ScreenMenuItemId &&
                                itm.Key == input.Key);

                        if (cartItem != null)
                        {
                            cartItem.Quantity = input.Quantity;
                            await _repository.UpdateAsync(cartItem);
                        }
                        else
                        {
                            List<WheelCartItem> allSubItems = new List<WheelCartItem>();
                            if (input.SubItems != null)
                            {
                                foreach (var addItemToCartInputDto in input.SubItems)
                                {
                                    List<WheelCartItem> allCartSubItems = new List<WheelCartItem>();
                                    if (addItemToCartInputDto.SubItems != null)
                                    {
                                        foreach (var insideCart in addItemToCartInputDto.SubItems)
                                        {
                                            var insideCartLocalItem = new WheelCartItem
                                            {
                                                CustomerId = customer.Id,
                                                ScreenMenuItemId = insideCart.ScreenMenuItemId,
                                                Key = insideCart.Key,
                                                Quantity = insideCart.Quantity,
                                                TenantId = AbpSession.TenantId.Value,
                                                Tags = insideCart.Tags != null
                                                    ? JsonConvert.SerializeObject(insideCart.Tags)
                                                    : null,
                                                Portion = insideCart.Portion != null
                                                    ? JsonConvert.SerializeObject(insideCart.Portion)
                                                    : null,
                                                Note = insideCart.Note,
                                                ChildItems = null,
                                                MenuItemId = insideCart.MenuItemId,
                                                ItemName = insideCart.ItemName,
                                                Price = insideCart.Price
                                            };
                                            allCartSubItems.Add(insideCartLocalItem);
                                        }
                                    }

                                    cartItem = new WheelCartItem
                                    {
                                        CustomerId = customer.Id,
                                        ScreenMenuItemId = addItemToCartInputDto.ScreenMenuItemId,
                                        Key = addItemToCartInputDto.Key,
                                        Quantity = addItemToCartInputDto.Quantity,
                                        TenantId = AbpSession.TenantId.Value,
                                        Tags = addItemToCartInputDto.Tags != null
                                            ? JsonConvert.SerializeObject(addItemToCartInputDto.Tags)
                                            : null,
                                        Portion = addItemToCartInputDto.Portion != null
                                            ? JsonConvert.SerializeObject(addItemToCartInputDto.Portion)
                                            : null,
                                        Note = addItemToCartInputDto.Note,
                                        ChildItems = JsonConvert.SerializeObject(allCartSubItems),
                                        MenuItemId = addItemToCartInputDto.MenuItemId,
                                        ItemName = addItemToCartInputDto.ItemName,
                                        Price = addItemToCartInputDto.Price

                                    };
                                    allSubItems.Add(cartItem);
                                }
                            }
                            cartItem = new WheelCartItem
                            {
                                CustomerId = customer.Id,
                                ScreenMenuItemId = input.ScreenMenuItemId,
                                Key = input.Key,
                                Quantity = input.Quantity,
                                TenantId = AbpSession.TenantId.Value,
                                Tags = input.Tags != null ? JsonConvert.SerializeObject(input.Tags) : null,
                                Portion = input.Portion != null ? JsonConvert.SerializeObject(input.Portion) : null,
                                Note = input.Note,
                                ChildItems = JsonConvert.SerializeObject(allSubItems),
                                MenuItemId = input.MenuItemId,
                                ItemName = input.ItemName,
                                Price = input.Price
                            };
                            await _repository.InsertAsync(cartItem);
                        }

                        await CurrentUnitOfWork.SaveChangesAsync();
                        var cartItemsCount = await _repository.CountAsync(itm => itm.CustomerId == customer.Id);
                        return cartItemsCount;
                    }
                }
                return 0;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task RemoveItemFromCart(int screenMenuItemId, string key)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();

            if (customer != null)
            {
                var cartItem = await _repository
                    .GetAll()
                    .FirstOrDefaultAsync(itm =>
                        itm.CustomerId == customer.Id && itm.ScreenMenuItemId == screenMenuItemId && itm.Key == key);

                if (cartItem != null) await _repository.DeleteAsync(cartItem);

                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        public async Task ResetCheckout()
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();
            await _wheelCartManager.ClearCartItems(customer);
            await _customerManager.ClearAllCustomerSessionData(customer.Id);
        }

        public async Task<bool> IsLocationSetup(int locationId)
        {
            var wheelSetup = await _wheelSetupRepository.FirstOrDefaultAsync(x => x.TenantId == AbpSession.TenantId);

            if (wheelSetup != null)
                if (!string.IsNullOrEmpty(wheelSetup.WheelLocationIds))
                {
                    var ids = wheelSetup.WheelLocationIds.Split(";", StringSplitOptions.RemoveEmptyEntries);

                    var wheelLocations = await _wheelLocationRepository.GetAll()
                        .Where(x => ids.Contains(x.Id.ToString())).ToListAsync();

                    return wheelLocations.Any(t => t.Id == locationId);
                }

            return false;
        }

        public async Task<CheckDeliveryZoneAndFeeOuput> IsLocationInDeliveryZone(int locationId)
        {
            var result = new CheckDeliveryZoneAndFeeOuput();

            var customer = await _customerManager.GetCurrentCustomerAsync();
            var postalCode = "";
            if (customer != null)
                postalCode =
                    await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.POSTAL_CODE);

            var location = await _wheelLocationRepository.FirstOrDefaultAsync(locationId);

            if (location == null) return null;

            result.AllowOutOfDeliveryZones = location.OutsideDeliveryZone;

            if (string.IsNullOrEmpty(postalCode))
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            if (string.IsNullOrEmpty(location.WheelDeliveryZoneIds))
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            var addressList = await _oneMapGatewayManager.GetAddressByPostCode(postalCode);

            var address = addressList.FirstOrDefault();

            if (address == null)
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            var longitude = Convert.ToDouble(address.Longitude);
            var latitude = Convert.ToDouble(address.Latitude);

            var output = await _wheelAppService.GetDeliveryZonesContainedPoint(longitude, latitude);
            var deliveryZoneIds = location.WheelDeliveryZoneIds.Split(";", StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            var locationDeliveryZones = output.Where(zone => deliveryZoneIds.Contains(zone.Id.ToString())).ToList();

            if (locationDeliveryZones.Any())
            {
                result.OutOfAllDeliveryZones = false;
                result.DeliveryFee = locationDeliveryZones.Min(x => x.DeliveryFee);
            }
            else
            {
                result.OutOfAllDeliveryZones = true;
                result.DeliveryFee = location.OutsideDeliveryZoneDeliveryFee;
                return result;
            }

            return result;
        }

        public async Task<decimal> CalculateDeliveryTime(double customerAddressLng, double customerAddressLat)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();
            var locationStr =
                await _customerManager.GetCustomerSessionData(customer.Id,
                    WheelCustomerSessionDataKeys.SELECTED_LOCATION);

            if (string.IsNullOrEmpty(locationStr)) return 0M;

            dynamic locationDy = JsonConvert.DeserializeObject(locationStr);
            if (locationDy != null)
            {
                int locationId = Convert.ToInt32(locationDy.id);

                var location = await _wheelLocationRepository.FirstOrDefaultAsync(x => x.Id == locationId);

                if (string.IsNullOrWhiteSpace(location.PickupAddressLong) || string.IsNullOrWhiteSpace(
                                                                              location.PickupAddressLat)
                                                                          || !double.TryParse(
                                                                              location.PickupAddressLong.Replace(".",
                                                                                  ","),
                                                                              out var pickupAddressLong) ||
                                                                          !double.TryParse(
                                                                              location.PickupAddressLat.Replace(".",
                                                                                  ","),
                                                                              out var pickupAddressLat)
                   )
                    return 0M;

                var storeCoordinate = new CoordinateModel(pickupAddressLong, pickupAddressLat);
                var customerCoordinate = new CoordinateModel(customerAddressLng, customerAddressLat);

                var distanceToCenterPoint = storeCoordinate.DistanceTo(customerCoordinate, UnitOfLengthModel.Kilometer);

                var deliveryDurations = await _wheelDeliveryDurationRepository.GetAll().ToListAsync();

                if (deliveryDurations.Count == 0) return 0M;

                var nearest = deliveryDurations
                    .OrderBy(x => Math.Abs(x.DeliveryDistance - (decimal)distanceToCenterPoint))
                    .First();
                var result = nearest.DeliveryTime;
                return result;
            }

            return 0M;
        }

        public async Task<CheckMinOrderByDeliveryZoneOutput> CheckMinOrderRequired()
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();

            var result = new CheckMinOrderByDeliveryZoneOutput();

            var locationStr =
                await _customerManager.GetCustomerSessionData(customer.Id,
                    WheelCustomerSessionDataKeys.SELECTED_LOCATION);

            if (!string.IsNullOrEmpty(locationStr))
            {
                dynamic locationDy = JsonConvert.DeserializeObject(locationStr);
                if (locationDy != null)
                {
                    int locationId = Convert.ToInt32(locationDy.id);

                    var postalCode =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.POSTAL_CODE);

                    var location = await _wheelLocationRepository.FirstOrDefaultAsync(Convert.ToInt32(locationId));

                    if (string.IsNullOrEmpty(location.WheelDeliveryZoneIds))
                    {
                        result.IsValid = true;
                        return result;
                    }

                    var addressList = await _oneMapGatewayManager.GetAddressByPostCode(postalCode);

                    var address = addressList.FirstOrDefault();

                    if (address == null)
                    {
                        result.IsValid = true;
                        return result;
                    }

                    var longitude = Convert.ToDouble(address.Longitude);
                    var latitude = Convert.ToDouble(address.Latitude);

                    var output = await _wheelAppService.GetDeliveryZonesContainedPoint(longitude, latitude);

                    var deliveryZoneIds = location.WheelDeliveryZoneIds
                        .Split(";", StringSplitOptions.RemoveEmptyEntries)
                        .ToList();

                    var locationDeliveryZones =
                        output.Where(zone => deliveryZoneIds.Contains(zone.Id.ToString())).ToList();
                    if (location.OutsideDeliveryZone)
                    {
                        var zone = await _zoneRepository.GetAll().Where(x => deliveryZoneIds.Contains(x.Id.ToString()))
                            .ToListAsync();

                        locationDeliveryZones = ObjectMapper.Map<List<WheelDeliveryZoneDto>>(zone);
                    }

                    if (locationDeliveryZones.Any())
                    {
                        var totalCartAmount =
                            await _wheelCartManager.GetCartSubTotal(await _customerManager.GetCurrentCustomerAsync());

                        result.MinimuOrderTotal = locationDeliveryZones.Min(x => x.MinimumTotalAmount);
                        result.IsValid = totalCartAmount >= result.MinimuOrderTotal;
                    }
                    else
                    {
                        result.IsValid = true;
                    }
                }
            }
            else
            {
                result.IsValid = true;
            }

            return result;
        }

        public async Task SaveCustomerSessionData(SaveCustomerSessionDataInput input)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();
            if (customer != null)
                await _customerManager.SaveCustomerSessionData(customer.Id, customer.TenantId, input.Key, input.Value);
        }

        public async Task<object> GetCustomerSessionData(string key)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();

            if (customer != null)
                return await _customerManager.GetCustomerSessionData(customer.Id, key);
            return new object();
        }

        public async Task<object> GetCustomerCheckoutData()
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();
            if (customer != null)
            {
                var tableId =
                    await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.SELECTED_TABLE);

                WheelTable table = null;
                if (!string.IsNullOrEmpty(tableId))
                    table = await _wheelTableRepository.FirstOrDefaultAsync(Convert.ToInt32(tableId));

                var myData = new
                {
                    Department = JsonConvert.DeserializeObject(
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT) ?? ""),
                    PostalCode =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.POSTAL_CODE),
                    Location = JsonConvert.DeserializeObject(
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_LOCATION) ?? ""),
                    Table = table,
                    PostalCodeOneMap = JsonConvert.DeserializeObject(
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.POSTAL_CODE_ONEMAP) ?? ""),
                    DateTimeDelivery =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.DATE_TIME_DELIVERY),
                    TimeSlotDelivery = JsonConvert.DeserializeObject(
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.TIME_SLOT_DELEVERY) ?? ""),
                    Language = await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.LANGUAGE_DISPLAY)
                };
                return myData;
            }

            return null;
        }

        public async Task<int> InitOrdering(WheeQROrderingInitInputDto input)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();

            if (!string.IsNullOrEmpty(input.SavedOTP))
                if (customer != null)
                {
                    var currentDepartmentStr = await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT);
                    var currentLocationStr = await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.SELECTED_LOCATION);
                    var currentTableIdStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_TABLE);

                    if (!string.IsNullOrEmpty(currentDepartmentStr) && !string.IsNullOrEmpty(currentLocationStr) &&
                        !string.IsNullOrEmpty(currentTableIdStr))
                    {
                        var currentDepartment =
                            JsonConvert.DeserializeObject<GetWheelDepartmentOutput>(currentDepartmentStr);
                        dynamic currentLocation = JsonConvert.DeserializeObject(currentLocationStr);

                        int.TryParse(currentTableIdStr, out var currentTableId);

                        if (currentLocation != null && currentDepartment.Id == input.DepartmentId &&
                            currentLocation.id == input.LocationId && currentTableId == input.TableId)
                        {
                            var wheelTable = await _wheelTableRepository
                                .GetAllIncluding(x => x.WheelLocation, x => x.WheelDepartment)
                                .Where(x => x.WheelLocationId == input.LocationId &&
                                            x.WheelDepartmentId == input.DepartmentId && x.Id == input.TableId)
                                .FirstOrDefaultAsync();

                            if (wheelTable != null && wheelTable.OtpCode == input.SavedOTP) return 3;
                        }
                    }
                }

            if (!string.IsNullOrEmpty(input.PhoneNumber))
            {
                if (customer != null)
                {
                    var currentDepartmentStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT);
                    var currentLocationStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_LOCATION);
                    var currentTableIdStr =
                        await _customerManager.GetCustomerSessionData(customer.Id,
                            WheelCustomerSessionDataKeys.SELECTED_TABLE);

                    if (!string.IsNullOrEmpty(currentDepartmentStr) && !string.IsNullOrEmpty(currentLocationStr) &&
                        !string.IsNullOrEmpty(currentTableIdStr))
                    {
                        var currentDepartment =
                            JsonConvert.DeserializeObject<GetWheelDepartmentOutput>(currentDepartmentStr);
                        dynamic currentLocation = JsonConvert.DeserializeObject(currentLocationStr);

                        int.TryParse(currentTableIdStr, out var currentTableId);

                        if (currentLocation != null && currentDepartment.Id == input.DepartmentId &&
                            currentLocation.id == input.LocationId && currentTableId == input.TableId) return 2;
                    }
                }
            }
            else
            {
                var departments = await _wheelDepartmentsAppService.GetWheelDepartments();
                var department = departments.FirstOrDefault(t => t.Id == input.DepartmentId);

                if (department == null || !department.Enable) return 0;

                var locations = await _wheelSetupsAppService.GetWheelSetupLocationsByTenant();

                var location = locations.Items.FirstOrDefault(t => t.Id == input.LocationId);

                if (location == null) return 0;

                var table = _tableRepository.FirstOrDefaultAsync(t => t.Id == input.TableId);

                if (table == null) return 0;

                var formatting = new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    },
                    Formatting = Formatting.Indented
                };

                if (customer != null)
                {
                    await _customerManager.SaveCustomerSessionData(customer.Id, customer.TenantId,
                        WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT,
                        JsonConvert.SerializeObject(department, formatting));
                    await _customerManager.SaveCustomerSessionData(customer.Id, customer.TenantId,
                        WheelCustomerSessionDataKeys.SELECTED_LOCATION,
                        JsonConvert.SerializeObject(location, formatting));
                    await _customerManager.SaveCustomerSessionData(customer.Id, customer.TenantId,
                        WheelCustomerSessionDataKeys.SELECTED_TABLE, input.TableId.ToString());
                    await _customerManager.SaveCustomerSessionData(customer.Id, customer.TenantId,
                        WheelCustomerSessionDataKeys.SCAN_QR_CODE, JsonConvert.SerializeObject(input, formatting));
                }

                return 1;
            }

            return 0;
        }

        private async Task<(decimal serviceTotal, decimal serviceTax)> GetCartServiceFee(decimal subTotalPrice,
            int locationId)
        {
            var customer = await _customerManager.GetCurrentCustomerAsync();

            dynamic department = JsonConvert.DeserializeObject(
                await _customerManager.GetCustomerSessionData(customer.Id,
                    WheelCustomerSessionDataKeys.SELECTED_DEPARTMENT));
            if (department != null)
            {
                List<WheelServiceFeeDto> serviceFees =
                    JsonConvert.DeserializeObject<List<WheelServiceFeeDto>>(
                        Convert.ToString(department.wheelServiceFees));

                var totalFee = 0m;
                var totalTax = 0m;

                if (serviceFees != null)
                    serviceFees.ForEach(item =>
                    {
                        var feeValue = item.FeeType == WheelServiceFeeType.Currency
                            ? Convert.ToDecimal(item.Value)
                            : subTotalPrice * (Convert.ToDecimal(item.Value) / 100);
                        totalFee += feeValue;

                        if (item.CalculateTax)
                            if (item.ServiceTaxes.Any() && item.ServiceTaxes.ContainsKey(locationId))
                            {
                                var feeTax = feeValue * item.ServiceTaxes[locationId] / 100;
                                totalTax += feeTax;
                            }
                    });
                return (totalFee, totalTax);
            }

            return (0, 0);
        }

        //TODO: Taxes has to be Reworked
        private async Task<List<TaxForMenuItemOutput>> GetCartTotalTax(List<WheelCartItemDto> cartItems)
        {
            
            var customer = await _customerManager.GetCurrentCustomerAsync();

            var output = new List<TaxForMenuItemOutput>();

            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var wheelTaxes = await _wheelTaxRepository.GetAll().Select(t => t.DinePlanTaxId).ToListAsync();
                var taxLocations = await _dinePlanTaxLocationRepository.GetAll()
                    .Where(t => wheelTaxes.Contains(t.DinePlanTaxRefId)).ToListAsync();

                var locationStr =
                    await _customerManager.GetCustomerSessionData(customer.Id,
                        WheelCustomerSessionDataKeys.SELECTED_LOCATION);

                if (!string.IsNullOrEmpty(locationStr))
                {
                    dynamic locationDy = JsonConvert.DeserializeObject(locationStr);
                    if (locationDy != null)
                    {
                        int locationId = Convert.ToInt32(locationDy.locationId);
                        bool taxInclusive = Convert.ToBoolean(locationDy.taxInclusive);

                        taxLocations = taxLocations
                            .Where(t => !string.IsNullOrEmpty(t.Locations) && JsonConvert
                                .DeserializeObject<List<SimpleLocationDto>>(t.Locations)
                                .Any(l => l.Id == Convert.ToInt32(locationId))).ToList();

                        foreach (var taxLocation in taxLocations)
                        {
                            foreach (var item in cartItems)
                            { 
                                output.Add(new TaxForMenuItemOutput
                                {
                                    MenuItemId = item.MenuItemId,
                                    TaxPercentage = taxLocation.TaxPercentage,
                                    TaxInclusive = taxInclusive
                                });

                                if (item.IsCombo && item.SubItems!=null && item.SubItems.Any())
                                {
                                    foreach (var wheelCartItemDto in item.SubItems)
                                    {
                                        output.Add(new TaxForMenuItemOutput
                                        {
                                            MenuItemId = wheelCartItemDto.MenuItemId,
                                            TaxPercentage = taxLocation.TaxPercentage,
                                            TaxInclusive = taxInclusive
                                        });

                                        if (wheelCartItemDto.IsCombo && item.SubItems.Any())
                                        {
                                            output.Add(new TaxForMenuItemOutput
                                            {
                                                MenuItemId = wheelCartItemDto.MenuItemId,
                                                TaxPercentage = taxLocation.TaxPercentage,
                                                TaxInclusive = taxInclusive
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            output = output.GroupBy(c => c.MenuItemId).Select(c => new TaxForMenuItemOutput
            {
                MenuItemId = c.Key, TaxPercentage = c.Any() ? c.First().TaxPercentage : 0,
                TaxInclusive = c.Any() && c.First().TaxInclusive
            }).ToList();

            return output;
        }
    }
}
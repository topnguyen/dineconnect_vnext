﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.MultiTenancy.OneMap;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Exporting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace DinePlan.DineConnect.Wheel.Impl
{
    
    public class WheelLocationsAppService : DineConnectAppServiceBase, IWheelLocationsAppService
    {
        private readonly IRepository<Location, int> _lookup_locationRepository;
        private readonly IRepository<ScreenMenu, int> _lookup_screenMenuRepository;
        private readonly IWheelAppService _wheelAppSerivce;
        private readonly IRepository<WheelDeliveryZone, int> _wheelDeliveryZoneRepository;
        private readonly IRepository<WheelDepartment, int> _wheelDepartmentRepository;
        private readonly IRepository<WheelLocation> _wheelLocationRepository;
        private readonly IWheelLocationsExcelExporter _wheelLocationsExcelExporter;
        private readonly IRepository<WheelServiceFee, int> _wheelServiceFeeRepository;
        private readonly IRepository<WheelTax, int> _wheelTaxRepository;
        private readonly IWheelDepartmentsAppService _wheelDepartmentsAppService;
        private readonly OneMapGatewayManager _oneMapGatewayManager;
        private readonly IRepository<DinePlanTaxLocation> _dinePlanTaxLocationRepository;

        public WheelLocationsAppService(IRepository<WheelLocation> wheelLocationRepository,
            IWheelLocationsExcelExporter wheelLocationsExcelExporter,
            IRepository<Location, int> lookup_locationRepository,
            IRepository<ScreenMenu, int> lookup_screenMenuRepository,
            IWheelAppService wheelAppService,
            IRepository<WheelDeliveryZone, int> wheelDeliveryZoneRepository,
            IRepository<WheelDepartment, int> wheelDepartmentRepository,
            IRepository<WheelServiceFee, int> wheelServiceFeeRepository,
            IRepository<WheelTax, int> wheelTaxRepository,
            IWheelDepartmentsAppService wheelDepartmentsAppService,
            OneMapGatewayManager oneMapGatewayManager,
            IRepository<DinePlanTaxLocation> dinePlanTaxLocationRepository)
        {
            _wheelLocationRepository = wheelLocationRepository;
            _wheelLocationsExcelExporter = wheelLocationsExcelExporter;
            _lookup_locationRepository = lookup_locationRepository;
            _lookup_screenMenuRepository = lookup_screenMenuRepository;
            _wheelAppSerivce = wheelAppService;
            _wheelDeliveryZoneRepository = wheelDeliveryZoneRepository;
            _wheelDepartmentRepository = wheelDepartmentRepository;
            _wheelServiceFeeRepository = wheelServiceFeeRepository;
            _wheelTaxRepository = wheelTaxRepository;
            _wheelDepartmentsAppService = wheelDepartmentsAppService;
            _oneMapGatewayManager = oneMapGatewayManager;
            _dinePlanTaxLocationRepository = dinePlanTaxLocationRepository;
        }

        public async Task<PagedResultDto<GetWheelLocationForViewDto>> GetAll(GetAllWheelLocationsInput input)
        {
            var query = _wheelLocationRepository.GetAll()
                .Include(e => e.Location)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                .WhereIf(input.EnabledFilter > -1,
                    e => input.EnabledFilter == 1 && e.Enabled || input.EnabledFilter == 0 && !e.Enabled)
                .WhereIf(input.TaxInclusiveFilter > -1,
                    e => input.TaxInclusiveFilter == 1 && e.TaxInclusive ||
                         input.TaxInclusiveFilter == 0 && !e.TaxInclusive)
                .WhereIf(input.OutsideDeliveryZoneFilter > -1,
                    e => input.OutsideDeliveryZoneFilter == 1 && e.OutsideDeliveryZone ||
                         input.OutsideDeliveryZoneFilter == 0 && !e.OutsideDeliveryZone)
                .WhereIf(!string.IsNullOrWhiteSpace(input.LocationNameFilter),
                    e => e.Location != null && e.Location.Name.Contains(input.LocationNameFilter));

            var pagedAndFilteredWheelLocations = query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var totalCount = await pagedAndFilteredWheelLocations.CountAsync();

            return new PagedResultDto<GetWheelLocationForViewDto>(
                totalCount,
                ObjectMapper.Map<List<GetWheelLocationForViewDto>>(await pagedAndFilteredWheelLocations.ToListAsync())
            );
        }

        [AbpAllowAnonymous]
        public async Task<GetWheelLocationForEditOutput> GetWheelLocationForEdit(EntityDto input)
        {
            var wheelLocation = await _wheelLocationRepository.GetAll()
                .Include(x => x.Location)
                .Where(x => x.Id == input.Id)
                .FirstOrDefaultAsync();

            var output = ObjectMapper.Map<GetWheelLocationForEditOutput>(wheelLocation);

            output.LocationName = wheelLocation.Location?.Name;

            var wheelDepartmentIds = wheelLocation.WheelDepartmentIds.Split(";");
            var wheelDepartmentDtos = new List<GetWheelDepartmentForViewDto>();

            var wheelDepartments = await _wheelDepartmentRepository.GetAll()
                .Where(x => wheelDepartmentIds.Contains(x.Id.ToString())).ToListAsync();
            foreach (var wheelDepartment in wheelDepartments)
            {
                var mapObject = ObjectMapper.Map<GetWheelDepartmentForViewDto>(wheelDepartment);
                if (string.IsNullOrEmpty(wheelDepartment.Dynamic) == false)
                {
                    dynamic itemDynamic = JObject.Parse(wheelDepartment.Dynamic);
                    mapObject.IsRequireOtp = itemDynamic.IsRequireOtp ?? false;
                }

                mapObject.WheelPaymentMethods =
                    await _wheelDepartmentsAppService.GetListPaymentMethodFromSystemNames(wheelDepartment.WheelPaymentMethodIds);
                wheelDepartmentDtos.Add(mapObject);
            }

            output.WheelDepartments = wheelDepartmentDtos;
            var wheelDeliveryZoneIds = wheelLocation.WheelDeliveryZoneIds.Split(";");
            output.WheelDeliveryZones = ObjectMapper.Map<List<WheelDeliveryZoneDto>>(await _wheelDeliveryZoneRepository
                .GetAll().Where(x => wheelDeliveryZoneIds.Contains(x.Id.ToString())).ToListAsync());

            var wheelServiceFeeIds = wheelLocation.WheelServiceFeeIds.Split(";");
            output.WheelServiceFees = ObjectMapper.Map<List<WheelServiceFeeDto>>(await _wheelServiceFeeRepository
                .GetAll().Where(x => wheelServiceFeeIds.Contains(x.Id.ToString())).ToListAsync());

            if (!wheelLocation.WheelTaxIds.IsNullOrEmpty())
            {
                var wheelTaxIds = wheelLocation.WheelTaxIds.Split(";");
                output.WheelTaxes = ObjectMapper.Map<List<WheelTaxDto>>(await _wheelTaxRepository
                    .GetAll().Where(x => wheelTaxIds.Contains(x.Id.ToString())).ToListAsync());

                foreach (var item in output.WheelTaxes)
                {
                    item.TaxPercent = (await _dinePlanTaxLocationRepository.GetAll().FirstOrDefaultAsync(x => item.DinePlanTaxId == x.DinePlanTaxRefId))?.TaxPercentage ?? decimal.Zero;
                }
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditWheelLocationDto input)
        {
            if (input.WheelDeliveryZones != null)
                input.WheelDeliveryZoneIds = string.Join(";", input.WheelDeliveryZones.Select(x => x.Id));
            else
                input.WheelDeliveryZoneIds = string.Empty;

            if (input.WheelDepartments != null)
                input.WheelDepartmentIds = string.Join(";", input.WheelDepartments.Select(x => x.Id));
            else
                input.WheelDepartmentIds = string.Empty;

            if (input.WheelServiceFees != null)
                input.WheelServiceFeeIds = string.Join(";", input.WheelServiceFees.Select(x => x.Id));
            else
                input.WheelServiceFeeIds = string.Empty;

            if (input.WheelTaxes != null)
                input.WheelTaxIds = string.Join(";", input.WheelTaxes.Select(x => x.Id));
            else
                input.WheelServiceFeeIds = string.Empty;

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        
        public async Task Delete(EntityDto input)
        {
            await _wheelLocationRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetWheelLocationsToExcel(GetAllWheelLocationsForExcelInput input)
        {
            var query = _wheelLocationRepository.GetAll()
                .Include(e => e.Location)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                .WhereIf(input.EnabledFilter > -1,
                    e => input.EnabledFilter == 1 && e.Enabled || input.EnabledFilter == 0 && !e.Enabled)
                .WhereIf(input.TaxInclusiveFilter > -1,
                    e => input.TaxInclusiveFilter == 1 && e.TaxInclusive ||
                         input.TaxInclusiveFilter == 0 && !e.TaxInclusive)
                .WhereIf(input.OutsideDeliveryZoneFilter > -1,
                    e => input.OutsideDeliveryZoneFilter == 1 && e.OutsideDeliveryZone ||
                         input.OutsideDeliveryZoneFilter == 0 && !e.OutsideDeliveryZone)
                .WhereIf(!string.IsNullOrWhiteSpace(input.LocationNameFilter),
                    e => e.Location != null && e.Location.Name == input.LocationNameFilter);

            var wheelLocationListDtos = await query.ToListAsync();

            return _wheelLocationsExcelExporter.ExportToFile(
                ObjectMapper.Map<List<GetWheelLocationForViewDto>>(wheelLocationListDtos));
        }

        
        public async Task<List<WheelLocationLookupTableDto>> GetAllLocationForTableDropdown()
        {
            return await _wheelLocationRepository.GetAll()
                .Select(location => new WheelLocationLookupTableDto
                {
                    Id = location.Id,
                    DisplayName = location.Name.ToString()
                }).ToListAsync();
        }

        
        public async Task<List<WheelLocationScreenMenuLookupTableDto>> GetAllScreenMenuForTableDropdown()
        {
            return await _lookup_screenMenuRepository.GetAll()
                .Select(screenMenu => new WheelLocationScreenMenuLookupTableDto
                {
                    Id = screenMenu.Id,
                    DisplayName = screenMenu.Name.ToString()
                }).ToListAsync();
        }

        
        protected virtual async Task Create(CreateOrEditWheelLocationDto input)
        {
            var wheelLocation = ObjectMapper.Map<WheelLocation>(input);

            if (AbpSession.TenantId != null) wheelLocation.TenantId = (int)AbpSession.TenantId;

            await _wheelLocationRepository.InsertAsync(wheelLocation);
        }

        
        protected virtual async Task Update(CreateOrEditWheelLocationDto input)
        {
            var wheelLocation = await _wheelLocationRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, wheelLocation);
        }

        
        public async Task<List<WheelLocationDto>> GetAllWheelLocations()
        {
            var locations = await _wheelLocationRepository.GetAllListAsync();
            return ObjectMapper.Map<List<WheelLocationDto>>(locations);
        }

        [AbpAllowAnonymous]
        public async Task<List<AddressByPostcode>> GetAddressByPostCode(string postcode)
        {
            var result = await _oneMapGatewayManager.GetAddressByPostCode(postcode);
            return result;
        }

        public async Task<List<WheelLocationForDeliveryZone>> GetAllWheelLocationForDeliveryZone()
        {
            var query = await _wheelLocationRepository.GetAll()
                    .Where(x => x.PickupAddressLat != null && x.PickupAddressLong != null)
                           .Select(x => new WheelLocationForDeliveryZone
                           {
                               Name = x.Name,
                               PickupAddress = x.PickupAddress,
                               PickupAddressLat = x.PickupAddressLat,
                               PickupAddressLong = x.PickupAddressLong
                           }).ToListAsync();
            return query;
        }
    }
}
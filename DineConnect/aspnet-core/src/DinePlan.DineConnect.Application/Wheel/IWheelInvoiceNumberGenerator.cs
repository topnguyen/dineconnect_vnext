using System.Threading.Tasks;
using Abp.Dependency;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}
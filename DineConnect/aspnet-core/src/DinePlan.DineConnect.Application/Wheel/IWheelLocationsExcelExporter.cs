﻿using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Wheel.Exporting
{
    public interface IWheelLocationsExcelExporter
    {
        FileDto ExportToFile(List<GetWheelLocationForViewDto> wheelLocations);
    }
}
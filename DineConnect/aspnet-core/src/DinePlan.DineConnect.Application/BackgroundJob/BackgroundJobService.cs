﻿using System;
using System.Data;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Threading;
using DinePlan.DineConnect.BackgroundJob.EmailNotification;
using DinePlan.DineConnect.ReportService;

namespace DinePlan.DineConnect.BackgroundJob
{
    public class BackgroundJobService : IBackgroundJobService
    {
        private readonly IBackgroundJobManager _backgroundJobManager;


        public BackgroundJobService(
            IBackgroundJobManager backgroundJobManager

            )
        {
            _backgroundJobManager = backgroundJobManager;

        }

        public async Task<string> SendMailTestAsync(string emailAddress)
        {
            await _backgroundJobManager.EnqueueAsync<BackgroundJobEmail, EmailSendingArgs>(
                new EmailSendingArgs
                {
                    EmailAddress = emailAddress,
                    Subject = "You've successfully registered!",
                    Body = "..."
                }
            );
            return "Add email job Ok";
        }
        public async Task<string> GenerateReport(string fromDate, string toDate)
        {
            await _backgroundJobManager.EnqueueAsync<BackgroundJobReport, GenerateReportArgs>(
                new GenerateReportArgs
                {
                    FromDate = fromDate,
                    ToDate = toDate,
                }
            );
            return "Add report job Ok";
        }
    }

    public class BackgroundJobEmail : AsyncBackgroundJob<EmailSendingArgs>, ITransientDependency
    {
        private readonly IEmailContentBackGroundJob _emailContentBackGroundJob;
        public BackgroundJobEmail(IEmailContentBackGroundJob emailNotificationAppService)
        {
            _emailContentBackGroundJob = emailNotificationAppService;
        }

        protected override async Task ExecuteAsync(EmailSendingArgs args)
        {
            await new EmailSender().SendAsync(args, _emailContentBackGroundJob);
        }
    }

    
    public class EmailSendingArgs
    {
        public string EmailAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
    public class EmailSender
    {
        public EmailSender()
        {
        }

        public async Task<string> SendAsync(EmailSendingArgs args, IEmailContentBackGroundJob emailContentBackGroundJob)
        {
            var str = await Task.FromResult("zxc");
            using (var sw = new System.IO.StreamWriter("D:/eDine/DineConnect_VNext/DineConnect/send_mail.txt", true))
            {
                await sw.WriteLineAsync($"{DateTime.Now:dd-MM-yyyy} - Send mail to {args.EmailAddress}");
            }

            AsyncHelper.RunSync(() => emailContentBackGroundJob.SendMailExample(args.EmailAddress, "Name"));
            return "Ok";

        }
    }

    public class GenerateReportArgs
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class BackgroundJobReport : AsyncBackgroundJob<GenerateReportArgs>, ITransientDependency
    {
        private readonly IReportService _reportService;
        public BackgroundJobReport(IReportService reportService)
        {
            _reportService = reportService;
        }

        protected override async Task ExecuteAsync(GenerateReportArgs args)
        {
            await new GenerateReporter().GenerateAsync(args, _reportService);
        }
    }

    public class GenerateReporter
    {
        public GenerateReporter()
        {
        }

        public Task<DataTable> GenerateAsync(GenerateReportArgs args, IReportService _reportService)
        {
            var dt = _reportService.WheelPayments(args.FromDate, args.ToDate);
            return dt;
        }
    }
}

﻿using System.Threading.Tasks;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Storage;

namespace DinePlan.DineConnect.BackgroundJob.EmailNotification
{
    public class EmailContentBackGroundJob : DineConnectAppServiceBase, IEmailContentBackGroundJob
    {
        private readonly ITenantEmailSendAppService _emailAppService;

        public EmailContentBackGroundJob(ITenantEmailSendAppService emailAppService,
            ITempFileCacheManager tempFileCacheManager)
        {
            _emailAppService = emailAppService;
        }

        public async Task SendMailExample(string customerEmail, string customerName)
        {
            await _emailAppService.SendAsyc(customerEmail, "Mail Subject", "<p>Mail Content</p>", true, 1);
        }
    }
}
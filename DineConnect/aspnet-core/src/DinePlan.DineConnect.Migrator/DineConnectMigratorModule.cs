using Abp.AspNetZeroCore;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.MicroKernel.Registration;
using Microsoft.Extensions.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.EntityFrameworkCore;
using DinePlan.DineConnect.Migrator.DependencyInjection;
using System;

namespace DinePlan.DineConnect.Migrator
{
    [DependsOn(typeof(DineConnectEntityFrameworkCoreModule))]
    public class DineConnectMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public DineConnectMigratorModule(DineConnectEntityFrameworkCoreModule abpZeroTemplateEntityFrameworkCoreModule)
        {
            abpZeroTemplateEntityFrameworkCoreModule.SkipDbSeed = true;

            var environment = Program.environment;

            _appConfiguration = AppConfigurations.Get(
                typeof(DineConnectMigratorModule).GetAssembly().GetDirectoryPathOrNull(), environment,
                addUserSecrets: true
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                DineConnectConsts.ConnectionStringName
                );
            Configuration.Modules.AspNetZero().LicenseCode = _appConfiguration["AbpZeroLicenseCode"];

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
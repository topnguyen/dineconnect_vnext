﻿using System;
using Abp;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Castle.Facilities.Logging;
using Abp.Castle.Logging.Log4Net;
using Microsoft.Extensions.Configuration;
using System.IO;
using Abp.Runtime.Security;
using Microsoft.Extensions.Hosting;

namespace DinePlan.DineConnect.Migrator
{
    public class Program
    {
        private static bool _skipConnVerification;

        public static string environment;

        public static void Main(string[] args)
        {
            

            //Server=tcp:dineconnect.database.windows.net,1433;Initial Catalog=DineConnect-Dev-Singapore;Persist Security Info=False;User ID=dineconnect_admin;Password=123@qwe!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=240;
            //Server=.\\SQLExpress02;Initial Catalog=DineConnect-Dev;Persist Security Info=False;User ID=sa;Password=ahghbfgfc;MultipleActiveResultSets=True;Connection Timeout=240
            
            /*
            var encryptCS = "ItrAN6ZJ+3pgQOouv1BWN6VMn0VZztWHUlEZFy+g6brSuO+Oy9xIZ9hBVjZOoT9WMdMshfQDuo12KkIDBvG+7vki9iwEGgyuIan8Dvpb/H8a8PX+/m1jyiaAMu+M892ixEUtkxR74Mmq8C0QZDXfcVbbx4WSo8hm8dbbzuqJp+Epqg7+RFGxIAHEYgGIg4AUhnZFFLUStb6teJI0xaQfqbAYw7qumIjcpiRofKUrgAbvopxmCswZ9CpMECF5XHDZLAlDE4s7VrlmgJ1u9/9JbTnTmeBI3ESnKjEhr6Z9UvUQmbgDTWE1LkrHQFap7tsanrgybSrjqfFgymSvnPJ7p8G65YcMRtQl9Zo0+I37KYU=";
            var myDecrypt = SimpleStringCipher.Instance.Decrypt(encryptCS);

            var decryptCS =
                @"Server=.\\SQLExpress02;Initial Catalog=DineConnect-Dev-Singapore;Persist Security Info=False;User ID=sa;Password=ahghbfgfc;MultipleActiveResultSets=True;Connection Timeout=240";
            var myEncrypt = SimpleStringCipher.Instance.Encrypt(decryptCS);
            */

            ParseArgs(args);

            bool.TryParse(Environment.GetEnvironmentVariable("ASPNETCORE_Docker_Enabled"), out bool isDockerEnabled);

            using (var bootstrapper = AbpBootstrapper.Create<DineConnectMigratorModule>())
            {
                bootstrapper.IocManager.IocContainer
                    .AddFacility<LoggingFacility>(f => f.UseAbpLog4Net()
                        .WithConfig("log4net.config")
                    );

                bootstrapper.Initialize();

                using (var migrateExecuter = bootstrapper.IocManager.ResolveAsDisposable<MultiTenantMigrateExecuter>())
                {
                    migrateExecuter.Object.Run(_skipConnVerification, isDockerEnabled);
                }

                if (_skipConnVerification || isDockerEnabled) return;

                Console.WriteLine("Press Enter to exit...");
                Console.ReadLine();
            }
        }

        private static void ParseArgs(string[] args)
        {
            if (args.IsNullOrEmpty())
            {
                return;
            }

            foreach (var arg in args)
            {
                if (arg == "-s")
                {
                    _skipConnVerification = true;
                }

                if (arg == "-prod")
                {
                    environment = "Production";
                }
            }
        }
    }
}

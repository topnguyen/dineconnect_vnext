﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DinePlan.DineConnect.Reserve.ReserveDto;

namespace DinePlan.DineConnect.ReserveAppConfig
{
    public interface IReserveAppService
    {
        Task SaveReserveTable(ReserveSeatMapInputOutputModel reserveTable);
        Task SaveReserveSeatMap(List<ReserveSeatMapInputOutputModel> reserveSeatMap);
        Task SaveReservationDetail(ReserveDetailInputModel reserveDetailInputModel);
        Task<List<ScheduleOutputModel>> GetReservation(DateTime dateBook);
        Task<List<ReserveSeatMapInputOutputModel>> GetSeatMapList();
        Task<ReservationOutput> GetReservationById(int reservationId);
        Task<SeatMapBackgroundUrlInputOutputModel> GetSeatMapBackgroundUrl();
        Task DeleteSeatMap();
        Task SaveSeatMapBackgroundUrl(SeatMapBackgroundUrlInputOutputModel request);
        Task<bool> CheckAvailabelBooking(DateTime dateBook, int people);
        Task<CustomerOutput> GetCustomerById(int id);
    }
}

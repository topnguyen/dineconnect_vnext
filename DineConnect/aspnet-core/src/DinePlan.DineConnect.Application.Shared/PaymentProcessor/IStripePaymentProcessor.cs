﻿using DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IStripePaymentProcessor : IPaymentProcessor<StripePaymentRequestDto, StripePaymentResponseDto>
    {
    }
}

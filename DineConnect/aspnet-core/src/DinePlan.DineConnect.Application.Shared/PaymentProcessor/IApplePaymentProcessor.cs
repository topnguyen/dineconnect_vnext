﻿using System.Threading;
using System.Threading.Tasks;
using DinePlan.DineConnect.PaymentProcessor.Dto.ApplePaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IApplePaymentProcessor : IPaymentProcessor<ApplePaymentRequestDto, ApplePaymentResponseDto>
    {
        Task<string> GetMerchantSessionAsync(string url, MerchantSessionRequest request, CancellationToken cancellationToken = default);

        Task<string> ValidationAsync(string url, CancellationToken cancellationToken = default);

        Task<MerchantIdentity> GetMerchantIdentityAsync(CancellationToken cancellationToken = default);
    }
}

﻿using DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface ISwipePaymentProcessor : IPaymentProcessor<SwipePaymentRequestDto, SwipePaymentResponseDto>
    {
    }
}

﻿using System.Threading.Tasks;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IOmisePaymentProcessor : IPaymentProcessor<OmisePaymentRequestDto, OmisePaymentResponseDto>
    {
        Task<OmisePaymentResponseDto> ProcessPaymentNow(OmisePaymentRequestDto input);

        Task<OmisePaymentResponseDto> GetStatus(string reference);

        Task<string> CreateOmisePaymentNowRequest(long amount, string currency);
    }
}

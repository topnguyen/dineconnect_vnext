﻿using DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IPaypalPaymentProcessor : IPaymentProcessor<PaypalPaymentRequestDto, PaypalPaymentResponseDto>
    {
    }
}

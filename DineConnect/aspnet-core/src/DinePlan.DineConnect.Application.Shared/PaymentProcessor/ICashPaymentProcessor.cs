﻿using DinePlan.DineConnect.PaymentProcessor.Dto.CashPaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface ICashPaymentProcessor : IPaymentProcessor<CashPaymentRequestDto, CashPaymentResponseDto>
    {
    }
}

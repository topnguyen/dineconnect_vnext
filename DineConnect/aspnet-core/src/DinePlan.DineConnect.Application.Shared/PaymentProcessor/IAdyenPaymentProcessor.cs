﻿using DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IAdyenPaymentProcessor : IPaymentProcessor<AdyenPaymentRequestDto, AdyenPaymentResponseDto>
    {
    }
}

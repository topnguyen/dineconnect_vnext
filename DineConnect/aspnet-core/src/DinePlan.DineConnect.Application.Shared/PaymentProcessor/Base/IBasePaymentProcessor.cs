﻿using DinePlan.DineConnect.PaymentProcessor.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IBasePaymentProcessor
    {
        Task<PaymentProcessorInfo> GetSetting();
        Task UpdateSetting(PaymentProcessorInfo input);

        string PaymentProcessorName { get; set; }

        //Task Install(DineConnectDbContext context, int tenantId);
    }
}

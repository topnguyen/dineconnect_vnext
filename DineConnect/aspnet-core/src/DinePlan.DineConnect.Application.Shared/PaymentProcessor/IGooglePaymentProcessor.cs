﻿using DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IGooglePaymentProcessor : IPaymentProcessor<GooglePaymentRequestDto, GooglePaymentResponseDto>
    {
    }
}

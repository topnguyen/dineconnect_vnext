﻿using System.Threading.Tasks;
using DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IHitPayPaymentProcessor : IPaymentProcessor<HitPayGetPaymentRequestDto, HitPayPaymentStatusResponse>
    {
        Task<HitPayCreatePaymentResponseDto> CreatePaymentRequest(HitPayPaymentRequestDto input);

        Task<HitPayPaymentStatusResponse> ProcessPayment(HitPayGetPaymentRequestDto input);
    }
}
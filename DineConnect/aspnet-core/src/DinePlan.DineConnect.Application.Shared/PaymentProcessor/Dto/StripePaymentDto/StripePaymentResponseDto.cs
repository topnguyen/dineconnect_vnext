﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto
{
    public class StripePaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.StripePaymentDto
{
    public class StripePaymentRequestDto
    {
        public string StripeToken { get; set; }
        
        public long Amount { get; set; }

        public string Currency { get; set; }
    }
}

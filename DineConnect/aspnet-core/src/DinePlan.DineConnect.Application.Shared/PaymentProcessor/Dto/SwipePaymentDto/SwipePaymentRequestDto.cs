﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto
{
    public class SwipePaymentRequestDto
    {
        public string CardNumber { get; set; }

        public string OTP { get; set; }

        public decimal Amount { get; set; }

        public string Note { get; set; }
    }
}

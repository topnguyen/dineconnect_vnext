﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.SwipePaymentDto
{
    public class SwipePaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto
{
    public class GooglePaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.GooglePaymentDto
{
    public class GooglePaymentRequestDto
    {
        public string GooglePayToken { get; set; }
        
        public long Amount { get; set; }

        public string Currency { get; set; }
    }
}

using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentStatusResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public object Phone { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("purpose")]
        public object Purpose { get; set; }

        [JsonProperty("reference_number")]
        public object ReferenceNumber { get; set; }

        [JsonProperty("payment_methods")]
        public string[] PaymentMethods { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("redirect_url")]
        public Uri RedirectUrl { get; set; }

        [JsonProperty("webhook")]
        public Uri Webhook { get; set; }

        [JsonProperty("send_sms")]
        public bool SendSms { get; set; }

        [JsonProperty("send_email")]
        public bool SendEmail { get; set; }

        [JsonProperty("sms_status")]
        public string SmsStatus { get; set; }

        [JsonProperty("email_status")]
        public string EmailStatus { get; set; }

        [JsonProperty("allow_repeated_payments")]
        public bool AllowRepeatedPayments { get; set; }

        [JsonProperty("expiry_date")]
        public object ExpiryDate { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("payments")]
        public HitPayPaymentDetailResponse[] Payments { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}
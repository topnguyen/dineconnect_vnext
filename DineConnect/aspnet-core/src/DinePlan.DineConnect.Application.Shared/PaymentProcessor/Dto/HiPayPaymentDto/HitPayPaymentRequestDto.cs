﻿using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentRequestDto
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("payment_methods")]
        public string[] PaymentMethods { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("purpose")]
        public string Purpose { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("reference_number")]
        public string ReferenceNumber { get; set; }

        [JsonProperty("redirect_url")]
        public string RedirectUrl { get; set; }

        [JsonProperty("webhook")]
        public string Webhook { get; set; }

        [JsonProperty("allow_repeated_payments")]
        public bool AllowRepeatedPayments { get; set; }

        [JsonProperty("expiry_date")]
        public DateTime? ExpiryDate { get; set; }
    }
}
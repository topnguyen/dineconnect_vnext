namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentMethodConst
    {
        public const string PaynowOnline = "paynow_online";
        public const string Card = "card";
        public const string Wechat = "wechat";
        public const string Alipay = "alipay";
        public const string GrabPay = "grabpay";
    }
}
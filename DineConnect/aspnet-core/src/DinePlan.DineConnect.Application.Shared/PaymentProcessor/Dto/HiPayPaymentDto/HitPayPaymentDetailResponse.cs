using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentDetailResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("buyer_name")]
        public object BuyerName { get; set; }

        [JsonProperty("buyer_phone")]
        public object BuyerPhone { get; set; }

        [JsonProperty("buyer_email")]
        public string BuyerEmail { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("payment_type")]
        public string PaymentType { get; set; }

        [JsonProperty("fees")]
        public string Fees { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}
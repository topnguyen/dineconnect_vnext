﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayGetPaymentRequestDto
    {
        public string PaymentRequestId { get; set; }
    }
}
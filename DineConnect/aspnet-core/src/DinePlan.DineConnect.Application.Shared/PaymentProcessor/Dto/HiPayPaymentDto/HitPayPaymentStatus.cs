﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto
{
    public class HitPayPaymentStatus
    {
        public const string Completed = "completed";
        public const string Expired = "expired";
        public const string Ending = "ending";
        public const string Failed = "failed";
    }
}
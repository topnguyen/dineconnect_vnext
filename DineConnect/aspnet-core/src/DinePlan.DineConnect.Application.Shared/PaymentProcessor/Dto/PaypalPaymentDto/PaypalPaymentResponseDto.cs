﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto
{
    public class PaypalPaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}

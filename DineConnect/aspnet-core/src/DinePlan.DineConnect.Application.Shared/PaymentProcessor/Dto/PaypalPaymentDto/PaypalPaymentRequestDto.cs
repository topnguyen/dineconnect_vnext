﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.PaypalPaymentDto
{
    public class PaypalPaymentRequestDto
    {
        public string OrderId { get; set; }
    }
}

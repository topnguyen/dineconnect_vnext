﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.AdyenPaymentDto
{
    public class AdyenPaymentRequestDto
    {
        public string EncryptedCardNumber { get; set; }
        
        public string EncryptedExpiryMonth { get; set; }
        
        public string EncryptedExpiryYear { get; set; }

        public string EncryptedSecurityCode { get; set; }

        public long Amount { get; set; }

        public string Currency { get; set; }

        public string ReturnUrl { get; set; }

    }
}

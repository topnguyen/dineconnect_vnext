﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto
{
    public class OmisePaymentResponseDto
    {
        public string PaymentId { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public string AuthorizeUri { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto
{
    public class OmisePaymentRequestDto
    {
        public string OmiseToken { get; set; }
        
        public long Amount { get; set; }

        public string Currency { get; set; }

        public string ReturnUrl { get; set; }
    }
}

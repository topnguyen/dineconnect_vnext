﻿namespace DinePlan.DineConnect.PaymentProcessor.Dto.ApplePaymentDto
{
    public class MerchantIdentity
    {
        public string StoreName { get; set; }

        public string Identity { get; set; }
    }
}

﻿using DinePlan.DineConnect.PaymentProcessor.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.PaymentProcessor
{
    public interface IPaymentProcessor
    {
        string PaymentProcessorName { get; set; }

        Task<PaymentProcessorInfo> GetSetting();

        Task UpdateSetting(PaymentProcessorInfo input);
    }

    public interface IPaymentProcessor<TProcessPaymentRequest, TProcessPaymentResponse> : IPaymentProcessor
        where TProcessPaymentRequest : class, new() 
        where TProcessPaymentResponse : class, new()
    {
        Task<TProcessPaymentResponse> ProcessPayment(TProcessPaymentRequest input);
    }
}

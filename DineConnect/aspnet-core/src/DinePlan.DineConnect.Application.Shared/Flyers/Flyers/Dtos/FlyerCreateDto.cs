using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Flyers.Flyers.Dtos
{
    public class FlyerCreateDto
    {
        [Required]
        public string Content { get; set; }

        [Required]
        public string Data { get; set; }
    }
}
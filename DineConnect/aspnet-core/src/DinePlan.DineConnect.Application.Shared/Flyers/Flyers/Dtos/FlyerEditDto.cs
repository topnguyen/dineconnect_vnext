using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Flyers.Flyers.Dtos
{
    public class FlyerEditDto : FlyerCreateDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }
}
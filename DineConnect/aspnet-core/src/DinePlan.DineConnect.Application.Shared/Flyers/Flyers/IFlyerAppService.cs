﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Flyers.Flyers.Dtos;

namespace DinePlan.DineConnect.Flyers.Flyers
{
    public interface IFlyerAppService : IApplicationService
    {
        Task<ListResultDto<FlyerListDto>> GetFlyers();

        Task<FlyerEditDto> GetFlyerForEdit(EntityDto input);

        Task CreateFlyer(FlyerCreateDto input);

        Task UpdateFlyer(FlyerEditDto input);

        Task DeleteFlyer(EntityDto input);
    }
}
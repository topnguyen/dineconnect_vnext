﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.MenuItemSchedules.Dtos
{
    public class ScreenMenuListInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MenuItemId";
            }
        }
    }
}

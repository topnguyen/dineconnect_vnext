﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.MenuItemSchedules.Dtos
{
    public class CreateOrUpdateScreenMenuCliqueInput
    {
        public int Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public string Days { get; set; }
        public int MenuItemId { get; set; }
    }
}

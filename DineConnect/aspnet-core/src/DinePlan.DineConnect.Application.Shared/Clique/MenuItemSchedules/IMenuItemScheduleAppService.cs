﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.MenuItemSchedules.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.MenuItemSchedules
{
    public interface IMenuItemScheduleAppService
    {
        Task<ListResultDto<ScreenMenuListDataDto>> GetMenuItemSchedule(ScreenMenuListInput input);
        ScreenMenuListDataDto GetMenuItemScheduleById(int menuItemSchedule);
        Task CreateOrUpdateMenuItemSchedule(CreateOrUpdateScreenMenuCliqueInput input);
        Task DeleteMenuItemSchedule(int menuItemSchedule);
    }
}

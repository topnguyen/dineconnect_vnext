﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueScreenMenu.Dtos
{
    public class CliqueScreenMenuDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public int CategoryCount { get; set; }
        public int RefLocationId { get; set; }
        public bool Group { get; set; }
    }
    public class CliqueScreenMenuInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }

        public int MenuId { get; set; }

        public int CategoryId { get; set; }
    }
}

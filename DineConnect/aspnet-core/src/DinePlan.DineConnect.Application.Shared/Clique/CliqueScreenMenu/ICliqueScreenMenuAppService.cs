﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueScreenMenu.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueScreenMenu
{
    public interface ICliqueScreenMenuAppService
    {
        Task<PagedResultDto<CliqueScreenMenuDto>> GetAll(CliqueScreenMenuInput input);
    }
}

﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueMealTimes.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueMealTimes
{
    public interface ICliqueMealTimeAppService
    {
        Task<ListResultDto<CliqueMealTimeListDto>> GetCliqueMealTime(CliqueMealTimeInput input);
        CliqueMealTimeListDto GetCliqueMealTimeById(int cliqueMealTimeId);
        Task CreateOrUpdateCliqueMealTime(CreateOrUpdateCliqueMealTimeInput input);
        Task DeleteCliqueMealTime(int cliqueMealTimeId);
        Task<List<CliqueMealTimeListDto>> GetCliqueMealTimeDropdown();
    }
}

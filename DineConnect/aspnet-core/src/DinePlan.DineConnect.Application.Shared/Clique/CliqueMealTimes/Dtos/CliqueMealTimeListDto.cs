﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMealTimes.Dtos
{
    public class CliqueMealTimeListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SortOrder { get; set; }
        public DateTime CreationTime { get; set; }
    }
}

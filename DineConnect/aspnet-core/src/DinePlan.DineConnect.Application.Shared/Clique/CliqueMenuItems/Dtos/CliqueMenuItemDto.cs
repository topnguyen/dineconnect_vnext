﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos
{
    public class CliqueMenuItemDto
    {
        public int? MenuItemId { get; set; }
        public int? ScreenMenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public int? Mnt { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string AllergicInfo { get; set; }
        public decimal Price { get; set; }
        public double FeedbackPoint { get; set; }
        public int FeedBackCount { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsAddToCart { get; set; }
        public string Dynamic { get; set; }
        public string Description { get; set; }
        public int LocationId { get; set; }
        public int? ScreenMenuCategoryId { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string CliqueMealTimeName { get; set; }
        public int Quantity { get; set; }
        public string OrderDateStr { get; set; }
        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public int Order { get; set; }
        public List<CliqueOrderTagDto> OrderTags { get; set; }
    }
}

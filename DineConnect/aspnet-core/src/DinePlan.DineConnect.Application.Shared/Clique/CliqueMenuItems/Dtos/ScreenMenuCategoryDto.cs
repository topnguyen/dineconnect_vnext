﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos
{
    public class ScreenMenuCategoryDto
    {
        public int ScreenMenuCategoryId { get; set; }
        public int ScreenMenuId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int LocationId { get; set; }
        public bool IsSelected { get; set; }
        public int Order { get; set; }
    }
}

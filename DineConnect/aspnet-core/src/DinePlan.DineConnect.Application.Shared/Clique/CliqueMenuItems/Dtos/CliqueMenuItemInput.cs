﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos
{
    public class CliqueMenuItemInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int LocationId { get; set; }
        public int? ScreenMenuCategoryId { get; set; }
        public int? ScreenMenuId { get; set; }
        public int? CustomerId { get; set; }
        public string Days { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string DayOrder { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class CliqueMenuItemFavoriteInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? CustomerId { get; set; }
        public string Days { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string DayOrder { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}

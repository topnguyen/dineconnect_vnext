﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos
{
    public class CliqueMealTimeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SortOrder { get; set; }
        public bool IsSelected { get; set; }
    }
}

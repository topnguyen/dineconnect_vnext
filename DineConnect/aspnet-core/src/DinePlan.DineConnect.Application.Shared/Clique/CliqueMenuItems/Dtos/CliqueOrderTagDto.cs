﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos
{
    public class CliqueOrderTagDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public int orderTagGroupId { get; set; }
    }
}

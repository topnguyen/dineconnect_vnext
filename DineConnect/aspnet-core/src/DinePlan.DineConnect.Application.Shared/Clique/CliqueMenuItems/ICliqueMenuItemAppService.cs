﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueMenuItems.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueMenuItems
{
    public interface ICliqueMenuItemAppService : IApplicationService
    {
        ListResultDto<CliqueMealTimeDto> GetCliqueMealTime();

        ListResultDto<ScreenMenuCategoryDto> GetScreenMenuCategory(int CliqueMealTimeId, string Days, string DayOrder);

        PagedResultDto<CliqueMenuItemDto> GetListMenuItemSchudule(CliqueMenuItemInput input);

        PagedResultDto<CliqueMenuItemDto> GetListMenuItemFavorite(CliqueMenuItemFavoriteInput input);
    }
}

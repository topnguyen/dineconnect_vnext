﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueMenus.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueMenus
{
    public interface ICliqueMenuAppService
    {
        Task<ListResultDto<CliqueMenuDto>> GetCliqueMenu(CliqueMenuInput input);
        CliqueMenuDto GetCliqueMenuById(int cliqueMenuId);
        Task CreateOrUpdateCliqueMenu(CreateOrUpdateCliqueMenuInput input);
        Task DeleteCliqueMenu(int cliqueMenuId);
        Task<List<ScreenMenuListDto>> GetScreenMenuDropdown();
    }
}

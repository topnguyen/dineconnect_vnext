﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenus.Dtos
{
    public class CreateOrUpdateCliqueMenuInput
    {
        public int Id { get; set; }
        public bool FixedMenu { get; set; }
        public bool AllDay { get; set; }
        public string Days { get; set; }
        public int ScreenMenuId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? CutOffDate { get; set; }
        public int LocationId { get; set; }
        public int PublishStatus { get; set; }
        public int CliqueMealTimeId { get; set; }
    }
}

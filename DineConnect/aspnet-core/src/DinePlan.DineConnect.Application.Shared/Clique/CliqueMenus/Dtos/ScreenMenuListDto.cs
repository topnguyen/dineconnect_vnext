﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenus.Dtos
{
    public class ScreenMenuListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

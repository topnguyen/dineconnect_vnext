﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenus.Dtos
{
    public class CliqueMenuInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public bool? FixedMenu { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ScreenMenuId";
            }
        }
    }
}

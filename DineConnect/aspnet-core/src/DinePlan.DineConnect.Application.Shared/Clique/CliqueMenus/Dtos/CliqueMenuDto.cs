﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueMenus.Dtos
{
    public class CliqueMenuDto
    {
        public int Id { get; set; }
        public bool FixedMenu { get; set; }
        public bool AllDay { get; set; }
        public string Days { get; set; }
        public int ScreenMenuId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? CutOffDate { get; set; }
        public int LocationId { get; set; }
        public int PublishStatus { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string CliqueMealTimeName { get; set; }
        public string ScreenMenuName { get; set; }
        public string LocationName { get; set; }
        public bool Monday { get; set; } = false;
        public bool Tuesday { get; set; } = false;
        public bool Wednesday { get; set; } = false;
        public bool Thursday { get; set; } = false;
        public bool Friday { get; set; } = false;
        public bool Saturday { get; set; } = false;
        public bool Sunday { get; set; } = false;
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.Payment.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.Payment
{
    public interface ICliquePaymentAppService : IApplicationService
    {
        Task<CliquePaymentDto> PaymentOrder(CliquePaymentInput input);
        Task<ListResultDto<CliquePaymentMethodDto>> GetPaymentMethod(CliquePaymentMethodInput input);
        Task<CliquePaymentDto> CreatePaymentForOrder(CreateOrderCliquePaymentInput input);

        Task ValidateCutOff(List<DateTime> orderDate);
    }
}

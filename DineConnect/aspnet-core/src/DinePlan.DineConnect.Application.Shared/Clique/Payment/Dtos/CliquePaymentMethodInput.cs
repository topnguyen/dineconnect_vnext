﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static DinePlan.DineConnect.Wheel.WheelPaymentMethodConsts;

namespace DinePlan.DineConnect.Clique.Payment.Dtos
{
    public class CliquePaymentMethodInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string SystemNameFilter { get; set; }
        public string FriendlyNameFilter { get; set; }
        public RecurringPaymentType? RecurringSupportFilter { get; set; }
        public int SupportsCaptureFilter { get; set; }
        public int RefundFilter { get; set; }
        public int PartialRefundFilter { get; set; }
        public int VoidFilter { get; set; }
        public int ActiveFilter { get; set; }
    }
}

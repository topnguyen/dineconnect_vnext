﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Payment.Dtos
{
    public class CliquePaymentInput
    {
        public int CliqueOrderId { get; set; }

        public decimal Total { get; set; }

        public int CustomerId { get; set; }

        public string PaymentMethod { get; set; }
        public int CardId { get; set; }
    }
}

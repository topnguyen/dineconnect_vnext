﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Payment.Dtos
{
    public class CreateOrderCliquePaymentInput
    {
        public string PaymentOption { get; set; }
        public decimal PaidAmount { get; set; }
        public int CliqueOrderId { get; set; }
        public int CustomerId { get; set; }
        public string CallbackUrl { get; set; }
        // Omise
        public string OmiseToken { get; set; }
        public string Source { get; set; }
        // Adyen
        public string AdyenEncryptedCardNumber { get; set; }
        public string AdyenEncryptedExpiryMonth { get; set; }
        public string AdyenEncryptedExpiryYear { get; set; }
        public string AdyenEncryptedSecurityCode { get; set; }
        // Paypal
        public string PaypalOrderId { get; set; }
        // Stripe
        public string StripeToken { get; set; }
        // Google
        public string GooglePayToken { get; set; }
        public int CardId { get; set; }
    }
}

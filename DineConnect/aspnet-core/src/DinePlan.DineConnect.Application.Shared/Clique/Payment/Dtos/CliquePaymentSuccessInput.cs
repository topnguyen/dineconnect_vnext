﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Payment.Dtos
{
    public class CliquePaymentSuccessInput
    {
        public int PaymentId { get; set; }

        public int CustomerId { get; set; }
    }
}

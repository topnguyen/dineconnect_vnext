﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Payment.Dtos
{
    public class CliquePaymentDto
    {
        public decimal Total { get; set; }

        public string PaymentMethod { get; set; }

        public DateTime? PaidTime { get; set; }

        public string DatePaidTimeStr { get; set; }

        public string TimePaidTimeStr { get; set; }

        public int CliqueOrderId { get; set; }
    }
}

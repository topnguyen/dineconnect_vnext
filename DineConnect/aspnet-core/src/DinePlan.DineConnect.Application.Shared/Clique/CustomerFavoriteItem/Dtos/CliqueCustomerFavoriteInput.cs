﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CustomerFavoriteItem.Dtos
{
    public class CliqueCustomerFavoriteInput
    {
        public int CustomerId { get; set; }

        public int MenuItemId { get; set; }

        public bool IsFavorite { get; set; }

        public int ScreenMenuItemId { get; set; }
    }
}

﻿using Abp.Application.Services;
using DinePlan.DineConnect.Clique.CustomerFavoriteItem.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CustomerFavoriteItem
{
    public interface ICliqueCustomerFavoriteItemAppService : IApplicationService
    {
        Task CreateOrUpdateCliqueCustomerFavoriteItem(CliqueCustomerFavoriteInput input);
    }
}

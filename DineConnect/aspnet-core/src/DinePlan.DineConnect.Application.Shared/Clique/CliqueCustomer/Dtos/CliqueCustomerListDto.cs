﻿using DinePlan.DineConnect.Tiffins.Customer.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueCustomer.Dtos
{
    public class CliqueCustomerListDto : CustomerListDto
    {
        public decimal Balance { get; set; }
    }
}

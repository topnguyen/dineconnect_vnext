﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueCustomer.Dtos
{
    public class CliqueCustomerDto
    {
        public int CustomerId { get; set; }
        public string UserName { get; set; }
        public string CustomerName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueCustomer.Dtos
{
    public class CliqueCustomerLocationInput
    {
        public int UserId { get; set; }
        public int CountryId { get; set; }
    }
}

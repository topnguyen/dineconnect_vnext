﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueCustomer.Dtos
{
    public class CliqueCustomerPasswordInput
    {
        //public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueCustomer.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueCustomer
{
    public interface ICliqueCustomerAppService
    {
        Task<CliqueCustomerDto> GetCustomerInfomation(int CustomerId);

        Task UpdateCustomerInfomation(CliqueCustomerInput input);

        Task CliqueCustomerChangePassword(CliqueCustomerPasswordInput input);

        Task UpdateLocationCustomer(CliqueCustomerLocationInput input);

        Task<PagedResultDto<CliqueCustomerListDto>> GetAllCustomer(CliqueCustomerListInput input);
    }
}

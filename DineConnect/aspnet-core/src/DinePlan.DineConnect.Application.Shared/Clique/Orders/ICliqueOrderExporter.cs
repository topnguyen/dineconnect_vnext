﻿using DinePlan.DineConnect.Clique.Orders.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders
{
    public interface ICliqueOrderExporter
    {
        FileDto PrintOrderDetail(CliqueOrderDetailForPrintDto input);
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.Orders.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.Orders
{
    public interface ICliqueOrderAppService : IApplicationService
    {
        Task CreateOrUpdateCliqueOrder(CliqueOrderInput input);

        CliqueOrderDto GetCliqueOrderCustomer(int CustomerId);

        Task<ListResultDto<CliqueOrderHistoryDto>> GetCliqueOrderForApp(string customerName);

        Task<ListResultDto<CliqueOrderHistoryDto>> GetCliqueOrderHistory(CliqueOrderHistoryInput input);

        Task<CliqueOrderHistoryDto> GetCliqueOrderHistoryDetail(int cliqueOrderId);

        Task UpdateStatusCliqueOrder(CliqueOrderStatusInput input);

        Task CopyCliqueOrder(int cliqueOrderId);

        Task<PagedResultDto<CliqueOrderHistoryDto>> GetCustomerCliqueOrder(CliqueOrderHistoryAdminInput input);

        FileDto PrintOrderDetail(CliqueOrderDetailForPrintDto input);

        Task DeleteCliqueOrderDetailById(int cliqueOrderDetailId);
        Task DeleteCliqueOrderById(int cliqueOrderId);
    }
}

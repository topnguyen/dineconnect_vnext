﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderHistoryDto
    {
        public string Name { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public CliqueOrderHistoryStatus CliqueOrderStatus { get; set; }
        public string OrderStatus { get; set; }
        public DateTime OrderTimeFor { get; set; }
        public string OrderTimeForStr { get; set; }
        public string OrderTimeForDetailStr { get; set; }
        public string OrderDateForDetailMobileStr { get; set; }
        public string OrderTimeForDetailMobileStr { get; set; }
        public string OrderTimeStr { get; set; }
        public decimal Total { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime? PaidTime { get; set; }
        public string PaidTimeStr { get; set; }
        public string Image { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string CliqueMealTimeName { get; set; }
        public int CliqueOrderId { get; set; }
        public List<CliqueProductOrderDto> ProductOrders { get; set; }
    }

    public enum CliqueOrderHistoryStatus
    {
        InQueue = 0,
        Complete = 1
    }
}

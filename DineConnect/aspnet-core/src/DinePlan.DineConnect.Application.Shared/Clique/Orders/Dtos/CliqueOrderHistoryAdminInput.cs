﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderHistoryAdminInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string customerName { get; set; }
        public DateTime? orderDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CliqueMealTimeName";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderHistoryInput
    {
        public int CustomerId { get; set; }

        public string SortField { get; set; }

        public SortResult SortResult { get; set; }

        public int? OrderStatus { get; set; }
    }

    public enum SortResult
    {
        ASC = 0,
        DESC = 1
    }
}

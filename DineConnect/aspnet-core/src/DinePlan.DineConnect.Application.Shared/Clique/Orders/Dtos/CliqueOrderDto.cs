﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderDto
    {
        public List<CliqueProductOrderDto> ProductOrders { get; set; }

        public decimal Total { get; set; }

        public int CustomerId { get; set; }

        public int CountItems { get; set; }

        public int CliqueOrderId { get; set; }
    }
}
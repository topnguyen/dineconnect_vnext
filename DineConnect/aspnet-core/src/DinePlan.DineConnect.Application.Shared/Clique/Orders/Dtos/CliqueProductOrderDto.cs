﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueProductOrderDto
    {
        public int CliqueOrderDetailId { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public string OrderTags { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; } 
        public DateTime OrderDate { get; set; }
        public string OrderDateStr { get; set; }
        public int CliqueMealTimeId { get; set; }
        public string CliqueMealTimeName { get; set; }
        public string Name { get; set; }
        public int ScreenMenuItemId { get; set; }
        public int LocationId { get; set; }
        public string Image { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderStatusInput
    {
        public int CliqueOrderId;
        public int OrderStatusId;
        public int CustomerId;
    }
}

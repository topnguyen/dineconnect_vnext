﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderInput
    {
        public int CustomerId { get; set; }

        public int MenuItemId { get; set; }

        public int ScreenMenuItemId { get; set; }

        public int CliqueMealTimeId { get; set; }

        public int MenuItemPortionId { get; set; }

        public string OrderTags { get; set; } 

        public int Quantity { get; set; } = 1;

        public decimal Price { get; set; }

        public DateTime OrderDate { get; set; }

        public string OrderDateStr { get; set; }

        public bool IsAddToCart { get; set; }

        public int LocationId { get; set; }
    }

    public class CliqueOrderMenuInput
    {
        public int MenuItemId { get; set; }

        public int ScreenMenuItemId { get; set; }

        public int CliqueMealTimeId { get; set; }

        public int MenuItemPortionId { get; set; }

        public string OrderTags { get; set; } 

        public int Quantity { get; set; } = 1;

        public DateTime OrderDate { get; set; }

        public decimal Price { get; set; }
    }
}

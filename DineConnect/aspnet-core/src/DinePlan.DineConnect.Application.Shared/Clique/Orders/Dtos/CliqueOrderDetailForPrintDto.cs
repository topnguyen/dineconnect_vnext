﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.Orders.Dtos
{
    public class CliqueOrderDetailForPrintDto
    {
        public string MealTime { get; set; }
        public DateTime DateOrder { get; set; }
        public decimal Total { get; set; }
        public List<CliqueProductOrderDto> ProductOrders { get; set; }
    }
}

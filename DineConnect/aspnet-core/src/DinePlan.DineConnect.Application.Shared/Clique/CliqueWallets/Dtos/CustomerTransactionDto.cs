﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerTransactionDto
    {
        public int TransactionId { get; set; }
        public int TransactionType { get; set; }
        public string Description { get; set; }
        public string TransactionTime { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public int TransactionStatus { get; set; }
    }
}

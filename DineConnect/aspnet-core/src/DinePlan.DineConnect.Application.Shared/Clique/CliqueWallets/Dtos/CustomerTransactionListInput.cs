﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerTransactionListInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public int? customerId { get; set; }
        public string paymentMethod { get; set; }
        public int? dayExpiry { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CustomerName";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerTopupInput
    {
        public int CardId { get; set; }
        public decimal Money { get; set; }
        public int CustomerId { get; set; }
        public string PaymentType { get; set; }
        public string OmiseToken { get; set; }
        public string CallbackUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerTransferInput
    {
        public int CardId { get; set; }
        public decimal Money { get; set; }
        public int FromCustomerId { get; set; }
        public string FromCustomerName { get; set; }
        public int ToCustomerId { get; set; }
        public string ToCustomerName { get; set; }
        public int ToCustomerCardId { get; set; }
    }
}

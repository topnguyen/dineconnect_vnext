﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerForTransferDto
    {
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public int CustomerCardId { get; set; }
        public bool isSelected { get; set; }
    }
}

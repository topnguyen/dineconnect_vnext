﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerWalletDto
    {
        public int CardId { get; set; }
        public int CustomerId { get; set; }
        public decimal Balance { get; set; }
        public string CardNumber { get; set; }
    }
}

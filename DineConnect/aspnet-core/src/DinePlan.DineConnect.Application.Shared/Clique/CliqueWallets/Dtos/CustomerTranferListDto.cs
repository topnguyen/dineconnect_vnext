﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Clique.CliqueWallets.Dtos
{
    public class CustomerTranferListDto
    {
        public int TransactionId { get; set; }
        public int CardId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public DateTime TransactionTime { get; set; }
        public string Description { get; set; }
        public decimal? Credit { get; set; }
        public decimal? Debit { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string PaymentType { get; set; }
        public int TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Clique.CliqueWallets.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Clique.CliqueWallets
{
    public interface ICliqueWalletAppService
    {
        CustomerWalletDto GetCustomerWallet(int customerId);
        List<CustomerTransactionDto> GetCustomerTransaction(int customerId);
        Task ToptupRecharge(CustomerTopupInput input);
        Task CustomerTransferTopup(CustomerTransferInput input);
        List<CustomerForTransferDto> GetCustomerForTranfer(string filter);
        PagedResultDto<CustomerTranferListDto> GetCustomerTransactionAdmin(CustomerTransactionListInput input);
    }
}
 
﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace DinePlan.DineConnect.Authorization.Accounts.Dto
{
    public class SendPasswordResetCodeInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public string Domain { get; set; }
    }
}
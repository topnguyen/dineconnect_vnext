﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}

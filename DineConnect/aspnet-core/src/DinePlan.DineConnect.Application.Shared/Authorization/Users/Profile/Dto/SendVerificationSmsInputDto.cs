namespace DinePlan.DineConnect.Authorization.Users.Profile.Dto
{
    public class SendVerificationSmsInputDto
    {
        public string PhoneNumber { get; set; }
    }
}
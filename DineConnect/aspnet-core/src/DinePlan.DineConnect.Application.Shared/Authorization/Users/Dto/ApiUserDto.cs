﻿using DinePlan.DineConnect.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    public class ApiUserDto : IOutputDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
    }
}

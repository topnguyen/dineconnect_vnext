﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}

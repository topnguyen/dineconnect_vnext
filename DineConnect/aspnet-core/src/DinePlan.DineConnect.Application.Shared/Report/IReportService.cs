using Abp.Application.Services;
using System.Data;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.ReportService
{
    public interface IReportService : IApplicationService
    {
        Task<DataTable> WheelPayments(string fromDate, string toDate);
    }
}

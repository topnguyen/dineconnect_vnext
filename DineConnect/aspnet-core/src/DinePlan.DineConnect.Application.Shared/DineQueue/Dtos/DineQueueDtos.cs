﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.DineQueue.Dtos
{
    public class QueueLocationDto : EntityDto
    {
        public bool EnableQueue { get; set; }

        public string Name { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public List<QueueLocationOptionDto> QueueLocationOptions { get; set; }

        public List<int> LocationIds { get; set; }

        public string ThemeSettings { get; set; }

    }

    public class QueueLocationOptionDto : EntityDto
    {
        public string Name { get; set; }

        public string Prefix { get; set; }

        public int MinPax { get; set; }

        public int MaxPax { get; set; }

        public bool Active { get; set; }

        public int SortOrder { get; set; }

        public int QueueLocationId { get; set; }

        public List<CustomerQueueDto> CustomerQueues { get; set; }
    }

    public class CustomerQueueDto : EntityDto
    {
        public int QueueLocationOptionId { get; set; }

        public string QueueNo { get; set; }

        public string CustomerName { get; set; }

        public string CustomerMobile { get; set; }

        public int? CustomerId { get; set; }

        public DateTimeOffset? QueueTime { get; set; }

        public DateTimeOffset? CallTime { get; set; }

        public DateTimeOffset? FinishTime { get; set; }

        public bool Seated { get; set; }

        public DateTime CreationTime { get; set; }
    }
}

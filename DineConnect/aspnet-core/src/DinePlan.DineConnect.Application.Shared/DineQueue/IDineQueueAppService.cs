﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.DineQueue.Dtos;

namespace DinePlan.DineConnect.DineQueue
{
    public interface IDineQueueAppService : IApplicationService
    {
        // Queue Location

        Task<QueueLocationDto> GetQueueLocationFullDataByLocationId(int locationId);

        Task<ListResultDto<QueueLocationDto>> GetQueueLocationList();

        Task<QueueLocationDto> GetQueueLocationFullDataById(int id);

        Task CreateOrUpdateQueueLocation(QueueLocationDto input);

        Task DeleteQueueLocation(EntityDto input);

        // Queue Option

        Task CreateOrUpdateQueueOption(QueueLocationOptionDto input);

        Task DeleteQueueOption(EntityDto input);

        // Customer Queue

        Task CreateOrUpdateCustomerQueue(CustomerQueueDto input);

        Task DeleteCustomerQueue(EntityDto input);
    }
}

﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    [DependsOn(typeof(DineConnectCoreSharedModule))]
    public class DineConnectApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectApplicationSharedModule).GetAssembly());
        }
    }
}
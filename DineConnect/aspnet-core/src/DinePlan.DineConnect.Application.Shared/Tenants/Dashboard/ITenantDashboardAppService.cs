﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Tenants.Dashboard.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();

        GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input);

        GetDailySalesOutput GetDailySales();

        GetProfitShareOutput GetProfitShare();

        GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input);

        Task<GetTopStatsOutput> GetTopStats(GetChartInput input);

        GetRegionalStatsOutput GetRegionalStats();

        GetGeneralStatsOutput GetGeneralStats();
        Task<List<ChartOutputDto>> GetTransactionChart(GetChartInput input);
        Task<List<ChartOutputDto>> GetPaymentChart(GetChartInput input);
        Task<List<ChartOutputDto>> GetDepartmentChart(GetChartInput input);
        Task<List<ChartOutputDto>> GetItemChart(GetChartInput input);
    }
}

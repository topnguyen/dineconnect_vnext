﻿namespace DinePlan.DineConnect.Tenants.Dashboard.Dto
{
    public class GetDailySalesOutput
    {
        public int[] DailySales { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tenants.Dashboard.Dto
{
    public class GetTopStatsOutput
    {
        public GetTopStatsOutput()
        {
            TotalTicketCount = 0;
            TotalOrderCount = 0;
            TotalItemSold = 0;
            TotalAmount = 0M;
            AverageTicketAmount = 0M;
            AverageOrderAmount = 0M;
        }
        public int TotalTicketCount { get; set; }
        public int TotalOrderCount { get; set; }
        public decimal TotalItemSold { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AverageTicketAmount { get; set; }
        public decimal AverageOrderAmount { get; set; }
    }
}

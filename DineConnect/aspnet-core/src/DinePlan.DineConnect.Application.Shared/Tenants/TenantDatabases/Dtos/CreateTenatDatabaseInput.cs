﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DinePlan.DineConnect.Tenants.TenantDatabases.Dtos
{
    public class CreateOrUpdateTenantDatabaseInput : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string ServerIp { get; set; }

        [Required]
        public string ServerPort { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
        public bool PersistSecurityInfo { get; set; }
        public bool MultipleActiveResultSets { get; set; }
        public bool Encrypt { get; set; }
        public bool TrustServerCertificate { get; set; }
        public int ConnectionTimeout { get; set; }
    }
}

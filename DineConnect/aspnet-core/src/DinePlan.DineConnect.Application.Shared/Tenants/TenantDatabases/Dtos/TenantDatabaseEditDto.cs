﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tenants.TenantDatabases.Dtos
{
    public class TenantDatabaseEditDto : AuditedEntityDto
    {
        public string Name { get; set; }
        public string ServerIp { get; set; }
        public string ServerPort { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool PersistSecurityInfo { get; set; }
        public bool MultipleActiveResultSets { get; set; }
        public bool Encrypt { get; set; }
        public bool TrustServerCertificate { get; set; }
        public int ConnectionTimeout { get; set; }
    }
}

﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Tenants.TenantDatabases.Dtos
{
    public class TenantDatabaseDto : Entity<int?>
    {
        public string Name { get; set; }
        public string ServerIp { get; set; }
        public string ServerPort { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
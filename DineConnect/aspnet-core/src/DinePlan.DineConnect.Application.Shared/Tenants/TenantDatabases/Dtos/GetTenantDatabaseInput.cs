﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tenants.TenantDatabases.Dtos
{
    public class GetTenantDatabaseInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }

    }
}

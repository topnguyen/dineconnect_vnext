﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tenants.TenantDatabases.Dtos
{
    public class TenantDatabaseComboboxItemDto : ComboboxItemDto
    {
        public TenantDatabaseComboboxItemDto(string value, string displayText, bool isSelected)
        {
            Value = value;
            DisplayText = displayText;
            IsSelected = isSelected;
        }
    }
}

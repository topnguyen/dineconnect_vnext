﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tenants.TenantDatabases.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tenants.TenantDatabases
{
    public interface ITenantDatabaseAppService : IApplicationService
    {
        Task<PagedResultDto<TenantDatabaseListDto>> GetTenantDatabases(GetTenantDatabaseInput input);
        Task CreateOrUpdateTenantDatabase(CreateOrUpdateTenantDatabaseInput input);
        Task<TenantDatabaseEditDto> GetTenantDatabaseForEdit(GetTenantDatabaseForEditInput input);
        Task DeleteTenantDatabase(EntityDto input);
        Task<List<TenantDatabaseComboboxItemDto>> GetTenantDatabaseForCombobox(int? selectedEditionId = null);
    }
}

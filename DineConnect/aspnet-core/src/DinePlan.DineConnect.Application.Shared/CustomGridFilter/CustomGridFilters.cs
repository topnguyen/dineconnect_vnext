﻿using DinePlan.DineConnect.CustomGridFilter.dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DinePlan.DineConnect.CustomGridFilter
{
    public  static class CustomGridFilters
    {
        public static IQueryable<T> GetGenaricFilterQuery<T>(this IQueryable<T> source , string CustomFilters)
        {
             
            if (string.IsNullOrEmpty(CustomFilters))
            {
                return source;
            }

            var filters = JsonConvert.DeserializeObject<List<GenericFilterModel>>(CustomFilters).Where(x=>x.value !="").ToList();
            if(filters.Count > 0)
            {
                string query = "x=>";
                foreach (var filter in filters)
                {
                    switch (filter.type)
                    {
                        case "number":
                            query += @"x." + filter.name + @" == " + filter.value;
                            break;
                        default:
                            query += @"x." + filter.name + @".Contains(""" + filter.value.Replace("\"","\\\"") + @""")";
                            break;

                    }
                    if (!(filters.IndexOf(filter) == filters.Count() - 1))
                    {
                        query += " && ";
                    }
                }
                return source.Where(query);
            }
            return source;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.CustomGridFilter.dto
{
    public  class GenericFilterModel
    {
        public string value { get; set; }
        public string name { get; set; }
        public string label { get; set; }
        public string type { get; set; }
    }
}

using System.Threading.Tasks;
using Abp.Application.Services;

namespace DinePlan.DineConnect.BackgroundJob
{
    public interface IBackgroundJobService : IApplicationService
    {
        Task<string> SendMailTestAsync(string emailAddress);
        Task<string> GenerateReport(string fromDate, string toDate);
    }
}

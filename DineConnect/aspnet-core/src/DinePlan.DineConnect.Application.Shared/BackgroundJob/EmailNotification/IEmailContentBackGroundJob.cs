﻿using System.Threading.Tasks;

namespace DinePlan.DineConnect.BackgroundJob.EmailNotification
{
    public interface IEmailContentBackGroundJob
    {
        Task SendMailExample(string customerEmail, string customerName);
    }
}

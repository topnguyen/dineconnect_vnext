﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Addresses.Dtos
{
    public class AddressDto : EntityDto<int?>
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostalCode { get; set; }

        [Range(1, double.MaxValue, ErrorMessage = "City is required")]
        public int? CityId { get; set; }

        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public int TenantId { get; set; }
    }

    public class CityDto : FullAuditedEntityDto
    {
        [Required]
        public string Name { get; set; }

        public string StateName { get; set; }
        public int StateId { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Country is required")]
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }

    public class StateDto : FullAuditedEntityDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Country is required")]
        public int CountryId { get; set; }

        public string CountryName { get; set; }
    }

    public class CountryDto : FullAuditedEntityDto
    {
        [Required]
        public string Name { get; set; }

        public string TwoLetterCode { get; set; }
        public int CallingCode { get; set; }
    }

    public class GetCountryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Id";
            }
        }
    }

    public class GetCountryForExcelInput
    {
        public string Filter { get; set; }
    }

    public class GetStateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int? CoundtryId { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Id";
            }
        }
    }

    public class GetStateForExcelInput
    {
        public string Filter { get; set; }

        public int? CoundtryId { get; set; }
    }

    public class GetCityInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }

        public bool IsDeleted { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Id";
            }
        }
    }

    public class GetCityForExcelInput
    {
        public string Filter { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
    }

    public class GetAddressInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Address1,Id";
            }
        }
    }
}
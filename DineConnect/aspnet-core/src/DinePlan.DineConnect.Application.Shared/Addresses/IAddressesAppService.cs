﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addresses
{
    public interface IAddressesAppService : IApplicationService
    {
        //Country
        Task<int> CreateorUpdateCountryAsync(CountryDto country);
        Task<CountryDto> GetCountryAsync(int id);
        Task<PagedResultDto<CountryDto>> GetCountryList(GetCountryInput input);
        Task<FileDto> GetCountryListToExcel(GetCountryForExcelInput input);

        Task DeleteCountryAsync(int countryId);
        //State 
        Task<int> CreateorUpdateStateAsync(StateDto state);
        Task<StateDto> GetSteteAsync(int id);
        Task<PagedResultDto<StateDto>> GetStateList(GetStateInput input);
        Task<List<StateDto>> GetStateListByCountry(int id);
        Task DeleteStateAsync(int stateId);
        Task<FileDto> GetStateListToExcel(GetStateForExcelInput input);


        //City 
        Task<int> CreateorUpdateCityAsync(CityDto city);
        Task<CityDto> GetCityAsync(int id);
        Task<PagedResultDto<CityDto>> GetCityList(GetCityInput input);
        Task DeleteCityAsync(int cityId);
        Task<FileDto> GetCityListToExcel(GetCityForExcelInput input);

        //Address
        Task<int> CreateorUpdateAddressAsync(AddressDto address);
        Task<AddressDto> GetAddressAsync(int id);
        Task<PagedResultDto<AddressDto>>  GetAddressList(string filter);
        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Address.Countries.Edit" })]
        Task ActivateState(int id);
        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Address.Countries.Edit" })]
        Task ActivateCity(int id);
        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Address.Countries.Edit" })]
        Task ActivateCoutry(int id);
    }
}

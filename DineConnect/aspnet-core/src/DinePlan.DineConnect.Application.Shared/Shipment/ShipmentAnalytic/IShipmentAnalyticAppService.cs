﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentAnalyticAppService : IApplicationService
    {
        Task<PagedResultDto<ShipmentAnalyticDto>> GetAll(GetAllShipmentAnalyticInput input);
    }
}
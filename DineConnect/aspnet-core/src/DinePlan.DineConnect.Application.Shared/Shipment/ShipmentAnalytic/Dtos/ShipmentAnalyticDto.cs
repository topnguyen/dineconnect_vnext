﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
	public class ShipmentAnalyticDto : EntityDto
	{
		public string Type { get; set; }
		public string OrderNo { get; set; }
		public DateTime Date { get; set; }
		public string Driver { get; set; }
		public string DriverPhone { get; set; }
		public string RouteCode { get; set; }
		public DateTime RouteDate { get; set; }
		public string Client { get; set; }
		public string FullAddress { get; set; }
		public Boolean Status { get; set; }
		public string DeliveryTimeFrom { get; set; }
		public string DeliveryTimeTo { get; set; }
		public string DepotOrShipFrom { get; set; }
		public string Zone { get; set; }
		public string Comment { get; set; }
		public string Number { get; set; }
		public string Note { get; set; }

		public int? AddressId { get; set; }

		public string Address { get; set; }

		public string GeoStatus { get; set; }

		public string Warehouse { get; set; }

		public string Customer { get; set; }

		public decimal? Weight { get; set; }

		public decimal? Size { get; set; }

		public decimal? Volume { get; set; }

		public decimal? Pallets { get; set; }

		public string ContactName { get; set; }

		public string Tel { get; set; }

		public string Email { get; set; }

		public int COD { get; set; }

		public double? GPSLat { get; set; }

		public double? GPSLon { get; set; }

		public int IsLoading { get; set; }

		public bool IsPD { get; set; }

		public int CreateSource { get; set; }

		public string Barcode { get; set; }

		public Guid? TrackKey { get; set; }

		public DateTime? ArrivalDate { get; set; }
		public string DepotAddress { get; set; }

	}
}
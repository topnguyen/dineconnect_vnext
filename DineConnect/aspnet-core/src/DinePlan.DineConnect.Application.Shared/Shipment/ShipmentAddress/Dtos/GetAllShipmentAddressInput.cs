﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetAllShipmentAddressInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentAddressAppService : IApplicationService
    {
        Task<PagedResultDto<ShipmentAddressDto>> GetAll(GetAllShipmentAddressInput input);

        Task<CreateOrEditShipmentAddressDto> GetShipmentAddressEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditShipmentAddressDto input);

        Task Delete(EntityDto input);
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentClientAppService : IApplicationService
    {
        Task<PagedResultDto<ShipmentClientDto>> GetAll(GetAllShipmentClientInput input);

        Task<CreateOrEditShipmentClientDto> GetShipmentClientEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditShipmentClientDto input);

        Task Delete(EntityDto input);
    }
}
﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class ShipmentClientDto : EntityDto
    {
        public string Name { get; set; }

        public string ExtCode { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string ContactName { get; set; }

        public int? Priority { get; set; }

        public string DeliveryTimeFrom { get; set; }

        public string DeliveryTimeTo { get; set; }

        public bool HasAdvancedRouting { get; set; }
    }
}
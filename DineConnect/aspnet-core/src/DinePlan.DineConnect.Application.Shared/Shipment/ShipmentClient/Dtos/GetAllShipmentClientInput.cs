﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetAllShipmentClientInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
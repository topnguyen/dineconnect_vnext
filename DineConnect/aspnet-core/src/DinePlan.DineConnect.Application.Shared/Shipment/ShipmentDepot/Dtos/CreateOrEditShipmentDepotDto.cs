﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class CreateOrEditShipmentDepotDto : EntityDto<int?>
    {
        public string Code { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string StateProvinceCountry { get; set; }
        public string PostalCode { get; set; }
        public string FullAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
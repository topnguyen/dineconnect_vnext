﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetAllShipmentDepotInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class ShipmentDepotDto : EntityDto
    {
        public string Code { get; set; }

        public string FullAddress { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentDepotAppService : IApplicationService
    {
        Task<PagedResultDto<ShipmentDepotDto>> GetAll(GetAllShipmentDepotInput input);

        Task<CreateOrEditShipmentDepotDto> GetTrackDepotEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditShipmentDepotDto input);

        Task Delete(EntityDto input);
    }
}
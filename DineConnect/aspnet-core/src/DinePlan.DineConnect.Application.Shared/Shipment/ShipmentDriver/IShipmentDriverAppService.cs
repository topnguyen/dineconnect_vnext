﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentDriverAppService : IApplicationService
    {
        Task<PagedResultDto<TrackShipmentDto>> GetAll(GetAllShipmentDriverInput input);

        Task<CreateOrEditShipmentDriverDto> GetDriverForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditShipmentDriverDto input);

        Task Delete(EntityDto input);
    }
}
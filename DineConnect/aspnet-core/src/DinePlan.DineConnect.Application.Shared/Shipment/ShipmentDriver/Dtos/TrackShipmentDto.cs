﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class TrackShipmentDto : EntityDto
    {
        public string Driver { get; set; }
        public string Vehicle { get; set; }
        public string Phone { get; set; }
        public string CompanyId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string HomeAddress { get; set; }
        public string ShipFrom { get; set; }
        public string Zone { get; set; }
        public bool Active { get; set; }

    }
}
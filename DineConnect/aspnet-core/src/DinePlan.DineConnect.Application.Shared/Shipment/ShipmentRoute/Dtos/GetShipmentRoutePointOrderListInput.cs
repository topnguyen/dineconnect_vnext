﻿using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetShipmentRoutePointOrderListInput : Abp.Application.Services.Dto.ISortedResultRequest,IShouldNormalize
    {
        public int RoutePointId { get; set; }

        public string Sorting { get; set; }

        public void Normalize()
        {
            Sorting = Sorting ?? "Number ASC";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class RemoveOrdersFromRouteInput
    {
        public List<int> OrderIds { get; set; }

        public int RouteId { get; set; }
    }

    public class AddOrdersToRouteInputDto
    {
        public List<int> OrderIds { get; set; }

        public int RouteId { get; set; }

        public AddOrdersToRouteInputDto()
		{
            OrderIds = new List<int>();
        }
    }
}

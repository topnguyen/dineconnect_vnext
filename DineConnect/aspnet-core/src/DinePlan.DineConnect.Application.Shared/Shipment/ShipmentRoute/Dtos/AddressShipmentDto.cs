﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Dtos
{
    public class AddressShipmentDto
    {
        public string Label { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}


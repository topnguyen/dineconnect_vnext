﻿using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetShipmentRoutePointListInput : Abp.Application.Services.Dto.ISortedResultRequest,IShouldNormalize
    {
        public int RouteId { get; set; }

        public string Sorting { get; set; }

        public void Normalize()
        {
            Sorting = Sorting ?? "Number ASC";
        }
    }
}
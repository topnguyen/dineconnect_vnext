﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetShipmentItemInput : ISortedResultRequest, IShouldNormalize
    {
        public int OrderId { get; set; }
        public string Sorting { get; set; }

        public void Normalize()
        {
            Sorting = Sorting ?? "OrderNum ASC";
        }
    }
}
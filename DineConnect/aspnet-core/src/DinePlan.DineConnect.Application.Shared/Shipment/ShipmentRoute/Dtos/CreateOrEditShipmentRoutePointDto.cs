﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class CreateOrEditShipmentRoutePointDto : EntityDto<int?>
    {
        public int RouteId { get; set; }
        public string SequenceNo { get; set; }
        public string Address1 { get; set; }
        public string Address { get; set; }
        public string Client { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public DateTime OrderDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public DateTime ArrivedAt { get; set; }
        public DateTime DepartedAt { get; set; }
        public string Goods { get; set; }
        public bool Status { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class CreateOrEditShipmentRouteDto : EntityDto<int?>
    {
        public string Code { get; set; }

        public DateTime DateInput { get; set; }

        public DateTime Time { get; set; }

        public DateTime Date
        {
            get
            {
                DateTime combined = DateInput.Date.Add(Time.TimeOfDay);
                return combined;
            }
        }

        public int? ShipFromId { get; set; }

        public int? DriverId { get; set; }

        public bool IsStartFromDepot { get; set; }

        public bool IsEndAtDepot { get; set; }

        public bool Status { get; set; }
    }
}
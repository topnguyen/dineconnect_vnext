﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class GetShipmentRouteListInput : ISortedResultRequest, IShouldNormalize
    {
        public string Filter { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Sorting { get; set; }

        public void Normalize()
        {
            Sorting = Sorting ?? "Date ASC";
        }
    }
}
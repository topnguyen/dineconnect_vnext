﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class ShipmentRoutePointDto : EntityDto
    {
        public int RouteId { get; set; }
        
        public int Number { get; set; }

        public string Address { get; set; }

        public string State { get; set; }

        public string GeoStatus { get; set; }

        public DateTime? StateDate { get; set; }

        public bool HasPhoto { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public DateTime? DepartureDate { get; set; }

        public decimal? PlanDistance { get; set; }

        public DateTime? PlanArrivalDate { get; set; }

        public DateTime? PlanDepartureDate { get; set; }

        public bool Lock { get; set; }

        public int? Priority { get; set; }

        public int Type { get; set; }

        public bool EtaOutOfTime { get; set; }
        
        public DateTime? EtaEnRoute { get; set; }

        public int Weight { get; set; }

        public int Volume { get; set; }

        public object Pallets { get; set; }

        public string Clients { get; set; }

        public DateTime RouteDate { get; set; }

        public string TimeSlot { get; set; }

    }
}

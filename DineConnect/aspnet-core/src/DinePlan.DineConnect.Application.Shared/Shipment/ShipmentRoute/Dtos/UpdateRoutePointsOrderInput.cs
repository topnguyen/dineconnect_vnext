﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment.ShipmentRoute.Dtos
{
    public class UpdateRoutePointsOrderInput
    {
       public List<int> RoutePointIds { get; set; }
    }
}

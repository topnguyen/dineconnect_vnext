﻿
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class ShipmentRoutePointDeliveryDto : EntityDto
    {
        public string Type { get; set; }

        public string Number { get; set; }

        public DateTime Date { get; set; }

        public string Note { get; set; }

        public string Client { get; set; }

        public int? AddressId { get; set; }

        public string Address { get; set; }

        public string GeoStatus { get; set; }

        public string Warehouse { get; set; }

        public string Customer { get; set; }

        public string Zone { get; set; }

        public string DeliveryTimeFrom { get; set; }

        public string DeliveryTimeTo { get; set; }

        public decimal? Weight { get; set; }

        public decimal? Size { get; set; }

        public decimal? Volume { get; set; }

        public decimal? Pallets { get; set; }

        public string ContactName { get; set; }

        public string Tel { get; set; }

        public string Email { get; set; }

        public int COD { get; set; }

        public double GPSLat { get; set; }

        public double GPSLon { get; set; }

        public int IsLoading { get; set; }

        public bool IsPD { get; set; }

        public int CreateSource { get; set; }

        public string Barcode { get; set; }
        public string DepotAddress { get; set; }
    }

    public class ShipmentItemListDto: EntityDto
    {
        public int OrderId { get; set; }

        public int? TrackSiteId { get; set; }

        public decimal? Amount { get; set; }

        public decimal? ActualQuantity { get; set; }

        public decimal? ExtraQuantity { get; set; }

        public decimal? RejectedQuantity { get; set; }

        public decimal? Cost { get; set; }

        public string Note { get; set; }

        public string ItemExtCode { get; set; }

        public bool HasPhoto { get; set; }

        public string RejectReason { get; set; }

        public int OrderNum { get; set; }

        public string ItemBarcode { get; set; }

        public string Name { get; set; }

        public string Metrik { get; set; }

        public string ExtCode { get; set; }

        public string Barcode { get; set; }

        public bool DeliveryScan { get; set; }
    }
    public class TrackRouteDeliveryInfoDto
    {
        public string ContactDetail { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int? Weight { get; set; }
        public int? Volume { get; set; }
        public int? Pallets { get; set; }
        public int? Code { get; set; }
        public int? ActualCode { get; set; }
        public int? TimeSlot { get; set; }
        public string BarCode { get; set; }
    }
}
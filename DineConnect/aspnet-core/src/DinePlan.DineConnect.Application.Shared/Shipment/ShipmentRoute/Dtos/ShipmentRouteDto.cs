﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class ShipmentRouteDto : EntityDto
    {
        public string Code { get; set; }
        public DateTime Date { get; set; }
        public string ShipFrom { get; set; }
        public string Driver { get; set; }
        public bool IsStartFromDepot { get; set; }
        public bool IsEndAtDepot { get; set; }
        public bool Status { get; set; }
    }
}
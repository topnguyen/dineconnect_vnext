﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using DinePlan.DineConnect.Shipment.ShipmentRoute.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.TrackPod.TrackRoute
{
    public interface IShipmentRouteAppService : IApplicationService
    {
        Task<List<ShipmentRouteDto>> GetRouteList(GetShipmentRouteListInput input);

        Task<CreateOrEditShipmentRouteDto> GetRouteForEdit(EntityDto input);

        Task CreateOrEditRoute(CreateOrEditShipmentRouteDto input);

        Task DeleteRoute(EntityDto input);

        Task<List<ShipmentRoutePointDto>> GetRoutePointList(GetShipmentRoutePointListInput input);

        Task UpdateRoutePointsOrder(UpdateRoutePointsOrderInput input);

        Task<List<ShipmentRoutePointOrderListDto>> GetOrderList(GetShipmentRoutePointOrderListInput input);

        Task<List<ShipmentRoutePointDeliveryDto>> GetFreeOrderList(GetShipmentRoutePointListInput input);

        Task<List<ShipmentItemListDto>> GetOrderItemsList(GetShipmentItemInput input);

        Task RemoveOrdersFromRoute(RemoveOrdersFromRouteInput input);

        Task<int> AddOrdersFromRoute(AddOrdersToRouteInputDto input);

        Task CreateOrUpdateOrder(CreateOrUpdateOrderDto input);

        Task<CreateOrUpdateOrderDto> GetOrderById(int id);

        Task DeleteOrder(int id);
        Task CreateOrdersFromExcel(byte[] fileBytes);
        FileDto GetImportTemplateToExcel();
        Task<List<AddressShipmentDto>> GetOrderListOnMap(GetShipmentRoutePointListInput input);
        Task DuplicateRoute(CreateOrEditShipmentRouteDto input);
        Task CloseRoute(int routeId);
    }
}
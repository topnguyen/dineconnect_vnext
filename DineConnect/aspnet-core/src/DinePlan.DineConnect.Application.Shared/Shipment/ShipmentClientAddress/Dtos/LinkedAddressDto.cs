﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
	public class LinkedAddressDto : EntityDto<long>
	{
		public string ExtCode { get; set; }
		public string Name { get; set; }
	}
}

﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
	public class LinkedClientDto : EntityDto<long>
	{
		public string ExtCode { get; set; }
		public string Name { get; set; }
	}
}

﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Shipment.Dtos
{
    public class CreateOrEditShipmentClientAddressDto : EntityDto<int?>
    {
        public int ClientId { get; set; }
        public int AddressId { get; set; }
        public int TenantId { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Shipment.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipment
{
    public interface IShipmentClientAddressAppService : IApplicationService
    {
        Task<List<LinkedAddressDto>> GetLinkedAddressByClientId(int clientId);
        Task<List<LinkedClientDto>> GetLinkedClientByAddressId(int addressId);
        Task<List<LinkedClientDto>> GetClientesIsNotInAddressId(int addressId);
        Task<List<LinkedAddressDto>> GetAddressesIsNotInClientId(int clientId);
        Task<long> Create(CreateOrEditShipmentClientAddressDto input);
        Task Delete(EntityDto input);
    }
}
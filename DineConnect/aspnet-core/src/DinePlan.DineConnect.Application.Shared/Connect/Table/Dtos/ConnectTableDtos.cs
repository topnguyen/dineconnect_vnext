﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Table.Dtos
{
    public class ConnectTableListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int Pax { get; set; }
    }

    public class ConnectTableEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Pax { get; set; }
    }

    public class CreateOrUpdateConnectTableInput : IInputDto
    {
        [Required]
        public ConnectTableEditDto Table { get; set; }
    }

    public class GetConnectTableForEditOutput : IOutputDto
    {
        public ConnectTableEditDto Table { get; set; }
    }

    public class ConnectTableGroupListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string DisplayLocation { get; set; }
        public string Locations { get; set; }
    }

    public class ConnectTableGroupEditDto : ConnectOrgLocEditDto
    {
        public ConnectTableGroupEditDto()
        {
            ConnectTables = new List<ConnectTableEditDto>();
            AllTables = new List<ConnectTableEditDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public List<ConnectTableEditDto> ConnectTables { get; set; }
        public List<ConnectTableEditDto> AllTables { get; set; }
    }

    public class GetConnectTableGroupInput : CommonLocationGroupFilterInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetConnectTableGroupForEditOutput : IOutputDto
    {
        public ConnectTableGroupEditDto ConnectTableGroup { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        public GetConnectTableGroupForEditOutput()
        {
            ConnectTableGroup = new ConnectTableGroupEditDto();
            LocationGroup = new CommonLocationGroupDto();
        }
    }

    public class CreateOrUpdateConnectTableGroupInput : IInputDto
    {
        [Required]
        public ConnectTableGroupEditDto ConnectTableGroup { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateConnectTableGroupInput()
        {
            ConnectTableGroup = new ConnectTableGroupEditDto();
            LocationGroup = new CommonLocationGroupDto();
        }
    }

    public class ApiConnectTableGroupInput
    {
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
    }

    public class ApiConnectTableGroupOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

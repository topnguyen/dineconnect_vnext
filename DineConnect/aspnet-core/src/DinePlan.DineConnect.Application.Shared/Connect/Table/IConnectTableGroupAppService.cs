﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Table.Dtos;

namespace DinePlan.DineConnect.Connect.Table
{
    public interface IConnectTableGroupAppService : IApplicationService
    {
        Task<PagedResultDto<ConnectTableGroupListDto>> GetAll(GetConnectTableGroupInput inputDto);

        Task<GetConnectTableGroupForEditOutput> GetConnectTableGroupForEdit(NullableIdDto EntityDto);

        Task CreateOrUpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input);

        Task DeleteConnectTableGroup(EntityDto input);

        Task<ListResultDto<ConnectTableGroupListDto>> GetIds();

        Task<ListResultDto<ConnectTableGroupEditDto>> ApiGetAll(ApiConnectTableGroupInput inputDto);

        Task<ListResultDto<ConnectTableGroupListDto>> GetConnectTableGroups();

        Task<ListResultDto<ComboboxItemDto>> GetConnectTableGroupsForCombobox();

        Task<PagedResultDto<ConnectTableListDto>> GetAllTables(GetConnectTableGroupInput input);

        Task<int> CreateOrUpdateConnectTable(CreateOrUpdateConnectTableInput input);

        Task DeleteConnectTable(EntityDto input);

        Task<GetConnectTableForEditOutput> GetConnectTableForEdit(NullableIdDto EntityDto);

        Task ActivateItem(EntityDto input);
    }
}

﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class GetAllProgramSettingTemplateDto : CreationAuditedEntityDto<int>
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
}
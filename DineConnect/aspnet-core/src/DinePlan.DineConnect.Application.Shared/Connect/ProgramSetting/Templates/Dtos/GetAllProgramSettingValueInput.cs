﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class GetAllProgramSettingValueInput : CommonLocationGroupFilterInputDto
    {
        public int ProgramSettingTemplateId { get; set; }
    }
}
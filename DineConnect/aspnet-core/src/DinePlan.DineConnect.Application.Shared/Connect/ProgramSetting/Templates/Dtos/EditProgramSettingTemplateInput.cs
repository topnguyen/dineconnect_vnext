﻿namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class EditProgramSettingTemplateInput
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string DefaultValue { get; set; }
    }
}
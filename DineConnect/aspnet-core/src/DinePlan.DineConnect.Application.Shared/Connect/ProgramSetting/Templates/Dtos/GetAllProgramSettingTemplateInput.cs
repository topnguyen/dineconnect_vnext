﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class GetAllProgramSettingTemplateInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
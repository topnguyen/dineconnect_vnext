﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class CreateOrEditProgramSettingValueInput : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }

        public int ProgramSettingTemplateId { get; set; }

        public string ActualValue { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; } = new CommonLocationGroupDto();
    }
}
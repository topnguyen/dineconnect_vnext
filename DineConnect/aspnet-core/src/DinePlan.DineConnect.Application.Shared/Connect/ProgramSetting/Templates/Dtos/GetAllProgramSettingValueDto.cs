﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class GetAllProgramSettingValueDto : EntityDto<int>
    {
        public string ActualValue { get; set; }
    }
}
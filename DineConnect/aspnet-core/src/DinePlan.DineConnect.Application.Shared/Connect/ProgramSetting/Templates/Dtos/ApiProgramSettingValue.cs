﻿namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class ApiProgramSettingValue
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
}
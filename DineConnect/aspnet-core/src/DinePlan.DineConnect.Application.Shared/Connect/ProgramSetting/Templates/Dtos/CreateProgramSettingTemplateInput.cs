﻿namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos
{
    public class CreateProgramSettingTemplateInput
    {
        public string Name { get; set; }

        public string DefaultValue { get; set; }
    }
}
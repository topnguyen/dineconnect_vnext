﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.ProgramSetting.Templates.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.ProgramSetting.Templates
{
    public interface IProgramSettingTemplatesAppService
    {
        Task<PagedResultDto<GetAllProgramSettingTemplateDto>> GetAll(GetAllProgramSettingTemplateInput input);

        Task<FileDto> ExportToExcel();

        Task<EditProgramSettingTemplateInput> GetForEdit(int id);

        Task<int> Create(CreateProgramSettingTemplateInput input);

        Task<int> Edit(EditProgramSettingTemplateInput input);

        Task Delete(int id);

        Task<PagedResultDto<GetAllProgramSettingValueDto>> GetAllValues(GetAllProgramSettingValueInput input);

        Task<int> CreateOrUpdateValue(CreateOrEditProgramSettingValueInput input);

        Task DeleteValue(int id);
        Task<ListResultDto<ApiProgramSettingValue>> ApiGetProgramSettings(ApiLocationInput input);
    }
}
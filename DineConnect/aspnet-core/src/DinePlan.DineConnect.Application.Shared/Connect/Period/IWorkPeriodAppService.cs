﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Period
{
	public interface IWorkPeriodAppService : IApplicationService
	{
		Task<CreateOrUpdatePeriodOutput> CreateOrUpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input);
		Task<WorkPeriodOutput> BuildSummaryReport(WorkPeriodSummaryInput input);
		Task<FileDto> BuildSummaryExcel(WorkPeriodSummaryInput input);
		Task<FileDto> BuildDetailExcel(WorkPeriodSummaryInput input);
		Task<WorkPeriodOutput> GetSummaryReportOnTickets(WorkPeriodSummaryInput input);
		Task<List<WorkPeriodItemSales>> GetItemSalesForWorkPeriod(GetItemInput input);
		Task<List<WorkPeriodEditDto>> GetWorkPeriodByLocationForDates(WorkPeriodSummaryInput input);
		Task<FileDto> BuildItemSalesForWorkPeriodExcel(GetItemInput input);
		Task<List<WorkPeriodSummaryOutput>> BuildListWorkPeriodsSummary(WorkPeriodSummaryInput input);
        Task<StartWorkPeriodOutput> StartWorkPeriod(StartWorkPeriodInput input);
        Task<EndWorkPeriodOutput> EndWorkPeriod(EndWorkPeriodInput input);


		#region week day
		Task<List<WorkDayDetailSummaryOutput>> BuildWeekDaySales(WorkPeriodSummaryInput input);
		Task<FileDto> BuildWeekDaySalesExport(WorkPeriodSummaryInput input);
		#endregion week day

		#region Week end
		Task<List<WorkDayDetailSummaryOutput>> BuildWeekEndSales(WorkPeriodSummaryInput input);
		Task<FileDto> BuildWeekEndSalesExport(WorkPeriodSummaryInput input);
		#endregion Week day
	}
}
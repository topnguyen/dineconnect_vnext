﻿namespace DinePlan.DineConnect.Connect.Period.Dtos
{
	public class CreateOrUpdateWorkPeriodInput
	{
		public WorkPeriodEditDto WorkPeriod { get; set; }
	}
}
﻿using DinePlan.DineConnect.Common.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Period.Dtos
{
    public class WorkPeriodOutput : IOutputDto
    {
        public DashboardPeriod DashBoard { get; set; }
        public List<WorkPeriodSummaryOutput> Periods { get; set; }
        public string LastLocationClosed { get; set; }
        public DateTime TimeOfClosed { get; set; }
        public Dictionary<string, decimal> OtherPayments { get; set; }

        public WorkPeriodOutput()
        {
            DashBoard = new DashboardPeriod();
            Periods = new List<WorkPeriodSummaryOutput>();
            OtherPayments = new Dictionary<string, decimal>();
        }
    }
    public class DashboardPeriod : IOutputDto
    {
        public decimal TotalSales { get; set; }
        public decimal TotalTickets { get; set; }
        public decimal Average { get; set; }
    }
    public class WorkPeriodSummaryOutput
    {
        public DateTime ReportStartDay { get; set; }
        public DateTime ReportEndDay { get; set; }
        public string DayStartStr => ReportStartDay.ToString("D");
        public string DayEndStar => ReportEndDay.ToString("D");
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalTicketCount { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && TotalTicketCount > 0M)
                    return Total / TotalTicketCount;
                return 0M;
            }
        }

        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
        public string DayInformation { get; set; }
        public int Wid { get; set; }

        public List<WorkShiftDto> WorkShifts
        {
            get
            {
                if (string.IsNullOrEmpty(DayInformation))
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<List<WorkShiftDto>>(DayInformation);
            }
        }

        public List<WorkShiftDto> WorkShiftsTemp { get; set; }


        public List<string> Terminals
        {
            get
            {
                if (WorkShifts == null)
                {
                    return null;
                }

                return WorkShifts.Select(t => t.TerminalName).Distinct().ToList();
            }
        }
    }
    public class WorkShiftDto
    {
        public WorkShiftDto()
        {
            PaymentInfos = new List<WorkTimePaymentInformationDto>();
            PaymentDenos = new List<WorkTimePaymentDenomationDto>();
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TerminalName { get; set; }

        public string StartUser { get; set; }
        public string StopUser { get; set; }
        public int TillCount { get; set; }
        public List<WorkTimePaymentInformationDto> PaymentInfos { get; set; }
        public List<WorkTimePaymentDenomationDto> PaymentDenos { get; set; }
        public decimal Float { get; set; }

        public string UniqueId
        {
            get
            {
                return $"{StartDate.Ticks}:{EndDate.Ticks}:{TerminalName}";
            }
            set { }
        }
        private string _infoId;
        public string infoId
        {
            get
            {
                return _infoId != null && _infoId.ToUpper() == "ALL" ? _infoId : StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();
            }
            set
            {
                _infoId = value;
            }
        }
    }
    public class WorkTimePaymentInformationDto
    {
        public int PaymentType { get; set; }
        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
    }
    public class WorkTimePaymentDenomationDto
    {
        public string Value { get; set; }
        public int PaymentType { get; set; }
    }
}

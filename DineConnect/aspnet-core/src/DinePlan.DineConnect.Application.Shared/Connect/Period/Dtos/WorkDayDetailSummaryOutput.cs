﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Period.Dtos
{
    public class WorkDayDetailSummaryOutput
    {
        public string Date { get; set; }
        public decimal Sales { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTickets { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && TotalTickets > 0M)
                {
                    return Total / TotalTickets;
                }
                return 0M;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Period.Dtos
{
    public class StartWorkPeriodInput : IInputDto
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public int LocationId { get; set; }
        public decimal Float { get; set; }
        public string UserName { get; set; }
        public DateTime StartTime { get; set; }
        public int Wid { get; set; }
        public decimal TotalTaxs { get; set; }
    }

    public class StartWorkPeriodOutput : IOutputDto
    {
        public int WorkPeriodId { get; set; }
    }
    public class EndWorkPeriodOutput : IOutputDto
    {
        public int WorkPeriodId { get; set; }
    }
    public class EndWorkPeriodInput : IInputDto
    {
        public int TenantId { get; set; }

        public int WorkPeriodId { get; set; }
        public DateTime EndTime { get; set; }
        public List<EndPeriodPaymentInformation> Payments { get; set; }
    }

    public class EndPeriodPaymentInformation
    {
        public string Name { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
    }
}

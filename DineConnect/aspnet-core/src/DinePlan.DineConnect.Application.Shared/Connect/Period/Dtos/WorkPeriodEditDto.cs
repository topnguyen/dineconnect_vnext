﻿using System;

namespace DinePlan.DineConnect.Connect.Period.Dtos
{
	public class WorkPeriodEditDto
	{
		public int? Id { get; set; }
		public string StartUser { get; set; }
		public string EndUser { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public virtual int Wid { get; set; }
		public virtual decimal TotalSales { get; set; }
		public virtual int TotalTicketCount { get; set; }

		public virtual int LocationId { get; set; }
		public int TenantId { get; set; }
		public string WorkPeriodInformations { get; set; }
		public bool AutoClosed { get; set; }
	}
}
﻿using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Period.Dtos
{
    public class WorkPeriodSummaryInput
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public int LocationId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool RunInBackground { get; set; }
        public int? TenantId { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public int UserId { get; set; }
        public DateTime DateReport { get; set; }
        public bool Portrait { get; set; }
        public WorkPeriodSummaryInput()
        {
            Locations = new List<SimpleLocationDto>();
        }
    }
}

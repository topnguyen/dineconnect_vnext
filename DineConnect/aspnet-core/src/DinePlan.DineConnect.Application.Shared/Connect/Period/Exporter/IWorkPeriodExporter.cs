﻿using DinePlan.DineConnect.Connect.Period.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Period.Exporter
{
    public interface IWorkPeriodExporter
    {
        Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, IWorkPeriodAppService wpService);
        Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, IWorkPeriodAppService wpService);
        Task<FileDto> ExportItemSalesForWorkPeriodExcel(GetItemInput input, IWorkPeriodAppService appService, string location);
        Task<FileDto> ExportWeekDaySales(WorkPeriodSummaryInput input, IWorkPeriodAppService appService);
        Task<FileDto> ExportWeekEndSales(WorkPeriodSummaryInput input, IWorkPeriodAppService appService);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Job.Dto
{
    [Serializable]
    public class ConnectSyncJobArgs
    {
        public string TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenantUser { get; set; }
        public string TenantPassword { get; set; }
        public string TenantUrl { get; set; }
        public int LocalTenantId { get; set; }

    }
}

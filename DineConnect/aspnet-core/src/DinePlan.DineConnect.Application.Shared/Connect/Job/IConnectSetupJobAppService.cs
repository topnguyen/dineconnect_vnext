﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Job.Dto;

namespace DinePlan.DineConnect.Connect.Job
{
    public interface IConnectSetupJobAppService : IApplicationService
    {
        Task ApiSyncDineConnect(ConnectSyncJobArgs connectSyncJobArgs);
    }
}
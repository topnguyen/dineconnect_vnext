﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Promotions
{
    public interface IPromotionCategoryAppService : IApplicationService
    {
        Task<PagedResultDto<PromotionCategoryListDto>> GetAll(GetPromotionCategoryInput inputDto);
        
        Task<FileDto> GetAllToExcel(GetPromotionCategoryInput input);
        
        Task<GetPromotionCategoryForEditOutput> GetPromotionCategoryForEdit(NullableIdDto EntityDto);
        
        Task CreateOrUpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input);

        Task DeletePromotionCategory(EntityDto input);
        
        Task<ListResultDto<PromotionCategoryListDto>> GetPromotionCategories();
    }
}

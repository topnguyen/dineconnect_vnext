﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
    public class ComboboxItemCustomizeDto
    {
        public ComboboxItemCustomizeDto() { }
        public ComboboxItemCustomizeDto(int _value, string _displayText, int? _referencesId) {
            Value = _value;
            DisplayText = _displayText;
            ReferencesId = _referencesId;
        }
        public int Value { get; set; }

        public string DisplayText { get; set; }
        public int? ReferencesId { get; set; }
    }
}

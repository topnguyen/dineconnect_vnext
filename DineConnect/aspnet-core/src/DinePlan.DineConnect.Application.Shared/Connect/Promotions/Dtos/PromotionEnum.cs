﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public enum Days
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 0
    };

    public enum PromotionValueTypes
    {
        Percentage,
        Value
    };

    public enum PromotionTypes
    {
        NotAssigned = 0,
        HappyHour = 1,
        FixedDiscountPercentage = 2,
        FixedDiscountValue = 3,
        BuyXItemGetYItemFree = 4,
        BuyXItemGetYatZValue = 5,
        OrderDiscount = 6,
        FreeItems = 7,
        TicketDiscount = 8,
        PaymentOrderDiscount = 9,
        PaymentTicketDiscount = 10,
        BuyXAndYAtZValue = 11,
        StepDiscount = 12
    };

    public enum ResetType
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        Yearly = 3
    }
    public enum PromotionApplyTypes
    {
        Highest_Price_Item,
        Lowest_Price_Item
    };
    public enum StepDiscountType
    {
        Quantity =0,
        Price = 1
    }
}
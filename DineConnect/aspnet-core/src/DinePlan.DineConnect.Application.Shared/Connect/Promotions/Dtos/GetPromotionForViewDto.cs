﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
	public class GetPromotionForViewDto
	{
		public PromotionsDto Promotion { get; set; }
	}
}
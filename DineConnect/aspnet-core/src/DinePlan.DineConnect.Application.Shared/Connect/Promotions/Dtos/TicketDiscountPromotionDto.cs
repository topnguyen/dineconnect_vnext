﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class TicketDiscountPromotionDto
    {
        public TicketDiscountPromotionDto()
        {
            PromotionValueType = 1;
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public decimal PromotionValue { get; set; }
        public bool AskReference { get; set; }
        public bool AuthenticationRequired { get; set; }
        public  bool ApplyAsPayment { get; set; }
        public  bool PromotionApplyOnce { get; set; }
        public  int PromotionApplyType { get; set; }

    }
}
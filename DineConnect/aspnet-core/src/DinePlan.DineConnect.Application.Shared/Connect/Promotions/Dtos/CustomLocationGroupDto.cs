﻿using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class CustomLocationGroupDto
	{
		public CustomLocationGroupDto()
		{
			Locations = new List<QuotaLocationDto>();
			Groups = new List<SimpleLocationGroupDto>();
			Group = false;
			LocationTags = new List<SimpleLocationTagDto>();
			LocationTag = false;
			NonLocations = new List<SimpleLocationDto>();
		}

		public List<QuotaLocationDto> Locations { get; set; }
		public List<SimpleLocationGroupDto> Groups { get; set; }
		public List<SimpleLocationTagDto> LocationTags { get; set; }
		public List<SimpleLocationDto> NonLocations { get; set; }

		public bool Group { get; set; }
		public bool LocationTag { get; set; }
		public long UserId { get; set; }
	}
}
﻿using Abp.Collections.Extensions;
using Abp.Extensions;
using System;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class PromotionScheduleEditDto
    {
        public PromotionScheduleEditDto()
        {
        }

        public int? Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Days { get; set; }

        public string MonthDays { get; set; }

        public string DayNames
        {
            get
            {
                if (!Days.IsNullOrEmpty())
                {
                    var days = Days.Split(",").ToList();
                    return days.Select(d => (Days)Int32.Parse(d)).JoinAsString(",");
                }

                return null;
            }
        }
    }
}
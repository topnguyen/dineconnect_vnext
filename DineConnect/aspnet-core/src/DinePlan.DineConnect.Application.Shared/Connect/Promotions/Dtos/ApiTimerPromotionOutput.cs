﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiTimerPromotionOutput : IOutputDto
	{
		public string PriceTag { get; set; }
	}
}
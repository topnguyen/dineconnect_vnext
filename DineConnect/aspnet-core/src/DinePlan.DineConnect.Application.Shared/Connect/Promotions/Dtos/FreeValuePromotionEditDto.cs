﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class FreeValuePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> FreeValuePromotionExecutions { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> From { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> To { get; set; }

        public FreeValuePromotionEditDto()
        {
            FreeValuePromotionExecutions = new Collection<FreeValuePromotionExecutionEditDto>();
            From = new Collection<FreeValuePromotionExecutionEditDto>();
            To = new Collection<FreeValuePromotionExecutionEditDto>();
            FromCount = 1;
        }
    }
}
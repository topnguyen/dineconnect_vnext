﻿using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiPromotionOutput : IOutputDto
	{
		public List<CreateOrEditPromotionDto> Promotions { get; set; }
	}
}
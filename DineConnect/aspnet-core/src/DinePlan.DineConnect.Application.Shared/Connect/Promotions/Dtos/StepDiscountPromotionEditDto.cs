﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class StepDiscountPromotionEditDto
    {
        public StepDiscountPromotionEditDto()
        {
            StepDiscountPromotionExecutions = new Collection<StepDiscountPromotionExecutionEditDto>();
            StepDiscountPromotionMappingExecutions = new Collection<StepDiscountPromotionMappingExecutionEditDto>();
        }

        public int? Id { get; set; }

        public int StepDiscountType { get; set; }
        public bool ApplyForEachQuantity { get; set; }
        public Collection<StepDiscountPromotionExecutionEditDto> StepDiscountPromotionExecutions { get; set; }
        public Collection<StepDiscountPromotionMappingExecutionEditDto> StepDiscountPromotionMappingExecutions { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class GetPromotionForEditOutput
    {
        public GetPromotionForEditOutput()
        {
            Promotion = new CreateOrEditPromotionDto();
            TimerPromotion = new TimerPromotionEditDto();
            FreePromotion = new FreePromotionEditDto();
            FreeValuePromotion = new FreeValuePromotionEditDto();
            FixPromotions = new List<FixedPromotionEditDto>();
            FixPromotionsPer = new List<FixedPromotionEditDto>();
            Distributions = new List<FixedPromotionEditDto>();
            DemandPromotion = new DemandPromotionEditDto();
            FreeItemPromotion = new FreeItemPromotionEditDto();
            TicketDiscountPromotion = new TicketDiscountPromotionDto();
            StepDiscountPromotion = new StepDiscountPromotionEditDto();
            BuyXAndYAtZValuePromotion = new BuyXAndYAtZValuePromotionEditDto();

            PromotionTypes = new ListResultDto<ComboboxItemDto>();
            PromotionValueTypes = new ListResultDto<ComboboxItemDto>();
            DayOfWeeks = new ListResultDto<ComboboxItemDto>();
            RestTypeList = new ListResultDto<ComboboxItemDto>();
            PromotionPositions = new ListResultDto<ComboboxItemDto>();
            PromotionCategories = new ListResultDto<PromotionCategoryListDto>();
            CardTypes = new ListResultDto<ComboboxItemDto>();
        }

        public CreateOrEditPromotionDto Promotion { get; set; }

        public TimerPromotionEditDto TimerPromotion { get; set; }
        public List<FixedPromotionEditDto> FixPromotions { get; set; }
        public List<FixedPromotionEditDto> FixPromotionsPer { get; set; }
        public List<FixedPromotionEditDto> Distributions { get; set; }
        public FreePromotionEditDto FreePromotion { get; set; }
        public FreeValuePromotionEditDto FreeValuePromotion { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public DemandPromotionEditDto DemandPromotion { get; set; }
        public FreeItemPromotionEditDto FreeItemPromotion { get; set; }

        public TicketDiscountPromotionDto TicketDiscountPromotion { get; set; }
        public BuyXAndYAtZValuePromotionEditDto BuyXAndYAtZValuePromotion { get; set; }
        public StepDiscountPromotionEditDto StepDiscountPromotion { get; set; }
        public ListResultDto<ComboboxItemDto> PromotionTypes { get; set; }
        public ListResultDto<ComboboxItemDto> PromotionValueTypes { get; set; }
        public ListResultDto<PromotionCategoryListDto> PromotionCategories { get; set; }
        public ListResultDto<ComboboxItemDto> DayOfWeeks { get; set; }
        public ListResultDto<ComboboxItemDto> RestTypeList { get; set; }
        public ListResultDto<ComboboxItemDto> PromotionPositions { get; set; }
        public ListResultDto<ComboboxItemDto> CardTypes { get; set; }

        public string Filter { get; set; }
    }
}


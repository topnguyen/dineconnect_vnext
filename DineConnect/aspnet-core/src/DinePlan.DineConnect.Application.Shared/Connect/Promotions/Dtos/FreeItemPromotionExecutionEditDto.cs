﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class FreeItemPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
    }
}
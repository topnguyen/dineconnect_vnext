﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class GetQuotaDetailInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public int PromotionQuotaId { get; set; }
		public int LocationId { get; set; }
		public string DynamicFilter { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Plant";
			}
		}
	}
}
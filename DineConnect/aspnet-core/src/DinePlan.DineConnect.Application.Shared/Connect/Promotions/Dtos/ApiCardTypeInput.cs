﻿namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiCardTypeInput : ApiPromotionDefinitionInput
	{
		public int CardTypeId { get; set; }
		public string CardNumber { get; set; }
		public string TicketNumber { get; set; }
		public bool IsPromotion { get; set; }
	}
}
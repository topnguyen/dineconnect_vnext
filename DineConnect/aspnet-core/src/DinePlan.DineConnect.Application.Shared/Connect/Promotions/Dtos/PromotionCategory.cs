﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class PromotionCategoryListDto : FullAuditedEntityDto
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string Tag { get; set; }
	}

	public class PromotionCategoryEditDto
	{
		public int? Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Tag { get; set; }
	}

	public class GetPromotionCategoryInput : PagedAndSortedResultRequestDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "CreationTime DESC";
			}
		}
	}

	public class GetPromotionCategoryForEditOutput : IOutputDto
	{
		public PromotionCategoryEditDto PromotionCategory { get; set; }
	}

	public class CreateOrUpdatePromotionCategoryInput : IInputDto
	{
		[Required]
		public PromotionCategoryEditDto PromotionCategory { get; set; }
	}
}
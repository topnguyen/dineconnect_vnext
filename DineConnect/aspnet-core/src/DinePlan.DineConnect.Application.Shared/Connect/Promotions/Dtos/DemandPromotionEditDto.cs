﻿using System.Collections.ObjectModel;
using DinePlan.DineConnect.Connect.Discount.Dtos;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
    public class DemandPromotionEditDto
    {
        public DemandPromotionEditDto()
        {
            PromotionValueType = 1;
            DemandPromotionExecutions = new Collection<DemandPromotionExecutionEditDto>();
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public string ButtonColor { get; set; }
        public decimal PromotionValue { get; set; }
        public bool PromotionOverride { get; set; }
        public bool AuthenticationRequired { get; set; }
        public bool AskReference { get; set; }

        public  bool PromotionApplyOnce { get; set; }
        public  int PromotionApplyType { get; set; }
        public Collection<DemandPromotionExecutionEditDto> DemandPromotionExecutions { get; set; }
    }
}
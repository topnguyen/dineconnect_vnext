﻿using System;
using DinePlan.DineConnect.Common.Dto;
//using DinePlan.DineConnect.Connect.Master.LocationGroup;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
    public class PromotionQuotaDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public int PromotionId { get; set; }
        public int QuotaAmount { get; set; }
        public int QuotaUsed { get; set; }
        public int ResetType { get; set; }
        public int? ResetWeekValue { get; set; }
        public DateTime? ResetMonthValue { get; set; }
        public DateTime? LastResetDate { get; set; }
        public DateTime? PlantResetDate { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }
        public string Plants { get; set; }
        public bool Each { get; set; }
    }
}
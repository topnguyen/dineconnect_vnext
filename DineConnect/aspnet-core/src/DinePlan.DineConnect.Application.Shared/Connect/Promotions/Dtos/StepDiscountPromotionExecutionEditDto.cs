﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class StepDiscountPromotionExecutionEditDto
    {
        //public int? Id { get; set; }
        //public int MenuItemId { get; set; }
        //public int MenuItemPortionId { get; set; }
        //public int ProductGroupId { get; set; }
        //public int CategoryId { get; set; }
        public int? Id { get; set; }
        public decimal PromotionStepTypeValue { get; set; }
        public int PromotionValueType { get; set; }
        public string PromotionValueTypeName { get; set; }
        public decimal PromotionValue { get; set; }
    }
}
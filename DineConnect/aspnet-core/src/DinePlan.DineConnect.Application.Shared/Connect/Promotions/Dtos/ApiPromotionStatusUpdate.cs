﻿namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiPromotionStatusUpdate : ApiPromotionDefinitionInput
	{
		public bool Active { get; set; }
	}
}
﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class PromotionRestrictItemEditDto
    {
        public PromotionRestrictItemEditDto()
        {
        }

        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
    }
}
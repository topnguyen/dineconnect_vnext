﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiPromotionDefinitionInput : IInputDto
	{
		public int TenantId { get; set; }
		public int PromotionId { get; set; }
		public int LocationId { get; set; }
	}
}
﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
    public class GetComboboxForPromotionTypeOutput

    {
        public GetComboboxForPromotionTypeOutput()
        {
            PriceTags = new ListResultDto<ComboboxItemDto>();
            Departments = new ListResultDto<ComboboxItemDto>();
            DayOfWeeks = new ListResultDto<ComboboxItemDto>();
            RestTypeList = new ListResultDto<ComboboxItemDto>();
            ApplyTypes = new ListResultDto<ComboboxItemDto>();
            PromotionValueTypes = new ListResultDto<ComboboxItemDto>();
            StepDiscountTypes = new ListResultDto<ComboboxItemDto>();
        }
        public ListResultDto<ComboboxItemDto> PriceTags { get; set; }
        public ListResultDto<ComboboxItemDto> Departments { get; set; }
        public ListResultDto<ComboboxItemDto> DayOfWeeks { get; set; }
        public ListResultDto<ComboboxItemDto> RestTypeList { get; set; }
        public ListResultDto<ComboboxItemDto> ApplyTypes { get; set; }
        public ListResultDto<ComboboxItemDto> PromotionValueTypes { get; set; }
        public ListResultDto<ComboboxItemDto> StepDiscountTypes { get; set; }


    }
}

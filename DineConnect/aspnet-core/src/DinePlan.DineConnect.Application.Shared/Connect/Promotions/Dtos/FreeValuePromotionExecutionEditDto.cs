﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class FreeValuePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool From { get; set; }
        public  int ValueType { get; set; }
        public  decimal Value { get; set; }
        public  int ProductGroupId { get; set; }
        public  int CategoryId { get; set; }
        public string ProductGroupName { get; set; }
        public string MenuItemName { get; set; }
        public string CategoryName { get; set; }


    }
}
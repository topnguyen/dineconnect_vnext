﻿namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class QuotaLocationDto
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public long OrganizationUnitId { get; set; }
		public int CompanyRefId { get; set; }
		public int QuotaUsed { get; set; }
	}
}
﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class TimerPromotionEditDto
    {
        public int? Id { get; set; }
        public int PriceTagId { get; set; }
    }
}
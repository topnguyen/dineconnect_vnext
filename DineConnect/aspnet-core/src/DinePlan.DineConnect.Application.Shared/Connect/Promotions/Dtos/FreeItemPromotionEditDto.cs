﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class FreeItemPromotionEditDto
    {
        public int? Id { get; set; }
        public int ItemCount { get; set; }
        public bool Confirmation { get; set; }
        public decimal TotalAmount { get; set; }
        public bool MultipleTimes { get; set; }
        public Collection<FreeItemPromotionExecutionEditDto> Executions { get; set; }

        public FreeItemPromotionEditDto()
        {
            Executions = new Collection<FreeItemPromotionExecutionEditDto>();
        }
    }
}
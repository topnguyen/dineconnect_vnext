﻿using System;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class GetAllPromotionsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? PromotionTypeId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
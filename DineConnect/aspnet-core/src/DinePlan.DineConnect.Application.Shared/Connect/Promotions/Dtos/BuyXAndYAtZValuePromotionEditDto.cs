﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class BuyXAndYAtZValuePromotionEditDto
    {
        public BuyXAndYAtZValuePromotionEditDto()
        {
            BuyXAndYAtZValueExecutions = new Collection<BuyXAndYAtZValuePromotionExecutionEditDto>();
            PromotionValueType = (int)PromotionTypes.BuyXAndYAtZValue;
        }

        public int? Id { get; set; }

        public int ItemCount { get; set; }
        public int Value { get; set; }
        public int PromotionValueType { get; set; }
        public  int PromotionId { get; set; }
        public Collection<BuyXAndYAtZValuePromotionExecutionEditDto> BuyXAndYAtZValueExecutions { get; set; }
    }
}
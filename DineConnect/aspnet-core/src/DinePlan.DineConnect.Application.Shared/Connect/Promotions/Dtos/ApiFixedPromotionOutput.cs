﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiFixedPromotionOutput : IOutputDto
	{
		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public int MenuItemId { get; set; }
		public int ValueType { get; set; }
		public decimal Value { get; set; }
		public int ProductGroupId { get; set; }
		public int MenuItemPortionId { get; set; }
	}
}
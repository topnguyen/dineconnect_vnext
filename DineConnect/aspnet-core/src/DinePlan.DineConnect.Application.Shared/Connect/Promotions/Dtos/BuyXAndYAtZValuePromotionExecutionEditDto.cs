﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class BuyXAndYAtZValuePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public int ProductGroupId { get; set; }
        public int CategoryId { get; set; }
        public string ProductGroupName { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public string CategoryName { get; set; }
    }
}
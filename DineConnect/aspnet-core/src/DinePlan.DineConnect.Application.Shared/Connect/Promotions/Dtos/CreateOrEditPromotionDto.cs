﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Promotions.Dtos;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class CreateOrEditPromotionDto : ConnectOrgLocEditDto
    {
        public CreateOrEditPromotionDto()
        {
            LocationGroup = new CommonLocationGroupDto();
            PromotionSchedules = new List<PromotionScheduleEditDto>();
            PromotionRestrictItems = new List<PromotionRestrictItemEditDto>();
            PromotionQuotas = new List<PromotionQuotaDto>();
            ConnectCardTypeId = 0;
            CombineAllPro = false;
            Active = true;
        }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public int? Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Code { get; set; }

        public string RefCode { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int PromotionTypeId { get; set; }

        public string Filter { get; set; }

        public int OrganizationId { get; set; }

        public List<PromotionScheduleEditDto> PromotionSchedules { get; set; }
        public List<PromotionQuotaDto> PromotionQuotas{ get; set; }

        public List<PromotionRestrictItemEditDto> PromotionRestrictItems { get; set; }

        public bool Active { get; set; }
        public int SortOrder { get; set; }
        public string PromotionContents { get; set; }
        public bool Limi1PromotionPerItem { get; set; }
        public bool HasQuota { get; set; }
        public bool CombineAllPro { get; set; }
        public string CombineProValues { get; set; }
        public int? ConnectCardTypeId { get; set; }
        public int? PromotionCategoryId { get; set; }
        public bool DisplayPromotion { get; set; }
        public int PromotionPosition { get; set; }

        public void AddSchedule(int startHour, int startMinute, int endHour, int endMinute)
        {
            PromotionSchedules.Add(new PromotionScheduleEditDto
            {
                StartHour = startHour,
                StartMinute = startMinute,
                EndHour = endHour,
                EndMinute = endMinute
            });
        }

        public bool IgnorePromotionLimitation { get; set; }
        public bool ApplyOffline { get; set; }
        public string Description { get; set; }
    }
}
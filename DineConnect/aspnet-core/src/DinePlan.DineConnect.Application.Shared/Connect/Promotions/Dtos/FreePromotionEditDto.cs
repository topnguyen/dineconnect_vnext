﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class FreePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public int ToCount { get; set; }
        public Collection<FreePromotionExecutionEditDto> FreePromotionExecutions { get; set; }
        public Collection<FreePromotionExecutionEditDto> From { get; set; }
        public Collection<FreePromotionExecutionEditDto> To { get; set; }

        public FreePromotionEditDto()
        {
            FreePromotionExecutions = new Collection<FreePromotionExecutionEditDto>();
            From = new Collection<FreePromotionExecutionEditDto>();
            To = new Collection<FreePromotionExecutionEditDto>();
            FromCount = 1;
            ToCount = 1;
        }
    }
}
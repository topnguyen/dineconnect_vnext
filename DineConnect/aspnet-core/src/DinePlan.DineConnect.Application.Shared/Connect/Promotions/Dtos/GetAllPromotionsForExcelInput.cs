﻿namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class GetAllPromotionsForExcelInput
    {
        public string Filter { get; set; }
    }
}
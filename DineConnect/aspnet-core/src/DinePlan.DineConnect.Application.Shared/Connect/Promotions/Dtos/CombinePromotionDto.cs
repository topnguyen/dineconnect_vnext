﻿namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class CombinePromotionDto
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}
}
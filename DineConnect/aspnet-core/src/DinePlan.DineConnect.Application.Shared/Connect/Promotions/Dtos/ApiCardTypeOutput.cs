﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Promotions.Dtos
{
	public class ApiCardTypeOutput : IOutputDto
	{
		public bool Valid { get; set; }
		public string Reason { get; set; }
	}
}
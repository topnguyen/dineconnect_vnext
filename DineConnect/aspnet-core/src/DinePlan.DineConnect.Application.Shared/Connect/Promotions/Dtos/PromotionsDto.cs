﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using DinePlan.DineConnect.Connect.Master.Locations;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    public class PromotionsDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string RefCode { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int PromotionTypeId { get; set; }
        public string PromotionType { get; set; }

        public string StartDateString => StartDate.ToString("yyyy-MM-dd");
        public string EndDateString => EndDate.ToString("yyyy-MM-dd");
        public DateTime CreateDateTime { get; set; }
        public bool Limi1PromotionPerItem { get; set; }
        public bool Active { get; set; }
        public bool IsDeleted { get; set; }

        public string Locations { get; set; }

        public string LocationsName
        {
            get
            {
                if (!string.IsNullOrEmpty(Locations))
                {
                    var locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(Locations);
                    if (locations != null && locations.Any())
                        return locations.Select(l => l.Name).JoinAsString(",");
                    return "";
                }

                return "";
            }
        }

        public Collection<PromotionScheduleEditDto> PromotionSchedules { get; set; }
        public Collection<PromotionRestrictItemEditDto> PromotionRestrictItems { get; set; }
        public int SortOrder { get; set; }
    }
}
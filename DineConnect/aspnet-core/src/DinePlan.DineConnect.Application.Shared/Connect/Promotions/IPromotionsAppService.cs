﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Promotions.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Promotions
{
    public interface IPromotionsAppService : IApplicationService
    {
        Task<PagedResultDto<PromotionsDto>> GetAll(GetAllPromotionsInput input);

        Task<GetPromotionForEditOutput> GetPromotionForEdit(NullableIdDto input);

        Task CreateOrEdit(GetPromotionForEditOutput input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPromotionsToExcel(GetAllPromotionsForExcelInput input);

        ListResultDto<ComboboxItemDto> GetPromotionTypesForLookupTable();

        ListResultDto<ComboboxItemDto> GetDaysForLookupTable();

        ListResultDto<ComboboxItemDto> GetPromotionValueTypes();

        Task<ListResultDto<ComboboxItemDto>> GetSimplePortionsForCombobox();

        Task<ListResultDto<ComboboxItemCustomizeDto>> GetCategoriesForCombobox();

        Task UpdateQuotaUsage(int ticketId);

        Task<TicketDiscountPromotionDto> ApiTicketDiscountPromotion(ApiPromotionDefinitionInput input);

        Task<FreeItemPromotionEditDto> ApiGetFreeItemPromotion(ApiPromotionDefinitionInput input);

        Task<ApiPromotionOutput> ApiGetPromotions(ApiLocationInput input);

        Task<ApiTimerPromotionOutput> ApiGetTimerPromotions(ApiPromotionDefinitionInput input);

        Task<FreePromotionEditDto> ApiGetFreePromotion(ApiPromotionDefinitionInput input);

        Task<FreeValuePromotionEditDto> ApiGetFreeValuePromotion(ApiPromotionDefinitionInput input);

        Task<List<ApiFixedPromotionOutput>> ApiGetFixedPromotion(ApiPromotionDefinitionInput input);

        Task<DemandPromotionEditDto> ApiGetDemandPromotion(ApiPromotionDefinitionInput input);

        Task<ApiCardTypeOutput> ApiValidateCard(ApiCardTypeInput input);

        [AbpAuthorize]
        Task<ApiPromotionStatusUpdate> ApiUpdatePromotionStatus(ApiPromotionStatusUpdate input);

        Task<ApiCardTypeOutput> ApiUpdateCard(ApiCardTypeInput input);

        Task<ApiCardTypeOutput> ApiReverseAppliedCard(ApiCardTypeInput input);

        Task<PagedResultDto<CombinePromotionDto>> GetPromotionsCombine(GetQuotaDetailInput input);

        ListResultDto<ComboboxItemDto> GetPromotionPositions();

        Task<PromotionQuotaDto> ApiGetPromotionQuota(ApiPromotionDefinitionInput input);

        Task<GetComboboxForPromotionTypeOutput> GetComboboxByPromotionType(PromotionTypes input);

        Task SaveSortOrder(int[] all);
        Task ActivateItem(EntityDto input);
    }
}
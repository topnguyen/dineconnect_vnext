﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Print.Job.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Print.Job
{
    public interface IPrintJobAppService
    {
        Task<PagedResultDto<GetAllPrintJobDto>> GetAll(GetAllPrintJobInput input);

        Task<FileDto> ExportToExcel();

        Task<EditPrintJobInput> GetForEdit(int id);

        Task<int> Create(CreatePrintJobInput input);

        Task<int> Edit(EditPrintJobInput input);

        Task Delete(int id);

        Task<PagedResultDto<GetAllPrintJobMappingDto>> GetAllMappings(GetAllPrintJobMappingInput input);

        Task<CreateOrEditPrintJobMappingInput> GetMappingForEdit(int id);

        Task<int> CreateOrUpdateMapping(CreateOrEditPrintJobMappingInput input);

        Task DeleteMapping(int id);

        ListResultDto<ComboboxItemDto> GetWhatToPrints();
        Task ActivateItem(EntityDto input);
    }
}
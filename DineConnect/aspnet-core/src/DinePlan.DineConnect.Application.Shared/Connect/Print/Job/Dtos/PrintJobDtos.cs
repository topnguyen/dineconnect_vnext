﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Job.Dtos
{
    public class GetAllPrintJobInput : CommonLocationGroupFilterInputDto
	{
		public string Filter { get; set; }

		public bool IsDeleted { get; set; }
	}

	public class GetAllPrintJobDto : CreationAuditedEntityDto<int>
	{
		public string Name { get; set; }
	}

	// --

	public class CreatePrintJobInput
	{
		public string Name { get; set; }
	}

	public class EditPrintJobInput
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}

	// --

	public class GetAllPrintJobMappingInput : CommonLocationGroupFilterInputDto
	{
		public int PrintConfigurationId { get; set; }
	}

	public class GetAllPrintJobMappingDto : EntityDto<int>
	{
		public int WhatToPrint { get; set; }

		public bool Negative { get; set; }

		public bool ExcludeTax { get; set; }

		public string JobGroup { get; set; }

		public string WhatToPrintType { get; set; }
		public string AliasName { get; set; }
	}

	// --

	public class CreateOrEditPrintJobMappingInput : ConnectOrgLocEditDto
	{
		public int? Id { get; set; }

		public int PrintConfigurationId { get; set; }

		public int WhatToPrint { get; set; }
		public string AliasName { get; set; }

		public bool Negative { get; set; }

		public bool ExcludeTax { get; set; }

		public string JobGroup { get; set; }

		public CommonLocationGroupDto LocationGroup { get; set; } = new CommonLocationGroupDto();

		public List<CreateOrEditPrinterMapInput> PrinterMapInputs { get; set; } = new List<CreateOrEditPrinterMapInput>();
	}

	public class CreateOrEditPrinterMapInput : EntityDto<int?>
	{
		public int CategoryId { get; set; }

		public int MenuItemId { get; set; }

		public int PrinterId { get; set; }
		public string AliasName { get; set; }

		public int PrintTemplateId { get; set; }
		public int? DepartmentId { get; set; }
		public string DepartmentName { get; set; }

		public string CategoryName { get; set; }

		public string MenuItemName { get; set; }
	}

	public class ApiPrintJobOutput
	{
		public ApiPrintJobOutput()
		{
			PrintJobs = new List<ApiPrintJob>();
		}

		public IList<ApiPrintJob> PrintJobs { get; set; }
	}

	public class ApiPrintJob
	{
		public ApiPrintJob()
		{
			PrinterMaps = new List<ApiPrinterMap>();
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public int WhatToPrint { get; set; }
		public bool Negative { get; set; }
		public bool ExcludeTax { get; set; }
		public string JobGroup { get; set; }
		public IList<ApiPrinterMap> PrinterMaps { get; set; }
	}

	public class ApiPrinterMap
	{
		public int Id { get; set; }
		public string MenuItemGroupCode { get; set; }
		public int PrintJobId { get; set; }
		public int MenuItemId { get; set; }
		public int PrinterId { get; set; }
		public int PrintTemplateId { get; set; }
		public string CategoryName { get; set; }
		public int DepartmentId { get; set; }
	}
}
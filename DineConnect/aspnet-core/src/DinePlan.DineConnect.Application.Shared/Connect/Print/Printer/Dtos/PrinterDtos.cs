﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.Printers.Dtos
{
	public class GetAllPrinterInput : CommonLocationGroupFilterInputDto
	{
		public string Filter { get; set; }

		public bool IsDeleted { get; set; }
	}

	public class GetAllPrinterDto : CreationAuditedEntityDto<int>
	{
		public string Name { get; set; }
	}

	// --

	public class CreatePrinterInput
	{
		public string Name { get; set; }
	}

	public class EditPrinterInput
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}

	// --

	public class GetAllPrinterTemplateInput : CommonLocationGroupFilterInputDto
	{
		public int PrintConfigurationId { get; set; }
	}

	public class GetAllPrinterMappingDto : EntityDto<int>
	{
		public int PrinterType { get; set; }
		public string PrinterTypeName { get; set; }
		public string AliasName { get; set; }

		public int CodePage { get; set; }
		public int CharsPerLine { get; set; }
		public int NumberOfLines { get; set; }
		public int FallBackPrinter { get; set; }
		public string FallBackPrinterName { get; set; }
		public string CharReplacement { get; set; }
	}

	// --

	public class CreateOrEditPrinterMappingInput : ConnectOrgLocEditDto
	{
		public int? Id { get; set; }
		public string AliasName { get; set; }

		public int PrintConfigurationId { get; set; }

		public int PrinterType { get; set; }

		public int CodePage { get; set; }

		public int CharsPerLine { get; set; }

		public int NumberOfLines { get; set; }

		public int? FallBackPrinter { get; set; }

		public string CharReplacement { get; set; }

		public string CustomPrinterName { get; set; }

		public string CustomPrinterData { get; set; }

		public CommonLocationGroupDto LocationGroup { get; set; } = new CommonLocationGroupDto();
	}

	public class ApiPrinterOutput
	{
		public ApiPrinterOutput()
		{
			Printers = new List<ApiPrinterDto>();
		}

		public IList<ApiPrinterDto> Printers { get; set; }
	}

	public class ApiPrinterDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int PrinterType { get; set; }
		public int CodePage { get; set; }
		public int CharsPerLine { get; set; }
		public int NumberOfLines { get; set; }
		public string CustomPrinterName { get; set; }
		public string CustomPrinterData { get; set; }
		public string CharReplacement { get; set; }
		public int FallBackPrinter { get; set; }
	}
}
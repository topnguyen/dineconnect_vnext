﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Print.Printers.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Print.Printers
{
    public interface IPrinterAppService
    {
        Task<PagedResultDto<GetAllPrinterDto>> GetAll(GetAllPrinterInput input);

        Task<ListResultDto<ComboboxItemDto>> GetPrinterProcessors();

        Task<List<FormlyType>> GetPrinterProcessorSetting(string stringId);

        Task<FileDto> ExportToExcel();

        Task<EditPrinterInput> GetForEdit(int id);

        Task<int> Create(CreatePrinterInput input);

        Task<int> Edit(EditPrinterInput input);

        Task Delete(int id);

        Task<PagedResultDto<GetAllPrinterMappingDto>> GetAllPrinterTemplates(GetAllPrinterTemplateInput input);

        Task<int> CreateOrUpdatePrinterTemplate(CreateOrEditPrinterMappingInput input);

        Task DeletePrinterTemplate(int id);

        ListResultDto<ComboboxItemDto> GetPrinterTypes();

        Task<ListResultDto<ComboboxItemDto>> GetFallBackPrinters(int? id);

        Task<ListResultDto<ComboboxItemDto>> GetPrintersForComboBox();

        Task<ListResultDto<ComboboxItemDto>> GetPrinterTemplatesForComboBox();
		Task<ApiPrinterOutput> ApiPrinter(ApiLocationInput locationInput);
        Task ActivateItem(EntityDto input);
    }
}
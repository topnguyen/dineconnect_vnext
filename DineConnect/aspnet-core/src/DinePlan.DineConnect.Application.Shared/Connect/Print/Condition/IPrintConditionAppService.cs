﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Print.Condition.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Print.Condition
{
    public interface IPrintConditionAppService
    {
        Task<PagedResultDto<GetAllPrintConditionDto>> GetAll(GetAllPrintConditionInput input);

        Task<FileDto> ExportToExcel();

        Task<EditPrintConditionInput> GetForEdit(int id);

        Task<int> Create(CreatePrintConditionInput input);

        Task<int> Edit(EditPrintConditionInput input);

        Task Delete(int id);

        Task<PagedResultDto<GetAllPrintConditionMappingDto>> GetAllMappings(GetAllPrintConditionMappingInput input);

        Task<CreateOrEditPrintConditionMappingInput> GetMappingForEdit(int id);

        Task<int> CreateOrUpdateMapping(CreateOrEditPrintConditionMappingInput input);

        Task DeleteMapping(int id);

        ListResultDto<ComboboxItemDto> GetWhatToPrints();
		Task<ApiPrinterConditionOutput> ApiPrinterTemplateCondition(ApiLocationInput locationInput);
        Task ActivateItem(EntityDto input);
    }
}
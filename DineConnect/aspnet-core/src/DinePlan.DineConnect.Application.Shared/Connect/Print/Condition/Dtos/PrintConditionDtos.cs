﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Extensions;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Print.Condition.Dtos
{
	public class GetAllPrintConditionInput : CommonLocationGroupFilterInputDto
	{
		public string Filter { get; set; }

		public bool IsDeleted { get; set; }
	}

	public class GetAllPrintConditionDto : CreationAuditedEntityDto<int>
	{
		public string Name { get; set; }
	}

	// --

	public class CreatePrintConditionInput
	{
		public string Name { get; set; }
	}

	public class EditPrintConditionInput
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}

	// --

	public class GetAllPrintConditionMappingInput : CommonLocationGroupFilterInputDto
	{
		public int PrintConfigurationId { get; set; }
	}

	public class GetAllPrintConditionMappingDto : EntityDto<int>
	{
		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }

		public string Contents { get; set; }
		public string AliasName { get; set; }
	}

	// --

	public class CreateOrEditPrintConditionMappingInput : ConnectOrgLocEditDto
	{
		public int? Id { get; set; }

		public int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }

		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }

		[Required]
		public string Contents { get; set; }

		[Required]
		public string Filter { get; set; }

		[Required]
		public string ConditionSchedules { get; set; }

		public List<PrintConditionMappingScheduleEditDto> ConditionScheduleList { get; set; } = new List<PrintConditionMappingScheduleEditDto>();

		public CommonLocationGroupDto LocationGroup { get; set; } = new CommonLocationGroupDto();
	}

	public class PrintConditionMappingScheduleEditDto
	{
		public int? Id { get; set; }

		public int StartHour { get; set; }
		public int StartMinute { get; set; }

		public int EndHour { get; set; }
		public int EndMinute { get; set; }

		public string Days { get; set; }

		public string MonthDays { get; set; }

		public string DayNames
		{
			get
			{
				if (!Days.IsNullOrEmpty())
				{
					var days = Days.Split(",").ToList();
					return days.Select(x => (Days)int.Parse(x)).JoinAsString(",");
				}

				return null;
			}
		}
	}

	public class ApiPrinterConditionOutput
	{
		public ApiPrinterConditionOutput()
		{
			PrintTemplateConditions = new List<ApiPrinterTemplateConditionDto>();
		}

		public IList<ApiPrinterTemplateConditionDto> PrintTemplateConditions { get; set; }
	}

	public class ApiPrinterTemplateConditionDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		public string ConditionSchedules { get; set; }
		public string Filter { get; set; }
		public string Contents { get; set; }
	}
}
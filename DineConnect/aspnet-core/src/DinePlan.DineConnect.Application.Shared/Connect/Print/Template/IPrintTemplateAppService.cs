﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Print.Template.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Print.Template
{
    public interface IPrintTemplateAppService
    {
        Task<PagedResultDto<GetAllPrintTemplateDto>> GetAll(GetAllPrintTemplateInput input);

        Task<FileDto> ExportToExcel();

        Task<EditPrintTemplateInput> GetForEdit(int id);

        Task<int> Create(CreatePrintTemplateInput input);

        Task<int> Edit(EditPrintTemplateInput input);

        Task Delete(int id);

        Task<PagedResultDto<GetAllPrintTemplateMappingDto>> GetAllMappings(GetAllPrintTemplateMappingInput input);

        Task<CreateOrEditPrintTemplateMappingInput> GetMappingForEdit(int id);

        Task<int> CreateOrUpdateMapping(CreateOrEditPrintTemplateMappingInput input);

        Task DeleteMapping(int id);
		Task<ApiPrinterTemplateOutput> ApiPrinterTemplate(ApiLocationInput locationInput);
        Task ActivateItem(EntityDto input);
    }
}
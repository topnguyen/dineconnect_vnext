﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Print.Template.Dtos
{
	public class GetAllPrintTemplateInput : CommonLocationGroupFilterInputDto
	{
		public string Filter { get; set; }

		public bool IsDeleted { get; set; }
	}

	public class GetAllPrintTemplateDto : CreationAuditedEntityDto<int>
	{
		public string Name { get; set; }
	}

	// --

	public class CreatePrintTemplateInput
	{
		public string Name { get; set; }
	}

	public class EditPrintTemplateInput
	{
		public int Id { get; set; }

		public string Name { get; set; }
	}

	// --

	public class GetAllPrintTemplateMappingInput : CommonLocationGroupFilterInputDto
	{
		public int PrintConfigurationId { get; set; }
	}

	public class GetAllPrintTemplateMappingDto : EntityDto<int>
	{
		public string AliasName { get; set; }
		public string Contents { get; set; }
		public bool MergeLines { get; set; }
	}

	// --

	public class CreateOrEditPrintTemplateMappingInput : ConnectOrgLocEditDto
	{
		public int? Id { get; set; }

		public int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }

		[Required]
		public string Contents { get; set; }

		public bool MergeLines { get; set; }
		public string Files { get; set; }

		public CommonLocationGroupDto LocationGroup { get; set; } = new CommonLocationGroupDto();
	}

	public class ApiPrinterTemplateOutput
	{
		public ApiPrinterTemplateOutput()
		{
			PrinterTemplates = new List<ApiPrinterTemplateDto>();
		}

		public IList<ApiPrinterTemplateDto> PrinterTemplates { get; set; }
	}

	public class ApiPrinterTemplateDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public bool MergeLines { get; set; }
		public string Contents { get; set; }
		public string Files { get; set; }
	}
}
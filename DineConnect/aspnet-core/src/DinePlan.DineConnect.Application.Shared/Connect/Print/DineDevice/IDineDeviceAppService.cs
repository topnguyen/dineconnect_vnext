﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Print.DineDevice.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Print.DineDevice
{
    public interface IDineDeviceAppService : IApplicationService
    {
        Task<PagedResultDto<DineDeviceListDto>> GetAll(GetDineDeviceInput inputDto);
        
        Task<FileDto> GetAllToExcel(GetDineDeviceInput input);
        
        Task<CreateOrUpdateDineDeviceInput> GetDineDeviceForEdit(NullableIdDto input);
        
        Task CreateOrUpdateDineDevice(CreateOrUpdateDineDeviceInput input);
        
        Task DeleteDineDevice(EntityDto input);
        
        ListResultDto<ComboboxItemDto> GetDineDeviceTypes();

        Task<List<FormlyType>> GetDineDeviceTypeSetting(EntityDto input);
    }
}

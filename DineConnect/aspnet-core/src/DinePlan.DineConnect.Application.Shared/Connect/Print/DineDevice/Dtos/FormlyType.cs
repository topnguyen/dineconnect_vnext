﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Print.DineDevice.Dtos
{
    public class FormlyType
    {
        public FormlyType()
        {
            TemplateOptions = new FormlyTemplateOption();
            ClassName = "";
        }
        public string Key { get; set; }
        public string Type { get; set; }
        public string ClassName { get; set; }
        public FormlyTemplateOption TemplateOptions { get; set; }
    }

    public class FormlyTemplateOption
    {
        public FormlyTemplateOption()
        {
            Type = "text";
            Label = "";
            Placeholder = "";
            Pattern = "";
            BtnText = "";
            Min = "";
            Max = "";
        }
        public string Min { get; set; }
        public string Max{ get; set; }
        public string Label { get; set; }
        public string Placeholder { get; set; }
        public string Type { get; set; }
        public string Pattern { get; set; }
        public string BtnText { get; set; }

        public List<Field> Fields { get; set; }
    }
    public class Field
    {
        public Field()
        {
            ClassName = "";
            FieldGroup = new List<FormlyType>();
        }
        public string ClassName { get; set; }
        public List<FormlyType> FieldGroup { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.DineDevices;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Print.DineDevice.Dtos
{
    public class DineDeviceListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string Settings { get; set; }

        public DineDeviceType DeviceType { get; set; } // 0-> DineChef 1->DineQueue

        public string DineDeviceTypeName => (DeviceType).ToString();
    }

    public class DineDeviceEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Settings { get; set; }
        public int DeviceType { get; set; } // 0-> DineChef 1->DineQueue
        public string DineDeviceTypeName { get; set; }
        public int OrganizationId { get; set; }
    }

    public class GetDineDeviceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class GetDineDeviceForEditOutput : IOutputDto
    {
        public DineDeviceEditDto DineDevice { get; set; }
    }

    public class CreateOrUpdateDineDeviceInput : IInputDto
    {
        public CreateOrUpdateDineDeviceInput()
        {
            DineDevice = new DineDeviceEditDto();
            LocationGroup = new CommonLocationGroupDto();
        }

        [Required]
        public DineDeviceEditDto DineDevice { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }
    }
}
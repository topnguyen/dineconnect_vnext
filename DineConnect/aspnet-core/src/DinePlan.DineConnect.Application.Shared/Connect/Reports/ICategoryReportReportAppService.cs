﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
	public interface ICategoryReportAppService : IApplicationService
	{
		Task<CategoryStatsDto> BuildCategorySalesReport(GetItemInput input);
		Task<FileDto> BuildCategoryExcel(GetItemInput input);
	}
}
﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
	public interface ISaleSummaryReportAppService : IApplicationService
	{
		Task<SaleSummaryStatsDto> BuildSaleSummaryReport(GetTicketInput input);
		Task<FileDto> BuildSaleSummaryExcel(GetTicketInput input);
		Task<ConsolidatedDto> GetConsolidated(GetTicketInput input);
		Task<FileDto> BuildConsolidatedExport(GetTicketInput input);
		Task<FileDto> BuildLocationComparisonReportToExcel(GetTicketInput input);

	}
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports
{
    public interface IItemReportAppService : IApplicationService
    {
        Task<ItemSummaryReportOutput> BuildItemSalesReport(GetItemInput input);

        Task<ItemStatsDto> BuildItemHourlySales(GetItemInput input);

        Task<FileDto> BuildItemHourlyReportExcel(GetItemInput input);

        Task<FileDto> ExportItemComboSalesToExcel(GetItemInput input);

        Task<FileDto> ExportItemsAndOrderTagToExcel(GetItemInput input);

        Task<FileDto> ExportItemSalesByPriceToExcel(GetItemInput input);

        Task<FileDto> ExportItemsPortionToExcel(GetItemInput input);

        Task<FileDto> ExportItemsExcel(GetItemInput input);

        Task<ItemTagStatsDto> BuildItemTagSalesReport(GetItemInput input);

        Task<ItemStatsDto> BuldTopOrBottomItemSales(GetItemInput input);
        Task<FileDto> ExportItemTagSalesToExcel(GetItemInput input);

        Task<ItemStatsDto> BuildItemSalesByDepartment(GetItemInput input);

        Task<FileDto> ExportItemSalesByDepartmentExcel(GetItemInput input);
    }
}
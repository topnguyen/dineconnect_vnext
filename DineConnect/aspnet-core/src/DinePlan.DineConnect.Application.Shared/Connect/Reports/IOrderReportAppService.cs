﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
	public interface IOrderReportAppService : IApplicationService
	{
		Task<OrderStatsDto> BuildOrderReport(GetItemInput input);

		Task<FileDto> BuildOrderReportExcel(GetItemInput input);
	}
}
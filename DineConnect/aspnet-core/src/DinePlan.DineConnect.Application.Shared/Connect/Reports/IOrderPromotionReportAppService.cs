﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
	public interface IOrderPromotionReportAppService : IApplicationService
	{
		Task<OrderStatsDto> BuildOrderPromotionReport(GetItemInput input);
		Task<FileDto> BuildOrderPromotionExcel(GetItemInput input);
	}
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Reports
{
    public interface ITicketSyncReportAppService : IApplicationService
    {
        Task<PagedResultDto<GetTicketSyncOutput>> GetTicketSyncs(GetTicketSyncInput input);

        Task<FileDto> GetTicketSyncToExcel(GetTicketSyncInput input);
    }
}

﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class CategoryStatsDto
    {
        public PagedResultDto<CategoryReportDto> CategoryList { get; set; }
        public List<string> Categories { get; set; }
        public List<BarChartOutputDto> ChartOutput { get; set; }
    }
}

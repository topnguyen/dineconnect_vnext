﻿namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class DinePlanLogTypes
    {
        public static string BACKOFFICEMENUOPENED = "BACK OFFICE MENU OPENED";
        public static string CANCELCOMP = "CANCEL COMP";
        public static string CANCELGIFT = "CANCEL GIFT";
        public static string CANCELORDER = "CANCEL ORDER";
        public static string CANCELVOID = "CANCEL VOID";
        public static string CLEARDATA = "CLEAR DATA";
        public static string CLEARMENU = "CLEAR MENU";
        public static string CLEARORDERS = "CLEAR ORDERS";
        public static string CLEARTABLES = "CLEAR TABLES";
        public static string CLOSETICKET = "CLOSE TICKET";
        public static string COMP = "COMP";
        public static string DISPLAYTICKET = "DISPLAY TICKET";
        public static string GIFT = "GIFT";
        public static string GIFTALL = "GIFT ALL";
        public static string LOGIN = "USER LOGIN";
        public static string LOGOUT = "USER LOGOUT";
        public static string MODELSAVED = "MODEL SAVED";
        public static string MODULECLICKED = "MODULE CLICKED";
        public static string NEWORDER = "NEW ORDER";
        public static string OPENTILL = "OPEN TILL";
        public static string ORDERMOVED = "ORDER MOVED";
        public static string PAYMENT = "PAYMENT";
        public static string PAYTICKET = "PAY TICKET";
        public static string PRINTBILL = "PRINT BILL";
        public static string PRINTDUPLICATETICKET = "PRINT DUPLICATE TICKET";
        public static string PRINTLASTTICKET = "PRINT LAST TICKET";
        public static string REOPENTICKET = "REOPEN TICKET";
        public static string REPRINTKITCHEN = "REPRINT KITCHEN";
        public static string TICKETENTITYCHANGE = "TICKET ENTITY CHANGED";
        public static string TICKETSMERGED = "TICKETS MERGED";
        public static string UNLOCKTICKET = "UNLOCK TICKET";
        public static string USERPASSWORDCHANGED = "USER PASSWORD CHANGED";
        public static string SECURITYCODECHANGED = "SECURITY CODE CHANGED";

        public static string VOID = "VOID";
        public static string VOIDALL = "VOID ALL";

        public static string PREORDER = "PRE-ORDER";
        public static string NONPREORDER = "NON PRE-ORDER";

        public static string REFUNDTICKET = "REFUND TICKET";
    }
}

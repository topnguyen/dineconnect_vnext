﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class DashboardPaymentDto
    {
        public DashboardPaymentDto()
        {
            Dates = new List<string>();
            Totals = new List<decimal>();
            TicketCounts = new List<int>();
        }
        public List<string> Dates { get; set; }
        public List<decimal> Totals { get; set; }
        public List<int> TicketCounts { get; set; }
    }
}

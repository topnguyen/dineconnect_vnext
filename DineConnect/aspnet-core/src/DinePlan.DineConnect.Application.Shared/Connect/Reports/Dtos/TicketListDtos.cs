﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using Castle.Core.Internal;
using DinePlan.DineConnect.Connect.Master.Tickets.Dtos;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Helper;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class TicketStatsDto
    {
        public PagedResultDto<TicketListDto> TicketList { get; set; }

        public PagedResultDto<TicketViewDto> TicketViewList { get; set; }

        public DashboardTicketDto DashBoardDto { get; set; }
    }

    public class TicketHourListDto
    {
        public List<TicketHourDto> HourList { get; set; }
        public List<decimal> ChartTotal { get; set; }
        public List<int> ChartTicket { get; set; }
    }

    public class IdHourListDto
    {
        public List<IdHourDto> HourList { get; set; }
    }

    public class IdLocationHourListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<IdHourDto> HourList { get; set; }

        public IdLocationHourListDto()
        {
            HourList = new List<IdHourDto>();
        }
    }

    public class IdDayItemOutput
    {
        public List<IdDayItemOutputDto> Items { get; set; }
        public List<IdDayItemOutputDto> OrderTags { get; set; }
        public List<string> AllDates { get; set; }
        public Dictionary<string, string> AllLocations { get; set; }

        public IdDayItemOutput()
        {
            OrderTags = new List<IdDayItemOutputDto>();
            Items = new List<IdDayItemOutputDto>();
            AllDates = new List<string>();
            AllLocations = new Dictionary<string, string>();
        }
    }

    public class IdDayItemOutputDto
    {
        public string Name => MenuItemName + ":" + PortionName;
        public string MenuItemName { get; set; }
        public string LocationName { get; set; }
        public string PortionName { get; set; }
        public int MenuItemId { get; set; }
        public int PortionId { get; set; }

        public IdDayItemOutputDto()
        {
            Items = new List<DayWiseItemDto>();
        }

        public List<DayWiseItemDto> Items { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string AliasCode { get; set; }
        public bool IsOrderTag { get;  set; }
    }

    public class DayWiseItemDto
    {
        public String Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.###}")]
        public decimal Quantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.###}")]
        public decimal Total => Quantity * Price;

        [DisplayFormat(DataFormatString = "{0:#.###}")]
        public decimal Price { get; set; }
    }

    public class HourDto
    {
        public List<HourTotalDto> Hours { get; set; }
        public decimal Total { get; set; }
        public decimal Tickets { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && Tickets > 0M)
                {
                    return decimal.Round(Total / Tickets, 2, MidpointRounding.AwayFromZero);
                }
                return 0M;
            }
        }
    }

    public class TicketHourDto : HourDto
    {
        public DateTime Date { get; set; }
    }

    public class IdHourDto : HourDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class HourTotalDto
    {
        public int Hour { get; set; }
        public decimal Total { get; set; }
        public int Tickets { get; set; }
        public decimal Discount { get; set; }
        public int Taxable { get; set; }
        public int TotalTax { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && Tickets > 0M)
                {
                    return decimal.Round(Total / Tickets, 2, MidpointRounding.AwayFromZero);
                }
                return 0M;
            }
        }
    }

    public class OrderStatsDto
    {
        public PagedResultDto<OrderListDto> OrderList { get; set; }
        public PagedResultDto<OrderViewDto> OrderViewList { get; set; }
        public DashboardOrderDto DashBoardDto { get; set; }
    }

    public class OrderExchangeDto
    {
        public PagedResultDto<OrderExchangeViewDto> OrderViewList { get; set; }
        public DashboardOrderDto DashBoardDto { get; set; }
    }

    public class ItemStatsDto
    {
        public PagedResultDto<MenuListDto> MenuList { get; set; }

        public List<ChartOutputDto> Quantities { get; set; }
        public int Quantity { get; set; }

        public List<ChartOutputDto> Totals { get; set; }
        public int Total { get; set; }

        public decimal TotalQuantity { get; set; }
        public decimal TotalResult { get; set; }
    }

    public class WorkPeriodItemSales
    {
        public int MenuItemPortionId { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total => Quantity * Price;
        public string Category { get; set; }
    }

    public class AllItemSalesDto
    {
        public List<MenuListDto> Sales { get; set; }
        public List<MenuListDto> Comp { get; set; }
        public List<MenuListDto> Void { get; set; }
        public List<MenuListDto> Gift { get; set; }
    }

    public class PortionSalesOutput
    {
        public List<PortionSales> Portions { get; set; }
        public List<string> PortionNames { get; set; }

        public PortionSalesOutput()
        {
            Portions = new List<PortionSales>();
            PortionNames = new List<string>();
        }
    }

    public class PortionSales
    {
        public int MenuItemId { get; set; }
        public int CategoryId { get; set; }

        public string Category { get; set; }
        public string MenuItemName { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public List<PortionSalesDetail> Details { get; set; }

        public PortionSales()
        {
            Details = new List<PortionSalesDetail>();
        }

        public void AddToPortion(int pId, decimal quantity, decimal price, decimal aPrice, string name)
        {
            var anyP = Details.FirstOrDefault(a => a.PortionId == pId);
            if (anyP != null)
            {
                anyP.PortionPrice = aPrice;
                anyP.Price = ((anyP.Price * anyP.TotalQuantity) + (quantity * price)) / (anyP.TotalQuantity + quantity);
                anyP.TotalQuantity += quantity;
            }
            else
            {
                Details.Add(new PortionSalesDetail()
                {
                    PortionId = pId,
                    PortionName = name,
                    TotalQuantity = quantity,
                    PortionPrice = aPrice,
                    Price = price
                });
            }
        }
    }

    public class PortionSalesDetail
    {
        public int PortionId { get; set; }
        public string PortionName { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal PortionPrice { get; set; }
    }

    public class GroupStatsDto
    {
        public PagedResultDto<GroupReportDto> GroupList { get; set; }
        public List<string> Groups { get; set; }
        public List<BarChartOutputDto> ChartOutput { get; set; }
    }

    public class GroupReportDto
    {
        public List<CategoryReportDto> CategoryList { get; set; }

        public GroupReportDto()
        {
            MenuItems = new List<MenuReportView>();
            TotalItems = new List<MenuListDto>();
            CategoryList = new List<CategoryReportDto>();
        }

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<MenuListDto> TotalItems { get; set; }
        public List<MenuReportView> MenuItems { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Quantity { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Total { get; set; }
    }

    public class ItemTagStatsDto
    {
        public PagedResultDto<ItemTagReportDto> ItemTagList { get; set; }
        public List<string> ItemTags { get; set; }
        public List<BarChartOutputDto> ChartOutput { get; set; }
    }

    public class ItemTagReportDto
    {
        public string TagName { get; set; }
        public List<MenuListDto> TotalItems { get; set; }
        public decimal Quantity { get; set; }
        public decimal Total { get; set; }
    }

    public class DepartmentStatsDto
    {
        public PagedResultDto<DepartmentReportDto> DepartmentList { get; set; }
    }

    public class DepartmentSummaryDto
    {
        public string Location { get; set; }
        public string DepartmentName { get; set; }
        public Dictionary<int, decimal> Payments { get; set; }
        public Dictionary<int, decimal> Transactions { get; set; }

        public IEnumerable<IGrouping<int, PaymentEditDto>> AllPayments { get; set; }
        public IEnumerable<IGrouping<int, TicketTransactionListDto>> AllTransactions { get; set; }

        public DepartmentSummaryDto()
        {
            Payments = new Dictionary<int, decimal>();
            Transactions = new Dictionary<int, decimal>();

        }
    }

    public class DepartmentTallySummaryDto
    {
        public string Location { get; set; }
        public string DepartmentName { get; set; }
        public Dictionary<int, decimal> Payments { get; set; }
        public Dictionary<int, decimal> Transactions { get; set; }
        //public IEnumerable<IGrouping<int, Payment>> AllPayments { get; set; }
        //public IEnumerable<IGrouping<int, TicketTransaction>> AllTransactions { get; set; }

        public DepartmentTallySummaryDto()
        {
            Payments = new Dictionary<int, decimal>();
            Transactions = new Dictionary<int, decimal>();
        }
    }

    public class SaleSummaryStatsDto
    {
        public PagedResultDto<SaleSummaryDto> Sale { get; set; }
        public DashboardSaleDto Dashboard { get; set; }
    }

    public class PaymentSummaryStatsDto
    {
        public PagedResultDto<PaymentSummaryDto> Payment { get; set; }
        public DashboardPaymentDto Dashboard { get; set; }
    }

    public class TransactionSummaryStatsDto
    {
        public PagedResultDto<TransactionSummaryDto> Transaction { get; set; }
        public DashboardTransactionDto DashBoardDto { get; set; }
    }

    public class PaymentStatsDto
    {
        public PagedResultDto<GetPaymentOutput> Payments { get; set; }
        public DashboardTicketDto DashBoardDto { get; set; }
    }

    public class TransactionStatsDto
    {
        public PagedResultDto<GetTransactionOutput> Transactions { get; set; }
        public DashboardTicketDto DashBoardDto { get; set; }
    }

    public class TicketTransactionOutput
    {
        public List<TicketTransactionListDto> Transactions { get; set; }
    }

    public class TicketViewDto
    {
        public int Id { get; set; }
        public string LocationName { get; set; }
        public string LastModifiedUserName { get; set; }
        public int LocationId { get; set; }
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public decimal TotalAmount { get; set; }
        public string DepartmentName { get; set; }
        public string TerminalName { get; set; }
    }

    public class TicketListDto
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        /** Copy from here to the DinePlan Sync **/
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public DateTime TicketCreatedTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public DateTime LastOrderTime { get; set; }
        public DateTime LastPaymentTime { get; set; }
        public bool IsClosed { get; set; }
        public bool IsLocked { get; set; }
        public decimal RemainingAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DepartmentName { get; set; }
        public string TicketTypeName { get; set; }
        public string Note { get; set; }
        public string LastModifiedUserName { get; set; }
        public string TicketTags { get; set; }
        public string TicketStates { get; set; }
        public string TicketLogs { get; set; }
        public string ReferenceNumber { get; set; }
        public string ReferenceTicket { get; set; }

        public string InvoiceNo { get; set; }
        public bool TaxIncluded { get; set; }
        public string TerminalName { get; set; }
        public string TicketPromotionDetails { get; set; }

        public bool PreOrder { get; set; }
        public bool Credit { get; set; }
        public string TicketEntities { get; set; }
        /** End **/
        public List<OrderListDto> Orders { get; set; }
        public List<PaymentListDto> Payments { get; set; }
        public List<TicketTransactionListDto> Transactions { get; set; }

        /*Added for Excel Export*/
        public PairObject PaymentDetails { get; set; }
        public PairObject TransactionDetails { get; set; }
        public PairObject TicketTagDetails { get; set; }
        public IList<TicketTagValue> TicketTagValues => JsonHelper.Deserialize<List<TicketTagValue>>(TicketTags);

        public string Covers
        {
            get
            {
                var coverorPax = JsonHelper.Deserialize<List<TicketTagValue>>(TicketTags)
                    .FirstOrDefault(t => t.TagName.Equals("PAX") || t.TagName.Equals("COVERS"));
                if (coverorPax != null)
                {
                    return coverorPax.TagValue;
                }

                return "0";
            }
        }

        public decimal TicketPromotionAmount { get; set; }
    }

    public class OrderViewDto
    {
        public int OrderId { get; set; }
        public string LocationName { get; set; }
        public int Location_Id { get; set; }
        public int TicketId { get; set; }
        public string DepartmentName { get; set; }
        public string AliasCode { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public string CreatingUserName { get; set; }
        public decimal Quantity { get; set; }
    }

    //[AutoMapFrom(typeof(Order))]
    public class OrderExchangeViewDto
    {
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public string DateTime { get; set; }
        public string SellerName { get; set; }
        public string ReceiptNumber { get; set; }
        public string CustomerName { get; set; }
        public string Tender { get; set; }
        public string MaterialCode { get; set; }
        public string MaterialName { get; set; }
        public decimal Quantity { get; set; }
        public string ExchangeDate { get; set; }
        public string ExchangeBy { get; set; }
        public string Reason { get; set; }
        public string Detail { get; set; }
        public decimal Amount { get; set; }
    }

    public class OrderListDto
    {
        public int Id { get; set; }
        /**Copy from here to the DineSync **/
        public int OrderId { get; set; }
        public string LocationName { get; set; }
        public bool TaxIncluded { get; set; }
        public string TicketNumber { get; set; }

        public int Location_Id { get; set; }
        public int TicketId { get; set; }
        public string DepartmentName { get; set; }
        public string AliasCode { get; set; }
        public int MenuItemId { get; set; }
        public string MenuItemName { get; set; }

        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Quantity { get; set; }

        public decimal LineTotal => Quantity * Price;

        public int PortionCount { get; set; }
        public int PromotionSyncId { get; set; }
        public string Note { get; set; }
        public bool Locked { get; set; }
        public bool CalculatePrice { get; set; }
        public bool IncreaseInventory { get; set; }
        public bool DecreaseInventory { get; set; }
        public string OrderNumber { get; set; }
        public string CreatingUserName { get; set; }
        public DateTime OrderCreatedTime { get; set; }
        public string PriceTag { get; set; }
        public string Taxes { get; set; }
        public string OrderTags { get; set; }
        public string OrderStates { get; set; }
        public bool IsPromotionOrder { get; set; }
        public decimal PromotionAmount { get; set; }
        public int MenuItemPortionId { get; set; }
        public string TerminalName { get; set; }
        public string OrderLog { get; set; }

        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public virtual string OrderPromotionDetails { get; set; }

        public string PromotionName { get; set; }

        public decimal OriginalPrice { get; set; }
        public string Tags { get; set; }
        public virtual List<TransactionOrderTagReportDto> TransactionOrderTags { get; set; }

        public List<OrderStateValue> GetOrderState()
        {
            return JsonHelper.Deserialize<List<OrderStateValue>>(OrderStates);
        }

        private IList<TaxValue> _taxValues;
        public IList<TaxValue> TaxValues => _taxValues ?? (_taxValues = JsonHelper.Deserialize<List<TaxValue>>(Taxes));

        public decimal TaxPrice
        {
            get
            {
                decimal taxPrice = 0M;

                if (!TaxValues.Any())
                {
                    return 0M;
                }
                else
                {
                    foreach (var tax in TaxValues)
                    {
                        if (TaxIncluded)
                        {
                            var tRate = 100 + tax.TaxRate;
                            var taxVale = ((Price * Quantity) * tRate / 100) - (Price * Quantity);
                            taxPrice += taxVale;
                        }
                        else
                        {
                            taxPrice += ((Price * Quantity) * tax.TaxRate / 100);
                        }
                    }
                }
                return taxPrice;
            }
        }

        public List<PromotionDetailValue> GetPromotionDetails()
        {
            if (!string.IsNullOrEmpty(OrderPromotionDetails))
            {
                return JsonHelper.Deserialize<List<PromotionDetailValue>>(OrderPromotionDetails);
            }
            return null;
        }

        public string OperationUser
        {
            get
            {
                if (!CalculatePrice)
                {
                    var lastStates = GetOrderState();
                    if (lastStates != null && lastStates.Any())
                    {
                        var lastState = GetOrderState().LastOrDefault(a => a.StateName.Equals("GStatus"));
                        return !string.IsNullOrEmpty(lastState.UserName)
                                ? lastState.UserName
                                : lastState.UserId.ToString();
                    }
                }

                return "";
            }
        }

        public decimal GetTaxablePrice()
        {
            var result = Price + OrderTagValues.Where(x => !x.TaxFree && !x.AddTagPriceToOrderPrice).Sum(x => x.Price * x.Quantity);
            return result;
        }

        private IList<OrderTagValue> _orderTagValues;

        public IList<OrderTagValue> OrderTagValues =>
            _orderTagValues ?? (_orderTagValues = JsonHelper.Deserialize<List<OrderTagValue>>(OrderTags));

        public decimal GetTaxTotal()
        {
            decimal taxPrice = 0M;

            if (!TaxValues.Any())
            {
                return 0M;
            }
            else
            {
                foreach (var tax in TaxValues)
                {
                    if (TaxIncluded)
                    {
                        var tRate = 100 + tax.TaxRate;
                        var taxVale = (GetTaxablePrice() * tRate / 100) - GetTaxablePrice();
                        taxPrice += taxVale;
                    }
                    else
                    {
                        taxPrice += (GetTaxablePrice() * tax.TaxRate / 100);
                    }
                }
            }
            return taxPrice;
        }
    }

    public class AllTicketListDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }

        public int LocationId { get; set; }
        /** Copy from here to the DinePlan Sync **/
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public DateTime TicketCreatedTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public DateTime LastOrderTime { get; set; }
        public DateTime LastPaymentTime { get; set; }
        public bool IsClosed { get; set; }
        public bool IsLocked { get; set; }
        public decimal RemainingAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DepartmentName { get; set; }
        public string TicketTypeName { get; set; }
        public string Note { get; set; }
        public string LastModifiedUserName { get; set; }
        public string TicketTags { get; set; }
        public string TicketStates { get; set; }
        public string TicketLogs { get; set; }
        public bool TaxIncluded { get; set; }
        public string TerminalName { get; set; }
        public bool PreOrder { get; set; }
        public string TicketEntities { get; set; }
        public string TicketPromotionDetails { get; set; }

        /** End **/
        public List<AllOrderListDto> Orders { get; set; }
        public List<PaymentListDto> Payments { get; set; }
        public List<TicketTransactionListDto> Transactions { get; set; }
    }

    public class AllOrderListDto : CreationAuditedEntityDto<int>
    {
        /**Copy from here to the DineSync **/
        public int OrderId { get; set; }
        public string LocationName { get; set; }
        public int Location_Id { get; set; }
        public int TicketId { get; set; }
        public string DepartmentName { get; set; }
        public string AliasCode { get; set; }
        public int MenuItemId { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Quantity { get; set; }
        public int PortionCount { get; set; }
        public string Note { get; set; }
        public bool Locked { get; set; }
        public bool CalculatePrice { get; set; }
        public bool IncreaseInventory { get; set; }
        public bool DecreaseInventory { get; set; }
        public string OrderNumber { get; set; }
        public string CreatingUserName { get; set; }
        public DateTime OrderCreatedTime { get; set; }
        public string PriceTag { get; set; }
        public string Taxes { get; set; }
        public string OrderTags { get; set; }
        public string OrderStates { get; set; }
        public bool IsPromotionOrder { get; set; }
        public string PromotionName { get; set; }
        public int PromotionSyncId { get; set; }

        public decimal PromotionAmount { get; set; }
        public int MenuItemPortionId { get; set; }

        public List<OrderStateValue> GetOrderState()
        {
            return JsonHelper.Deserialize<List<OrderStateValue>>(OrderStates);
        }
    }

    public class MenuReportView
    {
        public string AliasCode { get; set; }
        public string MenuItemName { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Price { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Quantity { get; set; }
    }

    public class MenuListDto
    {
        public int MenuItemId { get; set; }
        public string AliasCode { get; set; }
        public string MenuItemName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string DepartmentName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string MenuItemPortionName { get; set; }
        public string Tag { get; set; }
        public string Tags { get; set; }

        public List<string> TagList
        {
            get
            {
                if (Tags.IsNullOrEmpty())
                    return new List<string>();
                return Tags.Split(",").ToList();
            }
        }

        [DataType("decimal(16 ,3")]
        public decimal Price { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Quantity { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal Total => Price * Quantity;

        public string LocationName { get; set; }
        public int Hour { get; set; }
        public decimal OrderTagPrice { get; set; }

        public bool IsOrderTag { get; set; }

        public decimal TaxValue { get; set; }
        public TimeSpan TimeOrder { get; set; }

        [DataType("decimal(16 ,3")]
        public decimal TaxPrice { get; set; }

        public DateTime OrderTime { get; set; }
        public string OrderNote { get; set; }
        public string OrderUser { get; set; }
        public string TicketNumber { get; set; }
        public int Id { get; set; }

        public decimal TotalAmount { get; set; }
    }

    public class DepartmentReportDto
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public int TotalTicketCount { get; set; }
        public decimal Total { get; set; }
    }

    public class SaleSummaryDto
    {
        public SaleSummaryDto()
        {
            Payments = new Dictionary<string, decimal>();
            Transactions = new Dictionary<string, decimal>();
            Promotions = new Dictionary<string, decimal>();
        }

        public DateTime Date { get; set; }
        public string DateStr => Date.ToString("yyyy-MM-dd");
        public int TotalOrderCount { get; set; }
        public int TotalCovers { get; set; }
        public int TotalTicketCount { get; set; }
        public decimal Total { get; set; }
        public List<DepartmentReportDto> Departments { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }
        public string LocationCode { get; set; }

        public Dictionary<string, decimal> Payments { get; set; }
        public Dictionary<string, decimal> Transactions { get; set; }
        public Dictionary<string, decimal> Promotions { get; set; }
    }

    public class PaymentSummaryDto
    {
        public DateTime Date { get; set; }
        public string DateStr => Date.ToShortDateString();

        public int TotalOrderCount { get; set; }
        public int TotalTicketCount { get; set; }
        public decimal Total { get; set; }
        public List<DepartmentReportDto> Departments { get; set; }
    }

    public class TransactionSummaryDto
    {
        public DateTime Date { get; set; }
        public string DateStr => Date.ToShortDateString();

        public int TotalOrderCount { get; set; }
        public int TotalTicketCount { get; set; }
        public decimal Total { get; set; }
        public List<DepartmentReportDto> Departments { get; set; }
    }

    public class PaymentListDto 
    {
        public int Id { get; set; }

        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public int TicketId { get; set; }
        public DateTime PaymentCreatedTime { get; set; }
        public decimal TenderedAmount { get; set; }
        public string TerminalName { get; set; }
        public decimal Amount { get; set; }
        public string PaymentUserName { get; set; }
        public string AccountCode { get; set; }

        public string PaymentTags { get; set; }
    }

    public class TicketTransactionListDto 
    {
        public int Id { get; set; }

        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public int TicketId { get; set; }
        public decimal Amount { get; set; }
        public string AccountCode { get; set; }
    }

    public class TicketViewTax
    {
        public DateTime Date { get; set; }
        public string RegNo { get; set; }
        public string TaxInvoiceNo { get; set; }
        public decimal SaleAmt { get; set; }
        public decimal SaleNonVat { get; set; }
        public decimal Service { get; set; }
        public decimal Tip { get; set; }
        public decimal SaleExcise { get; set; }
        public decimal StdExcise { get; set; }
        public decimal Vat { get; set; }
        public decimal SaleVat => (SaleAmt - Vat);
        public decimal Discount { get; set; }
        public decimal VoucherExcess { get; set; }
    }

    public class TicketViewTaxDetail
    {
        public DateTime Date { get; set; }
        public string RegNo { get; set; }
        public string TaxInvoiceNo { get; set; }
        public string Employee { get; set; }
        public string Detail { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal SalesNonVat => 0;
        public decimal SalesExcVat => SalesAmount - Vat - Service;
        public decimal Service { get; set; }
        public decimal Vat { get; set; }
        public string Payment { get; set; }
    }

    

    public class ItemComboSalesDto
    {
        public ItemComboSalesDto()
        {
            ItemSales = new List<ItemSale>();
        }

        public string Location { get; set; }
        public string LocationCode { get; set; }

        public List<ItemSale> ItemSales { get; set; }
    }

    public class ItemSale
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public string Location { get; set; }
        public string LocationCode { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }
        public List<ItemSale> OrderItems { get; set; }
        public List<ItemSale> OrderTags { get; set; }
    }

    public class RefundTicketDetail
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public DateTime LastPaymentTime { get; set; }
        public decimal SubAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DepartmentName { get; set; }
        public string InvoiceNo { get; set; }
        public string TerminalName { get; set; }
        public Decimal Quantity { get; set; }
        public string MenuItem { get; set; }
        public string Employee { get; set; }
        public string Note { get; set; }
    }

    public class WritersCafeList
    {
        public string Location { get; set; }
        public DateTime Date { get; set; }
        public string BillNo { get; set; }
        public string TransactionId { get; set; }
        public string TableNo { get; set; }
        public DateTime OrderTakenTime { get; set; }
        public DateTime BillGeneratedTime { get; set; }
        public DateTime TransactionClosedTime { get; set; }
        public string OrderType { get; set; }
        public string PaymentType { get; set; }
        public string MenuItemName { get; set; }
        public string CategoryName { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal PackingCharge { get; set; }
        public decimal TotalAmount => Price * Quantity;
        public string Pax { get; set; }
        public string ModificationDone { get; set; }
        public DateTime PrintTime { get; set; }
        public DateTime RePrintTime { get; set; }
        public decimal AmountAfterModification { get; set; }
        public decimal Amount { get; set; }
    }

    public class LocationReportDto
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
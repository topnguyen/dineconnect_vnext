﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using System;
using System.Collections.Generic;


namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class WorkPeriodSummaryInputWithPaging : PagedAndSortedInputDto, IShouldNormalize, IInputDto
    {
        public WorkPeriodSummaryInputWithPaging()
        {
            Locations = new List<SimpleLocationDto>();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public int LocationId { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }
        public bool RunInBackground { get; set; }
        public int? TenantId { get; set; }
        public string ReportDescription { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

}

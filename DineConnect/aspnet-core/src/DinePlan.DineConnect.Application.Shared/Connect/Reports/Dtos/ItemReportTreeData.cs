﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class ItemReportTreeData
    {
        public MenuListDto Data { get; set; }

        public List<ItemReportTreeData> Children { get; set; }
    }
}
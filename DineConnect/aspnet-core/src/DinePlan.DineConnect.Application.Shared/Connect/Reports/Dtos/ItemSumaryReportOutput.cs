﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class ItemSummaryReportOutput
    {
        public PagedResultDto<ItemReportTreeData> MenuList { get; set; }

        public List<ChartOutputDto> Quantities { get; set; }
        public int Quantity { get; set; }

        public List<ChartOutputDto> Totals { get; set; }
        public int Total { get; set; }

        public decimal TotalQuantity { get; set; }
        public decimal TotalResult { get; set; }
    }
}
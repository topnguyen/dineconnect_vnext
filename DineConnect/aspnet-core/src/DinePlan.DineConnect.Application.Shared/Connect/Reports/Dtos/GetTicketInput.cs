﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
	public class GetTicketInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
	{
		public GetTicketInput()
		{
			Locations = new List<SimpleLocationDto>();
			LocationGroup = new CommonLocationGroupDto();
			Promotions = new List<PromotionListDto>();
		}

		public List<PromotionListDto> Promotions { get; set; }
		public List<string> PaymentTags { get; set; }

		public List<int> Payments { get; set; }
		public List<ComboboxItemDto> Transactions { get; set; }
		public List<ComboboxItemDto> Departments { get; set; }
		public List<ComboboxItemDto> TicketTags { get; set; }
		public string Department { get; set; }
		public string TerminalName { get; set; }
		public string LastModifiedUserName { get; set; }
		public string OutputType { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Location { get; set; }
		public long UserId { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public bool NotCorrectDate { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}

		public ExportType ExportOutputType { get; set; }
		public string ExportType { get; set; }
		public string DynamicFilter { get; set; }
		public bool ReportDisplay { get; set; }
		public bool PaymentName { get; set; }
	}

	public class GetTicketSyncInput : PagedAndSortedInputDto, IDateInput
	{
		public GetTicketSyncInput()
		{
			Locations = new List<SimpleLocationDto>();
			LocationGroup = new CommonLocationGroupDto();
		}

		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Location { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public long UserId { get; set; }
		public bool NotCorrectDate { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }

		public ExportType ExportOutputType { get; set; }
	}

	public class GetTicketSyncOutput : AuditedEntity
	{
		public string LocationCode { get; set; }
		public string LocationName { get; set; }
		public int TicketSyncCount { get; set; }
		public decimal TicketTotalAmount { get; set; }
		public DateTime WorkStartDate { get; set; }
		public DateTime WorkCloseDate { get; set; }
		public int TotalTickets { get; set; }
		public decimal WorkTotalAmount { get; set; }
		public bool IsDifference { get; set; }
	}

	public class SaleTargetTicketInput : GetTicketInput
	{
		public string FileName { get; set; }
	}

	public class GetItemInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
	{
		public bool IncludeTax { get; set; }

		public List<string> FilterByUser { get; set; }
		public List<string> FilterByTerminal { get; set; }
		public List<string> FilterByDepartment { get; set; }
		public List<int?> FilterByCategory { get; set; }
		public List<int?> FilterByGroup { get; set; }
		public List<string> FilterByTag { get; set; }
		public bool IsItemSalesReport { get; set; }
		public bool IsSummary { get; set; }
		public string LastModifiedUserName { get; set; }
		public bool Gift { get; set; }
		public bool VoidParam { get; set; }
		public bool Comp { get; set; }
		public bool Sales { get; set; }
		public bool Exchange { get; set; }
		public bool NoSales { get; set; }
		public bool Stats { get; set; }
		public bool ByLocation { get; set; }
		public string Duration { get; set; }
		public int WorkPeriodId { get; set; }
		public string OutputType { get; set; }
		public List<PromotionListDto> Promotions { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Location { get; set; }
		public long UserId { get; set; }
		public bool NotCorrectDate { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }
		public ExportType ExportOutputType { get; set; }

		public List<int> MenuItemIds { get; set; }
		public List<int> MenuItemPortionIds { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}

		public bool IsExport { get; set; }
		public string ReportDescription { get; set; }

		public int TakeNumber { get; set; }
		public string TakeType { get; set; }
		public string Ranking { get; set; }

		public string TakeString
		{
			get { return $"{Ranking} {TakeType}"; }
		}

		public string DynamicFilter { get; set; }
		public List<ComboboxItemDto> AuditTypes { get; set; }
		public string TerminalName { get; set; }
		public bool TaxIncluded { get; set; }

		public string ExportOutput { get; set; }

		public bool Portrait { get; set; }

		public bool RunInBackground { get; set; }
		public int? TenantId { get; set; }

		public GetItemInput()
		{
			FilterByCategory = new List<int?>();
			FilterByGroup = new List<int?>();
			FilterByDepartment = new List<string>();
			Locations = new List<SimpleLocationDto>();
			FilterByTerminal = new List<string>();
			FilterByUser = new List<string>();
			MenuItemIds = new List<int>();
		}
	}

	public class GetDayItemInput : PagedAndSortedInputDto, IDateInput, IShouldNormalize
	{
		public bool Gift { get; set; }
		public bool Void { get; set; }
		public bool Comp { get; set; }
		public bool NoSales { get; set; }
		public bool ByLocation { get; set; }
		public List<DateTime> AllDates { get; set; }
		public List<int> MenuPortions { get; set; }
		public bool Stats { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Location { get; set; }
		public int TenantId { get; set; }
		public long UserId { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }
		public bool NotCorrectDate { get; set; }
		public List<SimpleLocationDto> LocationsByUser { get; set; }
		public List<string> FilterByDepartment { get; set; }
		public List<int?> FilterByCategory { get; set; }
		public List<int?> FilterByGroup { get; set; }
		public List<string> FilterByTag { get; set; }
		public List<int> MenuItemIds { get; set; }

		public List<int> MenuItemPortionIds { get; set; }
		public string DynamicFilter { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}
	}

	public class GetPaymentInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public bool NotCorrectDate { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }
		public int Location { get; set; }
		public long UserId { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}
	}

	public class GetTransactionInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }
		public int Location { get; set; }
		public long UserId { get; set; }
		public bool NotCorrectDate { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}
	}

	public class GetTicketIdInput
	{
		public int Location { get; set; }
		public string TicketNumber { get; set; }
	}

	public class GetPaymentOutput
	{
		public int Id { get; set; }
		public decimal Amount { get; set; }
		public string Name { get; set; }
	}

	public class GetTransactionOutput
	{
		public int Id { get; set; }
		public int TicketId { get; set; }
		public decimal Amount { get; set; }
		public string Name { get; set; }
	}
}
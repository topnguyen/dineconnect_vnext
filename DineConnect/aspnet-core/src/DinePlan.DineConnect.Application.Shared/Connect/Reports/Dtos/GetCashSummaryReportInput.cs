﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using System;
using System.Collections.Generic;


namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class GetCashSummaryReportInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
    {
        public GetCashSummaryReportInput()
        {
            Locations = new List<SimpleLocationDto>();
            Promotions = new List<PromotionListDto>();
            LocationGroup = new CommonLocationGroupDto();
        }

        public List<PromotionListDto> Promotions { get; set; }
        public List<string> PaymentTags { get; set; }

        public List<int> Payments { get; set; }
        public List<ComboboxItemDto> Transactions { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public List<ComboboxItemDto> TicketTags { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public string Department { get; set; }
        public string TerminalName { get; set; }
        public string LastModifiedUserName { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }

        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }

        public bool PaymentName { get; set; }

        public bool IncludeTax { get; set; }
        public bool ReportDisplay { get; set; }

        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
        public CommonLocationGroupDto LocationGroup { get ; set ; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
    }
    public class SubInput
    {
        public string Name { get; set; }
    }
}

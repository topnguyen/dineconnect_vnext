﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class PairObject
    {
        public PairObject()
        {
            Datas = new Dictionary<string, string>();
        }
        public string[] FieldNames { get; set; }
        public Dictionary<string, string> Datas { get; set; }
        public string[] Format { get; set; }
        public FieldAlignment[] Alignments { get; set; }
        public ExportFiledType[] FieldTypes { get; set; }
    }

    public enum FieldAlignment
    {
        Left = 0,
        Center = 1,
        Right = 2
    }

    public enum ExportFiledType
    {

        Decimal = 0,
        Integer = 1,
        String = 2,
        Date = 3,
        Pair = 4
    }
}

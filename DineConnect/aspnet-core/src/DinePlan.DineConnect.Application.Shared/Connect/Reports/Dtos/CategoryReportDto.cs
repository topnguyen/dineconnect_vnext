﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
	public class CategoryReportDto : EntityDto<int>
	{
		public CategoryReportDto()
		{
			TotalItems = new List<MenuListDto>();
			MenuItems = new List<MenuReportView>();
			LocationReports = new List<LocationReportDto>();
		}

		public int CategoryId { get; set; }
		public string CategoryName { get; set; }
		public List<MenuListDto> TotalItems { get; set; }

		[DataType("decimal(16 ,3")]
		public decimal Quantity { get; set; }

		[DataType("decimal(16 ,3")]
		public decimal Total { get; set; }

		public List<MenuReportView> MenuItems { get; set; }

		public List<LocationReportDto> LocationReports { get; set; }
		public string ProductGroupName { get; set; }
	}
}
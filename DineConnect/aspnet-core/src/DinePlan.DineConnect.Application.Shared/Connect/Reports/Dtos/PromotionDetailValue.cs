﻿using System;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    [DataContract]
    public class PromotionDetailValue
    {
        public PromotionDetailValue()
        {
            LastUpdateTime = DateTime.Now;
        }
        [DataMember(Name = "ID")]
        public int Id { get; set; }
        [DataMember(Name = "LUT")]
        public DateTime LastUpdateTime { get; set; }

        [DataMember(Name = "PN")]
        public string PromotionName { get; set; }

        [DataMember(Name = "PA")]
        public decimal PromotionAmount { get; set; }

        [DataMember(Name = "PV")]
        public decimal PromotionValue { get; set; }

        [DataMember(Name = "PI")]
        public int PromotionId { get; set; }

        [DataMember(Name = "PSI")]
        public int PromotionSyncId { get; set; }

        [DataMember(Name = "PNO")]
        public string PromotionNote { get; set; }

        [DataMember(Name = "PFT")]
        public bool IsFixed { get; set; }

        [DataMember(Name = "CT")]
        public int CalculationType { get; set; }

        [DataMember(Name = "PT")]
        public int PromotionType { get; set; }


    }
}

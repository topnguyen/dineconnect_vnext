﻿using DinePlan.DineConnect.Connect.Period.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Connect.Master.LocationGroup;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
	public class GetCashSummaryReportOutput
	{
		public List<GetCashSummaryReportByPlant> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportOutput()
		{
			ListItem = new List<GetCashSummaryReportByPlant>();
		}

	}
	public class GetCashSummaryReportByPlant
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public string SoldToID { get; set; }
		public List<GetCashSummaryReportByDate> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportByPlant()
		{
			ListItem = new List<GetCashSummaryReportByDate>();
		}
	}
	public class GetCashSummaryReportByDate
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public string SoldToID { get; set; }
		public DateTime Date { get; set; }
		public DateTime DateSort { get; set; }
		public List<GetCashSummaryReportByShift> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportByDate()
		{
			ListItem = new List<GetCashSummaryReportByShift>();
		}
	}

	public class GetCashSummaryReportByShift
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public DateTime Date { get; set; }
		public string SoldToID { get; set; }
		public string StartUser { get; set; }
		public string EndShift { get; set; }
		public string StartShift { get; set; }
		public List<GetCashSummaryReportByItem> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.Amount);
			}
		}
		public GetCashSummaryReportByShift()
		{
			ListItem = new List<GetCashSummaryReportByItem>();
		}
	}
	public class GetCashSummaryReportByItem
	{
		public int LocationID { get; set; }
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public DateTime Date { get; set; }
		public string StartUser { get; set; }
		public string StopUser { get; set; }
		public string EndShift { get; set; }
		public string StartShift { get; set; }
		public decimal Amount { get; set; }
		public string TaxId { get; set; }
		public string AddOn { get; set; }
		public string SoldToID
		{
			get
			{
				if (!string.IsNullOrEmpty(AddOn))
				{
					var result = JsonConvert.DeserializeObject<AddOn>(AddOn);
					return result.SoldTo;
				}
				return null;
			}
		}

		public string Detail { get; set; }
		public bool WithTWH { get; set; }
	}

	public class AddOn
	{
		public string SoldTo { get; set; }

	}
	public class WorkShiftByLocationDto : WorkShiftDto
	{
		public int LocationId { get; set; }
	}
	public class GetCashSummaryReportInput2 : PagedAndSortedInputDto, IShouldNormalize, IDateInput
	{
		public GetCashSummaryReportInput2()
		{
			Locations = new List<SimpleLocationDto>();
			LocationGroup2 = new LocationGroupDto();
			Promotions = new List<PromotionListDto>();
			LocationGroup = new CommonLocationGroupDto();
		}

		public List<PromotionListDto> Promotions { get; set; }
		public List<string> PaymentTags { get; set; }

		public List<int> Payments { get; set; }
		public List<ComboboxItemDto> Transactions { get; set; }
		public List<ComboboxItemDto> Departments { get; set; }
		public List<ComboboxItemDto> TicketTags { get; set; }
		public List<ComboboxItemDto> AuditTypes { get; set; }
		public string Department { get; set; }
		public string TerminalName { get; set; }
		public string LastModifiedUserName { get; set; }
		public string OutputType { get; set; }
		public string ExportType { get; set; }
		public bool ByLocation { get; set; }
		public string Duration { get; set; }
		public int TenantId { get; set; }
		public string ExportOutput { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int Location { get; set; }
		public long UserId { get; set; }
		public List<SimpleLocationDto> Locations { get; set; }
		public LocationGroupDto LocationGroup2 { get; set; }
		public bool NotCorrectDate { get; set; }
		public bool Credit { get; set; }
		public bool Refund { get; set; }
		public string TicketNo { get; set; }

		public ExportType ExportOutputType { get; set; }
		public bool Portrait { get; set; }

		public bool RunInBackground { get; set; }
		public string ReportDescription { get; set; }
		public FileDto FileDto { get; set; }

		public bool PaymentName { get; set; }

		public bool IncludeTax { get; set; }
		public bool ReportDisplay { get; set; }

		public string DynamicFilter { get; set; }
		public string DateFormat { get; set; }
		public string DatetimeFormat { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }

		public void Normalize()
		{
			if (Sorting.IsNullOrWhiteSpace())
			{
				Sorting = "Id";
			}
		}
	}
}

﻿namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class TransactionOrderTagReportDto
    {
        public int? Id { get; set; }

        public virtual int OrderId { get; set; }
        public int OrderTagGroupId { get; set; }
        public int OrderTagId { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string TagName { get; set; }
        public string TagNote { get; set; }
        public string TagValue { get; set; }
        public bool TaxFree { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
    }

}

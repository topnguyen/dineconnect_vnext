﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public interface IDateInput
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int Location { get; set; }
        long UserId { get; set; }
        bool NotCorrectDate { get; set; }
        List<SimpleLocationDto> Locations { get; set; }
        CommonLocationGroupDto LocationGroup { get; set; }
        bool Credit { get; set; }
        bool Refund { get; set; }
        string TicketNo { get; set; }

    }
}

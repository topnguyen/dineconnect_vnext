﻿using DinePlan.DineConnect.Connect.Period.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class PlanDayOutput 
    {
        public DateTime ReportStartDay { get; set; }
        public DateTime ReportEndDay { get; set; }
        public string DayStartStr => ReportStartDay.ToString("D");
        public string DayEndStar => ReportEndDay.ToString("D");
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalTicketCount { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && TotalTicketCount > 0M)
                    return Total / TotalTicketCount;
                return 0M;
            }
        }

        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
        public string DayInformation { get; set; }
        public int Wid { get; set; }

        public List<WorkShiftDto> WorkShifts
        {
            get
            {
                if (string.IsNullOrEmpty(DayInformation))
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<List<WorkShiftDto>>(DayInformation);
            }
        }

        public List<WorkShiftDto> WorkShiftsTemp { get; set; }


        public List<string> Terminals
        {
            get
            {
                if (WorkShifts == null)
                {
                    return null;
                }

                return WorkShifts.Select(t => t.TerminalName).Distinct().ToList();
            }
        }
    }
}

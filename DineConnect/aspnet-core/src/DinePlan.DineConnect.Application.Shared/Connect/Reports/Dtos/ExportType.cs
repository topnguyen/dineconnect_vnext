﻿namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public enum ExportType
    {
        Excel = 0,
        HTML = 1
    }
}

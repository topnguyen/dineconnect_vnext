﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class TicketEntityMap
    {
        public int EntityTypeId { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string EntityCustomData { get; set; }
        public string Notes { get; set; }

        public string GetCustomData(string fieldName)
        {
            if (string.IsNullOrEmpty(EntityCustomData)) return "";
            var pattern = $"\"Name\":\"{fieldName}\",\"Value\":\"([^\"]+)\"";

            return HasCustomData(fieldName)
                ? Regex.Unescape(Regex.Match(EntityCustomData, pattern).Groups[1].Value)
                : "";
        }

        public string GetCustomDataFormat(string fieldName, string format)
        {
            var result = GetCustomData(fieldName.Trim());
            return !string.IsNullOrEmpty(result) ? string.Format(format, result) : "";
        }

        public decimal GetCustomDataAsDecimal(string fieldName)
        {
            decimal result;
            if (decimal.TryParse(GetCustomData(fieldName), out result))
                return result;
            return 0;
        }

        public bool HasCustomData(string fieldName)
        {
            var pattern = string.Format("\"Name\":\"{0}\",\"Value\":\"([^\"]+)\"", fieldName);
            return EntityCustomData != null && Regex.IsMatch(EntityCustomData, pattern);
        }
    }
}

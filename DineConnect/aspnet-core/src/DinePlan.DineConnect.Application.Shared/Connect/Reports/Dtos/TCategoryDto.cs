﻿namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class TCategoryDto
    {
        public int CatId { get; set; }
        public string CatgoryName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string AliasCode { get; set; }
        public string LocationName { get; set; }
        public int PortionId { get; set; }
    }
}
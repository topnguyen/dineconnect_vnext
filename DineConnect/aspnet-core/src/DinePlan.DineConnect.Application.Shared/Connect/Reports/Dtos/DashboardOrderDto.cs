﻿namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class DashboardOrderDto
    {
        public DashboardOrderDto()
        {
            TotalOrderCount = 0;
            TotalItemSold = 0;
            TotalAmount = 0M;

        }
        public int TotalOrderCount { get; set; }
        public decimal TotalItemSold { get; set; }
        public decimal TotalAmount { get; set; }


    }
}

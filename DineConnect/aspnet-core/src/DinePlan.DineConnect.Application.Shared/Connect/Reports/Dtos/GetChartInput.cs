﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
    public class GetChartInput : IDateInput
    {
        public int Location { get; set; }
        public long UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Connect.Reports.Dtos
{
	public class ConsolidatedDto
	{
		public List<ConsolidatedBy_Location> ListItem { get; set; }
		public decimal TotalWithOutTax => ListItem.Sum(x => x.TotalWithOutTax);
		public decimal TotalWithTax => ListItem.Sum(x => x.TotalWithTax);
		public decimal TotalAmount => ListItem.Sum(x => x.TotalAmount);
		public ConsolidatedDto()
		{
			ListItem = new List<ConsolidatedBy_Location>();
		}
	}
	public class ConsolidatedBy_Location
	{
		public int LocationID { get; set; }
		public string LocationName { get; set; }
		public List<ConsolidatedBy_Location_Department> ListItem { get; set; }
		public decimal TotalWithOutTax => ListItem.Sum(x => x.TotalWithOutTax);
		public decimal TotalWithTax => ListItem.Sum(x => x.TotalWithTax);
		public decimal TotalAmount => ListItem.Sum(x => x.TotalAmount);
		public ConsolidatedBy_Location()
		{
			ListItem = new List<ConsolidatedBy_Location_Department>();
		}
	}
	public class ConsolidatedBy_Location_Department
	{
		public int LocationID { get; set; }
		public string LocationName { get; set; }
		public string DepartmentName { get; set; }
		public List<ConsolidatedItem> ListItem { get; set; }
		public decimal TotalWithOutTax => ListItem.Sum(x => x.WithOutTax);
		public decimal TotalWithTax => ListItem.Sum(x => x.WithTax);
		public decimal TotalAmount => ListItem.Sum(x => x.TotalAmount);

		public ConsolidatedBy_Location_Department()
		{
			ListItem = new List<ConsolidatedItem>();
		}
	}
	public class ConsolidatedItem
	{
		public int LocationID { get; set; }
		public string LocationName { get; set; }
		public string DepartmentName { get; set; }
		public Decimal WithOutTax { get; set; }
		public Decimal WithTax { get; set; }
		public Decimal TotalAmount => WithTax;

	}
}

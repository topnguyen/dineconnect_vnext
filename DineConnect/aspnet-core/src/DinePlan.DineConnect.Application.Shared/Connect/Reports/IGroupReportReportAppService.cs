﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
	public interface IGroupReportReportAppService : IApplicationService
	{
		Task<GroupStatsDto> BuildGroupSalesReport(GetItemInput input);
		Task<FileDto> BuildGroupExcel(GetItemInput input);
	}
}
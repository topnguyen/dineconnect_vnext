﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Reports
{
    public interface IDepartmentReportAppService
    {
        Task<DepartmentStatsDto> BuildDepartmentSales(GetItemInput input);

        Task<List<DepartmentSummaryDto>> GetDepartmentSalesSummary(GetItemInput input);

        Task<FileDto> BuildDepartmentSummaryExport(GetItemInput input);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.OrderDetail.Dtos
{
    public enum CheckoutStatus
    {
        Failed = 0,
        Expired = 1,
        Pending = 2,
        Successful = 3,
        Reversed = 4
    }
    public enum OrderPaymentMethod
    {
        CREDIT_CARD = 1,
        CASH_ON_DELIVERY = 2,
        CARD_ON_DELIVERY = 3,
        OTHER = 4,
    }
}

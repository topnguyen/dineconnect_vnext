﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.OrderDetail.Dtos
{
    public class CreateOrEditOrderDetailDto : EntityDto<long?>
    {
		public DateTime CreationTime { get; set; }
		public long CreatorUserId { get; set; }

		public DateTime? LastModificationTime { get; set; }

		public long? LastModifierUserId { get; set; }

		public int CheckoutStatus { get; set; }

		public long Amount { get; set; }

		public string OrderNotes { get; set; }

		public string OrderDetails { get; set; }

		public int DeliveryLocationId { get; set; }

		public string Building { get; set; }

		public string Street { get; set; }

		public string PostalCode { get; set; }

		public string FlatNo { get; set; }

		public string Landmark { get; set; }

		public int OrderPaymentMethod { get; set; }
		public string ChargedToken { get; set; }
		public string Currency { get; set; }
		public string PaymentOption { get; set; }

	}
}
﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.OrderDetail.Dtos
{
    public class GetAllOrderDetailsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public DateTime? MaxCreationTimeFilter { get; set; }
		public DateTime? MinCreationTimeFilter { get; set; }

		public long? MaxCreatorUserIdFilter { get; set; }
		public long? MinCreatorUserIdFilter { get; set; }

		public int? MaxCheckoutStatusFilter { get; set; }
		public int? MinCheckoutStatusFilter { get; set; }

		public decimal? MaxAmountFilter { get; set; }
		public decimal? MinAmountFilter { get; set; }

		public int? MaxDeliveryLocationIdFilter { get; set; }
		public int? MinDeliveryLocationIdFilter { get; set; }

		public string BuildingFilter { get; set; }

		public string StreetFilter { get; set; }

		public string FlatNoFilter { get; set; }

		public string LandmarkFilter { get; set; }

		public int? MaxOrderPaymentMethodFilter { get; set; }
		public int? MinOrderPaymentMethodFilter { get; set; }

		public string CurrencyFilter { get; set; }



    }
}
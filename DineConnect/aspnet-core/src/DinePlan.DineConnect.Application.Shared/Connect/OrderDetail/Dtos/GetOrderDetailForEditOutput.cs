﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.OrderDetail.Dtos
{
    public class GetOrderDetailForEditOutput
    {
		public CreateOrEditOrderDetailDto OrderDetail { get; set; }


    }
}
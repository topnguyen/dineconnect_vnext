﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.OrderDetail.Dtos
{
    public class OrderDetailDto : EntityDto<long>
    {
		public DateTime CreationTime { get; set; }

		public int CheckoutStatus { get; set; }

		public decimal Amount { get; set; }

		public string OrderNotes { get; set; }
		public string OrderDetails { get; set; }

		public int DeliveryLocationId { get; set; }

		public string Building { get; set; }

		public string Street { get; set; }

		public string FlatNo { get; set; }

		public string Landmark { get; set; }

		public int OrderPaymentMethod { get; set; }

		public string Currency { get; set; }



    }
}
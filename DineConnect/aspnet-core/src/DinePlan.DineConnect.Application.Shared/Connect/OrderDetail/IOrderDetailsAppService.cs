﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.OrderDetail.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Connect.OrderDetail
{
    public interface IOrderDetailsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetOrderDetailForViewDto>> GetAll(GetAllOrderDetailsInput input);

        Task<GetOrderDetailForViewDto> GetOrderDetailForView(long id);

		Task<GetOrderDetailForEditOutput> GetOrderDetailForEdit(EntityDto<long> input);

		Task CreateOrEdit(CreateOrEditOrderDetailDto input);

		Task Delete(EntityDto<long> input);

		Task<FileDto> GetOrderDetailsToExcel(GetAllOrderDetailsForExcelInput input);

		
    }
}
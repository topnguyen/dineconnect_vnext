﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Job.Dto;

namespace DinePlan.DineConnect.Connect.Setup
{
    public interface IConnectSetupAppService : IApplicationService
    {
        Task SyncDineConnect(ConnectSyncJobArgs connectSyncJobArgs);
    }
}
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Lookups
{
    public interface IConnectLookupAppService : IApplicationService
    {
        Task<ListResultDto<ComboboxItemDto>> GetTransactionTypesForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetPaymentTypes();

        Task<ListResultDto<ComboboxItemDto>> GetDepartments();

        ListResultDto<ComboboxItemDto> GetScreenMenuCategoryKeyBoardTypes();
        Task<ListResultDto<ComboboxItemDto>> GetTillAccounts();
    }
}
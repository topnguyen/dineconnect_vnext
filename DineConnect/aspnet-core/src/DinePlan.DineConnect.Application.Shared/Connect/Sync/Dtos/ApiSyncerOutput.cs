﻿using System;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
	public class ApiSyncerOutput
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public Guid SyncerGuid { get; set; }
	}
}
﻿namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class GetSyncerForViewDto
    {
        public SyncerDto Syncer { get; set; }

    }
}
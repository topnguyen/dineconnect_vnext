﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class CreateOrEditSyncerDto : EntityDto<int?>
    {

        public string Name { get; set; }

        public Guid SyncerGuid { get; set; }

    }
}
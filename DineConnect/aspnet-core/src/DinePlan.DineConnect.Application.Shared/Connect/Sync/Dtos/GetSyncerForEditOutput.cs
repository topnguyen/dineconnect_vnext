﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class GetSyncerForEditOutput
    {
        public CreateOrEditSyncerDto Syncer { get; set; }

    }
}
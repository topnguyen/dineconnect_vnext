﻿using DinePlan.DineConnect.Common.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
	public class UpdateSyncerInput : IInputDto
	{
		public int LocationId { get; set; }
		public int TenantId { get; set; }
		public string Name { get; set; }
		public Guid SyncerGuid { get; set; }
	}
}
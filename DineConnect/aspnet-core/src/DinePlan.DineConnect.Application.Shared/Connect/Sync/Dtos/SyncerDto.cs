﻿using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class SyncerDto : EntityDto
    {
        public string Name { get; set; }

        public Guid SyncerGuid { get; set; }

    }
}
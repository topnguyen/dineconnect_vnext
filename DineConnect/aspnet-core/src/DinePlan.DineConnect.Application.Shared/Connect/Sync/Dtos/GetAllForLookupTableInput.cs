﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
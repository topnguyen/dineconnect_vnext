﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class GetAllSyncersInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
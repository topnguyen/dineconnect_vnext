﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class LocationSyncerSyncerLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
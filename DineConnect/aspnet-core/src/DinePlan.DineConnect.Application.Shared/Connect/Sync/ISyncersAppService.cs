﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Sync
{
	public interface ISyncersAppService : IApplicationService
	{
		Task<PagedResultDto<GetSyncerForViewDto>> GetAll(GetAllSyncersInput input);

		Task<GetSyncerForViewDto> GetSyncerForView(int id);

		Task<GetSyncerForEditOutput> GetSyncerForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditSyncerDto input);

		Task Delete(EntityDto input);

		Task UpdateSyncer(UpdateSyncerInput input);

		Task UpdatePullTime(ApiLocationInput input);

		Task ApiUpdateAllSyncer(ApiLocationInput input);

        Task UpdateSync(string input);
    }
}
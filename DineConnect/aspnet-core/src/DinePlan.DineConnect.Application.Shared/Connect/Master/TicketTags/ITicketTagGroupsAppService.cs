﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.TicketTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.TicketTags
{
	public interface ITicketTagGroupsAppService : IApplicationService
	{
		Task<PagedResultDto<TicketTagGroupDto>> GetAll(GetAllTicketTagGroupsInput input);

		Task<CreateOrEditTicketTagGroupDto> GetTicketTagGroupForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditTicketTagGroupDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetTicketTagGroupsToExcel(GetAllTicketTagGroupsForExcelInput input);

		ListResultDto<ComboboxItemDto> GetTicketTagGroupTypeForLookupTable();

		Task<ListResultDto<ComboboxItemDto>> GetDepartmentForCombobox();

		Task<ListResultDto<TicketTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput);
        Task ActivateItem(EntityDto input);
    }
}
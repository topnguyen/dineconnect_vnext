﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Dtos
{
    public class GetAllTicketTagGroupsInput : CommonLocationGroupFilterInputDto
    {
		public string Filter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
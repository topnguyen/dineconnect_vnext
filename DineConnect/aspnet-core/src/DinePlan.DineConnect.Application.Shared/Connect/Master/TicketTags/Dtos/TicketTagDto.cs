﻿namespace DinePlan.DineConnect.Connect.Master.TicketTags.Dtos
{
    public class TicketTagDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public string AlternateName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Dtos
{
    public class TicketTagGroupEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }

        public string Departments { get; set; }

        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool ForceValue { get; set; }
        public bool AskBeforeCreatingTicket { get; set; }

        public int DataType { get; set; }
        public int TenantId { get; set; }
        public int SubTagId { get; set; }
        public bool IsAlphanumeric => DataType == 0;
        public bool IsInteger => DataType == 1;
        public bool IsDecimal => DataType == 2;

        public string Locations { get; set; }
        public bool Group { get; set; }
        public string NonLocations { get; set; }
        public virtual bool LocationTag { get; set; }

        public TicketTagGroupEditDto()
        {
            Tags = new Collection<TicketTagListDto>();
        }

        public ICollection<TicketTagListDto> Tags { get; set; }
    }
    public class TicketTagListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string AlternateName { get; set; }
    }
}

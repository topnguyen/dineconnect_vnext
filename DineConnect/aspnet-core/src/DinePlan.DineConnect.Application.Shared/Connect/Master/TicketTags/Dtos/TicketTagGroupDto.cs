﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Extensions;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Dtos
{
    public class TicketTagGroupDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public TicketTagGroupType DataType { get; set; }
        public string DataTypeName { get; set; }

        public string Departments { get; set; }
        public string DepartmentNames { get; set; }
        public string Locations { get; set; }

        public string LocationNames
        {
            get
            {
                if (!Locations.IsNullOrEmpty())
                {
                    var locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(Locations);

                    return locations.Select(l => l.Name).JoinAsString(", ");
                }
                return null;
            }
        }

        public bool SaveFreeTags { get; set; }

        public bool FreeTagging { get; set; }

        public bool ForceValue { get; set; }
    }
}
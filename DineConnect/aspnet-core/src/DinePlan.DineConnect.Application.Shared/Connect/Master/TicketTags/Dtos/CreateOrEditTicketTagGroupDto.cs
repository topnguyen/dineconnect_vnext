﻿using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.TicketTags.Dtos
{
    public class CreateOrEditTicketTagGroupDto : ConnectOrgLocEditDto
    {
        public CreateOrEditTicketTagGroupDto()
        {
            LocationGroup = new CommonLocationGroupDto();
            Tags = new List<TicketTagDto>();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public string Departments { get; set; }

        public bool SaveFreeTags { get; set; }

        public bool FreeTagging { get; set; }

        public bool ForceValue { get; set; }

        public bool AskBeforeCreatingTicket { get; set; }

        public TicketTagGroupType DataType { get; set; }

        public int OrganizationId { get; set; }

        public virtual List<TicketTagDto> Tags { get; set; }
    }
}
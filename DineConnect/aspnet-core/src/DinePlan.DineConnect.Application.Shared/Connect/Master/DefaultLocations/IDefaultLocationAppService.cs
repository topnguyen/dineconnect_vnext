﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.DefaultLocations
{
    public interface IDefaultLocationAppService : IApplicationService
    {
        Task<UserDefaultLocationDto> GetUserDefaultLocation();

        Task SetDefaultLocation(int locationId);

        Task<PagedResultDto<LocationDto>> GetLocationBasedOnUser(GetUserDefaultLocationInput input);

        Task<List<BrandDto>> GetBrandBasedOnUser();
    }
}
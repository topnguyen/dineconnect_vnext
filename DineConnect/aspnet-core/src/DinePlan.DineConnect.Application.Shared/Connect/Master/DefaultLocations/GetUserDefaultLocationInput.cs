﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.DefaultLocations
{
    public class GetUserDefaultLocationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; } 
        public int OrganizationId { get; set; }
         public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

}


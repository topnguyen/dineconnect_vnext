﻿namespace DinePlan.DineConnect.Connect.Master.DefaultLocations
{
    public class UserDefaultLocationDto
    {
        public int DefaultOrganizationId { get; set; }
        public string DefaultOrganizationName { get; set; }
        public long DefaultLocationId { get; set; }
        public string DefaultLocationName { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
    }
}
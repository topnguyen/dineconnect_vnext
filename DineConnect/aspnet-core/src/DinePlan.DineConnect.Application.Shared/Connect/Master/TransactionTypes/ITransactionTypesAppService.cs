﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Common.Dto;
using Abp.Authorization;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes
{
    public interface ITransactionTypesAppService : IApplicationService
    {
        Task<PagedResultDto<TransactionTypeDto>> GetAll(GetAllTransactionTypesInput input);

        Task<CreateOrEditTransactionTypeDto> GetTransactionTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTransactionTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTransactionTypesToExcel(GetAllTransactionTypesForExcelInput input);

        Task<ListResultDto<ComboboxItemDto>> GetTransactionTypeForLookupTable();
		Task<PagedResultDto<NameValueDto>> GetTransactionTypeForLookupTablePaging(GetAllForLookupTableInput input);

        Task<ListResultDto<ApiTransactionTypeOutput>> ApiGetAll(ApiTenantInputDto inputDto);
        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Master.TransactionTypes.Edit" })]
        Task ActivateItem(EntityDto input);
        Task<PagedResultDto<TransactionTypeDto>> GetAllItems();

    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
	public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }
	}
}
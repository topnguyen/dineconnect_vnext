﻿namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
    public class GetAllTransactionTypesForExcelInput
    {
        public string Filter { get; set; }
    }
}
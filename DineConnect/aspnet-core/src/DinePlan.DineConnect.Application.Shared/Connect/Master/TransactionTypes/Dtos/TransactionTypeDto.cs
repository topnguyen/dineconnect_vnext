﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
    public class TransactionTypeDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public bool Tax { get; set; }

        public bool Discount { get; set; }

        public string AccountCode { get; set; }
    }
}
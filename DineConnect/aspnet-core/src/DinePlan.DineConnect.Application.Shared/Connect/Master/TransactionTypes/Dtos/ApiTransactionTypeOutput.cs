﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
    public class ApiTransactionTypeOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Tax { get; set; }
        public string AccountCode { get; set; }
    }
}

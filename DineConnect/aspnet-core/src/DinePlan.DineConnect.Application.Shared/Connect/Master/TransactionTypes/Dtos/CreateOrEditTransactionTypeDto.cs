﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
    public class CreateOrEditTransactionTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(TransactionTypeConsts.MaxNameLength, MinimumLength = TransactionTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool Tax { get; set; }

        public bool Discount { get; set; }

        public string AccountCode { get; set; }
    }
}
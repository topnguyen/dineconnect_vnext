﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos
{
    public class GetAllTransactionTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
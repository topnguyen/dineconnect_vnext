﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Locations.Dtos.Api
{
    public class ApiUserInput
    {
        public int UserId { get; set; }
    }

    public class ApiLocationOutput
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}

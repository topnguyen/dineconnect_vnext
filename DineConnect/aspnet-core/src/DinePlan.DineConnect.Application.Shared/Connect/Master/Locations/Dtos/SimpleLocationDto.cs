﻿namespace DinePlan.DineConnect.Connect.Master.Locations
{
    public class SimpleLocationDto
    {
        public int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Code { get; set; }

        public int OrganizationId { get; set; }
        public long OrganizationUnitId { get; set; }
    }
}
﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Locations.Dtos
{
    public class ImportLocationsFromExcelJobArgs
    {
        public int TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Master.Locations.Dtos
{
    public class LocationDto : AuditedEntityDto
    {
        // Basic
        public int OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual string Code { get; set; }

        public List<LocationTagDto> LocationTags { get; set; }

        public List<LocationGroupDto> LocationGroups { get; set; }

        // Address

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string PostalCode { get; set; }

        public int? StateId { get; set; }

        public string StateName { get; set; }

        public int? CityId { get; set; }
        public string CityName { get; set; }

        public int? CountryId { get; set; }
        public string CountryName { get; set; }

        // Contact

        public string PhoneNumber { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public string Fax { get; set; }

        // Schedule

        public List<LocationScheduleEditDto> Schedules { get; set; }

        // Tax

        public bool ChangeTax { get; set; }

        public bool TaxInclusive { get; set; }


        // Addon

        public string AddOn { get; set; }

        // Branch

        public LocationBranchEditDto Branch { get; set; }

        // Queue Location

        public int? QueueLocationId { get; set; }

        // Other

        public virtual int ExtendedBusinessHours { get; set; }

        // Setting
        public virtual DateTime? HouseTransactionDate { get; set; }

        public virtual bool IsPurchaseAllowed { get; set; }

        public virtual bool IsProductionAllowed { get; set; }

        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }

        public bool IsProductionUnitAllowed { get; set; }

        public virtual string RequestedLocations { get; set; }

        public virtual int DefaultRequestLocationRefId { get; set; }

        public decimal TransferRequestLockHours { get; set; }

        public decimal TransferRequestGraceHours { get; set; }

        // Receipt

        public string ReceiptHeader { get; set; }
        public string ReceiptFooter { get; set; }
    }

    public class LocationBranchEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string BranchTaxCode { get; set; }
    }

    public class LocationScheduleEditDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int LocationId { get; set; }
        public int EndMinute { get; set; }
    }

    public class GetLocationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }

        public int OrganizationId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class ScheduleSetting
    {
        [JsonProperty("startHour")]
        public int StartHour { get; set; }

        [JsonProperty("endHour")]
        public int EndHour { get; set; }

        [JsonProperty("startMinute")]
        public int StartMinute { get; set; }

        [JsonProperty("endMinute")]
        public int EndMinute { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
    public class CheckLocationInput
    {
        public int LocationId { get; set; }
        public string Locations { get; set; }
        public string NonLocations { get; set; }
        public bool Group { get; set; }
        public int Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool LocationTag { get; set; }
    }

    public class GetLocationInputBasedOnUser
    {
        public long UserId { get; set; }
        public List<ComboboxItemDto> CompanyList { get; set; }
        public string LocationName { get; set; }
    }

    public class ApiLocationInput: IShouldNormalize
    {
        public int LocationId { get; set; }
        public string Tag { get; set; }
        public int TenantId { get; set; }
     
        public long UserId { get; set; }
        public string Sorting { get; set; }
        public int Skip { get; set; }
        public int PageSize { get; set; }
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public void Normalize()
        { 
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class ApiLocationSetting
    {
        public bool TaxInclusive { get; set; }
        public bool ChangeTax { get; set; }
        public int Id { get; set; }
        public string Code { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string CompanyCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string LocationTaxCode { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyWebsite { get; set; }
        public string CompanyEmail { get; set; }
        public string PostalCode { get; set; }
        public string BranchId { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchAddress1 { get; set; }
        public string BranchAddress2 { get; set; }
        public string BranchAddress3 { get; set; }
        public string BranchCity { get; set; }
        public string BranchState { get; set; }
        public string BranchPostalCode { get; set; }
        public string BranchCountry { get; set; }
        public string BranchWebsite { get; set; }
        public string BranchEmail { get; set; }
        public string BranchPhoneNumber { get; set; }
        public string BranchLocationTaxCode { get; set; }
        public string District { get; set; }
        public string BranchDistrict { get; set; }
        public string AddOn { get; set; }
    }
}
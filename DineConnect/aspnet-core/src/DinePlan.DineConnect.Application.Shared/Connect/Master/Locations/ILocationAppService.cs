﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos.Api;

namespace DinePlan.DineConnect.Connect.Master.Locations
{
	public interface ILocationAppService : IApplicationService
	{
		Task<int> CreateOrUpdteAsync(LocationDto location);

		Task<LocationDto> GetAsync(int id);

		Task<PagedResultDto<LocationDto>> GetList(GetLocationInput input);

		Task<bool> IsLocationExists(CheckLocationInput inputDto);

		Task DeleteAsync(int id);

		Task<FileDto> GetAllToExcel();

		Task<PagedResultDto<SimpleLocationDto>> GetSimpleLocations(GetLocationInput input);

		Task<FileDto> GetTemplateExcelToImport();

		Task ImportLocationFromExcel(string fileToken);
		Task<List<ScheduleSetting>> GetLocationSchedulesByLocation(int locationId);
		Task<int> GetOrgnizationIdByLocationId(int locationId);

		Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCode(string code);
        Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUser(ApiUserInput input);


		List<int> GetLocationForUserAndGroup(CommonLocationGroupDto locationDto);
        Task<ApiLocationSetting> ApiGetLocationSettings(ApiLocationInput locationInput);
        List<SimpleLocationDto> GetLocationsForUser(GetLocationInputBasedOnUser input);
        Task ActivateItem(EntityDto input);
    }
}
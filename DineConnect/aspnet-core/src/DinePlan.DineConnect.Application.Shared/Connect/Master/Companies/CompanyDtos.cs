﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Addresses.Dtos;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    public class BrandDto : AuditedEntityDto<int?>
    {
        public BrandDto()
        {
            Address = new AddressDto();
        }

        public virtual string Code { get; set; }

        [Required]
        public virtual string Name { get; set; }

        public AddressDto Address { get; set; }
        public int? AddressId { get; set; }
        public int TenantId { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string UserSerialNumber { get; set; }
        public virtual string GovtApprovalId { get; set; }
    }

    public class GetCompanyInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
        public string CustomFilters { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
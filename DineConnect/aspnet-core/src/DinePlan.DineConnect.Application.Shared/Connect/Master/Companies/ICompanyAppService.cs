﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    public interface ICompanyAppService : IApplicationService
    {
        Task<PagedResultDto<BrandDto>> GetAll(GetCompanyInput input);

        Task<BrandDto> GetCompanyForEdit(int? id);

        Task CreateOrUpdateAsync(BrandDto input);

        Task DeleteAsync(int organizationId);
        Task<FileDto> GetBrandsToExcel();
        Task ActivateItem(EntityDto input);
    }
}
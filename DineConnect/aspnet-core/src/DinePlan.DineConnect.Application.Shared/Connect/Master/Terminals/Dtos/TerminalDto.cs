﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.Terminals.Dtos
{
    public class TerminalDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public string TerminalCode { get; set; }

        public string Denominations { get; set; }

        public bool IsDefault { get; set; }

        public bool AutoLogout { get; set; }
        public decimal Float { get; set; }
        public decimal Tolerance { get; set; }

        public bool IsEndWorkTimeMoneyShown { get; set; }
        public bool IsCollectionTerminal { get; set; }
        public bool WorkTimeTotalInFloat { get; set; }
    }
}
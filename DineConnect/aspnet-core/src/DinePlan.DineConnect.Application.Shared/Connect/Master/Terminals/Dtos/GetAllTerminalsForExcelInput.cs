﻿namespace DinePlan.DineConnect.Connect.Master.Terminals.Dtos
{
    public class GetAllTerminalsForExcelInput
    {
        public string Filter { get; set; }
    }
}
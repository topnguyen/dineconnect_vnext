﻿using DinePlan.DineConnect.Common.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Terminals.Dtos
{
	public class CreateOrEditTerminalDto : ConnectOrgLocEditDto
	{
		public CreateOrEditTerminalDto()
		{
			LocationGroup = new CommonLocationGroupDto();
		}

		public int? Id { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }

		[Required]
		public string Name { get; set; }

		[Required]
		public string TerminalCode { get; set; }

		public decimal Float { get; set; }

		public decimal Tolerance { get; set; }

		public string Denominations { get; set; }

		public bool IsDefault { get; set; }

		public bool AutoLogout { get; set; }

		public bool IsEndWorkTimeMoneyShown { get; set; }

		public bool IsCollectionTerminal { get; set; }

		public bool WorkTimeTotalInFloat { get; set; }
		public string Tid { get; set; }

		public int OrganizationId { get; set; }
		public bool AcceptTolerance { get; set; }
		public int? TicketTypeId { get; set; }
		public string Settings { get; set; }
		public bool AutoDayClose { get; set; }
		public List<AutoDayCloseDto> AutoDayCloses { get; set; }
	}

	public class AutoDayCloseDto
	{
		public string CloseTime { get; set; }

		public int NotifyType { get; set; }
	}
}
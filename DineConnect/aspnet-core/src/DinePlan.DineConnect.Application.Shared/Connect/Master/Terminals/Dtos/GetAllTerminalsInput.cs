﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.Terminals.Dtos
{
    public class GetAllTerminalsInput : CommonLocationGroupFilterInputDto
    {
		public string Filter { get; set; }

        public bool IsDeleted { get; set; }

    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.Terminals.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Terminals
{
    public interface ITerminalsAppService : IApplicationService
    {
        Task<PagedResultDto<TerminalDto>> GetAll(GetAllTerminalsInput input);

        Task<CreateOrEditTerminalDto> GetTerminalForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTerminalDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTerminalsToExcel(GetAllTerminalsForExcelInput input);

        Task ActivateItem(EntityDto input);

        Task<List<ApiTerminalOutput>> ApiGetTerminals(ApiLocationInput locationInput);
    }
}
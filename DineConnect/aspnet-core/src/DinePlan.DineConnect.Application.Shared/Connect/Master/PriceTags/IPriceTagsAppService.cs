﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.PriceTags
{
	public interface IPriceTagsAppService : IApplicationService
	{
		Task<PagedResultDto<PriceTagDto>> GetAll(GetAllPriceTagsInput input);

		Task<CreateOrEditPriceTagDto> GetPriceTagForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPriceTagDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetPriceTagsToExcel(GetAllPriceTagsForExcelInput input);

		Task<PagedResultDto<TagMenuItemPortionDto>> GetMenuPricesForTag(GetPriceTagInput input);

		Task<FileDto> GetMenuPricesForTagToExcel(int tagId);

		Task UpdateTagPrice(TagPriceInput input);

		Task<FileDto> GetImportTemplate(int tagId);

		Task ImportPriceTagToDatabase(string fileToken, int? futureDataId);

		Task<List<PriceTagsForComboboxDto>> GetPriceTagsForCombobox(int? selectedId);

		Task CreatePriceTagLocationPrice(PriceTagLocationPriceEditDto input);

		Task<PagedResultDto<PriceTagLocationPriceListDto>> GetAllPriceTagLocationPrice(GetPriceTagInput input);

		Task<bool> CheckExistLocationPrice(PriceTagLocationPriceEditDto input);

		Task DeletePriceTagLocationPrice(EntityDto input);

		Task<TagPriceOutputDto> ApiGetTags(ApiLocationInput input);
        Task ActivateItem(EntityDto input);

		PriceTagDefinitionDto GetPriceTagDefinition(GetPriceTagInput input);

	}
}
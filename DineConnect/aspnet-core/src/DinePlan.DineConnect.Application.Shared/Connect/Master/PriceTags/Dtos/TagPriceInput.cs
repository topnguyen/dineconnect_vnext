﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class TagPriceInput
    {
        [Required]
        public int TagId { get; set; }

        public decimal Price { get; set; }

        [Required]
        public int PortionId { get; set; }

        public int? FutureDataId { get; set; }
    }
}
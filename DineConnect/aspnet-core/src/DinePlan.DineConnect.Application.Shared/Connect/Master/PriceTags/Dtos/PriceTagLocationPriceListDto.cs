﻿using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
	public class PriceTagLocationPriceListDto : CreationAuditedEntity
	{
		public int PriceTagDefinitionId { get; set; }
		public int LocationId { get; set; }
		public string LocationName { get; set; }
		public decimal Price { get; set; }
	}
}
﻿namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class TagMenuItemPortionDto
    {
        public int? Id { get; set; }
        public string MenuName { get; set; }
        public string PortionName { get; set; }
        public int MultiPlier { get; set; }
        public decimal Price { get; set; }
        public int MenuItemId { get; set; }
        public int TagId { get; set; }
        public decimal TagPrice { get; set; }
        public string DisplayName => MenuName + ":" + PortionName;

        public string TagName { get; set; }
    }
}
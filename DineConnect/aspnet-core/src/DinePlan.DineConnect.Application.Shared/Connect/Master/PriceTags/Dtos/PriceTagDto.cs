﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class PriceTagDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }
    }
}
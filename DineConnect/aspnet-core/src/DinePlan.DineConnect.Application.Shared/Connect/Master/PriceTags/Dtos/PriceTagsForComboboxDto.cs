﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class PriceTagsForComboboxDto : ComboboxItemDto
    {
        public PriceTagsForComboboxDto(string value, string displayText, bool isSelected)
        {
            Value = value;
            DisplayText = displayText;
            IsSelected = isSelected;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class TagPriceOutputDto
    {
        public List<TagPriceDto> Tags { get; set; }
    }
    public class TagPriceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TagDefinitionDto> Definitions { get; set; }
        public int Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class TagDefinitionDto
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class GetPriceTagInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int TagId { get; set; }

        public int MenuItemPortionId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public int? FutureDataId { get; set; }
    }
}
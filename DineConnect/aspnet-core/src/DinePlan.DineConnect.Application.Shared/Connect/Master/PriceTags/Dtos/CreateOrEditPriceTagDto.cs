﻿using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class CreateOrEditPriceTagDto : ConnectOrgLocEditDto
    {
        public CreateOrEditPriceTagDto()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public int Type { get; set; }

        public string Departments { get; set; }
        public int OrganizationId { get; set; }
    }
}
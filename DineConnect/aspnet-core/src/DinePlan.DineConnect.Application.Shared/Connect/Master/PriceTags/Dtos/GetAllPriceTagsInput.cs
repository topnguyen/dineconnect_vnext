﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class GetAllPriceTagsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
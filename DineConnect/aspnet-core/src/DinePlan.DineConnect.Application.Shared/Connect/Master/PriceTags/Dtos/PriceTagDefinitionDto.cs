﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PriceTags.Dtos
{
    public class PriceTagDefinitionDto
    {
        public int Id { get; set; }

        public int TagId { get; set; }

        public int MenuPortionId { get; set; }

        public decimal Price { get; set; }
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Numerators
{
    public interface INumeratorAppService : IApplicationService
    {
        Task<FileDto> GetAllToExcel();

        PagedResultDto<NumeratorListDto> GetAll(NumeratorInput input);

        Task<GetNumeratorForEditOutput> GetNumeratorForEdit(NullableIdDto EntityDto);

        Task<int> CreateOrUpdateNumerator(CreateOrUpdateNumeratorInput input);

        Task DeleteNumerator(EntityDto input);

        Task ActivateItem(EntityDto input);

        ListResultDto<ComboboxItemDto> GetResetNumeratorTypes();
    }
}

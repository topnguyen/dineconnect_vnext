﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Numberators;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Numerators.Dtos
{
    public class NumeratorListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string NumberFormat { get; set; }
        
        public string OutputFormat { get; set; }
        
        public bool ResetNumerator { get; set; }
        
        public ResetNumeratorType ResetNumeratorType { get; set; }
    }

    public class NumeratorEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        
        public string Name { get; set; }
        
        public string NumberFormat { get; set; }
        
        public string OutputFormat { get; set; }
        
        public bool ResetNumerator { get; set; }
        
        public ResetNumeratorType ResetNumeratorType { get; set; }
    }

    public class NumeratorInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    
        public bool? AcceptChange { get; set; }
        
        public string Location { get; set; }
        
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public NumeratorInput()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public CommonLocationGroupDto LocationGroup { get; set; }
    }

    public class GetNumeratorForEditOutput : IOutputDto
    {
        public NumeratorEditDto Numerator { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetNumeratorForEditOutput()
        {
            LocationGroup = new CommonLocationGroupDto();
            Numerator = new NumeratorEditDto();
        }
    }

    public class CreateOrUpdateNumeratorInput : IInputDto
    {
        [Required]
        public NumeratorEditDto Numerator { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateNumeratorInput()
        {
            LocationGroup = new CommonLocationGroupDto();
        }
    }
    public class ApiNumeratorOutput
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string NumberFormat { get; set; }
        public string OutputFormat { get; set; }
        public bool ResetNumerator { get; set; }
        public ResetNumeratorType ResetNumeratorType { get; set; }
    }
}

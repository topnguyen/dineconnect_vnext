﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Numerators.Dtos;
using DinePlan.DineConnect.Connect.Master.TransactionTypes.Dtos;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos
{
    public class TicketTypeListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public NumeratorEditDto InvoiceNumerator { get; set; }
        public NumeratorEditDto TicketNumerator { get; set; }
        public NumeratorEditDto OrderNumerator { get; set; }
        public NumeratorEditDto PromotionNumerator { get; set; }
        public NumeratorEditDto FullTaxNumerator { get; set; }
        public NumeratorEditDto QueueNumerator { get; set; }
        public TransactionTypeDto SaleTransactionType { get; set; }
    }

    public class TicketTypeEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public virtual int TicketTypeConfigurationId { get; set; }
        public virtual bool IsTaxIncluded { get; set; }
        public virtual bool IsPreOrder { get; set; }
        public virtual bool IsAllowZeroPriceProducts { get; set; }
        public string Name { get; set; }
        public int? ScreenMenuId { get; set; }
        public int? TicketInvoiceNumeratorId { get; set; }
        public string InvoiceNumeratorName { get; set; }
        public int? TicketNumeratorId { get; set; }
        public string TicketNumeratorName { get; set; }
        public int? OrderNumeratorId { get; set; }
        public string OrderNumeratorName { get; set; }
        public int? PromotionNumeratorId { get; set; }
        public string PromotionNumeratorName { get; set; }
        public int? FullTaxNumeratorId { get; set; }
        public string FullTaxNumeratorName { get; set; }
        public int? QueueNumeratorId { get; set; }
        public string QueueNumeratorName { get; set; }
        public int? ReturnNumeratorId { get; set; }
        public string ReturnNumeratorName { get; set; }

        public int? SaleTransactionType_Id { get; set; }
        public string SaleTransactionTypeName { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public TicketTypeEditDto()
        {
            LocationGroup = new CommonLocationGroupDto();
        }
    }

    public class GetTicketTypeInput : CommonLocationGroupFilterInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public GetTicketTypeInput()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public CommonLocationGroupDto LocationGroup { get; set; }
    }

    public class GetTicketTypeForEditOutput : IOutputDto
    {
        public TicketTypeEditDto TicketType { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public List<CommonLocationGroupDto> Departments { get; set; }

        public GetTicketTypeForEditOutput()
        {
            LocationGroup = new CommonLocationGroupDto();
            TicketType = new TicketTypeEditDto();
        }
    }

    public class CreateOrUpdateTicketTypeInput : IInputDto
    {
        public TicketTypeEditDto TicketType { get; set; }
    }

    public class GetTicketTypeOutputDto : IOutputDto
    {
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }
        public TicketTypeEditDto TicketType { get; set; }

    }
}

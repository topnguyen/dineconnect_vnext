﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos
{
    public class TicketTypeConfigurationListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
    }

    public class TicketTypeConfigurationEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public virtual Collection<TicketTypeEditDto> TicketTypes { get; set; }
    }

    public class GetTicketTypeConfigurationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public GetTicketTypeConfigurationInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetTicketTypeConfigurationForEditOutput : IOutputDto
    {
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetTicketTypeConfigurationForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            TicketTypeConfiguration = new TicketTypeConfigurationEditDto();
        }
    }

    public class CreateTicketTypeConfigurationInput : IInputDto
    {
        [Required]
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateTicketTypeConfigurationInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class TicketTypeFilterInputDto
    {
        public int? Id { get; set; }
        public int? FutureDataId { get; set; }
        public CommonLocationGroupDto LocationGroupToBeFiltered { get; set; }
    }
}

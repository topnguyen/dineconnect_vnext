﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes
{
    public interface ITicketTypeAppService : IApplicationService
    {
        Task<FileDto> GetAllToExcel(GetTicketTypeInput input);

        Task<ListResultDto<ComboboxItemDto>> GetAllTicketTypeForLookupTable();

        Task<PagedResultDto<TicketTypeConfigurationListDto>> GetAll(GetTicketTypeInput input);

        Task<GetTicketTypeOutputDto> GetTicketTypeForEdit(TicketTypeFilterInputDto input);

        Task DeleteTicketType(EntityDto input);

        Task ActivateItem(EntityDto input);

        Task<EntityDto> AddOrEditTicketType(CreateOrUpdateTicketTypeInput input);

        Task DeleteTicketTypeMapping(EntityDto input);
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.TicketTypes.Dtos;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes
{
    public interface ITicketTypeConfigurationAppService : IApplicationService
    {
        PagedResultDto<TicketTypeConfigurationListDto> GetAll(GetTicketTypeConfigurationInput input);

        Task<int> CreateOrUpdateTicketTypeConfiguration(CreateTicketTypeConfigurationInput input);
    }
}

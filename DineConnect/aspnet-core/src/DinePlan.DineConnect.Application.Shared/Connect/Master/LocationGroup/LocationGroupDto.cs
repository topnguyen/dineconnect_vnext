﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationGroup
{
    public class LocationGroupDto : EntityDto
    {
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }
    }
   
    public class SimpleLocationGroupDto
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}
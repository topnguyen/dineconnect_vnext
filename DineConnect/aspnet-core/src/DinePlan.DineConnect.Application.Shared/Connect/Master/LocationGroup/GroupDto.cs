﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.LocationGroup
{
    public class GroupDto : Entity
    {
        [Required]
        public string Name { get; set; }

        public string Code { get; set; }
    }
}
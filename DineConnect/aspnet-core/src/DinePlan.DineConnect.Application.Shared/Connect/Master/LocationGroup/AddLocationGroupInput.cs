﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.LocationGroup
{
    public class AddLocationGroupInput 
    {
        public int GroupId { get; set; }
        public int[] Locations { get; set; } 
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationGroup
{
    public interface ILocationGroupAppService : IApplicationService
    {
        Task CreateOrUpdateGroup(GroupDto group);
        Task<GroupDto> GetGroupAsync(int groupId);
        Task<PagedResultDto<GroupDto>> GetAllGroup(GetGroupInput input);
        Task DeleteGroup(int groupId);

        Task<PagedResultDto<LocationDto>> GetLocationNotInGroup(GetLocationNotInGroupInput input);
        Task AddLocationToGroup(AddLocationGroupInput locationGroup);
        Task RemoveLocationFromGroup(int locationGroupId);
        Task<PagedResultDto<LocationGroupDto>> GetAllLocationGroup(GetLocationsOfGroupInput input);

        Task<PagedResultDto<IdNameDto>> GetSimpleLocationGroups(GetGroupInput input);

        Task<FileDto> GetLocationGroupsToExcel(GetGroupInput input);
    }
}

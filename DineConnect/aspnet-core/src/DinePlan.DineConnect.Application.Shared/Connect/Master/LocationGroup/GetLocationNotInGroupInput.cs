﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.LocationGroup
{
    public class GetLocationNotInGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int OrganizationId { get; set; }
        public int GroupId { get; set; } 
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}
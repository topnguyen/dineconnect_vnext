﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations
{
    public interface IFutureDateInformationsAppService : IApplicationService
    {
        Task<PagedResultDto<FutureDateInformationDto>> GetAll(GetAllFutureDateInformationsInput input);

        Task<CreateOrEditFutureDateInformationDto> GetFutureDateInformationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditFutureDateInformationDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetFutureDateInformationsToExcel(GetAllFutureDateInformationsForExcelInput input);

        ListResultDto<ComboboxItemDto> GetFutureDateTypeForLookupTable();
        Task ActivateItem(EntityDto input);
    }
}
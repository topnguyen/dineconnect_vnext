﻿using DinePlan.DineConnect.Common.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos
{
    public class CreateOrEditFutureDateInformationDto : ConnectOrgLocEditDto
    {
        public CreateOrEditFutureDateInformationDto()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime FutureDate { get; set; }

        public int FutureDateType { get; set; }

        public string Contents { get; set; }

        public int OrganizationId { get; set; }

        public DateTime? FutureDateTo { get; set; }

        public string ObjectContents { get; set; }
    }
}
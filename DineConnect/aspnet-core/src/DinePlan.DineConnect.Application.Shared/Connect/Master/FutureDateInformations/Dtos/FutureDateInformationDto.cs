﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos
{
    public class FutureDateInformationDto : AuditedEntityDto
    {
        public string Name { get; set; }

        public DateTime FutureDate { get; set; }

        public int FutureDateType { get; set; }

        public string FutureDateTypeName { get; set; }

        public string Contents { get; set; }

        public DateTime? FutureDateTo { get; set; }

        public string ObjectContents { get; set; }
    }
}
﻿using System;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos
{
    public class GetAllFutureDateInformationsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDelete { get; set; }

        public DateTime? FutureDate { get; set; }

        public int? Type { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations.Dtos
{
    public class GetAllFutureDateInformationsForExcelInput
    {
        public string Filter { get; set; }
    }
}
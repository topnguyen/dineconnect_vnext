﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.UserLocations.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations
{
    public interface IUserLocationAppService
    {
        Task<PagedResultDto<UserLocationDto>> GetUsersOfLocation(GetUsersOfLocationInput input);
        Task<int> AddUserToLocationAsync(UserLocationDto userLocationDetails);
        Task<PagedResultDto<UserNameIdDto>> FindUsers(FindUserLocationUsersInput input);
        Task<int> RemoveUserFromLocation(int userLocationId);
    }
} 
  
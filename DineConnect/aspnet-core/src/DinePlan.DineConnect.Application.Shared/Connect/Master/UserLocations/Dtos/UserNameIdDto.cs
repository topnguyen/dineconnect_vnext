﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations.Dtos
{
    public class UserNameIdDto :EntityDto<long>
    {
        public string UserName { get; set; }
    }
}
 
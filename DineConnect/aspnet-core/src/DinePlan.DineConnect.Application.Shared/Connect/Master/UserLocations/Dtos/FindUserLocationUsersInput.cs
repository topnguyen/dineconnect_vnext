﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.UserLocations.Dtos
{
    public class FindUserLocationUsersInput : PagedAndFilteredInputDto
    {
        public int LocationId { get; set; }
    }
}
 
﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.UserLocations.Dtos
{
    public class UserLocationDto : EntityDto
    {
        public int LocationId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations.Dtos
{
    public class UserLocationDetailsDto
    {
        public long UserId { get; set; }
        public List<UserLocationDto> Locations { get; set; }
    }
}

﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations.Dtos
{
    public class GetUsersOfLocationInput : PagedAndSortedInputDto, IShouldNormalize
    {

        public int LocationId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id"; 
            }
        }
    }

}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.OrderTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using System.Linq;
using System.Transactions;
namespace DinePlan.DineConnect.Connect.Master.OrderTags
{
	public interface IOrderTagGroupAppService : IApplicationService
	{
		Task<PagedResultDto<OrderTagGroupListDto>> GetAll(GetOrderTagGroupInput inputDto);

		Task<FileDto> GetToExcel(GetOrderTagGroupInput inputDto);

		Task<GetOrderTagGroupForEditOutput> GetTagForEdit(NullableIdDto EntityDto);

		Task CreateOrUpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input);
		Task DeleteOrderTagGroup(EntityDto input);

		Task<ListResultDto<OrderTagGroupListDto>> GetIds();

		Task<ListResultDto<OrderTagGroupEditDto>> GetTagForCategoryAndItem(OrderTagInput input);

		Task ActivateItem(EntityDto input);

		// Report

		Task<OrderTagReportOutput> BuildOrderTagReport(GetOrderTagReportInput input);

		Task<FileDto> BuildOrderTagsExcel(GetOrderTagReportInput input);

		Task<OrderExchangeReportOutput> BuildOrderExchangeReport(GetOrderExchangeReportInput input);

		Task<FileDto> BuildOrderExchangesExcel(GetOrderExchangeReportInput input);
		Task<ReturnProductReportOutput> BuildReturnProductReport(GetOrderExchangeReportInput input);
		Task<FileDto> BuildReturnProductExcel(GetOrderExchangeReportInput input);
		Task<string> GetFilesOrderTagGroupById(int id);

		Task<string> GetFilesOrderTagById(int id);

		Task UpdateFilesOrderTag(int id, string files);

		Task UpdateFilesOrderTagGroup(int id, string files);

		Task<ListResultDto<OrderTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput);

		Task<GetOrderTagLocationPriceForEditOutput> GetOrderTagLocationPriceForEdit(NullableIdDto input);

		Task<PagedResultDto<OrderTagLocationPriceListDto>> GetAllOrderTagLocationPrice(GetOrderTagLocationPriceInput input);

		Task DeleteOrderTagLocationPrice(EntityDto input);

		Task CreateOrderTagLocationPrice(OrderTagLocationPriceEditDto input);

		Task<bool> CheckExistOrderTagLocationPrice(OrderTagLocationPriceEditDto input);
		
		Task<ListResultDto<OrderTagGroupEditDto>> GetOrderTagGroupNames();
		
		Task<ListResultDto<OrderTagDto>> GetOrderTagNames();
	}
}
﻿using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Transactions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Master.Tickets.Dtos
{
	public class CreateOrUpdateTicketInput : IInputDto
	{
		[Required]
		public TicketEditDto Ticket { get; set; }

		public string Email { get; set; }
		public string Phone { get; set; }
	}

	public class TicketEditDto
	{
		public int? Id { get; set; }

		public virtual int LocationId { get; set; }
		public virtual int TicketId { get; set; }
		public virtual string TicketNumber { get; set; }
		public virtual string InvoiceNo { get; set; }
		public virtual DateTime TicketCreatedTime { get; set; }
		public virtual DateTime LastUpdateTime { get; set; }
		public virtual DateTime LastOrderTime { get; set; }
		public virtual DateTime LastPaymentTime { get; set; }
		public virtual bool IsClosed { get; set; }
		public virtual bool IsLocked { get; set; }
		public virtual decimal RemainingAmount { get; set; }
		public virtual decimal TotalAmount { get; set; }
		public virtual string DepartmentName { get; set; }
		public virtual string TicketTypeName { get; set; }
		public virtual string Note { get; set; }
		public virtual string LastModifiedUserName { get; set; }
		public virtual string TicketTags { get; set; }
		public virtual List<TicketTagValue> TTags { get; set; }
		public virtual string TicketStates { get; set; }
		public virtual List<TicketStateValue> TStates { get; set; }

		public virtual string TicketLogs { get; set; }
		public virtual bool TaxIncluded { get; set; }
		public virtual string TerminalName { get; set; }
		public virtual string Email { get; set; }
		public virtual bool PreOrder { get; set; }
		public virtual string TicketEntities { get; set; }
		public virtual int WorkPeriodId { get; set; }
		public IList<OrderEditDto> Orders { get; set; }
		public IList<PaymentEditDto> Payments { get; set; }
		public IList<TicketTransactionDto> Transactions { get; set; }
		public int TenantId { get; set; }
		public DateTime LastPaymentTimeTruc { get; set; }
		public bool Credit { get; set; }
		public string ReferenceNumber { get; set; }
		public string TicketPromotionDetails { get; set; }
		public string ReferenceTicket { get; set; }
		public string DepartmentGroup { get; set; }

		public string TicketStatus { get; set; }
	}

	public class OrderEditDto
	{
		public int? Id { get; set; }
		public int Location_Id { get; set; }
		public int OrderId { get; set; }
		public int TicketId { get; set; }
		public string DepartmentName { get; set; }
		public int MenuItemId { get; set; }
		public string MenuItemName { get; set; }
		public string PortionName { get; set; }
		public decimal Price { get; set; }
		public decimal CostPrice { get; set; }
		public decimal Quantity { get; set; }
		public int PortionCount { get; set; }
		public string Note { get; set; }
		public bool Locked { get; set; }
		public bool CalculatePrice { get; set; }
		public bool IncreaseInventory { get; set; }
		public bool DecreaseInventory { get; set; }
		public string OrderNumber { get; set; }
		public string CreatingUserName { get; set; }
		public DateTime OrderCreatedTime { get; set; }
		public string PriceTag { get; set; }
		public string Taxes { get; set; }
		public List<TaxValueDto> TTaxes { get; set; }
		public string OrderLog { get; set; }
		public string OrderStatus { get; set; }

		public string OrderTags { get; set; }
		public List<OrderTagValue> OTags { get; set; }

		public IList<TransactionOrderTagDto> TransactionTag { get; set; }

		public string OrderStates { get; set; }
		public string OrderPromotionDetails { get; set; }

		public List<OrderStateValue> OStates { get; set; }

		public bool IsPromotionOrder { get; set; }
		public decimal PromotionAmount { get; set; }
		public int MenuItemPortionId { get; set; }
		public int PromotionSyncId { get; set; }

		public int MenuItemType { get; set; }
		public string OrderRef { get; set; }
		public bool IsParent { get; set; }
		public bool IsUpSelling { get; set; }
		public string UpSellingOrderRef { get; set; }
		public decimal OriginalPrice { get; set; }
	}

	public class TransactionOrderTagDto
	{
		public int? Id { get; set; }

		public virtual int OrderId { get; set; }
		public int OrderTagGroupId { get; set; }
		public int OrderTagId { get; set; }
		public int MenuItemPortionId { get; set; }
		public decimal Price { get; set; }
		public decimal Quantity { get; set; }
		public string TagName { get; set; }
		public string TagNote { get; set; }
		public string TagValue { get; set; }
		public bool TaxFree { get; set; }
		public bool AddTagPriceToOrderPrice { get; set; }
	}

	public class ReOrderTagValue
	{
		//AddTagPriceToOrderPrice
		public bool AP
		{
			get;
			set;
		}

		//FreeTag
		public bool FT
		{
			get;
			set;
		}

		//OrderTagGroupId
		public int OI
		{
			get;
			set;
		}

		//Price
		public decimal PR
		{
			get;
			set;
		}

		//Quantity
		public decimal Q
		{
			get;
			set;
		}

		//TagName
		public string TN
		{
			get;
			set;
		}

		//TagNote
		public string TO
		{
			get;
			set;
		}

		//TagValue
		public string TV
		{
			get;
			set;
		}

		//TaxFree
		public bool TF
		{
			get;
			set;
		}

		//MenuItemId
		public int MI
		{
			get;
			set;
		}

		public string ALIASCODE
		{
			get;
			set;
		}

		//GroupSyncId
		public int GID
		{
			get;
			set;
		}

		//TagSyncId
		public int TID
		{
			get;
			set;
		}

		public ReOrderTagValue()
		{
		}
	}

	public class PaymentEditDto
	{
		public int? Id { get; set; }
		public int PaymentTypeId { get; set; }
		public DateTime PaymentCreatedTime { get; set; }
		public decimal TenderedAmount { get; set; }
		public string TerminalName { get; set; }
		public decimal Amount { get; set; }
		public string PaymentUserName { get; set; }
		public string PaymentTags { get; set; }
        public string PaymentTypeName { get; set; }

	}

	public class TicketTransactionDto
	{
		public int? Id { get; set; }
		public int TransactionTypeId { get; set; }
		public decimal Amount { get; set; }
		public string TransactionNote { get; set; }
        public string TransactionTypeName { get; set; }

	}

	[DataContract]
	public class TaxValueDto
	{
		public TaxValueDto()
		{
		}

		[DataMember(Name = "TR")]
		public decimal TaxRate { get; set; }

		[DataMember(Name = "RN")]
		public int Rounding { get; set; }

		[DataMember(Name = "TN")]
		public string TaxTemplateName { get; set; }

		[DataMember(Name = "TT")]
		public int TaxTemplateId { get; set; }

		[DataMember(Name = "AT")]
		public int TaxTempleteAccountTransactionTypeId { get; set; }

		private static TaxValueDto _empty;
		public static TaxValueDto Empty => _empty ?? (_empty = new TaxValueDto());
	}
}
﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.Tickets.Dtos
{
	public class PostInput : IInputDto
	{
		public int LocationId { get; set; }
		public int TenantId { get; set; }
		public string Contents { get; set; }
		public int ContentType { get; set; }
	}

	public class PostOutput : IOutputDto
	{
		public int SyncId { get; set; }
		public string ErrorDesc { get; set; }
	}

	public class PostDataInput : PagedAndSortedResultRequestDto, IShouldNormalize
	{
		public int ContentType { get; set; }
		public int TotalCount { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}

	public class PostDataOutput
	{
		public string Contents { get; set; }
		public int ContentType { get; set; }
		public int Id { get; set; }
	}
}
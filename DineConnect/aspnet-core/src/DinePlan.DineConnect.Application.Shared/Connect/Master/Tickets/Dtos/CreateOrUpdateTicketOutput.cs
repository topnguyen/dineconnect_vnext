﻿namespace DinePlan.DineConnect.Connect.Master.Tickets.Dtos
{
	public class CreateOrUpdateTicketOutput
	{
		public int Ticket { get; set; }
	}
}
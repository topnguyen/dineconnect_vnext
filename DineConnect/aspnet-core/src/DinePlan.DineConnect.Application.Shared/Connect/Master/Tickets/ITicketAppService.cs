﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Tickets.Dtos;
using DinePlan.DineConnect.Connect.Reports.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Tickets
{
	public interface ITicketAppService : IApplicationService
    {
        Task<TicketStatsDto> GetDashboardChart(GetChartInput input);

        List<ChartOutputDto> GetTransactionChart(GetChartInput input);

        List<ChartOutputDto> GetPaymentChart(GetChartInput input);

        List<ChartOutputDto> GetDepartmentChart(GetChartInput input);

        List<ChartOutputDto> GetItemChart(GetChartInput input);

		Task<TicketListDto> GetInputTicket(NullableIdDto input);
        Task<CreateOrUpdateTicketOutput> CreateOrUpdateTicket(CreateOrUpdateTicketInput input);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes
{
    public interface IDinePlanTaxesAppService : IApplicationService
    {
        Task<PagedResultDto<DinePlanTaxDto>> GetAll(GetAllDinePlanTaxesInput input);

        Task<GetDinePlanTaxForEditOutput> GetDinePlanTaxForEdit(EntityDto input);

        Task<int> CreateOrEdit(CreateOrEditDinePlanTaxDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDinePlanTaxesToExcel(GetAllDinePlanTaxesForExcelInput input);

        Task<PagedResultDto<DinePlanTaxReturnDto>> GetLocationTaxesForGivenTax(GetLocationTaxesForGivenTaxInput input, CommonLocationGroupFilterInputDto locationInput);

        Task CreateOrUpdateTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input);

        Task<CreateOrUpdateDinePlanTaxLocationInput> GetDinePlanTaxLocationForEdit(GetDinePlanTaxLocationInput input);

        Task DeleteDinePlanTaxLocation(DeleteDinePlanTaxLocationInput input);

        Task<decimal> GetTaxPercentageForTiffin();

        Task<List<GetDinePlanTaxForComboboxDto>> GetDinePlanTaxForCombobox(int? selectedValue);

        Task<List<ApiTaxOutput>> ApiGetTaxes(ApiLocationInput input);

        Task<int> AddOrEditDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input);

        Task ActivateItem(EntityDto input);
    }
}
﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class CreateOrUpdateDinePlanTaxLocationInput
    {
        public int? FutureDataId { get; set; }

        public DinePlanTaxLocationEditDto DinePlanTaxLocation { get; set; }

        public List<DinePlanTaxMappingEditDto> TaxTemplateMapping { get; set; }
        public CreateOrEditDinePlanTaxDto DinePlanTax { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetDinePlanTaxForEditOutput
    {
        public CreateOrEditDinePlanTaxDto DinePlanTax { get; set; }
    }
}
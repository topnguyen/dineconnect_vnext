﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetDinePlanTaxForViewDto
    {
        public DinePlanTaxDto DinePlanTax { get; set; }
    }
}
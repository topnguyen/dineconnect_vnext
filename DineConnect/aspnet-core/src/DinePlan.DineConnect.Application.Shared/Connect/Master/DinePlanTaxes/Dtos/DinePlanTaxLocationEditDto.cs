﻿using System.Collections.ObjectModel;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DinePlanTaxLocationEditDto : ConnectOrgLocEditDto
    {
        public DinePlanTaxLocationEditDto()
        {
            LocationGroup = new CommonLocationGroupDto();
            DinePlanTaxMappings = new Collection<DinePlanTaxMappingEditDto>();
        }

        public int? Id { get; set; }
        public int DinePlanTaxRefId { get; set; }

        public decimal TaxPercentage { get; set; }
        public int? TransactionTypeId { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public Collection<DinePlanTaxMappingEditDto> DinePlanTaxMappings { get; set; }
        public int FutureDataSno { get; set; }
        public int TenantId { get; set; }
        public bool CloneFlag { get; set; }
    }
}
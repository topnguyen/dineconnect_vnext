﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetLocationTaxesForGivenTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int DinePlanTaxRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
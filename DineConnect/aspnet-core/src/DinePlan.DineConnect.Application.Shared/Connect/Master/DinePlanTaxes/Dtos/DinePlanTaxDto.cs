﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DinePlanTaxDto : CreationAuditedEntityDto
    {
        public string TaxName { get; set; }
        public string TransactionTypeName { get; set; }
    }
}
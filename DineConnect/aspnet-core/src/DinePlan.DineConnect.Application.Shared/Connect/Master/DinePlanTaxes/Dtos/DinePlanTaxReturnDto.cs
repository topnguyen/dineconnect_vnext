﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DinePlanTaxReturnDto
    {
        public int Id { get; set; }
        public decimal TaxPercentage { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetDinePlanTaxLocationInput
    {
        public int Id { get; set; }
    }
}
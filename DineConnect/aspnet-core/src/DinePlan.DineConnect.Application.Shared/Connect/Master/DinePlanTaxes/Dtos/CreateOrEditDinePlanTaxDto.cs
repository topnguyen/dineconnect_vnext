﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class CreateOrEditDinePlanTaxDto : Entity<int?>
    {
        [Required]
        [StringLength(DinePlanTaxConsts.MaxTaxNameLength, MinimumLength = DinePlanTaxConsts.MinTaxNameLength)]
        public string TaxName { get; set; }

        public int OrganizationId { get; set; }

        public int? TransactionTypeId { get; set; }
        public virtual Collection<DinePlanTaxLocationEditDto> DinePlanTaxLocations { get; set; }

        public CreateOrEditDinePlanTaxDto()
        {
            DinePlanTaxLocations = new Collection<DinePlanTaxLocationEditDto>();
        }
    }
}
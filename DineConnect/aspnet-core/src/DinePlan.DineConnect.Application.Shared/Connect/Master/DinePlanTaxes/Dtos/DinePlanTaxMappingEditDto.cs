﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DinePlanTaxMappingEditDto : EntityDto<int?>
    {
        public int DinePlanTaxLocationId { get; set; }

        public int? DepartmentId { get; set; }

        public int? CategoryId { get; set; }

        public int? MenuItemId { get; set; }
    }
}
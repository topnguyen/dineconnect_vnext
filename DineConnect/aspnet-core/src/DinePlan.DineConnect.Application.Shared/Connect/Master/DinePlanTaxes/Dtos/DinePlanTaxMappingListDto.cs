﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DinePlanTaxMappingListDto : FullAuditedEntityDto
    {
        public int DinePlanTaxLocationRefId { get; set; }

        public int? DepartmentId { get; set; }

        public int? CategoryId { get; set; }

        public int? MenuItemId { get; set; }
    }
}
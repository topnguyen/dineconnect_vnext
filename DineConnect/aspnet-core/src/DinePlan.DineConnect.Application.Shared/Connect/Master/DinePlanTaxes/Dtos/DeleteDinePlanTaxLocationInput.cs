﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class DeleteDinePlanTaxLocationInput
    {
        public int Id { get; set; }
    }
}
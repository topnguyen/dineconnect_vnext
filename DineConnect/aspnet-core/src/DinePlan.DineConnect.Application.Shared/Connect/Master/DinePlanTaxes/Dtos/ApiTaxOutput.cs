﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class ApiTaxOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }

        public decimal Percentage { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? MenuItemId { get; set; }
        public string MenuItemName { get; set; }
    }
}

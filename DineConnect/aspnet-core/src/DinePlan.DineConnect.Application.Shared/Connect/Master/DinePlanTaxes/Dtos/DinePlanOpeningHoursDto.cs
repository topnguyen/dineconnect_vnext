﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
	public class DinePlanOpeningHoursDto: CreationAuditedEntityDto
	{
		public string Name { get; set; }
		public DateTime From { get; set; }
		public DateTime To { get; set; }
	}
}

﻿namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetAllDinePlanTaxesForExcelInput
    {
        public string Filter { get; set; }
    }
}
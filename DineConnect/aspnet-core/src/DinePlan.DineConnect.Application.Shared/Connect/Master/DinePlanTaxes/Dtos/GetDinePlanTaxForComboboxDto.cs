﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos
{
    public class GetDinePlanTaxForComboboxDto : ComboboxItemDto
    {
        public GetDinePlanTaxForComboboxDto(string value, string displayText, bool isSelected)
        {
            Value = value;
            DisplayText = displayText;
            IsSelected = isSelected;
        }
    }
}

﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos
{
    public class GetAllForeignCurrenciesForExcelInput
    {
		public string Filter { get; set; }


		 public string PaymentTypeNameFilter { get; set; }

		 
    }
}
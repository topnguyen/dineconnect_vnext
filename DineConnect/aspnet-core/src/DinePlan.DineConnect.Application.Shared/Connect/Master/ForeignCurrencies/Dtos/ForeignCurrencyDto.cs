﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos
{
    public class ForeignCurrencyDto : FullAuditedEntityDto
    {
        [Required]
        public string Name { get; set; }

        public string CurrencySymbol { get; set; }

        [Required]
        public decimal ExchangeRate { get; set; }

        public decimal Rounding { get; set; }

        public int TenantId { get; set; }

        [Required]
        public int PaymentTypeId { get; set; }

        public string PaymentTypeName { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos
{
    public class GetAllForeignCurrenciesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

        public bool IsDeleted { get; set; }


        public string PaymentTypeNameFilter { get; set; }

		 
    }
}
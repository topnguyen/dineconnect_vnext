﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos
{
    public class CreateOrEditForeignCurrencyDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }

        [StringLength(ForeignCurrencyConsts.MaxCurrencySymbolLength, MinimumLength = ForeignCurrencyConsts.MinCurrencySymbolLength)]
        public string CurrencySymbol { get; set; }

        [Required]
        public decimal ExchangeRate { get; set; }

        public decimal Rounding { get; set; }

        public int TenantId { get; set; }

        [Required]
        public int PaymentTypeId { get; set; }

        public int OrganizationId { get; set; }
    }
}
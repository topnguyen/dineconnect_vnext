﻿namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos
{
    public class GetForeignCurrencyForEditOutput
    {
        public CreateOrEditForeignCurrencyDto ForeignCurrency { get; set; }
    }
}
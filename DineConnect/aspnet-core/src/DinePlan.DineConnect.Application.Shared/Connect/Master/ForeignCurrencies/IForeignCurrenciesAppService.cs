﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies
{
    public interface IForeignCurrenciesAppService : IApplicationService
    {
        Task<PagedResultDto<ForeignCurrencyDto>> GetAll(GetAllForeignCurrenciesInput input);

        Task<GetForeignCurrencyForEditOutput> GetForeignCurrencyForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditForeignCurrencyDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetForeignCurrenciesToExcel(GetAllForeignCurrenciesForExcelInput input);

        Task<ListResultDto<ComboboxItemDto>> GetAllPaymentTypeForLookupTable(string input);

        Task ActivateItem(EntityDto input);

        Task<ListResultDto<ForeignCurrencyDto>> ApiGetAll(ApiLocationInput locationInput);
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Departments
{
	public interface IDepartmentsAppService : IApplicationService
	{
		Task<PagedResultDto<DepartmentDto>> GetAll(GetAllDepartmentsInput input);

		Task<ListResultDto<DepartmentDto>> GetListAll();

		Task SaveSortSortItems(int[] menuItems);

		Task<CreateOrEditDepartmentDto> GetDepartmentForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditDepartmentDto input);

		Task Delete(EntityDto input);

		Task ActivateItem(EntityDto input);

		Task<FileDto> GetDepartmentsToExcel(GetAllDepartmentsForExcelInput input);

		Task<ListResultDto<ComboboxItemDto>> GetAllPriceTagForLookupTable(string input);

		Task<ListResultDto<ComboboxItemDto>> GetDepartmentsForCombobox();

		// Group

		Task<PagedResultDto<DepartmentGroupListDto>> GetAllDepartmentGroup(GetDepartmentGroupInput inputDto);

		Task<FileDto> GetAllDepartmentGroupToExcel();

		Task<GetDepartmentGroupForEditOutput> GetDepartmentGroupForEdit(NullableIdDto EntityDto);

		Task CreateOrUpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input);

		Task<bool> DeleteDepartmentGroup(EntityDto input);

		Task<ListResultDto<DepartmentGroupListDto>> GetDepartmentGroupIds();

		Task ActivateDepartmentGroupItem(EntityDto input);

		Task<ListResultDto<DepartmentGroupListDto>> GetDepartmentGroups();

		Task<ListResultDto<ComboboxItemDto>> GetComboBoxDepartmentGroups();

		Task<PagedResultDto<NameValueDto>> GetDepartmentsForLookup(GetDepartmentsForLoockup input);

		Task<ListResultDto<ApiDepartmentOutput>> ApiGetAll(ApiLocationInput inputDto);
	}
}
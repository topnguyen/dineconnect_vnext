﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class GetAllDepartmentsForExcelInput
    {
		public string Filter { get; set; }


		 public string PriceTagNameFilter { get; set; }

		 
    }
}
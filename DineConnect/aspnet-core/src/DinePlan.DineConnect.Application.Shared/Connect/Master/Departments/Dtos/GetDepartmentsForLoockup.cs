﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
	public class GetDepartmentsForLoockup : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }
	}
}
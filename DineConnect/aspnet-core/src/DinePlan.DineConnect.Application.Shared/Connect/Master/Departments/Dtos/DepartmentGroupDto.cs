﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class DepartmentGroupListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }
    public class DepartmentGroupEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOns { get; set; }
    }

    public class GetDepartmentGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDepartmentGroupForEditOutput : IOutputDto
    {
        public DepartmentGroupEditDto DepartmentGroup { get; set; }
    }
    public class CreateOrUpdateDepartmentGroupInput : IInputDto
    {
        [Required]
        public DepartmentGroupEditDto DepartmentGroup { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class ApiDepartmentOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TagId { get; set; }
        public int SortOrder { get; set; }
        public string DepartmentGroup { get; set; }
        public string DepartmentGroupCode { get; set; }
        public int? TicketTypeId { get; set; }
        public string Code { get; set; }
    }
}

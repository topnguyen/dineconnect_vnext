﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class DepartmentDto : CreationAuditedEntityDto
    {
        public virtual string Code { get; set; }

        public virtual string AddOns { get; set; }

        public string Name { get; set; }

        public string PriceTagName { get; set; }

        public int? PriceTagId { get; set; }

        public int? DepartmentGroupId { get; set; }

        public string DepartmentGroupName { get; set; }

        public int? TicketTypeId { get; set; }
    }
}
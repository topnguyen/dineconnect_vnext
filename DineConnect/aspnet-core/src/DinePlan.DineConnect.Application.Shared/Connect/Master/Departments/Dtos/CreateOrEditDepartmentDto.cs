﻿using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class CreateOrEditDepartmentDto : ConnectOrgLocEditDto
    {
        public CreateOrEditDepartmentDto()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public int? PriceTagId { get; set; }

        public int OrganizationId { get; set; }

        public virtual string Code { get; set; }

        public virtual string AddOns { get; set; }

        public int? DepartmentGroupId { get; set; }

        public string DepartmentGroupName { get; set; }

        public int? TicketTypeId { get; set; }
    }
}
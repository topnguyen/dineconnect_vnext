﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.Departments.Dtos
{
    public class GetAllDepartmentsInput : CommonLocationGroupFilterInputDto
    {
		public string Filter { get; set; }

        public bool IsDeleted { get; set; }

        public string PriceTagNameFilter { get; set; }

		 
    }
}
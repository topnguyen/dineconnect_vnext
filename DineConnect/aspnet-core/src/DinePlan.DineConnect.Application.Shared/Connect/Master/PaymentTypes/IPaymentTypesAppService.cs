﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes
{
    public interface IPaymentTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAll(GetAllPaymentTypesInput input);

        Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPaymentTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPaymentTypesToExcel(GetAllPaymentTypesForExcelInput input);

        Task<PagedResultDto<PaymentTypeLocationLookupTableDto>> GetAllLocationForLookupTable(GetAllForLookupTableInput input);

        Task<ListResultDto<ComboboxItemDto>> GetPaymentProcessors();

        Task UpdatePaymentTypeImage(UpdatePaymentTypeImageInput input);
		ListResultDto<ComboboxItemDto> GetPaymentTenderType();
		Task<PagedResultDto<NameValueDto>> GetPaymentTypeForLookupTable(GetAllForLookupTableInput input);
        Task<ListResultDto<ApiPaymentTypeOutput>> ApiGetAll(ApiTenantInputDto inputDto);

        Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAllItems();
        Task ActivateItem(EntityDto input);
    }
}
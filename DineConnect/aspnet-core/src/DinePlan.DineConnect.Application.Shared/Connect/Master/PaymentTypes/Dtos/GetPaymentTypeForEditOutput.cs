﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class GetPaymentTypeForEditOutput
    {
		public CreateOrEditPaymentTypeDto PaymentType { get; set; }

		public string LocationName { get; set;}


    }
}
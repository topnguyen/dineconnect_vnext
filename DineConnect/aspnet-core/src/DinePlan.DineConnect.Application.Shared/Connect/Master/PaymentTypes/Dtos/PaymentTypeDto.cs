﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
	public class PaymentTypeDto : FullAuditedEntityDto
	{
		public string Name { get; set; }

		public bool AcceptChange { get; set; }

		public bool DisplayInShift { get; set; }

		public bool NoRefund { get; set; }

		public string AccountCode { get; set; }

		public int SortOrder { get; set; }

		public int PaymentProcessor { get; set; }

		public string Processors { get; set; }

		public string ButtonColor { get; set; }

		public string PaymentGroup { get; set; }

		public string Files { get; set; }

		public Guid? DownloadImage { get; set; }

		public string PaymentTag { get; set; }

		public int? LocationId { get; set; }

		public string LocationName { get; set; }
	}
}
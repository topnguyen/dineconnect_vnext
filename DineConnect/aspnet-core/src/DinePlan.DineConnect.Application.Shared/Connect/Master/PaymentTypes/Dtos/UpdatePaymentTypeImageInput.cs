﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class UpdatePaymentTypeImageInput
    {
        public int PaymentTypeId { get; set; }

        [Required]
        [MaxLength(400)]
        public string FileToken { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class PaymentTypeLocationLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}
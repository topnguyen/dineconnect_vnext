﻿using System;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.PaymentType;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class GetAllPaymentTypesInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public string LocationNameFilter { get; set; }
        public bool? IsAcceptChange { get; set; }

        public bool IsDeleted { get; set; }
    }

   
    public class ApiPaymentTypeOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool AcceptChange { get; set; }
        public int SortOrder { get; set; }
        public int PaymentProcessor { get; set; }
        public string PaymentProcessorName { get; set; }

        public string Processors { get; set; }
        public string AccountCode { get; set; }
        public string Group { get; set; }
        public bool NoRefund { get; set; }
        public string Files { get; set; }
        public Guid? DownloadImage { get; set; }
        public string Departments { get; set; }
        public bool AutoPayment { get; set; }
        public bool DisplayInShift { get; set; }
        public string ButtonColor { get; set; }
        public PaymentTenderType PaymentTenderType { get; set; }
        public string PaymentTag { get; set; }
    }
}
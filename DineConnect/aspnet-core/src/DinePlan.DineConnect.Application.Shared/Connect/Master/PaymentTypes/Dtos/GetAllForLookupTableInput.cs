﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
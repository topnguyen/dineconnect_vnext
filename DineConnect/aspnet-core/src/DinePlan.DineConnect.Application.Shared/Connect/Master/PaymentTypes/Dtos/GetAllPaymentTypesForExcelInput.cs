﻿namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class GetAllPaymentTypesForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public string LocationNameFilter { get; set; }

        public bool? IsAcceptChange { get; set; }
    }
}
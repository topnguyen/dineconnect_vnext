﻿namespace DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos
{
    public class GetPaymentTypeForViewDto
    {
        public PaymentTypeDto PaymentType { get; set; }

        public string LocationName { get; set; }
    }
}
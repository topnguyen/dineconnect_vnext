﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class CalculationDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public int TransactionTypeId { get; set; }

        public string TransactionTypeName { get; set; }
        public string ConfirmationTypeName { get; set; }

        public string CalculationTypeName { get; set; }
        public int SortOrder { get; set; }
        public int FontSize { get; set; }
        public decimal Amount { get; set; }
        public bool IncludeTax { get; set; }
        public bool DecreaseAmount { get; set; }
        public string Departments { get; set; }
        public string Locations { get; set; }
        public string DepartmentNames { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class ApiCalculationOutput
    {
        public List<ApiCalculation> Calculations { get; set; }

        public ApiCalculationOutput()
        {
            Calculations = new List<ApiCalculation>();
        }
    }
}
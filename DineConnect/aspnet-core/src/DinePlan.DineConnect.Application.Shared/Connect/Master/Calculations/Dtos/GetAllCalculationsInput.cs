﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class GetAllCalculationsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public string TransactionTypeNameFilter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
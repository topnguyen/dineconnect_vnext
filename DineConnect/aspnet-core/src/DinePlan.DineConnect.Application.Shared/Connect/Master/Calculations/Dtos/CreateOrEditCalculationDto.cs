﻿using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class CreateOrEditCalculationDto : ConnectOrgLocEditDto
    {
        public CreateOrEditCalculationDto()
        {
            ConfirmationType = 0;
            CalculationTypeId = 0;
            Amount = 0M;
            LocationGroup = new CommonLocationGroupDto();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public int OrganizationId { get; set; }

        public string ButtonHeader { get; set; }

        public int ConfirmationType { get; set; }

        public int CalculationTypeId { get; set; }

        public decimal Amount { get; set; }

        public bool IncludeTax { get; set; }

        public bool DecreaseAmount { get; set; }

        public string Departments { get; set; }

        public int TransactionTypeId { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class GetAllCalculationsForExcelInput
    {
        public string Filter { get; set; }

        public string TransactionTypeNameFilter { get; set; }
    }
}
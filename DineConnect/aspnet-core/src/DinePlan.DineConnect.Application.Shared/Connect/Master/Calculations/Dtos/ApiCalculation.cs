﻿namespace DinePlan.DineConnect.Connect.Master.Calculations.Dtos
{
    public class ApiCalculation
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ButtonHeader { get; set; }
        public int SortOrder { get; set; }
        public int FontSize { get; set; }
        public decimal Amount { get; set; }
        public bool IncludeTax { get; set; }
        public bool DecreaseAmount { get; set; }
        public string Departments { get; set; }
        public string Locations { get; set; }
        public bool Group { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public int CalculationTypeId { get; set; }
        public int ConfirmationType { get; set; }
    }
}
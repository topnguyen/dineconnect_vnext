﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Calculations.Dtos;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Calculations
{
    public interface ICalculationsAppService : IApplicationService
    {
        Task<PagedResultDto<CalculationDto>> GetAll(GetAllCalculationsInput input);

        Task<CreateOrEditCalculationDto> GetCalculationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCalculationDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetCalculationsToExcel(GetAllCalculationsForExcelInput input);

        Task<ListResultDto<ComboboxItemDto>> GetAllTransactionTypeForLookupTable(string input);

        ListResultDto<ComboboxItemDto> GetCalculationTypeForLookupTable(string input);

        ListResultDto<ComboboxItemDto> GetConfirmationTypeForLookupTable(string input);

        Task<ListResultDto<ComboboxItemDto>> GetDepartmentForCombobox();

        Task ActivateItem(EntityDto input);

        Task<ApiCalculationOutput> ApiGetCalculations(ApiLocationInput locationInput);
    }
}
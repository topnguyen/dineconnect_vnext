﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts
{
    public interface ITillAccountsAppService : IApplicationService
    {
        Task<PagedResultDto<TillAccountDto>> GetAll(GetAllTillAccountsInput input);

        Task<CreateOrEditTillAccountDto> GetTillAccountForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTillAccountDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTillAccountsToExcel(GetAllTillAccountsForExcelInput input);
        Task<PagedResultDto<TillTransactionListDto>> BuildTillTransactionReport(GetTillAccountInput inputDto);

        Task<FileDto> BuildTillTransactionReportExcel(GetTillAccountInput input);
        ListResultDto<ComboboxItemDto> GetAccountTypeForLookupTable();
        Task ActivateItem(EntityDto input);
        Task<ListResultDto<ApiTillAccountOutput>> ApiGetAll(ApiLocationInput input);
        Task<CreateOrUpdateTillOutput> CreateOrUpdateTillTrans(CreateOrUpdateTillInput input);
        Task<List<TillAccountListDto>> GetTillAccounts();
    }
}
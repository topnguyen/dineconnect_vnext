﻿using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class CreateOrEditTillAccountDto : ConnectOrgLocEditDto
    {
        public CreateOrEditTillAccountDto()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public int? Id { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }

        [Required]
        public string Name { get; set; }

        public int? TillAccountTypeId { get; set; }

        public int OrganizationId { get; set; }
    }
}
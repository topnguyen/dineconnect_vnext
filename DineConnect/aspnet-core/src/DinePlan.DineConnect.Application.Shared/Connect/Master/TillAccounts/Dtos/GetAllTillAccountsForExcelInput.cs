﻿namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class GetAllTillAccountsForExcelInput
    {
        public string Filter { get; set; }
    }
}
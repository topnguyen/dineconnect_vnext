﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{ 
    public class GetTillAccountInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<int> Locations { get; set; }

        public List<int> Accounts { get; set; }
        public int? AccountType { get; set; }

        public string Filter { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool IsDeleted { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string UserName { get; set; }
        public GetTillAccountInput()
        {
            LocationGroup = new CommonLocationGroupDto();
        }

        public CommonLocationGroupDto LocationGroup { get; set; }

    }
}

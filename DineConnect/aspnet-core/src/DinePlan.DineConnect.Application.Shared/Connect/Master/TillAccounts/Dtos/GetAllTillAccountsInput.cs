﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class GetAllTillAccountsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public int? TillAccountTypeId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
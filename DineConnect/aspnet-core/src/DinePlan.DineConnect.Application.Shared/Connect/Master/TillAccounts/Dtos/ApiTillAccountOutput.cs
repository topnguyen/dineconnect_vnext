﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class ApiTillAccountOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TillAccountTypeId { get; set; }
    }
    public class CreateOrUpdateTillInput
    {
        [Required]
        public TillTransactionInput Transaction { get; set; }
    }
    public class TillAccountListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
        public int TillAccountTypeId { get; set; }
        public string DisplayLocation { get; set; }
        public string AccountType => ((TillAccounTypes)TillAccountTypeId).ToString();
    }
    public enum TillAccounTypes
    {
        Income = 0,
        Expense = 1,
        AccuralIncome = 2,
        AccuralExpense = 3
    }

    public class TillTransactionInput
    {
        public int? Id { get; set; }
        public int LocalId { get; set; }
        public int LocationId { get; set; }
        public int AccountId { get; set; }
        public decimal TotalAmount { get; set; }

        public int AccountType { get; set; }
        public string User { get; set; }
        public string Remarks { get; set; }
        public DateTime TransactionTime { get; set; }
        public int LocalWorkPeriodId { get; set; }
        public int WorkPeriodId { get; set; }
        public bool Status { get; set; }
    }

    public class CreateOrUpdateTillOutput
    {
        public int Trans { get; set; }
    }
}
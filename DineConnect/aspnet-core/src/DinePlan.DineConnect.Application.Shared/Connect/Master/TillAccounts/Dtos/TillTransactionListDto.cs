﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class TillTransactionListDto : FullAuditedEntityDto
    {
        public virtual string LocationName { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string User { get; set; }
        public virtual DateTime TransactionTime { get; set; }
        public virtual bool Status { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual int TillAccountId { get; set; }
        public virtual string TillAccountName { get; set; }
    }

}

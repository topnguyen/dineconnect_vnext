﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts.Dtos
{
    public class TillAccountDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public int TillAccountTypeId { get; set; }
        public string TillAccountType { get; set; }
    }
}
﻿using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos
{
    public class GetAllPlanReasonsInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
    }
}
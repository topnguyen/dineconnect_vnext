﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos
{
    public class PlanReasonDto : FullAuditedEntityDto
    {
        public string Reason { get; set; }

        public string AliasReason { get; set; }

        public string ReasonGroup { get; set; }
    }
}
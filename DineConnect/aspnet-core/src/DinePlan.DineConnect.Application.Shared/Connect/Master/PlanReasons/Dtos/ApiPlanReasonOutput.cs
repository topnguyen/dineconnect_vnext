﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos
{
    public class ApiPlanReasonOutput
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public string AliasReason { get; set; }
        public string ReasonGroup { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos
{
    public class GetAllPlanReasonsForExcelInput
    {
        public string Filter { get; set; }
    }
}
﻿using DinePlan.DineConnect.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos
{
	public class CreateOrEditPlanReasonDto : ConnectOrgLocEditDto
	{
		public CreateOrEditPlanReasonDto()
		{
			LocationGroup = new CommonLocationGroupDto();
		}

		public int? Id { get; set; }
		public CommonLocationGroupDto LocationGroup { get; set; }

		[Required]
		public string Reason { get; set; }

		public string AliasReason { get; set; }

		public string ReasonGroup { get; set; }
		public int OrganizationId { get; set; }
		public bool ReasonGroupCheckBox { get; set; }
	}
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Master.PlanReasons.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons
{
    public interface IPlanReasonsAppService : IApplicationService
    {
        Task<PagedResultDto<PlanReasonDto>> GetAll(GetAllPlanReasonsInput input);

        Task<CreateOrEditPlanReasonDto> GetPlanReasonForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPlanReasonDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPlanReasonsToExcel(GetAllPlanReasonsForExcelInput input);

        ListResultDto<ComboboxItemDto> GetReasonGroupForLookupTable();

        Task<List<ApiPlanReasonOutput>> ApiGetPlanReasons(ApiLocationInput locationInput);

        Task ActivateItem(EntityDto input);
    }
}
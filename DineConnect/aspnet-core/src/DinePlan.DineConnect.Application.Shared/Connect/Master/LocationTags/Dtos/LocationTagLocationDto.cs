﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class LocationTagLocationDto : EntityDto
    {
        public int LocationId { get; set; }

        public int LocationTagId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
    }
}
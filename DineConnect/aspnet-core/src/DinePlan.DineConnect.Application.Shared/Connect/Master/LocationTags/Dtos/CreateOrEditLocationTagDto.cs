﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class CreateOrEditLocationTagDto : EntityDto<int?>
    {

		[Required]
		public string Name { get; set; }
		
		
		public string Code { get; set; }
		
		

    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class LocationTagDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }

        public string Code { get; set; }
    }
}
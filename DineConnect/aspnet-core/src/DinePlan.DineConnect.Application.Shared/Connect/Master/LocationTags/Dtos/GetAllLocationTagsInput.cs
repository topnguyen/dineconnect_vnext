﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class GetAllLocationTagsInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
}
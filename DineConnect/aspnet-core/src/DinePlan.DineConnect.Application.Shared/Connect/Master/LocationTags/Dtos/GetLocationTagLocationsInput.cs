﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class GetLocationTagLocationsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int LocationTagId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
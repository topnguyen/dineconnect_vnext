﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class AddLocationToTagDto : EntityDto<int?>
    {
        public int[] LocationIds { get; set; }

        public int LocationTagId { get; set; }
    }
}
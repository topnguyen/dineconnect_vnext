﻿namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
	public class SimpleLocationTagDto
	{
		public virtual int Id { get; set; }
		public virtual string Name { get; set; }
	}
}
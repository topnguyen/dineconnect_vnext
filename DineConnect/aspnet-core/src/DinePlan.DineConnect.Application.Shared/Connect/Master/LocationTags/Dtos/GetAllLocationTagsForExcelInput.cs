﻿namespace DinePlan.DineConnect.Connect.Master.LocationTags.Dtos
{
    public class GetAllLocationTagsForExcelInput
    {
        public string Filter { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.LocationTags
{
    public interface ILocationTagsAppService : IApplicationService
    {
        Task<PagedResultDto<LocationTagDto>> GetAllTags(GetAllLocationTagsInput input);

        Task<CreateOrEditLocationTagDto> GetLocationTagForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLocationTagDto input);

        Task DeleteTag(EntityDto input);

        Task<FileDto> GetLocationTagsToExcel(GetAllLocationTagsForExcelInput input);

        Task<PagedResultDto<LocationTagLocationDto>> GetAllLocationOfTag(GetLocationTagLocationsInput input);

        Task<PagedResultDto<LocationDto>> GetLocationNotInTag(GetLocationTagLocationsInput input);

        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Master.LocationTags.Edit" })]
        Task AddLocationToTag(AddLocationToTagDto input);

        [AbpAuthorize(new[] { "Pages.Tenant.Connect.Master.LocationTags.Delete" })]
        Task DeleteLoctionTagLocation(EntityDto input);

        Task<PagedResultDto<IdNameDto>> GetSimpleLocationTags(GetAllLocationTagsInput input);
    }
}
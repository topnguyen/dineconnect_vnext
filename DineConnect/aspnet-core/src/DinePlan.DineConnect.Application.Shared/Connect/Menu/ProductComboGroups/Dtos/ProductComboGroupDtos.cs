﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos
{
    public class GetAllProductComboGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string ComboGroupSelectionIds { get; set; }

        public bool IsDeleted { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }

    public class GetAllProductComboGroupDto : CreationAuditedEntityDto<int>
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
    }

    // --

    public class CreateEditProductComboGroupInput
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public List<CreateEditProductComboItemDto> ComboItems { get; set; }
    }

    public class CreateEditProductComboItemDto : ProductComboItemEditDto
    {
        public int MenuItemPortionId { get; set; }

        public CreateEditProductComboItemDto()
        {
            Count = 1;
        }
    }
}
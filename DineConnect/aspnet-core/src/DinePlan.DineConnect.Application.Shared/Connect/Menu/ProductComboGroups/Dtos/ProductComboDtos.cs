﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos
{
    public class ProductComboDto : CreationAuditedEntityDto<int>
    {
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public bool AddPriceToOrderPrice { get; set; }
        public List<ProductComboGroupDto> ProductComboGroupDtos { get; set; }
    }
    
    public class ProductComboGroupDto 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int? ProductComboId { get; set; }
    }

    public class CreateOrEditComboInputDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool AddPriceToOrderPrice { get; set; }
        public List<int> ProductComboGroupIds { get; set; }
    }
    
    public class GetAllProductComboInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }
        public string ProductComboSelectionIds { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups
{
    public interface IProductComboAppService
    {
        Task<PagedResultDto<ProductComboDto>> GetAll(GetAllProductComboInput input);
        Task<ProductComboDto> GetForEdit(int id);

        Task<int> CreateOrEdit(CreateOrEditComboInputDto input);
        Task Delete(int id);
    }
}
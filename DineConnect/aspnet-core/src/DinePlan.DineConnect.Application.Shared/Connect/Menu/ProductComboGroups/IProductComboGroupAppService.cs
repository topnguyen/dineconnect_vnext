﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Menu.ProductComboGroups.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductComboGroups
{
    public interface IProductComboGroupAppService
    {
        Task<PagedResultDto<ProductComboGroupEditDto>> GetAll(GetAllProductComboGroupInput input);

        Task<FileDto> ExportToExcel();

        Task<CreateEditProductComboGroupInput> GetForEdit(int id);

        Task<int> Create(CreateEditProductComboGroupInput input);

        Task<int> Edit(CreateEditProductComboGroupInput input);

        Task Delete(int id);

        Task<List<CreateEditProductComboItemDto>> GetComboItemsByComboGroupId(int combogroupId);

        Task ActivateItem(EntityDto input);

        Task SortComboGroups(List<ProductComboGroupEditDto> input);
    }
}
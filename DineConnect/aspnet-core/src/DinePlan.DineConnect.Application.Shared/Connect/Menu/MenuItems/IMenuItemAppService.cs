﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public interface IMenuItemAppService : IApplicationService
    {
        Task<PagedResultDto<MenuItemListDto>> GetAll(GetMenuItemInput input);

        Task<PagedResultDto<MenuItemEditDto>> GetAllItems(GetMenuItemLocationInput input, bool restrictLocation = false);

        Task<GetMenuItemForEditOutput> GetMenuItemForEdit(NullableIdDto input);

        Task<int> CreateOrUpdateAsync(MenuItemEditDto input);

        Task DeleteAsync(int input);

        Task<PagedResultDto<IdNameDto>> GetSinglePortionItems(GetMenuItemForPromotionInput input);

        Task<PagedResultDto<IdNameDto>> GetAllMenuItemIncludePortionItems(GetMenuItemForPromotionInput input);
        Task<PagedResultDto<IdNameDto>> GetAllPortionItems(GetPortionItemInput input);

        Task<ListResultDto<MenuItemListDto>> GetMenuItemForCategoryCombobox(NullableIdDto input);

        ListResultDto<ComboboxItemDto> GetFoodTypes();

        ListResultDto<ComboboxItemDto> GetProductTypes();

        Task<ListResultDto<ComboboxItemDto>> GetTransactionTypesForCombobox();

        Task CreateOrUpdateMenuItem(CreateOrUpdateMenuItemInput input);

        Task<ListResultDto<ComboboxItemDto>> GetComboItems();

        Task<bool> IsBarcodeExists(string barcode);

        Task<List<MenuItemLocationPriceDto>> GetLocationPricesForMenu(GetMenuPortionPriceInput input);

        Task UpdateLocationPrice(List<LocationPriceInput> inputs);
        
        Task<FileDto> GetAllMenuItemToExcel();

        Task<PagedResultDto<TiffinProductLookupTableDto>> GetAllTiffinProductSetForLookupTable(Dtos.GetAllForLookupTableInput input);

        Task<FileDto> GetImportMenuItemTemplateToExcel();

        Task ImportMenuItemToDatabase(string fileToken);

        Task<PagedResultDto<MenuItemAndPriceForSelection>> GetMenuItemAndPriceForSelection(GetMenuItemAndPriceForSelectionInput input);

        // --

        Task<PagedResultDto<MenuItemPortionDto>> GetLocationMenuItemPrices(GetMenuPortionPriceInput input);

        Task<ApiMenuOutput> ApiItemsForLocation(ApiLocationInput locationInput);

        Task ActivateItem(EntityDto input);
    }
}
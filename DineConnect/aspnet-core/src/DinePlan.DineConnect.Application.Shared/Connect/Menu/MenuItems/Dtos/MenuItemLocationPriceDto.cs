﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class MenuItemLocationPriceDto
    {
        public MenuItemLocationPriceDto()
        {
            MenuItemPortions = new List<MenuItemPortionDto>();
        }

        public int MenuItemId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public List<MenuItemPortionDto> MenuItemPortions { get; set; }
    }
}
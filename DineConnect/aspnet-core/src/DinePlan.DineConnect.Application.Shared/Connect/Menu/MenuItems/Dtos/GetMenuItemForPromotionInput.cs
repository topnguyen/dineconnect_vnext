﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemForPromotionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int? ProductType { get; set; }

        public List<int> ExceptIds { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }

        public GetMenuItemForPromotionInput()
        {
            ExceptIds = new List<int>();
        }
    }
}
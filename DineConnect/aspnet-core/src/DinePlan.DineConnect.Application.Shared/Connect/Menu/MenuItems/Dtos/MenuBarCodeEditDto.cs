﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public class MenuBarCodeEditDto
    {
        public int? Id { get; set; }
        public string Barcode { get; set; }
    }
}
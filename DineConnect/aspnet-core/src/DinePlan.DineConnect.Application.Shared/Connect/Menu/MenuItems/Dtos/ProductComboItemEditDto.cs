﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class ProductComboItemEditDto
    {
        public ProductComboItemEditDto()
        {
            Count = 1;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }

        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }
    }
}
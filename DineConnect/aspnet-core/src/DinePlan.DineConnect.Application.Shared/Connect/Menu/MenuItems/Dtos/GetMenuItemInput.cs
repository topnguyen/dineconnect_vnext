﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemInput : CommonLocationGroupFilterInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }
        public int? CategoryId { get; set; }
        public int? MenuItemType { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}
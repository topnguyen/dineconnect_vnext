﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class MenuItemAndPriceForSelection : EntityDto
    {
        public string Name { get; set; }
        public List<MenuItemPortionEditDto> Prices { get; set; }
        public int ProductType { get; set; }
        public int? ProductComboId { get; set; }
    }
}

﻿using DinePlan.DineConnect.Connect.Master.LocationGroup;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class CreateOrUpdateMenuItemInput
    {
        public MenuItemEditDto MenuItem { get; set; }

        public int ProductType { get; set; }
        public List<ProductComboGroupEditDto> Combos { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool RefreshImage { get; set; }
        public bool NotOverride { get; set; }
        public bool UpdateItem { get; set; }
        public List<MenuItemLocationPriceDto> MenuItemLocationPrices { get; set; }

        public CreateOrUpdateMenuItemInput()
        {
            LocationGroup = new LocationGroupDto();
            Combos = new List<ProductComboGroupEditDto>();
            MenuItem = new MenuItemEditDto();
            MenuItemLocationPrices = new List<MenuItemLocationPriceDto>();
        }

    }
}
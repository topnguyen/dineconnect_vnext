﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemScheduleInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? CategoriesId { get; set; }
        public int? CustomerId { get; set; }
        public string Days { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}
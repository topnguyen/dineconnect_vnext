﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuPortionPriceInput : IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MenuName";
            }
        }

        public int LocationId { get; set; }
        public int? MenuItemId { get; set; }

        [Range(0, int.MaxValue)]
        public int MaxResultCount { get; set; } //0: Unlimited.

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public string Sorting { get; set; }
    }
}
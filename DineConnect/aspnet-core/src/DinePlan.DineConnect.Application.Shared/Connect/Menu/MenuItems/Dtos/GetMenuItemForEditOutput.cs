﻿using DinePlan.DineConnect.Connect.Master.LocationGroup;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemForEditOutput
    {
        public GetMenuItemForEditOutput()
        {
            Combos = new List<ProductComboGroupEditDto>();
            LocationGroup = new LocationGroupDto();
        }

        public MenuItemEditDto MenuItem { get; set; }
        public List<ProductComboGroupEditDto> Combos { get; set; }
        public int ProductType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        //public UrbanPiperMenuAddOn UrbanPiper { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
	public class MenuItemPortionDto
	{
		public int? Id { get; set; }
		public string MenuName { get; set; }
		public string PortionName { get; set; }
		public int MultiPlier { get; set; }
		public decimal Price { get; set; }
		public int MenuItemId { get; set; }
		public int LocationId { get; set; }
		public decimal LocationPrice { get; set; }
		public string DisplayName => MenuName + ":" + PortionName;
		public string LocationName { get; set; }

		public string Barcode { get; set; }
	}
}
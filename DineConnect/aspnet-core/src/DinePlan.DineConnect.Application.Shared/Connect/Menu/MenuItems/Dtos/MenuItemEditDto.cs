﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class MenuItemEditDto : ConnectOrgLocEditDto
    {
        public MenuItemEditDto()
        {
            Portions = new Collection<MenuItemPortionEditDto>();
            Barcodes = new Collection<MenuBarCodeEditDto>();
            UpMenuItems = new List<UpMenuItemEditDto>();
            UpSingleMenuItems = new List<UpMenuItemEditDto>();
            FoodType = 4;
            LocationGroup = new CommonLocationGroupDto();
            MenuItemDescriptions = new List<ApiMenuItemDescription>();
            MenuItemSchedules = new Collection<MenuItemScheduleEditDto>();
        }

        public int? Id { get; set; }
        public string BarCode { get; set; }

        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }

        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public bool RestrictPromotion { get; set; }
        public bool NoTax { get; set; }

        public int? CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }

        // For Importing Template
        public string TPName { get; set; }

        public decimal TPPrice { get; set; }

        public int? TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }

        public string Name { get; set; }
        public int? ProductType { get; set; }

        public string Tag { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }

        public string HsnCode { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }
        public Collection<MenuBarCodeEditDto> Barcodes { get; set; }
        public Collection<MenuItemPortionEditDto> Portions { get; set; }
        public List<UpMenuItemEditDto> UpMenuItems { get; set; }
        public List<UpMenuItemEditDto> UpSingleMenuItems { get; set; }
        public Collection<MenuItemScheduleEditDto> MenuItemSchedules { get; set; }
        public List<ApiMenuItemDescription> MenuItemDescriptions { get; set; }

        public int OrganizationId { get; set; }
        public string Uom { get; set; }
        public int RowIndex { get; set; }
        public int? FoodType { get; set; }

        public string Exception { get; set; }

        public void AddPortion(string itemName, decimal price)
        {
            Portions.Add(new MenuItemPortionEditDto
            {
                Name = itemName,
                Price = price,
                MultiPlier = 1
            });
        }

        public string AddOns { get; set; }

        // Taxes

        
        public string NutritionInfo { get; set; }

        public CategoryOutputEditDto Category { get; set; }

        public string AllergicInfo { get; set; }
    }

    public class MenuItemScheduleEditDto
    {
        private string _Days;

        public MenuItemScheduleEditDto()
        {
            _Days = null;
        }

        public int? Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public List<ComboboxItemDto> AllDays { get; set; }
    }
    public class CategoryOutputEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? ProductGroupId { get; set; }
        public ProductGroupOutputEditDto ProductGroup { get; set; }
    }

    public class ProductGroupOutputEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public virtual ProductGroupOutputEditDto Parent { get; set; }
        public virtual int? ParentId { get; set; }
    }

}
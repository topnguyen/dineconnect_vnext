﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
	public class MenuItemPortionEditDto
	{
		public int? Id { get; set; }
		public string Name { get; set; }
		public int MultiPlier { get; set; }
		public decimal Price { get; set; }
		public int MenuItemId { get; set; }

		public string Barcode { get; set; }
		public bool ChangePrice { get; set; }

		public int PreparationTime { get; set; }
		public string AliasName { get; set; }

		public int NumberOfPax { get; set; }
	}
}
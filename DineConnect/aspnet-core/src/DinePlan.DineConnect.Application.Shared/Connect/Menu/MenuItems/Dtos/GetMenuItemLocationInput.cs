﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemLocationInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public int PortionId { get; set; }

        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }

        public int LocationId { get; set; }

        [Range(0, int.MaxValue)]
        public int MaxResultCount { get; set; } //0: Unlimited.

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public string Sorting { get; set; }
    }
}

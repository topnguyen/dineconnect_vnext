﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
   public class GetMenuItemForProductSetInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        
        public int? ProductSetId { get; set; }
        
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ProductName";
            }
        }
    }
}
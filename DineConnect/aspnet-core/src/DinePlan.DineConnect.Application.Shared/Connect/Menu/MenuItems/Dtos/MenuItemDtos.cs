﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetLocationMenuItemsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LocationId { get; set; }

        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class LocationMenuItemInput : EntityDto<int>
    {
        public int LocationId { get; set; }

        public int MenuItemId { get; set; }

        public List<int> MenuItems { get; set; }

        public bool DeleteMenuItems { get; set; }
    }

    public class LocationMenuItemDataReaderDto : EntityDto<int>
    {
        public int LocationId { get; set; }

        public int MenuItemId { get; set; }

        public string MenuItemName { get; set; }
    }
}
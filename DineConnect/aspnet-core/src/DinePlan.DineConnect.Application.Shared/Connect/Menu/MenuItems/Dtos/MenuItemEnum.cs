﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public enum ProductFoodType
    {
        Veg = 1,
        NonVeg = 2,
        Egg = 3,
        NotSpecified = 4
    }

    public enum ProductType
    {
        MenuItem,
        Combo
    }
}
﻿using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class ProductComboGroupEditDto
    {
        public ProductComboGroupEditDto()
        {
            ComboItems = new Collection<ProductComboItemEditDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public Collection<ProductComboItemEditDto> ComboItems { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class LocationPriceInput
    {
        [Required]
        public int LocationId { get; set; }

        public decimal Price { get; set; }

        [Required]
        public int PortionId { get; set; }
    }
}
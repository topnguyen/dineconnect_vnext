﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;
using DinePlan.DineConnect.Connect.Menu.Categories;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class MenuItemListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int OrganizationId { get; set; }
        public int TenantId { get; set; }
        public virtual CategoryDto Category { get; set; }
        public List<LocationDto> LocationList { get; set; }
        public string BarCode { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }
        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public string CategoryName { get; set; }
        public string ProductGroupName { get; set; }
        public string CategoryCode { get; set; }
        public string ProductGroupCode { get; set; }
        public int ProductType { get; set; }
        public Collection<MenuItemPortionEditDto> Portions { get; set; }
        public string Locations { get; set; }
        public bool RestrictPromotion { get; set; }
        public string Tag { get; set; }
        public bool NoTax { get; set; }
    }
}
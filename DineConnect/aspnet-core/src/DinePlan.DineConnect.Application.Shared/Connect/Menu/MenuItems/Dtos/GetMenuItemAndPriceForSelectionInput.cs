﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class GetMenuItemAndPriceForSelectionInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
    
    public class GetScreenMenuItemAndPriceForSelectionInput : PagedAndSortedInputDto
    {
        public int MenuId { get; set; }
        public string Filter { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class ApiMenuOutput
    {
        public ApiMenuOutput()
        {
            Categories = new List<ApiCategoryOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
        }

        public IList<ApiCategoryOutput> Categories { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
    }
    public class ApiCategoryOutput
    {
        public ApiCategoryOutput()
        {
            MenuItems = new List<ApiMenuItemOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
            ProductGroup = new ApiProductGroup();
        }
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
        public IList<ApiMenuItemOutput> MenuItems { get; set; }
        public int SortOrder { get; set; }
        public string ImagePath { get; set; }

        public ApiProductGroup ProductGroup { get; set; }

        public int? ProductGroupId { get; set; }
    }
    public class ApiOrderTagGroupOutput
    {
        public ApiOrderTagGroupOutput()
        {
            OrderTags = new List<ApiOrderTagOutput>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public int MinSelectItems { get; set; }
        public int MaxSelectItems { get; set; }
        public IList<ApiOrderTagOutput> OrderTags { get; set; }
        public bool AddPriceToOrder { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool TaxFree { get; set; }

        public int SortOrder { get; set; }
    }
    public class ApiProductGroup
    {
        public ApiProductGroup()
        {
        }
        public virtual int Id { get; set; }

        public virtual string Code { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }

        public virtual ApiProductGroup Parent { get; set; }
        public virtual int? ParentId { get; set; }
    }
    public class ApiOrderTagOutput
    {
        public int SortOrder { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int MenuItemId { get; set; }
    }
    public class ApiMenuItemOutput
    {
        public ApiMenuItemOutput()
        {
            MenuPortions = new List<ApiMenuItemPortionOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
            Barcodes = new List<ApiMenuBarcodeOutput>();
            UpMenuItems = new List<ApiUpMenuItemOutput>();
            MenuItemSchedules = new List<ApiMenuItemSchedule>();
            MenuItemDescriptions = new List<ApiMenuItemDescription>();
        }

        public int Id { get; set; }
        public int? ProductType { get; set; }

        public string Name { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public bool IsFavorite { get; set; }
        public IList<ApiMenuItemPortionOutput> MenuPortions { get; set; }
        public IList<ApiMenuBarcodeOutput> Barcodes { get; set; }
        public IList<ApiUpMenuItemOutput> UpMenuItems { get; set; }
        public IList<ApiMenuItemSchedule> MenuItemSchedules { get; set; }
        public IList<ApiMenuItemDescription> MenuItemDescriptions { get; set; }

        public int SortOrder { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
        public string BarCode { get; set; }
        public string Files { get; set; }

        public bool ForceQuantity { get; set; }
        public bool RestrictPromotion { get; set; }
        public bool NoTax { get; set; }

        public bool ForceChangePrice { get; set; }
        public string Tag { get; set; }
        public int? TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public ApiProductComboOutput Combo { get; set; }
        public Guid DownloadImage { get; set; }
        public string Uom { get; set; }
        public int LocationId { get; set; }
    }
    public class ApiMenuItemPortionOutput
    {
        public int Id { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public int Multiplier { get; set; }
        public string Barcode { get; set; }
        public bool ChangePrice { get; set; }
    }

    public class ApiMenuBarcodeOutput
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
    }

    public class ApiUpMenuItemOutput
    {
        public int Id { get; set; }
        public int MenuItemId { get; set; }
        public bool AddBaseProductPrice { get; set; }
        public int RefMenuItemId { get; set; }
        public decimal Price { get; set; }
        public int MinimumQuantity { get; set; }
        public int MaxQty { get; set; }
        public bool AddAuto { get; set; }
        public int AddQuantity { get; set; }
        public int ProductType { get; set; }
    }

    public class ApiMenuItemSchedule
    {
        public int Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public string Days { get; set; }
        public int MenuItemId { get; set; }
    }
    public class ApiProductComboOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }

        public ApiProductComboOutput()
        {
            ComboGroups = new Collection<ApiProductComboGroupOutput>();
        }

        public Collection<ApiProductComboGroupOutput> ComboGroups { get; set; }
        public string Sorting { get; set; }
    }

    public class ApiMenuItemDescription
    {
        public string LanguageCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int MenuItemId { get; set; }
    }
    public class ApiProductComboGroupOutput
    {
        public ApiProductComboGroupOutput()
        {
            ComboItems = new Collection<ApiProductComboItemOutput>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public Collection<ApiProductComboItemOutput> ComboItems { get; set; }
    }
    public class ApiProductComboItemOutput
    {
        public ApiProductComboItemOutput()
        {
            Count = 1;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }

        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }
        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }


}

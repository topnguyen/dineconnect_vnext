﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
    public class TiffinProductLookupTableDto : NameValueDto
    {
        public int Id { get; set; }
    }
}

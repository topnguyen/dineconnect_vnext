﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
	public class UpMenuItemEditDto
	{
		public int? Id { get; set; }
		public int MenuItemId { get; set; }
		public string Name { get; set; }
		public int RefMenuItemId { get; set; }
		public bool AddBaseProductPrice { get; set; }
		public decimal Price { get; set; }
		public int ProductType { get; set; }
		public int MinimumQuantity { get; set; }
		public int MaxQty { get; set; }

		public bool AddAuto { get; set; }
		public int AddQuantity { get; set; }
	}
}
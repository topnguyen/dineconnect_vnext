﻿namespace DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos
{
	public class MenuItemScheduleDto
	{
		public int? Id { get; set; }
		public int? Mnt { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public string AllergicInfo { get; set; }
		public decimal Price { get; set; }
		public double FeedbackPoint { get; set; }
		public int FeedBackCount { get; set; }
		public bool IsFavorite { get; set; }
		public bool IsAddToCart { get; set; }
	}
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups
{
	public interface IProductGroupsAppService : IApplicationService
	{
		Task<ListResultDto<ProductGroupDto>> GetAll();

		Task<GetProductGroupForViewDto> GetProductGroupForView(int id);

		Task<GetProductGroupForEditOutput> GetProductGroupForEdit(EntityDto input);

		Task<int> CreateOrEdit(CreateOrEditProductGroupDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetProductGroupsToExcel(GetAllProductGroupsInput input);

		Task<CreateOrEditProductGroupDto> GetOrCreateByName(string groupname, string code);
	}
}
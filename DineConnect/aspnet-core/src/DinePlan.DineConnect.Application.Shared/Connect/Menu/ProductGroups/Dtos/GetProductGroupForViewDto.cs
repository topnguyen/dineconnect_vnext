﻿namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
    public class GetProductGroupForViewDto
    {
        public ProductGroupDto ProductGroup { get; set; }
    }
}
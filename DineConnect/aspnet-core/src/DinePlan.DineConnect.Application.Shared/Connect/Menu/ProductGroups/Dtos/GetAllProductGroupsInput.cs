﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
	public class GetAllProductGroupsInput : PagedAndSortedResultRequestDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Name";
			}
		}
	}
}
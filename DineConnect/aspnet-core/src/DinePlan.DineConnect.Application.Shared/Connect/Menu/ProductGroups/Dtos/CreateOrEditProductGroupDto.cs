﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
	public class CreateOrEditProductGroupDto : EntityDto<int?>
	{
		[StringLength(ProductGroupConsts.MaxCodeLength, MinimumLength = ProductGroupConsts.MinCodeLength)]
		public string Code { get; set; }

		[StringLength(ProductGroupConsts.MaxNameLength, MinimumLength = ProductGroupConsts.MinNameLength)]
		public string Name { get; set; }

		public int TenantId { get; set; }

		public int OrganizationId { get; set; }
		public int? ParentId { get; set; }
	}
}
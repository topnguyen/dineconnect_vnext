﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
	public class GetProductGroupForEditOutput :IOutputDto 
	{
		public CreateOrEditProductGroupDto ProductGroup { get; set; }


	}
}
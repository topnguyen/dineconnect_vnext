﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
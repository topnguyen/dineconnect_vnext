﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.Categories;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups.Dtos
{
	public class ProductGroupDto : FullAuditedEntityDto
	{
		public ProductGroupDto()
		{
			Categories = new HashSet<CategoryDto>();
		}

		public virtual string Code { get; set; }
		public virtual string Name { get; set; }
		public int TenantId { get; set; }
		public int? ParentId { get; set; }
		public int MemberCount { get; set; }
		public ICollection<CategoryDto> Categories { get; set; }
		public int OrganizationId { get; set; }
	}
}
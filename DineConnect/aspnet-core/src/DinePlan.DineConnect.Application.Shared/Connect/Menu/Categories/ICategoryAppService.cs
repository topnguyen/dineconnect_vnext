﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
    public interface ICategoryAppService : IApplicationService
    {
        Task<PagedResultDto<CategoryDto>> GetAll(GetCategoryInput input);

        Task<CategoryDto> GetCategoryForEdit(NullableIdDto input);

        Task<int> CreateOrUpdateAsync(CategoryDto input);

        Task DeleteAsync(int input);

        Task<ListResultDto<ComboboxItemDto>> GetCategoriesForCombobox();
        Task<FileDto> GetCategoryToExcel(GetCategoryInput input);
        Task<CategoryDto> GetOrCreateCategory(CategoryDto input);
		Task<PagedResultDto<CategoryDto>> GetCategoryByPgroductGroupId(GetCategoryByProductGroupIdInput input);
		Task UpdateCategoryProductgroupId(UpdateProductgroupCategoryInput input);
        Task ActivateItem(EntityDto input);
    }
}
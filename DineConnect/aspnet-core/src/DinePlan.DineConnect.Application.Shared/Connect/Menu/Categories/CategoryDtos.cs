﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
	public class CategoryDto : FullAuditedEntityDto<int?>
	{
		[Required]
		public string Name { get; set; }

		public string Code { get; set; }
		public int OrganizationId { get; set; }
		public int TenantId { get; set; }
		public int? ProductGroupId { get; set; }

		public string GroupCode { get; set; }
		public string GroupName { get; set; }
	}

	public class GetCategoryInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }
		public bool IsDeleted { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Name";
			}
		}
	}

	public class GetCategoryByProductGroupIdInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }
		public int ProductGroupId { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Name";
			}
		}
	}

	public class UpdateProductgroupCategoryInput
	{
		public int ProductGroupId { get; set; }
		public List<int> CategoryIds { get; set; }
	}
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus
{
	public interface IScreenMenuAppService : IApplicationService
	{
		Task<PagedResultDto<GetAllScreenMenuDto>> GetAll(GetAllScreenMenuInput input);
		Task<CreateOrUpdateScreenMenuInput> GetForEdit(int id);
		Task CreateOrUpdateScreenMenu(CreateOrUpdateScreenMenuInput input);
		Task Clone(int id);

		Task Delete(int id);

		FileDto GetImportTemplate();

		Task Generate(int? id, bool locationInput);

		Task<PagedResultDto<ScreenMenuCategoryListDto>> GetCategories(GetScreenMenuCategoriesInput input);

		Task CreateCategory(CreateCategoryInput input);

		Task<ScreenMenuCategoryEditInput> GetCategoryForEdit(int id);

		Task UpdateCategory(ScreenMenuCategoryEditInput input);

		Task DeleteCategory(int id);

		Task SaveSortCategories(int[] categories);


		Task SaveSortSortItems(int[] menuItems);


		Task<PagedResultDto<ScreenMenuItemListDto>> GetMenuItems(GetAllScreenMenuInput input);

		Task<GetScreenMenuItemForEditOutput> GetMenuItemForEdit(EntityDto input);

		Task CreateMenuItem(CreateScreenMenuItemInput input);

		Task CreateOrUpdateMenuItem(CreateOrUpdateScreenMenuItem input);

		Task DeleteScreenMenuItem(EntityDto input);

		Task ClearScreenMenu(EntityDto input);

		Task<GetScreenItemsOutput> GetScreenItemsForLocation(GetScreenMenuInput input);

		Task ActivateItem(EntityDto input);

		Task<GetScreenMenuItemBySubMenuOuput> GetMenuItemForVisualScreen(GetScreenMenuInput input);

		ListResultDto<ComboboxItemDto> GetConfirmationTypes();

		Task<ApiScreenMenuOutput> ApiScreenMenu(ApiLocationInput locationInput);

		Task<PagedResultDto<GetAllScreenMenuDto>> GetAllScreenMenuCategory(GetScreenMenuCategoryInput input);
	}
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public enum ScreenMenuNumerator
    {
        None = 0, Small = 1, Large = 2
    }

    public enum ScreenMenuItemConfirmationTypes
    {
        None = 0,  Confirmation = 1, AdminPin = 2
    }

}

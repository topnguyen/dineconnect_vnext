﻿using System;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DinePlan.DineConnect.Dto;
using System.Collections.ObjectModel;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public class GetAllScreenMenuInput : CommonLocationGroupFilterInputDto
    {
        public string Filter { get; set; }

        public bool IsDeleted { get; set; }

        public int MenuId { get; set; }

        public int CategoryId { get; set; }
    }

    public class GetScreenMenuInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LocationId { get; set; }
        public int MenuId { get; set; }
        public int CategoryId { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public string CurrentTag { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder,Id";
            }
        }
    }

    public class GetAllScreenMenuDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public int CategoryCount { get; set; }
        public int RefLocationId { get; set; }
        public bool Group { get; set; }
    }

    // --

    public class ScreenMenuEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Departments { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public int Type { get; set; }
    }

    public class MenuItemImportDto
    {
        public MenuItemImportDto()
        {
            Portions = new Collection<MenuItemPortionEditDto>();
        }

        public int? Id { get; set; }
        public string BarCode { get; set; }

        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }
        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public bool RestrictPromotion { get; set; }
        public bool NoTax { get; set; }

        public string CategoryName { get; set; }
        public string GroupName { get; set; }

        public string TPName { get; set; }

        public decimal TPPrice { get; set; }

        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public int ProductType { get; set; }
        public string Location { get; set; }
        public string Tag { get; set; }

        public int RowIndex { get; set; }

        public Collection<MenuItemPortionEditDto> Portions { get; set; }

        public void AddPortion(string itemName, decimal price, int preparation, int numberOfPax)
        {
            Portions.Add(new MenuItemPortionEditDto
            {
                Name = itemName,
                Price = price,
                MultiPlier = 1,
                Barcode = "",
                PreparationTime = preparation,
                NumberOfPax = numberOfPax
            });
        }
    }

    public class CreateOrUpdateScreenMenuInput : IInputDto
    {
        [Required] public ScreenMenuEditDto ScreenMenu { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public CreateOrUpdateScreenMenuInput()
        {
            LocationGroup = new CommonLocationGroupDto();
        }
    }

    // --

    public class GetScreenMenuCategoriesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int ScreenMenuId { get; set; }

        public bool DateFilterApplied { get; set; }

        public DateTime? StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
    }

    public class GetScreenMenuForEditOutput : IOutputDto
    {
        public GetScreenMenuForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public ScreenMenuEditDto ScreenMenu { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
    }

    public class ScreenMenuCategoryScheduleEditDto
    {
        private string _Days;
        private string _MonthDays;

        public ScreenMenuCategoryScheduleEditDto()
        {
            _Days = null;
            _MonthDays = null;
        }

        public int? Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<ComboboxItemDto> AllDays { get; set; }
        public List<ComboboxItemDto> AllMonthDays { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public string MonthDays
        {
            get
            {
                if (AllMonthDays == null || !AllMonthDays.Any())
                {
                    return _MonthDays;
                }
                return string.Join(",", AllMonthDays.Select(a => a.Value).ToArray());
            }
            set { _MonthDays = value; }
        }
    }

    public class GetScreenMenuCategoryInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
}
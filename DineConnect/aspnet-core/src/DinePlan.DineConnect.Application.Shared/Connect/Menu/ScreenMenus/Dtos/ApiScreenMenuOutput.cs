﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public class ApiScreenMenuOutput
    {
        public ApiScreenMenuOutput()
        {
            Menu = new List<ApiScreenMenu>();
        }

        public List<ApiScreenMenu> Menu { get; set; }
    }
    public class ApiScreenMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Departments { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public virtual ICollection<ApiScreenCategory> Categories { get; set; }

        public ApiScreenMenu()
        {
            Categories = new List<ApiScreenCategory>();
        }
        public string Dynamic { get; set; }

    }
    public class ApiScreenCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string ImagePath { get; set; }
        public int MainButtonHeight { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public int MaxItems { get; set; }
        public int PageCount { get; set; }
        public bool WrapText { get; set; }
        public int SortOrder { get; set; }
        public int NumeratorType { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }

        public string ScreenMenuCategorySchedule { get; set; }

        public string SubButtonColorDef { get; set; }
        public virtual ICollection<ApiScreenMenuItem> MenuItems { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
        public string Dynamic { get; set; }

    }
    public class ApiScreenMenuItem
    {
        public int Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool ShowAliasName { get; set; }
        public bool AutoSelect { get; set; }
        public string ButtonColor { get; set; }
        public double FontSize { get; set; }
        public string SubMenuTag { get; set; }
        public string OrderTags { get; set; }
        public string ItemPortion { get; set; }
        public int SortOrder { get; set; }
        public int ConfirmationType { get; set; }
        public bool WeightedItem { get; set; }
        public string Files { get; set; }
        public bool VoucherItem { get; set; }
        public int? ConnectCardTypeId { get; set; }
        public virtual Guid DownloadImage { get; set; }
        public string Dynamic { get; set; }

    }
}

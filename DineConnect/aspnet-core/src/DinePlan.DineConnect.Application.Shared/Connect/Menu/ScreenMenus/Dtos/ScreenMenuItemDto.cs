﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public class ScreenMenuItemListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ButtonColor { get; set; }
        public bool AutoSelect { get; set; }
        public string CategoryName { get; set; }
        public string SubMenuTag { get; set; }
        public double FontSize { get; set; }
        public bool ShowAliasName { get; set; }
        public string MenuItemAliasName { get; set; }
        public int ConfirmationType { get; set; }
        public string ConfirmationTypeName => ((ScreenMenuItemConfirmationTypes) ConfirmationType).ToString();
        public bool WeightedItem { get; set; }

        public int SortOrder { get; set; }
    }

    public class ScreenMenuItemEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool ShowAliasName { get; set; }
        public bool AutoSelect { get; set; }
        public string ButtonColor { get; set; }
        public double FontSize { get; set; }
        public string SubMenuTag { get; set; }
        public string OrderTags { get; set; }
        public string ItemPortion { get; set; }
        public Guid DownloadImage { get; set; }
        public bool RefreshImage { get; set; }
        public bool VoucherItem { get; set; }
        public string Files { get; set; }
        public string ConfirmationType { get; set; }
        public bool WeightedItem { get; set; }
    }

    public class CreateScreenMenuItemInput : IInputDto
    {
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public int CategoryId { get; set; }
        public List<IdNameDto> Items { get; set; }

        public CreateScreenMenuItemInput()
        {
            Items = new List<IdNameDto>();
        }

        public ScreenMenuItemEditDto MenuItem { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }

    public class CreateOrUpdateScreenMenuItem : IInputDto
    {
        [Required] public ScreenMenuItemEditDto MenuItem { get; set; }

        public bool CopyToAll { get; set; }
        public int MenuItemId { get; set; }
        public int CategoryId { get; set; }
    }
    public class GetScreenMenuItemForEditOutput : IOutputDto
    {
        public ScreenMenuItemEditDto MenuItem { get; set; }
        public int MenuItemId { get; set; }
    }

    public class GetScreenItemsOutput
    {
        public GetScreenItemsOutput()
        {
            Items = new List<IdNameDto>();
            SelectedItems = new List<IdNameDto>();
        }

        public List<IdNameDto> Items { get; set; }
        public List<IdNameDto> SelectedItems { get; set; }
    }

    public class GetScreenMenuItemBySubMenuOuput
    {
        public GetScreenMenuItemBySubMenuOuput()
        {
            MenuItems = new List<ScreenMenuItemListDto>();
            CategoryTags = new List<string>();
        }

        public List<ScreenMenuItemListDto> MenuItems { get; set; }

        public IEnumerable<string> CategoryTags { get; set; }
    }
}

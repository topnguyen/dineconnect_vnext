﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public class ScreenMenuCategoryListDto : EntityDto<int>
    {
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string MenuItemButtonColor { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MainButtonHeight { get; set; }
        public bool WrapText { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }
        public string SubButtonColorDef { get; set; }
        
        public int SortOrder { get; set; }

        public string ScreenMenuCategorySchedule { get; set; }

        public List<ScreenMenuCategoryScheduleEditDto> ScreenMenuCategorySchedules { get; set; }
    }

    public class ScreenMenuCategoryEditInput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string ImagePath { get; set; }
        public int MainButtonHeight { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MaxItems { get; set; }
        public int PageCount { get; set; }
        public bool WrapText { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }
        public string SubButtonColorDef { get; set; }
        public int NumeratorType { get; set; }
        public string Files { get; set; }
        public virtual Guid? DownloadImage { get; set; }
        public bool RefreshImage { get; set; }

        public int FromHour { get; set; }
        public int FromMinute { get; set; }

        public int ToHour { get; set; }
        public int ToMinute { get; set; }

        public bool CopyToAll { get; set; }
        public string ScreenMenuCategorySchedule { get; set; }
        public int ScreenMenuId { get; set; }
    }

    public class CreateCategoryInput : IInputDto
    {
        public string Name { get; set; }

        public int ScreenMenuId { get; set; }
    }

    public class GetScreenCategoryForEditOutput : IOutputDto
    {
        public ScreenMenuCategoryEditInput Category { get; set; }
    }

    public class CreateOrUpdateCategory : IInputDto
    {
        [Required] public ScreenMenuCategoryEditInput Category { get; set; }

        public bool CopyToAll { get; set; }
        public int MenuId { get; set; }
    }
}
﻿using DinePlan.DineConnect.Connect.Master.LocationGroup;

namespace DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos
{
    public class FilterInputDto
    {
        public int? Id { get; set; }
        public int? FutureDataId { get; set; }
        public LocationGroupDto LocationGroupToBeFiltered { get; set; }
    }
}

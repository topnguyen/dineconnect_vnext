﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TimeSettings
{
    public interface ITimeSettingsAppService : IApplicationService
    {
        Task<TimeSettingDto> GetTimeSettings();
        Task SetTimeSettings(TimeSettingDto timeSetting);
    }
}
 
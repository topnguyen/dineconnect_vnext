﻿using System;

namespace DinePlan.DineConnect.Connect.TimeSettings
{
    public  class TimeSettingDto
    {
        public DateTime CurrentDateTime { get; set; }
        public string TimeZone { get; set; }
        public string TimeFormat { get; set; }
    }
} 

﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Card.Card.Dtos
{
	public class ConnectCardListDto : CreationAuditedEntity<int?>
	{
		public virtual int ConnectCardTypeId { get; set; }

		public string ConnectCardTypeName { get; set; }

		public string CardNo { get; set; }

		public int TenantId { get; set; }

		public bool Redeemed { get; set; }

		public bool Active { get; set; }
	}

	public class ConnectCardEditDto : CreationAuditedEntity<int?>
	{
		public int ConnectCardTypeId { get; set; }
		public string ConnectCardTypeName { get; set; }

		public string CardNo { get; set; }

		public int? TenantId { get; set; }

		public bool Redeemed { get; set; }

		public bool Active { get; set; }
		public string Remarks { get; set; }
	}

	public class ValidateCardInput : IInputDto
	{
		public string CardNumber { get; set; }
		public int LocationId { get; set; }
		public int TenantId { get; set; }
	}

	public class ValidateCardOutput : IOutputDto
	{
		public int ConnectCardTypeId { get; set; }
		public string ErrorMessage { get; set; }
	}

	public class ApplyCardInput : IInputDto
	{
		public string CardNumber { get; set; }
		public int LocationId { get; set; }
		public int TenantId { get; set; }
		public string TicketNumber { get; set; }
		public int PromotionId { get; set; }
	}

	public class ApplyCardOutput : IOutputDto
	{
		public string ErrorMessage { get; set; }
		public int ReferenceId { get; set; }
	}

	public class GenerateCardsInputDto : IInputDto
	{
		public virtual int ConnectCardTypeId { get; set; }
		public virtual string Prefix { get; set; }
		public virtual string Suffix { get; set; }
		public virtual int CardNumberOfDigits { get; set; }
		public virtual int CardStartingNumber { get; set; }
		public virtual int CardCount { get; set; }
		public virtual int? TenantId { get; set; }
		public virtual long? UserId { get; set; }
	}

	public class GetConnectCardsInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public int? ConnectCardTypeId { get; set; }

		public bool? Redeemed { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "CreationTime DESC";
			}
		}
	}

	public class ConnectCardStatusDto
	{
		public PagedResultDto<ConnectCardEditDto> ConnectCards { get; set; }
	}

	public class ImportConnectCardsInBackgroundInputDto
	{
		public ImportConnectCardsInBackgroundInputDto()
		{
			ImportCardDataDetail = new ImportCardDataDetailEditDto();
		}

		public List<ConnectCardEditDto> ConnectCard { get; set; }
		public string FileName { get; set; }
		public bool Active { get; set; }
		public int TenantId { get; set; }
		public string TenantName { get; set; }
		public string JobName { get; set; }
		public ImportCardDataDetailEditDto ImportCardDataDetail { get; set; }
	}

	public class ImportCardDataDetailListDto
	{
		public int? Id { get; set; }
		public string JobName { get; set; }
		public string FileName { get; set; }
		public string FileLocation { get; set; }
		public DateTime? StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		public string OutputJsonDto { get; set; }
		public int TenantId { get; set; }
		public string Remarks { get; set; }
		public string TenantName { get; set; }
	}

	public class ImportCardDataDetailEditDto
	{
		public int? Id { get; set; }
		public string JobName { get; set; }
		public string FileName { get; set; }
		public string FileLocation { get; set; }
		public DateTime? StartTime { get; set; }
		public DateTime? EndTime { get; set; }
		public string OutputJsonDto { get; set; }
		public int TenantId { get; set; }
	}

	public class GetImportCardDataDetailInputDto
	{
		public ImportCardDataDetailListDto ImportCardDataDetail { get; set; }
	}

	public class GetConnectCardDataFromFileOutputDto
	{
		public List<ConnectCardEditDto> ConnectCards { get; set; }
		public MessageOutputDto MessageOutput { get; set; }
	}

	public class ExportConnectCardImportStatusDto
	{
		public List<ConnectCardEditDto> DuplicateExistInExcel { get; set; }
		public List<ConnectCardEditDto> DuplicateExistInDB { get; set; }
		public List<ConnectCardEditDto> ImportedCardsList { get; set; }
		public List<string> ErrorList { get; set; }
	}
}
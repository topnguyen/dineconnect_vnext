﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Card.Card
{
    public interface IConnectCardAppService : IApplicationService
    {
        Task GenerateConnectCardsInBackground(GenerateCardsInputDto input);

        Task GenerateConnectCards(GenerateCardsInputDto input);

        Task<PagedResultDto<ConnectCardListDto>> GetAllConnectCard(GetConnectCardsInput input);

        FileDto GetImportTemplateToExcel();

        Task ImportFromExcel(string fileToken);

        Task<FileDto> GetAllToExcel();

        Task<List<ConnectCardTypeListDto>> GetConnectCardTypes();

        Task<ConnectCardEditDto> GetConnectCardForEdit(NullableIdDto input);

        Task<ConnectCardEditDto> CreateOrUpdateConnectCard(ConnectCardEditDto input);

        Task DeleteConnectCard(NullableIdDto input);

        Task<ValidateCardOutput> ApiValidateCard(ValidateCardInput input);

        Task<ApplyCardOutput> ApiApplyCard(ApplyCardInput input);

        Task DeleteMultiConnectCard(List<int> input);

        Task ImportConnectCards(ImportConnectCardsInBackgroundInputDto input);

        Task<GetConnectCardDataFromFileOutputDto> ImportConnectCardInBackGround(ImportConnectCardsInBackgroundInputDto input);

        Task<PagedResultDto<ImportCardDataDetailListDto>> GetImportCardDataDetails(GetConnectCardsInput input);

        Task<FileDto> GetImportCardDataDetailToExport();
        Task ActiveOrInactiveConnectCard(EntityDto input);
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;

namespace DinePlan.DineConnect.Connect.Card.CardType.Dtos
{
    public class ConnectCardTypeListDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool UseOneTime { get; set; }

        public int Oid { get; set; }

        public int Length { get; set; }

        public int CardCount { get; set; }
    }

    public class GetConnectCardTypesInput : CommonLocationGroupFilterInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetConnectCardTypeForEditOutput
    {
        public GetConnectCardTypeForEditOutput()
        {
            CardList = new List<ConnectCardListDto>();
            LocationGroup = new CommonLocationGroupDto();
        }

        public ConnectCardTypeEditDto ConnectCardType { get; set; }

        public List<ConnectCardListDto> CardList { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
    }

    public class ConnectCardTypeEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public bool UseOneTime { get; set; }

        public int Length { get; set; }
        public int? ConnectCardTypeCategoryId { get; set; }
        public int? TenantId { get; set; }
        public string Tag { get; set; }

        public DateTime ValidTill { get; set; }
    }

    public class CreateOrUpdateConnectCardTypeInput : IInputDto
    {
        [Required]
        public ConnectCardTypeEditDto ConnectCardType { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Card.CardType.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Card.CardType
{
    public interface IConnectCardTypeAppService : IApplicationService
    {
        Task<PagedResultDto<ConnectCardTypeListDto>> GetCardTypes(GetConnectCardTypesInput input);

        Task<FileDto> GetAllToExcel();
        
        FileDto GetImportCardsTemplateToExcel();

        Task ImportCardsFromExcel(int cardTypeId, string fileToken);

        Task<GetConnectCardTypeForEditOutput> GetConnectCardTypeForEdit(int? id);

        Task<GetConnectCardTypeForEditOutput> CreateOrUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input);

        Task DeleteConnectCardType(NullableIdDto input);

        Task ActivateItem(EntityDto input);

        Task<List<ConnectCardTypeListDto>> GetAllCardTypes();
    }
}

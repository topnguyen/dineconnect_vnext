﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos
{
    public class ConnectCardTypeCategoryListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
    public class ConnectCardTypeCategoryEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class GetConnectCardTypeCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetConnectCardTypeCategoryForEditOutput : IOutputDto
    {
        public ConnectCardTypeCategoryEditDto ConnectCardTypeCategory { get; set; }
    }
    public class CreateOrUpdateConnectCardTypeCategoryInput : IInputDto
    {
        [Required]
        public ConnectCardTypeCategoryEditDto ConnectCardTypeCategory { get; set; }
    }
}

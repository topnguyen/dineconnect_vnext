﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Card.CardTypeCategory.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Card.CardTypeCategory
{
    public interface IConnectCardTypeCategoryAppService : IApplicationService
    {
        Task<PagedResultDto<ConnectCardTypeCategoryListDto>> GetAll(GetConnectCardTypeCategoryInput inputDto);
        
        Task<FileDto> GetAllToExcel(GetConnectCardTypeCategoryInput input);
        
        Task<GetConnectCardTypeCategoryForEditOutput> GetConnectCardTypeCategoryForEdit(NullableIdDto EntityDto);
        
        Task CreateOrUpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input);
        
        Task DeleteConnectCardTypeCategory(EntityDto input);
        
        Task<ListResultDto<ConnectCardTypeCategoryListDto>> GetConnectCardTypeCategorys();
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.MutilLanguage.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.MutilLanguage
{
	public interface ILanguageDescriptionsAppService : IApplicationService
	{
		Task<PagedResultDto<LanguageDescriptionDto>> GetAll(GetAllLanguageDescriptionsInput input);

		Task<GetLanguageDescriptionForEditOutput> GetLanguageDescriptionForEdit(NullableIdDto input);

		Task CreateOrEdit(CreateOrEditLanguageDescriptionDto input);

		Task Delete(EntityDto input);
	}
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.MutilLanguage.Dtos
{
	public class CreateOrEditLanguageDescriptionDto : EntityDto<int?>
	{
		public string LanguageCode { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public string Type { get; set; }

		public int LanguageDescriptionType { get; set; }

		public int ReferenceId { get; set; }
	}
}
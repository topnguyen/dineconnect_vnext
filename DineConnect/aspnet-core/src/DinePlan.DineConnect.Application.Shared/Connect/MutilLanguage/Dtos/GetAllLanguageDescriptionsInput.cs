﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.MutilLanguage.Dtos
{
	public class GetAllLanguageDescriptionsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }
		public int ReferenceId { get; set; }
		public string Operation { get; set; }
		public string LanguageDescriptionType { get; set; }
	}
}
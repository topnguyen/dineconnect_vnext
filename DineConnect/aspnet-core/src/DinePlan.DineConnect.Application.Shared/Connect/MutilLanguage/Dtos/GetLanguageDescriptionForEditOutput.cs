﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.MutilLanguage.Dtos
{
    public class GetLanguageDescriptionForEditOutput
    {
        public CreateOrEditLanguageDescriptionDto LanguageDescription { get; set; }

    }
}
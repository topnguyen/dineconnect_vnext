﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUserRoles
{
    public interface IDinePlanUserRoleAppService : IApplicationService
    {
        Task<PagedResultDto<DinePlanUserRoleListDto>> GetAll(GetDinePlanUserRoleInput input);

        Task<FileDto> GetAllToExcel();

        Task<GetDinePlanUserRoleForEditOutput> GetDinePlanUserRoleForEdit(NullableIdDto input);

        Task CreateOrUpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input);

        Task DeleteDinePlanUserRole(EntityDto input);

        Task<ListResultDto<DinePlanUserRoleListDto>> GetIds();

        Task ActivateItem(EntityDto input);
        Task<GetDinePlanUserRoleOutputList> ApiGetUserRoles(GetUserRolesNameInput input);
    }
}
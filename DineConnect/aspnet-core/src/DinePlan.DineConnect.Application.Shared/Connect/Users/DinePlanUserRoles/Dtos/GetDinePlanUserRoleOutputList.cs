﻿using DinePlan.DineConnect.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos
{
    public class GetDinePlanUserRoleOutputList : IOutputDto
    {
        public List<GetDinePlanUserRoleForEditOutput> Items { get; set; }
    }
}

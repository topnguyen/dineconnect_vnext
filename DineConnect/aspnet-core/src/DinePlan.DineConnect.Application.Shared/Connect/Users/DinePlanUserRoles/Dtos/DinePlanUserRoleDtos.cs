﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Authorization.Permissions.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos
{
    public class DinePlanUserRoleListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }

        public virtual bool IsAdmin { get; set; }
        
        public virtual string Permission { get; set; }
    }

    public class GetDinePlanUserRoleInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        
        public string Location { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDinePlanUserRoleForEditOutput : IOutputDto
    {
        public DinePlanUserRoleEditDto DinePlanUserRole { get; set; }

        public List<FlatPermissionDto> Permissions { get; set; }
        
        public List<string> GrantedPermissionNames { get; set; }
    }

    public class DinePlanUserRoleEditDto
    {
        public int? Id { get; set; }
        
        public virtual string Name { get; set; }
        
        public virtual bool IsAdmin { get; set; }

        public int? DepartmentId { get; set; }
    }

    public class CreateOrUpdateDinePlanUserRoleInput : IInputDto
    {
        [Required]
        public DinePlanUserRoleEditDto DinePlanUserRole { get; set; }

        [Required]
        public List<string> GrantedPermissionNames { get; set; }
    }
}

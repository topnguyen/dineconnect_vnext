﻿using DinePlan.DineConnect.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUserRoles.Dtos
{
    public class GetUserRolesNameInput : IInputDto
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
    }
}

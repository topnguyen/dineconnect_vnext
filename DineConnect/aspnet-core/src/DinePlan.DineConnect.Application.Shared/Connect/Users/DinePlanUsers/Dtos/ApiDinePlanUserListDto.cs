﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos
{
    public class ApiDinePlanUserListDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string ConnectUser { get; set; }
        public virtual string ConnectPassword { get; set; }
        public virtual string LanguageCode { get; set; }
        public virtual string AlternateLanguageCode { get; set; }
        public virtual bool IsActive { get; set; }
        public int DinePlanUserRoleId { get; set; }
        public string DinePlanUserRoleName { get; set; }
        public DateTime CreationTime { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos
{
    public class DinePlanUserListDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string SecurityCode { get; set; }
        public int DinePlanUserRoleId { get; set; }
        public string DinePlanUserRoleName { get; set; }
        public DateTime CreationTime { get; set; }
    }

    public class GetDinePlanUserInput : CommonLocationGroupFilterInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDinePlanUserForEditOutput : IOutputDto
    {
        public DinePlanUserEditDto DinePlanUser { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public GetDinePlanUserForEditOutput()
        {
            LocationGroup = new CommonLocationGroupDto();
            DinePlanUser = new DinePlanUserEditDto();
        }
    }

    public class DinePlanUserEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }
        public int? DinePlanUserRoleId { get; set; }
        public string LanguageCode { get; set; }
        public long? UserId { get; set; }
        public string AlternateLanguageCode { get; set; }

        public DinePlanUserEditDto()
        {
            PinCode = new Random().Next().ToString();
            LanguageCode = "en";
        }
    }

    public class CreateOrUpdateDinePlanUserInput : IInputDto
    {
        [Required]
        public DinePlanUserEditDto DinePlanUser { get; set; }

        public CommonLocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateDinePlanUserInput()
        {
            LocationGroup = new CommonLocationGroupDto();
            DinePlanUser = new DinePlanUserEditDto();
        }
    }
}
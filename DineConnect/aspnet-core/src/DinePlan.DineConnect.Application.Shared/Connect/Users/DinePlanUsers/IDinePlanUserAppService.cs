﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Users.DinePlanUsers.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Locations.Dtos;

namespace DinePlan.DineConnect.Connect.Users.DinePlanUsers
{
	public interface IDinePlanUserAppService : IApplicationService
	{
		Task<PagedResultDto<DinePlanUserListDto>> GetAll(GetDinePlanUserInput input);

		Task<FileDto> GetAllToExcel();

		Task<GetDinePlanUserForEditOutput> GetDinePlanUserForEdit(EntityDto input);

		Task CreateOrUpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input);

		Task DeleteDinePlanUser(EntityDto input);

		Task<ListResultDto<DinePlanUserListDto>> GetIds();

		Task ActivateItem(EntityDto input);

		Task<PagedResultDto<ApiDinePlanUserListDto>> ApiUsersByLocation(ApiLocationInput input);
	}
}
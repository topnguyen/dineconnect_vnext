﻿namespace DinePlan.DineConnect.Configuration
{
    public interface IExternalLoginOptionsCacheManager
    {
        void ClearCache();
    }
}

﻿namespace DinePlan.DineConnect.Configuration.Dto
{
    public class ThemeContentSettingsDto
    {
        public string Color { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Dto
{
    public class TouchSettingDto
    {
        public string Currency { get; set; }
        public decimal Rounding { get; set; }
        public int TotalDecimal { get; set; }
    }
}

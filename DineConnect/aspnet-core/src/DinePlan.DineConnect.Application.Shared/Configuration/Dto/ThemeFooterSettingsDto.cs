namespace DinePlan.DineConnect.Configuration.Dto
{
    public class ThemeFooterSettingsDto
    {
        public bool FixedFooter { get; set; }

        public string Color { get; set; }
    }
}
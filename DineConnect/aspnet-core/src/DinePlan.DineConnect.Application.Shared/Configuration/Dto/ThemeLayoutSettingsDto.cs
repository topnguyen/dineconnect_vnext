namespace DinePlan.DineConnect.Configuration.Dto
{
    public class ThemeLayoutSettingsDto
    {
        public string LayoutType { get; set; }
    }
}
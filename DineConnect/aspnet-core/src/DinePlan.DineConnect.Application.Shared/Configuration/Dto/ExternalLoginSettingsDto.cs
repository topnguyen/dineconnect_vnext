﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Configuration.Dto
{
    public class ExternalLoginSettingsDto
    {
        public List<string> EnabledSocialLoginSettings { get; set; } = new List<string>();
    }
}
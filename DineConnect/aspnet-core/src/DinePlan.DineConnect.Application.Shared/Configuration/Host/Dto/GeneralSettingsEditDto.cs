﻿namespace DinePlan.DineConnect.Configuration.Host.Dto
{
    public class GeneralSettingsEditDto
    {
        public string Timezone { get; set; }

        /// <summary>
        /// This value is only used for comparing user's timezone to default timezone
        /// </summary>
        public string TimezoneForComparison { get; set; }

        public string AdminWhatappNumber { get; set; }
        
        public string WhatappAccountSid { get; set; }
        
        public string WhatappAuthToken { get; set; }

        public string WhatappSenderNumber { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Configuration.Host.Dto
{
    public class OtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}
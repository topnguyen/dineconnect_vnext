﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;

namespace DinePlan.DineConnect.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();

        Task<string> CreateCompanyLogo(CompanyLogoInfo input);

        Task<CompanySettingDto> GetCompanySettingAsync();

        Task<OrderSettingDto> GetOrderSetting();

        Task UpdateOrderSetting(OrderSettingDto input);

        Task<ShippingProviderDto> GetShippingProviderSetting();
        Task<TouchSettingDto> GetCompactSetting();

    }
}
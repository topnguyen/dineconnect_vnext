using Abp.Runtime.Validation;
using Abp.Timing;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.Configuration.Host.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantSettingsEditDto
    {
        public TenantSettingsEditDto()
        {
            TermsAndCondition = new TermsAndConditionsDto();
            Connect = new ConnectSettingsDto();
        }
        public GeneralSettingsEditDto General { get; set; }
        [Required] public TenantUserManagementSettingsEditDto UserManagement { get; set; }
        public TenantEmailSettingsEditDto Email { get; set; }
        public LdapSettingsEditDto Ldap { get; set; }
        [Required] public SecuritySettingsEditDto Security { get; set; }
        public TenantBillingSettingsEditDto Billing { get; set; }
        public TenantOtherSettingsEditDto OtherSettings { get; set; }
        public ExternalLoginProviderSettingsEditDto ExternalLoginProviderSettings { get; set; }
        public CompanySettingDto CompanySetting { get; set; }
        public TenantThemeSettingsEditDto Theme { get; set; }
        public CompanyLogoForCustomerSettingDto CompanyLogoForCustomerSetting { get; set; }
        public FileDateTimeFormatDto FileDateTimeFormat { get; set; }
        public SignUpFormSettingDto SignUpForm { get; set; }
        public ShippingProviderDto ShippingProvider { get; set; } = new ShippingProviderDto();
        public ConnectSettingsDto Connect { get; set; }
        public TouchSaleSettingsDto TouchSaleSettings { get; set; }

        /// <summary>
        /// This validation is done for single-tenant applications.
        /// Because, these settings can only be set by tenant in a single-tenant application.
        /// </summary>
        public void ValidateHostSettings()
        {
            var validationErrors = new List<ValidationResult>();
            if (Clock.SupportsMultipleTimezone && General == null)
            {
                validationErrors.Add(new ValidationResult("General settings can not be null", new[] {"General"}));
            }

            if (Email == null)
            {
                validationErrors.Add(new ValidationResult("Email settings can not be null", new[] {"Email"}));
            }

            if (validationErrors.Count > 0)
            {
                throw new AbpValidationException("Method arguments are not valid! See ValidationErrors for details.",
                    validationErrors);
            }
        }

        #region Tiffins

        public bool IsTiffinEnableStudentInformation { get; set; }
        public string TiffinPopup { get; set; }
        public int TiffinAllowOrderInFutureDays { get; set; }
        public int TiffinCutOffDays { get; set; }
        public bool TiffinIsEnableDelivery { get; set; }
        public TermsAndConditionsDto TermsAndCondition { get; set; }
        public ReferralEarningDto ReferralEarning { get; set; }
        public TenantMealSettingEditDto TenantMealSetting { get; set; }

        #endregion

        #region Clique

        public int CliqueCutOffDays { get; set; }
        public TenantMealSettingEditDto TenantCliqueMealSetting { get; set; }
        public int CliqueAllowOrderInFutureDays { get; set; }
        public bool CliqueAddressObligatory { get; set; }

        #endregion
    }
}
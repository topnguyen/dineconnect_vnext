﻿namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TouchSaleSettingsDto
    {
        public string Currency { get; set; }
        public string Decimals { get; set; }
        public string Rounding { get; set; }
        public string TaxInclusive { get; set; }
        public string ReceiptHeader { get; set; }
        public string ReceiptFooter { get; set; }
        public string IsActivity { get; set; }
        public string IsReport { get; set; }
        public string IsBlindShift { get; set; }
        public string DuplicateReceipt { get; set; }
    }
}

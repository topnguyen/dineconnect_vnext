﻿namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class FileDateTimeFormatDto
    {
        public string FileDateTimeFormat { get; set; }
        
        public string FileDateFormat { get; set; }
        
        public string FileDayMonthFormat { get; set; }
    }
}

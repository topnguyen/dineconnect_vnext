﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class ConnectSettingsDto
    {
        public string WeekDay { get; set; }
        public string WeekEnd { get; set; }
        public string Schedules { get; set; }
        public string SyncUrl {get;set;}
        public string SyncTenantId {get;set;}
        public string SyncTenantName {get;set;}
        public string SyncUser {get;set;}
        public string SyncPassword {get;set;}
    }
}

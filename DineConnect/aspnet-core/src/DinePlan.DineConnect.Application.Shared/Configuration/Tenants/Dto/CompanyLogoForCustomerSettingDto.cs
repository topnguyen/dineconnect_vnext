﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
   public class CompanyLogoForCustomerSettingDto
    {
        public string CompanyLogoUrl { get; set; }
    }
}

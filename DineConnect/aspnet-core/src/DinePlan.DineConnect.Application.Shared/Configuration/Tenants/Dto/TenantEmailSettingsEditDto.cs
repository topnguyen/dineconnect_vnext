﻿using Abp.Auditing;
using DinePlan.DineConnect.Configuration.Dto;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}
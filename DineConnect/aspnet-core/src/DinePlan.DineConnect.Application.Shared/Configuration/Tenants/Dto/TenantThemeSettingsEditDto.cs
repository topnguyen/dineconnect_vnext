﻿namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantThemeSettingsEditDto
    {
        public string Color { get; set; }
    }
}
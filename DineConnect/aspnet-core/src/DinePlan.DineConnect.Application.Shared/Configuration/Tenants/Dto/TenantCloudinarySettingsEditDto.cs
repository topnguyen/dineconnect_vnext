﻿using Abp.Auditing;
using DinePlan.DineConnect.Configuration.Dto;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantCloudinarySettingsEditDto
    {
        public string CloudName { get; set; }

        public string APIKey { get; set; }

        public string APISecret { get; set; }

        public string EnvironmentVariable { get; set; }
    }
}
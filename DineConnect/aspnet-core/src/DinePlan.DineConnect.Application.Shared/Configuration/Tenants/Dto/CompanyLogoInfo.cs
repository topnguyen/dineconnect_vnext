﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class CompanyLogoInfo
    {
        public string Name { get; set; }

        public string UploadedImageToken { get; set; }
    }
}

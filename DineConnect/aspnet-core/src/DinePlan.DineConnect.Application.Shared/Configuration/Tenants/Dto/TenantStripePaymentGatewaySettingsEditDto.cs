﻿using Abp.Auditing;
using DinePlan.DineConnect.Configuration.Dto;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantStripePaymentGatewaySettingsEditDto
    {
        public string BaseUrl { get; set; }
        public string PublishableKey { get; set; }
        public string SecretKey { get; set; }

    }
}
﻿namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class WheelClientDto
    {
        public string Domain { get; set; }
    }
}

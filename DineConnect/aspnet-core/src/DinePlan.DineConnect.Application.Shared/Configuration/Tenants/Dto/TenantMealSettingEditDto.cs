﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TenantMealSettingEditDto
    {
        public int? MinMealCount { get; set; }
        public int? MinExpiryDay { get; set; }
        public DateTime? OrderCutOffTime { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class UpdateTermsAndConditionInput
    {
        public string Content { get; set; }
    }
}
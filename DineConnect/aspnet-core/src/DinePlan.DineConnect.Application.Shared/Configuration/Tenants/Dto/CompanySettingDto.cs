﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class CompanySettingDto
    {
        public string CompanyName { get; set; }
        public string CompanyLogoUrl { get; set; }
        public string CompanyAddress { get; set; }
        public string TaxNo { get; set; }
    }
}

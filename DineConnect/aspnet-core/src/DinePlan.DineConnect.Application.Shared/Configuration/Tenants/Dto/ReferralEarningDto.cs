﻿using DinePlan.DineConnect.Settings;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class ReferralEarningDto
    {
        public ReferralEarningType ReferralEarnType { get; set; }

        /// <summary>
        ///     If of Promotion or Product Offer
        /// </summary>
        public int ReferralEarnValue { get; set; }
    }

}

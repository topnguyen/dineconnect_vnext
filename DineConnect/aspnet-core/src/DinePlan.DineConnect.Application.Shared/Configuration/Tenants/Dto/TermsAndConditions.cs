﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class TermsAndConditionsDto
    {
        public string Content { get; set; }
    }

}

﻿using DinePlan.DineConnect.Settings;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class ShippingProviderDto
    {
        public ShippingProvider ShippingProvider { get; set; }

        public string ShippingProviderSetting { get; set; }

        public string ShippingGoGoVanSetting { get; set; }

        public string ShippingLaLaMoveSetting { get; set; }

        public string ShippingDunzoSetting { get; set; }
    }
}

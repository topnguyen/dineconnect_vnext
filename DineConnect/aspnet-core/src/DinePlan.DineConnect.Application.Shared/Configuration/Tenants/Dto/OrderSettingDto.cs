﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
   public class OrderSettingDto
    {
        public bool Accepted { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool AutoRequestDeliver { get; set; }
        public bool AskTheMobileNumberFirst { get; set; }
        public bool SignUp { get; set; }
    }
}

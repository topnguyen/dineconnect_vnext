﻿namespace DinePlan.DineConnect.EntityDynamicParameters
{
    public class EntityDynamicParameterGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}

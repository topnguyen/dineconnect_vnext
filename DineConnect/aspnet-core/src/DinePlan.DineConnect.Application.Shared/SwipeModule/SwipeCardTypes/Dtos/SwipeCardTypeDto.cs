﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos
{
    public class SwipeCardTypeDto : EntityDto
    {
        [MaxLength(30)]
        public virtual string Name { get; set; }

        public virtual bool DepositRequired { get; set; }

        public virtual decimal DepositAmount { get; set; }

        public virtual int? ExpiryDaysFromIssue { get; set; }

        public virtual bool RefundAllowed { get; set; }

        public virtual bool CustomerRequired { get; set; }

        public virtual decimal MinimumTopUpAmount { get; set; }
    }
}

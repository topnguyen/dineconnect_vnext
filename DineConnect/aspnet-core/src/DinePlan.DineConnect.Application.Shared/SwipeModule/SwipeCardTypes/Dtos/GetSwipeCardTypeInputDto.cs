﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos
{
    public class GetSwipeCardTypeInputDto : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}

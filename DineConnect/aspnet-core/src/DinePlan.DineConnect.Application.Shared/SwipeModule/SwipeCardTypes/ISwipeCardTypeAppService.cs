﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.SwipeModule.SwipeCardTypes.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCardTypes
{
    public interface ISwipeCardTypeAppService : IApplicationService
    {
        Task<PagedResultDto<SwipeCardTypeDto>> GetAll(GetSwipeCardTypeInputDto typeInput);

        Task<SwipeCardTypeDto> Get(EntityDto entityDto);

        Task<FileDto> GetToExcel(GetSwipeCardTypeInputDto typeInput);

        Task<int> AddOrUpdate(SwipeCardTypeDto dto);

        Task Delete(EntityDto entityDto);
    }
}

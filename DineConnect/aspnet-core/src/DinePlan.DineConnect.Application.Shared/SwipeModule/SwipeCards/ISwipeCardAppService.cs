﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards
{
    public interface ISwipeCardAppService : IApplicationService
    {
        Task<PagedResultDto<SwipeCardDto>> GetAll(GetSwipeCardInputDto input);

        Task<SwipeCardDto> Get(EntityDto entityDto);

        Task<FileDto> GetToExcel(GetSwipeCardInputDto input);

        Task<int> AddOrUpdate(SwipeCardDto dto);

        Task Delete(EntityDto entityDto);

        Task<SwipeCardDto> CheckCard(string cardNumber);

        Task<string> GenerateCardOTP(string cardNumber);
        
        Task ValidateCardOTP(string cardNumber, string OTP);

        Task<SwipeCardTransactionDto> AddTransaction(AddTransactionDto transaction);

        Task<SwipeCardTransactionDto> TopUp(SwipeCardTopUpDto input);

        Task<PagedResultDto<CustomerCardDto>> GetAllCustomerCard(GetCustomerCardInputDto input);

        Task<PagedResultDto<SwipeCardTransactionDto>> GetAllTransaction(GetCardTransactionInputDto input);
    }
}

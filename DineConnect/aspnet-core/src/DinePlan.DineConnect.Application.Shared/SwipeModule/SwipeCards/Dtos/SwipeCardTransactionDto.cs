﻿using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class SwipeCardTransactionDto : EntityDto
    {
        public virtual DateTime TransactionTime { get; set; }
        public virtual int? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public virtual int SwipeCardId { get; set; }

        public string SwipeCardNumber { get; set; }

        public virtual int TransactionType { get; set; }

        public virtual string Description { get; set; }

        public virtual decimal Credit { get; set; }

        public virtual decimal Debit { get; set; }

        public string PaymentType { get; set; }

        public string PaymentTransactionDetails { get; set; }
    }
}

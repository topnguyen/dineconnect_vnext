﻿namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public enum SwipeCardPaymentType
    {
        Cash,
        EWallet
    }
}

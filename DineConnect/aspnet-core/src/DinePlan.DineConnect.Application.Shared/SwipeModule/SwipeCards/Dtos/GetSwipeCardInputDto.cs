﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class GetSwipeCardInputDto : PagedAndSortedResultRequestDto
    {
        public string CardNumber { get; set; }

        public int? SwipeCardTypeId { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class CustomerCardDto
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CardNumber { get; set; }

        public int CardId { get; set; }

        public decimal Balance { get; set; }
    }
}

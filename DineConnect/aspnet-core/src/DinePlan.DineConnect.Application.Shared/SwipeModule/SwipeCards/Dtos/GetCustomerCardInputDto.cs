﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class GetCustomerCardInputDto : PagedAndSortedResultRequestDto
    {
        public int? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CardNumber { get; set; }
    }
}

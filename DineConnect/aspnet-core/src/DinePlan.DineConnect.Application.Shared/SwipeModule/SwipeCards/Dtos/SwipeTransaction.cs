﻿namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public enum SwipeTransaction
    {
        Topup,
        Refund,
        Deposit
    }
}

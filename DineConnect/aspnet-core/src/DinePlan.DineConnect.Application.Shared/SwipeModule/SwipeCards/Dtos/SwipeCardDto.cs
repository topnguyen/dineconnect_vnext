﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class SwipeCardDto : EntityDto
    {
        public virtual int CardTypeRefId { get; set; }

        [MaxLength(20)]
        public virtual string CardNumber { get; set; }

        public virtual decimal Balance { get; set; }

        public virtual bool Active { get; set; }

        public int? CustomerId { get; set; }

        public string CustomerName { get; set; }
    }
}

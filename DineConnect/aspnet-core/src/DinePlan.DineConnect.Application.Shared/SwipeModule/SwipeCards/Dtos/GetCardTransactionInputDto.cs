﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class GetCardTransactionInputDto: PagedAndSortedResultRequestDto
    {
        public int SwipeCardId { get; set; }
    }
}

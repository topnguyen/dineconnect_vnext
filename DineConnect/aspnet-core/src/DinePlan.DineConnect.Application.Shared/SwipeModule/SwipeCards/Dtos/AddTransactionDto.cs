﻿namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class AddTransactionDto
    {
        public string CardNumber { get; set; }

        public int? CustomerId { get; set; }

        public decimal Amount { get; set; }

        public string PaymentType { get; set; } = SwipeCardPaymentType.Cash.ToString();

        public string PaymentTransactionDetails { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.SwipeModule.SwipeCards.Dtos
{
    public class SwipeCardTopUpDto
    {
        public string CardNumber { get; set; }

        public string PaymentOption { get; set; }

        public decimal PaidAmount { get; set; }

        // Omise


        public string OmiseToken { get; set; }

        public string Source { get; set; }


        // Adyen

        public string AdyenEncryptedCardNumber { get; set; }

        public string AdyenEncryptedExpiryMonth { get; set; }

        public string AdyenEncryptedExpiryYear { get; set; }

        public string AdyenEncryptedSecurityCode { get; set; }

        // Paypal

        public string PaypalOrderId { get; set; }

        // Paypal
        public string StripeToken { get; set; }

        // Google

        public string GooglePayToken { get; set; }
    }
}

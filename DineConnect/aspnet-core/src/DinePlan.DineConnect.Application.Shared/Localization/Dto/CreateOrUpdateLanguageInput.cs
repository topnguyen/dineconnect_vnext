﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}
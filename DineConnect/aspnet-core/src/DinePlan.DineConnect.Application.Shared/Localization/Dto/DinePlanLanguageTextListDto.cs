﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Localization.Dto
{
    public class DinePlanLanguageTextListDto : FullAuditedEntityDto
    {
        public long? Id { get; set; }

        public string LanguageName { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
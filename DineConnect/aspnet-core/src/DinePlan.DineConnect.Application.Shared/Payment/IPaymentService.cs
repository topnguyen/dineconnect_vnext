﻿using DinePlan.DineConnect.PaymentProcessor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Payment
{
    public interface IPaymentService
    {
        Task<List<IPaymentProcessor>> GetAllPaymentProcessor();

        Task<IPaymentProcessor> GetPaymentProcessorBySystemName(string systemName);
    }
}

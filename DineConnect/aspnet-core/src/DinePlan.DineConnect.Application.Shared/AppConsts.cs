﻿using System;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// Some consts used in the application.
    /// </summary>
    public class AppConsts
    {
        /// <summary>
        /// Default page size for paged requests.
        /// </summary>
        public const int DefaultPageSize = 10;

        /// <summary>
        /// Maximum allowed page size for paged requests.
        /// </summary>
        public const int MaxPageSize = 1000;

        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";

        public const int ResizedMaxProfilPictureBytesUserFriendlyValue = 1024;

        public const int MaxProfilPictureBytesUserFriendlyValue = 5;

        public const string TokenValidityKey = "token_validity_key";
        public const string SecurityStampKey = "AspNet.Identity.SecurityStamp";

        public const string TokenType = "token_type";

        public static string UserIdentifier = "user_identifier";

        public const string CorsPublicName = "Public";
        public const string ConnectFilter = "ConnectFilter";
        public const string ThemeDefault = "default";
        public const string Theme2 = "theme2";
        public const string Theme3 = "theme3";
        public const string Theme4 = "theme4";
        public const string Theme5 = "theme5";
        public const string Theme6 = "theme6";
        public const string Theme7 = "theme7";
        public const string Theme8 = "theme8";
        public const string Theme9 = "theme9";
        public const string Theme10 = "theme10";
        public const string Theme11 = "theme11";
        public const string Theme12 = "theme12";
        public const string Cash = "Cash";
        public const string CashSales = "CashSales";
        public const string WithHoldTax = "WithHoldTax";
        public const string SalesWithHoldingTax = "SalesWithHoldingTax";
        public static TimeSpan AccessTokenExpiration = TimeSpan.FromDays(1);
        public static TimeSpan RefreshTokenExpiration = TimeSpan.FromDays(365);
        public const string TaxIds = "TaxIds";
        public const string DateTimeOffsetFormat = "yyyy-MM-ddTHH:mm:sszzz";

        // File Date Time Formats, please concern about the format using for File Name cases.
        
        public const string FileDateTimeFormat = "dd-MM-yyyy hh:mm:ss";
        public const string FileDateFormat = "dd-MM-yyyy";
        public const string FileDayMonthFormat = "dd-MM";

        public const string WheelTheme1 = "wheelTheme1";
        public const string WheelTheme2 = "wheelTheme2";
        public const string WheelTheme3 = "wheelTheme3";
        public const string WheelTheme4 = "wheelTheme4";
        public const string WheelTheme5 = "wheelTheme5";
        public const string WheelTheme6 = "wheelTheme6";
        public const string WheelTheme7 = "wheelTheme7";
        public const string WheelTheme8 = "wheelTheme8";
        public const string WheelTheme9 = "wheelTheme9";
        public const string WheelTheme10 = "wheelTheme10";
        public const string WheelTheme11 = "wheelTheme11";
        public const string WheelTheme12 = "wheelTheme12";

        public const string GetAllWheelScreenMenuKey = "GetAllWheelScreenMenu";
        public const string GetWheelScreenMenuForEditKey = "GetWheelScreenMenuForEdit";
        public const string GetChildrenScreenMenuKey = "GetChildrenScreenMenu";
        public const string GetChildrenSectionMenuKey = "GetChildrenSectionMenu";
        public const string GetWheelScreenMenuForComboboxKey = "GetWheelScreenMenuForCombobox";

        public const string GetChildrenScreenMenuItemKey = "GetChildrenScreenMenuItem";
        public const string GetWheelScreenMenuItemForEditKey = "GetWheelScreenMenuItemForEdit";
        public const string GetWheelScreenMenuItemForSelectionKey = "GetWheelScreenMenuItemForSelection";

        public const string GetMenuItemAndPriceForSelectionKey = "GetMenuItemAndPriceForSelection";
        public const string GetAllWheelProductSoldOutKey = "GetAllWheelProductSoldOutKey";
        public const string GetWheelProductSoldOutForEditKey = "GetWheelProductSoldOutForEditKey";
        public const string GetAllWheelRecommendationMenuKey = "GetAllWheelRecommendationMenuKey";
        public const string GetWheelRecommendationMenuForEditKey = "GetWheelRecommendationMenuForEditKey";
    }
}
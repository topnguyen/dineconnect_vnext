﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuItemsViewDto : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Published { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
        public WheelDisplayAs DisplayAs { get; set; }
        public int? NumberOfColumns { get; set; }
        public bool HideGridTitles { get; set; }
        public bool MarkAsNew { get; set; }
        public bool MarkAsSignature { get; set; }
        public string Note { get; set; }
        public int PreperationTime { get; set; }
        public string IngredientWarnings { get; set; }
        public bool DisplayNutritionFacts { get; set; }
        public string NutritionInfo { get; set; }
        public int Order { get; set; }
        public WheelScreenMenuItemType Type { get; set; }
        public int ScreenMenuId { get; set; }
        public int? ParentId { get; set; }
    }
}

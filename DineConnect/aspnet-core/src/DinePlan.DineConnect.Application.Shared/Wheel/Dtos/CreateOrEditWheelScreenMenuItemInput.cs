﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelScreenMenuItemInput : Entity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Published { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
        public WheelDisplayAs DisplayAs { get; set; }
        public int NumberOfColumns { get; set; }
        public bool HideGridTitles { get; set; }
        public bool MarkAsNew { get; set; }
        public bool MarkAsSignature { get; set; }
        public string Note { get; set; }
        public int? ParentId { get; set; }
        public int ScreenMenuId { get; set; }
        public int PreperationTime { get; set; }
        public string IngredientWarnings { get; set; }
        public bool DisplayNutritionFacts { get; set; }
        public int? SeletedMenuItemIdPrice { get; set; }
        public int? ProductComboId { get; set; }
        public bool? AllowPreOrder { get; set; }
        public WheelScreenMenuItemType Type { get; set; }
        public List<WheelScreenMenuItemlPricesDto> Prices { get; set; }
        public List<WheelRecommendedForItemEditDto> RecommendedItems { get; set; }
        public List<string> Tags { get; set; }
        public List<string> TimingSettings { get; set; }
        public string SubCategory { get; set; }
    }
}
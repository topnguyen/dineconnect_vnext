﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Configuration.Tenants.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelSetupForEditOutput
    {
		public CreateOrEditWheelSetupDto WheelSetup { get; set; }

        public string Currency { get; set; }

        public OrderSettingDto OrderSetting { get; set; }
    }
}
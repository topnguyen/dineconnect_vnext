﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelProductSoldOutListDto : AuditedEntityDto
    {
        public string Name { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public int MenuItemId { get; set; }
        public int? MenuItemPortionId { get; set; }
        public bool Disable { get; set; }
    }
}

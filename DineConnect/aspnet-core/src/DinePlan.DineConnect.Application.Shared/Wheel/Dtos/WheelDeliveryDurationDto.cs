﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDeliveryDurationDto : EntityDto
    {
		public int TenantId { get; set; }

		public decimal DeliveryDistance { get; set; }

		public decimal DeliveryTime { get; set; }



    }
}
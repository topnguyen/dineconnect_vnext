﻿using Abp.Application.Services.Dto;
using System;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelOpeningHourDto: EntityDto<int>
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public int OpenDays { get; set; }
        public WheelOpeningHourServiceType ServiceType { get; set; }
    }
}

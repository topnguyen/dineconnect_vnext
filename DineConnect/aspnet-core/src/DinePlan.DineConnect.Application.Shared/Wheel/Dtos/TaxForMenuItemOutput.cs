﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class TaxForMenuItemOutput
    {
        public int MenuItemId { get; set; }

        public decimal TaxPercentage { get; set; }

        public decimal TaxTotal { get; set; }
        public bool TaxInclusive { get; set; }
    }
}
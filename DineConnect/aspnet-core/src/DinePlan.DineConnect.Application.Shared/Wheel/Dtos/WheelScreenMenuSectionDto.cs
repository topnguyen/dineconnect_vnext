using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuSectionDto : WheelScreenMenuItemDto
    {
        public List<WheelScreenMenuSectionDto> Items { get; set; }
    }
}
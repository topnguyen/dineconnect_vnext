using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelDepartmentOutput : EntityDto
    {
        public string Name { get; set; }

        public bool Enable { get; set; }

        public WheelDepartmentConsts.WheelOrderType WheelOrderType { get; set; }

        public int? OpeningHourId { get; set; }

        public int? MenuId { get; set; }

        public int? PriceTagId { get; set; }

        public string WheelPaymentMethodIds { get; set; }
        public List<GetWheelPaymentMethodForViewDto> WheelPaymentMethods { get; set; }

        public string DinePlanTaxIds { get; set; }
        public List<DinePlanTaxDto> DinePlanTaxs { get; set; }
        public bool IsEnableTable { get; set; }
        public bool IsEnableDelivery { get; set; }

        public bool IsRequireOtp { get; set; }
        public List<GetWheelLocationOutput> WheelLocationOutputs { get; set; }

        public int? FutureOrderDaysLimit { get; set; }

        public bool IsTimeSlotMode { get; set; }

        public List<TimeSlotDto> TimeSlots { get; set; }

        public string WheelOpeningHourIds { get; set; }

        public List<DinePlanOpeningHoursDto> WheelOpeningHours { get; set; }

        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }
        public string WheelServiceFeeIds { get; set; }
        public bool IsOpeningHour { get; set; }
        public bool AllowPreOrder { get; set; }
        public DateTime? DateDefaultOrderFututer { get; set; }
        public List<DayOfWeek> CloseDayOfWeek { get; set; }
    }

    public class GetWheelLocationOutput : EntityDto
    {
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool TaxInclusive { get; set; }

        public bool OutsideDeliveryZone { get; set; }

        public int? LocationId { get; set; }

        public int? ScreenMenuId { get; set; }

        public List<WheelDeliveryZoneDto> WheelDeliveryZones { get; set; }
        public string WheelDeliveryZoneIds { get; set; }

        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }
        public string WheelServiceFeeIds { get; set; }
        public List<WheelTaxDto> WheelTaxes { get; set; }
        public string WheelTaxIds { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string LocationName { get; set; }
        public string PickupAddressLat { get; set; }
        public string PickupAddressLong { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelOpeningHoursInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public WheelOpeningHourServiceType WheelOpeningHourServiceType { get; set; }
    }
}

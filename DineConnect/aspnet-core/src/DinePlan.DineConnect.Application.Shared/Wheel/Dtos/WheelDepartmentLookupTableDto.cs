namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDepartmentLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDynamicPageForComboboxItemDto : ComboboxItemDto
    {
        public WheelDynamicPageForComboboxItemDto(string value, string displayText, bool isSelected)
        {
            Value = value;
            DisplayText = displayText;
            IsSelected = isSelected;
        }
    }
}

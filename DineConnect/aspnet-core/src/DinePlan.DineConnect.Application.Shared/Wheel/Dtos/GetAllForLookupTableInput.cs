﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
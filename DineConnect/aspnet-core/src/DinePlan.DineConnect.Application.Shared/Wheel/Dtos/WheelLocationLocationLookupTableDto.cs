﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelLocationLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}
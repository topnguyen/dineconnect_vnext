﻿using DinePlan.DineConnect.Connect.Menu.ScreenMenus.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetMenuItemsOuputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SubCategory { get; set; }
        public string ButtonColor { get; set; }
        public bool AutoSelect { get; set; }
        public string CategoryName { get; set; }
        public string SubMenuTag { get; set; }
        public double FontSize { get; set; }
        public bool ShowAliasName { get; set; }
        public string MenuItemAliasName { get; set; }
        public int ConfirmationType { get; set; }
        public string ConfirmationTypeName => ((ScreenMenuItemConfirmationTypes)ConfirmationType).ToString();
        public bool WeightedItem { get; set; }
        public int SortOrder { get; set; }
        public int? MenuItemId { get; set; }
        public int? ProductComboId { get; set; }
        public string ImagePath { get; set; }
        public int ProductType { get; set; }
        public List<WheelScreenMenuItemlPricesDto> Prices { get; set; }
        public GetMenuItemsOuputDto()
        {
            Prices = new List<WheelScreenMenuItemlPricesDto>();
        }
    }
}

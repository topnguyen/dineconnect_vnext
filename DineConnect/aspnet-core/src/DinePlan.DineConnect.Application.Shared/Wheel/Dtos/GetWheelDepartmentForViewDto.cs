﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.PriceTags.Dtos;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelDepartmentForViewDto : EntityDto
    {
        public string Name { get; set; }

        public WheelOrderType WheelOrderType { get; set; }

        public int? PriceTagId { get; set; }

        public int? MenuId { get; set; }

        public string OpeningHours { get; set; }

        public bool Enable { get; set; }

        public WheelScreenMenusListDto Menu { get; set; }
        public WheelOpeningHourDto OpeningHour { get; set; }
        public PriceTagDto PriceTag { get; set; }
        public bool IsEnableTable { get; set; }
        public bool IsEnableDelivery { get; set; }
        public bool IsRequireOtp { get; set; }
        public List<GetWheelPaymentMethodForViewDto> WheelPaymentMethods{ get; set; }

        public int? FutureOrderDaysLimit { get; set; }
        public int? OrderDeliveryTime { get; set; }
        public int? OrderPreparationTime { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelRecommendationMenuDto
    {
        public int Id { get; set; }
        public string RepeatTime { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Pax { get; set; }
        public WheelRecommendationMenuConst.RecommendationStatus Status { get; set; }
        public WheelRecommendationMenuConst.CriteriaNotShow? CriteriaNotShow { get; set; }
        public List<WheelRecommendationMenuConst.TriggerApplicable> TriggerApplicable { get; set; }
        public bool Disable { get; set; }
        public ICollection<WheelRecommendationDayPeriodDto> WheelRecommendationDayPeriods { get; set; }
        public ICollection<WheelRecommendationItemDto> WheelRecommendationItems { get; set; }
    }
}

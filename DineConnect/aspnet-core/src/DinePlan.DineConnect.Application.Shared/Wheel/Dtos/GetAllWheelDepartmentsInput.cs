﻿using Abp.Application.Services.Dto;
using System;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
	public class GetAllWheelDepartmentsInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public WheelOrderType? WheelOrderTypeFilter { get; set; }

		public string PriceTagFilter { get; set; }
		public string MenuFilter { get; set; }
		public string OpeningHoursFilter { get; set; }
		public bool? Enable { get; set; }

	}
}
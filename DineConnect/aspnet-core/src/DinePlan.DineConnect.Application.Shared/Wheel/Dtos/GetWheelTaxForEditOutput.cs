﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelTaxForEditOutput
    {
		public CreateOrEditWheelTaxDto WheelTax { get; set; }

		public string WheelTaxName { get; set;}


    }
}
﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelDynamicPagesInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
}

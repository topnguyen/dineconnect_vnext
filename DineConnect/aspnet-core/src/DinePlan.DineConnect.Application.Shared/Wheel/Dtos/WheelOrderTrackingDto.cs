﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelOrderTrackingDto : EntityDto<int?>
    {
        public int? TenantId { get; set; }
        
        public int? WheelDepartmentTimeSlotId { get; set; }

        public string OrderDate { get; set; }
    }
}
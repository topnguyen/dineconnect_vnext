﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelScreenMenuItemForSelection : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}

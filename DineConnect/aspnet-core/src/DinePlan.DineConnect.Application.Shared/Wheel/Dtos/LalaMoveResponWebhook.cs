﻿using Newtonsoft.Json;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class LalaMoveResponWebhook
    {

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("eventId")]
        public string EventId { get; set; }

        [JsonProperty("eventType")]
        public string EventType { get; set; }

        [JsonProperty("data")] 
        public DataRespon Data { get; set; }
    }

    public class DataRespon
    {
        [JsonProperty("order")]
        public OrderRespon Order { get; set; }
    }
    public class OrderRespon
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("driverId")]
        public string DriverId { get; set; }

        [JsonProperty("price")]
        public PriceRespon Price { get; set; }
    }

    public class PriceRespon
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("subTotal")]
        public string SubTotal { get; set; }

        [JsonProperty("priorityFee")]
        public string PriorityFee { get; set; }

        [JsonProperty("totalPrice")]
        public string TotalPrice { get; set; }
    }
}
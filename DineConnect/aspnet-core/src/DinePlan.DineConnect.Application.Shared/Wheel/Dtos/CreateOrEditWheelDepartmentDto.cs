﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes.Dtos;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelDepartmentDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }

        public bool Enable { get; set; }

        public WheelOrderType WheelOrderType { get; set; }

        public int? MenuId { get; set; }

        public int? PriceTagId { get; set; }

        //public IList<WheelServiceFeeDto> WheelServiceFees { get; set; }
        //public string WheelServiceFeeIds { get; set; }

        public string WheelPaymentMethodIds { get; set; }

        [NotMapped]
        public List<GetWheelPaymentMethodForViewDto> WheelPaymentMethods { get; set; }

        [NotMapped]
        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }

        public string WheelServiceFeeIds { get; set; }

        public string WheelOpeningHourIds { get; set; }

        [NotMapped]
        public List<DinePlanOpeningHoursDto> WheelOpeningHours { get; set; }

        public bool IsEnableTable { get; set; }

        public bool AutoAccept { get; set; }

        public bool IsEnableDelivery { get; set; }
        public bool IsRequireOtp { get; set; }
        public bool SendOtp { get; set; }

        public bool AllowPreOrder { get; set; }

        public int? FutureOrderDaysLimit { get; set; }
        public int? OrderDeliveryTime { get; set; }
        public int? OrderPreparationTime { get; set; }
    }
}
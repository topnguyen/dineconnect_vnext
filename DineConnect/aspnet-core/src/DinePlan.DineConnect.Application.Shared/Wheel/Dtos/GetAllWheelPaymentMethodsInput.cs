﻿using Abp.Application.Services.Dto;
using System;
using static DinePlan.DineConnect.Wheel.WheelPaymentMethodConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelPaymentMethodsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string SystemNameFilter { get; set; }

        public string FriendlyNameFilter { get; set; }

        public RecurringPaymentType? RecurringSupportFilter {get; set;}

        public int SupportsCaptureFilter { get; set; }
        public int RefundFilter { get; set; }
        public int PartialRefundFilter { get; set; }
        public int VoidFilter { get; set; }
        public int ActiveFilter { get; set; }
    }
}
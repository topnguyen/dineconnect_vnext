﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.Dtos.Syncs
{
    public class WheelTicketSyncOutputDto
    {
        public WheelTicketSyncOutputDto()
        {
            Tickets = new List<WheelTicketSyncDto>();
        }

        public List<WheelTicketSyncDto> Tickets { get; set; }
    }

    public class WheelTicketSyncDto

    {
        public WheelTicketSyncDto()
        {
            WheelBillingInfo = new WheelBillingInfo();
        }

        public int Id { get; set; }
        public string InvoiceNumber { get; set; }

        public string DepartmentName { get; set; }
        public string PaymentOption { get; set; }
        public decimal? PaidAmount { get; set; }
        public string Note { get; set; }
        public DateTime OrderCreatedTime { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? DateDelivery { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveringTime { get; set; }
        public int? TableId { get; set; }
        public string TableName { get; set; }
        public WheelOrderStatus OrderStatus { get; set; }
        public WheelBillingInfo WheelBillingInfo { get; set; }

        public WheelDepartmentConsts.WheelOrderType WheelOrderType { get; set; }
        public CartTotalDto CartTotal { get; set; }
        public List<WheelCartItemDto> Orders { get; set; }
        public string ActiveShippingDeliveryOrder { get; set; }
        public string ActiveShippingDeliveryOrderStatus { get; set; }
        public string ShippingProvider { get; set; }
    }



   
}
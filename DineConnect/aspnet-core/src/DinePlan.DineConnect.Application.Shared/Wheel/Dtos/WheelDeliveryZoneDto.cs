﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDeliveryZoneDto: EntityDto<int>
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public decimal MinimumTotalAmount { get; set; }
        public decimal DeliveryFee { get; set; }
        public WheelDeliveryZoneType ZoneType { get; set; }
        public string Color { get; set; }
        public string ZoneCoordinates { get; set; }
    }
}

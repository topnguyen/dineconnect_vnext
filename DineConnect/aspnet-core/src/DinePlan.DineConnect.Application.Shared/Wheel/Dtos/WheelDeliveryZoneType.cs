﻿
namespace DinePlan.DineConnect.Wheel.Dtos
{
    public enum WheelDeliveryZoneType
    {
        Circle = 0,
        Polygon = 1
    }
}

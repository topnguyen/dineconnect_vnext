﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelTaxesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }


		 public string WheelTaxNameFilter { get; set; }

		 
    }
}
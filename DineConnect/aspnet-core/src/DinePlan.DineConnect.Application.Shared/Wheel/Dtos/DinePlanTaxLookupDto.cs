﻿
namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class DinePlanTaxLookupDto
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }
}

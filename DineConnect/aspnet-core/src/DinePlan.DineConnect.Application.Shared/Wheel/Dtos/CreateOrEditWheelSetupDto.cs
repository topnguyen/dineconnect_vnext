﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelSetupDto : EntityDto<int?>
    {
        public int? HomePageId { get; set; }
        
        public int? CheckOutPageId { get; set; }

        public string ThemeName { get; set; }

        public string LogoCompanyUrl { get; set; }

        public string WelcomeUrl { get; set; }

        public string AdvertOneUrl { get; set; }

        public string AdvertTwoUrl { get; set; }

        public string WelcomeMessage { get; set; }

        public string TermsAndConditions { get; set; }

        public string Title { get; set; }

        public WheelPostCodeServiceType WheelPostCodeServiceType { get; set; }

        public List<WheelLocationDto> WheelLocations { get; set; }

        public string WheelLocationIds { get; set; }

        public string ClientUrl { get; set; }

        public WheelDynamicPageEditDto wheelDynamicPageEditDto { get; set; }
        public bool IsDisplayHomePage { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
    }

    public enum WheelPostCodeServiceType
    {
        OneMap,
        GeoApi
    }
}
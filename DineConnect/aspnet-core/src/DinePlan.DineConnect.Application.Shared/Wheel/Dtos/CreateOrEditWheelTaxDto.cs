﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelTaxDto : EntityDto<int?>
    {

		public string Name { get; set; }
		
		
		 public int? DinePlanTaxId { get; set; }
		 
		 
    }
}
﻿using DinePlan.DineConnect.Wheel;

using System;
using Abp.Application.Services.Dto;
using static DinePlan.DineConnect.Wheel.WheelLocationConsts;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelLocationDto : EntityDto
    {
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool TaxInclusive { get; set; }

        public bool OutsideDeliveryZone { get; set; }

        public WheelPostalCodeSearchType WheelPostalCodeSearchType { get; set; }

        public int? LocationId { get; set; }

        public int? ScreenMenuId { get; set; }

        public List<WheelDeliveryZoneDto> WheelDeliveryZones { get; set; }
        public List<WheelDepartmentDto> WheelDepartments { get; set; }
        public List<WheelTaxDto> WheelTaxes { get; set; }
        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }

        public string Email { get; set; }

        public string WelcomeMessage { get; set; }
        public string PickupContactName { get; set; }
        public string PickupContactPhoneNumber { get; set; }
        public string PickupAddressLat { get; set; }
        public string PickupAddressLong { get; set; }
        public string PickupAddress { get; set; }
    }
}
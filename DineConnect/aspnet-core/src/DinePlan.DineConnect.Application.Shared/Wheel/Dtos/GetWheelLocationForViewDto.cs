﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelLocationForViewDto : EntityDto
    {
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool TaxInclusive { get; set; }

        public bool OutsideDeliveryZone { get; set; }

        public int? LocationId { get; set; }

        public int? ScreenMenuId { get; set; }

        public List<WheelDeliveryZoneDto> WheelDeliveryZones { get; set; }

        public List<WheelDepartmentDto> WheelDepartments { get; set; }

        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }

        public string LocationName { get; set;}

		public string ScreenMenuName { get; set;}

        public string Email { get; set; }

        public decimal? OutsideDeliveryZoneDeliveryFee { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class PlaceOrderToShippingOutputDto
    {
        public string CustomerOrderId { get; set; }

        public string PlaceOrderResponse { get; set; }
    }
}

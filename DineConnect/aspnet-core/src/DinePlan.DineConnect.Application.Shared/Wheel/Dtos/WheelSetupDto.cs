﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelSetupDto : EntityDto
    {
        public string CheckOutPage { get; set; }

        public string Theme { get; set; }
        public List<WheelLocationDto> WheelLocations { get; set; }
        public string WheelLocationIds { get; set; }
        public string ClientUrl { get; set; }
        public WheelDynamicPageEditDto HomePage { get; set; }

        public string HomePages { get { return HomePage != null ? HomePage.Name : null; } }
        public string CountryCode { get; set; }

        public string Title { get; set; }
        public string Currency { get; set; }
    }
}
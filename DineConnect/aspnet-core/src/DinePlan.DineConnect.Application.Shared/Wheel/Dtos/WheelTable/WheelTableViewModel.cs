namespace DinePlan.DineConnect.Wheel.Dtos.WheelTable
{
    public class WheelTableViewModel : WheelTableBaseModel
    {
        public string OtpCode { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public string QrLink { get; set; } 
    }
}
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos.WheelTable
{
    public class GetAllWheelTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public string LocationName { get; set; }

        public string DepartmentName { get; set; }

        public WheelTableStatus? WheelTableStatus { get; set; }
    }
}
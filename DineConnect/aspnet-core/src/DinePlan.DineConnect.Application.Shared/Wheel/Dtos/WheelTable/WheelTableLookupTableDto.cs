namespace DinePlan.DineConnect.Wheel.Dtos.WheelTable
{
    public class WheelTableLookupTableDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
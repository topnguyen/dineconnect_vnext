namespace DinePlan.DineConnect.Wheel.Dtos.WheelTable
{
    public class WheelTableBaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public WheelTableStatus WheelTableStatus { get; set; } = WheelTableStatus.Vacant;
        public int? WheelLocationId { get; set; }
        public int? WheelDepartmentId { get; set; }
    }

    public class DineUrlObject
    {
        public string Url { get; set; }
        public string Pincode { get; set; }
    }

    public class DineUrlObjectInput
    {
        public int LocationId { get; set; }
        public string TableName { get; set; }
        public int TenantId { get; set; }

    }
}
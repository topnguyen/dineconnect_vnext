﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
   public class CreateWheelTicketShippingOrderInputDto
    {
        public int TicketId { get; set; }

        public bool IsManual { get; set; }

        public string ServiceType {get;set;}

        public string ManualTime { get; set; }
    }
}

﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelDeliveryDurationForEditOutput
    {
		public CreateOrEditWheelDeliveryDurationDto WheelDeliveryDuration { get; set; }


    }
}
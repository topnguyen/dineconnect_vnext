﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class SaveCustomerSessionDataInput
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}

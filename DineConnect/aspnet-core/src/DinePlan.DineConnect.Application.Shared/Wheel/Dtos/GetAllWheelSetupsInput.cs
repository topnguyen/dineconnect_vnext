﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelSetupsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string HomePageFilter { get; set; }

		public string CheckOutPageFilter { get; set; }

		public string ThemeFilter { get; set; }

    }
}
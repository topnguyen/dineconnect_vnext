﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetPageInput
    {
        public int CurrentPage { get; set; }
        public int WheelDepatermentId { get; set; }
        public bool IsGenerateQRCode { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class AddItemToCartInputDto
    {
        public string Key { get; set; }

        public int ScreenMenuItemId { get; set; }

        public int Quantity { get; set; }

        public List<WheelCartItemOrderTagDto> Tags { get; set; }

        public WheelCartItemPortionDto Portion { get; set; }

        public List<AddItemToCartInputDto> SubItems { get; set; }

        public string Note { get; set; }
        public string ItemName { get; set; }
        public decimal Price { get; set; }

        public int MenuItemId { get; set; }
    }

    public class WheelCartItemDto
    {
        public int ScreenMenuItemId { get; set; }

        public WheelScreenMenuItemSimpleDto ScreenMenuItem { get; set; }

        public string Key { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }

        public decimal? UnitPrice { get; set; }
        public decimal? LineUnitPrice { get; set; }

        public decimal? SubTotal => (UnitPrice ?? 0) * Quantity;

        public decimal? Tax { get; set; }

        public List<WheelCartItemOrderTagDto> Tags { get; set; }

        public WheelCartItemPortionDto Portion { get; set; }

        public List<WheelCartItemDto> SubItems { get; set; }

        public int MenuItemId { get; set; }
        public int Id { get; set; }


        public bool IsCombo => SubItems != null && SubItems.Any();
        

        public string TagDescription
        {
            get
            {
                var text = new List<string>();

                if (Tags != null && Tags.Any()) text.AddRange(Tags.Select(t => $"{t.Name} x {t.Quantity}").ToList());

                return string.Join(", ", text.ToArray());
            }
        }


        public string Note { get; set; }
    }


    public class WheelGetCartOutputDto
    {
        public WheelGetCartOutputDto()
        {
            CartItems = new List<WheelCartItemDto>();
            CartTotal = new CartTotalDto();
        }

        public int CustomerId { get; set; }

        public List<WheelCartItemDto> CartItems { get; set; }

        public List<WheelCartItemDto> OrderedItems { get; set; }

        public CartTotalDto CartTotal { get; set; }
    }

    public class CartTotalDto
    {
        public decimal? SubTotal { get; set; }
        public decimal? ServiceFeeTotal { get; set; }
        public decimal? ItemsTaxTotal { get; set; }
        public decimal? ServiceFeeTaxTotal { get; set; }
        public decimal TotalPrice { get; set; }

        public decimal TaxAmount
        {
            get
            {
                decimal returnAmount = 0;
                if (ItemsTaxTotal != null)
                {
                    returnAmount+=ItemsTaxTotal.Value;
                }
                if (ServiceFeeTaxTotal != null)
                {
                    returnAmount+=ServiceFeeTaxTotal.Value;
                }
                return returnAmount;
            }
        }

    }
}
﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelLocationForEditOutput : EntityDto
    {
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool TaxInclusive { get; set; }

        public bool OutsideDeliveryZone { get; set; }

        public int? LocationId { get; set; }

        public int? ScreenMenuId { get; set; }

        public List<WheelDeliveryZoneDto> WheelDeliveryZones { get; set; }
        public string WheelDeliveryZoneIds { get; set; }

        public List<GetWheelDepartmentForViewDto> WheelDepartments { get; set; }
        public string WheelDepartmentIds { get; set; }

        public List<WheelServiceFeeDto> WheelServiceFees { get; set; }
        public string WheelServiceFeeIds { get; set; }
        public List<WheelTaxDto> WheelTaxes { get; set; }
        public string WheelTaxIds { get; set; }

        public string LocationName { get; set; }

        public string Email { get; set; }

        public decimal? OutsideDeliveryZoneDeliveryFee { get; set; }

        public string WelcomeMessage { get; set; }
        public string PickupContactName { get; set; }
        public string PickupContactPhoneNumber { get; set; }
        public string PickupAddressLat { get; set; }
        public string PickupAddressLong { get; set; }
        public string PickupAddress { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuItemlPricesForEditDto : AuditedEntityDto
    {
        public string Name { get; set; }
        public int Multiplier { get; set; }
        public decimal Price { get; set; }
        public int ItemId { get; set; }
        public int TenantId { get; set; }
    }
}

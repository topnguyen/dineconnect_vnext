﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDynamicPageEditDto : Entity<int>
    {
        public string Name { get; set; }
        public string Html { get; set; }
        public string Css { get; set; }
        public string Assets { get; set; }
        public string Styles { get; set; }
        public string Components { get; set; }
    }
}

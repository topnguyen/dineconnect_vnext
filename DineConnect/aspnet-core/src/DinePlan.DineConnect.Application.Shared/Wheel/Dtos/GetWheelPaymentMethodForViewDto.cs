﻿using static DinePlan.DineConnect.Wheel.WheelPaymentMethodConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelPaymentMethodForViewDto
    {
        public string SystemName { get; set; }
        public string FriendlyName { get; set; }
        public string Logo { get; set; }
        public bool SupportsCapture { get; set; }
        public bool Refund { get; set; }
        public bool PartialRefund { get; set; }
        public bool Void { get; set; }
        public RecurringPaymentType RecurringSupport { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
        public dynamic DynamicFieldConfig { get; set; }

        public dynamic DynamicFieldData { get; set; }
    }
}
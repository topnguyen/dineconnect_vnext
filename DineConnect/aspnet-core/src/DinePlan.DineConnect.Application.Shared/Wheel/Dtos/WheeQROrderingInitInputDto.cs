﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheeQROrderingInitInputDto
    {
        public int DepartmentId { get; set; }

        public int LocationId { get; set; }

        public int TableId { get; set; }

        public string PhoneNumber { get; set; }

        public string SavedOTP { get; set; }
    }
}

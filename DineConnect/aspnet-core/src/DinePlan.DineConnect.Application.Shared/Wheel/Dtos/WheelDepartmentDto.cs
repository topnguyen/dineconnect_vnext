﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDepartmentDto : EntityDto
    {
        public string Name { get; set; }

        public int StartHour { get; set; }

        public int StartMinute { get; set; }

        public int EndHour { get; set; }

        public int EndMinute { get; set; }

        public string Days { get; set; }

        public WheelOrderType WheelOrderType { get; set; }

        public int? DepartmentId { get; set; }

        public int? PriceTagId { get; set; }

        public ICollection<WheelServiceFeeDto> WheelServiceFees { get; set; }
        public string WheelServiceFeeIds { get; set; }

        public ICollection<WheelPaymentMethodDto> WheelPaymentMethods { get; set; }
        public string WheelPaymentMethodIds { get; set; }

        public ICollection<WheelDeliveryDurationDto> WheelDeliveryDurations { get; set; }
        public string WheelDeliveryDurationIds { get; set; }
        public bool IsEnableTable { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelLocationForDeliveryZone
    {
        public string Name { get; set; }
        public string PickupAddressLat { get; set; }
        public string PickupAddressLong { get; set; }
        public string PickupAddress { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelTaxForDeliveryFeeDto
    {
        public int Id { get; set; }
        public string TaxName { get; set; }
        public decimal TaxPrecentage { get; set; }
    }
}
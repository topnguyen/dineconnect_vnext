﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelScreenMenuItemForEditInput : Entity<int>
    {
        public WheelScreenMenuItemType Type { get; set; }
    }
}

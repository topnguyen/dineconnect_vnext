﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelLocationsForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public int EnabledFilter { get; set; }

		public int TaxInclusiveFilter { get; set; }

		public int OutsideDeliveryZoneFilter { get; set; }

		public int WheelPostalCodeSearchTypeFilter { get; set; }


		 public string LocationNameFilter { get; set; }

		 		 public string ScreenMenuNameFilter { get; set; }

		 
    }
}
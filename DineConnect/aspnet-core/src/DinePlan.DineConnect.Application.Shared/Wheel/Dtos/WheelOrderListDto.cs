﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelOrderListDto
    {
        public WheelOrderListDtoOrderItem Order { get; set; }

        public WheelBillingInfo WheelBillingInfo { get; set; }

        public string PaymentOption { get; set; }

        public decimal? PaidAmount { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime OrderTime { get; set; }

        public WheelOrderStatus OrderStatus { get; set; }

        public string Note { get; set; }

        public decimal? DeliveryTime { get; set; }

        public DateTime? DateDelivery { get; set; }

        public string ActiveShippingDeliveryOrder { get; set; }

        public string ActiveShippingDeliveryOrderStatus { get; set; }

        public string ShippingProvider { get; set; }
    }

    public class WheelOrderListDtoOrderItem
    {
        public int? WheelTicketId { get; set; }

        public string DeliveryTime { get; set; }

        public decimal? Amount { get; set; }

        public int Quantity { get; set; } = 1;

        public string Address { get; set; }

        public int? CustomerAddressId { get; set; }
        
        public string AddOn { get; set; }

        public List<WheelOrderListMenuItemData> MenuItems { get; set; }

        public decimal? DeliveryCharge { get; set; }
        public int? SelfPickupLocationId { get; set; }
        public WheelDepartmentConsts.WheelOrderType WheelOrderType { get; set; }
        public int? PaymentId { get; set; }
        public string Note { get; set; }
        public DateTime OrderDate { get; set; }

        public string DeliveringTime { get; set; }

        public int? TableId { get; set; }

        public string TableName { get; set; }
    }

    public partial class WheelOrderListMenuItemData
    {
        public string Key { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Uri Image { get; set; }

        public long? ComboId { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal PricePerItem { get; set; }

        public long TotalItems { get; set; }

        public List<WheelOrderListOrderItem> OrderItems { get; set; }

        public List<WheelOrderListRecommendedItem> RecommendedItems { get; set; }

        public string Note { get; set; }

        public bool IsCombo { get; set; }
        
        public WheelOrderListMenuItemData()
        {

        }

        public WheelOrderListMenuItemData(WheelCartItemDto cartItem)
        {
            Key = cartItem.Key;
            ComboId = cartItem.IsCombo ? cartItem.ScreenMenuItemId : (long?)null;
            Name = cartItem.ScreenMenuItem?.Name;
            Description = cartItem.TagDescription;
            TotalPrice = cartItem.SubTotal ?? 0;
            PricePerItem = cartItem.UnitPrice ?? 0;
            TotalItems = cartItem.Quantity;
            Note = cartItem.Note;
            IsCombo = cartItem.IsCombo;
            OrderItems = new List<WheelOrderListOrderItem>();
        }
    }

    public class WheelOrderListOrderItem
    {
        public int Id { get; set; }

        public int? SectionComboId { get; set; }

        public string Name { get; set; }

        public object Image { get; set; }

        public string Description { get; set; }

        public decimal? TotalPrice { get; set; }

        public decimal? AddPrice { get; set; }

        public int Quantity { get; set; }

        public List<WheelCartItemOrderTagDto> Tags { get; set; }

        public WheelCartItemPortionDto Portion { get; set; }

        public WheelOrderListOrderItem(WheelCartItemDto cartItem)
        {
            Id = cartItem.ScreenMenuItemId;
            Name = cartItem.ScreenMenuItem?.Name;
            Description = cartItem.TagDescription;
            TotalPrice = cartItem.SubTotal ?? 0;
            AddPrice = cartItem.UnitPrice ?? 0;
            Quantity = cartItem.Quantity;
            Portion = cartItem.Portion;
            Tags = cartItem.Tags;
            IsCombo = cartItem.IsCombo;
        }

        public bool IsCombo { get; set; }
    }

    public partial class WheelOrderListRecommendedItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}

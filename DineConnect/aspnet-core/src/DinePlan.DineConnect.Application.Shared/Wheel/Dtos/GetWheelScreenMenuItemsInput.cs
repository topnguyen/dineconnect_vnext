﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelScreenMenuItemsInput : PagedAndSortedInputDto
    {
        public int? ParentSectionId { get; set; } 
        public string Filter { get; set; }
    }
}

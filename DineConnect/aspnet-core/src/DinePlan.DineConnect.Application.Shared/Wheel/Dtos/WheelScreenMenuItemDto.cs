﻿using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuItemDto : AuditedEntity
    {
        public string Name { get; set; }

        public string SubCategory { get; set; }

        public bool Published { get; set; }
        public int MenuItemId { get; set; }
        public string Image { get; set; }

        public string Video { get; set; }

        public decimal Price { get; set; }

        public int Order { get; set; }

        public WheelScreenMenuItemType Type { get; set; }

        public WheelDisplayAs DisplayAs { get; set; }

        public int NumberOfColumns { get; set; }

        public int? ParentId { get; set; }

        public int ScreenMenuId { get; set; }

        public List<WheelScreenMenuItemlPricesForEditDto> Portions { get; set; }

        public List<WheelOrderTagGroupListDto> OrderTags { get; set; }

        public List<WheelRecommendedForItemEditDto> RecommendedItems { get; set; }

        public string Calories { get; set; }

        public bool? DisplayNutritionFacts { get; set; }

        public string NutritionInfo { get; set; }

        public List<string> Tags { get; set; }

        public string Description { get; set; }

        //For Combo
        public string Dynamic { get; set; }

        public decimal BasePrice { get; set; }

        public int? ScreenCategoryId { get; set; }
        public bool? AllowPreOrder { get; set; }

        public virtual ICollection<WheelMenuComboGroupDto> WheelComboGroups { get; set; }
        
        //Sections
        public List<string> SubCategories { get; set; }
        public bool IsSoldOut{ get; set; }
    }

    public class WheelMenuComboGroupDto
    {
        public int Id { get; set; }
        public int MenuItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Dynamic { get; set; }

        public int? Maximum { get; set; }

        public int? Minimum { get; set; }

        public virtual int? WheelMenuComboId { get; set; }

        public virtual ICollection<WheelMenuComboItemDto> WheelComboItems { get; set; }
    }

    public class WheelScreenComboMenuItemDto : WheelScreenMenuItemDto
    {
        public int Quantity { get; set; }

        public bool AutoSelect { get; set; }

        public bool AddSeperately { get; set; }
    }

    public class WheelMenuComboItemDto: WheelScreenMenuItemDto
    {

        public virtual int? ScreenMenuItemId { get; set; }

        public int? WheelMenuComboItemId { get; set; }

        public int Quantity { get; set; }
        public int MaxQuantity { get; set; }
        public bool AutoSelect { get; set; }
        public bool AddSeperately { get; set; }

    }
}

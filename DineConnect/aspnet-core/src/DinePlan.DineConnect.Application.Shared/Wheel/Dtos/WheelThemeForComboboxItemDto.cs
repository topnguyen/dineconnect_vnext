﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelThemeForComboboxItemDto : ComboboxItemDto
    {
        public WheelThemeForComboboxItemDto(string value, string displayText, bool isSelected)
        {
            Value = value;
            DisplayText = displayText;
            IsSelected = isSelected;
        }
    }
}

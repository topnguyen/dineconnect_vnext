﻿using System;
using System.Collections.Generic;
using System.Text;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelOpeningHoursForExcelInput
    {
        public string Filter { get; set; }
        public WheelOpeningHourServiceType WheelOpeningHourServiceType { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelScreenMenusInput
    {
        public string Filter { get; set; }
        public int LocationId { get; set; }
    }
}

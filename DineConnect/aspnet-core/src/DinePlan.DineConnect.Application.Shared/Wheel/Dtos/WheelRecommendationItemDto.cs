﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelRecommendationItemDto
    {
        public int Id { get; set; }
        public int RecommendationId { get; set; }
        public WheelRecommendationMenuDto Recommendation { get; set; }
        public int MenuItemId { get; set; }
        public string MenuName { get; set; }
    }
}

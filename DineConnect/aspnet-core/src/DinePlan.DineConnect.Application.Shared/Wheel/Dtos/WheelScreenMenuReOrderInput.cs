﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuReOrderInput : Entity
    {
        public int Order { get; set; }

        public WheelScreenMenuItemType? Type { get; set; }
    }
}

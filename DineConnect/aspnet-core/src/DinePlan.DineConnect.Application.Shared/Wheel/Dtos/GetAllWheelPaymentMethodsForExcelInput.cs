﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelPaymentMethodsForExcelInput
    {
		public string Filter { get; set; }

		public string SystemNameFilter { get; set; }

		public string FriendlyNameFilter { get; set; }
    }
}
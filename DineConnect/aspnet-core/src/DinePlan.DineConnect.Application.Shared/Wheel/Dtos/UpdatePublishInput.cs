﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class UpdatePublishInput : EntityDto
    {
        public WheelScreenMenuItemType? Type { get; set; }

        public bool Publish { get; set; }
    }
}

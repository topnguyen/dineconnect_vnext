﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class DeleteWheelScreenMenuSectionInput: EntityDto
    {
        public WheelScreenMenuItemType Type { get; set; }
    }
}

﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenuItemForSection : Entity
    {
        public string Name { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelScreenMenusListDto : AuditedEntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Published { get; set; }
        public int ItemCount { get; set; }
        public int Order { get; set; }
    }
}

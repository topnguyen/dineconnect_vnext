﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelTaxWheelTaxLookupTableDto
    {
		public int Id { get; set; }

		public string DisplayName { get; set; }
    }
}
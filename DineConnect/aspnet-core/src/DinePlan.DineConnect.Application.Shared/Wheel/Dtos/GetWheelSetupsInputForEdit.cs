﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelSetupsInputForEdit : EntityDto
    {
        public GetWheelSetupsInputForEdit()
        {
            BaseUrl = null;
        }
        public string BaseUrl { get; set; }

    }
}
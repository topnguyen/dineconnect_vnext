﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetAllWheelDeliveryDurationsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public int? MaxTenantIdFilter { get; set; }
		public int? MinTenantIdFilter { get; set; }

		public decimal? MaxDeliveryDistanceFilter { get; set; }
		public decimal? MinDeliveryDistanceFilter { get; set; }

		public decimal? MaxDeliveryTimeFilter { get; set; }
		public decimal? MinDeliveryTimeFilter { get; set; }



    }
}
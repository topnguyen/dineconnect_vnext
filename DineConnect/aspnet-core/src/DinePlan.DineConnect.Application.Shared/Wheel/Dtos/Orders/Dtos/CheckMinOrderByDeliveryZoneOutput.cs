﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class CheckMinOrderByDeliveryZoneOutput
    {
        public bool IsValid { get; set; }

        public decimal? MinimuOrderTotal { get; set; }
    }
}

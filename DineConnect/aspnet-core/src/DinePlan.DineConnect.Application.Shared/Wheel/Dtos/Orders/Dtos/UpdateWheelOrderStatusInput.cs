﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class UpdateWheelOrderStatusInput
    {
        public string InvoiceNo { get; set; }

        public WheelOrderStatus Status { get; set; }
    }
}

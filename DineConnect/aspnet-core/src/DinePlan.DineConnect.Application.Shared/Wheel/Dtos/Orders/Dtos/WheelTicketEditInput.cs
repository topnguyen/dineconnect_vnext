﻿using System;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class WheelTicketEditInput
    {
        public WheelOrderType WheelOrderType { get; set; }

        public DateTime OrderDate { get; set; }

        public string Note { get; set; }
    }
}
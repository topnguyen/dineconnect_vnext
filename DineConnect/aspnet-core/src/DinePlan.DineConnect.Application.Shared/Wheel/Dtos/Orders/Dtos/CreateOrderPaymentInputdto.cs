﻿using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class CreateWheelOrderPaymentInputDto
    {
        public WheelOrderDto Order { get; set; }
        public WheelBillingInfo BillingInfo { get; set; }
        public WheelOrderTrackingDto WheelOrderTracking { get; set; }
        public string PaymentOption { get; set; }
        public decimal? PaidAmount { get; set; }
        public string CallbackUrl { get; set; }

        public string OmiseToken { get; set; }
        public string Source { get; set; }
        public string PaymentRequestId { get; set; }
        public long? UserId { get; set; }
        public List<PromotionCodeDiscountDetailPaymentDto> DiscountDetails { get; set; }
    }



    public class WheelTicketModel
    {
        public WheelOrderDto Order { get; set; }

        public WheelBillingInfo WheelBillingInfo { get; set; }

        public string PaymentOption { get; set; }

        public decimal? PaidAmount { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime OrderTime { get; set; }

        public string ShippingProviderResponseJson { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string Note { get; set; }
        public decimal? DeliveryTime { get; set; }
        public CartTotalDto AmountCalculations { get; set; }
    }

    public class PlaceOrderToShippingDto
    {
        public WheelOrderDto Order { get; set; }

        public WheelBillingInfo BillingInfo { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
	public class GeteDineOutputDto
	{
		public int Id { get; set; }
		public bool? IsClosed { get; set; }
		public DateTime? LastOrderTime { get; set; }
		public DateTime? LastPaymentTime { get; set; }
		public decimal? PaidAmount { get; set; }
		public decimal? Amount { get; set; }
		public string Note { get; set; }
		public bool IsTaxIncluded { get; set; }
		public string Status { get; set; }
		public List<eDineOrder> Orders { get; set; }
		public decimal? DeliveryCharge { get; set; }
		public decimal? DepartmentCharge { get; set; }
		public decimal? CardCharge { get; set; }
		public DateTime? CreationTime { get; set; }
		public string PaymentOption { get; set; }
		public GeteDineOutputDto()
		{
			Orders = new List<eDineOrder>();
		}

	}
	public class eDineOrder
	{
		public int Id { get; set; }
		public int TicketId { get; set; }
		public decimal? Amount { get; set; }
		public int ProductId { get; set; }
		//public List<TaxObj> Tax_obj { get; set; }
		public ScreenMenuObj ScreenMenu { get; set; }
		public string CostPrice { get; set; }
		public decimal? Price { get; set; }
		public object UserObj { get; set; }
		public decimal? Quantity { get; set; }
		public string AddOnPrice { get; set; }
		public string Remarks { get; set; }
	}
	public class ScreenMenuObj
	{

		public int? ScreenCategoryId { get; set; }

		public int? ScreenMenuId { get; set; }

		public string Name { get; set; }

		public string Dynamic { get; set; }

		public virtual MenuItemeDine MenuItem { get; set; }

		public virtual int? MenuItemId { get; set; }

		public virtual int? ProductComboId { get; set; }
	}

	public class MenuItemeDine
	{
		public virtual string Name { get; set; }

		public virtual string BarCode { get; set; }

		public virtual string AliasCode { get; set; }

		public virtual string AliasName { get; set; }

		public virtual string ItemDescription { get; set; }

		public virtual bool ForceQuantity { get; set; }
		public virtual bool ForceChangePrice { get; set; }
		public virtual bool RestrictPromotion { get; set; }

		public virtual bool NoTax { get; set; }

		public int ProductType { get; set; }

		public int CategoryId { get; set; }

		//public ICollection<LocationMenuItem> LocationMenuItems { get; set; }
		public virtual ICollection<MenuItemPortioneDine> Portions { get; set; }
		public virtual ICollection<UpMenuItemeDine> UpMenuItems { get; set; }
		public virtual string Tag { get; set; }

		public virtual ICollection<MenuBarCodeeDine> Barcodes { get; set; }

		public virtual TransactionTypyeDine TransactionType { get; set; }

		public virtual int? TransactionTypeId { get; set; }
		public string Files { get; set; }
		public virtual string Uom { get; set; }
		public virtual int FoodType { get; set; }
		public virtual string AddOns { get; set; }
		public virtual string Images { get; set; }

		public string NutritionInfo { get; set; }

		public virtual string HsnCode { get; set; }

		public virtual Guid? DownloadImage { get; set; }
	}

	public class TransactionTypyeDine
	{
		public virtual string Name { get; set; }

		public virtual bool Tax { get; set; }

		public virtual bool Discount { get; set; }

		public virtual string AccountCode { get; set; }
	}

	public class MenuItemPortioneDine
	{
		public virtual string Name { get; set; }
		public virtual string Barcode { get; set; }
		public virtual MenuItemeDine MenuItem { get; set; }

		public virtual int MenuItemId { get; set; }
		public virtual int MultiPlier { get; set; }
		public virtual decimal Price { get; set; }
	}

	public class UpMenuItemeDine
	{
		public virtual int MenuItemId { get; set; }
		public virtual int RefMenuItemId { get; set; }
		public virtual bool AddBaseProductPrice { get; set; }
		public virtual decimal Price { get; set; }
		public virtual int ProductType { get; set; }
		public int MinimumQuantity { get; set; }
		public int MaxQty { get; set; }
		public bool AddAuto { get; set; }
		public int AddQuantity { get; set; }
	}

	public class MenuBarCodeeDine
	{
		public virtual string Barcode { get; set; }
		public virtual int MenuItemId { get; set; }
	}


}

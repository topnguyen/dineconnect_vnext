namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class WheelOrderTagDto
    {
        public int? Id { get; set; }
        
        public string Name { get; set; }
        
        public string AlternateName { get; set; }
        
        public decimal Price { get; set; }
        
        public int? MenuItemId { get; set; }
        
        public int MaxQuantity { get; set; }
        
        public int SortOrder { get; set; }
        
        public int OrderTagGroupId { get; set; }
        
        public bool Checked { get; set; }

        public int Quantity { get; set; } = 1;
    }
}
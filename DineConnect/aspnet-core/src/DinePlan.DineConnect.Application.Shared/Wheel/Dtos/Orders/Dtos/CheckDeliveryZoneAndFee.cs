﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class CheckDeliveryZoneAndFeeOuput
    {
        public bool OutOfAllDeliveryZones { get; set; }

        public bool AllowOutOfDeliveryZones { get; set; }

        public decimal? DeliveryFee { get; set; }
    }
}

using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class WheelOrderTagGroupListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int MaxSelectedItems { get; set; }
        public int MinSelectedItems { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
        public List<WheelOrderTagDto> Tags { get; set; }
       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class CheckDeliveryZoneAndFeeInput
    {
        public string PostalCode { get; set; }

        public int LocationId { get; set; }

        public decimal TotalPrice { get; set; }
    }
}

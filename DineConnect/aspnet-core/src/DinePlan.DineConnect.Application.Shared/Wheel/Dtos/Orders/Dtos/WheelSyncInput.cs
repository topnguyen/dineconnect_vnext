﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class WheelSyncInput
    {
        public int TenantId { get; set; }

        public List<int> WheelTicketIds { get; set; }
    }
}
﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class GetWheelOrdersInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public WheelOrderStatus? OrderStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CustomerId { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
}
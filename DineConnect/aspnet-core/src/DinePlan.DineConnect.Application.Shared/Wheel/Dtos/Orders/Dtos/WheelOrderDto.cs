using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos
{
    public class WheelOrderDto
    {
        public int? WheelTicketId { get; set; }

        public string DeliveryTime { get; set; }

        public decimal? Amount { get; set; }

        public decimal? PaidAmount { get; set; }

        public int Quantity { get; set; } = 1;

        public string Address { get; set; }

        public string AddOn { get; set; }

        public int? CustomerAddressId { get; set; }

        public List<WheelCartItemDto> OrderItems { get; set; }

        public WheelDepartmentConsts.WheelOrderType WheelOrderType { get; set; }

        public decimal? DeliveryCharge { get; set; }

        public int? SelfPickupLocationId { get; set; }

        public int? PaymentId { get; set; }

        public int? TimeSlotId { get; set; }

        public string Note { get; set; }

        public DateTime? OrderDate { get; set; }

        public decimal? DiscountAmount { get; set; }

        public string DeliveringTime { get; set; }

        public int? TableId { get; set; }

        public string TableName { get; set; }

        public int WheelLocationId { get; set; }
    }
}
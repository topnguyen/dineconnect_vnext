﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelRecommendationDayPeriodDto
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public int OpenDays { get; set; }
        public int RecommendationId { get; set; }
        public WheelRecommendationMenuDto Recommendation { get; set; }
    }
}

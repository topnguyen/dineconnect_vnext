﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelScreenMenuInput : Entity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Published { get; set; }
    }
}

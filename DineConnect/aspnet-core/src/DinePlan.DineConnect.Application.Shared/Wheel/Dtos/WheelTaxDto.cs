﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelTaxDto : EntityDto
    {
        public string Name { get; set; }

        public int? DinePlanTaxId { get; set; }

        public decimal TaxPercent { get; set; }
    }
}
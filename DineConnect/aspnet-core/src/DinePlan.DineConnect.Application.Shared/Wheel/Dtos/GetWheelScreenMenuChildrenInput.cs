﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetWheelScreenMenuChildrenInput : Entity<int>
    {
        public string Filter { get; set; }
        public bool IsGetClient { get; set; }
    }
    
    public class GetWheelScreenMenuItemInput 
    {
        public List<int> Ids { get; set; }
        
    }
}

﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class GetTaxForMenuItemInput
    {
        public List<int> Ids { get; set; }
        public int LocationId { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class TimeSlotDto : EntityDto
    {
        public int? TenantId { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public int? NumberOfOrder { get; set; }
        public int? WheelDepartmentId { get; set; }

        public bool IsOpened { get; set; }
    }
}
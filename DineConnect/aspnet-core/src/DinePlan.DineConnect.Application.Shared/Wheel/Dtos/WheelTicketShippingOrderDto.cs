﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Settings;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelTicketShippingOrderDto : EntityDto
    {
        public string ShippingOrderStatus { get; set; }
        public ShippingProvider ShippingProviderName { get; set; } //save gogovan or lalamove
        public string ShippingOrderAmount { get; set; } //type json
        public string ShippingOrderId { get; set; } //= PlaceOrderResponse.CustomerOrderId
        public int? WheelTicketId { get; set; }
    }
}
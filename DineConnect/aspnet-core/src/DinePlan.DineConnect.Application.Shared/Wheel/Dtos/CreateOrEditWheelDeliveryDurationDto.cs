﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class CreateOrEditWheelDeliveryDurationDto : EntityDto<int?>
    {

		public int TenantId { get; set; }
		
		
		public decimal DeliveryDistance { get; set; }
		
		
		public decimal DeliveryTime { get; set; }
		
		

    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelDynamicPagesListDto : EntityDto<int>
    {
        public string Name { get; set; }
        public string Html { get; set; }
        public string Css { get; set; }
        public string Assets { get; set; }
        public string Styles { get; set; }
        public string Components { get; set; }
    }
}

﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public enum WheelServiceFeeType
    {
        Percentage,
        Currency
    }

    public enum WheelFeeOrderType
    {
        AllOrders,
        AllOrdersWithDiscount,
        OrdersWithDays,
        OrdersWithPaymentMethod,
        OrderWithType
    }

    public class WheelServiceFeeDto : EntityDto<int>
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public WheelServiceFeeType FeeType { get; set; }
        public decimal Value { get; set; }
        public bool ApplyOnSubTotal { get; set; }
        public bool CalculateTax { get; set; }
        public WheelFeeOrderType AppliedOrderType { get; set; }
        public string AppliedOrderInfo { get; set; }
        public string WheelTaxes { get; set; }
        public Dictionary<int, decimal> ServiceTaxes { get; set; }
        public WheelServiceFeeDto()
        {
            ServiceTaxes = new Dictionary<int, decimal>();
        }
    }
}
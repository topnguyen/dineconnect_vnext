﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelPaymentMethodDto : EntityDto
    {
		public string Name { get; set; }


		 public int? PaymentTypeId { get; set; }

		 
    }
}
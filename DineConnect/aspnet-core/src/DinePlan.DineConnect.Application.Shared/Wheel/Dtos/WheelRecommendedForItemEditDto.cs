﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class WheelRecommendedForItemEditDto
    {
        // public int ItemId { get; set; }
        public int RecommendedItemId { get; set; }
        public string RecommendedItemName { get; set; }
        // public int TenantId { get; set; }
    }
}

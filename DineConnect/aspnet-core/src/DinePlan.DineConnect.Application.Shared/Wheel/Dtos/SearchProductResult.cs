﻿namespace DinePlan.DineConnect.Wheel.Dtos
{
    public class SearchProductResult
    {
        public int CategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string SectionId { get; set; }
    }
}

using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Wheel.Dtos.WheelTable;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelTableAppService : IApplicationService
    {
        Task<DineUrlObject> TableUrl(DineUrlObjectInput entity);
        Task<DineUrlObject> ChangeTableStatus(DineUrlObjectInput entity);
    }
}
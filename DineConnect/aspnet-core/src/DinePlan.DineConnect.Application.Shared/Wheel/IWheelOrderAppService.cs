using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.PaymentProcessor.Dto.HiPayPaymentDto;
using DinePlan.DineConnect.Shipping;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelOrderAppService : IApplicationService
    {
        Task<PaymentOutputDto> CreatePaymentForOrder(CreateWheelOrderPaymentInputDto input);

        Task<PaymentOutputDto> ValidatePayment(EntityDto<int> input);

        Task<bool> CheckSlotOrder(WheelOrderTrackingDto input);

        Task<CheckDeliveryZoneAndFeeOuput> CheckDeliveryZone(CheckDeliveryZoneAndFeeInput input);

        Task<bool> IsOpeningHour(long wheelDepartmentId);

        Task<DateTime> GetDefaultDateFuture(long wheelDepartmentId);

        Task<decimal> CalculateDeliveryTime(int locationId, double customerAddressLng, double customerAddressLat);

        Task<int> GetPageGoToBack(GetPageInput input);
    }
}
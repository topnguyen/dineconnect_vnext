﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;

namespace DinePlan.DineConnect.Wheel.Promotion
{
    public interface IWheelPromotionAppService: ITiffinPromotionAppService
    {
    }
}
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class CalculatePromotionDiscountPaymentInputDto
    {
        public double PaidAmount { get; set; }

        public List<string> Codes { get; set; }
    }
}
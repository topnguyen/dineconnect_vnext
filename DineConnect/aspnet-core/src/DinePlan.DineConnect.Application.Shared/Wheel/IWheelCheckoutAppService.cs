﻿using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelCheckoutAppService
    {
        Task<int> AddMenuItemToCart(AddItemToCartInputDto input);

        Task<WheelGetCartOutputDto> GetCartItems(bool includeCartTotal = false);

        Task RemoveItemFromCart(int screenMenuItemId, string key);

        Task ResetCheckout();
    }
}

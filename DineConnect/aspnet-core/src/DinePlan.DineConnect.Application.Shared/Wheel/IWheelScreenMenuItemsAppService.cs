﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelScreenMenuItemsAppService : IApplicationService
    {
        Task<List<WheelScreenMenuItemDto>> GetChildren(GetWheelScreenMenuChildrenInput input);
        Task<WheelScreenMenuItemForEditDto> GetWheelScreenMenuItemForEditSection(GetWheelScreenMenuItemForEditInput input);
        Task CreateOrEditWheelScreenMenuSection(CreateOrEditWheelScreenMenuItemInput input);
        Task DeleteWheelScreenMenuSection(DeleteWheelScreenMenuSectionInput input);
        Task<PagedResultDto<WheelScreenMenuItemForSection>> GetWheelScreenMenuItemForSelection(GetWheelScreenMenuItemForSelection input);
        Task UpdatePublish(UpdatePublishInput input);
        Task CreateWheelScreenMenuSectionFromMenuItem(long menuItemId);
        Task CreateWheelScreenMenuSectionFromScreenMenuCategory(int wheelScreenMenuId, int screenMenuId);
    }
}

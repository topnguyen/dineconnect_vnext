﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Collections.Generic;


namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelLocationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetWheelLocationForViewDto>> GetAll(GetAllWheelLocationsInput input);

		Task<GetWheelLocationForEditOutput> GetWheelLocationForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditWheelLocationDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetWheelLocationsToExcel(GetAllWheelLocationsForExcelInput input);

		
		Task<List<WheelLocationLookupTableDto>> GetAllLocationForTableDropdown();
		
		Task<List<WheelLocationScreenMenuLookupTableDto>> GetAllScreenMenuForTableDropdown();

		Task<List<WheelLocationForDeliveryZone>> GetAllWheelLocationForDeliveryZone();

	}
}
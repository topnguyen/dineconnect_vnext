﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelDeliveryDurationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetWheelDeliveryDurationForViewDto>> GetAll(GetAllWheelDeliveryDurationsInput input);

        Task<GetWheelDeliveryDurationForViewDto> GetWheelDeliveryDurationForView(int id);

		Task<GetWheelDeliveryDurationForEditOutput> GetWheelDeliveryDurationForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditWheelDeliveryDurationDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetWheelDeliveryDurationsToExcel(GetAllWheelDeliveryDurationsForExcelInput input);

		
    }
}
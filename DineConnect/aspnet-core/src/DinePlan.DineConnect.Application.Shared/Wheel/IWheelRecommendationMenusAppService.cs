﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelRecommendationMenusAppService : IApplicationService
    {
        Task<PagedResultDto<WheelRecommendationMenuDto>> GetAll(GetWheelScreenMenusInput input);
        Task<int> CreateOrEditWheelRecommendationMenu(WheelRecommendationMenuDto input);
        Task<WheelRecommendationMenuDto> GetWheelRecommendationMenuForEdit(EntityDto input);
        Task<WheelRecommendationDayPeriodDto> GetDayPeriodById(EntityDto input);
        Task<int> CreateOrEditDayPeriod(WheelRecommendationDayPeriodDto input);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelDepartmentsAppService : IApplicationService
    {
        Task<PagedResultDto<GetWheelDepartmentForViewDto>> GetAll(GetAllWheelDepartmentsInput input);

        Task<CreateOrEditWheelDepartmentDto> GetWheelDepartmentForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditWheelDepartmentDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetWheelDepartmentsToExcel(GetAllWheelDepartmentsForExcelInput input);

        Task<List<WheelDepartmentPriceTagLookupTableDto>> GetAllPriceTagForTableDropdown();

        Task<List<GetWheelDepartmentForViewDto>> GetAllWheelDepartments();

        Task<List<GetWheelPaymentMethodForViewDto>> GetListPaymentMethodFromSystemNames(
            string systemNames);

        Task CreateOrEditTimeSlot(TimeSlotDto input);

        Task DeleteTimeSlot(int id);

        Task<List<TimeSlotDto>> GetTimeSlotByDepartmentId(int departmentId);

        Task<TimeSlotDto> GetTimeSlotById(int id);
        [AbpAllowAnonymous]
        Task<List<GetWheelDepartmentOutput>> GetWheelDepartments();
    }
}
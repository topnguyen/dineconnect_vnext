﻿using Abp.Application.Services;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelCustomizationSettingsAppService : IApplicationService
    {
        Task<List<ThemeSettingsDto>> GetUiManagementSettings();

        Task<ThemeSettingsDto> GetUiManagementSetting(string themeId);

        Task UpdateUiManagementSettings(ThemeSettingsDto settings);

        Task<List<WheelThemeForComboboxItemDto>> GetWheelThemeForCombobox(string selected);
    }
}

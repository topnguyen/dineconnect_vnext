﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelAppService : IApplicationService
    {
        Task<IEnumerable<WheelDeliveryZoneDto>> GetAllDeliveryZones();

        Task<WheelDeliveryZoneDto> AddZone(WheelDeliveryZoneDto zone);

        Task UpdateZone(WheelDeliveryZoneDto zoneDto);

        Task DeleteZone(int zoneId);

        Task<List<WheelDeliveryZoneDto>> GetDeliveryZonesContainedPoint(double longitude, double latitude);


        Task<PagedResultDto<WheelOpeningHourDto>> GetAllOpeningHours(GetAllWheelOpeningHoursInput input);

        Task CreateOrEditOpeningHour(WheelOpeningHourDto openingHourDto);

        Task<WheelOpeningHourDto> GetOpeningHour(EntityDto input);
        
        Task DeleteOpeningHour(int id);
      
        Task DeleteOpeningHoursByServiceType(WheelOpeningHourServiceType serviceType);
      
        Task<List<WheelOpeningHourForComboboxItemDto>> GetOpeningHourForComboboxByServiceType(WheelOpeningHourServiceType serviceType, int? selectedId = null);
       
        Task<FileDto> GetWheelOpeningHoursToExcel(GetAllWheelOpeningHoursForExcelInput input);

        Task<IEnumerable<WheelServiceFeeDto>> GetAllWheelServiceFees();
     
        Task<WheelServiceFeeDto> AddWheelServiceFee(WheelServiceFeeDto feeDto);
     
        Task UpdateWheelServiceFee(WheelServiceFeeDto feeDto);
      
        Task DeleteWheelServiceFee(int id);
    }
}

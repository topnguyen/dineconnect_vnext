﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelDynamicPagesAppService : IApplicationService
    {
        Task<PagedResultDto<WheelDynamicPagesListDto>> GetWheelDynamicPages(GetWheelDynamicPagesInput input);
        Task CreateOrUpdateWheelDynamicPage(CreateOrEditWheelDynamicPagesInput input);
        Task<WheelDynamicPageEditDto> GetWheelDynamicPageForEdit(GetWheelDynamicPageForEditInput input);
        Task DeleteWheelDynamicPage(EntityDto input);
        Task<List<WheelDynamicPageForComboboxItemDto>> GetDynamicPageForCombobox(int? selectedId = null);

        Task<WheelDynamicPageEditDto> GetDynamicPageTermOrPrivacy(WheelDynamicPage key);
    }
}

﻿using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.EmailNotification
{
    public interface IWheelEmailNotificationBackgroundJobService
    {
        Task SendEmailWhenCustomerOrderInBackground(List<CreateOrderInput> orderInputs,CompanySettingDto companyInfo);
    }
}

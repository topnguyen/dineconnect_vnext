﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.EmailNotification.Dtos
{
    public class WheelCustomerAddressSendEmail
    {
        public string CompanyAddress { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string PostcalCode { get; set; }
        public string Zone { get; set; }
        public string MobileNumber { get; set; }
    }
}

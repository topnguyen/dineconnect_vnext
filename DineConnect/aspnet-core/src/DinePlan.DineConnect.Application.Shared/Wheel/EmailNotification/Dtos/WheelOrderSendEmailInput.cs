﻿using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Wheel.EmailNotification.Dtos
{
    public class WheelOrderSendEmailInput
    {
        public WheelBillingInfo DeliveryAddress { get; set; }

        public List<WheelCartItemDto> OrderItems { get; set; }

        public WheelTicketDto Order { get; set; }

        public int TenantId { get; set; }
    }
}

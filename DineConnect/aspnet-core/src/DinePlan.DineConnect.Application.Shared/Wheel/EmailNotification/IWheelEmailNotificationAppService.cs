﻿using DinePlan.DineConnect.Wheel.Dtos.Orders.Dtos;
using DinePlan.DineConnect.Wheel.EmailNotification.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.EmailNotification
{
    public interface IWheelEmailNotificationAppService
    {
        Task SendEmailWhenCustomerOrder(WheelOrderSendEmailInput input);
    }
}

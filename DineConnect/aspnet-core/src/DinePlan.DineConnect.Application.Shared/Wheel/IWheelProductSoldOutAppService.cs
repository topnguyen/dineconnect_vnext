﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelProductSoldOutAppService : IApplicationService
    {
        Task<PagedResultDto<WheelProductSoldOutListDto>> GetAll(GetWheelScreenMenusInput input);
        Task<int> CreateOrEditWheelProductSoldOut(WheelProductSoldOutListDto input);
    }
}

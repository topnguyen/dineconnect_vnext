﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Wheel.WheelDashboard
{
    public class SaleByHourDashboardOutputDto
    {
        public int Hours { get; set; }
        public int Tickets { get; set; }

        public decimal TotalSale { get; set; }
    }

    public class ChartSaleByHourOutput
    {
        public List<string> Labels { get; set; }
        public List<Datasets> Datasets { get; set; }

        public ChartSaleByHourOutput()
        {
            Labels = new List<string>();
            Datasets = new List<Datasets>();
        }
    }

    public class Datasets
    {
        public string Label { get; set; }
        public List<int> Data { get; set; }

        public bool Fill { get; set; }
        public string BorderColor { get; set; }

        public Datasets()
        {
           
            Data = new List<int>();
        }
    }
}
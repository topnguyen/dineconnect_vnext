﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.WheelDashboard
{
    public class WheelOrderDashboardOutputDto
    {
        public int? TotalOrders { get; set; }
        public decimal? Totalsales { get; set; }
    }
}

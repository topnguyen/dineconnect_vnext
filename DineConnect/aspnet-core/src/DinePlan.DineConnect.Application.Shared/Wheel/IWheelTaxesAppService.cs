﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;


namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelTaxesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetWheelTaxForViewDto>> GetAll(GetAllWheelTaxesInput input);
        Task<GetWheelTaxForViewDto> GetWheelTaxForView(int id);
		Task<GetWheelTaxForEditOutput> GetWheelTaxForEdit(EntityDto input);
		Task CreateOrEdit(CreateOrEditWheelTaxDto input);
		Task Delete(EntityDto input);
		Task<FileDto> GetWheelTaxesToExcel(GetAllWheelTaxesForExcelInput input);
    }
}
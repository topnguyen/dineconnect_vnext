﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.Authorization;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelSetupsAppService : IApplicationService 
    {
		Task<GetWheelSetupForEditOutput> GetWheelSetupForEdit(GetWheelSetupsInputForEdit input);

		Task CreateOrEdit(CreateOrEditWheelSetupDto input);

        Task<PagedResultDto<GetWheelSetupForViewDto>> GetAll(GetAllWheelSetupsInput input);

        Task Delete(EntityDto input);

        [AbpAllowAnonymous]
        Task<ListResultDto<WheelLocationDto>> GetWheelSetupLocationsByTenant();
    }
}
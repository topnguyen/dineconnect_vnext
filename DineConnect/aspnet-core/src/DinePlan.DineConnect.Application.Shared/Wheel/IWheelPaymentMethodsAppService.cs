﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;


namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelPaymentMethodsAppService : IApplicationService
    {
        Task<ListResultDto<GetWheelPaymentMethodForViewDto>> GetAll(GetAllWheelPaymentMethodsInput input);

        Task<GetWheelPaymentMethodForEditOutput> GetWheelPaymentMethodForEdit(string systemName);

        Task Edit(EditWheelPaymentMethodDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetWheelPaymentMethodsToExcel(GetAllWheelPaymentMethodsForExcelInput input);

        Task<List<WheelPaymentMethodForComboboxDto>> GetAllPaymentMethodForCombobox(int? selectedId);
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Wheel.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelScreenMenusAppService : IApplicationService
    {
        Task<PagedResultDto<WheelScreenMenusListDto>> GetAll(GetWheelScreenMenusInput input);
        Task<WheelScreenMenuForEditDto> GetWheelScreenMenuForEdit(GetWheelScreenMenuForEditInput input);
        Task<int> CreateOrEditWheelScreenMenu(CreateOrEditWheelScreenMenuInput input);
        Task DeleteWheelScreenMenu(EntityDto input);
        Task<List<WheelScreenMenuItemDto>> GetChildren(GetWheelScreenMenuChildrenInput input);
        Task Reorder(List<WheelScreenMenuReOrderInput> inputs);
        Task<List<WheelScreenMenuForComboboxItemDto>> GetScreenMenuForCombobox(int? selectedId);
        Task UpdatePublish(UpdatePublishInput input);
    }
}

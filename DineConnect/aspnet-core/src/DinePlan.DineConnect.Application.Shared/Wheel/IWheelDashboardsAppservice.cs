﻿using DinePlan.DineConnect.Connect.Reports.Dtos;
using DinePlan.DineConnect.Wheel.WheelDashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel
{
    public interface IWheelDashboardsAppservice
    {
        Task<WheelOrderDashboardOutputDto> GetWheelOrderDashboard(GetChartInput input);
        Task<ChartSaleByHourOutput> GetSaleByHoursDashboard(GetChartInput input);
    }
}

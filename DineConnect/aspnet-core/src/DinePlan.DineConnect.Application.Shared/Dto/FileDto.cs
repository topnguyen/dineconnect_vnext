﻿using Abp.Timing;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Dto
{
    public class FileDto
    {
        [Required]
        public string FileName { get; set; }

        public string FileType { get; set; }

        [Required]
        public string FileToken { get; set; }

        public int Length { get; set; }
        public DateTime? Date { get; set; }

        public FileDto()
        {
        }

        public FileDto(string fileName, string fileType)
        {
            FileName = fileName;
            FileType = fileType;
            FileToken = Guid.NewGuid().ToString("N");
            Date = Clock.Now;
        }
    }
}
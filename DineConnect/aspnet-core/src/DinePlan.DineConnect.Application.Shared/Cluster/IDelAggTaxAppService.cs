﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggTaxAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggTaxListDto>> GetAll(GetDelAggTaxInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggTaxInput input);
        Task<GetDelAggTaxForEditOutput> GetDelAggTaxForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggTax(CreateOrUpdateDelAggTaxInput input);
        Task DeleteDelAggTax(EntityDto input);
        Task<ListResultDto<ComboboxItemDto>> GetDelAggTaxForCombobox();
        Task<ListResultDto<ComboboxItemDto>> GetDelAggTaxTypeForCombobox();
    }
}

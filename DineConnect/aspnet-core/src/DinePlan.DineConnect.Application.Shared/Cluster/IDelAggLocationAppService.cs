﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggLocationListDto>> GetAll(GetDelAggLocationInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDelAggLocationForEditOutput> GetDelAggLocationForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggLocation(CreateOrUpdateDelAggLocationInput input);
        Task DeleteDelAggLocation(EntityDto input);
        Task<ListResultDto<DelAggLocationListDto>> GetNames();
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocMappingAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggLocMappingListDto>> GetAll(GetDelAggLocMappingInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocMappingInput input);
        Task<GetDelAggLocMappingForEditOutput> GetDelAggLocMappingForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggLocMapping(CreateOrUpdateDelAggLocMappingInput input);
        Task DeleteDelAggLocMapping(EntityDto input);
        Task<ListResultDto<ComboboxItemDto>> GetDelAggTypeForCombobox();
        Task<ListResultDto<ComboboxItemDto>> GetDelAggImageTypeForCombobox();
        Task<ListResultDto<ComboboxItemDto>> GetDelPriceTypeForCombobox();
        Task<ListResultDto<DelAggLocationListDto>> GetAggLocationNames();
        Task<ListResultDto<DelAggLocMappingListDto>> GetAggLocationMapNames();
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationItemAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggLocationItemListDto>> GetAll(GetDelAggLocationItemInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocationItemInput input);
        Task<GetDelAggLocationItemForEditOutput> GetDelAggLocationItemForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggLocationItem(CreateOrUpdateDelAggLocationItemInput input);
        Task DeleteDelAggLocationItem(EntityDto input);
    }
}
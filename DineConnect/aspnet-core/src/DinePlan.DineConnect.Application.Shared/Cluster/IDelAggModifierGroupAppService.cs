﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggModifierGroupAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggModifierGroupListDto>> GetAll(GetDelAggModifierGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggModifierGroupInput input);
        Task<GetDelAggModifierGroupForEditOutput> GetDelAggModifierGroupForEdit(EntityDto EntityDto);
        Task<int> CreateOrUpdateDelAggModifierGroup(CreateOrUpdateDelAggModifierGroupInput input);
        Task DeleteDelAggModifierGroup(EntityDto input);
        Task<ListResultDto<DelAggModifierGroupListDto>> GetNames();
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggCategoryAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggCategoryListDto>> GetAll(GetDelAggCategoryInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggCategoryInput input);
        Task<GetDelAggCategoryForEditOutput> GetDelAggCategoryForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggCategory(CreateOrUpdateDelAggCategoryInput input);
        Task DeleteDelAggCategory(EntityDto input);
        Task<ListResultDto<DelAggCategoryListDto>> GetNames();
        Task<PagedResultDto<DelAggCategoryListDto>> GetDelAggCategories(GetCategoryInput input);
        Task SaveSortDelAggCategories(int[] categories);
        Task<GetItemByCategoryOuput> GetItemForVisualScreen(GetCategoryInput input);
    }
}
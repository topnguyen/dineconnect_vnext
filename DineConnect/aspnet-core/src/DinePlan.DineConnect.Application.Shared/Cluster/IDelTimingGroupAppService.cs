﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelTimingGroupAppService : IApplicationService
    {
        Task<PagedResultDto<DelTimingGroupListDto>> GetAll(GetDelTimingGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelTimingGroupInput input);
        Task<GetDelTimingGroupForEditOutput> GetDelTimingGroupForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelTimingGroup(CreateOrUpdateDelTimingGroupInput input);
        Task DeleteDelTimingGroup(EntityDto input);
        Task<ListResultDto<DelTimingGroupListDto>> GetTimingGroupNames();
    }
}
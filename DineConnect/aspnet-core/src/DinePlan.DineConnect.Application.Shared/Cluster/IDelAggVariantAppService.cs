﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggVariantAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggVariantListDto>> GetAll(GetDelAggVariantInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggVariantInput input);
        Task<GetDelAggVariantForEditOutput> GetDelAggVariantForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggVariant(CreateOrUpdateDelAggVariantInput input);
        Task DeleteDelAggVariant(EntityDto input);
        Task<ListResultDto<DelAggVariantListDto>> GetNames();
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggVariantGroupAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggVariantGroupListDto>> GetAll(GetDelAggVariantGroupInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDelAggVariantGroupForEditOutput> GetDelAggVariantGroupForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggVariantGroup(CreateOrUpdateDelAggVariantGroupInput input);
        Task DeleteDelAggVariantGroup(EntityDto input);
        Task<ListResultDto<DelAggVariantGroupListDto>> GetNames();
    }
}
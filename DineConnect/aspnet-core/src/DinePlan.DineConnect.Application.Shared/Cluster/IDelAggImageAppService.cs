﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggImageAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggImageListDto>> GetAll(GetDelAggImageInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggImageInput input);
        Task<GetDelAggImageForEditOutput> GetDelAggImageForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggImage(CreateOrUpdateDelAggImageInput input);
        Task DeleteDelAggImage(EntityDto input);
    }
}
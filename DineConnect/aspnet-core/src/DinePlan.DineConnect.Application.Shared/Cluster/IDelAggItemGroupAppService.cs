﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggItemGroupAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggItemGroupListDto>> GetAll(GetDelAggItemGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggItemGroupInput input);
        Task<GetDelAggItemGroupForEditOutput> GetDelAggItemGroupForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggItemGroup(CreateOrUpdateDelAggItemGroupInput input);
        Task DeleteDelAggItemGroup(EntityDto input);
        Task<ListResultDto<DelAggItemGroupListDto>> GetNames();
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLanguageAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggLanguageListDto>> GetAll(GetDelAggLanguageInput inputDto);
        Task<GetDelAggLanguageForEditOutput> GetDelAggLanguageForEdit(EntityDto EntityDto);
        Task<EntityDto> CreateOrUpdateDelAggLanguage(CreateOrUpdateDelAggLanguageInput input);
        Task DeleteDelAggLanguage(EntityDto input);

        Task<ListResultDto<ComboboxItemDto>> GetLanguageDescriptionTypes();



    }
}
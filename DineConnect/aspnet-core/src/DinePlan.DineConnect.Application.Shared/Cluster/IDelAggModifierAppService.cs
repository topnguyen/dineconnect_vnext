﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggModifierAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggModifierListDto>> GetAll(GetDelAggModifierInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggModifierInput input);
        Task<GetDelAggModifierForEditOutput> GetDelAggModifierForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggModifier(CreateOrUpdateDelAggModifierInput input);
        Task DeleteDelAggModifier(EntityDto input);
        Task<ListResultDto<DelAggModifierListDto>> GetNames();
    }
}
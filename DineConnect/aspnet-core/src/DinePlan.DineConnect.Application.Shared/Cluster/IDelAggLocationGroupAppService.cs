﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationGroupAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggLocationGroupListDto>> GetAll(GetDelAggLocationGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocationGroupInput input);
        Task<GetDelAggLocationGroupForEditOutput> GetDelAggLocationGroupForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input);
        Task DeleteDelAggLocationGroup(EntityDto input);
        Task<ListResultDto<DelAggLocationGroupListDto>> GetNames();
    }
}
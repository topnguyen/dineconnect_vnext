﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggVariantListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggVariant Properties Missing
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int DelAggVariantGroupId { get; set; }
        public string DelAggVariantGroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string MenuItemPortionName { get; set; }
        public virtual decimal SalesPrice { get; set; }
        public virtual decimal MarkupPrice { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        public string NestedModGroupName { get; set; }
    }
    public class DelAggVariantEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggVariant Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int DelAggVariantGroupId { get; set; }
        public string DelAggVariantGroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string MenuItemPortionName { get; set; }
        public virtual decimal SalesPrice { get; set; }
        public virtual decimal MarkupPrice { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        public string NestedModGroupName { get; set; }
        public virtual int TenantId { get; set; }
    }

    public class GetDelAggVariantInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? DelAggVariantGroupId { get; set; }
        public int? MenuItemPortionId { get; set; }
        public int? NestedModGroupId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggVariantForEditOutput : IOutputDto
    {
        public DelAggVariantEditDto DelAggVariant { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggVariantInput : IInputDto
    {
        [Required]
        public DelAggVariantEditDto DelAggVariant { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
}
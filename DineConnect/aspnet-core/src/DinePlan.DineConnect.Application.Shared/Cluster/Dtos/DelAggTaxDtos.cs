﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggTaxListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggTax Properties Missing
        public int Id { get; set; }
        //TODO: DTO DelAggTax Properties Missing
        public virtual string Name { get; set; }
        public virtual int TaxTypeId { get; set; }
        public virtual string TaxTypeName { get; set; }
        public decimal TaxPercentage { get; set; }
        public string LocalRefCode { get; set; }
    }
    public class DelAggTaxEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggTax Properties Missing
        public virtual string Name { get; set; }
        public virtual int TaxTypeId { get; set; }
        public virtual string TaxTypeName { get; set; }
        public decimal TaxPercentage { get; set; }
        public string LocalRefCode { get; set; }
    }
    public class DelAggTaxMappingListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggTax Properties Missing
        public int Id { get; set; }
        public int DelAggTaxId { get; set; }
        public string DelAggTaxName { get; set; }
        public int? DelAggLocationGroupId { get; set; }
        public string DelAggLocationGroupName { get; set; }
        public int? DelItemGroupId { get; set; }
        public string DelItemGroupName { get; set; }
    }
    public class GetDelAggTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public virtual int? TaxTypeId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggTaxForEditOutput : IOutputDto
    {
        public DelAggTaxEditDto DelAggTax { get; set; }
        public List<DelAggTaxMappingListDto> DelAggTaxMapping { get; set; }
    }
    public class CreateOrUpdateDelAggTaxInput : IInputDto
    {
        [Required]
        public DelAggTaxEditDto DelAggTax { get; set; }
        [Required]
        public List<DelAggTaxMappingListDto> DelAggTaxMapping { get; set; }
    }
}


﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class MenuDelivery
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("StartHour")]
        public string StartHour { get; set; }

        [JsonProperty("EndHour")]
        public string EndHour { get; set; }

        [JsonProperty("MenuType")]
        public string MenuType { get; set; }
    }

    public class Menus
    {
        [JsonProperty("Menu")]
        public List<MenuDelivery> Menu { get; set; }
    }

    public class ProductVariations
    {
        [JsonProperty("ProductVariation")]
        public List<ProductVariationsItem> ProductVariation { get; set; }
    }
    public class ProductVariationsItem
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Price")]
        public virtual string SalesPrice { get; set; }
        public virtual string MarkupPrice { get; set; }
        [JsonProperty("ContainerPrice")]
        public string ContainerPrice { get; set; }
    }
    public class Product
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Image")]
        public string Image { get; set; }

        [JsonProperty("ProductVariations")]
        public ProductVariations ProductVariations { get; set; }
    }

    public class Products
    {
        [JsonProperty("Product")]
        public List<Product> Product { get; set; }
    }

    public class MenuCategory
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Products")]
        public Products Products { get; set; }
    }

    public class MenuCategories
    {
        [JsonProperty("MenuCategory")]
        public List<MenuCategory> MenuCategory { get; set; }
    }

    public class MenuProduct
    {
        [JsonProperty("MenuId")]
        public string MenuId { get; set; }

        [JsonProperty("ProductId")]
        public string ProductId { get; set; }
    }

    public class MenuProducts
    {
        [JsonProperty("MenuProduct")]
        public List<MenuProduct> MenuProduct { get; set; }
    }

    public class ToppingTemplate
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("IsHalfHalf")]
        public string IsHalfHalf { get; set; }

        [JsonProperty("QuantityMin")]
        public string QuantityMin { get; set; }

        [JsonProperty("QuantityMax")]
        public string QuantityMax { get; set; }
    }

    public class ToppingTemplates
    {
        [JsonProperty("ToppingTemplate")]
        public List<ToppingTemplate> ToppingTemplate { get; set; }
    }

    public class ToppingTemplateProduct
    {
        [JsonProperty("ToppingTemplateId")]
        public string ToppingTemplateId { get; set; }

        [JsonProperty("ProductId")]
        public string ProductId { get; set; }

        [JsonProperty("Price")]
        public string Price { get; set; }
    }

    public class ToppingTemplateProducts
    {
        [JsonProperty("ToppingTemplateProduct")]
        public List<ToppingTemplateProduct> ToppingTemplateProduct { get; set; }
    }

    public class ProductVariationToppingTemplate
    {
        [JsonProperty("ProductVariationId")]
        public string ProductVariationId { get; set; }

        [JsonProperty("ToppingTemplateId")]
        public string ToppingTemplateId { get; set; }
    }

    public class ProductVariationToppingTemplates
    {
        [JsonProperty("ProductVariationToppingTemplate")]
        public List<ProductVariationToppingTemplate> ProductVariationToppingTemplate { get; set; }
    }

    public class MenuCategoryTranslation
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class MenuCategoryTranslations
    {
        [JsonProperty("MenuCategoryTranslation")]
        public List<MenuCategoryTranslation> MenuCategoryTranslation { get; set; }
    }

    public class ProductTranslation
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class ProductTranslations
    {
        [JsonProperty("ProductTranslation")]
        public List<ProductTranslation> ProductTranslation { get; set; }
    }

    public class ProductVariationTranslation
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }
    }

    public class ProductVariationTranslations
    {
        [JsonProperty("ProductVariationTranslation")]
        public List<ProductVariationTranslation> ProductVariationTranslation { get; set; }
    }

    public class ToppingTemplateTranslation
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }
    }

    public class ToppingTemplateTranslations
    {
        [JsonProperty("ToppingTemplateTranslation")]
        public ToppingTemplateTranslation ToppingTemplateTranslation { get; set; }
    }

    public class Language
    {
        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("MenuCategoryTranslations")]
        public MenuCategoryTranslations MenuCategoryTranslations { get; set; }

        [JsonProperty("ProductTranslations")]
        public ProductTranslations ProductTranslations { get; set; }

        [JsonProperty("ProductVariationTranslations")]
        public ProductVariationTranslations ProductVariationTranslations { get; set; }

        [JsonProperty("ToppingTemplateTranslations")]
        public ToppingTemplateTranslations ToppingTemplateTranslations { get; set; }
    }

    public class Translations
    {
        [JsonProperty("Language")]
        public List<Language> Language { get; set; }
    }

    [Serializable]
    public class DeliveryHeroApiOutputVendor
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Menus")]
        public Menus Menus { get; set; }

        [JsonProperty("MenuCategories")]
        public MenuCategories MenuCategories { get; set; }

        [JsonProperty("MenuProducts")]
        public MenuProducts MenuProducts { get; set; }

        [JsonProperty("ToppingTemplates")]
        public ToppingTemplates ToppingTemplates { get; set; }

        [JsonProperty("ToppingTemplateProducts")]
        public ToppingTemplateProducts ToppingTemplateProducts { get; set; }

        [JsonProperty("ProductVariationToppingTemplates")]
        public ProductVariationToppingTemplates ProductVariationToppingTemplates { get; set; }

        [JsonProperty("Translations")]
        public Translations Translations { get; set; }
    }

    public class DeliveryHeroApiOutput
    {
        [JsonProperty("Vendor")]
        public DeliveryHeroApiOutputVendor Vendor { get; set; }
    }


}

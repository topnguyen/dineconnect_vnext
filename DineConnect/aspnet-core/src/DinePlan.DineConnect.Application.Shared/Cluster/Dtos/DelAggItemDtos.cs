﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggItemListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggItem Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual string FoodType { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public virtual int DelAggCatId { get; set; }
        public virtual string DelAggCatName { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string MenuItemName { get; set; }
        public virtual string MenuItemAliasCode { get; set; }
        public virtual bool IsRecommended { get; set; }
        public virtual decimal Weight { get; set; }
        public virtual decimal Serves { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual string MenuItemPortionName { get; set; }
        public virtual decimal SalesPrice { get; set; }
        public virtual bool VariantExists { get; set; }
        public virtual decimal MarkupPrice { get; set; }
        public virtual int? DelAggTaxId { get; set; }
        public virtual string DelAggTaxName { get; set; }
        public virtual decimal DelAggTaxTaxPercentage { get; set; }
        public virtual int DelAggItemGroupId { get; set; }
        public virtual string DelAggItemGroupName { get; set; }
        public virtual int DelAggVariantGroupId { get; set; }
        public virtual string DelAggVariantGroupName { get; set; }
        public string ButtonColor { get; set; }


    }

    public class DelAggItemEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggItem Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual string FoodType { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public virtual int DelAggCatId { get; set; }
        public virtual string DelAggCatName { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string MenuItemName { get; set; }
        public virtual bool IsRecommended { get; set; }
        public virtual decimal Weight { get; set; }
        public virtual decimal Serves { get; set; }
        public virtual bool VariantExists { get; set; }
        public virtual int? DelAggTaxId { get; set; }
        public virtual string DelAggTaxName { get; set; }
        public virtual int DelAggItemGroupId { get; set; }
        public virtual string DelAggItemGroupName { get; set; }
        public virtual int DelAggVariantGroupId { get; set; }
        public virtual string DelAggVariantGroupName { get; set; }
        public int RowIndex { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public string OutputJsonDto { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public bool IsFailed { get; set; }
        public string Remarks { get; set; }
        public long? CreatorUserId { get; set; }

    }
    public class ExportDelAggItemImportStatusDto
    {
        public List<DelAggItemEditDto> DuplicateExistInExcel { get; set; }
        public List<DelAggItemEditDto> DuplicateExistInDB { get; set; }
        public List<DelAggItemEditDto> ImportedCardsList { get; set; }
        public List<string> ErrorList { get; set; }
        public bool IsFailed { get; set; }
    }

    public class GetDelAggItemInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public virtual int? DelAggCatId { get; set; }
        public virtual int? DelAggItemGroupId { get; set; }
        public virtual int? DelAggTaxId { get; set; }
        public virtual int? MenuItemId { get; set; }
        public virtual int? MenuItemPortionId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDelAggItemForEditOutput : IOutputDto
    {
        public GetDelAggItemForEditOutput()
        {
            DelAggImage = new List<DelAggImageEditDto>();
            DelAggVariantGroup = new DelAggVariantGroupEditDto();
            DelAggVariant = new List<DelAggVariantEditDto>();
            DelAggItem = new DelAggItemEditDto();
            DelAggModifierGroupItem = new List<DelAggModifierGroupItemEditDto>();
        }
        public DelAggItemEditDto DelAggItem { get; set; }
        public DelAggVariantGroupEditDto DelAggVariantGroup { get; set; }
        public List<DelAggModifierGroupItemEditDto> DelAggModifierGroupItem { get; set; }
        public List<DelAggVariantEditDto> DelAggVariant { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggItemInput : IInputDto
    {
        public CreateOrUpdateDelAggItemInput()
        {
            DelAggVariantGroup = new DelAggVariantGroupEditDto();
            DelAggImage = new List<DelAggImageEditDto>();
            DelAggVariant = new List<DelAggVariantEditDto>();
            DelAggModifierGroupItem = new List<DelAggModifierGroupItemEditDto>();
        }

        public DelAggItemEditDto DelAggItem { get; set; }
        public DelAggVariantGroupEditDto DelAggVariantGroup { get; set; }
        public List<DelAggVariantEditDto> DelAggVariant { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
        public List<DelAggModifierGroupItemEditDto> DelAggModifierGroupItem { get; set; }
        public bool NotOverride { get; set; }
        public bool UpdateItem { get; set; }
    }

    public class BulkDelAggItemImportDtos
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public List<CreateOrUpdateDelAggItemInput> CreateOrUpdateDelAggItemInputs { get; set; }
    }

    public class CloneDelAggItemDtos
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }

    public class TenantUserIdInput
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public int Id { get; set; }
    }
}


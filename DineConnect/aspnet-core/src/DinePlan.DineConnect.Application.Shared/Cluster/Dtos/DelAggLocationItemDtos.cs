﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggLocationItemListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggLocationItem Properties Missing
        public virtual int Id { get; set; }
        public virtual int DelAggLocMappingId { get; set; }
        public virtual string DelAggLocMappingName { get; set; }
        public virtual int? DelAggItemId { get; set; }
        public virtual string DelAggItemName { get; set; }
        public decimal Price { get; set; }
        public bool InActive { get; set; }
        public string AddOns { get; set; }
        public virtual int? DelAggVariantId { get; set; }
        public virtual string DelAggVariantName { get; set; }
        public virtual int? DelAggModifierId { get; set; }
        public virtual string DelAggModifierName { get; set; }
        public virtual int DelAggPriceTypeRefId { get; set; }
        public virtual string DelAggPriceTypeRefName { get; set; }
    }
    public class DelAggLocationItemEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggLocationItem Properties Missing
        public virtual int DelAggLocMappingId { get; set; }
        public virtual string DelAggLocMappingName { get; set; }
        public virtual int? DelAggItemId { get; set; }
        public virtual string DelAggItemName { get; set; }
        public decimal Price { get; set; }
        public bool InActive { get; set; }
        public bool ChangePrice { get; set; }

        public string AddOns { get; set; }
        public virtual int? DelAggVariantId { get; set; }
        public virtual string DelAggVariantName { get; set; }
        public virtual int? DelAggModifierId { get; set; }
        public virtual string DelAggModifierName { get; set; }
        public virtual ClusterEnum.DelAggSpecType DelAggPriceTypeRefId { get; set; }
        public virtual string DelAggPriceTypeRefName { get; set; }
    }

    public class GetDelAggLocationItemInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public virtual int? DelAggLocMappingId { get; set; }
        public virtual int? DelAggItemId { get; set; }
        public virtual int? DelAggVariantId { get; set; }
        public virtual int? DelAggModifierId { get; set; }
        public virtual int? DelAggPriceTypeRefId { get; set; }

        public bool? InActive { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDelAggLocationItemForEditOutput : IOutputDto
    {
        public DelAggLocationItemEditDto DelAggLocationItem { get; set; }
    }
    public class CreateOrUpdateDelAggLocationItemInput : IInputDto
    {
        [Required]
        public DelAggLocationItemEditDto DelAggLocationItem { get; set; }
    }
}


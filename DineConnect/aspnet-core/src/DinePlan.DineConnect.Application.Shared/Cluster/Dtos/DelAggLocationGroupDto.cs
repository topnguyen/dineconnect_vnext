﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggLocationGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggLocationGroup Properties Missing
        public int Id { get; set; }
        public  string Name { get; set; }
    }
    public class DelAggLocationGroupEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggLocationGroup Properties Missing
        public virtual string Name { get; set; }
    }

    public class GetDelAggLocationGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggLocationGroupForEditOutput : IOutputDto
    {
        public DelAggLocationGroupEditDto DelAggLocationGroup { get; set; }
    }
    public class CreateOrUpdateDelAggLocationGroupInput : IInputDto
    {
        [Required]
        public DelAggLocationGroupEditDto DelAggLocationGroup { get; set; }
    }
}
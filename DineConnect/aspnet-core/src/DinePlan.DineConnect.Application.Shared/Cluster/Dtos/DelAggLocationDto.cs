﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggLocationListDto : FullAuditedEntityDto
    {
        public int Id { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string LocationName { get; set; }
        public string AddOns { get; set; }
        public virtual string Name { get; set; }
        public virtual int? DelAggLocationGroupId { get; set; }
        public virtual string DelAggLocationGroupName { get; set; }
    }

    public class DelAggLocationEditDto : ConnectOrgLocEditDto
    {
        public int? Id { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string LocationName { get; set; }
        public string AddOns { get; set; }
        public virtual string Name { get; set; }
        public virtual int DelAggLocationGroupId { get; set; }
        public virtual string DelAggLocationGroupName { get; set; }

    }

    public class GetDelAggLocationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }
        public string Operation {get; set;}
        public virtual int? LocationId { get; set; }
        public virtual int? DelAggLocationGroupId { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDelAggLocationForEditOutput : IOutputDto
    {
        public DelAggLocationEditDto DelAggLocation { get; set; }
        public CommonLocationGroupDto LocationGroup { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }

        public GetDelAggLocationForEditOutput()
        {
            LocationGroup = new CommonLocationGroupDto();
            DelAggLocation = new DelAggLocationEditDto();
            DelAggImage = new List<DelAggImageEditDto>();

        }
    }
    public class CreateOrUpdateDelAggLocationInput : IInputDto
    {
        [Required]
        public DelAggLocationEditDto DelAggLocation { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }

        public CreateOrUpdateDelAggLocationInput()
        {
            LocationGroup = new LocationGroupDto();
            DelAggImage = new List<DelAggImageEditDto>();
        }
    }
}
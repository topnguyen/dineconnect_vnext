﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class GrabCurrency
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("exponent")]
        public int Exponent { get; set; }
    }

    public class GrabPeriod
    {
        public GrabPeriod()
        {
            StartTime = "10:00";
            EndTime = "22:00";
        }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("endTime")]
        public string EndTime { get; set; }
    }

    public class GrabDayFormat
    {
        [JsonProperty("openPeriodType")]
        public string OpenPeriodType { get; set; }

        [JsonProperty("periods")]
        public List<GrabPeriod> Periods { get; set; }
    }
    public class GrabServiceHours
    {
        [JsonProperty("mon")]
        public GrabDayFormat Monday { get; set; }
        [JsonProperty("tue")]
        public GrabDayFormat Tuesday { get; set; }
        [JsonProperty("wed")]
        public GrabDayFormat Wednesday { get; set; }
        [JsonProperty("thu")]
        public GrabDayFormat Thursday { get; set; }
        [JsonProperty("fri")]
        public GrabDayFormat Friday { get; set; }
        [JsonProperty("sat")]
        public GrabDayFormat Saturday { get; set; }
        [JsonProperty("sun")]
        public GrabDayFormat Sunday { get; set; }
    }

    public class GrabModifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; } = "AVAILABLE";

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonIgnore]
        public int? GroupId { get; set; }

        [JsonProperty("sequence")] public int Sequence { get; set; } = 1;
    }

    public class GrabModifierGroup
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; } = "AVAILABLE";

        [JsonProperty("selectionRangeMin")]
        public int? SelectionRangeMin { get; set; }

        [JsonProperty("selectionRangeMax")]
        public int? SelectionRangeMax { get; set; }

        [JsonProperty("modifiers")]
        public List<GrabModifier> Modifiers { get; set; }
        [JsonProperty("sequence")]
        public int Sequence { get; set; }
    }

    public class GrabItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; } = "AVAILABLE";

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }
        [JsonProperty("photos")]
        public List<string> Photos { get; set; }

        [JsonProperty("specialType")]
        public string SpecialType { get; set; }

        [JsonProperty("modifierGroups")]
        public List<GrabModifierGroup> ModifierGroups { get; set; }
        [JsonProperty("sequence")]
        public int Sequence { get; set; }

    }

    public class GrabCategory
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; } = "AVAILABLE";

        [JsonProperty("items")]
        public List<GrabItem> Items { get; set; }
        [JsonProperty("sequence")]
        public int Sequence { get; set; }
    }

    public class GrabSection
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("serviceHours")]
        public GrabServiceHours ServiceHours { get; set; }

        [JsonProperty("categories")]
        public List<GrabCategory> Categories { get; set; }
        [JsonProperty("sequence")]
        public int Sequence { get; set; }
    }

    public class GrabOutputApi
    {
        [JsonProperty("merchantID")]
        public string MerchantID { get; set; }

        [JsonProperty("partnerMerchantID")]
        public string PartnerMerchantID { get; set; }

        [JsonProperty("currency")]
        public GrabCurrency Currency { get; set; }

        [JsonProperty("sections")]
        public List<GrabSection> Sections { get; set; }
    }


    public class AggLocationItemsDto
    {
        public int DelAggItemId { get; set; }
        public string DelAggItemName { get; set; }
        public string DelAggItemDescription { get; set; }
        public int DelTimingGroupId { get; set; }
        public string DelTimingGroupName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public DelAggCategoryListDto DelAggCategory { get; set; }
        public DelTimingGroupListDto DelTimingGroup { get; set; }
        public DelAggItemListDto DelAggItem { get; set; }

        public DelAggVariantListDto DelAggVariant { get; set; }

        public DelAggModifierListDto DelAggModifier { get; set; }

        public DelAggImageListDto DelAggImage { get; set; }
        public decimal Price { get; set; }

        public bool IsCombo { get; set; }
        public int CategorySortOrder { get; set; }
    }
    public class DelAggInput
    {
        public int LocationId { get; set; }
        public ClusterEnum.DelAggType Type { get; set; }
    }
}

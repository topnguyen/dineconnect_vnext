﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggChargeListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggCharge Properties Missing
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string ApplicableOn { get; set; }
        public virtual Decimal Amount { get; set; }
        public virtual string ExcludeDeliveryTypes { get; set; }
        public virtual string FullFilmentTypes { get; set; }
        public string LocalRefCode { get; set; }
    }
    
    public class DelAggChargeEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggCharge Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string ApplicableOn { get; set; }
        public virtual Decimal Amount { get; set; }
        public virtual string ExcludeDeliveryTypes { get; set; }
        public virtual string FullFilmentTypes { get; set; }
        public string LocalRefCode { get; set; }
    }
    
    public class DelAggChargeMappingListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggCharge Properties Missing
        public virtual int Id { get; set; }
        public int DelAggChargeId { get; set; }
        public string DelAggChargeName { get; set; }
        public int DelAggLocationGroupId { get; set; }
        public string DelAggLocationGroupName { get; set; }
        public int DelItemGroupId { get; set; }
        public string DelItemGroupName { get; set; }
    }
    
    public class GetDelAggChargeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggChargeForEditOutput : IOutputDto
    {
        public DelAggChargeEditDto DelAggCharge { get; set; }
        public List<DelAggChargeMappingListDto> DelAggChargeMapping { get; set; }
    }
    public class CreateOrUpdateDelAggChargeInput : IInputDto
    {
        [Required]
        public DelAggChargeEditDto DelAggCharge { get; set; }
        [Required]
        public List<DelAggChargeMappingListDto> DelAggChargeMapping { get; set; }
    }
}


﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelTimingGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelTimingGroup Properties Missing
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
    public class DelTimingGroupEditDto
    {
        public DelTimingGroupEditDto()
        {
            DelTimingDetails = new Collection<DelTimingDetailEditDto>();
        }
        public int? Id { get; set; }
        //TODO: DTO DelTimingGroup Properties Missing
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public Collection<DelTimingDetailEditDto> DelTimingDetails { get; set; }
        public int TenantId { get; set; }
    }

    public class DelTimingDetailEditDto
    {
        private string _Days;

        public DelTimingDetailEditDto()
        {
            _Days = null;
        }

        public int? Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public List<ComboboxItemDto> AllDays { get; set; }
    }
    public class GetDelTimingGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelTimingGroupForEditOutput : IOutputDto
    {
        public DelTimingGroupEditDto DelTimingGroup { get; set; }
    }
    public class CreateOrUpdateDelTimingGroupInput : IInputDto
    {
        [Required]
        public DelTimingGroupEditDto DelTimingGroup { get; set; }
        public DelTimingDetailEditDto DelTimingDetail { get; set; }
    }
}


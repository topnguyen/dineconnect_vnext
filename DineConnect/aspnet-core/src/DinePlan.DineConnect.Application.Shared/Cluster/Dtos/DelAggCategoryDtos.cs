﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggCategoryListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggCategory Properties Missing
        public int Id { get; set; }
        //TODO: DTO DelAggCategory Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public int? DelAggCatParentId { get; set; }
        public string DelAggCatParentName { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int DelTimingGroupId { get; set; }
        public string DelTimingGroupName { get; set; }

        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public string MainButtonColor { get; set; }
    }
    
    public class DelAggCategoryEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggCategory Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public int? DelAggCatParentId { get; set; }
        public string DelAggCatParentName { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int DelTimingGroupId { get; set; }
        public string DelTimingGroupName { get; set; }
        public int TenantId { get; set; }
    }

    public class GetDelAggCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? DelTimingGroupId { get; set; }
        
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggCategoryForEditOutput : IOutputDto
    {
        public DelAggCategoryEditDto DelAggCategory { get; set; }
    }
    
    public class CreateOrUpdateDelAggCategoryInput : IInputDto
    {
        [Required]
        public DelAggCategoryEditDto DelAggCategory { get; set; }
    }

    public class GetCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int DelAggItemId { get; set; }
        public int DelAggCategoryId { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public string CurrentTag { get; set; }
        public int? FutureDataId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder,Id";
            }
        }
    }
    
    public class GetItemByCategoryOuput
    {
        public GetItemByCategoryOuput()
        {
            DelAggItems = new List<DelAggItemListDto>();
        }

        public List<DelAggItemListDto> DelAggItems { get; set; }
    }
}


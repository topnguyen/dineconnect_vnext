﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggModifierListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggModifier Properties Missing
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int? DelAggModifierGroupId { get; set; }
        public string DelAggModifierGroupName { get; set; }
        public int? OrderTagId { get; set; }
        public string OrderTagName { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        public string NestedModGroupName { get; set; }
    }
    public class DelAggModifierEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggModifier Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int? DelAggModifierGroupId { get; set; }
        public string DelAggModifierGroupName { get; set; }
        public int? OrderTagId { get; set; }
        public string OrderTagName { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        public string NestedModGroupName { get; set; }
    }

    public class GetDelAggModifierInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? DelAggModifierGroupId { get; set; }
        public int? OrderTagId { get; set; }
        public int? NestedModGroupId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggModifierForEditOutput : IOutputDto
    {
        public DelAggModifierEditDto DelAggModifier { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggModifierInput : IInputDto
    {
        [Required]
        public DelAggModifierEditDto DelAggModifier { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class ApiDelAggRequest 
    {
        public int DelLocationId { get; set; }
        public int LocationId { get; set; }
        public string MerchantCode { get; set; }
        public string PartnerMerchantCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string CurrencyCode{ get; set; }
        public int TenantId { get; set; }
    }
}
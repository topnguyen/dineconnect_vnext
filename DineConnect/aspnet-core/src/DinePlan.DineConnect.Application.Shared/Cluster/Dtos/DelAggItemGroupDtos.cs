﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    public class DelAggItemGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggItemGroup Properties Missing
        public int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
    }
    public class DelAggItemGroupEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int TenantId { get; set; }
    }

    public class GetDelAggItemGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggItemGroupForEditOutput : IOutputDto
    {
        public DelAggItemGroupEditDto DelAggItemGroup { get; set; }
    }
    public class CreateOrUpdateDelAggItemGroupInput : IInputDto
    {
        public CreateOrUpdateDelAggItemGroupInput()
        {
            DelAggItemGroup = new DelAggItemGroupEditDto();
        }
        [Required]
        public DelAggItemGroupEditDto DelAggItemGroup { get; set; }
    }
}



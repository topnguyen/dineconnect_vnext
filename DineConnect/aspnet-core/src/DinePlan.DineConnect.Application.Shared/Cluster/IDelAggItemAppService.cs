﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggItemAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggItemListDto>> GetAll(GetDelAggItemInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggItemInput input);
        Task<GetDelAggItemForEditOutput> GetDelAggItemForEdit(NullableIdDto EntityDto);
        Task<EntityDto> CreateOrUpdateDelAggItem(CreateOrUpdateDelAggItemInput input);
        Task DeleteDelAggItem(EntityDto input);
        Task<ListResultDto<DelAggItemListDto>> GetNames();
        Task<List<DelAggItemEditDto>> GetImportDelAggItemDetail(GetDelAggItemInput input);
        Task<FileDto> BulkDelAggItemImport(BulkDelAggItemImportDtos bulkList);
        Task<MessageOutputDto> ConvertAsMenuItemAsDelAggItem(TenantUserIdInput input);
        Task<FileDto> CloneItems(CloneDelAggItemDtos input);
        Task InitiateBackGroundClone();


    }
}
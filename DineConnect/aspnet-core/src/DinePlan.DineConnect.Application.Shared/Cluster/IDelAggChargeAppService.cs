﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggChargeAppService : IApplicationService
    {
        Task<PagedResultDto<DelAggChargeListDto>> GetAll(GetDelAggChargeInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggChargeInput input);
        Task<GetDelAggChargeForEditOutput> GetDelAggChargeForEdit(EntityDto EntityDto);
        Task CreateOrUpdateDelAggCharge(CreateOrUpdateDelAggChargeInput input);
        Task DeleteDelAggCharge(EntityDto input);
    }
}
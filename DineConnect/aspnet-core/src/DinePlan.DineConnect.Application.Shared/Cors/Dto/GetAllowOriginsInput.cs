﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Cors.Dto
{
    public class GetAllowOriginsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}

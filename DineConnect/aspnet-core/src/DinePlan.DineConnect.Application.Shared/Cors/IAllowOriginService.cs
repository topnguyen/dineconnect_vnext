﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cors.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cors
{
    public interface IAllowOriginService : IApplicationService
    {
        Task<PagedResultDto<GetAllowOriginForViewDto>> GetAll(GetAllowOriginsInput input);
        Task CreateOrEdit(CreateOrEditAllowOriginInput input);
        Task<GetAllowOriginForEditDto> GetAllowOriginForEdit(EntityDto input);
        Task Delete(EntityDto input);
    }
}

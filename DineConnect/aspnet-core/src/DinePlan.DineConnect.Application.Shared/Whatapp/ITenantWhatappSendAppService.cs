﻿using System.Threading.Tasks;

namespace DinePlan.DineConnect.Whatapp
{
    public interface ITenantWhatappSendAppService
    {
        Task SendWhatappAsync(string toPhone, string message);

        Task SendWhatappByTemplateAsync(string toPhone, string module, int? tenantId, object variables);
    }
}

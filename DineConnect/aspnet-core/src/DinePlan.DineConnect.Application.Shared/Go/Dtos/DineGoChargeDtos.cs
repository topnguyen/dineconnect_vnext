﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Go.Dtos
{
    public class DineGoChargeListDto : FullAuditedEntityDto
    {
        public string Label { get; set; }
        public bool IsPercent { get; set; }
        public bool TaxIncluded { get; set; }
        public decimal ChargeValue { get; set; }
        public virtual int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
    }
    public class DineGoChargeEditDto
    {
        public int? Id { get; set; }
        public string Label { get; set; }
        public bool IsPercent { get; set; }
        public bool TaxIncluded { get; set; }
        public decimal ChargeValue { get; set; }
        public virtual int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public int TenantId { get; set; }
    }

    public class GetDineGoChargeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
    public class GetDineGoChargeForEditOutput : IOutputDto
    {
        public DineGoChargeEditDto DineGoCharge { get; set; }
    }
    public class CreateOrUpdateDineGoChargeInput : IInputDto
    {
        [Required]
        public DineGoChargeEditDto DineGoCharge { get; set; }
    }
}

﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Go.Dtos
{
    public class DineGoBrandListDto : FullAuditedEntityDto
    {
        public string Label { get; set; }
        public virtual int LocationId { get; set; }
        public virtual int ScreenMenuId { get; set; }
        public string ScreenMenuName { get; set; }
        public string LocationName { get; set; }
    }

    public class DineGoBrandEditDto
    {
        public int? Id { get; set; }

        public string Label { get; set; }

        public virtual int LocationId { get; set; }

        public Collection<DineGoDepartmentEditDto> DineGoDepartments { get; set; }

        public virtual int ScreenMenuId { get; set; }
    }

    public class GetDineGoBrandInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
    public class GetDineGoBrandForEditOutput : IOutputDto
    {
        public DineGoBrandEditDto DineGoBrand { get; set; }
    }
    public class CreateOrUpdateDineGoBrandInput : IInputDto
    {
        [Required]
        public DineGoBrandEditDto DineGoBrand { get; set; }
    }
    public class CreateDineGoBrandDetail : IInputDto
    {
        [Required]
        public DineGoBrandDetailEditDto DineGoBrandDetailEditDto { get; set; }
    }

    public class DineGoBrandDetailEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int DineGoDepartmentId { get; set; }
        public int DineGoBrandId { get; set; }
        public string DineGoDepartmentName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Go.Dtos
{
    public class GetDineGoDeviceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class DineGoDeviceListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public int IdleTime { get; set; }

        public string OrderLink { get; set; }

        public string PinCode { get; set; }

        public string ThemeSettings { get; set; }

        public Guid ChangeGuid { get; set; }
    }

    public class DineGoDeviceDto : IOutputDto
    {
        public DineGoDeviceEditDto DineGoDevice { get; set; }

        public DeviceThemeSettingsDto DeviceThemeSettings { get; set; }
    }

    public class DineGoDeviceEditDto
    {
        public int? Id { get; set; }
        
        public string Name { get; set; }
        
        public int IdleTime { get; set; }
        
        public string OrderLink { get; set; }
        
        public string PinCode { get; set; }
        
        public bool PushToConnect { get; set; }
        
        public string ThemeSettings { get; set; }
        
        public Guid ChangeGuid { get; set; }
        
        public bool RefreshImage { get; set; }

        public List<DineGoBrandEditDto> DineGoBrands { get; set; } = new List<DineGoBrandEditDto>();

        public List<DineGoPaymentTypeEditDto> DineGoPaymentTypes { get; set; } = new List<DineGoPaymentTypeEditDto>();
    }

    public class DeviceThemeSettingsDto
    {
        public string IdleContents { get; set; }
        public string BannerContents { get; set; }
        public string BackgroundColor { get; set; }
        public string Logo { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Go.Dtos
{
    public class DineGoPaymentTypeListDto : FullAuditedEntityDto
    {
        public string Label { get; set; }
        public virtual int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    }
    public class DineGoPaymentTypeEditDto
    {
        public int? Id { get; set; }
        public string Label { get; set; }
        public virtual int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    }

    public class GetDineGoPaymentTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id Desc";
            }
        }
    }
    public class GetDineGoPaymentTypeForEditOutput : IOutputDto
    {
        public DineGoPaymentTypeEditDto DineGoPaymentType { get; set; }
    }
    public class CreateOrUpdateDineGoPaymentTypeInput : IInputDto
    {
        [Required]
        public DineGoPaymentTypeEditDto DineGoPaymentType { get; set; }
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go
{
    public interface IDineGoDepartmentAppService : IApplicationService
    {
        Task<PagedResultDto<DineGoDepartmentListDto>> GetAll(GetDineGoDepartmentInput input);

        Task<FileDto> GetAllToExcel(GetDineGoDepartmentInput input);

        Task<GetDineGoDepartmentForEditOutput> GetDineGoDepartmentForEdit(EntityDto input);

        Task<EntityDto> CreateOrUpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input);
        
        Task DeleteDineGoDepartment(EntityDto input);
    }
}

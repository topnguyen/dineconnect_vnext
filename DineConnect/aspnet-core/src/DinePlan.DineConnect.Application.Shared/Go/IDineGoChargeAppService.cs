﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go
{
    public interface IDineGoChargeAppService : IApplicationService
    {
        Task<PagedResultDto<DineGoChargeListDto>> GetAll(GetDineGoChargeInput input);

        Task<FileDto> GetAllToExcel(GetDineGoChargeInput input);

        Task<GetDineGoChargeForEditOutput> GetDineGoChargeForEdit(EntityDto input);

        Task<EntityDto> CreateOrUpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input);
        
        Task DeleteDineGoCharge(EntityDto input);

        Task<ListResultDto<ComboboxItemDto>> GetChargesForCombobox();

        Task<List<DineGoChargeListDto>> GetChargesListDtos();
    }
}

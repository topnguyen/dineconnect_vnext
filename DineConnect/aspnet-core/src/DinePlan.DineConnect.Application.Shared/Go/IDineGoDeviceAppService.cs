﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go
{
    public interface IDineGoDeviceAppService : IApplicationService
    {
        Task<PagedResultDto<DineGoDeviceListDto>> GetAll(GetDineGoDeviceInput input);

        Task<FileDto> GetAllToExcel(GetDineGoDeviceInput input);

        Task<DineGoDeviceDto> GetDineGoDeviceForEdit(EntityDto input);

        Task<EntityDto> CreateOrUpdateDineGoDevice(DineGoDeviceDto input);
        
        Task DeleteDineGoDevice(EntityDto input);

        Task<List<DineGoBrandListDto>> GetDineGoBrandForCombobox();

        Task<List<DineGoPaymentTypeListDto>> GetDineGoPaymentTypeForCombobox();
        
        Task<List<ComboboxItemDto>> ApiGetAllDevices();
    }
}

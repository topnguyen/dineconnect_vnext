﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go
{
    public interface IDineGoPaymentTypeAppService : IApplicationService
    {
        Task<PagedResultDto<DineGoPaymentTypeListDto>> GetAll(GetDineGoPaymentTypeInput input);

        Task<FileDto> GetAllToExcel(GetDineGoPaymentTypeInput input);

        Task<GetDineGoPaymentTypeForEditOutput> GetDineGoPaymentTypeForEdit(EntityDto input);

        Task<EntityDto> CreateOrUpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input);
        
        Task DeleteDineGoPaymentType(EntityDto input);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go
{
    public interface IDineGoBrandAppService : IApplicationService
    {
        Task<PagedResultDto<DineGoBrandListDto>> GetAll(GetDineGoBrandInput input);

        Task<FileDto> GetAllToExcel(GetDineGoBrandInput input);

        Task<GetDineGoBrandForEditOutput> GetDineGoBrandForEdit(EntityDto input);

        Task<EntityDto> CreateOrUpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input);
        
        Task DeleteDineGoBrand(EntityDto input);

        Task<ListResultDto<ComboboxItemDto>> GetScreenMenuForCombobox();

        Task<List<DineGoDepartmentListDto>> GetDineGodepartmentForCombobox();
    }
}

﻿using Abp.Application.Services;
using DinePlan.DineConnect.AddonSettings.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.AddonSettings
{
    public interface IAddonSettingsAppService : IApplicationService
    {
        Task CreateOrUpdateAddonSetting(CreateOrEditAddonDto input);

        Task CreateOrUpdateListAddon(List<CreateOrEditAddonDto> inputs);

        Task<CreateOrEditAddonDto> GetAddonSettingForEdit(GetAddonForEditDto input);

        Task<List<CreateOrEditAddonDto>> GetPaymentSettingForEdit();

        Task<List<ServiceChargeDto>> GetServiceCharge(string paymentMode);
    }
}
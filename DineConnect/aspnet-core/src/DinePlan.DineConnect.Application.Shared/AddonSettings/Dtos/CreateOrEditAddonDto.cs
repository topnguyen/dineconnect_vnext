﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.AddonSettings.Dtos
{
    public class CreateOrEditAddonDto : Entity<int?>
    {
        public string Name { get; set; }

        public string Countries { get; set; }

        public object Setting { get; set; }

        public string Settings { get; set; }
        public int AddonType { get; set; }
    }
}
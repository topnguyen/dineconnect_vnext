﻿namespace DinePlan.DineConnect.AddonSettings.Dtos
{
    public class GetAddonForEditDto
    {
        public int AddonType { get; set; }
    }
}
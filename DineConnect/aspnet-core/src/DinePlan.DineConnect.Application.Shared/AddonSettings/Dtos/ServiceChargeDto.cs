﻿namespace DinePlan.DineConnect.AddonSettings.Dtos
{
    public class ServiceChargeDto
    {
        public string Name { get; set; }

        public decimal Percentage { get; set; }

        public bool OmisePaymentUse { get; set; }

        public bool StripePaymentUse { get; set; }
        
        public bool AdyenPaymentUse { get; set; }
        
        public bool PaypalPaymentUse { get; set; }

        public bool GooglePayPaymentUse { get; set; }
    }
}
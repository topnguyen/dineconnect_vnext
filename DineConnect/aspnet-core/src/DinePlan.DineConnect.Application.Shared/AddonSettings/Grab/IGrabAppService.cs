﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace DinePlan.DineConnect.AddonSettings.Grab
{
    public interface IGrabAppService : IApplicationService
    {
        Task<bool> IsTokenExists();

        bool RemoveToken();

        Task<bool> Connect(string clientId, string clientSecret);

        Task<string> GrabGetMenuItem();

        Task<string> GrabSyncData(string merchantID);

        Task<string> GrabGetMenuItemForLocation(int locationid);
    }
}

﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabMenuCategory
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; }

        [JsonProperty("items")]
        public List<GrabMenuItem> Items { get; set; }

        //      "id": "<category_id>",
        //"name": "<category_name>",
        //"availableStatus": "<AVAILABLE|UNAVAILABLE|HIDE>",
        //"items": [], // for Food
    }
}
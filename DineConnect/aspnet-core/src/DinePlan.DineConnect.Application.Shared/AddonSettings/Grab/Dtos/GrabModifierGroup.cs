﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabModifierGroup
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; }

        [JsonProperty("selectionRangeMin")]
        public int SelectionRangeMin { get; set; }

        [JsonProperty("selectionRangeMax")]
        public int SelectionRangeMax { get; set; }

        [JsonProperty("modifiers")]
        public List<GrabModifier> Modifiers { get; set; }
    }
}
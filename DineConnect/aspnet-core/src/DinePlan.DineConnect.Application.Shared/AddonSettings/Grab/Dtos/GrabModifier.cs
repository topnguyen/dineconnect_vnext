﻿using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabModifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class ServiceHours
    {
        public string name { get; set; }

        public string startHour { get; set; }
        public string startMinute { get; set; }
        public string endHour { get; set; }
        public string endMinute { get; set; }

        public string startTime => $"{startHour}:{startMinute}";
        public string endTime => $"{endHour}:{endMinute}";
    }
}
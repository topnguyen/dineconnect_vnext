﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabMenuSection
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("serviceHours")]
        public object ServiceHours { get; set; }

        [JsonProperty("categories")]
        public List<GrabMenuCategory> Categories { get; set; }
    }
}
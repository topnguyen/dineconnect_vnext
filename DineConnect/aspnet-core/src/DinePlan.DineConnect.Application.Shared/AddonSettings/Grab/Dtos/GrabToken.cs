﻿using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabToken
    {
        public string access_token { get; set; }
        public double expires_in { get; set; }
        public DateTime ExpiresOn { get; set; }

        [JsonIgnore]
        public bool IsExpired
        {
            get
            {
                return DateTime.UtcNow >= ExpiresOn;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class GrabSetting
    {
        public bool Production { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }

    public class GrabAuthRequest
    {
        public GrabAuthRequest()
        {
            grant_type = "client_credentials";
            scope = "food.partner_api";
        }

        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string grant_type { get; set; }
        public string scope { get; set; }
    }
}
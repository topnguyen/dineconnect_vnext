﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Grab.Dtos
{
    public class GrabMenuItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("availableStatus")]
        public string AvailableStatus { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("photos")]
        public List<string> Photos { get; set; }

        [JsonProperty("specialType")]
        public string SpecialType { get; set; }

        [JsonProperty("modifierGroups")]
        public List<GrabModifierGroup> ModifierGroups { get; set; }
    }

    public class AvailableStatus
    {
        public const string AVAILABLE = "AVAILABLE";
        public const string UNAVAILABLE = "UNAVAILABLE";
        public const string HIDE = "HIDE";
    }
}
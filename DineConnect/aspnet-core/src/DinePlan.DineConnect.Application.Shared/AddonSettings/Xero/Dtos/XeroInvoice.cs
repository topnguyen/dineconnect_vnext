﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.AddonSettings.Xero.Dtos
{
    public class XeroInvoiceContact    {
        public string ContactID { get; set; } 
    }

    public class XeroInvoiceTracking    {
        public string Name { get; set; } 
        public string Option { get; set; } 
    }

    public class XeroInvoiceLineItem    {
        public XeroInvoiceLineItem()
        {
            Tracking = new List<XeroInvoiceTracking>();
            TaxType = "INPUT";
        }
        public string Description { get; set; } 
        public string TaxType { get; set; } 
        public string Quantity { get; set; } 
        public string UnitAmount { get; set; } 
        public string ItemCode { get; set; } 
        public List<XeroInvoiceTracking> Tracking { get; set; } 
    }

    public class XeroInvoice    {

        public XeroInvoice()
        {
            Contact = new XeroInvoiceContact();
            LineItems= new List<XeroInvoiceLineItem>();
            LineAmountTypes = "Inclusive";
        }
        public string Type { get; set; } 
        public XeroInvoiceContact Contact { get; set; } 
        public DateTime Date { get; set; } 
        public DateTime DueDate { get; set; } 
        public List<XeroInvoiceLineItem> LineItems { get; set; }
        public string InvoiceID { get; set; }

        public string Status { get; set; }
        public string LineAmountTypes { get; set; }

    }
    
    public class XeroInvoiceInput    {
        public string InvoiceID { get; set; } 
    }

    public class XeroPayment    {
        public XeroInvoiceInput Invoice { get; set; } 
        public XeroAccount Account { get; set; } 
        public string Date { get; set; } 
        public decimal Amount { get; set; }

        public XeroPayment()
        {
            Invoice = new XeroInvoiceInput();
            Account = new XeroAccount();
        }
    }

    public class XeroPaymentList    {
        public XeroPaymentList()
        {
            Payments = new List<XeroPayment>();
        }
        public List<XeroPayment> Payments { get; set; } 
    }
}

﻿using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Xero.Dtos
{
    public class XeroToken
    {
        public string id_token { get; set; }
        public string access_token { get; set; }
        public double expires_in { get; set; }
        public string refresh_token { get; set; }
        public string xero_tenant_id { get; set; }
        public DateTime ExpiresOn { get; set; }

        public string Scope { get; set; }

        [JsonIgnore]
        public bool IsExpired
        {
            get
            {
                return DateTime.UtcNow >= ExpiresOn;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

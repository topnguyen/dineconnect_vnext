﻿using Newtonsoft.Json;

namespace DinePlan.DineConnect.AddonSettings.Xero.Dtos
{
    public class XeroItem
    {
        public string ItemID{ get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

 
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.AddonSettings.Xero.Dtos;

namespace DinePlan.DineConnect.AddonSettings.Xero
{
    public interface IXeroAppService : IApplicationService
    {
        string GetLoginUrl();
        
        bool RemoveToken();
        
        Task<bool> IsTokenExists();
        
        Task<bool> KeepTokenComingFromGateway(XeroToken token);
        
        Task<XeroToken> RefreshTokenAndSave(XeroToken token);
        
        Task<XeroSetting> GetXeroSetting();
        
        Task UpdateXeroSetting(XeroSetting xeroSetting);
        
        Task<List<ComboboxItemDto>> GetItems();
        
        Task<List<ComboboxItemDto>> GetAccounts();
        
        Task<List<ComboboxItemDto>> GetCustomers();
    }
}

﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}

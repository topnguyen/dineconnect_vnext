﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.RedisCache.RedisProvider
{
    public interface ICacheProvider
    {
        bool Connect();
        bool Save<T>(string listKey, string key, T objValue, TimeSpan? expiryValueInMins = null);
        bool Save<T>(string listKey, List<KeyValuePair<string, T>> values, TimeSpan? expiryValueInMins = null);
        bool Remove(string listKey, string key);
        bool Contains(string listKey, string key);
        T Get<T>(string listKey, string key);
        List<T> Get<T>(string listKey, List<string> keys);
        bool Disconnect();
    }
}

﻿using Abp.Web.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Uploader.Dto
{
    public class UploadImageOutput : ErrorInfo
    {
        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileToken { get; set; }
        public string Url { get; set; }

        public UploadImageOutput()
        {

        }

        public UploadImageOutput(ErrorInfo error)
        {
            Code = error.Code;
            Details = error.Details;
            Message = error.Message;
            ValidationErrors = error.ValidationErrors;
        }
    }
}

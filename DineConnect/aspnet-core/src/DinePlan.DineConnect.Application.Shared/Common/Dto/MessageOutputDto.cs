﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Common.Dto
{
	public class MessageOutputDto
	{
		public MessageOutputDto()
		{
			OutputMessageList = new List<string>();
			ErrorMessageList = new List<string>();
			SuccessMessageList = new List<string>();
		}

		public bool SuccessFlag { get; set; }
		public bool Exists { get; set; }
		public string ErrorMessage { get; set; }
		public List<string> OutputMessageList { get; set; }
		public List<string> ErrorMessageList { get; set; }
		public List<string> SuccessMessageList { get; set; }
		public int Id { get; set; }
		public bool DuplicateEmployeeError { get; set; }
		public string DuplicateEmployeeErrorMessage { get; set; }
		public bool PrWithIn2Years { get; set; }
		public string PrWithin2YearsAlertMessage { get; set; }
	}
}
﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.LocationGroup;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags.Dtos;

namespace DinePlan.DineConnect.Common.Dto
{
    public class CommonLocationGroupDto
    {
        public CommonLocationGroupDto()
        {
            Locations = new List<SimpleLocationDto>();
            Groups = new List<SimpleLocationGroupDto>();
            Group = false;
            LocationTags = new List<SimpleLocationTagDto>();
            LocationTag = false;
            NonLocations = new List<SimpleLocationDto>();
        }

        public List<SimpleLocationDto> Locations { get; set; }
        public List<SimpleLocationGroupDto> Groups { get; set; }
        public List<SimpleLocationTagDto> LocationTags { get; set; }
        public List<SimpleLocationDto> NonLocations { get; set; }
        public bool Group { get; set; }
        public bool LocationTag { get; set; }
        public long UserId { get; set; }
    }
}
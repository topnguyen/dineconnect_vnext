﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Common.Dto
{
    public class CommonLocationGroupFilterInputDto : PagedAndSortedResultRequestDto
    {
        public string LocationIds { get; set; }

        public bool LocationGroup { get; set; }
        public bool LocationTag { get; set; }
        public bool NonLocation { get; set; }

        public List<int> ParseLocationIds()
        {
            var listId = new List<int>();
            if (!string.IsNullOrEmpty(LocationIds))
            {
                var idStringArr = LocationIds.Split(',');

                foreach (var idString in idStringArr)
                {
                    if (int.TryParse(idString, out var id))
                    {
                        listId.Add(id);
                    }
                }
            }

            return listId;
        }
    }
}
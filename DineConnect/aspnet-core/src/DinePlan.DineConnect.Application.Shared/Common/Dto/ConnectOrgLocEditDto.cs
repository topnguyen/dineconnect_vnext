﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Common.Dto
{
    public class ConnectOrgLocEditDto
    {
        public string Locations { get; set; }
        public bool Group { get; set; }
        public string NonLocations { get; set; }
        public virtual bool LocationTag { get; set; }
    }
}
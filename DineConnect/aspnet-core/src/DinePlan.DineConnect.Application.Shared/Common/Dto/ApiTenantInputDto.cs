﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Common.Dto
{
    public class ApiTenantInputDto: IInputDto
    {
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Editions.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false);

        Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input);

        GetDefaultEditionNameOutput GetDefaultEditionName();

        Task<ListResultDto<ComboboxItemDto>> GetProductGroupsForCombobox(string input);

        string GetConditionFilter();

        Task<ListResultDto<ComboboxItemDto>> GetDepartmentForLookupTable();
    }
}
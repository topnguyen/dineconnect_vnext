﻿namespace DinePlan.DineConnect.WebHooks.Dto
{
    public class GetAllSendAttemptsOfWebhookEventInput
    {
        public string Id { get; set; }
    }
}

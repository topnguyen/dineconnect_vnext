﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.WebHooks.Dto
{
    public class GetAllSendAttemptsInput : PagedInputDto
    {
        public string SubscriptionId { get; set; }
    }
}

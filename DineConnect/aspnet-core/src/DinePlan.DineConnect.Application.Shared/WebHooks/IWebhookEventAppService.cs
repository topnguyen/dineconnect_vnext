﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace DinePlan.DineConnect.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}

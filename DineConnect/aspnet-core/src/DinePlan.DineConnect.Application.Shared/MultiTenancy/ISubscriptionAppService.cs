﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace DinePlan.DineConnect.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}

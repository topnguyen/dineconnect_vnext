using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Editions.Dto;
using DinePlan.DineConnect.MultiTenancy.Dto;

namespace DinePlan.DineConnect.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}
﻿namespace DinePlan.DineConnect.MultiTenancy.Dto
{
    public class PaymentInfoInput
    {
        public int? UpgradeEditionId { get; set; }
    }
}

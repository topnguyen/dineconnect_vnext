﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.MultiTenancy;
using DinePlan.DineConnect.Tenants.TenantDatabases.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.MultiTenancy.Dto
{
    public class TenantEditDto : EntityDto
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(TenantConsts.MaxNameLength)]
        public string Name { get; set; }

        [DisableAuditing]
        public string ConnectionString { get; set; }

        public int? EditionId { get; set; }

        public bool IsActive { get; set; }

        public DateTime? SubscriptionEndDateUtc { get; set; }

        public bool IsInTrialPeriod { get; set; }

        public int? TenantDatabaseId { get; set; }

        public TenantDatabaseDto TenantDatabase { get; set; }

    }
}
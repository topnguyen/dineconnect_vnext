﻿namespace DinePlan.DineConnect.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeCreateSubscriptionInput
    {
        public string StripeSessionId { get; set; }
    }
}
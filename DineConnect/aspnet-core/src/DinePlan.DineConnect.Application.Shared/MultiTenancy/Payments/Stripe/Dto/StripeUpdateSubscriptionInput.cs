﻿namespace DinePlan.DineConnect.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeUpdateSubscriptionInput
    {
        public long PaymentId { get; set; }
    }
}
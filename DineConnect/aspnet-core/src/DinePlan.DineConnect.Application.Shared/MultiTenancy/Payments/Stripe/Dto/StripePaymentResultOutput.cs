﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.MultiTenancy.Payments.Stripe.Dto
{
    public class StripePaymentResultOutput
    {
        public bool PaymentDone { get; set; }
    }
}

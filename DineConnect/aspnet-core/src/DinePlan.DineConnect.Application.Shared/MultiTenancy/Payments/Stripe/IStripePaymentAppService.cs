﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.MultiTenancy.Payments.Dto;
using DinePlan.DineConnect.MultiTenancy.Payments.Stripe.Dto;

namespace DinePlan.DineConnect.MultiTenancy.Payments.Stripe
{
    public interface IStripePaymentAppService : IApplicationService
    {
        Task ConfirmPayment(StripeConfirmPaymentInput input);

        StripeConfigurationDto GetConfiguration();

        Task<SubscriptionPaymentDto> GetPaymentAsync(StripeGetPaymentInput input);

        Task<string> CreatePaymentSession(StripeCreatePaymentSessionInput input);
    }
}
﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.MultiTenancy.Payments.PayPal.Dto;

namespace DinePlan.DineConnect.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}

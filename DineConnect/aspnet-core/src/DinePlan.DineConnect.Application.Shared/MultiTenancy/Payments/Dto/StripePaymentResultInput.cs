﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.MultiTenancy.Payments.Dto
{
    public class StripePaymentResultInput
    {
        public long PaymentId { get; set; }
    }
}

﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Organizations.Dto
{
    public class FindOrganizationUnitUsersInput : PagedAndFilteredInputDto
    {
        public long OrganizationUnitId { get; set; }
    }
}

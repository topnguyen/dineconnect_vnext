﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Organizations.Dto
{
    public class FindOrganizationUnitRolesInput : PagedAndFilteredInputDto
    {
        public long OrganizationUnitId { get; set; }
    }
}
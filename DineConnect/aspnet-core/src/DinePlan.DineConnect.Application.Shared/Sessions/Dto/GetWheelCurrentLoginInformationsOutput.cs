﻿using DinePlan.DineConnect.UiCustomization.Dto;

namespace DinePlan.DineConnect.Sessions.Dto
{
    public class GetWheelCustomerLoginInformationsOutput
    {
        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public string CustomerGuid { get; set; }

        public long? UserId { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Schedule.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu.MenuItems.Dtos;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;

namespace DinePlan.DineConnect.Tiffins.Schedule
{
    public interface IScheduleAppService : IApplicationService
    {
        Task<int> Create(CreateScheduleDto input);

        Task<bool> CreateListSchedule(List<CreateScheduleDto> lst);

        Task<PagedResultDto<CreateScheduleDto>> GetAll(GetScheduleInputDto input);

        Task<CreateScheduleDto> GetDataForEdit(int id);

        Task Delete(int Id);

        Task BulkDelete(List<int> ids);

        Task UpdateList(List<CreateScheduleDto> lst);

        Task<List<GetScheduleForMenu>> GetScheduleForMenu(GetScheduleInputDto input);

        Task<FileDto> GetScheduleToExcel(GetScheduleInputDto input);
        
        Task<FileDto> GetImportScheduleTemplateToExcel();

        Task ImportScheduleToDatabase(string fileToken);

        Task UpdateStatus(UpdateScheduleStatusDto input);
        
        Task<PagedResultDto<ProductSetProductDto>> GetMenuItemForMulti(GetMenuItemForProductSetInputDto input);
    }
}
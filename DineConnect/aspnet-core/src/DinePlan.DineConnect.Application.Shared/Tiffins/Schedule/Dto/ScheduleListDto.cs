﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
   public class ScheduleListDto :EntityDto
    {
        public DateTime Date { get; set; }
        public string ProductSet { get; set; }
        public string Products { get; set; }
    }
}

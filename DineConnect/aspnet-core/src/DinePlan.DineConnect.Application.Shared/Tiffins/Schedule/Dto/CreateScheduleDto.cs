﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
    public class CreateScheduleDto : EntityDto<int?>, IShouldNormalize
    {
        public ScheduleDetailStatus Status { get; set; } = ScheduleDetailStatus.Draft;

        public DateTime Date { get; set; }

        public int MealTimeId { get; set; }
        public string MealTimeName { get; set; }

        public int? TenantId { get; set; }

        public int ProductSetId { get; set; }

        public string ProductSetName { get; set; }

        public string DayOfWeek { get; set; }
        public string Color { get; set; }
        public int Ordinal { get; set; }

        public string Images { get; set; }

        public string DateDayOfWeek
        {
            get
            {
                return Date.DayOfWeek.ToString();
            }
        }

        public List<ProductSetProductDto> Products { get; set; }

        public void Normalize()
        {
            if (Products == null)
            {
                Products = new List<ProductSetProductDto>();
            }
        }
    }
}
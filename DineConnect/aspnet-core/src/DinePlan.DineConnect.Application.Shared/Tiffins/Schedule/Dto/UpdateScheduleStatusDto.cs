﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
    public class UpdateScheduleStatusDto
    {
        public List<int> Ids { get; set; }

        public ScheduleDetailStatus NewStatus { get; set; } = ScheduleDetailStatus.Draft;
    }
}
﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
    public class GetScheduleForMenu
    {
        public string ProductSetName { get; set; }
        public string Color { get; set; }
        public int Ordinal { get; set; }
        public int ProductSetId { get; set; }

        public List<CreateScheduleDto> ListSchedule { get; set; }

        public GetScheduleForMenu()
        {
            ListSchedule = new List<CreateScheduleDto>();
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
    public class ScheduleMenuForViewDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<GetScheduleForMenu> Schedules { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Schedule.Dto
{
   public class GetScheduleInputDto: PagedAndSortedResultRequestDto, IShouldNormalize
    {

        public int? ProductSetId { get; set; }

        public int? ProductId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public ScheduleDetailStatus? Status { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Schedule.Date";
            }
        }

    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Orders
{
    public interface ITiffinOrderAppService : IApplicationService
    {
        Task CreateOrder(CreateOrderInput input);

        Task<ListResultDto<ComboboxItemDto>> GetOfferByCustomerId(int customerId);

        Task<PagedResultDto<ProducSetForOrderDto>> GetProductSets(GetProductSetForOrderInput input);

        Task<PagedResultDto<OrderHistoryDto>> GetOrderHistory(GetOrderHistoryInputDto input);

        Task<ListResultDto<PickUpReportDto>> GetPickUpOrderReport(GetPickUpOrderReportInputDto input);

        Task SendPickUpOrderRemindEmail(SendPickUpOrderRemindInput input);

        Task ChangePickupStatus(ChangePickupStatus input);

        Task<FileDto> ExportOrderHistory();

        Task<ListResultDto<ComboboxItemDto>> GetScheduleTimes();

        Task<List<CreateOrderInput>>  CreateListOrder(List<CreateOrderInput> orders);

        Task<FileDto> PrintOrderHistory(long orderId);

        FileDto PrintOrderDetail(PreparationReportViewDto input);

        Task<List<UpdateMyAddressInputDto>> GetCustomerAddresses();

        Task<UpdateMyAddressInputDto> GetCustomerAddressesById(int addressId);

        Task<DateTime?> GetCutOffTime();

        Task<bool> GetCutOffProceed(DateTime orderDate);
        
        Task ValidateCutOff(List<DateTime> orderDate);

        Task<CreateOrderInput> GetOrderForEdit(int id);

        Task UpdateDeliveryInfo(CreateOrderInput input);

        Task DeleteOrder(EntityDto input);

        Task<ListResultDto<AddonListDto>> GetAllOrderTags();

        Task<PaymentOutputDto> CreatePaymentForOrder(CreateOrderPaymentInputDto input);

        Task<FileDto> PrintOrderHistories(List<long> orderIds);

        Task<List<OrderHistoryForPrintDto>> GetOrderDetailsForPrintPreparationReport(List<long> orderIds);
        
        Task<PaymentOutputDto> ValidatePayment( EntityDto<int> input);
    }
}
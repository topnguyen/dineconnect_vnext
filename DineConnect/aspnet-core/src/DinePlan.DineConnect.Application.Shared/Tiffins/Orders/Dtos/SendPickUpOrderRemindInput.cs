﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class SendPickUpOrderRemindInput
    {
        public List<int> OrderIds { get; set; }
    }
}

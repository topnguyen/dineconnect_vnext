﻿using System;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class GetPickUpOrderReportInputDto
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}

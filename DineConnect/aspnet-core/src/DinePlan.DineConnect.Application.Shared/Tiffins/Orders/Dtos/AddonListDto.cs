﻿namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class AddonListDto
    {
        public int Value { get; set; }

        public string Label { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
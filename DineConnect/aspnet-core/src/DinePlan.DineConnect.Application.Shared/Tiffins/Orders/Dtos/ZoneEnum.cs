﻿namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public enum ZoneEnum
    {
        North = 1,
        NorthWest = 2,
        NorthEast = 3,
        Central = 4,
        West = 5,
        East = 6,
        SouthCbd = 7
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class BuyOffersInputDto
    {
        public InvoiceInfo InvoiceInfo { get; set; }

        public List<int> OfferList { get; set; }
    }
}

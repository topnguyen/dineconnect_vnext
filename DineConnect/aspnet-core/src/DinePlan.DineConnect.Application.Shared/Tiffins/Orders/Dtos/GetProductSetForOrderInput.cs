﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class GetProductSetForOrderInput
    {
        public long? CustomerOfferId { get; set; }
        public int? ProductSetId { get; set; }

        public DateTime Date { get; set; }
        public int? ScheduleType { get; set; }

        public bool IsEdit { get; set; }

        public List<OfferWithQuantityForOrderDto> AddedOrders { get; set; }

        public GetProductSetForOrderInput()
        {
            AddedOrders = new List<OfferWithQuantityForOrderDto>();
        }
    }
}
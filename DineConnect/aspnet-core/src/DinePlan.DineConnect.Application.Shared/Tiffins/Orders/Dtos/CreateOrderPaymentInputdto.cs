﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class CreateOrderPaymentInputDto
    {
        public CreateOrderPaymentInputDto()
        {
            PurcharsedOrderList = new List<CreateOrderInput>();
        }

        public List<CreateOrderInput> PurcharsedOrderList { get; set; }


        public BillingInfo BillingInfo { get; set; }

        public string PaymentOption { get; set; }

        public decimal? PaidAmount { get; set; }


        public string CallbackUrl { get; set; }

        public List<PromotionCodeDiscountDetailPaymentDto> DiscountDetails { get; set; }

        // Omise

        public string OmiseToken { get; set; }

        public string Source { get; set; }


        // Adyen

        public string AdyenEncryptedCardNumber { get; set; }

        public string AdyenEncryptedExpiryMonth { get; set; }

        public string AdyenEncryptedExpiryYear { get; set; }

        public string AdyenEncryptedSecurityCode { get; set; }

        // Paypal

        public string PaypalOrderId { get; set; }

        // Paypal
        public string StripeToken { get; set; }
        
        // Google

        public string GooglePayToken { get; set; }

        // Swipe

        public string SwipeOTP { get; set; }

        public string SwipeCardNumber { get; set; }
    }
}
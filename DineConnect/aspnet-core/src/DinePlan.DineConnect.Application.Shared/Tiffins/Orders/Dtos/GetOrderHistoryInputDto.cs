﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class GetOrderHistoryInputDto : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public int? CustomerId { get; set; }

        public bool? PickedUp { get; set; }

        public string Term { get; set; }

        public DateTime? OrderDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "OrderDate";
            }
        }
    }
}

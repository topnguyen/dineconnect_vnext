﻿namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class ChangePickupStatus
    {
        public int OrderId { get; set; }

        public bool IsPickup { get; set; }
    }
}

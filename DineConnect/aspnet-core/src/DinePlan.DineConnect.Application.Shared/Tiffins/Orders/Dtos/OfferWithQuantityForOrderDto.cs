﻿namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class OfferWithQuantityForOrderDto
    {
        public long CustomerProductOfferId { get; set; }
        public long ProductSetId { get; set; }
        public int Quantity { get; set; }
    }
}
﻿using System;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class CreateOrderInput
    {
        public int? Id { get; set; }
        public int CustomerProductOfferId { get; set; }

        public int ProductSetId { get; set; }

        public DateTime OrderDate { get; set; }
        public DateTime OrderDateFrom { get; set; }
        public DateTime OrderDateTo { get; set; }

        public int? Amount { get; set; }

        public int Quantity { get; set; } = 1;

        public string Address { get; set; }

        public int? CustomerAddressId { get; set; }

        public bool UseDefaultAddress { get; set; }

        public string ProductSetName { get; set; }

        public int MealTimeId { get; set; }

        public string MealTimeString { get; set; }

        public string AddOn { get; set; }

        public long? PaymentId { get; set; }

        public string MenuItemsDynamic { get; set; }

        public TiffinDeliveryTimeSlotType DeliveryType { get; set; }

        public long DeliveryCharge { get; set; }

        public string TimeSlotFormTo { get; set; }

        public int? SelfPickupLocationId { get; set; }
    }
}
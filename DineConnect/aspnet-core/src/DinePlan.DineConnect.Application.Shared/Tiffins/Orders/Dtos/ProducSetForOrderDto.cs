﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class ProducSetForOrderDto
    {
        public long? CustomerProductOfferId { get; set; }

        public string ProductSetName { get; set; }
        public string ProductSetNameExtra { get; set; }

        public List<NameValueDto> Products { get; set; }

        public int BalanceCredit { get; set; }
        public string Image { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime? ExpiryDate { get; set; }

        public int ProductSetId { get; set; }

        public List<ProducSetForOrderDto> ProducSets { get; set; }
    }
}
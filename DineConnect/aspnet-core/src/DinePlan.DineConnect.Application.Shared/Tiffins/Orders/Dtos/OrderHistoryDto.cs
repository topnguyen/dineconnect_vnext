﻿using System;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class OrderHistoryDto
    {
        public string ProductOfferName { get; set; }
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public long OrderId { get; set; }
        public string ChoiceOfSet { get; set; }
        public DateTime OrderDate { get; set; }
        public string MealTime { get; set; }
        public string DeliveryAddress { get; set; }
        public int Quantity { get; set; }

        public int CustomerProductOfferId { get; set; }

        public int? Amount { get; set; }

        public string Address { get; set; }

        public int? CustomerAddressId { get; set; }

        public int MealTimeId { get; set; }

        public string AddOn { get; set; }

        public long? PaymentId { get; set; }

        public string MenuItemsDynamic { get; set; }

        public TiffinDeliveryTimeSlotType DeliveryType { get; set; }

        public long DeliveryCharge { get; set; }

        public string TimeSlotFormTo { get; set; }

        public int? SelfPickupLocationId { get; set; }

        public DateTime? PickedUpTime { get; set; }

        public string JsonData { get; set; }
    }
}
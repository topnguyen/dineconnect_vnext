﻿using System;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class OrderHistoryForPrintDto
    {
        public string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public string DeliveryAddress { get; set; }

        public string FloorNumber { get; set; }

        public string UnitNumber { get; set; }

        public string PostalCode { get; set; }

        public string Zone { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string MealTime { get; set; }

        public string ProductSetName { get; set; }

        public int Quantity { get; set; }

        public string ProducOfferType { get; set; }
    }
}
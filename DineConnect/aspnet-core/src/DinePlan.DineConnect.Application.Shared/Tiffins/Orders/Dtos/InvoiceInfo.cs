﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class InvoiceInfo
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }
    }
}

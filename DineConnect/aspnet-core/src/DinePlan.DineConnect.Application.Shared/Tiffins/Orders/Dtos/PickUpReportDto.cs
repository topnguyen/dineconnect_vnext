﻿using System;

namespace DinePlan.DineConnect.Tiffins.Orders.Dtos
{
    public class PickUpReportDto
    {
        public DateTime Date { get; set; }

        public int TotalPickedUpOrder { get; set; }

        public int TotalNotPickedUpOrder { get; set; }
    }
}

﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Orders
{
    public interface ITiffinOrderExporter
    {
        Task<FileDto> ExportTiffinOrderSummary(List<OrderHistoryDto> input);

        FileDto PrintOrder(OrderHistoryForPrintDto orderHistory);

        FileDto PrintOrderDetail(PreparationReportViewDto input);

        Task<FileDto> PrintOrderDetailsAsZip(List<FileDto> files);
    }
}
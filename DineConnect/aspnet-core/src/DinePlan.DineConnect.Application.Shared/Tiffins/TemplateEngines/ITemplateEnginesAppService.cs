﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.TemplateEngines
{
    public interface ITemplateEnginesAppService : IApplicationService
    {
        Task<PagedResultDto<GetTemplateEngineForViewDto>> GetAll(GetAllTemplateEnginesInput input);

        Task<GetTemplateEngineForEditOutput> GetTemplateEngineForEdit(NullableIdDto input);

        Task CreateOrEdit(CreateOrEditTemplateEngineDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTemplateEnginesToExcel(GetAllTemplateEnginesForExcelInput input);

        ListResultDto<NameValueDto> GetTemplateVariables();

        ListResultDto<ComboboxItemDto> GetTemplateTypes();

        Task<CreateOrEditTemplateEngineDto> GetTemplateEngineByTypeForEdit(string input);
    }
}
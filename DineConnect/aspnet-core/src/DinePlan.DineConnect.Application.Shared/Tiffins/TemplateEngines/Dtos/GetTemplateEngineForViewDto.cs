﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class GetTemplateEngineForViewDto
    {
        public TemplateEngineDto TemplateEngine { get; set; }
    }
}
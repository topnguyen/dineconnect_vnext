﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class GetTemplateEngineForEditOutput
    {
        public CreateOrEditTemplateEngineDto TemplateEngine { get; set; }

        public GetTemplateEngineForEditOutput()
        {
            TemplateEngine = new CreateOrEditTemplateEngineDto();
        }
    }
}
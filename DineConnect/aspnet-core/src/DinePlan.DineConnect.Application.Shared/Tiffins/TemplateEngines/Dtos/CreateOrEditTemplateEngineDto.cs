﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class CreateOrEditTemplateEngineDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }

        public string Feature { get; set; }

        public string Module { get; set; }

        public string Variables { get; set; }

        public string Subject { get; set; }

        public virtual string ShortTemplate { get; set; }

        public string Template { get; set; }

        public List<string> TemplateVariables
        {
            get
            {
                if (!Variables.IsNullOrEmpty())
                    return Variables.Split(",").ToList();

                return new List<string>();
            }
        }
    }
}
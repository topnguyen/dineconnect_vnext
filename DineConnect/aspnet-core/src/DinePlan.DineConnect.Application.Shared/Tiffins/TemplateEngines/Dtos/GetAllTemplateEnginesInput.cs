﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class GetAllTemplateEnginesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
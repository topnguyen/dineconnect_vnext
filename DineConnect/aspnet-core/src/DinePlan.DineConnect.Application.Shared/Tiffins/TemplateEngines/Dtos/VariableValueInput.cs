﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class VariableValueInput
    {
        public string First_name { get; set; }

        public string Last_name { get; set; }
        public string Birthday { get; set; }
        public string Registered_date { get; set; }
        public string Registered_store_name { get; set; }
        public string Registered_employee_name { get; set; }
        public string Current_tier_name { get; set; }
        public string Point_balance { get; set; }
        public string Coupon_code { get; set; }
        public string Coupon_type { get; set; }
        public string Coupon_amount { get; set; }
        public string Coupon_expiry_date { get; set; }
        public string Tie_name { get; set; }
        public string Tie_desc { get; set; }
        public string Register_store_name { get; set; }
    }
}
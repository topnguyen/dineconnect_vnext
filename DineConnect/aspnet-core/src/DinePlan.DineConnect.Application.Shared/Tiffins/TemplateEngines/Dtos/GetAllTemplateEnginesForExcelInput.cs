﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class GetAllTemplateEnginesForExcelInput
    {
        public string Filter { get; set; }
    }
}
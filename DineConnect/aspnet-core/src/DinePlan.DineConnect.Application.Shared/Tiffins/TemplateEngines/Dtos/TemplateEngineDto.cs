﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.TemplateEngines.Dtos
{
    public class TemplateEngineDto : EntityDto
    {
        public string Name { get; set; }

        public string Feature { get; set; }

        public string Module { get; set; }

        public string Variables { get; set; }

        public string Subject { get; set; }
    }
}
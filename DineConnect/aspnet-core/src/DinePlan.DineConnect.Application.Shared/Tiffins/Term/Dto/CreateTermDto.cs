﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Term.Dto
{
   public class CreateTermDto: Entity
    {
        public int? TenantId { get; set; }
        public long Ordinal { get; set; }
        public string Content { get; set; }
    }
}

﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Term.Dto
{
   public class TermListDto: Entity
    {
        public long Ordinal { get; set; }
        public string Content { get; set; }
    }
}

﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.Term.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Term
{
    public interface ITermAppService: IApplicationService
    {
        Task<int> Create(CreateTermDto input);
        Task<PagedResultDto<TermListDto>> GetAll(GetTermInputDto input);
        Task<CreateTermDto> GetDataForEdit(int id);
        Task Delete(int id);
        Task Update(CreateTermDto input);
        Task<List<TermListDto>> GetAllForCustomer();
    }
}

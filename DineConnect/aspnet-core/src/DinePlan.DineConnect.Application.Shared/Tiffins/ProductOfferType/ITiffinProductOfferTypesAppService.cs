﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType
{
    public interface ITiffinProductOfferTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetTiffinProductOfferTypeForViewDto>> GetAll(GetAllTiffinProductOfferTypesInput input);

        Task<GetTiffinProductOfferTypeForEditOutput> GetTiffinProductOfferTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTiffinProductOfferTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTiffinProductOfferTypesToExcel(GetAllTiffinProductOfferTypesForExcelInput input);
    }
}
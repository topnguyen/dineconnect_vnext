﻿namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class GetTiffinProductOfferTypeForEditOutput
    {
        public CreateOrEditTiffinProductOfferTypeDto TiffinProductOfferType { get; set; }
    }
}
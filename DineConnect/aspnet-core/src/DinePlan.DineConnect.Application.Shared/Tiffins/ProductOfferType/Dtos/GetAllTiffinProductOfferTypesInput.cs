﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class GetAllTiffinProductOfferTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class GetAllTiffinProductOfferTypesForExcelInput
    {
        public string Filter { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class TiffinProductOfferTypeDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }
    }
}
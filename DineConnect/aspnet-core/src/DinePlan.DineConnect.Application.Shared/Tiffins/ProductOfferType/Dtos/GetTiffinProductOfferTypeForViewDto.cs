﻿namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class GetTiffinProductOfferTypeForViewDto
    {
        public TiffinProductOfferTypeDto TiffinProductOfferType { get; set; }
    }
}
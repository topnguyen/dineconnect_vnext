﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.ProductOfferType.Dtos
{
    public class CreateOrEditTiffinProductOfferTypeDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
    }
}
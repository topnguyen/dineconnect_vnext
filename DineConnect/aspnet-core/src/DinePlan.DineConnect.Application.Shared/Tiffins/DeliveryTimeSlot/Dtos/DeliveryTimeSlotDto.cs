﻿using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.Dtos
{
    public class DeliveryTimeSlotDto : EntityDto
    {
        public DayOfWeek? DayOfWeek { get; set; }

        public int MealTimeId { get; set; }

        public string MealTimeName { get; set; }

        public TiffinDeliveryTimeSlotType Type { get; set; }

        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public string TimeSlots { get; set; }
    }
}

﻿using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.Dtos
{
    public class GetDeliveryTimeSlotInputDto : PagedAndSortedResultRequestDto
    {
        public DayOfWeek? DayOfWeek { get; set; }

        public TiffinDeliveryTimeSlotType? Type { get; set; }
    }
}

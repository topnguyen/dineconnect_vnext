﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot.Dtos;

namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot
{
    public interface IDeliveryTimeSlotAppService
    {
        Task UpsertList(List<DeliveryTimeSlotDto> data);

        Task<PagedResultDto<DeliveryTimeSlotDto>> GetAll(GetDeliveryTimeSlotInputDto input);

        Task<DeliveryTimeSlotDto> GetByLocationAndMealTimeId(int locationId, int mealTimeId);

        Task Delete(int id);
    }
}

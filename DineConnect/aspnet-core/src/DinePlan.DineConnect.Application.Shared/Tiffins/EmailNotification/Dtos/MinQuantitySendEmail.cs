﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.EmailNotification.Dtos
{
    public class MinQuantitySendEmail
    {
        public string MealPlanName { get; set; }
        public string AvailableBalance { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}

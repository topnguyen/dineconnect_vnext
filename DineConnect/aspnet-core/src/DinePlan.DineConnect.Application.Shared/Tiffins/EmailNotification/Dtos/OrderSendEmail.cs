﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.EmailNotification.Dtos
{
    public class OrderSendEmail
    {
        public string DeliveryAddress { get; set; }
        public DateTime OrderDate { get; set; }
        public string ProductSetName { get; set; }
        public string Plan { get; set; }
        public string MealTime { get; set; }
        public int Quantity { get; set; }
        public string Addon { get; set; }
    }
}

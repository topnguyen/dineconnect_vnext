﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.EmailNotification.Dtos
{
    public class OrderMinQuantity
    {
        public string MealPlanName { get; set; }
        public int AvailableBalance { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}

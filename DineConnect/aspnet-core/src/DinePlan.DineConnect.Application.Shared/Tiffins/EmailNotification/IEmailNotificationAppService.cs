﻿using System.Collections.Generic;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;

namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public interface IEmailNotificationAppService
    {
        Task SendEmailWhenCustomerBuyMealPlan(TiffinPaymentDetailDto paymentDetail, int tenantId, FileDto fileAttach, CustomerAddressSendEmail customerAddress);
        Task SendEmailWhenCustomerOrder(List<OrderSendEmail> orderSendEmails, int tenantId, CustomerAddressSendEmail customerAddress);
        Task SendEmailWhenMinimumQuantity(OrderMinQuantity orderMinQuantity, int tenantId, CustomerAddressSendEmail customerAddress);

        Task SendEmailRemindBuyMealPlan(string customerEmail, string customerName, int tenantId);

        Task SendPickUpOrderRemind(string customerEmail, string customerName, int tenantId);

        Task SendPickUpOrderSuccess(string customerEmail, string customerName, int tenantId);
    }
}

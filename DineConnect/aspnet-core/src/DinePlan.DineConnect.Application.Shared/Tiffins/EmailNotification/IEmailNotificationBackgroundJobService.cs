﻿using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Tiffins.EmailNotification.Dtos;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;

namespace DinePlan.DineConnect.Tiffins.EmailNotification
{
    public interface IEmailNotificationBackgroundJobService
    {
        Task SendEmailWhenCustomerBuyMealPlanInBackground(long paymentId,TiffinPaymentDetailDto paymentDetail,CompanySettingDto companyInfo);
        Task SendEmailWhenCustomerOrderInBackground(List<CreateOrderInput> orderInputs,CompanySettingDto companyInfo);
        Task SendEmailWhenMinimumQuantityInBackground(OrderMinQuantity orderMinQuantity,CompanySettingDto companyInfo);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public interface ITiffinPaymentExporter
    {
        FileDto PrintPayment(List<PaymentCustomerHistoryDto> payment);

        FileDto PrintPaymentDetail(TiffinPaymentDetailDto paymentDetail, CompanySettingDto companyInfo);

        FileDto GenerateInvoice(TiffinPaymentDetailDto paymentDetail, CompanySettingDto companyInfo);
    }
}

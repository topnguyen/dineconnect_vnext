﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;

namespace DinePlan.DineConnect.Tiffins.Customer.Dto
{
    public class CustomerListDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public long UserId { get; set; }

        public bool AcceptReceivePromotionEmail { get; set; }

        public string JsonData { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public List<UpdateMyAddressInputDto> CustomerAddressDetails { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Customer.Dto
{
    public class CreateCustomerDto : EntityDto<int>
    {
        public int? TenantId { get; set; }

        public long UserId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public int? CityId { get; set; }

        public int? CountryId { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string ReferrerReferralCode { get; set; }

        public string JsonData { get; set; }

        public List<UpdateMyAddressInputDto> CustomerAddressDetails { get; set; }
    }
}
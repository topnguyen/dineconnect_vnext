﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Customer
{
    public interface ICustomerAppService : IApplicationService
    {
        Task<PagedResultDto<CustomerListDto>> GetAll(GetAllCustomerInputDto input);

        Task<CreateCustomerDto> GetCustomerForEdit(NullableIdDto input);
        Task<CreateCustomerDto> GetCustomerForEditByUserId(NullableIdDto input);

        Task<CreateCustomerDto> GetCustomerWithUserId(int userId);

        Task CreateOrEdit(CreateCustomerDto input);

        Task Delete(int id);

        Task<FileDto> GetCustomersToExcel(GetAllCustomerInputDto input);

        Task UpdateLastLoginTime(long userId);
        Task<int> UpdateUserAndCreateOrEditCustomer(CreateCustomerDto input);
    }
}
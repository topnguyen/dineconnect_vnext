﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.MealTimes.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.MealTimes
{
    public interface ITiffinMealTimesAppService : IApplicationService
    {
        Task<PagedResultDto<TiffinMealTimeDto>> GetAll(GetAllTiffinMealTimesInput input);

        Task<CreateOrEditTiffinMealTimeDto> GetTiffinMealTimeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTiffinMealTimeDto input);

        Task Delete(EntityDto input);
    }
}
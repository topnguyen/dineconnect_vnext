﻿namespace DinePlan.DineConnect.Tiffins.MealTimes.Dtos
{
    public class GetTiffinMealTimeForViewDto
    {
        public TiffinMealTimeDto TiffinMealTime { get; set; }
    }
}
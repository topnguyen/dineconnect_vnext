﻿namespace DinePlan.DineConnect.Tiffins.MealTimes.Dtos
{
    public class GetTiffinMealTimeForEditOutput
    {
        public CreateOrEditTiffinMealTimeDto TiffinMealTime { get; set; }
    }
}
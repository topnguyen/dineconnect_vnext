﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.MealTimes.Dtos
{
    public class TiffinMealTimeDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string Modifier { get; set; }
        
        /// <summary>
        ///     Default selection meal time for Customer
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
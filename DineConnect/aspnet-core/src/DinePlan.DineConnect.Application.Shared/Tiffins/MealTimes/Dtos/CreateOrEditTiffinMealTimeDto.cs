﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.MealTimes.Dtos
{
    public class CreateOrEditTiffinMealTimeDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        /// <summary>
        ///     Default selection meal time for Customer
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
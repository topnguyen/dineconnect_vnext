﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.MealTimes.Dtos
{
    public class GetAllTiffinMealTimesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
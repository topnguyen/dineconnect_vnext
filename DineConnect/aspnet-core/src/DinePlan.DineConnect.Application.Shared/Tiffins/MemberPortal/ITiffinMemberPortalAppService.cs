﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.MemberPortal.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.PaymentTypes.Dtos;
using DinePlan.DineConnect.PaymentProcessor.Dto.OmisePaymentDto;

namespace DinePlan.DineConnect.Tiffins.MemberPortal
{
    public interface ITiffinMemberPortalAppService : IApplicationService
    {
        Task<MyInfoDto> GetMyInfo();

        Task<List<UpdateMyAddressInputDto>> GetAllAddress();

        Task<UpdateMyAddressInputDto> GetMyAddress(long addressId);

        Task DeleteAddress(long addressId);

        Task UpdateMyBasicInfo(UpdateMyInfoDto input);

        Task AddOrUpdateMyAddress(UpdateMyAddressInputDto input);

        Task SetDefaultAddress(long addressId);

        Task UpdateCountry(int countryId);

        Task<ListResultDto<TiffinMemberProductOfferDto>> GetActiveOffers(GetAllTiffinProductOffersInput input);

        Task<PagedResultDto<BalanceDetailDto>> GetBalancesForCustomer(GetBalanceInput input);

        Task<PagedResultDto<PaymentCustomerHistoryDto>> GetPaymentHistory(GetPaymentInput input);

        Task<TiffinPaymentDetailDto> GetPaymentDetail(long id);

        Task<PaymentOutputDto> CreatePayment(CreatePaymentInputDto input);

        Task<string> CreateOmisePaymentNowRequest(long amount, string currency);

        Task<OmisePaymentResponseDto> GetOmisePaymentStatus(string chargeId);

        Task<TotalOfferDto> GetTotalOffer();

        Task<FileDto> PrintPaymentHistory();

        Task<FileDto> PrintPaymentDetailHistory(long paymentId);

        Task<CompanySettingDto> GetCompanyInfo();

        Task<PaymentOutputDto> ValidatePayment( EntityDto<int> input);
        Task SendFeedback(FeedbackEmailInput feedback);


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DinePlan.DineConnect.MultiTenancy.Payments.Stripe.Dto;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class UpdateMyInfoDto
    {
        [Required]
        public string Name { get; set; }

        public string Surname { get; set; }

        public string PhoneNumber { get; set; }

        public string WhatAppNumber { get; set; }

        public bool IsDisableDailyOrderReminder { get; set; }

        public string CartJsonData { get; set; }

        public string JsonData { get; set; }
    }
}

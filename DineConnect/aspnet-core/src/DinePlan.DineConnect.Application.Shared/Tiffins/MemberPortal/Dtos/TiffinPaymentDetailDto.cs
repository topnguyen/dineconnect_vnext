﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class TiffinPaymentDetailDto
    {
        public long Id { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal? PaidAmount { get; set; }

        public decimal? CardCharge { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string PaymentOption { get; set; }

        public string EmailAddress { get; set; }

        public string CustomerTaxNo { get; set; }

        public List<CustomerProductOfferDetailDto> Products { get; set; }
    }

    public class CustomerProductOfferDetailDto
    {
        public string ProductOfferName { get; set; }

        public decimal? PaidAmount { get; set; }

        public int Quantity { get; set; }

        public decimal? Price { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class GetBalanceInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public long? CustomerId { get; set; }

        public bool? IsExpiryOffer { get; set; }
        public string FilterText { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Type";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class GetPaymentInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public string PaymentMethodText { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "InvoiceNo";
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class CreatePaymentInputDto
    {
        public CreatePaymentInputDto()
        {
            PurcharsedOfferList = new List<TiffinMemberProductOfferDto>();
        }
        public BillingInfo BillingInfo { get; set; }
        
        public List<TiffinMemberProductOfferDto> PurcharsedOfferList { get; set; }

        [Required]
        public string PaymentOption { get; set; }

        [Required]
        public decimal? PaidAmount { get; set; }

        
        public string CallbackUrl { get; set; }

        public List<PromotionCodeDiscountDetailPaymentDto> DiscountDetails { get; set; }

        // Omise

        public string OmiseToken { get; set; }

        public string Source { get; set; }


        // Adyen

        public string AdyenEncryptedCardNumber { get; set; }

        public string AdyenEncryptedExpiryMonth { get; set; }

        public string AdyenEncryptedExpiryYear { get; set; }

        public string AdyenEncryptedSecurityCode { get; set; }

        // Paypal

        public string PaypalOrderId { get; set; }

        // Stripe
        public string StripeToken { get; set; }

        // Google

        public string GooglePayToken { get; set; }

        // Swipe

        public string SwipeOTP { get; set; }

        public string SwipeCardNumber { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class TotalOfferDto
    {
        public int TotalOffer { get; set; }
        public decimal? TotalAmount { get; set; }
    }
}

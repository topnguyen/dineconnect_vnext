﻿using System;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class BalanceDetailDto
    {
        public int CustomerOfferOrderId { get; set; }
        
        public DateTime CustomerOfferOrderDate { get; set; }
        
        public int TypeId { get; set; }
        
        public string Type { get; set; }
        
        public int OfferId { get; set; }
        
        public string Offer { get; set; }
        public int? ActualCredit { get; set; }
        public int? CreditBalance { get; set; }
        public decimal? PaidAmount { get; set; }
        public bool Expiry { get; set; }
        public string OrderHistory { get; set; }

        public int DaysOnFirstOrder { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
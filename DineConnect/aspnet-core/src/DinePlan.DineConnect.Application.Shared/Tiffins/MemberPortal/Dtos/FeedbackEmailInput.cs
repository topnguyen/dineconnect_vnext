﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class FeedbackEmailInput
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Feedback { get; set; }
        public string Subject { get; set; }

    }
}

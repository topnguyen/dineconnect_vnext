﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class MyInfoDto
    {
        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public int? CityId { get; set; }

        public int? CountryId { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string PhoneNumber { get; set; }

        public string WhatAppNumber { get; set; }

        public int? Property { get; set; }

        public int CallingCode { get; set; }

        public long DefaultAddresId { get; set; }

        //public UpdateMyAddressInputDto DefaultAddress { get; set; }

        public string ReferralCode { get; set; }

        public bool IsDisableDailyOrderReminder { get; set; }

        public string CartJsonData { get; set; }


        public string JsonData { get; set; }
    }
}
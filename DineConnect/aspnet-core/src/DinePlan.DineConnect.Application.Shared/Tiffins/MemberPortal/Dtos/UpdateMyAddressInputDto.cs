﻿using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class UpdateMyAddressInputDto
    {
        public long? AddressId { get; set; }

        public string Name { get; set; }

        [Required]
        public string Address1 { get; set; }

        [Required]
        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string PhoneNumber { get; set; }

        public string WhatAppNumber { get; set; }

        public int CallingCode { get; set; }

        public int? CityId { get; set; }

        public int? CountryId { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public int? Property { get; set; }

        public bool IsDefault { get; set; }

        public string CityName { get; set; }

        public string CountryName { get; set; }

        public string FullAddress => $"+{CallingCode}{PhoneNumber}, {WhatAppNumber}, {Address1}, {Address2}, {Address3}, {CityName}, {CountryName}";

        public int? CustomerId { get; set; }
    }
}
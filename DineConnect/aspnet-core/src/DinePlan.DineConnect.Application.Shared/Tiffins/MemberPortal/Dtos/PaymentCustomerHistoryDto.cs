﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class PaymentCustomerHistoryDto
    {
        public long Id { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal? PaidAmount { get; set; }

        public string InvoiceNo { get; set; }

        public string ModeOfPayment { get; set; }
    }
}

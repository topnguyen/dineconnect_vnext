﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class BillingInfo
    {
        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public int? CityId { get; set; }

        public int? CountryId { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public string GetFullAddress()
        {
            return $"{Address1}, {Address2}, {Address3}";
        }
    }
}

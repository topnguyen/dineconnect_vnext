﻿namespace DinePlan.DineConnect.Tiffins.MemberPortal.Dtos
{
    public class PaymentOutputDto
    {
        public bool PurchasedSuccess { get; set; }
        
        public long PaymentId { get; set; }

        public string InvoiceNo { get; set; }
        
        public string AuthenticateUri { get; set; }
        
        public string Message { get; set; }
        
        public string OmiseId { get; set; }
        
        public string ScanCode { get; set; }

        public PaymentOutputDto()
        {
            InvoiceNo = "";
        }
    }
}

﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class TiffinProductOfferDto : EntityDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int TotalDeliveryCount { get; set; }

        public bool Expiry { get; set; }

        public int DaysOnFirstOrder { get; set; }

        public int ProductSetId { get; set; }

        public int TiffinProductOfferTypeId { get; set; }

        public string ProductOfferTypeName { get; set; }
        public string Color { get; set; }

        public int MinimumOrderQuantity { get; set; }
        public int SortOrder { get; set; }
        public bool InActive { get; set; }

        public List<TiffinProductOfferLookupTableDto> ProductSets { get; set; }

        public string ProductSetName
        {
            get
            {
                if (ProductSets.Any()) { return ProductSets.Select(e => e.Name).JoinAsString(", "); }
                return null;
            }
        }
    }
}
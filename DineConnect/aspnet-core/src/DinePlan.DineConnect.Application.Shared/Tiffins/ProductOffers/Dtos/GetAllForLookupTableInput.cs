﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
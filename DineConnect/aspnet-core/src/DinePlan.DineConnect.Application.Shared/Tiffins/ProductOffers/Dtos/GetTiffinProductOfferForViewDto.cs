﻿namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class GetTiffinProductOfferForViewDto
    {
        public TiffinProductOfferDto TiffinProductOffer { get; set; }

        public string TiffinProductSetName { get; set; }

        public string TiffinProductOfferTypeName { get; set; }
    }
}
﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class CreateOrEditTiffinProductOfferDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Images { get; set; }

        public decimal Price { get; set; }

        public string Tag { get; set; }

        public int TotalDeliveryCount { get; set; }

        public bool Expiry { get; set; }

        public int DaysOnFirstOrder { get; set; }

        public int? TiffinProductOfferTypeId { get; set; }

        public string UploadedImageToken { get; set; }

        public string Color { get; set; }

        public int MinimumOrderQuantity { get; set; }

        public int SortOrder { get; set; }
        public bool InActive { get; set; }
        public List<TiffinProductOfferLookupTableDto> ProductSets { get; set; }

        public CreateOrEditTiffinProductOfferDto()
        {
            ProductSets = new List<TiffinProductOfferLookupTableDto>();
        }
    }
}
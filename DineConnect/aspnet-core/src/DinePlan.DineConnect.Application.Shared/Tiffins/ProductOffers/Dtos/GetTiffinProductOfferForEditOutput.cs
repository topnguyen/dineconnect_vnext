﻿namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class GetTiffinProductOfferForEditOutput
    {
        public GetTiffinProductOfferForEditOutput()
        {
            TiffinProductOffer = new CreateOrEditTiffinProductOfferDto();
        }

        public CreateOrEditTiffinProductOfferDto TiffinProductOffer { get; set; }

        public string TiffinProductSetName { get; set; }

        public string TiffinProductOfferTypeName { get; set; }
    }
}
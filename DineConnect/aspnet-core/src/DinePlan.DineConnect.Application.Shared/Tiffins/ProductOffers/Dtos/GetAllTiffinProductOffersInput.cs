﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class GetAllTiffinProductOffersInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string TiffinProductSetNameFilter { get; set; }

		 		 public string TiffinProductOfferTypeNameFilter { get; set; }

		 
    }
}
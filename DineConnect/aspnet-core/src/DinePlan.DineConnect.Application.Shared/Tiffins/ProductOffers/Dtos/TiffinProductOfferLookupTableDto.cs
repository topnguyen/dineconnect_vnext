﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.ProductOffers.Dtos
{
    public class TiffinProductOfferLookupTableDto : NameValueDto
    {
        public int Id { get; set; }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.ProductOffers.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.ProductOffers
{
    public interface ITiffinProductOffersAppService : IApplicationService
    {
        Task<PagedResultDto<GetTiffinProductOfferForViewDto>> GetAll(GetAllTiffinProductOffersInput input);

        Task<GetTiffinProductOfferForEditOutput> GetTiffinProductOfferForEdit(NullableIdDto input);

        Task CreateOrEdit(CreateOrEditTiffinProductOfferDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTiffinProductOffersToExcel(GetAllTiffinProductOffersForExcelInput input);

        Task<PagedResultDto<TiffinProductOfferLookupTableDto>> GetAllTiffinProductSetForLookupTable(GetAllForLookupTableInput input);

        Task<PagedResultDto<TiffinProductOfferLookupTableDto>> GetAllTiffinProductOfferTypeForLookupTable(GetAllForLookupTableInput input);
    }
}
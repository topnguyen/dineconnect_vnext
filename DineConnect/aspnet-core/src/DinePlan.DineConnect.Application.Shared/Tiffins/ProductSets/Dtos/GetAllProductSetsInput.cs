﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class GetAllProductSetsInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}
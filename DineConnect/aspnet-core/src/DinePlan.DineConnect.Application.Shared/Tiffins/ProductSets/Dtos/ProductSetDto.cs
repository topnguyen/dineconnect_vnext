﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class ProductSetDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Images { get; set; }

        public string Tag { get; set; }

        public string[] Tags { get; set; }

        public string Color { get; set; }
        public int Ordinal { get; set; }
    }
}
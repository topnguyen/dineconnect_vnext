﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class CreateOrEditProductSetDto : EntityDto<int?>, IShouldNormalize
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Images { get; set; }

        public string[] Tags { get; set; }

        public string UploadedImageToken { get; set; }
        public string Color { get; set; }
        public int Ordinal { get; set; }

        public List<ProductSetProductDto> Products { get; set; }

        public void Normalize()
        {
            if (Tags == null)
            {
                Tags = new string[0];
            }

            if (Products == null)
            {
                Products = new List<ProductSetProductDto>();
            }
        }
    }
}
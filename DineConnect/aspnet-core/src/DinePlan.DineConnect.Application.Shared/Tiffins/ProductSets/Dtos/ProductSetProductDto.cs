﻿using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class ProductSetProductDto
    {
        public int? Id { get; set; }

        public int ScheduleId { get; set; }
        
        public int MealTimeId { get; set; }
        
        public int ProductId { get; set; }

        public string ProductName { get; set; }
        
        public string ProductImage { get; set; }
        
        public string ProductDescription { get; set; }
        
        public DateTime CreationTime { get; set; }
    }
}
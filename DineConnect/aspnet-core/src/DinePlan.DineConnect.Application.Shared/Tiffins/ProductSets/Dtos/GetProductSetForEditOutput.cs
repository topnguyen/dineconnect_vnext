﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class GetProductSetForEditOutput
    {
		public CreateOrEditProductSetDto ProductSet { get; set; }

        public GetProductSetForEditOutput()
        {
            ProductSet = new CreateOrEditProductSetDto();
        }
    }
}
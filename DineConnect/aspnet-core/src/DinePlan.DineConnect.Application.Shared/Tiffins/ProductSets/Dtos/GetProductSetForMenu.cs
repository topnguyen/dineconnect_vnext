﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.MenuItems;

namespace DinePlan.DineConnect.Tiffins.ProductSets.Dtos
{
   public class ProductSetForMenu: EntityDto
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Products { get; set; }
    }

    public class GetProductSetForMenu: EntityDto
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Weekday { get; set; }
        public List<string> lstProduct { get; set; }

    }
}

﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos
{
    public class GetProductSetForViewDto
    {
        public ProductSetDto ProductSet { get; set; }
    }
}
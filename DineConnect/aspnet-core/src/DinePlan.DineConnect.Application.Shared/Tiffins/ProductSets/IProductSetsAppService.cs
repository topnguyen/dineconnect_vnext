﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Tiffin.ProductSets.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.ProductSets.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Tiffin.ProductSets
{
    public interface IProductSetsAppService : IApplicationService
    {
        Task<PagedResultDto<GetProductSetForViewDto>> GetAll(GetAllProductSetsInput input);

        Task<GetProductSetForViewDto> GetProductSetForView(int id);

        Task<GetProductSetForEditOutput> GetProductSetForEdit(NullableIdDto input);

        Task CreateOrEdit(CreateOrEditProductSetDto input);
        //Task CreateOrEditProduct(CreateOrEditProductDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetProductSetsToExcel(GetAllProductSetsInput input);
        
    }
}
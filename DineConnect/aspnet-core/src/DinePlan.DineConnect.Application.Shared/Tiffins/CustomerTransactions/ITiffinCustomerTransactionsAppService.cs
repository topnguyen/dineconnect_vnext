﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos;
using DinePlan.DineConnect.Tiffins.CustomerTransactions.Dtos;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions
{
    public interface ITiffinCustomerTransactionsAppService : IApplicationService
    {
        Task<PagedResultDto<TiffinCustomerTransactionDto>> GetEditHistories(long customerOfferId);

        Task<PagedResultDto<CustomerOfferForTransactionDto>> GetAllCustomerOffer(GetAllCustomerTransactionsInput input);

        Task<TiffinCustomerProductOfferDto> GetCustomerProductOfferForEdit(long id);

        Task UpdateCustomerOffer(TiffinCustomerProductOfferDto input);

        Task<List<ComboboxItemDto>> GetAllCustomerForCombobox();
        Task<PagedResultDto<CustomerOfferForTransactionDto>> GetCustomerTransactions(GetAllCustomerTransactionsInput input);
        Task<FileDto> GetCustomerTransactionsExcel(GetAllCustomerTransactionsInput input);


    }
}
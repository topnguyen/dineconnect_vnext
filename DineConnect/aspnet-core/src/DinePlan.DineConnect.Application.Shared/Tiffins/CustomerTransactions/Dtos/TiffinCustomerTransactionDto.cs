﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos
{
    public class TiffinCustomerTransactionDto : EntityDto
    {
        public string Reason { get; set; }

        public DateTime CreationTime { get; set; }

        public string ChangeDetails { get; set; }

        public List<BalanceChangeListDto> ChangeList
        {
            get
            {
                if (ChangeDetails.IsNullOrEmpty())
                    return new List<BalanceChangeListDto>();
                return JsonConvert.DeserializeObject<List<BalanceChangeListDto>>(ChangeDetails);
            }
        }

        public string OldExpiryDate
        {
            get
            {
                foreach (var e in ChangeList)
                {
                    if (e.PropertyName == "Expiry Date")
                    {
                        return e.OriginalValue;
                    }
                };
                return null;
            }
        }

        public string NewExpiryDate
        {
            get
            {
                foreach (var e in ChangeList)
                {
                    if (e.PropertyName == "Expiry Date")
                    {
                        return e.NewValue;
                    }
                };
                return null;
            }
        }

        public string OldBalanceCredit
        {
            get
            {
                foreach (var e in ChangeList)
                {
                    if (e.PropertyName == "Balance Credit")
                    {
                        return e.OriginalValue;
                    }
                };
                return null;
            }
        }

        public string NewBalanceCredit
        {
            get
            {
                foreach (var e in ChangeList)
                {
                    if (e.PropertyName == "Balance Credit")
                    {
                        return e.NewValue;
                    }
                };
                return null;
            }
        }
    }
}
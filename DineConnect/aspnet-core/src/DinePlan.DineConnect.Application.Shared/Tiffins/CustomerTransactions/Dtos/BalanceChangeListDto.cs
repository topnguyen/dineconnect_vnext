﻿using Newtonsoft.Json;

namespace DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos
{
    public class BalanceChangeListDto
    {
        [JsonProperty("new")]
        public string NewValue { get; set; }

        [JsonProperty("old")]
        public string OriginalValue { get; set; }

        [JsonProperty("property")]
        public string PropertyName { get; set; }
    }
}
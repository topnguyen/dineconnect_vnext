﻿using System;

namespace DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos
{
    public class CustomerOfferForTransactionDto
    {
        public long CustomerOfferId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerEmail { get; set; }

        public DateTime CreationTime { get; set; }

        public string OfferName { get; set; }

        public int? ActualCredit { get; set; }

        public int? BalanceCredit { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public decimal? Amount { get; set; }

        public string Paymode { get; set; }
    }
}
﻿using System;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;

namespace DinePlan.DineConnect.Tiffins.CustomerTransactions.Dtos
{
    public class GetAllCustomerTransactionsInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public int? CustomerId{ get; set; }

        public string PaymentFilter { get; set; }

        public int? DaysToExpiry { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public TiffinDeliveryTimeSlotType? DeliveryType { get; set; }
        public string TimeSlotFormTo { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime";
            }
        }
    }
}
﻿using System;

namespace DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos
{
    public class TiffinCustomerProductOfferDto
    {
        public int Id { get; set; }
        public int ProductOfferId { get; set; }

        public string ProductOfferName { get; set; }
        public long CustomerId { get; set; }

        public int? ActualCredit { get; set; }

        public decimal? PaidAmount { get; set; }

        public int? BalanceCredit { get; set; }

        public long TiffinPaymentId { get; set; }

        public int TenantId { get; set; }

        public DateTime ExpiryDate { get; set; }

        public bool Expiry { get; set; }

        public CreateOrEditCustomerTransactionDto CustomerTransactionDto { get; set; }

        public TiffinCustomerProductOfferDto()
        {
            CustomerTransactionDto = new CreateOrEditCustomerTransactionDto();
        }
    }
}
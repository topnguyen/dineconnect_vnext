﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.Customer.Transactions.Dtos
{
    public class CreateOrEditCustomerTransactionDto : EntityDto<int?>
    {
        [Required]
        public string Reason { get; set; }

        public string ChangeDetails { get; set; }
    }
}
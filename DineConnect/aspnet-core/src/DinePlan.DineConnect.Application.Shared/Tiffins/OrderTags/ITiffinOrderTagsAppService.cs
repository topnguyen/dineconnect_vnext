﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.OrderTags.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.OrderTags
{
    public interface ITiffinOrderTagsAppService : IApplicationService
    {
        Task<PagedResultDto<TiffinOrderTagDto>> GetAll(GetAllTiffinOrderTagsInput input);

        Task<CreateOrEditTiffinOrderTagDto> GetTiffinOrderTagForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTiffinOrderTagDto input);

        Task Delete(EntityDto input);
    }
}
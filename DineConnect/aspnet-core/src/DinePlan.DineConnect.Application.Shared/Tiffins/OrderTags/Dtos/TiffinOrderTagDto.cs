﻿
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.OrderTags.Dtos
{
    public class TiffinOrderTagDto : EntityDto
    {
		public string Images { get; set; }

		public string Name { get; set; }

		public decimal Price { get; set; }

    }
}
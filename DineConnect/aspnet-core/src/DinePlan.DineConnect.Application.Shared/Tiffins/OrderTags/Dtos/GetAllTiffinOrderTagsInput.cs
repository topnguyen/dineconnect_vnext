﻿using Abp.Application.Services.Dto;
using System;

namespace DinePlan.DineConnect.Tiffins.OrderTags.Dtos
{
    public class GetAllTiffinOrderTagsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }



    }
}
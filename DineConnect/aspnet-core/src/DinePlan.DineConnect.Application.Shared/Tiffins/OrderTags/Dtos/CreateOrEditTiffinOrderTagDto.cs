﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.OrderTags.Dtos
{
    public class CreateOrEditTiffinOrderTagDto : EntityDto<int?>
    {
        public string Images { get; set; }


		[Required]
		public string Name { get; set; }
		
		
		public decimal Price { get; set; }
    }
}
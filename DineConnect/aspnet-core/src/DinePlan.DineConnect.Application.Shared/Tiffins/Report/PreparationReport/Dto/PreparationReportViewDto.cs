﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto
{
    public class PreparationReportViewDto
    {
        public DateTime DateOrder { get; set; }
        public string MealTime { get; set; }
        public string NameProductSet { get; set; }
        public int Quantity { get; set; }
        public string TimeSlotFormTo { get; set; }

        public List<int> OrderIdList { get; set; }
    }

    public class PreparationReportExportDto
    {
        public PreparationReportExportDto()
        {
            ProductOrderExport = new List<ProductOrderExport>();
        }

        public DateTime DateOrder { get; set; }
        public string MealTime { get; set; }
        public string TimeSlotFormTo { get; set; }

        public List<ProductOrderExport> ProductOrderExport { get; set; }
    }

    public class ProductOrderExport
    {
        public string NameProductSet { get; set; }
        public int Quantity { get; set; }
    }
}
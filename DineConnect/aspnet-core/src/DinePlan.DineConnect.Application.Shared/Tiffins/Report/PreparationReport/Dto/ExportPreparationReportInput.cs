﻿using System;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto
{
    public class ExportPreparationReportInput
    {
        public string Filter { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? MealPlan { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductSetId { get; set; }
        public int? Zone { get; set; }
        public TiffinDeliveryTimeSlotType? DeliveryType { get; set; }
        public string TimeSlotFormTo { get; set; }
    }
}
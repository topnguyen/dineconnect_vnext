﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport
{
    public interface IPreparationReportAppService
    {
        Task<PagedResultDto<PreparationReportViewDto>> GetAll(GetPreparationReportInput input);
        Task<FileDto> GetPreparationReportExcel(ExportPreparationReportInput input);
        Task<FileDto> PrintPreparationReport(ExportPreparationReportInput input);
    }
}

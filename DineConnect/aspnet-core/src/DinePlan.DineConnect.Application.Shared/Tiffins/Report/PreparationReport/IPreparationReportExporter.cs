﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.PreparationReport
{
    public interface IPreparationReportExporter
    {
        Task<FileDto> ExportPreparationReport(ExportPreparationReportInput input);

        FileDto PrintListPreparation(List<PreparationReportViewDto> preparationReports);
    }
}

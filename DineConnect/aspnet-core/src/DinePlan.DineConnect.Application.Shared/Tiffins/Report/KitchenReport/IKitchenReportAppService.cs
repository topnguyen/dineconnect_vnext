﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.KitchenReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport
{
    public interface IKitchenReportAppService
    {
        Task<FileDto> GetListCustomerForKitchenExcel(ExportPreparationReportInput input);

        Task<PagedResultDto<KitchenReportViewDto>> GetAll(GetPreparationReportInput input);
    }
}
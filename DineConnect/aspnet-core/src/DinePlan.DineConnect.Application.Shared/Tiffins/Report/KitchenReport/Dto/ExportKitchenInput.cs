﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport.Dto
{
    public class ExportKitchenInput
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Zone { get; set; }
        public int? Meal { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductSetId { get; set; }
    }
}

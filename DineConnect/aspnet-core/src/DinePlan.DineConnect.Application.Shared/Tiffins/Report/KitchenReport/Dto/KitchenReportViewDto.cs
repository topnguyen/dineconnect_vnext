﻿using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport.Dto
{
    public class KitchenReportViewDto
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Zone { get; set; }
        public string Meal { get; set; }
        public string Plan { get; set; }
        public DateTime DateOrder { get; set; }
        public string TimeSlotFormTo { get; set; }

        public string Remarks => ConvertJsonAddOnToString(Addon);
        public string Addon { get; set; }
        public int? ZoneNumber { get; set; }

        protected static string ConvertJsonAddOnToString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
       
    }

    public class KitchenOrderViewDto
    {
        public KitchenOrderViewDto()
        {
            KitchenOrderListDtos = new List<KitchenOrderListDto>();
        }

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int? Zone { get; set; }
        public string Plan { get; set; }
        public List<KitchenOrderListDto> KitchenOrderListDtos { get; set; }
    }

    public class KitchenOrderListDto
    {
        public int Meal { get; set; }
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
        public string MealTime { get; set; }
    }
}
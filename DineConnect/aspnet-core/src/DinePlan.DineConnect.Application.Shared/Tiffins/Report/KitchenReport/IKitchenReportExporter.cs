﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.KitchenReport
{
    public interface IKitchenReportExporter
    {
        Task<FileDto> ExportSummary(ExportPreparationReportInput input);
    }
}
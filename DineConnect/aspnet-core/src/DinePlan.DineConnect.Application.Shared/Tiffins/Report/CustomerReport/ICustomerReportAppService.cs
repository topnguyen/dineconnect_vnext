using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport
{
    public interface ICustomerReportAppService
    {
        Task<FileDto> ExportExcel(CustomerReportInput input);

        Task<PagedResultDto<CustomerReportViewDto>> GetAll(CustomerReportInput input);

        Task<List<CustomerReportViewDto>> GetData(CustomerReportInput input);

        Task SendEmailRemindBuyMealPlan(long customerId);

        Task SendEmailRemindBuyMealPlanToNewCustomers();
    }
}
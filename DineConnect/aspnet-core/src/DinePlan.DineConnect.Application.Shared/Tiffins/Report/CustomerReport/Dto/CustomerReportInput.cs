using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto
{
    public class CustomerReportInput : PagedAndSortedResultRequestDto
    {
        public string CustomerName { get; set; }

        public bool? ActiveCustomer { get; set; }

        public bool? NewCustomer { get; set; }
    }
}
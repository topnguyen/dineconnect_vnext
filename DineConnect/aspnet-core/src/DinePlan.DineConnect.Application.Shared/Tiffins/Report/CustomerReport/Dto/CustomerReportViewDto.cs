using System;

namespace DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto
{
    public class CustomerReportViewDto
    {
        public long CustomerId { get; set; }
        
        public string CustomerName { get; set; }

        public string CustomerEmail { get; set; }

        public DateTime RegistrationDate { get; set; }

        public DateTimeOffset? LastLoginTime { get; set; }

        public decimal TotalSpent { get; set; }

        public string ActiveMealPlans { get; set; }

        public DateTime LastTimeSendBuyMealPlanRemind { get; set; }

        public DateTime LastTimeBuyMealPlan { get; set; }

        public int TotalTimeBuyMealPlan { get; set; }
    }
}
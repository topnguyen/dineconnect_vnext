namespace DinePlan.DineConnect.Tiffins.Report
{
    public enum TimeRangeMode
    {
        Day,
        Month,
        Year
    }
}
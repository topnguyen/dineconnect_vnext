﻿using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Tiffins.Orders.Dtos;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto
{
    public class OrderSummaryViewDto
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public int? Zone { get; set; }
        public DateTime DateOrder { get; set; }
        public int Total { get; set; }
        public string Remarks { get; set; }
        public string Meal { get; set; }
        public string Plan { get; set; }
        public string ZoneText { get; set; }
        public int Id { get; set; }
        public string SetName { get; set; }
        public string Email { get; set; }
        public string JsonData { get; set; }
        public string Grade
        {
            get
            {
                var result = "";
                if (!string.IsNullOrEmpty(JsonData))
                {
                    var jsonData = JsonConvert.DeserializeObject<JsonDataCustomer>(JsonData);
                    result = jsonData.Grade;
                }

                return result;
            }
        }
        public string Section
        {
            get
            {
                var result = "";
                if (!string.IsNullOrEmpty(JsonData))
                {
                    var jsonData = JsonConvert.DeserializeObject<JsonDataCustomer>(JsonData);
                    result = jsonData.Section;
                }

                return result;
            }
        }
        public string TimeSlotFormTo { get; set; }

        public string StrRemarks => ConvertJsonAddOnToString(Remarks);
        protected static string ConvertJsonAddOnToString(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "";
            }
            var lstAddOnObject = JsonConvert.DeserializeObject<List<AddonListDto>>(input);
            var lstAddOnString = new List<string>();

            if (lstAddOnObject != null)
            {
                lstAddOnString
                    .AddRange(lstAddOnObject.Select(item => item.Label + "x" + item.Quantity));
                return string.Join(", ", lstAddOnString);
            }

            return string.Empty;
        }
    }

    public class CustomerOrderSummaryDto
    {
        public CustomerOrderSummaryDto()
        {
            OrderSummaries = new List<OrderSummary>();
        }

        public long CustomerId { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string Plan { get; set; }
        public List<OrderSummary> OrderSummaries { get; set; }
    }

    public class OrderSummary
    {
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public int? Zone { get; set; }
        public DateTime DateOrder { get; set; }
        public int Total { get; set; }
        public string Remarks { get; set; }
    }
    public class JsonDataCustomer
    {
        public string Grade { get; set; }
        public string Section { get; set; }
    }
}
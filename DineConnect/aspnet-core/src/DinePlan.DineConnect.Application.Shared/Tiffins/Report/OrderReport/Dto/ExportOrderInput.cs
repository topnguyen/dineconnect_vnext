﻿using System;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto
{
    public class ExportOrderInput
    {
        public string Filter { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Zone { get; set; }
        public int? Meal { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductSetId { get; set; }
        public TiffinDeliveryTimeSlotType? DeliveryType { get; set; }
        public string TimeSlotFormTo { get; set; }
    }
}
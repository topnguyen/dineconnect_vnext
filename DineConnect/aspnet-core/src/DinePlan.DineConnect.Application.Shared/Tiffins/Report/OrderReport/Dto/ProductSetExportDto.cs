﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto
{
    public class ProductSetExportDto
    {
        public int ProductSetId { get; set; }
        public string ProductSetName { get; set; }
    }
}

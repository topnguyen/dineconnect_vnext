﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto
{
    public class OrderSummaryExcelDto
    {
        public long CustomerId { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public string ZoneDisplay { get; set; }
        public string MealDisplay { get; set; }
        public string Plan { get; set; }
        public OrderProductSet OrderProductSet { get; set; }
        public string Email { get; set; }
        public string TimeSlotFormTo { get; set; }
    }

    public class OrderGroupProductSet
    {
        public OrderGroupProductSet()
        {
            OrderProductSets = new List<OrderProductSet>();
        }

        public long CustomerId { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public string ZoneDisplay { get; set; }
        public string Plan { get; set; }
        public List<OrderProductSet> OrderProductSets { get; set; }
        public string Email { get; set; }
    }

    public class OrderProductSet
    {
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public int? Zone { get; set; }
        public int Meal { get; set; }
        public string MealTime { get; set; }
        public DateTime DateOrder { get; set; }
        public string NameProductSet { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
        public string TimeSlotFormTo { get; set; }


        public int OrderId { get; set; }
    }
}
﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport
{
    public interface IOrderSummaryAppService : IApplicationService
    {
        Task<FileDto> GetOrderSummaryExcel(ExportOrderInput input);
        Task<List<OrderSummaryViewDto>> GetListOfOrderViews(GetOrderSummaryInput input);

        Task<PagedResultDto<OrderSummaryViewDto>> GetAll(GetOrderSummaryInput input);
    }
}
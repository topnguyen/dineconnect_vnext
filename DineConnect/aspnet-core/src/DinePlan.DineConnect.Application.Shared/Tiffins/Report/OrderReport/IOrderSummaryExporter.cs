﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.OrderReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.OrderReport
{
    public interface IOrderSummaryExporter
    {
        Task<FileDto> ExportOrderSummary(ExportOrderInput input);
        Task<FileDto> ExportOrderSummaryNewFormat(IOrderSummaryAppService appService, ExportOrderInput input);

    }
}
using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.Report.MealPlanReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.MealPlanReport
{
    public interface IMealPlanReportAppService
    {
        Task<ListResultDto<MealPlanReportViewDto>> GetReport(MealPlanReportInput input);

        Task<ListResultDto<MealPLanCustomerAcquisitionViewDto>> GetCustomerAcquisitionReport(DateTime fromDate, DateTime toDate);
    }
}
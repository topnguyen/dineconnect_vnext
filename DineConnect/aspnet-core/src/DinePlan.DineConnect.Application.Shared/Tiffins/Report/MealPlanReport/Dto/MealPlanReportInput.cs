using System;

namespace DinePlan.DineConnect.Tiffins.Report.MealPlanReport.Dto
{
    public class MealPlanReportInput
    {
        public DateTime From { get; set; }
        
        public DateTime To { get; set; }
        
        public TimeRangeMode TimeRangeMode { get; set; }
    }
}
﻿namespace DinePlan.DineConnect.Tiffins.Report.MealPlanReport.Dto
{
    public class MealPLanCustomerAcquisitionViewDto
    {
        public long MealPlanId { get; set; }

        public string MealPlanName { get; set; }

        public int MealPlanTotalCountBoughtByNewCustomer { get; set; }

        public int MealPlanTotalCountBoughtByExistingCustomer { get; set; }

        public int MealPlanTotalBought => MealPlanTotalCountBoughtByNewCustomer + MealPlanTotalCountBoughtByExistingCustomer;
    }
}

using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Report.MealPlanReport.Dto
{
    public class MealPlanReportViewDto
    {
        public DateTime Date { get; set; }

        public List<MealPlanReportViewDetailDto> Details { get; set; }
    }

    public class MealPlanReportViewDetailDto
    {
        public long MealPlanId { get; set; }
        
        public string MealPlanName { get; set; }
        
        public decimal MealPlanTotalBought { get; set; }
    }
}
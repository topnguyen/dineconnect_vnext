﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport
{
    public interface IBalanceSummaryAppService : IApplicationService
    {
        Task<FileDto> GetBalanceSummaryExcel(ExportBalanceInput input);

        Task<PagedResultDto<BalanceSummaryViewDto>> GetAll(GetBalanceSummaryInput input);
    }
}

﻿using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport
{
    public interface IBalanceSummaryExporter
    {
        Task<FileDto> ExportBalanceSummary(ExportBalanceInput input);
    }
}

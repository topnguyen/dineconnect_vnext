﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto
{
    public class GetBalanceSummaryInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? ProductSetId { get; set; }
        public int? Zone { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CustomerName";
            }
        }
    }
}
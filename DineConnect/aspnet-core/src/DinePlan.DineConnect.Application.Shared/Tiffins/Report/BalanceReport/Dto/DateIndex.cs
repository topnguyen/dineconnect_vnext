﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto
{
    public class DateIndex
    {
        public DateTime DateHeader { get; set; }
        public int Index { get; set; }
    }
}

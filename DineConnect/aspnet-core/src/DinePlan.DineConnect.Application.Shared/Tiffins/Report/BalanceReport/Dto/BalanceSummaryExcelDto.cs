﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto
{
    public class BalanceSummaryExcelDto
    {
        public BalanceSummaryExcelDto()
        {
            OrderDetails = new List<OrderDetail>();
        }
        public DateTime SignUp { get; set; }
        public string PhoneNumber { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Plan { get; set; }
        public int? MealBought { get; set; }
        public int? MealBalance { get; set; }
        public decimal? Amount { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }

    public class OrderDetail
    {
        public int Quantity { get; set; }
        public DateTime DateOrder { get; set; }
    }
}

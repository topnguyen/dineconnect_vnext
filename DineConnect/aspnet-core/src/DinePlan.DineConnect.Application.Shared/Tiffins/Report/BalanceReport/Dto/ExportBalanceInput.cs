﻿using System;

namespace DinePlan.DineConnect.Tiffins.Report.BalanceReport.Dto
{
    public class ExportBalanceInput
    {
        public DateTime FromDateOrder { get; set; }
        public DateTime ToDateOrder { get; set; }
    }
}
﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport
{
    public interface IDriverReportExporter
    {
        Task<FileDto> ExportSummary(ExportPreparationReportInput input);
    }
}
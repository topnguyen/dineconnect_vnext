﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.DriverReport.Dto;
using DinePlan.DineConnect.Tiffins.Report.PreparationReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport
{
    public interface IDriverReportAppService
    {
        Task<FileDto> GetListCustomerForDriverExcel(ExportPreparationReportInput input);

        Task<PagedResultDto<DriverReportViewDto>> GetAll(GetPreparationReportInput input);
    }
}
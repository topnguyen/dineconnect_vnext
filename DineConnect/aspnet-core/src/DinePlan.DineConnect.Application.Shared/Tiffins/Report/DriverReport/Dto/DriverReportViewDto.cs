﻿using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport.Dto
{
    public class DriverReportViewDto
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public string Zone { get; set; }
        public DateTime DateOrder { get; set; }
        public int Total { get; set; }
        public string Remarks { get; set; }
        public string MealTime { get; set; }
        public int? ZoneNumber { get; set; }
        public string TimeSlotFormTo { get; set; }
    }

    public class DriverOrderViewDto
    {
        public DriverOrderViewDto()
        {
            DriverOrderListDtos = new List<DriverOrderListDto>();
        }

        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public List<DriverOrderListDto> DriverOrderListDtos { get; set; }
    }

    public class DriverOrderListDto
    {
        public string Address { get; set; }
        public string UnitNumber { get; set; }
        public string PostalCode { get; set; }
        public int? Zone { get; set; }
        public DateTime DateOrder { get; set; }
        public string NameProductSet { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
        public int Meal { get; set; }
        public string MealTime { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.DriverReport.Dto
{
    public class ExportDriverInput
    {
        public DateTime DateOrder { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport
{
    public interface IPaymentSummaryAppService
    {
        Task<PagedResultDto<PaymentSummaryViewDto>> GetAll(GetPaymentSummaryInput input);

        Task<FileDto> GetPaymentSummaryExcel(GetPaymentReportForExportInput input);
    }
}
﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport
{
    public interface IPaymentSummaryExporter
    {
        Task<FileDto> ExportPaymentSummary(GetPaymentReportForExportInput input);
    }
}
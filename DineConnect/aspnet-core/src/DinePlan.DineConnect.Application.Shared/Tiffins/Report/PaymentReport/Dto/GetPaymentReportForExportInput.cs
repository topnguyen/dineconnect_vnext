﻿using System;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto
{
    public class GetPaymentReportForExportInput
    {
        public string Filter { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Zone { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductSetId { get; set; }
    }
}
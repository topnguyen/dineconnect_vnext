﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Report.PaymentReport.Dto
{
    public class PaymentSummaryViewDto
    {
        public DateTime? PurchaseDateTime { get; set; }
        public string InvoiceNo { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string Plan { get; set; }
        public decimal? Amount { get; set; }
        public int? CurrentBalance { get; set; }
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Tiffins.HomeBanner.Dtos;

namespace DinePlan.DineConnect.Tiffins.HomeBanner
{
    public interface ITiffinHomeBannerAppService : IApplicationService
    {
        Task<HomeBannerEditDto> GetHomeBannerForEdit();

        Task UpdateHomeBanner(HomeBannerEditDto input);
    }
}
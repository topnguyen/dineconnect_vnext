using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.HomeBanner.Dtos
{
    public class HomeBannerEditDto
    {
        [Required]
        public string Images { get; set; }
    }
}
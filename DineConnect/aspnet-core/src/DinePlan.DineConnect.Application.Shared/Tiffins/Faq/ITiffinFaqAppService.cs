﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Tiffins.Faq.Dtos;

namespace DinePlan.DineConnect.Tiffins.Faq
{
    public interface ITiffinFaqAppService : IApplicationService
    {
        Task<ListResultDto<FaqListDto>> GetFaqs();

        Task<FaqEditDto> GetFaqForEdit(EntityDto input);

        Task CreateFaq(FaqCreateDto input);

        Task UpdateFaq(FaqEditDto input);

        Task DeleteFaq(EntityDto input);
    }
}
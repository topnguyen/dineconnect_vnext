using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.Faq.Dtos
{
    public class FaqCreateDto
    {
        [Required]
        public string Questions { get; set; }

        [Required]
        public string Answer { get; set; }

        public bool IsDisplayAtHomePage { get; set; }

        public double DisplayOrder { get; set; }
    }
}
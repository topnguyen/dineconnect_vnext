using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Faq.Dtos
{
    public class FaqEditDto : FaqCreateDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }
}
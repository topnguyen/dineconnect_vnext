using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class GetPromotionCodeInputDto : PagedAndSortedResultRequestDto
    {
        public string Code { get; set; }

        public int? CustomerId { get; set; }
    }
}
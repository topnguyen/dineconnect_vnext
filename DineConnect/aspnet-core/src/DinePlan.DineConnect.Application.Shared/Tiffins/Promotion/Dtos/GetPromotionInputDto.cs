using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class GetPromotionInputDto : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public TiffinPromotionType? Type { get; set; }

        public bool? IsAutoAttach { get; set; }
    }
}
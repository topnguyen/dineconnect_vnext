using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class GetPromotionDiscountPaymentInputDto
    {
        public List<GetPromotionDiscountProductOfferInputDto> Offers { get; set; }

        public List<string> Codes { get; set; }
    }

    public class GetPromotionDiscountProductOfferInputDto
    {
        public int Id { get; set; }

        public int Quantity { get; set; }
    }
}
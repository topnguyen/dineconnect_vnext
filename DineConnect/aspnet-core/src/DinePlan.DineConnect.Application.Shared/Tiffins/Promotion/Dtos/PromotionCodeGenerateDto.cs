using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class PromotionCodeGenerateDto
    {
        public int PromotionId { get; set; }
        
        public string Prefix { get; set; }
        
        public string Suffix { get; set; }

        [Required]
        public int TotalVouchers { get; set; }

        [Required]
        public int TotalDigits { get; set; }
    }
}
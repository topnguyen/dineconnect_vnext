using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class PromotionDiscountPaymentDto
    {
        public double OriginalAmount { get; set; }

        public double TotalClaimAmount { get; set; }

        public double DiscountedAmount { get; set; }

        public List<PromotionCodeDiscountDetailPaymentDto> DiscountDetails { get; set; }
    }

    public class PromotionCodeDiscountDetailPaymentDto
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public double ClaimAmount { get; set; }
    }
}
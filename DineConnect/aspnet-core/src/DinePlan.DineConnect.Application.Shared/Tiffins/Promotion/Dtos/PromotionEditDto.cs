using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class PromotionEditDto : PromotionCreateDto, IEntityDto<int>
    {
        public int Id { get; set; }
        
        public List<PromotionCodeDto> Codes { get; set; }

        public int VoucherCount => Codes.Count;

        public DateTime CreationTime { get; set; }
        public PromotionEditDto()
        {
            Codes = new List<PromotionCodeDto>();
        }
    }
}
namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class CustomerPromotionCodeInputDto
    {
        public int CodeId { get; set; }

        public int CustomerId { get; set; }

        public CustomerPromotionCodeSendBy SendBy { get; set; } = CustomerPromotionCodeSendBy.Email;


    }

    public enum CustomerPromotionCodeSendBy
    {
        Email,
        Whatapp,
        EmailAndWhatapp
    }
}
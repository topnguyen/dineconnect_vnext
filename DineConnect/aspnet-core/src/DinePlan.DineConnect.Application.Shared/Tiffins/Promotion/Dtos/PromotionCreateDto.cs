using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class PromotionCreateDto
    {
        [Required]
        public string Name { get; set; }

        public TiffinPromotionType Type { get; set; }

        [Required]
        public double VoucherValue { get; set; }

        public DateTime Validity { get; set; }

        public int MinimumOrderAmount { get; set; }

        public bool AllowMultipleRedemption { get; set; }

        public int MultiRedemptionCount { get; set; }

        // Auto Attach
        
        public bool IsAutoAttach { get; set; }
        
        // Generation
        
        public string GeneratePrefix { get; set; }
        
        public string GenerateSuffix { get; set; }

        public int GenerateTotalVouchers { get; set; }

        public int GenerateTotalDigits { get; set; }
    }
}
using System;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Tiffins.Promotion.Dtos
{
    public class PromotionCodeDto : IEntityDto<int>
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public bool IsClaimed { get; set; }

        public DateTime? ClaimedTime { get; set; }
        
        public double ClaimedAmount { get; set; }
        
        public int? PaymentId { get; set; }
        
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        
        public string Remarks { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Tiffins.Promotion.Dtos;
using DinePlan.DineConnect.Tiffins.Report.CustomerReport.Dto;

namespace DinePlan.DineConnect.Tiffins.Promotion
{
    public interface ITiffinPromotionAppService : IApplicationService
    {
        Task<PagedResultDto<PromotionListDto>> GetPromotions(GetPromotionInputDto input);
        
        Task<FileDto> GetPromotionsToExcel(GetPromotionInputDto input);

        Task<PromotionEditDto> GetPromotionForEdit(EntityDto input);
        
        Task<int> CreatePromotion(PromotionCreateDto input);

        Task UpdatePromotion(PromotionEditDto input);

        Task DeletePromotion(EntityDto input);
        
        Task<PagedResultDto<PromotionCodeDto>> GetPromotionCodes(GetPromotionCodeInputDto input);

        Task RegisterCustomerToPromotionCode(CustomerPromotionCodeInputDto input);

        Task RemoveCustomerToPromotionCode(CustomerPromotionCodeInputDto input);

        Task SendPromotionCodeToCustomer(CustomerPromotionCodeInputDto input);

        Task<FileDto> GetPromotionCodeToExcel(EntityDto input);

        Task<List<string>> GeneratePromotionCode(PromotionCodeGenerateDto input);
        
        Task<PromotionEditDto> GetPromotionByCode(string code, bool isIncludeCodesDetail = false, bool? claimedStatus = false);
        
        Task<PromotionDiscountPaymentDto> GetPromotionDiscountPayment(GetPromotionDiscountPaymentInputDto input);

        Task<string> GeneratePromotionCodeForReferralEarning(int customerId);

        Task GeneratePromotionCodeAndSendToAllCustomer(int promotionId, CustomerReportInput filter);

        Task GeneratePromotionCodeAndSendToCustomers(int promotionId, List<int> customerIds);
    }
}
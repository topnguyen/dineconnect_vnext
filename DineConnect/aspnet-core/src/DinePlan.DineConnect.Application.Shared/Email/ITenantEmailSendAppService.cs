﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Email;
using DinePlan.DineConnect.Tiffins.TemplateEngines;

namespace DinePlan.DineConnect.EmailConfiguration
{
    public interface ITenantEmailSendAppService
    {
        void Send(string toEmail, string subject, string content, bool html,
            FileAttach fileAttachment, int? tenantId);

        Task SendEmailWithAttachmentAsync(string toEmail, string subject, string content, bool html,
            FileAttach fileAttachment, int? tenantId);

        
        Task SendAsyc(string toEmail, string subject, string content, bool html,
             int? tenantId);

        Task SendEmailByTemplateAsync(SendEmailInput input);

        Task SendEmailByTemplateInBackgroundAsync(SendEmailInput input);

        Task<string> GetEmailTemplateContentAsync(int? tenatnId, string module);
    }
}

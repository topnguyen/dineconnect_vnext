﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    [DependsOn(typeof(DineConnectXamarinSharedModule))]
    public class DineConnectXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectXamarinAndroidModule).GetAssembly());
        }
    }
}
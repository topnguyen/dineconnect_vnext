﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Add_table_StepDiscountPromotionMappingExecutions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StepDiscountPromotionMappingExecutions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    StepDiscountPromotionId = table.Column<int>(nullable: false),
                    ProductGroupId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StepDiscountPromotionMappingExecutions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StepDiscountPromotionMappingExecutions_StepDiscountPromotions_StepDiscountPromotionId",
                        column: x => x.StepDiscountPromotionId,
                        principalTable: "StepDiscountPromotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StepDiscountPromotionMappingExecutions_StepDiscountPromotionId",
                table: "StepDiscountPromotionMappingExecutions",
                column: "StepDiscountPromotionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StepDiscountPromotionMappingExecutions");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class updateshipmentAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ServiceTime",
                table: "ShipmentAddress",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeliveryTimeFrom",
                table: "ShipmentAddress",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeliveryTimeTo",
                table: "ShipmentAddress",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "ShipmentAddress",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryTimeFrom",
                table: "ShipmentAddress");

            migrationBuilder.DropColumn(
                name: "DeliveryTimeTo",
                table: "ShipmentAddress");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "ShipmentAddress");

            migrationBuilder.AlterColumn<string>(
                name: "ServiceTime",
                table: "ShipmentAddress",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}

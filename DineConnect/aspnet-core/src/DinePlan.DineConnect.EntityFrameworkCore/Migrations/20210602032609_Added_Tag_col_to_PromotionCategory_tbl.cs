﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_Tag_col_to_PromotionCategory_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "PromotionCategories");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "PromotionCategories");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "PromotionCategories");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "PromotionCategories");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "PromotionCategories");

            migrationBuilder.AddColumn<string>(
                name: "Tag",
                table: "PromotionCategories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tag",
                table: "PromotionCategories");

            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "PromotionCategories",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "PromotionCategories",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "PromotionCategories",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "PromotionCategories",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "PromotionCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

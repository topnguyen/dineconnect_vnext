﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddAdvertFieldForWheelSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdvertOneUrl",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdvertTwoUrl",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WelcomeUrl",
                table: "WheelSetups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdvertOneUrl",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "AdvertTwoUrl",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "WelcomeUrl",
                table: "WheelSetups");
        }
    }
}

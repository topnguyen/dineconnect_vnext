﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TimeslotDayOfWeek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "TiffinDeliveryTimeSlots");

            migrationBuilder.AddColumn<int>(
                name: "DayOfWeek",
                table: "TiffinDeliveryTimeSlots",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DayOfWeek",
                table: "TiffinDeliveryTimeSlots");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "TiffinDeliveryTimeSlots",
                type: "date",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Alter_table_TicketDiscountPromotions_add_col_PromotionApplyOnce_PromotionApplyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PromotionApplyOnce",
                table: "TicketDiscountPromotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PromotionApplyType",
                table: "TicketDiscountPromotions",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionApplyOnce",
                table: "TicketDiscountPromotions");

            migrationBuilder.DropColumn(
                name: "PromotionApplyType",
                table: "TicketDiscountPromotions");
        }
    }
}

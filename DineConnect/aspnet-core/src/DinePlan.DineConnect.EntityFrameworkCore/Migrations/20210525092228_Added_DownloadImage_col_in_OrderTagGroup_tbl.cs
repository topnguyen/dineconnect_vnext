﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_DownloadImage_col_in_OrderTagGroup_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Oid",
                table: "OrderTagGroups");

            migrationBuilder.AddColumn<Guid>(
                name: "DownloadImage",
                table: "OrderTagGroups",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DownloadImage",
                table: "OrderTagGroups");

            migrationBuilder.AddColumn<int>(
                name: "Oid",
                table: "OrderTagGroups",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

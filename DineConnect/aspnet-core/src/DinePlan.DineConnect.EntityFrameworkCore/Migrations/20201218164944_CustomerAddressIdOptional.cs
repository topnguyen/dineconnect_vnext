﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CustomerAddressIdOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "DeliveryDate",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders",
                column: "CustomerAddressId",
                principalTable: "CustomerAddressDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeliveryDate",
                table: "TiffinCustomerProductOfferOrders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                table: "TiffinCustomerProductOfferOrders",
                column: "CustomerAddressId",
                principalTable: "CustomerAddressDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

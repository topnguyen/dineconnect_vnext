﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CustomerReferralDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedAmount",
                table: "Customers");

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedType",
                table: "Customers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedValue",
                table: "Customers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedType",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedValue",
                table: "Customers");

            migrationBuilder.AddColumn<double>(
                name: "ReferrerCustomerEarnedAmount",
                table: "Customers",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

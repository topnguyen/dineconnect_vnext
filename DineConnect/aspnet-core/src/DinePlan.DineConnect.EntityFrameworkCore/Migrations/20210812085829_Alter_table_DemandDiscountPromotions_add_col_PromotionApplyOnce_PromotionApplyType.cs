﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Alter_table_DemandDiscountPromotions_add_col_PromotionApplyOnce_PromotionApplyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PromotionApplyOnce",
                table: "DemandDiscountPromotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PromotionApplyType",
                table: "DemandDiscountPromotions",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionApplyOnce",
                table: "DemandDiscountPromotions");

            migrationBuilder.DropColumn(
                name: "PromotionApplyType",
                table: "DemandDiscountPromotions");
        }
    }
}

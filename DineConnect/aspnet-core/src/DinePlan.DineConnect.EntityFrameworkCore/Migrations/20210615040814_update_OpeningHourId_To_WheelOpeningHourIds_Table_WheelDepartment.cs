﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_OpeningHourId_To_WheelOpeningHourIds_Table_WheelDepartment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_WheelOpeningHours_OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.DropIndex(
                name: "IX_WheelDepartments_OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.AddColumn<string>(
                name: "WheelOpeningHourIds",
                table: "WheelDepartments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WheelOpeningHourIds",
                table: "WheelDepartments");

            migrationBuilder.AddColumn<int>(
                name: "OpeningHourId",
                table: "WheelDepartments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelDepartments_OpeningHourId",
                table: "WheelDepartments",
                column: "OpeningHourId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_WheelOpeningHours_OpeningHourId",
                table: "WheelDepartments",
                column: "OpeningHourId",
                principalTable: "WheelOpeningHours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

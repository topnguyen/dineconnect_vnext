﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RefactorScreenMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryColumnCount",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "CategoryColumnWidthRate",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "Departments",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "Oid",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "AutoSelect",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ButtonColor",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ConfirmationType",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "DownloadImage",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "Files",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "FontSize",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ItemPortion",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "OrderTags",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ShowAliasName",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "SubMenuTag",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "WeightedItem",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ColumnCount",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "DownloadImage",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "Files",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "FromHour",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "FromMinute",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MainButtonColor",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MainButtonHeight",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MainFontSize",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MaxItems",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MenuItemButtonHeight",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "MostUsedItemsCategory",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "NumeratorType",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "PageCount",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "SubButtonColorDef",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "SubButtonHeight",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "SubButtonRows",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "ToHour",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "ToMinute",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "WrapText",
                table: "ScreenMenuCategories");

            migrationBuilder.AddColumn<string>(
                name: "Dynamic",
                table: "ScreenMenus",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "ScreenMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Dynamic",
                table: "ScreenMenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Dynamic",
                table: "ScreenMenuCategories",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScreenMenuCategories_ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories",
                column: "ParentScreenMenuCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuCategories_ScreenMenuCategories_ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories",
                column: "ParentScreenMenuCategoryId",
                principalTable: "ScreenMenuCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuCategories_ScreenMenuCategories_ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories");

            migrationBuilder.DropIndex(
                name: "IX_ScreenMenuCategories_ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "Dynamic",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "Dynamic",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "Dynamic",
                table: "ScreenMenuCategories");

            migrationBuilder.DropColumn(
                name: "ParentScreenMenuCategoryId",
                table: "ScreenMenuCategories");

            migrationBuilder.AddColumn<int>(
                name: "CategoryColumnCount",
                table: "ScreenMenus",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CategoryColumnWidthRate",
                table: "ScreenMenus",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Departments",
                table: "ScreenMenus",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Oid",
                table: "ScreenMenus",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "AutoSelect",
                table: "ScreenMenuItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ButtonColor",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ConfirmationType",
                table: "ScreenMenuItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "DownloadImage",
                table: "ScreenMenuItems",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FontSize",
                table: "ScreenMenuItems",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemPortion",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrderTags",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ShowAliasName",
                table: "ScreenMenuItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "ScreenMenuItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SubMenuTag",
                table: "ScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WeightedItem",
                table: "ScreenMenuItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ColumnCount",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "DownloadImage",
                table: "ScreenMenuCategories",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "ScreenMenuCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FromHour",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FromMinute",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "ScreenMenuCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainButtonColor",
                table: "ScreenMenuCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MainButtonHeight",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "MainFontSize",
                table: "ScreenMenuCategories",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "MaxItems",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MenuItemButtonHeight",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "MostUsedItemsCategory",
                table: "ScreenMenuCategories",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "NumeratorType",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PageCount",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SubButtonColorDef",
                table: "ScreenMenuCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubButtonHeight",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubButtonRows",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ToHour",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ToMinute",
                table: "ScreenMenuCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "WrapText",
                table: "ScreenMenuCategories",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}

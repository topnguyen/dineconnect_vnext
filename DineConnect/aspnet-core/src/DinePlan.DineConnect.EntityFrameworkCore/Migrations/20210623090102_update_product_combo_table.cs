﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_product_combo_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ProductComboes_MenuItemId",
                table: "ProductComboes",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductComboes_MenuItems_MenuItemId",
                table: "ProductComboes",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductComboes_MenuItems_MenuItemId",
                table: "ProductComboes");

            migrationBuilder.DropIndex(
                name: "IX_ProductComboes_MenuItemId",
                table: "ProductComboes");
        }
    }
}

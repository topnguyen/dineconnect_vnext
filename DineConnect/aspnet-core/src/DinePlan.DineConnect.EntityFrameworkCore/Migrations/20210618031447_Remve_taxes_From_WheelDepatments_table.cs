﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Remve_taxes_From_WheelDepatments_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DinePlanTaxIds",
                table: "WheelDepartments");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DinePlanTaxIds",
                table: "WheelDepartments",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

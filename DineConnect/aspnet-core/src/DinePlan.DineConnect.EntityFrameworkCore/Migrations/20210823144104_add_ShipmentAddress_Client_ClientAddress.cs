﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_ShipmentAddress_Client_ClientAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShipmentAddress",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    LineAddress2 = table.Column<string>(nullable: true),
                    LineAddress3 = table.Column<string>(nullable: true),
                    LineAddress4 = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ExtCode = table.Column<string>(nullable: true),
                    Zone = table.Column<string>(nullable: true),
                    GeoStatus = table.Column<string>(nullable: true),
                    GPSLat = table.Column<string>(nullable: true),
                    GPSLon = table.Column<string>(nullable: true),
                    GPSLatR = table.Column<string>(nullable: true),
                    GPSLonR = table.Column<string>(nullable: true),
                    GPS = table.Column<string>(nullable: true),
                    ServiceTime = table.Column<string>(nullable: true),
                    Clients = table.Column<string>(nullable: true),
                    HasAdvancedRouting = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentAddress", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentClients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ExtCode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    ContactName = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: true),
                    DeliveryTimeFrom = table.Column<string>(nullable: true),
                    DeliveryTimeTo = table.Column<string>(nullable: true),
                    HasAdvancedRouting = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentClients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentClientAddress",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ClientId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentClientAddress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentClientAddress_ShipmentAddress_AddressId",
                        column: x => x.AddressId,
                        principalTable: "ShipmentAddress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShipmentClientAddress_ShipmentClients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "ShipmentClients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentClientAddress_AddressId",
                table: "ShipmentClientAddress",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentClientAddress_ClientId",
                table: "ShipmentClientAddress",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShipmentClientAddress");

            migrationBuilder.DropTable(
                name: "ShipmentAddress");

            migrationBuilder.DropTable(
                name: "ShipmentClients");
        }
    }
}

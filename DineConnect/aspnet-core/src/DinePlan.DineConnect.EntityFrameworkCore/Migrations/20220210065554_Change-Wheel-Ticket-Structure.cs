﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeWheelTicketStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelTickets_Locations_SelfPickupLocationId",
                table: "WheelTickets");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelTickets_WheelPayments_WheelPaymentId",
                table: "WheelTickets");

            migrationBuilder.DropIndex(
                name: "IX_WheelTickets_SelfPickupLocationId",
                table: "WheelTickets");

            migrationBuilder.DropIndex(
                name: "IX_WheelPayments_TenantId",
                table: "WheelPayments");

            migrationBuilder.DropColumn(
                name: "DeliveryCharge",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "SelfPickupLocationId",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "CustomerTaxNo",
                table: "WheelPayments");
            
            migrationBuilder.RenameColumn(
                name: "MenuItemsDynamic",
                table: "WheelPayments",
                newName:"ExtraInformation");


            migrationBuilder.AddColumn<string>(
                name: "AmountCalculations",
                table: "WheelTickets",
                nullable: true);
           

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTickets_WheelPayments_WheelPaymentId",
                table: "WheelTickets",
                column: "WheelPaymentId",
                principalTable: "WheelPayments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelTickets_WheelPayments_WheelPaymentId",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "AmountCalculations",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "ExtraInformation",
                table: "WheelPayments");

            migrationBuilder.AddColumn<decimal>(
                name: "DeliveryCharge",
                table: "WheelTickets",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "SelfPickupLocationId",
                table: "WheelTickets",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerTaxNo",
                table: "WheelPayments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MenuItemsDynamic",
                table: "WheelPayments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelTickets_SelfPickupLocationId",
                table: "WheelTickets",
                column: "SelfPickupLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelPayments_TenantId",
                table: "WheelPayments",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTickets_Locations_SelfPickupLocationId",
                table: "WheelTickets",
                column: "SelfPickupLocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTickets_WheelPayments_WheelPaymentId",
                table: "WheelTickets",
                column: "WheelPaymentId",
                principalTable: "WheelPayments",
                principalColumn: "Id");
        }
    }
}

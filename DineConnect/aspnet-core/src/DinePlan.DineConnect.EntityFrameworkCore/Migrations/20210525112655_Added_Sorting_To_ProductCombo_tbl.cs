﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_Sorting_To_ProductCombo_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "ProductComboes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "ProductComboes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "ProductComboes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "ProductComboes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "ProductComboes");

            migrationBuilder.AddColumn<string>(
                name: "Sorting",
                table: "ProductComboes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sorting",
                table: "ProductComboes");

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "ProductComboes",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "ProductComboes",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "ProductComboes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "ProductComboes",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "ProductComboes",
                type: "bigint",
                nullable: true);
        }
    }
}

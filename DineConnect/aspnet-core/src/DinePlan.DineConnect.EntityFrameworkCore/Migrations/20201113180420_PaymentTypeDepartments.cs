﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class PaymentTypeDepartments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentTypes_Departments_DepartmentId",
                table: "PaymentTypes");

            migrationBuilder.DropIndex(
                name: "IX_PaymentTypes_DepartmentId",
                table: "PaymentTypes");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "PaymentTypes");

            migrationBuilder.AddColumn<bool>(
                name: "DisplayInShift",
                table: "PaymentTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PaymentTypeDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PaymentTypeId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTypeDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTypeDepartments_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentTypeDepartments_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeDepartments_DepartmentId",
                table: "PaymentTypeDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypeDepartments_PaymentTypeId",
                table: "PaymentTypeDepartments",
                column: "PaymentTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentTypeDepartments");

            migrationBuilder.DropColumn(
                name: "DisplayInShift",
                table: "PaymentTypes");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "PaymentTypes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypes_DepartmentId",
                table: "PaymentTypes",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentTypes_Departments_DepartmentId",
                table: "PaymentTypes",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

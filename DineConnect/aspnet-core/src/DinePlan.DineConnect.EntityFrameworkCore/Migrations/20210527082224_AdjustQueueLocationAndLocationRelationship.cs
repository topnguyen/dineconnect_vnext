﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AdjustQueueLocationAndLocationRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_QueueLocations_Locations_LocationId",
                table: "QueueLocations");

            migrationBuilder.DropIndex(
                name: "IX_QueueLocations_LocationId",
                table: "QueueLocations");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "QueueLocations");

            migrationBuilder.AddColumn<int>(
                name: "QueueLocationId",
                table: "Locations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locations_QueueLocationId",
                table: "Locations",
                column: "QueueLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_QueueLocations_QueueLocationId",
                table: "Locations",
                column: "QueueLocationId",
                principalTable: "QueueLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders",
                column: "TicketId",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_QueueLocations_QueueLocationId",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Locations_QueueLocationId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "QueueLocationId",
                table: "Locations");

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "QueueLocations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_QueueLocations_LocationId",
                table: "QueueLocations",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders",
                column: "TicketId",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QueueLocations_Locations_LocationId",
                table: "QueueLocations",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class DineGo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DineGoBrands",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    ScreenMenuId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoBrands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoBrands_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DineGoBrands_ScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "ScreenMenus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoCharges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    IsPercent = table.Column<bool>(nullable: false),
                    TaxIncluded = table.Column<bool>(nullable: false),
                    ChargeValue = table.Column<decimal>(nullable: false),
                    TransactionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoCharges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoCharges_TransactionTypes_TransactionTypeId",
                        column: x => x.TransactionTypeId,
                        principalTable: "TransactionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoDepartments_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoDevices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IdleTime = table.Column<int>(nullable: false),
                    PushToConnect = table.Column<bool>(nullable: false),
                    OrderLink = table.Column<string>(nullable: true),
                    PinCode = table.Column<string>(nullable: true),
                    ThemeSettings = table.Column<string>(nullable: true),
                    ChangeGuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoDevices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DineGoPaymentTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    PaymentTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoPaymentTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoPaymentTypes_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoBrandDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DineGoBrandId = table.Column<int>(nullable: false),
                    DineGoDepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoBrandDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoBrandDepartments_DineGoBrands_DineGoBrandId",
                        column: x => x.DineGoBrandId,
                        principalTable: "DineGoBrands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DineGoBrandDepartments_DineGoDepartments_DineGoDepartmentId",
                        column: x => x.DineGoDepartmentId,
                        principalTable: "DineGoDepartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoChargeDepartments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DineGoChargeId = table.Column<int>(nullable: false),
                    DineGoDepartmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoChargeDepartments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoChargeDepartments_DineGoCharges_DineGoChargeId",
                        column: x => x.DineGoChargeId,
                        principalTable: "DineGoCharges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DineGoChargeDepartments_DineGoDepartments_DineGoDepartmentId",
                        column: x => x.DineGoDepartmentId,
                        principalTable: "DineGoDepartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoBrandsDevices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DineGoBrandId = table.Column<int>(nullable: false),
                    DineGoDeviceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoBrandsDevices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoBrandsDevices_DineGoBrands_DineGoBrandId",
                        column: x => x.DineGoBrandId,
                        principalTable: "DineGoBrands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DineGoBrandsDevices_DineGoDevices_DineGoDeviceId",
                        column: x => x.DineGoDeviceId,
                        principalTable: "DineGoDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DineGoPaymentTypesDevices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DineGoPaymentTypeId = table.Column<int>(nullable: false),
                    DineGoDeviceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DineGoPaymentTypesDevices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DineGoPaymentTypesDevices_DineGoDevices_DineGoDeviceId",
                        column: x => x.DineGoDeviceId,
                        principalTable: "DineGoDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DineGoPaymentTypesDevices_DineGoPaymentTypes_DineGoPaymentTypeId",
                        column: x => x.DineGoPaymentTypeId,
                        principalTable: "DineGoPaymentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrandDepartments_DineGoBrandId",
                table: "DineGoBrandDepartments",
                column: "DineGoBrandId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrandDepartments_DineGoDepartmentId",
                table: "DineGoBrandDepartments",
                column: "DineGoDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrands_LocationId",
                table: "DineGoBrands",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrands_ScreenMenuId",
                table: "DineGoBrands",
                column: "ScreenMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrandsDevices_DineGoBrandId",
                table: "DineGoBrandsDevices",
                column: "DineGoBrandId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoBrandsDevices_DineGoDeviceId",
                table: "DineGoBrandsDevices",
                column: "DineGoDeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoChargeDepartments_DineGoChargeId",
                table: "DineGoChargeDepartments",
                column: "DineGoChargeId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoChargeDepartments_DineGoDepartmentId",
                table: "DineGoChargeDepartments",
                column: "DineGoDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoCharges_TransactionTypeId",
                table: "DineGoCharges",
                column: "TransactionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoDepartments_DepartmentId",
                table: "DineGoDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoPaymentTypes_PaymentTypeId",
                table: "DineGoPaymentTypes",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoPaymentTypesDevices_DineGoDeviceId",
                table: "DineGoPaymentTypesDevices",
                column: "DineGoDeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_DineGoPaymentTypesDevices_DineGoPaymentTypeId",
                table: "DineGoPaymentTypesDevices",
                column: "DineGoPaymentTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DineGoBrandDepartments");

            migrationBuilder.DropTable(
                name: "DineGoBrandsDevices");

            migrationBuilder.DropTable(
                name: "DineGoChargeDepartments");

            migrationBuilder.DropTable(
                name: "DineGoPaymentTypesDevices");

            migrationBuilder.DropTable(
                name: "DineGoBrands");

            migrationBuilder.DropTable(
                name: "DineGoCharges");

            migrationBuilder.DropTable(
                name: "DineGoDepartments");

            migrationBuilder.DropTable(
                name: "DineGoDevices");

            migrationBuilder.DropTable(
                name: "DineGoPaymentTypes");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Updaeted_MenuItem_Tbls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxQty",
                table: "UpMenuItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MenuItemPortions",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Barcode",
                table: "MenuItemPortions",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxQty",
                table: "UpMenuItems");

            migrationBuilder.DropColumn(
                name: "Barcode",
                table: "MenuItemPortions");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MenuItemPortions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}

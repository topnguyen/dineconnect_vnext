﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeCliqueModelBrand : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "CliqueMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PublishStatus",
                table: "CliqueMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "CliqueMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CliqueMenus_LocationId",
                table: "CliqueMenus",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueMenus_Locations_LocationId",
                table: "CliqueMenus",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueMenus_Locations_LocationId",
                table: "CliqueMenus");

            migrationBuilder.DropIndex(
                name: "IX_CliqueMenus_LocationId",
                table: "CliqueMenus");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "CliqueMenus");

            migrationBuilder.DropColumn(
                name: "PublishStatus",
                table: "CliqueMenus");

            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "CliqueMenus");
        }
    }
}

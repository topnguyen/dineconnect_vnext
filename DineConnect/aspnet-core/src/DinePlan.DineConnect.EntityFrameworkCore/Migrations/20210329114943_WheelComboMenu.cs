﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class WheelComboMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelMenuCombos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Dynamic = table.Column<string>(nullable: true),
                    ScreenCategoryId = table.Column<int>(nullable: true),
                    ScreenMenuId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombos_ScreenMenuCategories_ScreenCategoryId",
                        column: x => x.ScreenCategoryId,
                        principalTable: "ScreenMenuCategories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelMenuCombos_ScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "ScreenMenus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelMenuCombosItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Dynamic = table.Column<string>(nullable: true),
                    WheelMenuComboId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombosItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItems_WheelMenuCombos_WheelMenuComboId",
                        column: x => x.WheelMenuComboId,
                        principalTable: "WheelMenuCombos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelMenuCombosItemMenus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Dynamic = table.Column<string>(nullable: true),
                    AddPrice = table.Column<decimal>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: true),
                    WheelMenuComboItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombosItemMenus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItemMenus_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItemMenus_WheelMenuCombosItems_WheelMenuComboItemId",
                        column: x => x.WheelMenuComboItemId,
                        principalTable: "WheelMenuCombosItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_ScreenCategoryId",
                table: "WheelMenuCombos",
                column: "ScreenCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_ScreenMenuId",
                table: "WheelMenuCombos",
                column: "ScreenMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_TenantId",
                table: "WheelMenuCombos",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_MenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_TenantId",
                table: "WheelMenuCombosItemMenus",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_WheelMenuComboItemId",
                table: "WheelMenuCombosItemMenus",
                column: "WheelMenuComboItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItems_TenantId",
                table: "WheelMenuCombosItems",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItems_WheelMenuComboId",
                table: "WheelMenuCombosItems",
                column: "WheelMenuComboId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelMenuCombosItemMenus");

            migrationBuilder.DropTable(
                name: "WheelMenuCombosItems");

            migrationBuilder.DropTable(
                name: "WheelMenuCombos");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Table_Order_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductOrders",
                table: "CliquePayments",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderTime",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ProductGroupId",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrders_ProductGroupId",
                table: "CliqueOrders",
                column: "ProductGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrders_ProductGroups_ProductGroupId",
                table: "CliqueOrders",
                column: "ProductGroupId",
                principalTable: "ProductGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrders_ProductGroups_ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrders_ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "ProductOrders",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "OrderTime",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "CliqueOrders");
        }
    }
}

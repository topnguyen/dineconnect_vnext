﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class remove_OrganizationId_From_Numerator_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "Numerators");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "Numerators");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "Numerators");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "Numerators");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "Numerators");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "Numerators",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "Numerators",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "Numerators",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "Numerators",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "Numerators",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

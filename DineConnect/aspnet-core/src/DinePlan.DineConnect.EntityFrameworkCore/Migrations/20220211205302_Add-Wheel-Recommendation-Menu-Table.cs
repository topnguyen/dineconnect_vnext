﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddWheelRecommendationMenuTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelRecommendationMenu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    RepeatTime = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Pax = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CriteriaNotShow = table.Column<int>(nullable: true),
                    TriggerApplicable = table.Column<string>(nullable: true),
                    Disable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelRecommendationMenu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WheelRecommendationDayPeriod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    From = table.Column<string>(nullable: false),
                    To = table.Column<string>(nullable: false),
                    OpenDays = table.Column<int>(nullable: false),
                    RecommendationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelRecommendationDayPeriod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelRecommendationDayPeriod_WheelRecommendationMenu_RecommendationId",
                        column: x => x.RecommendationId,
                        principalTable: "WheelRecommendationMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WheelRecommendationMenuItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    RecommendationId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelRecommendationMenuItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelRecommendationMenuItem_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelRecommendationMenuItem_WheelRecommendationMenu_RecommendationId",
                        column: x => x.RecommendationId,
                        principalTable: "WheelRecommendationMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendationDayPeriod_RecommendationId",
                table: "WheelRecommendationDayPeriod",
                column: "RecommendationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendationMenuItem_MenuItemId",
                table: "WheelRecommendationMenuItem",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendationMenuItem_RecommendationId",
                table: "WheelRecommendationMenuItem",
                column: "RecommendationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelRecommendationDayPeriod");

            migrationBuilder.DropTable(
                name: "WheelRecommendationMenuItem");

            migrationBuilder.DropTable(
                name: "WheelRecommendationMenu");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Table_Order_Payment_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrders_MenuItems_MenuItemId",
                table: "CliqueOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrders_ProductGroups_ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrders_MenuItemId",
                table: "CliqueOrders");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrders_ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "OrderTime",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "OrderTime",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "ProductGroupId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "CliqueOrders");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ProductOrders",
                table: "CliqueOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrders_CustomerId",
                table: "CliqueOrders",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrders_Customers_CustomerId",
                table: "CliqueOrders",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrders_Customers_CustomerId",
                table: "CliqueOrders");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrders_CustomerId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "ProductOrders",
                table: "CliqueOrders");

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderTime",
                table: "CliquePayments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "CliqueOrders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderTime",
                table: "CliqueOrders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "CliqueOrders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ProductGroupId",
                table: "CliqueOrders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "CliqueOrders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrders_MenuItemId",
                table: "CliqueOrders",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrders_ProductGroupId",
                table: "CliqueOrders",
                column: "ProductGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrders_MenuItems_MenuItemId",
                table: "CliqueOrders",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrders_ProductGroups_ProductGroupId",
                table: "CliqueOrders",
                column: "ProductGroupId",
                principalTable: "ProductGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

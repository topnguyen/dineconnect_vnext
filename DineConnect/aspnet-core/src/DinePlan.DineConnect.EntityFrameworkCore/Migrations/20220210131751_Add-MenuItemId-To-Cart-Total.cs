﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddMenuItemIdToCartTotal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems");

            migrationBuilder.AlterColumn<int>(
                name: "ScreenMenuItemId",
                table: "WheelCartItems",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "WheelCartItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.AlterColumn<int>(
                name: "ScreenMenuItemId",
                table: "WheelCartItems",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

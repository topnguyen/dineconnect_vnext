﻿using System.Linq;
using Abp.Configuration;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Net.Mail;
using Microsoft.EntityFrameworkCore;
using DinePlan.DineConnect.EntityFrameworkCore;

namespace DinePlan.DineConnect.Migrations.Seed.Host
{
    public class DefaultSettingsCreator
    {
        private readonly DineConnectDbContext _context;

        public DefaultSettingsCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            int? tenantId = null;

            //Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "mail@dineconnect.xyz", tenantId);
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "DineConnect", tenantId);

            //Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "en", tenantId);
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.IgnoreQueryFilters().Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}
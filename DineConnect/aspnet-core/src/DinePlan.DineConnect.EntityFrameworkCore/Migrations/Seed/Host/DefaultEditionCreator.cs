using Abp.Application.Features;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.EntityFrameworkCore;
using DinePlan.DineConnect.Features;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Migrations.Seed.Host
{
    public class DefaultEditionCreator
    {
        private readonly DineConnectDbContext _context;
        public static List<SubscribableEdition> InitialEditions => GetInitialEditions();

        private static List<SubscribableEdition> GetInitialEditions()
        {
            return new List<SubscribableEdition>
            {
                new SubscribableEdition { Name = EditionManager.DefaultEditionName, DisplayName = EditionManager.DefaultEditionName },
                new SubscribableEdition { Name = EditionManager.ConnectEdition, DisplayName = EditionManager.ConnectEdition },
            };
        }

        public DefaultEditionCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateEditions();
        }

        private void CreateEditions()
        {
            foreach (var edition in InitialEditions)
            {
                AddEditionIfNotExists(edition);
            }
        }

        private void AddEditionIfNotExists(SubscribableEdition edition)
        {
            var defaultEdition = _context.Editions.IgnoreQueryFilters().FirstOrDefault(e => e.Name == edition.Name);
            if (defaultEdition == null)
            {
                defaultEdition = edition;
                _context.Editions.Add(defaultEdition);
                _context.SaveChanges();

                /* Add desired features to the standard edition, if wanted... */
            }

            if (defaultEdition.Id > 0)
            {
                CreateFeatureIfNotExists(defaultEdition.Id, AppFeatures.ChatFeature, true);
                CreateFeatureIfNotExists(defaultEdition.Id, AppFeatures.TenantToTenantChatFeature, true);
                CreateFeatureIfNotExists(defaultEdition.Id, AppFeatures.TenantToHostChatFeature, true);
            }
        }

        private void CreateFeatureIfNotExists(int editionId, string featureName, bool isEnabled)
        {
            var defaultEditionChatFeature = _context.EditionFeatureSettings.IgnoreQueryFilters()
                                                        .FirstOrDefault(ef => ef.EditionId == editionId && ef.Name == featureName);

            if (defaultEditionChatFeature == null)
            {
                _context.EditionFeatureSettings.Add(new EditionFeatureSetting
                {
                    Name = featureName,
                    Value = isEnabled.ToString().ToLower(),
                    EditionId = editionId
                });
            }
        }
    }
}
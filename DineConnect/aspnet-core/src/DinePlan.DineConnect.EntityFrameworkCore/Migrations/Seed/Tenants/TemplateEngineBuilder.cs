﻿using DinePlan.DineConnect.EntityFrameworkCore;
using DinePlan.DineConnect.Tiffins.TemplateEngines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.BaseCore.TemplateEngines;

namespace DinePlan.DineConnect.Migrations.Seed.Tenants
{
    public class TemplateEngineBuilder
    {
        public List<TemplateEngine> InitialTemplates => GetInitialTemplates();

        private readonly DineConnectDbContext _context;
        private readonly int _tenantId;

        public TemplateEngineBuilder(DineConnectDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        private List<TemplateEngine> GetInitialTemplates()
        {
            var variables = Enum.GetNames(typeof(TemplateVariable));
            var registerVar = Enum.GetNames(typeof(RegisterVariable));
            var mealPlanVar = Enum.GetNames(typeof(BuyMealPlanVariable));
            var orderVar = Enum.GetNames(typeof(OrderVariable));
            var minQuantityVar = Enum.GetNames(typeof(MinQuantityVariable));
            var minExpiryDateVar = Enum.GetNames(typeof(MinExpiryDateVariable));
            var customerPromotionCodeVar = Enum.GetNames(typeof(CustomerPromotionCodeVariable));
            var referralEarnProductOfferVar = Enum.GetNames(typeof(ReferralEarningProductOfferVariable));
            var buyMealPlanReminderVar = Enum.GetNames(typeof(BuyMealPlanReminderVariable));
            var pickUpOrderReminderVar = Enum.GetNames(typeof(PickUpOrderReminderVariable));
            var pickUpOrderSuccessVar = Enum.GetNames(typeof(PickUpOrderSuccessVariable));

            return new List<TemplateEngine>
            {
                new TemplateEngine(_tenantId, "Name of the Template", "Subject of the Template", "Customer", string.Join(", ", variables)),
                new TemplateEngine(_tenantId, "Welcome", "Welcome to DineConnect", "Register", string.Join(", ", registerVar)),
                new TemplateEngine(_tenantId, "BuyMealPlan", "Detail Buy Meal Plan", "BuyMealPlan", string.Join(", ", mealPlanVar)),
                new TemplateEngine(_tenantId, "Order", "Detail Order", "Order", string.Join(", ", orderVar)),
                new TemplateEngine(_tenantId, "MinQuantity", "Minimum Quantity", "MinQuantity", string.Join(", ", minQuantityVar)),
                new TemplateEngine(_tenantId, "MinExpiryDay", "Minimum Expiry Days", "MinExpiryDay", string.Join(", ", minExpiryDateVar)),
                new TemplateEngine(_tenantId, "Customer Promotion Code", "Promotion Code", "CustomerPromotionCode", string.Join(", ", customerPromotionCodeVar)),
                new TemplateEngine(_tenantId, "Referral Earning Product Offer", "Referral Earning Product Offer", "ReferralEarningProductOffer", string.Join(", ", referralEarnProductOfferVar)),
                new TemplateEngine(_tenantId, "Buy Meal Plan Reminder", "Buy Meal Plan Reminder", "BuyMealPlanReminder", string.Join(", ", buyMealPlanReminderVar)),
                new TemplateEngine(_tenantId, "PickUp Order Reminder", "PickUp Order Reminder", "PickUpOrderReminder", string.Join(", ", pickUpOrderReminderVar)),
                new TemplateEngine(_tenantId, "PickUp Order Success", "PickUp Order Success", "PickUpOrderSuccess", string.Join(", ", pickUpOrderSuccessVar))
            };
        }

        public void Create()
        {
            CreateTemplates();
        }

        private void CreateTemplates()
        {
            foreach (var template in InitialTemplates)
            {
                AddTemplateIfNotExists(template);
            }
        }

        private void AddTemplateIfNotExists(TemplateEngine template)
        {
            if (_context.TemplateEngines.IgnoreQueryFilters().Any(l => l.TenantId == template.TenantId && l.Module == template.Module))
            {
                return;
            }

            _context.TemplateEngines.Add(template);

            _context.SaveChanges();
        }
    }
}
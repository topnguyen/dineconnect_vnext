using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.EntityFrameworkCore;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Migrations.Seed.Tenants
{
    public class DeliveryTimeSlotBuilder
    {
        private readonly DineConnectDbContext _context;

        public DeliveryTimeSlotBuilder(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create(int tenantId)
        {
            if (_context.TiffinDeliveryTimeSlots.Any())
            {
                return;
            }

            var mealTimes = _context.TiffinMealTimes.ToList();

            if (!mealTimes.Any())
            {
                return;
            }

            var locations = _context.Locations.ToList();

            var timeSlots = new List<dynamic>
            {
                new
                {
                    From = "7:00",
                    To = "9:00"
                },
                new
                {
                    From = "8:00",
                    To = "10:00"
                },
                new
                {
                    From = "9:00",
                    To = "11:00"
                },
                new
                {
                    From = "10:00",
                    To = "11:00"
                },
                new
                {
                    From = "10:00",
                    To = "12:00"
                },
                new
                {
                    From = "11:00",
                    To = "13:00"
                },
                new
                {
                    From = "12:00",
                    To = "14:00"
                },
                new
                {
                    From = "13:00",
                    To = "15:00"
                }
            };

            // Delivery Type

            foreach (var mealTime in mealTimes)
            {
                for (int i = 0; i < 7; i++)
                {
                    _context.TiffinDeliveryTimeSlots.Add(new Tiffins.DeliveryTimeSlot.TiffinDeliveryTimeSlot
                    {
                        Type = TiffinDeliveryTimeSlotType.Delivery,
                        DayOfWeek = (DayOfWeek)i,
                        MealTimeId = mealTime.Id,
                        LocationId = null,
                        TimeSlots = JsonConvert.SerializeObject(timeSlots),
                        TenantId = tenantId
                    });
                }

                foreach (var location in locations)
                {
                    _context.TiffinDeliveryTimeSlots.Add(new Tiffins.DeliveryTimeSlot.TiffinDeliveryTimeSlot
                    {
                        Type = TiffinDeliveryTimeSlotType.SelfPickup,
                        DayOfWeek = null,
                        MealTimeId = mealTime.Id,
                        LocationId = location.Id,
                        TimeSlots = JsonConvert.SerializeObject(timeSlots),
                        TenantId = tenantId
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
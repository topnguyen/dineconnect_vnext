﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemoveAdvertUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdvertOneUrl",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "AdvertTwoUrl",
                table: "WheelSetups");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceTime",
                table: "ShipmentAddress",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdvertOneUrl",
                table: "WheelSetups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdvertTwoUrl",
                table: "WheelSetups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ServiceTime",
                table: "ShipmentAddress",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}

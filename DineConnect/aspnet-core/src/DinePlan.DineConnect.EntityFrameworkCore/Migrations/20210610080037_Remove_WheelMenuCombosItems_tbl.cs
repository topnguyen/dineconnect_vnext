﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Remove_WheelMenuCombosItems_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelMenuCombosItemMenus");

            migrationBuilder.DropTable(
                name: "WheelMenuCombosItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelMenuCombosItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Dynamic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    WheelMenuComboId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombosItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItems_WheelMenuCombos_WheelMenuComboId",
                        column: x => x.WheelMenuComboId,
                        principalTable: "WheelMenuCombos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelMenuCombosItemMenus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Dynamic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScreenMenuItemId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    WheelMenuComboItemId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombosItemMenus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItemMenus_ScreenMenuItems_ScreenMenuItemId",
                        column: x => x.ScreenMenuItemId,
                        principalTable: "ScreenMenuItems",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelMenuCombosItemMenus_WheelMenuCombosItems_WheelMenuComboItemId",
                        column: x => x.WheelMenuComboItemId,
                        principalTable: "WheelMenuCombosItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "ScreenMenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_TenantId",
                table: "WheelMenuCombosItemMenus",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_WheelMenuComboItemId",
                table: "WheelMenuCombosItemMenus",
                column: "WheelMenuComboItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItems_TenantId",
                table: "WheelMenuCombosItems",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItems_WheelMenuComboId",
                table: "WheelMenuCombosItems",
                column: "WheelMenuComboId");
        }
    }
}

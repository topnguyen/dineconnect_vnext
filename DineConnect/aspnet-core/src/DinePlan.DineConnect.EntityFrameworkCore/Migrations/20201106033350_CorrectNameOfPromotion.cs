﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CorrectNameOfPromotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinimumTicketNumber",
                table: "TiffinPromotion");

            migrationBuilder.AddColumn<int>(
                name: "MinimumOrderAmount",
                table: "TiffinPromotion",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinimumOrderAmount",
                table: "TiffinPromotion");

            migrationBuilder.AddColumn<int>(
                name: "MinimumTicketNumber",
                table: "TiffinPromotion",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddTillTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "TillAccounts");

            migrationBuilder.CreateTable(
                name: "TillTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LocalId = table.Column<int>(nullable: false),
                    WorkPeriodId = table.Column<int>(nullable: false),
                    LocalWorkPeriodId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    User = table.Column<string>(nullable: true),
                    TransactionTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    TillAccountId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TillTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TillTransactions_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TillTransactions_TillAccounts_TillAccountId",
                        column: x => x.TillAccountId,
                        principalTable: "TillAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TillTransactions_LocationId",
                table: "TillTransactions",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_TillTransactions_TillAccountId",
                table: "TillTransactions",
                column: "TillAccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TillTransactions");

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "TillAccounts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

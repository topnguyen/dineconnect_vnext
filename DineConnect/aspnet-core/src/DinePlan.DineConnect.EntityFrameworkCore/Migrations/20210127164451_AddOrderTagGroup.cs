﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddOrderTagGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OrderTagGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MaxSelectedItems = table.Column<int>(nullable: false),
                    MinSelectedItems = table.Column<int>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    AddTagPriceToOrderPrice = table.Column<bool>(nullable: false),
                    SaveFreeTags = table.Column<bool>(nullable: false),
                    FreeTagging = table.Column<bool>(nullable: false),
                    TaxFree = table.Column<bool>(nullable: false),
                    Prefix = table.Column<string>(nullable: true),
                    Departments = table.Column<string>(nullable: true),
                    Oid = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTagGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderTagMaps",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    OrderTagGroupId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: true),
                    CategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTagMaps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderTagMaps_OrderTagGroups_OrderTagGroupId",
                        column: x => x.OrderTagGroupId,
                        principalTable: "OrderTagGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    AlternateName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    MaxQuantity = table.Column<int>(nullable: false),
                    OrderTagGroupId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderTags_OrderTagGroups_OrderTagGroupId",
                        column: x => x.OrderTagGroupId,
                        principalTable: "OrderTagGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderTagLocationPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    OrderTagId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTagLocationPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderTagLocationPrices_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderTagLocationPrices_OrderTags_OrderTagId",
                        column: x => x.OrderTagId,
                        principalTable: "OrderTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderTagLocationPrices_LocationId",
                table: "OrderTagLocationPrices",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTagLocationPrices_OrderTagId",
                table: "OrderTagLocationPrices",
                column: "OrderTagId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTagMaps_OrderTagGroupId",
                table: "OrderTagMaps",
                column: "OrderTagGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderTags_OrderTagGroupId",
                table: "OrderTags",
                column: "OrderTagGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderTagLocationPrices");

            migrationBuilder.DropTable(
                name: "OrderTagMaps");

            migrationBuilder.DropTable(
                name: "OrderTags");

            migrationBuilder.DropTable(
                name: "OrderTagGroups");
        }
    }
}

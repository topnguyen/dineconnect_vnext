﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Remove_OrganizationId_paymenttype_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "PaymentTypes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "PaymentTypes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class addproductsoldouttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelItemSoldOuts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: true),
                    Disable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelItemSoldOuts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelItemSoldOuts_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelItemSoldOuts_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelItemSoldOuts_MenuItemPortions_MenuItemPortionId",
                        column: x => x.MenuItemPortionId,
                        principalTable: "MenuItemPortions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WheelTagSoldOuts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    WheelLocationId = table.Column<int>(nullable: false),
                    OrderTagId = table.Column<int>(nullable: false),
                    Disable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelTagSoldOuts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelTagSoldOuts_OrderTags_OrderTagId",
                        column: x => x.OrderTagId,
                        principalTable: "OrderTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelTagSoldOuts_WheelLocations_WheelLocationId",
                        column: x => x.WheelLocationId,
                        principalTable: "WheelLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelItemSoldOuts_LocationId",
                table: "WheelItemSoldOuts",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelItemSoldOuts_MenuItemId",
                table: "WheelItemSoldOuts",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelItemSoldOuts_MenuItemPortionId",
                table: "WheelItemSoldOuts",
                column: "MenuItemPortionId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTagSoldOuts_OrderTagId",
                table: "WheelTagSoldOuts",
                column: "OrderTagId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTagSoldOuts_WheelLocationId",
                table: "WheelTagSoldOuts",
                column: "WheelLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelItemSoldOuts");

            migrationBuilder.DropTable(
                name: "WheelTagSoldOuts");
        }
    }
}

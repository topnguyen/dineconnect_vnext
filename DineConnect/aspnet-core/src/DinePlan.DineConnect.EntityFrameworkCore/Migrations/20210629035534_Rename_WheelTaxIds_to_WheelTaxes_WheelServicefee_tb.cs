﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Rename_WheelTaxIds_to_WheelTaxes_WheelServicefee_tb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WheelTaxIds",
                table: "WheelServiceFees");

            migrationBuilder.AddColumn<string>(
                name: "WheelTaxes",
                table: "WheelServiceFees",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WheelTaxes",
                table: "WheelServiceFees");

            migrationBuilder.AddColumn<string>(
                name: "WheelTaxIds",
                table: "WheelServiceFees",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

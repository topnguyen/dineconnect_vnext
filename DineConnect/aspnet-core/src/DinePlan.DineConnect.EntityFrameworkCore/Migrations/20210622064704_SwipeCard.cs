﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class SwipeCard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SwipeCardTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: true),
                    DepositRequired = table.Column<bool>(nullable: false),
                    DepositAmount = table.Column<decimal>(nullable: false),
                    ExpiryDaysFromIssue = table.Column<int>(nullable: true),
                    RefundAllowed = table.Column<bool>(nullable: false),
                    CustomerRequired = table.Column<bool>(nullable: false),
                    MinimumTopUpAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwipeCardTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SwipeCards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CardTypeRefId = table.Column<int>(nullable: false),
                    CardNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Balance = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwipeCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SwipeCards_SwipeCardTypes_CardTypeRefId",
                        column: x => x.CardTypeRefId,
                        principalTable: "SwipeCardTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SwipeCardLedgers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LedgerTime = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true),
                    CardRefId = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    OpeningBalance = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Debit = table.Column<decimal>(nullable: false),
                    ClosingBalance = table.Column<decimal>(nullable: false),
                    LocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwipeCardLedgers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SwipeCardLedgers_SwipeCards_CardRefId",
                        column: x => x.CardRefId,
                        principalTable: "SwipeCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SwipeCardLedgers_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SwipeCardLedgers_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SwipeCustomerCards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true),
                    CardRefId = table.Column<int>(nullable: false),
                    IssueDate = table.Column<DateTime>(nullable: false),
                    ReturnDate = table.Column<DateTime>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    DepositAmount = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwipeCustomerCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SwipeCustomerCards_SwipeCards_CardRefId",
                        column: x => x.CardRefId,
                        principalTable: "SwipeCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SwipeCustomerCards_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SwipeTransactionDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    TransactionTime = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true),
                    CardRefId = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Credit = table.Column<decimal>(nullable: false),
                    Debit = table.Column<decimal>(nullable: false),
                    PaymentType = table.Column<string>(nullable: true),
                    PaymentTransactionDetails = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwipeTransactionDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SwipeTransactionDetails_SwipeCards_CardRefId",
                        column: x => x.CardRefId,
                        principalTable: "SwipeCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SwipeTransactionDetails_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCardLedgers_CardRefId",
                table: "SwipeCardLedgers",
                column: "CardRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCardLedgers_CustomerId",
                table: "SwipeCardLedgers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCardLedgers_LocationId",
                table: "SwipeCardLedgers",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCards_CardTypeRefId",
                table: "SwipeCards",
                column: "CardTypeRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCustomerCards_CardRefId",
                table: "SwipeCustomerCards",
                column: "CardRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeCustomerCards_CustomerId",
                table: "SwipeCustomerCards",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeTransactionDetails_CardRefId",
                table: "SwipeTransactionDetails",
                column: "CardRefId");

            migrationBuilder.CreateIndex(
                name: "IX_SwipeTransactionDetails_CustomerId",
                table: "SwipeTransactionDetails",
                column: "CustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SwipeCardLedgers");

            migrationBuilder.DropTable(
                name: "SwipeCustomerCards");

            migrationBuilder.DropTable(
                name: "SwipeTransactionDetails");

            migrationBuilder.DropTable(
                name: "SwipeCards");

            migrationBuilder.DropTable(
                name: "SwipeCardTypes");
        }
    }
}

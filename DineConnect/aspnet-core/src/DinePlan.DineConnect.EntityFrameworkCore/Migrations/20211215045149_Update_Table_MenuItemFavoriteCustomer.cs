﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Table_MenuItemFavoriteCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Days",
                table: "MenuItemFavoriteCustomer");

            migrationBuilder.DropColumn(
                name: "EndHour",
                table: "MenuItemFavoriteCustomer");

            migrationBuilder.DropColumn(
                name: "EndMinute",
                table: "MenuItemFavoriteCustomer");

            migrationBuilder.DropColumn(
                name: "StartHour",
                table: "MenuItemFavoriteCustomer");

            migrationBuilder.DropColumn(
                name: "StartMinute",
                table: "MenuItemFavoriteCustomer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Days",
                table: "MenuItemFavoriteCustomer",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EndHour",
                table: "MenuItemFavoriteCustomer",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EndMinute",
                table: "MenuItemFavoriteCustomer",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StartHour",
                table: "MenuItemFavoriteCustomer",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StartMinute",
                table: "MenuItemFavoriteCustomer",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

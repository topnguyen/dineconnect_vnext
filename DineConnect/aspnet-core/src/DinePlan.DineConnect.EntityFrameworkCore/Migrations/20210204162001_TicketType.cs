﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TicketType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TicketTypeConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketTypeConfigurations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TicketTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    TicketInvoiceNumeratorId = table.Column<int>(nullable: true),
                    TicketNumeratorId = table.Column<int>(nullable: true),
                    OrderNumeratorId = table.Column<int>(nullable: true),
                    PromotionNumeratorId = table.Column<int>(nullable: true),
                    FullTaxNumeratorId = table.Column<int>(nullable: true),
                    QueueNumeratorId = table.Column<int>(nullable: true),
                    ReturnNumeratorId = table.Column<int>(nullable: true),
                    SaleTransactionType_Id = table.Column<int>(nullable: true),
                    ScreenMenuId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    TicketTypeConfigurationId = table.Column<int>(nullable: true),
                    IsTaxIncluded = table.Column<bool>(nullable: false),
                    IsPreOrder = table.Column<bool>(nullable: false),
                    IsAllowZeroPriceProducts = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_FullTaxNumeratorId",
                        column: x => x.FullTaxNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_OrderNumeratorId",
                        column: x => x.OrderNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_PromotionNumeratorId",
                        column: x => x.PromotionNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_QueueNumeratorId",
                        column: x => x.QueueNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_ReturnNumeratorId",
                        column: x => x.ReturnNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_TransactionTypes_SaleTransactionType_Id",
                        column: x => x.SaleTransactionType_Id,
                        principalTable: "TransactionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_ScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "ScreenMenus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_TicketInvoiceNumeratorId",
                        column: x => x.TicketInvoiceNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketTypes_Numerators_TicketNumeratorId",
                        column: x => x.TicketNumeratorId,
                        principalTable: "Numerators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_FullTaxNumeratorId",
                table: "TicketTypes",
                column: "FullTaxNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_OrderNumeratorId",
                table: "TicketTypes",
                column: "OrderNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_PromotionNumeratorId",
                table: "TicketTypes",
                column: "PromotionNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_QueueNumeratorId",
                table: "TicketTypes",
                column: "QueueNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_ReturnNumeratorId",
                table: "TicketTypes",
                column: "ReturnNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_SaleTransactionType_Id",
                table: "TicketTypes",
                column: "SaleTransactionType_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_ScreenMenuId",
                table: "TicketTypes",
                column: "ScreenMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_TicketInvoiceNumeratorId",
                table: "TicketTypes",
                column: "TicketInvoiceNumeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTypes_TicketNumeratorId",
                table: "TicketTypes",
                column: "TicketNumeratorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TicketTypeConfigurations");

            migrationBuilder.DropTable(
                name: "TicketTypes");
        }
    }
}

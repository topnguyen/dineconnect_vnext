﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddTablesForScreenMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelScreenMenus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Published = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WheelScreenMenuItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Published = table.Column<bool>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Video = table.Column<string>(nullable: true),
                    DisplayAs = table.Column<int>(nullable: false),
                    NumberOfColumns = table.Column<int>(nullable: true),
                    HideGridTitles = table.Column<bool>(nullable: false),
                    MarkAsNew = table.Column<bool>(nullable: false),
                    MarkAsSignature = table.Column<bool>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    PreperationTime = table.Column<int>(nullable: false),
                    IngredientWarnings = table.Column<string>(nullable: true),
                    DisplayNutritionFacts = table.Column<bool>(nullable: false),
                    NutritionInfo = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    ScreenMenuId = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenuItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItems_WheelScreenMenuItems_ParentId",
                        column: x => x.ParentId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItems_WheelScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "WheelScreenMenus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WheelRecommendedItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ItemId = table.Column<int>(nullable: false),
                    RecommendedItemId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelRecommendedItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelRecommendedItems_WheelScreenMenuItems_ItemId",
                        column: x => x.ItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelRecommendedItems_WheelScreenMenuItems_RecommendedItemId",
                        column: x => x.RecommendedItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WheelScreenMenuItemPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Multiplier = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenuItemPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItemPrices_WheelScreenMenuItems_ItemId",
                        column: x => x.ItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendedItems_ItemId",
                table: "WheelRecommendedItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendedItems_RecommendedItemId",
                table: "WheelRecommendedItems",
                column: "RecommendedItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItemPrices_ItemId",
                table: "WheelScreenMenuItemPrices",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_ParentId",
                table: "WheelScreenMenuItems",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_ScreenMenuId",
                table: "WheelScreenMenuItems",
                column: "ScreenMenuId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelRecommendedItems");

            migrationBuilder.DropTable(
                name: "WheelScreenMenuItemPrices");

            migrationBuilder.DropTable(
                name: "WheelScreenMenuItems");

            migrationBuilder.DropTable(
                name: "WheelScreenMenus");
        }
    }
}

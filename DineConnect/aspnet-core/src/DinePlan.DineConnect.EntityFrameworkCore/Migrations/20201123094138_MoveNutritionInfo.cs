﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class MoveNutritionInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NutritionInfo",
                table: "WheelScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "IsUrbanPiperRecommend",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperFiles",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperPlatforms",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperSwiggyTags",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperZomatoTags",
                table: "MenuItems");

            migrationBuilder.AddColumn<string>(
                name: "NutritionInfo",
                table: "MenuItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NutritionInfo",
                table: "MenuItems");

            migrationBuilder.AddColumn<string>(
                name: "NutritionInfo",
                table: "WheelScreenMenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsUrbanPiperRecommend",
                table: "MenuItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperFiles",
                table: "MenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperPlatforms",
                table: "MenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperSwiggyTags",
                table: "MenuItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperZomatoTags",
                table: "MenuItems",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemoveRedundantTableForWheelScreenMenuItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelRecommendedItems");

            migrationBuilder.DropTable(
                name: "WheelScreenMenuItemPrices");

            migrationBuilder.AddColumn<string>(
                name: "Prices",
                table: "WheelScreenMenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecommendItems",
                table: "WheelScreenMenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecommendedItems",
                table: "WheelScreenMenuItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Prices",
                table: "WheelScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "RecommendItems",
                table: "WheelScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "RecommendedItems",
                table: "WheelScreenMenuItems");

            migrationBuilder.CreateTable(
                name: "WheelRecommendedItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    RecommendedItemId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelRecommendedItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelRecommendedItems_WheelScreenMenuItems_ItemId",
                        column: x => x.ItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelRecommendedItems_WheelScreenMenuItems_RecommendedItemId",
                        column: x => x.RecommendedItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WheelScreenMenuItemPrices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Multiplier = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenuItemPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItemPrices_WheelScreenMenuItems_ItemId",
                        column: x => x.ItemId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendedItems_ItemId",
                table: "WheelRecommendedItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelRecommendedItems_RecommendedItemId",
                table: "WheelRecommendedItems",
                column: "RecommendedItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItemPrices_ItemId",
                table: "WheelScreenMenuItemPrices",
                column: "ItemId");
        }
    }
}

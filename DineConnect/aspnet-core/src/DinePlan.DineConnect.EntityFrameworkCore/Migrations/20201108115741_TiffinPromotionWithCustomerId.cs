﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TiffinPromotionWithCustomerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "TiffinPromotion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotion_CustomerId",
                table: "TiffinPromotion",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinPromotion_Customers_CustomerId",
                table: "TiffinPromotion",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinPromotion_Customers_CustomerId",
                table: "TiffinPromotion");

            migrationBuilder.DropIndex(
                name: "IX_TiffinPromotion_CustomerId",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "TiffinPromotion");
        }
    }
}

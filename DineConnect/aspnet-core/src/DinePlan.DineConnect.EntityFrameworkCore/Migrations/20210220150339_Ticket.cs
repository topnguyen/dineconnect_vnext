﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Ticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "MenuItemPortions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PlanLoggers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Terminal = table.Column<string>(nullable: true),
                    TicketNo = table.Column<string>(nullable: true),
                    TicketTotal = table.Column<decimal>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    EventLog = table.Column<string>(nullable: true),
                    EventName = table.Column<string>(nullable: true),
                    EventTime = table.Column<DateTime>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanLoggers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanLoggers_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    Contents = table.Column<string>(nullable: true),
                    Errors = table.Column<string>(nullable: true),
                    Processed = table.Column<bool>(nullable: false),
                    ContentType = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Tried = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostDatas_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PreTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    TicketNumber = table.Column<string>(maxLength: 30, nullable: true),
                    Contents = table.Column<string>(nullable: true),
                    Errors = table.Column<string>(nullable: true),
                    Processed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PreTickets_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TicketId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: true),
                    TicketNumber = table.Column<string>(maxLength: 30, nullable: true),
                    InvoiceNo = table.Column<string>(nullable: true),
                    TicketCreatedTime = table.Column<DateTime>(nullable: false),
                    LastUpdateTime = table.Column<DateTime>(nullable: false),
                    LastOrderTime = table.Column<DateTime>(nullable: false),
                    LastPaymentTime = table.Column<DateTime>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    RemainingAmount = table.Column<decimal>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    DepartmentName = table.Column<string>(nullable: true),
                    DepartmentGroup = table.Column<string>(nullable: true),
                    TicketTypeName = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    LastModifiedUserName = table.Column<string>(nullable: true),
                    TicketTags = table.Column<string>(nullable: true),
                    TicketStates = table.Column<string>(nullable: true),
                    TicketLogs = table.Column<string>(nullable: true),
                    TaxIncluded = table.Column<bool>(nullable: false),
                    TerminalName = table.Column<string>(nullable: true),
                    PreOrder = table.Column<bool>(nullable: false),
                    TicketEntities = table.Column<string>(nullable: true),
                    WorkPeriodId = table.Column<int>(nullable: false),
                    Credit = table.Column<bool>(nullable: false),
                    LastPaymentTimeTruc = table.Column<DateTime>(type: "date", nullable: false),
                    ReferenceNumber = table.Column<string>(maxLength: 20, nullable: true),
                    QueueNumber = table.Column<string>(maxLength: 20, nullable: true),
                    CreditProcessed = table.Column<bool>(nullable: false),
                    FullTaxInvoiceDetails = table.Column<string>(nullable: true),
                    TicketPromotionDetails = table.Column<string>(nullable: true),
                    ReferenceTicket = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ExternalProcessed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tickets_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    OrderId = table.Column<int>(nullable: false),
                    Location_Id = table.Column<int>(nullable: false),
                    TicketId = table.Column<int>(nullable: false),
                    MenuItemType = table.Column<int>(nullable: false),
                    OrderRef = table.Column<string>(nullable: true),
                    IsParent = table.Column<bool>(nullable: false),
                    IsUpSelling = table.Column<bool>(nullable: false),
                    UpSellingOrderRef = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(maxLength: 50, nullable: true),
                    MenuItemId = table.Column<int>(nullable: false),
                    MenuItemName = table.Column<string>(maxLength: 100, nullable: true),
                    PortionName = table.Column<string>(maxLength: 50, nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    CostPrice = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    PortionCount = table.Column<int>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Locked = table.Column<bool>(nullable: false),
                    CalculatePrice = table.Column<bool>(nullable: false),
                    IncreaseInventory = table.Column<bool>(nullable: false),
                    DecreaseInventory = table.Column<bool>(nullable: false),
                    OrderNumber = table.Column<string>(nullable: true),
                    CreatingUserName = table.Column<string>(maxLength: 50, nullable: true),
                    OrderCreatedTime = table.Column<DateTime>(nullable: false),
                    PriceTag = table.Column<string>(maxLength: 50, nullable: true),
                    Taxes = table.Column<string>(nullable: true),
                    OrderTags = table.Column<string>(nullable: true),
                    OrderStates = table.Column<string>(nullable: true),
                    IsPromotionOrder = table.Column<bool>(nullable: false),
                    PromotionAmount = table.Column<decimal>(nullable: false),
                    PromotionSyncId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: false),
                    OrderPromotionDetails = table.Column<string>(nullable: true),
                    OrderLog = table.Column<string>(nullable: true),
                    OriginalPrice = table.Column<decimal>(nullable: false),
                    TicketId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Locations_Location_Id",
                        column: x => x.Location_Id,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_MenuItemPortions_MenuItemPortionId",
                        column: x => x.MenuItemPortionId,
                        principalTable: "MenuItemPortions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Tickets_TicketId",
                        column: x => x.TicketId,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Tickets_TicketId1",
                        column: x => x.TicketId1,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    PaymentTypeId = table.Column<int>(nullable: false),
                    TicketId = table.Column<int>(nullable: false),
                    PaymentCreatedTime = table.Column<DateTime>(nullable: false),
                    TenderedAmount = table.Column<decimal>(nullable: false),
                    TerminalName = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    PaymentUserName = table.Column<string>(nullable: true),
                    PaymentTags = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payments_PaymentTypes_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payments_Tickets_TicketId",
                        column: x => x.TicketId,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TicketTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    TransactionTypeId = table.Column<int>(nullable: false),
                    TicketId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    TransactionNote = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TicketTransactions_Tickets_TicketId",
                        column: x => x.TicketId,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TicketTransactions_TransactionTypes_TransactionTypeId",
                        column: x => x.TransactionTypeId,
                        principalTable: "TransactionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionOrderTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    OrderId = table.Column<int>(nullable: false),
                    OrderTagGroupId = table.Column<int>(nullable: false),
                    OrderTagId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    TagName = table.Column<string>(nullable: true),
                    TagNote = table.Column<string>(nullable: true),
                    TagValue = table.Column<string>(nullable: true),
                    TaxFree = table.Column<bool>(nullable: false),
                    AddTagPriceToOrderPrice = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionOrderTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionOrderTags_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_Location_Id",
                table: "Orders",
                column: "Location_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MenuItemId",
                table: "Orders",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MenuItemPortionId",
                table: "Orders",
                column: "MenuItemPortionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_TicketId",
                table: "Orders",
                column: "TicketId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_TicketId1",
                table: "Orders",
                column: "TicketId1");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_PaymentTypeId",
                table: "Payments",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_TicketId",
                table: "Payments",
                column: "TicketId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanLoggers_LocationId",
                table: "PlanLoggers",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PostDatas_LocationId",
                table: "PostDatas",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PreTickets_LocationId",
                table: "PreTickets",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_LocationId",
                table: "Tickets",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTransactions_TicketId",
                table: "TicketTransactions",
                column: "TicketId");

            migrationBuilder.CreateIndex(
                name: "IX_TicketTransactions_TransactionTypeId",
                table: "TicketTransactions",
                column: "TransactionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionOrderTags_OrderId",
                table: "TransactionOrderTags",
                column: "OrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "PlanLoggers");

            migrationBuilder.DropTable(
                name: "PostDatas");

            migrationBuilder.DropTable(
                name: "PreTickets");

            migrationBuilder.DropTable(
                name: "TicketTransactions");

            migrationBuilder.DropTable(
                name: "TransactionOrderTags");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "MenuItemPortions");
        }
    }
}

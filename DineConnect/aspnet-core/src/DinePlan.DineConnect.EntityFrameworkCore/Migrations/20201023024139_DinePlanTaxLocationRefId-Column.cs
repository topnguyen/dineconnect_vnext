﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class DinePlanTaxLocationRefIdColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings");

            migrationBuilder.DropColumn(
                name: "DinePlanTaxLocationRefId",
                table: "DinePlanTaxMappings");

            migrationBuilder.AlterColumn<int>(
                name: "DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                column: "DinePlanTaxLocationId",
                principalTable: "DinePlanTaxLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings");

            migrationBuilder.AlterColumn<int>(
                name: "DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "DinePlanTaxLocationRefId",
                table: "DinePlanTaxMappings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                column: "DinePlanTaxLocationId",
                principalTable: "DinePlanTaxLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

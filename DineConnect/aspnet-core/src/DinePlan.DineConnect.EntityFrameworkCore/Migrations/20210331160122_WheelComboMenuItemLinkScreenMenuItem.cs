﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class WheelComboMenuItemLinkScreenMenuItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelMenuCombosItemMenus_MenuItems_MenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.DropIndex(
                name: "IX_WheelMenuCombosItemMenus_MenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "ScreenMenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelMenuCombosItemMenus_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelMenuCombosItemMenus_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.DropIndex(
                name: "IX_WheelMenuCombosItemMenus_ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.DropColumn(
                name: "ScreenMenuItemId",
                table: "WheelMenuCombosItemMenus");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "WheelMenuCombosItemMenus",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombosItemMenus_MenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelMenuCombosItemMenus_MenuItems_MenuItemId",
                table: "WheelMenuCombosItemMenus",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id");
        }
    }
}

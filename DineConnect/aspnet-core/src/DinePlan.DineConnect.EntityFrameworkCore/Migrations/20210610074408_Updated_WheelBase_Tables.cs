﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Updated_WheelBase_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelCustomerProductOfferOrders");

            migrationBuilder.DropTable(
                name: "WheelCustomerProductOffers");

            migrationBuilder.CreateTable(
                name: "WheelTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    PaidAmount = table.Column<decimal>(nullable: true),
                    OrderStatus = table.Column<int>(nullable: false),
                    TimeSlotFrom = table.Column<DateTime>(nullable: true),
                    TimeSlotTo = table.Column<DateTime>(nullable: true),
                    WheelPaymentId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    CustomerAddressId = table.Column<int>(nullable: true),
                    WheelOrderType = table.Column<int>(nullable: false),
                    DeliveryCharge = table.Column<long>(nullable: false),
                    TimeSlotFormTo = table.Column<string>(nullable: true),
                    SelfPickupLocationId = table.Column<int>(nullable: true),
                    ShippingProvider = table.Column<int>(nullable: false),
                    ShippingProviderResponseJson = table.Column<string>(nullable: true),
                    OrderDate = table.Column<DateTime>(type: "date", nullable: false),
                    OrderCreatedTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelTickets_CustomerAddressDetails_CustomerAddressId",
                        column: x => x.CustomerAddressId,
                        principalTable: "CustomerAddressDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelTickets_Locations_SelfPickupLocationId",
                        column: x => x.SelfPickupLocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelTickets_WheelPayments_WheelPaymentId",
                        column: x => x.WheelPaymentId,
                        principalTable: "WheelPayments",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WheelTicketId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    AddOn = table.Column<string>(nullable: true),
                    MenuItemsDynamic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelOrders_WheelTickets_WheelTicketId",
                        column: x => x.WheelTicketId,
                        principalTable: "WheelTickets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelOrders_WheelTicketId",
                table: "WheelOrders",
                column: "WheelTicketId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTickets_CustomerAddressId",
                table: "WheelTickets",
                column: "CustomerAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTickets_SelfPickupLocationId",
                table: "WheelTickets",
                column: "SelfPickupLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTickets_WheelPaymentId",
                table: "WheelTickets",
                column: "WheelPaymentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelOrders");

            migrationBuilder.DropTable(
                name: "WheelTickets");

            migrationBuilder.CreateTable(
                name: "WheelCustomerProductOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActualCredit = table.Column<int>(type: "int", nullable: true),
                    BalanceCredit = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    DateFrom = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateTo = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ExpiryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    OrderStatus = table.Column<int>(type: "int", nullable: false),
                    PaidAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    WheelPaymentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelCustomerProductOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOffers_WheelPayments_WheelPaymentId",
                        column: x => x.WheelPaymentId,
                        principalTable: "WheelPayments",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelCustomerProductOfferOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddOn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerAddressId = table.Column<int>(type: "int", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeliveryCharge = table.Column<long>(type: "bigint", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    MenuItemsDynamic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderDate = table.Column<DateTime>(type: "date", nullable: false),
                    PaymentId = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    SelfPickupLocationId = table.Column<int>(type: "int", nullable: true),
                    ShippingProvider = table.Column<int>(type: "int", nullable: false),
                    ShippingProviderResponseJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimeSlotFormTo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WheelCustomerProductOfferId = table.Column<int>(type: "int", nullable: true),
                    WheelOrderType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelCustomerProductOfferOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                        column: x => x.CustomerAddressId,
                        principalTable: "CustomerAddressDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_WheelPayments_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "WheelPayments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_Locations_SelfPickupLocationId",
                        column: x => x.SelfPickupLocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_WheelCustomerProductOffers_WheelCustomerProductOfferId",
                        column: x => x.WheelCustomerProductOfferId,
                        principalTable: "WheelCustomerProductOffers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_CustomerAddressId",
                table: "WheelCustomerProductOfferOrders",
                column: "CustomerAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_PaymentId",
                table: "WheelCustomerProductOfferOrders",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_SelfPickupLocationId",
                table: "WheelCustomerProductOfferOrders",
                column: "SelfPickupLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_WheelCustomerProductOfferId",
                table: "WheelCustomerProductOfferOrders",
                column: "WheelCustomerProductOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOffers_WheelPaymentId",
                table: "WheelCustomerProductOffers",
                column: "WheelPaymentId");
        }
    }
}

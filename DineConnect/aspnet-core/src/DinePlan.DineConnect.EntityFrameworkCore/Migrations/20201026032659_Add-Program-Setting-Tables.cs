﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddProgramSettingTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramSettingTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DefaultValue = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramSettingTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProgramSettingValues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    ProgramSettingTemplateId = table.Column<int>(nullable: false),
                    ActualValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramSettingValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramSettingValues_ProgramSettingTemplates_ProgramSettingTemplateId",
                        column: x => x.ProgramSettingTemplateId,
                        principalTable: "ProgramSettingTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramSettingValues_ProgramSettingTemplateId",
                table: "ProgramSettingValues",
                column: "ProgramSettingTemplateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramSettingValues");

            migrationBuilder.DropTable(
                name: "ProgramSettingTemplates");
        }
    }
}

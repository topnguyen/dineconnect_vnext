﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_ProductComboGroupDetail_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductComboGroupDetails",
                table: "ProductComboGroupDetails");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ProductComboGroupDetails");

            migrationBuilder.CreateIndex(
                name: "IX_ProductComboGroupDetails_ProductComboId",
                table: "ProductComboGroupDetails",
                column: "ProductComboId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductComboGroupDetails_ProductComboId",
                table: "ProductComboGroupDetails");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ProductComboGroupDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductComboGroupDetails",
                table: "ProductComboGroupDetails",
                columns: new[] { "ProductComboId", "ProductComboGroupId" });
        }
    }
}

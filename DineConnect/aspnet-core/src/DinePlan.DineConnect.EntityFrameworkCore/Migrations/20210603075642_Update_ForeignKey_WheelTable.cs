﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_ForeignKey_WheelTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelTables_WheelDepartments_WheelDepartmentId",
                table: "WheelTables");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelTables_WheelLocations_WheelDepartmentId",
                table: "WheelTables");

            migrationBuilder.DropIndex(
                name: "IX_WheelTables_TenantId",
                table: "WheelTables");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTables_WheelLocationId",
                table: "WheelTables",
                column: "WheelLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTables_WheelDepartments_WheelDepartmentId",
                table: "WheelTables",
                column: "WheelDepartmentId",
                principalTable: "WheelDepartments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTables_WheelLocations_WheelLocationId",
                table: "WheelTables",
                column: "WheelLocationId",
                principalTable: "WheelLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelTables_WheelDepartments_WheelDepartmentId",
                table: "WheelTables");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelTables_WheelLocations_WheelLocationId",
                table: "WheelTables");

            migrationBuilder.DropIndex(
                name: "IX_WheelTables_WheelLocationId",
                table: "WheelTables");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTables_TenantId",
                table: "WheelTables",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTables_WheelDepartments_WheelDepartmentId",
                table: "WheelTables",
                column: "WheelDepartmentId",
                principalTable: "WheelDepartments",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelTables_WheelLocations_WheelDepartmentId",
                table: "WheelTables",
                column: "WheelDepartmentId",
                principalTable: "WheelLocations",
                principalColumn: "Id");
        }
    }
}

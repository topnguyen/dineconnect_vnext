﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeCliqueOrderModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliquePayments_Customers_CustomerId",
                table: "CliquePayments");

            migrationBuilder.DropIndex(
                name: "IX_CliquePayments_CustomerId",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "IsUseVoucher",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "OrderStatus",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "ProductOrders",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "VoucherDetail",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "ProductOrders",
                table: "CliqueOrders");

            migrationBuilder.AddColumn<int>(
                name: "CliqueOrderId",
                table: "CliquePayments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CliqueOrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MenuItemId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: false),
                    OrderTags = table.Column<string>(nullable: true),
                    Quantity = table.Column<decimal>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    OrderDate = table.Column<DateTime>(type: "date", nullable: false),
                    CliqueMealTimeId = table.Column<int>(nullable: false),
                    CliqueOrderStatus = table.Column<int>(nullable: false),
                    CliqueOrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CliqueOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CliqueOrderDetails_CliqueMealTimes_CliqueMealTimeId",
                        column: x => x.CliqueMealTimeId,
                        principalTable: "CliqueMealTimes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CliqueOrderDetails_CliqueOrders_CliqueOrderId",
                        column: x => x.CliqueOrderId,
                        principalTable: "CliqueOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CliqueOrderDetails_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CliquePayments_CliqueOrderId",
                table: "CliquePayments",
                column: "CliqueOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrders_LocationId",
                table: "CliqueOrders",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrderDetails_CliqueMealTimeId",
                table: "CliqueOrderDetails",
                column: "CliqueMealTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrderDetails_CliqueOrderId",
                table: "CliqueOrderDetails",
                column: "CliqueOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrderDetails_MenuItemId",
                table: "CliqueOrderDetails",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrders_Locations_LocationId",
                table: "CliqueOrders",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CliquePayments_CliqueOrders_CliqueOrderId",
                table: "CliquePayments",
                column: "CliqueOrderId",
                principalTable: "CliqueOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrders_Locations_LocationId",
                table: "CliqueOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_CliquePayments_CliqueOrders_CliqueOrderId",
                table: "CliquePayments");

            migrationBuilder.DropTable(
                name: "CliqueOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_CliquePayments_CliqueOrderId",
                table: "CliquePayments");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrders_LocationId",
                table: "CliqueOrders");

            migrationBuilder.DropColumn(
                name: "CliqueOrderId",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "CliqueOrders");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "CliquePayments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsUseVoucher",
                table: "CliquePayments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "OrderStatus",
                table: "CliquePayments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductOrders",
                table: "CliquePayments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VoucherDetail",
                table: "CliquePayments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductOrders",
                table: "CliqueOrders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CliquePayments_CustomerId",
                table: "CliquePayments",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliquePayments_Customers_CustomerId",
                table: "CliquePayments",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

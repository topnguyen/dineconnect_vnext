﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class DineQueue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QueueLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    EnableQueue = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StartTime = table.Column<TimeSpan>(nullable: false),
                    EndTime = table.Column<TimeSpan>(nullable: false),
                    ThemeSettings = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueueLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QueueLocations_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QueueLocationOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Prefix = table.Column<string>(nullable: true),
                    MinPax = table.Column<int>(nullable: false),
                    MaxPax = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    QueueLocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueueLocationOptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QueueLocationOptions_QueueLocations_QueueLocationId",
                        column: x => x.QueueLocationId,
                        principalTable: "QueueLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomerQueues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    QueueLocationOptionId = table.Column<int>(nullable: false),
                    QueueNo = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    CustomerMobile = table.Column<string>(nullable: true),
                    CustomerId = table.Column<int>(nullable: false),
                    QueueTime = table.Column<DateTimeOffset>(nullable: false),
                    CallTime = table.Column<DateTimeOffset>(nullable: false),
                    Seated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerQueues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerQueues_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerQueues_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerQueues_QueueLocationOptions_QueueLocationOptionId",
                        column: x => x.QueueLocationOptionId,
                        principalTable: "QueueLocationOptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerQueues_CustomerId",
                table: "CustomerQueues",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerQueues_LocationId",
                table: "CustomerQueues",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerQueues_QueueLocationOptionId",
                table: "CustomerQueues",
                column: "QueueLocationOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueLocationOptions_QueueLocationId",
                table: "QueueLocationOptions",
                column: "QueueLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueLocations_LocationId",
                table: "QueueLocations",
                column: "LocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerQueues");

            migrationBuilder.DropTable(
                name: "QueueLocationOptions");

            migrationBuilder.DropTable(
                name: "QueueLocations");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DinePlan.DineConnect.Migrations
{
	public partial class Update_PaymentType_table_Added_LocationBranchs_tbl : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AddColumn<string>(
				name: "AddOn",
				table: "Locations",
				nullable: true);

			migrationBuilder.AddColumn<int>(
				name: "BranchId",
				table: "Locations",
				nullable: true);

			migrationBuilder.AddColumn<bool>(
				name: "ChangeTax",
				table: "Locations",
				nullable: false,
				defaultValue: false);

			migrationBuilder.AddColumn<bool>(
				name: "TaxInclusive",
				table: "Locations",
				nullable: false,
				defaultValue: false);

			migrationBuilder.CreateTable(
				name: "LocationBranch",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:Identity", "1, 1"),
					CreationTime = table.Column<DateTime>(nullable: false),
					CreatorUserId = table.Column<long>(nullable: true),
					Code = table.Column<string>(nullable: true),
					Name = table.Column<string>(nullable: true),
					Address1 = table.Column<string>(nullable: true),
					Address2 = table.Column<string>(nullable: true),
					Address3 = table.Column<string>(nullable: true),
					District = table.Column<string>(nullable: true),
					City = table.Column<string>(nullable: true),
					State = table.Column<string>(nullable: true),
					Country = table.Column<string>(nullable: true),
					PostalCode = table.Column<string>(nullable: true),
					PhoneNumber = table.Column<string>(nullable: true),
					Fax = table.Column<string>(nullable: true),
					Website = table.Column<string>(nullable: true),
					Email = table.Column<string>(nullable: true),
					BranchTaxCode = table.Column<string>(nullable: true),
					AddOns = table.Column<string>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_LocationBranch", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "LocationSchedules",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:Identity", "1, 1"),
					CreationTime = table.Column<DateTime>(nullable: false),
					CreatorUserId = table.Column<long>(nullable: true),
					LocationId = table.Column<int>(nullable: false),
					Name = table.Column<string>(nullable: true),
					StartHour = table.Column<int>(nullable: false),
					StartMinute = table.Column<int>(nullable: false),
					EndHour = table.Column<int>(nullable: false),
					EndMinute = table.Column<int>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_LocationSchedules", x => x.Id);
					table.ForeignKey(
						name: "FK_LocationSchedules_Locations_LocationId",
						column: x => x.LocationId,
						principalTable: "Locations",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateIndex(
				name: "IX_Locations_BranchId",
				table: "Locations",
				column: "BranchId");

			migrationBuilder.CreateIndex(
				name: "IX_LocationSchedules_LocationId",
				table: "LocationSchedules",
				column: "LocationId");

			migrationBuilder.AddForeignKey(
				 name: "FK_Locations_LocationBranch_BranchId",
				 table: "Locations",
				 column: "BranchId",
				 principalTable: "LocationBranch",
				 principalColumn: "Id",
				 onDelete: ReferentialAction.Restrict);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_Locations_LocationBranch_BranchId",
				table: "Locations");

			migrationBuilder.DropTable(
				name: "LocationBranch");

			migrationBuilder.DropTable(
				name: "LocationSchedules");

			migrationBuilder.DropIndex(
				name: "IX_Locations_BranchId",
				table: "Locations");

			migrationBuilder.DropColumn(
				name: "AddOn",
				table: "Locations");

			migrationBuilder.DropColumn(
				name: "BranchId",
				table: "Locations");

			migrationBuilder.DropColumn(
				name: "ChangeTax",
				table: "Locations");

			migrationBuilder.DropColumn(
				name: "TaxInclusive",
				table: "Locations");
		}
	}
}
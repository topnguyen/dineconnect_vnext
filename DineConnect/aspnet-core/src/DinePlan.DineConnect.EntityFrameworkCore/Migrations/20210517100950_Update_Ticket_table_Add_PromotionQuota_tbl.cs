﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Ticket_table_Add_PromotionQuota_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TicketStatus",
                table: "Tickets",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ApplyOffline",
                table: "Promotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CombineAllPro",
                table: "Promotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "CombineProValues",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DisplayPromotion",
                table: "Promotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IgnorePromotionLimitation",
                table: "Promotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Limi1PromotionPerItem",
                table: "Promotions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PromotionPosition",
                table: "Promotions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OrderStatus",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PromotionQuotas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    PromotionId = table.Column<int>(nullable: false),
                    QuotaAmount = table.Column<int>(nullable: false),
                    QuotaUsed = table.Column<int>(nullable: false),
                    ResetType = table.Column<int>(nullable: false),
                    ResetWeekValue = table.Column<int>(nullable: true),
                    ResetMonthValue = table.Column<DateTime>(nullable: true),
                    LastResetDate = table.Column<DateTime>(nullable: true),
                    PlantResetDate = table.Column<DateTime>(nullable: true),
                    Plants = table.Column<string>(nullable: true),
                    LocationQuota = table.Column<string>(nullable: true),
                    Each = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionQuotas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromotionQuotas_Promotions_PromotionId",
                        column: x => x.PromotionId,
                        principalTable: "Promotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PromotionQuotas_PromotionId",
                table: "PromotionQuotas",
                column: "PromotionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PromotionQuotas");

            migrationBuilder.DropColumn(
                name: "TicketStatus",
                table: "Tickets");

            migrationBuilder.DropColumn(
                name: "ApplyOffline",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "CombineAllPro",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "CombineProValues",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "DisplayPromotion",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "IgnorePromotionLimitation",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "Limi1PromotionPerItem",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "PromotionPosition",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "OrderStatus",
                table: "Orders");
        }
    }
}

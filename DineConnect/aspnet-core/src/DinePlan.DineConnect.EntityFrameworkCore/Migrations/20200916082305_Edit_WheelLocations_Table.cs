﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Edit_WheelLocations_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelLocations_ScreenMenus_ScreenMenuId",
                table: "WheelLocations");

            migrationBuilder.DropIndex(
                name: "IX_WheelLocations_ScreenMenuId",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "ScreenMenuId",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "WheelPostalCodeSearchType",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "WheelTaxeIds",
                table: "WheelLocations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuId",
                table: "WheelLocations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WheelPostalCodeSearchType",
                table: "WheelLocations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "WheelTaxeIds",
                table: "WheelLocations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelLocations_ScreenMenuId",
                table: "WheelLocations",
                column: "ScreenMenuId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelLocations_ScreenMenus_ScreenMenuId",
                table: "WheelLocations",
                column: "ScreenMenuId",
                principalTable: "ScreenMenus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

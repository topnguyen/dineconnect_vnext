﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_MenuItemId_cols_DemandPromotionExecutions_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "DemandPromotionExecutions");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "DemandPromotionExecutions");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "DemandPromotionExecutions");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "DemandPromotionExecutions");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "DemandPromotionExecutions");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemPortionId",
                table: "DemandPromotionExecutions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProductGroupId",
                table: "DemandPromotionExecutions",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MenuItemPortionId",
                table: "DemandPromotionExecutions");

            migrationBuilder.DropColumn(
                name: "ProductGroupId",
                table: "DemandPromotionExecutions");

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "DemandPromotionExecutions",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "DemandPromotionExecutions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "DemandPromotionExecutions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "DemandPromotionExecutions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "DemandPromotionExecutions",
                type: "bigint",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Column_Table_Order_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoinUse",
                table: "CliquePayments");

            migrationBuilder.DropColumn(
                name: "QrCode",
                table: "CliquePayments");

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "CliqueOrders",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Total",
                table: "CliqueOrders");

            migrationBuilder.AddColumn<decimal>(
                name: "CoinUse",
                table: "CliquePayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "QrCode",
                table: "CliquePayments",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

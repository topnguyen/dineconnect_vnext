﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class DineQueueImprove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerQueues_Customers_CustomerId",
                table: "CustomerQueues");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerQueues_Locations_LocationId",
                table: "CustomerQueues");

            migrationBuilder.DropIndex(
                name: "IX_CustomerQueues_LocationId",
                table: "CustomerQueues");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "CustomerQueues");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerQueues",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "FinishTime",
                table: "CustomerQueues",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerQueues_Customers_CustomerId",
                table: "CustomerQueues",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerQueues_Customers_CustomerId",
                table: "CustomerQueues");

            migrationBuilder.DropColumn(
                name: "FinishTime",
                table: "CustomerQueues");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerQueues",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "CustomerQueues",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerQueues_LocationId",
                table: "CustomerQueues",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerQueues_Customers_CustomerId",
                table: "CustomerQueues",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerQueues_Locations_LocationId",
                table: "CustomerQueues",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

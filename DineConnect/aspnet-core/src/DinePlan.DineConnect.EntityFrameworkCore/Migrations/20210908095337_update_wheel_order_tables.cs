﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_wheel_order_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WheelOrderItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WheelCartItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Note",
                table: "WheelOrderItems");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "WheelCartItems");
        }
    }
}

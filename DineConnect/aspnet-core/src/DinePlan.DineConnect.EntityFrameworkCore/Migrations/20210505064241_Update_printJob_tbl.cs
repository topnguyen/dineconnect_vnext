﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_printJob_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "PrinterMaps",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrinterMaps_DepartmentId",
                table: "PrinterMaps",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrinterMaps_Departments_DepartmentId",
                table: "PrinterMaps",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrinterMaps_Departments_DepartmentId",
                table: "PrinterMaps");

            migrationBuilder.DropIndex(
                name: "IX_PrinterMaps_DepartmentId",
                table: "PrinterMaps");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "PrinterMaps");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Cluster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TriggerApplicable",
                table: "WheelRecommendationMenu",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "RepeatTime",
                table: "WheelRecommendationMenu",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.CreateTable(
                name: "DelAggCharges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ApplicableOn = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    ExcludeDeliveryTypes = table.Column<string>(nullable: true),
                    FullFilmentTypes = table.Column<string>(nullable: true),
                    LocalRefCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggCharges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggImages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggImageTypeRefId = table.Column<int>(nullable: false),
                    DelAggTypeRefId = table.Column<int>(nullable: true),
                    ReferenceId = table.Column<int>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    AddOns = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggImages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggItemGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggItemGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggLanguages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggTypeRefId = table.Column<int>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    LanguageValue = table.Column<string>(nullable: true),
                    ReferenceId = table.Column<int>(nullable: true),
                    AddOns = table.Column<string>(nullable: true),
                    LanguageDescriptionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggLanguages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggLocationGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggLocationGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggModifierGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true),
                    Min = table.Column<int>(nullable: true),
                    Max = table.Column<int>(nullable: true),
                    OrderTagGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggModifierGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggModifierGroups_OrderTagGroups_OrderTagGroupId",
                        column: x => x.OrderTagGroupId,
                        principalTable: "OrderTagGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DelAggTaxes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    TaxTypeId = table.Column<int>(nullable: false),
                    TaxPercentage = table.Column<decimal>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggTaxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggVariantGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    LocalRefCode = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggVariantGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelTimingGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelTimingGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DelAggChargeMappings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggChargeId = table.Column<int>(nullable: false),
                    DelAggLocationGroupId = table.Column<int>(nullable: false),
                    DelItemGroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggChargeMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggChargeMappings_DelAggCharges_DelAggChargeId",
                        column: x => x.DelAggChargeId,
                        principalTable: "DelAggCharges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggChargeMappings_DelAggLocationGroups_DelAggLocationGroupId",
                        column: x => x.DelAggLocationGroupId,
                        principalTable: "DelAggLocationGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggChargeMappings_DelAggItemGroups_DelItemGroupId",
                        column: x => x.DelItemGroupId,
                        principalTable: "DelAggItemGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelAggLocations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    AddOns = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DelAggLocationGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggLocations_DelAggLocationGroups_DelAggLocationGroupId",
                        column: x => x.DelAggLocationGroupId,
                        principalTable: "DelAggLocationGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggLocations_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelAggModifiers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DelAggModifierGroupId = table.Column<int>(nullable: true),
                    OrderTagId = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true),
                    NestedModGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggModifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggModifiers_DelAggModifierGroups_DelAggModifierGroupId",
                        column: x => x.DelAggModifierGroupId,
                        principalTable: "DelAggModifierGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggModifiers_DelAggModifiers_NestedModGroupId",
                        column: x => x.NestedModGroupId,
                        principalTable: "DelAggModifiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggModifiers_OrderTags_OrderTagId",
                        column: x => x.OrderTagId,
                        principalTable: "OrderTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DelAggTaxMappings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggTaxId = table.Column<int>(nullable: false),
                    DelAggLocationGroupId = table.Column<int>(nullable: true),
                    DelItemGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggTaxMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggTaxMappings_DelAggLocationGroups_DelAggLocationGroupId",
                        column: x => x.DelAggLocationGroupId,
                        principalTable: "DelAggLocationGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggTaxMappings_DelAggTaxes_DelAggTaxId",
                        column: x => x.DelAggTaxId,
                        principalTable: "DelAggTaxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggTaxMappings_DelAggItemGroups_DelItemGroupId",
                        column: x => x.DelItemGroupId,
                        principalTable: "DelAggItemGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DelAggVariants",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DelAggVariantGroupId = table.Column<int>(nullable: false),
                    MenuItemPortionId = table.Column<int>(nullable: false),
                    SalesPrice = table.Column<decimal>(nullable: false),
                    MarkupPrice = table.Column<decimal>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true),
                    NestedModGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggVariants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggVariants_DelAggVariantGroups_DelAggVariantGroupId",
                        column: x => x.DelAggVariantGroupId,
                        principalTable: "DelAggVariantGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggVariants_MenuItemPortions_MenuItemPortionId",
                        column: x => x.MenuItemPortionId,
                        principalTable: "MenuItemPortions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggVariants_DelAggVariants_NestedModGroupId",
                        column: x => x.NestedModGroupId,
                        principalTable: "DelAggVariants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DelAggCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DelAggCatParentId = table.Column<int>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true),
                    DelTimingGroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggCategories_DelAggCategories_DelAggCatParentId",
                        column: x => x.DelAggCatParentId,
                        principalTable: "DelAggCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggCategories_DelTimingGroups_DelTimingGroupId",
                        column: x => x.DelTimingGroupId,
                        principalTable: "DelTimingGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelTimingDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelTimingGroupId = table.Column<int>(nullable: false),
                    StartHour = table.Column<int>(nullable: false),
                    StartMinute = table.Column<int>(nullable: false),
                    EndHour = table.Column<int>(nullable: false),
                    EndMinute = table.Column<int>(nullable: false),
                    Days = table.Column<string>(nullable: true),
                    AddOns = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelTimingDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelTimingDetails_DelTimingGroups_DelTimingGroupId",
                        column: x => x.DelTimingGroupId,
                        principalTable: "DelTimingGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelAggLocMappings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DelAggLocationId = table.Column<int>(nullable: false),
                    DelAggTypeRefId = table.Column<int>(nullable: false),
                    GatewayCode = table.Column<string>(nullable: true),
                    RemoteCode = table.Column<string>(nullable: true),
                    DeliveryUrl = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    AddOns = table.Column<string>(nullable: true),
                    MenuContents = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggLocMappings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggLocMappings_DelAggLocations_DelAggLocationId",
                        column: x => x.DelAggLocationId,
                        principalTable: "DelAggLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelAggItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    FoodType = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    LocalRefCode = table.Column<string>(nullable: true),
                    IsRecommended = table.Column<bool>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    Serves = table.Column<decimal>(nullable: false),
                    DelAggVariantGroupId = table.Column<int>(nullable: false),
                    DelAggTaxId = table.Column<int>(nullable: true),
                    DelAggItemGroupId = table.Column<int>(nullable: false),
                    DelAggCatId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggItems_DelAggCategories_DelAggCatId",
                        column: x => x.DelAggCatId,
                        principalTable: "DelAggCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggItems_DelAggItemGroups_DelAggItemGroupId",
                        column: x => x.DelAggItemGroupId,
                        principalTable: "DelAggItemGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggItems_DelAggTaxes_DelAggTaxId",
                        column: x => x.DelAggTaxId,
                        principalTable: "DelAggTaxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggItems_DelAggVariantGroups_DelAggVariantGroupId",
                        column: x => x.DelAggVariantGroupId,
                        principalTable: "DelAggVariantGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggItems_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DelAggLocationItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggLocMappingId = table.Column<int>(nullable: false),
                    DelAggItemId = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    InActive = table.Column<bool>(nullable: false),
                    AddOns = table.Column<string>(nullable: true),
                    DelAggVariantId = table.Column<int>(nullable: true),
                    DelAggModifierId = table.Column<int>(nullable: true),
                    DelAggPriceTypeRefId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggLocationItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggLocationItems_DelAggItems_DelAggItemId",
                        column: x => x.DelAggItemId,
                        principalTable: "DelAggItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggLocationItems_DelAggLocMappings_DelAggLocMappingId",
                        column: x => x.DelAggLocMappingId,
                        principalTable: "DelAggLocMappings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggLocationItems_DelAggModifiers_DelAggModifierId",
                        column: x => x.DelAggModifierId,
                        principalTable: "DelAggModifiers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelAggLocationItems_DelAggVariants_DelAggVariantId",
                        column: x => x.DelAggVariantId,
                        principalTable: "DelAggVariants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DelAggModifierGroupItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    DelAggModifierGroupId = table.Column<int>(nullable: false),
                    DelAggItemId = table.Column<int>(nullable: false),
                    Addon = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelAggModifierGroupItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DelAggModifierGroupItems_DelAggItems_DelAggItemId",
                        column: x => x.DelAggItemId,
                        principalTable: "DelAggItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DelAggModifierGroupItems_DelAggModifierGroups_DelAggModifierGroupId",
                        column: x => x.DelAggModifierGroupId,
                        principalTable: "DelAggModifierGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DelAggCategories_DelAggCatParentId",
                table: "DelAggCategories",
                column: "DelAggCatParentId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggCategories_DelTimingGroupId",
                table: "DelAggCategories",
                column: "DelTimingGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggChargeMappings_DelAggChargeId",
                table: "DelAggChargeMappings",
                column: "DelAggChargeId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggChargeMappings_DelAggLocationGroupId",
                table: "DelAggChargeMappings",
                column: "DelAggLocationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggChargeMappings_DelItemGroupId",
                table: "DelAggChargeMappings",
                column: "DelItemGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggItems_DelAggCatId",
                table: "DelAggItems",
                column: "DelAggCatId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggItems_DelAggItemGroupId",
                table: "DelAggItems",
                column: "DelAggItemGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggItems_DelAggTaxId",
                table: "DelAggItems",
                column: "DelAggTaxId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggItems_DelAggVariantGroupId",
                table: "DelAggItems",
                column: "DelAggVariantGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggItems_MenuItemId",
                table: "DelAggItems",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocationItems_DelAggItemId",
                table: "DelAggLocationItems",
                column: "DelAggItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocationItems_DelAggLocMappingId",
                table: "DelAggLocationItems",
                column: "DelAggLocMappingId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocationItems_DelAggModifierId",
                table: "DelAggLocationItems",
                column: "DelAggModifierId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocationItems_DelAggVariantId",
                table: "DelAggLocationItems",
                column: "DelAggVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocations_DelAggLocationGroupId",
                table: "DelAggLocations",
                column: "DelAggLocationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocations_LocationId",
                table: "DelAggLocations",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggLocMappings_DelAggLocationId",
                table: "DelAggLocMappings",
                column: "DelAggLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifierGroupItems_DelAggItemId",
                table: "DelAggModifierGroupItems",
                column: "DelAggItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifierGroupItems_DelAggModifierGroupId",
                table: "DelAggModifierGroupItems",
                column: "DelAggModifierGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifierGroups_OrderTagGroupId",
                table: "DelAggModifierGroups",
                column: "OrderTagGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifiers_DelAggModifierGroupId",
                table: "DelAggModifiers",
                column: "DelAggModifierGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifiers_NestedModGroupId",
                table: "DelAggModifiers",
                column: "NestedModGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggModifiers_OrderTagId",
                table: "DelAggModifiers",
                column: "OrderTagId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggTaxMappings_DelAggLocationGroupId",
                table: "DelAggTaxMappings",
                column: "DelAggLocationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggTaxMappings_DelAggTaxId",
                table: "DelAggTaxMappings",
                column: "DelAggTaxId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggTaxMappings_DelItemGroupId",
                table: "DelAggTaxMappings",
                column: "DelItemGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggVariants_DelAggVariantGroupId",
                table: "DelAggVariants",
                column: "DelAggVariantGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggVariants_MenuItemPortionId",
                table: "DelAggVariants",
                column: "MenuItemPortionId");

            migrationBuilder.CreateIndex(
                name: "IX_DelAggVariants_NestedModGroupId",
                table: "DelAggVariants",
                column: "NestedModGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DelTimingDetails_DelTimingGroupId",
                table: "DelTimingDetails",
                column: "DelTimingGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DelAggChargeMappings");

            migrationBuilder.DropTable(
                name: "DelAggImages");

            migrationBuilder.DropTable(
                name: "DelAggLanguages");

            migrationBuilder.DropTable(
                name: "DelAggLocationItems");

            migrationBuilder.DropTable(
                name: "DelAggModifierGroupItems");

            migrationBuilder.DropTable(
                name: "DelAggTaxMappings");

            migrationBuilder.DropTable(
                name: "DelTimingDetails");

            migrationBuilder.DropTable(
                name: "DelAggCharges");

            migrationBuilder.DropTable(
                name: "DelAggLocMappings");

            migrationBuilder.DropTable(
                name: "DelAggModifiers");

            migrationBuilder.DropTable(
                name: "DelAggVariants");

            migrationBuilder.DropTable(
                name: "DelAggItems");

            migrationBuilder.DropTable(
                name: "DelAggLocations");

            migrationBuilder.DropTable(
                name: "DelAggModifierGroups");

            migrationBuilder.DropTable(
                name: "DelAggCategories");

            migrationBuilder.DropTable(
                name: "DelAggItemGroups");

            migrationBuilder.DropTable(
                name: "DelAggTaxes");

            migrationBuilder.DropTable(
                name: "DelAggVariantGroups");

            migrationBuilder.DropTable(
                name: "DelAggLocationGroups");

            migrationBuilder.DropTable(
                name: "DelTimingGroups");

            migrationBuilder.AlterColumn<int>(
                name: "TriggerApplicable",
                table: "WheelRecommendationMenu",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RepeatTime",
                table: "WheelRecommendationMenu",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Updated_ProductComboGroup_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "ProductComboGroups",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "ProductComboGroups",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "ProductComboGroups");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "ProductComboGroups");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShipmentRoutePointOrders",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    RoutePointId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    TrackKey = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentRoutePointOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentRoutePointOrders_ShipmentOrders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "ShipmentOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShipmentRoutePointOrders_ShipmentRoutePoints_RoutePointId",
                        column: x => x.RoutePointId,
                        principalTable: "ShipmentRoutePoints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutePointOrders_OrderId",
                table: "ShipmentRoutePointOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutePointOrders_RoutePointId",
                table: "ShipmentRoutePointOrders",
                column: "RoutePointId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShipmentRoutePointOrders");
        }
    }
}

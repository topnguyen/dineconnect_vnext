﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddPortionPaxMenuItemPortion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TriggerApplicable",
                table: "WheelRecommendationMenu",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "RepeatTime",
                table: "WheelRecommendationMenu",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<int>(
                name: "NumberOfPax",
                table: "MenuItemPortions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PreparationTime",
                table: "MenuItemPortions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfPax",
                table: "MenuItemPortions");

            migrationBuilder.DropColumn(
                name: "PreparationTime",
                table: "MenuItemPortions");

            migrationBuilder.AlterColumn<int>(
                name: "TriggerApplicable",
                table: "WheelRecommendationMenu",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RepeatTime",
                table: "WheelRecommendationMenu",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

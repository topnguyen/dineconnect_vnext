﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_wheelCartitem_talbe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems");
        }
    }
}

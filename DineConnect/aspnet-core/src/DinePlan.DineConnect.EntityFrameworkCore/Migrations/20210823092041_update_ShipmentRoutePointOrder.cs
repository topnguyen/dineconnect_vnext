﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_ShipmentRoutePointOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "ShipmentRoutePointOrders");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "ShipmentRoutePointOrders");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "ShipmentRoutePointOrders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "ShipmentRoutePointOrders",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "ShipmentRoutePointOrders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "ShipmentRoutePointOrders",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_note_to_wheeelOrder_wheelTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WheelTickets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "WheelOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Note",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "WheelOrders");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddUpMenuItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          
            migrationBuilder.DropTable(
                name: "ExternalPriceDefinitions");

           
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "PriceTags");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "PriceTags");

           

            migrationBuilder.AddColumn<string>(
                name: "AliasName",
                table: "PrintTemplates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AliasName",
                table: "PrintTemplateConditions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AliasName",
                table: "PrintJobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AliasName",
                table: "Printers",
                nullable: true);

          
            migrationBuilder.CreateTable(
                name: "PriceTagLocationPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    PriceTagDefinitionId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceTagLocationPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceTagLocationPrices_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PriceTagLocationPrices_PriceTagDefinitions_PriceTagDefinitionId",
                        column: x => x.PriceTagDefinitionId,
                        principalTable: "PriceTagDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

          
            migrationBuilder.CreateTable(
                name: "UpMenuItemLocationPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    UpMenuItemId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpMenuItemLocationPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpMenuItemLocationPrices_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpMenuItemLocationPrices_UpMenuItems_UpMenuItemId",
                        column: x => x.UpMenuItemId,
                        principalTable: "UpMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PriceTagLocationPrices_LocationId",
                table: "PriceTagLocationPrices",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceTagLocationPrices_PriceTagDefinitionId",
                table: "PriceTagLocationPrices",
                column: "PriceTagDefinitionId");

            migrationBuilder.CreateIndex(
                name: "IX_UpMenuItemLocationPrices_LocationId",
                table: "UpMenuItemLocationPrices",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_UpMenuItemLocationPrices_UpMenuItemId",
                table: "UpMenuItemLocationPrices",
                column: "UpMenuItemId");

          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreTickets_Locations_LocationId",
                table: "PreTickets");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramSettingValues_ProgramSettingTemplates_ProgramSettingTemplateId",
                table: "ProgramSettingValues");

            migrationBuilder.DropTable(
                name: "PriceTagLocationPrices");

            migrationBuilder.DropTable(
                name: "ProgramSettingTemplates");

            migrationBuilder.DropTable(
                name: "UpMenuItemLocationPrices");

            migrationBuilder.DropColumn(
                name: "MaxQty",
                table: "UpMenuItems");

            migrationBuilder.DropColumn(
                name: "AliasName",
                table: "PrintTemplates");

            migrationBuilder.DropColumn(
                name: "AliasName",
                table: "PrintTemplateConditions");

            migrationBuilder.DropColumn(
                name: "AliasName",
                table: "PrintJobs");

            migrationBuilder.DropColumn(
                name: "AliasName",
                table: "Printers");

            migrationBuilder.DropColumn(
                name: "Barcode",
                table: "MenuItemPortions");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "ProgramSettingValues",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductComboGroups",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Barcode",
                table: "ProductComboes",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "PriceTags",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "PriceTags",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DefaultValue",
                table: "PreTickets",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "PreTickets",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "PreTickets",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MenuItemPortions",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ExternalPriceDefinitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ExternalPriceTagId = table.Column<int>(type: "int", nullable: false),
                    MenuPortionId = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalPriceDefinitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalPriceDefinitions_MenuItemPortions_MenuPortionId",
                        column: x => x.MenuPortionId,
                        principalTable: "MenuItemPortions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PreTicket",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "FK_PreTicket_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExternalPriceDefinitions_MenuPortionId",
                table: "ExternalPriceDefinitions",
                column: "MenuPortionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramSettingValues_PreTickets_ProgramSettingTemplateId",
                table: "ProgramSettingValues",
                column: "ProgramSettingTemplateId",
                principalTable: "PreTickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

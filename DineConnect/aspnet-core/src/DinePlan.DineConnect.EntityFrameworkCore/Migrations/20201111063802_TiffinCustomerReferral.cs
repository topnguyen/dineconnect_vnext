﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TiffinCustomerReferral : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Customers_ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Customers_ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferralCode",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedByPaymentId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedRemarks",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedTime",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedType",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedValue",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.CreateTable(
                name: "TiffinCustomerReferrals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    ReferralCode = table.Column<string>(nullable: true),
                    ReferrerCustomerId = table.Column<int>(nullable: true),
                    ReferrerCustomerEarnedType = table.Column<int>(nullable: false),
                    ReferrerCustomerEarnedValue = table.Column<int>(nullable: false),
                    ReferrerCustomerEarnedTime = table.Column<DateTime>(nullable: true),
                    ReferrerCustomerEarnedByPaymentId = table.Column<int>(nullable: true),
                    ReferrerCustomerEarnedRemarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinCustomerReferrals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiffinCustomerReferrals_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TiffinCustomerReferrals_Customers_ReferrerCustomerId",
                        column: x => x.ReferrerCustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TiffinCustomerReferrals_CustomerId",
                table: "TiffinCustomerReferrals",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_TiffinCustomerReferrals_ReferrerCustomerId",
                table: "TiffinCustomerReferrals",
                column: "ReferrerCustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TiffinCustomerReferrals");

            migrationBuilder.AddColumn<string>(
                name: "ReferralCode",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedByPaymentId",
                table: "Customers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferrerCustomerEarnedRemarks",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReferrerCustomerEarnedTime",
                table: "Customers",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedType",
                table: "Customers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedValue",
                table: "Customers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerId",
                table: "Customers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ReferrerCustomerId",
                table: "Customers",
                column: "ReferrerCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Customers_ReferrerCustomerId",
                table: "Customers",
                column: "ReferrerCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

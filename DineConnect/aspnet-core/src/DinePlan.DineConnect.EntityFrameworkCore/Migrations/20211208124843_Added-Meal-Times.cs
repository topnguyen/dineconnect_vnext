﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddedMealTimes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CliqueMealTimeId",
                table: "CliqueMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CliqueMealTimeId",
                table: "CliqueItemStocks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CliqueMealTimes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    SortOrder = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CliqueMealTimes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CliqueMenus_CliqueMealTimeId",
                table: "CliqueMenus",
                column: "CliqueMealTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueItemStocks_CliqueMealTimeId",
                table: "CliqueItemStocks",
                column: "CliqueMealTimeId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueItemStocks_CliqueMealTimes_CliqueMealTimeId",
                table: "CliqueItemStocks",
                column: "CliqueMealTimeId",
                principalTable: "CliqueMealTimes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueMenus_CliqueMealTimes_CliqueMealTimeId",
                table: "CliqueMenus",
                column: "CliqueMealTimeId",
                principalTable: "CliqueMealTimes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueItemStocks_CliqueMealTimes_CliqueMealTimeId",
                table: "CliqueItemStocks");

            migrationBuilder.DropForeignKey(
                name: "FK_CliqueMenus_CliqueMealTimes_CliqueMealTimeId",
                table: "CliqueMenus");

            migrationBuilder.DropTable(
                name: "CliqueMealTimes");

            migrationBuilder.DropIndex(
                name: "IX_CliqueMenus_CliqueMealTimeId",
                table: "CliqueMenus");

            migrationBuilder.DropIndex(
                name: "IX_CliqueItemStocks_CliqueMealTimeId",
                table: "CliqueItemStocks");

            migrationBuilder.DropColumn(
                name: "CliqueMealTimeId",
                table: "CliqueMenus");

            migrationBuilder.DropColumn(
                name: "CliqueMealTimeId",
                table: "CliqueItemStocks");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CardAndCardType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConnectCardTypeCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCardTypeCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCardTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: true),
                    UseOneTime = table.Column<bool>(nullable: false),
                    Oid = table.Column<int>(nullable: false),
                    Length = table.Column<int>(nullable: false),
                    ConnectCardTypeCategoryId = table.Column<int>(nullable: true),
                    Tag = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCardTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectCardTypes_ConnectCardTypeCategories_ConnectCardTypeCategoryId",
                        column: x => x.ConnectCardTypeCategoryId,
                        principalTable: "ConnectCardTypeCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    ConnectCardTypeId = table.Column<int>(nullable: false),
                    CardNo = table.Column<string>(maxLength: 30, nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Redeemed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectCards_ConnectCardTypes_ConnectCardTypeId",
                        column: x => x.ConnectCardTypeId,
                        principalTable: "ConnectCardTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCardRedemptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    ConnectCardId = table.Column<int>(nullable: false),
                    TicketNumber = table.Column<string>(maxLength: 50, nullable: true),
                    LocationId = table.Column<int>(nullable: false),
                    RedemptionDate = table.Column<DateTime>(nullable: false),
                    RedemptionReferences = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCardRedemptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectCardRedemptions_ConnectCards_ConnectCardId",
                        column: x => x.ConnectCardId,
                        principalTable: "ConnectCards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCardRedemptions_ConnectCardId",
                table: "ConnectCardRedemptions",
                column: "ConnectCardId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCards_ConnectCardTypeId",
                table: "ConnectCards",
                column: "ConnectCardTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCardTypes_ConnectCardTypeCategoryId",
                table: "ConnectCardTypes",
                column: "ConnectCardTypeCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConnectCardRedemptions");

            migrationBuilder.DropTable(
                name: "ConnectCards");

            migrationBuilder.DropTable(
                name: "ConnectCardTypes");

            migrationBuilder.DropTable(
                name: "ConnectCardTypeCategories");
        }
    }
}

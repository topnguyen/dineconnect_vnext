﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class remove_Locations_Group_from_ConnectCardTypes_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "ConnectCardTypes");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "ConnectCardTypes");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "ConnectCardTypes");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "ConnectCardTypes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "ConnectCardTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "ConnectCardTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "ConnectCardTypes",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "ConnectCardTypes",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class FutureDateData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FutureDateTo",
                table: "FutureDateInformations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ObjectContents",
                table: "FutureDateInformations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FutureDateTo",
                table: "FutureDateInformations");

            migrationBuilder.DropColumn(
                name: "ObjectContents",
                table: "FutureDateInformations");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_DownloadImage_Files_cols_To_ProductComboItem_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DownloadImage",
                table: "ProductComboItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "ProductComboItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DownloadImage",
                table: "ProductComboItems");

            migrationBuilder.DropColumn(
                name: "Files",
                table: "ProductComboItems");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Updated_ScreenMenu_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoryColumnCount",
                table: "ScreenMenus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CategoryColumnWidthRate",
                table: "ScreenMenus",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryColumnCount",
                table: "ScreenMenus");

            migrationBuilder.DropColumn(
                name: "CategoryColumnWidthRate",
                table: "ScreenMenus");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Alter_table_WheelDepartments_add_IsEnableDelivery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsEnableDelivery",
                table: "WheelDepartments",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsEnableDelivery",
                table: "WheelDepartments");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_table_TrackRoute : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TrackRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    ShipFromId = table.Column<int>(nullable: true),
                    DriverId = table.Column<int>(nullable: true),
                    IsStartFromDepot = table.Column<bool>(nullable: false),
                    IsEndAtDepot = table.Column<bool>(nullable: false),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrackRoutes_TrackDrivers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "TrackDrivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TrackRoutes_TrackDepots_ShipFromId",
                        column: x => x.ShipFromId,
                        principalTable: "TrackDepots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TrackRoutes_DriverId",
                table: "TrackRoutes",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_TrackRoutes_ShipFromId",
                table: "TrackRoutes",
                column: "ShipFromId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrackRoutes");
        }
    }
}

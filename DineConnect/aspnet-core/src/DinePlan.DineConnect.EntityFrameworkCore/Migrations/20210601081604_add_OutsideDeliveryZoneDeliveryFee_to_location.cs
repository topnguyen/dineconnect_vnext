﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_OutsideDeliveryZoneDeliveryFee_to_location : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "OutsideDeliveryZoneDeliveryFee",
                table: "WheelLocations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OutsideDeliveryZoneDeliveryFee",
                table: "WheelLocations");
        }
    }
}

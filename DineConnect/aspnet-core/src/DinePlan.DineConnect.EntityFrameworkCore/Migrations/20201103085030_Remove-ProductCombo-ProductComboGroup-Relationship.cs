﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemoveProductComboProductComboGroupRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups");

            migrationBuilder.DropIndex(
                name: "IX_ProductComboGroups_ProductComboId",
                table: "ProductComboGroups");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ProductComboGroups_ProductComboId",
                table: "ProductComboGroups",
                column: "ProductComboId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups",
                column: "ProductComboId",
                principalTable: "ProductComboes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

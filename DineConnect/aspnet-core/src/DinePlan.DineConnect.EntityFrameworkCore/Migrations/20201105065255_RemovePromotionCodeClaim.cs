﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemovePromotionCodeClaim : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TiffinPromotionCodeClaim");

            migrationBuilder.AddColumn<double>(
                name: "ClaimedAmount",
                table: "TiffinPromotionCode",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ClaimedTime",
                table: "TiffinPromotionCode",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<bool>(
                name: "IsClaimed",
                table: "TiffinPromotionCode",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClaimedAmount",
                table: "TiffinPromotionCode");

            migrationBuilder.DropColumn(
                name: "ClaimedTime",
                table: "TiffinPromotionCode");

            migrationBuilder.DropColumn(
                name: "IsClaimed",
                table: "TiffinPromotionCode");

            migrationBuilder.CreateTable(
                name: "TiffinPromotionCodeClaim",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Amount = table.Column<double>(type: "float", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PromotionCodeId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    Time = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinPromotionCodeClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiffinPromotionCodeClaim_TiffinPromotionCode_PromotionCodeId",
                        column: x => x.PromotionCodeId,
                        principalTable: "TiffinPromotionCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCodeClaim_PromotionCodeId",
                table: "TiffinPromotionCodeClaim",
                column: "PromotionCodeId");
        }
    }
}

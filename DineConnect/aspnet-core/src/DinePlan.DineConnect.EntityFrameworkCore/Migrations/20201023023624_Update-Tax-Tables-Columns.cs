﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class UpdateTaxTablesColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxLocations_Locations_LocationRefId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxLocations_TransactionTypes_TransactionTypeId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropIndex(
                name: "IX_DinePlanTaxLocations_LocationRefId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropIndex(
                name: "IX_DinePlanTaxLocations_TransactionTypeId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "LocationRefId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "Group",
                table: "DinePlanTaxes");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "DinePlanTaxes");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "DinePlanTaxes");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "DinePlanTaxes");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "DinePlanTaxes");

            migrationBuilder.AddColumn<int>(
                name: "DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "DinePlanTaxLocations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "DinePlanTaxLocations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "DinePlanTaxLocations",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "DinePlanTaxLocations",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "DinePlanTaxLocations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "DinePlanTaxLocations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransactionTypeId",
                table: "DinePlanTaxes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DinePlanTaxMappings_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                column: "DinePlanTaxLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_DinePlanTaxLocations_DinePlanTaxRefId",
                table: "DinePlanTaxLocations",
                column: "DinePlanTaxRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DinePlanTaxes_TransactionTypeId",
                table: "DinePlanTaxes",
                column: "TransactionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxes_TransactionTypes_TransactionTypeId",
                table: "DinePlanTaxes",
                column: "TransactionTypeId",
                principalTable: "TransactionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxLocations_DinePlanTaxes_DinePlanTaxRefId",
                table: "DinePlanTaxLocations",
                column: "DinePlanTaxRefId",
                principalTable: "DinePlanTaxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings",
                column: "DinePlanTaxLocationId",
                principalTable: "DinePlanTaxLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql("UPDATE DinePlanTaxMappings SET DinePlanTaxLocationId = DinePlanTaxLocationRefId;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxes_TransactionTypes_TransactionTypeId",
                table: "DinePlanTaxes");

            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxLocations_DinePlanTaxes_DinePlanTaxRefId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_DinePlanTaxMappings_DinePlanTaxLocations_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings");

            migrationBuilder.DropIndex(
                name: "IX_DinePlanTaxMappings_DinePlanTaxLocationId",
                table: "DinePlanTaxMappings");

            migrationBuilder.DropIndex(
                name: "IX_DinePlanTaxLocations_DinePlanTaxRefId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropIndex(
                name: "IX_DinePlanTaxes_TransactionTypeId",
                table: "DinePlanTaxes");

            migrationBuilder.DropColumn(
                name: "DinePlanTaxLocationId",
                table: "DinePlanTaxMappings");

            migrationBuilder.DropColumn(
                name: "Group",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "DinePlanTaxLocations");

            migrationBuilder.DropColumn(
                name: "TransactionTypeId",
                table: "DinePlanTaxes");

            migrationBuilder.AddColumn<int>(
                name: "LocationRefId",
                table: "DinePlanTaxLocations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "DinePlanTaxes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "DinePlanTaxes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "DinePlanTaxes",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "DinePlanTaxes",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                table: "DinePlanTaxes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DinePlanTaxLocations_LocationRefId",
                table: "DinePlanTaxLocations",
                column: "LocationRefId");

            migrationBuilder.CreateIndex(
                name: "IX_DinePlanTaxLocations_TransactionTypeId",
                table: "DinePlanTaxLocations",
                column: "TransactionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxLocations_Locations_LocationRefId",
                table: "DinePlanTaxLocations",
                column: "LocationRefId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DinePlanTaxLocations_TransactionTypes_TransactionTypeId",
                table: "DinePlanTaxLocations",
                column: "TransactionTypeId",
                principalTable: "TransactionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
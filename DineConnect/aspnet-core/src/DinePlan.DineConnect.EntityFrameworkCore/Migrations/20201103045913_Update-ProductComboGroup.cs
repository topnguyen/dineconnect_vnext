﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class UpdateProductComboGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups");

            migrationBuilder.AlterColumn<int>(
                name: "ProductComboId",
                table: "ProductComboGroups",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups",
                column: "ProductComboId",
                principalTable: "ProductComboes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups");

            migrationBuilder.AlterColumn<int>(
                name: "ProductComboId",
                table: "ProductComboGroups",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductComboGroups_ProductComboes_ProductComboId",
                table: "ProductComboGroups",
                column: "ProductComboId",
                principalTable: "ProductComboes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

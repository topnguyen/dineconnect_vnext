﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_shipment_module_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentRoutePoints_ShipmentRoutes_RouterId",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropIndex(
                name: "IX_ShipmentRoutePoints_RouterId",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "Clients",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "IsRealRoute",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "RouteDate",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "RouterId",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "TimeSlot",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "Volume",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "ShipmentRoutePoints");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StateDate",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "Priority",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PlanDepartureDate",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PlanArrivalDate",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<bool>(
                name: "EtaOutOfTime",
                table: "ShipmentRoutePoints",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EtaEnRoute",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DepartureDate",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ArrivalDate",
                table: "ShipmentRoutePoints",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "GeoStatus",
                table: "ShipmentRoutePoints",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutePoints_RouteId",
                table: "ShipmentRoutePoints",
                column: "RouteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentRoutePoints_ShipmentRoutes_RouteId",
                table: "ShipmentRoutePoints",
                column: "RouteId",
                principalTable: "ShipmentRoutes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentRoutePoints_ShipmentRoutes_RouteId",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropIndex(
                name: "IX_ShipmentRoutePoints_RouteId",
                table: "ShipmentRoutePoints");

            migrationBuilder.DropColumn(
                name: "GeoStatus",
                table: "ShipmentRoutePoints");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StateDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Priority",
                table: "ShipmentRoutePoints",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "PlanDepartureDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "PlanArrivalDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EtaOutOfTime",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<string>(
                name: "EtaEnRoute",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DepartureDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ArrivalDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Clients",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRealRoute",
                table: "ShipmentRoutePoints",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "RouteDate",
                table: "ShipmentRoutePoints",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "RouterId",
                table: "ShipmentRoutePoints",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeSlot",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Volume",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Weight",
                table: "ShipmentRoutePoints",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutePoints_RouterId",
                table: "ShipmentRoutePoints",
                column: "RouterId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentRoutePoints_ShipmentRoutes_RouterId",
                table: "ShipmentRoutePoints",
                column: "RouterId",
                principalTable: "ShipmentRoutes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeTiffinOrderTag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.DropTable(
                name: "Terms");

           

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderTags",
                table: "OrderTags");

            migrationBuilder.RenameTable(
                name: "OrderTags",
                newName: "TiffinOrderTags");

            migrationBuilder.RenameIndex(
                name: "IX_OrderTags_TenantId",
                table: "TiffinOrderTags",
                newName: "IX_TiffinOrderTags_TenantId");

          

            migrationBuilder.AddColumn<string>(
                name: "Images",
                table: "TiffinOrderTags",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Ordinal",
                table: "TiffinOrderTags",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TiffinOrderTags",
                table: "TiffinOrderTags",
                column: "Id");

            migrationBuilder.RenameColumn(
                name: "TiffinCustomerId",
                table: "CustomerAddressDetails",
                newName: "CustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerAddressDetails_Customers_CustomerId",
                table: "CustomerAddressDetails");

            migrationBuilder.DropIndex(
                name: "IX_CustomerAddressDetails_CustomerId",
                table: "CustomerAddressDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TiffinOrderTags",
                table: "TiffinOrderTags");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "CustomerAddressDetails");

            migrationBuilder.DropColumn(
                name: "Images",
                table: "TiffinOrderTags");

            migrationBuilder.DropColumn(
                name: "Ordinal",
                table: "TiffinOrderTags");

            migrationBuilder.RenameTable(
                name: "TiffinOrderTags",
                newName: "OrderTags");

            migrationBuilder.RenameIndex(
                name: "IX_TiffinOrderTags_TenantId",
                table: "OrderTags",
                newName: "IX_OrderTags_TenantId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderTags",
                table: "OrderTags",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Terms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Ordinal = table.Column<long>(type: "bigint", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Terms", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerAddressDetails_TiffinCustomerId",
                table: "CustomerAddressDetails",
                column: "TiffinCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerAddressDetails_Customers_TiffinCustomerId",
                table: "CustomerAddressDetails",
                column: "TiffinCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

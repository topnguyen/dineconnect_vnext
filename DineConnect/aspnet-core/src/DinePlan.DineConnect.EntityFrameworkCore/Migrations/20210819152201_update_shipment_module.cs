﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_shipment_module : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RouteSites");

            migrationBuilder.DropTable(
                name: "TrackRoutes");

            migrationBuilder.DropTable(
                name: "TrackDrivers");

            migrationBuilder.DropTable(
                name: "TrackDepots");

            migrationBuilder.CreateTable(
                name: "ShipmentDepots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    StateProvinceCountry = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    FullAddress = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentDepots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Client = table.Column<string>(nullable: true),
                    AddressId = table.Column<int>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    GeoStatus = table.Column<string>(nullable: true),
                    Warehouse = table.Column<string>(nullable: true),
                    Customer = table.Column<string>(nullable: true),
                    Zone = table.Column<string>(nullable: true),
                    DeliveryTimeFrom = table.Column<DateTime>(nullable: false),
                    DeliveryTimeTo = table.Column<DateTime>(nullable: false),
                    Weight = table.Column<decimal>(nullable: true),
                    Size = table.Column<decimal>(nullable: true),
                    Volume = table.Column<decimal>(nullable: true),
                    Pallets = table.Column<decimal>(nullable: true),
                    ContactName = table.Column<string>(nullable: true),
                    Tel = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    COD = table.Column<int>(nullable: false),
                    GPSLat = table.Column<double>(nullable: false),
                    GPSLon = table.Column<double>(nullable: false),
                    IsLoading = table.Column<int>(nullable: false),
                    IsPD = table.Column<bool>(nullable: false),
                    CreateSource = table.Column<int>(nullable: false),
                    Barcode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentOrders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentDrivers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Driver = table.Column<string>(nullable: true),
                    Vehicle = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    HomeAddress = table.Column<string>(nullable: true),
                    TrackDepotId = table.Column<int>(nullable: true),
                    Zone = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentDrivers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentDrivers_ShipmentDepots_TrackDepotId",
                        column: x => x.TrackDepotId,
                        principalTable: "ShipmentDepots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false),
                    TrackSiteId = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    ActualQuantity = table.Column<decimal>(nullable: true),
                    ExtraQuantity = table.Column<decimal>(nullable: true),
                    RejectedQuantity = table.Column<decimal>(nullable: true),
                    Cost = table.Column<decimal>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    ItemExtCode = table.Column<string>(nullable: true),
                    HasPhoto = table.Column<bool>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true),
                    OrderNum = table.Column<int>(nullable: false),
                    ItemBarcode = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Metrik = table.Column<string>(nullable: true),
                    ExtCode = table.Column<string>(nullable: true),
                    Barcode = table.Column<string>(nullable: true),
                    DeliveryScan = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentItems_ShipmentOrders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "ShipmentOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    ShipFromId = table.Column<int>(nullable: true),
                    DriverId = table.Column<int>(nullable: true),
                    IsStartFromDepot = table.Column<bool>(nullable: false),
                    IsEndAtDepot = table.Column<bool>(nullable: false),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentRoutes_ShipmentDrivers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "ShipmentDrivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ShipmentRoutes_ShipmentDepots_ShipFromId",
                        column: x => x.ShipFromId,
                        principalTable: "ShipmentDepots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShipmentRoutePoints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    StateDate = table.Column<DateTime>(nullable: false),
                    HasPhoto = table.Column<bool>(nullable: false),
                    ArrivalDate = table.Column<DateTime>(nullable: false),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    PlanDistance = table.Column<decimal>(nullable: true),
                    PlanArrivalDate = table.Column<DateTime>(nullable: false),
                    PlanDepartureDate = table.Column<DateTime>(nullable: false),
                    Lock = table.Column<bool>(nullable: false),
                    Weight = table.Column<string>(nullable: true),
                    Volume = table.Column<string>(nullable: true),
                    Clients = table.Column<string>(nullable: true),
                    RouteDate = table.Column<DateTime>(nullable: false),
                    IsRealRoute = table.Column<bool>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    TimeSlot = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    EtaOutOfTime = table.Column<string>(nullable: true),
                    EtaEnRoute = table.Column<string>(nullable: true),
                    RouteId = table.Column<int>(nullable: false),
                    RouterId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipmentRoutePoints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShipmentRoutePoints_ShipmentRoutes_RouterId",
                        column: x => x.RouterId,
                        principalTable: "ShipmentRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentDrivers_TrackDepotId",
                table: "ShipmentDrivers",
                column: "TrackDepotId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentItems_OrderId",
                table: "ShipmentItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutePoints_RouterId",
                table: "ShipmentRoutePoints",
                column: "RouterId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutes_DriverId",
                table: "ShipmentRoutes",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentRoutes_ShipFromId",
                table: "ShipmentRoutes",
                column: "ShipFromId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShipmentItems");

            migrationBuilder.DropTable(
                name: "ShipmentRoutePoints");

            migrationBuilder.DropTable(
                name: "ShipmentOrders");

            migrationBuilder.DropTable(
                name: "ShipmentRoutes");

            migrationBuilder.DropTable(
                name: "ShipmentDrivers");

            migrationBuilder.DropTable(
                name: "ShipmentDepots");

            migrationBuilder.CreateTable(
                name: "TrackDepots",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FullAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Latitude = table.Column<double>(type: "float", nullable: true),
                    Longitude = table.Column<double>(type: "float", nullable: true),
                    PostalCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StateProvinceCountry = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackDepots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrackDrivers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CompanyId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Driver = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HomeAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    TrackDepotId = table.Column<int>(type: "int", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Vehicle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Zone = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackDrivers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrackDrivers_TrackDepots_TrackDepotId",
                        column: x => x.TrackDepotId,
                        principalTable: "TrackDepots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TrackRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DriverId = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsEndAtDepot = table.Column<bool>(type: "bit", nullable: false),
                    IsStartFromDepot = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    ShipFromId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrackRoutes_TrackDrivers_DriverId",
                        column: x => x.DriverId,
                        principalTable: "TrackDrivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TrackRoutes_TrackDepots_ShipFromId",
                        column: x => x.ShipFromId,
                        principalTable: "TrackDepots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RouteSites",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ArrivedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Client = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<int>(type: "int", nullable: true),
                    ContactEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContactPhone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DepartedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Goods = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OrderNo = table.Column<int>(type: "int", nullable: false),
                    Pallets = table.Column<int>(type: "int", nullable: true),
                    RouteId = table.Column<int>(type: "int", nullable: false),
                    SequenceNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    TimeFrom = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimeTo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Volume = table.Column<int>(type: "int", nullable: true),
                    Weight = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RouteSites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RouteSites_TrackRoutes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "TrackRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RouteSites_RouteId",
                table: "RouteSites",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_TrackDrivers_TrackDepotId",
                table: "TrackDrivers",
                column: "TrackDepotId");

            migrationBuilder.CreateIndex(
                name: "IX_TrackRoutes_DriverId",
                table: "TrackRoutes",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_TrackRoutes_ShipFromId",
                table: "TrackRoutes",
                column: "ShipFromId");
        }
    }
}

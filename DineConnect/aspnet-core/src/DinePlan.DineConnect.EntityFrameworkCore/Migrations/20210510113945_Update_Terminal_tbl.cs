﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Terminal_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AcceptTolerance",
                table: "Terminals",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AutoDayClose",
                table: "Terminals",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AutoDayCloseSettings",
                table: "Terminals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Settings",
                table: "Terminals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TicketTypeId",
                table: "Terminals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tid",
                table: "Terminals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Terminals_TicketTypeId",
                table: "Terminals",
                column: "TicketTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Terminals_TicketTypeConfigurations_TicketTypeId",
                table: "Terminals",
                column: "TicketTypeId",
                principalTable: "TicketTypeConfigurations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Terminals_TicketTypeConfigurations_TicketTypeId",
                table: "Terminals");

            migrationBuilder.DropIndex(
                name: "IX_Terminals_TicketTypeId",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "AcceptTolerance",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "AutoDayClose",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "AutoDayCloseSettings",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "Settings",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "TicketTypeId",
                table: "Terminals");

            migrationBuilder.DropColumn(
                name: "Tid",
                table: "Terminals");
        }
    }
}

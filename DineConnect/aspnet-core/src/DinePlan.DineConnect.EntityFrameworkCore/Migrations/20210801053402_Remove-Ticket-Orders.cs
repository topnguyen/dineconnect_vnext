﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemoveTicketOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Orders_MenuItemPortions_MenuItemPortionId",
            //    table: "Orders");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Orders_Tickets_TicketId",
            //    table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_MenuItemPortionId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_TicketId",
                table: "Orders");
           
            //migrationBuilder.DropIndex(
            //    name: "IX_MenuItemPortions_OrderId",
            //    table: "MenuItemPortions");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_MenuItemPortions_Orders_OrderId",
            //    table: "MenuItemPortions");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "MenuItemPortions");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MenuItemPortionId",
                table: "Orders",
                column: "MenuItemPortionId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Orders_MenuItemPortions_MenuItemPortionId",
            //    table: "Orders",
            //    column: "MenuItemPortionId",
            //    principalTable: "MenuItemPortions",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Orders_Tickets_TicketId",
            //    table: "Orders",
            //    column: "TicketId",
            //    principalTable: "Tickets",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Orders_MenuItemPortions_MenuItemPortionId",
            //    table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_MenuItemPortionId",
                table: "Orders");

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "MenuItemPortions",
                type: "int",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_MenuItemPortionId",
            //    table: "Orders",
            //    column: "MenuItemPortionId",
            //    unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_MenuItemPortions_MenuItemPortionId",
                table: "Orders",
                column: "MenuItemPortionId",
                principalTable: "MenuItemPortions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Tickets_TicketId",
                table: "Orders",
                column: "TicketId",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

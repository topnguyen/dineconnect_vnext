﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class MenuItemTaxDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsChangeValue",
                table: "MenuItemTaxDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "TaxValue",
                table: "MenuItemTaxDetails",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsUrbanPiperRecommend",
                table: "MenuItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperFiles",
                table: "MenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperPlatforms",
                table: "MenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperSwiggyTags",
                table: "MenuItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrbanPiperZomatoTags",
                table: "MenuItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsChangeValue",
                table: "MenuItemTaxDetails");

            migrationBuilder.DropColumn(
                name: "TaxValue",
                table: "MenuItemTaxDetails");

            migrationBuilder.DropColumn(
                name: "IsUrbanPiperRecommend",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperFiles",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperPlatforms",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperSwiggyTags",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "UrbanPiperZomatoTags",
                table: "MenuItems");
        }
    }
}

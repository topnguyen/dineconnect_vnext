﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class EditWheelScreenMenuItemTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems",
                column: "SeletedMenuItemIdPrice");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelScreenMenuItems_MenuItems_SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems",
                column: "SeletedMenuItemIdPrice",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelScreenMenuItems_MenuItems_SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelScreenMenuItems_SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems");
        }
    }
}

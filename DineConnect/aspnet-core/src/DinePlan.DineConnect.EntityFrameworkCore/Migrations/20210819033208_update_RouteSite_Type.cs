﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_RouteSite_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Code",
                table: "RouteSites",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "RouteSites",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrderNo",
                table: "RouteSites",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Pallets",
                table: "RouteSites",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "RouteSites",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Volume",
                table: "RouteSites",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Weight",
                table: "RouteSites",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Parameters",
                table: "AbpAuditLogs",
                maxLength: 1024,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldMaxLength: 10000,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "OrderNo",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "Pallets",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "Volume",
                table: "RouteSites");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "RouteSites");

            migrationBuilder.AlterColumn<string>(
                name: "Parameters",
                table: "AbpAuditLogs",
                type: "nvarchar(max)",
                maxLength: 10000,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1024,
                oldNullable: true);
        }
    }
}

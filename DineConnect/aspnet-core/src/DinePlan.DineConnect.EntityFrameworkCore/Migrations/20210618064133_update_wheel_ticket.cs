﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_wheel_ticket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelOrders");

            migrationBuilder.DropColumn(
                name: "OrderDate",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "TimeSlotFrom",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "TimeSlotTo",
                table: "WheelTickets");

            migrationBuilder.AlterColumn<decimal>(
                name: "DeliveryCharge",
                table: "WheelTickets",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<string>(
                name: "AddOn",
                table: "WheelTickets",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "WheelTickets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DelilveryTime",
                table: "WheelTickets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MenuItemsDynamic",
                table: "WheelTickets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "WheelTickets",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddOn",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "DelilveryTime",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "MenuItemsDynamic",
                table: "WheelTickets");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "WheelTickets");

            migrationBuilder.AlterColumn<long>(
                name: "DeliveryCharge",
                table: "WheelTickets",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderDate",
                table: "WheelTickets",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeSlotFrom",
                table: "WheelTickets",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeSlotTo",
                table: "WheelTickets",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WheelOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddOn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    MenuItemsDynamic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    WheelTicketId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelOrders_WheelTickets_WheelTicketId",
                        column: x => x.WheelTicketId,
                        principalTable: "WheelTickets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelOrders_WheelTicketId",
                table: "WheelOrders",
                column: "WheelTicketId");
        }
    }
}

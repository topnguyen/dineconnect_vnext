﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TiffinPromotionGenerationInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinPromotion_Customers_CustomerId",
                table: "TiffinPromotion");

            migrationBuilder.DropIndex(
                name: "IX_TiffinPromotion_CustomerId",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "TiffinPromotion");

            migrationBuilder.AddColumn<string>(
                name: "GeneratePrefix",
                table: "TiffinPromotion",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GenerateSuffix",
                table: "TiffinPromotion",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GenerateTotalDigits",
                table: "TiffinPromotion",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GenerateTotalVouchers",
                table: "TiffinPromotion",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsAutoAttach",
                table: "TiffinPromotion",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneratePrefix",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "GenerateSuffix",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "GenerateTotalDigits",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "GenerateTotalVouchers",
                table: "TiffinPromotion");

            migrationBuilder.DropColumn(
                name: "IsAutoAttach",
                table: "TiffinPromotion");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "TiffinPromotion",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotion_CustomerId",
                table: "TiffinPromotion",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinPromotion_Customers_CustomerId",
                table: "TiffinPromotion",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

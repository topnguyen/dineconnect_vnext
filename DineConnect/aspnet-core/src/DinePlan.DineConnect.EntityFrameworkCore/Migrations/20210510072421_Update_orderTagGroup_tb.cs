﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_orderTagGroup_tb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "OrderTags",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Files",
                table: "OrderTagGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Files",
                table: "OrderTags");

            migrationBuilder.DropColumn(
                name: "Files",
                table: "OrderTagGroups");
        }
    }
}

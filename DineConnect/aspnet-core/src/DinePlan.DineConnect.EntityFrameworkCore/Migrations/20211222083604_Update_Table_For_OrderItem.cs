﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Table_For_OrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuItemId",
                table: "CliqueOrderDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CliqueOrderDetails_ScreenMenuItemId",
                table: "CliqueOrderDetails",
                column: "ScreenMenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_CliqueCustomerFavoriteItems_ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems",
                column: "ScreenMenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueCustomerFavoriteItems_ScreenMenuItems_ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CliqueOrderDetails_ScreenMenuItems_ScreenMenuItemId",
                table: "CliqueOrderDetails",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CliqueCustomerFavoriteItems_ScreenMenuItems_ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems");

            migrationBuilder.DropForeignKey(
                name: "FK_CliqueOrderDetails_ScreenMenuItems_ScreenMenuItemId",
                table: "CliqueOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_CliqueOrderDetails_ScreenMenuItemId",
                table: "CliqueOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_CliqueCustomerFavoriteItems_ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems");

            migrationBuilder.DropColumn(
                name: "ScreenMenuItemId",
                table: "CliqueOrderDetails");

            migrationBuilder.DropColumn(
                name: "ScreenMenuItemId",
                table: "CliqueCustomerFavoriteItems");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeComboItemPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_WheelCartItems_MainCartItemId",
                table: "WheelCartItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelCartItems_MainCartItemId",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "MainCartItemId",
                table: "WheelCartItems");

            migrationBuilder.AddColumn<string>(
                name: "ChildItems",
                table: "WheelCartItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "WheelCartItems",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "WheelCartItems",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChildItems",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "WheelCartItems");

            migrationBuilder.AddColumn<int>(
                name: "MainCartItemId",
                table: "WheelCartItems",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_MainCartItemId",
                table: "WheelCartItems",
                column: "MainCartItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_WheelCartItems_MainCartItemId",
                table: "WheelCartItems",
                column: "MainCartItemId",
                principalTable: "WheelCartItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

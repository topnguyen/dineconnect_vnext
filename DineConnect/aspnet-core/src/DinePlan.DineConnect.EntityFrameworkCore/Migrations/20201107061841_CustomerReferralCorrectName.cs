﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CustomerReferralCorrectName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Customers_RefererCustomerId",
                table: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Customers_RefererCustomerId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedAmount",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedByPaymentId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedRemarks",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedTime",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerId",
                table: "Customers");

            migrationBuilder.AddColumn<double>(
                name: "ReferrerCustomerEarnedAmount",
                table: "Customers",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerEarnedByPaymentId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferrerCustomerEarnedRemarks",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReferrerCustomerEarnedTime",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReferrerCustomerId",
                table: "Customers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ReferrerCustomerId",
                table: "Customers",
                column: "ReferrerCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Customers_ReferrerCustomerId",
                table: "Customers",
                column: "ReferrerCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Customers_ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Customers_ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedAmount",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedByPaymentId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedRemarks",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerEarnedTime",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferrerCustomerId",
                table: "Customers");

            migrationBuilder.AddColumn<double>(
                name: "RefererCustomerEarnedAmount",
                table: "Customers",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "RefererCustomerEarnedByPaymentId",
                table: "Customers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RefererCustomerEarnedRemarks",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RefererCustomerEarnedTime",
                table: "Customers",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RefererCustomerId",
                table: "Customers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_RefererCustomerId",
                table: "Customers",
                column: "RefererCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Customers_RefererCustomerId",
                table: "Customers",
                column: "RefererCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

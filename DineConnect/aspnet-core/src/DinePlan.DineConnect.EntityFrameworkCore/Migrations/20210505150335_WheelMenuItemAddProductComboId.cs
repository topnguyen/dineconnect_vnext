﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class WheelMenuItemAddProductComboId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProductComboId",
                table: "ScreenMenuItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScreenMenuItems_ProductComboId",
                table: "ScreenMenuItems",
                column: "ProductComboId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_ProductComboes_ProductComboId",
                table: "ScreenMenuItems",
                column: "ProductComboId",
                principalTable: "ProductComboes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_ProductComboes_ProductComboId",
                table: "ScreenMenuItems");

            migrationBuilder.DropIndex(
                name: "IX_ScreenMenuItems_ProductComboId",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ProductComboId",
                table: "ScreenMenuItems");
        }
    }
}

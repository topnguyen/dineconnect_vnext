﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ChangeName_Faq_Term_HomeBanner_TenantId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "TiffinTermAndCondition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "TiffinHomeBanner",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "TiffinFaq",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "TiffinTermAndCondition");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "TiffinHomeBanner");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "TiffinFaq");
        }
    }
}

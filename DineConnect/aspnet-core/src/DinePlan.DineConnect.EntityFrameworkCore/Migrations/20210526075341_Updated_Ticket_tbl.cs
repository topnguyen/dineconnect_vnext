﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
	public partial class Updated_Ticket_tbl : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_Orders_Tickets_TicketId1",
				table: "Orders");

			migrationBuilder.DropIndex(
				name: "IX_Orders_TicketId1",
				table: "Orders");


			migrationBuilder.DropColumn(
				name: "OrderId",
				table: "Tickets");

			migrationBuilder.DropColumn(
				name: "TicketId1",
				table: "Orders");

			migrationBuilder.AlterColumn<string>(
				name: "TicketNumber",
				table: "Tickets",
				maxLength: 100,
				nullable: true,
				oldClrType: typeof(string),
				oldType: "nvarchar(30)",
				oldMaxLength: 30,
				oldNullable: true);

			migrationBuilder.AlterColumn<string>(
				name: "ReferenceNumber",
				table: "Tickets",
				maxLength: 100,
				nullable: true,
				oldClrType: typeof(string),
				oldType: "nvarchar(20)",
				oldMaxLength: 20,
				oldNullable: true);

			migrationBuilder.AlterColumn<string>(
				name: "QueueNumber",
				table: "Tickets",
				maxLength: 100,
				nullable: true,
				oldClrType: typeof(string),
				oldType: "nvarchar(20)",
				oldMaxLength: 20,
				oldNullable: true);

			//migrationBuilder.CreateIndex(
			//	name: "IX_Orders_TicketId",
			//	table: "Orders",
			//	column: "TicketId");

			//migrationBuilder.AddForeignKey(
			//	name: "FK_Orders_Tickets_TicketId",
			//	table: "Orders",
			//	column: "TicketId",
			//	principalTable: "Tickets",
			//	principalColumn: "Id",
			//	onDelete: ReferentialAction.Restrict);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AlterColumn<string>(
				name: "TicketNumber",
				table: "Tickets",
				type: "nvarchar(30)",
				maxLength: 30,
				nullable: true,
				oldClrType: typeof(string),
				oldMaxLength: 100,
				oldNullable: true);

			migrationBuilder.AlterColumn<string>(
				name: "ReferenceNumber",
				table: "Tickets",
				type: "nvarchar(20)",
				maxLength: 20,
				nullable: true,
				oldClrType: typeof(string),
				oldMaxLength: 100,
				oldNullable: true);

			migrationBuilder.AlterColumn<string>(
				name: "QueueNumber",
				table: "Tickets",
				type: "nvarchar(20)",
				maxLength: 20,
				nullable: true,
				oldClrType: typeof(string),
				oldMaxLength: 100,
				oldNullable: true);

			migrationBuilder.AddColumn<int>(
				name: "OrderId",
				table: "Tickets",
				type: "int",
				nullable: true);

			migrationBuilder.AddColumn<int>(
				name: "TicketId1",
				table: "Orders",
				type: "int",
				nullable: true);

			migrationBuilder.CreateIndex(
				name: "IX_Orders_TicketId1",
				table: "Orders",
				column: "TicketId1");

			migrationBuilder.AddForeignKey(
				name: "FK_Orders_Tickets_TicketId1",
				table: "Orders",
				column: "TicketId1",
				principalTable: "Tickets",
				principalColumn: "Id",
				onDelete: ReferentialAction.Restrict);
		}
	}
}
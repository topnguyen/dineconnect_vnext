﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_newtables_order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelCartItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Tags = table.Column<string>(nullable: true),
                    Portion = table.Column<string>(nullable: true),
                    MainCartItemId = table.Column<int>(nullable: true),
                    CustomerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelCartItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelCartItems_WheelCartItems_MainCartItemId",
                        column: x => x.MainCartItemId,
                        principalTable: "WheelCartItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WheelOrderItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Tags = table.Column<string>(nullable: true),
                    Portion = table.Column<string>(nullable: true),
                    MainOrderItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelOrderItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelOrderItems_WheelOrderItems_MainOrderItemId",
                        column: x => x.MainOrderItemId,
                        principalTable: "WheelOrderItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_MainCartItemId",
                table: "WheelCartItems",
                column: "MainCartItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelOrderItems_MainOrderItemId",
                table: "WheelOrderItems",
                column: "MainOrderItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelCartItems");

            migrationBuilder.DropTable(
                name: "WheelOrderItems");
        }
    }
}

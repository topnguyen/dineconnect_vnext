﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Added_Location_Group_cols_PromotionQuotas_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "PromotionQuotas",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "PromotionQuotas",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "PromotionQuotas",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "PromotionQuotas",
                maxLength: 1000,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "PromotionQuotas");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "PromotionQuotas");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "PromotionQuotas");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "PromotionQuotas");
        }
    }
}

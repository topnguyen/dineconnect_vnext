﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddDepartmentsToPaymentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Departments",
                table: "PaymentTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProcessorName",
                table: "PaymentTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Departments",
                table: "PaymentTypes");

            migrationBuilder.DropColumn(
                name: "ProcessorName",
                table: "PaymentTypes");
        }
    }
}

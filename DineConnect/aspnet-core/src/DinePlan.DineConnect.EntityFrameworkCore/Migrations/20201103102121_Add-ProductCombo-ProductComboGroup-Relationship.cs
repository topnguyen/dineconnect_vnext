﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddProductComboProductComboGroupRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductComboGroupDetails",
                columns: table => new
                {
                    ProductComboId = table.Column<int>(nullable: false),
                    ProductComboGroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductComboGroupDetails", x => new { x.ProductComboId, x.ProductComboGroupId });
                    table.ForeignKey(
                        name: "FK_ProductComboGroupDetails_ProductComboGroups_ProductComboGroupId",
                        column: x => x.ProductComboGroupId,
                        principalTable: "ProductComboGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductComboGroupDetails_ProductComboes_ProductComboId",
                        column: x => x.ProductComboId,
                        principalTable: "ProductComboes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductComboGroupDetails_ProductComboGroupId",
                table: "ProductComboGroupDetails",
                column: "ProductComboGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductComboGroupDetails");
        }
    }
}

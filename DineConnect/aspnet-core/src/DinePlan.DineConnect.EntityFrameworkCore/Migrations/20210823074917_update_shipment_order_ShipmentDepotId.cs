﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_shipment_order_ShipmentDepotId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "DeliveryTimeTo",
                table: "ShipmentOrders",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<string>(
                name: "DeliveryTimeFrom",
                table: "ShipmentOrders",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<int>(
                name: "ShipmentDepotId",
                table: "ShipmentOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentOrders_ShipmentDepotId",
                table: "ShipmentOrders",
                column: "ShipmentDepotId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentOrders_ShipmentDepots_ShipmentDepotId",
                table: "ShipmentOrders",
                column: "ShipmentDepotId",
                principalTable: "ShipmentDepots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentOrders_ShipmentDepots_ShipmentDepotId",
                table: "ShipmentOrders");

            migrationBuilder.DropIndex(
                name: "IX_ShipmentOrders_ShipmentDepotId",
                table: "ShipmentOrders");

            migrationBuilder.DropColumn(
                name: "ShipmentDepotId",
                table: "ShipmentOrders");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryTimeTo",
                table: "ShipmentOrders",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryTimeFrom",
                table: "ShipmentOrders",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}

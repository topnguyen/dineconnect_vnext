﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class EditTableReservations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_ReserveTables_ReserveTableId",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_ReserveTableId",
                table: "Reservations");

            migrationBuilder.DropColumn(
                name: "ReserveTableId",
                table: "Reservations");

            migrationBuilder.AddColumn<string>(
                name: "ReserveTableIds",
                table: "Reservations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReserveTableIds",
                table: "Reservations");

            migrationBuilder.AddColumn<int>(
                name: "ReserveTableId",
                table: "Reservations",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_ReserveTableId",
                table: "Reservations",
                column: "ReserveTableId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_ReserveTables_ReserveTableId",
                table: "Reservations",
                column: "ReserveTableId",
                principalTable: "ReserveTables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

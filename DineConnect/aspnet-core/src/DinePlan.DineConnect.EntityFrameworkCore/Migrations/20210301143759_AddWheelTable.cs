﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddWheelTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCustomerProductOfferOrders_WheelCustomerProductOffers_WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders");

            migrationBuilder.DropIndex(
                name: "IX_WheelCustomerProductOfferOrders_WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders");

            migrationBuilder.AddColumn<bool>(
                name: "IsEnableTable",
                table: "WheelDepartments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "WheelTables",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    WheelTableStatus = table.Column<int>(nullable: false),
                    WheelLocationId = table.Column<int>(nullable: true),
                    WheelDepartmentId = table.Column<int>(nullable: true),
                    OtpCode = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelTables", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelTables_WheelDepartments_WheelDepartmentId",
                        column: x => x.WheelDepartmentId,
                        principalTable: "WheelDepartments",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelTables_WheelLocations_WheelDepartmentId",
                        column: x => x.WheelDepartmentId,
                        principalTable: "WheelLocations",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelTables_TenantId",
                table: "WheelTables",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelTables_WheelDepartmentId",
                table: "WheelTables",
                column: "WheelDepartmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelTables");

            migrationBuilder.DropColumn(
                name: "IsEnableTable",
                table: "WheelDepartments");

            migrationBuilder.AddColumn<int>(
                name: "WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders",
                column: "WheelCustomerProductOfferId1");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCustomerProductOfferOrders_WheelCustomerProductOffers_WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders",
                column: "WheelCustomerProductOfferId1",
                principalTable: "WheelCustomerProductOffers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

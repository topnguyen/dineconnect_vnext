﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class MenuItemOptionalForScreenMenuItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_MenuItems_MenuItemId",
                table: "ScreenMenuItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenuCategories_ScreenCategoryId",
                table: "ScreenMenuItems");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_WheelScreenMenus_MenuId",
                table: "WheelDepartments");

            migrationBuilder.DropTable(
                name: "WheelScreenMenuItems");

            migrationBuilder.DropTable(
                name: "WheelScreenMenus");

            migrationBuilder.AlterColumn<int>(
                name: "ScreenCategoryId",
                table: "ScreenMenuItems",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemId",
                table: "ScreenMenuItems",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuId",
                table: "ScreenMenuItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScreenMenuItems_ScreenMenuId",
                table: "ScreenMenuItems",
                column: "ScreenMenuId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_MenuItems_MenuItemId",
                table: "ScreenMenuItems",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenuCategories_ScreenCategoryId",
                table: "ScreenMenuItems",
                column: "ScreenCategoryId",
                principalTable: "ScreenMenuCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenus_ScreenMenuId",
                table: "ScreenMenuItems",
                column: "ScreenMenuId",
                principalTable: "ScreenMenus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_ScreenMenus_MenuId",
                table: "WheelDepartments",
                column: "MenuId",
                principalTable: "ScreenMenus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_MenuItems_MenuItemId",
                table: "ScreenMenuItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenuCategories_ScreenCategoryId",
                table: "ScreenMenuItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenus_ScreenMenuId",
                table: "ScreenMenuItems");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_ScreenMenus_MenuId",
                table: "WheelDepartments");

            migrationBuilder.DropIndex(
                name: "IX_ScreenMenuItems_ScreenMenuId",
                table: "ScreenMenuItems");

            migrationBuilder.DropColumn(
                name: "ScreenMenuId",
                table: "ScreenMenuItems");

            migrationBuilder.AlterColumn<int>(
                name: "ScreenCategoryId",
                table: "ScreenMenuItems",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemId",
                table: "ScreenMenuItems",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "WheelScreenMenus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WheelScreenMenuItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DisplayAs = table.Column<int>(type: "int", nullable: false),
                    DisplayNutritionFacts = table.Column<bool>(type: "bit", nullable: false),
                    HideGridTitles = table.Column<bool>(type: "bit", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IngredientWarnings = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    MarkAsNew = table.Column<bool>(type: "bit", nullable: false),
                    MarkAsSignature = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumberOfColumns = table.Column<int>(type: "int", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    PreperationTime = table.Column<int>(type: "int", nullable: false),
                    Prices = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    RecommendItems = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RecommendedItems = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScreenMenuId = table.Column<int>(type: "int", nullable: false),
                    SeletedMenuItemIdPrice = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Video = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelScreenMenuItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItems_WheelScreenMenuItems_ParentId",
                        column: x => x.ParentId,
                        principalTable: "WheelScreenMenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItems_WheelScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "WheelScreenMenus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WheelScreenMenuItems_MenuItems_SeletedMenuItemIdPrice",
                        column: x => x.SeletedMenuItemIdPrice,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_ParentId",
                table: "WheelScreenMenuItems",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_ScreenMenuId",
                table: "WheelScreenMenuItems",
                column: "ScreenMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelScreenMenuItems_SeletedMenuItemIdPrice",
                table: "WheelScreenMenuItems",
                column: "SeletedMenuItemIdPrice");

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_MenuItems_MenuItemId",
                table: "ScreenMenuItems",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScreenMenuItems_ScreenMenuCategories_ScreenCategoryId",
                table: "ScreenMenuItems",
                column: "ScreenCategoryId",
                principalTable: "ScreenMenuCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_WheelScreenMenus_MenuId",
                table: "WheelDepartments",
                column: "MenuId",
                principalTable: "WheelScreenMenus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Remove_WheelMenuCombos_tbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelMenuCombos");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelMenuCombos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BasePrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Dynamic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    ScreenCategoryId = table.Column<int>(type: "int", nullable: true),
                    ScreenMenuId = table.Column<int>(type: "int", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelMenuCombos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelMenuCombos_ScreenMenuCategories_ScreenCategoryId",
                        column: x => x.ScreenCategoryId,
                        principalTable: "ScreenMenuCategories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelMenuCombos_ScreenMenus_ScreenMenuId",
                        column: x => x.ScreenMenuId,
                        principalTable: "ScreenMenus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_ScreenCategoryId",
                table: "WheelMenuCombos",
                column: "ScreenCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_ScreenMenuId",
                table: "WheelMenuCombos",
                column: "ScreenMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelMenuCombos_TenantId",
                table: "WheelMenuCombos",
                column: "TenantId");
        }
    }
}

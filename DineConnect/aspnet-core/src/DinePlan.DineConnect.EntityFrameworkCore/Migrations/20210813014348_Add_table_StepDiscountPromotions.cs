﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Add_table_StepDiscountPromotions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "Parameters",
            //    table: "AbpAuditLogs",
            //    maxLength: 10000,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldType: "nvarchar(1024)",
            //    oldMaxLength: 1024,
            //    oldNullable: true);

            migrationBuilder.CreateTable(
                name: "StepDiscountPromotions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PromotionId = table.Column<int>(nullable: false),
                    StepDiscountType = table.Column<int>(nullable: false),
                    ApplyForEachQuantity = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StepDiscountPromotions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StepDiscountPromotions_Promotions_PromotionId",
                        column: x => x.PromotionId,
                        principalTable: "Promotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StepDiscountPromotions_PromotionId",
                table: "StepDiscountPromotions",
                column: "PromotionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StepDiscountPromotions");

            //migrationBuilder.AlterColumn<string>(
            //    name: "Parameters",
            //    table: "AbpAuditLogs",
            //    type: "nvarchar(1024)",
            //    maxLength: 1024,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 10000,
            //    oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddColumnsIntoTenantDatabaseTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConnectionTimeout",
                table: "TenantDatabases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Encrypt",
                table: "TenantDatabases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "MultipleActiveResultSets",
                table: "TenantDatabases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "PersistSecurityInfo",
                table: "TenantDatabases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "TrustServerCertificate",
                table: "TenantDatabases",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectionTimeout",
                table: "TenantDatabases");

            migrationBuilder.DropColumn(
                name: "Encrypt",
                table: "TenantDatabases");

            migrationBuilder.DropColumn(
                name: "MultipleActiveResultSets",
                table: "TenantDatabases");

            migrationBuilder.DropColumn(
                name: "PersistSecurityInfo",
                table: "TenantDatabases");

            migrationBuilder.DropColumn(
                name: "TrustServerCertificate",
                table: "TenantDatabases");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class LocationTagRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LocationTags_Locations_LocationId",
                table: "LocationTags");

            migrationBuilder.DropIndex(
                name: "IX_LocationTags_LocationId",
                table: "LocationTags");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "LocationTags");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "LocationTags",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LocationTags_LocationId",
                table: "LocationTags",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_LocationTags_Locations_LocationId",
                table: "LocationTags",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

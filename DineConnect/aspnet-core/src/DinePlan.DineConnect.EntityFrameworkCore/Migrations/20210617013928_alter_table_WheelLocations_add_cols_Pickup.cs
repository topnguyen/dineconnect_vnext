﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class alter_table_WheelLocations_add_cols_Pickup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PickupAddress",
                table: "WheelLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupAddressLat",
                table: "WheelLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupAddressLong",
                table: "WheelLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupContactName",
                table: "WheelLocations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickupContactPhoneNumber",
                table: "WheelLocations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PickupAddress",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "PickupAddressLat",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "PickupAddressLong",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "PickupContactName",
                table: "WheelLocations");

            migrationBuilder.DropColumn(
                name: "PickupContactPhoneNumber",
                table: "WheelLocations");
        }
    }
}

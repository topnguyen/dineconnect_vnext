﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class add_table_WheelDepartmentTimeSlot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelDepartmentTimeSlots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    TimeFrom = table.Column<string>(nullable: true),
                    TimeTo = table.Column<string>(nullable: true),
                    NumberOfOrder = table.Column<int>(nullable: true),
                    WheelDepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelDepartmentTimeSlots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelDepartmentTimeSlots_WheelDepartments_WheelDepartmentId",
                        column: x => x.WheelDepartmentId,
                        principalTable: "WheelDepartments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelDepartmentTimeSlots_WheelDepartmentId",
                table: "WheelDepartmentTimeSlots",
                column: "WheelDepartmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelDepartmentTimeSlots");
        }
    }
}

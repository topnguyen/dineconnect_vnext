﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class PromotionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PromotionCategoryId",
                table: "Promotions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PromotionCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    Code = table.Column<string>(maxLength: 10, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Promotions_PromotionCategoryId",
                table: "Promotions",
                column: "PromotionCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotions_PromotionCategories_PromotionCategoryId",
                table: "Promotions",
                column: "PromotionCategoryId",
                principalTable: "PromotionCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotions_PromotionCategories_PromotionCategoryId",
                table: "Promotions");

            migrationBuilder.DropTable(
                name: "PromotionCategories");

            migrationBuilder.DropIndex(
                name: "IX_Promotions_PromotionCategoryId",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "PromotionCategoryId",
                table: "Promotions");
        }
    }
}

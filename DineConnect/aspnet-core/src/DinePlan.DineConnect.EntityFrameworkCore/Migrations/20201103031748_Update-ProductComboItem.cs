﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class UpdateProductComboItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ButtonColor",
                table: "ProductComboItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupTag",
                table: "ProductComboItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MenuItemPortionId",
                table: "ProductComboItems",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ButtonColor",
                table: "ProductComboItems");

            migrationBuilder.DropColumn(
                name: "GroupTag",
                table: "ProductComboItems");

            migrationBuilder.DropColumn(
                name: "MenuItemPortionId",
                table: "ProductComboItems");
        }
    }
}

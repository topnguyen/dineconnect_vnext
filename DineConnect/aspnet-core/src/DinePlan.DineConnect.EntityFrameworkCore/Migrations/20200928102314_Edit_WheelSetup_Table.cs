﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Edit_WheelSetup_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CheckOutPage",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "HomePage",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "Theme",
                table: "WheelSetups");

            migrationBuilder.AddColumn<int>(
                name: "CheckOutPageId",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HomePageId",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogoCompanyUrl",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ThemeName",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelSetups_CheckOutPageId",
                table: "WheelSetups",
                column: "CheckOutPageId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelSetups_HomePageId",
                table: "WheelSetups",
                column: "HomePageId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelSetups_WheelDynamicPages_CheckOutPageId",
                table: "WheelSetups",
                column: "CheckOutPageId",
                principalTable: "WheelDynamicPages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelSetups_WheelDynamicPages_HomePageId",
                table: "WheelSetups",
                column: "HomePageId",
                principalTable: "WheelDynamicPages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelSetups_WheelDynamicPages_CheckOutPageId",
                table: "WheelSetups");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelSetups_WheelDynamicPages_HomePageId",
                table: "WheelSetups");

            migrationBuilder.DropIndex(
                name: "IX_WheelSetups_CheckOutPageId",
                table: "WheelSetups");

            migrationBuilder.DropIndex(
                name: "IX_WheelSetups_HomePageId",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "CheckOutPageId",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "HomePageId",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "LogoCompanyUrl",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "ThemeName",
                table: "WheelSetups");

            migrationBuilder.AddColumn<string>(
                name: "CheckOutPage",
                table: "WheelSetups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HomePage",
                table: "WheelSetups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Theme",
                table: "WheelSetups",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TiffinDeliveryTimeSlot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TiffinDeliveryTimeSlots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "date", nullable: false),
                    MealTimeId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: true),
                    TimeSlots = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinDeliveryTimeSlots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiffinDeliveryTimeSlots_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TiffinDeliveryTimeSlots_TiffinMealTimes_MealTimeId",
                        column: x => x.MealTimeId,
                        principalTable: "TiffinMealTimes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TiffinDeliveryTimeSlots_LocationId",
                table: "TiffinDeliveryTimeSlots",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_TiffinDeliveryTimeSlots_MealTimeId",
                table: "TiffinDeliveryTimeSlots",
                column: "MealTimeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TiffinDeliveryTimeSlots");
        }
    }
}

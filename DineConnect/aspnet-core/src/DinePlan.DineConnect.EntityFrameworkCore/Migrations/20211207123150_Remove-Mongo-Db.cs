﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class RemoveMongoDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MongoDBConnectionString",
                table: "AbpTenants");

            migrationBuilder.AddColumn<DateTime>(
                name: "CutOffDate",
                table: "CliqueMenus",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CutOffDate",
                table: "CliqueMenus");

            migrationBuilder.AddColumn<string>(
                name: "MongoDBConnectionString",
                table: "AbpTenants",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

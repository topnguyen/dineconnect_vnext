﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_wheelCartitem_talbe222 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "WheelOrderItems");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "WheelCartItems");

            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuItemId",
                table: "WheelOrderItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ScreenMenuItemId",
                table: "WheelCartItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_WheelOrderItems_ScreenMenuItemId",
                table: "WheelOrderItems",
                column: "ScreenMenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_ScreenMenuItemId",
                table: "WheelCartItems",
                column: "ScreenMenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelOrderItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelOrderItems",
                column: "ScreenMenuItemId",
                principalTable: "ScreenMenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelCartItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelOrderItems_ScreenMenuItems_ScreenMenuItemId",
                table: "WheelOrderItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelOrderItems_ScreenMenuItemId",
                table: "WheelOrderItems");

            migrationBuilder.DropIndex(
                name: "IX_WheelCartItems_ScreenMenuItemId",
                table: "WheelCartItems");

            migrationBuilder.DropColumn(
                name: "ScreenMenuItemId",
                table: "WheelOrderItems");

            migrationBuilder.DropColumn(
                name: "ScreenMenuItemId",
                table: "WheelCartItems");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "WheelOrderItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "WheelCartItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_WheelCartItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelCartItems_MenuItems_MenuItemId",
                table: "WheelCartItems",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddWheelOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelPaymentRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    PaidAmount = table.Column<decimal>(nullable: false),
                    PaymentOption = table.Column<string>(nullable: true),
                    Request = table.Column<string>(nullable: true),
                    PaymentReference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelPaymentRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WheelPayments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    InvoiceNo = table.Column<string>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    PaidAmount = table.Column<decimal>(nullable: true),
                    CustomerId = table.Column<int>(nullable: false),
                    CustomerName = table.Column<string>(nullable: true),
                    CustomerAddress = table.Column<string>(nullable: true),
                    CustomerTaxNo = table.Column<string>(nullable: true),
                    PaymentOption = table.Column<string>(nullable: true),
                    MenuItemsDynamic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelPayments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WheelCustomerProductOffers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    ActualCredit = table.Column<int>(nullable: true),
                    PaidAmount = table.Column<decimal>(nullable: true),
                    BalanceCredit = table.Column<int>(nullable: true),
                    WheelPaymentId = table.Column<int>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelCustomerProductOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOffers_WheelPayments_WheelPaymentId",
                        column: x => x.WheelPaymentId,
                        principalTable: "WheelPayments",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WheelCustomerProductOfferOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    WheelCustomerProductOfferId = table.Column<int>(nullable: true),
                    OrderDate = table.Column<DateTime>(type: "date", nullable: false),
                    Amount = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    AddOn = table.Column<string>(nullable: true),
                    CustomerAddressId = table.Column<int>(nullable: true),
                    PaymentId = table.Column<int>(nullable: true),
                    MenuItemsDynamic = table.Column<string>(nullable: true),
                    WheelOrderType = table.Column<int>(nullable: false),
                    DeliveryCharge = table.Column<long>(nullable: false),
                    TimeSlotFormTo = table.Column<string>(nullable: true),
                    SelfPickupLocationId = table.Column<int>(nullable: true),
                    WheelCustomerProductOfferId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelCustomerProductOfferOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_CustomerAddressDetails_CustomerAddressId",
                        column: x => x.CustomerAddressId,
                        principalTable: "CustomerAddressDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_WheelPayments_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "WheelPayments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_Locations_SelfPickupLocationId",
                        column: x => x.SelfPickupLocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_WheelCustomerProductOffers_WheelCustomerProductOfferId",
                        column: x => x.WheelCustomerProductOfferId,
                        principalTable: "WheelCustomerProductOffers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WheelCustomerProductOfferOrders_WheelCustomerProductOffers_WheelCustomerProductOfferId1",
                        column: x => x.WheelCustomerProductOfferId1,
                        principalTable: "WheelCustomerProductOffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_CustomerAddressId",
                table: "WheelCustomerProductOfferOrders",
                column: "CustomerAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_PaymentId",
                table: "WheelCustomerProductOfferOrders",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_SelfPickupLocationId",
                table: "WheelCustomerProductOfferOrders",
                column: "SelfPickupLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_WheelCustomerProductOfferId",
                table: "WheelCustomerProductOfferOrders",
                column: "WheelCustomerProductOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOfferOrders_WheelCustomerProductOfferId1",
                table: "WheelCustomerProductOfferOrders",
                column: "WheelCustomerProductOfferId1");

            migrationBuilder.CreateIndex(
                name: "IX_WheelCustomerProductOffers_WheelPaymentId",
                table: "WheelCustomerProductOffers",
                column: "WheelPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelPaymentRequests_TenantId",
                table: "WheelPaymentRequests",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelPayments_TenantId",
                table: "WheelPayments",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelCustomerProductOfferOrders");

            migrationBuilder.DropTable(
                name: "WheelPaymentRequests");

            migrationBuilder.DropTable(
                name: "WheelCustomerProductOffers");

            migrationBuilder.DropTable(
                name: "WheelPayments");
        }
    }
}

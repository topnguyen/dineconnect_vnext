﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_Promotion_tbls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Oid",
                table: "ConnectCardTypes");

            migrationBuilder.AddColumn<int>(
                name: "ConnectCardTypeId",
                table: "Promotions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MenuItemPortionId",
                table: "FixedPromotions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProductGroupId",
                table: "FixedPromotions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ValidTill",
                table: "ConnectCardTypes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ConnectCards",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Promotions_ConnectCardTypeId",
                table: "Promotions",
                column: "ConnectCardTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Promotions_ConnectCardTypes_ConnectCardTypeId",
                table: "Promotions",
                column: "ConnectCardTypeId",
                principalTable: "ConnectCardTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promotions_ConnectCardTypes_ConnectCardTypeId",
                table: "Promotions");

            migrationBuilder.DropIndex(
                name: "IX_Promotions_ConnectCardTypeId",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "ConnectCardTypeId",
                table: "Promotions");

            migrationBuilder.DropColumn(
                name: "MenuItemPortionId",
                table: "FixedPromotions");

            migrationBuilder.DropColumn(
                name: "ProductGroupId",
                table: "FixedPromotions");

            migrationBuilder.DropColumn(
                name: "ValidTill",
                table: "ConnectCardTypes");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ConnectCards");

            migrationBuilder.AddColumn<int>(
                name: "Oid",
                table: "ConnectCardTypes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

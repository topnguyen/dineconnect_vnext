﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ClusterLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Group",
                table: "DelAggLocations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LocationTag",
                table: "DelAggLocations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Locations",
                table: "DelAggLocations",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NonLocations",
                table: "DelAggLocations",
                maxLength: 1000,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "DelAggLocations");

            migrationBuilder.DropColumn(
                name: "LocationTag",
                table: "DelAggLocations");

            migrationBuilder.DropColumn(
                name: "Locations",
                table: "DelAggLocations");

            migrationBuilder.DropColumn(
                name: "NonLocations",
                table: "DelAggLocations");
        }
    }
}

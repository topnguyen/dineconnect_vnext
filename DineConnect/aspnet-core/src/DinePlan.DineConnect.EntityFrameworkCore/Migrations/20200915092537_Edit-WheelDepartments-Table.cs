﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class EditWheelDepartmentsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_Departments_DepartmentId",
                table: "WheelDepartments");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_PriceTags_PriceTagId",
                table: "WheelDepartments");

            migrationBuilder.DropIndex(
                name: "IX_WheelDepartments_DepartmentId",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "Days",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "EndHour",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "EndMinute",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "StartHour",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "StartMinute",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "WheelDeliveryDurationIds",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "WheelServiceFeeIds",
                table: "WheelDepartments");

            migrationBuilder.AlterColumn<int>(
                name: "PriceTagId",
                table: "WheelDepartments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "DinePlanTaxIds",
                table: "WheelDepartments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enable",
                table: "WheelDepartments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "MenuId",
                table: "WheelDepartments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OpeningHourId",
                table: "WheelDepartments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelDepartments_MenuId",
                table: "WheelDepartments",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_WheelDepartments_OpeningHourId",
                table: "WheelDepartments",
                column: "OpeningHourId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_WheelScreenMenus_MenuId",
                table: "WheelDepartments",
                column: "MenuId",
                principalTable: "WheelScreenMenus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_WheelOpeningHours_OpeningHourId",
                table: "WheelDepartments",
                column: "OpeningHourId",
                principalTable: "WheelOpeningHours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_PriceTags_PriceTagId",
                table: "WheelDepartments",
                column: "PriceTagId",
                principalTable: "PriceTags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_WheelScreenMenus_MenuId",
                table: "WheelDepartments");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_WheelOpeningHours_OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.DropForeignKey(
                name: "FK_WheelDepartments_PriceTags_PriceTagId",
                table: "WheelDepartments");

            migrationBuilder.DropIndex(
                name: "IX_WheelDepartments_MenuId",
                table: "WheelDepartments");

            migrationBuilder.DropIndex(
                name: "IX_WheelDepartments_OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "DinePlanTaxIds",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "Enable",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "MenuId",
                table: "WheelDepartments");

            migrationBuilder.DropColumn(
                name: "OpeningHourId",
                table: "WheelDepartments");

            migrationBuilder.AlterColumn<int>(
                name: "PriceTagId",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Days",
                table: "WheelDepartments",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EndHour",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EndMinute",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StartHour",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StartMinute",
                table: "WheelDepartments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "WheelDeliveryDurationIds",
                table: "WheelDepartments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WheelServiceFeeIds",
                table: "WheelDepartments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WheelDepartments_DepartmentId",
                table: "WheelDepartments",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_Departments_DepartmentId",
                table: "WheelDepartments",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WheelDepartments_PriceTags_PriceTagId",
                table: "WheelDepartments",
                column: "PriceTagId",
                principalTable: "PriceTags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

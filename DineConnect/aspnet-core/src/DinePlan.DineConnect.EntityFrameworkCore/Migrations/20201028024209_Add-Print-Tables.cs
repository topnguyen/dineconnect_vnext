﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddPrintTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrintConfigurations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    PrintConfigurationType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintConfigurations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Printers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    PrintConfigurationId = table.Column<int>(nullable: false),
                    PrinterType = table.Column<int>(nullable: false),
                    CodePage = table.Column<int>(nullable: false),
                    CharsPerLine = table.Column<int>(nullable: false),
                    NumberOfLines = table.Column<int>(nullable: false),
                    FallBackPrinter = table.Column<int>(nullable: false),
                    CharReplacement = table.Column<string>(nullable: true),
                    CustomPrinterName = table.Column<string>(nullable: true),
                    CustomPrinterData = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Printers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrintJobs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    PrintConfigurationId = table.Column<int>(nullable: false),
                    WhatToPrint = table.Column<int>(nullable: false),
                    Negative = table.Column<bool>(nullable: false),
                    ExcludeTax = table.Column<bool>(nullable: false),
                    JobGroup = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintJobs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrintTemplateConditions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    PrintConfigurationId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    ConditionSchedules = table.Column<string>(nullable: true),
                    Filter = table.Column<string>(nullable: true),
                    Contents = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintTemplateConditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrintTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Locations = table.Column<string>(maxLength: 1000, nullable: true),
                    NonLocations = table.Column<string>(maxLength: 1000, nullable: true),
                    Group = table.Column<bool>(nullable: false),
                    LocationTag = table.Column<bool>(nullable: false),
                    PrintConfigurationId = table.Column<int>(nullable: false),
                    Contents = table.Column<string>(nullable: true),
                    MergeLines = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrinterMaps",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    MenuItemGroupCode = table.Column<string>(nullable: true),
                    MenuItemId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    CategoryName = table.Column<string>(nullable: true),
                    PrinterId = table.Column<int>(nullable: false),
                    PrintTemplateId = table.Column<int>(nullable: false),
                    PrintJobId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrinterMaps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrinterMaps_PrintJobs_PrintJobId",
                        column: x => x.PrintJobId,
                        principalTable: "PrintJobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrinterMaps_PrintJobId",
                table: "PrinterMaps",
                column: "PrintJobId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrintConfigurations");

            migrationBuilder.DropTable(
                name: "PrinterMaps");

            migrationBuilder.DropTable(
                name: "Printers");

            migrationBuilder.DropTable(
                name: "PrintTemplateConditions");

            migrationBuilder.DropTable(
                name: "PrintTemplates");

            migrationBuilder.DropTable(
                name: "PrintJobs");
        }
    }
}

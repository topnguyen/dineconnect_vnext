﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class TiffinPromotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TiffinPromotion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    VoucherValue = table.Column<double>(nullable: false),
                    Validity = table.Column<DateTime>(nullable: false),
                    MinimumTicketNumber = table.Column<int>(nullable: false),
                    AllowMultipleRedemption = table.Column<bool>(nullable: false),
                    MultiRedemptionCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinPromotion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiffinPromotionCode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    PromotionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinPromotionCode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiffinPromotionCode_TiffinPromotion_PromotionId",
                        column: x => x.PromotionId,
                        principalTable: "TiffinPromotion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TiffinPromotionCodeClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Time = table.Column<DateTimeOffset>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    PromotionCodeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiffinPromotionCodeClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TiffinPromotionCodeClaim_TiffinPromotionCode_PromotionCodeId",
                        column: x => x.PromotionCodeId,
                        principalTable: "TiffinPromotionCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCode_PromotionId",
                table: "TiffinPromotionCode",
                column: "PromotionId");

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCodeClaim_PromotionCodeId",
                table: "TiffinPromotionCodeClaim",
                column: "PromotionCodeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TiffinPromotionCodeClaim");

            migrationBuilder.DropTable(
                name: "TiffinPromotionCode");

            migrationBuilder.DropTable(
                name: "TiffinPromotion");
        }
    }
}

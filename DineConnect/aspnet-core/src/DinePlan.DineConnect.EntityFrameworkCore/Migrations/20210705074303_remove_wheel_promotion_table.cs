﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class remove_wheel_promotion_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WheelPaymentId",
                table: "TiffinPromotionCode",
                nullable: true);


            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCode_WheelPaymentId",
                table: "TiffinPromotionCode",
                column: "WheelPaymentId");

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinPromotionCode_WheelPayments_WheelPaymentId",
                table: "TiffinPromotionCode",
                column: "WheelPaymentId",
                principalTable: "WheelPayments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinPromotionCode_WheelPayments_WheelPaymentId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropIndex(
                name: "IX_TiffinPromotionCode_WheelPaymentId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropColumn(
                name: "WheelPaymentId",
                table: "TiffinPromotionCode");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_timeslot_order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateFrom",
                table: "WheelOrderTrackings");

            migrationBuilder.DropColumn(
                name: "DateTo",
                table: "WheelOrderTrackings");

            migrationBuilder.AddColumn<string>(
                name: "OrderDate",
                table: "WheelOrderTrackings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderDate",
                table: "WheelOrderTrackings");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateFrom",
                table: "WheelOrderTrackings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateTo",
                table: "WheelOrderTrackings",
                type: "datetime2",
                nullable: true);
        }
    }
}

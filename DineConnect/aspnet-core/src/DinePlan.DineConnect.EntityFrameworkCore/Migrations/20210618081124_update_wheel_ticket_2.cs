﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class update_wheel_ticket_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DelilveryTime",
                table: "WheelTickets");

            migrationBuilder.AddColumn<string>(
                name: "DeliveryTime",
                table: "WheelTickets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryTime",
                table: "WheelTickets");

            migrationBuilder.AddColumn<string>(
                name: "DelilveryTime",
                table: "WheelTickets",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class WheelShippingProvider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ShippingProvider",
                table: "WheelCustomerProductOfferOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ShippingProviderResponseJson",
                table: "WheelCustomerProductOfferOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShippingProvider",
                table: "WheelCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "ShippingProviderResponseJson",
                table: "WheelCustomerProductOfferOrders");
        }
    }
}

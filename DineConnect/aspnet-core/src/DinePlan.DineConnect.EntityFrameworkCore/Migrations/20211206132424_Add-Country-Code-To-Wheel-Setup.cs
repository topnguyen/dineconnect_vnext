﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddCountryCodeToWheelSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddOns",
                table: "WheelSetups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                table: "WheelSetups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddOns",
                table: "WheelSetups");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                table: "WheelSetups");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Add_table_StepDiscountPromotionExecutions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StepDiscountPromotionExecutions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    StepDiscountPromotionId = table.Column<int>(nullable: false),
                    PromotionStepTypeValue = table.Column<decimal>(nullable: false),
                    PromotionValueType = table.Column<int>(nullable: false),
                    PromotionValueTypeName = table.Column<string>(nullable: true),
                    PromotionValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StepDiscountPromotionExecutions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StepDiscountPromotionExecutions_StepDiscountPromotions_StepDiscountPromotionId",
                        column: x => x.StepDiscountPromotionId,
                        principalTable: "StepDiscountPromotions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StepDiscountPromotionExecutions_StepDiscountPromotionId",
                table: "StepDiscountPromotionExecutions",
                column: "StepDiscountPromotionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StepDiscountPromotionExecutions");
        }
    }
}

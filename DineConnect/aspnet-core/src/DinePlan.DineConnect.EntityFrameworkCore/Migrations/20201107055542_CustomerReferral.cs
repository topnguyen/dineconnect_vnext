﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class CustomerReferral : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "ClaimedTime",
                table: "TiffinPromotionCode",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "TiffinPromotionCode",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "TiffinPromotionCode",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "RefererCustomerEarnedAmount",
                table: "Customers",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "RefererCustomerEarnedByPaymentId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RefererCustomerEarnedRemarks",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RefererCustomerEarnedTime",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RefererCustomerId",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferralCode",
                table: "Customers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCode_CustomerId",
                table: "TiffinPromotionCode",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_TiffinPromotionCode_PaymentId",
                table: "TiffinPromotionCode",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_RefererCustomerId",
                table: "Customers",
                column: "RefererCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Customers_RefererCustomerId",
                table: "Customers",
                column: "RefererCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinPromotionCode_Customers_CustomerId",
                table: "TiffinPromotionCode",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinPromotionCode_TiffinPayments_PaymentId",
                table: "TiffinPromotionCode",
                column: "PaymentId",
                principalTable: "TiffinPayments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Customers_RefererCustomerId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_TiffinPromotionCode_Customers_CustomerId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropForeignKey(
                name: "FK_TiffinPromotionCode_TiffinPayments_PaymentId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropIndex(
                name: "IX_TiffinPromotionCode_CustomerId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropIndex(
                name: "IX_TiffinPromotionCode_PaymentId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropIndex(
                name: "IX_Customers_RefererCustomerId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "TiffinPromotionCode");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedAmount",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedByPaymentId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedRemarks",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerEarnedTime",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "RefererCustomerId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ReferralCode",
                table: "Customers");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ClaimedTime",
                table: "TiffinPromotionCode",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}

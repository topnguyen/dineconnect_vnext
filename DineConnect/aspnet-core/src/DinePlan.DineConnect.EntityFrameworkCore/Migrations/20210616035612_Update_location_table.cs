﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Update_location_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BackGrandEndTime",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BackGroundStartTime",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DefaultRequestLocationRefId",
                table: "Locations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "District",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DoesDayCloseRunInBackGround",
                table: "Locations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "HouseTransactionDate",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDayCloseRecursiveUptoDateAllowed",
                table: "Locations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsProductionAllowed",
                table: "Locations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsProductionUnitAllowed",
                table: "Locations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPurchaseAllowed",
                table: "Locations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LocationTaxCode",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "OrganizationUnitId",
                table: "Locations",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "ReceiptFooter",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReceiptHeader",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferenceCode",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RequestedLocations",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SharingPercentage",
                table: "Locations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TransferRequestGraceHours",
                table: "Locations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TransferRequestLockHours",
                table: "Locations",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Address1",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address2",
                table: "Brands",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address3",
                table: "Brands",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyProfilePictureId",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NeedToTreatAnyWorkDayAsHalfDay",
                table: "Brands",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WeekDayTreatAsHalfDay",
                table: "Brands",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BackGrandEndTime",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "BackGroundStartTime",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "DefaultRequestLocationRefId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "District",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "DoesDayCloseRunInBackGround",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "HouseTransactionDate",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "IsDayCloseRecursiveUptoDateAllowed",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "IsProductionAllowed",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "IsProductionUnitAllowed",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "IsPurchaseAllowed",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "LocationTaxCode",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "OrganizationUnitId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ReceiptFooter",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ReceiptHeader",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ReferenceCode",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "RequestedLocations",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SharingPercentage",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "TransferRequestGraceHours",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "TransferRequestLockHours",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Address1",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Address2",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Address3",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "CompanyProfilePictureId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "NeedToTreatAnyWorkDayAsHalfDay",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "WeekDayTreatAsHalfDay",
                table: "Brands");
        }
    }
}

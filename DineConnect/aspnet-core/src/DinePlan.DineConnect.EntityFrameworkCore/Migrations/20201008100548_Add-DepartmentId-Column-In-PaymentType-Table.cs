﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AddDepartmentIdColumnInPaymentTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "PaymentTypes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTypes_DepartmentId",
                table: "PaymentTypes",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentTypes_Departments_DepartmentId",
                table: "PaymentTypes",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentTypes_Departments_DepartmentId",
                table: "PaymentTypes");

            migrationBuilder.DropIndex(
                name: "IX_PaymentTypes_DepartmentId",
                table: "PaymentTypes");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "PaymentTypes");
        }
    }
}

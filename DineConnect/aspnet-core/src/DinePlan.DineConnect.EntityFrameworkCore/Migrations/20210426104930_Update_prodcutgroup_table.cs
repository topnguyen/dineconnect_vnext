﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
	public partial class Update_prodcutgroup_table : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AddColumn<int>(
				name: "ParentId",
				table: "ProductGroups",
				nullable: true);

			migrationBuilder.CreateIndex(
				name: "IX_ProductGroups_ParentId",
				table: "ProductGroups",
				column: "ParentId");

			migrationBuilder.AddForeignKey(
				name: "FK_ProductGroups_ProductGroups_ParentId",
				table: "ProductGroups",
				column: "ParentId",
				principalTable: "ProductGroups",
				principalColumn: "Id",
				onDelete: ReferentialAction.Restrict);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_ProductGroups_ProductGroups_ParentId",
				table: "ProductGroups");

			migrationBuilder.DropIndex(
				name: "IX_ProductGroups_ParentId",
				table: "ProductGroups");

			migrationBuilder.DropColumn(
				name: "MaxQty",
				table: "UpMenuItems");

			migrationBuilder.DropColumn(
				name: "ParentId",
				table: "ProductGroups");
		}
	}
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Add_table_WheelTicketShippingOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WheelTicketShippingOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ShippingOrderStatus = table.Column<string>(nullable: true),
                    ShippingProviderName = table.Column<int>(nullable: false),
                    ShippingOrderAmount = table.Column<string>(nullable: true),
                    ShippingOrderId = table.Column<string>(nullable: true),
                    WheelTicketId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WheelTicketShippingOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WheelTicketShippingOrders_WheelTickets_WheelTicketId",
                        column: x => x.WheelTicketId,
                        principalTable: "WheelTickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WheelTicketShippingOrders_WheelTicketId",
                table: "WheelTicketShippingOrders",
                column: "WheelTicketId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WheelTicketShippingOrders");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class AdjustTiffinOrderInformation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DeliveryCharge",
                table: "TiffinCustomerProductOfferOrders",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeliveryDate",
                table: "TiffinCustomerProductOfferOrders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "DeliveryType",
                table: "TiffinCustomerProductOfferOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "MenuItemsDynamic",
                table: "TiffinCustomerProductOfferOrders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeSlotFormTo",
                table: "TiffinCustomerProductOfferOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TiffinCustomerProductOfferOrders_SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders",
                column: "SelfPickupLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_Locations_SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders",
                column: "SelfPickupLocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TiffinCustomerProductOfferOrders_Locations_SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropIndex(
                name: "IX_TiffinCustomerProductOfferOrders_SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "DeliveryCharge",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "DeliveryDate",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "DeliveryType",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "MenuItemsDynamic",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "SelfPickupLocationId",
                table: "TiffinCustomerProductOfferOrders");

            migrationBuilder.DropColumn(
                name: "TimeSlotFormTo",
                table: "TiffinCustomerProductOfferOrders");
        }
    }
}

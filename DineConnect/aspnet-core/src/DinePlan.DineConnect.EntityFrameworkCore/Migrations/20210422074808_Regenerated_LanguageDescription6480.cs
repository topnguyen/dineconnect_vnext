﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class Regenerated_LanguageDescription6480 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "LanguageDescriptions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "LanguageDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "LanguageDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "LanguageDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "LanguageDescriptions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "LanguageDescriptions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "LanguageDescriptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "LanguageDescriptions");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "LanguageDescriptions");
        }
    }
}

﻿using Abp.Domain.Entities;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using DinePlan.DineConnect.AddOn;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Authorization.Delegation;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Chat;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Language;
using DinePlan.DineConnect.Connect.Master.Calculations;
using DinePlan.DineConnect.Connect.Master.Cards;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.DineDevices;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.LocationGroups;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.LocationTags;
using DinePlan.DineConnect.Connect.Master.Numberators;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.PlanReasons;
using DinePlan.DineConnect.Connect.Master.Printer;
using DinePlan.DineConnect.Connect.Master.Terminals;
using DinePlan.DineConnect.Connect.Master.TicketTypes;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Master.UserLocations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Categories;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using DinePlan.DineConnect.Connect.Messages.TickMessage;
using DinePlan.DineConnect.Connect.MutilLanguage;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Table;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Connect.Tag.TicketTags;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Core.Report;
using DinePlan.DineConnect.Core.Swipe;
using DinePlan.DineConnect.CORS;
using DinePlan.DineConnect.DineQueue;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Framework;
using DinePlan.DineConnect.Friendships;
using DinePlan.DineConnect.Go;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.MultiTenancy.Accounting;
using DinePlan.DineConnect.MultiTenancy.Payments;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Shipment;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Tiffins;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;
using DinePlan.DineConnect.Tiffins.Faq;
using DinePlan.DineConnect.Tiffins.HomeBanner;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MealTimes;
using DinePlan.DineConnect.Tiffins.OrderTags;
using DinePlan.DineConnect.Tiffins.Promotion;
using DinePlan.DineConnect.Wheel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq.Expressions;
using DinePlan.DineConnect.BaseCore.TemplateEngines;
using DinePlan.DineConnect.Clique;
using DinePlan.DineConnect.Cluster;
using LocationSchedule = DinePlan.DineConnect.Connect.Master.Locations.LocationSchedule;
using DinePlan.DineConnect.Wheel.V2;
using DinePlan.DineConnect.Reserve;

namespace DinePlan.DineConnect.EntityFrameworkCore
{
    public class DineConnectDbContext : AbpZeroDbContext<Tenant, Role, User, DineConnectDbContext>,
        IAbpPersistedGrantDbContext
    {
        public virtual DbSet<LastSync> LastSyncs { get; set; }
        public virtual DbSet<LocationSyncer> LocationSyncers { get; set; }
        public virtual DbSet<Syncer> Syncers { get; set; }

        public virtual DbSet<ReserveTable> ReserveTable { get; set; }
        public virtual DbSet<ReserveSeatMap> ReserveSeatMap { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<ReserveSeatMapBackgroundUrl> ReserveSeatMapBackgroundUrl { get; set; }


        public ConnectSession ConnectSession { get; set; }
        protected virtual int? OrganizationId => GetOrganizationId();

        protected virtual bool IsMustHaveOrganizationFilterEnabled =>
            CurrentUnitOfWorkProvider?.Current?.IsFilterEnabled(DineConnectDataFilters.MustHaveOrganization) == true;

        #region Core

        public virtual DbSet<AllowOrigin> AllowOrigins { get; set; }

        public virtual DbSet<LanguageDescription> LanguageDescriptions { get; set; }
        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual DbSet<Friendship> Friendships { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { get; set; }
        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }
        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }
        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

        public virtual DbSet<ReportBackground> ReportBackgrounds { get; set; }
        public virtual DbSet<UserLocation> UserLocations { get; set; }
        public virtual DbSet<TenantDatabase> TenantDatabases { get; set; }
        public virtual DbSet<TemplateEngine> TemplateEngines { get; set; }

        public virtual DbSet<Addon> Addons { get; set; }
        public virtual DbSet<TenantAddon> TenantAddons { get; set; }

        public virtual DbSet<UserDelegation> UserDelegations { get; set; }

        #region Common

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerAddressDetail> CustomerAddressDetails { get; set; }
        public virtual DbSet<ConnectGuest> ConnectGuests { get; set; }

        #endregion Common

        #region Address

        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }

        #endregion Address

        #endregion Core

        #region Connect

        #region Master

        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationSchedule> LocationSchedules { get; set; }
        public virtual DbSet<LocationBranch> LocationBranchs { get; set; }
        public virtual DbSet<LocationGroup> LocationGroups { get; set; }
        public virtual DbSet<LocationTag> LocationTags { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<LocationTagLocation> LocationTagLocations { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Terminal> Terminals { get; set; }
        public virtual DbSet<Calculation> Calculations { get; set; }
        public virtual DbSet<TillAccount> TillAccounts { get; set; }
        public virtual DbSet<TillTransaction> TillTransactions { get; set; }
        public virtual DbSet<TransactionOrderTag> TransactionOrderTags { get; set; }
        public virtual DbSet<TransactionType> TransactionTypes { get; set; }
        public virtual DbSet<PlanReason> PlanReasons { get; set; }
        public virtual DbSet<ForeignCurrency> ForeignCurrencies { get; set; }

        public virtual DbSet<PaymentType> PaymentTypes { get; set; }

        public virtual DbSet<Numerator> Numerators { get; set; }

        public virtual DbSet<TicketType> TicketTypes { get; set; }

        public virtual DbSet<TicketTypeConfiguration> TicketTypeConfigurations { get; set; }

        public virtual DbSet<DepartmentGroup> DepartmentGroups { get; set; }

        public virtual DbSet<DineDevice> DineDevices { get; set; }
        public virtual DbSet<DinePlanLanguageText> DinePlanLanguageTexts { get; set; }
        public virtual DbSet<TickMessage> TickMessages { get; set; }

        #endregion Master

        #region Menu

        public virtual DbSet<ProductGroup> ProductGroups { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<MenuItemPortion> MenuItemPortions { get; set; }
        public virtual DbSet<LocationMenuItemPrice> LocationMenuItemPrices { get; set; }
        public virtual DbSet<LocationMenuItem> LocationMenuItems { get; set; }
        public virtual DbSet<UpMenuItem> UpMenuItems { get; set; }
        public virtual DbSet<UpMenuItemLocationPrice> UpMenuItemLocationPrices { get; set; }
        public virtual DbSet<MenuItemSchedule> Schedules { get; set; }

        public virtual DbSet<MenuBarCode> MenuBarCodes { get; set; }
        public virtual DbSet<ProductCombo> ProductCombos { get; set; }
        public virtual DbSet<ProductComboItem> ProductComboItems { get; set; }
        public virtual DbSet<ProductComboGroup> ProductComboGroups { get; set; }
        public virtual DbSet<ProductComboGroupDetail> ProductComboGroupDetails { get; set; }

        #region ScreenMenu

        public virtual DbSet<ScreenMenu> ScreenMenus { get; set; }
        public virtual DbSet<ScreenMenuCategory> ScreenMenuCategories { get; set; }
        public virtual DbSet<ScreenMenuItem> ScreenMenuItems { get; set; }

        #endregion ScreenMenu

        #endregion Menu

        #region Print

        public virtual DbSet<PrintConfiguration> PrintConfigurations { get; set; }
        public virtual DbSet<Printer> Printers { get; set; }
        public virtual DbSet<PrintTemplate> PrintTemplates { get; set; }
        public virtual DbSet<PrinterMap> PrinterMaps { get; set; }
        public virtual DbSet<PrintJob> PrintJobs { get; set; }
        public virtual DbSet<PrintTemplateCondition> PrintTemplateConditions { get; set; }

        #endregion Print

        #region ProgramSetting

        public virtual DbSet<ProgramSettingTemplate> ProgramSettingTemplates { get; set; }
        public virtual DbSet<ProgramSettingValue> ProgramSettingValues { get; set; }

        #endregion ProgramSetting

        #region Taxes

        public virtual DbSet<DinePlanTax> DinePlanTaxes { get; set; }
        public virtual DbSet<DinePlanTaxLocation> DinePlanTaxLocations { get; set; }
        public virtual DbSet<DinePlanTaxMapping> DinePlanTaxMappings { get; set; }

        #endregion Taxes

        #region Promotions

        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<TimerPromotion> TimerPromotions { get; set; }
        public virtual DbSet<FixedPromotion> FixedPromotions { get; set; }
        public virtual DbSet<FreePromotion> FreePromotions { get; set; }
        public virtual DbSet<FreePromotionExecution> FreePromotionExecutions { get; set; }
        public virtual DbSet<FreeValuePromotion> FreeValuePromotions { get; set; }
        public virtual DbSet<FreeValuePromotionExecution> FreeValuePromotionExecutions { get; set; }
        public virtual DbSet<DemandDiscountPromotion> DemandPromotions { get; set; }
        public virtual DbSet<DemandPromotionExecution> DemandPromotionExecutions { get; set; }
        public virtual DbSet<FreeItemPromotion> FreeItemPromotions { get; set; }
        public virtual DbSet<TicketDiscountPromotion> TicketDiscountPromotions { get; set; }
        public virtual DbSet<FreeItemPromotionExecution> FreeItemPromotionExecutions { get; set; }
        public virtual DbSet<PromotionSchedule> PromotionSchedules { get; set; }
        public virtual DbSet<PromotionRestrictItem> PromotionRestrictItems { get; set; }

        public virtual DbSet<PromotionCategory> PromotionCategories { get; set; }

        public virtual DbSet<PromotionQuota> PromotionQuotas { get; set; }
        public virtual DbSet<BuyXAndYAtZValuePromotion> BuyXAndYAtZValuePromotions { get; set; }
        public virtual DbSet<BuyXAndYAtZValuePromotionExecution> BuyXAndYAtZValuePromotionExecutions { get; set; }
        public virtual DbSet<StepDiscountPromotion> StepDiscountPromotions { get; set; }
        public virtual DbSet<StepDiscountPromotionExecution> StepDiscountPromotionExecutions { get; set; }
        public virtual DbSet<StepDiscountPromotionMappingExecution> DiscountPromotionMappingExecutions { get; set; }

        #endregion Promotions

        #region Tags

        public virtual DbSet<PriceTag> PriceTags { get; set; }
        public virtual DbSet<PriceTagDefinition> PriceTagDefinitions { get; set; }
        public virtual DbSet<PriceTagLocationPrice> PriceTagLocationPrices { get; set; }

        public virtual DbSet<TicketTag> TicketTags { get; set; }
        public virtual DbSet<TicketTagGroup> TicketTagGroups { get; set; }

        public virtual DbSet<OrderTagGroup> OrderTagGroups { get; set; }
        public virtual DbSet<OrderTag> OrderTags { get; set; }
        public virtual DbSet<OrderTagMap> OrderTagMaps { get; set; }
        public virtual DbSet<OrderTagLocationPrice> OrderTagLocationPrices { get; set; }

        #endregion Tags

        #region Table

        public virtual DbSet<ConnectTable> ConnectTables { get; set; }

        public virtual DbSet<ConnectTableGroup> ConnectTableGroups { get; set; }

        #endregion Table

        #region Card

        public virtual DbSet<ConnectCardTypeCategory> ConnectCardTypeCategories { get; set; }

        public virtual DbSet<ConnectCardType> ConnectCardTypes { get; set; }

        public virtual DbSet<ConnectCard> ConnectCards { get; set; }

        public virtual DbSet<ConnectCardRedemption> ConnectCardRedemptions { get; set; }
        public virtual DbSet<ImportCardDataDetail> ImportCardDataDetails { get; set; }

        #endregion Card

        #region FutureDate

        public virtual DbSet<FutureDateInformation> FutureDateInformations { get; set; }

        #endregion FutureDate

        #region Plan User

        public virtual DbSet<DinePlanUser> DinePlanUsers { get; set; }

        public virtual DbSet<DinePlanUserRole> DinePlanUserRoles { get; set; }

        public virtual DbSet<DinePlanPermission> DinePlanPermission { get; set; }

        #endregion Plan User

        #region Ticket

        public virtual DbSet<PostData> PostDatas { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<Connect.Transaction.Payment> Payments { get; set; }

        public virtual DbSet<PlanLogger> PlanLoggers { get; set; }

        public virtual DbSet<PreTicket> PreTickets { get; set; }

        public virtual DbSet<Ticket> Tickets { get; set; }

        public virtual DbSet<TicketTransaction> TicketTransactions { get; set; }

        #endregion Ticket

        #region Period

        public virtual DbSet<WorkPeriod> WorkPeriods { get; set; }

        #endregion Period

        #endregion Connect

        #region Tiffins

        public virtual DbSet<TiffinOrderTag> TiffinOrderTags { get; set; }

        public virtual DbSet<TiffinCustomerTransaction> TiffinCustomerTransactions { get; set; }

        public virtual DbSet<TiffinMealTime> TiffinMealTimes { get; set; }
        public virtual DbSet<TiffinProductOfferSet> TiffinProductOfferSets { get; set; }
        public virtual DbSet<TiffinProductSet> TiffinProductSets { get; set; }
        public virtual DbSet<TiffinProductOfferType> TiffinProductOfferTypes { get; set; }
        public virtual DbSet<TiffinProductOffer> TiffinProductOffers { get; set; }
        public virtual DbSet<TiffinSchedule> TiffinSchedules { get; set; }
        public virtual DbSet<TiffinScheduleDetail> TiffinScheduleDetails { get; set; }
        public virtual DbSet<TiffinProductSetProduct> TiffinProductSetProducts { get; set; }
        public virtual DbSet<TiffinScheduleDetailProduct> TiffinScheduleDetailProducts { get; set; }
        public virtual DbSet<TiffinCustomerProductOffer> TiffinCustomerProductOffers { get; set; }
        public virtual DbSet<TiffinCustomerProductOfferOrder> TiffinCustomerProductOfferOrders { get; set; }
        public virtual DbSet<TiffinPayment> TiffinInvoices { get; set; }
        public virtual DbSet<TiffinPaymentRequest> TiffinPaymentRequests { get; set; }

        public virtual DbSet<TiffinFaq> TiffinFaqs { get; set; }
        public virtual DbSet<TiffinHomeBanner> TiffinHomeBanners { get; set; }

        public virtual DbSet<TiffinPromotion> TiffinPromotion { get; set; }
        public virtual DbSet<TiffinPromotionCode> TiffinPromotionCodes { get; set; }

        public virtual DbSet<TiffinCustomerReferral> TiffinCustomerReferrals { get; set; }

        public virtual DbSet<TiffinCustomerMealPlanReminder> TiffinCustomerMealPlanReminders { get; set; }

        public virtual DbSet<TiffinDeliveryTimeSlot> TiffinDeliveryTimeSlots { get; set; }

        #endregion Tiffins

        #region Wheel

        public virtual DbSet<WheelDynamicPages> WheelDynamicPages { get; set; }

        public virtual DbSet<WheelSetup> WheelSetups { get; set; }
        public virtual DbSet<WheelDepartment> WheelDepartments { get; set; }
        public virtual DbSet<WheelLocation> WheelLocations { get; set; }
        public virtual DbSet<WheelTax> WheelTaxes { get; set; }
        public virtual DbSet<WheelPaymentMethod> WheelPaymentMethods { get; set; }
        public virtual DbSet<WheelDeliveryDuration> WheelDeliveryDurations { get; set; }
        public virtual DbSet<WheelDeliveryZone> WheelDeliveryZones { get; set; }
        public virtual DbSet<WheelOpeningHour> WheelOpeningHours { get; set; }
        public virtual DbSet<WheelServiceFee> WheelServiceFees { get; set; }
        public virtual DbSet<WheelPayment> WheelPayments { get; set; }
        public virtual DbSet<WheelPaymentRequest> WheelPaymentRequests { get; set; }
        public virtual DbSet<WheelTicket> WheelTickets { get; set; }
        public virtual DbSet<WheelTable> WheelTables { get; set; }
        public virtual DbSet<WheelDepartmentTimeSlot> WheelDepartmentTimeSlots { get; set; }
        public virtual DbSet<WheelOrderTracking> WheelOrderTrackings { get; set; }
        public virtual DbSet<WheelTicketShippingOrder> WheelTicketShippingOrders { get; set; }
        public virtual DbSet<WheelItemSoldOut> WheelItemSoldOuts { get; set; }
        public virtual DbSet<WheelTagSoldOut> WheelTagSoldOuts { get; set; }
        public virtual DbSet<WheelRecommendationMenu> WheelRecommendationMenus { get; set; }
        public virtual DbSet<WheelRecommendationDayPeriod> WheelRecommendationDayPeriods { get; set; }
        public virtual DbSet<WheelRecommendationMenuItem> WheelRecommendationMenuItems { get; set; }


        #endregion Wheel

        #region Queue

        public virtual DbSet<QueueLocation> QueueLocations { get; set; }

        public virtual DbSet<QueueLocationOption> QueueLocationOptions { get; set; }

        public virtual DbSet<CustomerQueue> CustomerQueues { get; set; }

        #endregion DineQueue

        #region Swipe

        public virtual DbSet<SwipeCardType> SwipeCardTypes { get; set; }

        public virtual DbSet<SwipeCard> SwipeCards { get; set; }

        public virtual DbSet<SwipeCardOTP> SwipeCardOTPs { get; set; }

        public virtual DbSet<SwipeCustomerCard> SwipeCustomerCards { get; set; }

        public virtual DbSet<SwipeTransactionDetail> SwipeTransactionDetails { get; set; }

        public virtual DbSet<SwipeCardLedger> SwipeCardLedgers { get; set; }


        #endregion

        #region Go

        public virtual DbSet<DineGoDevice> DineGoDevices { get; set; }
        public virtual DbSet<DineGoBrand> DineGoBrands { get; set; }
        public virtual DbSet<DineGoDepartment> DineGoDepartments { get; set; }
        public virtual DbSet<DineGoPaymentType> DineGoPaymentTypes { get; set; }
        public virtual DbSet<DineGoPaymentTypesDevice> DineGoPaymentTypeDevices { get; set; }
        public virtual DbSet<DineGoCharge> DineGoCharges { get; set; }
        public virtual DbSet<DineGoBrandDepartment> DineGoBrandDepartments { get; set; }
        public virtual DbSet<DineGoBrandDevice> DineGoBrandDevices { get; set; }
        public virtual DbSet<DineGoChargeDepartment> DineGoChargeDepartments { get; set; }


        #endregion

		#region Shipment
        public virtual DbSet<ShipmentDepot> ShipmentDepots { get; set; }

        public virtual DbSet<ShipmentDriver> ShipmentDrivers { get; set; }

        public virtual DbSet<ShipmentItem> ShipmentItems { get; set; }

        public virtual DbSet<ShipmentOrder> ShipmentOrders { get; set; }

        public virtual DbSet<ShipmentRoute> ShipmentRoutes { get; set; }

        public virtual DbSet<ShipmentRoutePoint> ShipmentRoutePoints { get; set; }

        public virtual DbSet<ShipmentRoutePointOrder> ShipmentRoutePointOrders { get; set; }
        
        public virtual DbSet<ShipmentAddress> ShipmentAddress { get; set; }
        
        public virtual DbSet<ShipmentClient> ShipmentClients { get; set; }

        public virtual DbSet<ShipmentClientAddress> ShipmentClientAddress { get; set; }

        public virtual DbSet<WheelCartItem> WheelCartItems { get; set; }

        public virtual DbSet<WheelOrderItem> WheelOrderItems { get; set; }

        public virtual DbSet<WheelCustomerSessionData> WheelCustomerSessionData { get; set; }

        #endregion

        #region Audit

        public virtual DbSet<ExternalLog> ExternalLogs { get; set; }

        public virtual DbSet<ConnectAddOn> ConnectAddOns { get; set; }


        #endregion

        #region Clique
        public virtual DbSet<CliqueMenu> CliqueMenus { get; set; }
        public virtual DbSet<CliqueItemStock> CliqueItemStocks { get; set; }
        public virtual DbSet<CliqueMealTime> CliqueMealTimes { get; set; }
        public virtual DbSet<CliqueCustomerFavoriteItem> CliqueCustomerFavoriteItems { get; set; }

        public virtual DbSet<CliqueOrder> CliqueOrders { get; set; }
        public virtual DbSet<CliqueOrderDetail> CliqueOrderDetails { get; set; }
        public virtual DbSet<CliquePayment> CliquePayments { get; set; }


        #endregion

        #region Flyer

        public virtual DbSet<Flyer.Flyer> Flyers { get; set; }

        #endregion

        #region Cluster
        public virtual DbSet<DelAggLocationGroup> DelAggLocationGroups { get; set; }
        public virtual DbSet<DelAggLocation> DelAggLocations { get; set; }
        public virtual DbSet<DelAggLocMapping> DelAggLocMappings { get; set; }
        public virtual DbSet<DelAggLocationItem> DelAggLocationItems { get; set; }
        public virtual DbSet<DelAggItemGroup> DelAggItemGroups { get; set; }
        public virtual DbSet<DelAggItem> DelAggItems { get; set; }
        public virtual DbSet<DelAggVariantGroup> DelAggVariantGroups { get; set; }
        public virtual DbSet<DelAggVariant> DelAggVariants { get; set; }
        public virtual DbSet<DelAggModifierGroup> DelAggModifierGroups { get; set; }
        public virtual DbSet<DelAggModifier> DelAggModifiers { get; set; }
        public virtual DbSet<DelAggModifierGroupItem> DelAggModifierGroupItems { get; set; }
        public virtual DbSet<DelAggCategory> DelAggCategorys { get; set; }
        public virtual DbSet<DelTimingGroup> DelTimingGroups { get; set; }
        public virtual DbSet<DelTimingDetail> DelTimingDetails { get; set; }
        public virtual DbSet<DelAggTax> DelAggTaxs { get; set; }
        public virtual DbSet<DelAggTaxMapping> DelAggTaxMappings { get; set; }
        public virtual DbSet<DelAggCharge> DelAggCharges { get; set; }
        public virtual DbSet<DelAggChargeMapping> DelAggChargeMappings { get; set; }
        public virtual DbSet<DelAggLanguage> DelAggLanguages { get; set; }
        public virtual DbSet<DelAggImage> DelAggImages { get; set; }
        #endregion Cluster
        
        protected override bool ShouldFilterEntity<TEntity>(IMutableEntityType entityType)
        {
            if (typeof(IMustHaveOrganization).IsAssignableFrom(typeof(TEntity))) return true;
            if (typeof(ISoftDelete).IsAssignableFrom(typeof(TEntity))) return true;

            if (typeof(IMayHaveTenant).IsAssignableFrom(typeof(TEntity))) return true;

            if (typeof(IMustHaveTenant).IsAssignableFrom(typeof(TEntity))) return true;

            return false;
        }

        protected override Expression<Func<TEntity, bool>> CreateFilterExpression<TEntity>()
        {
            Expression<Func<TEntity, bool>> expression = null;

            if (typeof(IMustHaveOrganization).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> organizationFilter = e =>
                    ((IMustHaveOrganization)e).OrganizationId == OrganizationId ||
                    ((IMustHaveOrganization)e).OrganizationId == OrganizationId == IsMustHaveOrganizationFilterEnabled;
                expression = organizationFilter;
            }

            if (typeof(ISoftDelete).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> softDeleteFilter = e =>
                    !((ISoftDelete)e).IsDeleted || ((ISoftDelete)e).IsDeleted != IsSoftDeleteFilterEnabled;
                expression = expression == null ? softDeleteFilter : CombineExpressions(expression, softDeleteFilter);
            }

            if (typeof(IMayHaveTenant).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> mayHaveTenantFilter = e =>
                    ((IMayHaveTenant)e).TenantId == CurrentTenantId || ((IMayHaveTenant)e).TenantId ==
                    CurrentTenantId == IsMayHaveTenantFilterEnabled;
                expression = expression == null
                    ? mayHaveTenantFilter
                    : CombineExpressions(expression, mayHaveTenantFilter);
            }

            if (typeof(IMustHaveTenant).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> mustHaveTenantFilter = e =>
                    ((IMustHaveTenant)e).TenantId == CurrentTenantId || ((IMustHaveTenant)e).TenantId ==
                    CurrentTenantId == IsMustHaveTenantFilterEnabled;
                expression = expression == null
                    ? mustHaveTenantFilter
                    : CombineExpressions(expression, mustHaveTenantFilter);
            }

            return expression;
        }

        protected int GetOrganizationId()
        {
            return ConnectSession.OrgId;
        }

        public DineConnectDbContext(DbContextOptions<DineConnectDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Tiffins

            modelBuilder.Entity<LastSync>(l =>
            {
                l.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<Syncer>(s =>
                       {
                           s.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<LanguageDescription>(l =>
                    {
                        l.HasIndex(e => new { e.TenantId });
                    });
            modelBuilder.Entity<TiffinCustomerProductOfferOrder>()
                           .HasOne(c => c.CustomerProductOffer)
                           .WithMany()
                           .HasForeignKey(d => d.CustomerProductOfferId)
                           .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<TiffinCustomerProductOffer>()
                .HasOne(c => c.Customer)
                .WithMany()
                .HasForeignKey(d => d.CustomerId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<TiffinOrderTag>(t =>
            {
                t.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<TiffinMealTime>(t =>
            {
                t.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<TiffinCustomerTransaction>(t =>
            {
                t.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<TiffinProductOfferSet>(entity =>
            {
                entity.HasOne(d => d.TiffinProductSet)
                    .WithMany(p => p.ProductOfferSets)
                    .HasForeignKey(d => d.TiffinProductSetId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TiffinProductOffer)
                    .WithMany(p => p.ProductOfferSets)
                    .HasForeignKey(d => d.TiffinProductOfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            #endregion Tiffins

            #region Wheel

            modelBuilder.Entity<WheelSetup>(w =>
            {
                w.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<WheelDepartment>(w =>
                       {
                           w.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<WheelLocation>(w =>
                       {
                           w.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<WheelTax>(w =>
                       {
                           w.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<WheelPaymentMethod>(w =>
                       {
                           w.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<WheelDeliveryDuration>(w =>
                       {
                           w.HasIndex(e => new { e.TenantId });
                       });
          

            modelBuilder.Entity<WheelPaymentRequest>(w =>
            {
                w.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<WheelTicket>(w =>
            {
                modelBuilder.Entity<WheelTicket>().Ignore(c => c.DateDelivery);
            });

            #endregion Wheel

            #region Common

            modelBuilder.Entity<TemplateEngine>(t => { t.HasIndex(e => new { e.TenantId }); });

            #endregion Common

            modelBuilder.Entity<TiffinProductOffer>(t => { t.HasIndex(e => new { e.TenantId }); });

            modelBuilder.Entity<DinePlanTax>(d => { d.HasIndex(e => new { e.TenantId }); });

            modelBuilder.Entity<Promotion>(p => { p.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<Promotion>(p => { p.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<LocationTagLocation>(l => { l.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<LocationTag>(l => { l.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<Department>(d => { d.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<PriceTag>(p => { p.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<TicketTagGroup>(t => { t.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<FutureDateInformation>(f => { f.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<Terminal>(t => { t.HasIndex(e => new { e.TenantId }); });

            modelBuilder.Entity<Calculation>(c => { c.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<TillAccount>(t => { t.HasIndex(e => new { e.TenantId }); });

            modelBuilder.Entity<PlanReason>(p => { p.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<ForeignCurrency>(f => { f.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<PaymentType>(p => { p.HasIndex(e => new { e.TenantId }); });
            modelBuilder.Entity<BinaryObject>(b => { b.HasIndex(e => new { e.TenantId }); });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.OrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LocationGroup>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.LocationGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationGroups)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LocationMenuItem>(entity =>
            {
                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.LocationMenuItems)
                    .HasForeignKey(d => d.MenuItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationMenuItems)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MenuItem>(entity =>
            {
                entity.HasOne(d => d.Category)
                    .WithMany(p => p.MenuItems)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserLocation>(entity =>
            {
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.UserLocations)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ConnectTableInGroup>()
                .HasOne(pt => pt.ConnectTable)
                .WithMany(p => p.ConnectTableInGroups)
                .HasForeignKey(pt => pt.ConnectTableId);

            modelBuilder.Entity<ConnectTableInGroup>()
                .HasOne(pt => pt.ConnectTableGroup)
                .WithMany(p => p.ConnectTableInGroups)
                .HasForeignKey(pt => pt.ConnectTableGroupId);

            modelBuilder.Entity<QueueLocationOption>()
                .HasOne(u => u.QueueLocation)
                .WithMany(s => s.QueueLocationOptions)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CustomerQueue>()
                .HasOne(u => u.QueueLocationOption)
                .WithMany(s => s.CustomerQueues)
                .OnDelete(DeleteBehavior.Restrict);  
            
            ////Increase Audit Log Parameter
            //// default is 1024
            //// https://forum.aspnetboilerplate.com/viewtopic.php?p=21760
            ///AuditLog.MaxParametersLength = 10000;
            ////modelBuilder.Entity<AuditLog>().Property(p => p.Parameters).HasMaxLength(10000);

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
﻿using Abp.Data;
using Abp.EntityFrameworkCore;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.EntityFrameworkCore.Repositories
{
    public class LocationMenuItemCustomRepository : DineConnectRepositoryBase<LocationMenuItem, int>, ILocationMenuItemCustomRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;

        public LocationMenuItemCustomRepository(IDbContextProvider<DineConnectDbContext> dbContextProvider,
            IActiveTransactionProvider transactionProvider) : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public async Task AddLocationMenuItems(List<LocationMenuItem> locationMenuItems)
        {
            await Context.LocationMenuItems.AddRangeAsync(locationMenuItems);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteLocationMenuItems(List<LocationMenuItem> locationMenuItems)
        {
            Context.LocationMenuItems.RemoveRange(locationMenuItems);
            await Context.SaveChangesAsync();
        }
    }
}
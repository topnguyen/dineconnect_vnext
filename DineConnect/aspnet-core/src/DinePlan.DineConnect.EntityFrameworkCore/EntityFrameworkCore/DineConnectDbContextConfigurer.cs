using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.EntityFrameworkCore
{
    public static class DineConnectDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DineConnectDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DineConnectDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Web;

namespace DinePlan.DineConnect.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DineConnectDbContextFactory : IDesignTimeDbContextFactory<DineConnectDbContext>
    {
        public DineConnectDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DineConnectDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            DineConnectDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DineConnectConsts.ConnectionStringName));

            return new DineConnectDbContext(builder.Options);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DinePlan.DineConnect.Core.QueryScheduler.Helpers
{
    public class DBHelper
    {
        public static DataSet ExecuteSqlQueryDataSet(string sqlQuery, SqlParameter[] paramColl, string connStr = "")
        {
            using (SqlConnection con = new SqlConnection(connStr))
            {
                con.Open();
                using (SqlCommand dbCommand = new SqlCommand(sqlQuery, con))
                {
                    if (paramColl != null)
                    {
                        foreach (SqlParameter sp in paramColl)
                        {
                            dbCommand.Parameters.Add(sp);
                        }
                    }

                    SqlDataAdapter da = new SqlDataAdapter(dbCommand);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    return ds;
                }
            }
        }
    }
}

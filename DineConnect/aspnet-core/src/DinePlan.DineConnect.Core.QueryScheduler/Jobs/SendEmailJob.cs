﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using ClosedXML.Excel;
using DinePlan.DineConnect.Core.QueryScheduler.Interfaces;
using DinePlan.DineConnect.Core.QueryScheduler.Models;
using DinePlan.DineConnect.EmailConfiguration;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using DinePlan.DineConnect.Core.QueryScheduler.Helpers;
using DinePlan.DineConnect.CoreQueryScheduler.Enums;
using System.Data;

namespace DinePlan.DineConnect.Core.QueryScheduler.Jobs
{
    public class SendEmailJob<T> : ISendEmailJob, ISingletonDependency where T : class
    {
        public static ITenantEmailSendAppService _tenantEmailSendAppService;
        public SendEmailJob(ITenantEmailSendAppService tenantEmailSendAppService) {
            _tenantEmailSendAppService = tenantEmailSendAppService;
        }

        public static void ExecuteJob(SendQueryJobModel<T> args)
        {
            try
            {
                var toDate = DateTime.Now.ToShortDateString();
                var Days = args.querySchedulerType == QuerySchedulerType.Daily ? 1 : args.querySchedulerType == QuerySchedulerType.Monthly ? 30 : args.querySchedulerType == QuerySchedulerType.Weekly ? 7 : 0;
                var fromDate = DateTime.Now.AddDays(-Days).ToShortDateString();
                var SQLquery = args.query.ToString();
                SQLquery += $" {(SQLquery.Contains("Where") ? "And".ToString() : "Where".ToString())} {args.dateField} < {toDate} And  {args.dateField} > {fromDate} ";
                //QueryData.
                var dataSet = DBHelper.ExecuteSqlQueryDataSet(SQLquery, null, args.connectionString);
                if (dataSet.Tables == null || dataSet.Tables.Count == 0)
                    throw new Exception("Job failed Query didn't fetch any data");
                //Build Excel/CSV.
                var workbook = new XLWorkbook();
                workbook.AddWorksheet(args.ReportName);
                var ws = workbook.Worksheet(args.ReportName);

                int row = 1;
                int headerColumn = 1;
                //building header:
                foreach (DataColumn dataColumn in dataSet.Tables[0].Columns)
                {
                    ws.Cell(row, headerColumn).Value = dataColumn.ColumnName.ToString();
                    headerColumn++;
                }
                row++;
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    int column = 1;
                    foreach (DataColumn dataColumn in dataSet.Tables[0].Columns)
                    {
                        object item = dataRow[dataColumn];
                        // read column and item
                        ws.Cell(row, column).Value = item.ToString();
                        column++;
                    }
                    row++;
                }
                //Save Excel File.
                using (var msB = new MemoryStream())
                {
                    workbook.SaveAs(msB);
                    //Send Eamil
                    _tenantEmailSendAppService.Send(args.email, args.emailSubject, args.content, args.isHTML, new Email.FileAttach { FileBytes = msB.ToArray(), FileMime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", FileName = $"{args.emailSubject}.xlsx" }, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ExecuteReportJob(SendQueryJobModel<T> args)
        {
            try
            {
                var sqLquery = args.query;
                //QueryData.
                
                var dataSet = DBHelper.ExecuteSqlQueryDataSet(sqLquery, null, args.connectionString);
                if (dataSet.Tables == null || dataSet.Tables.Count == 0)
                    throw new Exception("Job failed Query didn't fetch any data");
               
                //Build Excel/CSV.

                var workbook = new XLWorkbook();
                workbook.AddWorksheet(args.ReportName);
                var ws = workbook.Worksheet(args.ReportName);

                int row = 1;
                int headerColumn = 1;
               
                //building header:
                foreach (DataColumn dataColumn in dataSet.Tables[0].Columns)
                {
                    ws.Cell(row, headerColumn).Value = dataColumn.ColumnName.ToString();
                    headerColumn++;
                }
                row++;
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    int column = 1;
                    foreach (DataColumn dataColumn in dataSet.Tables[0].Columns)
                    {
                        object item = dataRow[dataColumn];
                        // read column and item
                        ws.Cell(row, column).Value = item.ToString();
                        column++;
                    }
                    row++;
                }
                //Save Excel File.
                using (var msB = new MemoryStream())
                {
                    workbook.SaveAs(msB);
                    //Send Eamil
                   _tenantEmailSendAppService.Send(args.email, args.emailSubject, args.content, args.isHTML, new Email.FileAttach { FileBytes = msB.ToArray(), FileMime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", FileName = $"{args.emailSubject}.xlsx" }, null);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}

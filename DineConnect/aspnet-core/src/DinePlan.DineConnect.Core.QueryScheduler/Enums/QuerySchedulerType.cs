﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.CoreQueryScheduler.Enums
{
    public enum QuerySchedulerType
    {
        Daily,
        Weekly,
        Monthly,
        Once
    }
}

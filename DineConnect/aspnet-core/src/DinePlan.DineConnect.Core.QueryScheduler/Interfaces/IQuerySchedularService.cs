﻿using Abp.Application.Services;
using DinePlan.DineConnect.CoreQueryScheduler.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Core.QueryScheduler.Interfaces
{
    public interface IQuerySchedularService<T> : IApplicationService where T : class
    {
        void AddNewQueryJob(string toEmail, string emailSubject, string content, bool isHTML, string reportName, IQueryable<T> query, QuerySchedulerType querySchedulerType, string dateField, string connectionString);
        void ExcuteQueryJob(string toEmail, string emailSubject, string content, bool isHTML, string reportName, IQueryable<T> query, string connectionString);
    }
}

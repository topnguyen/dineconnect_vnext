﻿using DinePlan.DineConnect.CoreQueryScheduler.Enums;
using DinePlan.DineConnect.EmailConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Core.QueryScheduler.Models
{
    [Serializable]
    public class SendQueryJobModel<T> where T: class
    {
        public string email { get; set; }
        public string emailSubject { get; set; }
        public string content { get; set; }
        public bool isHTML { get; set; }
        public string query { get; set; }
        public QuerySchedulerType querySchedulerType { get; set; }
        public string ReportName { get; set; }
        public string dateField { get; set; }
        public string connectionString { get; set; }
    }
}

﻿using System.Linq;
using Abp.Dependency;
using DinePlan.DineConnect.Core.QueryScheduler.Helpers;
using DinePlan.DineConnect.Core.QueryScheduler.Interfaces;
using DinePlan.DineConnect.Core.QueryScheduler.Jobs;
using DinePlan.DineConnect.Core.QueryScheduler.Models;
using DinePlan.DineConnect.CoreQueryScheduler.Enums;
using DinePlan.DineConnect.EmailConfiguration;
using Hangfire;

namespace DinePlan.DineConnect.Core.QueryScheduler.Services
{
    public class QuerySchedularService<T> : ISingletonDependency, IQuerySchedularService<T> where T : class
    {
        private readonly ITenantEmailSendAppService _tenantEmailSendAppService;
        private readonly ISendEmailJob _sendEmailJob;
        public static string ConnectionString { get; set; }

        public QuerySchedularService(ITenantEmailSendAppService tenantEmailSendAppService, ISendEmailJob sendEmailJob)
        {
            _tenantEmailSendAppService = tenantEmailSendAppService;
            _sendEmailJob = sendEmailJob;
        }
        public void AddNewQueryJob(string toEmail, string emailSubject, string content, bool isHTML, string reportName, IQueryable<T> query, QuerySchedulerType querySchedulerType, string dateField, string connectionString)
        {
            var schedulerType = querySchedulerType == QuerySchedulerType.Daily ? Cron.Daily() : (querySchedulerType == QuerySchedulerType.Monthly ? Cron.Monthly() : querySchedulerType == QuerySchedulerType.Weekly ? Cron.Weekly() : Cron.Weekly());
            RecurringJob.AddOrUpdate(() => SendEmailJob<T>.ExecuteJob(new SendQueryJobModel<T>
            {
                email = toEmail,
                query = query.ToSql(),
                querySchedulerType = QuerySchedulerType.Daily,
                ReportName = reportName,
                emailSubject = emailSubject,
                content = content,
                isHTML = isHTML,
                connectionString = connectionString,
                dateField = dateField
            }), schedulerType);
        }
        public void ExcuteQueryJob(string toEmail, string emailSubject, string content, bool isHTML, string reportName, IQueryable<T> query, string connectionString)
        {
            Hangfire.BackgroundJob.Enqueue(() => SendEmailJob<T>.ExecuteReportJob(new SendQueryJobModel<T>
            {
                email = toEmail,
                query = query.ToSql(),
                ReportName = reportName,
                emailSubject = emailSubject,
                content = content,
                isHTML = isHTML,
                connectionString = connectionString
            }));
        }
    }
}

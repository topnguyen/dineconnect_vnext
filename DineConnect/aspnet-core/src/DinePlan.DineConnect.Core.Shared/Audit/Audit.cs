﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Audit
{
    [Table("ExternalLogs")]
    public class ExternalLog : FullAuditedEntity, IMustHaveTenant
    {
        [Column(TypeName = "date")] public virtual DateTime ExternalLogTrunc { get; set; }

        public int AuditType { get; set; }
        public string LogDescription { get; set; }
        public string LogData { get; set; }
        public DateTime LogTime { get; set; }

        public int TenantId { get; set; }
    }

    public enum ExternalLogType
    {
        TicketOutbound = 0,
        PromotionOutbound = 1,
        TagOutbound = 2,
        LocationGroupInbound = 3,
        LocationInbound = 4,
        MaterialGroupInbound = 5,
        ComboInbound = 6,
        OrderTagGroupInbound = 7,
        InboundFile = 8,
        MaterialInbound = 9,
        MinioError = 10,
        Xero = 11,
        Grab = 11,
    }
}

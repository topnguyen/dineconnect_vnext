﻿namespace DinePlan.DineConnect.Cluster
{
    public class ClusterEnum
    {
        public enum DelLanguageDescriptionType
        {
            Group = 1,
            Category = 2,
            Item = 3,
            VariantGroup = 4,
            Variant = 5,
            ModifierGroup = 6,
            Modifer = 7
        }



        public enum DelAggType
        {
            Grab = 0,
            DeliveryHero = 1,
            Zomato = 2,
            Deliveroo = 3,
            UrbanPiper = 4
        }
        public enum DelPriceType
        {
            Item = 0,
            Variant = 1,
            Modifier = 2
        }
        public enum DelAggImageType
        {
            Location = 0,
            Item = 1,
            VariantGroup = 2,
            ModifierGroup = 3,
            Modifier = 4,
            Variant = 5

        }
        public enum DelAggTaxType
        {
            ItemPrice = 0,
            Charge = 1,
        }


        public enum DelAggSpecType
        {
            Item = 0,
            Variant = 1,
            Modifier = 2,
        }
    }
}
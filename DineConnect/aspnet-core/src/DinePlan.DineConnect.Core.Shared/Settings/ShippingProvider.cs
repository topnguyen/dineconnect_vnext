﻿namespace DinePlan.DineConnect.Settings
{
    public enum ShippingProvider
    {
        None = 0 ,
        GoGoVan = 1,
        LalaMove = 2,
        Dunzo = 3,
    }
}

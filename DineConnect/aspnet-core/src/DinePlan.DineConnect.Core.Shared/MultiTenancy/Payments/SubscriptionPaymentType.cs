﻿namespace DinePlan.DineConnect.MultiTenancy.Payments
{
    public enum SubscriptionPaymentType
    {
        Manual = 0,
        RecurringAutomatic = 1,
        RecurringManual = 2
    }
}
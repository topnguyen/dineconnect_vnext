﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace DinePlan.DineConnect
{
    public class DineConnectCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectCoreSharedModule).GetAssembly());
        }
    }
}
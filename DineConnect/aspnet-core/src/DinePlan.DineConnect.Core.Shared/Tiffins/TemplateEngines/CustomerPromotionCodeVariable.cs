﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines
{
    public enum CustomerPromotionCodeVariable
    {
        CustomerName,

        PromotionCode
    }
}

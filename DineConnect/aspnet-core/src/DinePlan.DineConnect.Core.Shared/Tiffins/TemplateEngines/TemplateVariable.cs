﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines
{
    public enum TemplateVariable
    {
        First_name,
        Last_name,
        Email_address,
        Phone_number,
        Address1,
        Address2,
        Address3,
        City,
        Country,
        Postcal_code,
        Zone,
        MealPlanName,
        PaidAmount,
        Payment_Detail,
        List_Order,
        Min_Meal_Count,
        Min_Expiry_Day
    }
}
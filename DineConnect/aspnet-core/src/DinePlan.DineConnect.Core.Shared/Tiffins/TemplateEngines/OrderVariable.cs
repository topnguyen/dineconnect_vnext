﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.TemplateEngines
{
    public enum OrderVariable
    {
        Company_Address,
        First_name,
        Last_name,
        Email_address,
        Phone_number,
        Address1,
        Address2,
        Address3,
        City,
        Country,
        Postcal_code,
        Zone,
        Delivery_Address,
        Mobile_No,
        List_Order

    }
}

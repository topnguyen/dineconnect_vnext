﻿namespace DinePlan.DineConnect.Tiffins.TemplateEngines
{
    public enum ReferralEarningProductOfferVariable
    {
        CustomerName,

        ProductOfferName,

        BalanceCredit
    }
}

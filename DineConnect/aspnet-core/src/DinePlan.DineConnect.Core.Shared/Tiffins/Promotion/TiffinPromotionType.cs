namespace DinePlan.DineConnect.Tiffins.Promotion
{
    public enum TiffinPromotionType
    {
        Percentage = 0,
        
        Value = 1
    }
}
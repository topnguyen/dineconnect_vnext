namespace DinePlan.DineConnect.Tiffins.Schedule
{
    public enum ScheduleDetailStatus
    {
        Draft = 0,
        Published = 1
    }
}
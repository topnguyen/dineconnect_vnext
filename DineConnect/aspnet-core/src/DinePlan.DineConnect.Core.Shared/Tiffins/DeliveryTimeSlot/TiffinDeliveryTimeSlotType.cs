﻿namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot
{
    public enum TiffinDeliveryTimeSlotType
    {
        Delivery,

        SelfPickup
    }
}

namespace DinePlan.DineConnect.Tiffins.Invoice
{
    public enum TiffinPaymentType
    {
        MealPlan,
        Order
    }
}
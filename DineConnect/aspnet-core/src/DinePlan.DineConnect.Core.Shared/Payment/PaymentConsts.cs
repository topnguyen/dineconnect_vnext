﻿using static DinePlan.DineConnect.Wheel.WheelPaymentMethodConsts;

namespace DinePlan.DineConnect.Payment
{
    public class PaymentConsts
    {
        public const string PrefixSettingPayment = "Abp.Tenant.";

        public const string FriendlyNameSetting = "FriendlyName";
        public const string LogoSetting = "Logo";
        public const string SupportsCaptureSetting = "SupportsCapture";
        public const string RefundSetting = "Refund";
        public const string PartialRefundSetting = "PartialRefund";
        public const string VoidSetting = "Void";
        public const string RecurringSupportSetting = "RecurringSupport";
        public const string DisplayOrderSetting = "DisplayOrder";
        public const string ActiveSetting = "Active";
        public const string DynamicFieldData = "DynamicFieldData";
        public const string DynamicFieldConfig = "DynamicFieldConfig";

        public const string StripePaymentSystemName = "Payments.Stripe";
        public const string SwipePaymentSystemName = "Payments.Swipe";
        public const string PayPalPaymentSystemName = "Payments.PayPal";
        public const string OmisePaymentSystemName = "Payments.Omise";
        public const string ApplePaymentSystemName = "Payments.Apple";
        public const string AdyenPaymentSystemName = "Payments.Adyen";
        public const string CashPaymentSystemName = "Payments.Cash";
        public const string GooglePaySystemName = "Payments.Google";
        public const string HitPayPaymentSystemName = "Payments.HitPay";

        public static string DefaultFriendlyName(string systemName)
        {
            switch (systemName)
            {
                case StripePaymentSystemName:
                    return "Stripe";

                case PayPalPaymentSystemName:
                    return "PayPal";

                case OmisePaymentSystemName:
                    return "Omise";

                case ApplePaymentSystemName:
                    return "Apple";

                case AdyenPaymentSystemName:
                    return "Adyen";

                case CashPaymentSystemName:
                    return "Cash";

                case HitPayPaymentSystemName:
                    return "HitPay";

                case SwipePaymentSystemName:
                    return "Swipe";

                default:
                    return string.Empty;
            }
        }

        public const string DefaultLogoSetting = "";
        public const bool DefaultSupportsCaptureSetting = false;
        public const bool DefaultRefundSetting = false;
        public const bool DefaultPartialRefundSetting = false;
        public const bool DefaultVoidSetting = false;
        public const RecurringPaymentType DefaultRecurringSupportSetting = RecurringPaymentType.NotSupported;
        public const int DefaultDisplayOrderSetting = 1;
        public const bool DefaultActiveSetting = false;
    }
}
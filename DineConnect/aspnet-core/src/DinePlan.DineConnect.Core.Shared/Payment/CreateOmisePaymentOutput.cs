﻿namespace DinePlan.DineConnect.Payment
{
    public class CreateOmisePaymentOutput
    {
        public bool PurchasedSuccess { get; set; }
        public string Message { get; set; }
        public string AuthorizeUri { get; set; }
        public long PaymentId { get; set; }
        public string OmiseId { get; set; }
        public string ScanCode { get; set; }
    }
}

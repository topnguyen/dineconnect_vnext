﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Reserve
{
    public class ReserveDto
    {
        public class ReserveSeatMapInputOutputModel
        {
            public int id { get; set; }
            public int idTable { get; set; }
            public string name { get; set; }
            public int capacity { get; set; }
            public int minimum { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public double positionX { get; set; }
            public double positionZ { get; set; }
            public bool isDeleted { get; set; }
        }

        public class ReserveDetailInputModel
        {
            public int id { get; set; }
            public DateTime fromDate { get; set; }
            public string hourFrom { get; set; }
            public string minuteFrom { get; set; }
            public DateTime toDate { get; set; }
            public string hourTo { get; set; }
            public string minuteTo { get; set; }
            public int uniqueId { get; set; }
            public string status { get; set; }
            public double depositFee { get; set; }
            public string voucherCode { get; set; }
            public string paymentMethod { get; set; }
            public bool depositPaid { get; set; }
            public int people { get; set; }
            public List<int> tableIds { get; set; }
            public string title { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string companyName { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public string note { get; set; }
            public int customerId { get; set; }
        }

        public class ScheduleOutputModel
        {
            public string name { get; set; }
            public BookingOutputModel booking { get; set; }
            public int? tableId { get; set; }
            public string status { get; set; }
        }

        public class BookingOutputModel
        {
            public double from { get; set; }
            public double to { get; set; }
            public int bookingId { get; set; }
        }

        public class CustomerOutput
        {
            public string name { get; set; }
            public string email { get; set; }
        }

        public class ReservationOutput
        {
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
            public string Status { get; set; }
            public double? DepositFee { get; set; }
            public string PaymentDetails { get; set; }
            public bool? DepositPay { get; set; }
            public int? TotalPeopleCount { get; set; }
            public string ReserveTableIds { get; set; }
            public int? CustomerId { get; set; }
            public int? Id { get; set; }
        }
        public class ReserveTableOutput
        {
            public int Capacity { get; set; }
        }
        public class SeatMapBackgroundUrlInputOutputModel
        {
            public int tenantId { get; set; }
            public string url { get; set; }
        }
        public class DeleteSeatMapInputModel
        {
            public int tenantId { get; set; }
            public string url { get; set; }
        }

    }
}

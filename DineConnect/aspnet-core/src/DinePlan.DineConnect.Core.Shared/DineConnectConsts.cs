﻿namespace DinePlan.DineConnect
{
    public class DineConnectConsts
    {
        public const string LocalizationSourceName = "DineConnect";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const string TenantIdResolveKey = "Abp-TenantId";

        public const bool AllowTenantsToChangeEmailSettings = true;

        public const string Currency = "SGD";

        public const string CurrencySign = "S$";

        public const string AbpApiClientUserAgent = "AbpApiClient";

        // Note:
        // Minimum accepted payment amount. If a payment amount is less then that minimum value payment progress will continue without charging payment
        // Even though we can use multiple payment methods, users always can go and use the highest accepted payment amount.
        //For example, you use Stripe and PayPal. Let say that stripe accepts min 5$ and PayPal accepts min 3$. If your payment amount is 4$.
        // User will prefer to use a payment method with the highest accept value which is a Stripe in this case.
        public const decimal MinimumUpgradePaymentAmount = 1M;
        
        public const int PaymentCacheDurationInMinutes = 30;

        public const int MinNameLength = 3;
        public const int MaxNameLength = 100;

        public const string EnvironmentStaging = "Staging";
        public static readonly string[] LogConsts =
       {
            "BACK OFFICE MENU OPENED",
            "CANCEL COMP",
            "CANCEL GIFT",
            "CANCEL ORDER",
            "CANCEL VOID",
            "CLEAR DATA",
            "CLEAR MENU",
            "CLEAR ORDERS",
            "CLEAR TABLES",
            "CLOSE TICKET",
            "COMP",
            "DISPLAY TICKET",
            "GIFT",
            "GIFT ALL",
            "USER LOGIN",
            "USER LOGOUT",
            "MODEL SAVED",
            "MODULE CLICKED",
            "NEW ORDER",
            "OPEN TILL",
            "ORDER MOVED",
            "PAYMENT",
            "PAY TICKET",
            "PRINT BILL",
            "PRINT DUPLICATE TICKET",
            "PRINT INVOICE",
            "PRINT LAST TICKET",
            "REOPEN TICKET",
            "REPRINT KITCHEN",
            "TICKET ENTITY CHANGED",
            "TICKETS MERGED",
            "UNLOCK TICKET",
            "USER PASSWORD CHANGED",
            "VOID",
            "VOID ALL",
            "PRE-ORDER",
            "NON PRE-ORDER"
        };
    }
}
namespace DinePlan.DineConnect.Wheel
{
    public enum WheelTableStatus
    {
        Vacant,
        Booked
    }
}
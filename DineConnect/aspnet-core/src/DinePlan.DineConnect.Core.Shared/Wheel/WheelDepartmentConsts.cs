﻿namespace DinePlan.DineConnect.Wheel
{
    public class WheelDepartmentConsts
    {
        public enum WheelOrderType
        {
            PickUp = 0,
            Delivery = 1,
            DineIn = 2,
            PickUpPlate = 3
        }

        public enum Page
        {
            Department = 0,
            PostalCode = 1,
            Location = 2,
            PhoneNumber=3,
            Language= 4,
            MenuOrder =5,
            Payment=6,
            PaymentInformation= 7,
            DateOrderFuture= 8,
            VerifyCode= 9
        }
    }
}
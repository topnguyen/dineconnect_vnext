﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel
{
    public static class WheelOpeningHourConsts
    {
        public enum WheelOpeningHourServiceType
        {
            Default,
            PickupService,
            DeliveryService,
            TableReservationService,
            OrderService
        }
    }
}

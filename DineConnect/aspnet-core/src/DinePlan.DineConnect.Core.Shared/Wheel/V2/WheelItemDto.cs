﻿using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelBillingInfo
    {
        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string PhonePrefix { get; set; }

        public string Blockunit { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public int? CityId { get; set; }

        public int? CountryId { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public string Lat { get; set; }

        public string Long { get; set; }

        public decimal? DeliveryFee { get; set; }

        public decimal? Tax { get; set; }

        public string WheelServiceFees { get; set; }

        public long? WheelDepartmentId { get; set; }

        public int? WheelLocationId { get; set; }

        public string PlateNumber { get; set; }

        public List<ServiceFee> CalculatedServiceFees { get; private set; }

        public void CalculateServiceFees(decimal? totalAmount)
        {
            if (string.IsNullOrEmpty(WheelServiceFees))
            {
                CalculatedServiceFees = new List<ServiceFee>();
                return;
            }

            CalculatedServiceFees = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceFee>>(WheelServiceFees);

            CalculatedServiceFees.ForEach(s =>
            {
                s.CalculatedValue = (totalAmount * s.Value ?? 0M) / 100M;
            });
        }

        public string WheelDepartmentName { get; set; }

        public string FullAddress => GetFullAddress();

        public string GetFullAddress()
        {
            List<string> address = new List<string>();

            if (!string.IsNullOrEmpty(Blockunit))
            {
                address.Add(Blockunit);
            }

            if (!string.IsNullOrEmpty(Address1))
            {
                address.Add(Address1);
            }

            if (!string.IsNullOrEmpty(Address2))
            {
                address.Add(Address2);
            }

            if (!string.IsNullOrEmpty(Address3))
            {
                address.Add(Address3);
            }

            if (!string.IsNullOrEmpty(City))
            {
                address.Add(City);
            }

            if (!string.IsNullOrEmpty(Country))
            {
                address.Add(Country);
            }

            if (!string.IsNullOrEmpty(PostcalCode))
            {
                address.Add(PostcalCode);
            }

            return address.Any() ? string.Join(", ", address.ToArray()) : "Not Available";
        }
    }

    public class ServiceFee
    {
        public string Name { get; set; }

        public decimal? Value { get; set; }

        public decimal? CalculatedValue { get; set; }
    }
}

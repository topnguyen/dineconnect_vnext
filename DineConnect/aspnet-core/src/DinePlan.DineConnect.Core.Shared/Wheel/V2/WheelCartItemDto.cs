﻿using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelTicketDto
    {
        public decimal? SubTotal { get; set; }

        public decimal? Total { get; set; }

        public string DeliveryTime { get; set; }

        public string Note { get; set; }
    }

   

    public class WheelScreenMenuItemSimpleDto
    {
        public string Name { get; set; }

        public string Image { get; set; }

        public int ProductType { get; set; }
    }

    public class WheelRecommendedCartItemDto
    {
        public int RecommendedItemId { get; set; }

        public string RecommendedItemName { get; set; }
    }

    public class WheelCartItemOrderTagDto
    {
        public string Name { get; set; }

        public decimal? Price { get; set; }

        public int? Quantity { get; set; }

        public string TagGroupName { get; set; }
    }

    public class WheelCartItemPortionDto
    {
        public string Name { get; set; }

        public int? Multiplier { get; set; }

        public decimal? Price { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Wheel
{
    public enum WheelDisplayAs
    {
        Grid,
        List,
        Collapse
    }

    public enum WheelScreenMenuItemType
    {
        Section,
        Item,
        Combo
    }
}

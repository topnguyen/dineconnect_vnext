﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel
{
    public enum WheelOrderStatus
    {
        OrderReceived = 0,
        Rejected = 1,
        Accepted = 2,
        Preparing = 3,
        Prepared = 4,
        Delivering = 5,
        Delivered = 6
    }
}

﻿namespace DinePlan.DineConnect.Wheel
{
    public class WheelPaymentMethodConsts
    {
        public enum RecurringPaymentType
        {
            /// <summary>
            /// Not supported
            /// </summary>
            NotSupported = 0,
            /// <summary>
            /// Manual
            /// </summary>
            Manual = 1,
            /// <summary>
            /// Automatic (payment is processed on payment gateway site)
            /// </summary>
            Automatic = 2
        }
    }
}
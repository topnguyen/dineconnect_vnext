﻿using DinePlan.DineConnect.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping
{
    public class GetShippingOrderQuotationResult
    {
        public ShippingProvider ShippingProvider { get; set; }

        public string ShippingFee { get; set; }

        public string ShippingFeeCurrency { get; set; }

        public string ScheduleAt { get; set; }
    }
}

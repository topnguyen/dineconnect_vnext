﻿using DinePlan.DineConnect.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping
{
    public class ProcessShippingOrderResult
    {
        public ShippingProvider ShippingProvider { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CustomerOrderId { get; set; }

        public string PlaceOrderResponse { get; set; }

        public System.Guid? RequestGuid { get; set; }

        public System.Guid? DeliveryGuid { get; set; }

        public List<string> ErrorMessages { get; set; }

        public string ShippingOrderAmount { get; set; }

        public  string ShippingOrderStatus { get; set; }
    }
}

﻿namespace DinePlan.DineConnect.Wheel
{
    public class WheelRecommendationMenuConst
    {
        public enum RecommendationStatus
        {
            InActive = 0,
            Active = 1
        }

        public enum CriteriaNotShow
        {
            Category = 0,
            Item = 1
        }

        public enum TriggerApplicable
        {
            RecommendAtCheckout = 0,
            RecommendAtMakePayment = 1,
            TimerBasedRecommendation = 2,
        }
    }
}

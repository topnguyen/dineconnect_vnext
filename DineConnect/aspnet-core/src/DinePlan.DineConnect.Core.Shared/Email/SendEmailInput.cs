﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace DinePlan.DineConnect.Email
{
    public class SendEmailInput
    {
        public int TemplateId { get; set; }
        public int? TenantId { get; set; }
        public string Module { get; set; }
        public FileAttach FileAttach { get; set; }
       
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public object Variables { get; set; }
    }
}

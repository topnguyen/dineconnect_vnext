﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Email
{
    public class FileAttach
    {
        public string FileName { get; set; }
        public byte[] FileBytes { get; set; }
        public string FileMime { get; set; }

    }
}

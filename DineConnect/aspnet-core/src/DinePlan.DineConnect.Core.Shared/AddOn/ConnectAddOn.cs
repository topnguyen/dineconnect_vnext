﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.AddOn
{
    [Table("ConnectAddOns")]
    public class ConnectAddOn : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string Name { get; set; }
        public string AddOnSettings { get; set; }
        public string Settings { get; set; }
        public int TenantId { get; set; }
    }
}

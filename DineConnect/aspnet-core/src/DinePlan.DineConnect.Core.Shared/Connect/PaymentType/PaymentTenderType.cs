﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.PaymentType
{
    public enum PaymentTenderType
    {
        NonCash = 0,
        Cash = 1
    }
}

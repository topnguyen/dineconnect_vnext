﻿namespace DinePlan.DineConnect.Connect.Transactions
{
    public enum PostContentType
    {
        Ticket = 0,
        Task = 1,
        WorkPeriod = 2,
        TillTransaction = 3,
        Clock = 4,
        Tax = 5
    }
}

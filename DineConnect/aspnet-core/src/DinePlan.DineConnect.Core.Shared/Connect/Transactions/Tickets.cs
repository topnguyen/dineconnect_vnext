﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Transactions
{
    [DataContract]
    public class TicketStateValue : IEquatable<TicketStateValue>
    {
        private static TicketStateValue _default;
        public TicketStateValue()
        {
            LastUpdateTime = DateTime.Now;
        }

        [DataMember(Name = "SN")] public string StateName { get; set; }

        [DataMember(Name = "S")] public string State { get; set; }

        [DataMember(Name = "SV", EmitDefaultValue = false)]
        public string StateValue { get; set; }

        [DataMember(Name = "Q", EmitDefaultValue = false)]
        public int Quantity { get; set; }

        [DataMember(Name = "D", IsRequired = false, EmitDefaultValue = false)]
        public DateTime LastUpdateTime { get; set; }

        public static TicketStateValue Default => _default ?? (_default = new TicketStateValue());
        public bool Equals(TicketStateValue other)
        {
            if (other == null) return false;
            return other.StateName == StateName && other.State == State;
        }

        public override int GetHashCode()
        {
            return (StateName + "_" + StateValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as TicketStateValue;
            return item != null && Equals(item);
        }
    }

    [DataContract]
    public class TicketTagValue : IEquatable<TicketTagValue>
    {
        [DataMember(Name = "TN")] public string TagName { get; set; }

        [DataMember(Name = "TV")] public string TagValue { get; set; }

        [DataMember(Name = "TT")] public int TagType { get; set; }

        public string TagNameShort => string.Join("", TagName.Where(char.IsUpper));

        public bool Equals(TicketTagValue other)
        {
            if (other == null) return false;
            return other.TagName == TagName && other.TagValue == TagValue;
        }

        public override int GetHashCode()
        {
            return (TagName + "_" + TagValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as TicketTagValue;
            return item != null && Equals(item);
        }
    }

    [DataContract]
    public class TicketLogValue
    {
        public TicketLogValue()
        {
        }

        public TicketLogValue(string ticketNo, string userName)
        {
            DateTime = DateTime.Now;
            TicketNo = ticketNo;
            UserName = userName;
        }

        [DataMember(Name = "N")] public string TicketNo { get; set; }

        [DataMember(Name = "D")] public DateTime DateTime { get; set; }

        [DataMember(Name = "U")] public string UserName { get; set; }

        [DataMember(Name = "C")] public string Category { get; set; }

        [DataMember(Name = "L")] public string Log { get; set; }
    }
}

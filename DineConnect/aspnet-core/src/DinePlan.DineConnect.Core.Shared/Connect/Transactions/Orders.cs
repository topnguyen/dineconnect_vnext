﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Transactions
{
    [DataContract]
    public class OrderTagValue : IEquatable<OrderTagValue>
    {
        private static OrderTagValue _empty;
        private string _shortName;

        public OrderTagValue()
        {
            TagValue = "";
        }

        [DataMember(Name = "AP", EmitDefaultValue = false)]
        public bool AddTagPriceToOrderPrice { get; set; }

        public static OrderTagValue Empty
        {
            get
            {
                var orderTagValue = _empty;
                if (orderTagValue == null)
                {
                    var orderTagValue1 = new OrderTagValue
                    {
                        TagValue = "",
                        OrderKey = ""
                    };
                    orderTagValue = orderTagValue1;
                    _empty = orderTagValue;
                }

                return orderTagValue;
            }
        }

        [DataMember(Name = "FT", EmitDefaultValue = false)]
        public bool FreeTag { get; set; }

        [DataMember(Name = "MI", EmitDefaultValue = false)]
        public int MenuItemId { get; set; }

        [DataMember(Name = "OK")] public string OrderKey { get; set; }

        [DataMember(Name = "OI")] public int OrderTagGroupId { get; set; }

        [DataMember(Name = "PN", EmitDefaultValue = false)]
        public string PortionName { get; set; }

        [DataMember(Name = "PR", EmitDefaultValue = false)]
        public decimal Price { get; set; }

        [DataMember(Name = "Q", EmitDefaultValue = false)]
        public decimal Quantity { get; set; }

        public string ShortName
        {
            get
            {
                var str = _shortName;
                if (str == null)
                {
                    var num = ToShort(TagValue);
                    var str1 = num;
                    _shortName = num;
                    str = str1;
                }

                return str;
            }
        }

        [DataMember(Name = "TN")] public string TagName { get; set; }

        [DataMember(Name = "TO", EmitDefaultValue = false)]
        public string TagNote { get; set; }

        [DataMember(Name = "TV")] public string TagValue { get; set; }

        [DataMember(Name = "TF", EmitDefaultValue = false)]
        public bool TaxFree { get; set; }

        [DataMember(Name = "UI")] public int UserId { get; set; }

        [DataMember(Name = "GID")] public int GroupSync { get; set; }

        [DataMember(Name = "TID")] public int TagSync { get; set; }
        [DataMember(Name = "PSI")] public int PromotionSyncId { get; set; }

        public bool Equals(OrderTagValue other)
        {
            if (other == null) return false;
            if (other.TagName != TagName || !(other.TagValue == TagValue) || !(other.Price == Price) ||
                !(other.OrderKey == OrderKey)) return false;
            return other.Quantity == Quantity;
        }

        public override bool Equals(object obj)
        {
            var orderTagValue = obj as OrderTagValue;
            if (orderTagValue == null) return false;
            return Equals(orderTagValue);
        }

        public override int GetHashCode()
        {
            object[] tagName = { TagName, "_", TagValue, "_", Price, "_", OrderKey, "_", Quantity };
            return string.Concat(tagName).GetHashCode();
        }

        public int GetSortOrder()
        {
            int num;
            try
            {
                if (OrderKey.Length != 6)
                {
                    num = Convert.ToInt32(OrderKey);
                }
                else
                {
                    var str = OrderKey.Substring(3);
                    num = Convert.ToInt32(str);
                }
            }
            catch (Exception)
            {
                num = 0;
            }

            return num;
        }

        public bool Identifies(string tagExp)
        {
            if (!tagExp.Contains("=")) return TagName == tagExp;
            return string.Concat(TagName, "=", TagValue) == tagExp;
        }

        private string ToShort(string name)
        {
            if (string.IsNullOrEmpty(name)) return "";
            if (TagValue.Length < 3) return name;
            if (!name.Contains(" ")) return TagValue.Substring(0, 2);
            char[] chrArray = { ' ' };
            return string.Join("", name.Split(chrArray).Select(x =>
            {
                if (char.IsNumber(x.ElementAt(0))) return x;
                return x.ElementAt(0).ToString();
            }));
        }

        public void UpdatePrice(decimal orderTagPrice)
        {
            Price = orderTagPrice;
        }
    }

    [DataContract]
    public class TaxValue
    {
        [DataMember(Name = "TR")] public decimal TaxRate { get; set; }

        [DataMember(Name = "RN")] public int Rounding { get; set; }

        [DataMember(Name = "TN")] public string TaxTemplateName { get; set; }

        [DataMember(Name = "TT")] public int TaxTemplateId { get; set; }

        [DataMember(Name = "AT")] public int TaxTempleteAccountTransactionTypeId { get; set; }

        public decimal GetTax(bool taxIncluded, decimal price, decimal totalRate)
        {
            decimal result;
            if (taxIncluded && totalRate > 0)
            {
                if (Rounding > 0)
                    result = decimal.Round(price * TaxRate / (100 + totalRate), Rounding,
                        MidpointRounding.AwayFromZero);
                else
                    result = price * TaxRate / (100 + totalRate);
            }
            else if (TaxRate > 0)
            {
                result = price * TaxRate / 100;
            }
            else
            {
                result = 0;
            }

            return result;
        }

        public decimal GetTaxAmount(bool taxIncluded, decimal price, decimal totalRate, decimal plainSum,
            decimal preTaxServices)
        {
            if (preTaxServices != 0 && plainSum > 0)
                price += (price * preTaxServices) / plainSum;

            var result = GetTax(taxIncluded, price, totalRate);
            return result;
        }
    }

    [DataContract]
    public class OrderStateValue : IEquatable<OrderStateValue>
    {
        private static OrderStateValue _default;

        public OrderStateValue()
        {
            LastUpdateTime = DateTime.Now;
        }

        [DataMember(Name = "SN")]
        public string StateName { get; set; }

        [DataMember(Name = "S")]
        public string State { get; set; }

        [DataMember(Name = "SV")]
        public string StateValue { get; set; }

        [DataMember(Name = "OK", EmitDefaultValue = false)]
        public string OrderKey { get; set; }

        [DataMember(Name = "D", IsRequired = false, EmitDefaultValue = false)]
        public DateTime LastUpdateTime { get; set; }

        [DataMember(Name = "U", IsRequired = false, EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(Name = "UN", IsRequired = false, EmitDefaultValue = false)]
        public string UserName { get; set; }

        public bool Equals(OrderStateValue other)
        {
            if (other == null) return false;
            return other.State == State && other.StateName == StateName && other.StateValue == StateValue;
        }

        public override int GetHashCode()
        {
            return (StateName + "_" + StateValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as OrderStateValue;
            return item != null && Equals(item);
        }
    }
}
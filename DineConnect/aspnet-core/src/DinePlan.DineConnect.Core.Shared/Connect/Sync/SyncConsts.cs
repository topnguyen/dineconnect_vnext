﻿namespace DinePlan.DineConnect.Connect.Sync
{
	public class SyncConsts
	{
		public static string USER = "USER";
		public static string TAX = "TAX";
		public static string MENU = "MENU";
		public static string SCREENMENU = "SCREENMENU";
		public static string PRICETAG = "PRICETAG";
		public static string DEPARTMENT = "DEPARTMENT";
		public static string ORDERTAG = "ORDERTAG";
		public static string PAYMENTTYPE = "PAYMENTTYPE";
		public static string TRANSACTIONTYPE = "TRANSACTIONTYPE";
		public static string PROMOTION = "PROMOTION";
		public static string TABLE = "TABLE";
		public static string TILLACCOUNT = "TILLACCOUNT";
		public static string TICKETTAG = "TICKETTAG";
		public static string CALCULATION = "CALCULATION";
		public static string LOCATION = "LOCATION";
		public static string FORIEGNCURRENCY = "FORIEGNCURRENCY";
		public static string REASON = "REASON";
		public static string TICKMESSAGE = "TICKMESSAGE";
		public static string TERMINAL = "TERMINAL";
		public static string PRINTER = "PRINTER";
		public static string PRINTERTEMPLATE = "PRINTERTEMPLATE";
		public static string PRINTERJOB = "PRINTERJOB";
		public static string PRINTERTEMPLATECONDITION = "PRINTERTEMPLATECONDITION";
		public static string PROGRAMSETTINGS = "PROGRAMSETTINGS";
		public static string LOCALIZATION = "LOCALIZATION";
		public static string TICKETTYPE = "TICKETTYPE";
	}
}
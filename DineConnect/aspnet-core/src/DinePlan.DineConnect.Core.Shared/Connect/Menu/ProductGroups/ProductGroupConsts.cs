﻿namespace DinePlan.DineConnect.Connect.Menu.ProductGroups
{
    public class ProductGroupConsts
    {

		public const int MinCodeLength = 0;
		public const int MaxCodeLength = 50;
						
		public const int MinNameLength = 0;
		public const int MaxNameLength = 255;
						
    }
}
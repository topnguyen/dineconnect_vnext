﻿namespace DinePlan.DineConnect.Connect.Master.Numberators
{
    public enum ResetNumeratorType
    {
        Daily = 0,
        Monthly = 1,
        Yearly = 2
    }
}

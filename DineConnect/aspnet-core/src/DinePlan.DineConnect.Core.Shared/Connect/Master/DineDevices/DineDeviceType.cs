﻿namespace DinePlan.DineConnect.Connect.Master.DineDevices
{
    public enum DineDeviceType
    {
        DineChef = 0,
        DineQueue = 1
    }
}

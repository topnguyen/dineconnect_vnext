﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Connect.Tag.TicketTags
{
    public enum TicketTagGroupType
    {
        Alphanumeric = 0,
        Integer = 1,
        Decimal = 2
    }
}

﻿namespace DinePlan.DineConnect.Features
{
    public static class AppFeatures
    {
        public const string MaxUserCount = "App.MaxUserCount";
        public const string ChatFeature = "App.ChatFeature";
        public const string TenantToTenantChatFeature = "App.ChatFeature.TenantToTenant";
        public const string TenantToHostChatFeature = "App.ChatFeature.TenantToHost";
  
        public const string Connect = "DinePlan.DineConnect.Connect";
        public const string ConnectLocationCount = "DinePlan.DineConnect.Connect.LocationCount";
        public const string ConnectCountry = "DinePlan.DineConnect.Connect.Country";
        public const string ConnectCurrency = "DinePlan.DineConnect.Connect.Currency";

        public const string Tiffins = "DinePlan.DineConnect.Tiffins";
        public const string Go = "DinePlan.DineConnect.Go";
        public const string Wheel = "DinePlan.DineConnect.Wheel";
        public const string Track = "DinePlan.DineConnect.Track";
        public const string Clique = "DinePlan.DineConnect.Clique";
        public const string Touch = "DinePlan.DineConnect.Touch";

    }
}
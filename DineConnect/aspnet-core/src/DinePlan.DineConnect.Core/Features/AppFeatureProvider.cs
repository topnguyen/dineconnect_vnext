﻿using Abp.Application.Features;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.UI.Inputs;

namespace DinePlan.DineConnect.Features
{
    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            var chatFeature =
                context.Create(
                    AppFeatures.ChatFeature,
                    defaultValue: "true",
                    displayName: L("ChatFeature"),
                    inputType: new CheckboxInputType()
                );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToTenantChatFeature,
                defaultValue: "true",
                displayName: L("TenantToTenantChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToHostChatFeature,
                defaultValue: "true",
                displayName: L("TenantToHostChatFeature"),
                inputType: new CheckboxInputType()
            );

            context.Create(
                AppFeatures.MaxUserCount,
                defaultValue: "0", //0 = unlimited
                displayName: L("MaximumUserCount"),
                description: L("MaximumUserCount_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                ValueTextNormalizer = value => value == "0" ? L("Unlimited") : new FixedLocalizableString(value),
                IsVisibleOnPricingTable = true
            };
            /*
            #region ######## Example Features - You can delete them #########

            context.Create("TestTenantScopeFeature", "false", L("TestTenantScopeFeature"), scope: FeatureScopes.Tenant);
            context.Create("TestEditionScopeFeature", "false", L("TestEditionScopeFeature"), scope: FeatureScopes.Edition);

            context.Create(
                AppFeatures.TestCheckFeature,
                defaultValue: "false",
                displayName: L("TestCheckFeature"),
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };

            context.Create(
                AppFeatures.TestCheckFeature2,
                defaultValue: "true",
                displayName: L("TestCheckFeature2"),
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };

            #endregion
            */

            var connectFeature = context.Create(
                AppFeatures.Connect,
                defaultValue: "false",
                displayName: L("Connect"),
                inputType: new CheckboxInputType()
            );

            connectFeature.CreateChildFeature(
                AppFeatures.ConnectLocationCount,
                defaultValue: "0",
                displayName: L("LocationCount"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            );

            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCountry,
                defaultValue: "",
                displayName: L("Country"),
                inputType: new ComboboxInputType(
                    new StaticLocalizableComboboxItemSource(
                        new LocalizableComboboxItem("SG", L("Singapore")),
                        new LocalizableComboboxItem("IN", L("India")),
                        new LocalizableComboboxItem("MR", L("Myanmar")),
                        new LocalizableComboboxItem("MY", L("Malaysia")),
                        new LocalizableComboboxItem("KSA", L("KSA")),
                        new LocalizableComboboxItem("TH", L("Thailand"))
                    )
                )
            );
            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCurrency, "", L("Currency"),
                inputType: new SingleLineStringInputType(new StringValueValidator(1, 3))
            );

          

            var tiffinFeature = context.Create(
                AppFeatures.Tiffins,
                defaultValue: "false",
                displayName: L("Tiffins"),
                inputType: new CheckboxInputType()
            );


            var goFeature = context.Create(
                AppFeatures.Go,
                defaultValue: "false",
                displayName: L("Go"),
                inputType: new CheckboxInputType()
            );

            var trackFeature = context.Create(
                AppFeatures.Track,
                defaultValue: "false",
                displayName: L("Track"),
                inputType: new CheckboxInputType()
            );

            var wheelFeature = context.Create(
                AppFeatures.Wheel,
                defaultValue: "false",
                displayName: L("Wheel"),
                inputType: new CheckboxInputType()
            );

            var cliqueFeature = context.Create(
                AppFeatures.Clique,
                defaultValue: "false",
                displayName: L("Clique"),
                inputType: new CheckboxInputType()
            );

            var touchFeature = context.Create(
                AppFeatures.Touch,
                defaultValue: "false",
                displayName: L("Touch"),
                inputType: new CheckboxInputType()
            );
           
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DineConnectConsts.LocalizationSourceName);
        }
    }
}

using System.Threading.Tasks;

namespace DinePlan.DineConnect.Net.Sms
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}
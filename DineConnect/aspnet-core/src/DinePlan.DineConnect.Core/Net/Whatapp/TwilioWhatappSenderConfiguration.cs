﻿using Abp.Dependency;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Net.Whatapp
{
    public class TwilioWhatappSenderConfiguration : ITransientDependency
    {
        public readonly string AccountSid;

        public readonly string AuthToken;
        
        public readonly string SenderNumber;

        public TwilioWhatappSenderConfiguration(ITenantSettingsAppService settingsAppService, IAppConfigurationAccessor configurationAccessor)
        {
            var allSettingsTask = settingsAppService.GetAllSettings();
            allSettingsTask.Wait();
            var allSettings = allSettingsTask.Result;
            if (allSettings == null)
            {
                AccountSid = configurationAccessor.Configuration["Twilio:AccountSid"];
                AuthToken = configurationAccessor.Configuration["Twilio:AuthToken"];
                SenderNumber = configurationAccessor.Configuration["Twilio:SenderNumber"];
            }
            else
            {
                AccountSid = allSettings?.General?.WhatappAccountSid;
                AuthToken = allSettings?.General?.WhatappAuthToken;
                SenderNumber = allSettings?.General?.WhatappSenderNumber;   
            }
        }
    }
}

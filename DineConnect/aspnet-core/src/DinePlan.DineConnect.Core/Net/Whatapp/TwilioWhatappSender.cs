﻿using System;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Extensions;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace DinePlan.DineConnect.Net.Whatapp
{
    public class TwilioWhatappSender : IWhatappSender, ITransientDependency
    {
        private readonly TwilioWhatappSenderConfiguration _twilioWhatappSenderConfiguration;
        
        public TwilioWhatappSender(TwilioWhatappSenderConfiguration twilioWhatappSenderConfiguration)
        {
            _twilioWhatappSenderConfiguration = twilioWhatappSenderConfiguration;
        }

        public async Task SendAsync(string number, string message)
        {
            // Check Content
            if (number.IsNullOrWhiteSpace() || message.IsNullOrWhiteSpace())
            {
                Console.WriteLine("Whatapp does not send by number or message is empty");
                return;
            }
            number = !number.StartsWith("+") ? "+" + number : number;
            
            // Check Config
            if (_twilioWhatappSenderConfiguration.AccountSid.IsNullOrWhiteSpace()
                || _twilioWhatappSenderConfiguration.AuthToken.IsNullOrWhiteSpace()
                || _twilioWhatappSenderConfiguration.SenderNumber.IsNullOrWhiteSpace())
            {
                Console.WriteLine("Whatapp does not send by twilio config missing (sid, auth token, sender is missing)");
                return;
            }
            
            TwilioClient.Init(_twilioWhatappSenderConfiguration.AccountSid, _twilioWhatappSenderConfiguration.AuthToken);

            var messageOptions = new CreateMessageOptions(new PhoneNumber($"whatsapp:{number}")); 
            messageOptions.From = new PhoneNumber($"whatsapp:{_twilioWhatappSenderConfiguration.SenderNumber}");    
            messageOptions.Body = message;

            try
            {
                var result = await MessageResource.CreateAsync(messageOptions); 
            
                Console.WriteLine("Whatapp message send via twilio successfully");
                Console.WriteLine(result.Body);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}

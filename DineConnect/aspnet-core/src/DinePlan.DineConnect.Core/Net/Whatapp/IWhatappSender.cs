using System.Threading.Tasks;

namespace DinePlan.DineConnect.Net.Whatapp
{
    public interface IWhatappSender
    {
        Task SendAsync(string number, string message);
    }
}
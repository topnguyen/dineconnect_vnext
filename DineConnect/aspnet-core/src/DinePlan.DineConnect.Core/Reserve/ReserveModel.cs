﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;

namespace DinePlan.DineConnect.Reserve
{

    [Table("ReserveTables")]
    public class ReserveTable : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int Minimum { get; set; }
        public int? ReserveSeatMapId { get; set; }
        [ForeignKey(nameof(ReserveSeatMapId))]
        public virtual ReserveSeatMap ReserveSeatMap { get; set; }
    }

    [Table("ReserveSeatMaps")]
    public class ReserveSeatMap : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public double PositionX { get; set; }
        public double PositionZ { get; set; }
        public ICollection<ReserveTable> ReserveTables { get; set; }
    }

    [Table("Reservations")]
    public class Reservation : ConnectFullMultiTenantAuditEntity
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public double? DepositFee { get; set; }
        public string PaymentDetails { get; set; }
        public bool? DepositPay { get; set; }
        public int? TotalPeopleCount { get; set; }
        public string ReserveTableIds { get; set; }
        public int? CustomerId { get; set; }
        [ForeignKey(nameof(CustomerId))]
        public Customer Customer { get; set; }
    }

    [Table("ReserveSeatMapBackgroundUrls")]
    public class ReserveSeatMapBackgroundUrl : ConnectFullMultiTenantAuditEntity
    {
        public string Url { get; set; }
    }
}

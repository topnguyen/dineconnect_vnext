﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentClientAddress : FullAuditedEntity<long>, IMustHaveTenant
    {
        public int ClientId { get; set; }

        [ForeignKey("ClientId")]
        public ShipmentClient Client { get; set; }

        public int AddressId { get; set; }

        [ForeignKey("AddressId")]
        public ShipmentAddress Address { get; set; }

        public int TenantId { get; set; }
    }
}

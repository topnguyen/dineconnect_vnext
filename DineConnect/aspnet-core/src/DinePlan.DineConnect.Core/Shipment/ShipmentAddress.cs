﻿using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentAddress : ConnectFullMultiTenantAuditEntity
    {
        public string Address { get; set; }
        public string Street { get; set; }
        public string LineAddress2 { get; set; }
        public string LineAddress3 { get; set; }
        public string LineAddress4 { get; set; }
        public string Name { get; set; }
        public string ExtCode { get; set; }
        public string Zone { get; set; }
        public string GeoStatus { get; set; }
        public string GPSLat { get; set; }
        public string GPSLon { get; set; }
        public string GPSLatR { get; set; }
        public string GPSLonR { get; set; }
        public string GPS { get; set; }
        public int? ServiceTime { get; set; }
        public string Clients { get; set; }
        public bool HasAdvancedRouting { get; set; }
        public int? Priority { get; set; }
        public string DeliveryTimeFrom { get; set; }
        public string DeliveryTimeTo { get; set; }
    }
}

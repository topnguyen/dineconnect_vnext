﻿using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentDriver : ConnectFullMultiTenantAuditEntity
    {
        public string Driver { get; set; }

        public string Vehicle { get; set; }

        public string Phone { get; set; }

        public string CompanyId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string HomeAddress { get; set; }

        public int? TrackDepotId { get; set; }

        public ShipmentDepot TrackDepot { get; set; }

        public string Zone { get; set; }

        public bool Active { get; set; }
    }
}

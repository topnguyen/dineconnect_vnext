﻿namespace DinePlan.DineConnect.Shipment
{
    public enum ShipmentType
    {
        Delivery = 0 ,
        Collection = 1,
        PickupDelivery = 2,
        PickupHubDelivery = 3
    }
}

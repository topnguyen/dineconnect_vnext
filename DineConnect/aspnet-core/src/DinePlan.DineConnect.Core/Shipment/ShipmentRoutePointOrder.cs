﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentRoutePointOrder : AuditedEntity<long>, IMustHaveTenant
    {
        public int RoutePointId { get; set; }

        [ForeignKey("RoutePointId")]
        public ShipmentRoutePoint RoutePoint { get; set; }

        public int OrderId { get; set; }

        [ForeignKey("OrderId")]
        public ShipmentOrder Order { get; set; }

        public int TenantId { get; set; }

        public Guid TrackKey { get; set; }
    }
}

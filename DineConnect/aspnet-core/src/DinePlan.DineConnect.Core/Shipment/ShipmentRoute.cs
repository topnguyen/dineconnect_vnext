﻿using DinePlan.DineConnect.BaseCore;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentRoute : ConnectFullMultiTenantAuditEntity
    {
        public string Code { get; set; }

        public DateTime Date { get; set; }

        public int? ShipFromId { get; set; }

        public ShipmentDepot ShipFrom { get; set; }

        public int? DriverId { get; set; }

        public ShipmentDriver Driver { get; set; }

        public bool IsStartFromDepot { get; set; }

        public bool IsEndAtDepot { get; set; }

        public bool Status { get; set; }

        public virtual List<ShipmentRoutePoint> RoutePoints { get; set; }

        public ShipmentRoute()
        {
            RoutePoints = new List<ShipmentRoutePoint>();
        }
    }
}

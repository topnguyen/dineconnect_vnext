﻿using DinePlan.DineConnect.BaseCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentOrder : ConnectFullMultiTenantAuditEntity
    {
        public string Type { get; set; }

        public string Number { get; set; }

        public DateTime Date { get; set; }

        public string Note { get; set; }

        public string Client { get; set; }

        public int? AddressId { get; set; }

        public string Address { get; set; }

        public string GeoStatus { get; set; }

        public string Warehouse { get; set; }

        public string Customer { get; set; }

        public string Zone { get; set; }

        public string DeliveryTimeFrom { get; set; }

        public string DeliveryTimeTo { get; set; }

        public decimal? Weight { get; set; }

        public decimal? Size { get; set; }

        public decimal? Volume { get; set; }

        public decimal? Pallets { get; set; }

        public string ContactName { get; set; }

        public string Tel { get; set; }

        public string Email { get; set; }

        public int COD { get; set; }

        public double? GPSLat { get; set; }

        public double? GPSLon { get; set; }

        public int IsLoading { get; set; }

        public bool IsPD { get; set; }

        public int CreateSource { get; set; }

        public string Barcode { get; set; }

        public virtual List<ShipmentItem> Items { get; set; }

        public Guid? TrackKey { get; set; }
        
        public int? ShipmentDepotId { get; set; }
        
        public ShipmentDepot ShipmentDepot { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipment
{
    public  enum RoutePointType
    {
        Delivery = 0,
        DepartFromDepot = 5,
        BackToDepot = 9
    }
}

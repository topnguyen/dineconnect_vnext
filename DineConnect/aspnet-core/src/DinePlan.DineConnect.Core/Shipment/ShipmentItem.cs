﻿using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentItem : ConnectFullMultiTenantAuditEntity
    {
        public int OrderId { get; set; }

        public int? TrackSiteId { get; set; }

        public decimal? Amount { get; set; }

        public decimal? ActualQuantity { get; set; }

        public decimal? ExtraQuantity { get; set; }

        public decimal? RejectedQuantity { get; set; }

        public decimal? Cost { get; set; }

        public string Note { get; set; }

        public string ItemExtCode { get; set; }

        public bool HasPhoto { get; set; }

        public string RejectReason { get; set; }

        public int OrderNum { get; set; }

        public string ItemBarcode { get; set; }

        public string Name { get; set; }

        public string Metrik { get; set; }

        public string ExtCode { get; set; }

        public string Barcode { get; set; }

        public bool DeliveryScan { get; set; }

        [ForeignKey("OrderId")]
        public ShipmentOrder Order { get; set; }
    }
}

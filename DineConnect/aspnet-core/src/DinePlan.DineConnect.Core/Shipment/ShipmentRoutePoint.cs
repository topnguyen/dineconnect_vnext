﻿using DinePlan.DineConnect.BaseCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentRoutePoint : ConnectFullMultiTenantAuditEntity
    {
        public int RouteId { get; set; }

        [ForeignKey("RouteId")]
        public ShipmentRoute Route { get; set; }

        public int Number { get; set; }

        public string Address { get; set; }

        public string State { get; set; }

        public string GeoStatus { get; set; }

        public DateTime? StateDate { get; set; }

        public bool HasPhoto { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public DateTime? DepartureDate { get; set; }

        public decimal? PlanDistance { get; set; }

        public DateTime? PlanArrivalDate { get; set; }

        public DateTime? PlanDepartureDate { get; set; }

        public bool Lock { get; set; }

        public int? Priority { get; set; }

        public int Type { get; set; }

        public bool EtaOutOfTime { get; set; }

        public DateTime? EtaEnRoute { get; set; }

        public virtual List<ShipmentRoutePointOrder> Orders { get; set; }

        public ShipmentRoutePoint()
        {
            Orders = new List<ShipmentRoutePointOrder>();
        }
    }
}



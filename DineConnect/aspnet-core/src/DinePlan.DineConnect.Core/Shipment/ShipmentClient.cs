﻿using DinePlan.DineConnect.BaseCore;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Shipment
{
    public class ShipmentClient : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public string ExtCode { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string ContactName { get; set; }

        public int? Priority { get; set; }

        public string DeliveryTimeFrom { get; set; }

        public string DeliveryTimeTo { get; set; }

        public bool HasAdvancedRouting { get; set; }

        public virtual List<ShipmentClientAddress> Addresses { get; set; }
    }
}

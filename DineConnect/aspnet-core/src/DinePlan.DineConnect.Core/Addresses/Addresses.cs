﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Addresses
{
    [Table("Addresses")]
    public class Address : ConnectFullMultiTenantAuditEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public virtual City City { get; set; }

        [Required]
        public int CityId { get; set; }

        public string PostalCode { get; set; }
    }

    [Table("Cities")]
    public class City : ConnectFullMultiTenantAuditEntity
    {
        public ICollection<Address> Addresses { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public State State { get; set; }
        public int? StateId { get; set; }
    }

    [Table("Countries")]
    public class Country : ConnectFullMultiTenantAuditEntity
    {
        public ICollection<City> Cities { get; set; }
        public string Name { get; set; }
        public ICollection<State> States { get; set; }
        public string TwoLetterCode { get; set; }
        public int CallingCode { get; set; }
    }

    [Table("States")]
    public class State : ConnectFullMultiTenantAuditEntity
    {
        public ICollection<City> Cities { get; set; }
        public Country Country { get; set; }

        [Required]
        public int CountryId { get; set; }

        public string Name { get; set; }
    }
}
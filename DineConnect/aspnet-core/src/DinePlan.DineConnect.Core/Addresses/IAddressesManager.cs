﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Services;

namespace DinePlan.DineConnect.Addresses
{
    public interface IAddressesManager : IDomainService
    {
        Task<int> CreateorUpdateAddressAsync(Address address);

        Task<int> CreateorUpdateCityAsync(City city);

        Task<int> CreateorUpdateCountryAsync(Country country);

        Task<int> CreateorUpdateStateAsync(State state);

        Task DeleteCityAsync(int cityId);

        Task DeleteCountryAsync(int countryId);

        Task DeleteStateAsync(int stateId);

        Task<Address> GetAddressAsync(int id);

        IQueryable<Address> GetAddressList(string filter);

        Task<City> GetCityAsync(int id);

        IQueryable<City> GetCityList(string filter, int countryId, int stateId);

        Task<Country> GetCountryAsync(int id);

        IQueryable<Country> GetCountryList(string filter);

        IQueryable<State> GetStateList(string filter, int? countryId);

        Task<List<State>> GetStateListByCountry(int id);

        Task<State> GetStateAsync(int id);
    }
}
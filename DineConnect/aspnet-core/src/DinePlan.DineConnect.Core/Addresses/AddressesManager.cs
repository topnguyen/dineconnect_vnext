﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Addresses
{
    public class AddressesManager : DomainService, IAddressesManager
    {
        private readonly IRepository<Address> _addressRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<State> _stateRepository;

        public AddressesManager(IRepository<Country> countryRepository, IRepository<State> stateRepository, IRepository<City> cityRepository, IRepository<Address> addressRepository)
        {
            _countryRepository = countryRepository;
            _stateRepository = stateRepository;
            _cityRepository = cityRepository;
            _addressRepository = addressRepository;
        }

        public async Task<int> CreateorUpdateAddressAsync(Address address)
        {
            var addressId = await _addressRepository.InsertOrUpdateAndGetIdAsync(address);
            await CurrentUnitOfWork.SaveChangesAsync();
            return addressId;
        }

        public async Task<int> CreateorUpdateCityAsync(City city)
        {
            return await _cityRepository.InsertOrUpdateAndGetIdAsync(city);
        }

        public async Task<int> CreateorUpdateCountryAsync(Country country)
        {
            return await _countryRepository.InsertOrUpdateAndGetIdAsync(country);
        }

        public async Task<int> CreateorUpdateStateAsync(State state)
        {
            return await _stateRepository.InsertOrUpdateAndGetIdAsync(state);
        }

        public async Task DeleteCityAsync(int cityId)
        {
            var addressAssocaitedWithCity = (await _addressRepository.GetAllListAsync(c => c.CityId == cityId)).Count;
            if (addressAssocaitedWithCity > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }
            await _cityRepository.DeleteAsync(cityId);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteCountryAsync(int countryId)
        {
            var stateAssociatedWithCountry = (await _stateRepository.GetAllListAsync(c => c.CountryId == countryId)).Count;
            var cityAssocaitedWithCountry = (await _cityRepository.GetAllListAsync(c => c.CountryId == countryId)).Count;
            if (stateAssociatedWithCountry > 0 || cityAssocaitedWithCountry > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }
            await _countryRepository.DeleteAsync(countryId);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteStateAsync(int stateId)
        {
            var cityAssocaitedWithState = (await _stateRepository.GetAllListAsync(c => c.CountryId == stateId)).Count;
            if (cityAssocaitedWithState > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            await _stateRepository.DeleteAsync(stateId);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<Address> GetAddressAsync(int id)
        {
            return await _addressRepository.GetAsync(id);
        }

        public IQueryable<Address> GetAddressList(string filter)
        {
            return _addressRepository.GetAllIncluding(a => a.City)
            .WhereIf(!string.IsNullOrWhiteSpace(filter), c =>
            EF.Functions.Like(c.Address1, filter.ToILikeString()) ||
            EF.Functions.Like(c.Address2, filter.ToILikeString()) ||
            EF.Functions.Like(c.Address3, filter.ToILikeString()));
        }

        public async Task<City> GetCityAsync(int id)
        {
            return await _cityRepository.GetAllIncluding(c => c.State).Where(c => c.Id == id).FirstOrDefaultAsync();
        }

        public IQueryable<City> GetCityList(string filter, int countryId, int stateId)
        {
            var query = _cityRepository.GetAll()
                    .WhereIf(stateId > 0, c => c.StateId == stateId)
                    .WhereIf(!filter.IsNullOrEmpty(), c => EF.Functions.Like(c.Name, filter.ToILikeString()));

            if (stateId <= 0)
            {
                query = query.WhereIf(countryId > 0, c => c.CountryId == countryId);
            }

            return query;
        }

        public async Task<Country> GetCountryAsync(int id)
        {
            return await _countryRepository.GetAsync(id);
        }

        public IQueryable<Country> GetCountryList(string filter)
        {
            return _countryRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(filter), c => EF.Functions.Like(c.Name, filter.ToILikeString()));
        }

        public IQueryable<State> GetStateList(string filter, int? countryId)
        {
            return _stateRepository.GetAll()
                .Include(c => c.Country)
                .WhereIf(!string.IsNullOrWhiteSpace(filter), c => EF.Functions.Like(c.Name, filter.ToILikeString()))
                .WhereIf(countryId.HasValue && countryId.Value > 0, c => c.CountryId == countryId);
        }

        public async Task<List<State>> GetStateListByCountry(int id)
        {
            var query = _stateRepository.GetAll();

            if (id > 0)
            {
                query = query.Where(c => c.CountryId == id);
            }

            var result = await query.ToListAsync();

            return result;
        }

        public async Task<State> GetStateAsync(int id)
        {
            return await _stateRepository.GetAsync(id);
        }
    }
}
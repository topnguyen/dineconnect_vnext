﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Authorization.Users;

namespace DinePlan.DineConnect.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}

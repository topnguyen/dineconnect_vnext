﻿using Abp.Dependency;
using Abp.Runtime.Caching;
using DinePlan.DineConnect.AppSetting;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cache
{
    public class RedisCacheService : IRedisCacheService, ITransientDependency
    {
        private readonly ICacheManager _cacheManager;
        private ICache _cache;
        private readonly IOptions<EnvironmentSetting> _environmentSetting;

        public RedisCacheService(ICacheManager cacheManager, IOptions<EnvironmentSetting> environmentSetting)
        {
            _cacheManager = cacheManager;
            _cache = GetCache();
            _environmentSetting = environmentSetting;
        }

        public async Task<TModel> GetAsync<TModel>(string key)
        {
            //if (_environmentSetting.Value.EnvironmentName == DineConnectConsts.EnvironmentStaging)
                return await _cache.GetOrDefaultAsync<string, TModel>(key);

            //return default;
        }

        public async Task RemoveAsync(string key)
        {
            //if (_environmentSetting.Value.EnvironmentName == DineConnectConsts.EnvironmentStaging)
                await _cache.RemoveAsync(key);
        }

        public async Task RemoveAsync(IEnumerable<string> keys)
        {
            //if (_environmentSetting.Value.EnvironmentName == DineConnectConsts.EnvironmentStaging)
            {
                keys ??= new List<string>();
                foreach (var item in keys)
                {
                    await _cache.RemoveAsync(item);
                }
            }
        }

        public async Task RemoveAsync(params string[] keys)
        {
            //if (_environmentSetting.Value.EnvironmentName == DineConnectConsts.EnvironmentStaging)
            {
                foreach (var item in keys)
                {
                    await _cache.RemoveAsync(item);
                }
            }    
        }
        

        public async Task SetAsync<TModel>(string key, TModel model, int? expriresInMinutes = null)
        {
            //if (_environmentSetting.Value.EnvironmentName == DineConnectConsts.EnvironmentStaging)
            {
                int exprires = expriresInMinutes ?? 15;

                if (model != null)
                    await _cache.SetAsync(key, model, TimeSpan.FromMinutes(exprires));

            }
        }

        private ICache GetCache()
        {
            return _cacheManager.GetCache("MyCache");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cache
{
    public interface IRedisCacheService
    {
        Task<TModel> GetAsync<TModel>(string key);
        Task RemoveAsync(string key);
        Task RemoveAsync(IEnumerable<string> keys);
        Task RemoveAsync(params string[] keys);
        Task SetAsync<TModel>(string key, TModel model, int? expriresInMinutes = null);
    }
}

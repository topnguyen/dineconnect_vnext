﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping
{
    public class ExtraPlaceShippingOrderInfo
    {
        public  string ServiceType { get; set; }

        public  DateTime? ManualScheduleAt { get; set; }
    }
}

﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.UI;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Shipping.ShippingProviders;
using DinePlan.DineConnect.Wheel;
using Newtonsoft.Json.Linq;
using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Threading;
using DinePlan.DineConnect.Timing;
using DinePlan.DineConnect.Wheel.V2;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Shipping
{
    public class ShippingService : DineConnectDomainServiceBase, IShippingService
    {
        private readonly IIocResolver _iocResolver;
        private IRepository<WheelLocation> _locationRepository;
        private IRepository<WheelDepartment> _departmentnRepository;
        private IRepository<WheelTicketShippingOrder> _wheelTicketShippingOrderRepository;

        private IAbpSession _abpSession;
        private readonly ITimeZoneService _timeZoneService;

        public ShippingService(IIocResolver iocResolver, IRepository<WheelLocation> locationRepository, IRepository<WheelDepartment> departmentnRepository, IAbpSession abpSession, IRepository<WheelTicketShippingOrder> wheelTicketShippingOrderRepository, ITimeZoneService timeZoneService)
        {
            _iocResolver = iocResolver;
            _locationRepository = locationRepository;
            _departmentnRepository = departmentnRepository;
            _abpSession = abpSession;
            _wheelTicketShippingOrderRepository = wheelTicketShippingOrderRepository;
            _timeZoneService = timeZoneService;
        }

        public void CalculateScheduleAt(ProcessShippingOrderRequest request, string deliveryTime)
        {
            var timeZone = GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

            if (request.ManualScheduleAt.HasValue)
            {
                var utc = TimeZoneInfo.ConvertTimeToUtc(request.ManualScheduleAt.Value, timezoneInfo);

                request.ScheduleAtUtc = utc;
                request.ScheduleAt = utc.ToString("yyyy-MM-ddTHH:mm:ssZ");
                request.ScheduleTime = utc.Ticks;
                return;
            }

            var scheduleAtUtc = Clock.Now.ToUniversalTime();

            dynamic itemDynamic = JObject.Parse(request.Department.Dynamic);

            var totalPrepareTime = (double)itemDynamic.OrderDeliveryTime + (double)itemDynamic.OrderPreparationTime;
            var timeArr = deliveryTime.Split(" ");

            if (deliveryTime == "ASAP") //case  ASAP
            {
                scheduleAtUtc = Clock.Now.ToUniversalTime();
            }
            else if (timeArr[1].Split('-').Count() == 1) //case  2021-06-23 15:00
            {
                var deliveryDateTime = DateTime.Parse(deliveryTime);
                scheduleAtUtc = SettingScheduleAtPrepareByDate(deliveryDateTime, totalPrepareTime);
            }
            else // CASE 2021-06-23 15:00-16:00
            {
                var time = timeArr[1].Split('-');
                var deliveryDateTime = DateTime.Parse($"{timeArr[0].Trim()} {time[0].Trim()}");

                scheduleAtUtc = SettingScheduleAtPrepareByDate(deliveryDateTime, totalPrepareTime);
            }

            request.ScheduleAtUtc = scheduleAtUtc;
            request.ScheduleAt = scheduleAtUtc.ToString("yyyy-MM-ddTHH:mm:ssZ"); //scheduleAt.ToString("o");
            request.ScheduleTime = scheduleAtUtc.Ticks;
        }

        private DateTime SettingScheduleAtPrepareByDate(DateTime deliveryDateTime, double totalPrepareTime)
        {
            DateTime scheduleAt;

            var timeZone = GetTimezone();
            var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
            var now = TimeZoneInfo.ConvertTimeFromUtc(Clock.Now.ToUniversalTime(), timezoneInfo);

            if (deliveryDateTime.Date == now.Date && deliveryDateTime <= now.AddMinutes(totalPrepareTime))
            {
                scheduleAt = Clock.Now.ToUniversalTime();
            }
            else
            {
                var schjeduleUtc = TimeZoneInfo.ConvertTimeToUtc(deliveryDateTime, timezoneInfo);
                scheduleAt = schjeduleUtc.AddMinutes(-totalPrepareTime);
            }

            return scheduleAt;
        }

        public async Task<ProcessShippingOrderResult> PlaceShippingOrder(WheelTicket ticket, ExtraPlaceShippingOrderInfo extraInfo)
        {
            var output = new ProcessShippingOrderResult
            {
                CustomerOrderId = "",
            };

            if (ticket.WheelOrderType != WheelOrderType.Delivery)
            {
                throw new UserFriendlyException($"Cannot place shipping order for a {ticket.WheelOrderType} order");
            }

            var request = new ProcessShippingOrderRequest();

            if (extraInfo != null)
            {
                request.ServiceType = extraInfo.ServiceType;
                request.ManualScheduleAt = extraInfo.ManualScheduleAt;
            }

            request.RequestGuid = Guid.NewGuid();
            request.DeliveryGuid = Guid.NewGuid();

            request.BillingInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<WheelBillingInfo>(ticket.Payment.ExtraInformation);
            request.Location = await _locationRepository.FirstOrDefaultAsync(ticket.WheelLocationId.Value);
            request.Department = await _departmentnRepository.FirstOrDefaultAsync(Convert.ToInt32(request.BillingInfo.WheelDepartmentId.Value));

            this.CalculateScheduleAt(request, ticket.DeliveryTime);

            var shippingProvider = await this.ResolveShippingProvider();

            output = await shippingProvider.Provider.ProcessShippingOrder(request);

            if (output != null && !string.IsNullOrEmpty(output.CustomerOrderId))
            {
                var inputDto = new WheelTicketShippingOrder
                {
                    WheelTicketId = ticket.Id,
                    TenantId = _abpSession.GetTenantId(),
                    ShippingOrderId = output.CustomerOrderId,
                    ShippingOrderAmount = output.ShippingOrderAmount,
                    ShippingProviderName = output.ShippingProvider,
                    ShippingOrderStatus = output.ShippingOrderStatus
                };

                await _wheelTicketShippingOrderRepository.InsertAsync(inputDto);

                output.ShippingProvider = (Settings.ShippingProvider)shippingProvider.ProviderValue;
                output.RequestGuid = request.RequestGuid;
                output.DeliveryGuid = request.DeliveryGuid;

                return output;

            }

            throw new UserFriendlyException($"Cannot get shipping order quotation.");
        }

        public async Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(WheelTicket ticket)
        {
            var output = new GetShippingOrderQuotationResult
            {
            };

            if (ticket.WheelOrderType != WheelOrderType.Delivery)
            {
                throw new UserFriendlyException($"Cannot place shipping order for {ticket.WheelOrderType} order");
            }

            var request = new ProcessShippingOrderRequest();

            request.RequestGuid = Guid.NewGuid();
            request.DeliveryGuid = Guid.NewGuid();

            request.BillingInfo =
                Newtonsoft.Json.JsonConvert.DeserializeObject<WheelBillingInfo>(ticket.Payment.ExtraInformation);
            request.Location = await _locationRepository.FirstOrDefaultAsync(ticket.WheelLocationId.Value);
            request.Department =
                await _departmentnRepository.FirstOrDefaultAsync(
                    Convert.ToInt32(request.BillingInfo.WheelDepartmentId.Value));

            this.CalculateScheduleAt(request, ticket.DeliveryTime);

            var shippingProvider = await this.ResolveShippingProvider();

            output = await shippingProvider.Provider.GetShippingOrderQuotation(request);

            if (output != null)
            {
                output.ShippingProvider = (Settings.ShippingProvider)shippingProvider.ProviderValue;

                var timeZone = GetTimezone();
                var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);
                output.ScheduleAt = TimeZoneInfo.ConvertTimeFromUtc(request.ScheduleAtUtc, timezoneInfo).ToString("yyyy-MM-dd HH:mm");

                return output;
            }

            throw new UserFriendlyException($"Cannot get shipping order quotation.");
        }

        public async Task CancelShippingOrder(WheelTicket ticket)
        {
            var shippingProvider = await this.ResolveShippingProvider();

            var lastShippingOrder = _wheelTicketShippingOrderRepository
                .GetAll()
                .Where(t => t.WheelTicketId == ticket.Id)
                .OrderByDescending(t => t.Id)
                .FirstOrDefault();

            if (lastShippingOrder != null)
            {
                var request = new CancelShippingOrderRequest()
                {
                    OrderNo = lastShippingOrder.ShippingOrderId
                };

                var output = await shippingProvider.Provider.CancelShippingOrder(request);

                if (output.Canceled)
                {
                    await _wheelTicketShippingOrderRepository.DeleteAsync(lastShippingOrder);
                    return;
                }

                throw new UserFriendlyException($"Failed to cancel shipping order.", output.Message);
            }
        }

        private async Task<(IShippingProvider Provider, int ProviderValue)> ResolveShippingProvider()
        {
            var shippingProviderStr =
                await SettingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingProvider,
                    _abpSession.GetTenantId());

            if (int.TryParse(shippingProviderStr, out var shippingProvider))
            {
                IShippingProvider provider = null;

                switch ((Settings.ShippingProvider)shippingProvider)
                {
                    case Settings.ShippingProvider.LalaMove:
                        provider = _iocResolver.Resolve<LalaMoveShippingProvider>();
                        break;
                    case Settings.ShippingProvider.GoGoVan:
                        provider = _iocResolver.Resolve<GogoVanShippingProvider>();
                        break;
                    case Settings.ShippingProvider.Dunzo:
                        provider = _iocResolver.Resolve<DunzoShippingProvider>();
                        break;
                    default:
                        break;
                }

                return (provider, shippingProvider);
            }

            throw new UserFriendlyException($"Can not resolve shipping provider. Please check that you have configured shipping provider");
        }

        private string GetTimezone()
        {
            var timezone = string.Empty;
            if (Clock.SupportsMultipleTimezone)
                timezone = SettingManager.GetSettingValueForTenant(TimingSettingNames.TimeZone,
                    _abpSession.GetTenantId());

            var defaultTimeZoneId =
              AsyncHelper.RunSync<string>(() => _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, _abpSession.GetTenantId()));

            return timezone ?? defaultTimeZoneId;
        }
    }
}

﻿using DinePlan.DineConnect.Wheel;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipping
{
    public interface IShippingService
    {
        Task<ProcessShippingOrderResult> PlaceShippingOrder(WheelTicket ticket, ExtraPlaceShippingOrderInfo extraInfo);

        Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(WheelTicket ticket);

        Task CancelShippingOrder(WheelTicket ticket);
    }
}

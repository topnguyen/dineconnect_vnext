﻿using Abp.Configuration;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Wheel.ShippingProviders.Lalamove;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipping.ShippingProviders
{
    public class LalaMoveShippingProvider : DineConnectDomainServiceBase, IShippingProvider
    {
        private readonly LalaMoveProviderGateway _lalamoveProvider;
        private readonly ISettingManager _settingManager;
        private readonly Abp.Runtime.Session.IAbpSession _AbpSession;

        public LalaMoveShippingProvider(ISettingManager settingManager, LalaMoveProviderGateway lalamoveProvider, Abp.Runtime.Session.IAbpSession abpSession)
        {
            _settingManager = settingManager;
            _lalamoveProvider = lalamoveProvider;
            _AbpSession = abpSession;
        }

        public async Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(ProcessShippingOrderRequest request)
        {

            var output = new ProcessShippingOrderResult();

            var shippingLaLaMoveSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingLalaMove, _AbpSession.GetTenantId());

            var scheduleAt = request.ScheduleAt;

            try
            {
                var config = !string.IsNullOrWhiteSpace(shippingLaLaMoveSetting)
                    ? (dynamic)JObject.Parse(shippingLaLaMoveSetting)
                    : null;

                var clientId = (string)config.apiIdLaLaMove;
                var clientSecret = (string)config.apiSecretKeyLaLaMove;

                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV
                    ? LaLaMoveBaseUrlConst.BASE_URL_DEV
                    : LaLaMoveBaseUrlConst.BASE_URL_PROD;

                var country = Convert.ToString(config.Country) ?? "SG";

                _lalamoveProvider.Setup(baseUrl, clientId, clientSecret);

                var pickup = new WayPointRequest
                {
                    Location = new Location
                    {
                        Lat = request.Location.PickupAddressLat,
                        Lng = request.Location.PickupAddressLong
                    },

                    Addresses = new Lalamove.Models.Addresses
                    {
                        EnSG = new Address
                        {
                            DisplayString = request.Location.PickupAddress,
                            Country = country
                        }
                    }
                };

                var delivery = new WayPointRequest
                {
                    Location = new Location
                    {
                        Lat = request.DeliveryLat,
                        Lng = request.DeliveryLong
                    },
                    Addresses = new Lalamove.Models.Addresses
                    {
                        EnSG = new Address
                        {
                            DisplayString = request.DeliveryAddress,
                            Country = country
                        }
                    }
                };

                var deliveryInfoRequests = new[]
                {
                    new DeliveryInfoRequest
                    {
                        ToStop = 1,
                        ToContact = new ContactRequest
                        {
                            Name = request.CustomerName,
                            Phone = request.CustomerPhoneNumber
                        },
                        Remarks = null
                    }
                };

                var requesterContact = new ContactRequest
                {
                    Phone = request.Location.PickupContactPhoneNumber,
                    Name = request.Location.PickupContactName
                };

                var quotation = await _lalamoveProvider.GetQuotations(new GetQuotationRequest
                {
                    ScheduleAt = scheduleAt,
                    Stops = new[]
                    {
                        pickup, delivery
                    },
                    ServiceType = request.ServiceType ?? SgServiceTypeEnum.MOTORCYCLE.ToString(),
                    SpecialRequests = new[]
                    {
                        SgSpecialRequestEnum.COD.ToString()
                    },
                    DeliveryInfoRequests = deliveryInfoRequests,
                    RequesterContact = requesterContact
                });

                if (quotation != null)
                {
                    return new GetShippingOrderQuotationResult()
                    {
                        ShippingFee = quotation.TotalFee,
                        ShippingFeeCurrency = quotation.TotalFeeCurrency
                    };
                }
            }
            catch (Exception e)
            {
                // DEBUG
                throw e;
            }

            return null;
        }

        public Settings.ShippingProvider Provider => Settings.ShippingProvider.LalaMove;

        public async Task<ProcessShippingOrderResult> ProcessShippingOrder(ProcessShippingOrderRequest request)
        {
            var output = new ProcessShippingOrderResult();

            var shippingLaLaMoveSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingLalaMove, _AbpSession.GetTenantId());

            var scheduleAt = request.ScheduleAt;

            try
            {
                var config = !string.IsNullOrWhiteSpace(shippingLaLaMoveSetting) ? (dynamic)JObject.Parse(shippingLaLaMoveSetting) : null;

                var clientId = (string)config.apiIdLaLaMove;
                var clientSecret = (string)config.apiSecretKeyLaLaMove;

                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV ? LaLaMoveBaseUrlConst.BASE_URL_DEV : LaLaMoveBaseUrlConst.BASE_URL_PROD;
                var country = Convert.ToString(config.Country) ?? "SG";

                _lalamoveProvider.Setup(baseUrl, clientId, clientSecret);

                var pickup = new WayPointRequest
                {
                    Location = new Location
                    {
                        Lat = request.Location.PickupAddressLat,
                        Lng = request.Location.PickupAddressLong
                    },

                    Addresses = new Lalamove.Models.Addresses
                    {
                        EnSG = new Address
                        {
                            DisplayString = request.Location.PickupAddress,
                            Country = country
                        }
                    }
                };

                var delivery = new WayPointRequest
                {
                    Location = new Location
                    {
                        Lat = request.DeliveryLat,
                        Lng = request.DeliveryLong
                    },
                    Addresses = new Lalamove.Models.Addresses
                    {
                        EnSG = new Address
                        {
                            DisplayString = request.DeliveryAddress,
                            Country = country
                        }
                    }
                };

                var deliveryInfoRequests = new[]
                {
                    new DeliveryInfoRequest
                    {
                        ToStop = 1,
                        ToContact = new ContactRequest
                        {
                            Name = request.CustomerName,
                            Phone = request.CustomerPhoneNumber
                        },
                        Remarks = null
                    }
                };

                var requesterContact = new ContactRequest
                {
                    Phone = request.Location.PickupContactPhoneNumber,
                    Name = request.Location.PickupContactName
                };

                var quotation = await GetShippingOrderQuotation(request);

                var placeOrder = await _lalamoveProvider.PlaceOrder(new PlaceOrderRequest
                {
                    ScheduleAt = scheduleAt,
                    ServiceType = request.ServiceType ?? SgServiceTypeEnum.MOTORCYCLE.ToString(),
                    SpecialRequests = new[]
                    {
                        SgSpecialRequestEnum.COD.ToString()
                    },
                    RequesterContact = requesterContact,
                    Stops = new[]
                    {
                        pickup, delivery
                    },
                    DeliveryInfoRequests = deliveryInfoRequests,
                    QuotaTotalFeeRequest = new QuotaTotalFeeRequest
                    {
                        Amount = quotation.ShippingFee,
                        Currency = quotation.ShippingFeeCurrency
                    }
                });

                output.CustomerOrderId = placeOrder.OrderRef;
                output.ShippingOrderAmount = Newtonsoft.Json.JsonConvert.SerializeObject(new { Amount = quotation.ShippingFee, Currency = quotation.ShippingFeeCurrency });

                output.PlaceOrderResponse = Newtonsoft.Json.JsonConvert.SerializeObject(placeOrder);

                return output;
            }
            catch (Exception e)
            {
                // DEBUG
                throw e;
            }
        }

        public async Task<CancelShippingOrderResponse> CancelShippingOrder(CancelShippingOrderRequest request)
        {
            try
            {
                var shippingLaLaMoveSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingLalaMove, _AbpSession.GetTenantId());

                var config = !string.IsNullOrWhiteSpace(shippingLaLaMoveSetting)
                    ? (dynamic)JObject.Parse(shippingLaLaMoveSetting)
                    : null;

                var clientId = (string)config.apiIdLaLaMove;
                var clientSecret = (string)config.apiSecretKeyLaLaMove;

                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV
                    ? LaLaMoveBaseUrlConst.BASE_URL_DEV
                    : LaLaMoveBaseUrlConst.BASE_URL_PROD;

                var country = Convert.ToString(config.Country) ?? "SG";

                _lalamoveProvider.Setup(baseUrl, clientId, clientSecret);

                var result = await _lalamoveProvider.CancelOrder(request.OrderNo);

                if (string.IsNullOrEmpty(result))
                {
                    return new CancelShippingOrderResponse()
                    {
                        Canceled = true,
                        NewStatus = "CANCELED",
                    };
                }

                return new CancelShippingOrderResponse()
                {
                    Canceled = false,
                    Message = result
                };
            }
            catch (Exception e)
            {
                // DEBUG
                throw e;
            }
        }
    }
}

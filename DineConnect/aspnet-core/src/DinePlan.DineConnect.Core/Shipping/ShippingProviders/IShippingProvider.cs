﻿using DinePlan.DineConnect.Settings;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Shipping
{
    public interface IShippingProvider
    {
        Task<ProcessShippingOrderResult> ProcessShippingOrder(ProcessShippingOrderRequest request);

        Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(ProcessShippingOrderRequest request);

        Task<CancelShippingOrderResponse> CancelShippingOrder(CancelShippingOrderRequest request);
       
        Settings.ShippingProvider Provider { get; }
    }
}

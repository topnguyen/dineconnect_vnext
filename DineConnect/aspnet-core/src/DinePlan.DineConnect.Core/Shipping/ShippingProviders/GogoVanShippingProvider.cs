﻿using Abp.Configuration;
using Abp.Runtime.Session;
using Abp.Timing;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Settings;
using DinePlan.DineConnect.Timing;
using DinePlan.DineConnect.Wheel.ShippingProviders.GogoVan;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.GogoVan;

namespace DinePlan.DineConnect.Shipping.ShippingProviders
{
    public class GogoVanShippingProvider : DineConnectDomainServiceBase, IShippingProvider
    {
        public Settings.ShippingProvider Provider => Settings.ShippingProvider.GoGoVan;

        private readonly GoGoVanShippingProviderGateway _gogoVanProvider;
        private readonly ISettingManager _settingManager;
        private readonly Abp.Runtime.Session.IAbpSession _AbpSession;
        private readonly ITimeZoneService _timeZoneService;
        public GogoVanShippingProvider(ISettingManager settingManager, GoGoVanShippingProviderGateway gogoVanProvider, Abp.Runtime.Session.IAbpSession abpSession, ITimeZoneService timeZoneService)
        {
            _settingManager = settingManager;
            _gogoVanProvider = gogoVanProvider;
            _AbpSession = abpSession;
            _timeZoneService = timeZoneService;
        }

        public async Task<ProcessShippingOrderResult> ProcessShippingOrder(ProcessShippingOrderRequest request)
        {
            var output = new ProcessShippingOrderResult();

            var gogoVanApiSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingGoGoVan, _AbpSession.GetTenantId());

            try
            {
                var config = !string.IsNullOrWhiteSpace(gogoVanApiSetting) ? (dynamic)JObject.Parse(gogoVanApiSetting) : null;
                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV ? GoGoVanBaseUrlConst.BASE_URL_DEV : GoGoVanBaseUrlConst.BASE_URL_PROD;

                _gogoVanProvider.Setup(baseUrl);

                var placeOrderRequest = new PlaceOrderInput();

                placeOrderRequest.ClientId = config.apiIdGoGoVan;
                placeOrderRequest.ClientSecret = config.apiSecretKeyGoGoVan;

                var scheduleAt_String = request.ScheduleAt;

                var scheduleAt_DateTime = DateTimeOffset.Parse(scheduleAt_String).UtcDateTime;

                var timeZone = await GetTimezone();
                var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

                var scheduleAt_Utc = TimeZoneInfo.ConvertTimeFromUtc(scheduleAt_DateTime, timezoneInfo);
                var scheduleAt = new DateTimeOffset(scheduleAt_Utc).ToUnixTimeSeconds();
                // Pickup
                placeOrderRequest.Pickup = new WaypointInfo
                {
                    //ScheduleAt = wheelOrder.WheelTicket.OrderDate.Hour,
                    Contact = new ContactInfo
                    {
                        Name = request.Location.PickupContactName,
                        PhoneNumber = request.Location.PickupContactPhoneNumber
                    },
                    Name = request.Location?.PickupContactName,
                    Location = new LocationInfo
                    {
                        Latitude = float.Parse(request.Location.PickupAddressLat),
                        Longitude = float.Parse(request.Location.PickupAddressLong)
                    },
                    StreetAddress = request.Location?.PickupAddress,
                    ScheduleAt = scheduleAt
                };

                // Destination
                placeOrderRequest.Destinations = new List<WaypointInfo>
                {
                    new WaypointInfo
                    {
                        Contact = new ContactInfo
                        {
                            Name = request.CustomerName,
                            PhoneNumber = request.CustomerPhoneNumber
                        },
                        Name = request.CustomerName,
                        Location = new LocationInfo
                        {
                            Latitude = float.Parse(request.DeliveryLat),
                            Longitude = float.Parse(request.DeliveryLong)
                        },
                        StreetAddress = request.DeliveryAddress,
                        ScheduleAt = 1625535947
                    }
                };

                //var placeOrder = await GoGoVanShippingProviderUtil.PlaceOrderAsync(placeOrderRequest);
                var placeOrder = await _gogoVanProvider.PlaceOrderAsync(placeOrderRequest);

                output.CustomerOrderId = placeOrder.UUID;
                output.ShippingOrderAmount = Newtonsoft.Json.JsonConvert.SerializeObject(new { Amount = placeOrder.Price.Amount.ToString(), Currency = placeOrder.Price.Currency });
                output.PlaceOrderResponse = Newtonsoft.Json.JsonConvert.SerializeObject(placeOrder);

                return output;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
            //return null;
        }

        public async Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(ProcessShippingOrderRequest request)
        {
            var output = new ProcessShippingOrderResult();

            var gogoVanApiSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingGoGoVan, _AbpSession.GetTenantId());

            try
            {
                var config = !string.IsNullOrWhiteSpace(gogoVanApiSetting) ? (dynamic)JObject.Parse(gogoVanApiSetting) : null;
                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV ? GoGoVanBaseUrlConst.BASE_URL_DEV : GoGoVanBaseUrlConst.BASE_URL_PROD;

                _gogoVanProvider.Setup(baseUrl);

                var placeOrderRequest = new PlaceOrderInput();

                placeOrderRequest.ClientId = config.apiIdGoGoVan;
                placeOrderRequest.ClientSecret = config.apiSecretKeyGoGoVan;

                var scheduleAt_String = request.ScheduleAt;

                var scheduleAt_DateTime = DateTimeOffset.Parse(scheduleAt_String).UtcDateTime;

                var timeZone = await GetTimezone();
                var timezoneInfo = _timeZoneService.FindTimeZoneById(timeZone);

                var scheduleAt_Utc = TimeZoneInfo.ConvertTimeFromUtc(scheduleAt_DateTime, timezoneInfo);
                var scheduleAt = new DateTimeOffset(scheduleAt_Utc).ToUnixTimeSeconds();

                // Pickup
                placeOrderRequest.Pickup = new WaypointInfo
                {
                    Location = new LocationInfo
                    {
                        Latitude = float.Parse(request.Location.PickupAddressLat),
                        Longitude = float.Parse(request.Location.PickupAddressLong)
                    },
                    ScheduleAt = scheduleAt
                };

                // Destination
                placeOrderRequest.Destinations = new List<WaypointInfo>
                {
                    new WaypointInfo
                    {
                        Location = new LocationInfo
                        {
                            Latitude = float.Parse(request.DeliveryLat),
                            Longitude = float.Parse(request.DeliveryLong)
                        },
                        ScheduleAt = 1625535947
                    }
                };

                //var placeOrder = await GoGoVanShippingProviderUtil.PlaceOrderAsync(placeOrderRequest);
                var quotation = await _gogoVanProvider.GetQuotationAsync(placeOrderRequest);

                if (quotation != null)
                {
                    return new GetShippingOrderQuotationResult()
                    {
                        ShippingFee = quotation.EstimatedPrice.Amount.ToString(),
                        ShippingFeeCurrency = quotation.EstimatedPrice.Currency
                    };
                }

                return null;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
            //return null;
        }

        public async Task<CancelShippingOrderResponse> CancelShippingOrder(CancelShippingOrderRequest request)
        {
            throw new NotImplementedException();
        }

        private async Task<string> GetTimezone()
        {
            var timezone = string.Empty;
            if (Clock.SupportsMultipleTimezone)
            {
                timezone = await _settingManager.GetSettingValueForTenantAsync(TimingSettingNames.TimeZone, _AbpSession.TenantId ?? 1);
            }

            var defaultTimeZoneId =
                await _timeZoneService.GetDefaultTimezoneAsync(SettingScopes.Tenant, _AbpSession.TenantId);

            timezone = timezone ?? defaultTimeZoneId;

            return timezone;
        }
    }
}

﻿namespace DinePlan.DineConnect.Wheel.ShippingProviders.Dunzo

{
    public class DunzoBaseUrlConst
    {
        public const string BASE_URL_DEV = "https://apis-staging.dunzo.in";
        public const string BASE_URL_PROD = "https://api.dunzo.in";
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoQuoteRequest
    {
        [JsonProperty("pickup_details")]
        public List<DunzoQuotePickupDetail> PickupDetails { get; set; }

        [JsonProperty("optimised_route")]
        public bool OptimisedRoute { get; set; }

        [JsonProperty("drop_details")]
        public List<DunzoQuoteDropDetail> DropDetails { get; set; }

        [JsonProperty("delivery_type")]
        public string DeliveryType { get; set; }

        [JsonProperty("schedule_time")]
        public long ScheduleTime { get; set; }

        public class DunzoQuotePickupDetail
        {
            [JsonProperty("lat")]
            public double Lat { get; set; }

            [JsonProperty("lng")]
            public double Lng { get; set; }
        }

        public class DunzoQuoteDropDetail
        {
            [JsonProperty("lat")]
            public double Lat { get; set; }

            [JsonProperty("lng")]
            public double Lng { get; set; }

            [JsonProperty("reference_id")]
            public string ReferenceId { get; set; }
        }
    }
}

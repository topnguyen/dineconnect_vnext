﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoQuoteResponse : DunzoBaseResponse
    {
        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("distance")]
        public double Distance { get; set; }

        [JsonProperty("estimated_price")]
        public int EstimatedPrice { get; set; }

        [JsonProperty("eta")]
        public DunzoEta Eta { get; set; }

        [JsonProperty("drop_order")]
        public List<string> DropOrder { get; set; }

        public bool IsValid
        {
            get
            {
                return string.IsNullOrEmpty(Code) && string.IsNullOrEmpty(Message);
            }
        }
    }
}

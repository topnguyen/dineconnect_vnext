﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoBaseResponse
    {
        [JsonProperty("code")]
        
        public string Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
        
        [JsonProperty("details")]
        public string Details { get; set; }
    }

    public class DunzoEta
    {
        [JsonProperty("pickup")]
        public int Pickup { get; set; }


        [JsonProperty("dropoff")]
        public int Dropoff { get; set; }
    }
}

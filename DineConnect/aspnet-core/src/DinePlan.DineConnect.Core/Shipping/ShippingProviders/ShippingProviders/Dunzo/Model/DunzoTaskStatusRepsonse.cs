﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoTaskStatusRepsonse
    {
        [JsonProperty("task_id")]
        public string TaskId { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("event_timestamp")]
        public long EventTimestamp { get; set; }

        [JsonProperty("cancelled_by")]
        public string CancelledBy { get; set; }

        [JsonProperty("cancellation_reason")]
        public string CancellationReason { get; set; }

        [JsonProperty("reference_id")]
        public string ReferenceId { get; set; }

        [JsonProperty("eta")]
        public DunzoEta Eta { get; set; }

        [JsonProperty("request_timestamp")]
        public long RequestTimestamp { get; set; }

        [JsonProperty("locations_order")]
        public List<DunzoLocationsOrder> LocationsOrder { get; set; }

        [JsonProperty("current_drop_id")]
        public string CurrentDropId { get; set; }

        [JsonProperty("runner")]
        public DunzoRunner Runner { get; set; }
    }

    public class DunzoLocationsOrder
    {
        [JsonProperty("event_timestamp")]
        public int EventTimestamp { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("reference_id")]
        public string ReferenceId { get; set; }

        [JsonProperty("cancelled_by")]
        public string CancelledBy { get; set; }

        [JsonProperty("cancellation_reason")]
        public string CancellationReason { get; set; }

        [JsonProperty("is_added")]
        public bool? IsAdded { get; set; }

        [JsonProperty("is_return")]
        public bool? IsReturn { get; set; }
    }

    public class DunzoLocation
    {
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }
    }

    public class DunzoRunner
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("location")]
        public DunzoLocation Location { get; set; }
    }
}

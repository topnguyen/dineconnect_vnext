﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoGetAccessTokenResponse : DunzoBaseResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}

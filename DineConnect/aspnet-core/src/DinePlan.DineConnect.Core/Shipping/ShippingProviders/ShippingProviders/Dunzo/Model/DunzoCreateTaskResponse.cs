﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoCreateTaskResponse : DunzoBaseResponse
    {
        [JsonProperty("task_id")]
        public string TaskId { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("estimated_price")]
        public double EstimatedPrice { get; set; }

        [JsonProperty("eta")]
        public DunzoEta Eta { get; set; }

        [JsonProperty("drop_ids")]
        public List<string> DropIds { get; set; }
    }
}

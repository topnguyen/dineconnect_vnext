using Abp.Dependency;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Lalamove.Models.Response;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.ShippingProviders.Dunzo
{
    public class DunzoProviderGateway : ITransientDependency
    {
        private string _baseUrl = "";
        private string _clientId = "";
        private string _clientSecret = "";

        private string _accessToken;

        private string AccessToken
        {
            get
            {
                if (string.IsNullOrEmpty(_accessToken))
                {
                    _accessToken = GetAccessToken();
                }

                return _accessToken;
            }
        }

        public void Setup(string baseUrl, string apiKey, string secretKey)
        {
            _baseUrl = baseUrl;
            _clientId = apiKey;
            _clientSecret = secretKey;
        }

        public string GetAccessToken()
        {
            var client = new RestClient($"{_baseUrl}/api/v1/token");
            var request = new RestRequest(Method.GET);

            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept-language", "en_US");
            request.AddHeader("client-secret", _clientSecret);
            request.AddHeader("client-id", _clientId);

            IRestResponse<DunzoGetAccessTokenResponse> response = client.Execute<DunzoGetAccessTokenResponse>(request);

            return response.Data.Token;
        }

        public async Task<DunzoQuoteResponse> GetQuotation(DunzoQuoteRequest request)
        {
            var client = new RestClient($"{_baseUrl}/api/v2/quote");
            IRestResponse<DunzoQuoteResponse> response = await client.ExecuteAsync<DunzoQuoteResponse>(CreateRequestMessage(request));
            return response.Data;
        }

        public async Task<DunzoCreateTaskResponse> PlaceOrder(DunzoCreateTaskRequest request)
        {
            var client = new RestClient($"{_baseUrl}/api/v2/tasks");
            IRestResponse<DunzoCreateTaskResponse> response = await client.ExecuteAsync<DunzoCreateTaskResponse>(CreateRequestMessage(request));
            return response.Data;
        }

        public async Task<DunzoTaskStatusRepsonse> GetTaskStatus(string orderId)
        {
            var client = new RestClient($"{_baseUrl}/api/v1/tasks/{orderId}/status");
            var request = new RestRequest(Method.GET);

            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept-language", "en_US");
            request.AddHeader("authorization", $"{AccessToken}");
            request.AddHeader("client-id", _clientId);

            IRestResponse<DunzoTaskStatusRepsonse> response = await client.ExecuteAsync<DunzoTaskStatusRepsonse>(CreateRequestMessage(request));

            return response.Data;
        }

        private string GenerateSignatureToken(string body, HttpMethod httpMethod, string path)
        {
            var time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var method = httpMethod.Method.ToUpper();
            var rawSignature = $"{time}\r\n{method}\r\n{path}\r\n\r\n{body}";
            var keyByte = Encoding.UTF8.GetBytes(_clientSecret);
            var messageBytes = Encoding.UTF8.GetBytes(rawSignature);
            var hashMessage = new HMACSHA256(keyByte).ComputeHash(messageBytes);
            var signature = string.Concat(Array.ConvertAll(hashMessage, x => x.ToString("x2")));
            var token = $"{_clientId}:{time}:{signature}";

            return token;
        }

        private RestRequest CreateRequestMessage(object body)
        {
            var request = new RestRequest(Method.POST);

            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept-language", "en_US");
            request.AddHeader("authorization", $"{AccessToken}");
            request.AddHeader("client-id", _clientId);

            var bodyStr = JsonConvert.SerializeObject(body, Newtonsoft.Json.Formatting.None,
                               new JsonSerializerSettings
                               {
                                   NullValueHandling = NullValueHandling.Ignore
                               });

            request.AddParameter("application/json", bodyStr, ParameterType.RequestBody);

            return request;
        }
    }
}
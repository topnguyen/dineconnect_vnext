﻿using DinePlan.DineConnect.Wheel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model
{
    public class DunzoCreateTaskRequest
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        [JsonProperty("reference_id")]
        public string ReferenceId { get; set; }

        [JsonProperty("pickup_details")]
        public List<DunzoPickupDetail> PickupDetails { get; set; }

        [JsonProperty("optimised_route")]
        public bool OptimisedRoute { get; set; }

        [JsonProperty("drop_details")]
        public List<DunzoDropDetail> DropDetails { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("delivery_type")]
        public string DeliveryType { get; set; }

        [JsonProperty("schedule_time")]
        public long ScheduleTime { get; set; }
    }
    public class DunzoContactDetails
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }
    }

    public class DunzoAddress
    {
        [JsonProperty("apartment_address")]
        public string ApartmentAddress { get; set; }

        [JsonProperty("street_address_1")]
        public string StreetAddress1 { get; set; }

        [JsonProperty("street_address_2")]
        public string StreetAddress2 { get; set; }

        [JsonProperty("landmark")]
        public string Landmark { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("pincode")]
        public string Pincode { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("contact_details")]
        public DunzoContactDetails ContactDetails { get; set; }

        public DunzoAddress() { }

        public DunzoAddress(WheelLocation location)
        {
            StreetAddress1 = location.PickupAddress;
            Lat = Convert.ToDouble(location.PickupAddressLat);
            Lng = Convert.ToDouble(location.PickupAddressLong);
            ContactDetails = new DunzoContactDetails
            {
                Name = location.PickupContactName,
                PhoneNumber = location.PickupContactPhoneNumber
            };
        }
    }

    public class DunzoPickupDetail
    {
        [JsonProperty("reference_id")]
        public string ReferenceId { get; set; }

        [JsonProperty("address")]
        public DunzoAddress Address { get; set; }

        [JsonProperty("payment_data")]
        public DunzoPaymentData PaymentData { get; set; }
    }

    public class DunzoDropDetail
    {
        [JsonProperty("reference_id")]
        public string ReferenceId { get; set; }

        [JsonProperty("address")]
        public DunzoAddress Address { get; set; }

        [JsonProperty("otp_required")]
        public bool OtpRequired { get; set; }

        [JsonProperty("special_instructions")]
        public string SpecialInstructions { get; set; }

        [JsonProperty("payment_data")]
        public DunzoPaymentData PaymentData { get; set; }
    }

    public class DunzoPaymentData
    {
        [JsonProperty("payment_Method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("Amount")]
        public double Amount { get; set; }
    }
}

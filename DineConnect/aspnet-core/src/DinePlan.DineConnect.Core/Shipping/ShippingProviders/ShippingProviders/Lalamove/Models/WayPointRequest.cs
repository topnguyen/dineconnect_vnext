using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class WayPointRequest
    {
        [JsonProperty("location")] public Location Location { get; set; }

        [JsonProperty("addresses")] public Addresses Addresses { get; set; }
    }

    public class Addresses
    {
        [JsonProperty("en_SG")] public Address EnSG { get; set; }
    }

    public class Address
    {
        [JsonProperty("displayString")] public string DisplayString { get; set; }

        [JsonProperty("country")] public string Country { get; set; }
    }

    public class Location
    {
        [JsonProperty("lat")] public string Lat { get; set; }

        [JsonProperty("lng")] public string Lng { get; set; }
    }
}
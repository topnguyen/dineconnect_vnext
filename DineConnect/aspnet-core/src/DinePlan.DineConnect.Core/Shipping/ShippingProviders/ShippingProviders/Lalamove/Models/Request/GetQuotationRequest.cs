using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class GetQuotationRequest
    {
        [JsonProperty("scheduleAt")] public string ScheduleAt { get; set; }
        
        [JsonProperty("stops")] public WayPointRequest[] Stops { get; set; }

        [JsonProperty("deliveries")] public DeliveryInfoRequest[] DeliveryInfoRequests { get; set; }

        [JsonProperty("serviceType")] public string ServiceType { get; set; }

        [JsonProperty("specialRequests")] public string[] SpecialRequests { get; set; }

        [JsonProperty("requesterContact")] public ContactRequest RequesterContact { get; set; }

    }
}
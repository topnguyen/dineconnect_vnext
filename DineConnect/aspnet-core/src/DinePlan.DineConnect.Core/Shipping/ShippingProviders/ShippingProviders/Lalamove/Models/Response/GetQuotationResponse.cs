using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models.Response
{
    public class GetQuotationResponse
    {
        [JsonProperty("totalFee")]
        public string TotalFee { get; set; }

        [JsonProperty("totalFeeCurrency")]
        public string TotalFeeCurrency { get; set; }
    }
}
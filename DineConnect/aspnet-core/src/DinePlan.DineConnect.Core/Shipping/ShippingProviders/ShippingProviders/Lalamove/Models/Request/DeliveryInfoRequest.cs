using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class DeliveryInfoRequest
    {
        //The index of waypoint in stops this information associates with, has to be >= 1, since the first stop's Delivery Info is tided to requesterContact

        [JsonProperty("toStop")]
        public long ToStop { get; set; }

        [JsonProperty("toContact")]
        public ContactRequest ToContact { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class QuotaTotalFeeRequest
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
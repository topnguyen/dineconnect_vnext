namespace DinePlan.DineConnect.Lalamove.Models
{
    public enum OrderStatuEnum
    {
        //Trying to match shipment with a driver.
        ASSIGNING_DRIVER,

        //A driver has accepted the order.
        ON_GOING,

        //The driver has picked up the order.
        PICKED_UP,

        //The order has been delivered sucessfully and transaction has concluded.
        COMPLETED,

        //User has canceled the order.
        CANCELED,

        //The order was matched and rejected twice by two drivers in a row, see Order Flow.
        REJECTED,

        //The order expired as no drivers accepted the order.
        EXPIRED
    }
}
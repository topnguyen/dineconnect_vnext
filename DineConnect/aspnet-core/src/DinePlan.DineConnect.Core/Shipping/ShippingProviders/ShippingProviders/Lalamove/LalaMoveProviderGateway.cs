using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Lalamove.Models.Response;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Wheel.ShippingProviders.Lalamove
{
    public class LalaMoveProviderGateway : ITransientDependency
    {
        private string _baseUrl = "https://sandbox-rest.lalamove.com";
        private string _apiKey = "02bd00b1a3304b8a9175cbc4b7cc7f5d";
        private string _secret = "MC0CAQACBQC+5UT/AgMBAAECBQCvL77RAgMAyAMCAwD0VQIDAK5dAgIsOQIC";

        // private string _baseUrl;
        // private string _apiKey;
        // private string _secret;

        public void Setup(string baseUrl, string apiKey, string secretKey)
        {
            _baseUrl = baseUrl;
            _apiKey = apiKey;
            _secret = secretKey;
        }

        public async Task<GetQuotationResponse> GetQuotations(GetQuotationRequest getQuotationRequest)
        {
            var path = "/v2/quotations";
            var body = JsonConvert.SerializeObject(getQuotationRequest);
            using (var client = new HttpClient())
            {
                var request = CreateRequestMessage(HttpMethod.Post, path, body);
                var response = await client.SendAsync(request);
                var resoonseContent = await response.Content.ReadAsStringAsync();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<GetQuotationResponse>(resoonseContent);
            }
        }

        public async Task<PlaceOrderResponse> PlaceOrder(PlaceOrderRequest placeOrderRequest)
        {
            var path = "/v2/orders";
            var body = JsonConvert.SerializeObject(placeOrderRequest);
            using (var client = new HttpClient())
            {
                var request = CreateRequestMessage(HttpMethod.Post, path, body);
                var response = await client.SendAsync(request);
                var resoonseContent = await response.Content.ReadAsStringAsync();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<PlaceOrderResponse>(resoonseContent);
            }
        }

        public async Task<string> CancelOrder(string orderId)
        {
            var path = $"/v2/orders/{orderId}/cancel";
            using (var client = new HttpClient())
            {
                var request = CreateRequestMessage(HttpMethod.Put, path, "{}");
                var response = await client.SendAsync(request);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var resoonseContent = await response.Content.ReadAsStringAsync();
                    return resoonseContent;
                }

                return string.Empty;
            }
        }

        public async Task<OrderDetailResponse> GetOrderDetail(string orderId)
        {
            var path = "/v2/orders/" + orderId;
            using (var client = new HttpClient())
            {
                var request = CreateRequestMessage(HttpMethod.Post, path, null);
                var response = await client.SendAsync(request);
                var resoonseContent = await response.Content.ReadAsStringAsync();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<OrderDetailResponse>(resoonseContent);
            }
        }

        private string GenerateSignatureToken(string body, HttpMethod httpMethod, string path)
        {
            var time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var method = httpMethod.Method.ToUpper();
            var rawSignature = $"{time}\r\n{method}\r\n{path}\r\n\r\n{body}";
            var keyByte = Encoding.UTF8.GetBytes(_secret);
            var messageBytes = Encoding.UTF8.GetBytes(rawSignature);
            var hashMessage = new HMACSHA256(keyByte).ComputeHash(messageBytes);
            var signature = string.Concat(Array.ConvertAll(hashMessage, x => x.ToString("x2")));
            var token = $"{_apiKey}:{time}:{signature}";

            return token;
        }

        private HttpRequestMessage CreateRequestMessage(HttpMethod method, string path, string body)
        {
            var url = $"{_baseUrl}{path}";
            var request = new HttpRequestMessage(method, url);
            var token = GenerateSignatureToken(body, method, path);
            request.Headers.TryAddWithoutValidation("Authorization", $"hmac {token}");
            request.Headers.TryAddWithoutValidation("X-LLM-Country", "SG");
            request.Headers.TryAddWithoutValidation("X-Request-ID",Guid.NewGuid().ToString());
            request.Content = new StringContent(body);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            return request;
        }
    }
}
namespace DinePlan.DineConnect.Lalamove.Models
{
    public enum SgServiceTypeEnum
    {
        //Bike	40 × 25 × 25 cm, 8 kg
        MOTORCYCLE,

        //Car	70 × 50 × 50 cm, 20 kg
        CAR,

        //1.7m Van	160 × 120 × 100 cm
        MINIVAN,

        //2.4m Van	230 × 120 × 120 cm
        VAN,

        //10ft Lorry	290 × 140 × 170 cm
        TRUCK330,

        //14ft Lorry	420 × 170 × 190 cm
        TRUCK550
    }
}
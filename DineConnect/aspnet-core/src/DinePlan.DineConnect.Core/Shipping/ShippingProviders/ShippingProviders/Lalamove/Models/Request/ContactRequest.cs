using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class ContactRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }
    }
}
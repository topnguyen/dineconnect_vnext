using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models.Response
{
    public class OrderDetailResponse
    {
        [JsonProperty("driverId")] public string DriverId { get; set; }

        [JsonProperty("shareLink")] public string ShareLink { get; set; }

        [JsonProperty("status")] public string Status { get; set; }

        [JsonProperty("pod")] public string Pod { get; set; }

        [JsonProperty("price")] public PriceResponse Price { get; set; }
    }

    public class PriceResponse
    {
        [JsonProperty("amount")] public decimal Amount { get; set; }

        [JsonProperty("currency")] public string Currency { get; set; }
    }
}
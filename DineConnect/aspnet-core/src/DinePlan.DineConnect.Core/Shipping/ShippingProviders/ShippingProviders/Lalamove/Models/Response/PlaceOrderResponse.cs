using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models.Response
{
    public class PlaceOrderResponse
    {
        [JsonProperty("orderRef")]
        public string OrderRef { get; set; }

        [JsonProperty("customerOrderId")]
        public string CustomerOrderId { get; set; }
    }
}
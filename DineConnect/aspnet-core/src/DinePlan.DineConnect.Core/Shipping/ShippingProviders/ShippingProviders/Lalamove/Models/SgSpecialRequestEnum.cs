namespace DinePlan.DineConnect.Lalamove.Models
{
    public enum SgSpecialRequestEnum
    {
        //Cash on delivery	
        COD,

        //Secured Zone	
        RESTRICTED,

        //Return Trip (for documents only)	
        RETURNTRIP,

        //Return Trip (for documents only)	
        RETURNTRIP_LORRY,

        //Moving service: Driver + 1 Helper		
        MOVING_DRIVER_1HELPER_VAN,

        //Moving service: Driver + 2 Helpers		
        MOVING_DRIVER_2HELPER_VAN,

        //Moving service: Driver		
        MOVING_DRIVER,

        //Moving service: Driver + 1 Helper		
        MOVING_DRIVER_1HELPER,

        //Moving service: Driver + 2 Helpers		
        MOVING_DRIVER_2HELPER,

        //Tailgate
        TAILGATE
    }
}
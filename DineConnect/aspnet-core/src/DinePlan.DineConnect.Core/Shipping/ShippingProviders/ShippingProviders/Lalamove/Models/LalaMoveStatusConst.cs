namespace DinePlan.DineConnect.Lalamove.Models
{
    public class LalaMoveStatusConst
    {
        public const string ORDER_STATUS_CHANGED = "ORDER_STATUS_CHANGED";
        public const string ORDER_AMOUNT_CHANGED = "ORDER_AMOUNT_CHANGED";
    }
}
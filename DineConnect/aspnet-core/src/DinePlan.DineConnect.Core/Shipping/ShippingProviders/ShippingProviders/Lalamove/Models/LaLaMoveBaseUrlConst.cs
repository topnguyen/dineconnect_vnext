﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Lalamove.Models

{
    public class LaLaMoveBaseUrlConst
    {
        public const string BASE_URL_DEV = "https://rest.sandbox.lalamove.com";
        public const string BASE_URL_PROD = "https://rest.lalamove.com";
    }
    public class EnvironmentTypeConst
    {
        public const string DEV = "DEV";
        public const string PROD = "PROD";
    }
}

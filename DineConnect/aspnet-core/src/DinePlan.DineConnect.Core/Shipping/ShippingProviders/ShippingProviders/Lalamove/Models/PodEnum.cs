namespace DinePlan.DineConnect.Lalamove.Models
{
    public enum PodEnum
    {
        //Driver has not yet carried out proof of delivery for this stop.
        PENDING,

        //The recipient has signed.
        SIGNED,

        //The package has been delivered. Recipient (or delegates) did not sign.
        DELIVERED,

        //	The delivery failed.
        FAILED
    }
}
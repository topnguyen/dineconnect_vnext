using Newtonsoft.Json;

namespace DinePlan.DineConnect.Lalamove.Models
{
    public class PlaceOrderRequest :GetQuotationRequest
    {
        [JsonProperty("quotedTotalFee")]
        public QuotaTotalFeeRequest QuotaTotalFeeRequest { get; set; }

        //Send delivery updates via SMS to THE recipient, or the recipient of the LAST STOP for multi-stop orders once the order has been picked-up by the driver. Default to true
        [JsonProperty("sms")]
        public bool Sms { get; set; }

        [JsonProperty("pod")]
        public bool Pod { get; set; }

        [JsonProperty("fleetOption")]
        public string FleetOption { get; set; }
    }
}
﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.GogoVan
{
	public class PlaceOrderInput
	{
		[JsonProperty("client_id")]
		public string ClientId { get; set; }

		[JsonProperty("client_secret")]
		public string ClientSecret { get; set; }

		[JsonProperty("vehicle_type")]
		public string VehicleType { get; set; } = "motorcycle";

		/// <summary>
		///     "prepaid_wallet" or "monthly_settlement"
		/// </summary>
		[JsonProperty("payment_method")]
		public string PaymentMethod { get; set; } = "prepaid_wallet";
		[JsonProperty("pickup")]
		public WaypointInfo Pickup { get; set; } 

		[JsonProperty("destinations")]
		public List<WaypointInfo> Destinations { get; set; }
		public PlaceOrderInput()
		{
			Destinations = new List<WaypointInfo>();
		}

	}

	public class WaypointInfo
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("street_address")]
		public string StreetAddress { get; set; }

		[JsonProperty("floor_or_unit_number")]
		public string FloorOrUnitNumber { get; set; }

		[JsonProperty("schedule_at")]
		public long ScheduleAt { get; set; }

		[JsonProperty("location")]
		public LocationInfo Location { get; set; }

		[JsonProperty("contact")]
		public ContactInfo Contact { get; set; }
	}

	public class LocationInfo
	{
		[JsonProperty("lat")]
		public float Latitude { get; set; }

		[JsonProperty("lng")]
		public float Longitude { get; set; }
	}

	public class ContactInfo
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("phone_number")]
		public string PhoneNumber { get; set; }

		[JsonProperty("phone_extension")]
		public string PhoneExtension { get; set; }
	}

	public class GoGoVanResponWebhook
	{
		[JsonProperty("kinds")]
		public List<string> Kinds { get; set; } 

		[JsonProperty("reasons")]
		public List<string> Reasons { get; set; } 

		[JsonProperty("created_at")]
		public int Created_At { get; set; }

		[JsonProperty("data")]
		public GoGoVanResponDataWebhook Data { get; set; }

		public GoGoVanResponWebhook()
		{
			Kinds  = new List<string>();
			Reasons   = new List<string>();
		}

	}
	public class GoGoVanResponDataWebhook
	{
		[JsonProperty("uuid")]
		public string UUID { get; set; }

		[JsonProperty("vehicle_type")]
		public string VehicleType { get; set; }

		[JsonProperty("payment_method")]
		public string PaymentMethod { get; set; }

		[JsonProperty("note_to_courier")]
		public string NoteToCourier { get; set; }

		[JsonProperty("courier")]
		public GoGoVanCourierDto Courier { get; set; }
		
		[JsonProperty("status")]
		public string Status { get; set; }
		
		[JsonProperty("pickup")]
		public WaypointInfo PickUp { get; set; }
		
		[JsonProperty("destinations")]
		public List<WaypointInfo> Destinations { get; set; } 

		[JsonProperty("price")]
		public GoGoVanPriceDto Price { get; set; }

		[JsonProperty("price_breakdown")]
		public List<GoGoVanPriceBreakdownDto> PriceBreakdown { get; set; }

		public GoGoVanResponDataWebhook()
		{
			Destinations  = new List<WaypointInfo>();
			PriceBreakdown = new List<GoGoVanPriceBreakdownDto>();
		}
	}

	public class GoGoVanCourierDto
	{
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("phone_number")]
		public string PhoneNumber { get; set; }
		[JsonProperty("location")]
		public LocationInfo Location { get; set; }
	}	
	
	public class GoGoVanPriceDto
	{
		[JsonProperty("currency")]
		public string Currency { get; set; }
		
		[JsonProperty("amount")]
		public decimal Amount { get; set; }
	}	
	public class GoGoVanPriceBreakdownDto
	{
		[JsonProperty("key")]
		public string Key { get; set; }
		[JsonProperty("amount")]
		public decimal Amount { get; set; }

		[JsonProperty("quantity")]
		public int Quantity { get; set; }
		
		[JsonProperty("reason")]
		public string Reason { get; set; }
	}
    public class GoGoVanQuotationResult
	{
        [JsonProperty("vehicle_type")]
        public string VehicleType { get; set; }

        [JsonProperty("estimated_price")]
        public GoGoVanPriceDto EstimatedPrice { get; set; }
    }

}

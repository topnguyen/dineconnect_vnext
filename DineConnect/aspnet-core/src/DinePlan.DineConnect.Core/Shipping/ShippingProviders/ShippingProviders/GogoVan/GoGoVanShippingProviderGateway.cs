﻿using Abp.Dependency;
using Flurl.Http;
using System;
using System.Threading.Tasks;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.GogoVan;

namespace DinePlan.DineConnect.Wheel.ShippingProviders.GogoVan
{
    public class GoGoVanShippingProviderGateway : ITransientDependency
    {
        private string _baseUrl = "https://sg-api.gogox.com";
        public void Setup(string baseUrl)
        {
            _baseUrl = baseUrl;
        }
        public async Task<GoGoVanQuotationResult> GetQuotationAsync(PlaceOrderInput input)
        {
            try
            {
                var accessToken = await GetAccessTokenAsync(input.ClientId, input.ClientSecret);

                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    return null;
                }

                var url = $"{_baseUrl}/transport/quotations";

                var response = await url
                    .WithOAuthBearerToken(accessToken)
                    .PostJsonAsync(input)
                    .ReceiveJson<GoGoVanQuotationResult>();

                return response;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
        }

        public async Task<GoGoVanResponDataWebhook> PlaceOrderAsync(PlaceOrderInput input)
        {
            try
            {
                var accessToken = await GetAccessTokenAsync(input.ClientId, input.ClientSecret);

                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    return null;
                }

                var url = $"{_baseUrl}/transport/orders";

                var response = await url
                    .WithOAuthBearerToken(accessToken)
                    .PostJsonAsync(input)
                    .ReceiveJson<GoGoVanResponDataWebhook>();

                return response;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
        }

        public async Task<string> GetAccessTokenAsync(string clientId, string clientSecret)
        {
            try
            {

                var url = $"{_baseUrl}/oauth/token";
                var response = await url
                    .PostJsonAsync(new
                    {
                        grant_type = "client_credentials",
                        client_id = clientId,
                        client_secret = clientSecret
                    }).ReceiveJson();

                return response.access_token;
            }
            catch (Exception e)
            {
                // DEBUG
                return string.Empty;
            }
        }

        public async Task<dynamic> GetOrderAsync(string clientId, string clientSecret, string uuid)
        {
            try
            {
                var accessToken = await GetAccessTokenAsync(clientId, clientSecret);

                if (string.IsNullOrWhiteSpace(accessToken))
                {
                    return string.Empty;
                }

                var url = $"{_baseUrl}/transport/orders/{uuid}";

                var response = await url
                    .WithOAuthBearerToken(accessToken)
                    .GetJsonAsync();

                return response;
            }
            catch (Exception e)
            {
                // DEBUG
                return string.Empty;
            }
        }
    }
}

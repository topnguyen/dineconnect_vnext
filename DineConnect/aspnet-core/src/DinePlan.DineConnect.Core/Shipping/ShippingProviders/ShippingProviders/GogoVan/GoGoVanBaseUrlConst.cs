﻿namespace DinePlan.DineConnect.Wheel.ShippingProviders.GogoVan
{
	public class GoGoVanBaseUrlConst
	{
		public const string BASE_URL_DEV = "https://stag-sg-api.gogox.com";
		public const string BASE_URL_PROD = "https://sg-api.gogox.com";
	}
}

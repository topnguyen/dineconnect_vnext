﻿using Abp.Configuration;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Lalamove.Models;
using DinePlan.DineConnect.Shipping.ShippingProviders.ShippingProviders.Dunzo.Model;
using DinePlan.DineConnect.Wheel.ShippingProviders.Dunzo;
using DinePlan.DineConnect.Wheel.ShippingProviders.Lalamove;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Extensions;

namespace DinePlan.DineConnect.Shipping.ShippingProviders
{
    public class DunzoShippingProvider : DineConnectDomainServiceBase, IShippingProvider
    {
        private readonly DunzoProviderGateway _dunzoProvider;
        private readonly ISettingManager _settingManager;
        private readonly Abp.Runtime.Session.IAbpSession _AbpSession;

        public DunzoShippingProvider(ISettingManager settingManager, DunzoProviderGateway dunzoProvider, Abp.Runtime.Session.IAbpSession abpSession)
        {
            _settingManager = settingManager;
            _dunzoProvider = dunzoProvider;
            _AbpSession = abpSession;
        }

        public async Task<GetShippingOrderQuotationResult> GetShippingOrderQuotation(ProcessShippingOrderRequest request)
        {
            var output = new ProcessShippingOrderResult() { ErrorMessages = new List<string>() };

            var dunzoSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingDunzo, _AbpSession.GetTenantId());

            try
            {
                var config = !string.IsNullOrWhiteSpace(dunzoSetting) ? (dynamic)JObject.Parse(dunzoSetting) : null;

                var clientId = (string)config.apiIdDunzo;
                var clientSecret = (string)config.apiSecretKeyDunzo;

                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV
                    ? DunzoBaseUrlConst.BASE_URL_DEV
                    : DunzoBaseUrlConst.BASE_URL_PROD;
                var country = Convert.ToString(config.Country) ?? "IN";

                _dunzoProvider.Setup(baseUrl, clientId, clientSecret);

                DunzoQuoteRequest input = new DunzoQuoteRequest();
                input.PickupDetails = new List<DunzoQuoteRequest.DunzoQuotePickupDetail>()
                {
                    new DunzoQuoteRequest.DunzoQuotePickupDetail
                    {
                        Lat = Convert.ToDouble(request.Location.PickupAddressLat),
                        Lng = Convert.ToDouble(request.Location.PickupAddressLong)
                    }
                };
                input.OptimisedRoute = false;
                input.DropDetails = new List<DunzoQuoteRequest.DunzoQuoteDropDetail>()
                {
                    new DunzoQuoteRequest.DunzoQuoteDropDetail
                    {
                        ReferenceId = request.DeliveryGuid.ToString(),
                        Lat = Convert.ToDouble(request.DeliveryLat),
                        Lng = Convert.ToDouble(request.DeliveryLong),
                    }
                };

                input.ScheduleTime = request.ScheduleTime;

                var placeOrder = await _dunzoProvider.GetQuotation(input);

                if (placeOrder != null)
                {
                    return new GetShippingOrderQuotationResult()
                    {
                        ShippingFee = placeOrder.EstimatedPrice.ToString(),
                        ShippingFeeCurrency = "INR"
                    };
                }

                return null;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
        }

        public async Task<CancelShippingOrderResponse> CancelShippingOrder(CancelShippingOrderRequest request)
        {
            throw new NotImplementedException();
        }

        public Settings.ShippingProvider Provider => Settings.ShippingProvider.Dunzo;

        public async Task<ProcessShippingOrderResult> ProcessShippingOrder(ProcessShippingOrderRequest request)
        {
            var output = new ProcessShippingOrderResult() { ErrorMessages = new List<string>() };
            var dunzoSetting = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ShippingProviderSetting.ShippingDunzo, _AbpSession.GetTenantId());

            try
            {
                var config = !string.IsNullOrWhiteSpace(dunzoSetting) ? (dynamic)JObject.Parse(dunzoSetting) : null;

                var clientId = (string)config.apiIdDunzo;
                var clientSecret = (string)config.apiSecretKeyDunzo;

                var baseUrl = (string)config.environmentTypes == EnvironmentTypeConst.DEV ? DunzoBaseUrlConst.BASE_URL_DEV : DunzoBaseUrlConst.BASE_URL_PROD;
                var country = Convert.ToString(config.Country) ?? "SG";

                _dunzoProvider.Setup(baseUrl, clientId, clientSecret);

                DunzoCreateTaskRequest input = new DunzoCreateTaskRequest();
                input.RequestId = request.RequestGuid.ToString();
                input.ReferenceId = request.RequestGuid.ToString();
                input.PickupDetails = new List<DunzoPickupDetail>()
                {
                    new DunzoPickupDetail
                    {
                        Address = new DunzoAddress(request.Location),
                        ReferenceId = request.RequestGuid.ToString(),
                    },
                };
                input.OptimisedRoute = false;
                input.DropDetails = new List<DunzoDropDetail>()
                {
                    new DunzoDropDetail
                    {
                        ReferenceId = request.DeliveryGuid.ToString(),
                        Address = new DunzoAddress
                        {
                            StreetAddress1 = request.DeliveryAddress,
                            Lat = Convert.ToDouble(request.DeliveryLat),
                            Lng = Convert.ToDouble(request.DeliveryLong),
                            ContactDetails = new DunzoContactDetails
                            {
                                Name = request.CustomerName,
                                PhoneNumber = request.CustomerPhoneNumber
                            }
                        }
                    }
                };

                input.PaymentMethod = "DUNZO_CREDIT";

                input.ScheduleTime = request.ScheduleTime;

                var placeOrder = await _dunzoProvider.PlaceOrder(input);

                if (string.IsNullOrEmpty(placeOrder.Code))
                {
                    output.CustomerOrderId = placeOrder.TaskId;
                    output.ShippingOrderAmount = Newtonsoft.Json.JsonConvert.SerializeObject(new { Amount = placeOrder.EstimatedPrice.ToString(), Currency = "INR" });
                }
                else
                {
                    output.ErrorMessages.Add(placeOrder.Message);
                }

                output.PlaceOrderResponse = Newtonsoft.Json.JsonConvert.SerializeObject(placeOrder);

                return output;
            }
            catch (Exception e)
            {
                // DEBUG
                return null;
            }
        }
    }
}

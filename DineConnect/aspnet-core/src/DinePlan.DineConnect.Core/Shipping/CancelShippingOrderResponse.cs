﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Shipping
{
    public class CancelShippingOrderResponse
    {
        public bool Canceled { get; set; }

        public  string Message { get; set; }

        public string NewStatus { get; set; }
    }
}

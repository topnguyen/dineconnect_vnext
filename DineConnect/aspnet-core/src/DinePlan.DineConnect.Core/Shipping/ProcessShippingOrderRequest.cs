﻿using System;
using DinePlan.DineConnect.Wheel;
using DinePlan.DineConnect.Wheel.V2;

namespace DinePlan.DineConnect.Shipping
{
    public class ProcessShippingOrderRequest
    {
        public WheelDepartment Department { get; set; }

        public WheelLocation Location { get; set; }

        public WheelBillingInfo BillingInfo { get; set; }

        public string ScheduleAt { get; set; }

        public DateTime ScheduleAtUtc { get; set; }

        public System.Guid? RequestGuid { get; set; }

        public System.Guid? DeliveryGuid { get; set; }

        public long ScheduleTime { get; set; }

        public string DeliveryLat
        {
            get
            {
                return BillingInfo.Lat;
            }
        }

        public string DeliveryLong
        {
            get
            {
                return BillingInfo.Long;
            }
        }

        public string DeliveryAddress
        {
            get
            {
                return BillingInfo.FullAddress;
            }
        }


        public string CustomerName
        {
            get
            {
                return BillingInfo.Name;
            }
        }

        public string CustomerPhoneNumber
        {
            get
            {
                return BillingInfo.PhoneNumber;
            }
        }

        public string ServiceType { get; set; }

        public DateTime? ManualScheduleAt { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Common
{
    [Table("Addons")]
    public class Addon : ConnectFullAuditEntity
    {
        public AddonType AddonType { get; set; }
        public string Name { get; set; }
        public string Countries { get; set; }
        public string Settings { get; set; }
    }

    public enum AddonType
    {
        Payment = 0,
        Email = 1,
        Image = 2
    }

    [Table("TenantAddons")]
    public class TenantAddon : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("AddonId")]
        public Addon Addon { get; set; }
        public int AddonId { get; set; }
        public string Settings { get; set; }
    }
}
﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace DinePlan.DineConnect.Authorization
{
    /// <summary>
    ///     Application's authorization provider.
    ///     Defines permissions for the application.
    ///     See <see cref="AppPermissions" /> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            #region Core

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ??
                        context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var lastSyncs = pages.CreateChildPermission(AppPermissions.Pages_LastSyncs, L("LastSyncs"),
                multiTenancySides: MultiTenancySides.Tenant);
            lastSyncs.CreateChildPermission(AppPermissions.Pages_LastSyncs_Create, L("CreateNewLastSync"),
                multiTenancySides: MultiTenancySides.Tenant);
            lastSyncs.CreateChildPermission(AppPermissions.Pages_LastSyncs_Edit, L("EditLastSync"),
                multiTenancySides: MultiTenancySides.Tenant);
            lastSyncs.CreateChildPermission(AppPermissions.Pages_LastSyncs_Delete, L("DeleteLastSync"),
                multiTenancySides: MultiTenancySides.Tenant);

            var locationSyncers = pages.CreateChildPermission(AppPermissions.Pages_LocationSyncers,
                L("LocationSyncers"), multiTenancySides: MultiTenancySides.Host);
            locationSyncers.CreateChildPermission(AppPermissions.Pages_LocationSyncers_Create,
                L("CreateNewLocationSyncer"), multiTenancySides: MultiTenancySides.Host);
            locationSyncers.CreateChildPermission(AppPermissions.Pages_LocationSyncers_Edit, L("EditLocationSyncer"),
                multiTenancySides: MultiTenancySides.Host);
            locationSyncers.CreateChildPermission(AppPermissions.Pages_LocationSyncers_Delete,
                L("DeleteLocationSyncer"), multiTenancySides: MultiTenancySides.Host);

            var syncers = pages.CreateChildPermission(AppPermissions.Pages_Syncers, L("Syncers"),
                multiTenancySides: MultiTenancySides.Tenant);
            syncers.CreateChildPermission(AppPermissions.Pages_Syncers_Create, L("CreateNewSyncer"),
                multiTenancySides: MultiTenancySides.Tenant);
            syncers.CreateChildPermission(AppPermissions.Pages_Syncers_Edit, L("EditSyncer"),
                multiTenancySides: MultiTenancySides.Tenant);
            syncers.CreateChildPermission(AppPermissions.Pages_Syncers_Delete, L("DeleteSyncer"),
                multiTenancySides: MultiTenancySides.Tenant);

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var languageDescriptions = administration.CreateChildPermission(
                AppPermissions.Pages_Administration_LanguageDescriptions, L("LanguageDescriptions"),
                multiTenancySides: MultiTenancySides.Tenant);
            languageDescriptions.CreateChildPermission(AppPermissions.Pages_Administration_LanguageDescriptions_Create,
                L("CreateNewLanguageDescription"), multiTenancySides: MultiTenancySides.Tenant);
            languageDescriptions.CreateChildPermission(AppPermissions.Pages_Administration_LanguageDescriptions_Edit,
                L("EditLanguageDescription"), multiTenancySides: MultiTenancySides.Tenant);
            languageDescriptions.CreateChildPermission(AppPermissions.Pages_Administration_LanguageDescriptions_Delete,
                L("DeleteLanguageDescription"), multiTenancySides: MultiTenancySides.Tenant);

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions,
                L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages =
                administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create,
                L("CreatingNewLanguage"),
                multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"),
                multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete,
                L("DeletingLanguages"),
                multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts,
                L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits =
                administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits,
                    L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(
                AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree,
                L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers,
                L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles,
                L("ManagingRoles"));

            ////administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription,
                L("Webhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create,
                L("CreatingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit,
                L("EditingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity,
                L("ChangingWebhookActivity"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail,
                L("DetailingSubscription"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts,
                L("ListingSendAttempts"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook,
                L("ResendingWebhook"));

            var dynamicParameters =
                administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters,
                    L("DynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Create,
                L("CreatingDynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Edit,
                L("EditingDynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Delete,
                L("DeletingDynamicParameters"));

            var dynamicParameterValues =
                dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue,
                    L("DynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(
                AppPermissions.Pages_Administration_DynamicParameterValue_Create, L("CreatingDynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Edit,
                L("EditingDynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(
                AppPermissions.Pages_Administration_DynamicParameterValue_Delete, L("DeletingDynamicParameterValue"));

            var entityDynamicParameters = dynamicParameters.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameters, L("EntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameters_Create,
                L("CreatingEntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameters_Edit, L("EditingEntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameters_Delete,
                L("DeletingEntityDynamicParameters"));

            var entityDynamicParameterValues = dynamicParameters.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameterValue, L("EntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameterValue_Create,
                L("CreatingEntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameterValue_Edit,
                L("EditingEntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(
                AppPermissions.Pages_Administration_EntityDynamicParameterValue_Delete,
                L("DeletingEntityDynamicParameterValue"));

            #endregion Core

            #region Host

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition,
                L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"),
                multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"),
                multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"),
                multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard,
                L("HangfireDashboard"),
                multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"),
                multiTenancySides: MultiTenancySides.Host);

            var tenantDatabases = administration.CreateChildPermission(
                AppPermissions.Pages_Administration_Host_TenantDatabase, L("TenantDatabase"),
                multiTenancySides: MultiTenancySides.Host);
            tenantDatabases.CreateChildPermission(AppPermissions.Pages_Administration_Host_TenantDatabase_Create,
                L("CreateNewTenantDatabase"), multiTenancySides: MultiTenancySides.Host);
            tenantDatabases.CreateChildPermission(AppPermissions.Pages_Administration_Host_TenantDatabase_Edit,
                L("EditTenantDatabase"), multiTenancySides: MultiTenancySides.Host);
            tenantDatabases.CreateChildPermission(AppPermissions.Pages_Administration_Host_TenantDatabase_Delete,
                L("DeleteTenantDatabase"), multiTenancySides: MultiTenancySides.Host);

            var origins = administration.CreateChildPermission(AppPermissions.Pages_Administration_Origins,
                L("Origins"), multiTenancySides: MultiTenancySides.Host);
            origins.CreateChildPermission(AppPermissions.Pages_Administration_Origins_Create, L("CreateNewOrigins"),
                multiTenancySides: MultiTenancySides.Host);
            origins.CreateChildPermission(AppPermissions.Pages_Administration_Origins_Edit, L("EditOrigins"),
                multiTenancySides: MultiTenancySides.Host);
            origins.CreateChildPermission(AppPermissions.Pages_Administration_Origins_Delete, L("DeleteOrigins"),
                multiTenancySides: MultiTenancySides.Host);

            #endregion Host

            #region Tenant

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"),
                multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"),
                multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement,
                L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_AddonSettings,
                L("AddonSettings"), multiTenancySides: MultiTenancySides.Tenant);

            var templateEngines = administration.CreateChildPermission(
                AppPermissions.Pages_Administration_Tenant_TemplateEngines, L("TemplateEngines"),
                multiTenancySides: MultiTenancySides.Tenant);
            templateEngines.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_TemplateEngines_Create,
                L("CreateNewTemplateEngine"), multiTenancySides: MultiTenancySides.Tenant);
            templateEngines.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_TemplateEngines_Edit,
                L("EditTemplateEngine"), multiTenancySides: MultiTenancySides.Tenant);
            templateEngines.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_TemplateEngines_Delete,
                L("DeleteTemplateEngine"), multiTenancySides: MultiTenancySides.Tenant);

            var userLocations = administration.CreateChildPermission(AppPermissions.Pages_Administration_UserLocations,
                L("UserLocation"), multiTenancySides: MultiTenancySides.Tenant);
            userLocations.CreateChildPermission(AppPermissions.Pages_Administration_UserLocations_Create,
                L("CreateMember"), multiTenancySides: MultiTenancySides.Tenant);
            //userLocations.CreateChildPermission(AppPermissions.Pages_Administration_UserLocations_Edit, L("EditMember"), multiTenancySides:MultiTenancySides.Tenant);
            userLocations.CreateChildPermission(AppPermissions.Pages_Administration_UserLocations_Delete,
                L("DeleteMember"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization,
                L("VisualSettings"), multiTenancySides: MultiTenancySides.Tenant);

            var terms = administration.CreateChildPermission(AppPermissions.Pages_Administration_Terms,
                L("TermsAndConditions"), multiTenancySides: MultiTenancySides.Tenant);
            terms.CreateChildPermission(AppPermissions.Pages_Administration_Terms_Create, L("CreateNew"),
                multiTenancySides: MultiTenancySides.Tenant);
            terms.CreateChildPermission(AppPermissions.Pages_Administration_Terms_Edit, L("Edit"),
                multiTenancySides: MultiTenancySides.Tenant);
            terms.CreateChildPermission(AppPermissions.Pages_Administration_Terms_Delete, L("Delete"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Tenant

            #region Connect

            var connect = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Connect, L("Connect"),
                multiTenancySides: MultiTenancySides.Tenant);
            var master = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master, L("Master"),
                multiTenancySides: MultiTenancySides.Tenant);


            var promotions = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Promotions,
                L("Promotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Promotions_Create,
                L("CreateNewPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Promotions_Edit, L("EditPromotion"),
                multiTenancySides: MultiTenancySides.Tenant);
            promotions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Promotions_Delete,
                L("DeletePromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Promotions_Revert,
                L("RevertPromotion"), multiTenancySides: MultiTenancySides.Tenant);

            var promotionCategory = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_PromotionCategory,
                L("PromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotionCategory.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_PromotionCategory_Create,
                L("CreateNewPromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotionCategory.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_PromotionCategory_Edit,
                L("EditPromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotionCategory.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_PromotionCategory_Delete,
                L("DeletePromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);

            #region Address

            var address = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address, L("Address"),
                multiTenancySides: MultiTenancySides.Tenant);

            var countries = address.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Countries,
                L("Country"), multiTenancySides: MultiTenancySides.Tenant);
            countries.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Countries_Create,
                L("CreatingNewCountry"), multiTenancySides: MultiTenancySides.Tenant);
            countries.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Countries_Edit,
                L("EditingCountry"), multiTenancySides: MultiTenancySides.Tenant);
            countries.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Countries_Delete,
                L("DeletingCountry"), multiTenancySides: MultiTenancySides.Tenant);

            var states = address.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_States, L("State"),
                multiTenancySides: MultiTenancySides.Tenant);
            states.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_States_Create,
                L("CreatingNewState"), multiTenancySides: MultiTenancySides.Tenant);
            states.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_States_Edit, L("EditingState"),
                multiTenancySides: MultiTenancySides.Tenant);
            states.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_States_Delete, L("DeletingState"),
                multiTenancySides: MultiTenancySides.Tenant);

            var cities = address.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Cities, L("City"),
                multiTenancySides: MultiTenancySides.Tenant);
            cities.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Cities_Create,
                L("CreatingNewCity"), multiTenancySides: MultiTenancySides.Tenant);
            cities.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Cities_Edit, L("EditingCity"),
                multiTenancySides: MultiTenancySides.Tenant);
            cities.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Address_Cities_Delete, L("DeletingCity"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Address

            #region Tags

            var tags = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag, L("Tag"));
            var priceTags = tags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_PriceTags, L("PriceTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            priceTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_PriceTags_Create,
                L("CreateNewPriceTag"), multiTenancySides: MultiTenancySides.Tenant);
            priceTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_PriceTags_Edit, L("EditPriceTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            priceTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_PriceTags_Delete,
                L("DeletePriceTag"), multiTenancySides: MultiTenancySides.Tenant);

            var ticketTagGroups = tags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_TicketTagGroups,
                L("TicketTagGroups"), multiTenancySides: MultiTenancySides.Tenant);
            ticketTagGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_TicketTagGroups_Create,
                L("CreateNewTicketTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ticketTagGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_TicketTagGroups_Edit,
                L("EditTicketTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ticketTagGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_TicketTagGroups_Delete,
                L("DeleteTicketTagGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var orderTags = tags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_OrderTags, L("OrderTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            orderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_OrderTags_Create,
                L("CreateNewOrderTag"), multiTenancySides: MultiTenancySides.Tenant);
            orderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_OrderTags_Edit, L("EditOrderTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            orderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tag_OrderTags_Delete,
                L("DeleteOrderTag"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Tags

            #region Location

            var location = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location, L("Location"),
                multiTenancySides: MultiTenancySides.Tenant);

            var organizations = location.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Brands,
                L("Brand"), multiTenancySides: MultiTenancySides.Tenant);
            organizations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Brands_Create,
                L("CreatingNewBrand"), multiTenancySides: MultiTenancySides.Tenant);
            organizations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Brands_Edit,
                L("EditingBrand"), multiTenancySides: MultiTenancySides.Tenant);
            organizations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Brands_Delete,
                L("DeletingBrand"), multiTenancySides: MultiTenancySides.Tenant);

            var locations = location.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Locations,
                L("Location"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Locations_Create,
                L("CreatingNewLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Locations_Edit,
                L("EditingLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_Locations_Delete,
                L("DeletingLocation"), multiTenancySides: MultiTenancySides.Tenant);

            var locationGroups = location.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Location_LocationGroups, L("LocationGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            locationGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationGroups_Create,
                L("CreatingNewLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            locationGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationGroups_Edit,
                L("EditingLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            locationGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationGroups_Delete,
                L("DeletingLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var locationTags = location.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationTags,
                L("LocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationTags_Create,
                L("CreateNewLocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationTags_Edit,
                L("EditLocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTags.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Location_LocationTags_Delete,
                L("DeleteLocationTag"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Location

            #region Master

            var paymentTypes = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentTypes,
                L("PaymentType"));
            paymentTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentTypes_Create,
                L("CreateNewPaymentType"));
            paymentTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentTypes_Edit,
                L("EditPaymentType"));
            paymentTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentTypes_Delete,
                L("DeletePaymentType"));

            var dinePlanTaxes = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTaxes,
                L("Tax"), multiTenancySides: MultiTenancySides.Tenant);
            dinePlanTaxes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTaxes_Create,
                L("CreateNewDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);
            dinePlanTaxes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTaxes_Edit,
                L("EditDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);
            dinePlanTaxes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTaxes_Delete,
                L("DeleteDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);

            var departments = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Departments,
                L("Department"), multiTenancySides: MultiTenancySides.Tenant);
            departments.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Departments_Create,
                L("CreateNewDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            departments.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Departments_Edit,
                L("EditDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            departments.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Departments_Delete,
                L("DeleteDepartment"), multiTenancySides: MultiTenancySides.Tenant);

            var departmentgroupPermission = master.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup, L("DepartmentGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Delete, L("DeleteDepartmentGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Edit, L("EditDepartmentGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Create, L("CreateDepartmentGroup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var futureDateInformations = master.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_FutureDateInformations, L("FutureDateInformations"),
                multiTenancySides: MultiTenancySides.Tenant);
            futureDateInformations.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_FutureDateInformations_Create,
                L("CreateNewFutureDateInformation"), multiTenancySides: MultiTenancySides.Tenant);
            futureDateInformations.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_FutureDateInformations_Edit, L("EditFutureDateInformation"),
                multiTenancySides: MultiTenancySides.Tenant);
            futureDateInformations.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_FutureDateInformations_Delete,
                L("DeleteFutureDateInformation"), multiTenancySides: MultiTenancySides.Tenant);

            var terminals = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Terminals,
                L("Terminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminals.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Terminals_Create,
                L("CreateNewTerminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminals.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Terminals_Edit,
                L("EditTerminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminals.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Terminals_Delete,
                L("DeleteTerminal"), multiTenancySides: MultiTenancySides.Tenant);

            //var printTemplates = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PrintTemplates, L("PrintTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            //printTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PrintTemplates_Create, L("CreateNewPrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            //printTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PrintTemplates_Edit, L("EditPrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            //printTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PrintTemplates_Delete, L("DeletePrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            var calculations = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Calculations,
                L("Calculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Calculations_Create,
                L("CreateNewCalculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Calculations_Edit,
                L("EditCalculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculations.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Calculations_Delete,
                L("DeleteCalculation"), multiTenancySides: MultiTenancySides.Tenant);

            var foreignCurrencies = master.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrencies, L("ForeignCurrency"),
                multiTenancySides: MultiTenancySides.Tenant);
            foreignCurrencies.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrencies_Create,
                L("CreateNewForeignCurrency"), multiTenancySides: MultiTenancySides.Tenant);
            foreignCurrencies.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrencies_Edit,
                L("EditForeignCurrency"), multiTenancySides: MultiTenancySides.Tenant);
            foreignCurrencies.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrencies_Delete,
                L("DeleteForeignCurrency"), multiTenancySides: MultiTenancySides.Tenant);

            var planReasons = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReasons,
                L("PlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planReasons.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReasons_Create,
                L("CreateNewPlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planReasons.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReasons_Edit,
                L("EditPlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planReasons.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReasons_Delete,
                L("DeletePlanReason"), multiTenancySides: MultiTenancySides.Tenant);

            var transactionTypes = master.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_TransactionTypes, L("TransactionType"),
                multiTenancySides: MultiTenancySides.Tenant);
            transactionTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TransactionTypes_Create,
                L("CreateNewTransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactionTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TransactionTypes_Edit,
                L("EditTransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactionTypes.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TransactionTypes_Delete,
                L("DeleteTransactionType"), multiTenancySides: MultiTenancySides.Tenant);

            var tillAccounts = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccounts,
                L("TillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillAccounts.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccounts_Create,
                L("CreateNewTillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillAccounts.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccounts_Edit,
                L("EditTillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillAccounts.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccounts_Delete,
                L("DeleteTillAccount"), multiTenancySides: MultiTenancySides.Tenant);

            var locationsInGroup = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Locations,
                L("Location"), multiTenancySides: MultiTenancySides.Tenant);
            locationsInGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Locations_Create,
                L("CreatingNewLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locationsInGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Locations_Edit,
                L("EditingLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locationsInGroup.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Locations_Delete,
                L("DeletingLocation"), multiTenancySides: MultiTenancySides.Tenant);

            var numeratorPermission = master.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator,
                L("Numerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Delete,
                L("DeleteNumerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Edit,
                L("EditNumerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Create,
                L("CreateNumerator"), multiTenancySides: MultiTenancySides.Tenant);

            var tickettypePermission = master.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_TicketType, L("TicketType"),
                multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Delete,
                L("DeleteTicketType"), multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Edit,
                L("EditTicketType"), multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Create,
                L("CreateTicketType"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Master

            #region Menu

            var menu = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu, L("Menu"),
                multiTenancySides: MultiTenancySides.Tenant);

            var productGroups = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroups,
                L("ProductGroup"), multiTenancySides: MultiTenancySides.Tenant);
            productGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroups_Create,
                L("CreateNewProductGroup"), multiTenancySides: MultiTenancySides.Tenant);
            productGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroups_Edit,
                L("EditProductGroup"), multiTenancySides: MultiTenancySides.Tenant);
            productGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroups_Delete,
                L("DeleteProductGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var categories = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Categories,
                L("Category"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Categories_Create,
                L("CreatingNewCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Categories_Edit,
                L("EditingCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categories.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Categories_Delete,
                L("DeletingCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var screenMenu = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenus,
                L("ScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);
            screenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenus_Create,
                L("CreateScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            screenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenus_Edit,
                L("EditScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            screenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenus_Delete,
                L("DeleteScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var menuItems = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItems,
                L("MenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuItems.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItems_Create,
                L("CreatingNewMenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuItems.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItems_Edit,
                L("EditingMenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuItems.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItems_Delete,
                L("DeletingMenuItem"), multiTenancySides: MultiTenancySides.Tenant);

            var comboGroups = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroups,
                L("ComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroups_Create,
                L("CreateComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroups_Edit,
                L("EditComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroups.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroups_Delete,
                L("DeleteComboGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var menuLocationPrices = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_LocationPrices,
                L("LocationPrices"), multiTenancySides: MultiTenancySides.Tenant);
            menuLocationPrices.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_LocationPrices_Edit,
                L("EditLocationPrices"), multiTenancySides: MultiTenancySides.Tenant);

            var menuLocationMenus = menu.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_LocationMenus,
                L("LocationMenus"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Menu

            #region Print

            var print = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print, L("Print"),
                multiTenancySides: MultiTenancySides.Tenant);

            var printer = print.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_Printers, L("Printer"),
                multiTenancySides: MultiTenancySides.Tenant);

            printer.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_Printers_Create, L("CreatePrinter"),
                multiTenancySides: MultiTenancySides.Tenant);
            printer.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_Printers_Edit, L("EditPrinter"),
                multiTenancySides: MultiTenancySides.Tenant);
            printer.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_Printers_Delete, L("DeletePrinter"),
                multiTenancySides: MultiTenancySides.Tenant);

            var printerTemplates = print.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintTemplates,
                L("Template"), multiTenancySides: MultiTenancySides.Tenant);

            printerTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintTemplates_Create,
                L("CreateTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            printerTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintTemplates_Edit,
                L("EditTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            printerTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintTemplates_Delete,
                L("DeleteTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            var printerJob = print.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintJobs, L("Job"),
                multiTenancySides: MultiTenancySides.Tenant);

            printerJob.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintJobs_Create, L("CreateJob"),
                multiTenancySides: MultiTenancySides.Tenant);
            printerJob.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintJobs_Edit, L("EditJob"),
                multiTenancySides: MultiTenancySides.Tenant);
            printerJob.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintJobs_Delete, L("DeleteJob"),
                multiTenancySides: MultiTenancySides.Tenant);

            var printerCondition = print.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Print_PrintConditions, L("Condition"),
                multiTenancySides: MultiTenancySides.Tenant);

            printerCondition.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintConditions_Create,
                L("CreateCondition"), multiTenancySides: MultiTenancySides.Tenant);
            printerCondition.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintConditions_Edit,
                L("EditCondition"), multiTenancySides: MultiTenancySides.Tenant);
            printerCondition.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Print_PrintConditions_Delete,
                L("DeleteCondition"), multiTenancySides: MultiTenancySides.Tenant);

            var dinedevicePermission = print.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Display_DineDevice, L("DineDevice"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Delete,
                L("DeleteDineDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Edit,
                L("EditDineDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Create,
                L("CreateDineDevice"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Print

            #region ProgramSettings

            var programSetting = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting,
                L("ProgramSetting"), multiTenancySides: MultiTenancySides.Tenant);

            var programSettingTemplates = programSetting.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_ProgramSetting_Templates, L("Templates"),
                multiTenancySides: MultiTenancySides.Tenant);

            programSettingTemplates.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_ProgramSetting_Templates_Create, L("CreateTemplate"),
                multiTenancySides: MultiTenancySides.Tenant);
            programSettingTemplates.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_ProgramSetting_Templates_Edit, L("EditTemplate"),
                multiTenancySides: MultiTenancySides.Tenant);
            programSettingTemplates.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_ProgramSetting_Templates_Delete, L("DeleteTemplate"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion ProgramSettings

            #region Table

            var tablePermission = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table,
                L("Table"),
                multiTenancySides: MultiTenancySides.Tenant);

            var connectGroupPerm =
                tablePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group,
                    L("ConnectTableGroup"),
                    multiTenancySides: MultiTenancySides.Tenant);

            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Create,
                L("CreateConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);
            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Edit,
                L("EditConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);
            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Delete,
                L("DeleteConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var tablesPerm =
                tablePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables, L("Tables"),
                    multiTenancySides: MultiTenancySides.Tenant);

            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Create,
                L("CreateConnectTables"), multiTenancySides: MultiTenancySides.Tenant);
            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Edit,
                L("EditConnectTables"), multiTenancySides: MultiTenancySides.Tenant);
            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Delete,
                L("DeleteConnectTables"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region DinePlan User

            var userPermissions = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User,
                L("User"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dineplanuserPermission =
                userPermissions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser,
                    L("DinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Delete,
                L("DeleteDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Edit,
                L("EditDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Create,
                L("CreateDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);

            var dineplanuserrolePermission =
                userPermissions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole,
                    L("DinePlanUserRole"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Delete, L("DeleteDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Edit, L("EditDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Create, L("CreateDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Card

            var connectCardPermission = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card,
                L("Card"),
                multiTenancySides: MultiTenancySides.Tenant);
            var connectCardsPermission = connectCardPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Card_Cards, L("ConnectCard"),
                multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Import,
                L("ImportConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Delete,
                L("DeleteConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Edit,
                L("EditConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Create,
                L("CreateConnectCard"), multiTenancySides: MultiTenancySides.Tenant);

            var connectCardTypePermission =
                connectCardPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType,
                    L("ConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Delete,
                L("DeleteConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Edit,
                L("EditConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Create,
                L("CreateConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);

            var connectcardtypecategoryPermission = connectCardPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory, L("ConnectCardTypeCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Delete,
                L("DeleteConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Edit, L("EditConnectCardTypeCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Create,
                L("CreateConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Report

            var reportConnnect = connect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report, L("Report"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Ticket, L("Ticket"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TicketSync, L("TicketSync"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TicketHourlySales, L("TicketHourlySales"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Order, L("Order"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Department, L("Department"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Department_Summary, L("DepartmentSummary"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Department_Items, L("DepartmentItems"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Item, L("Item"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Group, L("Group"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Category, L("Category"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Promotion, L("Promotion"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Sale, L("Sale"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Schedule, L("Schedules"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_ScheduleItems, L("Items"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_ScheduleLocations, L("Locations"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Log, L("Log"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TillTransaction, L("TillTransaction"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Tender, L("Tender"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SummaryOther, L("Summary"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_PaymentExcess, L("PaymentExcess"), multiTenancySides: MultiTenancySides.Tenant);
            reportConnnect.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Other, L("Other"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #endregion Connect

            #region Tiffins

            var tiffin = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin, L("Tiffin"),
                multiTenancySides: MultiTenancySides.Tenant);

            var manage = tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage, L("Manage"),
                multiTenancySides: MultiTenancySides.Tenant);

            var tiffinOrderTags = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_OrderTags,
                L("OrderTag"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinOrderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_OrderTags_Create,
                L("CreateNewOrderTag"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinOrderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_OrderTags_Edit,
                L("EditOrderTag"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinOrderTags.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_OrderTags_Delete,
                L("DeleteOrderTag"), multiTenancySides: MultiTenancySides.Tenant);

            var tiffinMealTimes = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_MealTimes,
                L("MealTime"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinMealTimes.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_MealTimes_Create,
                L("CreateNewMealTime"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinMealTimes.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_MealTimes_Edit,
                L("EditMealTime"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinMealTimes.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_MealTimes_Delete,
                L("DeleteMealTime"), multiTenancySides: MultiTenancySides.Tenant);

            var productSets = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductSets,
                L("ProductSet"), multiTenancySides: MultiTenancySides.Tenant);
            productSets.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductSets_Create,
                L("CreateNewProductSet"), multiTenancySides: MultiTenancySides.Tenant);
            productSets.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductSets_Edit,
                L("EditProductSet"), multiTenancySides: MultiTenancySides.Tenant);
            productSets.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductSets_Delete,
                L("DeleteProductSet"), multiTenancySides: MultiTenancySides.Tenant);

            var tiffinProductOffers = manage.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOffers, L("ProductOffer"),
                multiTenancySides: MultiTenancySides.Tenant);
            tiffinProductOffers.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOffers_Create,
                L("CreateNewProductOffer"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinProductOffers.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOffers_Edit,
                L("EditProductOffer"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinProductOffers.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOffers_Delete,
                L("DeleteProductOffer"), multiTenancySides: MultiTenancySides.Tenant);

            var schdules = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Schedules,
                L("Schedule"), multiTenancySides: MultiTenancySides.Tenant);
            schdules.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Schedules_Delete, L("Delete"),
                multiTenancySides: MultiTenancySides.Tenant);
            schdules.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Schedules_EditCalender,
                L("EditCalender"), multiTenancySides: MultiTenancySides.Tenant);
            schdules.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Schedules_EditSchedule,
                L("EditSchedule"), multiTenancySides: MultiTenancySides.Tenant);

            var tiffinProductOfferTypes =
                manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOfferTypes,
                    L("TiffinProductOfferType"));
            tiffinProductOfferTypes.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Create,
                L("CreateNewTiffinProductOfferType"));
            tiffinProductOfferTypes.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Edit, L("EditTiffinProductOfferType"));
            tiffinProductOfferTypes.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Delete, L("DeleteTiffinProductOfferType"));

            var tiffinFaq = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Faq, L("TiffinFaq"));
            tiffinFaq.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Faq_Create,
                L("CreateNewTiffinFaq"));
            tiffinFaq.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Faq_Edit, L("EditTiffinFaq"));
            tiffinFaq.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Faq_Delete, L("DeleteTiffinFaq"));

            var tiffinHomeBanner = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_HomeBanner,
                L("TiffinHomeBanner"));
            tiffinHomeBanner.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_HomeBanner_Edit,
                L("EditTiffinHomeBanner"));

            var tiffinPromotion = manage.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Promotion,
                L("TiffinPromotion"));
            tiffinPromotion.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Promotion_Create,
                L("CreateNewTiffinPromotion"));
            tiffinPromotion.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Promotion_Edit,
                L("EditTiffinPromotion"));
            tiffinPromotion.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Manage_Promotion_Delete,
                L("DeleteTiffinPromotion"));

            // customer
            var customers = tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Customer, L("Customer"),
                multiTenancySides: MultiTenancySides.Tenant);

            var customer = customers.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Customer_Customers,
                L("Customer"), multiTenancySides: MultiTenancySides.Tenant);
            customer.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Customer_Customers_Create,
                L("CreateNewCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            customer.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Customer_Customers_Edit,
                L("EditCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            customer.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Customer_Customers_Delete, L("Delete"),
                multiTenancySides: MultiTenancySides.Tenant);

            var tiffinCustomerTransactions = customers.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Customer_Transactions, L("CustomerTransaction"),
                multiTenancySides: MultiTenancySides.Tenant);
            tiffinCustomerTransactions.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Customer_Transactions_Create, L("CreateNewCustomerTransaction"),
                multiTenancySides: MultiTenancySides.Tenant);
            tiffinCustomerTransactions.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Customer_Transactions_Edit, L("EditCustomerTransaction"),
                multiTenancySides: MultiTenancySides.Tenant);
            tiffinCustomerTransactions.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tiffin_Customer_Transactions_Delete, L("DeleteCustomerTransaction"),
                multiTenancySides: MultiTenancySides.Tenant);

            var report = tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report, L("Report"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_OrderSummary, L("OrderSummary"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_Preparation, L("Preparation"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_Balance, L("Balance"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_Driver, L("Driver"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_Payment, L("Payment"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_Kitchen, L("Kitchen"),
                multiTenancySides: MultiTenancySides.Tenant);
            report.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_Report_TiffinTransaction,
                L("TiffinTransaction"), multiTenancySides: MultiTenancySides.Tenant);

            var tiffinScreenMenu = tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_ScreenMenus,
                L("ScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinScreenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_ScreenMenus_Create,
                L("CreateScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinScreenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_ScreenMenus_Edit,
                L("EditScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinScreenMenu.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_ScreenMenus_Delete,
                L("DeleteScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var tiffinTimeslot = tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_TimeSlot,
                L("TimeSlot"), multiTenancySides: MultiTenancySides.Tenant);
            tiffinTimeslot.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_TimeSlot_Edit, L("EditTimeSlots"),
                multiTenancySides: MultiTenancySides.Tenant);
            tiffinTimeslot.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_TimeSlot_Delete, L("Delete"),
                multiTenancySides: MultiTenancySides.Tenant);

            tiffin.CreateChildPermission(AppPermissions.Pages_Tenant_Tiffin_OrderCollected,
                L("OrderCollected"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Tiffins

            #region Wheel

            var wheelPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Wheel, L("Wheel"),
                multiTenancySides: MultiTenancySides.Tenant);
            var wheelSetups = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelSetups, L("WheelSetups"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelSetups.CreateChildPermission(AppPermissions.Pages_WheelSetups_Create, L("CreateNewWheelSetup"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelSetups.CreateChildPermission(AppPermissions.Pages_WheelSetups_Edit, L("EditWheelSetup"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelSetups.CreateChildPermission(AppPermissions.Pages_WheelSetups_Delete, L("DeleteWheelSetup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var wheelDepartments = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelDepartments,
                L("WheelDepartments"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDepartments.CreateChildPermission(AppPermissions.Pages_WheelDepartments_Create,
                L("CreateNewWheelDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDepartments.CreateChildPermission(AppPermissions.Pages_WheelDepartments_Edit, L("EditWheelDepartment"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelDepartments.CreateChildPermission(AppPermissions.Pages_WheelDepartments_Delete,
                L("DeleteWheelDepartment"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelLocations = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelLocations,
                L("WheelLocations"), multiTenancySides: MultiTenancySides.Tenant);
            wheelLocations.CreateChildPermission(AppPermissions.Pages_WheelLocations_Create,
                L("CreateNewWheelLocation"), multiTenancySides: MultiTenancySides.Tenant);
            wheelLocations.CreateChildPermission(AppPermissions.Pages_WheelLocations_Edit, L("EditWheelLocation"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelLocations.CreateChildPermission(AppPermissions.Pages_WheelLocations_Delete, L("DeleteWheelLocation"),
                multiTenancySides: MultiTenancySides.Tenant);

            var wheelTaxes = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelTaxes, L("WheelTaxes"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTaxes.CreateChildPermission(AppPermissions.Pages_WheelTaxes_Create, L("CreateNewWheelTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTaxes.CreateChildPermission(AppPermissions.Pages_WheelTaxes_Edit, L("EditWheelTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTaxes.CreateChildPermission(AppPermissions.Pages_WheelTaxes_Delete, L("DeleteWheelTax"),
                multiTenancySides: MultiTenancySides.Tenant);

            var wheelPaymentMethods = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelPaymentMethods,
                L("WheelPaymentMethods"), multiTenancySides: MultiTenancySides.Tenant);
            wheelPaymentMethods.CreateChildPermission(AppPermissions.Pages_WheelPaymentMethods_Create,
                L("CreateNewWheelPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            wheelPaymentMethods.CreateChildPermission(AppPermissions.Pages_WheelPaymentMethods_Edit,
                L("EditWheelPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);
            wheelPaymentMethods.CreateChildPermission(AppPermissions.Pages_WheelPaymentMethods_Delete,
                L("DeleteWheelPaymentMethod"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelDeliveryDurations = wheelPermission.CreateChildPermission(
                AppPermissions.Pages_WheelDeliveryDurations, L("WheelDeliveryDurations"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelDeliveryDurations.CreateChildPermission(AppPermissions.Pages_WheelDeliveryDurations_Create,
                L("CreateNewWheelDeliveryDuration"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDeliveryDurations.CreateChildPermission(AppPermissions.Pages_WheelDeliveryDurations_Edit,
                L("EditWheelDeliveryDuration"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDeliveryDurations.CreateChildPermission(AppPermissions.Pages_WheelDeliveryDurations_Delete,
                L("DeleteWheelDeliveryDuration"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelDynamicPages = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelDynamicPages,
                L("WheelDynamicPages"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDynamicPages.CreateChildPermission(AppPermissions.Pages_WheelDynamicPages_Create,
                L("CreateNewWheelDynamicPages"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDynamicPages.CreateChildPermission(AppPermissions.Pages_WheelDynamicPages_Edit,
                L("EditWheelDynamicPages"), multiTenancySides: MultiTenancySides.Tenant);
            wheelDynamicPages.CreateChildPermission(AppPermissions.Pages_WheelDynamicPages_Delete,
                L("DeleteWheelDynamicPages"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelScreenMenus = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelScreenMenus,
                L("WheelScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);
            wheelScreenMenus.CreateChildPermission(AppPermissions.Pages_WheelScreenMenus_Create,
                L("CreateNewWheelScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);
            wheelScreenMenus.CreateChildPermission(AppPermissions.Pages_WheelScreenMenus_Edit,
                L("EditWheelScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);
            wheelScreenMenus.CreateChildPermission(AppPermissions.Pages_WheelScreenMenus_Delete,
                L("DeleteWheelScreenMenus"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelTables = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelTables, L("WheelTables"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTables.CreateChildPermission(AppPermissions.Pages_WheelTables_Create, L("CreateNewWheelTables"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTables.CreateChildPermission(AppPermissions.Pages_WheelTables_Edit, L("EditWheelTables"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelTables.CreateChildPermission(AppPermissions.Pages_WheelTables_Delete, L("DeleteWheelTables"),
                multiTenancySides: MultiTenancySides.Tenant);

            var wheelFaqs = wheelPermission.CreateChildPermission(AppPermissions.Pages_Wheel_Faq, L("FAQ"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelFaqs.CreateChildPermission(AppPermissions.Pages_Wheel_Faq_Create, L("CreateNewTiffinFaq"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelFaqs.CreateChildPermission(AppPermissions.Pages_Wheel_Faq_Edit, L("EditTiffinFaq"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelFaqs.CreateChildPermission(AppPermissions.Pages_Wheel_Faq_Delete, L("DeleteTiffinFaq"),
                multiTenancySides: MultiTenancySides.Tenant);


            var wheelProductSoldOut = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelProductSoldOut,
                L("WheelProductSoldout"), multiTenancySides: MultiTenancySides.Tenant);
            wheelProductSoldOut.CreateChildPermission(AppPermissions.Pages_WheelProductSoldOut_Create,
                L("CreateNewWheelProductSoldout"), multiTenancySides: MultiTenancySides.Tenant);
            wheelProductSoldOut.CreateChildPermission(AppPermissions.Pages_WheelProductSoldOut_Edit,
                L("EditWheelProductSoldout"), multiTenancySides: MultiTenancySides.Tenant);
            wheelProductSoldOut.CreateChildPermission(AppPermissions.Pages_WheelProductSoldOut_Delete,
                L("DeleteWheelProductSoldout"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelRecommendationMenu = wheelPermission.CreateChildPermission(
                AppPermissions.Pages_WheelRecommendationMenu, L("wheelRecommendationMenu"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelRecommendationMenu.CreateChildPermission(AppPermissions.Pages_WheelRecommendationMenu_Create,
                L("CreateNewwheelRecommendationMenu"), multiTenancySides: MultiTenancySides.Tenant);
            wheelRecommendationMenu.CreateChildPermission(AppPermissions.Pages_WheelRecommendationMenu_Edit,
                L("EditwheelRecommendationMenu"), multiTenancySides: MultiTenancySides.Tenant);
            wheelRecommendationMenu.CreateChildPermission(AppPermissions.Pages_WheelRecommendationMenu_Delete,
                L("DeletewheelRecommendationMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var manageFlyer = wheelPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Flyer_Manage_Flyer,
                L("Flyer"), multiTenancySides: MultiTenancySides.Tenant);
            manageFlyer.CreateChildPermission(AppPermissions.Pages_Tenant_Flyer_Manage_Flyer_Create, L("CreateFlyer"),
                multiTenancySides: MultiTenancySides.Tenant);
            manageFlyer.CreateChildPermission(AppPermissions.Pages_Tenant_Flyer_Manage_Flyer_Edit, L("EditFlyer"),
                multiTenancySides: MultiTenancySides.Tenant);
            manageFlyer.CreateChildPermission(AppPermissions.Pages_Tenant_Flyer_Manage_Flyer_Delete, L("DeleteFlyer"),
                multiTenancySides: MultiTenancySides.Tenant);


            var wheelPromotions = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelPromotions,
                L("WheelPromotions"), multiTenancySides: MultiTenancySides.Tenant);
            var wheelGeneralSettings = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelGeneralSettings,
                L("WheelGeneralSettings"), multiTenancySides: MultiTenancySides.Tenant);
            var wheelDeliveryZone = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelDeliveryZone,
                L("WheelDeliveryZone"), multiTenancySides: MultiTenancySides.Tenant);
            var wheelServiceFee = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelServiceFee,
                L("WheelServiceFee"), multiTenancySides: MultiTenancySides.Tenant);
            var wheelOpeningHour = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelOpeningHour,
                L("WheelOpeningHour"), multiTenancySides: MultiTenancySides.Tenant);

            var wheelReserve = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelReserve,
                L("WheelReserve"), multiTenancySides: MultiTenancySides.Tenant);
            wheelReserve.CreateChildPermission(AppPermissions.Pages_WheelReserve_SeatMap, L("SeatMap"),
                multiTenancySides: MultiTenancySides.Tenant);
            wheelReserve.CreateChildPermission(AppPermissions.Pages_WheelReserve_Reservation, L("Reservation"),
                multiTenancySides: MultiTenancySides.Tenant);

            var wheelReport = wheelPermission.CreateChildPermission(AppPermissions.Pages_WheelReport, L("WheelReport"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Wheel


            #region Queue

            var queuePermission = wheelPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Queue, L("Queue"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Flyer

            #endregion
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DineConnectConsts.LocalizationSourceName);
        }
    }
}
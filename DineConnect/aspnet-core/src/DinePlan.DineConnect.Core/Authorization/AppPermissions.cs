﻿namespace DinePlan.DineConnect.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
                #region Core

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";

        public const string Pages_Administration_Users_ChangePermissions =
            "Pages.Administration.Users.ChangePermissions";

        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicParameters = "Pages.Administration.DynamicParameters";
        public const string Pages_Administration_DynamicParameters_Create = "Pages.Administration.DynamicParameters.Create";
        public const string Pages_Administration_DynamicParameters_Edit = "Pages.Administration.DynamicParameters.Edit";
        public const string Pages_Administration_DynamicParameters_Delete = "Pages.Administration.DynamicParameters.Delete";

        public const string Pages_Administration_DynamicParameterValue = "Pages.Administration.DynamicParameterValue";
        public const string Pages_Administration_DynamicParameterValue_Create = "Pages.Administration.DynamicParameterValue.Create";
        public const string Pages_Administration_DynamicParameterValue_Edit = "Pages.Administration.DynamicParameterValue.Edit";
        public const string Pages_Administration_DynamicParameterValue_Delete = "Pages.Administration.DynamicParameterValue.Delete";

        public const string Pages_Administration_EntityDynamicParameters = "Pages.Administration.EntityDynamicParameters";
        public const string Pages_Administration_EntityDynamicParameters_Create = "Pages.Administration.EntityDynamicParameters.Create";
        public const string Pages_Administration_EntityDynamicParameters_Edit = "Pages.Administration.EntityDynamicParameters.Edit";
        public const string Pages_Administration_EntityDynamicParameters_Delete = "Pages.Administration.EntityDynamicParameters.Delete";

        public const string Pages_Administration_EntityDynamicParameterValue = "Pages.Administration.EntityDynamicParameterValue";
        public const string Pages_Administration_EntityDynamicParameterValue_Create = "Pages.Administration.EntityDynamicParameterValue.Create";
        public const string Pages_Administration_EntityDynamicParameterValue_Edit = "Pages.Administration.EntityDynamicParameterValue.Edit";
        public const string Pages_Administration_EntityDynamicParameterValue_Delete = "Pages.Administration.EntityDynamicParameterValue.Delete";
        public const string Pages_Administration_Terms = "Pages.Administration.Terms";
        public const string Pages_Administration_Terms_Create = "Pages.Administration.Terms.Create";
        public const string Pages_Administration_Terms_Edit = "Pages.Administration.Terms.Edit";
        public const string Pages_Administration_Terms_Delete = "Pages.Administration.Terms.Delete";

        public const string Pages_Administration_UserLocations = "Pages.Administration.UserLocation";
        public const string Pages_Administration_UserLocations_Create = "Pages.Administration.UserLocation.Create";
        public const string Pages_Administration_UserLocations_Edit = "Pages.Administration.UserLocation.Edit";
        public const string Pages_Administration_UserLocations_Delete = "Pages.Administration.UserLocation.Delete";

        public const string Pages_Syncers = "Pages.Syncers";
        public const string Pages_Syncers_Create = "Pages.Syncers.Create";
        public const string Pages_Syncers_Edit = "Pages.Syncers.Edit";
        public const string Pages_Syncers_Delete = "Pages.Syncers.Delete";

        public const string Pages_LastSyncs = "Pages.LastSyncs";
        public const string Pages_LastSyncs_Create = "Pages.LastSyncs.Create";
        public const string Pages_LastSyncs_Edit = "Pages.LastSyncs.Edit";
        public const string Pages_LastSyncs_Delete = "Pages.LastSyncs.Delete";

        public const string Pages_LocationSyncers = "Pages.LocationSyncers";
        public const string Pages_LocationSyncers_Create = "Pages.LocationSyncers.Create";
        public const string Pages_LocationSyncers_Edit = "Pages.LocationSyncers.Edit";
        public const string Pages_LocationSyncers_Delete = "Pages.LocationSyncers.Delete";

       
        public const string Pages_Administration_LanguageDescriptions = "Pages.Administration.LanguageDescriptions";
        public const string Pages_Administration_LanguageDescriptions_Create = "Pages.Administration.LanguageDescriptions.Create";
        public const string Pages_Administration_LanguageDescriptions_Edit = "Pages.Administration.LanguageDescriptions.Edit";
        public const string Pages_Administration_LanguageDescriptions_Delete = "Pages.Administration.LanguageDescriptions.Delete";

      


        #endregion Core

        #region Host

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

        public const string Pages_Administration_Host_TenantDatabase = "Pages.Administration.Host.TenantDatabase";
        public const string Pages_Administration_Host_TenantDatabase_Create = "Pages.Administration.Host.TenantDatabase.Create";
        public const string Pages_Administration_Host_TenantDatabase_Edit = "Pages.Administration.Host.TenantDatabase.Edit";
        public const string Pages_Administration_Host_TenantDatabase_Delete = "Pages.Administration.Host.TenantDatabase.Delete";

        public const string Pages_Administration_Origins = "Pages.Administration.Host.Origins";
        public const string Pages_Administration_Origins_Create = "Pages.Administration.Host.Origins.Create";
        public const string Pages_Administration_Origins_Edit = "Pages.Administration.Host.Origins.Edit";
        public const string Pages_Administration_Origins_Delete = "Pages.Administration.Host.Origins.Delete";

        #endregion Host

        #region Tenant

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        public const string Pages_Administration_Tenant_AddonSettings = "Pages.Administration.Tenant.AddonSettings";

        public const string Pages_Administration_Tenant_TemplateEngines = "Pages.Administration.Tenant.TemplateEngines";
        public const string Pages_Administration_Tenant_TemplateEngines_Create = "Pages.Administration.Tenant.TemplateEngines.Create";
        public const string Pages_Administration_Tenant_TemplateEngines_Edit = "Pages.Administration.Tenant.TemplateEngines.Edit";
        public const string Pages_Administration_Tenant_TemplateEngines_Delete = "Pages.Administration.Tenant.TemplateEngines.Delete";

        #endregion Tenant

        #region Connect

        public const string Pages_Tenant_Connect = "Pages.Tenant.Connect";

        #region Address

        public const string Pages_Tenant_Connect_Address = "Pages.Tenant.Connect.Address";

        public const string Pages_Tenant_Connect_Address_Countries = "Pages.Tenant.Connect.Address.Countries";
        public const string Pages_Tenant_Connect_Address_Countries_Create = "Pages.Tenant.Connect.Address.Countries.Create";
        public const string Pages_Tenant_Connect_Address_Countries_Edit = "Pages.Tenant.Connect.Address.Countries.Edit";
        public const string Pages_Tenant_Connect_Address_Countries_Delete = "Pages.Tenant.Connect.Address.Countries.Delete";

        public const string Pages_Tenant_Connect_Address_States = "Pages.Tenant.Connect.Address.States";
        public const string Pages_Tenant_Connect_Address_States_Create = "Pages.Tenant.Connect.Address.States.Create";
        public const string Pages_Tenant_Connect_Address_States_Edit = "Pages.Tenant.Connect.Address.States.Edit";
        public const string Pages_Tenant_Connect_Address_States_Delete = "Pages.Tenant.Connect.Address.States.Delete";

        public const string Pages_Tenant_Connect_Address_Cities = "Pages.Tenant.Connect.Address.Cities";
        public const string Pages_Tenant_Connect_Address_Cities_Create = "Pages.Tenant.Connect.Address.Cities.Create";
        public const string Pages_Tenant_Connect_Address_Cities_Delete = "Pages.Tenant.Connect.Address.Cities.Delete";
        public const string Pages_Tenant_Connect_Address_Cities_Edit = "Pages.Tenant.Connect.Address.Cities.Edit";

        #endregion Address

        #region Location

        public const string Pages_Tenant_Connect_Location = "Pages.Tenant.Connect.Location";

        public const string Pages_Tenant_Connect_Location_Locations = "Pages.Tenant.Connect.Location.Locations";
        public const string Pages_Tenant_Connect_Location_Locations_Create = "Pages.Tenant.Connect.Location.Locations.Create";
        public const string Pages_Tenant_Connect_Location_Locations_Delete = "Pages.Tenant.Connect.Location.Locations.Delete";
        public const string Pages_Tenant_Connect_Location_Locations_Edit = "Pages.Tenant.Connect.Location.Locations.Edit";

        public const string Pages_Tenant_Connect_Location_LocationGroups = "Pages.Tenant.Connect.Location.LocationGroups";
        public const string Pages_Tenant_Connect_Location_LocationGroups_Create = "Pages.Tenant.Connect.Location.LocationGroups.Create";
        public const string Pages_Tenant_Connect_Location_LocationGroups_Delete = "Pages.Tenant.Connect.Location.LocationGroups.Delete";
        public const string Pages_Tenant_Connect_Location_LocationGroups_Edit = "Pages.Tenant.Connect.Location.LocationGroups.Edit";

        public const string Pages_Tenant_Connect_Location_LocationTags = "Pages.Tenant.Connect.Location.LocationTags";
        public const string Pages_Tenant_Connect_Location_LocationTags_Create = "Pages.Tenant.Connect.Location.LocationTags.Create";
        public const string Pages_Tenant_Connect_Location_LocationTags_Edit = "Pages.Tenant.Connect.Location.LocationTags.Edit";
        public const string Pages_Tenant_Connect_Location_LocationTags_Delete = "Pages.Tenant.Connect.Location.LocationTags.Delete";

        public const string Pages_Tenant_Connect_Location_Brands = "Pages.Tenant.Connect.Location.Brands";
        public const string Pages_Tenant_Connect_Location_Brands_Create = "Pages.Tenant.Connect.Location.Brands.Create";
        public const string Pages_Tenant_Connect_Location_Brands_Delete = "Pages.Tenant.Connect.Location.Brands.Delete";
        public const string Pages_Tenant_Connect_Location_Brands_Edit = "Pages.Tenant.Connect.Location.Brands.Edit";

        #endregion Location

        #region Tag

        public const string Pages_Tenant_Connect_Tag = "Pages.Tenant.Connect.Tag";

        public const string Pages_Tenant_Connect_Tag_PriceTags = "Pages.Tenant.Connect.Tag.PriceTags";
        public const string Pages_Tenant_Connect_Tag_PriceTags_Create = "Pages.Tenant.Connect.Tag.PriceTags.Create";
        public const string Pages_Tenant_Connect_Tag_PriceTags_Edit = "Pages.Tenant.Connect.Tag.PriceTags.Edit";
        public const string Pages_Tenant_Connect_Tag_PriceTags_Delete = "Pages.Tenant.Connect.Tag.PriceTags.Delete";

        public const string Pages_Tenant_Connect_Tag_TicketTagGroups = "Pages.Tenant.Connect.Tag.TicketTagGroups";
        public const string Pages_Tenant_Connect_Tag_TicketTagGroups_Create = "Pages.Tenant.Connect.Tag.TicketTagGroups.Create";
        public const string Pages_Tenant_Connect_Tag_TicketTagGroups_Edit = "Pages.Tenant.Connect.Tag.TicketTagGroups.Edit";
        public const string Pages_Tenant_Connect_Tag_TicketTagGroups_Delete = "Pages.Tenant.Connect.Tag.TicketTagGroups.Delete";

        public const string Pages_Tenant_Connect_Tag_OrderTags = "Pages.Tenant.Connect.Tag.OrderTags";
        public const string Pages_Tenant_Connect_Tag_OrderTags_Create = "Pages.Tenant.Connect.Tag.OrderTags.Create";
        public const string Pages_Tenant_Connect_Tag_OrderTags_Edit = "Pages.Tenant.Connect.Tag.OrderTags.Edit";
        public const string Pages_Tenant_Connect_Tag_OrderTags_Delete = "Pages.Tenant.Connect.Tag.OrderTags.Delete";

        #endregion Tag

        #region Master

        public const string Pages_Tenant_Connect_Master = "Pages.Tenant.Connect.Master";

        public const string Pages_Tenant_Connect_Master_Departments = "Pages.Tenant.Connect.Master.Departments";
        public const string Pages_Tenant_Connect_Master_Departments_Create = "Pages.Tenant.Connect.Master.Departments.Create";
        public const string Pages_Tenant_Connect_Master_Departments_Edit = "Pages.Tenant.Connect.Master.Departments.Edit";
        public const string Pages_Tenant_Connect_Master_Departments_Delete = "Pages.Tenant.Connect.Master.Departments.Delete";

        public const string Pages_Tenant_Connect_Master_DepartmentGroup = "Pages.Tenant.Connect.Master.DepartmentGroup";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Create = "Pages.Tenant.Connect.Master.DepartmentGroup.Create";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Delete = "Pages.Tenant.Connect.Master.DepartmentGroup.Delete";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Edit = "Pages.Tenant.Connect.Master.DepartmentGroup.Edit";

        public const string Pages_Tenant_Connect_Master_FutureDateInformations = "Pages.Tenant.Connect.Master.FutureDateInformations";
        public const string Pages_Tenant_Connect_Master_FutureDateInformations_Create = "Pages.Tenant.Connect.Master.FutureDateInformations.Create";
        public const string Pages_Tenant_Connect_Master_FutureDateInformations_Edit = "Pages.Tenant.Connect.Master.FutureDateInformations.Edit";
        public const string Pages_Tenant_Connect_Master_FutureDateInformations_Delete = "Pages.Tenant.Connect.Master.FutureDateInformations.Delete";

        public const string Pages_Tenant_Connect_Master_Terminals = "Pages.Tenant.Connect.Master.Terminals";
        public const string Pages_Tenant_Connect_Master_Terminals_Create = "Pages.Tenant.Connect.Master.Terminals.Create";
        public const string Pages_Tenant_Connect_Master_Terminals_Edit = "Pages.Tenant.Connect.Master.Terminals.Edit";
        public const string Pages_Tenant_Connect_Master_Terminals_Delete = "Pages.Tenant.Connect.Master.Terminals.Delete";

        public const string Pages_Tenant_Connect_Master_PrintTemplates = "Pages.Tenant.Connect.Master.PrintTemplates";
        public const string Pages_Tenant_Connect_Master_PrintTemplates_Create = "Pages.Tenant.Connect.Master.PrintTemplates.Create";
        public const string Pages_Tenant_Connect_Master_PrintTemplates_Edit = "Pages.Tenant.Connect.Master.PrintTemplates.Edit";
        public const string Pages_Tenant_Connect_Master_PrintTemplates_Delete = "Pages.Tenant.Connect.Master.PrintTemplates.Delete";

        public const string Pages_Tenant_Connect_Master_PaymentTypes = "Pages.Tenant.Connect.Master.PaymentTypes";
        public const string Pages_Tenant_Connect_Master_PaymentTypes_Create = "Pages.Tenant.Connect.Master.PaymentTypes.Create";
        public const string Pages_Tenant_Connect_Master_PaymentTypes_Delete = "Pages.Tenant.Connect.Master.PaymentTypes.Delete";
        public const string Pages_Tenant_Connect_Master_PaymentTypes_Edit = "Pages.Tenant.Connect.Master.PaymentTypes.Edit";

        public const string Pages_Tenant_Connect_Master_ForeignCurrencies = "Pages.Tenant.Connect.Master.ForeignCurrencies";
        public const string Pages_Tenant_Connect_Master_ForeignCurrencies_Create = "Pages.Tenant.Connect.Master.ForeignCurrencies.Create";
        public const string Pages_Tenant_Connect_Master_ForeignCurrencies_Edit = "Pages.Tenant.Connect.Master.ForeignCurrencies.Edit";
        public const string Pages_Tenant_Connect_Master_ForeignCurrencies_Delete = "Pages.Tenant.Connect.Master.ForeignCurrencies.Delete";

        public const string Pages_Tenant_Connect_Master_PlanReasons = "Pages.Tenant.Connect.Master.PlanReasons";
        public const string Pages_Tenant_Connect_Master_PlanReasons_Create = "Pages.Tenant.Connect.Master.PlanReasons.Create";
        public const string Pages_Tenant_Connect_Master_PlanReasons_Edit = "Pages.Tenant.Connect.Master.PlanReasons.Edit";
        public const string Pages_Tenant_Connect_Master_PlanReasons_Delete = "Pages.Tenant.Connect.Master.PlanReasons.Delete";

        public const string Pages_Tenant_Connect_Master_TransactionTypes = "Pages.Tenant.Connect.Master.TransactionTypes";
        public const string Pages_Tenant_Connect_Master_TransactionTypes_Create = "Pages.Tenant.Connect.Master.TransactionTypes.Create";
        public const string Pages_Tenant_Connect_Master_TransactionTypes_Edit = "Pages.Tenant.Connect.Master.TransactionTypes.Edit";
        public const string Pages_Tenant_Connect_Master_TransactionTypes_Delete = "Pages.Tenant.Connect.Master.TransactionTypes.Delete";

        public const string Pages_Tenant_Connect_Master_TillAccounts = "Pages.Tenant.Connect.Master.TillAccounts";
        public const string Pages_Tenant_Connect_Master_TillAccounts_Create = "Pages.Tenant.Connect.Master.TillAccounts.Create";
        public const string Pages_Tenant_Connect_Master_TillAccounts_Edit = "Pages.Tenant.Connect.Master.TillAccounts.Edit";
        public const string Pages_Tenant_Connect_Master_TillAccounts_Delete = "Pages.Tenant.Connect.Master.TillAccounts.Delete";

        public const string Pages_Tenant_Connect_Master_Calculations = "Pages.Tenant.Connect.Master.Calculations";
        public const string Pages_Tenant_Connect_Master_Calculations_Create = "Pages.Tenant.Connect.Master.Calculations.Create";
        public const string Pages_Tenant_Connect_Master_Calculations_Edit = "Pages.Tenant.Connect.Master.Calculations.Edit";
        public const string Pages_Tenant_Connect_Master_Calculations_Delete = "Pages.Tenant.Connect.Master.Calculations.Delete";

        public const string Pages_Tenant_Connect_Master_DinePlanTaxes = "Pages.Tenant.Connect.Master.DinePlanTaxes";
        public const string Pages_Tenant_Connect_Master_DinePlanTaxes_Create = "Pages.Tenant.Connect.Master.DinePlanTaxes.Create";
        public const string Pages_Tenant_Connect_Master_DinePlanTaxes_Edit = "Pages.Tenant.Connect.Master.DinePlanTaxes.Edit";
        public const string Pages_Tenant_Connect_Master_DinePlanTaxes_Delete = "Pages.Tenant.Connect.Master.DinePlanTaxes.Delete";

        public const string Pages_Tenant_Connect_Master_Locations = "Pages.Tenant.Connect.Master.Locations";
        public const string Pages_Tenant_Connect_Master_Locations_Create = "Pages.Tenant.Connect.Master.Locations.Create";
        public const string Pages_Tenant_Connect_Master_Locations_Edit = "Pages.Tenant.Connect.Master.Locations.Edit";
        public const string Pages_Tenant_Connect_Master_Locations_Delete = "Pages.Tenant.Connect.Master.Locations.Delete";

        public const string Pages_Tenant_Connect_Master_Numerator = "Pages.Tenant.Connect.Master.Numerator";
        public const string Pages_Tenant_Connect_Master_Numerator_Create = "Pages.Tenant.Connect.Master.Numerator.Create";
        public const string Pages_Tenant_Connect_Master_Numerator_Delete = "Pages.Tenant.Connect.Master.Numerator.Delete";
        public const string Pages_Tenant_Connect_Master_Numerator_Edit = "Pages.Tenant.Connect.Master.Numerator.Edit";

        public const string Pages_Tenant_Connect_Master_TicketType = "Pages.Tenant.Connect.Master.TicketType";
        public const string Pages_Tenant_Connect_Master_TicketType_Create = "Pages.Tenant.Connect.Master.TicketType.Create";
        public const string Pages_Tenant_Connect_Master_TicketType_Delete = "Pages.Tenant.Connect.Master.TicketType.Delete";
        public const string Pages_Tenant_Connect_Master_TicketType_Edit = "Pages.Tenant.Connect.Master.TicketType.Edit";

        #endregion Master

        #region Menu

        public const string Pages_Tenant_Connect_Menu = "Pages.Tenant.Connect.Menu";

        public const string Pages_Tenant_Connect_Menu_ProductGroups = "Pages.Tenant.Connect.Menu.ProductGroups";
        public const string Pages_Tenant_Connect_Menu_ProductGroups_Create = "Pages.Tenant.Connect.Menu.ProductGroups.Create";
        public const string Pages_Tenant_Connect_Menu_ProductGroups_Edit = "Pages.Tenant.Connect.Menu.ProductGroups.Edit";

        public const string Pages_Tenant_Connect_Menu_ProductGroups_Delete = "Pages.Tenant.Connect.Menu.ProductGroups.Delete";

        public const string Pages_Tenant_Connect_Menu_Categories = "Pages.Tenant.Connect.Menu.Categories";
        public const string Pages_Tenant_Connect_Menu_Categories_Create = "Pages.Tenant.Connect.Menu.Categories.Create";
        public const string Pages_Tenant_Connect_Menu_Categories_Delete = "Pages.Tenant.Connect.Menu.Categories.Delete";
        public const string Pages_Tenant_Connect_Menu_Categories_Edit = "Pages.Tenant.Connect.Menu.Categories.Edit";

        public const string Pages_Tenant_Connect_Menu_MenuItems = "Pages.Tenant.Connect.Menu.MenuItems";
        public const string Pages_Tenant_Connect_Menu_MenuItems_Create = "Pages.Tenant.Connect.Menu.MenuItems.Create";
        public const string Pages_Tenant_Connect_Menu_MenuItems_Delete = "Pages.Tenant.Connect.Menu.MenuItems.Delete";
        public const string Pages_Tenant_Connect_Menu_MenuItems_Edit = "Pages.Tenant.Connect.Menu.MenuItems.Edit";

        public const string Pages_Tenant_Connect_Menu_ScreenMenus = "Pages.Tenant.Connect.Menu.ScreenMenus";
        public const string Pages_Tenant_Connect_Menu_ScreenMenus_Create = "Pages.Tenant.Connect.Menu.ScreenMenus.Create";
        public const string Pages_Tenant_Connect_Menu_ScreenMenus_Delete = "Pages.Tenant.Connect.Menu.ScreenMenus.Delete";
        public const string Pages_Tenant_Connect_Menu_ScreenMenus_Edit = "Pages.Tenant.Connect.Menu.ScreenMenus.Edit";

        public const string Pages_Tenant_Connect_Menu_ComboGroups = "Pages.Tenant.Connect.Menu.ComboGroups";
        public const string Pages_Tenant_Connect_Menu_ComboGroups_Create = "Pages.Tenant.Connect.Menu.ComboGroups.Create";
        public const string Pages_Tenant_Connect_Menu_ComboGroups_Edit = "Pages.Tenant.Connect.Menu.ComboGroups.Edit";
        public const string Pages_Tenant_Connect_Menu_ComboGroups_Delete = "Pages.Tenant.Connect.Menu.ComboGroups.Delete";

        public const string Pages_Tenant_Connect_Menu_LocationPrices = "Pages.Tenant.Connect.Menu.LocationPrices";
        public const string Pages_Tenant_Connect_Menu_LocationPrices_Edit = "Pages.Tenant.Connect.Menu.LocationPrices.Edit";

        public const string Pages_Tenant_Connect_Menu_LocationMenus = "Pages.Tenant.Connect.Menu.LocationMenus";

        #endregion Menu

        #region Print

        public const string Pages_Tenant_Connect_Print = "Pages.Tenant.Connect.Print";

        public const string Pages_Tenant_Connect_Print_Printers = "Pages.Tenant.Connect.Print.Printers";
        public const string Pages_Tenant_Connect_Print_Printers_Create = "Pages.Tenant.Connect.Print.Printers.Create";
        public const string Pages_Tenant_Connect_Print_Printers_Edit = "Pages.Tenant.Connect.Print.Printers.Edit";
        public const string Pages_Tenant_Connect_Print_Printers_Delete = "Pages.Tenant.Connect.Print.Printers.Delete";

        public const string Pages_Tenant_Connect_Print_PrintTemplates = "Pages.Tenant.Connect.Print.PrintTemplates";
        public const string Pages_Tenant_Connect_Print_PrintTemplates_Create = "Pages.Tenant.Connect.Print.PrintTemplates.Create";
        public const string Pages_Tenant_Connect_Print_PrintTemplates_Edit = "Pages.Tenant.Connect.Print.PrintTemplates.Edit";
        public const string Pages_Tenant_Connect_Print_PrintTemplates_Delete = "Pages.Tenant.Connect.Print.PrintTemplates.Delete";

        public const string Pages_Tenant_Connect_Print_PrintJobs = "Pages.Tenant.Connect.Print.PrintJobs";
        public const string Pages_Tenant_Connect_Print_PrintJobs_Create = "Pages.Tenant.Connect.Print.PrintJobs.Create";
        public const string Pages_Tenant_Connect_Print_PrintJobs_Edit = "Pages.Tenant.Connect.Print.PrintJobs.Edit";
        public const string Pages_Tenant_Connect_Print_PrintJobs_Delete = "Pages.Tenant.Connect.Print.PrintJobs.Delete";

        public const string Pages_Tenant_Connect_Print_PrintConditions = "Pages.Tenant.Connect.Print.PrintConditions";
        public const string Pages_Tenant_Connect_Print_PrintConditions_Create = "Pages.Tenant.Connect.Print.PrintConditions.Create";
        public const string Pages_Tenant_Connect_Print_PrintConditions_Edit = "Pages.Tenant.Connect.Print.PrintConditions.Edit";
        public const string Pages_Tenant_Connect_Print_PrintConditions_Delete = "Pages.Tenant.Connect.Print.PrintConditions.Delete";

        public const string Pages_Tenant_Connect_Display_DineDevice = "Pages.Tenant.Connect.Display.DineDevice";
        public const string Pages_Tenant_Connect_Display_DineDevice_Create = "Pages.Tenant.Connect.Display.DineDevice.Create";
        public const string Pages_Tenant_Connect_Display_DineDevice_Delete = "Pages.Tenant.Connect.Display.DineDevice.Delete";
        public const string Pages_Tenant_Connect_Display_DineDevice_Edit = "Pages.Tenant.Connect.Display.DineDevice.Edit";

        #endregion Print

        #region ProgramSetting

        public const string Pages_Tenant_Connect_ProgramSetting = "Pages.Tenant.Connect.ProgramSetting";

        public const string Pages_Tenant_Connect_ProgramSetting_Templates = "Pages.Tenant.Connect.ProgramSetting.Templates";
        public const string Pages_Tenant_Connect_ProgramSetting_Templates_Create = "Pages.Tenant.Connect.ProgramSetting.Templates.Create";
        public const string Pages_Tenant_Connect_ProgramSetting_Templates_Edit = "Pages.Tenant.Connect.ProgramSetting.Templates.Edit";
        public const string Pages_Tenant_Connect_ProgramSetting_Templates_Delete = "Pages.Tenant.Connect.ProgramSetting.Templates.Delete";

        #endregion ProgramSetting


        #region Promotion

        public const string Pages_Tenant_Connect_Promotions = "Pages.Tenant.Connect.Promotions";
        public const string Pages_Tenant_Connect_Promotions_Create = "Pages.Tenant.Connect.Promotions.Create";
        public const string Pages_Tenant_Connect_Promotions_Edit = "Pages.Tenant.Connect.Promotions.Edit";
        public const string Pages_Tenant_Connect_Promotions_Delete = "Pages.Tenant.Connect.Promotions.Delete";
        public const string Pages_Tenant_Connect_Promotions_Revert = "Pages.Tenant.Connect.Promotions.Revert";

        public const string Pages_Tenant_Connect_PromotionCategory = "Pages.Tenant.Connect.PromotionCategory";
        public const string Pages_Tenant_Connect_PromotionCategory_Create = "Pages.Tenant.Connect.PromotionCategory.Create";
        public const string Pages_Tenant_Connect_PromotionCategory_Delete = "Pages.Tenant.Connect.PromotionCategory.Delete";
        public const string Pages_Tenant_Connect_PromotionCategory_Edit = "Pages.Tenant.Connect.PromotionCategory.Edit";

        #endregion Promotion

        #region Table

        public const string Pages_Tenant_Connect_Table = "Pages.Tenant.Connect.Table";

        public const string Pages_Tenant_Connect_Table_Group = "Pages.Tenant.Connect.Table.Group";
        public const string Pages_Tenant_Connect_Table_Group_Create = "Pages.Tenant.Connect.Table.Group.Create";
        public const string Pages_Tenant_Connect_Table_Group_Delete = "Pages.Tenant.Connect.Table.Group.Delete";
        public const string Pages_Tenant_Connect_Table_Group_Edit = "Pages.Tenant.Connect.Table.Group.Edit";

        public const string Pages_Tenant_Connect_Tables = "Pages.Tenant.Connect.Tables";
        public const string Pages_Tenant_Connect_Tables_Create = "Pages.Tenant.Connect.Tables.Create";
        public const string Pages_Tenant_Connect_Tables_Delete = "Pages.Tenant.Connect.Tables.Delete";
        public const string Pages_Tenant_Connect_Tables_Edit = "Pages.Tenant.Connect.Tables.Edit";

        #endregion

        #region DinePlan User

        public const string Pages_Tenant_Connect_User = "Pages.Tenant.Connect.User";

        public const string Pages_Tenant_Connect_User_DinePlanUser = "Pages.Tenant.Connect.User.DinePlanUser";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Create = "Pages.Tenant.Connect.User.DinePlanUser.Create";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Delete = "Pages.Tenant.Connect.User.DinePlanUser.Delete";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Edit = "Pages.Tenant.Connect.User.DinePlanUser.Edit";

        public const string Pages_Tenant_Connect_User_DinePlanUserRole = "Pages.Tenant.Connect.User.DinePlanUserRole";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Create = "Pages.Tenant.Connect.User.DinePlanUserRole.Create";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Delete = "Pages.Tenant.Connect.User.DinePlanUserRole.Delete";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Edit = "Pages.Tenant.Connect.User.DinePlanUserRole.Edit";

        #endregion

        #region Card

        public const string Pages_Tenant_Connect_Card = "Pages.Tenant.Connect.Card";

        public const string Pages_Tenant_Connect_Card_Cards = "Pages.Tenant.Connect.Card.Cards";
        public const string Pages_Tenant_Connect_Card_Cards_Create = "Pages.Tenant.Connect.Card.Cards.Create";
        public const string Pages_Tenant_Connect_Card_Cards_Edit = "Pages.Tenant.Connect.Card.Cards.Edit";
        public const string Pages_Tenant_Connect_Card_Cards_Delete = "Pages.Tenant.Connect.Card.Cards.Delete";
        public const string Pages_Tenant_Connect_Card_Cards_Import = "Pages.Tenant.Connect.Card.Cards.Import";

        public const string Pages_Tenant_Connect_Card_CardType = "Pages.Tenant.Connect.Card.CardType";
        public const string Pages_Tenant_Connect_Card_CardType_Create = "Pages.Tenant.Connect.Card.CardType.Create";
        public const string Pages_Tenant_Connect_Card_CardType_Edit = "Pages.Tenant.Connect.Card.CardType.Edit";
        public const string Pages_Tenant_Connect_Card_CardType_Delete = "Pages.Tenant.Connect.Card.CardType.Delete";

        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Create = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Create";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Delete = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Delete";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Edit = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Edit";

        #endregion


        #region Report
        public const string Pages_Tenant_Connect_Report = "Pages.Tenant.Connect.Report";
        public const string Pages_Tenant_Connect_Report_Ticket = "Pages.Tenant.Connect.Report.Ticket";
        public const string Pages_Tenant_Connect_Report_TicketSync = "Pages.Tenant.Connect.Report.TicketSync";
        public const string Pages_Tenant_Connect_Report_TicketHourlySales = "Pages.Tenant.Connect.Report.TicketHourlySales";
        public const string Pages_Tenant_Connect_Report_Order = "Pages.Tenant.Connect.Report.Order";
        public const string Pages_Tenant_Connect_Report_Other = "Pages.Tenant.Connect.Report.Other";
        public const string Pages_Tenant_Connect_Report_Department = "Pages.Tenant.Connect.Report.Department";
        public const string Pages_Tenant_Connect_Report_Department_Summary = "Pages.Tenant.Connect.Report.Department.Summary";
        public const string Pages_Tenant_Connect_Report_Department_Items = "Pages.Tenant.Connect.Report.Department.Items";
        public const string Pages_Tenant_Connect_Report_Item = "Pages.Tenant.Connect.Report.Item";
        public const string Pages_Tenant_Connect_Report_Group = "Pages.Tenant.Connect.Report.Group";
        public const string Pages_Tenant_Connect_Report_Category = "Pages.Tenant.Connect.Report.Category";
        public const string Pages_Tenant_Connect_Report_Promotion = "Pages.Tenant.Connect.Report.Promotion";
        public const string Pages_Tenant_Connect_Report_Sale = "Pages.Tenant.Connect.Report.Sale";
        public const string Pages_Tenant_Connect_Report_Log = "Pages.Tenant.Connect.Report.Log";
        public const string Pages_Tenant_Connect_Report_SummaryOther = "Pages.Tenant.Connect.Report.SummaryOther";
        public const string Pages_Tenant_Connect_Report_PaymentExcess = "Pages.Tenant.Connect.Report.PaymentExcess";
        public const string Pages_Tenant_Connect_Report_Tender = "Pages.Tenant.Connect.Report.Tender";
        public const string Pages_Tenant_Connect_Report_TillTransaction = "Pages.Tenant.Connect.Report.TillTransaction";
        public const string Pages_Tenant_Connect_Report_Schedule = "Pages.Tenant.Connect.Report.ScheduleReport";
        public const string Pages_Tenant_Connect_Report_ScheduleItems = "Pages.Tenant.Connect.Report.ScheduleItems";
        public const string Pages_Tenant_Connect_Report_ScheduleLocations = "Pages.Tenant.Connect.Report.ScheduleLocations";

        #endregion

        #endregion Connect

        #region Tiffins

        public const string Pages_Tenant_Tiffin = "Pages.Tenant.Tiffin";

        public const string Pages_Tenant_Tiffin_Customer = "Pages.Tenant.Tiffin.Customer";
        public const string Pages_Tenant_Tiffin_Customer_Customers = "Pages.Tenant.Tiffin.Customer.Customers";
        public const string Pages_Tenant_Tiffin_Customer_Customers_Create = "Pages.Tenant.Tiffin.Customer.Customers.Create";
        public const string Pages_Tenant_Tiffin_Customer_Customers_Edit = "Pages.Tenant.Tiffin.Customer.Customers.Edit";
        public const string Pages_Tenant_Tiffin_Customer_Customers_Delete = "Pages.Tenant.Tiffin.Customer.Customers.Delete";

        public const string Pages_Tenant_Tiffin_Customer_Transactions = "Pages.Tenant.Tiffin.Customer.Transactions";
        public const string Pages_Tenant_Tiffin_Customer_Transactions_Create = "Pages.Tenant.Tiffin.Customer.Transactions.Create";
        public const string Pages_Tenant_Tiffin_Customer_Transactions_Edit = "Pages.Tenant.Tiffin.Customer.Transactions.Edit";
        public const string Pages_Tenant_Tiffin_Customer_Transactions_Delete = "Pages.Tenant.Tiffin.Customer.Transactions.Delete";

        public const string Pages_Tenant_Tiffin_Manage = "Pages.Tenant.Tiffin.Manage";

        public const string Pages_Tenant_Tiffin_Manage_ProductOffers = "Pages.Tenant.Tiffin.Manage.ProductOffers";
        public const string Pages_Tenant_Tiffin_Manage_ProductOffers_Create = "Pages.Tenant.Tiffin.Manage.ProductOffers.Create";
        public const string Pages_Tenant_Tiffin_Manage_ProductOffers_Edit = "Pages.Tenant.Tiffin.Manage.ProductOffers.Edit";
        public const string Pages_Tenant_Tiffin_Manage_ProductOffers_Delete = "Pages.Tenant.Tiffin.Manage.ProductOffers.Delete";

        public const string Pages_Tenant_Tiffin_Manage_Schedules = "Pages.Tenant.Tiffin.Manage.Schedules";
        public const string Pages_Tenant_Tiffin_Manage_Schedules_Delete = "Pages.Tenant.Tiffin.Manage.Schedules.Delete";
        public const string Pages_Tenant_Tiffin_Manage_Schedules_EditCalender = "Pages.Tenant.Tiffin.Manage.Schedules.EditCalender";
        public const string Pages_Tenant_Tiffin_Manage_Schedules_EditSchedule = "Pages.Tenant.Tiffin.Manage.Schedules.EditSchedule";

        public const string Pages_Tenant_Tiffin_Manage_ProductOfferTypes = "Pages.Tenant.Tiffin.Manage.ProductOfferTypes";
        public const string Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Create = "Pages.Tenant.Tiffin.Manage.ProductOfferTypes.Create";
        public const string Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Edit = "Pages.Tenant.Tiffin.Manage.ProductOfferTypes.Edit";
        public const string Pages_Tenant_Tiffin_Manage_ProductOfferTypes_Delete = "Pages.Tenant.Tiffin.Manage.ProductOfferTypes.Delete";

        public const string Pages_Tenant_Tiffin_Manage_OrderTags = "Pages.Tenant.Tiffin.Manage.OrderTags";
        public const string Pages_Tenant_Tiffin_Manage_OrderTags_Create = "Pages.Tenant.Tiffin.Manage.OrderTags.Create";
        public const string Pages_Tenant_Tiffin_Manage_OrderTags_Edit = "Pages.Tenant.Tiffin.Manage.OrderTags.Edit";
        public const string Pages_Tenant_Tiffin_Manage_OrderTags_Delete = "Pages.Tenant.Tiffin.Manage.OrderTags.Delete";

        public const string Pages_Tenant_Tiffin_Manage_MealTimes = "Pages.Tenant.Tiffin.Manage.MealTimes";
        public const string Pages_Tenant_Tiffin_Manage_MealTimes_Create = "Pages.Tenant.Tiffin.Manage.MealTimes.Create";
        public const string Pages_Tenant_Tiffin_Manage_MealTimes_Edit = "Pages.Tenant.Tiffin.Manage.MealTimes.Edit";
        public const string Pages_Tenant_Tiffin_Manage_MealTimes_Delete = "Pages.Tenant.Tiffin.Manage.MealTimes.Delete";

        public const string Pages_Tenant_Tiffin_Manage_ProductSets = "Pages.Tenant.Tiffin.Manage.ProductSets";
        public const string Pages_Tenant_Tiffin_Manage_ProductSets_Create = "Pages.Tenant.Tiffin.Manage.ProductSets.Create";
        public const string Pages_Tenant_Tiffin_Manage_ProductSets_Edit = "Pages.Tenant.Tiffin.Manage.ProductSets.Edit";
        public const string Pages_Tenant_Tiffin_Manage_ProductSets_Delete = "Pages.Tenant.Tiffin.Manage.ProductSets.Delete";

        public const string Pages_Tenant_Tiffin_Report = "Pages.Tenant.Tiffin.Report";
        public const string Pages_Tenant_Tiffin_Report_OrderSummary = "Pages.Tenant.Tiffin.Report.OrderSummary";
        public const string Pages_Tenant_Tiffin_Report_Preparation = "Pages.Tenant.Tiffin.Report.Preparation";

        public const string Pages_Tenant_Tiffin_Report_Balance = "Pages.Tenant.Tiffin.Report.Balance";
        public const string Pages_Tenant_Tiffin_Report_Payment = "Pages.Tenant.Tiffin.Report.Payment";
        public const string Pages_Tenant_Tiffin_Report_Kitchen = "Pages.Tenant.Tiffin.Report.Kitchen";
        public const string Pages_Tenant_Tiffin_Report_Driver = "Pages.Tenant.Tiffin.Report.Driver";
        public const string Pages_Tenant_Tiffin_Report_TiffinTransaction = "Pages.Tenant.Tiffin.Report.TiffinTransaction";

        public const string Pages_Tenant_Tiffin_Manage_Faq = "Pages.Tenant.Tiffin.Manage.Faq";
        public const string Pages_Tenant_Tiffin_Manage_Faq_Create = "Pages.Tenant.Tiffin.Manage.Faq.Create";
        public const string Pages_Tenant_Tiffin_Manage_Faq_Edit = "Pages.Tenant.Tiffin.Manage.Faq.Edit";
        public const string Pages_Tenant_Tiffin_Manage_Faq_Delete = "Pages.Tenant.Tiffin.Manage.Faq.Delete";

        public const string Pages_Tenant_Tiffin_Manage_HomeBanner = "Pages.Tenant.Tiffin.Manage.HomeBanner";
        public const string Pages_Tenant_Tiffin_Manage_HomeBanner_Edit = "Pages.Tenant.Tiffin.Manage.HomeBanner.Edit";

        public const string Pages_Tenant_Tiffin_Manage_Promotion = "Pages.Tenant.Tiffin.Manage.Promotion";
        public const string Pages_Tenant_Tiffin_Manage_Promotion_Create = "Pages.Tenant.Tiffin.Manage.Promotion.Create";
        public const string Pages_Tenant_Tiffin_Manage_Promotion_Edit = "Pages.Tenant.Tiffin.Manage.Promotion.Edit";
        public const string Pages_Tenant_Tiffin_Manage_Promotion_Delete = "Pages.Tenant.Tiffin.Manage.Promotion.Delete";

        public const string Pages_Tenant_Tiffin_ScreenMenus = "Pages.Tenant.Tiffin.ScreenMenus";
        public const string Pages_Tenant_Tiffin_ScreenMenus_Create = "Pages.Tenant.Tiffin.ScreenMenus.Create";
        public const string Pages_Tenant_Tiffin_ScreenMenus_Delete = "Pages.Tenant.Tiffin.ScreenMenus.Delete";
        public const string Pages_Tenant_Tiffin_ScreenMenus_Edit = "Pages.Tenant.Tiffin.ScreenMenus.Edit";

        public const string Pages_Tenant_Tiffin_TimeSlot = "Pages.Tenant.Tiffin.TimeSlot";
        public const string Pages_Tenant_Tiffin_TimeSlot_Delete = "Pages.Tenant.Tiffin.TimeSlot.Delete";
        public const string Pages_Tenant_Tiffin_TimeSlot_Edit = "Pages.Tenant.Tiffin.TimeSlot.Edit";

        public const string Pages_Tenant_Tiffin_OrderCollected = "Pages.Tenant.Tiffin.OrderCollected";

        #endregion Tiffins

        #region Wheel

        public const string Pages_Tenant_Wheel = "Pages.Tenant.Wheel";
     
        public const string Pages_WheelSetups = "Pages.WheelSetups";
        public const string Pages_WheelSetups_Create = "Pages.WheelSetups.Create";
        public const string Pages_WheelSetups_Edit = "Pages.WheelSetups.Edit";
        public const string Pages_WheelSetups_Delete = "Pages.WheelSetups.Delete";

        public const string Pages_WheelDepartments = "Pages.WheelDepartments";
        public const string Pages_WheelDepartments_Create = "Pages.WheelDepartments.Create";
        public const string Pages_WheelDepartments_Edit = "Pages.WheelDepartments.Edit";
        public const string Pages_WheelDepartments_Delete = "Pages.WheelDepartments.Delete";

        public const string Pages_WheelDynamicPages = "Pages.WheelDynamicPages";
        public const string Pages_WheelDynamicPages_Create = "Pages.WheelDynamicPages.Create";
        public const string Pages_WheelDynamicPages_Edit = "Pages.WheelDynamicPages.Edit";
        public const string Pages_WheelDynamicPages_Delete = "Pages.WheelDynamicPages.Delete";

        public const string Pages_WheelTables = "Pages.WheelTables";
        public const string Pages_WheelTables_Create = "Pages.WheelTables.Create";
        public const string Pages_WheelTables_Edit = "Pages.WheelTables.Edit";
        public const string Pages_WheelTables_Delete = "Pages.WheelTables.Delete";

    
        public const string Pages_WheelLocations = "Pages.WheelLocations";
        public const string Pages_WheelLocations_Create = "Pages.WheelLocations.Create";
        public const string Pages_WheelLocations_Edit = "Pages.WheelLocations.Edit";
        public const string Pages_WheelLocations_Delete = "Pages.WheelLocations.Delete";

        public const string Pages_WheelTaxes = "Pages.WheelTaxes";
        public const string Pages_WheelTaxes_Create = "Pages.WheelTaxes.Create";
        public const string Pages_WheelTaxes_Edit = "Pages.WheelTaxes.Edit";
        public const string Pages_WheelTaxes_Delete = "Pages.WheelTaxes.Delete";

        public const string Pages_WheelPaymentMethods = "Pages.WheelPaymentMethods";
        public const string Pages_WheelPaymentMethods_Create = "Pages.WheelPaymentMethods.Create";
        public const string Pages_WheelPaymentMethods_Edit = "Pages.WheelPaymentMethods.Edit";
        public const string Pages_WheelPaymentMethods_Delete = "Pages.WheelPaymentMethods.Delete";

        public const string Pages_WheelDeliveryDurations = "Pages.WheelDeliveryDurations";
        public const string Pages_WheelDeliveryDurations_Create = "Pages.WheelDeliveryDurations.Create";
        public const string Pages_WheelDeliveryDurations_Edit = "Pages.WheelDeliveryDurations.Edit";
        public const string Pages_WheelDeliveryDurations_Delete = "Pages.WheelDeliveryDurations.Delete";

     
        public const string Pages_WheelScreenMenus = "Pages.WheelScreenMenus";
        public const string Pages_WheelScreenMenus_Create = "Pages.WheelScreenMenus.Create";
        public const string Pages_WheelScreenMenus_Edit = "Pages.WheelScreenMenus.Edit";
        public const string Pages_WheelScreenMenus_Delete = "Pages.WheelScreenMenus.Delete";

         public const string Pages_Wheel_Faq = "Pages.Wheel.Faq";
        public const string Pages_Wheel_Faq_Create = "Pages.Wheel.Faq.Create";
        public const string Pages_Wheel_Faq_Edit = "Pages.Wheel.Faq.Edit";
        public const string Pages_Wheel_Faq_Delete = "Pages.Wheel.Faq.Delete";

        public const string Pages_WheelProductSoldOut = "Pages.WheelProductSoldoutMenus";
        public const string Pages_WheelProductSoldOut_Create = "Pages.WheelProductSoldoutMenus.Create";
        public const string Pages_WheelProductSoldOut_Edit = "Pages.WheelProductSoldoutMenus.Edit";
        public const string Pages_WheelProductSoldOut_Delete = "Pages.WheelProductSoldoutMenus.Delete";

        public const string Pages_WheelRecommendationMenu = "Pages.WheelRecommendationMenu";
        public const string Pages_WheelRecommendationMenu_Create = "Pages.WheelRecommendationMenu.Create";
        public const string Pages_WheelRecommendationMenu_Edit = "Pages.WheelRecommendationMenu.Edit";
        public const string Pages_WheelRecommendationMenu_Delete = "Pages.WheelRecommendationMenu.Delete";

        #region Flyer

        public const string Pages_Tenant_Flyer_Manage_Flyer = "Pages.Tenant.Flyer.Manage.Flyer";
        public const string Pages_Tenant_Flyer_Manage_Flyer_Create = "Pages.Tenant.Flyer.Manage.Flyer.Create";
        public const string Pages_Tenant_Flyer_Manage_Flyer_Edit = "Pages.Tenant.Flyer.Manage.Flyer.Edit";
        public const string Pages_Tenant_Flyer_Manage_Flyer_Delete = "Pages.Tenant.Flyer.Manage.Flyer.Delete";

        #endregion
        public const string Pages_Tenant_Queue = "DinePlan.DineConnect.Queue";

        public const string Pages_WheelGeneralSettings = "Pages.Wheel.GeneralSettings";
        public const string Pages_WheelPromotions = "Pages.WheelPromotions";
        public const string Pages_WheelDeliveryZone = "Pages.WheelDeliveryZone";
        public const string Pages_WheelServiceFee = "Pages.WheelServiceFee";
        public const string Pages_WheelOpeningHour = "Pages.WheelOpeningHour";

        public const string Pages_WheelReserve = "Pages.Wheel.Reserve";
        public const string Pages_WheelReserve_SeatMap = "Pages.Wheel.Reserve.SeatMap";
        public const string Pages_WheelReserve_Reservation = "Pages.Wheel.Reserve.Reservation";
        public const string Pages_WheelReport = "Pages.Wheel.Report";


        #endregion Wheel



      
    }
}
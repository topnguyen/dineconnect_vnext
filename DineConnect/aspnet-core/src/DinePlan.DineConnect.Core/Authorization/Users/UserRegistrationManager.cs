﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Features;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IdentityFramework;
using Abp.Linq;
using Abp.Notifications;
using Abp.RealTime;
using Abp.Runtime.Session;
using Abp.UI;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Chat;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Friendships;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.Tiffins.Customer;
using Elect.Core.StringUtils;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Authorization.Users
{
    public class UserRegistrationManager : DineConnectDomainServiceBase
    {
        public IAbpSession AbpSession { get; set; }
        public IAsyncQueryableExecuter AsyncQueryableExecuter { get; set; }

        private readonly TenantManager _tenantManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IUserPolicy _userPolicy;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<TiffinCustomerReferral> _tiffinCustoRefRepo;
        private readonly IRepository<CustomerAddressDetail> _customerAddressRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IOnlineClientManager<ChatChannel> _onlineClientManager;
        private readonly IChatCommunicator _chatCommunicator;
        private readonly IFriendshipManager _friendshipManager;

        public UserRegistrationManager(
            TenantManager tenantManager,
            UserManager userManager,
            RoleManager roleManager,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IUserPolicy userPolicy,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Customer> customerRepository,
            IRepository<TiffinCustomerReferral> tiffinCustoRefRepo,
            IRepository<CustomerAddressDetail> customerAddressRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IOnlineClientManager<ChatChannel> onlineClientManager,
            IChatCommunicator chatCommunicator,
            IFriendshipManager friendshipManager)
        {
            _tenantManager = tenantManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _userPolicy = userPolicy;
            _unitOfWorkManager = unitOfWorkManager;
            _customerRepository = customerRepository;
            _tiffinCustoRefRepo = tiffinCustoRefRepo;
            _customerAddressRepository = customerAddressRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _onlineClientManager = onlineClientManager;
            _chatCommunicator = chatCommunicator;
            _friendshipManager = friendshipManager;
            AbpSession = NullAbpSession.Instance;
            AsyncQueryableExecuter = NullAsyncQueryableExecuter.Instance;
        }

        public async Task<User> RegisterAsync(string name, string surname, string emailAddress, string userName,
            string plainPassword, bool isEmailConfirmed, string emailActivationLink)
        {
            CheckForTenant();
            CheckSelfRegistrationIsEnabled();

            var tenant = await GetActiveTenantAsync();
            var isNewRegisteredUserActiveByDefault =
                await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement
                    .IsNewRegisteredUserActiveByDefault);

            await _userPolicy.CheckMaxUserCountAsync(tenant.Id);

            var user = new User
            {
                TenantId = tenant.Id,
                Name = name,
                Surname = surname,
                EmailAddress = emailAddress,
                IsActive = isNewRegisteredUserActiveByDefault,
                UserName = userName,
                IsEmailConfirmed = isEmailConfirmed,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            var defaultRoles = await AsyncQueryableExecuter.ToListAsync(_roleManager.Roles.Where(r => r.IsDefault));
            foreach (var defaultRole in defaultRoles)
            {
                user.Roles.Add(new UserRole(tenant.Id, user.Id, defaultRole.Id));
            }

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
            CheckErrors(await _userManager.CreateAsync(user, plainPassword));
            await CurrentUnitOfWork.SaveChangesAsync();
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);
            await _appNotifier.NewUserRegisteredAsync(user);

            return user;
        }

        public async Task<User> RegisterMemberAsync(
            string name,
            string surname,
            string emailAddress,
            string userName,
            string plainPassword,
            string phoneNumber,
            string address1,
            string address2,
            string address3,
            int? cityId,
            int? countryId,
            string postcalCode,
            int? zone,
            bool isEmailConfirmed,
            string emailActivationLink,
            string referrerReferralCode,
            string jsonData,
            bool acceptReceivePromotionEmail)
        {
            CheckForTenant();

            CheckSelfRegistrationIsEnabled();

            var tenant = await GetActiveTenantAsync();
            await _userPolicy.CheckMaxUserCountAsync(tenant.Id);

            var user = new User
            {
                TenantId = tenant.Id,
                Name = name,
                Surname = surname,
                EmailAddress = emailAddress,
                IsActive = true,
                UserName = userName,
                PhoneNumber = phoneNumber,
                IsEmailConfirmed = isEmailConfirmed,
                AcceptReceivePromotionEmail = acceptReceivePromotionEmail,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            var defaultRoles =
                await AsyncQueryableExecuter.ToListAsync(_roleManager.Roles.Where(r =>
                    r.Name == StaticRoleNames.Tenants.Customer));
            foreach (var defaultRole in defaultRoles)
            {
                user.Roles.Add(new UserRole(tenant.Id, user.Id, defaultRole.Id));
            }

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
            CheckErrors(await _userManager.CreateAsync(user, plainPassword));
            await CurrentUnitOfWork.SaveChangesAsync();

            await CreateCustomer(userName, address1, address2, address3, cityId, countryId, postcalCode, zone,
                referrerReferralCode, jsonData, acceptReceivePromotionEmail);

            return user;
        }

        [UnitOfWork]
        private async Task CreateCustomer(string userName, string address1,
            string address2,
            string address3,
            int? cityId,
            int? countryId,
            string postcalCode,
            int? zone,
            string referrerReferralCode,
            string jsonData,
            bool acceptReceivePromotionEmail)
        {
            var tenant = await GetActiveTenantAsync();

            var userSaved = await _userManager.GetMemberOrNullAsync(tenant.Id, userName);

            if (userSaved != null)
            {
                var customer = new Customer
                {
                    TenantId = tenant.Id,
                    UserId = userSaved.Id,
                    Name = userSaved.Name,
                    CountryId = countryId,
                    EmailAddress = userSaved.EmailAddress,
                    PhoneNumber = userSaved.PhoneNumber,
                    JsonData = jsonData, 
                    AcceptReceivePromotionEmail = acceptReceivePromotionEmail
                };

                using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                {
                    var customerId = await _customerRepository.InsertAndGetIdAsync(customer);

                    var userAddress = new CustomerAddressDetail
                    {
                        IsDefault = true,
                        Address1 = address1,
                        Address2 = address2,
                        Address3 = address3,
                        CityId = cityId,
                        PostcalCode = postcalCode,
                        Zone = zone,
                        CustomerId = customerId,
                        PhoneNumber = userSaved.PhoneNumber
                    };

                    await _customerAddressRepository.InsertAsync(userAddress);

                    var customerReferral = new TiffinCustomerReferral
                    {
                        CustomerId = customerId
                    };

                    if (!string.IsNullOrWhiteSpace(referrerReferralCode))
                    {
                        var referrerReferralEntity = _tiffinCustoRefRepo.GetAll().FirstOrDefault(x => x.ReferralCode == referrerReferralCode);

                        if (referrerReferralEntity != null)
                        {
                            customerReferral.ReferrerCustomerId = referrerReferralEntity.CustomerId;
                        }
                    }

                    // Generate Referral Code
                    var isUniqueReferralCode = false;

                    while (!isUniqueReferralCode)
                    {
                        customerReferral.ReferralCode = StringHelper.Generate(8, true, false, false);

                        var isExistingReferralCode = _tiffinCustoRefRepo.GetAll().Any(x => x.ReferralCode == customerReferral.ReferralCode);

                        isUniqueReferralCode = !isExistingReferralCode;
                    }

                    await _tiffinCustoRefRepo.InsertAsync(customerReferral);

                    // Add friend to chat service
                    var tenantId = AbpSession?.TenantId ?? tenant.Id;

                    var adminRole = _roleRepository
                        .GetAll()
                        .FirstOrDefault(x =>
                            x.Name == StaticRoleNames.Tenants.Admin && x.TenantId == tenantId);
                    if (adminRole == null) return;

                    var adminUserIds = _userRoleRepository.GetAll().Where(x => x.RoleId == adminRole.Id)
                        .Select(x => x.UserId).ToList();

                    foreach (var adminUserId in adminUserIds)
                    {
                        var adminUser = await _userManager.FindByIdAsync(adminUserId.ToString());

                        var probableFriend = new UserIdentifier(tenantId, adminUser.Id);

                        var userIdentifier = new UserIdentifier(tenantId, userSaved.Id);

                        var isExistFriendShip =
                            await _friendshipManager.GetFriendshipOrNullAsync(userIdentifier, probableFriend) != null;

                        if (!isExistFriendShip)
                        {
                            var sourceFriendship = new Friendship(userIdentifier, probableFriend, tenant.Name,
                                adminUser.UserName, adminUser.ProfilePictureId, FriendshipState.Accepted);
                            await _friendshipManager.CreateFriendshipAsync(sourceFriendship);

                            var targetFriendship = new Friendship(probableFriend, userIdentifier, tenant.Name,
                                userSaved.UserName, userSaved.ProfilePictureId, FriendshipState.Accepted);
                            await _friendshipManager.CreateFriendshipAsync(targetFriendship);

                            var clients = _onlineClientManager.GetAllByUserId(probableFriend);
                            if (clients.Any())
                            {
                                var isFriendOnline = _onlineClientManager.IsOnline(sourceFriendship.ToUserIdentifier());
                                await _chatCommunicator.SendFriendshipRequestToClient(clients, targetFriendship, false,
                                    isFriendOnline);
                            }

                            var senderClients = _onlineClientManager.GetAllByUserId(userIdentifier);
                            if (senderClients.Any())
                            {
                                var isFriendOnline = _onlineClientManager.IsOnline(targetFriendship.ToUserIdentifier());
                                await _chatCommunicator.SendFriendshipRequestToClient(senderClients, sourceFriendship,
                                    true, isFriendOnline);
                            }
                        }
                    }
                }
            }
        }

        public async Task<User> RegisterWheelCustomerAsync(
           string name,
           string surname,
           string emailAddress,
           string userName,
           string plainPassword,
           string phoneNumber,
           bool isEmailConfirmed,
           string emailActivationLink,
           string jsonData)
        {
            CheckForTenant();

            CheckSelfRegistrationIsEnabled();

            var tenant = await GetActiveTenantAsync();
       

            await _userPolicy.CheckMaxUserCountAsync(tenant.Id);

            var user = new User
            {
                TenantId = tenant.Id,
                Name = name,
                Surname = surname,
                EmailAddress = emailAddress,
                IsActive = true,
                UserName = userName,
                PhoneNumber = phoneNumber,
                IsEmailConfirmed = isEmailConfirmed,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            var defaultRoles =
                await AsyncQueryableExecuter.ToListAsync(_roleManager.Roles.Where(r =>
                    r.Name == StaticRoleNames.Tenants.Customer));
            foreach (var defaultRole in defaultRoles)
            {
                user.Roles.Add(new UserRole(tenant.Id, user.Id, defaultRole.Id));
            }

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
            CheckErrors(await _userManager.CreateAsync(user, plainPassword));
            await CurrentUnitOfWork.SaveChangesAsync();

            await CreateWheelCustomerInfo(userName, jsonData);

            return user;
        }

        [UnitOfWork]
        private async Task CreateWheelCustomerInfo(string userName, string jsonData)
        {
            var tenant = await GetActiveTenantAsync();

            var userSaved = await _userManager.GetMemberOrNullAsync(tenant.Id, userName);

            if (userSaved != null)
            {
                var customer = new Customer
                {
                    TenantId = tenant.Id,
                    UserId = userSaved.Id,
                    Name = userSaved.Name,
                    EmailAddress = userSaved.EmailAddress,
                    PhoneNumber = userSaved.PhoneNumber,
                    JsonData = jsonData,
                    CustomerGuid = Guid.NewGuid()
                };

                using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                {
                    await _customerRepository.InsertAsync(customer);
                    // Add friend to chat service

                    var adminRole = _roleRepository
                        .GetAll()
                        .FirstOrDefault(x => x.Name == StaticRoleNames.Tenants.Admin && x.TenantId == AbpSession.TenantId);

                    var adminUserIds = _userRoleRepository.GetAll().Where(x => x.RoleId == adminRole.Id)
                        .Select(x => x.UserId).ToList();

                    foreach (var adminUserId in adminUserIds)
                    {
                        var adminUser = await _userManager.FindByIdAsync(adminUserId.ToString());

                        var probableFriend = new UserIdentifier(AbpSession.TenantId, adminUser.Id);

                        var userIdentifier = new UserIdentifier(AbpSession.TenantId, userSaved.Id);

                        var isExistFriendShip =
                            await _friendshipManager.GetFriendshipOrNullAsync(userIdentifier, probableFriend) != null;

                        if (!isExistFriendShip)
                        {
                            var sourceFriendship = new Friendship(userIdentifier, probableFriend, tenant.Name,
                                adminUser.UserName, adminUser.ProfilePictureId, FriendshipState.Accepted);
                            await _friendshipManager.CreateFriendshipAsync(sourceFriendship);

                            var targetFriendship = new Friendship(probableFriend, userIdentifier, tenant.Name,
                                userSaved.UserName, userSaved.ProfilePictureId, FriendshipState.Accepted);
                            await _friendshipManager.CreateFriendshipAsync(targetFriendship);

                            var clients = _onlineClientManager.GetAllByUserId(probableFriend);
                            if (clients.Any())
                            {
                                var isFriendOnline = _onlineClientManager.IsOnline(sourceFriendship.ToUserIdentifier());
                                await _chatCommunicator.SendFriendshipRequestToClient(clients, targetFriendship, false,
                                    isFriendOnline);
                            }

                            var senderClients = _onlineClientManager.GetAllByUserId(userIdentifier);
                            if (senderClients.Any())
                            {
                                var isFriendOnline = _onlineClientManager.IsOnline(targetFriendship.ToUserIdentifier());
                                await _chatCommunicator.SendFriendshipRequestToClient(senderClients, sourceFriendship,
                                    true, isFriendOnline);
                            }
                        }
                    }
                }
            }
        }

        private void CheckForTenant()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                throw new InvalidOperationException("Can not register host users!");
            }
        }

        private void CheckSelfRegistrationIsEnabled()
        {
            if (!SettingManager.GetSettingValue<bool>(AppSettings.UserManagement.AllowSelfRegistration))
            {
                throw new UserFriendlyException(L("SelfUserRegistrationIsDisabledMessage_Detail"));
            }
        }

       

        public async Task<Tenant> GetActiveTenantAsync()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return null;
            }

            return await GetActiveTenantAsync(AbpSession.TenantId.Value);
        }

        private async Task<Tenant> GetActiveTenantAsync(int tenantId)
        {
            var tenant = await _tenantManager.FindByIdAsync(tenantId);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("UnknownTenantId{0}", tenantId));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIdIsNotActive{0}", tenantId));
            }

            return tenant;
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<bool> IsCustomer(int? tenantId, string emailAddress)
        {
            if (!tenantId.HasValue) return false;
            var user = await _userManager
                .FindByNameOrEmailAsync(emailAddress);

            if (user == null)
                return false;

            var customerRole = await AsyncQueryableExecuter
                .FirstOrDefaultAsync(_roleManager.Roles.Where(r => r.Name == StaticRoleNames.Tenants.Customer));

            if (customerRole == null)
                return false;

            var userRole = _userRoleRepository.GetAll()
                .Where(ur => ur.UserId == user.Id)
                .ToList();

            var onlyCustomer = userRole.FirstOrDefault(u => u.RoleId == customerRole.Id && u.TenantId == tenantId);

            return onlyCustomer != null;
        }
    }
}
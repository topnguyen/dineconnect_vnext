﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace DinePlan.DineConnect.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}

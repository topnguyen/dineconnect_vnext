﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Extension
{
    public static class StringExtension
    {
        public static string ToILikeString(this string value)
        {
            return $"%{value}%";
        }
    }
}

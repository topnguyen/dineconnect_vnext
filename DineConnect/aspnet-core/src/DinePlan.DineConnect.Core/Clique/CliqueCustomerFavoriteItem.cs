﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;

namespace DinePlan.DineConnect.Clique
{
    [Table("CliqueCustomerFavoriteItems")]
    public class CliqueCustomerFavoriteItem : ConnectFullAuditEntity
    {
        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }

        [ForeignKey("ScreenMenuItemId")]
        public ScreenMenuItem ScreenMenuItem { get; set; }
        public int ScreenMenuItemId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        public virtual int CustomerId { get; set; }
    }
}
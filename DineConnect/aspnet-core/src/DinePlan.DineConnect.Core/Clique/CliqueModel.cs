﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Tiffins.Schedule;

namespace DinePlan.DineConnect.Clique
{
    [Table("CliqueMealTimes")]
    public class CliqueMealTime : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string Code { get; set; }

        public string SortOrder { get; set; }
    }

    [Table("CliqueMenus")]
    public class CliqueMenu : ConnectFullMultiTenantAuditEntity
    {
        public bool FixedMenu { get; set; }
        public bool AllDay { get; set; }
        public string Days { get; set; }
        
        [ForeignKey("ScreenMenuId")]
        public ScreenMenu ScreenMenu { get; set; }
        public int ScreenMenuId { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? CutOffDate { get; set; }
        public int SortOrder { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int LocationId { get; set; }
        public ScheduleDetailStatus PublishStatus { get; set; } = ScheduleDetailStatus.Draft;

        
        [ForeignKey("CliqueMealTimeId")]
        public CliqueMealTime CliqueMealTime { get; set; }
        public int CliqueMealTimeId { get; set; }

    }

    [Table("CliqueItemStocks")]
    public class CliqueItemStock : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("MenuItemPortionId")]
        public MenuItemPortion MenuItemPortion { get; set; }
        public int? MenuItemPortionId { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public decimal Quantity { get; set; }

        [ForeignKey("CliqueMealTimeId")]
        public CliqueMealTime CliqueMealTime { get; set; }
        public int CliqueMealTimeId { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;

namespace DinePlan.DineConnect.Clique
{
    [Table("CliqueOrders")]
    public class CliqueOrder : ConnectFullAuditEntity
    {
        public virtual decimal Total { get; set; }

        
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }


        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        public virtual int CustomerId { get; set; }


        public ICollection<CliqueOrderDetail> OrderDetails { get; set; }
        public ICollection<CliquePayment> Payments { get; set; }

    }

    [Table("CliqueOrderDetails")]
    public class CliqueOrderDetail : ConnectFullAuditEntity
    {
        public MenuItem MenuItem{ get; set; }
        public int MenuItemId{ get; set; }

        public ScreenMenuItem ScreenMenuItem { get; set; }
        public int ScreenMenuItemId { get; set; } 

        public int MenuItemPortionId{ get; set; }

        public string OrderTags { get; set; } 

        public decimal Quantity { get; set; }
        public decimal Price { get; set; }

        [Column(TypeName = "date")]
        public DateTime OrderDate { get; set; }

        [ForeignKey("CliqueMealTimeId")]
        public CliqueMealTime CliqueMealTime { get; set; }
        public int CliqueMealTimeId { get; set; }

        public CliqueOrderStatus CliqueOrderStatus { get; set; }

        [ForeignKey("CliqueOrderId")]
        public virtual CliqueOrder CliqueOrder { get; set; }
        public virtual int CliqueOrderId { get; set; }

    }

    [Table("CliquePayments")]
    public class CliquePayment : ConnectFullAuditEntity
    {
        public virtual string PaymentMethod { get; set; }
        public virtual decimal Total { get; set; } 
        public virtual DateTime PaidTime { get; set; }

        [ForeignKey("CliqueOrderId")]
        public virtual CliqueOrder CliqueOrder { get; set; }
        public virtual int CliqueOrderId { get; set; }
    }

    public enum CliqueOrderStatus
    {
        Pending = 0,
        Prepared = 1,
        Delivered = 2,
        Completed = 3,
        Canceled = 4
    }
}
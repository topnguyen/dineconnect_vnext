﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.CORS
{
    public class AllowOrigin : Entity
    {
        public string Origin { get; set; }
    }
}

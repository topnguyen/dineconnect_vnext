﻿using System.Globalization;

namespace DinePlan.DineConnect.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}
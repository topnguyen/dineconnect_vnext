﻿using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Setting
{
    [Table("ProgramSettingTemplates")]
    public class ProgramSettingTemplate : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }

    [Table("ProgramSettingValues")]
    public class ProgramSettingValue : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [ForeignKey("ProgramSettingTemplateId")]
        public ProgramSettingTemplate ProgramSettingTemplate { get; set; }

        public virtual int ProgramSettingTemplateId { get; set; }

        public virtual string ActualValue { get; set; }
    }
}
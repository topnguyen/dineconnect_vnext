﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.MutilLanguage
{
	[Table("LanguageDescriptions")]
	public class LanguageDescription : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual string LanguageCode { get; set; }

		public virtual string Name { get; set; }

		public virtual string Description { get; set; }

		public virtual LanguageDescriptionType LanguageDescriptionType { get; set; }

		public virtual int ReferenceId { get; set; }
	}
}
﻿namespace DinePlan.DineConnect.Connect.MutilLanguage
{
	public enum LanguageDescriptionType
	{
		ProductGroup = 1,
		Category = 2,
		MenuItem = 3,
		Promotion = 4,
		OrderTagGroup = 5,
		OrderTag = 6,
		TicketTagGroup = 7,
		TicketTag = 8,
		DepartmentName = 9,
		PaymentType = 10,
		Reason = 11,
		Location = 12,
		LocationGroup = 13,
		LocationGroupTag = 14,
		TillAccount = 15,
		ComboGroup = 16,
		LocationBranch = 17,
	}
}
﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
	[Table("MenuItemPortions")]
	public class MenuItemPortion : ConnectFullAuditEntity
	{
		[StringLength(50)]
		public virtual string Name { get; set; }
		[StringLength(50)]
		public virtual string Barcode { get; set; }

		[ForeignKey("MenuItemId")]
		public virtual MenuItem MenuItem { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual int MultiPlier { get; set; }
        public virtual decimal Price { get; set; }
        public int? PreparationTime { get; set; }
        public int? NumberOfPax { get; set; }

        public virtual ICollection<LocationMenuItemPrice> LocationPrices { get; set; }
        public virtual ICollection<PriceTagDefinition> PriceTags { get; set; }

	}
}
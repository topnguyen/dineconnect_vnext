﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master.Locations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public class LocationMenuItemManager : DineConnectServiceBase, ILocationMenuItemManager, ITransientDependency
    {
        private readonly IRepository<LocationMenuItem> _locationMenuItemRepository;

        public LocationMenuItemManager(IRepository<LocationMenuItem> locationMenuItemRepository)
        {
            _locationMenuItemRepository = locationMenuItemRepository;
        }

        public async Task<List<LocationMenuItem>> GetAllIncludingLocaions(int menuItemId)
        {
            return await _locationMenuItemRepository
                .GetAllIncluding(c => c.Location)
                .Where(c => c.MenuItemId == menuItemId)
                .ToListAsync();
        }

        public async Task<List<LocationMenuItem>> GetAllByMenuItemId(int input)
        {
            return await _locationMenuItemRepository.GetAllListAsync(c => c.MenuItemId == input);
        }

        public async Task<List<Location>> GetAllLocationOfMenuItem(int meuItemId)
        {
            return await _locationMenuItemRepository
                  .GetAllIncluding(c => c.Location)
                  .Where(c => c.MenuItemId == meuItemId)
                  .Select(l => l.Location)
                  .ToListAsync();
        }
    }
}
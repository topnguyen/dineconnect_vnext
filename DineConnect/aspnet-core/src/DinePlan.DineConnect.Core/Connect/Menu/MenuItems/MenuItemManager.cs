﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public class MenuItemManager : DineConnectServiceBase, IMenuItemManager, ITransientDependency
    {
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<LocationMenuItem> _locationMenuItemRepository;

        public MenuItemManager(IRepository<MenuItem> menuItemRepository, 
            IRepository<LocationMenuItem> locationMenuItemRepository
            )
        {
            _menuItemRepository = menuItemRepository;
            _locationMenuItemRepository = locationMenuItemRepository;
        }

        public async Task<int> CreateOrUpdateAsync(MenuItem input)
        {
            if (_menuItemRepository.GetAll().WhereIf(input.Id > 0, m => m.Id != input.Id).Any(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            return await _menuItemRepository.InsertOrUpdateAndGetIdAsync(input);
        }

        public async Task DeleteAsync(int input)
        {
            var locationMenuItemCount = (await _locationMenuItemRepository.GetAllListAsync(lmi => lmi.MenuItemId == input)).Count;
            if (locationMenuItemCount > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }
            await _menuItemRepository.DeleteAsync(input);
        }

        public IQueryable<MenuItem> GetAll(string filter)
        {
            return _menuItemRepository
                .GetAllIncluding(c => c.Category)
                .WhereIf(!string.IsNullOrWhiteSpace(filter), c => EF.Functions.Like(c.Name, filter.ToILikeString()));
        }

        public async Task<MenuItem> GetAsync(int id)
        {
            return await _menuItemRepository.GetAllIncluding(c => c.Category).Where(m => m.Id == id).FirstOrDefaultAsync();
        }
    }
}
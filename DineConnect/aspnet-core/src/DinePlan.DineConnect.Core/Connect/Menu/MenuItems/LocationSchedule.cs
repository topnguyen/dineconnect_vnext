﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Discount;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    [Table("LocationSchedules")]
    public class LocationSchedule : ConnectFullAuditEntity
    {
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }

        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        [StringLength(100)]
        public virtual string Days { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu.Categories;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    [Table("MenuItems")]
    public class MenuItem : ConnectOrgLocFullMultiTenantAuditEntity
    {
        public MenuItem()
        {
            LocationMenuItems = new HashSet<LocationMenuItem>();
        }

        public virtual string Name { get; set; }

        [StringLength(50)]
        public virtual string BarCode { get; set; }

        [StringLength(50)]
        public virtual string AliasCode { get; set; }

        [StringLength(100)]
        public virtual string AliasName { get; set; }

        [StringLength(1000)]
        public virtual string ItemDescription { get; set; }

        public virtual bool ForceQuantity { get; set; }
        public virtual bool ForceChangePrice { get; set; }
        public virtual bool RestrictPromotion { get; set; }

        public virtual bool NoTax { get; set; }

        public int ProductType { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual ICollection<LocationMenuItem> LocationMenuItems { get; set; }
        public virtual ICollection<MenuItemPortion> Portions { get; set; }
        public virtual ICollection<UpMenuItem> UpMenuItems { get; set; }

        [StringLength(50)]
        public virtual string Tag { get; set; }

        public virtual ICollection<MenuBarCode> Barcodes { get; set; }

        [ForeignKey("TransactionTypeId")]
        public virtual TransactionType TransactionType { get; set; }

        public virtual int? TransactionTypeId { get; set; }
        public string Files { get; set; }
        public virtual string Uom { get; set; }
        public virtual int FoodType { get; set; }
        public virtual string AddOns { get; set; }
        public virtual string Images { get; set; }

        public string NutritionInfo { get; set; }

        [StringLength(50)]
        public virtual string HsnCode { get; set; }

        public virtual Guid? DownloadImage { get; set; }

        public string AllergicInfo { get; set; }
    }

    [Table("ProductComboes")]
    public class ProductCombo : CreationAuditedEntity
    {
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public bool AddPriceToOrderPrice { get; set; }

        public IList<ProductComboGroupDetail> ProductComboGroupDetails { get; set; }

        [NotMapped]
        public virtual IList<ProductComboGroup> ComboGroups
        {
            get { return ProductComboGroupDetails.Select(x => x.ProductComboGroup).ToList(); }
        }

        public string Sorting { get; set; }

        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }
    }

    [Table("ProductComboGroups")]
    public class ProductComboGroup : ConnectFullMultiTenantAuditEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public IList<ProductComboGroupDetail> ProductComboGroupDetails { get; set; }

        public virtual IList<ProductCombo> ProductCombos
        {
            get { return ProductComboGroupDetails.Select(x => x.ProductCombo).ToList(); }
        }

        private IList<ProductComboItem> _comboItems;

        public virtual IList<ProductComboItem> ComboItems
        {
            get { return _comboItems; }
            set { _comboItems = value; }
        }

        public ProductComboGroup()
        {
            ComboItems = new List<ProductComboItem>();
        }
    }

    [Table("ProductComboGroupDetails")]
    public class ProductComboGroupDetail : Entity
    {
        public int ProductComboId { get; set; }

        [ForeignKey("ProductComboId")]
        public ProductCombo ProductCombo { get; set; }

        public int ProductComboGroupId { get; set; }

        [ForeignKey("ProductComboGroupId")]
        public ProductComboGroup ProductComboGroup { get; set; }
    }

    [Table("ProductComboItems")]
    public class ProductComboItem : ConnectFullAuditEntity
    {
        public ProductComboItem()
        {
            Count = 1;
        }
        public string Name { get; set; }
        [ForeignKey("ProductComboGroupId")]
        public ProductComboGroup ProductComboGroup { get; set; }
        public int ProductComboGroupId { get; set; }
        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }
        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }
        public virtual string Files { get; set; }
        public virtual Guid? DownloadImage { get; set; }
    }

    [Table("MenuItemSchedules")]
    public class MenuItemSchedule : ConnectFullAuditEntity
    {
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public virtual string Days { get; set; }

        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
    }
}
﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    [Table("LocationMenuItems")]
    public class LocationMenuItem : ConnectFullAuditEntity
    {
        public virtual int LocationId { get; set; }
        public virtual int MenuItemId { get; set; }

        public virtual Location Location { get; set; }
        public virtual MenuItem MenuItem { get; set; }
    }
}
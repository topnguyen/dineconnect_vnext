﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
	[Table("UpMenuItems")]
	public class UpMenuItem : ConnectFullAuditEntity
	{
		[ForeignKey("MenuItemId")]
		public virtual MenuItem MenuItem { get; set; }

		public virtual int MenuItemId { get; set; }
		public virtual int RefMenuItemId { get; set; }
		public virtual bool AddBaseProductPrice { get; set; }
		public virtual decimal Price { get; set; }
		public virtual int ProductType { get; set; }
		public int MinimumQuantity { get; set; }
		public int MaxQty { get; set; }
		public bool AddAuto { get; set; }
		public int AddQuantity { get; set; }
	}

	[Table("UpMenuItemLocationPrices")]
	public class UpMenuItemLocationPrice : ConnectCreateAuditEntity
	{
		[ForeignKey("UpMenuItemId")]
		public UpMenuItem UpMenuItem { get; set; }

		public int UpMenuItemId { get; set; }

		[ForeignKey("LocationId")]
		public Location Location { get; set; }

		public int LocationId { get; set; }

		public decimal Price { get; set; }
	}
}
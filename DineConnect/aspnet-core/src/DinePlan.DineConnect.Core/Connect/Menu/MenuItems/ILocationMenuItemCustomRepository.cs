﻿using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public interface ILocationMenuItemCustomRepository : IRepository
    {
        Task DeleteLocationMenuItems(List<LocationMenuItem> locationMenuItems);

        Task AddLocationMenuItems(List<LocationMenuItem> locationMenuItems);
    }
}
﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    [Table("MenuBarCodes")]

    public class MenuBarCode : ConnectFullAuditEntity
    {
        [StringLength(50)]
        public virtual string Barcode { get; set; }

        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
    }
}
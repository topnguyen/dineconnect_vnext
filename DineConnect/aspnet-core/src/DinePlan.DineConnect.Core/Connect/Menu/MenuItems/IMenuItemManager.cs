﻿using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public interface IMenuItemManager
    {
        IQueryable<MenuItem> GetAll(string filter);

        Task<MenuItem> GetAsync(int id);

        Task<int> CreateOrUpdateAsync(MenuItem input);

        Task DeleteAsync(int input);
    }
}
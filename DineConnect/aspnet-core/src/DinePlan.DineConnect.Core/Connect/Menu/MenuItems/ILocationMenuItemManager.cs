﻿using DinePlan.DineConnect.Connect.Master.Locations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    public interface ILocationMenuItemManager
    {
        Task<List<Location>> GetAllLocationOfMenuItem(int meuItemId);

        Task<List<LocationMenuItem>> GetAllIncludingLocaions(int menuItemId);

        Task<List<LocationMenuItem>> GetAllByMenuItemId(int input);
    }
}
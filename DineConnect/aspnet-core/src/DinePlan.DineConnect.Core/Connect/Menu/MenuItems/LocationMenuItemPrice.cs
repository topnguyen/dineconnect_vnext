﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Menu.MenuItems
{
    [Table("LocationMenuItemPrices")]

    public class LocationMenuItemPrice : ConnectFullAuditEntity
    {
        [ForeignKey("MenuPortionId")]
        public virtual MenuItemPortion MenuPortion { get; set; }

        public virtual int MenuPortionId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }
        public virtual decimal Price { get; set; }
    }
}
﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("ScreenMenus")]
    public class ScreenMenu : ConnectOrgLocFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public ScreenMenuType Type { get; set; }
        public string Dynamic { get; set; }
        public virtual ICollection<ScreenMenuCategory> Categories { get; set; }
        public virtual ICollection<ScreenMenuItem> Items { get; set; }
        public virtual string Departments { get; set; }
        public virtual int CategoryColumnCount { get; set; }
        public virtual int CategoryColumnWidthRate { get; set; }
    }

    public enum ScreenMenuType
    {
        ConnectScreenMenu = 0,
        WheelScreenMenu = 1,
        CliqueMenu= 2
    }

    [Table("ScreenMenuCategories")]
    public class ScreenMenuCategory : FullAuditedEntity
    {
        public string Name { get; set; }

        public string Dynamic { get; set; }

        [ForeignKey("ScreenMenuId")]
        public virtual ScreenMenu ScreenMenu { get; set; }

        public int ScreenMenuId { get; set; }

        [ForeignKey("ParentScreenMenuCategoryId")]
        public virtual ScreenMenuCategory ParentScreenMenuCategory { get; set; }

        public int? ParentScreenMenuCategoryId { get; set; }

        public virtual ICollection<ScreenMenuItem> MenuItems { get; set; }

        public string ScreenMenuCategorySchedule { get; set; }
    }

    [Table("ScreenMenuItems")]
    public class ScreenMenuItem : FullAuditedEntity
    {
        [ForeignKey("ScreenCategoryId")]
        public virtual ScreenMenuCategory ScreenMenuCategory { get; set; }

        public int? ScreenCategoryId { get; set; }

        [ForeignKey("ScreenMenuId")]
        public virtual ScreenMenu ScreenMenu { get; set; }

        public int? ScreenMenuId { get; set; }

        public string Name { get; set; }

        public string Dynamic { get; set; }

        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }

        public virtual int? MenuItemId { get; set; }

        [ForeignKey("ProductComboId")]
        public virtual ProductCombo ProductCombo { get; set; }

        public virtual int? ProductComboId { get; set; }

        public string SubCategory { get; set; }

        public bool Published { get; set; }

        public int Order { get; set; }
    }

    public enum ScreenMenuNumerator
    {
        None = 0, Small = 1, Large = 2
    }

    public enum ScreenMenuItemConfirmationTypes
    {
        None = 0, Confirmation = 1, AdminPin = 2
    }
}
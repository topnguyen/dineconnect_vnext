﻿using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
    public interface ICategoryManager
    {
        IQueryable<Category> GetAll(string filter, bool isDeleted = false);

        Task<Category> GetAsync(int id);

        //Task<int> CreateOrUpdateAsync(Category input);

        Task DeleteAsync(int input);
    }
}
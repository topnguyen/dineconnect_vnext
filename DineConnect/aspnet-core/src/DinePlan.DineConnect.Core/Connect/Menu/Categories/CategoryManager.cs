﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
    public class CategoryManager : DineConnectServiceBase, ICategoryManager, ITransientDependency
    {
        private readonly IRepository<Category> _categotyRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly ConnectSession _connectSession;

        public CategoryManager(IRepository<Category> categotyRepository, IRepository<MenuItem> menuItemRepository, ConnectSession connectSession)
        {
            _categotyRepository = categotyRepository;
            _menuItemRepository = menuItemRepository;
            _connectSession = connectSession;
        }

        public async Task<int> CreateOrUpdateAsync(Category input)
        {
            if (_connectSession.OrgId <= 0)
                throw new UserFriendlyException("Set your default location/Organization before creating new Category");

            input.OrganizationId = _connectSession.OrgId;

            var exists = _categotyRepository.GetAll().WhereIf(input.Id > 0, m => m.Id != input.Id).Where(o => o.Name.Equals(input.Name));

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            if (input.Id > 0)
            {
                var category = await _categotyRepository.GetAsync(input.Id);

                ObjectMapper.Map(input, category);
                return input.Id;
            }

            await _categotyRepository.InsertOrUpdateAsync(input);
            await CurrentUnitOfWork.SaveChangesAsync();
            return input.Id;
        }

        public async Task DeleteAsync(int input)
        {
            var menuItemCount = (await _menuItemRepository.GetAllListAsync(c => c.CategoryId == input)).Count;
            if (menuItemCount > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }
            await _categotyRepository.DeleteAsync(input);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public IQueryable<Category> GetAll(string filter, bool isDeleted = false)
        {
            return _categotyRepository.GetAll()
                .Include(x => x.ProductGroup)
                .Where(i => i.IsDeleted == isDeleted)
                .WhereIf(!string.IsNullOrWhiteSpace(filter), i => EF.Functions.Like(i.Name, filter.ToILikeString()) || EF.Functions.Like(i.Code, filter.ToILikeString()));
        }

        public async Task<Category> GetAsync(int id)
        {
            return await _categotyRepository.GetAsync(id);
        }
    }
}
﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Menu.ProductGroups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu.Categories
{
	[Table("Categories")]
	public class Category : ConnectOrgFullMultiTenantAuditEntity
	{
		public Category()
		{
			MenuItems = new HashSet<MenuItem>();
		}

		public virtual string Name { get; set; }
		public virtual string Code { get; set; }

		[ForeignKey("ProductGroupId")]
		public virtual ProductGroup ProductGroup { get; set; }

		public int? ProductGroupId { get; set; }

		public virtual ICollection<MenuItem> MenuItems { get; set; }
	}
}
﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Menu.Categories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Menu.ProductGroups
{
	[Table("ProductGroups")]
	public class ProductGroup : ConnectOrgFullMultiTenantAuditEntity
	{
		public ProductGroup()
		{
			Categories = new HashSet<Category>();
		}

		[StringLength(ProductGroupConsts.MaxCodeLength, MinimumLength = ProductGroupConsts.MinCodeLength)]
		public virtual string Code { get; set; }

		[StringLength(ProductGroupConsts.MaxNameLength, MinimumLength = ProductGroupConsts.MinNameLength)]
		public virtual string Name { get; set; }

		public virtual ICollection<Category> Categories { get; set; }

		[ForeignKey("ParentId")]
		public virtual ProductGroup Parent { get; set; }

		public virtual int? ParentId { get; set; }
	}
}
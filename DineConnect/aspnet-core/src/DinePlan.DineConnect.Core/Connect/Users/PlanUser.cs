﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Departments;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Users
{
	public class DinePlanUser : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual string Code { get; set; }
		public virtual string Name { get; set; }

		public virtual string PinCode { get; set; }
		public virtual string SecurityCode { get; set; }

		[ForeignKey("DinePlanUserRoleId")]
		public virtual DinePlanUserRole DinePlanUserRole { get; set; }

		public virtual int? DinePlanUserRoleId { get; set; }

		[ForeignKey("UserId")]
		public virtual User User { get; set; }

		public long? UserId { get; set; }

		public string LanguageCode { get; set; }
		public string AlternateLanguageCode { get; set; }
	}

	public class DinePlanUserRole : FullAuditedEntity, IMustHaveTenant
	{
		public virtual string Name { get; set; }
		public virtual int TenantId { get; set; }
		public virtual bool IsAdmin { get; set; }

		[ForeignKey("DepartmentId")]
		public virtual Department Department { get; set; }

		public virtual int? DepartmentId { get; set; }
	}

	public class DinePlanPermission : FullAuditedEntity, IMustHaveTenant
	{
		public virtual string Name { get; set; }
		public virtual bool IsGrantedByDefault { get; set; }
		public virtual int? DinePlanUserRoleId { get; set; }
		public virtual int TenantId { get; set; }
	}
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Messages.TickMessage
{
    [Table("TickMessages")]
    public class TickMessage : ConnectFullMultiTenantAuditEntity
    {
        public string MessageHeading { get; set; }
        public string MessageContent { get; set; }
        public TickMessageType MessageType { get; set; }
        public bool Acknowledgement { get; set; }
        public bool OneTimeMessage { get; set; }
        public bool InstantMessage { get; set; }

        public bool Reply { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TimeIntervals { get; set; }
        //public virtual ICollection<TickMessageReply> Replies { get; set; }
    }

    public enum TickMessageType
    {
        Text = 0,
        Image = 1,
        Video = 2,
        PDF = 3,
        Word = 4,
        Excel = 5
    }
}
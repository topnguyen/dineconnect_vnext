﻿using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Sync;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Connect.Sync
{
    [Table("LocationSyncers")]
    public class LocationSyncer : CreationAuditedEntity
    {

        public virtual Guid SyncerGuid { get; set; }

        public virtual DateTime LastSyncTime { get; set; }

        public virtual int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location LocationFk { get; set; }

        public virtual int SyncerId { get; set; }

        [ForeignKey("SyncerId")]
        public Syncer SyncerFk { get; set; }

    }
}
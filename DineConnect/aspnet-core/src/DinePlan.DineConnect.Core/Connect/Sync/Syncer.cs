﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Sync
{
	[Table("Syncers")]
	public class Syncer : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		public virtual string Name { get; set; }

		public virtual Guid SyncerGuid { get; set; }
		public virtual ICollection<LocationSyncer> LocationSyncers { get; set; }
	}
}
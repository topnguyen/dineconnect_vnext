﻿using DinePlan.DineConnect.Connect.Master.Locations;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Connect.Sync
{
    [Table("LastSyncs")]
    public class LastSync : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual DateTime LastPush { get; set; }

        public virtual DateTime LastPull { get; set; }

        public virtual string Version { get; set; }

        public virtual string SystemIp { get; set; }

        public virtual int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location LocationFk { get; set; }

    }
}
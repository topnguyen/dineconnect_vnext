﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Tag.OrderTags
{
	[Table("OrderTagGroups")]
	public class OrderTagGroup : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public string Name { get; set; }
		public int MaxSelectedItems { get; set; }
		public int MinSelectedItems { get; set; }
		public int SortOrder { get; set; }
		public bool AddTagPriceToOrderPrice { get; set; }
		public bool SaveFreeTags { get; set; }
		public bool FreeTagging { get; set; }
		public bool TaxFree { get; set; }
		public string Prefix { get; set; }
		public string Departments { get; set; }
		public virtual ICollection<OrderTag> Tags { get; set; }
		public virtual ICollection<OrderTagMap> Maps { get; set; }
		public string Files { get; set; }
		public virtual Guid DownloadImage { get; set; }
	}

	[Table("OrderTags")]
	public class OrderTag : ConnectCreateAuditEntity
	{
		public string Name { get; set; }
		public string AlternateName { get; set; }
		public int SortOrder { get; set; }
		public decimal Price { get; set; }
		public int MaxQuantity { get; set; }

		[ForeignKey("OrderTagGroupId")]
		public virtual OrderTagGroup OrderTagGroup { get; set; }

		public virtual int OrderTagGroupId { get; set; }
		public virtual int? MenuItemId { get; set; }
		public string Files { get; set; }
		public virtual Guid DownloadImage { get; set; }
		public string Tag { get; set; }
	}

	[Table("OrderTagLocationPrices")]
	public class OrderTagLocationPrice : ConnectCreateAuditEntity
	{
		[ForeignKey("OrderTagId")]
		public OrderTag OrderTag { get; set; }

		public int OrderTagId { get; set; }

		[ForeignKey("LocationId")]
		public Location Location { get; set; }

		public int LocationId { get; set; }
		public decimal Price { get; set; }
	}

	[Table("OrderTagMaps")]
	public class OrderTagMap : ConnectCreateAuditEntity
	{
		[ForeignKey("OrderTagGroupId")]
		public virtual OrderTagGroup OrderTagGroup { get; set; }

		public virtual int OrderTagGroupId { get; set; }
		public virtual int? MenuItemId { get; set; }
		public virtual int? CategoryId { get; set; }
	}
}
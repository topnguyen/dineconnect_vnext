﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.TicketTags;

namespace DinePlan.DineConnect.Connect.Tag.TicketTags
{

    [Table("TicketTagGroups")]
    public class TicketTagGroup : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual string Departments { get; set; }
        public virtual bool SaveFreeTags { get; set; }
        public virtual bool FreeTagging { get; set; }
        public virtual bool ForceValue { get; set; }
        public virtual bool AskBeforeCreatingTicket { get; set; }
        public virtual int DataType { get; set; }
        public virtual int SubTagId { get; set; }
        public bool IsAlphanumeric => DataType == 0;
        public bool IsInteger => DataType == 1;
        public bool IsDecimal => DataType == 2;
        public virtual ICollection<TicketTag> Tags { get; set; }
    }

    [Table("TicketTags")]
    public class TicketTag : ConnectCreateAuditEntity
    {
        public string Name { get; set; }
        public string AlternateName { get; set; }
        [ForeignKey("TicketTagGroupId")]
        public virtual TicketTagGroup TicketTagGroup { get; set; }
        public virtual int TicketTagGroupId { get; set; }
    }
}
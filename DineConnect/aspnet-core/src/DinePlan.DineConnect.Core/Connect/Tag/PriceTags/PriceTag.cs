﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Tag.PriceTags
{
	[Table("PriceTags")]
	public class PriceTag : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual int Type { get; set; }

		[StringLength(10)]
		public virtual string Name { get; set; }

		public virtual ICollection<PriceTagDefinition> PriceTagDefinitions { get; set; }
		public virtual string Departments { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public bool IsNormal => Type == 0;
		public bool IsDate => Type == 1;
	}

	[Table("PriceTagDefinitions")]
	public class PriceTagDefinition : ConnectCreateAuditEntity
	{
		[ForeignKey("TagId")]
		public virtual PriceTag PriceTag { get; set; }

		public virtual int TagId { get; set; }

		[ForeignKey("MenuPortionId")]
		public virtual MenuItemPortion MenuPortion { get; set; }

		public virtual int MenuPortionId { get; set; }
		public virtual decimal Price { get; set; }
	}

	[Table("PriceTagLocationPrices")]
	public class PriceTagLocationPrice : ConnectCreateAuditEntity
	{
		[ForeignKey("PriceTagDefinitionId")]
		public PriceTagDefinition PriceTagDefinition { get; set; }

		public int PriceTagDefinitionId { get; set; }

		[ForeignKey("LocationId")]
		public Location Location { get; set; }

		public int LocationId { get; set; }

		public decimal Price { get; set; }
	}
}
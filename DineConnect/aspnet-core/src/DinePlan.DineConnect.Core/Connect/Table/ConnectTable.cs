﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Table
{
    public class ConnectTable : ConnectFullMultiTenantAuditEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }
        
        public int Pax { get; set; }

        public List<ConnectTableInGroup> ConnectTableInGroups { get; set; }
    }
}

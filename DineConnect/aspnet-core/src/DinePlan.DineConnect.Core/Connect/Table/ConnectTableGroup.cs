﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Table
{
    public class ConnectTableGroup : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public List<ConnectTableInGroup> ConnectTableInGroups { get; set; }
    }
}

﻿using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Table
{
    public class ConnectTableInGroup: CreationAuditedEntity
    {
        public int ConnectTableId { get; set; }

        public virtual ConnectTable ConnectTable { get; set; }

        public int ConnectTableGroupId { get; set; }

        public virtual ConnectTableGroup ConnectTableGroup { get; set; }
    }
}

﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Transaction
{
	[Table("Payments")]
	public class Payment : CreationAuditedEntity
	{
		[ForeignKey("PaymentTypeId")]
		public virtual Master.PaymentTypes.PaymentType PaymentType { get; set; }

		public virtual int PaymentTypeId { get; set; }

		[ForeignKey("TicketId")]
		public virtual Ticket Ticket { get; set; }

		public virtual int TicketId { get; set; }

		public virtual DateTime PaymentCreatedTime { get; set; }
		public virtual decimal TenderedAmount { get; set; }
		public virtual string TerminalName { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual string PaymentUserName { get; set; }
		public virtual string PaymentTags { get; set; }

		private IList<string> _paymentTagValues;

		internal IList<string> PaymentTagValues
		{
			get
			{
				return _paymentTagValues ?? (_paymentTagValues = PaymentTags.Split(","));
			}
		}

		public IList<string> GetPaymentTagValues()
		{
			return PaymentTagValues;
		}
	}
}
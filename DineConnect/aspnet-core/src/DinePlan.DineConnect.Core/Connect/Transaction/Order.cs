﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Transaction
{
    [Table("Orders")]
    public class Order : CreationAuditedEntity
    {
        private IList<OrderTagValue> _orderTagValues;
        private IList<TaxValue> _taxValues;
        private IList<PromotionDetailValue> _promoDetails;

        public Order()
        {
            TransactionOrderTags = new List<TransactionOrderTag>();
        }

        public virtual int OrderId { get; set; }

        [JsonIgnore]
        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }

        [ForeignKey("Location_Id")] public virtual Location Location { get; set; }

        public virtual int Location_Id { get; set; }

        public virtual int TicketId { get; set; }

        public virtual int MenuItemType { get; set; }
        public virtual string OrderRef { get; set; }
        public virtual bool IsParent { get; set; }
        public virtual bool IsUpSelling { get; set; }
        public virtual string UpSellingOrderRef { get; set; }

        [StringLength(50)] public virtual string DepartmentName { get; set; }

        [ForeignKey("MenuItemId")] public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }

        [StringLength(100)] public virtual string MenuItemName { get; set; }

        [StringLength(50)] public virtual string PortionName { get; set; }

        public virtual decimal Price { get; set; }
        public virtual decimal CostPrice { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual int PortionCount { get; set; }
        public virtual string Note { get; set; }
        public virtual bool Locked { get; set; }
        public virtual bool CalculatePrice { get; set; }
        public virtual bool IncreaseInventory { get; set; }
        public virtual bool DecreaseInventory { get; set; }
        public virtual string OrderNumber { get; set; }

        [StringLength(50)] public virtual string CreatingUserName { get; set; }

        public virtual DateTime OrderCreatedTime { get; set; }

        [StringLength(50)] public virtual string PriceTag { get; set; }

        public virtual string Taxes { get; set; }
        public virtual string OrderTags { get; set; }
        public virtual string OrderStates { get; set; }
        public virtual string OrderStatus { get; set; }
        public virtual bool IsPromotionOrder { get; set; }
        public virtual decimal PromotionAmount { get; set; }
        public virtual int PromotionSyncId { get; set; }

        [ForeignKey("MenuItemPortionId")]
        public virtual MenuItemPortion MenuItemPortion { get; set; }

        public virtual int MenuItemPortionId { get; set; }
        public virtual IList<TransactionOrderTag> TransactionOrderTags { get; set; }

        public virtual string OrderPromotionDetails { get; set; }
        public virtual string OrderLog { get; set; }

        internal IList<PromotionDetailValue> OrderPromotionDetailsList =>
            _promoDetails ?? (_promoDetails = JsonHelper.Deserialize<List<PromotionDetailValue>>(OrderPromotionDetails));

        public IList<PromotionDetailValue> GetOrderPromotionList()
        {
            return OrderPromotionDetailsList;
        }

        internal IList<TaxValue> TaxValues =>
            _taxValues ?? (_taxValues = JsonHelper.Deserialize<List<TaxValue>>(Taxes));

        internal IList<OrderTagValue> OrderTagValues =>
            _orderTagValues ?? (_orderTagValues = JsonHelper.Deserialize<List<OrderTagValue>>(OrderTags));

        public IList<OrderTagValue> GetOrderTagValues()
        {
            return OrderTagValues;
        }

        private IList<OrderStateValue> _orderStateValues;

        internal IList<OrderStateValue> OrderStateValues
        {
            get
            {
                return _orderStateValues ??
                       (_orderStateValues = JsonHelper.Deserialize<List<OrderStateValue>>(OrderStates));
            }
        }

        public IList<OrderStateValue> GetOrderStateValues()
        {
            return OrderStateValues;
        }

        public decimal GetTaxablePrice()
        {
            var result = Price + OrderTagValues.Where(x => !x.TaxFree && !x.AddTagPriceToOrderPrice).Sum(x => x.Price * x.Quantity);
            return result;
        }

        public decimal GetTaxableOriginalPrice()
        {
            var result = OriginalPrice + OrderTagValues.Where(x => !x.TaxFree && !x.AddTagPriceToOrderPrice).Sum(x => x.Price * x.Quantity);
            return result;
        }

        public decimal GetTaxAmount(bool taxIncluded, decimal plainSum, decimal preTaxServices)
        {
            if (!CalculatePrice)
                return 0;

            var myReturnTax = 0M;
            foreach (var myTax in TaxValues)
            {
                var retuTax = myTax.GetTaxAmount(taxIncluded, GetTaxablePrice(), TaxValues.Sum(y => y.TaxRate),
                    plainSum,
                    preTaxServices);

                myReturnTax += decimal.Round(retuTax, 2, MidpointRounding.AwayFromZero);
            }

            return myReturnTax;
        }

        public decimal GetTotal()
        {
            if (CalculatePrice)
            {
                return GetValue();
            }
            return 0;
        }

        public decimal GetOriginalPriceTotal()
        {
            if (CalculatePrice)
            {
                return GetValue();
            }
            return 0;
        }

        public decimal GetValue()
        {
            return GetTaxablePrice() * Quantity;
        }

        public decimal GetOriginalValue()
        {
            return GetTaxableOriginalPrice() * Quantity;
        }

        public decimal GetPrice()
        {
            var result = Price + OrderTagValues.Sum(x => x.Price * x.Quantity);

            return result;
        }

        public decimal GetTotalTaxAmount(bool taxIncluded, decimal plainSum, decimal preTaxServices)
        {
            if (!CalculatePrice)
                return 0;
            var myReturnTax = 0M;
            foreach (var myTax in TaxValues)
            {
                var retuTax = myTax.GetTaxAmount(taxIncluded, GetTaxablePrice(), TaxValues.Sum(y => y.TaxRate),
                                  plainSum,
                                  preTaxServices) * Quantity;

                myReturnTax += retuTax;
            }

            return myReturnTax;
        }

        public bool IsInState(string stateName, string state, string stateValue)
        {
            state = state.Trim();
            stateName = stateName.Trim();
            stateValue = stateValue.Trim();
            return OrderStateValues.Any(x => x.StateName == stateName && x.State == state && x.StateValue == stateValue);
        }

        public decimal GetTaxTotal()
        {
            decimal taxPrice = 0M;

            if (!TaxValues.Any())
            {
                return 0M;
            }
            foreach (var tax in TaxValues)
            {
                if (Ticket.TaxIncluded)
                {
                    var tRate = 100 + tax.TaxRate;
                    var taxVale = (GetTaxablePrice() * tRate / 100) - GetTaxablePrice();
                    taxPrice += taxVale;
                }
                else
                {
                    taxPrice += (GetTaxablePrice() * tax.TaxRate / 100);
                }
            }
            return taxPrice;
        }

        public string GetPortionDesc()
        {
            if (PortionCount > 1
                && !string.IsNullOrEmpty(PortionName)
                && !string.IsNullOrEmpty(PortionName.Trim('\b', ' ', '\t'))
                && PortionName.ToLower() != "normal")
                return "." + PortionName;
            return "";
        }

        public string Description
        {
            get
            {
                var desc = MenuItemName + GetPortionDesc();
                //if (SelectedQuantity > 0 && SelectedQuantity != Quantity)
                //    desc = $"({SelectedQuantity:#.##}) {desc}";
                return desc;
            }
        }

        public decimal OriginalPrice { get; set; }
    }

    [Table("TransactionOrderTags")]
    public class TransactionOrderTag : CreationAuditedEntity
    {
        [ForeignKey("OrderId")] public virtual Order Order { get; set; }

        public virtual int OrderId { get; set; }

        public int OrderTagGroupId { get; set; }
        public int OrderTagId { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string TagName { get; set; }
        public string TagNote { get; set; }
        public string TagValue { get; set; }
        public bool TaxFree { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
    }
}
﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Transactions;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Transaction
{
	[Table("Tickets")]
	public class Ticket : FullAuditedEntity, IMustHaveTenant
	{
		public virtual int TicketId { get; set; }

		[ForeignKey("LocationId")]
		public virtual Location Location { get; set; }

		public virtual int LocationId { get; set; }

		[StringLength(100)]
		public virtual string TicketNumber { get; set; }

		public virtual string InvoiceNo { get; set; }
		public virtual DateTime TicketCreatedTime { get; set; }
		public virtual DateTime LastUpdateTime { get; set; }
		public virtual DateTime LastOrderTime { get; set; }
		public virtual DateTime LastPaymentTime { get; set; }
		public virtual bool IsClosed { get; set; }
		public virtual bool IsLocked { get; set; }
		public virtual decimal RemainingAmount { get; set; }
		public virtual decimal TotalAmount { get; set; }
		public virtual string DepartmentName { get; set; }
		public virtual string DepartmentGroup { get; set; }
		public virtual string TicketTypeName { get; set; }
		public virtual string Note { get; set; }
		public virtual string LastModifiedUserName { get; set; }
		public virtual string TicketTags { get; set; }
		public virtual string TicketStates { get; set; }
		public virtual string TicketLogs { get; set; }
		public virtual bool TaxIncluded { get; set; }
		public virtual string TerminalName { get; set; }
		public virtual bool PreOrder { get; set; }
		public virtual string TicketEntities { get; set; }
		public virtual int WorkPeriodId { get; set; }
		public virtual bool Credit { get; set; }

		[Column(TypeName = "date")]
		public virtual DateTime LastPaymentTimeTruc { get; set; }

		[StringLength(100)]
		public virtual string ReferenceNumber { get; set; }

		[StringLength(100)]
		public virtual string QueueNumber { get; set; }

		public virtual bool CreditProcessed { get; set; }
		public virtual string FullTaxInvoiceDetails { get; set; }
		public virtual string TicketPromotionDetails { get; set; }

		public string ReferenceTicket { get; set; }
		public virtual int TenantId { get; set; }

		public virtual bool ExternalProcessed { get; set; }

		public virtual string TicketStatus { get; set; }

		public virtual IList<Order> Orders { get; set; }
		public virtual IList<Payment> Payments { get; set; }
		public virtual IList<TicketTransaction> Transactions { get; set; }

		private IList<PromotionDetailValue> _promotionDetailValues;

		internal IList<PromotionDetailValue> TicketPromotionDetailsList =>
			_promotionDetailValues ?? (_promotionDetailValues = JsonHelper.Deserialize<List<PromotionDetailValue>>(TicketPromotionDetails));

		public IList<PromotionDetailValue> GetTicketPromotionList()
		{
			return TicketPromotionDetailsList;
		}

		private IList<TicketStateValue> _ticketStateValues;

		internal IList<TicketStateValue> TicketStateValues => _ticketStateValues ??
															  (_ticketStateValues =
																  JsonHelper.Deserialize<List<TicketStateValue>>(
																	  TicketStates));

		public IEnumerable<TicketStateValue> GetTicketStateValues()
		{
			return TicketStateValues;
		}

		private IList<TicketTagValue> _ticketTagValues;

		internal IList<TicketTagValue> TicketTagValues =>
			_ticketTagValues ?? (_ticketTagValues = JsonConvert.DeserializeObject<List<TicketTagValue>>(TicketTags));

		private IList<TicketLogValue> _ticketLogValues;

		internal IList<TicketLogValue> TicketLogValues =>
			_ticketLogValues ?? (_ticketLogValues = JsonHelper.Deserialize<List<TicketLogValue>>(TicketLogs));

		public IEnumerable<TicketLogValue> GetTicketLogValues()
		{
			return TicketLogValues;
		}

		public bool IsTagged
		{
			get { return TicketTagValues.Any(x => !string.IsNullOrEmpty(x.TagValue)); }
		}

		public decimal GetPlainSum()
		{
			return Orders.Sum(item => item.GetTotal());
		}

		public decimal GetPreTaxServicesTotal()
		{
			var plainSum = GetPlainSum();
			return plainSum;
		}

		public decimal GetTaxTotal()
		{
			var result = Orders.Sum(x => x.GetTotalTaxAmount(TaxIncluded, GetPlainSum(), GetPreTaxServicesTotal()));
			return decimal.Round(result, 2, MidpointRounding.AwayFromZero);
		}

		public bool IsInState(string stateName, string state)
		{
			stateName = stateName.Trim();
			state = state.Trim();
			if (stateName == "*") return TicketStateValues.Any(x => x.State.Equals(state));
			if (string.IsNullOrEmpty(state)) return TicketStateValues.All(x => !x.StateName.Equals(stateName));
			return TicketStateValues.Any(x => x.StateName.Equals(stateName) && x.State.Equals(state));
		}

		public void SetLogs(IList<TicketLogValue> logs)
		{
			TicketLogs = JsonHelper.Serialize(logs);
			_ticketLogValues = null;
		}

		public void AddLog(string userName, string category, string log)
		{
			var lm = new TicketLogValue(TicketNumber, userName) { Category = category, Log = log };
			TicketLogValues.Add(lm);
			SetLogs(TicketLogValues);
		}

		public string GetLog(string category)
		{
			if (!string.IsNullOrEmpty(TicketLogs))
			{
				var myLog = TicketLogValues.LastOrDefault(a => a.Category.Equals(category));
				if (myLog != null) return myLog.Log;
			}

			return null;
		}

		public TicketStateValue GetState(string groupName)
		{
			return TicketStateValues.FirstOrDefault(x => x.State.Equals(groupName)) ?? TicketStateValue.Default;
		}

		public IEnumerable<TicketLogValue> GetLogTicketLogs(string category)
		{
			if (!string.IsNullOrEmpty(TicketLogs))
			{
				var myLog = TicketLogValues.Where(a => a.Category.Equals(category));
				return myLog;
			}

			return null;
		}

		public IEnumerable<TicketTagValue> GetTicketTagValues()
		{
			return TicketTagValues;
		}

		public string GetTicketTagValue(string name)
		{
			if (!string.IsNullOrEmpty(name))
			{
				var tt = TicketTagValues.SingleOrDefault(a => a.TagName.Equals(name));
				if (tt != null) return tt.TagValue;
			}

			return "";
		}

		public string GetTagData()
		{
			return string.Join("\r",
				TicketTagValues.Where(x => !string.IsNullOrEmpty(x.TagValue))
					.Select(x => string.Format("{0}: {1}", x.TagName, x.TagValue)));
		}

		public decimal GetIncludedAmount()
		{
			var taxAmout = TotalAmount * 7 / 107;
			return taxAmout;
		}

		public decimal CalculateTax(decimal plainSum, decimal preTaxServices)
		{
			var taxTotal = 0M;
			foreach (var order in Orders.Where(x => x.CalculatePrice))
			{
				taxTotal += order.GetTotalTaxAmount(TaxIncluded, GetPlainSum(), preTaxServices);
			}
			var myRound = decimal.Round(taxTotal, 2, MidpointRounding.AwayFromZero);
			return myRound;
		}
		public Ticket()
        {
			Orders = new List<Order>();
        }
	}
}
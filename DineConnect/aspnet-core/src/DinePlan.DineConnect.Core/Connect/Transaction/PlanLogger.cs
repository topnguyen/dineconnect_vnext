﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Transaction
{
    [Table("PlanLoggers")]
    public class PlanLogger : FullAuditedEntity, IMustHaveTenant
    {
        public virtual string Terminal { get; set; }
        public virtual string TicketNo { get; set; }
        public virtual decimal TicketTotal { get; set; }
        public virtual string UserName { get; set; }
        public virtual string EventLog { get; set; }
        public virtual string EventName { get; set; }
        public virtual DateTime EventTime { get; set; }

        public virtual int TenantId { get; set; }
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }

    }


}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Helper;

namespace DinePlan.DineConnect.Connect.Transaction
{

    [Table("PostDatas")]
    public class PostData : FullAuditedEntity, IMustHaveTenant
    {
        private IList<PreContentValue> _preContentValues;

        private IList<PreErrorValue> _preErrorValues;

        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        public virtual string Contents { get; set; }

        public virtual string Errors { get; set; }

        public virtual bool Processed { get; set; }

        public virtual int ContentType { get; set; }

        internal IList<PreContentValue> PreContentValues => _preContentValues ?? (_preContentValues = JsonHelper.Deserialize<List<PreContentValue>>(Contents));

        internal IList<PreErrorValue> PreErrorValues => _preErrorValues ?? (_preErrorValues = JsonHelper.Deserialize<List<PreErrorValue>>(Errors));

        public void AddPreContent(PreContentValue preContentValue, bool applyStack)
        {
            if (!applyStack) PreContentValues.Clear();
            PreContentValues.Add(preContentValue);
            Contents = JsonHelper.Serialize(PreContentValues);
        }

        public void AddPreContents(List<PreContentValue> preContents)
        {
            PreContentValues.Clear();

            if (preContents.Any())
            {
                foreach (var pdv in preContents) PreContentValues.Add(pdv);
                Contents = JsonHelper.Serialize(PreContentValues);
            }
            else
            {
                Contents = null;
            }
        }

        public void AddErrorContent(PreErrorValue preErrorValue, bool applyStack)
        {
            if (!applyStack) PreErrorValues.Clear();
            PreErrorValues.Add(preErrorValue);
            Errors = JsonHelper.Serialize(PreErrorValues);
        }

        public void AddErrorContents(List<PreErrorValue> preErrorValues)
        {
            PreErrorValues.Clear();

            if (preErrorValues.Any())
            {
                foreach (var pdv in preErrorValues) PreErrorValues.Add(pdv);
                Errors = JsonHelper.Serialize(PreErrorValues);
            }
            else
            {
                Errors = null;
            }
        }

        public int TenantId { get; set; }
        public int Tried { get; set; }
    }

    [DataContract]
    public class PreContentValue : IEquatable<PreContentValue>
    {
        public string Contents { get; set; }

        public bool Equals(PreContentValue obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PreContentValue) obj);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PreContentValue) obj);
        }

        public override int GetHashCode()
        {
            return GetHashCode();
        }
    }


    [DataContract]
    public class PreErrorValue : IEquatable<PreErrorValue>
    {
        public string Contents { get; set; }

        public bool Equals(PreErrorValue obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PreErrorValue) obj);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PreErrorValue) obj);
        }

        public override int GetHashCode()
        {
            return GetHashCode();
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace DinePlan.DineConnect.Connect.OrderDetail
{
	[Table("OrderDetails")]
    [Audited]
    public class OrderDetail : Entity<long> , IMustHaveTenant
    {
			public int TenantId { get; set; }
			

		public virtual DateTime CreationTime { get; set; }
		
		public virtual long? CreatorUserId { get; set; }
		
		public virtual DateTime? LastModificationTime { get; set; }
		
		public virtual long? LastModifierUserId { get; set; }
		
		public virtual bool IsDeleted { get; set; }
		
		public virtual long? DeleterUserId { get; set; }
		
		public virtual DateTime? DeletionTime { get; set; }
		
		public virtual int CheckoutStatus { get; set; }
		
		public virtual decimal Amount { get; set; }
		
		public virtual string OrderNotes { get; set; }
		
		[Required]
		public virtual string OrderDetails { get; set; }
		
		public virtual int DeliveryLocationId { get; set; }
		
		[Required]
		public virtual string Building { get; set; }
		
		[Required]
		public virtual string Street { get; set; }
		
		public virtual int PostalCode { get; set; }
		
		public virtual string FlatNo { get; set; }
		
		public virtual string Landmark { get; set; }
		
		public virtual int OrderPaymentMethod { get; set; }
		
		[Required]
		public virtual string Currency { get; set; }
		

    }
}
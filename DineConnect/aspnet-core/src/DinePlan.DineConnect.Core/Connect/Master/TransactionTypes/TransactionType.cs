﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.TransactionTypes
{
    [Table("TransactionTypes")]
    public class TransactionType : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        [StringLength(TransactionTypeConsts.MaxNameLength, MinimumLength = TransactionTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual bool Tax { get; set; }

        public virtual bool Discount { get; set; }

        public virtual string AccountCode { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.TicketTypes;

namespace DinePlan.DineConnect.Connect.Master.Terminals
{
    [Table("Terminals")]
    public class Terminal : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual string TerminalCode { get; set; }

        public virtual decimal Float { get; set; }

        public decimal Tolerance { get; set; }

        public virtual string Denominations { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual bool AutoLogout { get; set; }

        public virtual bool IsEndWorkTimeMoneyShown { get; set; }

        public virtual bool IsCollectionTerminal { get; set; }

        public virtual bool WorkTimeTotalInFloat { get; set; }

        public string Tid { get; set; }
        public bool AcceptTolerance { get; set; }

        [ForeignKey("TicketTypeId")]
        public TicketTypeConfiguration TicketType { get; set; }

        public int? TicketTypeId { get; set; }

        public virtual bool AutoDayClose { get; set; }
        public virtual string AutoDayCloseSettings { get; set; }

        public string Settings { get; set; }
        public string SerialNumber { get; set; }
    }
}
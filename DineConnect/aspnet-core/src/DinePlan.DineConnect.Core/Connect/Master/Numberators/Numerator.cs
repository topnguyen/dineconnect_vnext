﻿using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.Numberators
{
	[Table("Numerators")]
	public class Numerator : ConnectFullMultiTenantAuditEntity
	{
		public Numerator()
		{
			NumberFormat = "#";

			OutputFormat = "[Number]";
		}

		public virtual string Name { get; set; }

		public virtual string NumberFormat { get; set; }

		public virtual string OutputFormat { get; set; }

		public virtual bool ResetNumerator { get; set; }

		public virtual ResetNumeratorType ResetNumeratorType { get; set; }
	}
}
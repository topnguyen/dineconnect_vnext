﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Departments;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.Printer
{
	#region Enum

	public enum PrintConfigurationType
	{
		Printer = 0,
		PrintTemplate = 1,
		PrintJob = 2,
		PrintCondition = 3
	}

	public enum PrinterTypes
	{
		ReceiptPrinter = 0,
		Text = 1,
		HTML = 2,
		PortPrinter = 3,
		DemoPrinter = 4,
		WindowsPrinter = 5,
		CustomPrinter = 6,
		RawPrinter = 7,
		NetworkPrinter = 8,
		PortTicketPrinter = 9,
		CopyPrinter = 10,
		DocumentPrinter = 11,
	}

	public enum WhatToPrintTypes
	{
		Everything,
		LastLinesByPrinterLineCount,
		LastPaidOrders,
		OrdersByQuantity,
		SeparatedByQuantity,
		CoursePrinting,
		EverythingExcludeZeroPrice,
		EverythingReverse,
        
	}

	public enum PrintTemplateType
	{
		Receipt = 0,
		Bill = 1,
		Kitchen = 2
	}

	#endregion Enum

	[Table("PrintConfigurations")]
	public class PrintConfiguration : ConnectFullMultiTenantAuditEntity
	{
		[Required]
		[MaxLength(50)]
		public string Name { get; set; }

		public int TenantId { get; set; }
		public PrintConfigurationType PrintConfigurationType { get; set; }
	}

	[Table("Printers")]
	public class Printer : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }

		public int PrinterType { get; set; }
		public int CodePage { get; set; }
		public int CharsPerLine { get; set; }
		public int NumberOfLines { get; set; }

		public int FallBackPrinter { get; set; }
		public string CharReplacement { get; set; }

		public string CustomPrinterName { get; set; }
		public string CustomPrinterData { get; set; }

		public Printer()
		{
			CharsPerLine = 42;
			CodePage = 857;
		}
	}

	[Table("PrintTemplates")]
	public class PrintTemplate : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }

		public virtual string Contents { get; set; }
		public virtual bool MergeLines { get; set; }
		public virtual string Files { get; set; }
	}

	[Table("PrintJobs")]
	public class PrintJob : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }
		public int WhatToPrint { get; set; }
		public bool Negative { get; set; }
		public bool ExcludeTax { get; set; }

		private ICollection<PrinterMap> _printerMaps;

		public virtual ICollection<PrinterMap> PrinterMaps
		{
			get { return _printerMaps; }
			set { _printerMaps = value; }
		}

		public PrintJob()
		{
			_printerMaps = new List<PrinterMap>();
		}

		public string JobGroup { get; set; }
		public WhatToPrintTypes WhatToPrintType => (WhatToPrintTypes)WhatToPrint;
	}

	[Table("PrinterMaps")]
	public class PrinterMap : ConnectFullAuditEntity
	{
		public virtual int? DepartmentId { get; set; }
		public virtual string MenuItemGroupCode { get; set; }
		public virtual int MenuItemId { get; set; }
		public virtual int CategoryId { get; set; }
		public virtual string CategoryName { get; set; }
		public virtual int PrinterId { get; set; }
		public int PrintTemplateId { get; set; }

		public int? PrintJobId { get; set; }

		[ForeignKey("PrintJobId")]
		public virtual PrintJob PrintJob { get; set; }

		[ForeignKey("DepartmentId")]
		public virtual Department Department { get; set; }
	}

	[Table("PrintTemplateConditions")]
	public class PrintTemplateCondition : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public virtual int PrintConfigurationId { get; set; }
		public string AliasName { get; set; }

		public virtual DateTime StartDate { get; set; }
		public virtual DateTime EndDate { get; set; }
		public virtual string ConditionSchedules { get; set; }
		public virtual string Filter { get; set; }
		public virtual string Contents { get; set; }
	}
}
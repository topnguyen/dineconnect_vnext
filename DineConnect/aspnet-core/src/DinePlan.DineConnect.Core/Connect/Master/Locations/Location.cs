﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.LocationGroups;
using DinePlan.DineConnect.Connect.Master.LocationTags;
using DinePlan.DineConnect.Connect.Master.UserLocations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.DineQueue;

namespace DinePlan.DineConnect.Connect.Master.Locations
{
    [Table("Locations")]
    public class Location : ConnectFullMultiTenantAuditEntity
    {
        public Location()
        {
            LocationMenuItems = new HashSet<LocationMenuItem>();
            UserLocations = new HashSet<UserLocation>();
            LocationGroups = new HashSet<DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup>();
            LocationTags = new HashSet<LocationTagLocation>();
            MenuItems = new List<LocationMenuItem>();
            Prices = new List<LocationMenuItemPrice>();
        }

        // Basic
        public int LocationGroupId { get; set; }

        [ForeignKey("OrganizationId")]
        public Brand Organization { get; set; }

        public int OrganizationId { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual string Code { get; set; }

        public virtual ICollection<DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup> LocationGroups { get; set; }

        public virtual ICollection<LocationTagLocation> LocationTags { get; set; }

        // Address

        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public string PostalCode { get; set; }

        public virtual City City { get; set; }

        public virtual int? CityId { get; set; }
        public virtual string District { get; set; }

        //public virtual string City { get; set; }
        public virtual string State { get; set; }

        public virtual string Country { get; set; }

        public virtual string PhoneNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual ICollection<LocationSchedule> Schedules { get; set; }
        public virtual long OrganizationUnitId { get; set; }
        public virtual bool IsPurchaseAllowed { get; set; }
        public virtual bool IsProductionAllowed { get; set; }
        public virtual int DefaultRequestLocationRefId { get; set; }
        public virtual string RequestedLocations { get; set; }
        public virtual DateTime? HouseTransactionDate { get; set; }
        public bool DoesDayCloseRunInBackGround { get; set; }
        public DateTime? BackGroundStartTime { get; set; }
        public DateTime? BackGrandEndTime { get; set; }
        public virtual decimal SharingPercentage { get; set; }
        public virtual bool TaxInclusive { get; set; }
        public virtual bool ChangeTax { get; set; }
        public bool IsProductionUnitAllowed { get; set; }
        public virtual int ExtendedBusinessHours { get; set; }
        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
        public virtual string ReceiptHeader { get; set; }
        public virtual string ReceiptFooter { get; set; }

        // Addon

        public string AddOn { get; set; }
        public virtual string ReferenceCode { get; set; }
        public string LocationTaxCode { get; set; }

        public virtual int? BranchId { get; set; }

        [ForeignKey(nameof(BranchId))]
        public virtual LocationBranch Branch { get; set; }

        // Queue Location

        public virtual int? QueueLocationId { get; set; }

        [ForeignKey(nameof(QueueLocationId))]
        public virtual QueueLocation QueueLocation { get; set; }

        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }
        public virtual ICollection<LocationMenuItem> LocationMenuItems { get; set; }
        public virtual ICollection<UserLocation> UserLocations { get; set; }
        public virtual List<LocationMenuItem> MenuItems { get; set; }
        public virtual List<LocationMenuItemPrice> Prices { get; set; }
        public bool RestrictSync { get; set; }
    }

    [Table("LocationBranch")]
    public class LocationBranch : CreationAuditedEntity
    {
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public virtual string Address3 { get; set; }

        public virtual string District { get; set; }

        public virtual string City { get; set; }

        public virtual string State { get; set; }

        public virtual string Country { get; set; }

        public virtual string PostalCode { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual string Fax { get; set; }

        public virtual string Website { get; set; }

        public virtual string Email { get; set; }

        public virtual string BranchTaxCode { get; set; }

        public virtual string AddOns { get; set; }

        public virtual ICollection<Location> Locations { get; set; }
    }

    [Table("LocationSchedules")]
    public class LocationSchedule : CreationAuditedEntity
    {
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        public virtual string Name { get; set; }

        public virtual int StartHour { get; set; }

        public virtual int StartMinute { get; set; }

        public virtual int EndHour { get; set; }

        public virtual int EndMinute { get; set; }
    }
}
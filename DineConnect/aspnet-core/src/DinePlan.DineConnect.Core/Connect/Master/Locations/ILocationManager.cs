﻿using DinePlan.DineConnect.Connect.Master.Companies;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Locations
{
    public interface ILocationManager
    {
        Task<int> CreateOrUpdteAsync(Location location);

        Task<Location> GetAsync(int id);

        IQueryable<Location> GetList(int organizationId, string filter);

        Task DeleteAsync(int id);

        IQueryable<Location> GetAllLocation(int organizationId, int countryId, int stateId, int cityId, string filter);

        Task<Location> GetLocationWithoutBrandFilter(int tenantId, int locationId);

        IQueryable<Location> GetListOfAllowedLocationForUser(int tenantId, long userId, int organizationId, int countryId, int stateId, int cityId, string filter);

        Task<List<Brand>> GetListOfAllowedBrandForUser(long userId);

        IQueryable<Location> GetAllLocationIgnoringBrandFilter(int organizationId, string filter, bool isDelete = false);
    }
}
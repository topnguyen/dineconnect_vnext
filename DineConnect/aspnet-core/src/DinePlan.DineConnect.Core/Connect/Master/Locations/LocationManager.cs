﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Connect.Master.Companies;
using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.LocationGroups;
using DinePlan.DineConnect.Connect.Master.LocationTags;
using DinePlan.DineConnect.Connect.Master.UserLocations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Extension;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Locations
{
    public class LocationManager : DineConnectServiceBase, ILocationManager, ITransientDependency
    {
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Address> _addressRepositotry;
        private readonly IRepository<LocationSchedule> _locationScheduleRepository;
        private readonly IRepository<LocationBranch> _locationBranchRepo;
        private readonly ConnectSession _connectSession;
        private readonly IRepository<UserLocation> _userLocationRepository;
        private readonly IRepository<LocationMenuItem> _locationMenuItemRepository;
        private readonly IRepository<DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup> _locationGroupRepository;
        private readonly IRepository<LocationTagLocation> _locationTagLocationRepository;
        private readonly IRepository<Group> _groupRepository;
        private readonly IRepository<LocationTag> _locationTagRepository;

        public LocationManager(IRepository<Location> location
            , IRepository<Address> addressRepositotry
            , IRepository<LocationSchedule> locationScheduleRepository
            , IRepository<LocationBranch> locationBranchRepo
            , IRepository<DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup> locationGroupRepository
            , IRepository<LocationMenuItem> locationMenuItemRepository
            , IRepository<LocationTagLocation> locationTagLocationRepository
            , ConnectSession connectSession
            , IRepository<UserLocation> userLocationRepository
            , IRepository<Group> groupRepository
            , IRepository<LocationTag> locationTagRepository

            )
        {
            _locationRepository = location;
            _addressRepositotry = addressRepositotry;
            _locationScheduleRepository = locationScheduleRepository;
            _locationBranchRepo = locationBranchRepo;
            _connectSession = connectSession;
            _userLocationRepository = userLocationRepository;
            _locationMenuItemRepository = locationMenuItemRepository;
            _locationGroupRepository = locationGroupRepository;
            _locationTagLocationRepository = locationTagLocationRepository;
            _groupRepository = groupRepository;
            _locationTagRepository = locationTagRepository;
        }

        public async Task<int> CreateOrUpdteAsync(Location location)
        {
            var exists = _locationRepository.GetAll().WhereIf(location.Id > 0, m => m.Id != location.Id);

            if (exists.Any(o => o.Name.Equals(location.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
            if (exists.Any(o => o.Code.Equals(location.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }

            var locationId = location.Id;

            if (location.Id > 0)
            {
                // Schedule

                var schedulesToDeleteIds =
                    _locationScheduleRepository
                        .GetAll()
                        .Where(x => x.LocationId == location.Id).Select(x => x.Id)
                        .ToList();

                foreach (var locationScheduleId in schedulesToDeleteIds)
                {
                    await _locationScheduleRepository.DeleteAsync(locationScheduleId);
                }

                if (location.Schedules?.Any() == true)
                {
                    foreach (var locationSchedule in location.Schedules)
                    {
                        locationSchedule.LocationId = location.Id;

                        await _locationScheduleRepository.InsertAsync(locationSchedule);
                    }
                }

                // Branch

                var branch = _locationRepository.GetAll().Where(x => x.Id == location.Id)
                    .Select(x => x.Branch)
                    .FirstOrDefault();

                if (branch != null)
                {
                    await _locationBranchRepo.DeleteAsync(branch);
                }

                location.BranchId = await _locationBranchRepo.InsertAndGetIdAsync(location.Branch);

                // Groups
                var locationGroupsExisting = _locationGroupRepository.GetAll().Where(x => x.LocationId == location.Id).ToList();

                foreach (var group in locationGroupsExisting)
                {
                    await _locationGroupRepository.DeleteAsync(group.Id);
                }

                foreach (var group in location.LocationGroups)
                {
                    if (group.Id == 0)
                    {
                        group.Group.Code = group.Group.Name;
                        var id = _groupRepository.InsertAndGetId(group.Group);
                        group.Group.Id = id;
                        group.Id = id;
                    }
                    _locationGroupRepository.Insert(new DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup
                    {
                        LocationId = location.Id,
                        GroupId = group.Id
                    });
                }

                // Tags
                var locationTagsExisting = _locationTagLocationRepository.GetAll().Where(x => x.LocationId == location.Id).ToList();
                foreach (var tag in locationTagsExisting)
                {
                    await _locationTagLocationRepository.DeleteAsync(tag.Id);
                }

                foreach (var tag in location.LocationTags)
                {
                    if (tag.Id == 0)
                    {
                        tag.LocationTag.Code = tag.LocationTag.Name;
                        var id = _locationTagRepository.InsertAndGetId(tag.LocationTag);
                        tag.Id = id;
                        tag.LocationTag.Id = id;
                    }
                    _locationTagLocationRepository.Insert(new LocationTagLocation
                    {
                        LocationId = location.Id,
                        LocationTagId = tag.Id
                    });
                }

                // Update Location
                await _locationRepository.InsertOrUpdateAndGetIdAsync(location);

                await CurrentUnitOfWork.SaveChangesAsync();
            }
            else
            {
                foreach (var group in location.LocationGroups)
                {
                    if (group.Id == 0)
                    {
                        group.Group.Code = group.Group.Name;
                        var id = _groupRepository.InsertAndGetId(group.Group);
                        group.Group.Id = id;
                        group.Id = id;
                    }
                }

                foreach (var tag in location.LocationTags)
                {
                    if (tag.Id == 0)
                    {
                        tag.LocationTag.Code = tag.LocationTag.Name;
                        var id = _locationTagRepository.InsertAndGetId(tag.LocationTag);
                        tag.Id = id;
                        tag.LocationTag.Id = id;
                    }
                }
                var groups = location.LocationGroups;
                var tags = location.LocationTags;

                location.LocationGroups = null;
                location.LocationTags = null;

                locationId = _locationRepository.InsertAndGetId(location);

                foreach (var group in groups)
                {
                    _locationGroupRepository.Insert(new DinePlan.DineConnect.Connect.Master.LocationGroups.LocationGroup
                    {
                        LocationId = locationId,
                        GroupId = group.Id
                    });
                }

                foreach (var tag in tags)
                {
                    _locationTagLocationRepository.Insert(new LocationTagLocation
                    {
                        LocationId = locationId,
                        LocationTagId = tag.Id
                    });
                }
            }

            return locationId;
        }

        public async Task DeleteAsync(int id)
        {
            var location = await _locationRepository.GetAll().Where(c => c.Id == id).FirstOrDefaultAsync();
            var locaionGroupCount = (await _locationGroupRepository.GetAllListAsync(lg => lg.LocationId == id)).Count;
            var locaionMenuItemCount = (await _locationMenuItemRepository.GetAllListAsync(lm => lm.LocationId == id)).Count;
            var userLocationCount = (await _userLocationRepository.GetAllListAsync(ul => ul.LocationId == id)).Count;
            if (locaionGroupCount > 0 || locaionMenuItemCount > 0 || userLocationCount > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            _locationRepository.Delete(id);

            CurrentUnitOfWork.SaveChanges();
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<Location> GetAsync(int id)
        {
            return await _locationRepository
                .GetAllIncluding(o => o.Organization,
                    a => a.City.State,
                    co => co.City.Country,
                    a => a.Schedules,
                    a => a.Branch)
                .Include(x => x.LocationGroups)
                .ThenInclude(x => x.Group)
                .Include(x => x.LocationTags)
                .ThenInclude(x => x.LocationTag)
                .Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public IQueryable<Location> GetList(int organizationId, string filter)
        {
            if (_connectSession.OrgId < 0)
            {
                int tenant = _connectSession.TenantId ?? 0;
                return GetAllLocationIgnoringBrandFilter(organizationId, filter);
            }
            else
            {
                return _locationRepository.GetAllIncluding(o => o.Organization, a => a.City.State, co => co.City.Country)
                    .WhereIf(!string.IsNullOrWhiteSpace(filter), i =>
                    EF.Functions.Like(i.Name, filter.ToILikeString())).AsQueryable();
            }
        }

        public IQueryable<Location> GetAllLocationIgnoringBrandFilter(int organizationId, string filter, bool isDelete = false)
        {
            return _locationRepository
                 .GetAllIncluding(o => o.Organization, a => a.City, a => a.City.State, co => co.City.Country)
                 .Where(x => x.IsDeleted == isDelete)
                 .WhereIf(!string.IsNullOrWhiteSpace(filter), i =>
                 EF.Functions.Like(i.Name, filter.ToILikeString()))
                 .WhereIf(organizationId > 0, i => i.OrganizationId == organizationId)
                 .AsQueryable();
        }

        public IQueryable<Location> GetAllLocation(int organizationId, int countryId, int stateId, int cityId, string filter)
        {
            return _locationRepository.GetAll()
                 .WhereIf(organizationId > 0, i => i.OrganizationId == organizationId)
                 .WhereIf(!string.IsNullOrWhiteSpace(filter), l =>
                 EF.Functions.Like(l.Name, filter.ToILikeString()))
                 .WhereIf(cityId > 0, l => l.CityId == cityId)
                 .WhereIf(stateId > 0, l => l.City.StateId == stateId)
                 .WhereIf(countryId > 0, l => l.City.CountryId == countryId);
        }

        public async Task<Location> GetLocationWithoutBrandFilter(int tenantId, int locationId)
        {
            return await _locationRepository.GetAsync(locationId);
        }

        public IQueryable<Location> GetListOfAllowedLocationForUser(int tenantId, long userId, int organizationId, int countryId, int stateId, int cityId, string filter)
        {
            return _userLocationRepository.GetAll()
                .Where(u => u.UserId == userId && u.Location.TenantId == tenantId)
                .Select(u => u.Location)
                .WhereIf(organizationId > 0, i => i.OrganizationId == organizationId)
                .WhereIf(!string.IsNullOrWhiteSpace(filter), l =>
                EF.Functions.Like(l.Name, filter.ToILikeString()))
                .WhereIf(cityId > 0, l => l.CityId == cityId)
                .WhereIf(stateId > 0, l => l.City.StateId == stateId)
                .WhereIf(countryId > 0, l => l.City.CountryId == countryId);
        }

        public async Task<List<Brand>> GetListOfAllowedBrandForUser(long userId)
        {
            return await _userLocationRepository
               .GetAllIncluding(l => l.Location, c => c.Location.Organization)
               .Where(c => c.UserId == userId)
               .Select(o => o.Location.Organization)
               .ToListAsync();
        }
    }
}
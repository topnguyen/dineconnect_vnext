﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations
{
    public class UserLocationManger : DineConnectServiceBase, IUserLocationManger, ITransientDependency
    {
        private readonly IRepository<UserLocation> _userLocationRepository;

        public UserLocationManger(IRepository<UserLocation> userLocationRepository)
        {
            _userLocationRepository = userLocationRepository;
        }

        public async Task CreateOrUpdateAsync(UserLocation userLocation)
        {
            await _userLocationRepository.InsertAndGetIdAsync(userLocation);
        }

        public async Task<List<UserLocation>> GetAllLocationByUserId(long userId)
        {
            return await _userLocationRepository
                .GetAllIncluding(c => c.Location, o => o.Location.Organization)
                .Where(ul => ul.UserId == userId)
                .ToListAsync();
        }

        public async Task DeleteAsync(int userLocationId)
        {
            await _userLocationRepository.DeleteAsync(userLocationId);
        }

        public async Task<bool> HasAlloedLocaionConfigured(long userId)
        {
            var defaultLocationCount = await _userLocationRepository.GetAll().Where(c => c.UserId == userId).CountAsync();
            return defaultLocationCount > 0 ? true : false;
        }

        public async Task<UserLocation> GetFirstAllowedLocation(long userId)
        {
            return await _userLocationRepository
                .GetAllIncluding(c => c.Location)
                .Where(c => c.UserId == userId)
                .FirstAsync();
        }
    }
}
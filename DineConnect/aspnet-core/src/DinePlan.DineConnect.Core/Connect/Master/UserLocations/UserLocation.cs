﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Master.UserLocations
{
    [Table("UserLocations")]
    public class UserLocation : ConnectFullMultiTenantAuditEntity
    {
        public long UserId { get; set; }
        public int LocationId { get; set; }
        public Location Location { get; set; }
    }
}
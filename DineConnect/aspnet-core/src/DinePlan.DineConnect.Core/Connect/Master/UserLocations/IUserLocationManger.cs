﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.UserLocations
{
    public interface IUserLocationManger
    {
        Task CreateOrUpdateAsync(UserLocation userLocation);

        Task<List<UserLocation>> GetAllLocationByUserId(long userId);

        Task DeleteAsync(int userLocationId);

        Task<bool> HasAlloedLocaionConfigured(long userId);

        Task<UserLocation> GetFirstAllowedLocation(long userId);
    }
}
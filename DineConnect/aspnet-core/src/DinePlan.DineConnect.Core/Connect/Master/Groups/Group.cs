﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.LocationGroups;
using System.Collections.Generic;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.Groups
{
    public class Group : ConnectFullMultiTenantAuditEntity
    {
        public Group()
        {
            LocationGroups = new HashSet<LocationGroups.LocationGroup>();
        }

        public string Name { get; set; }
        public virtual string Code { get; set; }
        public ICollection<LocationGroups.LocationGroup> LocationGroups { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.Departments
{
    [Table("DepartmentGroups")]
    public class DepartmentGroup : ConnectFullMultiTenantAuditEntity
    {
        [StringLength(100)]
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOns { get; set; }
    }
}

﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.TicketTypes;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.Departments
{
	[Table("Departments")]
	public class Department : ConnectLocFullMultiTenantAuditEntity
	{
		public virtual string Code { get; set; }

		public virtual string AddOns { get; set; }

		[Required]
		public virtual string Name { get; set; }

		public virtual int SortOrder { get; set; }

		public virtual int? PriceTagId { get; set; }

		[ForeignKey("PriceTagId")]
		public PriceTag PriceTag { get; set; }

		[ForeignKey("DepartmentGroupId")]
		public DepartmentGroup DepartmentGroup { get; set; }

		public int? DepartmentGroupId { get; set; }

		[ForeignKey("TicketTypeId")]
		public TicketTypeConfiguration TicketType { get; set; }

		public int? TicketTypeId { get; set; }
	}
}
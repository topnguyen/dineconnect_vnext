﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Master.TillAccounts
{
    [Table("TillAccounts")]
    public class TillAccount : ConnectLocFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }

        public virtual int TillAccountTypeId { get; set; }
    }
    [Table("TillTransactions")]
    public class TillTransaction : ConnectFullMultiTenantAuditEntity
    {
        public virtual int LocalId { get; set; }
        public virtual int WorkPeriodId { get; set; }
        public virtual int LocalWorkPeriodId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string User { get; set; }
        public virtual DateTime TransactionTime { get; set; }
        public virtual bool Status { get; set; }
        public virtual decimal TotalAmount { get; set; }

        [ForeignKey("TillAccountId")]
        public virtual TillAccount TillAccount { get; set; }
        public virtual int TillAccountId { get; set; }
    }
    public enum TillAccounTypes
    {
        Income = 0,
        Expense = 1,
        AccuralIncome = 2,
        AccuralExpense = 3
    }
}
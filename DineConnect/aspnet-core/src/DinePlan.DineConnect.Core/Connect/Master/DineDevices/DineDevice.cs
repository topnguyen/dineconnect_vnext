﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.DineDevices
{
    [Table("DineDevices")]
    public class DineDevice : ConnectOrgLocFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public string Settings { get; set; }

        // 0-> DineChef 1->DineQueue
        public DineDeviceType DeviceType { get; set; }
    }
}

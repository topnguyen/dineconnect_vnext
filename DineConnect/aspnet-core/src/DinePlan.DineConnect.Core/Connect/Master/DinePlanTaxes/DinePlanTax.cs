﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.DinePlanTaxes
{
    [Table("DinePlanTaxes")]
    public class DinePlanTax : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        [StringLength(DinePlanTaxConsts.MaxTaxNameLength, MinimumLength = DinePlanTaxConsts.MinTaxNameLength)]
        public virtual string TaxName { get; set; }

        [ForeignKey("TransactionTypeId")]
        public TransactionType TransactionType { get; set; }
        public int? TransactionTypeId { get; set; }
    }

    [Table("DinePlanTaxLocations")]
    public class DinePlanTaxLocation : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [Required]
        public int DinePlanTaxRefId { get; set; }

        [ForeignKey("DinePlanTaxRefId")]
        public DinePlanTax DinePlanTax { get; set; }

        [Required]
        public decimal TaxPercentage { get; set; }

        public int? TransactionTypeId { get; set; }

        [StringLength(50)]
        public virtual string Tag { get; set; }

        private ICollection<DinePlanTaxMapping> _dinePlanTaxMappings;

        public virtual ICollection<DinePlanTaxMapping> DinePlanTaxMappings
        {
            get { return _dinePlanTaxMappings; }
            set { _dinePlanTaxMappings = value; }
        }

        public DinePlanTaxLocation()
        {
            _dinePlanTaxMappings = new List<DinePlanTaxMapping>();
        }
    }

    [Table("DinePlanTaxMappings")]
    public class DinePlanTaxMapping : ConnectFullAuditEntity
    {
        public int? DepartmentId { get; set; }

        public int? CategoryId { get; set; }

        public int? MenuItemId { get; set; }

        [ForeignKey("DinePlanTaxLocationId")]
        public DinePlanTaxLocation DinePlanTaxLocation { get; set; }
        public int DinePlanTaxLocationId { get; set; }

    }
}
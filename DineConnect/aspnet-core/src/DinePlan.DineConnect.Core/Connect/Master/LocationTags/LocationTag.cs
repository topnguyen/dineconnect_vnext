﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.LocationTags
{
    [Table("LocationTags")]
    public class LocationTag : ConnectFullMultiTenantAuditEntity
    {
        public LocationTag()
        {
            LocationTags = new HashSet<LocationTagLocation>();
        }

        [Required]
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }
        public ICollection<LocationTagLocation> LocationTags { get; set; }
    }
}
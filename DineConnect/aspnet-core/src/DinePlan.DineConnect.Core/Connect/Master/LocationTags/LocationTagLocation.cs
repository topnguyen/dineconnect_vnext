﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.LocationTags
{
    [Table("LocationTagLocations")]
    public class LocationTagLocation : ConnectFullMultiTenantAuditEntity
    {
        public virtual int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }

        public virtual int LocationTagId { get; set; }

        [ForeignKey("LocationTagId")]
        public LocationTag LocationTag { get; set; }
    }
}
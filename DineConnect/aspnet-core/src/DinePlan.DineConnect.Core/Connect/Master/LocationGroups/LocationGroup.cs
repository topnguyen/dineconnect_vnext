﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups
{
    public class LocationGroup : ConnectFullMultiTenantAuditEntity
    {
        public int LocationId { get; set; }
        public int GroupId { get; set; }
        public Location Location { get; set; }
        public Group Group { get; set; }
    }
}
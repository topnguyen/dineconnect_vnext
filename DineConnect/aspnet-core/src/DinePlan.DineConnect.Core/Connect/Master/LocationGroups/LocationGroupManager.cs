﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups
{
    public class LocationGroupManager : DineConnectServiceBase, ILocationGroupManager, ITransientDependency
    {
        private readonly IRepository<LocationGroup> _locationGroupRepository;
        private readonly IRepository<Group> _groupRepository;
        private readonly IRepository<Location> _locationRepository;

        public LocationGroupManager(IRepository<LocationGroup> locationGroupRepository, IRepository<Group> groupRepository, IRepository<Location> locationRepository)
        {
            _locationGroupRepository = locationGroupRepository;
            _groupRepository = groupRepository;
            _locationRepository = locationRepository;
        }

        public async Task AddLocationToGroup(LocationGroup locationGroup)
        {
            await _locationGroupRepository.InsertOrUpdateAsync(locationGroup);
        }

        public async Task CreateOrUpdateGroup(Group group)
        {
            var exists = _groupRepository.GetAll().WhereIf(group.Id > 0, m => m.Id != group.Id);

            if (exists.Any(o => o.Name.Equals(group.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
            if (exists.Any(o => o.Code.Equals(group.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }

            await _groupRepository.InsertOrUpdateAsync(group);
        }

        public async Task DeleteGroup(int groupId)
        {
            var locationGroup = await _locationGroupRepository.GetAll().Where(c => c.GroupId == groupId).ToListAsync();

            if (locationGroup.Count > 0)
            {
                foreach (var item in locationGroup)
                {
                    await _locationGroupRepository.DeleteAsync(item.Id);
                }
            }

            await _groupRepository.DeleteAsync(groupId);
        }

        public IQueryable<Group> GetAllGroup(string filter)
        {
            return _groupRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(filter), t =>
            EF.Functions.Like(t.Name, filter.ToILikeString()));
        }

        public IQueryable<LocationGroup> GetAllLocationGroup(int groupId)
        {
            return _locationGroupRepository.GetAllIncluding(l => l.Location).Where(g => g.GroupId == groupId);
        }

        public async Task<Group> GetGroupAsync(int groupId)
        {
            return await _groupRepository.GetAsync(groupId);
        }

        public IQueryable<Location> GetLocationNotInGroup(string filter, int organizationId, int groupId)
        {
            var locationIdsInGroup = _locationGroupRepository.GetAll().Where(c => c.GroupId == groupId).Select(l => l.LocationId).Distinct().ToList();

            return _locationRepository.GetAll()
                .Where(c => !locationIdsInGroup.Contains(c.Id))
                .WhereIf(!string.IsNullOrWhiteSpace(filter), c => EF.Functions.Like(c.Name, filter.ToILikeString()))
                .WhereIf(organizationId > 0, c => c.OrganizationId == organizationId);
        }

        public async Task RemoveLocationFromGroup(int locationGroupId)
        {
            await _locationGroupRepository.DeleteAsync(locationGroupId);
        }
    }
}
﻿using DinePlan.DineConnect.Connect.Master.Groups;
using DinePlan.DineConnect.Connect.Master.Locations;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.LocationGroups
{
    public interface ILocationGroupManager
    {
        Task CreateOrUpdateGroup(Group group);

        Task<Group> GetGroupAsync(int groupId);

        IQueryable<Group> GetAllGroup(string filter);

        Task DeleteGroup(int groupId);

        Task AddLocationToGroup(LocationGroup locationGroup);

        Task RemoveLocationFromGroup(int locationGroupId);

        IQueryable<LocationGroup> GetAllLocationGroup(int groupId);

        IQueryable<Location> GetLocationNotInGroup(string filter, int organizationId, int groupId);
    }
}
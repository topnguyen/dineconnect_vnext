﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes
{
    [Table("TicketTypeConfigurations")]
    public class TicketTypeConfiguration : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
    }
}

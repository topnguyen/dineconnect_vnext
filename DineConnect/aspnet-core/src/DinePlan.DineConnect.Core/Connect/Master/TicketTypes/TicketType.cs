﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Numberators;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.TicketTypes
{
	[Table("TicketTypes")]
	public class TicketType : ConnectLocFullMultiTenantAuditEntity
	{
		[ForeignKey("InvoiceNumerator")]
		public int? TicketInvoiceNumeratorId { get; set; }

		[ForeignKey("TicketNumerator")]
		public int? TicketNumeratorId { get; set; }

		[ForeignKey("OrderNumerator")]
		public int? OrderNumeratorId { get; set; }

		[ForeignKey("PromotionNumerator")]
		public int? PromotionNumeratorId { get; set; }

		[ForeignKey("FullTaxNumerator")]
		public int? FullTaxNumeratorId { get; set; }

		[ForeignKey("QueueNumerator")]
		public int? QueueNumeratorId { get; set; }

		[ForeignKey("ReturnNumerator")]
		public int? ReturnNumeratorId { get; set; }

		[ForeignKey("SaleTransactionType")]
		public int? SaleTransactionType_Id { get; set; }

		[ForeignKey("ScreenMenu")]
		public int? ScreenMenuId { get; set; }

		public virtual string Name { get; set; }

		public virtual Numerator InvoiceNumerator { get; set; }

		public virtual Numerator TicketNumerator { get; set; }

		public virtual Numerator OrderNumerator { get; set; }

		public virtual Numerator PromotionNumerator { get; set; }

		public virtual Numerator FullTaxNumerator { get; set; }

		public virtual Numerator QueueNumerator { get; set; }

		public virtual Numerator ReturnNumerator { get; set; }

		public virtual TransactionType SaleTransactionType { get; set; }

		public virtual ScreenMenu ScreenMenu { get; set; }

		public virtual int? TicketTypeConfigurationId { get; set; }

		public virtual bool IsTaxIncluded { get; set; }

		public virtual bool IsPreOrder { get; set; }

		public virtual bool IsAllowZeroPriceProducts { get; set; }
	}
}
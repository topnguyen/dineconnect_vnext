﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.PaymentType;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master.PaymentTypes
{
	[Table("PaymentTypes")]
	public class PaymentType : ConnectLocFullMultiTenantAuditEntity
	{
		public virtual string Name { get; set; }

		public virtual bool DisplayInShift { get; set; }

		public virtual bool AcceptChange { get; set; }

		public virtual bool NoRefund { get; set; }
		public virtual bool AutoPayment { get; set; }

		public virtual string AccountCode { get; set; }

		public virtual int SortOrder { get; set; }

		public virtual int PaymentProcessor { get; set; }

		public virtual string Processors { get; set; }

		public virtual string ButtonColor { get; set; }

		public virtual string PaymentGroup { get; set; }

		public virtual string Files { get; set; }

		public virtual Guid? DownloadImage { get; set; }

		public virtual string PaymentTag { get; set; }

		public virtual int? LocationId { get; set; }

		[ForeignKey("LocationId")]
		public Location Location { get; set; }

		public virtual PaymentTenderType PaymentTenderType { get; set; }
		public string Departments { get; set; }
		public string ProcessorName { get; set; }
		public virtual string AddOns { get; set; }
	}
}
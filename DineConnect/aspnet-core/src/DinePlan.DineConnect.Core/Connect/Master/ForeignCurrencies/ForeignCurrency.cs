﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.ForeignCurrencies
{
    [Table("ForeignCurrencies")]
    public class ForeignCurrency : ConnectOrgLocFullMultiTenantAuditEntity
    {

        [Required]
        public virtual string Name { get; set; }

        [StringLength(ForeignCurrencyConsts.MaxCurrencySymbolLength, MinimumLength = ForeignCurrencyConsts.MinCurrencySymbolLength)]
        public virtual string CurrencySymbol { get; set; }

        public virtual decimal ExchangeRate { get; set; }

        public virtual decimal Rounding { get; set; }

        public virtual int PaymentTypeId { get; set; }

        [ForeignKey("PaymentTypeId")]
        public PaymentTypes.PaymentType PaymentType { get; set; }
    }
}
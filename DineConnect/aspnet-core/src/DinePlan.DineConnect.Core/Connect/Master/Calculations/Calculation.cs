﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Filter;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.Calculations
{
    [Table("Calculations")]
    public class Calculation : ConnectOrgLocFullMultiTenantAuditEntity
    {

        [Required]
        public virtual string Name { get; set; }

        public virtual string ButtonHeader { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual int FontSize { get; set; }

        public virtual int ConfirmationType { get; set; }

        public virtual int CalculationTypeId { get; set; }

        public virtual decimal Amount { get; set; }

        public virtual bool IncludeTax { get; set; }

        public virtual bool DecreaseAmount { get; set; }

        public virtual string Departments { get; set; }


        public virtual int TransactionTypeId { get; set; }

        [ForeignKey("TransactionTypeId")]
        public TransactionType TransactionType { get; set; }
    }

    public enum CalculationType
    {
        RateFromTicketAmount = 0,
        RateFromPreviousTemplate = 1,
        FixedAmount = 2
    };

    public enum ConfirmationType
    {
        None = 0,
        Confirmation = 1,
        AdminPin = 2
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.FutureDateInformations
{
    [Table("FutureDateInformations")]
    public class FutureDateInformation : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }

        public virtual DateTime FutureDate { get; set; }

        public virtual int FutureDateType { get; set; }

        public virtual string Contents { get; set; }

        public DateTime? FutureDateTo { get; set; }

        public string ObjectContents { get; set; }
    }

    public enum FutureDateType
    {
        MenuItem = 1,
        PriceTag = 2,
        Tax = 3,
        ScreenMenu = 4
    }
}
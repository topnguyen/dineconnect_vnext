﻿using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Extension;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    public class CompanyManager : DineConnectServiceBase, ICompanyManager, ITransientDependency
    {
        private readonly IRepository<Brand> _organizationRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<Address> _addressRepository;

        public CompanyManager(IRepository<Brand> organization, IRepository<Location> locationRepository,
            IRepository<Address> addressRepository)
        {
            _organizationRepository = organization;
            _locationRepository = locationRepository;
            _addressRepository = addressRepository;
        }

        public async Task<int> CreateOrUpdteAsync(Brand organization)
        {
            var exists = _organizationRepository.GetAll().AsEnumerable().WhereIf(organization.Id > 0, m => m.Id != organization.Id);

            if (exists.Any(o => o.Name.Equals(organization.Name)))
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }
            if (exists.Any(o => o.Code.Equals(organization.Code)))
            {
                throw new UserFriendlyException(L("CodeAlreadyExists"));
            }

            if (organization.Id > 0)
            {
                var old = await _organizationRepository.GetAsync(organization.Id);

                ObjectMapper.Map(organization, old);
                return organization.Id;
            }

            await _organizationRepository.InsertAsync(organization);
            await CurrentUnitOfWork.SaveChangesAsync();
            return organization.Id;
        }

        public async Task DeleteAsync(int id)
        {
            var organization = await _organizationRepository.GetAllIncluding(c => c.Address).Where(c => c.Id == id).FirstOrDefaultAsync();
            
            var locationCount = (await _locationRepository.GetAllListAsync(c => c.OrganizationId == id)).Count;

            if (locationCount > 0)
            {
                throw new UserFriendlyException("Can not delete, delete related information and try again");
            }

            if (organization.Address != null)
            {
                await _addressRepository.DeleteAsync(organization.Address.Id);
            }
            
            await _organizationRepository.DeleteAsync(id);
            
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<Brand> GetAsync(int id)
        {
            return await _organizationRepository.GetAllIncluding(a => a.Address, c => c.Address.City, s => s.Address.City.State, co => co.Address.City.Country).Where(i => i.Id == id).FirstOrDefaultAsync(); ;
        }

        public IQueryable<Brand> GetList(string filter)
        {
            return _organizationRepository.GetAll()
                .Include(a => a.Address)
                .ThenInclude(c => c.City)
                .ThenInclude(s => s.State)
                .Include(a => a.Address.City)
                .ThenInclude(co => co.Country)
                .WhereIf(!string.IsNullOrWhiteSpace(filter), i => EF.Functions.Like(i.Name, filter.ToILikeString())).AsQueryable();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    [Table("Brands")]
    public class Brand : ConnectFullMultiTenantAuditEntity
    {
        public Brand()
        {
            Locations = new HashSet<Location>();
        }

        [Required]
        [MaxLength(60)]
        public virtual string Name { get; set; }

        [Required]
        [MaxLength(30)]
        public virtual string Code { get; set; }

        public virtual string GovtApprovalId { get; set; }

        public virtual string PhoneNumber { get; set; }
        public virtual string UserSerialNumber { get; set; }
        public virtual string FaxNumber { get; set; }
        public int? AddressId { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
        public virtual string Address1 { get; set; }

        [MaxLength(100)]
        public virtual string Address2 { get; set; }

        [MaxLength(100)]
        public virtual string Address3 { get; set; }

        public virtual string City { get; set; }
        public virtual string State { get; set; }

        public virtual string Country { get; set; }

        public virtual string PostalCode { get; set; }
        public virtual Guid? CompanyProfilePictureId { get; set; }

        //public virtual string EmailIdList { get; set; }
        public bool NeedToTreatAnyWorkDayAsHalfDay { get; set; }

        public int? WeekDayTreatAsHalfDay { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
    }
}
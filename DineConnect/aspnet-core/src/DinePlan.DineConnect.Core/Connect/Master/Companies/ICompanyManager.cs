﻿using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Companies
{
    public interface ICompanyManager
    {
        Task<int> CreateOrUpdteAsync(Brand organization);

        Task<Brand> GetAsync(int id);

        IQueryable<Brand> GetList(string filter);

        Task DeleteAsync(int id);
    }
}
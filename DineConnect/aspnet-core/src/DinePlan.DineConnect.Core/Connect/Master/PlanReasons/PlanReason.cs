﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Master.PlanReasons
{
    [Table("PlanReasons")]
    public class PlanReason : ConnectOrgLocFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Reason { get; set; }

        public virtual string AliasReason { get; set; }

        public virtual string ReasonGroup { get; set; }
    }

    public class ReasonGroup
    {
        public static readonly string[] ReasonGroups = { "Comp", "Gift", "Void", "Reprint", "Refund", "Exchange" };
    }
}
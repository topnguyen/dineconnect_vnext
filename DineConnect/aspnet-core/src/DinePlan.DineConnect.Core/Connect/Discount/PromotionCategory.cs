﻿using Abp.Auditing;
using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Audited]
	[Table("PromotionCategories")]
	public class PromotionCategory : ConnectFullMultiTenantAuditEntity
	{
		[StringLength(10)]
		public virtual string Code { get; set; }

		[StringLength(50)]
		public virtual string Name { get; set; }

		public virtual string Tag { get; set; }
	}
}
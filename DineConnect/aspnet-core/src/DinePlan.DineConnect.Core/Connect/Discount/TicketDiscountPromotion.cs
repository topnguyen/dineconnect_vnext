﻿using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Table("TicketDiscountPromotions")]
	public class TicketDiscountPromotion : ConnectFullAuditEntity
	{
		[ForeignKey("PromotionId")]
		public virtual Promotion Promotion { get; set; }

		public virtual int PromotionId { get; set; }
		public virtual int PromotionValueType { get; set; }

		[StringLength(50)]
		public virtual string ButtonCaption { get; set; }

		public virtual decimal PromotionValue { get; set; }
		public virtual bool AuthenticationRequired { get; set; }
		public virtual bool AskReference { get; set; }
		public virtual bool ApplyAsPayment { get; set; }
		public virtual bool PromotionApplyOnce { get; set; }
		public virtual int PromotionApplyType { get; set; }
	}
}
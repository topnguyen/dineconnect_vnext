﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreeValuePromotionExecutions")]
    public class FreeValuePromotionExecution : ConnectFullAuditEntity
    {
        [ForeignKey("FreeValuePromotionId")]
        public virtual FreeValuePromotion FreeValuePromotion { get; set; }

        public virtual int FreeValuePromotionId { get; set; }
        public virtual int CategoryId { get; set; }

        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual bool From { get; set; }
        public virtual int ValueType { get; set; }
        public virtual decimal Value { get; set; }
        public virtual int ProductGroupId { get; set; }
    }
}
﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("StepDiscountPromotions")]
    public class StepDiscountPromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }
        public virtual int PromotionId { get; set; }
        public virtual int StepDiscountType { get; set; }
        public virtual bool ApplyForEachQuantity { get; set; }

        public virtual ICollection<StepDiscountPromotionExecution> StepDiscountPromotionExecutions { get; set; }
        public virtual ICollection<StepDiscountPromotionMappingExecution> StepDiscountPromotionMappingExecutions { get; set; }
    }
}
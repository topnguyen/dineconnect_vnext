﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("BuyXAndYAtZValuePromotionExecutions")]
    public class BuyXAndYAtZValuePromotionExecution : ConnectFullAuditEntity
    {
        [ForeignKey("BuyXAndYAtZValuePromotionId")]
        public virtual BuyXAndYAtZValuePromotion BuyXAndYAtZValuePromotion { get; set; }

        public virtual int BuyXAndYAtZValuePromotionId { get; set; }
        public virtual int CategoryId { get; set; }

        public virtual int MenuItemId { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual int ProductGroupId { get; set; }
    }
}
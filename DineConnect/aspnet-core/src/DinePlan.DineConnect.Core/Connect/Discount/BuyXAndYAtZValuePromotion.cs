﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("BuyXAndYAtZValuePromotions")]
    public class BuyXAndYAtZValuePromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int ItemCount { get; set; }
        public virtual int Value { get; set; }
        public virtual int PromotionValueType { get; set; }
        public virtual ICollection<BuyXAndYAtZValuePromotionExecution> BuyXAndYAtZValuePromotionExecutions { get; set; }
    }
}
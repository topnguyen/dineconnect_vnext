﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("TimerPromotions")]
    public class TimerPromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int PriceTagId { get; set; }
    }
}
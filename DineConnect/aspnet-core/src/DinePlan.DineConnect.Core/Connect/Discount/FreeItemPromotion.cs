﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreeItemPromotions")]
    public class FreeItemPromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual ICollection<FreeItemPromotionExecution> Executions { get; set; }
        public virtual int ItemCount { get; set; }
        public virtual bool Confirmation { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual bool MultipleTimes { get; set; }
    }
}
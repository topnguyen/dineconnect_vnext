﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreePromotionExecutions")]
    public class FreePromotionExecution : ConnectFullAuditEntity
    {
        [ForeignKey("FreePromotionId")]
        public virtual FreePromotion FreePromotion { get; set; }

        public virtual int FreePromotionId { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual bool From { get; set; }
        public virtual int ProductGroupId { get; set; }
    }
}
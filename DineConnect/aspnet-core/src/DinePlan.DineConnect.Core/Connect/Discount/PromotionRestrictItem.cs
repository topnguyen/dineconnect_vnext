﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("PromotionRestrictItems")]
    public class PromotionRestrictItem : ConnectFullAuditEntity
    {
        public int MenuItemId { get; set; }

        public int PromotionId { get; set; }

        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }
    }
}
﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Table("DemandPromotionExecutions")]
	public class DemandPromotionExecution : CreationAuditedEntity
	{
		[ForeignKey("DemandDiscountPromotionId")]
		public virtual DemandDiscountPromotion DemandDiscountPromotion { get; set; }

		public virtual int DemandDiscountPromotionId { get; set; }
		public virtual int ProductGroupId { get; set; }
		public virtual int CategoryId { get; set; }
		public virtual int MenuItemId { get; set; }
		public virtual int MenuItemPortionId { get; set; }
	}
}
﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("StepDiscountPromotionMappingExecutions")]
    public class StepDiscountPromotionMappingExecution : ConnectFullAuditEntity
    {
        [ForeignKey("StepDiscountPromotionId")]
        public virtual StepDiscountPromotion StepDiscountPromotion { get; set; }

        public virtual int StepDiscountPromotionId { get; set; }
        public int ProductGroupId { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
    }
}
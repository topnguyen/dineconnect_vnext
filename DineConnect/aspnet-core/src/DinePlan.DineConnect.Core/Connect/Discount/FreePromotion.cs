﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreePromotions")]
    public class FreePromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual bool AllFrom { get; set; }
        public virtual bool AllTo { get; set; }
        public virtual int FromCount { get; set; }
        public virtual int ToCount { get; set; }
        public virtual ICollection<FreePromotionExecution> FreePromotionExecutions { get; set; }
    }
}
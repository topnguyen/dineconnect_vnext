﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreeValuePromotions")]
    public class FreeValuePromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual bool AllFrom { get; set; }
        public virtual int FromCount { get; set; }
        public virtual ICollection<FreeValuePromotionExecution> FreeValuePromotionExecutions { get; set; }
    }
}
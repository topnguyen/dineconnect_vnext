﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Cards;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Table("Promotions")]
	public class Promotion : ConnectOrgLocFullMultiTenantAuditEntity
	{
		public Promotion()
		{
			CombineAllPro = true;
			CombineProValues = "*";
			PromotionSchedules = new HashSet<PromotionSchedule>();
			PromotionRestrictItems = new HashSet<PromotionRestrictItem>();
			PromotionQuotas = new HashSet<PromotionQuota>();
		}

		[Required]
		public virtual string Name { get; set; }

		public virtual string Code { get; set; }

		public virtual string RefCode { get; set; }

		public virtual DateTime StartDate { get; set; }

		public virtual DateTime EndDate { get; set; }

		public virtual int PromotionTypeId { get; set; }

		public virtual bool Active { get; set; }

		public virtual int SortOrder { get; set; }

		public virtual string Filter { get; set; }

		public virtual string Description { get; set; }
		public virtual string PromotionContents { get; set; }

		public virtual ICollection<PromotionSchedule> PromotionSchedules { get; set; }
		public virtual ICollection<PromotionQuota> PromotionQuotas { get; set; }

		public virtual ICollection<PromotionRestrictItem> PromotionRestrictItems { get; set; }

		[ForeignKey("ConnectCardTypeId")]
		public virtual ConnectCardType ConnectCardType { get; set; }

		public virtual int? ConnectCardTypeId { get; set; }

		[ForeignKey("PromotionCategoryId")]
		public virtual PromotionCategory PromotionCategory { get; set; }

		public virtual int? PromotionCategoryId { get; set; }
		public virtual bool Limi1PromotionPerItem { get; set; }
		public virtual bool CombineAllPro { get; set; }
		public virtual string CombineProValues { get; set; }
		public virtual bool DisplayPromotion { get; set; }
		public virtual int PromotionPosition { get; set; }

		public virtual bool IgnorePromotionLimitation { get; set; }
		public virtual bool ApplyOffline { get; set; }
	}
}
﻿using DinePlan.DineConnect.BaseCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Table("FixedPromotions")]
	public class FixedPromotion : ConnectFullAuditEntity
	{
		[ForeignKey("PromotionId")]
		public virtual Promotion Promotion { get; set; }

		public virtual int PromotionId { get; set; }
		public virtual int CategoryId { get; set; }
		public virtual int MenuItemId { get; set; }
		public virtual int PromotionValueType { get; set; }
		public virtual decimal PromotionValue { get; set; }
		public virtual int ProductGroupId { get; set; }
		public virtual int MenuItemPortionId { get; set; }
	}
}
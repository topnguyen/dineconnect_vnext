﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("StepDiscountPromotionExecutions")]
    public class StepDiscountPromotionExecution : ConnectFullAuditEntity
    {
        [ForeignKey("StepDiscountPromotionId")]
        public virtual StepDiscountPromotion StepDiscountPromotion { get; set; }

        public virtual int StepDiscountPromotionId { get; set; }
        public decimal PromotionStepTypeValue { get; set; }
        public int PromotionValueType { get; set; }
        public string PromotionValueTypeName { get; set; }
        public decimal PromotionValue { get; set; }
    }
}
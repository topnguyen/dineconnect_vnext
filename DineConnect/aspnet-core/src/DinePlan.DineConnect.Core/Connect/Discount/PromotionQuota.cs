﻿using DinePlan.DineConnect.BaseCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Discount
{
	[Table("PromotionQuotas")]
	public class PromotionQuota : ConnectLocFullMultiTenantAuditEntity
	{
		public virtual int PromotionId { get; set; }

		[ForeignKey("PromotionId")]
		public virtual Promotion Promotion { get; set; }

		public virtual int QuotaAmount { get; set; }
		public virtual int QuotaUsed { get; set; }

		public virtual int ResetType { get; set; }

		public virtual int? ResetWeekValue { get; set; }
		public virtual DateTime? ResetMonthValue { get; set; }
		public virtual DateTime? LastResetDate { get; set; }
		public virtual DateTime? PlantResetDate { get; set; }
		public virtual string Plants { get; set; }
		public virtual string LocationQuota { get; set; }
		public bool Each { get; set; }
	}
	
}
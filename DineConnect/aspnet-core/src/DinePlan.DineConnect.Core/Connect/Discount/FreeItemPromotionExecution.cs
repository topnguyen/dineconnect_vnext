﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("FreeItemPromotionExecutions")]
    public class FreeItemPromotionExecution : ConnectFullAuditEntity
    {
        [ForeignKey("FreeItemPromotionId")]
        public virtual FreeItemPromotion FreeItemPromotion { get; set; }

        public virtual int FreeItemPromotionId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual decimal Price { get; set; }
    }
}
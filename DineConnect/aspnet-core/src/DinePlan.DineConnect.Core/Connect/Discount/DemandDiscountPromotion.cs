﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Connect.Discount
{
    [Table("DemandDiscountPromotions")]
    public class DemandDiscountPromotion : ConnectFullAuditEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int PromotionValueType { get; set; }

        [StringLength(50)]
        public virtual string ButtonCaption { get; set; }

        [StringLength(10)]
        public virtual string ButtonColor { get; set; }

        public virtual decimal PromotionValue { get; set; }
        public virtual bool PromotionOverride { get; set; }
        public virtual bool AskReference { get; set; }
        public virtual bool AuthenticationRequired { get; set; }

        public virtual bool PromotionApplyOnce { get; set; }
        public virtual int PromotionApplyType { get; set; }

        public virtual ICollection<DemandPromotionExecution> DemandPromotionExecutions { get; set; }

       
    }
}
﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Locations;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Period
{
	[Table("WorkPeriods")]
	public class WorkPeriod : FullAuditedEntity, IMustHaveTenant
	{
		[StringLength(50)]
		public string StartUser { get; set; }

		[StringLength(50)]
		public string EndUser { get; set; }

		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }

		[ForeignKey("LocationId")]
		public virtual Location Location { get; set; }

		public virtual int LocationId { get; set; }

		public virtual int Wid { get; set; }

		public virtual decimal TotalSales { get; set; }

		public virtual int TotalTicketCount { get; set; }

		public virtual int TenantId { get; set; }

		public string WorkPeriodInformations { get; set; }

		public virtual string AddOn { get; set; }

		public virtual bool AutoClosed { get; set; }
	}
}
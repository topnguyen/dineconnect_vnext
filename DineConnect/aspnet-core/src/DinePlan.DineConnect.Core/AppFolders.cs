﻿using Abp.Dependency;

namespace DinePlan.DineConnect
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string SampleProfileImagesFolder { get; set; }

        public string WebLogsFolder { get; set; }
        public string TempFileDownloadFolder { get; set; }
    }
}
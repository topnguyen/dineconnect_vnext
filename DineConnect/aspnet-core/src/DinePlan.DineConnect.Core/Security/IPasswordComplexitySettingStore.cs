﻿using System.Threading.Tasks;

namespace DinePlan.DineConnect.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}

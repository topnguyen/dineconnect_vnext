﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Core.Swipe
{
    [Table("SwipeCardTypes")]
    public class SwipeCardType : ConnectFullMultiTenantAuditEntity
    {
        [MaxLength(30)]
        public virtual string Name { get; set; }
        
        public virtual bool DepositRequired { get; set; }
        
        public virtual decimal DepositAmount { get; set; }
        
        public virtual int? ExpiryDaysFromIssue { get; set; }
        
        public virtual bool RefundAllowed { get; set; }
        
        public virtual bool CustomerRequired { get; set; }

        public virtual decimal MinimumTopUpAmount { get; set; }
    }

    [Table("SwipeCards")]
    public class SwipeCard : ConnectFullMultiTenantAuditEntity
    {
        public virtual int CardTypeRefId { get; set; }
        
        [ForeignKey("CardTypeRefId")]
        public SwipeCardType SwipeCardTypes { get; set; }

        [MaxLength(20)]
        public virtual string CardNumber { get; set; }
        
        public virtual decimal Balance { get; set; }
        
        public virtual bool Active { get; set; }
    }


    [Table("SwipeCardOTPs")]
    public class SwipeCardOTP : ConnectFullMultiTenantAuditEntity
    {
        public int SwipeCardId { get; set; }

        [ForeignKey("SwipeCardId")]
        public virtual SwipeCard SwipeCard { get; set; }

        public string OTP { get; set; }

        public DateTime ExpiredAt { get; set; }
    }

    [Table("SwipeCustomerCards")]
    public class SwipeCustomerCard : ConnectFullMultiTenantAuditEntity
    {
        public virtual int? CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public virtual int CardRefId { get; set; }
        
        [ForeignKey("CardRefId")]
        public SwipeCard SwipeCard { get; set; }

        public virtual DateTime IssueDate { get; set; }
    
        public virtual DateTime? ReturnDate { get; set; }
        
        public virtual DateTime? ExpiryDate { get; set; }
        
        public virtual decimal DepositAmount { get; set; }
        
        public virtual bool Active { get; set; }
    }

    [Table("SwipeTransactionDetails")]
    public class SwipeTransactionDetail : ConnectFullMultiTenantAuditEntity
    {
        public virtual DateTime TransactionTime { get; set; }
        public virtual int? CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public virtual int CardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard SwipeCard { get; set; }

        public virtual int TransactionType { get; set; }

        public virtual string Description { get; set; }
        
        public virtual decimal Credit { get; set; }
        
        public virtual decimal Debit { get; set; }

        public string PaymentType { get; set; }

        public string PaymentTransactionDetails { get; set; }
    }


    [Table("SwipeCardLedgers")]
    public class SwipeCardLedger : ConnectFullMultiTenantAuditEntity
    {
        public virtual DateTime LedgerTime { get; set; }

        public virtual int? CustomerId { get; set; }
        
        [ForeignKey("CustomerId")] 
        public Customer Customer { get; set; }
        
        public virtual int CardRefId { get; set; }
        
        [ForeignKey("CardRefId")] 
        public SwipeCard SwipeCard { get; set; }
        
        public virtual int TransactionType { get; set; }
        
        public virtual string Description { get; set; }
        
        public virtual decimal OpeningBalance { get; set; }
        
        public virtual decimal Credit { get; set; }
        
        public virtual decimal Debit { get; set; }
        
        public virtual decimal ClosingBalance { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        
        public virtual int? LocationId { get; set; }
    }

}

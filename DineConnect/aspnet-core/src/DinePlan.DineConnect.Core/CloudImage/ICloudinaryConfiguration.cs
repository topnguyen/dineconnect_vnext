﻿namespace DinePlan.DineConnect.CloudImage
{
    public interface ICloudinaryConfiguration
    {
        string CloudName { get; }

        string APIKey { get; }

        string APISecret { get; }

        string EnvironmentVariable { get; }
    }
}

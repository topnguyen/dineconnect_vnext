using System.Threading.Tasks;

namespace DinePlan.DineConnect.CloudImage
{
    public interface ICloudImageUploader
    {

        Task<CloudImageUploadResult> UploadImage(string imageName, byte[] data);
        Task<CloudImageUploadResult> UploadVideo(string imageName, byte[] data);


        Task<bool> CloudinaryAvailable();

    }
}
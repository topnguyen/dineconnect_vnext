﻿using Abp;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using Newtonsoft.Json;
using System.Linq;

namespace DinePlan.DineConnect.CloudImage
{
    public class CloudinaryConfiguration : ICloudinaryConfiguration, ITransientDependency
    {
        protected readonly ISettingManager SettingManager;
        private readonly IRepository<TenantAddon> _tenantAddonRepository;

        public CloudinaryConfiguration(ISettingManager settingManager, IRepository<TenantAddon> tenantAddonRepository)
        {
            SettingManager = settingManager;
            _tenantAddonRepository = tenantAddonRepository;
        }

        public virtual string CloudName
        {
            get
            {
                var value = GetNotEmptySettingValue().CloudName;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for Cloud Name  is null or empty!");
                }

                return value;
            }
        }

        public virtual string APIKey
        {
            get
            {
                var value = GetNotEmptySettingValue().APIKey;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for APIKey  is null or empty!");
                }

                return value;
            }
        }

        public virtual string APISecret
        {
            get
            {
                var value = GetNotEmptySettingValue().APISecret;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for APISecret is null or empty!");
                }

                return value;
            }
        }

        public virtual string EnvironmentVariable
        {
            get
            {
                var value = GetNotEmptySettingValue().EnvironmentVariable;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for EnvironmentVariable is null or empty!");
                }

                return value;
            }
        }

        protected CloudinarySettingNames GetNotEmptySettingValue()
        {
            var addon = _tenantAddonRepository.GetAll()
                            .Where(e => e.Addon.AddonType == AddonType.Image)
                            .Select(e => e.Addon)
                            .FirstOrDefault(e => e.Name == AddonType.Image.ToString());

            var setting = new CloudinarySettingNames();

            if (addon == null || addon.Settings.IsNullOrEmpty())
            {
                setting.CloudName = SettingManager.GetSettingValueForApplication(AppSettings.CloudinarySetting.CloudName);
                setting.APIKey = SettingManager.GetSettingValueForApplication(AppSettings.CloudinarySetting.ApiKey);
                setting.APISecret = SettingManager.GetSettingValueForApplication(AppSettings.CloudinarySetting.ApiSecret);
                setting.EnvironmentVariable = SettingManager.GetSettingValueForApplication(AppSettings.CloudinarySetting.EnvironmentVariable);
            }
            else
            {
                setting = JsonConvert.DeserializeObject<CloudinarySettingNames>(addon.Settings);
            }

            return setting;
        }
    }
}
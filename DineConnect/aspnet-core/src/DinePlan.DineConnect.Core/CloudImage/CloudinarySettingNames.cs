﻿namespace DinePlan.DineConnect.CloudImage
{
    public class CloudinarySettingNames
    {
        public string CloudName { get; set; }

        public string APIKey { get; set; }

        public string APISecret { get; set; }

        public string EnvironmentVariable { get; set; }
    }
}
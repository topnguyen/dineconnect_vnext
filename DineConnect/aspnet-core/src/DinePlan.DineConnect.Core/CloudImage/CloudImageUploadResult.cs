﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.CloudImage
{
    public class CloudImageUploadResult
    {
        public string PublicId { get; set; }

        public string Url { get; set; }
        
        public string Format { get; set; }
    }
}

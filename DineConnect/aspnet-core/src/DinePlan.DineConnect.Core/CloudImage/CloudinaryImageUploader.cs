﻿using System;
using System.IO;
using System.Threading.Tasks;
using Abp.Dependency;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace DinePlan.DineConnect.CloudImage
{
    public class CloudinaryImageUploader : ICloudImageUploader, ITransientDependency
    {
        private readonly ICloudinaryConfiguration _configuration;

        private Cloudinary m_cloudinary;

        protected Cloudinary Cloudinary
        {
            get
            {
                if (m_cloudinary == null)
                {
                    Account acc = new Account(
                    _configuration.CloudName,
                    _configuration.APIKey,
                    _configuration.APISecret);

                    m_cloudinary = new Cloudinary(acc);
                }

                return m_cloudinary;
            }
        }

        public CloudinaryImageUploader(ICloudinaryConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<CloudImageUploadResult> UploadImage(string imageName, byte[] data)
        {
            var name = Guid.NewGuid().ToString("N");
            var imageId = Guid.NewGuid().ToString("N");

            var uploadParam = new ImageUploadParams()
            {
                File = new FileDescription(name, new MemoryStream(data)),
                PublicId = imageId,
            };

            var result = await Cloudinary.UploadAsync(uploadParam);
            if (result != null)
            {
                return new CloudImageUploadResult
                {
                    PublicId = result.PublicId,
                    Url = result.SecureUri.ToString(),
                    Format = result.Format
                };
            }

            return null;
        }

        public async Task<CloudImageUploadResult> UploadVideo(string videoName, byte[] data)
        {
            var name = Guid.NewGuid().ToString("N");
            var videoId = Guid.NewGuid().ToString("N");

            var uploadParams = new VideoUploadParams()
            {
                File = new FileDescription(name, new MemoryStream(data)),
                PublicId = videoId
            };

            var result = await Cloudinary.UploadAsync(uploadParams);
            if (result != null)
            {
                return new CloudImageUploadResult
                {
                    PublicId = result.PublicId,
                    Url = result.SecureUri.ToString(),
                    Format = result.Format
                };
            }

            return null;
        }

        public async Task<bool> CloudinaryAvailable()
        {
            if (Cloudinary != null && !string.IsNullOrEmpty(_configuration.APIKey))
            {
                return true;
            }

            return false;
        }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Flyer
{
    [Table("Flyer")]
    public class Flyer : ConnectFullMultiTenantAuditEntity
    {
        public string Content { get; set; }

        public string Data { get; set; }
    }
}

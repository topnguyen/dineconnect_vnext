﻿using System;
using Abp.AspNetZeroCore;
using Abp.AspNetZeroCore.Timing;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Modules;
using Abp.Net.Mail;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries.Xml;
using Abp.Localization.Sources;
using Abp.MailKit;
using Abp.Net.Mail.Smtp;
using Abp.Zero;
using Abp.Zero.Configuration;
using Abp.Zero.Ldap;
using Abp.Zero.Ldap.Configuration;
using Castle.MicroKernel.Registration;
using MailKit.Security;
using DinePlan.DineConnect.Authorization.Delegation;
using DinePlan.DineConnect.Authorization.Ldap;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Chat;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.DashboardCustomization.Definitions;
using DinePlan.DineConnect.Debugging;
using DinePlan.DineConnect.DynamicEntityParameters;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Friendships;
using DinePlan.DineConnect.Friendships.Cache;
using DinePlan.DineConnect.Localization;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Net.Emailing;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.WebHooks;
using Abp.MultiTenancy;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.Core.QueryScheduler.Interfaces;
using DinePlan.DineConnect.Core.Report;
using DinePlan.DineConnect.Core.QueryScheduler.Jobs;
using DinePlan.DineConnect.Core.QueryScheduler.Services;

namespace DinePlan.DineConnect
{
    [DependsOn(
        typeof(DineConnectCoreSharedModule),
        typeof(AbpZeroCoreModule),
        typeof(AbpZeroLdapModule),
        typeof(AbpAutoMapperModule),
        typeof(AbpAspNetZeroCoreModule),
        typeof(AbpMailKitModule))]
    public class DineConnectCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            //workaround for issue: https://github.com/aspnet/EntityFrameworkCore/issues/9825
            //related github issue: https://github.com/aspnet/EntityFrameworkCore/issues/10407
            AppContext.SetSwitch("Microsoft.EntityFrameworkCore.Issue9825", true);

            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            //Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            DineConnectLocalizationConfigurer.Configure(Configuration.Localization);

            //Adding feature providers
            Configuration.Features.Providers.Add<AppFeatureProvider>();

            //Adding setting providers
            Configuration.Settings.Providers.Add<AppSettingProvider>();

            //Adding notification providers
            Configuration.Notifications.Providers.Add<AppNotificationProvider>();

            //Adding webhook definition providers
            Configuration.Webhooks.Providers.Add<AppWebhookDefinitionProvider>();
            Configuration.Webhooks.TimeoutDuration = TimeSpan.FromMinutes(1);
            Configuration.Webhooks.IsAutomaticSubscriptionDeactivationEnabled = false;

            //Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = DineConnectConsts.MultiTenancyEnabled;

            Configuration.MultiTenancy.TenantIdResolveKey = DineConnectConsts.TenantIdResolveKey;



            //Adding DynamicEntityParameters definition providers
            Configuration.DynamicEntityParameters.Providers.Add<AppDynamicEntityParameterDefinitionProvider>();

            // MailKit configuration
            //Configuration.Modules.AbpMailKit().SecureSocketOption = SecureSocketOptions.Auto;
            //Configuration.ReplaceService<IMailKitSmtpBuilder, DineConnectMailKitSmtpBuilder>(DependencyLifeStyle.Transient);
         

            Configuration.ReplaceService<ITenantCache, DineConnectTenantCacheExtended>(Abp.Dependency.DependencyLifeStyle.Transient);

            //Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            if (DebugHelper.IsDebug)
            {
                Configuration.ReplaceService<IEmailSender, NullEmailSender>(DependencyLifeStyle.Transient);
            }

            Configuration.ReplaceService(typeof(IEmailSenderConfiguration), () =>
            {
                Configuration.IocManager.IocContainer.Register(
                    Component.For<IEmailSenderConfiguration, ISmtpEmailSenderConfiguration>()
                             .ImplementedBy<DineConnectSmtpEmailSenderConfiguration>()
                             .LifestyleTransient()
                );
            });

            Configuration.Caching.Configure(FriendCacheItem.CacheName, cache =>
            {
                cache.DefaultSlidingExpireTime = TimeSpan.FromMinutes(30);
            });

            IocManager.Register<DashboardConfiguration>();

            Configuration.IocManager.IocContainer.Register(
                   Component.For<ICloudinaryConfiguration>()
                            .ImplementedBy<CloudinaryConfiguration>()
                            .LifestyleTransient()
               );

            Configuration.IocManager.IocContainer.Register(
                   Component.For<ICloudImageUploader>()
                            .ImplementedBy<CloudinaryImageUploader>()
                            .LifestyleTransient()
               );

            Configuration.IocManager.IocContainer.Register(
                Component.For<IQuerySchedularService<ReportBackground>>()
                .ImplementedBy<QuerySchedularService<ReportBackground>>()
                .LifestyleSingleton()
                );

            Configuration.IocManager.IocContainer.Register(
                Component.For<ISendEmailJob>()
                .ImplementedBy<SendEmailJob<ReportBackground>>()
                .LifestyleSingleton()
                );

            Configuration.IocManager.IocContainer.Register(
    Component.For<IQuerySchedularService<string>>()
    .ImplementedBy<QuerySchedularService<string>>()
    .LifestyleSingleton()
    );

            Configuration.IocManager.IocContainer.Register(
                Component.For<ISendEmailJob>()
                .ImplementedBy<SendEmailJob<string>>()
                .LifestyleSingleton()
                );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.RegisterIfNot<IChatCommunicator, NullChatCommunicator>();
            IocManager.Register<IUserDelegationConfiguration, UserDelegationConfiguration>();

            IocManager.Resolve<ChatUserStateWatcher>().Initialize();
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }
    }
}
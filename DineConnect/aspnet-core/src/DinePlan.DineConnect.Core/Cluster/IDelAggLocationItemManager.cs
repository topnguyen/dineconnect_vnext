﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationItemManager
    {
        Task<IdentityResult> CreateSync(DelAggLocationItem delAggLocationItem);

        Task<IdentityResult> UpdateSync(DelAggLocationItem delAggLocationItem);
    }
}

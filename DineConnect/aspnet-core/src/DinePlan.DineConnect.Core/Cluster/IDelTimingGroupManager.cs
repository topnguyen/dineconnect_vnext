﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelTimingGroupManager
    {
        Task<IdentityResult> CreateSync(DelTimingGroup delTimingGroup);
    }
}


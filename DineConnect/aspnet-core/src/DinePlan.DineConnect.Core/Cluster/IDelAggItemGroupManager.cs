﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggItemGroupManager
    {
        Task<IdentityResult> CreateSync(DelAggItemGroup delAggItemGroup);
    }
}
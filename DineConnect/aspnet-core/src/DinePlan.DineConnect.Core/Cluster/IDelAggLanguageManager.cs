﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLanguageManager
    {
        Task<IdentityResult> CreateOrUpdateSync(DelAggLanguage delAggLanguage);
    }
}

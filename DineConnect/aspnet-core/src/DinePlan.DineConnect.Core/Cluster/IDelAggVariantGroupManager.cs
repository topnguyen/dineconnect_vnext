﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggVariantGroupManager
    {
        Task<IdentityResult> CreateSync(DelAggVariantGroup delAggVariantGroup);
    }
}

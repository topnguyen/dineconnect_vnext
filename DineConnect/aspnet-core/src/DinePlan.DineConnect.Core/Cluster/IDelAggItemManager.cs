﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggItemManager
    {
        Task<IdentityResult> CreateSync(DelAggItem delAggItem);
    }
}
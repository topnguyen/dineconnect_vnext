﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggChargeManager
    {
        Task<IdentityResult> CreateSync(DelAggCharge delAggCharge);
    }
}
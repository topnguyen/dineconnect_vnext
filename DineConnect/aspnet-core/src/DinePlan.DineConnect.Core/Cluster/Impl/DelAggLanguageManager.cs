﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    
    public class DelAggLanguageManager : DineConnectServiceBase, IDelAggLanguageManager, ITransientDependency
    {

        private readonly IRepository<DelAggLanguage> _languageDescriptionRepo;

        public DelAggLanguageManager(
            IRepository<DelAggLanguage> languageDescriptionRepo)
        {
            _languageDescriptionRepo = languageDescriptionRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(DelAggLanguage languageDescription)
        {
            if (languageDescription.Id == 0)
            {
                if (_languageDescriptionRepo.GetAll().Any(a => a.Id.Equals(languageDescription.Id)))
                {
                                       IdentityError[] errors = { new IdentityError
                    {
                        Code = "NameAlreadyExists",
                        Description = L("NameAlreadyExists")
                    } };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }
                await _languageDescriptionRepo.InsertAndGetIdAsync(languageDescription);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggLanguage> lst = _languageDescriptionRepo.GetAll().Where(a => a.Id.Equals(languageDescription.Id) && a.Id != languageDescription.Id).ToList();
                if (lst.Count > 0)
                {
                                       IdentityError[] errors = { new IdentityError
                    {
                        Code = "NameAlreadyExists",
                        Description = L("NameAlreadyExists")
                    } };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

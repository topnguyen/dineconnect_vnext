﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelTimingGroupManager : DineConnectServiceBase, IDelTimingGroupManager, ITransientDependency
    {
        private readonly IRepository<DelTimingGroup> _delTimingGroupRepo;

        public DelTimingGroupManager(IRepository<DelTimingGroup> delTimingGroup)
        {
            _delTimingGroupRepo = delTimingGroup;
        }

        public async Task<IdentityResult> CreateSync(DelTimingGroup delTimingGroup)
        {
            //  if the New Addition
            if (delTimingGroup.Id == 0)
            {
                if (_delTimingGroupRepo.GetAll().Any(a => a.Name.Equals(delTimingGroup.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delTimingGroupRepo.InsertAndGetIdAsync(delTimingGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<DelTimingGroup> lst = _delTimingGroupRepo.GetAll()
                    .Where(a => a.Name.Equals(delTimingGroup.Name) && a.Id != delTimingGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);

                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
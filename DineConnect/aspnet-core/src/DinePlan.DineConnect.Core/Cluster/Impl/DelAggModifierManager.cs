﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;


namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggModifierManager : DineConnectServiceBase, IDelAggModifierManager, ITransientDependency
    {
        private readonly IRepository<DelAggModifier> _delAggModifierRepo;

        public DelAggModifierManager(IRepository<DelAggModifier> modifier)
        {
            _delAggModifierRepo = modifier;
        }

        public async Task<IdentityResult> CreateSync(DelAggModifier delAggModifier)
        {
            //  if the New Addition
            if (delAggModifier.Id == 0)
            {
                if (_delAggModifierRepo.GetAll().Any(a => a.Name.Equals(delAggModifier.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggModifierRepo.InsertAndGetIdAsync(delAggModifier);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggModifier> lst = _delAggModifierRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggModifier.Id) && a.Id != delAggModifier.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
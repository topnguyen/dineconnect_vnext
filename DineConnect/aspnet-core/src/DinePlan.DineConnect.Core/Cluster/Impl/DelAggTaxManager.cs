﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggTaxManager : DineConnectServiceBase, IDelAggTaxManager, ITransientDependency
    {
        private readonly IRepository<DelAggTax> _delAggTaxRepo;

        public DelAggTaxManager(IRepository<DelAggTax> delAggTax)
        {
            _delAggTaxRepo = delAggTax;
        }

        public async Task<IdentityResult> CreateSync(DelAggTax delAggTax)
        {
            //  if the New Addition
            if (delAggTax.Id == 0)
            {
                if (_delAggTaxRepo.GetAll().Any(a => a.Name.Equals(delAggTax.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggTaxRepo.InsertAndGetIdAsync(delAggTax);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggTax> lst = _delAggTaxRepo.GetAll()
                    .Where(a => a.Name.Equals(delAggTax.Name) && a.Id != delAggTax.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
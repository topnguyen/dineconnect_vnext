﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggCategoryManager : DineConnectServiceBase, IDelAggCategoryManager, ITransientDependency
    {
        private readonly IRepository<DelAggCategory> _delAggCategoryRepo;

        public DelAggCategoryManager(IRepository<DelAggCategory> delAggCategory)
        {
            _delAggCategoryRepo = delAggCategory;
        }

        public async Task<IdentityResult> CreateSync(DelAggCategory delAggCategory)
        {
            //  if the New Addition
            if (delAggCategory.Id == 0)
            {
                if (_delAggCategoryRepo.GetAll().Any(a => a.Name.Equals(delAggCategory.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggCategoryRepo.InsertAndGetIdAsync(delAggCategory);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggCategory> lst = _delAggCategoryRepo.GetAll()
                    .Where(a => a.Name.Equals(delAggCategory.Name) && a.Id != delAggCategory.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
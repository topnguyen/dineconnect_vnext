﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;


namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationItemManager : DineConnectServiceBase, IDelAggLocationItemManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocationItem> _delAggLocationItemRepo;

        public DelAggLocationItemManager(IRepository<DelAggLocationItem> delAggLocationItemRepo)
        {
            _delAggLocationItemRepo = delAggLocationItemRepo;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocationItem delAggLocationItem)
        {
            var existed = _delAggLocationItemRepo.GetAll().Any(a =>
                a.DelAggLocMappingId == delAggLocationItem.DelAggLocMappingId
                && a.DelAggItemId == delAggLocationItem.DelAggItemId);
            if (existed)
            {
                IdentityError[] errors =
                {
                    new IdentityError
                    {
                        Code = "NameAlreadyExists",
                        Description = L("NameAlreadyExists")
                    }
                };
                var success = IdentityResult.Failed(errors);
                return success;
            }

            await _delAggLocationItemRepo.InsertAndGetIdAsync(delAggLocationItem);
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateSync(DelAggLocationItem delAggLocationItem)
        {
            await _delAggLocationItemRepo.UpdateAsync(delAggLocationItem);
            return IdentityResult.Success;
        }
    }
}
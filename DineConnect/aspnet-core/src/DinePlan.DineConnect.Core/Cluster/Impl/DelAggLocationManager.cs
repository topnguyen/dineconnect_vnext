﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;


namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationManager : DineConnectServiceBase, IDelAggLocationManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocation> _delAggLocationRepo;

        public DelAggLocationManager(IRepository<DelAggLocation> location)
        {
            _delAggLocationRepo = location;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocation delAggLocation)
        {
            if (delAggLocation.Id == 0)
            {
                if (_delAggLocationRepo.GetAll().Any(a =>
                        a.Id.Equals(delAggLocation.Id) && a.LocationId.Equals(delAggLocation.Location) &&
                        a.DelAggLocationGroupId == delAggLocation.DelAggLocationGroupId))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggLocationRepo.InsertAndGetIdAsync(delAggLocation);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggLocation> lst = _delAggLocationRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggLocation.Id) && a.Id != delAggLocation.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
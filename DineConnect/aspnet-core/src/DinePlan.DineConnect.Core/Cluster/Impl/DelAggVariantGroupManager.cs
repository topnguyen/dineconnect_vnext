﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggVariantGroupManager : DineConnectServiceBase, IDelAggVariantGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggVariantGroup> _delAggVariantGroupRepo;

        public DelAggVariantGroupManager(IRepository<DelAggVariantGroup> variantgroup)
        {
            _delAggVariantGroupRepo = variantgroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggVariantGroup delAggVariantGroup)
        {
            //  if the New Addition
            if (delAggVariantGroup.Id == 0)
            {
                if (_delAggVariantGroupRepo.GetAll().Any(a => a.Name.Equals(delAggVariantGroup.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggVariantGroupRepo.InsertAndGetIdAsync(delAggVariantGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggVariantGroup> lst = _delAggVariantGroupRepo.GetAll()
                    .Where(a => a.Name.Equals(delAggVariantGroup.Name) && a.Id != delAggVariantGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
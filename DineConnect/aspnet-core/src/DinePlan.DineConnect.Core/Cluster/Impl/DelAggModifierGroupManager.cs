﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;


namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggModifierGroupManager : DineConnectServiceBase, IDelAggModifierGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggModifierGroup> _delAggModifierGroupRepo;

        public DelAggModifierGroupManager(IRepository<DelAggModifierGroup> modifiergroup)
        {
            _delAggModifierGroupRepo = modifiergroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggModifierGroup delAggModifierGroup)
        {
            //  if the New Addition
            if (delAggModifierGroup.Id == 0)
            {
                if (_delAggModifierGroupRepo.GetAll().Any(a => a.Name.Equals(delAggModifierGroup.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggModifierGroupRepo.InsertAndGetIdAsync(delAggModifierGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggModifierGroup> lst = _delAggModifierGroupRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggModifierGroup.Id) && a.Id != delAggModifierGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
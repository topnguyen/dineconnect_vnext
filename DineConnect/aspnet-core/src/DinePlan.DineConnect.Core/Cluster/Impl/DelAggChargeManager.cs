﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggChargeManager : DineConnectServiceBase, IDelAggChargeManager, ITransientDependency
    {
        private readonly IRepository<DelAggCharge> _delAggChargeRepo;

        public DelAggChargeManager(IRepository<DelAggCharge> delAggCharge)
        {
            _delAggChargeRepo = delAggCharge;
        }

        public async Task<IdentityResult> CreateSync(DelAggCharge delAggCharge)
        {
            //  if the New Addition
            if (delAggCharge.Id == 0)
            {
                if (_delAggChargeRepo.GetAll().Any(a => a.Name.Equals(delAggCharge.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggChargeRepo.InsertAndGetIdAsync(delAggCharge);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggCharge> lst = _delAggChargeRepo.GetAll()
                    .Where(a => a.Name.Equals(delAggCharge.Name) && a.Id != delAggCharge.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
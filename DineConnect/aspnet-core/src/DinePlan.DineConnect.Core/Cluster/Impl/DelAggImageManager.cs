﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggImageManager : DineConnectServiceBase, IDelAggImageManager, ITransientDependency
    {
        private readonly IRepository<DelAggImage> _delAggImageRepo;

        public DelAggImageManager(IRepository<DelAggImage> delAggImage)
        {
            _delAggImageRepo = delAggImage;
        }

        public async Task<IdentityResult> CreateSync(DelAggImage delAggImage)
        {
            //  if the New Addition
            if (delAggImage.Id == 0)
            {
                if (_delAggImageRepo.GetAll().Any(a => a.Id.Equals(delAggImage.Id)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggImageRepo.InsertAndGetIdAsync(delAggImage);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggImage> lst = _delAggImageRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggImage.Id) && a.Id != delAggImage.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
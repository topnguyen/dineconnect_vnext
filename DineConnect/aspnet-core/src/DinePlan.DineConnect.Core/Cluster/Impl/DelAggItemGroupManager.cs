﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggItemGroupManager : DineConnectServiceBase, IDelAggItemGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggItemGroup> _delAggItemGroupRepo;

        public DelAggItemGroupManager(IRepository<DelAggItemGroup> delAggItemGroup)
        {
            _delAggItemGroupRepo = delAggItemGroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggItemGroup delAggItemGroup)
        {
            //  if the New Addition
            if (delAggItemGroup.Id == 0)
            {
                if (_delAggItemGroupRepo.GetAll().Any(a => a.Id.Equals(delAggItemGroup.Id)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggItemGroupRepo.InsertAndGetIdAsync(delAggItemGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggItemGroup> lst = _delAggItemGroupRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggItemGroup.Id) && a.Id != delAggItemGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
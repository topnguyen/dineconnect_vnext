﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggItemManager : DineConnectServiceBase, IDelAggItemManager, ITransientDependency
    {
        private readonly IRepository<DelAggItem> _delAggItemRepo;

        public DelAggItemManager(IRepository<DelAggItem> delAggItem)
        {
            _delAggItemRepo = delAggItem;
        }

        public async Task<IdentityResult> CreateSync(DelAggItem delAggItem)
        {
            //  if the New Addition
            if (delAggItem.Id == 0)
            {
                if (_delAggItemRepo.GetAll().Any(a => a.Name.Equals(delAggItem.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggItemRepo.InsertAndGetIdAsync(delAggItem);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggItem> lst = _delAggItemRepo.GetAll()
                    .Where(a => a.Name.Equals(delAggItem.Name) && a.Id != delAggItem.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
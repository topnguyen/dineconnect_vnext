﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;


namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationGroupManager : DineConnectServiceBase, IDelAggLocationGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocationGroup> _delAggLocationGroupRepo;

        public DelAggLocationGroupManager(IRepository<DelAggLocationGroup> locationgroup)
        {
            _delAggLocationGroupRepo = locationgroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocationGroup delAggLocationGroup)
        {
            //  if the New Addition
            if (delAggLocationGroup.Id == 0)
            {
                if (_delAggLocationGroupRepo.GetAll().Any(a => a.Name.Equals(delAggLocationGroup.Name)))
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                await _delAggLocationGroupRepo.InsertAndGetIdAsync(delAggLocationGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<DelAggLocationGroup> lst = _delAggLocationGroupRepo.GetAll()
                    .Where(a => a.Id.Equals(delAggLocationGroup.Id) && a.Id != delAggLocationGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    IdentityError[] errors =
                    {
                        new IdentityError
                        {
                            Code = "NameAlreadyExists",
                            Description = L("NameAlreadyExists")
                        }
                    };
                    var success = IdentityResult.Failed(errors);
                    return success;
                }

                return IdentityResult.Success;
            }
        }
    }
}
﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggTaxManager
    {
        Task<IdentityResult> CreateSync(DelAggTax delAggTax);
    }
}
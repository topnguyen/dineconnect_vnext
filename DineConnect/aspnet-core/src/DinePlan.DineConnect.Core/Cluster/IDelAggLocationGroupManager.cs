﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationGroupManager
    {
        Task<IdentityResult> CreateSync(DelAggLocationGroup delAggLocationGroup);
    }
}

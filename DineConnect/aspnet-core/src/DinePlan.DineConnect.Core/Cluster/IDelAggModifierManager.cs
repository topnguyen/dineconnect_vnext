﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggModifierManager
    {
        Task<IdentityResult> CreateSync(DelAggModifier delAggModifier);
    }
}

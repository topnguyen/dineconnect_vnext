﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tag.OrderTags;

namespace DinePlan.DineConnect.Cluster
{
    [Table("DelAggLocationGroups")]
    public class DelAggLocationGroup : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
    }

    [Table("DelAggLocations")]
    public class DelAggLocation : ConnectLocFullMultiTenantAuditEntity
    {
        public virtual int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual string AddOns { get; set; }
        public virtual string Name { get; set; }

        public virtual int? DelAggLocationGroupId { get; set; }
        [ForeignKey("DelAggLocationGroupId")]
        public virtual DelAggLocationGroup DelAggLocationGroup { get; set; }
    }

    [Table("DelAggLocMappings")]
    public class DelAggLocMapping : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public virtual int DelAggLocationId { get; set; }
        [ForeignKey("DelAggLocationId")]
        public virtual DelAggLocation DelAggLocation { get; set; }

        public virtual ClusterEnum.DelAggType DelAggTypeRefId { get; set; }

        public string GatewayCode { get; set; }
        public string RemoteCode { get; set; }
        public string DeliveryUrl { get; set; }
        public bool Active { get; set; }
        public string AddOns { get; set; }
        public string MenuContents { get; set; }


    }
    [Table("DelAggLocationItems")]
    public class DelAggLocationItem : ConnectFullMultiTenantAuditEntity
    {
        public virtual int DelAggLocMappingId { get; set; }
        [ForeignKey("DelAggLocMappingId")]
        public virtual DelAggLocMapping DelAggLocMapping { get; set; }

        public virtual int? DelAggItemId { get; set; }
        [ForeignKey("DelAggItemId")]
        public virtual DelAggItem DelAggItem { get; set; }

        public decimal Price { get; set; }
        public bool InActive { get; set; }

        public string AddOns { get; set; }

        public virtual int? DelAggVariantId { get; set; }
        [ForeignKey("DelAggVariantId")]
        public virtual DelAggVariant DelAggVariant { get; set; }

        public virtual int? DelAggModifierId { get; set; }
        [ForeignKey("DelAggModifierId")]
        public virtual DelAggModifier DelAggModifier { get; set; }

        public virtual ClusterEnum.DelAggSpecType DelAggPriceTypeRefId { get; set; }
    }

    [Table("DelAggItemGroups")]
    public class DelAggItemGroup : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

    }

    [Table("DelAggCategories")]
    public class DelAggCategory : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }

        public int? DelAggCatParentId { get; set; }
        [ForeignKey("DelAggCatParentId")]
        public DelAggCategory DelAggCategories { get; set; }

        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }

        public int DelTimingGroupId { get; set; }
        [ForeignKey("DelTimingGroupId")]
        public DelTimingGroup DelTimingGroup { get; set; }

    }
    [Table("DelAggItems")]
    public class DelAggItem : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual string FoodType { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public virtual bool IsRecommended { get; set; }
        public virtual decimal Weight { get; set; }
        public virtual decimal Serves { get; set; }

        public virtual int DelAggVariantGroupId { get; set; }
        [ForeignKey("DelAggVariantGroupId")]
        public virtual DelAggVariantGroup DelAggVariantGroup { get; set; }

        public virtual int? DelAggTaxId { get; set; }
        [ForeignKey("DelAggTaxId")]
        public virtual DelAggTax DelAggTax { get; set; }


        public virtual int DelAggItemGroupId { get; set; }
        [ForeignKey("DelAggItemGroupId")]
        public virtual DelAggItemGroup DelAggItemGroup { get; set; }

        public virtual int DelAggCatId { get; set; }
        [ForeignKey("DelAggCatId")]
        public virtual DelAggCategory DelAggCategory { get; set; }
        public virtual int MenuItemId { get; set; }
        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }
    }

    [Table("DelAggVariantGroups")]
    public class DelAggVariantGroup : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public string LocalRefCode { get; set; }
        public int SortOrder { get; set; }

        public List<DelAggVariant> DelAggVariants { get; set; }
    }

    [Table("DelAggVariants")]
    public class DelAggVariant : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public int DelAggVariantGroupId { get; set; }
        [ForeignKey("DelAggVariantGroupId")]
        public DelAggVariantGroup DelAggVariantGroup { get; set; }
        public int MenuItemPortionId { get; set; }
        [ForeignKey("MenuItemPortionId")]
        public MenuItemPortion MenuItemPortion { get; set; }
        public virtual decimal SalesPrice { get; set; }
        public virtual decimal MarkupPrice { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        [ForeignKey("NestedModGroupId")]
        public DelAggVariant DelAggVariants { get; set; }

    }


    [Table("DelAggModifierGroups")]
    public class DelAggModifierGroup : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int? OrderTagGroupId { get; set; }
        [ForeignKey("OrderTagGroupId")]
        public OrderTagGroup OrderTagGroup { get; set; }

    }
    [Table("DelAggModifiers")]
    public class DelAggModifier : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public int? DelAggModifierGroupId { get; set; }
        [ForeignKey("DelAggModifierGroupId")]
        public DelAggModifierGroup DelAggModifierGroup { get; set; }
        public int? OrderTagId { get; set; }
        [ForeignKey("OrderTagId")]
        public OrderTag OrderTag { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? NestedModGroupId { get; set; }
        [ForeignKey("NestedModGroupId")]
        public DelAggModifier DelAggModifiers { get; set; }

    }
    [Table("DelAggModifierGroupItems")]
    public class DelAggModifierGroupItem : ConnectFullMultiTenantAuditEntity
    {
        public int DelAggModifierGroupId { get; set; }
        [ForeignKey("DelAggModifierGroupId")]
        public DelAggModifierGroup DelAggModifierGroup { get; set; }
        public int DelAggItemId { get; set; }
        [ForeignKey("DelAggItemId")]
        public DelAggItem DelAggItem { get; set; }
        public string Addon  { get; set; }
    }

    [Table("DelTimingGroups")]

    public class DelTimingGroup : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual ICollection<DelTimingDetail> DelTimingDetails { get; set; }

    }
    [Table("DelTimingDetails")]
    public class DelTimingDetail : ConnectFullMultiTenantAuditEntity
    {
        public virtual int DelTimingGroupId { get; set; }

        [ForeignKey("DelTimingGroupId")]
        public virtual DelTimingGroup DelTimingGroup { get; set; }
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public string Days { get; set; }
        public string AddOns { get; set; }

    }

    [Table("DelAggTaxes")]
    public class DelAggTax : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual int TaxTypeId { get; set; }
        public decimal TaxPercentage { get; set; }
        public string LocalRefCode { get; set; }
    }

    [Table("DelAggTaxMappings")]
    public class DelAggTaxMapping : ConnectFullMultiTenantAuditEntity
    {
        public int DelAggTaxId { get; set; }
        [ForeignKey("DelAggTaxId")]
        public DelAggTax DelAggTax { get; set; }

        public int? DelAggLocationGroupId { get; set; }
        [ForeignKey("DelAggLocationGroupId")]
        public DelAggLocationGroup DelAggLocationGroup { get; set; }

        public int? DelItemGroupId { get; set; }
        [ForeignKey("DelItemGroupId")]
        public DelAggItemGroup DelItemGroup { get; set; }
    }

    [Table("DelAggCharges")]
    public class DelAggCharge : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string ApplicableOn { get; set; }
        public virtual Decimal Amount { get; set; }
        public virtual string ExcludeDeliveryTypes { get; set; }
        public virtual string FullFilmentTypes { get; set; }
        public string LocalRefCode { get; set; }
    }

    [Table("DelAggChargeMappings")]
    public class DelAggChargeMapping : ConnectFullMultiTenantAuditEntity
    {
        public int DelAggChargeId { get; set; }
        [ForeignKey("DelAggChargeId")]
        public DelAggCharge DelAggCharge { get; set; }

        public int DelAggLocationGroupId { get; set; }
        [ForeignKey("DelAggLocationGroupId")]
        public DelAggLocationGroup DelAggLocationGroup { get; set; }
        public int DelItemGroupId { get; set; }
        [ForeignKey("DelItemGroupId")]
        public DelAggItemGroup DelItemGroup { get; set; }
    }
    [Table("DelAggLanguages")]
    public class DelAggLanguage : ConnectFullMultiTenantAuditEntity
    {
        public virtual int DelAggTypeRefId { get; set; }
        public string Language { get; set; }
        public string LanguageValue { get; set; }
        public int? ReferenceId { get; set; }
        public string AddOns { get; set; }
        public ClusterEnum.DelLanguageDescriptionType LanguageDescriptionType { get; set; }

    }
    [Table("DelAggImages")]
    public class DelAggImage : ConnectFullMultiTenantAuditEntity
    {
        public virtual int DelAggImageTypeRefId { get; set; }
        public virtual int? DelAggTypeRefId { get; set; }
        public int? ReferenceId { get; set; }
        public string ImagePath { get; set; }
        public string AddOns { get; set; }
    }
}

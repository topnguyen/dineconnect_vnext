﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocMappingManager
    {
        Task<IdentityResult> CreateSync(DelAggLocMapping delAggLocMapping);
    }
}

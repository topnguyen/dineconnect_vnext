﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggCategoryManager
    {
        Task<IdentityResult> CreateSync(DelAggCategory delAggCategory);
    }
}
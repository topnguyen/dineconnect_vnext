﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationManager
    {
        Task<IdentityResult> CreateSync(DelAggLocation delAggLocation);
    }
}

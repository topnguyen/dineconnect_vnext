﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggImageManager
    {
        Task<IdentityResult> CreateSync(DelAggImage delAggImage);
    }
}
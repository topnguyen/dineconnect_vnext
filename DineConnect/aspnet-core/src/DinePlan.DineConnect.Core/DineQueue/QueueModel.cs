﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.DineQueue
{

    [Table("QueueLocations")]
    public class QueueLocation : ConnectFullMultiTenantAuditEntity
    {
        public bool EnableQueue { get; set; }

        public string Name { get; set; }

        public TimeSpan StartTime { get; set; }
        
        public TimeSpan EndTime { get; set; }
        
        public ICollection<QueueLocationOption> QueueLocationOptions { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public string ThemeSettings { get; set; }

    }

    [Table("QueueLocationOptions")]
    public class QueueLocationOption : ConnectFullAuditEntity
    {
        public string Name { get; set; }
        
        public string Prefix { get; set; }
        
        public int MinPax { get; set; }
        
        public int MaxPax { get; set; }

        public bool Active { get; set; }

        public int SortOrder { get; set; }
        
        public int QueueLocationId { get; set; }

        [ForeignKey(nameof(QueueLocationId))]
        public virtual QueueLocation QueueLocation { get; set; }

        public ICollection<CustomerQueue> CustomerQueues { get; set; }

    }

    [Table("CustomerQueues")]
    public class CustomerQueue : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey(nameof(QueueLocationOptionId))]
        public QueueLocationOption QueueLocationOption { get; set; }
        
        public int QueueLocationOptionId { get; set; }

        public string QueueNo { get; set; }
        
        public string CustomerName { get; set; }
        
        public string CustomerMobile { get; set; }
        
        [ForeignKey(nameof(CustomerId))] 
        public Customer Customer { get; set; }

        public int? CustomerId { get; set; }

        public DateTimeOffset? QueueTime { get; set; }
        
        public DateTimeOffset? CallTime { get; set; }

        public DateTimeOffset? FinishTime { get; set; }

        public bool Seated { get; set; }

    }

}

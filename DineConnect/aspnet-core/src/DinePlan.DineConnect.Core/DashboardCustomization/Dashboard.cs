﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.DashboardCustomization
{
    public class Dashboard
    {
        public string DashboardName { get; set; }

        public List<Page> Pages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Session
{
    public interface IWheelSession
    {
        long? UserId { get; }
        Guid? GuestCustomerId { get; }
    }
}

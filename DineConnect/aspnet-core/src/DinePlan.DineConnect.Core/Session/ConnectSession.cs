﻿using Abp;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.MultiTenancy;
using Abp.Runtime;
using Abp.Runtime.Session;
using System;

namespace DinePlan.DineConnect.Session
{
    public class ConnectSession : ClaimsAbpSession, ITransientDependency
    {
        private readonly ISettingManager _settingManager;

        public ConnectSession(
        IPrincipalAccessor principalAccessor,
        IMultiTenancyConfig multiTenancy,
        ISettingManager settingManager,
        ITenantResolver tenantResolver,
        IAmbientScopeProvider<SessionOverride> sessionOverrideScopeProvider) :
        base(principalAccessor, multiTenancy, tenantResolver, sessionOverrideScopeProvider)
        {
            _settingManager = settingManager;
        }

        public int OrgId
        {
            get
            {
                bool result = Int32.TryParse(_settingManager.GetSettingValue("OrganizationId"), out int organizationId);
                if (result)
                {
                    return organizationId;
                }
                return -1;
            }

            set
            {
                _settingManager.ChangeSettingForUser(new UserIdentifier(this.TenantId, this.GetUserId()), "OrganizationId", value.ToString());
            }
        }

        public int LocationId
        {
            get
            {
                bool result = Int32.TryParse(_settingManager.GetSettingValue("LocationId"), out int organizationId);
                if (result)
                {
                    return organizationId;
                }
                return -1;
            }

            set
            {
                _settingManager.ChangeSettingForUser(new UserIdentifier(this.TenantId, this.GetUserId()), "LocationId", value.ToString());
            }
        }
    }
}
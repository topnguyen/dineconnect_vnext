﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.MultiTenancy;
using Abp.Runtime;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DinePlan.DineConnect.Session
{
    public class WheelSession : ClaimsAbpSession, IWheelSession, ITransientDependency
    {
        public const string customerIdResolveKey = "Abp-CustomerId";

        private IHttpContextAccessor _httpContextAccessor;

        public WheelSession(
            IPrincipalAccessor principalAccessor,
            IMultiTenancyConfig multiTenancy,
            ITenantResolver tenantResolver,
            IAmbientScopeProvider<SessionOverride> sessionOverrideScopeProvider,
            IHttpContextAccessor httpContextAccessor
            ) :
            base(principalAccessor, multiTenancy, tenantResolver, sessionOverrideScopeProvider)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Guid? GuestCustomerId
        {
            get
            {
                return this.ResolveCustomerId();
            }
        }

        public Guid? ResolveCustomerId()
        {
            var httpContext = _httpContextAccessor.HttpContext;

            if (httpContext == null)
            {
                return null;
            }

            var customerId = httpContext.Request.Headers[customerIdResolveKey];

            if (customerId == string.Empty || customerId.Count < 1)
            {
                return null;
            }

            return Guid.TryParse(customerId.First(), out var tenantId) ? tenantId : (Guid?)null;
        }
    }
}


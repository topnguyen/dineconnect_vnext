﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Wheel
{
    [Table("LocationDeliveryZones")]

    public class LocationDeliveryZone : ConnectFullAuditEntity
    {
        public decimal MinimumAmount { get; set; }
        public decimal DeliveryFee { get; set; }
        public string Coordinates { get; set; }

    }
}
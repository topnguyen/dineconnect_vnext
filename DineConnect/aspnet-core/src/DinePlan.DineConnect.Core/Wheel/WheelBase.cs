﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Connect.Tag.OrderTags;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Settings;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;
using static DinePlan.DineConnect.Wheel.WheelOpeningHourConsts;

namespace DinePlan.DineConnect.Wheel
{
    public enum WheelPostalCodeSearchType
    {
        Singapore = 0,
        India = 1
    }

    public enum WheelFeeType
    {
        Percentage,
        Currency
    }

    public enum WheelFeeOrderType
    {
        AllOrders,
        AllOrdersWithDiscount,
        OrdersWithDays,
        OrdersWithPaymentMethod,
        OrderWithType
    }

    public enum WheelPostCodeServiceType
    {
        OneMap,
        GeoApi
    }

    [Table("WheelSetups")]
    public class WheelSetup : ConnectFullMultiTenantAuditEntity
    {
        [NotMapped] public ICollection<WheelLocation> WheelLocations { get; set; }
        public string WheelLocationIds { get; set; }
        public int? HomePageId { get; set; }
        public int? CheckOutPageId { get; set; }
        public string LogoCompanyUrl { get; set; }
        public string WelcomeUrl { get; set; }
        public string WelcomeMessage { get; set; }
        public string TermsAndConditions { get; set; }
        public string ClientUrl { get; set; }
        public bool IsDisplayHomePage { get; set; }
        public WheelPostCodeServiceType WheelPostCodeServiceType { get; set; }
        [ForeignKey("HomePageId")] public virtual WheelDynamicPages HomePage { get; set; }
        [ForeignKey("CheckOutPageId")] public virtual WheelDynamicPages CheckOutPage { get; set; }
        public string CountryCode { get; set; }
        public string ThemeName { get; set; }
        public string AddOns { get; set; }
        public string Title { get; set; }
        public string Currency { get; set; }


    }

    [Table("WheelLocations")]
    public class WheelLocation : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public bool Enabled { get; set; }
        [ForeignKey("LocationId")] public Location Location { get; set; }

        public int LocationId { get; set; }
        public bool TaxInclusive { get; set; }
        public bool OutsideDeliveryZone { get; set; }
        public string WheelDeliveryZoneIds { get; set; }
        public string WheelDepartmentIds { get; set; }
        public string WheelServiceFeeIds { get; set; }
        public string WheelTaxIds { get; set; }
        public string Email { get; set; }
        public decimal? OutsideDeliveryZoneDeliveryFee { get; set; }
        public string PickupContactName { get; set; }
        public string PickupContactPhoneNumber { get; set; }
        public string PickupAddressLat { get; set; }
        public string PickupAddressLong { get; set; }
        public string PickupAddress { get; set; }
    }

    [Table("WheelTaxes")]
    public class WheelTax : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        [ForeignKey("DinePlanTaxId")]
        public DinePlanTax DinePlanTax { get; set; }

        public int DinePlanTaxId { get; set; }
    }

    [Table("WheelDeliveryZones")]
    public class WheelDeliveryZone : ConnectFullMultiTenantAuditEntity
    {
        [Required] public string Name { get; set; }

        public decimal MinimumTotalAmount { get; set; }
        public decimal DeliveryFee { get; set; }
        public DeliveryZoneType ZoneType { get; set; }
        public string Color { get; set; }
        public string ZoneCoordinates { get; set; }
    }

    [Table("WheelOpeningHours")]
    public class WheelOpeningHour : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        public string Name { get; set; }

        public int OpenDays { get; set; }

        public WheelOpeningHourServiceType ServiceType { get; set; }
    }

    [Table("WheelDepartments")]
    public class WheelDepartment : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public int? MenuId { get; set; }
        public int? PriceTagId { get; set; }
        public bool Enable { get; set; }
        public WheelOrderType WheelOrderType { get; set; }

        [ForeignKey("PriceTagId")]
        public PriceTag PriceTag { get; set; }

        [ForeignKey("MenuId")]
        public ScreenMenu Menu { get; set; }

        public string WheelOpeningHourIds { get; set; }
        public string WheelPaymentMethodIds { get; set; }
        public string WheelServiceFeeIds { get; set; }
        public bool IsEnableTable { get; set; }
        public bool IsEnableDelivery { get; set; }
        public bool AllowPreOrder { get; set; }
        public string Dynamic { get; set; }
        public bool AutoAccept { get; set; }
        public ICollection<WheelTable> WheelTables { get; set; }
        public ICollection<WheelDepartmentTimeSlot> WheelDepartmentTimeSlot { get; set; }
    }

    [Table("WheelPaymentMethods")]
    public class WheelPaymentMethod : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        [ForeignKey("PaymentTypeId")] public PaymentType PaymentType { get; set; }

        public int PaymentTypeId { get; set; }
    }

    [Table("WheelServiceFees")]
    public class WheelServiceFee : ConnectFullMultiTenantAuditEntity
    {
        [Required] public string Name { get; set; }

        public WheelFeeType FeeType { get; set; }
        public decimal Value { get; set; }
        public bool ApplyOnSubTotal { get; set; }
        public bool CalculateTax { get; set; }
        public WheelFeeOrderType AppliedOrderType { get; set; }
        public string AppliedOrderInfo { get; set; }
        public string WheelTaxes { get; set; }
    }

    [Table("WheelDeliveryDurations")]
    public class WheelDeliveryDuration : ConnectFullMultiTenantAuditEntity
    {
        public decimal DeliveryDistance { get; set; }
        public decimal DeliveryTime { get; set; }
    }

    [Table("WheelDynamicPages")]
    public class WheelDynamicPages : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public string Html { get; set; }
        public string Css { get; set; }
        public string Assets { get; set; }
        public string Styles { get; set; }
        public string Components { get; set; }
    }

    [Table("WheelPayments")]
    public class WheelPayment : ConnectFullMultiTenantAuditEntity
    {
        public string InvoiceNo { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal? PaidAmount { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string PaymentOption { get; set; }

        public string ExtraInformation { get; set; }

    }

    [Table("WheelPaymentRequests")]
    public class WheelPaymentRequest : ConnectFullMultiTenantAuditEntity
    {
        public DateTime PaymentDate { get; set; }

        public decimal PaidAmount { get; set; }

        public string PaymentOption { get; set; }

        public string Request { get; set; }

        public string PaymentReference { get; set; }
    }

    [Table("WheelTickets")]
    public class WheelTicket : ConnectFullMultiTenantAuditEntity
    {
        private DateTime? _date = null;

        public WheelTicket()
        {
            WheelTicketShippingOrders = new List<WheelTicketShippingOrder>();
        }

        public int CustomerId { get; set; }

        public decimal? PaidAmount { get; set; }

        public WheelOrderStatus OrderStatus { get; set; }

        public int WheelPaymentId { get; set; }

        [ForeignKey("WheelPaymentId")]
        public WheelPayment Payment { get; set; }

        public string Address { get; set; }

        public int? CustomerAddressId { get; set; }

        [ForeignKey("CustomerAddressId")]
        public CustomerAddressDetail CustomerAddress { get; set; }

        public WheelOrderType WheelOrderType { get; set; }

        public ShippingProvider ShippingProvider { get; set; }

        public string ShippingProviderResponseJson { get; set; }

        public DateTime OrderCreatedTime { get; set; }

        public string Note { get; set; }

        public string DeliveryTime { get; set; }

        public decimal? Amount { get; set; }

        public int Quantity { get; set; } = 1;

        public string AddOn { get; set; }

        public string MenuItemsDynamic { get; set; }


        public string DeliveringTime { get; set; }

        [NotMapped]
        public DateTime? DateDelivery
        {
            get
            {
                DateTime? _date = null;
                if (!string.IsNullOrEmpty(DeliveryTime) && DeliveryTime != "Not Available")
                {
                    if (DeliveryTime.Contains("ASAP"))
                    {
                        _date = OrderCreatedTime.ToLocalTime();
                    }
                    else
                    {
                        var arrDate = DeliveryTime.Split(" ");

                        var date = arrDate[0].Trim();
                        var time = arrDate[1].Trim().Contains("-") ? arrDate[1].Trim().Split("-")[0].Trim() : arrDate[1].Trim();
                        _date = DateTime.Parse($"{date} {time}").ToLocalTime();
                    }
                }
                return _date;
            }
            set { _date = value; }
        }

        public decimal DiscountAmount { get; set; }

        public bool Synced { get; set; }

        public bool GatewayNotified { get; set; }

        public int? TableId { get; set; }

        public virtual int? WheelLocationId { get; set; }

        public List<WheelTicketShippingOrder> WheelTicketShippingOrders { get; set; }
        public string AmountCalculations { get; set; }
    }

    [Table("WheelTables")]
    public class WheelTable : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public WheelTableStatus WheelTableStatus { get; set; } = WheelTableStatus.Vacant;

        [ForeignKey("WheelLocationId")]
        public virtual WheelLocation WheelLocation { get; set; }

        public int? WheelLocationId { get; set; }

        [ForeignKey("WheelDepartmentId")]
        public virtual WheelDepartment WheelDepartment { get; set; }

        public int? WheelDepartmentId { get; set; }

        public string OtpCode { get; set; }
        public string BookedPhoneNumber { get; set; }
    }

    [Table("WheelDepartmentTimeSlots")]
    public class WheelDepartmentTimeSlot : ConnectFullMultiTenantAuditEntity
    {
        public string TimeFrom { get; set; }

        public string TimeTo { get; set; }

        public int? NumberOfOrder { get; set; }

        public int? WheelDepartmentId { get; set; }

        public virtual WheelDepartment WheelDepartment { get; set; }
    }

    [Table("WheelOrderTrackings")]
    public class WheelOrderTracking : ConnectFullMultiTenantAuditEntity
    {
        public int? CurrentCount { get; set; }

        public int? WheelDepartmentTimeSlotId { get; set; }

        public virtual WheelDepartmentTimeSlot WheelDepartmentTimeSlot { get; set; }

        public string OrderDate { get; set; }
    }

    [Table("WheelTicketShippingOrders")]
    public class WheelTicketShippingOrder : ConnectFullMultiTenantAuditEntity
    {
        public string ShippingOrderStatus { get; set; }
        public ShippingProvider ShippingProviderName { get; set; } 
        public string ShippingOrderAmount { get; set; } 
        public string ShippingOrderId { get; set; } 
        public int? WheelTicketId { get; set; }

        [ForeignKey("WheelTicketId")]
        public WheelTicket WheelTicket { get; set; }
    }

    [Table("WheelItemSoldOuts")]
    public class WheelItemSoldOut : ConnectFullMultiTenantAuditEntity
    {

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int LocationId { get; set; }
        
        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }
        public int MenuItemId { get; set; }

        [ForeignKey("MenuItemPortionId")]
        public MenuItemPortion MenuItemPortion { get; set; }
        public int? MenuItemPortionId { get; set; }
        public bool Disable { get; set; }
    }

    [Table("WheelTagSoldOuts")]
    public class WheelTagSoldOut : ConnectFullMultiTenantAuditEntity
    {

        [ForeignKey("WheelLocationId")]
        public WheelLocation WheelLocation { get; set; }
        public int WheelLocationId { get; set; }
        [ForeignKey("OrderTagId")]
        public OrderTag OrderTag { get; set; }
        public int OrderTagId { get; set; }
        public bool Disable { get; set; }
    }

    [Table("WheelRecommendationMenu")]
    public class WheelRecommendationMenu : ConnectFullMultiTenantAuditEntity
    {
        public string RepeatTime { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Pax { get; set; }
        public WheelRecommendationMenuConst.RecommendationStatus Status { get; set; } = WheelRecommendationMenuConst.RecommendationStatus.Active;
        public WheelRecommendationMenuConst.CriteriaNotShow? CriteriaNotShow { get; set; }
        public string TriggerApplicable { get; set; }
        public bool Disable { get; set; }
        public ICollection<WheelRecommendationDayPeriod> WheelRecommendationDayPeriods { get; set; }
        public ICollection<WheelRecommendationMenuItem> WheelRecommendationItems { get; set; }
    }

    [Table("WheelRecommendationDayPeriod")]
    public class WheelRecommendationDayPeriod : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public string From { get; set; }
        [Required]
        public string To { get; set; }
        public int OpenDays { get; set; }
        public int RecommendationId { get; set; }
        [ForeignKey("RecommendationId")]
        public WheelRecommendationMenu Recommendation { get; set; }
    }

    [Table("WheelRecommendationMenuItem")]
    public class WheelRecommendationMenuItem : ConnectFullMultiTenantAuditEntity
    {
        public int RecommendationId { get; set; }
        [ForeignKey("RecommendationId")]
        public WheelRecommendationMenu Recommendation { get; set; }
        public int MenuItemId { get; set; }
        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }

    }

}
﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Threading;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Session;
using Microsoft.EntityFrameworkCore;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class CustomerManager : DineConnectDomainServiceBase
    {
        private readonly IRepository<ConnectGuest> _connectGuestRepository;
        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<WheelCustomerSessionData> _customerSessionDataRepository;
        private readonly IWheelSession _wheelSession;

        public CustomerManager(IWheelSession wheelSession, IRepository<Customer> customerRepository,
            IRepository<ConnectGuest> connectGuestRepository,
            IRepository<WheelCustomerSessionData> customerSessionDataRepository)
        {
            _wheelSession = wheelSession;
            _customerRepository = customerRepository;
            _customerSessionDataRepository = customerSessionDataRepository;
            _connectGuestRepository = connectGuestRepository;
        }

        public virtual Customer GetCurrentCustomer()
        {
            return AsyncHelper.RunSync(GetCurrentCustomerAsync);
        }

        public virtual async Task<Customer> GetCurrentCustomerAsync()
        {
            Customer customer = null;
            if (_wheelSession.UserId.HasValue)
            {
                customer = await _customerRepository.GetAllIncluding(t => t.CustomerAddressDetails)
                    .FirstOrDefaultAsync(t => t.UserId == _wheelSession.UserId);
                if (customer != null)
                    return customer;
            }

            else if (_wheelSession.GuestCustomerId.HasValue)
            {
                var guestCustomer = await _connectGuestRepository.GetAll()
                    .FirstOrDefaultAsync(t => t.CustomerGuid == _wheelSession.GuestCustomerId);
                if (guestCustomer != null)
                    return new Customer
                    {
                        CustomerGuid = guestCustomer.CustomerGuid,
                        Id = guestCustomer.Id,
                        TenantId = guestCustomer.TenantId
                    };
                customer = await _customerRepository.GetAll()
                    .FirstOrDefaultAsync(t => t.CustomerGuid == _wheelSession.GuestCustomerId);
                return customer;
            }

            return null;
        }

        public virtual async Task<string> GetCustomerSessionData(int customerId, string key)
        {
            var data = await _customerSessionDataRepository.FirstOrDefaultAsync(t =>
                t.CustomerId == customerId && t.Key == key);

            if (data != null) return data.Value;

            return null;
        }

        public virtual async Task<string> SaveCustomerSessionData(int customerId, int tenantId, string key,
            string value)
        {
            var data = await _customerSessionDataRepository.FirstOrDefaultAsync(t =>
                t.CustomerId == customerId && t.Key == key);

            if (data != null)
                data.Value = value;
            else
                await _customerSessionDataRepository.InsertAsync(new WheelCustomerSessionData
                {
                    CustomerId = customerId,
                    TenantId = tenantId,
                    Key = key,
                    Value = value,
                    CreatedOn = DateTime.Now
                });

            return null;
        }

        public virtual async Task ClearAllCustomerSessionData(int customerId)
        {
            await _customerSessionDataRepository.DeleteAsync(t => t.CustomerId == customerId);
        }
    }
}
﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static DinePlan.DineConnect.Wheel.WheelDepartmentConsts;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelOrder : ConnectFullMultiTenantAuditEntity
    {
        private DateTime? _date;

        public WheelOrder()
        {
            WheelTicketShippingOrders = new List<WheelTicketShippingOrder>();
            WheelOrderItems = new List<WheelOrderItem>();
        }

        public int? CustomerId { get; set; }

        public decimal? PaidAmount { get; set; }

        public WheelOrderStatus OrderStatus { get; set; }

        public int WheelPaymentId { get; set; }

        [ForeignKey("WheelPaymentId")]
        public WheelPayment Payment { get; set; }

        public string Address { get; set; }

        public int? CustomerAddressId { get; set; }

        [ForeignKey("CustomerAddressId")]
        public CustomerAddressDetail CustomerAddress { get; set; }

        public WheelOrderType WheelOrderType { get; set; }

        public decimal? DeliveryCharge { get; set; }

        public int? SelfPickupLocationId { get; set; }

        [ForeignKey("SelfPickupLocationId")]
        public virtual Location SelfPickupLocation { get; set; }

        public ShippingProvider ShippingProvider { get; set; }

        public string ShippingProviderResponseJson { get; set; }

        public DateTime OrderCreatedTime { get; set; }

        public string Note { get; set; }

        public string DeliveryTime { get; set; }

        public decimal? Amount { get; set; }

        public int Quantity { get; set; } = 1;

        public string AddOn { get; set; }

        public List<WheelTicketShippingOrder> WheelTicketShippingOrders { get; set; }

        public List<WheelOrderItem> WheelOrderItems { get; set; }

        public string DeliveringTime { get; set; }

        [NotMapped]
        public DateTime? DateDelivery
        {
            get
            {
                DateTime? _date = null;
                if (!string.IsNullOrEmpty(DeliveryTime) && DeliveryTime != "Not Available")
                {
                    if (DeliveryTime.Contains("ASAP"))
                    {
                        _date = OrderCreatedTime.ToLocalTime();
                    }
                    else
                    {
                        var arrDate = DeliveryTime.Split(" ");

                        var date = arrDate[0].Trim();
                        var time = arrDate[1].Trim().Contains("-") ? arrDate[1].Trim().Split("-")[0].Trim() : arrDate[1].Trim();
                        _date = DateTime.Parse($"{date} {time}").ToLocalTime();
                    }
                }
                return _date;
            }
            set { _date = value; }
        }

        public decimal DiscountAmount { get; set; }

        public bool Synced { get; set; }

        public bool GatewayNotified { get; set; }

        public int? TableId { get; set; }

        public virtual int? WheelLocationId { get; set; }
    }
}

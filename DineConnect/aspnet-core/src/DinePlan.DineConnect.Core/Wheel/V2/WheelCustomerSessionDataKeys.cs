﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.V2
{
   public class WheelCustomerSessionDataKeys
    {
        public const string POSTAL_CODE = "POSTAL_CODE";
        public const string SELECTED_LOCATION = "SELECTED_LOCATION";
        public const string SELECTED_DEPARTMENT = "SELECTED_DEPARTMENT";
        public const string SELECTED_TABLE = "SELECTED_TABLE";
        public const string SCAN_QR_CODE = "SCAN_QR_CODE";
        public const string POSTAL_CODE_ONEMAP = "POSTAL_CODE_ONEMAP";
        public const string DATE_TIME_DELIVERY = "DATE_TIME_DELIVERY";
        public const string TIME_SLOT_DELEVERY = "TIME_SLOT_DELEVERY";
        public const string LANGUAGE_DISPLAY = "LANGUAGE_DISPLAY";
    }
}

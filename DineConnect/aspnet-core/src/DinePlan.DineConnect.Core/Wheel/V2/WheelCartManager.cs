﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Filter;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelCartManager : DineConnectDomainServiceBase
    {
        private readonly IRepository<WheelCartItem> _cartItemRepository;

        public WheelCartManager(
            IRepository<WheelCartItem> cartItemRepository)
        {
            _cartItemRepository = cartItemRepository;
        }

        public async Task<List<WheelCartItem>> GetCartItems(Customer customer)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                var cartItems = await _cartItemRepository
                 .GetAll()
                 .Include(t => t.ScreenMenuItem).ThenInclude(x => x.MenuItem).ThenInclude(x=>x.Portions)
                 .Include(t => t.ScreenMenuItem).ThenInclude(x => x.ProductCombo).ThenInclude(x => x.MenuItem).ThenInclude(x=>x.Portions)
                 .Where(t => t.CustomerId == customer.Id)
                 .ToListAsync();

                return cartItems;
            }
        }
      
        public async Task ClearCartItems(Customer customer)
        {
            using (CurrentUnitOfWork.DisableFilter(DineConnectDataFilters.MustHaveOrganization))
            {
                await _cartItemRepository.DeleteAsync(t => t.CustomerId == customer.Id);
            }
        }

        public async Task<decimal> GetCartSubTotal(Customer customer)
        {
            var carItems = await GetCartItems(customer);
            var subTotal = 0m;

            carItems.ForEach(t =>
            {
                subTotal += t.Quantity * (GetUnitPrice(t).Item1 ?? 0);
            });

            return subTotal;
        }

        public (decimal?, decimal?) GetUnitPrice(WheelCartItem item)
        {
            decimal? price = 0;
            decimal? lineItemPrice = 0;

            if (!string.IsNullOrEmpty(item.Portion))
            {
                var portion = JsonConvert.DeserializeObject<WheelCartItemPortionDto>(item.Portion);
                price = portion.Price ?? 0;
                lineItemPrice = portion.Price ?? 0;

            }
            else
            {
                price = item.Price;
                lineItemPrice = item.Price;

            }

            if (!string.IsNullOrEmpty(item.Tags) && !item.Tags.Equals("[]"))
            {
                var tags = JsonConvert.DeserializeObject<List<WheelCartItemOrderTagDto>>(item.Tags);
                price += tags.Sum(t => (t.Price ?? 0) * (t.Quantity ?? 1));

            }

            if (item.ChildItems != null)
            {
                if (!item.ChildItems.ToUpper().Equals("NULL"))
                {
                    var allSubItems = JsonConvert.DeserializeObject<List<WheelCartItem>>(item.ChildItems);
                    if (allSubItems.Any())
                    {
                        foreach (var sub in allSubItems)
                        {
                            price += GetUnitPrice(sub).Item1 * (sub.Quantity);
                        }
                    }
                }
            }

            return (price, lineItemPrice);
        }
    }
}

﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelCartItem : ConnectFullMultiTenantAuditEntity
    {
        public WheelCartItem()
        {
        }
        public string Key { get; set; }
        public int? MenuItemId { get; set; }
        public string Portion { get; set; }
        public string ItemName { get; set; }
        public int? ScreenMenuItemId { get; set; }

        public int Quantity { get; set; }
        public string Tags { get; set; }
        public decimal Price { get; set; }
        public string ChildItems { get; set; }
        public int? CustomerId { get; set; }
        public string Note { get; set; }

        
        [ForeignKey("ScreenMenuItemId")]
        public ScreenMenuItem ScreenMenuItem { get; set; }

        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }

    }
}

﻿using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelOrderItem : ConnectFullMultiTenantAuditEntity
    {
        public WheelOrderItem()
        {
            SubItems = new List<WheelOrderItem>();
        }

        public int ScreenMenuItemId { get; set; }

        [ForeignKey("ScreenMenuItemId")]
        public ScreenMenuItem ScreenMenuItem { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public string Tags { get; set; }

        public string Portion { get; set; }

        public int? MainOrderItemId { get; set; }

        [ForeignKey("MainOrderItemId")]
        public WheelOrderItem MainOrderItem { get; set; }

        public List<WheelOrderItem> SubItems { get; set; }

        public string Note { get; set; }
    }
}

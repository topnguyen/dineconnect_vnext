﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Wheel.V2
{
    public class WheelCustomerSessionData : Entity, IMustHaveTenant
    {
        public string Key { get; set; }

        public int CustomerId { get; set; }

        public string Value { get; set; }

        public int TenantId { get; set; }

        public DateTime? CreatedOn { get; set; }
    }
}

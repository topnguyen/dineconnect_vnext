using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Wheel.Promotion;

namespace DinePlan.DineConnect.Tiffins.Promotion
{
    [Table("WheelPromotion")]
    public class WheelPromotion : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public PromotionType Type { get; set; }

        public double VoucherValue { get; set; }

        public DateTime Validity { get; set; }

        public int MinimumOrderAmount { get; set; }

        public bool AllowMultipleRedemption { get; set; }

        public int MultiRedemptionCount { get; set; }

        public bool IsAutoAttach { get; set; }
        
        public string GeneratePrefix { get; set; }
        
        public string GenerateSuffix { get; set; }

        public int GenerateTotalVouchers { get; set; }

        public int GenerateTotalDigits { get; set; }

        public virtual ICollection<WheelPromotionCode> Codes { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Tiffins.Promotion;

namespace DinePlan.DineConnect.Wheel.Promotion
{
    [Table("WheelPromotionCode")]
    public class WheelPromotionCode : ConnectFullMultiTenantAuditEntity
    {
        public string Code { get; set; }

        public int PromotionId { get; set; }

        public virtual WheelPromotion Promotion { get; set; }

        public bool IsClaimed { get; set; }

        public DateTime? ClaimedTime { get; set; }

        public double ClaimedAmount { get; set; }

        public int? CustomerId { get; set; }

        public virtual BaseCore.Customer.Customer Customer { get; set; }

        public string Remarks { get; set; }

        public int? WheelPaymentId { get; set; }

        public WheelPayment WheelPayment { get; set; }
    }
}
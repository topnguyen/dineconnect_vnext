﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Tiffins.MealTimes;

namespace DinePlan.DineConnect.Tiffins.DeliveryTimeSlot
{
    public class TiffinDeliveryTimeSlot : ConnectFullMultiTenantAuditEntity
    {
        public DayOfWeek? DayOfWeek { get; set; }

        public int MealTimeId { get; set; }

        [ForeignKey("MealTimeId")]
        public virtual TiffinMealTime MealTime { get; set; }

        public TiffinDeliveryTimeSlotType Type { get; set; }

        public int? LocationId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public string TimeSlots { get; set; }
    }
}

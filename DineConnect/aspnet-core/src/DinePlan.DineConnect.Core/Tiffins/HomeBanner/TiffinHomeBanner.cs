using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.HomeBanner
{
    [Table("TiffinHomeBanner")]
    public class TiffinHomeBanner : ConnectFullMultiTenantAuditEntity
    {
        public string Images { get; set; }
    }
}
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.Faq
{
    [Table("TiffinFaq")]
    public class TiffinFaq : ConnectFullMultiTenantAuditEntity
    {
        public string Questions { get; set; }
        public string Answer { get; set; }

        public bool IsDisplayAtHomePage { get; set; }
        public double DisplayOrder { get; set; }
    }
}
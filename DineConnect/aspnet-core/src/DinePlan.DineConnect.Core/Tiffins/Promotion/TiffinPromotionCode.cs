using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Wheel;

namespace DinePlan.DineConnect.Tiffins.Promotion
{
    [Table("TiffinPromotionCode")]
    public class TiffinPromotionCode : ConnectFullMultiTenantAuditEntity
    {
        public string Code { get; set; }

        public int PromotionId { get; set; }

        public virtual TiffinPromotion Promotion { get; set; }
        
        public bool IsClaimed { get; set; }

        public DateTime? ClaimedTime { get; set; }

        public double ClaimedAmount { get; set; }

        public int? PaymentId { get; set; }

        public virtual TiffinPayment Payment { get; set; }
        
        public int? CustomerId { get; set; }

        public virtual BaseCore.Customer.Customer Customer { get; set; }

        public string Remarks { get; set; }

        public int? WheelPaymentId { get; set; }

        public WheelPayment WheelPayment { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.Promotion
{
    [Table("TiffinPromotion")]
    public class TiffinPromotion : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public TiffinPromotionType Type { get; set; }

        public double VoucherValue { get; set; }

        // Condition
        
        public DateTime Validity { get; set; }

        public int MinimumOrderAmount { get; set; }

        public bool AllowMultipleRedemption { get; set; }

        public int MultiRedemptionCount { get; set; }

        // Auto Attach
        
        public bool IsAutoAttach { get; set; }
        
        // Generation
        
        public string GeneratePrefix { get; set; }
        
        public string GenerateSuffix { get; set; }

        public int GenerateTotalVouchers { get; set; }

        public int GenerateTotalDigits { get; set; }

        public virtual ICollection<TiffinPromotionCode> Codes { get; set; }
    }
}
﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Payment;

namespace DinePlan.DineConnect.Tiffins.Payment.Omise
{
    public interface IOmisePaymentGatewayManager
    {
        Task<CreateOmisePaymentOutput> PaymentStatus(string paymentRequestPaymentReference);
    }
}

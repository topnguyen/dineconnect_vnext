﻿using Abp;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Tiffins.Payment.Omise;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public class OmiseConfiguration : IOmiseConfiguration, ITransientDependency
    {
        protected readonly ISettingManager SettingManager;
        private readonly IRepository<TenantAddon> _tenantAddonRepository;

        public OmiseConfiguration(ISettingManager settingManager, IRepository<TenantAddon> tenantAddonRepository)
        {
            SettingManager = settingManager;
            _tenantAddonRepository = tenantAddonRepository;
        }

        public virtual string BaseUrl
        {
            get
            {
                var value = GetNotEmptySettingValue().BaseUrl;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for Base Url is null or empty!");
                }

                return value;
            }
        }

        public virtual string PublishableKey
        {
            get
            {
                var value = GetNotEmptySettingValue().PublishableKey;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for Publishable Key is null or empty!");
                }

                return value;
            }
        }

        public virtual string SecretKey
        {
            get
            {
                var value = GetNotEmptySettingValue().SecretKey;
                if (value.IsNullOrEmpty())
                {
                    throw new AbpException($"Setting value for Secret Key is null or empty!");
                }

                return value;
            }
        }

        public virtual List<string> PaymentMethodTypes
        {
            get { return new List<string>((GetNotEmptySettingValue().PaymentMethodTypes ?? "").Split(",", StringSplitOptions.RemoveEmptyEntries)); }
        }

        protected PaymentGatewaySettings GetNotEmptySettingValue()
        {
            var addon = _tenantAddonRepository.GetAll()
                            .Where(e => e.Addon.AddonType == AddonType.Payment)
                            .Select(e => e.Addon)
                            .FirstOrDefault(e => e.Name == "Omise");

            if (addon == null || addon.Settings.IsNullOrEmpty())
            {
                throw new AbpException($"Setting value for Omise payment method is null or empty!");
            }

            var setting = JsonConvert.DeserializeObject<PaymentGatewaySettings>(addon.Settings);

            return setting;
        }
    }
}
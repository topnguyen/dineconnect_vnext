﻿using Abp.Dependency;
using Omise;
using Omise.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Payment;
using DinePlan.DineConnect.Tiffins.Payment.Omise;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public class OmisePaymentGatewayManager : IOmisePaymentGatewayManager, ITransientDependency
    {
        private readonly IOmiseConfiguration _configuration;

        private Client client;

        protected Client Client
        {
            get
            {
                if (client == null) client = new Client(_configuration.PublishableKey, _configuration.SecretKey);

                return client;
            }
        }

        public OmisePaymentGatewayManager(IOmiseConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<CreateOmisePaymentOutput> PaymentStatus(string paymentRequestPaymentReference)
        {
            CreateOmisePaymentOutput returnOutput = new CreateOmisePaymentOutput();
            returnOutput.OmiseId = paymentRequestPaymentReference;
            returnOutput.PurchasedSuccess = false;

            var charge = await Client.Charges.Get(paymentRequestPaymentReference);
            if (charge == null)
            {
                returnOutput.Message = "Charge is not available ";
                return returnOutput;
            }
            if (charge.Authorized && charge.Paid)
            {
                returnOutput.PurchasedSuccess = true;
            }
            returnOutput.Message = charge.FailureMessage;
            return returnOutput;
        }
    }
}
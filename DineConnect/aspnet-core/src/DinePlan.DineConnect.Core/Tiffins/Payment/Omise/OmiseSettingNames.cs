﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public static class OmiseSettingNames
    {
        public const string BaseUrl = "App.Omise.BaseUrl";

        public const string PublishableKey = "App.Omise.PublishableKey";

        public const string SecretKey = "App.Omise.SecretKey";

        public const string PaymentMethodTypes = "App.Omise.PaymentMethodTypes";
    }
}

﻿namespace DinePlan.DineConnect.Tiffins.Payment.Omise
{
    public class PaymentGatewaySettings
    {
        public string BaseUrl { get; set; }

        public string PublishableKey { get; set; }

        public string SecretKey { get; set; }

        public string PaymentMethodTypes { get; set; }
    }
}
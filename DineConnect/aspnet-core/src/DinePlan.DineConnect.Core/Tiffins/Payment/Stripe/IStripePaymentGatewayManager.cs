﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public interface IStripePaymentGatewayManager
    {
        Task<bool> CreateCharge(string omiseToken, long amount, string currency);

        Task<bool> CreateChargeWithCustomer(string token, string customerEmail, string customerName, long amount, string currency);
    }
}

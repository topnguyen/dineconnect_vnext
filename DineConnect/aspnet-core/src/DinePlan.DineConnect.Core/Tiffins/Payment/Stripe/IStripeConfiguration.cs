﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public interface IStripeConfiguration
    {
        string BaseUrl { get; }

        string PublishableKey { get; }

        string SecretKey { get; }

        List<string> PaymentMethodTypes { get; }
    }
}

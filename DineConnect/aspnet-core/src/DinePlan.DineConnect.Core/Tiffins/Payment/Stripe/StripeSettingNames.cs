﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public static class StripeSettingNames
    {
        public const string BaseUrl = "App.Stripe.BaseUrl";
        public const string PublishableKey = "App.Stripe.PublishableKey";
        public const string SecretKey = "App.Stripe.SecretKey";
    }
}

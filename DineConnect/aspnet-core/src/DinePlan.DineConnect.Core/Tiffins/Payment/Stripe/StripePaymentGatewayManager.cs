﻿using Abp.Dependency;
using Omise;
using Omise.Models;
using Stripe;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Tiffins.Payment
{
    public class StripePaymentGatewayManager : IStripePaymentGatewayManager, ITransientDependency
    {
        private readonly IStripeConfiguration _configuration;

        private Client client;

        protected Client Client
        {
            get
            {
                if (client == null)
                {
                    client = new Client(_configuration.SecretKey);
                }

                return client;
            }
        }

        public StripePaymentGatewayManager(IStripeConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> CreateCharge(string token, long amount, string currency)
        {
            var options = new ChargeCreateOptions
            {
                Amount = amount,
                Currency = currency,
                Source = token,
            };

            var service = new ChargeService(new StripeClient(_configuration.SecretKey));

            var charge = await service.CreateAsync(options);

            return charge.Status == "succeeded";
        }

        public async Task<bool> CreateChargeWithCustomer(string token, string customerEmail, string customerName, long amount, string currency)
        {
            return await CreateCharge(token, amount, currency);
        }
    }
}

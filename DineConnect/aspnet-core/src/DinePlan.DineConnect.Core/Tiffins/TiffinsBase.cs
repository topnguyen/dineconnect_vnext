﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.BaseCore.Customer;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Menu.MenuItems;
using DinePlan.DineConnect.Tiffins.DeliveryTimeSlot;
using DinePlan.DineConnect.Tiffins.Invoice;
using DinePlan.DineConnect.Tiffins.MealTimes;
using DinePlan.DineConnect.Tiffins.Schedule;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DinePlan.DineConnect.Tiffins
{
    [Table("TiffinProductSets")]
    public class TiffinProductSet : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Images { get; set; }

        public string Tag { get; set; }

        public string Color { get; set; }

        public int Ordinal { get; set; }

        public virtual ICollection<TiffinProductOfferSet> ProductOfferSets { get; set; }

        [NotMapped]
        public virtual IEnumerable<TiffinProductOffer> ProductOffers => ProductOfferSets.Select(e => e.TiffinProductOffer);

        public virtual List<TiffinProductSetProduct> Products { get; set; }

        public TiffinProductSet()
        {
            Products = new List<TiffinProductSetProduct>();
            ProductOfferSets = new HashSet<TiffinProductOfferSet>();
        }
    }

    [Table("TiffinProductSetProducts")]
    public class TiffinProductSetProduct : ConnectFullMultiTenantAuditEntity
    {
        public int ProductSetId { get; set; }

        [ForeignKey("ProductSetId")]
        public TiffinProductSet ProductSet { get; set; }

        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public MenuItem Product { get; set; }
    }

    [Table("TiffinProductOfferTypes")]
    public class TiffinProductOfferType : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
    }

    [Table("TiffinProductOffers")]
    public class TiffinProductOffer : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Images { get; set; }
        public decimal Price { get; set; }
        public string Tag { get; set; }

        public int MinimumOrderQuantity { get; set; }

        [ForeignKey("TiffinProductOfferTypeId")]
        public TiffinProductOfferType TiffinProductOfferType { get; set; }

        public int TiffinProductOfferTypeId { get; set; }

        public int TotalDeliveryCount { get; set; }
        public bool Expiry { get; set; }
        public int DaysOnFirstOrder { get; set; }
        public string Color { get; set; }
        public virtual ICollection<TiffinProductOfferSet> ProductOfferSets { get; set; }
        public int SortOrder { get; set; }
        public bool InActive { get; set; }

        [NotMapped]
        public virtual IEnumerable<TiffinProductSet> ProductSets => ProductOfferSets.Select(e => e.TiffinProductSet);

        public TiffinProductOffer()
        {
            ProductOfferSets = new HashSet<TiffinProductOfferSet>();
        }
    }

    [Table("TiffinProductOfferSets")]
    public class TiffinProductOfferSet : FullAuditedEntity
    {
        public virtual int TiffinProductOfferId { get; set; }

        [ForeignKey("TiffinProductOfferId")]
        public virtual TiffinProductOffer TiffinProductOffer { get; set; }

        public virtual int TiffinProductSetId { get; set; }

        [ForeignKey("TiffinProductSetId")]
        public virtual TiffinProductSet TiffinProductSet { get; set; }
    }

    [Table("TiffinSchedules")]
    public class TiffinSchedule : ConnectFullMultiTenantAuditEntity
    {
        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public virtual int MealTimeId { get; set; }

        [ForeignKey("MealTimeId")]
        public virtual TiffinMealTime MealTime { get; set; }

        public List<TiffinScheduleDetail> ScheduleDetails { get; set; }
    }

    [Table("TiffinScheduleDetails")]
    public class TiffinScheduleDetail : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("ScheduleId")]
        public TiffinSchedule Schedule { get; set; }

        public int ScheduleId { get; set; }

        [ForeignKey("ProductSetId")]
        public TiffinProductSet ProductSet { get; set; }

        public int ProductSetId { get; set; }

        public virtual List<TiffinScheduleDetailProduct> Products { get; set; }

        public ScheduleDetailStatus Status { get; set; } = ScheduleDetailStatus.Draft;
    }

    [Table("TiffinScheduleDetailProducts")]
    public class TiffinScheduleDetailProduct : ConnectFullMultiTenantAuditEntity
    {
        public int ScheduleDetailId { get; set; }

        [ForeignKey("ScheduleDetailId")]
        public TiffinScheduleDetail ScheduleDetail { get; set; }

        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public MenuItem Product { get; set; }
    }

    [Table("TiffinCustomerProductOffers")]
    public class TiffinCustomerProductOffer : ConnectFullMultiTenantAuditEntity
    {
        public int ProductOfferId { get; set; }

        [ForeignKey("ProductOfferId")]
        public TiffinProductOffer ProductOffer { get; set; }

        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public BaseCore.Customer.Customer Customer { get; set; }

        public int? ActualCredit { get; set; }

        public decimal? PaidAmount { get; set; }

        public int? BalanceCredit { get; set; }

        public int TiffinPaymentId { get; set; }

        [ForeignKey("TiffinPaymentId")]
        public TiffinPayment Payment { get; set; }

        public DateTime ExpiryDate { get; set; }
        public virtual List<TiffinCustomerProductOfferOrder> Orders { get; set; }
    }

    [Table("TiffinCustomerProductOfferOrders")]
    public class TiffinCustomerProductOfferOrder : ConnectFullAuditEntity
    {
        public int CustomerProductOfferId { get; set; }

        [ForeignKey("CustomerProductOfferId")]
        public virtual TiffinCustomerProductOffer CustomerProductOffer { get; set; }

        public int ProductSetId { get; set; }

        [ForeignKey("ProductSetId")]
        public virtual TiffinProductSet ProductSet { get; set; }

        [Column(TypeName = "date")]
        public DateTime OrderDate { get; set; }

        public int? Amount { get; set; }

        public int Quantity { get; set; } = 1;

        public string Address { get; set; }

        public int MealTimeId { get; set; }

        [ForeignKey("MealTimeId")]
        public TiffinMealTime TiffinMealTime { get; set; }

        public string AddOn { get; set; }

        public int? CustomerAddressId { get; set; }

        [ForeignKey("CustomerAddressId")]
        public CustomerAddressDetail CustomerAddress { get; set; }

        public int? PaymentId { get; set; }

        [ForeignKey("PaymentId")]
        public virtual TiffinPayment Payment { get; set; }

        public string MenuItemsDynamic { get; set; }

        public TiffinDeliveryTimeSlotType DeliveryType { get; set; }

        public long DeliveryCharge { get; set; }

        public string TimeSlotFormTo { get; set; }

        public int? SelfPickupLocationId { get; set; }

        [ForeignKey("SelfPickupLocationId")]
        public virtual Location SelfPickupLocation { get; set; }

        public DateTime? PickedUpTime { get; set; }
    }
}
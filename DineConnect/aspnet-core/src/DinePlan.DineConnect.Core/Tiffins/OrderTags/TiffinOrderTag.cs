﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.OrderTags
{
    [Table("TiffinOrderTags")]
    public class TiffinOrderTag : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }

        public virtual decimal Price { get; set; }
        public string Images { get; set; }
        public int Ordinal { get; set; }

    }
}
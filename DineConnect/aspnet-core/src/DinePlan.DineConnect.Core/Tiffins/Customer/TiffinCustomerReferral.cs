using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Settings;

namespace DinePlan.DineConnect.Tiffins.Customer
{
    [Table("TiffinCustomerReferrals")]
    public class TiffinCustomerReferral : ConnectFullMultiTenantAuditEntity
    {
        public int CustomerId { get; set; }
        
        [ForeignKey("CustomerId")]
        public virtual BaseCore.Customer.Customer Customer { get; set; }
        
        // Referral Code
        
        public string ReferralCode { get; set; }

        // Referrer Information
        
        public int? ReferrerCustomerId { get; set; }
        
        [ForeignKey("ReferrerCustomerId")]
        public virtual BaseCore.Customer.Customer ReferrerCustomer { get; set; }
        
        public ReferralEarningType ReferrerCustomerEarnedType { get; set; }

        public int ReferrerCustomerEarnedValue { get; set; }

        public DateTime? ReferrerCustomerEarnedTime { get; set; }
        
        public int? ReferrerCustomerEarnedByPaymentId { get; set; }

        public string ReferrerCustomerEarnedRemarks { get; set; }
    }
}
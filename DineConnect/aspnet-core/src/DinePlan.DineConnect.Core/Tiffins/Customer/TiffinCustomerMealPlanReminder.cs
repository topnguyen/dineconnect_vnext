﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.Customer
{
    [Table("TiffinCustomerMealPlanReminders")]
    public class TiffinCustomerMealPlanReminder : ConnectFullMultiTenantAuditEntity
    {
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual BaseCore.Customer.Customer Customer { get; set; }

        public int BuyMealPlanReminderTimes { get; set; }

        public DateTime LastTimeSentBuyMealPlanReminder { get; set; }
    }
}

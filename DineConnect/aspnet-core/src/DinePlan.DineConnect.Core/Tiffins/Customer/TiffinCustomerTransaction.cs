﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.Customer
{
    [Table("TiffinCustomerTransactions")]
    public class TiffinCustomerTransaction : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Reason { get; set; }

        public virtual string ChangeDetails { get; set; }

        public virtual int CustomerProductOfferId { get; set; }

        [ForeignKey("CustomerProductOfferId")]
        public TiffinCustomerProductOffer CustomerProductOffer { get; set; }
    }
}
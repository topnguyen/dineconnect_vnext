﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Locations;

namespace DinePlan.DineConnect.Tiffins.MealTimes
{
    [Table("TiffinMealTimes")]
    public class TiffinMealTime : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string Code { get; set; }

        public bool IsDefault { get; set; }
    }

    public class TiffinMealTimeLocation : ConnectFullAuditEntity
    {

        public Location Location { get; set; }
        public int LocationId  { get; set; }
        public List<TiffinDeliveryZone> TiffinDeliveryZones { get; set; }
        public string DeliverySlots { get; set; }

    }

    public class TiffinDeliveryZone :ConnectFullMultiTenantAuditEntity
    {

        public decimal MinimumTotalAmount { get; set; }        
        public decimal DeliveryFee { get; set; }
        public DeliveryZoneType ZoneType { get; set; }
        public string Color { get; set; }
        public string ZoneCoordinates { get; set; }
    }


  
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Tiffins.Invoice
{
    [Table("TiffinPayments")]
    public class TiffinPayment : ConnectFullAuditEntity
    {
        public TiffinPaymentType PaymentType { get; set; }
        
        public string InvoiceNo { get; set; }
        
        public DateTime PaymentDate { get; set; }

        public decimal? PaidAmount { get; set; }

        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public BaseCore.Customer.Customer Customer { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerTaxNo { get; set; }

        public string PaymentOption { get; set; }

        public virtual List<TiffinCustomerProductOffer> CustomerProductOffers { get; set; }
    }

    [Table("TiffinPaymentRequests")]
    public class TiffinPaymentRequest : ConnectFullAuditEntity
    {
        public DateTime PaymentDate { get; set; }

        public decimal PaidAmount { get; set; }
      
        public string PaymentOption { get; set; }

        public string Request { get; set; }
        public string PaymentReference { get; set; }

    }

}
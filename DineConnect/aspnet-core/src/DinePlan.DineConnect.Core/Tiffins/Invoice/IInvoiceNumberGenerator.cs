﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace DinePlan.DineConnect.Tiffins.Invoice
{
    public interface ITiffinInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}
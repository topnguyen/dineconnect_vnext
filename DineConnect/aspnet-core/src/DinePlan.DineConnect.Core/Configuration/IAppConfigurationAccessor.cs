﻿using Microsoft.Extensions.Configuration;

namespace DinePlan.DineConnect.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}

namespace DinePlan.DineConnect.Configuration
{
    /// <summary>
    /// Defines string constants for setting names in the application.
    /// See <see cref="AppSettingProvider"/> for setting definitions.
    /// </summary>
    public static class AppSettings
    {
        public static class DashboardCustomization
        {
            public const string Configuration = "App.DashboardCustomization.Configuration";
        }

        public static class HostManagement
        {
            public const string BillingLegalName = "App.HostManagement.BillingLegalName";
            public const string BillingAddress = "App.HostManagement.BillingAddress";
        }

        public static class UiManagement
        {
            public const string LayoutType = "App.UiManagement.LayoutType";
            public const string FixedBody = "App.UiManagement.Layout.FixedBody";
            public const string MobileFixedBody = "App.UiManagement.Layout.MobileFixedBody";
            public const string Theme = "App.UiManagement.Theme";
            public const string SearchActive = "App.UiManagement.MenuSearch";

            public static class Header
            {
                public const string DesktopFixedHeader = "App.UiManagement.Header.DesktopFixedHeader";
                public const string MobileFixedHeader = "App.UiManagement.Header.MobileFixedHeader";
                public const string Skin = "App.UiManagement.Header.Skin";
                public const string MinimizeType = "App.UiManagement.Header.MinimizeType";
                public const string MenuArrows = "App.UiManagement.Header.MenuArrows";
                public const string Color = "App.UiManagement.Header.Color";
            }

            public static class SubHeader
            {
                public const string Fixed = "App.UiManagement.SubHeader.Fixed";
                public const string Style = "App.UiManagement.SubHeader.Style";
            }

            public static class LeftAside
            {
                public const string Position = "App.UiManagement.Left.Position";
                public const string AsideSkin = "App.UiManagement.Left.AsideSkin";
                public const string FixedAside = "App.UiManagement.Left.FixedAside";
                public const string AllowAsideMinimizing = "App.UiManagement.Left.AllowAsideMinimizing";
                public const string DefaultMinimizedAside = "App.UiManagement.Left.DefaultMinimizedAside";
                public const string SubmenuToggle = "App.UiManagement.Left.SubmenuToggle";
                public const string Color = "App.UiManagement.Left.Color";
            }

            public static class Footer
            {
                public const string FixedFooter = "App.UiManagement.Footer.FixedFooter";
                public const string Color = "App.UiManagement.Footer.Color";
            }

            public static class Content
            {
                public const string Color = "App.UiManagement.Content.Color";
            }
        }

        public static class TenantManagement
        {
            public const string AllowSelfRegistration = "App.TenantManagement.AllowSelfRegistration";

            public const string IsNewRegisteredTenantActiveByDefault =
                "App.TenantManagement.IsNewRegisteredTenantActiveByDefault";

            public const string UseCaptchaOnRegistration = "App.TenantManagement.UseCaptchaOnRegistration";
            public const string DefaultEdition = "App.TenantManagement.DefaultEdition";

            public const string SubscriptionExpireNotifyDayCount =
                "App.TenantManagement.SubscriptionExpireNotifyDayCount";

            public const string BillingLegalName = "App.TenantManagement.BillingLegalName";
            public const string BillingAddress = "App.TenantManagement.BillingAddress";
            public const string BillingTaxVatNo = "App.TenantManagement.BillingTaxVatNo";
        }

        public static class UserManagement
        {
            public static class TwoFactorLogin
            {
                public const string IsGoogleAuthenticatorEnabled =
                    "App.UserManagement.TwoFactorLogin.IsGoogleAuthenticatorEnabled";
            }

            public static class SessionTimeOut
            {
                public const string IsEnabled = "App.UserManagement.SessionTimeOut.IsEnabled";
                public const string TimeOutSecond = "App.UserManagement.SessionTimeOut.TimeOutSecond";

                public const string ShowTimeOutNotificationSecond =
                    "App.UserManagement.SessionTimeOut.ShowTimeOutNotificationSecond";

                public const string ShowLockScreenWhenTimedOut = "App.UserManagement.SessionTimeOut.ShowLockScreenWhenTimedOut";
            }

            public const string AllowSelfRegistration = "App.UserManagement.AllowSelfRegistration";

            public const string IsNewRegisteredUserActiveByDefault =
                "App.UserManagement.IsNewRegisteredUserActiveByDefault";

            public const string UseCaptchaOnRegistration = "App.UserManagement.UseCaptchaOnRegistration";
            public const string UseCaptchaOnLogin = "App.UserManagement.UseCaptchaOnLogin";
            public const string SmsVerificationEnabled = "App.UserManagement.SmsVerificationEnabled";
            public const string IsCookieConsentEnabled = "App.UserManagement.IsCookieConsentEnabled";
            public const string IsQuickThemeSelectEnabled = "App.UserManagement.IsQuickThemeSelectEnabled";
            public const string AllowOneConcurrentLoginPerUser = "App.UserManagement.AllowOneConcurrentLoginPerUser";
            public const string AllowUsingGravatarProfilePicture = "App.UserManagement.AllowUsingGravatarProfilePicture";
            public const string UseGravatarProfilePicture = "App.UserManagement.UseGravatarProfilePicture";
        }

        public static class Email
        {
            public const string UseHostDefaultEmailSettings = "App.Email.UseHostDefaultEmailSettings";
            public const string SendGridApiKey = "App.Email.SendGridApiKey";
        }

        public static class Recaptcha
        {
            public const string SiteKey = "Recaptcha.SiteKey";
        }

        public static class ExternalLoginProvider
        {
            public const string OpenIdConnectMappedClaims = "ExternalLoginProvider.OpenIdConnect.MappedClaims";
            public const string WsFederationMappedClaims = "ExternalLoginProvider.WsFederation.MappedClaims";

            public static class Host
            {
                public const string Facebook = "ExternalLoginProvider.Facebook";
                public const string Google = "ExternalLoginProvider.Google";
                public const string Twitter = "ExternalLoginProvider.Twitter";
                public const string Microsoft = "ExternalLoginProvider.Microsoft";
                public const string OpenIdConnect = "ExternalLoginProvider.OpenIdConnect";
                public const string WsFederation = "ExternalLoginProvider.WsFederation";
            }

            public static class Tenant
            {
                public const string Twitter = "ExternalLoginProvider.Twitter.Tenant";
                public const string Microsoft = "ExternalLoginProvider.Microsoft.Tenant";
                public const string OpenIdConnect = "ExternalLoginProvider.OpenIdConnect.Tenant";
                public const string WsFederation = "ExternalLoginProvider.WsFederation.Tenant";

                public static class Google
                {
                    public const string ClientId = "ExternalLoginProvider.Google.Tenant.ClientId";
                    public const string ClientSecret = "ExternalLoginProvider.Google.Tenant.ClientSecret";
                    public const string UserInfoEndpoint = "ExternalLoginProvider.Google.Tenant.UserInfoEndpoint";
                }

                public static class Facebook
                {
                    public const string AppId = "ExternalLoginProvider.Facebook.Tenant.AppId";
                    public const string AppSecret = "ExternalLoginProvider.Facebook.Tenant.AppSecret";
                }
            }
        }

        public static class CloudinarySetting
        {
            public const string CloudName = "App.Cloudinary.CloudName";
            public const string ApiKey = "App.Cloudinary.ApiKey";
            public const string ApiSecret = "App.Cloudinary.ApiSecret";
            public const string EnvironmentVariable = "App.Cloudinary.EnvironmentVariable";
        }

        public static class SignUpFormSetting
        {
            public const string HintImageUrl = "App.SignUp.HintImageUrl";
        }

        #region Connect

        public static class ConnectSettings
        {
            public const string WeekDay = "App.Connect.WeekDay";
            public const string WeekEnd = "App.Connect.WeekEnd";
            public const string Schedules = "App.Connect.Schedules";

            public const string SyncUrl = "App.General.SyncUrl";
            public const string SyncTenantId = "App.General.SyncTenantId";
            public const string SyncTenantName = "App.General.SyncTenantName";
            public const string SyncUser = "App.General.SyncUser";
            public const string SyncPassword = "App.General.SyncPassword";

        }

        #endregion Connect

        #region Touch

        public static class TouchSaleSettings
        {
            public const string Currency = "App.Connect.Compact.Currency";
            public const string Decimals = "App.Connect.Compact.Decimals";
            public const string Rounding = "App.Connect.Compact.Rounding";
        }

        #endregion Touch

        #region Tiffins

        public static class CompanySetting
        {
            public const string CompanyName = "App.CompanySetting.CompanyName";
            public const string CompanyLogoUrl = "App.CompanySetting.CompanyLogoUrl";
            public const string CompanyLogoUrlForCustomer = "App.CompanySetting.CompanyLogoUrlForCustomer";
        }

        public static class TermAndConditionSetting
        {
            public const string TermAndCondition = "App.TermAndConditionSetting.TermAndCondition";
        }

        public static class MealSetting
        {
            public const string MinMealCount = "App.MealSetting.MinMealCount";
            public const string MinExpiryDay = "App.MealSetting.MinExpiryDay";
            public const string OrderCutOffTime = "App.MealSetting.OrderCutOffTime";
        }

        public static class ThemeSetting
        {
            public const string Color = "App.Theme.Color";
        }

        public static class ReferralEarningSetting
        {
            public const string ReferralEarningType = "App.ReferralEarningSetting.ReferralEarningType";
            public const string ReferralEarningValue = "App.ReferralEarningSetting.ReferralEarningValue";
        }

        public static class FileDateTimeFormatSetting
        {
            public const string FileDateTimeFormat = "App.FileDateTimeFormatSetting.FileDateTimeFormat";
            public const string FileDateFormat = "App.FileDateTimeFormatSetting.FileDateFormat";
            public const string FileDayMonthFormat = "App.FileDateTimeFormatSetting.FileDayMonthFormat";
        }

        public static class ShippingProviderSetting
        {
            public const string ShippingProvider = "App.ShippingProviderSetting.ShippingProvider";
            public const string ShippingSetting = "App.ShippingProviderSetting.ShippingProviderSetting";
            public const string ShippingGoGoVan = "App.ShippingProviderSetting.ShippingGoGoVan";
            public const string ShippingLalaMove = "App.ShippingProviderSetting.ShippingLalaMove";
            public const string ShippingDunzo = "App.ShippingProviderSetting.ShippingDunzo";
        }

        public const string TiffinPopup = "App.TiffinPopup";

        public const string TiffinAllowOrderInFutureDays = "App.TiffinAllowOrderInFutureDays";

        public const string TiffinCutOffDays = "App.TiffinCutOffDays";

        public const string TiffinIsEnableDelivery = "App.TiffinIsEnableDelivery";

        public const string IsTiffinEnableStudentInformation = "App.IsTiffinEnableStudentInformation";

        public const string AdminWhatappNumber = "App.AdminWhatappNumber";
        
        public const string WhatappAccountSid = "App.WhatappAccountSid";
        
        public const string WhatappAuthToken = "App.WhatappAuthToken";
        
        public const string WhatappSenderNumber = "App.WhatappSenderNumber";

        #endregion Tiffins

        #region Clique

        public const string CliqueCutOffDays = "App.CliqueCutOffDays";
        public static class CliqueMealSetting
        {
            public const string CliqueMinMealCount = "App.CliqueMealSetting.MinMealCount";
            public const string CliqueMinExpiryDay = "App.CliqueMealSetting.MinExpiryDay";
            public const string CliqueOrderCutOffTime = "App.CliqueMealSetting.OrderCutOffTime";
        }
        public const string CliqueAllowOrderInFutureDays = "App.CliqueAllowOrderInFutureDays";
        public const string CliqueAddressObligatory = "App.CliqueAddressObligatory";

        #endregion Clique

        #region Wheel

        public static class OrderSetting
        {
            public const string Accepted = "App.Wheel.OrderSetting.Accepted";
            public const string Signup = "App.Wheel.OrderSetting.SignUp";
            public const string Width = "App.Wheel.OrderSetting.Width";
            public const string Height = "App.Wheel.OrderSetting.Height";
            public const string AutoRequestDeliver = "App.Wheel.OrderSetting.AutoRequestDeliver ";
            public const string AskTheMobileNumberFirst = "App.Wheel.OrderSetting.AskTheMobileNumberFirst ";
        }

        #endregion Wheel
    }
}
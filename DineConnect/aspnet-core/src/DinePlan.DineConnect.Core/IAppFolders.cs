﻿namespace DinePlan.DineConnect
{
    public interface IAppFolders
    {
        string SampleProfileImagesFolder { get; }   
        string TempFileDownloadFolder { get; }   
        string WebLogsFolder { get; set; }

    }
}
﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.EntityHistory
{
    public class EntityHistoryUiSetting
    {
        public bool IsEnabled { get; set; }

        public List<string> EnabledEntities { get; set; }
    }
}
﻿using DinePlan.DineConnect.Connect.OrderDetail;
using DinePlan.DineConnect.Connect.Master.DinePlanTaxes;
using DinePlan.DineConnect.Connect.Card.CardTypes;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master.LocationTags;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.PriceTags;
using DinePlan.DineConnect.Connect.Master.TicketTags;
using DinePlan.DineConnect.Connect.Master.FutureDateInformations;
using DinePlan.DineConnect.Connect.Master.Terminals;
using DinePlan.DineConnect.Connect.Master.PrintTemplates;
using DinePlan.DineConnect.Connect.Master.Calculations;
using DinePlan.DineConnect.Connect.Master.TillAccounts;
using Abp.Organizations;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Connect.Master.ForeignCurrencies;
using DinePlan.DineConnect.Connect.Master.PlanReasons;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.MultiTenancy;
using System;
using System.Linq;
using DinePlan.DineConnect.Connect.Tag.PriceTags;
using DinePlan.DineConnect.Connect.Tag.TicketTags;

namespace DinePlan.DineConnect.EntityHistory
{
    public static class EntityHistoryHelper
    {
        public const string EntityHistoryConfigurationName = "EntityHistory";

        public static readonly Type[] HostSideTrackedTypes =
        {
            typeof(OrganizationUnit), typeof(Role), typeof(Tenant)
        };

        public static readonly Type[] TenantSideTrackedTypes =
        {
            typeof(OrderDetail),
            typeof(DinePlanTax),
            typeof(Promotion),
            typeof(LocationTagLocation),
            typeof(LocationTag),
            typeof(Department),
            typeof(PriceTag),
            typeof(TicketTagGroup),
            typeof(FutureDateInformation),
            typeof(Terminal),
            typeof(Calculation),
            typeof(TillAccount),
            typeof(TransactionType),
            typeof(PlanReason),
            typeof(ForeignCurrency),
            typeof(OrganizationUnit),
            typeof(Role)
        };

        public static readonly Type[] TrackedTypes =
            HostSideTrackedTypes
                .Concat(TenantSideTrackedTypes)
                .GroupBy(type => type.FullName)
                .Select(types => types.First())
                .ToArray();
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.BaseCore.TemplateEngines
{
    [Table("TemplateEngines")]
    public class TemplateEngine : ConnectFullMultiTenantAuditEntity
    {
        [Required]
        public virtual string Name { get; set; }

        public virtual string Subject { get; set; }

        public virtual string Feature { get; set; }

        public virtual string Module { get; set; }

        public virtual string Variables { get; set; }

        public virtual string Template { get; set; }

        public virtual string ShortTemplate { get; set; }

        public TemplateEngine(int tenantId, string name, string subject, string module, string variables)
        {
            TenantId = tenantId;
            Name = name;
            Subject = subject;
            Module = module;
            Variables = variables;
        }

        //public virtual string ShortMessage { get; set; }
    }
}
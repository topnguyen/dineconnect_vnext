﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Core.Report
{
    [Table("ReportBackgrounds")]
    public class ReportBackground : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public string ReportDescription { get; set; }
        public string ReportName { get; set; }
        public string ReportOutput { get; set; }
        public bool Completed { get; set; }

    }
}

﻿using Abp.Auditing;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.BaseCore
{
	[Audited]
	public class ConnectOrgLocFullMultiTenantAuditEntity : ConnectLocFullMultiTenantAuditEntity, IMustHaveOrganization
	{
		public int OrganizationId { get; set; }
	}

	[Audited]
	public class ConnectLocFullMultiTenantAuditEntity : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }

		[MaxLength(1000)]
		public virtual string Locations { get; set; }

		[MaxLength(1000)]
		public virtual string NonLocations { get; set; }

		public virtual bool Group { get; set; }
		public virtual bool LocationTag { get; set; }
	}

	[Audited]
	public class ConnectOrgFullMultiTenantAuditEntity : FullAuditedEntity, IMustHaveTenant, IMustHaveOrganization
	{
		public int OrganizationId { get; set; }
		public int TenantId { get; set; }
	}

	[Audited]
	public class ConnectFullMultiTenantAuditEntity : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }
	}

	[Audited]
	public class ConnectFullAuditEntity : FullAuditedEntity
	{
	}

	[Audited]
	public class ConnectCreateAuditEntity : CreationAuditedEntity
	{
	}
}
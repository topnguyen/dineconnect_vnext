﻿using Abp.Extensions;
using DinePlan.DineConnect.Addresses;
using DinePlan.DineConnect.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DinePlan.DineConnect.BaseCore.Customer
{
    [Table("Customers")]
    public class Customer : ConnectFullMultiTenantAuditEntity
    {
        [StringLength(100)] public string Name { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public List<CustomerContactDetail> CustomerContactDetails { get; set; }
        public long? UserId { get; set; }
        public virtual User User { get; set; }
        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }
        public string PhoneNumber { get; set; }
        public string WhatAppNumber { get; set; }

        public string EmailAddress { get; set; }

        public Guid? CustomerGuid { get; set; }

        public bool IsRegistered { get; set; }

        public bool AcceptReceivePromotionEmail { get; set; }

        public virtual ICollection<CustomerAddressDetail> CustomerAddressDetails { get; set; }

        public DateTimeOffset? LastLoginTime { get; set; }

        public bool IsDisableDailyOrderReminder { get; set; }

        public string CartJsonData { get; set; }

        public string JsonData { get; set; }

        [NotMapped]
        public string Address1 =>
                    CustomerAddressDetails?.Where(t => t.IsDefault).Select(c => c.Address1).FirstOrDefault();

        [NotMapped]
        public string Address2 =>
            CustomerAddressDetails?.Where(t => t.IsDefault).Select(c => c.Address2).FirstOrDefault();

        [NotMapped]
        public string Address3 =>
            CustomerAddressDetails?.Where(t => t.IsDefault).Select(c => c.Address3).FirstOrDefault();

        [NotMapped]
        public int? CityId =>
            CustomerAddressDetails.Where(t => t.IsDefault).Select(c => c.CityId).FirstOrDefault();

        [NotMapped]
        public string PostcalCode =>
            CustomerAddressDetails.Where(t => t.IsDefault).Select(c => c.PostcalCode).FirstOrDefault();

        [NotMapped]
        public int? Zone =>
            CustomerAddressDetails.Where(t => t.IsDefault).Select(c => c.Zone).FirstOrDefault();

        [NotMapped]
        public int? Property =>
            CustomerAddressDetails.Where(t => t.IsDefault).Select(c => c.Property).FirstOrDefault();
    }

    [Table("CustomerContactDetails")]
    public class CustomerContactDetail : ConnectFullMultiTenantAuditEntity
    {
        public CustomerContactType CustomerContactType { get; set; }
        [StringLength(150)] public string Detail { get; set; }
    }

    [Table("CustomerAddressDetails")]
    public class CustomerAddressDetail : ConnectFullMultiTenantAuditEntity
    {
        public CustomerAddressType CustomerAddressType { get; set; }
        public bool IsDefault { get; set; }

        public string Name { get; set; }

        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public int? CityId { get; set; }

        public virtual City City { get; set; }

        public string PostcalCode { get; set; }

        public int? Zone { get; set; }

        public string PhoneNumber { get; set; }

        public string WhatAppNumber { get; set; }
        public int? Property { get; set; }

        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [NotMapped]
        public string FullAddress
        {
            get
            {
                if (!Address2.IsNullOrEmpty() && !Address3.IsNullOrEmpty())
                {
                    return Address1 + ", " + Address2 + ", " + Address3;
                }

                if (!Address2.IsNullOrEmpty() && Address3.IsNullOrEmpty())
                {
                    return Address1 + ", " + Address2;
                }

                if (Address2.IsNullOrEmpty() && !Address3.IsNullOrEmpty())
                {
                    return Address1 + ", " + Address3;
                }

                return Address1;
            }
        }
    }

    public enum CustomerContactType
    {
        Phone = 1, Mobile = 2, Email = 3
    }

    public enum CustomerAddressType
    {
        Personal = 1, Official = 2, Others = 3
    }

    [Table("ConnectGuests")]
    public class ConnectGuest : ConnectFullMultiTenantAuditEntity
    {
        [StringLength(100)] public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public Guid? CustomerGuid { get; set; }
        public string CartJsonData { get; set; }
        public string EmailAddress { get; set; }
    }

}
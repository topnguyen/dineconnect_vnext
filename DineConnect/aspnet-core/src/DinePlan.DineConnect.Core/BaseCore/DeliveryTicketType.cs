﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.BaseCore
{
    public enum DeliveryZoneType
    {
        Circle = 0,
        Polygon = 1
    }

    public enum DeliveryTicketType
    {
        Delivery=1,
        PickUp = 2,
    }
}

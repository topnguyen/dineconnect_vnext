﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.BaseCore;

namespace DinePlan.DineConnect.Framework
{
    [Table("TenantDatabases")]
    public class TenantDatabase : ConnectFullAuditEntity
    {
        public string Name { get; set; }
        public string ServerIp  { get; set; }
        public string ServerPort  { get; set; }
        public string UserName  { get; set; }
        public string Password  { get; set; }
        public bool PersistSecurityInfo { get; set; }
        public bool MultipleActiveResultSets { get; set; }
        public bool Encrypt { get; set; }
        public bool TrustServerCertificate { get; set; }
        public int ConnectionTimeout { get; set; }
    }
}

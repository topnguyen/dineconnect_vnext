﻿namespace DinePlan.DineConnect.Filter
{
    public static class DineConnectDataFilters
    {
        public const string MustHaveOrganization = "MustHaveOrganization";

        public static class Parameters
        {
            public const string OrganizationId = "organizationId";
        }
    }
}
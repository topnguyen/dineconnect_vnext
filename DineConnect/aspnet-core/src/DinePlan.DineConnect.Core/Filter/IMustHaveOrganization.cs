﻿namespace DinePlan.DineConnect.Filter
{
    /**
     * Data Filter Interface
     */

    public interface IMustHaveOrganization
    {
        int OrganizationId { get; set; }
    }
}
﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.MultiTenancy;
using Abp.Runtime.Caching;
using Abp.Runtime.Security;
using DinePlan.DineConnect.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.MultiTenancy
{
    public class DineConnectTenantCacheExtended : TenantCache<Tenant, User>
    {
        public DineConnectTenantCacheExtended(
            ICacheManager cacheManager,
            IRepository<Tenant> tenantRepository,
            IUnitOfWorkManager unitOfWorkManager) : base(cacheManager, tenantRepository, unitOfWorkManager)
        {
        }

        protected override TenantCacheItem CreateTenantCacheItem(Tenant tenant)
        {
            return new TenantCacheItem
            {
                Id = tenant.Id,
                Name = tenant.Name,
                TenancyName = tenant.TenancyName,
                EditionId = tenant.EditionId,
                ConnectionString = SimpleStringCipher.Instance.Decrypt(tenant.ConnectionString),
                IsActive = tenant.IsActive
            };
        }
    }

   
}

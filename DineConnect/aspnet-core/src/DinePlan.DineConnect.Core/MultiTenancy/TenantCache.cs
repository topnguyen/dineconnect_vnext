﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.MultiTenancy;
using Abp.Runtime.Caching;
using DinePlan.DineConnect.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace DinePlan.DineConnect.MultiTenancy
{
    public class DineConnectTenantCache : Abp.MultiTenancy.TenantCache<Tenant, User>
    {
        public DineConnectTenantCache(
            ICacheManager cacheManager,
            IRepository<Tenant> tenantRepository,
            IUnitOfWorkManager unitOfWorkManager)
            : 
            base(cacheManager, tenantRepository, unitOfWorkManager)
        {
        }

        public override TenantCacheItem GetOrNull(string tenancyName)
        {
            return base.GetOrNull(tenancyName);
        }
    }
}

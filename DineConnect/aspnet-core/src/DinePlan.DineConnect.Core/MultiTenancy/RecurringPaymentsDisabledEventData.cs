﻿using Abp.Events.Bus;

namespace DinePlan.DineConnect.MultiTenancy
{
    public class RecurringPaymentsDisabledEventData : EventData
    {
        public int TenantId { get; set; }

        public int EditionId { get; set; }
    }
}

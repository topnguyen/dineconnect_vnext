﻿using Abp.Events.Bus;

namespace DinePlan.DineConnect.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}
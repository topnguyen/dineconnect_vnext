namespace DinePlan.DineConnect.MultiTenancy.Payments.Stripe
{
    public class StripeIdResponse
    {
        public string Id { get; set; }
    }
}
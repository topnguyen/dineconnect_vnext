﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.MultiTenancy.Payments
{
    public interface IPaymentGatewayStore
    {
        List<PaymentGatewayModel> GetActiveGateways();
    }
}

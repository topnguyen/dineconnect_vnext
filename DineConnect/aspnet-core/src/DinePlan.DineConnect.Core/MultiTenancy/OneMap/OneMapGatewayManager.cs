using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Abp.Dependency;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.MultiTenancy.OneMap
{
    public class OneMapGatewayManager : DineConnectServiceBase, ITransientDependency
    {
        private readonly HttpClient _client;

        public OneMapGatewayManager()
        {
            _client = new HttpClient();
        }

        public async Task<List<AddressByPostcode>> GetAddressByPostCode(string postalCode)
        {
            var url = $"https://developers.onemap.sg/commonapi/search?searchVal={postalCode}&returnGeom=Y&getAddrDetails=Y";
            var response = await _client.GetAsync(url);
            Console.WriteLine(response.StatusCode);
            var result = new List<AddressByPostcode>();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                var oneMapSearchResult = JsonConvert.DeserializeObject<OneMapSearchResult>(jsonString);
                if (oneMapSearchResult != null && oneMapSearchResult.Results.Length > 0)
                {
                    result = oneMapSearchResult.Results.Select(x => new AddressByPostcode
                    {
                        Searchval = x.Searchval,
                        BlkNo = x.BlkNo,
                        RoadName = x.RoadName,
                        Building = x.Building,
                        Address = x.Address,
                        Postal = x.Postal,
                        X = x.X,
                        Y = x.Y,
                        Latitude = x.Latitude,
                        Longitude = x.Longitude,
                        Longtitude = x.Longtitude
                    }).ToList();
                }
            }
            return result;
        }
    }
}
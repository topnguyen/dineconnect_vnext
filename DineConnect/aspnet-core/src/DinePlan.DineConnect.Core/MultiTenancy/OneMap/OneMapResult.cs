using Newtonsoft.Json;

namespace DinePlan.DineConnect.MultiTenancy.OneMap
{
    public class OneMapResult
    {
        [JsonProperty("SEARCHVAL")]
        public string Searchval { get; set; }

        [JsonProperty("BLK_NO")]
        public string BlkNo { get; set; }

        [JsonProperty("ROAD_NAME")]
        public string RoadName { get; set; }

        [JsonProperty("BUILDING")]
        public string Building { get; set; }

        [JsonProperty("ADDRESS")]
        public string Address { get; set; }

        [JsonProperty("POSTAL")]
        public string Postal { get; set; }

        [JsonProperty("X")]
        public string X { get; set; }

        [JsonProperty("Y")]
        public string Y { get; set; }

        [JsonProperty("LATITUDE")]
        public string Latitude { get; set; }

        [JsonProperty("LONGITUDE")]
        public string Longitude { get; set; }

        [JsonProperty("LONGTITUDE")]
        public string Longtitude { get; set; }
    }
    
    public class AddressByPostcode
    {
        public string Searchval { get; set; }
        public string BlkNo { get; set; }
        public string RoadName { get; set; }
        public string Building { get; set; }
        public string Address { get; set; }
        public string Postal { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Longtitude { get; set; }
    }
}
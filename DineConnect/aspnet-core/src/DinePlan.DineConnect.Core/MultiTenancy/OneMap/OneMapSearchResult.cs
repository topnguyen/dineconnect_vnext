using Newtonsoft.Json;

namespace DinePlan.DineConnect.MultiTenancy.OneMap
{
    public class OneMapSearchResult
    {
        [JsonProperty("found")]
        public long Found { get; set; }

        [JsonProperty("totalNumPages")]
        public long TotalNumPages { get; set; }

        [JsonProperty("pageNum")]
        public long PageNum { get; set; }

        [JsonProperty("results")]
        public OneMapResult[] Results { get; set; }
    }

   
}
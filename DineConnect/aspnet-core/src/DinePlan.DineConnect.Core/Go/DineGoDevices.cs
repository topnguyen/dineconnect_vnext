﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.BaseCore;
using DinePlan.DineConnect.Connect.Master.Departments;
using DinePlan.DineConnect.Connect.Master.Locations;
using DinePlan.DineConnect.Connect.Master.PaymentTypes;
using DinePlan.DineConnect.Connect.Master.TransactionTypes;
using DinePlan.DineConnect.Connect.Menu;

namespace DinePlan.DineConnect.Go
{
    [Table("DineGoDevices")]
    public class DineGoDevice : ConnectFullMultiTenantAuditEntity
    {
        public string Name { get; set; }
        public int IdleTime { get; set; }
        public bool PushToConnect { get; set; }

        public string OrderLink { get; set; }
        public string PinCode { get; set; }

        public virtual ICollection<DineGoBrandDevice> DineGoBrands { get; set; }

        public virtual ICollection<DineGoPaymentTypesDevice> DineGoPaymentTypes { get; set; }

        public string ThemeSettings { get; set; }
        public Guid ChangeGuid { get; set; }

    }

    public class DeviceThemeSettings
    {
        public string IdleContents { get; set; }
        public string BannerContents { get; set; }
        public string BackgroundColor { get; set; }
        public string Logo { get; set; }
    }

    [Table("DineGoBrands")]
    public class DineGoBrand : ConnectFullMultiTenantAuditEntity
    {
        public string Label { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public virtual int LocationId { get; set; }
        [ForeignKey("ScreenMenuId")]
        public ScreenMenu ScreenMenu { get; set; }
        public virtual int ScreenMenuId { get; set; }

        public virtual ICollection<DineGoBrandDevice> DineGoDevices { get; set; }

        public virtual ICollection<DineGoBrandDepartment> DineGoDepartments { get; set; }
    }

    [Table("DineGoBrandDepartments")]
    public class DineGoBrandDepartment : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("DineGoBrandId")]
        public DineGoBrand DineGoBrand { get; set; }

        public virtual int DineGoBrandId { get; set; }

        [ForeignKey("DineGoDepartmentId")]
        public DineGoDepartment DineGoDepartment { get; set; }

        public virtual int DineGoDepartmentId { get; set; }
    }


    [Table("DineGoBrandsDevices")]
    public class DineGoBrandDevice : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("DineGoBrandId")]
        public DineGoBrand DineGoBrand { get; set; }
      
        public virtual int DineGoBrandId { get; set; }

        [ForeignKey("DineGoDeviceId")]
        public DineGoDevice DineGoDevice { get; set; }
        
        public virtual int DineGoDeviceId { get; set; }
    }

    [Table("DineGoDepartments")]
    public class DineGoDepartment : ConnectFullMultiTenantAuditEntity
    {
        public string Label { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }
        public virtual int DepartmentId { get; set; }

        public virtual ICollection<DineGoChargeDepartment> DineGoCharges { get; set; }
        public virtual ICollection<DineGoBrandDepartment> DineGoBrands { get; set; }
    }

    [Table("DineGoPaymentTypes")]
    public class DineGoPaymentType : ConnectFullMultiTenantAuditEntity
    {
        public string Label { get; set; }

        [ForeignKey("PaymentTypeId")]
        public PaymentType PaymentType { get; set; }
        public virtual int PaymentTypeId { get; set; }

        public virtual ICollection<DineGoPaymentTypesDevice> DineGoDevices { get; set; }
    }

    [Table("DineGoPaymentTypesDevices")]
    public class DineGoPaymentTypesDevice : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("DineGoPaymentTypeId")]
        public DineGoPaymentType DineGoPaymentType { get; set; }

        public virtual int DineGoPaymentTypeId { get; set; }

        [ForeignKey("DineGoDeviceId")]
        public DineGoDevice DineGoDevice { get; set; }

        public virtual int DineGoDeviceId { get; set; }
    }

    [Table("DineGoCharges")]
    public class DineGoCharge : ConnectFullMultiTenantAuditEntity
    {
        public string Label { get; set; }
        public bool IsPercent { get; set; }
        public bool TaxIncluded { get; set; }
        public decimal ChargeValue { get; set; }

        [ForeignKey("TransactionTypeId")]
        public TransactionType TransactionType { get; set; }
        public virtual int TransactionTypeId { get; set; }

        public virtual ICollection<DineGoChargeDepartment> DineGoDepartments { get; set; }
    }

    [Table("DineGoChargeDepartments")]
    public class DineGoChargeDepartment : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("DineGoChargeId")]
        public DineGoCharge DineGoCharge { get; set; }

        public virtual int DineGoChargeId { get; set; }

        [ForeignKey("DineGoDepartmentId")]
        public DineGoDepartment DineGoDepartment { get; set; }

        public virtual int DineGoDepartmentId { get; set; }
    }
}

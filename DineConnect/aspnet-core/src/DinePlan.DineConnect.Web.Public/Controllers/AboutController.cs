﻿using Microsoft.AspNetCore.Mvc;
using DinePlan.DineConnect.Web.Controllers;

namespace DinePlan.DineConnect.Web.Public.Controllers
{
    public class AboutController : DineConnectControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
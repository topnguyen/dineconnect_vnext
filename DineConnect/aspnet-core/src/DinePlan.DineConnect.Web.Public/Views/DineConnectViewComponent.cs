﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace DinePlan.DineConnect.Web.Public.Views
{
    public abstract class DineConnectViewComponent : AbpViewComponent
    {
        protected DineConnectViewComponent()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }
    }
}
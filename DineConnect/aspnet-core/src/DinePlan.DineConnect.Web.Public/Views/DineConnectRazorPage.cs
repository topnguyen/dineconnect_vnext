﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace DinePlan.DineConnect.Web.Public.Views
{
    public abstract class DineConnectRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected DineConnectRazorPage()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }
    }
}

﻿using Abp.Dependency;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Url;
using DinePlan.DineConnect.Web.Url;

namespace DinePlan.DineConnect.Web.Public.Url
{
    public class WebUrlService : WebUrlServiceBase, IWebUrlService, ITransientDependency
    {
        public WebUrlService(
            IAppConfigurationAccessor appConfigurationAccessor) :
            base(appConfigurationAccessor)
        {
        }

        public override string WebSiteRootAddressFormatKey => "App:WebSiteRootAddress";

        public override string ServerRootAddressFormatKey => "App:AdminWebSiteRootAddress";

        public override string WebSiteTiffinRootAddressKey => "App:WebSiteTiffinRootAddress";
    }
}
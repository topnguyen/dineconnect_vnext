﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.IO.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Card.Card;
using DinePlan.DineConnect.Connect.Card.Card.Dtos;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Tiffins.TrackPod.TrackRoute;
using Microsoft.AspNetCore.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpAuthorize]
    [IgnoreAntiforgeryToken]
    public class ImportControllerBase : DineConnectControllerBase
    {
        private readonly IConnectCardAppService _connectCardAppService;
        private readonly IShipmentRouteAppService _shipmentRouteAppService;

        public ImportControllerBase(IConnectCardAppService connectCardAppService,
            IShipmentRouteAppService shipmentRouteAppService
        )
        {
            _connectCardAppService = connectCardAppService;
            _shipmentRouteAppService = shipmentRouteAppService;
        }

        public TenantManager TenantManager { get; set; }

        [HttpPost]
        public async Task ImportConnectCardData(ImportInput input)
        {
            try
            {
                #region Saving the Request Template File Locally

                //ImportInput input = new ImportInput();
                //Check input
                if (Request.Form.Files.Count <= 0 || Request.Form.Files[0] == null)
                    throw new UserFriendlyException(L("File") + " " + L("NotExist"));

                #region FilePathDefinition

                if (AbpSession.TenantId != null)
                {
                    var tenant = await TenantManager.GetByIdAsync(AbpSession.TenantId.Value);
                    var tenantName = tenant.TenancyName;
                    var tenantId = tenant.Id;
                    var subPath = "\\DocumentFiles\\" + tenantName + "\\ImportConnectCardData\\" +
                                  DateTime.Now.ToString("dd-MMM-yyyy HH-mm-ss") + "\\";

                    var phyPath = Path.GetFullPath(subPath);

                    var exists = Directory.Exists(phyPath); //System.IO.Server.MapPath

                    if (!exists) Directory.CreateDirectory(phyPath);

                    #endregion FilePathDefinition

                    var file = Request.Form.Files.First();
                    var fileSavedName = Path.GetFileName(file.FileName);
                    if (fileSavedName != null)
                    {
                        using (var stream = new FileStream(Path.Combine(phyPath, fileSavedName), FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        var templateFileName = phyPath + fileSavedName;

                        #endregion Saving the Request Template File Locally

                        var allItems = new List<ConnectCardEditDto>();
                        var inputDto = new ImportConnectCardsInBackgroundInputDto
                        {
                            ConnectCard = allItems,
                            FileName = templateFileName,
                            Active = input.Active,
                            TenantId = tenantId,
                            TenantName = tenantName,
                            JobName = input.JobName
                        };

                        await _connectCardAppService.ImportConnectCards(inputDto);
                    }
                }
            }
            catch (UserFriendlyException ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [HttpPost]
        public async Task ImportShipmentOrders()
        {
            var orders = Request.Form.Files.First();
            if (orders == null) throw new UserFriendlyException(L("Order_Change_Error"));
            byte[] fileBytes;
            using (var stream = orders.OpenReadStream())
            {
                fileBytes = stream.GetAllBytes();
            }

            await _shipmentRouteAppService.CreateOrdersFromExcel(fileBytes);
        }

        public class ImportInput
        {
            public ImportInput(bool active)
            {
                Active = active;
            }

            public bool Active { get; set; }
            public string JobName { get; set; }
        }
    }
}
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetZeroCore.Net;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpMvcAuthorize]
    public class TenantCustomizationController : DineConnectControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly IBinaryObjectManager _binaryObjectManager;

        public TenantCustomizationController(
            TenantManager tenantManager,
            IBinaryObjectManager binaryObjectManager)
        {
            _tenantManager = tenantManager;
            _binaryObjectManager = binaryObjectManager;
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Tenant_Settings)]
        public async Task<JsonResult> UploadLogo()
        {
            try
            {
                var logoFile = Request.Form.Files.First();

                //Check input
                if (logoFile == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (logoFile.Length > 30720) //30KB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                byte[] fileBytes;
                using (var stream = logoFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);
                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                {
                    throw new UserFriendlyException(L("File_Invalid_Type_Error"));
                }

                var logoObject = new BinaryObject(AbpSession.GetTenantId(), fileBytes);
                await _binaryObjectManager.SaveAsync(logoObject);

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.GetTenantId());
                tenant.LogoId = logoObject.Id;
                tenant.LogoFileType = logoFile.ContentType;

                return Json(new AjaxResponse(new { id = logoObject.Id, TenantId = tenant.Id, fileType = tenant.LogoFileType }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Tenant_Settings)]
        public async Task<JsonResult> UploadCustomCss()
        {
            try
            {
                var cssFile = Request.Form.Files.First();

                //Check input
                if (cssFile == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (cssFile.Length > 1048576) //1MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                byte[] fileBytes;
                using (var stream = cssFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var cssFileObject = new BinaryObject(AbpSession.GetTenantId(), fileBytes);
                await _binaryObjectManager.SaveAsync(cssFileObject);

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.GetTenantId());
                tenant.CustomCssId = cssFileObject.Id;

                return Json(new AjaxResponse(new { id = cssFileObject.Id, TenantId = tenant.Id }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetLogo(int? tenantId)
        {
            if (tenantId == null)
            {
                tenantId = AbpSession.TenantId;
            }
            if (!tenantId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasLogo())
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.LogoId != null)
                {
                    var logoObject = await _binaryObjectManager.GetOrNullAsync(tenant.LogoId.Value);
                    if (logoObject == null)
                    {
                        return StatusCode((int)HttpStatusCode.NotFound);
                    }

                    return File(logoObject.Bytes, tenant.LogoFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetFavicon(int? tenantId)
        {
            if (tenantId == null)
            {
                tenantId = AbpSession.TenantId;
            }
            if (!tenantId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasFavicon())
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.FaviconId != null)
                {
                    var faviconObject = await _binaryObjectManager.GetOrNullAsync(tenant.FaviconId.Value);
                    if (faviconObject == null)
                    {
                        return StatusCode((int)HttpStatusCode.NotFound);
                    }

                    return File(faviconObject.Bytes, tenant.FaviconFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetMenuItemDefault(int? tenantId)
        {
            if (tenantId == null)
            {
                tenantId = AbpSession.TenantId;
            }
            if (!tenantId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasMenuItemDefault())
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.MenuItemDefaultID != null)
                {
                    var menuItemObject = await _binaryObjectManager.GetOrNullAsync(tenant.MenuItemDefaultID.Value);
                    if (menuItemObject == null)
                    {
                        return StatusCode((int)HttpStatusCode.NotFound);
                    }

                    return File(menuItemObject.Bytes, tenant.MenuItemDefaultFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetTenantLogo(string skin, int? tenantId)
        {
            var defaultLogo = "/Common/Images/app-logo-on-" + skin + ".svg";

            if (tenantId == null)
            {
                return File(defaultLogo, MimeTypeNames.ImageSvgXml);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasLogo())
            {
                return File(defaultLogo, MimeTypeNames.ImageSvgXml);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.LogoId != null)
                {
                    var logoObject = await _binaryObjectManager.GetOrNullAsync(tenant.LogoId.Value);
                    if (logoObject == null)
                    {
                        return File(defaultLogo, MimeTypeNames.ImageSvgXml);
                    }

                    return File(logoObject.Bytes, tenant.LogoFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetTenantFavicon(string skin, int? tenantId)
        {
            var defaultFavicon = "/Common/Images/app-logo-on-" + skin + ".svg";

            if (tenantId == null)
            {
                return File(defaultFavicon, MimeTypeNames.ImageSvgXml);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasFavicon())
            {
                return File(defaultFavicon, MimeTypeNames.ImageSvgXml);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.FaviconId != null)
                {
                    var faviconObject = await _binaryObjectManager.GetOrNullAsync(tenant.FaviconId.Value);
                    if (faviconObject == null)
                    {
                        return File(defaultFavicon, MimeTypeNames.ImageSvgXml);
                    }

                    return File(faviconObject.Bytes, tenant.FaviconFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetTenantMenuItemDefault(string skin, int? tenantId)
        {
            var defaultMenuItem = "/Common/Images/app-logo-on-" + skin + ".svg";

            if (tenantId == null)
            {
                return File(defaultMenuItem, MimeTypeNames.ImageSvgXml);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.HasMenuItemDefault())
            {
                return File(defaultMenuItem, MimeTypeNames.ImageSvgXml);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                if (tenant.MenuItemDefaultID != null)
                {
                    var menuItemObject = await _binaryObjectManager.GetOrNullAsync(tenant.MenuItemDefaultID.Value);
                    if (menuItemObject == null)
                    {
                        return File(defaultMenuItem, MimeTypeNames.ImageSvgXml);
                    }

                    return File(menuItemObject.Bytes, tenant.MenuItemDefaultFileType);
                }
            }

            return null;
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetCustomCss(int? tenantId)
        {
            if (tenantId == null)
            {
                tenantId = AbpSession.TenantId;
            }
            if (!tenantId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            var tenant = await _tenantManager.FindByIdAsync(tenantId.Value);
            if (tenant == null || !tenant.CustomCssId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            using (CurrentUnitOfWork.SetTenantId(tenantId.Value))
            {
                var cssFileObject = await _binaryObjectManager.GetOrNullAsync(tenant.CustomCssId.Value);
                if (cssFileObject == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return File(cssFileObject.Bytes, MimeTypeNames.TextCss);
            }
        }
    }
}
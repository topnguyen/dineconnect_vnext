﻿using System;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetZeroCore.Net;
using Abp.Authorization;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using DinePlan.DineConnect.CloudImage;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Uploader.Dto;
using DinePlan.DineConnect.Web.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpAuthorize]
    [IgnoreAntiforgeryToken]
    public class ImageUploaderControllerBase : DineConnectControllerBase
    {
        private const int MaxImageSize = 5242880; //5MB
        private const string VideoType = "video/mp4";
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ICloudImageUploader _cloudImageUploder;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly TenantManager _tenantManager;

        public ImageUploaderControllerBase(ITempFileCacheManager tempFileCacheManager,
            TenantManager tenantManager, ICloudImageUploader cloudImageUploader,
            IBinaryObjectManager binaryObjectManager
        )
        {
            _tempFileCacheManager = tempFileCacheManager;
            _binaryObjectManager = binaryObjectManager;
            _tenantManager = tenantManager;
            _cloudImageUploder = cloudImageUploader;
        }

        [HttpPost]
        public async Task<UploadImageOutput> UploadImage()
        {
            try
            {
                var uploadCloud = await _cloudImageUploder.CloudinaryAvailable();

                var fileUpload = Request.Form.Files.First();

                //Check input
                if (fileUpload == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (fileUpload.Length > MaxImageSize) throw new UserFriendlyException(L("File_SizeLimit_Error"));

                byte[] fileBytes;
                using (var stream = fileUpload.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);

                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                    throw new UserFriendlyException("File_Invalid_Type_Error");

                var fileToken = Guid.NewGuid().ToString("N");
                _tempFileCacheManager.SetFile(fileToken, fileBytes);

                var returnCom = new UploadImageOutput
                {
                    FileToken = fileToken,
                    FileName = fileUpload.FileName,
                    FileType = fileUpload.ContentType,
                    Url = ""
                };
                if (uploadCloud)
                {
                    var output = await _cloudImageUploder.UploadImage(fileUpload.FileName, fileBytes);
                    if (output != null) returnCom.Url = output.Url;
                }

                return returnCom;
            }
            catch (UserFriendlyException ex)
            {
                return new UploadImageOutput(new ErrorInfo(ex.Message));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadLogo()
        {
            try
            {
                var logoFile = Request.Form.Files.First();

                //Check input
                if (logoFile == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (logoFile.Length > 30720) //30KB
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));

                byte[] fileBytes;
                using (var stream = logoFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);
                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                    throw new UserFriendlyException("File_Invalid_Type_Error");

                var logoObject = new BinaryObject(AbpSession.GetTenantId(), fileBytes);
                await _binaryObjectManager.SaveAsync(logoObject);

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.GetTenantId());
                tenant.LogoId = logoObject.Id;
                tenant.LogoFileType = logoFile.ContentType;

                return Json(new AjaxResponse(new
                    { id = logoObject.Id, TenantId = tenant.Id, fileType = tenant.LogoFileType }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadMenuItemDefault()
        {
            try
            {
                var menuItemFile = Request.Form.Files.First();

                //Check input
                if (menuItemFile == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (menuItemFile.Length > 30720) //30KB
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));

                byte[] fileBytes;
                using (var stream = menuItemFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);
                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                    throw new UserFriendlyException("File_Invalid_Type_Error");

                var menuItemObject = new BinaryObject(AbpSession.GetTenantId(), fileBytes);
                await _binaryObjectManager.SaveAsync(menuItemObject);

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.GetTenantId());
                tenant.MenuItemDefaultID = menuItemObject.Id;
                tenant.MenuItemDefaultFileType = menuItemFile.ContentType;
                _tenantManager.Update(tenant);

                return Json(new AjaxResponse(new
                    { id = menuItemObject.Id, TenantId = tenant.Id, fileType = tenant.MenuItemDefaultFileType }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadFavicon()
        {
            try
            {
                var faviconFile = Request.Form.Files.First();

                //Check input
                if (faviconFile == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (faviconFile.Length > 30720) //30KB
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));

                byte[] fileBytes;
                using (var stream = faviconFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);
                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif, ImageFormat.Icon))
                    throw new UserFriendlyException("File_Invalid_Type_Error");

                var faviconObject = new BinaryObject(AbpSession.GetTenantId(), fileBytes);
                await _binaryObjectManager.SaveAsync(faviconObject);

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.GetTenantId());
                tenant.FaviconId = faviconObject.Id;
                tenant.FaviconFileType = faviconFile.ContentType;

                return Json(new AjaxResponse(new
                    { id = faviconObject.Id, TenantId = tenant.Id, fileType = tenant.FaviconFileType }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public UploadImageOutput UploadFile()
        {
            try
            {
                var fileUpload = Request.Form.Files.First();

                //Check input
                if (fileUpload == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (fileUpload.Length > MaxImageSize) throw new UserFriendlyException(L("File_SizeLimit_Error"));
                if (!fileUpload.ContentType.IsIn(
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet,
                    MimeTypeNames.ApplicationVndMsExcel)) throw new UserFriendlyException("Import_Warn_FileType");

                byte[] fileBytes;
                using (var stream = fileUpload.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var fileToken = Guid.NewGuid().ToString("N");

                _tempFileCacheManager.SetFile(fileToken, fileBytes);

                return new UploadImageOutput
                {
                    FileToken = fileToken,
                    FileName = fileUpload.FileName,
                    FileType = fileUpload.ContentType
                };
            }
            catch (UserFriendlyException ex)
            {
                return new UploadImageOutput(new ErrorInfo(ex.Message));
            }
        }

        [HttpPost]
        public async Task<UploadImageOutput> UploadVideo()
        {
            try
            {
                var uploadCloud = await _cloudImageUploder.CloudinaryAvailable();

                var fileUpload = Request.Form.Files.First();

                //Check input
                if (fileUpload == null) throw new UserFriendlyException(L("File_Empty_Error"));

                byte[] fileBytes;
                using (var stream = fileUpload.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                if (!fileUpload.ContentType.IsIn(VideoType)) throw new UserFriendlyException("File_Invalid_Type_Error");

                var fileToken = Guid.NewGuid().ToString("N");
                _tempFileCacheManager.SetFile(fileToken, fileBytes);

                var returnCom = new UploadImageOutput
                {
                    FileToken = fileToken,
                    FileName = fileUpload.FileName,
                    FileType = fileUpload.ContentType,
                    Url = ""
                };
                if (uploadCloud)
                {
                    var output = await _cloudImageUploder.UploadVideo(fileUpload.FileName, fileBytes);
                    if (output != null) returnCom.Url = output.Url;
                }

                return returnCom;
            }
            catch (UserFriendlyException ex)
            {
                return new UploadImageOutput(new ErrorInfo(ex.Message));
            }
        }

        [HttpPost]
        public async Task<UploadImageOutput> UploadImageForScreenMenuItem()
        {
            try
            {
                var uploadCloud = await _cloudImageUploder.CloudinaryAvailable();

                var fileUpload = Request.Form.Files.First();

                //Check input
                if (fileUpload == null) throw new UserFriendlyException(L("File_Empty_Error"));

                if (fileUpload.Length > MaxImageSize) throw new UserFriendlyException(L("File_SizeLimit_Error"));
                byte[] fileBytes;

                using (var stream = fileUpload.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var imageFormat = ImageFormatHelper.GetRawImageFormat(fileBytes);

                if (!imageFormat.IsIn(ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif))
                    throw new UserFriendlyException("File_Invalid_Type_Error");

                //var width = await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Width, AbpSession.GetTenantId());
                //var height = await SettingManager.GetSettingValueForTenantAsync<int>(AppSettings.OrderSetting.Height, AbpSession.GetTenantId());

                var fileToken = Guid.NewGuid().ToString("N");
                _tempFileCacheManager.SetFile(fileToken, fileBytes);

                var returnCom = new UploadImageOutput
                {
                    FileToken = fileToken,
                    FileName = fileUpload.FileName,
                    FileType = fileUpload.ContentType,
                    Url = ""
                };
                if (uploadCloud)
                {
                    var output = await _cloudImageUploder.UploadImage(fileUpload.FileName, fileBytes);
                    if (output != null) returnCom.Url = output.Url;
                }

                return returnCom;
            }
            catch (UserFriendlyException ex)
            {
                return new UploadImageOutput(new ErrorInfo(ex.Message));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Features;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetZeroCore.Web.Authentication.External;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.MultiTenancy;
using Abp.Notifications;
using Abp.RealTime;
using Abp.Runtime.Caching;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Abp.Timing;
using Abp.UI;
using Abp.Web.Models;
using Abp.Zero.Configuration;
using DinePlan.DineConnect.Authentication.TwoFactor.Google;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Accounts.Dto;
using DinePlan.DineConnect.Authorization.Delegation;
using DinePlan.DineConnect.Authorization.Impersonation;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Chat;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.EmailConfiguration;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Friendships;
using DinePlan.DineConnect.Identity;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Net.Sms;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.Security.Recaptcha;
using DinePlan.DineConnect.Tiffins.Customer;
using DinePlan.DineConnect.Web.Authentication.External;
using DinePlan.DineConnect.Web.Authentication.JwtBearer;
using DinePlan.DineConnect.Web.Authentication.TwoFactor;
using DinePlan.DineConnect.Web.Common;
using DinePlan.DineConnect.Web.Models.TokenAuth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DinePlan.DineConnect.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TokenAuthController : DineConnectControllerBase
    {
        private const string UserIdentifierClaimType = "http://dineconnect.xyz/claims/useridentifier";
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly IAppNotifier _appNotifier;
        private readonly ICacheManager _cacheManager;
        private readonly IChatCommunicator _chatCommunicator;
        private readonly AbpUserClaimsPrincipalFactory<User, Role> _claimsPrincipalFactory;
        private readonly TokenAuthConfiguration _configuration;
        private readonly ITenantEmailSendAppService _emailSender;
        private readonly IExternalAuthConfiguration _externalAuthConfiguration;
        private readonly IExternalAuthManager _externalAuthManager;
        private readonly ExternalLoginInfoManagerFactory _externalLoginInfoManagerFactory;
        private readonly IFriendshipManager _friendshipManager;
        private readonly GoogleAuthenticatorProvider _googleAuthenticatorProvider;
        private readonly IdentityOptions _identityOptions;
        private readonly IImpersonationManager _impersonationManager;
        private readonly IOptions<JwtBearerOptions> _jwtOptions;

        private readonly LogInManager _logInManager;
        private readonly IOnlineClientManager<ChatChannel> _onlineClientManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IJwtSecurityStampHandler _securityStampHandler;
        private readonly ISettingManager _settingManager;
        private readonly ISmsSender _smsSender;
        private readonly ITenantCache _tenantCache;
        private readonly IUserDelegationManager _userDelegationManager;
        private readonly IUserLinkManager _userLinkManager;
        private readonly UserManager _userManager;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly IFeatureChecker _featureProvider;

        private readonly IRepository<UserRole, long> _userRoleRepository;

        public TokenAuthController(
            LogInManager logInManager,
            ITenantCache tenantCache,
            AbpLoginResultTypeHelper abpLoginResultTypeHelper,
            TokenAuthConfiguration configuration,
            UserManager userManager,
            ICacheManager cacheManager,
            IOptions<JwtBearerOptions> jwtOptions,
            IExternalAuthConfiguration externalAuthConfiguration,
            IExternalAuthManager externalAuthManager,
            UserRegistrationManager userRegistrationManager,
            IImpersonationManager impersonationManager,
            IUserLinkManager userLinkManager,
            IAppNotifier appNotifier,
            ISmsSender smsSender,
            ITenantEmailSendAppService emailSender,
            IOptions<IdentityOptions> identityOptions,
            GoogleAuthenticatorProvider googleAuthenticatorProvider,
            ExternalLoginInfoManagerFactory externalLoginInfoManagerFactory,
            ISettingManager settingManager,
            IJwtSecurityStampHandler securityStampHandler,
            AbpUserClaimsPrincipalFactory<User, Role> claimsPrincipalFactory,
            IUserDelegationManager userDelegationManager,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IOnlineClientManager<ChatChannel> onlineClientManager,
            IChatCommunicator chatCommunicator,
            IFriendshipManager friendshipManager,
            RoleManager roleManager,IFeatureChecker featureProvider)
        {
            _logInManager = logInManager;
            _tenantCache = tenantCache;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _configuration = configuration;
            _userManager = userManager;
            _cacheManager = cacheManager;
            _jwtOptions = jwtOptions;
            _externalAuthConfiguration = externalAuthConfiguration;
            _externalAuthManager = externalAuthManager;
            _userRegistrationManager = userRegistrationManager;
            _impersonationManager = impersonationManager;
            _userLinkManager = userLinkManager;
            _appNotifier = appNotifier;
            _smsSender = smsSender;
            _emailSender = emailSender;
            _googleAuthenticatorProvider = googleAuthenticatorProvider;
            _externalLoginInfoManagerFactory = externalLoginInfoManagerFactory;
            _settingManager = settingManager;
            _securityStampHandler = securityStampHandler;
            _identityOptions = identityOptions.Value;
            _claimsPrincipalFactory = claimsPrincipalFactory;
            RecaptchaValidator = NullRecaptchaValidator.Instance;
            _userDelegationManager = userDelegationManager;
            _roleRepository = roleRepository;
            _onlineClientManager = onlineClientManager;
            _chatCommunicator = chatCommunicator;
            _friendshipManager = friendshipManager;
            _roleManager = roleManager;
            _userRoleRepository = userRoleRepository;
            _featureProvider = featureProvider;
        }

        public IRecaptchaValidator RecaptchaValidator { get; set; }

        [HttpPost]
        public async Task<AuthenticateResultModel> AuthenticatePortal([FromBody] AuthenticateModel model)
        {
            if (UseCaptchaOnLogin()) await ValidateReCaptcha(model.CaptchaResponse);

            var loginResult = await GetLoginResultAsync(
                model.UserNameOrEmailAddress,
                model.Password,
                GetTenancyNameOrNull()
            );

            var isCustomer =
                await _userRegistrationManager.IsCustomer(AbpSession.TenantId, model.UserNameOrEmailAddress);

            if (!isCustomer) throw new UserFriendlyException("User is not a customer");

            var returnUrl = model.ReturnUrl;

            if (model.SingleSignIn.HasValue && model.SingleSignIn.Value &&
                loginResult.Result == AbpLoginResultType.Success)
            {
                loginResult.User.SetSignInToken();
                returnUrl = AddSingleSignInParametersToReturnUrl(model.ReturnUrl, loginResult.User.SignInToken,
                    loginResult.User.Id, loginResult.User.TenantId);
            }

            //Password reset
            if (loginResult.User.ShouldChangePasswordOnNextLogin)
            {
                loginResult.User.SetNewPasswordResetCode();
                return new AuthenticateResultModel
                {
                    ShouldResetPassword = true,
                    PasswordResetCode = loginResult.User.PasswordResetCode,
                    UserId = loginResult.User.Id,
                    ReturnUrl = returnUrl
                };
            }

            //Two factor auth
            await _userManager.InitializeOptionsAsync(loginResult.Tenant?.Id);

            string twoFactorRememberClientToken = null;
            if (await IsTwoFactorAuthRequiredAsync(loginResult, model))
            {
                if (model.TwoFactorVerificationCode.IsNullOrEmpty())
                {
                    //Add a cache item which will be checked in SendTwoFactorAuthCode to prevent sending unwanted two factor code to users.
                    _cacheManager
                        .GetTwoFactorCodeCache()
                        .Set(
                            loginResult.User.ToUserIdentifier().ToString(),
                            new TwoFactorCodeCacheItem()
                        );

                    return new AuthenticateResultModel
                    {
                        RequiresTwoFactorVerification = true,
                        UserId = loginResult.User.Id,
                        TwoFactorAuthProviders = await _userManager.GetValidTwoFactorProvidersAsync(loginResult.User),
                        ReturnUrl = returnUrl
                    };
                }

                twoFactorRememberClientToken = await TwoFactorAuthenticateAsync(loginResult.User, model);
            }

            // One Concurrent Login
            if (AllowOneConcurrentLoginPerUser())
            {
                await _userManager.UpdateSecurityStampAsync(loginResult.User);
                await _securityStampHandler.SetSecurityStampCacheItem(loginResult.User.TenantId, loginResult.User.Id,
                    loginResult.User.SecurityStamp);
                loginResult.Identity.ReplaceClaim(new Claim(AppConsts.SecurityStampKey,
                    loginResult.User.SecurityStamp));
            }

            var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
            var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                tokenType: TokenType.RefreshToken));

            var myTenantid = loginResult.User.TenantId ?? 0;
            return new AuthenticateResultModel
            {
                AccessToken = accessToken,
                ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                RefreshToken = refreshToken,
                RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds,
                EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                TwoFactorRememberClientToken = twoFactorRememberClientToken,
                UserId = loginResult.User.Id,
                ReturnUrl = returnUrl,
                PhoneNumber = loginResult.User.PhoneNumber,
                TenantId = myTenantid,
                CliqueEnabled = myTenantid > 0 && await _featureProvider.IsEnabledAsync(myTenantid, AppFeatures.Clique),
                WheelEnabled  = myTenantid > 0 && await _featureProvider.IsEnabledAsync(myTenantid, AppFeatures.Wheel),
                TouchEnabled  =  myTenantid > 0 && await _featureProvider.IsEnabledAsync(myTenantid, AppFeatures.Touch)
            };
        }

        [HttpPost]
        public async Task<AuthenticateResultModel> Authenticate([FromBody] AuthenticateModel model)
        {
            if (UseCaptchaOnLogin()) await ValidateReCaptcha(model.CaptchaResponse);

            var myTenantId = GetTenancyNameOrNull(model.TenancyName);

            var loginResult = await GetLoginResultAsync(
                model.UserNameOrEmailAddress,
                model.Password,
                myTenantId
            );

            var returnUrl = model.ReturnUrl;

            if (model.SingleSignIn.HasValue && model.SingleSignIn.Value &&
                loginResult.Result == AbpLoginResultType.Success)
            {
                loginResult.User.SetSignInToken();
                returnUrl = AddSingleSignInParametersToReturnUrl(model.ReturnUrl, loginResult.User.SignInToken,
                    loginResult.User.Id, loginResult.User.TenantId);
            }

            //Password reset
            if (loginResult.User.ShouldChangePasswordOnNextLogin)
            {
                loginResult.User.SetNewPasswordResetCode();
                return new AuthenticateResultModel
                {
                    ShouldResetPassword = true,
                    PasswordResetCode = loginResult.User.PasswordResetCode,
                    UserId = loginResult.User.Id,
                    ReturnUrl = returnUrl
                };
            }

            //Two factor auth
            await _userManager.InitializeOptionsAsync(loginResult.Tenant?.Id);

            string twoFactorRememberClientToken = null;
            if (await IsTwoFactorAuthRequiredAsync(loginResult, model))
            {
                if (model.TwoFactorVerificationCode.IsNullOrEmpty())
                {
                    //Add a cache item which will be checked in SendTwoFactorAuthCode to prevent sending unwanted two factor code to users.
                    _cacheManager
                        .GetTwoFactorCodeCache()
                        .Set(
                            loginResult.User.ToUserIdentifier().ToString(),
                            new TwoFactorCodeCacheItem()
                        );

                    return new AuthenticateResultModel
                    {
                        RequiresTwoFactorVerification = true,
                        UserId = loginResult.User.Id,
                        TwoFactorAuthProviders = await _userManager.GetValidTwoFactorProvidersAsync(loginResult.User),
                        ReturnUrl = returnUrl
                    };
                }

                twoFactorRememberClientToken = await TwoFactorAuthenticateAsync(loginResult.User, model);
            }

            // One Concurrent Login
            if (AllowOneConcurrentLoginPerUser())
            {
                await _userManager.UpdateSecurityStampAsync(loginResult.User);
                await _securityStampHandler.SetSecurityStampCacheItem(loginResult.User.TenantId, loginResult.User.Id,
                    loginResult.User.SecurityStamp);
                loginResult.Identity.ReplaceClaim(new Claim(AppConsts.SecurityStampKey,
                    loginResult.User.SecurityStamp));
            }

            var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
            var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                tokenType: TokenType.RefreshToken));


            try
            {
                await AddFriendForChatting(loginResult.User.Id);
            }
            catch (Exception)
            {
                // ignored
            }

            var myTenant = loginResult.User.TenantId ?? 0;
            return new AuthenticateResultModel
            {
                AccessToken = accessToken,
                ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                RefreshToken = refreshToken,
                RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds,
                EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                TwoFactorRememberClientToken = twoFactorRememberClientToken,
                UserId = loginResult.User.Id,
                ReturnUrl = returnUrl,
                PhoneNumber = loginResult.User.PhoneNumber,
                TenantId = myTenant,
                CliqueEnabled = myTenant > 0 && await _featureProvider.IsEnabledAsync(myTenant, AppFeatures.Clique),
                WheelEnabled  = myTenant > 0 && await _featureProvider.IsEnabledAsync(myTenant, AppFeatures.Wheel),
                TouchEnabled  =  myTenant > 0 && await _featureProvider.IsEnabledAsync(myTenant, AppFeatures.Touch)
            };
        }

        [HttpPost]
        public async Task<AuthenticationAjaxResponse> AuthenticateDineplan([FromBody] AuthenticateDineplanModel model)
        {
            var loginResult = await GetLoginResultAsync(
                model.UserNameOrEmailAddress,
                model.Password,
                model.TenancyName
            );
            var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
            var result = new AuthenticationAjaxResponse(accessToken)
            {
                Success = true
            };
            return result;
        }

        [HttpPost]
        public async Task<AuthenticationAjaxResponse> AuthenticateLive([FromBody] AuthenticateDineplanModel model)
        {
            var loginResult = await GetLoginResultAsync(
                model.UserNameOrEmailAddress,
                model.Password,
                model.TenancyName
            );
            var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
            var result = new AuthenticationAjaxResponse(accessToken)
            {
                Success = true
            };
            return result;
        }

        [HttpPost]
        public async Task<RefreshTokenResult> RefreshToken(string refreshToken)
        {
            if (string.IsNullOrWhiteSpace(refreshToken)) throw new ArgumentNullException(nameof(refreshToken));

            if (!IsRefreshTokenValid(refreshToken, out var principal))
                throw new ValidationException("Refresh token is not valid!");

            try
            {
                var user = _userManager.GetUser(
                    UserIdentifier.Parse(principal.Claims.First(x => x.Type == AppConsts.UserIdentifier).Value));
                if (user == null) throw new UserFriendlyException("Unknown user or user identifier");

                principal = await _claimsPrincipalFactory.CreateAsync(user);

                var accessToken = CreateAccessToken(await CreateJwtClaims(principal.Identity as ClaimsIdentity, user));

                return await Task.FromResult(new RefreshTokenResult(accessToken, GetEncryptedAccessToken(accessToken),
                    (int)_configuration.AccessTokenExpiration.TotalSeconds));
            }
            catch (UserFriendlyException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new ValidationException("Refresh token is not valid!", e);
            }
        }

        private bool UseCaptchaOnLogin()
        {
            return SettingManager.GetSettingValue<bool>(AppSettings.UserManagement.UseCaptchaOnLogin);
        }

        [HttpGet]
        [AbpAuthorize]
        public async Task LogOut()
        {
            if (AbpSession.UserId != null)
            {
                var tokenValidityKeyInClaims = User.Claims.First(c => c.Type == AppConsts.TokenValidityKey);
                await _userManager.RemoveTokenValidityKeyAsync(_userManager.GetUser(AbpSession.ToUserIdentifier()),
                    tokenValidityKeyInClaims.Value);
                _cacheManager.GetCache(AppConsts.TokenValidityKey).Remove(tokenValidityKeyInClaims.Value);

                if (AllowOneConcurrentLoginPerUser())
                    await _securityStampHandler.RemoveSecurityStampCacheItem(AbpSession.TenantId,
                        AbpSession.GetUserId());
            }
        }

        [HttpPost]
        public async Task SendTwoFactorAuthCode([FromBody] SendTwoFactorAuthCodeModel model)
        {
            var cacheKey = new UserIdentifier(AbpSession.TenantId, model.UserId).ToString();

            var cacheItem = await _cacheManager
                .GetTwoFactorCodeCache()
                .GetOrDefaultAsync(cacheKey);

            if (cacheItem == null)
                //There should be a cache item added in Authenticate method! This check is needed to prevent sending unwanted two factor code to users.
                throw new UserFriendlyException(L("SendSecurityCodeErrorMessage"));

            var user = await _userManager.FindByIdAsync(model.UserId.ToString());

            if (model.Provider != GoogleAuthenticatorProvider.Name)
            {
                cacheItem.Code = await _userManager.GenerateTwoFactorTokenAsync(user, model.Provider);
                var message = L("EmailSecurityCodeBody", cacheItem.Code);

                if (model.Provider == "Email")
                    await _emailSender.SendAsyc(await _userManager.GetEmailAsync(user), L("EmailSecurityCodeSubject"),
                        message, true, null);
                else if (model.Provider == "Phone")
                    await _smsSender.SendAsync(await _userManager.GetPhoneNumberAsync(user), message);
            }

            _cacheManager.GetTwoFactorCodeCache().Set(
                cacheKey,
                cacheItem
            );
            _cacheManager.GetCache("ProviderCache").Set(
                "Provider",
                model.Provider
            );
        }

        [HttpPost]
        public async Task<ImpersonatedAuthenticateResultModel> ImpersonatedAuthenticate(string impersonationToken)
        {
            var result = await _impersonationManager.GetImpersonatedUserAndIdentity(impersonationToken);
            var accessToken = CreateAccessToken(await CreateJwtClaims(result.Identity, result.User));

            return new ImpersonatedAuthenticateResultModel
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds
            };
        }

        [HttpPost]
        public async Task<ImpersonatedAuthenticateResultModel> DelegatedImpersonatedAuthenticate(long userDelegationId,
            string impersonationToken)
        {
            var result = await _impersonationManager.GetImpersonatedUserAndIdentity(impersonationToken);
            var userDelegation = await _userDelegationManager.GetAsync(userDelegationId);

            if (!userDelegation.IsCreatedByUser(result.User.Id))
                throw new UserFriendlyException("User delegation error...");

            var expiration = userDelegation.EndTime.Subtract(Clock.Now);
            var accessToken = CreateAccessToken(await CreateJwtClaims(result.Identity, result.User, expiration),
                expiration);

            return new ImpersonatedAuthenticateResultModel
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                ExpireInSeconds = (int)expiration.TotalSeconds
            };
        }

        [HttpPost]
        public async Task<SwitchedAccountAuthenticateResultModel> LinkedAccountAuthenticate(string switchAccountToken)
        {
            var result = await _userLinkManager.GetSwitchedUserAndIdentity(switchAccountToken);
            var accessToken = CreateAccessToken(await CreateJwtClaims(result.Identity, result.User));

            return new SwitchedAccountAuthenticateResultModel
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds
            };
        }

        [HttpGet]
        public List<ExternalLoginProviderInfoModel> GetExternalAuthenticationProviders()
        {
            var allProviders = _externalAuthConfiguration.ExternalLoginInfoProviders
                .Select(infoProvider => infoProvider.GetExternalLoginInfo()).ToList();
            return ObjectMapper.Map<List<ExternalLoginProviderInfoModel>>(allProviders);
        }

        [HttpPost]
        public async Task<ExternalAuthenticateResultModel> ExternalAuthenticate(
            [FromBody] ExternalAuthenticateModel model)
        {
            var externalUser = await GetExternalUserInfo(model);

            var loginResult = await _logInManager.LoginAsync(
                new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider), GetTenancyNameOrNull());

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                {
                    var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
                    var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                        tokenType: TokenType.RefreshToken));

                    var returnUrl = model.ReturnUrl;

                    if (model.SingleSignIn.HasValue && model.SingleSignIn.Value &&
                        loginResult.Result == AbpLoginResultType.Success)
                    {
                        loginResult.User.SetSignInToken();
                        returnUrl = AddSingleSignInParametersToReturnUrl(model.ReturnUrl, loginResult.User.SignInToken,
                            loginResult.User.Id, loginResult.User.TenantId);
                    }

                    return new ExternalAuthenticateResultModel
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                        ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                        ReturnUrl = returnUrl,
                        RefreshToken = refreshToken,
                        RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds
                    };
                }
                case AbpLoginResultType.UnknownExternalLogin:
                {
                    var newUser = await RegisterExternalUserAsync(externalUser);
                    if (!newUser.IsActive)
                        return new ExternalAuthenticateResultModel
                        {
                            WaitingForActivation = true
                        };

                    //Try to login again with newly registered user!
                    loginResult = await _logInManager.LoginAsync(
                        new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider),
                        GetTenancyNameOrNull());
                    if (loginResult.Result != AbpLoginResultType.Success)
                        throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                            loginResult.Result,
                            model.ProviderKey,
                            GetTenancyNameOrNull()
                        );

                    var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
                    var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                        tokenType: TokenType.RefreshToken));

                    return new ExternalAuthenticateResultModel
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                        ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                        RefreshToken = refreshToken,
                        RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds
                    };
                }
                default:
                {
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                        loginResult.Result,
                        model.ProviderKey,
                        GetTenancyNameOrNull()
                    );
                }
            }
        }

        [HttpPost]
        public async Task<ExternalAuthenticateResultModel> ExternalAuthenticateMember(
            [FromBody] ExternalAuthenticateModel model)
        {
            var externalUser = await GetExternalUserInfo(model);

            var loginResult = await _logInManager.LoginAsync(
                new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider), GetTenancyNameOrNull());

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                {
                    var isCustomer =
                        await _userRegistrationManager.IsCustomer(AbpSession.TenantId, externalUser.EmailAddress);

                    if (!isCustomer)
                        throw new UserFriendlyException(
                            "User already exists and this user is not a customer. Please use other account.");
                    var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
                    var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                        tokenType: TokenType.RefreshToken));

                    var returnUrl = model.ReturnUrl;

                    if (model.SingleSignIn.HasValue && model.SingleSignIn.Value &&
                        loginResult.Result == AbpLoginResultType.Success)
                    {
                        loginResult.User.SetSignInToken();
                        returnUrl = AddSingleSignInParametersToReturnUrl(model.ReturnUrl, loginResult.User.SignInToken,
                            loginResult.User.Id, loginResult.User.TenantId);
                    }

                    return new ExternalAuthenticateResultModel
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                        ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                        ReturnUrl = returnUrl,
                        RefreshToken = refreshToken,
                        RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds
                    };
                }
                case AbpLoginResultType.UnknownExternalLogin:
                {
                    var newUser = await RegisterExternalCustomerUserAsync(externalUser);
                    if (!newUser.IsActive)
                        return new ExternalAuthenticateResultModel
                        {
                            WaitingForActivation = true
                        };

                    //Try to login again with newly registered user!
                    loginResult = await _logInManager.LoginAsync(
                        new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider),
                        GetTenancyNameOrNull());
                    if (loginResult.Result != AbpLoginResultType.Success)
                        throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                            loginResult.Result,
                            model.ProviderKey,
                            GetTenancyNameOrNull()
                        );

                    var accessToken = CreateAccessToken(await CreateJwtClaims(loginResult.Identity, loginResult.User));
                    var refreshToken = CreateRefreshToken(await CreateJwtClaims(loginResult.Identity, loginResult.User,
                        tokenType: TokenType.RefreshToken));

                    return new ExternalAuthenticateResultModel
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncryptedAccessToken(accessToken),
                        ExpireInSeconds = (int)_configuration.AccessTokenExpiration.TotalSeconds,
                        RefreshToken = refreshToken,
                        RefreshTokenExpireInSeconds = (int)_configuration.RefreshTokenExpiration.TotalSeconds
                    };
                }
                default:
                {
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                        loginResult.Result,
                        model.ProviderKey,
                        GetTenancyNameOrNull()
                    );
                }
            }
        }

        #region Etc

        [AbpMvcAuthorize]
        [HttpGet]
        public async Task<ActionResult> TestNotification(string message = "", string severity = "info")
        {
            if (message.IsNullOrEmpty()) message = "This is a test notification, created at " + Clock.Now;

            await _appNotifier.SendMessageAsync(
                AbpSession.ToUserIdentifier(),
                message,
                severity.ToPascalCase().ToEnum<NotificationSeverity>()
            );

            return Content("Sent notification: " + message);
        }

        #endregion Etc

        private async Task<User> RegisterExternalUserAsync(ExternalAuthUserInfo externalLoginInfo)
        {
            string username;

            using (var providerManager =
                _externalLoginInfoManagerFactory.GetExternalLoginInfoManager(externalLoginInfo.Provider))
            {
                username = providerManager.Object.GetUserNameFromExternalAuthUserInfo(externalLoginInfo);
            }

            var user = await _userRegistrationManager.RegisterAsync(
                externalLoginInfo.Name,
                externalLoginInfo.Surname,
                externalLoginInfo.EmailAddress,
                username,
                await _userManager.CreateRandomPassword(),
                true,
                null
            );

            user.Logins = new List<UserLogin>
            {
                new UserLogin
                {
                    LoginProvider = externalLoginInfo.Provider,
                    ProviderKey = externalLoginInfo.ProviderKey,
                    TenantId = user.TenantId
                }
            };

            await CurrentUnitOfWork.SaveChangesAsync();

            return user;
        }

        private async Task<User> RegisterExternalCustomerUserAsync(ExternalAuthUserInfo externalLoginInfo)
        {
            using (var providerManager =
                   _externalLoginInfoManagerFactory.GetExternalLoginInfoManager(externalLoginInfo.Provider))
            {
                providerManager.Object.GetUserNameFromExternalAuthUserInfo(externalLoginInfo);
            }

            var user = await _userRegistrationManager.RegisterWheelCustomerAsync(
                externalLoginInfo.Name,
                externalLoginInfo.Surname,
                externalLoginInfo.EmailAddress,
                externalLoginInfo.EmailAddress,
                null,
                await _userManager.CreateRandomPassword(),
                true,
                null,
                null
            );

            user.Logins = new List<UserLogin>
            {
                new UserLogin
                {
                    LoginProvider = externalLoginInfo.Provider,
                    ProviderKey = externalLoginInfo.ProviderKey,
                    TenantId = user.TenantId
                }
            };

            await CurrentUnitOfWork.SaveChangesAsync();

            return user;
        }

        private async Task<ExternalAuthUserInfo> GetExternalUserInfo(ExternalAuthenticateModel model)
        {
            var userInfo = await _externalAuthManager.GetUserInfo(model.AuthProvider, model.ProviderAccessCode);
            if (userInfo.ProviderKey != model.ProviderKey)
                throw new UserFriendlyException(L("CouldNotValidateExternalUser"));

            return userInfo;
        }

        private async Task<bool> IsTwoFactorAuthRequiredAsync(AbpLoginResult<Tenant, User> loginResult,
            AuthenticateModel authenticateModel)
        {
            if (!await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                .IsEnabled))
                return false;

            if (!loginResult.User.IsTwoFactorEnabled) return false;

            if ((await _userManager.GetValidTwoFactorProvidersAsync(loginResult.User)).Count <= 0) return false;

            if (await TwoFactorClientRememberedAsync(loginResult.User.ToUserIdentifier(), authenticateModel))
                return false;

            return true;
        }

        private async Task<bool> TwoFactorClientRememberedAsync(UserIdentifier userIdentifier,
            AuthenticateModel authenticateModel)
        {
            if (!await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                .IsRememberBrowserEnabled))
                return false;

            if (string.IsNullOrWhiteSpace(authenticateModel.TwoFactorRememberClientToken)) return false;

            try
            {
                var validationParameters = new TokenValidationParameters
                {
                    ValidAudience = _configuration.Audience,
                    ValidIssuer = _configuration.Issuer,
                    IssuerSigningKey = _configuration.SecurityKey
                };

                foreach (var validator in _jwtOptions.Value.SecurityTokenValidators)
                    if (validator.CanReadToken(authenticateModel.TwoFactorRememberClientToken))
                        try
                        {
                            var principal = validator.ValidateToken(authenticateModel.TwoFactorRememberClientToken,
                                validationParameters, out _);
                            var useridentifierClaim = principal.FindFirst(c => c.Type == UserIdentifierClaimType);
                            if (useridentifierClaim == null) return false;

                            return useridentifierClaim.Value == userIdentifier.ToString();
                        }
                        catch (Exception ex)
                        {
                            Logger.Debug(ex.ToString(), ex);
                        }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.ToString(), ex);
            }

            return false;
        }

        /* Checkes two factor code and returns a token to remember the client (browser) if needed */

        private async Task<string> TwoFactorAuthenticateAsync(User user, AuthenticateModel authenticateModel)
        {
            var twoFactorCodeCache = _cacheManager.GetTwoFactorCodeCache();
            var userIdentifier = user.ToUserIdentifier().ToString();
            var cachedCode = await twoFactorCodeCache.GetOrDefaultAsync(userIdentifier);
            var provider = _cacheManager.GetCache("ProviderCache").Get("Provider", cache => cache).ToString();

            if (provider == GoogleAuthenticatorProvider.Name)
            {
                if (!await _googleAuthenticatorProvider.ValidateAsync("TwoFactor",
                    authenticateModel.TwoFactorVerificationCode, _userManager, user))
                    throw new UserFriendlyException(L("InvalidSecurityCode"));
            }
            else if (cachedCode?.Code == null || cachedCode.Code != authenticateModel.TwoFactorVerificationCode)
            {
                throw new UserFriendlyException(L("InvalidSecurityCode"));
            }

            //Delete from the cache since it was a single usage code
            await twoFactorCodeCache.RemoveAsync(userIdentifier);

            if (authenticateModel.RememberClient)
                if (await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin
                    .IsRememberBrowserEnabled))
                    return CreateAccessToken(new[]
                        {
                            new Claim(UserIdentifierClaimType, user.ToUserIdentifier().ToString())
                        },
                        TimeSpan.FromDays(365)
                    );

            return null;
        }

        private string GetTenancyNameOrNull(string name=null)
        {
            if (!string.IsNullOrEmpty(name))
                return name;

            var tenantId = AbpSession.TenantId;
            if (!tenantId.HasValue && HttpContext.Request.Headers.ContainsKey("Abp.TenantId"))
            {
                var myTenant = HttpContext.Request.Headers["Abp.TenantId"].LastOrDefault();
                if (!string.IsNullOrEmpty(myTenant))
                    tenantId = Convert.ToInt32(HttpContext.Request.Headers["Abp.TenantId"]);
                else
                    throw new Exception("Tenant is not Available");
            }

            return !tenantId.HasValue ? null : _tenantCache.GetOrNull(tenantId.Value)?.TenancyName;
        }

        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress,
            string password, string tenancyName)
        {
            var loginResult = await _logInManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;

                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result,
                        usernameOrEmailAddress, tenancyName);
            }
        }

        private string CreateAccessToken(IEnumerable<Claim> claims, TimeSpan? expiration = null)
        {
            return CreateToken(claims, expiration ?? _configuration.AccessTokenExpiration);
        }

        private string CreateRefreshToken(IEnumerable<Claim> claims)
        {
            return CreateToken(claims, AppConsts.RefreshTokenExpiration);
        }

        private string CreateToken(IEnumerable<Claim> claims, TimeSpan? expiration = null)
        {
            var now = DateTime.UtcNow;

            var jwtSecurityToken = new JwtSecurityToken(
                _configuration.Issuer,
                _configuration.Audience,
                claims,
                now,
                signingCredentials: _configuration.SigningCredentials,
                expires: expiration == null ? (DateTime?)null : now.Add(expiration.Value)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        private static string GetEncryptedAccessToken(string accessToken)
        {
            return SimpleStringCipher.Instance.Encrypt(accessToken, AppConsts.DefaultPassPhrase);
        }

        private async Task<IEnumerable<Claim>> CreateJwtClaims(ClaimsIdentity identity, User user,
            TimeSpan? expiration = null, TokenType tokenType = TokenType.AccessToken)
        {
            var tokenValidityKey = Guid.NewGuid().ToString();
            var claims = identity.Claims.ToList();
            var nameIdClaim = claims.First(c => c.Type == _identityOptions.ClaimsIdentity.UserIdClaimType);

            if (_identityOptions.ClaimsIdentity.UserIdClaimType != JwtRegisteredClaimNames.Sub)
                claims.Add(new Claim(JwtRegisteredClaimNames.Sub, nameIdClaim.Value));

            claims.AddRange(new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.Now.ToUnixTimeSeconds().ToString(),
                    ClaimValueTypes.Integer64),
                new Claim(AppConsts.TokenValidityKey, tokenValidityKey),
                new Claim(AppConsts.UserIdentifier, user.ToUserIdentifier().ToUserIdentifierString()),
                new Claim(AppConsts.TokenType, tokenType.To<int>().ToString())
            });

            if (!expiration.HasValue)
                expiration = tokenType == TokenType.AccessToken
                    ? _configuration.AccessTokenExpiration
                    : _configuration.RefreshTokenExpiration;

            _cacheManager
                .GetCache(AppConsts.TokenValidityKey)
                .Set(tokenValidityKey, "", absoluteExpireTime: expiration);

            await _userManager.AddTokenValidityKeyAsync(
                user,
                tokenValidityKey,
                DateTime.UtcNow.Add(expiration.Value)
            );

            return claims;
        }

        private static string AddSingleSignInParametersToReturnUrl(string returnUrl, string signInToken, long userId,
            int? tenantId)
        {
            returnUrl += (returnUrl.Contains("?") ? "&" : "?") +
                         "accessToken=" + signInToken +
                         "&userId=" + Convert.ToBase64String(Encoding.UTF8.GetBytes(userId.ToString()));
            if (tenantId.HasValue)
                returnUrl += "&tenantId=" + Convert.ToBase64String(Encoding.UTF8.GetBytes(tenantId.Value.ToString()));

            return returnUrl;
        }

        private bool IsRefreshTokenValid(string refreshToken, out ClaimsPrincipal principal)
        {
            principal = null;

            try
            {
                var validationParameters = new TokenValidationParameters
                {
                    ValidAudience = _configuration.Audience,
                    ValidIssuer = _configuration.Issuer,
                    IssuerSigningKey = _configuration.SecurityKey
                };

                foreach (var validator in _jwtOptions.Value.SecurityTokenValidators)
                {
                    if (!validator.CanReadToken(refreshToken)) continue;

                    try
                    {
                        principal = validator.ValidateToken(refreshToken, validationParameters, out _);

                        if (principal.Claims.FirstOrDefault(x => x.Type == AppConsts.TokenType)?.Value ==
                            TokenType.RefreshToken.To<int>().ToString())
                            return true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Debug(ex.ToString(), ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.ToString(), ex);
            }

            return false;
        }

        private bool AllowOneConcurrentLoginPerUser()
        {
            return _settingManager.GetSettingValue<bool>(AppSettings.UserManagement.AllowOneConcurrentLoginPerUser);
        }

        private async Task ValidateReCaptcha(string captchaResponse)
        {
            var requestUserAgent = Request.Headers["User-Agent"].ToString();
            if (!requestUserAgent.IsNullOrWhiteSpace() &&
                WebConsts.ReCaptchaIgnoreWhiteList.Contains(requestUserAgent.Trim()))
                return;

            await RecaptchaValidator.ValidateAsync(captchaResponse);
        }

        private async Task AddFriendForChatting(long userId)
        {
            var userInfo = await _userManager.GetUserByIdAsync(userId);

            var tenant = await _userRegistrationManager.GetActiveTenantAsync();

            var customerRolesIds = _roleManager.Roles.Where(r => r.Name == StaticRoleNames.Tenants.Customer)
                .Select(x => x.Id).ToList();

            var userRoles = _userRoleRepository.GetAll().Where(ur => ur.UserId == userId).ToList();

            var isCustomer = userRoles.Any(x => customerRolesIds.Contains(x.RoleId) && x.TenantId == tenant.Id);

            if (!isCustomer) return;

            // Add friend to chat service

            var adminRole = _roleRepository
                .GetAll()
                .FirstOrDefault(x => x.Name == StaticRoleNames.Tenants.Admin && x.TenantId == AbpSession.TenantId);

            var adminUserIds = _userRoleRepository.GetAll().Where(x => x.RoleId == adminRole.Id)
                .Select(x => x.UserId).ToList();

            foreach (var adminUserId in adminUserIds)
            {
                var adminUser = await _userManager.FindByIdAsync(adminUserId.ToString());

                var probableFriend = new UserIdentifier(AbpSession.TenantId, adminUser.Id);

                var userIdentifier = new UserIdentifier(AbpSession.TenantId, userId);

                var isExistFriendShip =
                    await _friendshipManager.GetFriendshipOrNullAsync(userIdentifier, probableFriend) != null;

                if (!isExistFriendShip)
                {
                    var sourceFriendship = new Friendship(userIdentifier, probableFriend, tenant.Name,
                        adminUser.UserName, adminUser.ProfilePictureId, FriendshipState.Accepted);
                    await _friendshipManager.CreateFriendshipAsync(sourceFriendship);

                    var targetFriendship = new Friendship(probableFriend, userIdentifier, tenant.Name,
                        userInfo.UserName, userInfo.ProfilePictureId, FriendshipState.Accepted);
                    await _friendshipManager.CreateFriendshipAsync(targetFriendship);

                    var clients = _onlineClientManager.GetAllByUserId(probableFriend);
                    if (clients.Any())
                    {
                        var isFriendOnline = _onlineClientManager.IsOnline(sourceFriendship.ToUserIdentifier());
                        await _chatCommunicator.SendFriendshipRequestToClient(clients, targetFriendship, false,
                            isFriendOnline);
                    }

                    var senderClients = _onlineClientManager.GetAllByUserId(userIdentifier);
                    if (senderClients.Any())
                    {
                        var isFriendOnline = _onlineClientManager.IsOnline(targetFriendship.ToUserIdentifier());
                        await _chatCommunicator.SendFriendshipRequestToClient(senderClients, sourceFriendship, true,
                            isFriendOnline);
                    }
                }
            }
        }
    }

    public class AuthenticationAjaxResponse : AjaxResponse
    {
        public AuthenticationAjaxResponse(object result) : base(result)
        {
        }

        public long UserId { get; set; }
        public int TenantId { get; set; }
        public string TimeZoneUtc { get; set; }
        public bool Member { get; set; }
    }
}
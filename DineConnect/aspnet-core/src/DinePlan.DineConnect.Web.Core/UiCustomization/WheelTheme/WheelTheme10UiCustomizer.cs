﻿using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.UiCustomization;
using DinePlan.DineConnect.UiCustomization.Dto;
using DinePlan.DineConnect.Web.UiCustomization.Metronic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Web.UiCustomization.WheelTheme
{
    public class WheelTheme10UiCustomizer : UiThemeCustomizerBase, IWheelUiCustomizer
    {
        public WheelTheme10UiCustomizer(ISettingManager settingManager)
            : base(settingManager, AppConsts.WheelTheme10)
        {
        }

        public async Task<UiCustomizationSettingsDto> GetUiSettings()
        {
            var settings = new UiCustomizationSettingsDto
            {
                BaseSettings = new ThemeSettingsDto
                {
                    Layout = new ThemeLayoutSettingsDto
                    {
                        LayoutType = await GetSettingValueAsync(AppSettings.UiManagement.LayoutType)
                    },
                    Header = new ThemeHeaderSettingsDto
                    {
                        DesktopFixedHeader = await GetSettingValueAsync<bool>(AppSettings.UiManagement.Header.DesktopFixedHeader),
                        MobileFixedHeader = await GetSettingValueAsync<bool>(AppSettings.UiManagement.Header.MobileFixedHeader),
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.Header.Color)
                    },
                    Menu = new ThemeMenuSettingsDto()
                    {
                        SearchActive = await GetSettingValueAsync<bool>(AppSettings.UiManagement.SearchActive),
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.LeftAside.Color)
                    },
                    Content = new ThemeContentSettingsDto
                    {
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.Content.Color)
                    }
                }
            };

            settings.BaseSettings.Theme = ThemeName;
            settings.BaseSettings.Layout.LayoutType = "fluid";
            settings.BaseSettings.Header.DesktopFixedHeader = true;
            settings.BaseSettings.Menu.Position = "tab";
            settings.BaseSettings.Menu.AsideSkin = "dark";

            settings.BaseSettings.SubHeader.SubheaderSize = 5;
            settings.BaseSettings.SubHeader.TitleStlye = "text-dark font-weight-bold my-2 mr-5";
            settings.BaseSettings.SubHeader.ContainerStyle = "subheader py-2 py-lg-4  subheader-transparent";

            settings.IsLeftMenuUsed = false;
            settings.IsTopMenuUsed = false;
            settings.IsTabMenuUsed = true;

            return settings;
        }

        public async Task UpdateTenantUiManagementSettingsAsync(int tenantId, ThemeSettingsDto settings)
        {
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.LayoutType, settings.Layout.LayoutType);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.DesktopFixedHeader, settings.Header.DesktopFixedHeader.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.MobileFixedHeader, settings.Header.MobileFixedHeader.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.SearchActive, settings.Menu.SearchActive.ToString());

            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.Color, settings.Header.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.LeftAside.Color, settings.Menu.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Footer.Color, settings.Footer.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Content.Color, settings.Content.Color);
        }

        public async Task<ThemeSettingsDto> GetTenantUiCustomizationSettings(int tenantId)
        {
            var theme = await SettingManager.GetSettingValueForTenantAsync(AppSettings.UiManagement.Theme, tenantId);

            return new ThemeSettingsDto
            {
                Theme = theme,
                Layout = new ThemeLayoutSettingsDto
                {
                    LayoutType = await GetSettingValueForTenantAsync(AppSettings.UiManagement.LayoutType, tenantId)
                },
                Header = new ThemeHeaderSettingsDto
                {
                    DesktopFixedHeader = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.Header.DesktopFixedHeader, tenantId),
                    MobileFixedHeader = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.Header.MobileFixedHeader, tenantId),
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.Header.Color, tenantId)
                },
                Menu = new ThemeMenuSettingsDto()
                {
                    SearchActive = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.SearchActive, tenantId),
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.LeftAside.Color, tenantId)
                },
                Content = new ThemeContentSettingsDto()
                {
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.Content.Color, tenantId)
                }
            };
        }
    }
}
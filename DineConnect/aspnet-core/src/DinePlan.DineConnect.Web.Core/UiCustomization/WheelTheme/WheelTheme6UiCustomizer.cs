﻿using System.Threading.Tasks;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Dto;
using DinePlan.DineConnect.UiCustomization;
using DinePlan.DineConnect.UiCustomization.Dto;
using DinePlan.DineConnect.Web.UiCustomization.Metronic;

namespace DinePlan.DineConnect.Web.UiCustomization.WheelTheme
{
    public class WheelTheme6UiCustomizer : UiThemeCustomizerBase, IWheelUiCustomizer
    {
        public WheelTheme6UiCustomizer(ISettingManager settingManager)
            : base(settingManager, AppConsts.WheelTheme6)
        {
        }

        public async Task<UiCustomizationSettingsDto> GetUiSettings()
        {
            var settings = new UiCustomizationSettingsDto
            {
                BaseSettings = new ThemeSettingsDto
                {
                    Header = new ThemeHeaderSettingsDto
                    {
                        DesktopFixedHeader = await GetSettingValueAsync<bool>(AppSettings.UiManagement.Header.DesktopFixedHeader),
                        MobileFixedHeader = await GetSettingValueAsync<bool>(AppSettings.UiManagement.Header.MobileFixedHeader),
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.Header.Color)
                    },
                    SubHeader = new ThemeSubHeaderSettingsDto
                    {
                        FixedSubHeader = await GetSettingValueAsync<bool>(AppSettings.UiManagement.SubHeader.Fixed),
                        SubheaderStyle = await GetSettingValueAsync(AppSettings.UiManagement.SubHeader.Style)
                    },
                    Footer = new ThemeFooterSettingsDto
                    {
                        FixedFooter = await GetSettingValueAsync<bool>(AppSettings.UiManagement.Footer.FixedFooter)
                    },
                    Menu = new ThemeMenuSettingsDto()
                    {
                        SearchActive = await GetSettingValueAsync<bool>(AppSettings.UiManagement.SearchActive),
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.LeftAside.Color)
                    },
                    Content = new ThemeContentSettingsDto
                    {
                        Color = await GetSettingValueAsync(AppSettings.UiManagement.Content.Color)
                    }
                }
            };

            settings.BaseSettings.Theme = ThemeName;
            settings.BaseSettings.Layout.LayoutType = "fluid";
            settings.BaseSettings.Menu.Position = "left";
            settings.BaseSettings.Header.HeaderSkin = "light";
            settings.BaseSettings.Header.DesktopFixedHeader = true;
            settings.BaseSettings.Menu.AsideSkin = "light";
            settings.BaseSettings.Menu.FixedAside = true;
            settings.BaseSettings.Menu.SubmenuToggle = "false";
            settings.BaseSettings.Menu.AllowAsideMinimizing = true;
            settings.BaseSettings.Menu.DefaultMinimizedAside = true;

            settings.BaseSettings.SubHeader.SubheaderSize = 5;
            settings.BaseSettings.SubHeader.TitleStlye = "text-dark font-weight-bold my-2 mr-5";
            settings.BaseSettings.SubHeader.ContainerStyle = "subheader py-2 py-lg-4  subheader-transparent";
            
            settings.IsLeftMenuUsed = true;
            settings.IsTopMenuUsed = false;
            settings.IsTabMenuUsed = false;
            settings.AllowMenuScroll = true;

            return settings;
        }

        public async Task UpdateTenantUiManagementSettingsAsync(int tenantId, ThemeSettingsDto settings)
        {
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.DesktopFixedHeader, settings.Header.DesktopFixedHeader.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.MobileFixedHeader, settings.Header.MobileFixedHeader.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.SubHeader.Fixed, settings.SubHeader.FixedSubHeader.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.SubHeader.Style, settings.SubHeader.SubheaderStyle);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Footer.FixedFooter, settings.Footer.FixedFooter.ToString());
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.SearchActive, settings.Menu.SearchActive.ToString());

            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Header.Color, settings.Header.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.LeftAside.Color, settings.Menu.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Footer.Color, settings.Footer.Color);
            await ChangeSettingForTenantAsync(tenantId, AppSettings.UiManagement.Content.Color, settings.Content.Color);
        }

        public async Task<ThemeSettingsDto> GetTenantUiCustomizationSettings(int tenantId)
        {
            var theme = await SettingManager.GetSettingValueForTenantAsync(AppSettings.UiManagement.Theme, tenantId);

            return new ThemeSettingsDto
            {
                Theme = theme,
                Header = new ThemeHeaderSettingsDto
                {
                    DesktopFixedHeader = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.Header.DesktopFixedHeader, tenantId),
                    MobileFixedHeader = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.Header.MobileFixedHeader, tenantId),
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.Header.Color, tenantId)
                },
                SubHeader = new ThemeSubHeaderSettingsDto
                {
                    FixedSubHeader = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.SubHeader.Fixed, tenantId),
                    SubheaderStyle = await GetSettingValueForTenantAsync(AppSettings.UiManagement.SubHeader.Style, tenantId)
                },
                Footer = new ThemeFooterSettingsDto
                {
                    FixedFooter = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.Footer.FixedFooter, tenantId)
                },
                Menu = new ThemeMenuSettingsDto()
                {
                    SearchActive = await GetSettingValueForTenantAsync<bool>(AppSettings.UiManagement.SearchActive, tenantId),
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.LeftAside.Color, tenantId)
                },
                Content = new ThemeContentSettingsDto()
                {
                    Color = await GetSettingValueForTenantAsync(AppSettings.UiManagement.Content.Color, tenantId)
                }
            };
        }
    }
}
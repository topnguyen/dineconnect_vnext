﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Sessions.Dto;

namespace DinePlan.DineConnect.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}

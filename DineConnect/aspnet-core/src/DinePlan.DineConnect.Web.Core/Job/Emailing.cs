﻿using Abp.Dependency;
using Abp.Quartz;
using Quartz;
using System.Threading.Tasks;
using DinePlan.DineConnect.BaseCore.ScheduleEmail;

namespace DinePlan.DineConnect.Web.Job
{
    public class Emailing : JobBase, ITransientDependency
    {
        private readonly IScheduleEmailAppService _scheduleEmailAppService;

        public Emailing(IScheduleEmailAppService scheduleEmailAppService)
        {
            _scheduleEmailAppService = scheduleEmailAppService;
        }

        public override Task Execute(IJobExecutionContext context)
        {
            _scheduleEmailAppService.Emailing();
            return Task.CompletedTask;
        }
    }
}

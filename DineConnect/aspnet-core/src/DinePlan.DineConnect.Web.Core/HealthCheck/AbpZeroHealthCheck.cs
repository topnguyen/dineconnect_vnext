﻿using Microsoft.Extensions.DependencyInjection;
using DinePlan.DineConnect.HealthChecks;

namespace DinePlan.DineConnect.Web.HealthCheck
{
    public static class AbpZeroHealthCheck
    {
        public static IHealthChecksBuilder AddAbpZeroHealthCheck(this IServiceCollection services)
        {
            var builder = services.AddHealthChecks();
            builder.AddCheck<DineConnectDbContextHealthCheck>("Database Connection");
            builder.AddCheck<DineConnectDbContextUsersHealthCheck>("Database Connection with user check");
            builder.AddCheck<CacheHealthCheck>("Cache");

            // add your custom health checks here
            // builder.AddCheck<MyCustomHealthCheck>("my health check");

            return builder;
        }
    }
}

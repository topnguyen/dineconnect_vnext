﻿using System;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Json;
using Abp.UI;
using Microsoft.AspNetCore.Http;
using DinePlan.DineConnect.Security.Recaptcha;
using Owl.reCAPTCHA;
using Owl.reCAPTCHA.v3;

namespace DinePlan.DineConnect.Web.Security.Recaptcha
{
    public class RecaptchaValidator : DineConnectServiceBase, IRecaptchaValidator, ITransientDependency
    {
        public const string RecaptchaResponseKey = "g-recaptcha-response";

        private readonly IreCAPTCHASiteVerifyV3 _reCaptchaSiteVerifyV3;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RecaptchaValidator(IreCAPTCHASiteVerifyV3 reCaptchaSiteVerifyV3, IHttpContextAccessor httpContextAccessor)
        {
            _reCaptchaSiteVerifyV3 = reCaptchaSiteVerifyV3;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task ValidateAsync(string captchaResponse)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext == null)
            {
                throw new Exception("RecaptchaValidator should be used in a valid HTTP context!");
            }

            if (captchaResponse.IsNullOrEmpty())
            {
                throw new UserFriendlyException(L("CaptchaCanNotBeEmpty"));
            }

            var response = await _reCaptchaSiteVerifyV3.Verify(new reCAPTCHASiteVerifyRequest
            {
                Response = captchaResponse,
                RemoteIp = _httpContextAccessor.HttpContext.Connection?.RemoteIpAddress?.ToString()
            });

            if (!response.Success || response.Score < 0.5)
            {
                Logger.Warn(response.ToJsonString());
                throw new UserFriendlyException(L("IncorrectCaptchaAnswer"));
            }
        }
    }
}

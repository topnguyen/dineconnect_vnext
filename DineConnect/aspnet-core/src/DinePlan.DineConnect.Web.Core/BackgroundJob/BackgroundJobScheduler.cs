﻿using Abp.Dependency;
using Abp.Quartz;
using Microsoft.Extensions.Configuration;
using Quartz;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Web.BackgroundJob
{
    public class BackgroundJobScheduler
    {
        public static void Schedule(IIocManager iocManager, IConfigurationRoot appConfiguration)
        {
            var workManager = iocManager.Resolve<IQuartzScheduleJobManager>();

            var tasks = new List<Task>
            {
                SendEmailing(workManager),
            };

            Task.WhenAll(tasks);
        }
        //check 1p
        private static async Task SendEmailing(IQuartzScheduleJobManager workManager)
        {
            await workManager.ScheduleAsync<Job.Emailing>(
                job =>
                {
                    job.WithIdentity("Emailing")
                        .WithDescription("Emailing, Emailing, Emailing");
                },
                trigger =>
                {
                    trigger.StartNow()
                    .WithIdentity("Emailing")
                     .WithCronSchedule("0 0 0 ? * * *")
                     .Build();
                });
        }
    }
}

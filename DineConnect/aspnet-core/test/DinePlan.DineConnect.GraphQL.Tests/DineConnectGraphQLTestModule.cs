﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using DinePlan.DineConnect.Configure;
using DinePlan.DineConnect.Startup;
using DinePlan.DineConnect.Test.Base;

namespace DinePlan.DineConnect.GraphQL.Tests
{
    [DependsOn(
        typeof(DineConnectGraphQLModule),
        typeof(DineConnectTestBaseModule))]
    public class DineConnectGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DineConnectGraphQLTestModule).GetAssembly());
        }
    }
}
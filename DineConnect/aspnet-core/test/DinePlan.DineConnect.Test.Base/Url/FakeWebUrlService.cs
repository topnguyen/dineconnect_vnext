using System.Collections.Generic;
using DinePlan.DineConnect.Url;

namespace DinePlan.DineConnect.Test.Base.Url
{
    public class FakeWebUrlService : IWebUrlService
    {
        public string WebSiteRootAddressFormat { get; }

        public string ServerRootAddressFormat { get; }

        public bool SupportsTenancyNameInUrl { get; }

        public string GetSiteRootAddress(string tenancyName = null)
        {
            return "http://test.com/";
        }

        public string GetServerRootAddress(string tenancyName = null)
        {
            return "http://test.com/";
        }

        public List<string> GetRedirectAllowedExternalWebSites()
        {
            return new List<string>();
        }

        public string GetSiteTiffinRootAddress(string tenancyName = null)
        {
            return "http://test.com/";
        }
    }
}
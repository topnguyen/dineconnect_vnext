﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Security.Recaptcha;

namespace DinePlan.DineConnect.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}

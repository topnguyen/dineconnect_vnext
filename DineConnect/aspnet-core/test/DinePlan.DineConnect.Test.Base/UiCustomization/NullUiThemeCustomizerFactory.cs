﻿using System.Threading.Tasks;
using DinePlan.DineConnect.UiCustomization;

namespace DinePlan.DineConnect.Test.Base.UiCustomization
{
    public class NullUiThemeCustomizerFactory : IUiThemeCustomizerFactory
    {
        public Task<IUiCustomizer> GetCurrentUiCustomizer()
        {
            return Task.FromResult(new NullThemeUiCustomizer() as IUiCustomizer);
        }

        public IUiCustomizer GetUiCustomizer(string theme)
        {
            return new NullThemeUiCustomizer();
        }

        public IWheelUiCustomizer GetWheelUiCustomizer(string theme)
        {
            return new NullWheelThemeUiCustomizer();
        }
    }
}

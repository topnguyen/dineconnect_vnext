﻿using Abp.Dependency;
using Abp.Reflection.Extensions;
using Microsoft.Extensions.Configuration;
using DinePlan.DineConnect.Configuration;

namespace DinePlan.DineConnect.Test.Base.Configuration
{
    public class TestAppConfigurationAccessor : IAppConfigurationAccessor, ISingletonDependency
    {
        public IConfigurationRoot Configuration { get; }

        public TestAppConfigurationAccessor()
        {
            Configuration = AppConfigurations.Get(
                typeof(DineConnectTestBaseModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }
    }
}

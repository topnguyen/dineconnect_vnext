﻿using Abp.Modules;
using DinePlan.DineConnect.Test.Base;

namespace DinePlan.DineConnect.Tests
{
    [DependsOn(typeof(DineConnectTestBaseModule))]
    public class DineConnectTestModule : AbpModule
    {
       
    }
}

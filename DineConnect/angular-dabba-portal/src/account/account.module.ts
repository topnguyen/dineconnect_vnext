import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { DineConnectCommonModule } from "@shared/common/common.module";
import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { AccountRoutingModule } from "./account-routing.module";
import { AccountComponent } from "./account.component";
import { AccountRouteGuard } from "./auth/account-route-guard";
import { ConfirmEmailComponent } from "./email-activation/confirm-email.component";
import { EmailActivationComponent } from "./email-activation/email-activation.component";
import { LanguageSwitchComponent } from "./language-switch.component";
import { LoginComponent } from "./login/login.component";
import { LoginService } from "./login/login.service";
import { SendTwoFactorCodeComponent } from "./login/send-two-factor-code.component";
import { ValidateTwoFactorCodeComponent } from "./login/validate-two-factor-code.component";
import { ForgotPasswordComponent } from "./password/forgot-password.component";
import { ResetPasswordComponent } from "./password/reset-password.component";
import { RegisterTenantResultComponent } from "./register/register-tenant-result.component";
import { RegisterTenantComponent } from "./register/register-tenant.component";
import { RegisterComponent } from "./register/register.component";
import { SelectEditionComponent } from "./register/select-edition.component";
import { TenantRegistrationHelperService } from "./register/tenant-registration-helper.service";
import { TenantChangeModalComponent } from "./shared/tenant-change-modal.component";
import { TenantChangeComponent } from "./shared/tenant-change.component";
import { OAuthModule } from "angular-oauth2-oidc";
import { LocaleMappingService } from "@shared/locale-mapping.service";
import { PasswordModule } from "primeng/password";
import {
    CustomersServiceProxy,
    CommonServiceProxy,
    TenantSettingsServiceProxy,
    TiffinMemberPortalServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppModule } from "@app/app.module";
import { MemberComponent } from "./member/member.component";
import { CreateMemberComponent } from "./member/create-member/create-member.component";
import { ForgotPasswordMemberComponent } from "./member/forgot-password-member/forgot-password-member.component";
import { TextMaskModule } from "angular2-text-mask";
import { SessionLockScreenComponent } from "./login/session-lock-screen.component";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import { CommonModule } from "@angular/common";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ReCaptchaV3Service, ScriptService } from "ngx-captcha";
// import { CollapseModule } from "ngx-bootstrap/collapse";


export function getRecaptchaLanguage(): string {
    return new LocaleMappingService().map('recaptcha', abp.localization.currentLanguage.name);
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        ModalModule.forRoot(),
        DineConnectCommonModule,
        UtilsModule,
        ServiceProxyModule,
        AccountRoutingModule,
        OAuthModule.forRoot(),
        PasswordModule,
        AppModule,
        BsDropdownModule.forRoot(),
        TabsModule,
        // CollapseModule.forRoot(),
        TextMaskModule,
        AppBsModalModule
    ],
    declarations: [
        AccountComponent,
        TenantChangeComponent,
        TenantChangeModalComponent,
        LoginComponent,
        RegisterComponent,
        RegisterTenantComponent,
        RegisterTenantResultComponent,
        SelectEditionComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        EmailActivationComponent,
        ConfirmEmailComponent,
        SendTwoFactorCodeComponent,
        ValidateTwoFactorCodeComponent,
        LanguageSwitchComponent,
        SessionLockScreenComponent,
        MemberComponent,
        CreateMemberComponent,
        ForgotPasswordMemberComponent,
    ],
    exports: [
        MemberComponent,
        CreateMemberComponent,
        ForgotPasswordMemberComponent,
    ],
    providers: [
        ScriptService,
        ReCaptchaV3Service,
        // TenantSettingsServiceProxy,
        // TiffinMemberPortalServiceProxy,
        LoginService,
        CustomersServiceProxy,
        CommonServiceProxy,
        TenantRegistrationHelperService,
        AccountRouteGuard
    ],
})
export class AccountModule {}

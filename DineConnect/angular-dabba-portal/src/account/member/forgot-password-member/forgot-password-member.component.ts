import { Component, OnInit, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    SendPasswordResetCodeInput,
    AccountServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppUrlService } from "@shared/common/nav/app-url.service";
import { Router } from "@angular/router";
import { finalize } from "rxjs/operators";

@Component({
    selector: "forgot-password-member",
    templateUrl: "./new-forgot-password-member.component.html",
    styleUrls: ["./forgot-password-member.component.scss"],
    animations: [appModuleAnimation()],
})
export class ForgotPasswordMemberComponent extends AppComponentBase {
    model: SendPasswordResetCodeInput = new SendPasswordResetCodeInput();

    saving = false;

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _appUrlService: AppUrlService,
        private _router: Router
    ) {
        super(injector);
    }

    save(): void {
        this.saving = true;
        this._accountService
            .sendPasswordResetCodeTiffin(this.model)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.message
                    .success(
                        this.l("PasswordResetMailSentMessage"),
                        this.l("MailSent")
                    )
                    .then(() => {
                        this._router.navigate(["portal/login"]);
                    });
            });
    }
}

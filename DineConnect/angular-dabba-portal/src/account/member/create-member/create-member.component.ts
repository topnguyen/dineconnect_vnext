import { Component, OnInit, Injector, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    PasswordComplexitySetting,
    ProfileServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
    AccountServiceProxy,
    RegisterOutput,
} from "@shared/service-proxies/service-proxies";
import { finalize, catchError } from "rxjs/operators";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppConsts } from "@shared/AppConsts";
import { ActivatedRoute, Router } from "@angular/router";
import { LoginService } from "@account/login/login.service";
import { RegisterMemberModel } from "./register-member.model";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import { ReCaptchaV3Service } from "ngx-captcha";

@Component({
    selector: "app-create-member",
    templateUrl: "./new-create-member.component.html",
    styleUrls: ["./create-member.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateMemberComponent extends AppComponentBase implements OnInit {
    model: RegisterMemberModel = new RegisterMemberModel();
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    recaptchaSiteKey: string = AppConsts.recaptchaSiteKey;

    saving = false;
    lstCity: ERPComboboxItem[] = [];
    lstCountry: ERPComboboxItem[] = [];
    callingCode: number;
    // zoneOptions = [
    //     { text: this.l("North"), value: 1 },
    //     { text: this.l("NorthWest"), value: 2 },
    //     { text: this.l("NorthEast"), value: 3 },
    //     { text: this.l("Central"), value: 4 },
    //     { text: this.l("West"), value: 5 },
    //     { text: this.l("East"), value: 6 },
    //     { text: this.l("South/Cbd"), value: 7 },
    // ];

    floorNo = "";
    unitNo = "";
    tower = "";
    numberMask: any;
    referrerReferralCode: string;
    questions = [
        { displayText: this.l("Google"), value: 1 },
        { displayText: this.l("FB Post"), value: 2 },
        { displayText: this.l("FB Ads"), value: 3 },
        { displayText: this.l("Friend Referral, Type the name of friend if through friend's referral"), value: 4 },
        { displayText: this.l("Word of Mouth"), value: 5 },
    ];
    answer: number;

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private readonly _loginService: LoginService,
        private _profileService: ProfileServiceProxy,
        private _commonService: CommonServiceProxy,
        private _reCaptchaV3Service: ReCaptchaV3Service
    ) {
        super(injector);

        this.referrerReferralCode = this._activatedRoute.snapshot.queryParams['referralCode'];
        this.model.acceptReceivePromotionEmail = true;
    }

    ngOnInit() {
        //Prevent to register new users in the host context
        // if (this.appSession.tenant == null) {
        //     this._router.navigate(['account/member']);
        //     return;
        // }

        this.initMaskInput();

        this._profileService
            .getPasswordComplexitySetting()
            .subscribe((result) => {
                this.passwordComplexitySetting = result.setting;
            });

        this._commonService
            .getLookups("Cities", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCity = result;
            });
        this._commonService
            .getLookups("Countries", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCountry = result;
            });
    }

    initMaskInput() {
        this.numberMask = createNumberMask({
            prefix: "",
            allowDecimal: false,
            autoGroup: true,
            allowLeadingZeroes: true,
            includeThousandsSeparator: false,
        });
    }

    get useCaptcha(): boolean {
        return this.setting.getBoolean(
            "App.UserManagement.UseCaptchaOnRegistration"
        );
    }

    save(): void {
        // if (this.useCaptcha && !this.model.captchaResponse) {
        //     this.message.warn(this.l('CaptchaCanNotBeEmpty'));
        //     return;
        // }
        this.model.address2 = this.setAddress2();

        this.saving = true;
        this.model.userName = this.model.emailAddress;
        this.model.referrerReferralCode = this.referrerReferralCode;
        this.model.jsonData = JSON.stringify({
            answer: this.answer,
        })

        this._accountService
            .registerMember(this.model)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result: RegisterOutput) => {
                if (!result.canLogin) {
                    this.notify.success(this.l("SuccessfullyRegistered"));
                    this._router.navigate(["/app/portal/member/buy-offer"]);
                    return;
                }

                //Autheticate
                this.saving = true;
                this._loginService.authenticateModel.userNameOrEmailAddress = this.model.userName;
                this._loginService.authenticateModel.password = this.model.password;
                this._loginService.authenticate(() => {
                    this.saving = false;
                });
            });

        // let recaptchaCallback = (token: string) => {
        //     this.saving = true;
        //     this.model.captchaResponse = token;
        //     this._accountService
        //         .registerMember(this.model)
        //         .pipe(
        //             finalize(() => {
        //                 this.saving = false;
        //             })
        //         )
        //         .subscribe((result: RegisterOutput) => {
        //             if (!result.canLogin) {
        //                 this.notify.success(this.l("SuccessfullyRegistered"));
        //                 this._router.navigate(["/app/portal/member/buy-offer"]);
        //                 return;
        //             }

        //             //Autheticate
        //             this.saving = true;
        //             this._loginService.authenticateModel.userNameOrEmailAddress = this.model.userName;
        //             this._loginService.authenticateModel.password = this.model.password;
        //             this._loginService.authenticate(() => {
        //                 this.saving = false;
        //             });
        //         });
        // };

        // this._reCaptchaV3Service.execute(
        //     this.recaptchaSiteKey,
        //     "register",
        //     (token) => {
        //         recaptchaCallback(token);
        //     }
        // );
    }

    captchaResolved(captchaResponse: string): void {
        this.model.captchaResponse = captchaResponse;
    }

    changeCountry(id) {
        this._commonService
            .getLookups("Cities", this.appSession.tenantId, id)
            .subscribe((result) => {
                this.lstCity = result;
            });

        this._commonService.getCountryCallCode(id).subscribe((result) => {
            this.callingCode = result;
        });
    }

    close() {}

    setAddress2(): string {
        let address2 = [];
        address2.push(this.floorNo);
        address2.push(this.unitNo);
        address2.push(this.tower);
        return address2.join();
    }
}

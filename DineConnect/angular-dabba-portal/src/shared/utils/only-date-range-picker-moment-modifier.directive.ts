import { Directive, Self, Output, EventEmitter, Input, SimpleChanges, OnDestroy, OnChanges, HostListener } from '@angular/core';
import { BsDatepickerDirective, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as moment from 'moment';
import compare from 'just-compare';

///this directive ensures that the date value will always be the moment.
@Directive({
    selector: '[onlyDateRangePickerMomentModifier]'
})
export class OnlyDateRangePickerMomentModifierDirective implements OnDestroy, OnChanges {
    subscribe: Subscription;
    lastDates: Date[] = null;
    @Output('ngModelChange') update = new EventEmitter();

    constructor(@Self() private bsDateRangepicker: BsDaterangepickerDirective) {
        this.subscribe = bsDateRangepicker.bsValueChange
            .pipe(filter(dates => !!(dates && dates[0] instanceof Date && dates[1] instanceof Date
                && !compare(this.lastDates, dates) && dates[0].toString() !== 'Invalid Date' && dates[1].toString() !== 'Invalid Date')))
            .subscribe((dates: Date[]) => {
                this.lastDates = [this.removeTimeZone(dates[0]), this.removeTimeZone(dates[1])];
                
                this.update.emit(this.lastDates);
            });
    }

    ngOnDestroy() {
        this.subscribe.unsubscribe();
    }

    ngOnChanges({ date }: SimpleChanges) {
        if (date && date.currentValue && !compare(date.currentValue, date.previousValue)) {
            var currentValue = [this.removeTimeZone(new Date(date.currentValue[0])), this.removeTimeZone(new Date(date.currentValue[1]))];
            setTimeout(() => this.bsDateRangepicker.bsValue = [currentValue[0], currentValue[1]], 0);
        }
    }

    removeTimeZone(date: Date) {
        date.setUTCFullYear(date.getFullYear(), date.getMonth(), date.getDate());
        date.setUTCHours(0, 0, 0, 0);
        
        return date;
    }
}

import {
    Component,
    Injector,
    NgZone,
    OnInit,
    TemplateRef,
    ElementRef,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as moment from 'moment';
import { TimeSettingDto, TimeSettingsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'timeZone',
    templateUrl: './timezone.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TimeZoneComponent extends AppComponentBase implements OnInit {
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    date: Date;
    public count = 0;
    @ViewChild('counter', { static: true }) public myCounter: ElementRef;

    TimeFormat = [];
    TimeZone = [];
    selectedFormate: string = null;
    selectedZone: string = null;
    modalRef: BsModalRef;
    config = {
        backdrop: true,
        class: 'modal-lg',
        ignoreBackdropClick: true
    };
    momentTime = moment();
    currentDateTime: string = null;
    constructor(
        injector: Injector,
        private zone: NgZone,
        private modalService: BsModalService,
        private _timeSettingsServiceProxy: TimeSettingsServiceProxy
    ) {
        super(injector);

        this.zone.runOutsideAngular(() => {
            setInterval(() => {
                if (this.selectedFormate) {
                    this.currentDateTime = this.momentTime
                        .add(1, 'second')
                        .tz(this.selectedZone)
                        .format(this.selectedFormate);
                }
            }, 1000);
        });
        this.TimeFormat = [
            { label: '12Hrs', value: 'll hh:mm:ss a' },
            { label: '24hrs', value: 'll H:m:s' }
        ];
        let timeZones = moment.tz.names();
        timeZones.forEach(element => {
            this.TimeZone.push({ label: element, value: element });
        });
    }

    ngOnInit() {
        this._timeSettingsServiceProxy.getTimeSettings()
            .subscribe(result => {
                this.selectedFormate = result.timeFormat;
                this.selectedZone = result.timeZone;
                this.momentTime = result.currentDateTime;
            });
    }

    show(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, this.config);
    }

    close() {
        this.modalService.hide(1);
    }

    save() {
        let timeSetting = new TimeSettingDto();
        timeSetting.timeFormat = this.selectedFormate;
        timeSetting.timeZone = this.selectedZone;

        this._timeSettingsServiceProxy
            .setTimeSettings(timeSetting)
            .subscribe(result => {
                this.modalService.hide(1);
                this.notify.success('Time Setting updated');
            });
    }
}

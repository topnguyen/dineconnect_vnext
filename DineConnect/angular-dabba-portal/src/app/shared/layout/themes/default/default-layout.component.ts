import { Injector, Component, OnInit, Inject } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ThemesLayoutBaseComponent } from "@app/shared/layout/themes/themes-layout-base.component";
import { UrlHelper } from "@shared/helpers/UrlHelper";
import { DOCUMENT } from "@angular/common";
import { OffcanvasOptions } from "@metronic/app/core/_base/layout/directives/offcanvas.directive";
import { AppConsts } from "@shared/AppConsts";
import { TiffinMemberProductOfferDto } from "@shared/service-proxies/service-proxies";
import { ToggleOptions } from "@metronic/app/core/_base/layout/directives/toggle.directive";

@Component({
    templateUrl: "./new-default-layout.component.html",
    styleUrls: ["./default-layout.component.less"],
    selector: "default-layout",
    animations: [appModuleAnimation()],
})
export class DefaultLayoutComponent extends ThemesLayoutBaseComponent
    implements OnInit {
    cartProductsCount = 0;

    menuCanvasOptions: OffcanvasOptions = {
        baseClass: 'aside',
        overlay: true,
        closeBy: 'kt_aside_close_btn',
        toggleBy: 'kt_aside_mobile_toggle'
    };

    userMenuToggleOptions: ToggleOptions = {
        target: this.document.body,
        targetState: 'topbar-mobile-on',
        toggleState: 'active'
    };

    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;

    constructor(
        injector: Injector,
        @Inject(DOCUMENT) private document: Document
    ) {
        super(injector);
    }

    ngOnInit() {
        this.installationMode = UrlHelper.isInstallUrl(location.href);
        this.registerToEvents();
    }

    registerToEvents() {
        abp.event.on(
            "app.chat.cartProductsCountChanged",
            (cartProductsCount) => {
                this.cartProductsCount = cartProductsCount;
            }
        );
    }
}

import { Component, Injector, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { LocationDto, LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'select-single-location',
  templateUrl: './select-single-location.component.html',
  styleUrls: ['./select-single-location.component.scss'],
  animations: [appModuleAnimation()]
})
export class SelectSingleLocationComponent extends AppComponentBase {
  @Input() selectedLocation: LocationDto;
  @Input() location: SingleLocationDto;

  @Output() locationAdded: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
  @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

  filterText = '';

  constructor(
    injector: Injector,
    private _locationServiceProxy: LocationServiceProxy
  ) {
    super(injector);
    this.primengTableHelper = new PrimengTableHelper();
  }

  show(): void {
    this.primengTableHelper.records = [];
    this.modal.show();
  }


  shown(): void {
    this.getLocations(null);
  }

  getLocations(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    this._locationServiceProxy.getList(
      this.filterText,
      false,
      0,
      this.primengTableHelper.getSorting(this.locationDataTable),
      this.primengTableHelper.getMaxResultCount(this.locationPaginator, event),
      this.primengTableHelper.getSkipCount(this.locationPaginator, event)
    )
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe(result => {
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.totalRecordsCount = result.totalCount;

        if (this.location) {
          this.selectedLocation = new LocationDto();
          this.selectedLocation.id = this.location.locationId;
          this.selectedLocation.name = this.location.locationName;
          // this.locationAdded.emit(this.location);
        }
      });
  }

  save() {
    this.location.locationId = this.selectedLocation.id;
    this.location.locationName = this.selectedLocation.name;

    this.selectedLocation = undefined;
    this.locationAdded.emit(this.location);
    this.close();
  }

  close() {
    this.modal.hide();
  }
}


export class SingleLocationDto {
  locationId: number;
  locationName: string;
}

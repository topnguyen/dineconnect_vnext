import { Component, OnInit, Injector } from '@angular/core';

import { maxBy, orderBy, uniqBy } from 'lodash';
import * as moment from 'moment';
import { finalize, map } from 'rxjs/operators';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import {
    ScheduleServiceProxy,
    GetScheduleForMenu,
    ScheduleDetailStatus,
    TiffinMealTimesServiceProxy,
    TiffinMealTimeDto,
    CreateScheduleDto
} from '@shared/service-proxies/service-proxies';
import { DateRange } from '@app/portal/services/order.service';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: [appModuleAnimation()]
})
export class MenuComponent extends AppComponentBase implements OnInit {
    dateRange: DateRange;
    current = new Date();
    mealTimes: TiffinMealTimeDto[];
    listschedule: GetScheduleForMenu[] = [];
    archive = false;
    loading = false;
    mealTimeId: number;
    dateRanges: DateRange[] = [];
    ScheduleDetailStatus = ScheduleDetailStatus;

    constructor(
        injector: Injector,
        private _scheduleServiceProxy: ScheduleServiceProxy,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.archive = true;
        forkJoin([
            this._getSchedules(),
            this._getMealTimes()
        ])
        .pipe(finalize(() => this.archive = false))
        .subscribe((results) => {
            this.getAll();
        });
    }

    onChangeWeek(date: DateRange) {
        this.dateRange = date;
        this.getAll();
    }

    onChangeMealTime(mealTime: TiffinMealTimeDto) {
        this.mealTimeId = mealTime.id;
        this.getAll();
    }

    getAll() {
        this.loading = true;
        this.current = this.dateRange[0];
        this._scheduleServiceProxy
            .getScheduleForMenu(
                undefined,
                undefined,
                moment(this.dateRange.startDate),
                moment(this.dateRange.endDate),
                ScheduleDetailStatus.Published,
                undefined,
                undefined,
                undefined
            )
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                //this.listschedule = orderBy(result, ['productSetName'], 'asc');
                this.listschedule = result;

                this.listschedule.forEach((item) => {
                    if (item.listSchedule.length > 0) {
                        item.listSchedule.unshift(item.listSchedule[item.listSchedule.length - 1]);
                        item.listSchedule.splice(item.listSchedule.length - 1, 1);
                    }

                    item.listSchedule.forEach((schedule) => {
                        if (this.mealTimeId) {
                            schedule.products = schedule.products.filter((p) => p.mealTimeId == this.mealTimeId);
                        }
                        schedule.products = uniqBy(schedule.products, (item) => {
                            item.productName = item.productName?.toLowerCase();
                            return item.productName;
                        });
                        schedule.products = orderBy(schedule.products, ['creationTime'], 'asc');
                    });
                });
            });
    }

    private _getSchedules() {
        return this._scheduleServiceProxy
            .getAll(undefined, undefined, moment().startOf('isoWeek'), undefined, ScheduleDetailStatus.Published, undefined, 0, 1000)
            .pipe(
                map((result) => {
                    this._initCalendar(result.items);
                    return result;
                })
            );
    }

    private _initCalendar(schedules: CreateScheduleDto[]) {
        let weeks = [0];
        if (schedules.length > 0) {
            const lastScheduleDate = maxBy(schedules, (s) => s.date).date;
            const diffWeekNumber = Math.round(lastScheduleDate.diff(moment().startOf('isoWeek'), 'weeks', true)) + 1;

            weeks = [...Array(diffWeekNumber).keys()];
            if (weeks.length == 0) {
                weeks = [0];
            }
        }

        this.dateRanges = weeks.map((week, i) => {
            return {
                id: week,
                name: 'Week ' + moment.utc().add(i, 'week').week(),
                startDate: moment.utc().startOf('week').add(i, 'week').toDate(),
                endDate: moment().utc().endOf('week').add(i, 'week').subtract('1', 'day').add('1', 'millisecond').toDate(),
                dateOfWeeks: Array.apply(null, Array(7)).map((e, j) => {
                    return {
                        timeSlots: [],
                        date: moment().utc().startOf('week').add(i, 'week').add(j, 'day')
                    };
                })
            };
        });
        this.dateRange = this.dateRanges[0];
    }

    get startDate(): Date {
        return this.dateRange ? this.dateRange[0] : null;
    }

    get endDate(): Date {
        return this.dateRange ? this.dateRange[1] : null;
    }

    private _getMealTimes() {
        return this._tiffinMealTimesServiceProxy.getAll(undefined, undefined, 0, 1000).pipe(
            map((result) => {
                this.mealTimes = result.items || [];
                const mealTimeDefault = this.mealTimes.find((item) => item.isDefault);
                this.mealTimeId = mealTimeDefault ? mealTimeDefault.id : null;
                return this.mealTimes;
            })
        );
    }
}

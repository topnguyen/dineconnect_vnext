import {
  Component,
  Injector,
  Output,
  EventEmitter,
  ViewEncapsulation,
  ViewChild,
  OnInit
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import {
  LocationDto,
  AddLocationGroupInput,
  LocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';

@Component({
  selector: 'select-location-modal',
  templateUrl: './select-location.component.html',
  styleUrls: ['./select-location.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class SelectLocationComponent extends AppComponentBase implements OnInit {
  groupId = 0;

  @Output()
  locationAdded: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
  @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

  selectedLocations: LocationDto[];

  filterTextForUserTable = '';
  locationNotInGroupTableHelper: PrimengTableHelper;
  organizationListDropDown = [];
  organizationSelected: number;
  selectedLocation: LocationDto;
  loadUserInLocation = false;
  constructor(
    injector: Injector,
    private _userLocationServiceProxy: LocationGroupServiceProxy
  ) {
    super(injector);
    this.locationNotInGroupTableHelper = new PrimengTableHelper();
  }

  show(): void {
    this.locationNotInGroupTableHelper.records = [];
    this.modal.show();
  }

  ngOnInit() { }

  shown(): void {
    this.getLocations(null);
  }

  getLocations(event?: LazyLoadEvent) {
    if (this.locationNotInGroupTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }
    this.locationNotInGroupTableHelper.showLoadingIndicator();
    this._userLocationServiceProxy.getLocationNotInGroup(
      this.filterTextForUserTable,
      0,
      this.groupId,
      this.locationNotInGroupTableHelper.getSorting(this.locationDataTable),
      this.locationNotInGroupTableHelper.getMaxResultCount(this.locationPaginator, event),
      this.locationNotInGroupTableHelper.getSkipCount(this.locationPaginator, event)
    )
      .subscribe(result => {
        this.locationNotInGroupTableHelper.records = result.items;
        this.locationNotInGroupTableHelper.totalRecordsCount =
          result.totalCount;
        this.locationNotInGroupTableHelper.records = result.items;
        this.locationNotInGroupTableHelper.hideLoadingIndicator();
      });
  }

  location() {
    // let a = new AddLocationGroupInput();
    // a.groupId = this.groupId;
    // a.locations = _.map(
    //   this.selectedLocations,
    //   selectedLocations => selectedLocations.id
    // );

    // this._userLocationServiceProxy
    //   .location(a)
    //   .subscribe(result => {
    //     this.notify.success(this.l('SuccessfullyAdded'));
    //     this.locationAdded.emit(true);
    //     this.modal.hide();
    //   });
  }

  close() {
    this.modal.hide();
  }
}


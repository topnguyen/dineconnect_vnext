import { Component, AfterViewInit, OnDestroy, ViewChild, ElementRef, Injector, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { AppComponentBase } from '@shared/common/app-component-base';

import { TiffinMemberPortalServiceProxy, FeedbackEmailInput } from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'app-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent extends AppComponentBase implements OnInit {
    feedbackMessage: string = '';
    saving = false;

    constructor(injector: Injector, public _portalServiceProxy: TiffinMemberPortalServiceProxy) {
        super(injector);
    }

    ngOnInit() {}

    save(): void {
        this.saving = true;
        let request = new FeedbackEmailInput();
        request.email = 'sales@dabbajunction.com';
        request.subject = 'Feedback : Dabba Junction';
        request.feedback = this.feedbackMessage;

        if (request.feedback.length > 0) {
            this._portalServiceProxy.sendFeedback(Object.assign({}, request)).subscribe((result) => {
                this.message.success('Thanks for the Feedback');
                this.feedbackMessage = '';
            });
        }
    }
}

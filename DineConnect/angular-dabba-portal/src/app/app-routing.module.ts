import { NgModule } from "@angular/core";
import {
    NavigationEnd,
    RouteConfigLoadEnd,
    RouteConfigLoadStart,
    Router,
    RouterModule,
} from "@angular/router";
import { AppComponent } from "./app.component";
import { AppRouteGuard } from "./shared/common/auth/auth-route-guard";
import { NgxSpinnerService } from "ngx-spinner";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "app",
                component: AppComponent,
                canActivate: [AppRouteGuard],
                canActivateChild: [AppRouteGuard],
                children: [
                    {
                        path: "portal",
                        loadChildren: () =>
                            import("app/portal/portal.module").then(
                                (m) => m.PortalModule
                            ), //Lazy load main module
                        data: { preload: true },
                    },
                    {
                        path: "**",
                        redirectTo: "portal",
                    },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
    constructor(
        private router: Router,
        private spinnerService: NgxSpinnerService
    ) {
        router.events.subscribe((event) => {
            if (event instanceof RouteConfigLoadStart) {
                spinnerService.show();
            }

            if (event instanceof RouteConfigLoadEnd) {
                spinnerService.hide();
            }

            if (event instanceof NavigationEnd) {
                document.querySelector('meta[property=og\\:url').setAttribute('content', window.location.href);
            }
        });
    }
}

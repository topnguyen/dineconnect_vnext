import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuyOfferComponent } from './member/buy-offer/buy-offer.component';
import { PaymentCompletedComponent } from './member/payment/payment-completed.component';
import { PaymentOrderCompletedComponent } from './member/payment/payment-order-completed.component';

import { CartProductsComponent } from './member/cart-products/cart-products.component';
import { CheckoutComponent } from './member/checkout/checkout.component';
import { BalanceComponent } from './member/balance/balance.component';
import { ProfileComponent } from './member/profile/profile.component';
import { OrderHistoryComponent } from './member/order-history/order-history.component';
import { PaymentHistoryComponent } from './member/payment-history/payment-history.component';
import { AddressProfileComponent } from './member/profile/address-profile/address-profile.component';
import { PaymentDetailComponent } from './member/payment-history/payment-detail.component';
import { ScheduleComponent } from './member/schedule/schedule.component';
import { CreateAddressProfileComponent } from './member/profile/address-profile/create-address-profile/create-address-profile.component';
import { TermAndConditionsComponent } from '@app/shared/common/term-and-conditions/term-and-conditions.component';
import { PrivacyComponent } from '@app/shared/common/privacy/privacy.component';
import { ContactComponent } from '@app/shared/common/contact/contact.component';
import { FaqComponent } from '@app/shared/common/faq/faq.component';
import { FeedbackComponent } from '@app/shared/common/feedback/feedback.component';
import { OrderComponent } from './member/orders/order.component';

const routes: Routes = [];

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'member/buy-offer', component: BuyOfferComponent },
                    {
                        path: 'member/cart-items',
                        component: CartProductsComponent,
                    },
                    { path: 'member/checkout', component: CheckoutComponent },
                    { path: 'member/balance', component: BalanceComponent },
                    { path: 'member/profile', component: ProfileComponent },
                    {
                        path: 'member/order-history',
                        component: OrderHistoryComponent,
                    },
                    {
                        path: 'member/payment-history',
                        component: PaymentHistoryComponent,
                    },
                    {
                        path: 'member/payment-detail/:id',
                        component: PaymentDetailComponent,
                    },
                    { path: 'member/order', component: OrderComponent },
                    {
                        path: 'member/profile/list-address',
                        component: AddressProfileComponent,
                    },
                    {
                        path: 'member/profile/address-detail/:id',
                        component: CreateAddressProfileComponent,
                    },
                    { path: 'member/schedule', component: ScheduleComponent },
                    { path: 'member/payment-completed/:id', component: PaymentCompletedComponent },
                    { path: 'member/payment-order-completed/:id', component: PaymentOrderCompletedComponent },

                    {
                        path: 'member/terms-conditions',
                        component: TermAndConditionsComponent,
                    },
                    {
                        path: 'member/privacy',
                        component: PrivacyComponent,
                    },
                    {
                        path: 'member/contact',
                        component: ContactComponent,
                    },
                    {
                        path: 'member/faq',
                        component: FaqComponent,
                    },
                    {
                        path: 'member/feedback',
                        component: FeedbackComponent,
                    },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class PortalRoutingModule {}

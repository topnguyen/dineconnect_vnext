import { AddonListDto, CreateOrderInput, PromotionCodeDiscountDetailPaymentDto, TiffinDeliveryTimeSlotType } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
export interface OrderMenu {
    items: OrderItem[];
    deliveryType: TiffinDeliveryTimeSlotType;
    deliverylocation: number;
    deliverylocationName: string;
    deliveryCharge: number;
    selfPickupLocation: number;
    selfPickupLocationName: string;
    selfPickupTimeSlot: any;
    paymentId: number;
    purcharsedOrderList: CreateOrderInput[];
    orderDateList: OrderDateList[];
}

export interface OrderDateList {
    date: moment.Moment,
    timeSlots: any[],
    mealTimeId: number,
    productSetId: number,
    productSetName: string,
    timeSlot: any,
    id: string;
    mealTimeName: string;
}
export interface OrderItem {
    id: number | undefined;
    customerProductOfferId: number;
    productSetId: number;
    date: moment.Moment;
    quantity: number;
    productSetName: string | undefined;
    mealTimeId: number;
    mealTimeName: string;
    addOn: AddonListDto[] | undefined;
}

export interface OrderFilter {
    productOfferId: number;
    customerProductOfferId: number;
    productSetId: number;
    mealTimeId: number;
    date: moment.Moment;
    productIds: number[];
    dateRange: DateRange;
    step: number;
    cutOffTime: number;
    cutOffDate: moment.Moment;
}

export interface DateOfWeek {
    date: moment.Moment;
    timeSlots: TimeSlot[];
}

export interface TimeSlot {
    from: string;
    to: string;
}

export interface DateRange {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
    dateOfWeeks: DateOfWeek[];
}

export interface AdyenState {
    isValid: true;
    data: {
        paymentMethod: {
            type: string;
            encryptedCardNumber: string;
            encryptedExpiryMonth: string;
            encryptedExpiryYear: string;
            encryptedSecurityCode: string;
            holderName: string;
        };
    };
}

export const SYSTEM_NAME = {
    Adyen: 'Payments.Adyen',
    Stripe: 'Payments.Stripe',
    Omise: 'Payments.Omise',
    PayPal: "Payments.PayPal",
    GooglePay: "Payments.Google",
};

export const PAYMENT_METHOD = {
    Adyen: 'adyen',
    Stripe: 'stripe',
    Omise: 'omise',
    Paynow: 'paynow',
    PayPal: "paypal",
    Google: "google",
};

export const STEP_ORDER = {
    ORDER: 1,
    DELIVERY: 2,
    PAYMENT: 3
};

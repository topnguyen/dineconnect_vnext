import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute } from "@angular/router";
import { LazyLoadEvent } from "primeng/api";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TiffinMemberPortalServiceProxy, EntityDto, TiffinOrderServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { ProductService } from "@app/shared/services/product.service";
import { Router } from "@angular/router";

@Component({
    selector: "app-payment-order-completed",
    templateUrl: "./payment-order-completed.component.html",
    styleUrls: ["./payment-order-completed.component.scss"],
    animations: [appModuleAnimation()],
})
export class PaymentOrderCompletedComponent extends AppComponentBase implements OnInit 
{
    public id: number;
    errorMessage  = "";
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _portalServiceProxy: TiffinOrderServiceProxy,
        private _productService: ProductService,
        private _router: Router,

    ) {
        super(injector);
    }

    ngOnInit() {
        this.id = Number(this._activatedRoute.snapshot.paramMap.get('id'));
        
        const input = new EntityDto();
        input.id = this.id;
        
        this._portalServiceProxy.validatePayment(input).subscribe((result) => {
           if(result.purchasedSuccess){
               this._productService.removeAllLocalCartProducts();
               this.message
               .success(this.l("PaymentedSuccessfully"), "", {
                   timer: 1500,
               })
               .then(() => {
                   this._router.navigate([
                    "/app/portal/member/order-history",
                   ]);
               });
           }else{
               this.errorMessage = result.message;
           }
        });            
     }
   
}

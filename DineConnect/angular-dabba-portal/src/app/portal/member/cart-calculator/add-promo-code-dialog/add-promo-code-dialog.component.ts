import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GetPromotionDiscountProductOfferInputDto, TiffinMemberProductOfferDto, TiffinPromotionServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-add-promo-code-dialog',
    templateUrl: './add-promo-code-dialog.component.html',
    styleUrls: ['./add-promo-code-dialog.component.scss']
})
export class AddPromoCodeDialogComponent extends AppComponentBase {

    @ViewChild('addPromoCodeModal', { static: true }) modal: ModalDirective;
    @ViewChild('addPromoCodeModalForm', { static: true }) addPromoCodeModalForm: FormGroup;

    @Output() onClose = new EventEmitter<any>();

    code: string;
    saving = false;
    products: TiffinMemberProductOfferDto[];
    promoCodes: string[] = [];
    isInvalidCode = false;

    constructor(
        injector: Injector,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
    ) {
        super(injector);
    }

    close(): void {
        this.hide();
    }

    onFocus() {
        this.setErrorCode();
    }

    setErrorCode(isError = false) {
        this.addPromoCodeModalForm.controls['promoCode'].setErrors({'code': isError});
    }

    save(): void {
        this._tiffinPromotionServiceProxy.getPromotionByCode(this.code, false, false)
            .pipe(
                catchError(error => {
                    return throwError(error);
                }),
                switchMap(result => {
                    if (result && result.id) {
                        const products = this.products.map(item => {
                            return {
                                id: item.id,
                                quantity: item.quantity
                            } as GetPromotionDiscountProductOfferInputDto;
                        });

                        return this._tiffinPromotionServiceProxy.getPromotionDiscountPayment(products, [this.code, ...this.promoCodes]);
                    } else {
                        this.setErrorCode(true);
                        this.notify.error(this.l('InvalidCode'));
                        return throwError(result);
                    }
                })
            ).subscribe(result => {
                if (result) {
                    this.notify.success(this.l('AddedVoucherSuccessfully'));
                    this.onClose.emit(result);
                    this.hide();
                }
            });
    }

    show(products: TiffinMemberProductOfferDto[], promoCodes: string[]): void {
        this.products = products;
        this.promoCodes = promoCodes;
        this.modal.show();
    }

    hide(): void {
        this.code = '';
        this.saving = false;
        this.products = [];
        this.promoCodes = [];
        this.modal.hide();
    }
}

import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinMemberPortalServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { interval, Subscription } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Component({
    selector: 'app-qr-code-dialog',
    templateUrl: './qr-code-dialog.component.html',
    styleUrls: ['./qr-code-dialog.component.scss']
})
export class QRCodeDialogComponent extends AppComponentBase {
    @ViewChild('QRCodeModal', { static: true }) modal: ModalDirective;

    @Output() onClose = new EventEmitter<any>();
    QRcode: string;
    total: number;

    private _chargeId: string;
    private _inteval$: Subscription;
    constructor(injector: Injector, private _tiffinMemberPortalServiceProxy: TiffinMemberPortalServiceProxy) {
        super(injector);
    }

    close(): void {
        this.QRcode = '';
        this._inteval$.unsubscribe();
        this.modal.hide();
    }

    show(QRcode: string, chargeId: string, total: number): void {
        this.QRcode = QRcode;
        this._chargeId = chargeId;
        this.total = total;
        this.modal.show();

        this._inteval$ = interval(2000)
            .pipe(concatMap(() => this._tiffinMemberPortalServiceProxy.getOmisePaymentStatus(this._chargeId)))
            .subscribe((result) => {
                if (result.isSuccess) {
                    this.onClose.emit(result);
                    this.close();
                }
            });
    }
}

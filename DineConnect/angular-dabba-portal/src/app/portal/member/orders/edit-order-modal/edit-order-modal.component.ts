import { Component, ViewChild, Output, EventEmitter, Injector, NgZone } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TiffinOrderServiceProxy, CreateOrderInput, TiffinMemberPortalServiceProxy, BalanceDetailDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    selector: 'edit-order-modal',
    templateUrl: './edit-order-modal.component.html',
    styleUrls: ['./edit-order-modal.component.scss']
})
export class EditOrderModalComponent extends AppComponentBase {
    @ViewChild('createModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    order = new CreateOrderInput();
    scheduleDateUtc: any;
    minDate = moment().toDate();
    cutOffTime = moment(abp.setting.get('App.MealSetting.OrderCutOffTime').replace(/"/g, '')).format('HH:mm');
    isInTime = true;
    selectedBalance: BalanceDetailDto = null;

    constructor(
        injector: Injector,
        private zone: NgZone,
        private _orderService: TiffinOrderServiceProxy,
        private _tiffinMemberPortalServiceProxy: TiffinMemberPortalServiceProxy
    ) {
        super(injector);

        this.zone.runOutsideAngular(() => {
            setInterval(() => {
                let currentDate = moment();

                this.isInTime = this.cutOffTime > currentDate.format('HH:mm');
                this.minDate = moment().add(1, 'days').toDate();

                if (!this.isInTime) {
                    this.minDate = currentDate.add(2, 'days').toDate();
                }
            }, 1000);
        });
    }

    show(orderId: number) {
        this.init(orderId);
        this.checkCutOffTime();

        this.modal.show();
        this.active = true;
    }

    checkCutOffTime() {
        let currentDate = moment();

        this.isInTime = this.cutOffTime > currentDate.format('HH:mm');
        this.minDate = moment().add(1, 'days').toDate();

        if (!this.isInTime) {
            this.minDate = currentDate.add(2, 'days').toDate();
        }
    }

    onShown(): void {}

    init(orderId) {
        this._orderService.getOrderForEdit(orderId).subscribe((result) => {
            this.order = result;
            this.scheduleDateUtc = this.order.orderDate.toDate();
            this._getBalancesForCustomer();
        });
    }

    save(): void {
        this.saving = true;

        this._orderService
            .updateDeliveryInfo(this.order)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.modalSave.emit(null);
                this.close();
            });
    }

    close(): void {
        this.order.quantity = 1;

        this.active = false;
        this.modal.hide();
    }

    dateChanged(date) {
        const momentDate = moment(date).add(-new Date().getTimezoneOffset(), 'minutes');
        this.scheduleDateUtc = momentDate;
    }

    private _getBalancesForCustomer() {
        this._tiffinMemberPortalServiceProxy.getBalancesForCustomer(undefined, false, undefined, undefined, 0, 1000).subscribe((result) => {
            const listOfBalance = result.items || [];

            this.selectedBalance = listOfBalance.find((balance) => balance.customerOfferOrderId == this.order.customerProductOfferId);
        });
    }

    get expiryDate() {
        return this.selectedBalance && this.selectedBalance.expiryDate.toDate();
    }
}

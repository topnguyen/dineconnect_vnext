import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    ERPComboboxItem,
    TiffinMemberPortalServiceProxy,
    UpdateMyAddressInputDto,
} from "@shared/service-proxies/service-proxies";
import { Router } from "@angular/router";

@Component({
    selector: "app-address-profile",
    templateUrl: "./new-address-profile.component.html",
    styleUrls: ["./address-profile.component.scss"],
})
export class AddressProfileComponent extends AppComponentBase
    implements OnInit {
    customerProfile: UpdateMyAddressInputDto = new UpdateMyAddressInputDto();
    lstCity: ERPComboboxItem[] = [];
    countryName = "";
    listAddress: UpdateMyAddressInputDto[] = [];

    zoneOptions = [
        { text: this.l("North"), value: 1 },
        { text: this.l("NorthWest"), value: 2 },
        { text: this.l("NorthEast"), value: 3 },
        { text: this.l("Central"), value: 4 },
        { text: this.l("West"), value: 5 },
        { text: this.l("East"), value: 6 },
        { text: this.l("South/Cbd"), value: 7 },
    ];

    propertyOptions = [
        { text: this.l("Hdb"), value: 1 },
        { text: this.l("Condo"), value: 2 },
        { text: this.l("Landed"), value: 3 },
        { text: this.l("University/School"), value: 4 },
    ];

    constructor(
        injector: Injector,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getAllAddress();
    }

    getAllAddress() {
        this._memberServiceProxy.getAllAddress().subscribe((result) => {
            this.listAddress = result;
            this.listAddress.forEach((item) => {
                item.address2 = this.getAddress2(item);
            });
        });
    }

    viewAddress(id?): void {
        this._router.navigate([
            "/app/portal/member/profile/address-detail",
            id ? id : "null",
        ]);
    }

    deleteAddress(id: number) {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._memberServiceProxy.deleteAddress(id).subscribe(() => {
                    this.getAllAddress();
                    this.notify.success(this.l("SuccessfullyDeleted"), "", {
                        timer: 500,
                    });
                });
            }
        });
    }

    setDefaultAddress(id: number) {
        this._memberServiceProxy.setDefaultAddress(id).subscribe(() => {
            this.notify.success(this.l("SuccessfullySetDefaultAddress"), "", {
                timer: 500,
            });
            this.getAllAddress();
        });
    }

    back() {
        this._router.navigate(["/app/portal/member/profile"]);
    }

    getAddress2(addressDetail: UpdateMyAddressInputDto): string {
        if (addressDetail.address2) {
            let addressSpilit = addressDetail.address2.split(",");
            let floorNo = "";
            let unitNo = "";
            let tower = "";
            if (addressSpilit[0]) {
                floorNo = addressSpilit[0];
            }
            if (addressSpilit[1]) {
                unitNo = addressSpilit[1];
            }
            if (addressSpilit[2]) {
                tower = addressSpilit[2];
            }
            let address2 = [];
            address2.push(floorNo);
            address2.push(unitNo);
            address2.push(tower);
            return address2
                .filter((x) => typeof x === "string" && x.length > 0)
                .join();
        }
    }
}

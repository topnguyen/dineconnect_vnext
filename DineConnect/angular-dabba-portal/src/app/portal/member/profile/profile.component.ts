import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Router } from "@angular/router";
import {
    CommonServiceProxy,
    ERPComboboxItem,
    TiffinMemberPortalServiceProxy,
    TotalOfferDto,
    MyInfoDto,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent extends AppComponentBase implements OnInit {
    customerProfile: MyInfoDto = new MyInfoDto();

    cityName = "";
    countryName = "";
    totalAddressDelivery = 0;
    lstCountry: ERPComboboxItem[] = [];
    totalOfferDto: TotalOfferDto = new TotalOfferDto();
    referralLink = "";
    public saving = false;
    constructor(
        injector: Injector,
        private _router: Router,
        private _commonService: CommonServiceProxy,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.customerProfile.address2 = "";
        this.getAddressProfile();
        this.getTotalCountOffer();
        this.getListCountry();
        this.getTotalDeliveryAddress();
    }

    redictEditAddressProfile(defaultAddresId) {
        this._router.navigate([
            '/app/portal/member/profile/address-detail',
            defaultAddresId,
        ]);
    }

    redictToBuyOffer() {
        this._router.navigate(['/app/portal/member/buy-offer']);
    }

    getAddressProfile() {
        this._memberServiceProxy.getMyInfo().subscribe((result) => {
            this.customerProfile = result;
            if (this.customerProfile.cityId) {
                this.getCityName(this.customerProfile.cityId);
            }

            if (result.referralCode) {
                this.referralLink = `${location.origin}/portal/register?referralCode=${result.referralCode}`;
            }

            if (this.customerProfile.countryId) {
                this.getCountryName(this.customerProfile.countryId);
            }
        });
    }

    getCityName(cityId: number) {
        this._commonService.getCityName(cityId).subscribe((result) => {
            this.cityName = result;
        });
    }

    getCountryName(countryId: number) {
        this._commonService.getCountryName(countryId).subscribe((result) => {
            this.countryName = result;
        });
    }

    getTotalCountOffer() {
        this._memberServiceProxy.getTotalOffer().subscribe((result) => {
            this.totalOfferDto = result;
        });
    }

    getListCountry() {
        this._commonService
            .getLookups("Countries", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCountry = result;
            });
    }

    settingCountry() {
        this.saving = true;
        this._memberServiceProxy
            .updateCountry(this.customerProfile.countryId)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.getCountryName(this.customerProfile.countryId);
            });
    }

    redictEditDeliveryAddress() {
        this._router.navigate(["/app/portal/member/profile/list-address"]);
    }

    getTotalDeliveryAddress() {
        this._memberServiceProxy.getAllAddress().subscribe((result) => {
            this.totalAddressDelivery = result.length;
        });
    }

    copy() {
        const copyText = document.getElementById("referralLink") as any;

        copyText.select();
        copyText.setSelectionRange(0, 99999);

        document.execCommand("copy");

        alert("Copied the referral link: " + copyText.value);
    }
}

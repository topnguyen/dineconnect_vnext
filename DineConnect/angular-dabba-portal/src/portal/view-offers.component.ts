import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Router } from "@angular/router";
import {
    TiffinMemberPortalServiceProxy,
    GetAllTiffinProductOffersInput,
    TiffinMemberProductOfferDto,
} from "@shared/service-proxies/service-proxies";
import { ProductService } from "@app/shared/services/product.service";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Location } from "@angular/common";

@Component({
    selector: "app-view-offers",
    templateUrl: "./new-view-offers.component.html",
    styleUrls: ["./view-offers.component.scss"],
    animations: [appModuleAnimation()],
})
export class ViewOffersComponent extends AppComponentBase implements OnInit {
    productOffers: TiffinMemberProductOfferDto[];

    constructor(
        injector: Injector,
        public _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private productService: ProductService,
        private _router: Router,
        private _location: Location
    ) {
        super(injector);
    }

    ngOnInit() {
        this._portalServiceProxy
            .getActiveOffers(new GetAllTiffinProductOffersInput())
            .subscribe((result) => {
                this.productOffers = result.items;
            });
    }

    loginToBuyOffer() {
        this._router.navigate(["/app/portal/member/buy-offer"]);
    }

    back() {
        this._location.back();
    }
}

import { Component, Injector, OnInit } from '@angular/core';

import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { finalize } from 'rxjs/operators';

import { AppComponentBase } from '@shared/common/app-component-base';

import { FaqListDto, TiffinFaqServiceProxy, TiffinHomeBannerServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-portal-index',
    templateUrl: './portal-index.component.html',
    styleUrls: ['./portal-index.component.scss'],
    providers: [
        {
            provide: CarouselConfig,
            useValue: { interval: 5000, noPause: true, showIndicators: false },
        },
    ],
})
export class PortalIndexComponent extends AppComponentBase implements OnInit {
    questions: FaqListDto[];
    bannerList: string[] = [];

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy,
        private _tiffinHomeBannerServiceProxy: TiffinHomeBannerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getFaqs();
        this.getHomeBanner();
    }

    getFaqs(): void {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinFaqServiceProxy.getFaqs()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.questions = result.items.filter(item => item.isDisplayAtHomePage);
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    getHomeBanner() {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinHomeBannerServiceProxy.getHomeBannerForEdit()
            .subscribe(result => {
                if (result.images) {
                    this.bannerList = result.images.split(',');
                }
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
}

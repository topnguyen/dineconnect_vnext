import { Injectable } from '@angular/core';
import { TiffinMemberProductOfferDto } from '@shared/service-proxies/service-proxies';
import { NgxSpinnerService } from 'ngx-spinner';
import { merge } from 'rxjs';

export class FavouriteProduct {
    product: TiffinMemberProductOfferDto;
    productId: string;
    userId: string;
}

@Injectable()
export class ProductService {
    products: TiffinMemberProductOfferDto[];
    product: TiffinMemberProductOfferDto;

    // NavbarCounts
    navbarCartCount = 0;

    constructor(private spinnerService: NgxSpinnerService) {
        this.calculateLocalCartProdCounts();
    }

    //----------  Cart Product Function  ----------

    // Adding new Product to cart db if logged in else localStorage
    addToCart(
        finallyCallback?: () => void,
        data?: TiffinMemberProductOfferDto
    ): void {
        finallyCallback =
            finallyCallback ||
            (() => {
                this.spinnerService.hide();
            });
        let a: TiffinMemberProductOfferDto[];

        a = JSON.parse(localStorage.getItem('avct_item')) || [];

        let isNew = true;
        a.map(item => {
            if (item.id === data.id) {
                item.quantity++;
                isNew = false;
            }
        });

        if (isNew) {
            a.push(data);
        }
        //abp.notify.info('Adding Product to Cart', 'Product Adding to the cart');

        setTimeout(() => {
            localStorage.setItem('avct_item', JSON.stringify(a));
            this.calculateLocalCartProdCounts();
            finallyCallback();
        }, 500);
    }

    // Removing cart from local
    removeLocalCartProduct(product: TiffinMemberProductOfferDto) {
        const products: TiffinMemberProductOfferDto[] = JSON.parse(
            localStorage.getItem('avct_item')
        );

        for (let i = 0; i < products.length; i++) {
            if (products[i].id === product.id) {
                products.splice(i, 1);
                break;
            }
        }
        // ReAdding the products after remove
        localStorage.setItem('avct_item', JSON.stringify(products));

        this.calculateLocalCartProdCounts();
    }

    removeAllLocalCartProducts() {
        localStorage.setItem('avct_item', JSON.stringify([]));
        this.calculateLocalCartProdCounts();
    }

    // Fetching Locat CartsProducts
    getLocalCartProducts(): TiffinMemberProductOfferDto[] {
        const products: TiffinMemberProductOfferDto[] = JSON.parse(localStorage.getItem('avct_item')) || [];
        return products;
    }

    // returning LocalCarts Product Count
    calculateLocalCartProdCounts() {
        this.navbarCartCount = 0;
        this.getLocalCartProducts().map(item => {
            this.navbarCartCount += +item.quantity;
        });

        abp.event.trigger(
            'app.chat.cartProductsCountChanged',
            this.navbarCartCount
        );
    }

    updateQuatity(product: TiffinMemberProductOfferDto) {
        const products: TiffinMemberProductOfferDto[] = JSON.parse(localStorage.getItem('avct_item'));

        products.map(item => {
            if (item.id === product.id) {
                item.quantity = product.quantity;
            }
        });

        // ReAdding the products after remove
        localStorage.setItem('avct_item', JSON.stringify(products));

        this.calculateLocalCartProdCounts();
    }

    updateLocalCartProducts(products: TiffinMemberProductOfferDto[]) {
        localStorage.setItem('avct_item', JSON.stringify(products));
    }
}

import { Router } from '@angular/router';
import { CliqueMenuItemDto, CliqueMenuItemServiceProxy, CliqueOrderDto, CliqueCustomerServiceProxy, CliqueOrderServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';

import { maxBy, orderBy, uniqBy } from 'lodash';
import * as moment from 'moment';
import { finalize, map } from 'rxjs/operators';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import {
    ScheduleServiceProxy,
    GetScheduleForMenu,
    ScheduleDetailStatus,
    TiffinMealTimesServiceProxy,
    TiffinMealTimeDto,
    CreateScheduleDto,
    ProductGroupDto,
    CliqueMealTimeDto,
    ScreenMenuCategoryDto,
    ProfileServiceProxy,
    CliqueCustomerLocationInput
} from '@shared/service-proxies/service-proxies';
import { DateRange } from '@app/portal/services/order.service';
import { forkJoin } from 'rxjs';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { DataService } from '../service/data.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogLogoutComponent } from '../dialog-logout/dialog-logout.component';
import { ProductService } from '@app/shared/services/product.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: [appModuleAnimation()]
})
export class MenuComponent extends AppComponentBase implements OnInit {
    dateRange: DateRange;
    current = new Date();
    mealTimes: TiffinMealTimeDto[];
    listschedule: GetScheduleForMenu[] = [];
    archive = false;
    loading = false;
    mealTimeId: number;
    dateRanges: DateRange[] = [];
    ScheduleDetailStatus = ScheduleDetailStatus;
    isShowOrder: boolean = false;
    screenMenuCategoryList: Array<ScreenMenuCategoryDto>
    @ViewChild('slickModal') slickModal: SlickCarouselComponent;
    updateLocationCustomer: CliqueCustomerLocationInput = new CliqueCustomerLocationInput();
    // hotels: [{img: string, name: string}];

    slideConfig = {
        "slidesToShow": 11,
        "slidesToScroll": 1,
        "infinite": false,
        "draggable": false
    };
    slideConfig7 = {
        "slidesToShow": 7,
        "slidesToScroll": 1,
        "infinite": false,
        "draggable": false
    };
    slideConfig4 = {
        "slidesToShow": 4,
        "slidesToScroll": 4,
        "infinite": false,
        "draggable": true,
        'cssEase': 'cubic-bezier(0.250,  0.060, 0.050, 0.040)'
    };

    isSelectedFavorite: boolean = false;
    totalCount: number;
    productInCartList: Array<CliqueMenuItemDto> = [];
    productAddToCart: CliqueMenuItemDto;
    productGroupSelected: ProductGroupDto;
    cliqueMealTimeList: Array<CliqueMealTimeDto>;
    thuSchedule: string;
    daySchedule: any;
    currentSlideTemp: number = 0;
    messageMobile: string;
    constructor(
        injector: Injector,
        private _scheduleServiceProxy: ScheduleServiceProxy,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy,
        private _screenMenuCategoryDtoServiceProxy: CliqueMenuItemServiceProxy,
        private _dataService: DataService,
        private _cliqueCustomerService: CliqueCustomerServiceProxy,
        private _cliqueOrderService: CliqueOrderServiceProxy,
        private router: Router,
        private dialog: MatDialog,
        public productService: ProductService
    ) {
        super(injector);
    }
    productOrdersDto: CliqueOrderDto = new CliqueOrderDto();
    async ngOnInit() {
    const hours = new Date().getHours();
    this.messageMobile = hours < 12 ? 'Good Morning' : hours < 18 ? 'Good Afternoon' : 'Good Evening';
        const countryId = localStorage.getItem("countryIdFirst");
        if(countryId && countryId != ''){
            this.updateLocationCustomer.countryId = +countryId;
            this.updateLocationCustomer.userId = this.appSession.userId;
            this._cliqueCustomerService.updateLocationCustomer(this.updateLocationCustomer).subscribe(
                () => {
                    localStorage.removeItem("countryIdFirst");
                }
            );
        }
        this.archive = true;
        forkJoin([
            this._getSchedules(),
            this._getMealTimes()
        ])
            .pipe(finalize(() => this.archive = false))
            .subscribe((results) => {
                this.getAll();
            });
        await this.getCliqueMealTime();
        this.getProductInCart();
        this._dataService.currentData.subscribe(data => {
            if (data[1] == "productGroup") {
                this.productGroupSelected = data[0];
            }
            if (data[1] == "decrease") {
                // this.productList.find(productItem=>productItem.screenMenuItemId == data[0].screenMenuItemId).isAddToCart = false;
                const productIndex = this.productOrdersDto.productOrders.findIndex(productItem => productItem.screenMenuItemId == data[0].screenMenuItemId
                    && productItem.cliqueMealTimeId == data[0].cliqueMealTimeId 
                    && moment(data[0].orderDate).format('YYYY-MM-DD') == moment(localStorage.getItem("daySchedule")).format('YYYY-MM-DD')
                );
                if (productIndex != -1) {
                    this.productOrdersDto.countItems--;
                    this.productOrdersDto.productOrders.splice(productIndex, 1);
                }
                return;
            }
            if (data[1] == "isShowOrder") {
                this.isShowOrder = data[0].isShowOrder;
            }
            if (data[1] == "thuSchedule") {
                this.thuSchedule = data[0].thu;
                this.daySchedule = data[0].day;
                this.clickCliqueMealTime(this.cliqueMealTimeList[0], false);
              }
        })
    }

    onChangeWeek(date: DateRange) {
        this.dateRange = date;
        this.getAll();
    }

    onChangeMealTime(mealTime: TiffinMealTimeDto) {
        this.mealTimeId = mealTime.id;
        this.getAll();
    }

    goToSetting() {
        this.router.navigate(["/app/portal/member/settings-common"]);
    }

    logout(): void {
        // this._authService.logout();
        const dialogRef = this.dialog.open(DialogLogoutComponent, {
            width: '529px',
            data: {},
            panelClass: 'dialog-logout'
        });

        dialogRef.afterClosed().subscribe(result => {
            this.productService.removeAllLocalCartProducts();
            console.log('The dialog was closed');
        });
    }

    getAll() {
        this.loading = true;
        this.current = this.dateRange[0];
        this._scheduleServiceProxy
            .getScheduleForMenu(
                undefined,
                undefined,
                moment(this.dateRange.startDate),
                moment(this.dateRange.endDate),
                ScheduleDetailStatus.Published,
                undefined,
                undefined,
                undefined
            )
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                //this.listschedule = orderBy(result, ['productSetName'], 'asc');
                this.listschedule = result;

                this.listschedule.forEach((item) => {
                    if (item.listSchedule.length > 0) {
                        item.listSchedule.unshift(item.listSchedule[item.listSchedule.length - 1]);
                        item.listSchedule.splice(item.listSchedule.length - 1, 1);
                    }

                    item.listSchedule.forEach((schedule) => {
                        if (this.mealTimeId) {
                            schedule.products = schedule.products.filter((p) => p.mealTimeId == this.mealTimeId);
                        }
                        schedule.products = uniqBy(schedule.products, (item) => {
                            item.productName = item.productName?.toLowerCase();
                            return item.productName;
                        });
                        schedule.products = orderBy(schedule.products, ['creationTime'], 'asc');
                    });
                });
            });
    }

    private _getSchedules() {
        return this._scheduleServiceProxy
            .getAll(undefined, undefined, moment().startOf('isoWeek'), undefined, ScheduleDetailStatus.Published, undefined, 0, 1000)
            .pipe(
                map((result) => {
                    this._initCalendar(result.items);
                    return result;
                })
            );
    }

    private _initCalendar(schedules: CreateScheduleDto[]) {
        let weeks = [0];
        if (schedules.length > 0) {
            const lastScheduleDate = maxBy(schedules, (s) => s.date).date;
            const diffWeekNumber = Math.round(lastScheduleDate.diff(moment().startOf('isoWeek'), 'weeks', true)) + 1;

            weeks = [...Array(diffWeekNumber).keys()];
            if (weeks.length == 0) {
                weeks = [0];
            }
        }

        this.dateRanges = weeks.map((week, i) => {
            return {
                id: week,
                name: 'Week ' + moment.utc().add(i, 'week').week(),
                startDate: moment.utc().startOf('week').add(i, 'week').toDate(),
                endDate: moment().utc().endOf('week').add(i, 'week').subtract('1', 'day').add('1', 'millisecond').toDate(),
                dateOfWeeks: Array.apply(null, Array(7)).map((e, j) => {
                    return {
                        timeSlots: [],
                        date: moment().utc().startOf('week').add(i, 'week').add(j, 'day')
                    };
                })
            };
        });
        this.dateRange = this.dateRanges[0];
    }

    get startDate(): Date {
        return this.dateRange ? this.dateRange[0] : null;
    }

    get endDate(): Date {
        return this.dateRange ? this.dateRange[1] : null;
    }

    private _getMealTimes() {
        return this._tiffinMealTimesServiceProxy.getAll(undefined, undefined, 0, 1000).pipe(
            map((result) => {
                this.mealTimes = result.items || [];
                const mealTimeDefault = this.mealTimes.find((item) => item.isDefault);
                this.mealTimeId = mealTimeDefault ? mealTimeDefault.id : null;
                return this.mealTimes;
            })
        );
    }

    next() {
        this.slickModal.slickNext();
    }

    prev() {
        this.slickModal.slickPrev();
    }

    slickInit(_e: any) {
    }

    breakpoint(_e: any) {
    }

    beforeChange(_e: any) {
    }

    showCartOrder() {
        this.isShowOrder = !this.isShowOrder;
        this._dataService.changeData({ product: null, isShowOrder: this.isShowOrder }, "productToCart");
    }

    async getCliqueMealTime() {
        const result = await this._screenMenuCategoryDtoServiceProxy.getCliqueMealTime().toPromise();
        if (result) {
            this.cliqueMealTimeList = result.items;
            this.cliqueMealTimeList[0].isSelected = true;
            this.clickCliqueMealTime(this.cliqueMealTimeList[0], false);
            // this._dataService.changeData(this.cliqueMealTimeList[0], "mealTime");
            localStorage.setItem("cliqueMealTimeFirst", JSON.stringify(this.cliqueMealTimeList[0]));
        }
    }
    async clickCliqueMealTime(mealTime: CliqueMealTimeDto, click: boolean = true) {
        this.showMainSpinner();
        localStorage.setItem("cliqueMealTimeId", mealTime.id.toString());
        this.isSelectedFavorite = false;
        this.cliqueMealTimeList.forEach(mealTimeItem => {
            if (mealTimeItem.id === mealTime.id) {
                mealTimeItem.isSelected = true;
            } else {
                mealTimeItem.isSelected = false;
            }
        })
        const result = await this._screenMenuCategoryDtoServiceProxy.getScreenMenuCategory(mealTime.id, 
            this.thuSchedule ? this.thuSchedule : localStorage.getItem("thuSchedule"),
            moment(this.daySchedule).local().format('YYYY-MM-DD')).toPromise();
        if (result) {
            this.screenMenuCategoryList = result.items;
            if (this.screenMenuCategoryList.length > 0) {
                this.screenMenuCategoryList[0].isSelected = true;
                if (click) {
                    this._dataService.changeData({ isClickFavorite: false, screenMenuCategory: this.screenMenuCategoryList[0], mealTime }, "screenMenuCategory");
                } else {
                    localStorage.setItem("screenMenuCategoryFirst", JSON.stringify(this.screenMenuCategoryList[0]));
                    this._dataService.changeData({ isClickFavorite: false, screenMenuCategory: this.screenMenuCategoryList[0], mealTime }, "screenMenuCategory");
                }
            } else {
                this._dataService.changeData(null, "screenMenuCategory");
            }
            this.hideMainSpinner();
        }
        else {
            this.hideMainSpinner();
        }
    }
    clickFavorite() {
        this.showMainSpinner();
        if (!this.isSelectedFavorite) {
            this.cliqueMealTimeList.find(mealTimeItem => mealTimeItem.isSelected).isSelected = false;
            this.isSelectedFavorite = true;
            this.screenMenuCategoryList = [];
        }
        this._dataService.changeData(true, "favorite");
    }

    totalResult(event) {
        this.totalCount = event;
    }



    addToCart(product: any) {
        this.getProductInCart();
    }

    clickScreenMenuCategory(screenMenuCategory: ScreenMenuCategoryDto) {
        this.showMainSpinner();
        this._dataService.changeData({ isClickFavorite: false, screenMenuCategory }, "screenMenuCategory");
        this.screenMenuCategoryList.forEach(screenMenuCategoryItem => {
            if (screenMenuCategoryItem.screenMenuCategoryId === screenMenuCategory.screenMenuCategoryId) {
                screenMenuCategoryItem.isSelected = true;
            } else {
                screenMenuCategoryItem.isSelected = false;
            }
        })
    }
    getProductInCart() {
        const customerId = +localStorage.getItem("customerId");

        this._cliqueOrderService.getCliqueOrderCustomer(customerId).subscribe( res => {
        if (res) {
            this.productOrdersDto.productOrders = res.productOrders;
            this.productOrdersDto.countItems = res.countItems;
            }
        });
        
    }
    goToSideCart(){
        this.router.navigate(['/app/portal/member/side-cart']);
    }
}

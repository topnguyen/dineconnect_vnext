import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import moment from 'moment';
import { DataService } from '../service/data.service';
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  animations: [appModuleAnimation()]
})
export class ScheduleComponent implements OnInit {
  weekScheduleList = [];
  isShowDropdown: boolean = false;
  currentWeek: any;
  currentDayList: any = [];
  constructor(private _dataService: DataService) { }

  ngOnInit() {
    let result = this.getWeekNumber(new Date());
    this.currentWeek = this.getWeekRange(result[1], new Date().getFullYear());
    this.setCurrentDayList(this.currentWeek);
    this.currentDayList[new Date().getDay()].isCurrentDay = true;
    this._dataService.changeData({thu:this.currentDayList[new Date().getDay()].thu, day:this.currentDayList[new Date().getDay()].day}, "thuSchedule");
    localStorage.setItem("thuSchedule", this.currentDayList[new Date().getDay()].thu);
    localStorage.setItem("daySchedule", moment().local().format("YYYY-MM-DD"));
    let remainingWeek = 52 - result[1];

    for (let index = result[1]; index <= 52; index++) {
      this.weekScheduleList.push(this.getWeekRange(index, result[0]));
    }
    if (remainingWeek <= 52) {
      for (let index = 1; index < result[1]; index++) {
        this.weekScheduleList.push(this.getWeekRange(index, result[0] + 1));
      }
    }
  }

  getWeekRange(weekNo, yearNo) {
    let firstDayofYear = new Date(yearNo, 0, 1);
    if (firstDayofYear.getDay() > 4) {
      let weekStart = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7);
      let weekEnd5 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 5);
      let weekEnd4 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 4);
      let weekEnd3 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 3);
      let weekEnd2 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 2);
      let weekEnd1 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 1);
      let weekEnd = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 7 + 6);
      return { week: weekNo, startDay: weekStart, day2: weekEnd1, day3: weekEnd2, day4: weekEnd3, day5: weekEnd4, day6: weekEnd5, endDay: weekEnd }
    }
    else {
      let weekStart = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay());
      let weekEnd5 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 5);
      let weekEnd4 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 4);
      let weekEnd3 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 3);
      let weekEnd2 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 2);
      let weekEnd1 = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 1);
      let weekEnd = new Date(yearNo, 0, 1 + (weekNo - 1) * 7 - firstDayofYear.getDay() + 6);
      return { week: weekNo, startDay: weekStart, day2: weekEnd1, day3: weekEnd2, day4: weekEnd3, day5: weekEnd4, day6: weekEnd5, endDay: weekEnd }
    }
  }

  getWeekNumber(d) {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    let yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1)).getTime();
    let weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    return [d.getUTCFullYear(), weekNo];
  }
  toggleDropdown() {
    this.isShowDropdown = !this.isShowDropdown
  }
  selectSchedule(week: any) {
    
    this.currentWeek = week;
    this.isShowDropdown = false;
    this.currentWeek = this.getWeekRange(week.week, new Date().getFullYear());
    this.setCurrentDayList(this.currentWeek);
    this.currentDayList[new Date().getDay()].isCurrentDay = true;
    this.currentDayList[0].isActive = true;
    this.selectDay(this.currentDayList[0]);
  }
  selectDay(day: any) {
    // this._dataService.changeData(day.day, "daySchedule");
    this._dataService.changeData({thu:day.thu, day: moment(day.day).local().format("YYYY-MM-DD")}, "thuSchedule");
    localStorage.setItem("thuSchedule", day.thu);
    localStorage.setItem("daySchedule", moment(day.day).local().format("YYYY-MM-DD"));
  }
  activeDay(day: any) {
    this.currentDayList.forEach(dayItem => {
      if (dayItem.thu === day.thu) {
        dayItem.isActive = true;
      } else {
        dayItem.isActive = false;
      }
    });
  }
  setCurrentDayList(currentWeek) {
    this.currentDayList = [
      { thu: "Sunday", title: "SUN", day: currentWeek.startDay, isActive: false, isCurrentDay: false },
      { thu: "Monday", title: "MON", day: currentWeek.day2, isActive: false, isCurrentDay: false },
      { thu: "Tuesday", title: "TUE", day: currentWeek.day3, isActive: false, isCurrentDay: false },
      { thu: "Wednesday", title: "WED", day: currentWeek.day4, isActive: false, isCurrentDay: false },
      { thu: "Thursday", title: "THU", day: currentWeek.day5, isActive: false, isCurrentDay: false },
      { thu: "Friday", title: "FRI", day: currentWeek.day6, isActive: false, isCurrentDay: false },
      { thu: "Saturday", title: "SAT", day: currentWeek.endDay, isActive: false, isCurrentDay: false }
    ];
  }
}

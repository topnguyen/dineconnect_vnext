import {
  ProductGroupDto,
  CliqueProductOrderDto,
  CliqueOrderDto,
  CliqueOrderServiceProxy,
  CliqueOrderInput
} from './../../../../shared/service-proxies/service-proxies';
import { Component, Input, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CliqueMenuItemDto } from '@shared/service-proxies/service-proxies';
import { DataService } from '../service/data.service';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OrderTag } from '@app/shared/model/order-tag.model';

@Component({
  selector: 'app-side-cart',
  templateUrl: './side-cart.component.html',
  styleUrls: ['./side-cart.component.scss'],
  animations: [appModuleAnimation()]
})
export class SideCartComponent extends AppComponentBase implements OnInit {
  @Input() productInCart: CliqueMenuItemDto;

  weekScheduleList = [];
  isShowDropdown: boolean = false;
  currentWeek: any;
  currentDayList: any = [];
  productInCartList: Array<CliqueMenuItemDto> = [];
  productGroupSelected: ProductGroupDto;
  totalPriceAllProduct: number = 0;
  productOrderList: Array<CliqueProductOrderDto>;
  productOrderDto: CliqueOrderDto;
  private subscriptions = new Subscription();

  get total() {
    let orderTagTotal = 0;
    (this.productOrderList || []).forEach((element) => {
      const orderTags: OrderTag[] = <any>element.orderTags || [];

      orderTags.forEach((item) => {
        orderTagTotal += item.price * item.quantity;
      });
    });

    return this.productOrderDto?.total + orderTagTotal;
  }

  constructor(
    injector: Injector,
    private dataService: DataService,
    private router: Router,
    private _dataService: DataService,
    private cliqueOrderServiceProxy: CliqueOrderServiceProxy
  ) {
    super(injector);
  }

  ngOnInit() {
    const SCREEN_MOBILE = 991;
    if (window.innerWidth <= SCREEN_MOBILE) {
      this.getProductInCart();
    } else {
      this.subscriptions.add(
        this.dataService.currentData.subscribe((data) => {
          this.totalPriceAllProduct = 0;
          if (data[1] == 'addOrRemoveToCart') {
            const product = data[0].product;
            product.currentDay = new Date();
            product.currentThu = this.getCurrentDay();
            product.quantity = 1;
            product.totalPrice = product.price * product.quantity;
            if (this.productInCartList.some((productItem) => productItem.id === product.id)) {
              this.productInCartList = this.productInCartList.filter((productItem) => productItem.id !== product.id);
            } else {
              this.productInCartList.push(product);
            }
            this.productInCartList.forEach((product) => {
              this.totalPriceAllProduct += product.totalPrice;
            });
          } else if (data[1] == 'showCart') {
            this.productOrderList = data[0].productOrders;
            this.productOrderList.forEach((item) => {
              if (typeof item.orderTags === 'string' && item.orderTags) {
                item.orderTags = JSON.parse(item.orderTags);
              }
            });
            this.productOrderDto = data[0];
          } else if (data[1] == 'productToCart' && data[0].isShowOrder) {
            this.getProductInCart();
          }
        })
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
  getCurrentDay(): string {
    const date = new Date();
    let thu = '';
    const result = date.getDay();
    switch (result) {
      case 0:
        thu = 'Sun';
        break;
      case 1:
        thu = 'Mon';
        break;
      case 2:
        thu = 'Tue';
        break;
      case 3:
        thu = 'Wed';
        break;
      case 4:
        thu = 'Thu';
        break;
      case 5:
        thu = 'Fri';
        break;
      case 6:
        thu = 'Sat';
        break;
      default:
        break;
    }
    return thu;
  }

  decrease(product: CliqueProductOrderDto) {
    product.quantity--;
    const cliqueOrderInput = this.setCliqueOrderInput(product);
    cliqueOrderInput.isAddToCart = true;
    if (product.quantity == 0) {
      cliqueOrderInput.isAddToCart = false;
      this.productOrderList = this.productOrderList.filter(
        (productItem) =>
          productItem.cliqueMealTimeId !== product.cliqueMealTimeId ||
          productItem.screenMenuItemId !== product.screenMenuItemId ||
          productItem.orderDate !== product.orderDate
      );
    }
    this.productOrderDto.total -= product.price;
    this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(cliqueOrderInput).subscribe((result) => {
      if (cliqueOrderInput.quantity == 0) {
        this._dataService.changeData(cliqueOrderInput, 'decrease');
      }
    });
  }
  increase(product: CliqueProductOrderDto) {
    product.quantity++;
    const cliqueOrderInput = this.setCliqueOrderInput(product);
    cliqueOrderInput.isAddToCart = true;
    this.productOrderDto.total += product.price;
    this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(cliqueOrderInput).subscribe((result) => {});
  }

  increaseTag(orderTag: OrderTag, product: CliqueProductOrderDto) {
    orderTag.quantity++;

    const cliqueOrderInput = this.setCliqueOrderInput(product);
    this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(cliqueOrderInput).subscribe((result) => {});
  }

  decreaseTag(orderTag: OrderTag, product: CliqueProductOrderDto) {
    orderTag.quantity--;

    if (orderTag.quantity == 0) {
      product.orderTags = (<any>product.orderTags || []).filter((item) => item.quantity);
    }

    const cliqueOrderInput = this.setCliqueOrderInput(product);
    this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(cliqueOrderInput).subscribe((result) => {});
  }

  checkOut() {
    this.router.navigate(['/app/portal/member/payment-details']);
  }
  getProductInCart() {
    this.showMainSpinner();
    const customerId = +localStorage.getItem('customerId');
    this.cliqueOrderServiceProxy
      .getCliqueOrderCustomer(customerId)
      .pipe(finalize(() => this.hideMainSpinner()))
      .subscribe((res) => {
        if (res) {
          this.productOrderList = res.productOrders;
          this.productOrderList.forEach((item) => {
            if (typeof item.orderTags === 'string' && item.orderTags) {
              item.orderTags = JSON.parse(item.orderTags);
            }
          });
          this.productOrderDto = res;
        }
      });
  }

  setCliqueOrderInput(product: CliqueProductOrderDto): CliqueOrderInput {
    const createOrUpdateCliqueOrderInput = new CliqueOrderInput();
    createOrUpdateCliqueOrderInput.customerId = +localStorage.getItem('customerId');
    createOrUpdateCliqueOrderInput.menuItemId = product.menuItemId;
    createOrUpdateCliqueOrderInput.screenMenuItemId = product.screenMenuItemId;
    createOrUpdateCliqueOrderInput.cliqueMealTimeId = product.cliqueMealTimeId;
    createOrUpdateCliqueOrderInput.menuItemPortionId = product.menuItemPortionId;
    createOrUpdateCliqueOrderInput.quantity = product.quantity;
    createOrUpdateCliqueOrderInput.price = product.price;
    createOrUpdateCliqueOrderInput.orderDate = moment(product.orderDate);
    createOrUpdateCliqueOrderInput.locationId = product.locationId;
    createOrUpdateCliqueOrderInput.isAddToCart = true;
    createOrUpdateCliqueOrderInput.orderTags = JSON.stringify(product.orderTags);
    createOrUpdateCliqueOrderInput.orderDateStr =moment(product.orderDate).local().format("YYYY-MM-DD")

    return createOrUpdateCliqueOrderInput;
  }

  goToMenuOrder() {
    this.router.navigate(['/app/portal/member/schedule']);
  }
}

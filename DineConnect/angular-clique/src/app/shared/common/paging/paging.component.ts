import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss'],
  animations: [appModuleAnimation()]
})
export class PagingComponent implements OnInit {
  idPage: number = 0;
  totalPage: Array<number> =[];
  offset: number = 0;
  startPage: number = 0;
  endPage: number = 5;
  currentPage: number = 0;
  LIMIT_PAGE = 9;
  totalPageList: Array<number> = new Array(9);
  totalPageDisplay = 9;
  TOTAL_PAGE = 9;
  OFFSET_FIRST = 0;
  @Input() totalCount: number;
  @Output() pageChangeProduct = new EventEmitter<any>();
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }
  ngAfterViewInit(){
    
    // this.totalPage = this.calculatePaging(this.totalCount);
    
    // this.cdr.detectChanges();
  }
  ngOnChanges(){
    this.totalPage = this.calculatePaging(this.totalCount);
    this.cdr.detectChanges();
  }
  calculatePaging(totalRecords: number): Array<number> {
    
    if (totalRecords == 0) {
      return [];
    }
    let totalPage = new Array<number>();
    if (totalRecords < this.LIMIT_PAGE) {
      totalPage = new Array(1);
    } else if (totalRecords % this.LIMIT_PAGE == 0) {
      totalPage = new Array(Math.floor(totalRecords / this.LIMIT_PAGE));
    } else {
      totalPage = new Array(Math.floor(totalRecords / this.LIMIT_PAGE) + 1);
    }
    return totalPage;
  }
  /**
   * page change
   */
  pageChange(offset: number) {
    this.idPage = offset;
    let offsetList = 0;
    if (offset > this.OFFSET_FIRST) {
      offsetList = this.LIMIT_PAGE * offset;
    }
    this.startPage =
      (Math.ceil((offset + 1) / this.TOTAL_PAGE) - 1) * this.TOTAL_PAGE;
    this.endPage = this.startPage + this.TOTAL_PAGE;
    this.pageChangeProduct.emit(offset);
  }

  /**
   * prev page
   */
  prevPage() {
    if (this.idPage == 0) {
      return;
    }
    this.idPage--;
    this.pageChange(this.idPage);
    if (this.idPage < this.startPage) {
      this.startPage = this.idPage - this.TOTAL_PAGE + 1;
      this.endPage = this.startPage + this.TOTAL_PAGE;
    }
  }
  nextPage() {
    if (this.idPage == this.totalPage.length - 1) {
      return;
    }
    this.idPage++;
    this.pageChange(this.idPage);
    if (this.idPage >= this.endPage) {
      this.startPage = this.idPage;
      this.endPage = this.startPage + this.TOTAL_PAGE;
    }
  }
  
  prevGroupPage() {
    this.startPage = this.startPage - this.TOTAL_PAGE;
    this.endPage = this.startPage + this.TOTAL_PAGE;
    this.idPage = this.startPage;
    this.offset = this.startPage * this.LIMIT_PAGE;
    this.pageChange(this.idPage);
  }
  
  nextGroupPage() {
    this.startPage = this.startPage + this.TOTAL_PAGE;
    this.endPage = this.startPage + this.TOTAL_PAGE;
    this.idPage = this.startPage;
    this.offset = this.startPage * this.LIMIT_PAGE;
    this.pageChange(this.idPage);
  }
}

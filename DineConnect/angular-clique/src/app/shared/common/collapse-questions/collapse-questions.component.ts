import { Component, OnInit, Input } from "@angular/core";

import { IFaqListDto } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "app-collapse-questions",
    templateUrl: "./collapse-questions.component.html",
    styleUrls: ["./collapse-questions.component.scss"],
})
export class CollapseQuestionsComponent implements OnInit {
    @Input() questions: IFaqListDto[];

    constructor() {}

    ngOnInit() {}
}

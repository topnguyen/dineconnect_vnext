import {
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";

import { AppComponentBase } from "@shared/common/app-component-base";

import {  TiffinMemberPortalServiceProxy, FeedbackEmailInput } from "@shared/service-proxies/service-proxies";
import { ThankForFeedbackComponent } from "./thank-for-feedback/thank-for-feedback.component";
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent extends AppComponentBase implements OnInit {
  
  feedbackMessage: string = "";
  saving = false;
  
  constructor(
    injector: Injector,
    public _portalServiceProxy: TiffinMemberPortalServiceProxy,public dialog: MatDialog) { 
    super(injector);
  }

  ngOnInit() {
  }

  save(): void {
    this.saving = true;
    let request = new FeedbackEmailInput();
    request.email = "sales@dabbajunction.com";
    request.subject = "Feedback : Stack Canteen";
    request.feedback = this.feedbackMessage;
    if(request.feedback.length>0){
      this._portalServiceProxy.sendFeedback(Object.assign({}, request)).subscribe((result) => {
        this.message.success("Thanks for the Feedback");
        this.feedbackMessage = "";

      });   
    }
  }
  submit(){
    const dialogRef = this.dialog.open(ThankForFeedbackComponent, {
      width: '529px',
      data: {},
      panelClass: 'dialog-thank-for-feedback'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

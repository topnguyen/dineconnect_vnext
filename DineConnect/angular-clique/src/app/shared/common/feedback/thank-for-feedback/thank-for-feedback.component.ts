import { FeedbackComponent } from '@app/shared/common/feedback/feedback.component';
import { MatDialogRef } from '@angular/material/dialog';
import {
  Component,
  Injector,
  OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { Router } from '@angular/router';
@Component({
  selector: 'app-thank-for-feedback',
  templateUrl: './thank-for-feedback.component.html',
  styleUrls: ['./thank-for-feedback.component.scss']
})
export class ThankForFeedbackComponent extends AppComponentBase implements OnInit {


  constructor(injector: Injector,
    private dialogRef: MatDialogRef<FeedbackComponent>,
    private router: Router) {
    super(injector);
  }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close();
  }
  gotToMenu() {
    this.dialogRef.close();
    this.router.navigate(["/app/portal/member/schedule"]);
  }
}

import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Subscription } from 'rxjs';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-top-bar-mobile',
  templateUrl: './top-bar-mobile.component.html',
  styleUrls: ['./top-bar-mobile.component.scss'],
  animations: [appModuleAnimation()]
})
export class TopBarMobileComponent extends AppComponentBase implements OnInit {

  private subscriptions = new Subscription();
  constructor(
    injector: Injector,
    private router: Router,) {
      super(injector);
  }
  tabMenu:boolean = false;
  tabMyOrder:boolean = false;
  tabFeedBack:boolean = false;
  tabSupport:boolean = false;
  tabNotification:boolean = false;
  ngOnInit() {
    if (localStorage.getItem("tabMenu")) {
      this.tabMenu = true;
    } else if(localStorage.getItem("tabMyOrder")) {
      this.tabMyOrder = true;
    } else if(localStorage.getItem("tabFeedBack")) {
      this.tabFeedBack = true;
    } else if(localStorage.getItem("tabSupport")) {
      this.tabSupport = true;
    } else if(localStorage.getItem("tabNotification")) {
      this.tabNotification = true;
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
  selectTabMenu() {
    localStorage.setItem("tabMenu",'tabMenu');
    localStorage.removeItem("tabMyOrder");
    localStorage.removeItem("tabFeedBack");
    localStorage.removeItem("tabSupport");
    localStorage.removeItem("tabNotification");
      this.tabMenu = true;
      this.tabMyOrder = false;
      this.tabFeedBack = false;
      this.tabSupport = false;
      this.tabNotification = false;
      this.router.navigate(['/app/portal/member/schedule']);
  }
  selectTabMyOrder() {
    localStorage.setItem("tabMyOrder",'tabMyOrder');
    localStorage.removeItem("tabMenu");
    localStorage.removeItem("tabFeedBack");
    localStorage.removeItem("tabSupport");
    localStorage.removeItem("tabNotification");
    this.tabMenu = false;
    this.tabMyOrder = true;
    this.tabFeedBack = false;
    this.tabSupport = false;
    this.tabNotification = false;
    this.router.navigate(['/app/portal/member/buy-offer']);
}
  
  selectTabFeedback(){
    localStorage.setItem("tabFeedBack",'tabFeedBack');
    localStorage.removeItem("tabMenu");
    localStorage.removeItem("tabMyOrder");
    localStorage.removeItem("tabSupport");
    localStorage.removeItem("tabNotification");
    this.tabMenu = false;
    this.tabMyOrder = false;
    this.tabFeedBack = true;
    this.tabSupport = false;
    this.tabNotification = false;
    this.router.navigate(['/app/portal/member/feedback']);
  }
  selectTabSupport(){
    localStorage.setItem("tabSupport",'tabSupport');
    localStorage.removeItem("tabMenu");
    localStorage.removeItem("tabFeedBack");
    localStorage.removeItem("tabMyOrder");
    localStorage.removeItem("tabNotification");
    this.tabMenu = false;
    this.tabMyOrder = false;
    this.tabFeedBack = false;
    this.tabSupport = true;
    this.tabNotification = false;
    this.router.navigate(['/app/portal/member/contact']);
  }
}

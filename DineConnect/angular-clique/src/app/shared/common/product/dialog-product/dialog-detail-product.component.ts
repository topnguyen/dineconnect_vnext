
import { Component } from '@angular/core';
import { Inject } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderTag } from '@app/shared/model/order-tag.model';

import { CliqueMealTimeDto, CliqueMenuItemDto, CliqueOrderTagDto } from '@shared/service-proxies/service-proxies';

import { DialogAddOrderTagComponent } from '../dialog-add-order-tag/dialog-add-order-tag.component';
@Component({
    selector: 'dialog-detail-product',
    templateUrl: './dialog-detail-product.component.html',
    styleUrls: ['./dialog-detail-product.component.scss'],
})
export class DialogDetailProductComponent {
    product: CliqueMenuItemDto;
    isShowOrder: boolean = false;
    mealTime: CliqueMealTimeDto;
    daySchedule: any

    constructor(
        public dialogRef: MatDialogRef<DialogDetailProductComponent>,
        public dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    closeDialog(): void {
        this.dialogRef.close(this.product);
    }

    ngOnInit() {
        this.product = this.data.product;
    }
    decrease(product: CliqueMenuItemDto) {
        product.quantity--;
    }
    increase(product: CliqueMenuItemDto) {
        product.quantity++;
    }
    addOrderTag(): void {
        const dialogRef = this.dialog.open(DialogAddOrderTagComponent, {
            width: '480px',
            data: {  },
            panelClass: 'dialog-detail-product'
        });

        dialogRef.afterClosed().subscribe((orderTags: OrderTag[]) => {
            if (orderTags && orderTags.length > 0) {
                let orderData = new Array<CliqueOrderTagDto>();
                for(let i = 0; i < orderTags.length; i++){
                    let temp = new CliqueOrderTagDto;
                    temp.id = orderTags[i].id;
                    temp.name = orderTags[i].name;
                    temp.quantity = orderTags[i].quantity;
                    temp.price = orderTags[i].price;
                    temp.orderTagGroupId = orderTags[i].orderTagGroupId;
                    orderData.push(temp);
                }
                this.product.orderTags = orderData;
            }
        });
    }
}

export interface DialogData {
    product: CliqueMenuItemDto;
    mealTime: CliqueMealTimeDto;
    daySchedule: any
}
import { CliqueMealTimeDto, CliqueCustomerFavoriteInput, CliqueOrderInput, ProductGroupDto, ScreenMenuCategoryDto,CliqueCustomerFavoriteItemServiceProxy, CliqueOrderServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { Component, EventEmitter, Injector, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CliqueMenuItemDto, CliqueMenuItemServiceProxy } from '@shared/service-proxies/service-proxies';
import { DataService } from '../service/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatDialog } from '@angular/material/dialog';
import { DialogDetailProductComponent } from './dialog-product/dialog-detail-product.component';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { AbpSessionService } from 'abp-ng2-module';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [appModuleAnimation()]
})
export class ProductComponent extends AppComponentBase implements OnInit {
  @Input() isShowOrder: boolean = false;

  @Output() totalResult = new EventEmitter<any>();
  @Output() addToCart = new EventEmitter<any>();

  modalRef: BsModalRef;
  constructor(
    injector: Injector,
    private productService: CliqueMenuItemServiceProxy,
    private dataService: DataService,
    public dialog: MatDialog,
    private cliqueCustomerFavoriteItemServiceProxy: CliqueCustomerFavoriteItemServiceProxy,
    private cliqueOrderServiceProxy: CliqueOrderServiceProxy,
    private _abpSessionService: AbpSessionService) {
      super(injector);
  }
  productList: Array<CliqueMenuItemDto>;
  totalCount: number;
  productGroupSelected: ProductGroupDto;
  screenMenuCategory: ScreenMenuCategoryDto;
  cliqueMealTime: CliqueMealTimeDto;
  daySchedule: any;
  thuSchedule: string;
  appSession: AppSessionService;
  NUMBER_PAGE_ITEMS = 9;
  isClickFavorite: boolean = false;
  mealTime: CliqueMealTimeDto;

  private subscriptions = new Subscription();

  ngOnInit() {
    this.screenMenuCategory = JSON.parse(localStorage.getItem("screenMenuCategoryFirst"));
    this.subscriptions.add(this.dataService.currentData.subscribe(data => {
      if (data[1] == "thuSchedule") {
        this.thuSchedule = data[0].thu;
        this.daySchedule = data[0].day;
        if (this.isClickFavorite) {
          this.getProductFavorite(0);
        }
      }
      if (data[1] == "screenMenuCategory") {
        if (data[0]) {
          this.screenMenuCategory = data[0].screenMenuCategory;
          this.isClickFavorite = data[0].isClickFavorite;
          this.mealTime = data[0].mealTime;
        } else {
          this.productList = [];
          this.totalResult.emit(0);
          this.totalCount = 0;
          this.hideMainSpinner();
          return;
        }
      }
      if (data[1] == "favorite") {
        this.isClickFavorite = data[0];
        this.getProductFavorite(0);
        return;
      }
      if (data[1] == "mealTime") {
        this.mealTime = data[0];

      }
      if (data[1] == "decrease") {
        this.productList.forEach(productItem => {
          if (productItem.cliqueMealTimeId == data[0].cliqueMealTimeId 
            && moment(data[0].orderDate).format('YYYY-MM-DD') == moment(localStorage.getItem("daySchedule")).format('YYYY-MM-DD')
            && productItem.screenMenuItemId == data[0].screenMenuItemId
          ) {
            productItem.isAddToCart = false;
          }
        })
        return;
      }

      if (!this.isClickFavorite) {
        this.getAllProduct(this.screenMenuCategory, 0, this.thuSchedule ? this.thuSchedule : localStorage.getItem("thuSchedule"));
      }
      else {
        this.hideMainSpinner();
      }
    }))
    this.getProductInCart();
  }
  setCliqueOrderInput(product: CliqueMenuItemDto): CliqueOrderInput {
    const createOrUpdateCliqueOrderInput = new CliqueOrderInput();
    createOrUpdateCliqueOrderInput.customerId = +localStorage.getItem("customerId");
    createOrUpdateCliqueOrderInput.menuItemId = product.menuItemId;
    createOrUpdateCliqueOrderInput.screenMenuItemId = product.screenMenuItemId;
    createOrUpdateCliqueOrderInput.cliqueMealTimeId = this.mealTime ? this.mealTime.id : JSON.parse(localStorage.getItem("cliqueMealTimeFirst")).id;
    createOrUpdateCliqueOrderInput.menuItemPortionId = product.menuItemPortionId;
    createOrUpdateCliqueOrderInput.quantity = product.quantity;
    createOrUpdateCliqueOrderInput.price = product.price;
    createOrUpdateCliqueOrderInput.orderDate = this.daySchedule ? moment(this.daySchedule) : moment(localStorage.getItem("daySchedule"));
    createOrUpdateCliqueOrderInput.locationId = this.screenMenuCategory?.locationId;
    createOrUpdateCliqueOrderInput.isAddToCart = product.isAddToCart;
    createOrUpdateCliqueOrderInput.orderTags = JSON.stringify(product.orderTags);
    createOrUpdateCliqueOrderInput.orderDateStr = localStorage.getItem('daySchedule');

    return createOrUpdateCliqueOrderInput;
  }
  setCreateOrUpdateCliqueCustomerFavoriteItemInput(product: CliqueMenuItemDto): CliqueCustomerFavoriteInput {
    const createOrUpdateCliqueCustomerFavoriteItemInput = new CliqueCustomerFavoriteInput();
    createOrUpdateCliqueCustomerFavoriteItemInput.customerId = +localStorage.getItem("customerId");
    createOrUpdateCliqueCustomerFavoriteItemInput.menuItemId = product.menuItemId;
    createOrUpdateCliqueCustomerFavoriteItemInput.isFavorite = product.isFavorite;
    createOrUpdateCliqueCustomerFavoriteItemInput.screenMenuItemId = product.screenMenuItemId;
    return createOrUpdateCliqueCustomerFavoriteItemInput;
  }
  addToFavorite(product: CliqueMenuItemDto) {
    product.isFavorite = true;
    const CreateOrUpdateCliqueCustomerFavoriteItemInput = this.setCreateOrUpdateCliqueCustomerFavoriteItemInput(product);
    this.cliqueCustomerFavoriteItemServiceProxy.createOrUpdateCliqueCustomerFavoriteItem(CreateOrUpdateCliqueCustomerFavoriteItemInput).subscribe(result => {
    })
  }
  removeToFavorite(product: CliqueMenuItemDto) {
    product.isFavorite = false;
    const CreateOrUpdateCliqueCustomerFavoriteItemInput = this.setCreateOrUpdateCliqueCustomerFavoriteItemInput(product);
    this.cliqueCustomerFavoriteItemServiceProxy.createOrUpdateCliqueCustomerFavoriteItem(CreateOrUpdateCliqueCustomerFavoriteItemInput).subscribe(result => {
    })
  }
  addOrRemoveCart(product: CliqueMenuItemDto, oldStatus: boolean) {
    product.quantity = 1;
    if (product.isAddToCart) {
      product.isAddToCart = false;
    } else {
      product.isAddToCart = true;
    }
    product.orderDateStr = `${this.mealTime ? this.mealTime.name : JSON.parse(localStorage.getItem("cliqueMealTimeFirst")).name} - ${moment(this.daySchedule).local().format("ddd DD MMM, yyyy")}`;
    const createOrUpdateCliqueOrderInput = this.setCliqueOrderInput(product);

    product.isAddToCart = oldStatus;

    this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(createOrUpdateCliqueOrderInput).subscribe(result => {
      if(result == null){
        this.dataService.changeData({ product, isShowOrder: this.isShowOrder }, "productToCart")
        this.addToCart.emit(product);
        product.isAddToCart = !oldStatus;
      }
    })
  }
  getAllProduct(screenMenuCategory: ScreenMenuCategoryDto, page?: number, thuSchedule?: string) {
    const customerId = +localStorage.getItem("customerId");
    this.productService.getListMenuItemSchudule(
      null,
      screenMenuCategory.locationId,
      screenMenuCategory.screenMenuCategoryId,
      screenMenuCategory.screenMenuId,
      customerId,
      thuSchedule,
      this.mealTime ? this.mealTime.id : JSON.parse(localStorage.getItem("cliqueMealTimeFirst")).id,
      this.daySchedule ? this.daySchedule : localStorage.getItem("daySchedule"),
      undefined, 
      this.NUMBER_PAGE_ITEMS, 
      page)
      .pipe(
        finalize(() => this.hideMainSpinner())
      )
      .subscribe(product => {
        this.productList = product.items;
        this.totalCount = product.totalCount;
        this.totalResult.emit(this.totalCount);
      })
  }
  pageChangeProduct(event) {
    if (this.isClickFavorite) {
      this.getProductFavorite(this.NUMBER_PAGE_ITEMS * event)
    } else {
      this.getAllProduct(this.screenMenuCategory, this.NUMBER_PAGE_ITEMS * event, this.thuSchedule);
    }
  }
  openDialog(product: CliqueMenuItemDto): void {
    const mealTime = this.mealTime ? this.mealTime : JSON.parse(localStorage.getItem("cliqueMealTimeFirst"));
    const daySchedule = this.daySchedule;
    const dialogRef = this.dialog.open(DialogDetailProductComponent, {
      width: '1093px',
      data: { product, mealTime, daySchedule },
      panelClass: 'dialog-detail-product'
    });

    dialogRef.afterClosed().subscribe(product => {
      if (product) {
        this.showMainSpinner();
        this.productList.forEach(productItem => {
          if (productItem.screenMenuItemId == product.screenMenuItemId) {
            productItem.isAddToCart = true;
            productItem.quantity = product.quantity;
            productItem.orderDateStr = `${this.mealTime ? this.mealTime.name : JSON.parse(localStorage.getItem("cliqueMealTimeFirst")).name} - ${moment(this.daySchedule).local().format("ddd DD MMM, yyyy")}`;
            productItem.orderTags = product.orderTags;

            const createOrUpdateCliqueOrderInput = this.setCliqueOrderInput(productItem);

            this.cliqueOrderServiceProxy.createOrUpdateCliqueOrder(createOrUpdateCliqueOrderInput).subscribe(result => {
              this.hideMainSpinner();
              // productItem.isClickFromDetailProduct = true;
              this.addToCart.emit(productItem);
              if (this.isShowOrder) {
                this.dataService.changeData({ product: productItem, isShowOrder: this.isShowOrder }, "productToCart");
              }
            })
          }
        })
      }
    });
  }
  getProductFavorite(page?: number) {
    const customerId = +localStorage.getItem("customerId");
    this.productService.getListMenuItemFavorite(
      null, 
      customerId,
      this.thuSchedule ? this.thuSchedule : localStorage.getItem("thuSchedule"),
      this.mealTime ? this.mealTime.id : JSON.parse(localStorage.getItem("cliqueMealTimeFirst")).id,
      this.daySchedule ? this.daySchedule : localStorage.getItem("daySchedule"),
      undefined, 
      this.NUMBER_PAGE_ITEMS, 
      page)
      .pipe(
        finalize(() => this.hideMainSpinner())
      )
      .subscribe(product => {
        this.productList = product.items;
        this.totalCount = product.totalCount;
        this.totalResult.emit(this.totalCount);
      })
  }
  getProductInCart() {
    this.showMainSpinner();
    const customerId = +localStorage.getItem("customerId");
    this.cliqueOrderServiceProxy.getCliqueOrderCustomer(customerId)
    .pipe(
      finalize(() => this.hideMainSpinner())
    )
    .subscribe(product => {
      this.addToCart.emit(product);
    })
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

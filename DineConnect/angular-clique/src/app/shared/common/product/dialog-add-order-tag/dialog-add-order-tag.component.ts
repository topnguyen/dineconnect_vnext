import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderTag } from '@app/shared/model/order-tag.model';

import { OrderTagDto, OrderTagGroupListDto, OrderTagGroupServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-dialog-add-order-tag',
    templateUrl: './dialog-add-order-tag.component.html',
    styleUrls: ['./dialog-add-order-tag.component.scss']
})
export class DialogAddOrderTagComponent implements OnInit {
    tagGroupList: OrderTagGroupListDto[] = [];
    tagGroupSelected: OrderTagGroupListDto;

    constructor(
        public dialogRef: MatDialogRef<DialogAddOrderTagComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy
    ) {}

    ngOnInit(): void {
        this._getOrderTagGroup();
    }

    decrease(orderTag: OrderTagDto & { quantity: number }) {
        if (orderTag.quantity > 0) {
            orderTag.quantity--;
        }
    }
    increase(orderTag: OrderTagDto & { quantity: number }) {
        if (!orderTag.quantity) {
            orderTag.quantity = 0;
        }

        orderTag.quantity++;
    }

    addOrderTag() {
        const orderTags: OrderTag[] = [];
        this.tagGroupList.forEach((group) => {
            group.tags.forEach((item) => {
                if (item['quantity']) {
                    orderTags.push({
                        id: item.id,
                        name: item.name,
                        quantity: item['quantity'],
                        price: item.price,
                        orderTagGroupId: item.orderTagGroupId
                    });
                }
            });
        });

        this.dialogRef.close(orderTags);
    }

    private _getOrderTagGroup() {
        this._orderTagGroupServiceProxy
            .getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                0,
                1000
            )
            .subscribe((data) => {
                this.tagGroupList = data.items || [];
                this.tagGroupSelected = data.items?.length > 0 ? data.items[0] : null;
            });
    }
}

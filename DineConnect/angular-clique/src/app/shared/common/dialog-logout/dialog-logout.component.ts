import { AppAuthService } from './../auth/app-auth.service';
import {
    Component,
    Injector,
    OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { CliqueOrderServiceProxy } from '../../../../shared/service-proxies/service-proxies';

@Component({
    selector: 'dialog-logout',
    templateUrl: './dialog-logout.component.html',
    styleUrls: ['./dialog-logout.component.scss']
})
export class DialogLogoutComponent extends AppComponentBase implements OnInit {
        productDetailIds: number[];
        cliqueOrderId: number;
        constructor(injector: Injector,
                    private _authService: AppAuthService,
                    private _cliqueOrderService: CliqueOrderServiceProxy,
        ) {
        super(injector);
    }

    ngOnInit() {
    }

    logout(){
        const customerId = +localStorage.getItem("customerId");

        this._cliqueOrderService.getCliqueOrderCustomer(customerId).subscribe( res => {
        if (res) {
            this.productDetailIds = res.productOrders.map((x) => x.cliqueOrderDetailId);
            this.cliqueOrderId = res.cliqueOrderId;
            if(this.productDetailIds.length > 0 ) {
                this._cliqueOrderService.deleteCliqueOrderById(this.cliqueOrderId).subscribe( res => {
                })
            }
                this._authService.logout();
            }
        });
    }
}

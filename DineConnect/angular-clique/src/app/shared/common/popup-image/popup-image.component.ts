import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-popup-image',
    templateUrl: './popup-image.component.html',
    styleUrls: ['./popup-image.component.scss']
})
export class PopupImageComponent extends AppComponentBase implements OnInit {
    constructor(injector: Injector, private _modalService: BsModalService) {
        super(injector);
    }

    ngOnInit(): void {
        setTimeout(() => {
            if (this.appSession.tenant.tiffinPopup) {
                this.openModal()
            }
        }, 3000);
    }

    openModal() {
        const initialState = {
            popupImage: this.appSession.tenant.tiffinPopup
        };
        this._modalService.show(ModalContentComponent, { initialState, class: 'popup-image' });
    }
}

@Component({
    template: `
        <div class="modal-body">
            <img [src]="popupImage" alt="" />
        </div>
    `
})
export class ModalContentComponent implements OnInit {
    popupImage: string;

    constructor(public bsModalRef: BsModalRef) {}

    ngOnInit() {}
}

export interface OrderTag {
    id: number;
    name: string;
    quantity: number;
    price: number;
    orderTagGroupId: number;
}

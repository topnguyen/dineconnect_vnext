import { CliqueCustomerFavoriteItemServiceProxy, CliquePaymentServiceProxy, CliqueWalletServiceProxy } from './../../shared/service-proxies/service-proxies';
import { TransferComponent } from './member/settings-common/transfer/transfer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortalRoutingModule } from './portal-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ImageCropperModule } from 'ngx-image-cropper';
import { TableModule } from 'primeng/table';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxTagsInputModule } from 'ngx-tags-input';
import { QueryBuilderModule } from 'angular2-query-builder';

import { FileUploadModule as PrimeNgFileUploadModule } from 'primeng/fileupload';
import {
    CustomersServiceProxy,
    CommonServiceProxy,
    ProductSetsServiceProxy,
    TiffinMemberPortalServiceProxy,
    ScheduleServiceProxy,
    TiffinOrderServiceProxy,
    EmailNotificationBackgroundJobServiceServiceProxy,
    TiffinPromotionServiceProxy,
    WheelPaymentMethodsServiceProxy,
    TiffinMealTimesServiceProxy,
    DeliveryTimeSlotServiceProxy,
    LocationServiceProxy,
    WheelServiceProxy,
    ProfileServiceProxy,
    CliqueCustomerServiceProxy,
    CliqueOrderServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { LoginService } from '@account/login/login.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { BuyOfferComponent } from './member/buy-offer/buy-offer.component';
import { PaymentCompletedComponent } from './member/payment/payment-completed.component';
import { PaymentOrderCompletedComponent } from './member/payment/payment-order-completed.component';

import { ProductService } from '@app/shared/services/product.service';
import { CartProductsComponent } from './member/cart-products/cart-products.component';
import { CartCalculatorComponent } from './member/cart-calculator/cart-calculator.component';
import { CheckoutComponent } from './member/checkout/checkout.component';
import { ProfileComponent } from './member/profile/profile.component';
import { BalanceComponent } from './member/balance/balance.component';
import { ChangePasswordComponent } from './member/profile/change-password/change-password.component';
import { DetailProfileComponent } from './member/profile/detail-profile/detail-profile.component';
import { OrderHistoryComponent } from './member/order-history/order-history.component';
import { PaymentHistoryComponent } from './member/payment-history/payment-history.component';

import { AddressProfileComponent } from './member/profile/address-profile/address-profile.component';
import { PaymentDetailComponent } from './member/payment-history/payment-detail.component';
import { ScheduleComponent } from './member/schedule/schedule.component';
import { CreateAddressProfileComponent } from './member/profile/address-profile/create-address-profile/create-address-profile.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FileUploadModule } from 'ng2-file-upload';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { PasswordModule } from 'primeng/password';
import { DragDropModule } from 'primeng/dragdrop';
import { CardModule } from 'primeng/card';
import { AgmCoreModule } from "@agm/core";
import { RadioButtonModule } from 'primeng/radiobutton';
import { AddPromoCodeDialogComponent } from './member/cart-calculator/add-promo-code-dialog/add-promo-code-dialog.component';
import { ScriptLoadingService } from '@app/shared/services/script-loading.service';
import { PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { AddonDialogComponent } from './member/orders/addon-dialog/addon-dialog.component';
import { AppConsts } from '@shared/AppConsts';
import { GooglePayButtonModule } from '@google-pay/button-angular';
import { OrderComponent } from './member/orders/order.component';
import { EditOrderModalComponent } from './member/orders/edit-order-modal/edit-order-modal.component';
import { QRCodeDialogComponent } from './member/cart-calculator/qr-code-dialog/qr-code-dialog.component';
import { PaymentDetailsComponent } from './member/payment-details/payment-details.component';
import { MatIconModule } from '@angular/material/icon';
import { PaymentSuccessfulDialogComponent } from './member/payment-details/payment-successful-dialog/payment-successful-dialog/payment-successful-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SettingsCommonComponent } from './member/settings-common/settings-common.component';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatExpansionModule } from '@angular/material/expansion';
import { MyProfileComponent } from './member/settings-common/my-account/my-profile/my-profile.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from "@angular/material/input";
import { MatNativeDateModule } from '@angular/material/core';
import { MyPasswordComponent } from './member/settings-common/my-account/my-password/my-password.component';
import {MatRadioModule} from '@angular/material/radio';
import { StatusComponent } from './member/settings-common/notification/status/status.component';
import { DialogLogoutComponent } from '@app/shared/common/dialog-logout/dialog-logout.component';
import { StackWalletComponent } from './member/settings-common/my-wallet/stack-wallet/stack-wallet.component';
import { TopUpWalletComponent } from './member/settings-common/my-wallet/top-up-wallet/top-up-wallet.component';
import { TransferConfirmationComponent } from './member/settings-common/transfer/transfer-confirmation/transfer-confirmation.component';
import { GuiTopUpWalletComponent } from './member/settings-common/my-wallet/gui-top-up-wallet/gui-top-up-wallet.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { DialogCancelOrderComponent } from './member/order-history/dialog-cancel-order/dialog-cancel-order.component';
import { DialogEnterPriceComponent } from './member/settings-common/transfer/dialog-enter-price/dialog-enter-price.component';

@NgModule({
    declarations: [
        BalanceComponent,
        BuyOfferComponent,
        PaymentCompletedComponent,
        PaymentOrderCompletedComponent,
        CartProductsComponent,
        CartCalculatorComponent,
        CheckoutComponent,
        ProfileComponent,
        ChangePasswordComponent,
        DetailProfileComponent,
        OrderHistoryComponent,
        PaymentHistoryComponent,
        OrderComponent,
        EditOrderModalComponent,
        AddressProfileComponent,
        PaymentDetailComponent,
        ScheduleComponent,
        CreateAddressProfileComponent,
        AddPromoCodeDialogComponent,
        AddonDialogComponent,
        QRCodeDialogComponent,
        PaymentDetailsComponent,
        PaymentSuccessfulDialogComponent,
        SettingsCommonComponent,
        MyProfileComponent,
        MyPasswordComponent,
        StatusComponent,
        DialogLogoutComponent,
        StackWalletComponent,
        TopUpWalletComponent,
        TransferComponent,
        TransferConfirmationComponent,
        GuiTopUpWalletComponent,
        DialogCancelOrderComponent,
        DialogEnterPriceComponent
    ],
    imports: [
        CommonModule,
        PortalRoutingModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        CountoModule,
        AutoCompleteModule,
        EditorModule,
        FileUploadModule,
        PrimeNgFileUploadModule,
        ImageCropperModule,
        InputMaskModule,
        PaginatorModule,
        TableModule,
        CheckboxModule,
        CalendarModule,
        MultiSelectModule,
        DropdownModule,
        NgSelectModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        AppBsModalModule,
        NgxTagsInputModule,
        TimepickerModule.forRoot(),
        // QueryBuilderModule,
        OAuthModule.forRoot(),
        PasswordModule,
        DragDropModule,
        CardModule,
        RadioButtonModule,
        CollapseModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: AppConsts.GOOGLE_MAP_KEY,
            libraries: ["drawing"],
        }),
        GooglePayButtonModule,
        MatIconModule,
        MatDialogModule,
        MatListModule,
        MatSidenavModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatDatepickerModule,
        ReactiveFormsModule,
        MatInputModule, 
        MatNativeDateModule,
        MatRadioModule,
        MatStepperModule,
        MatTabsModule
    ],
    providers: [
        CustomersServiceProxy,
        LoginService,
        CommonServiceProxy,
        ProductSetsServiceProxy,
        TiffinMemberPortalServiceProxy,
        ProductService,
        ScheduleServiceProxy,
        TiffinOrderServiceProxy,
        TiffinPromotionServiceProxy,
        EmailNotificationBackgroundJobServiceServiceProxy,
        WheelPaymentMethodsServiceProxy,
        ScriptLoadingService,
        PaypalScriptService,
        TiffinMealTimesServiceProxy,
        DeliveryTimeSlotServiceProxy,
        LocationServiceProxy,
        WheelServiceProxy,
        CliqueWalletServiceProxy,
        ProfileServiceProxy,
        CliqueCustomerServiceProxy,
        CliqueOrderServiceProxy,
        CliqueCustomerFavoriteItemServiceProxy,
        CliquePaymentServiceProxy,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
    ],
})
export class PortalModule {}

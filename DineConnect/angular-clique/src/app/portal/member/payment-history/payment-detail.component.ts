import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    TiffinMemberPortalServiceProxy,
    PaymentCustomerHistoryDto,
    TiffinPaymentDetailDto,
    CustomerProductOfferDetailDto,
} from "@shared/service-proxies/service-proxies";
import { ActivatedRoute } from "@angular/router";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    selector: "app-history",
    templateUrl: "./new-payment-detail.component.html",
    styleUrls: ["./payment-detail.component.less"],
})
export class PaymentDetailComponent extends AppComponentBase implements OnInit {
    paymentHistory: PaymentCustomerHistoryDto[] = [];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    paymentId: number;
    paymentDetail: TiffinPaymentDetailDto = new TiffinPaymentDetailDto();
    companyName: string;
    companyLogoUrl: string;
    companyAddress: string;
    taxNo: string;

    ngOnInit() {
        this.paymentDetail.products = [];
        this._activatedRoute.params.subscribe((params) => {
            this.paymentId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.paymentId)) {
                this.paymentId = undefined;
            }

            this._portalServiceProxy
                .getPaymentDetail(this.paymentId)
                .subscribe((result) => {
                    this.paymentDetail = result;
                });

            this._portalServiceProxy.getCompanyInfo().subscribe((result) => {
                this.companyName = result.companyName;
                this.companyLogoUrl = result.companyLogoUrl;
                this.companyAddress = result.companyAddress;
                this.taxNo = result.taxNo;
            });
        });
    }

    sumAmount(products: CustomerProductOfferDetailDto[]): number {
        return products.map((p) => p.paidAmount).reduce((a, b) => a + b);
    }

    printPaymentDetail(paymentId: number) {
        this._portalServiceProxy
            .printPaymentDetailHistory(paymentId)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    isEmpty(val): boolean {
        return val === undefined || val == null || val.length <= 0
            ? true
            : false;
    }
}

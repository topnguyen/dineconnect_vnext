import { Component, OnInit, Injector, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    TiffinMemberPortalServiceProxy,
    UpdateMyInfoDto,
    MyInfoDto,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-detail-profile',
    templateUrl: './detail-profile.component.html',
    styleUrls: ['./detail-profile.component.scss'],
})
export class DetailProfileComponent extends AppComponentBase {
    @Input() myInfo: MyInfoDto;
    public active = false;
    public saving = false;
    constructor(
        injector: Injector,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy
    ) {
        super(injector);
    }

    save(): void {
        this.saving = true;
        let updateInfo = new UpdateMyInfoDto();
        updateInfo.init(this.myInfo);
        this._memberServiceProxy
            .updateMyBasicInfo(updateInfo)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.appSession.user.name = this.myInfo.name;
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }

    returnOnlyNumber(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
          keynum = e.keyCode;
        } else if (e.which) {
          keynum = e.which;
        }

        if (keynum === 11) {
          return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[0123456789]/;
        return charcheck.test(keychar);
      }
}

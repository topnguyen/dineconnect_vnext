import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-payment-stripe-dialog',
  templateUrl: './payment-stripe-dialog.component.html',
  styleUrls: ['./payment-stripe-dialog.component.scss']
})
export class PaymentStripeDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PaymentStripeDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      payment?: string;
      status?: string;
      date?: string;
      time?: string;
      total?: string;
    },
  ) { }

  ngOnInit() {

  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-payment-successful-dialog',
  templateUrl: './payment-successful-dialog.component.html',
  styleUrls: ['./payment-successful-dialog.component.scss']
})
export class PaymentSuccessfulDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PaymentSuccessfulDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      payment?: string;
      status?: string;
      date?: string;
      time?: string;
      total?: string;
    },
  ) { }

  ngOnInit() {

  }

}

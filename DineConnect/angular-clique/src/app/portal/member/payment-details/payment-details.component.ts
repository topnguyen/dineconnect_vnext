import { DataService } from './../../../shared/common/service/data.service';
import { finalize } from 'rxjs/operators';
import { CreateOrderCliquePaymentInput } from './../../../../shared/service-proxies/service-proxies';
import { Location } from '@angular/common';
import { Component, Injector, OnInit, NgZone, ElementRef, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StripeHelper } from '@shared/helpers/StripeHelper';
import AdyenCheckout from '@adyen/adyen-web';
import {
    CustomerProductOfferDetailDto,
    PaymentCustomerHistoryDto,
    TiffinMemberPortalServiceProxy,
    CliqueOrderDto,
    CliquePaymentServiceProxy,
    GetWheelPaymentMethodForViewDto,
    // CliquePaymentInputDto,
    AddonSettingsServiceProxy,
    CustomerCardDto,
    SwipeCardServiceProxy,
    CliqueWalletServiceProxy,
    CustomerWalletDto,
    CliqueOrderServiceProxy,
} from '@shared/service-proxies/service-proxies';
import {
    AdyenState,
    PAYMENT_METHOD,
    SYSTEM_NAME,
    STEP_ORDER,
} from '@app/portal/services/order.service';
import { DataApproved, PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { PaymentSuccessfulDialogComponent } from './payment-successful-dialog/payment-successful-dialog/payment-successful-dialog.component';
import { OrderTag } from '@app/shared/model/order-tag.model';

declare let Stripe: any;
declare let OmiseCard: any;
declare let Omise: any;
declare const google: any;

@Component({
    selector: 'app-payment-details',
    templateUrl: './payment-details.component.html',
    styleUrls: ['./payment-details.component.scss']
})
export class PaymentDetailsComponent extends AppComponentBase implements OnInit {

    paymentHistory: PaymentCustomerHistoryDto[] = [];
    paymentDetail: CliqueOrderDto = new CliqueOrderDto();
    customerWallet: CustomerWalletDto = new CustomerWalletDto();
    companyName: string;
    companyLogoUrl: string;
    companyAddress: string;
    taxNo: string;
    payPalButtonId: 'payPalButton';
    paymentRequest: any;
    stripe: any;
    checkoutAdyen: AdyenCheckout;
    saving = false;
    serviceFees = [];
    serviceFeePercentage: number;
    paymentMethod: "";
    STEP_ORDER = STEP_ORDER;
    PAYMENT_METHOD = PAYMENT_METHOD;
    swipeCardList: CustomerCardDto[];
    swipeCard: string;
    swipeOTP: string;
    isPayStackWallet = true;
    isWalletEnough = false;

    omiseConfig = {
        publishKey: '',
        secretKey: '',
        enablePaynowQR: false
    };
    stripeConfig = {
        secretKey: '',
        publishKey: ''
    };
    adyenConfig = {
        apiKey: '',
        merchantAccount: '',
        environment: '',
        clientKey: ''
    };
    paypalConfig = {
        clientId: '',
        clientSecret: '',
        environment: 0
    };
    googlePayConfig = {
        merchantId: '',
        merchantName: '',
        publicKey: '',
        environment: ''
    };
    omiseData = {
        label: '',
        omiseToken: '',
        omisePaynowToken: ''
    };
    stripeData = {
        label: '',
        stripeToken: ''
    };
    paypalData = {
        label: '',
        orderId: null
    };
    adyenData = {
        label: '',
        adyenEncryptedCardNumber: '',
        adyenEncryptedExpiryMonth: '',
        adyenEncryptedExpiryYear: '',
        adyenEncryptedSecurityCode: ''
    };
    googlePayData = {
        label: '',
        googlePayToken: null
    };
    cashData = {
        label: 'Offline Payment - Cash bash',
    };


    get total() {
        let orderTagTotal = 0;
        (this.paymentDetail?.productOrders || []).forEach(element => {
            const orderTags: OrderTag[] = <any>element.orderTags || [];

            orderTags.forEach(item => {
                orderTagTotal += item.price * item.quantity;
            });
        });

        return this.paymentDetail?.total + orderTagTotal;
    }

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _productServiceProxy: CliqueOrderServiceProxy,
        private _cliqueWalletServiceProxy: CliqueWalletServiceProxy,
        private _router: Router,
        private dialog: MatDialog,
        private location: Location,
        private _cliquePaymentMethodServiceProxy: CliquePaymentServiceProxy,
        private _paypalScriptService: PaypalScriptService,
        private _ngZone: NgZone,
        private _addonSettingsServiceProxy: AddonSettingsServiceProxy,
        private _elRef: ElementRef,
        private _render: Renderer2,
        private _swipeCardServiceProxy: SwipeCardServiceProxy,
        private dataService: DataService

    ) {
        super(injector);
    }

    private _getServiceCharge(name) {
        this.serviceFeePercentage = 0;
        this._addonSettingsServiceProxy.getServiceCharge(name).subscribe((result) => {
            this.serviceFees = result || [];
            this.serviceFees.map((item) => {
                this.serviceFeePercentage = this.serviceFeePercentage + item.percentage;
            });
        });
    }

    private _getPaymentMethods() {
        this._cliquePaymentMethodServiceProxy.getPaymentMethod(undefined, undefined, undefined, undefined, -1, -1, -1, -1, 1, undefined, 0, 1000).subscribe((result) => {
            this._getSettings(result.items);
            this._initPayment();
        });
    }

    choosepaymentMethod() {
        this._getServiceCharge(this.paymentMethod);

        let els = this._elRef.nativeElement.querySelectorAll('div.payment-method');
        els.forEach((el) => {
            this._render.removeClass(el, 'selected');
        });

        if (this.paymentMethod === PAYMENT_METHOD.Omise || this.paymentMethod === PAYMENT_METHOD.Adyen) {
            els.forEach((el) => {
                this._render.addClass(el, 'selected');
            });
        }
    }

    private _getSettings(items: GetWheelPaymentMethodForViewDto[]) {
        items.map((item) => {
            switch (item.systemName) {
                case SYSTEM_NAME.Omise:
                    this.omiseConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Stripe:
                    this.stripeConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Adyen:
                    this.adyenConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.PayPal:
                    this.paypalConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.GooglePay:
                    this.googlePayConfig = item.dynamicFieldData;
                    break;

                default:
                    break;
            }
        });
    }

    private _initPayment() {
        this.stripeData.label = this.l('OnlinePayment') + ' - ' + this.l('Stripe');
        this.omiseData.label = this.l('OnlinePayment') + ' - ' + this.l('Omise');
        this.adyenData.label = this.l('OnlinePayment') + ' - ' + this.l('Adyen');
        this.paypalData.label = this.l('OnlinePayment') + ' - ' + this.l('PayPal');
        this.googlePayData.label = this.l('OnlinePayment') + ' - ' + this.l('GooglePay');

        if (this.stripeConfig?.publishKey) {
            this.initStripeElements();
        }
        if (this.omiseConfig?.publishKey) {
            this.initOmiseElements();
        }
        if (this.adyenConfig?.apiKey) {
            this.initAdyenElements();
        }
        if (this.paypalConfig?.clientId) {
            this.initPayPalElements();
        }
        if (this.googlePayConfig?.merchantId) {
            this.initGooglePayElements();
        }
    }

    private initPayPalElements() {
        const config = {
            clientId: this.paypalConfig.clientId,
            currency: this.appSession.application.currency,
            locale: 'en_SG',
            components: ['buttons', 'funding-eligibility']
        };
        this._paypalScriptService.registerScript(config, (payPal: any) => {
            this.initPayPalConfig(payPal);
        });
    }

    private initPayPalConfig(payPal) {
        const self = this;
        this._ngZone.runOutsideAngular(() => {
            payPal
                .Buttons({
                    fundingSource: payPal.FUNDING.PAYPAL,
                    createOrder: (data: any, actions: { order: any }) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        currency_code: this.appSession.application.currency,
                                        value: this.paymentDetail.total
                                    }
                                }
                            ]
                        });
                    },
                    onApprove: (data: DataApproved, actions: any) => {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function (details) {
                            // This function shows a transaction success message to your buyer.

                            self.paypalData.orderId = data.orderID;
                            self._ngZone.run(() => {
                                self._createOrderList(PAYMENT_METHOD.PayPal);
                            });
                        });
                    }
                })
                .render(`#${this.payPalButtonId}`);
        });
    }

    private initGooglePayElements() {
        this.paymentRequest = {
            environment: this.googlePayConfig.environment,
            buttonType: 'buy',
            buttonColor: 'default',
            apiVersion: 2,
            apiVersionMinor: 0,
            allowedPaymentMethods: [
                {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
                        allowedCardNetworks: ['MASTERCARD', 'VISA']
                    },
                    tokenizationSpecification: {
                        type: 'DIRECT',
                        parameters: {
                            protocolVersion: 'ECv2',
                            publicKey: this.googlePayConfig.publicKey
                        }
                    }
                }
            ],
            merchantInfo: {
                merchantId: this.googlePayConfig.merchantId,
                merchantName: this.googlePayConfig.merchantName
            }
        };
    }

    private initStripeElements() {
        if (this.stripeConfig.publishKey) {
            this.stripe = Stripe(this.stripeConfig.publishKey);

            let elements = this.stripe.elements({
                fonts: [
                    {
                        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro'
                    }
                ]
            });

            let elementStyles = {
                base: {
                    color: '#32325D',
                    fontWeight: 500,
                    fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',

                    '::placeholder': {
                        color: '#CFD7DF'
                    },
                    ':-webkit-autofill': {
                        color: '#e39f48'
                    }
                },
                invalid: {
                    color: '#E25950',

                    '::placeholder': {
                        color: '#FFCCA5'
                    }
                }
            };

            let elementClasses = {
                focus: 'focused',
                empty: 'empty',
                invalid: 'invalid'
            };

            let cardNumber = elements.create('cardNumber', {
                style: elementStyles,
                classes: elementClasses
            });
            cardNumber.mount('#stripe-card-number');

            let cardExpiry = elements.create('cardExpiry', {
                style: elementStyles,
                classes: elementClasses
            });
            cardExpiry.mount('#stripe-card-expiry');

            let cardCvc = elements.create('cardCvc', {
                style: elementStyles,
                classes: elementClasses
            });

            cardCvc.mount('#stripe-card-cvc');

            let self = this;
            StripeHelper.registerElements(this.stripe, [cardNumber, cardExpiry, cardCvc], 'stripe-check-out-container', function (token) {
                self.stripeData.stripeToken = token.id;
                self._createOrderList(PAYMENT_METHOD.Stripe);
            });
        }
    }

    private initOmiseElements() {
        OmiseCard.configure({
            publicKey: this.omiseConfig.publishKey
        });
        Omise.setPublicKey(this.omiseConfig.publishKey);

        let button = document.querySelector('#checkoutButton');
        if (!button) {
            return;
        }

        button.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this._isValidBillingInformation()) {
                return;
            }

            let self = this;

            OmiseCard.open({
                amount: this.paymentDetail.total * 100,
                currency: this.appSession.application.currency,
                defaultPaymentMethod: 'credit_card',
                onCreateTokenSuccess: (nonce) => {
                    if (nonce.startsWith('tokn_')) {
                        this.omiseData.omiseToken = nonce;

                        self._createOrderList(PAYMENT_METHOD.Omise);
                    }
                }
            });
        });

        let myButton = document.querySelector('#payNow');
        if (!myButton || !this.omiseConfig?.enablePaynowQR) {
            return;
        }

        myButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this.isValidBillingInformation()) {
                return;
            }

            this._portalServiceProxy
                .createOmisePaymentNowRequest(this.paymentDetail.total * 100, this.appSession.application.currency)
                .subscribe((result) => {
                    const response = result ? JSON.parse(result) : null;

                    // if (response && response.source && response.source.scannable_code && response.source.scannable_code.image) {
                    //     this.QRCodeModal.show(response.source.scannable_code.image.download_uri, response.id, this.paymentDetail.total);
                    // }
                });
        });
    }

    private isValidBillingInformation() {
        return true;
    }

    private initAdyenElements() {
        const configuration = {
            clientKey: this.adyenConfig.clientKey,
            locale: 'en-US',
            environment: this.adyenConfig.environment,
            amount: {
                currency: this.appSession.application.currency,
                value: this.paymentDetail.total * 100
            },
            countryCode: 'SG'
        };

        this.checkoutAdyen = new AdyenCheckout(configuration);
        this.checkoutAdyen
            .create('securedfields', {
                // Optional configuration
                type: 'card',
                brands: ['mc', 'visa', 'amex', 'bcmc', 'maestro'],
                styles: {
                    base: {
                        color: 'black',
                        fontSize: '14px',
                        fontFamily: 'Helvetica'
                    },
                    error: {
                        color: 'red'
                    },
                    validated: {
                        color: 'green'
                    },
                    placeholder: {
                        color: '#d8d8d8'
                    }
                },
                ariaLabels: {
                    lang: 'en-GB',
                    encryptedCardNumber: {
                        label: 'Credit or debit card number field'
                    }
                },
                // Events
                onChange: (state: AdyenState, component) => {
                    const data = state.data.paymentMethod;

                    if (state?.data?.paymentMethod) {
                        this.adyenData.adyenEncryptedCardNumber = data.encryptedCardNumber;
                        this.adyenData.adyenEncryptedExpiryMonth = data.encryptedExpiryMonth;
                        this.adyenData.adyenEncryptedExpiryYear = data.encryptedExpiryYear;
                        this.adyenData.adyenEncryptedSecurityCode = data.encryptedSecurityCode;
                    }
                }
            })
            .mount('#customCard-container');
    }

    ngOnInit() {
        this._getPaymentMethods();
        this.paymentDetail.productOrders = [];
        this._activatedRoute.params.subscribe((params) => {
            this._productServiceProxy
                .getCliqueOrderCustomer(this.appSession.customerId)
                .subscribe((result) => {
                    this.paymentDetail = result;
                    this.paymentDetail.productOrders.forEach(item => {
                        if (typeof item.orderTags === 'string' && item.orderTags) {
                            item.orderTags = JSON.parse(item.orderTags);
                        }
                    })

                    this._cliqueWalletServiceProxy
                        .getCustomerWallet(this.appSession.customerId)
                        .subscribe((resultWallet) => {
                            this.customerWallet = resultWallet;
                            if(this.paymentDetail.total <= this.customerWallet.balance){
                                this.isWalletEnough = true;
                            }
                        });
                });
        });
    }

    sumAmount(products: CustomerProductOfferDetailDto[]): number {
        return products.map((p) => p.paidAmount).reduce((a, b) => a + b);
    }

    printPaymentDetail(paymentId: number) {
        this._portalServiceProxy
            .printPaymentDetailHistory(paymentId)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    isEmpty(val): boolean {
        return val === undefined || val == null || val.length <= 0
            ? true
            : false;
    }

    backprev() {
        this.location.back();
    }

    payNow(paymentMethod: string) {
        this._createOrderList(paymentMethod);
        // const dialog = this.dialog.open(PaymentSuccessfulDialogComponent, {
        //     width: '530px',
        //     data: {
        //         payment: `Stack Wallet`,
        //         status: `Paid`,
        //         date: `21 Nov, 2021`,
        //         time: `17:07`,
        //         total: `$41,98`,
        //     },
        //     panelClass: 'dialog-payment-success'
        // });
    }

    private _createOrderList(paymentOption?: string) {
        this.saving = true;
        let request = new CreateOrderCliquePaymentInput();
        // request.purcharsedOrderList = this.order.purcharsedOrderList.map((item) => {
        //     item.timeSlotFormTo = JSON.stringify(item.timeSlotFormTo);

        //     return CreateOrderInput.fromJS(item);
        // });

        if(this.isPayStackWallet){
            paymentOption = "Stack Wallet"
        }

        request.paymentOption = paymentOption || null;
        request.source = '101';
        // request.billingInfo = this.billingInfo;
        request.paidAmount = this.total;
        request.cliqueOrderId = this.paymentDetail.cliqueOrderId;
        request.customerId = this.paymentDetail.customerId;
        request.callbackUrl = location.origin + '/app/portal/member/payment-order-completed/';
        request.cardId = this.customerWallet.cardId;

        switch (paymentOption) {
            case PAYMENT_METHOD.Omise:
                {
                    request.omiseToken = this.omiseData.omiseToken;
                }

                break;
            case PAYMENT_METHOD.Paynow:
                {
                    request.omiseToken = this.omiseData.omisePaynowToken;
                }

                break;
            case PAYMENT_METHOD.Stripe:
                {
                    request.stripeToken = this.stripeData.stripeToken;
                }

                break;
            case PAYMENT_METHOD.Adyen:
                {
                    request.adyenEncryptedCardNumber = this.adyenData.adyenEncryptedCardNumber;
                    request.adyenEncryptedExpiryMonth = this.adyenData.adyenEncryptedExpiryMonth;
                    request.adyenEncryptedExpiryYear = this.adyenData.adyenEncryptedExpiryYear;
                    request.adyenEncryptedSecurityCode = this.adyenData.adyenEncryptedSecurityCode;
                }

                break;
            case PAYMENT_METHOD.PayPal:
                {
                    request.paypalOrderId = this.paypalData.orderId;
                }

                break;
            case PAYMENT_METHOD.Google:
                {
                    request.googlePayToken = this.googlePayData.googlePayToken;
                }

                break;
        }
        const orderDates = this.paymentDetail.productOrders.map((item) => item.orderDate);
        this._cliquePaymentMethodServiceProxy
            .validateCutOff(orderDates)
            .subscribe((result) => {
                this._cliquePaymentMethodServiceProxy
                    .createPaymentForOrder(request)
                    .pipe(finalize(() => (this.saving = false)))
                    .subscribe((result) => {
                        if (this.paymentDetail.total == 0) {
                            this.message.success(this.l('SavedSuccessfully'), '', {
                                timer: 700
                            });
                            setTimeout(() => {
                                this._router.navigate(['/app/portal/member/order-history', result.cliqueOrderId]);
                            }, 700);
                        } else {
                            const dialog = this.dialog.open(PaymentSuccessfulDialogComponent, {
                                width: '530px',
                                data: {
                                    payment: result.paymentMethod,
                                    status: `Paid`,
                                    date: result.datePaidTimeStr,
                                    time: result.timePaidTimeStr,
                                    total: result.total,
                                },
                                panelClass: 'dialog-payment-success'
                            });
                            dialog.afterClosed().subscribe(product => {
                                this._router.navigate(['/app/portal/member/order-history', result.cliqueOrderId]);
                            });
                        }
                    });
        });
    }

    private _isValidBillingInformation() {
        return true;
    }


    payWithAdyen() {
        this._createOrderList(PAYMENT_METHOD.Adyen);
    }

    sendOTP() {
        if (this.swipeCard) {
            this._swipeCardServiceProxy.generateCardOTP(this.swipeCard).subscribe((result) => {
                this.notify.success('Sent Successfully');
            });
        } else {
            this.notify.error('Please select Card Number');
        }
    }
    goToTopup(){
        this.dataService.changeData("topUpHere", "topUpHere");
        localStorage.setItem("paymentTopUp","paymentTopUp");
        this._router.navigate(['/app/portal/member/settings-common']);
    }
}

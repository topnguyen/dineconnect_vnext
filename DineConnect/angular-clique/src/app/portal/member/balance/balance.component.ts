import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Paginator } from 'primeng/paginator';
import { ActivatedRoute } from "@angular/router";
import { LazyLoadEvent } from 'primeng';
import { AppComponentBase } from "@shared/common/app-component-base";
import { TiffinMemberPortalServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-balance",
    templateUrl: "./new-balance.component.html",
    styleUrls: ["./balance.component.scss"],
    animations: [appModuleAnimation()],
})
export class BalanceComponent extends AppComponentBase implements OnInit {
    @ViewChild("balanceTable", { static: true }) balanceTable: Table;
    @ViewChild("balancePaginator", { static: true })
    balancePaginator: Paginator;

    filterText = "";
    isExpiriedOfferFilter = false;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
    }

    ngOnInit() {}

    getBalanceList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.balancePaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._portalServiceProxy
            .getBalancesForCustomer(
                undefined,
                this.isExpiriedOfferFilter,
                this.filterText,
                this.primengTableHelper.getSorting(this.balanceTable) || undefined,
                this.primengTableHelper.getSkipCount(
                    this.balancePaginator,
                    event
                ),
                this.primengTableHelper.getMaxResultCount(
                    this.balancePaginator,
                    event
                )
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });

        this.primengTableHelper.hideLoadingIndicator();
    }

    getActiveBalanceList(number: number) {
        switch (number) {
            case 1:
                this.isExpiriedOfferFilter = false;
                break;
            case 2:
                this.isExpiriedOfferFilter = true;
                break;
            case 3:
                this.isExpiriedOfferFilter = undefined;
                break;

            default:
                break;
        }
        this.getBalanceList();
    }

    reloadPage(): void {
        this.balancePaginator.changePage(this.balancePaginator.getPage());
    }
}

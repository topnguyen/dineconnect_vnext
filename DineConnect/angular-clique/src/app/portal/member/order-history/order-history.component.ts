import { DialogCancelOrderComponent } from './dialog-cancel-order/dialog-cancel-order.component';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CliqueOrderServiceProxy, ListResultDtoOfCliqueOrderHistoryDto, CliqueOrderHistoryDto, CliquePaymentServiceProxy, CliqueOrderStatusInput, CliqueOrderDetailForPrintDto } from './../../../../shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ViewChild, NgZone } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocationDto, OrderHistoryDto, TiffinOrderServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { EditOrderModalComponent } from '../orders/edit-order-modal/edit-order-modal.component';
import { Location } from '@angular/common';
import { DataService } from '@app/shared/common/service/data.service';
import { Subscription } from 'rxjs';
import { OrderTag } from '@app/shared/model/order-tag.model';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('editOrderModal', { static: true }) editOrderModal: EditOrderModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    orderHistory: OrderHistoryDto[] = [];
    listLocation: LocationDto[] = [];
    currentDate = moment();
    cutOffTime = moment(abp.setting.get('App.MealSetting.OrderCutOffTime').replace(/"/g, ''));
    isTimeEdit = false;
    allowDate = moment();
    cliqueOrderHistoryDto: CliqueOrderHistoryDto = new CliqueOrderHistoryDto();
    cliqueOrderHistoryDetailDto: ListResultDtoOfCliqueOrderHistoryDto;
    cliquePreparationReportViewDto: CliqueOrderDetailForPrintDto = new CliqueOrderDetailForPrintDto();


    get total() {
        let orderTagTotal = 0;
        (this.cliqueOrderHistoryDto?.productOrders || []).forEach(element => {
            const orderTags: OrderTag[] = <any>element.orderTags || [];

            orderTags.forEach(item => {
                orderTagTotal += item.price * item.quantity;
            });
        });

        return this.cliqueOrderHistoryDto?.total + orderTagTotal;
    }
    constructor(
        injector: Injector,
        private zone: NgZone,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private location: Location,
        private route: ActivatedRoute,
        private cliqueOrderServiceProxy: CliqueOrderServiceProxy,
        private cliquePaymentService: CliquePaymentServiceProxy,
        private router: Router,
        private dialog: MatDialog
    ) {
        super(injector);

        this.zone.runOutsideAngular(() => {
            setInterval(() => {
                this.disabledEdit();
            }, 1000);
        });
    }

    ngOnInit() {
        this.cliqueOrderServiceProxy.getCliqueOrderHistoryDetail(+this.route.snapshot.paramMap.get("id")).subscribe(product=>{
            this.cliqueOrderHistoryDto = product;
            this.cliqueOrderHistoryDto.productOrders.forEach(item => {
                if (typeof item.orderTags === 'string' && item.orderTags) {
                    item.orderTags = JSON.parse(item.orderTags);
                }
            })

            if(this.cliqueOrderHistoryDto){
                if(this.cliqueOrderHistoryDto.paymentMethod && this.cliqueOrderHistoryDto.paymentMethod != ''){}
                else {
                    this.cliqueOrderHistoryDto.paymentMethod = 'Cash bash';
                }
            }
            
        })
    }

    
    exportExcel() {
        this._tiffinOrderProxy.exportOrderHistory().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
            this.notify.success(this.l('DownloadedSuccessfully'));
        });
    }

    printOrder(record) {
        this._tiffinOrderProxy.printOrderHistory(record.orderId).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    getCutOffTime() {
        this._tiffinOrderProxy.getCutOffTime().subscribe((result) => {
            this.cutOffTime = result;
            this.disabledEdit();
        });
    }

    disabledEdit() {
        let currentDate = moment().add(1, 'second');
        this.isTimeEdit = currentDate.format('HH:mm') < this.cutOffTime.format('HH:mm');

        if (!this.isTimeEdit) {
            this.allowDate = currentDate.add(2, 'days').startOf('day');
        }
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    editOrder(item) {
        this.editOrderModal.show(item.orderId);
    }

    deleteOrder(record) {
        this.message.confirm(this.l('OrderDeleteWarningMessage', record.choiceOfSet), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._tiffinOrderProxy.deleteOrder(record.orderId).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'), '', { timer: 700 });
                });
            }
        });
    }

    backprev() {
        this.location.back();
    }

    cancelOrder(cliqueOrderHistoryDto) {
        this.dialog.open(DialogCancelOrderComponent, {
            width: '530px',
            data: {
                cliqueOrderHistoryDto: cliqueOrderHistoryDto
            },
            panelClass: 'dialog-cancel-order'
        });
        // const STATUS_CANCEL = 4;
        // const cliqueOrderStatus = new CliqueOrderStatusInput();
        // cliqueOrderStatus.cliqueOrderId = cliqueOrderHistoryDto.cliqueOrderId;
        // cliqueOrderStatus.customerId = +localStorage.getItem("customerId");
        // cliqueOrderStatus.orderStatusId = STATUS_CANCEL;
        // this.cliqueOrderServiceProxy.updateStatusCliqueOrder(cliqueOrderStatus).subscribe(result=>{
        //     this.router.navigate(['/app/portal/member/buy-offer']);
        // })
    }

    orderAgain(cliqueOrderHistoryDto) {
        this.cliqueOrderServiceProxy.copyCliqueOrder(cliqueOrderHistoryDto.cliqueOrderId).subscribe(()=>{
            this.router.navigate(['/app/portal/member/payment-details']);
        });
    }

    // ngOnDestroy() {
    //     this.subscriptions.unsubscribe();
    //   }

    print() {
    //     dateOrder: moment.Moment;
    // mealTime: string | undefined;
    // total: number;
    // productOrderList: ProductOrdersDto[] | undefined;
        this.cliquePreparationReportViewDto.dateOrder = this.cliqueOrderHistoryDto.orderTimeFor;
        this.cliquePreparationReportViewDto.mealTime = this.cliqueOrderHistoryDto.cliqueMealTimeName;
        this.cliquePreparationReportViewDto.total = this.cliqueOrderHistoryDto.total;
        this.cliquePreparationReportViewDto.productOrders = this.cliqueOrderHistoryDto.productOrders;

        this.cliqueOrderServiceProxy.printOrderDetail(this.cliquePreparationReportViewDto).subscribe((result)=>{console.log(result);
            this._fileDownloadService.downloadTempFile(result);
        })
    }
}

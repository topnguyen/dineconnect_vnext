import { CliqueOrderServiceProxy, CliqueOrderStatusInput } from './../../../../../shared/service-proxies/service-proxies';
import { OrderHistoryComponent } from './../order-history.component';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
    Component,
    Inject,
    Injector,
    OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { CliqueOrderHistoryDto} from "@shared/service-proxies/service-proxies";
@Component({
    selector: 'dialog-cancel-order',
    templateUrl: './dialog-cancel-order.component.html',
    styleUrls: ['./dialog-cancel-order.component.scss']
})
export class DialogCancelOrderComponent extends AppComponentBase implements OnInit {

    cliqueOrderHistoryDto: CliqueOrderHistoryDto
    constructor(injector: Injector,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private cliqueOrderServiceProxy: CliqueOrderServiceProxy,
        private router: Router,
        private dialogRef: MatDialogRef<OrderHistoryComponent>) {
        super(injector);
    }

    ngOnInit() {
        this.cliqueOrderHistoryDto = this.data.cliqueOrderHistoryDto;
    }

    cancelOrder(){
        const STATUS_CANCEL = 4;
        const cliqueOrderStatus = new CliqueOrderStatusInput();
        cliqueOrderStatus.cliqueOrderId = this.cliqueOrderHistoryDto.cliqueOrderId;
        cliqueOrderStatus.customerId = +localStorage.getItem("customerId");
        cliqueOrderStatus.orderStatusId = STATUS_CANCEL;
        this.cliqueOrderServiceProxy.updateStatusCliqueOrder(cliqueOrderStatus).subscribe(result=>{
            this.router.navigate(['/app/portal/member/buy-offer']);
            this.dialogRef.close();
        })
    }
}
export interface DialogData {
    cliqueOrderHistoryDto: any;
}
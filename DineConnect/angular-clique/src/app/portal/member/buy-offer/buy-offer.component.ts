import { DialogCancelOrderComponent } from './../order-history/dialog-cancel-order/dialog-cancel-order.component';
import { finalize } from 'rxjs/operators';
import { CliquePaymentServiceProxy, ListResultDtoOfCliqueOrderHistoryDto, CliqueOrderStatusInput } from './../../../../shared/service-proxies/service-proxies';
import { Component, OnInit, Injector } from "@angular/core";
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    TiffinMemberPortalServiceProxy,
    TiffinMemberProductOfferDto,
    CliqueOrderServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { DataService } from '@app/shared/common/service/data.service';
import { OrderTag } from '@app/shared/model/order-tag.model';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: "app-buy-offer",
    templateUrl: "./buy-offer.component.html",
    styleUrls: ["./buy-offer.component.scss"],
})
export class BuyOfferComponent extends AppComponentBase implements OnInit {
    productOffers: TiffinMemberProductOfferDto[];
    isCollapsed = new Array(100).fill(true);
    sortResult: number = 1;
    cliqueOrderHistoryDto: ListResultDtoOfCliqueOrderHistoryDto;
    IN_QUEUE_STATUS = 0;
    COMPLETE_STATUS = 1;


    constructor(
        injector: Injector,
        public _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _router: Router,
        private _location: Location,
        private cliqueOrderServiceProxy: CliqueOrderServiceProxy,
        private router: Router,
        private dialog: MatDialog
    ) {
        super(injector);
    }

    ngOnInit() {
        const SCREEN_MOBILE = 991;
        if (window.innerWidth <= SCREEN_MOBILE) {
            this.getCliqueOrderHistoryMobile(this.sortResult, this.IN_QUEUE_STATUS);
        } else {
            this.getCliqueOrderHistory(this.sortResult);
        }
    }

    addToCart(product: TiffinMemberProductOfferDto) {

    }

    back() {
        this._location.back();
    }

    orderDetail(orderHistory) {
        this._router.navigate(["/app/portal/member/order-history", orderHistory.cliqueOrderId]);
        // this._dataService.changeData(orderHistory, "orderHistory");
    }


    getCliqueOrderHistory(sortResult) {
        const customerId = +localStorage.getItem("customerId");

        this.cliqueOrderServiceProxy.getCliqueOrderHistory(customerId, undefined, sortResult, undefined).subscribe(product => {
            this.cliqueOrderHistoryDto = product;

            this.cliqueOrderHistoryDto.items.forEach(item => {
                item.productOrders.forEach(item => {
                    if (typeof item.orderTags === 'string' && item.orderTags) {
                        item.orderTags = JSON.parse(item.orderTags);
                    }
                })
            })
        })
    }
    getCliqueOrderHistoryMobile(sortResult, inQueueStatus) {
        this.showMainSpinner();
        const customerId = +localStorage.getItem("customerId");
        this.cliqueOrderServiceProxy.getCliqueOrderHistory(customerId, undefined, sortResult, inQueueStatus).pipe(
            finalize(() => this.hideMainSpinner())
        ).subscribe(product => {
            this.cliqueOrderHistoryDto = product;
            this.cliqueOrderHistoryDto.items.forEach(item => {
                item.productOrders.forEach(item => {
                    if (typeof item.orderTags === 'string' && item.orderTags) {
                        item.orderTags = JSON.parse(item.orderTags);
                    }
                })
            });
        })
    }

    onChange(sortResult) {
        this.getCliqueOrderHistory(sortResult);
    }

    onGoing() {
        this.getCliqueOrderHistoryMobile(this.sortResult, this.IN_QUEUE_STATUS);
    }
    
    history() {
        this.getCliqueOrderHistoryMobile(this.sortResult, this.COMPLETE_STATUS);
    }

    orderAgain(orderHistory){
        this.cliqueOrderServiceProxy.copyCliqueOrder(orderHistory.cliqueOrderId).subscribe(()=>{
            this.router.navigate(['/app/portal/member/payment-details']);
        });
    }
    cancelOrderMobile(orderHistory) {

        const dialogRef = this.dialog.open(DialogCancelOrderComponent, {
            width: '530px',
            data: {
                cliqueOrderHistoryDto: orderHistory
            },
            panelClass: 'dialog-cancel-order'
        });
        dialogRef.afterClosed().subscribe(result => {
            this.getCliqueOrderHistoryMobile(this.sortResult, this.IN_QUEUE_STATUS);
        });
    }

    total(orderHistory) {
        let orderTagTotal = 0;
        (orderHistory?.productOrders || []).forEach(element => {
            const orderTags: OrderTag[] = <any>element.orderTags || [];

            orderTags.forEach(item => {
                orderTagTotal += item.price * item.quantity;
            });
        });

        return orderHistory?.total + orderTagTotal;
    }

}

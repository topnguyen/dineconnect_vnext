import { Component, OnInit, Injector } from '@angular/core';
import { cloneDeep, remove } from 'lodash';
import { ProductService } from '@app/shared/services/product.service';
import { GetAllTiffinProductOffersInput, TiffinMemberPortalServiceProxy, TiffinMemberProductOfferDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'app-cart-products',
    templateUrl: './new-cart-products.component.html',
    styleUrls: ['./cart-products.component.scss']
})
export class CartProductsComponent extends AppComponentBase implements OnInit {
    cartProducts: TiffinMemberProductOfferDto[];
    showDataNotFound = true;
    productOffers: TiffinMemberProductOfferDto[];
    messageTitle = 'No Products Found in Cart';
    messageDescription = 'Please, Add Products to Cart';
    messageRemoveMealPlan = 'Some meal plans were removed from the cart!';

    constructor(injector: Injector, private productService: ProductService, public _portalServiceProxy: TiffinMemberPortalServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getCartProduct();
    }

    removeCartProduct(product: TiffinMemberProductOfferDto) {
        this.productService.removeLocalCartProduct(product);
        this.getCartProduct();
    }

    getCartProduct() {
        this.cartProducts = this.productService.getLocalCartProducts();
        const input = new GetAllTiffinProductOffersInput();

        this._portalServiceProxy.getActiveOffers(input).subscribe((result) => {
            this.productOffers = result.items;

            const productsremoved = remove(this.cartProducts, (item)=> !this.productOffers.some(product => product.id == item.id));
            if (productsremoved && productsremoved.length > 0) {
                const message = productsremoved.map(item => item.name).join(', ');
                this.message.warn(message, this.messageRemoveMealPlan);
            }

            this.cartProducts.forEach(product => {
                const activeProduct = this.productOffers.find(item => item.id == product.id);
                product = cloneDeep(activeProduct);
                this.productService.updateLocalCartProducts(this.cartProducts);
            })
        });
    }

    clearCart() {
        this.productService.removeAllLocalCartProducts();
        this.getCartProduct();
    }

    changeQty(product) {
        this.productService.updateQuatity(product);

        this.getCartProduct();
    }
}

import { TransferComponent } from './../transfer.component';
import { CustomerForTransferDto } from './../../../../../../shared/service-proxies/service-proxies';
import { TransferConfirmationComponent } from './../transfer-confirmation/transfer-confirmation.component';
import {
    Component,
    Inject,
    Injector,
    OnInit,
} from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { AppComponentBase } from "@shared/common/app-component-base";
@Component({
    selector: 'dialog-enter-price',
    templateUrl: './dialog-enter-price.component.html',
    styleUrls: ['./dialog-enter-price.component.scss']
})
export class DialogEnterPriceComponent extends AppComponentBase implements OnInit {

    price: number;
    customerForTransferDto: CustomerForTransferDto;
    constructor(
        injector: Injector, 
        private dialog: MatDialog, 
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private dialogPrice: MatDialogRef<TransferComponent>) {
        super(injector);
    }

    ngOnInit() {
    }

    enterPrice() {
        const dialogRef = this.dialog.open(TransferConfirmationComponent, {
            width: '529px',
            data: { customerForTransferDto: this.data.customerForTransferDto, price: this.price },
            panelClass: 'dialog-transfer-confirm'
        });
        this.dialogPrice.close();
        dialogRef.afterClosed().subscribe(result => {
        });
    }
}

export interface DialogData {
    customerForTransferDto: CustomerForTransferDto;
}
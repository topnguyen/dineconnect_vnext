import { CustomerForTransferDto, CustomerTransferInput, CliqueWalletServiceProxy } from './../../../../../../shared/service-proxies/service-proxies';
import { Inject } from '@angular/core';
import { TransferComponent } from './../transfer.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Injector } from '@angular/core';
import * as moment from 'moment';
import { DataService } from '../../../../../shared/common/service/data.service';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-transfer-confirmation',
  templateUrl: './transfer-confirmation.component.html',
  styleUrls: ['./transfer-confirmation.component.scss']
})
export class TransferConfirmationComponent extends AppComponentBase implements OnInit {

  public itemPrices: Array<any> = [
    {
      id:1,
      price: '$25',
      isSelected: false
    },
    {
      id:2,
      price: '$50',
      isSelected: false
    },
    {
      id:3,
      price: '$100',
      isSelected: false
    },
    {
      id:4,
      price: '$150',
      isSelected: false
    },
    {
      id:5,
      price: '$200',
      isSelected: false
    },
    {
      id:6,
      price: '$300',
      isSelected: false
    },
    {
      id:7,
      price: '$400',
      isSelected: false
    },
    {
      id:8,
      price: '$500',
      isSelected: false
    },
    {
      id:9,
      price: '$1000',
      isSelected: false
    },
  ]


  customerForTransferDto: CustomerForTransferDto;
  currentDay: any;
  price: number;
  customerTransferInputDto: CustomerTransferInput;
  cardId: number;
  balancerShow: number;

  constructor(
    injector: Injector,
    private dialogRef: MatDialogRef<TransferComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private customerForTransferService: CliqueWalletServiceProxy,
    private dataService: DataService,
  ) { 
    super(injector);
  }

  ngOnInit(): void {
    this.currentDay = moment(new Date()).local().format("MMM DD, YYYY");
    this.customerForTransferDto = this.data.customerForTransferDto;
    this.price = this.data.price;
    this.customerForTransferService
    .getCustomerWallet(this.appSession.customerId)
    .subscribe((resultWallet) => {
      if(resultWallet != null){
        this.cardId = resultWallet.cardId;
        this.balancerShow = resultWallet.balance - this.price;
      }
    });
  }

  confirm(){
    this.customerTransferInputDto = new CustomerTransferInput();
    this.customerTransferInputDto.cardId = this.cardId;
    this.customerTransferInputDto.money = this.price;
    this.customerTransferInputDto.fromCustomerId = +localStorage.getItem("customerId");
    this.customerTransferInputDto.fromCustomerName = "";
    this.customerTransferInputDto.toCustomerCardId = this.customerForTransferDto.customerCardId;
    this.customerTransferInputDto.toCustomerName = this.customerForTransferDto.customerName;
    this.customerTransferInputDto.toCustomerId = this.customerForTransferDto.customerId;
    this.customerForTransferService.customerTransferTopup(this.customerTransferInputDto).subscribe(result => {
      this.dataService.changeData( this.balancerShow, "balanceCustomer")
      this.dialogRef.close();
      this.notify.success(this.l('Money transfer successful'));
    })
  }
}

export interface DialogData {
  customerForTransferDto: CustomerForTransferDto;
  price: number;
}
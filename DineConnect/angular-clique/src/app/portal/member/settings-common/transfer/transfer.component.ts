import { CliqueWalletServiceProxy, CustomerForTransferDto, CustomerTransferInput } from './../../../../../shared/service-proxies/service-proxies';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TransferConfirmationComponent } from './transfer-confirmation/transfer-confirmation.component';
import { DialogEnterPriceComponent } from './dialog-enter-price/dialog-enter-price.component';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  public itemPrices: Array<any> = [
    {
      id: 1,
      price: 25,
      isSelected: false
    },
    {
      id: 2,
      price: 50,
      isSelected: false
    },
    {
      id: 3,
      price: 100,
      isSelected: false
    },
    {
      id: 4,
      price: 150,
      isSelected: false
    },
    {
      id: 5,
      price: 200,
      isSelected: false
    },
    {
      id: 6,
      price: 300,
      isSelected: false
    },
    {
      id: 7,
      price: 400,
      isSelected: false
    },
    {
      id: 8,
      price: 500,
      isSelected: false
    },
    {
      id: 9,
      price: 1000,
      isSelected: false
    },
  ]

  filter: string;
  price: number = 0;
  customerForTransferDtoList: Array<CustomerForTransferDto>;
  customerForTransferDto: CustomerForTransferDto;
  customerTransferInputDto: CustomerTransferInput;
  constructor(
    private dialog: MatDialog,
    private customerForTransferService: CliqueWalletServiceProxy
  ) { }

  ngOnInit(): void {
    this.getCustomerForTransfer(this.filter);
  }
  selectPrice(priceObj: any) {
    this.price = priceObj.price;
    this.itemPrices.forEach(priceItem => {
      if (priceItem.id === priceObj.id) {
        priceItem.isSelected = true;
      } else {
        priceItem.isSelected = false;
      }
    })
  }

  showDialog() {
    const dialogRef = this.dialog.open(TransferConfirmationComponent, {
      width: '529px',
      data: { customerForTransferDto: this.customerForTransferDto, price: this.price },
      panelClass: 'dialog-transfer-confirm'
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  getCustomerForTransfer(filter: string) {
    this.customerForTransferService.getCustomerForTranfer(filter).subscribe(result => {
      if(result != null && result.length > 3){
        this.customerForTransferDtoList = result.slice(0, 3);
      }
      else {
        this.customerForTransferDtoList = result;
      }
    })
  }
  selectCustomer(customer: CustomerForTransferDto) {
    this.customerForTransferDto = customer;
    this.customerForTransferDtoList.forEach(customerItem => {
      if (customerItem.customerId == customer.customerId) {
        customerItem.isSelected = true;
      } else {
        customerItem.isSelected = false;
      }
    })
  }
  selectAnyPrice() {
    this.price = 0;
    this.itemPrices.forEach(priceItem => {
      priceItem.isSelected = false;
    })
    
    const dialogRef = this.dialog.open(DialogEnterPriceComponent, {
      width: '529px',
      data: { customerForTransferDto: this.customerForTransferDto },
      panelClass: 'dialog-enter-price'
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}

import { AppComponentBase } from '@shared/common/app-component-base';
import { CliqueCustomerDto, CliqueCustomerServiceProxy, CliqueCustomerInput } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { EmployerAccountInfo } from '@app/portal/services/order.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent extends AppComponentBase {

  constructor(
    private fb: FormBuilder,
    private profileService: CliqueCustomerServiceProxy,
    injector: Injector
  ) {
    super(injector);
   }
  form: FormGroup
  lstGenders: string[] = ['Male', 'Female', 'Others'];
  favoriteGenders: string;
  formChanged = false;
  originAccountInfo: EmployerAccountInfo;
  customerInformation: CliqueCustomerDto
  updateCustomer: CliqueCustomerInput;
  ngOnInit() {
    this.loadProfile();
    this.form = this.fb.group({
      username: new FormControl(''),
      name: new FormControl('', {
        // validators: [Validators.required, Validators.maxLength(100)],
      }),
      email: new FormControl(''),

      contactNumber: new FormControl('', {
        // validators: [Validators.required, PhoneNumberValidator, Validators.maxLength(14), Validators.minLength(6)]
      }),

      genderId: new FormControl('', {
        // validators: [Validators.required],
      }),
      dob: new FormControl(''),
    });
  }

  get username() {
    return this.form.controls.username as FormControl;
  }

  get name() {
    return this.form.controls.name as FormControl;
  }

  get email() {
    return this.form.controls.email as FormControl;
  }

  get contactNumber() {
    return this.form.controls.contactNumber as FormControl;
  }

  get genderId() {
    return this.form.controls.genderId as FormControl;
  }

  get dob() {
    return this.form.controls.dob as FormControl;
  }

  listenToFormChanges() {
    this.form.valueChanges.subscribe(val => {
      this.formChanged = val.username !== this.originAccountInfo.username;
      this.formChanged = val.name !== this.originAccountInfo.name;
      this.formChanged = val.email !== this.originAccountInfo.email;
      this.formChanged = val.contactNumber !== this.originAccountInfo.contactNumber;
      this.formChanged = val.genderId !== this.originAccountInfo.genderId;
      this.formChanged = val.dob !== this.originAccountInfo.dob;
    });
  }

  bindFormValues(info: CliqueCustomerDto) {
    if (info) {
      this.form.controls.username.setValue(info.userName);
      this.form.controls.name.setValue(info.customerName);
      this.form.controls.email.setValue(info.emailAddress);
      this.form.controls.contactNumber.setValue(info.phoneNumber);
      // this.form.controls.genderId.setValue(info.genderId);
      // this.form.controls.dob.setValue(info.dob);
    }
  }
  resetValue() {
    // this.bindFormValues(this.originAccountInfo)
  }

  changeInfo() {
    const updateCustomerInformation = new CliqueCustomerInput();
    updateCustomerInformation.customerId = +localStorage.getItem("customerId");
    updateCustomerInformation.customerName = this.name.value;
    updateCustomerInformation.phoneNumber = this.contactNumber.value;
    this.profileService.updateCustomerInfomation(updateCustomerInformation).subscribe(customer=>{
      this.notify.success(this.l('Your Information Has Changed Successfully'));
    })
  }


  loadProfile(){
    this.profileService.getCustomerInfomation(+localStorage.getItem("customerId")).subscribe(customer=>{
      this.customerInformation = customer;
      this.bindFormValues(customer);
    })
  }

}

import { Validators } from '@angular/forms';
import { Component, OnInit, Injector } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CliqueCustomerPasswordInput, CliqueCustomerServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-my-password',
  templateUrl: './my-password.component.html',
  styleUrls: ['./my-password.component.scss']
})
export class MyPasswordComponent extends AppComponentBase {

  constructor(
    private fb: FormBuilder,
    private cliqueCustomerServiceProxy: CliqueCustomerServiceProxy,
    injector: Injector
  ) { 
    super(injector);
  }
  form: FormGroup
  lstGenders: string[] = ['Male', 'Female', 'Others'];
  hideNewPassword = true;
  hideReEnterPassword = true;

  ngOnInit() {
    this.form = this.fb.group(
      {
        newPassword: new FormControl('', {
          // validators: [
          //   Validators.required,
          //   Validators.minLength(this.minPasswordLen),
          //   Validators.pattern(/^(?=.{8,}$)(?=.*[a-z])(?!.*[\s])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/),
          // ],
        }),
        confirmPassword: new FormControl('', {
          validators: [Validators.required, this.passwordCf.bind(this)],
        }),
      },
      // { validators: confirmPasswordValidator() },
    );
  }

  get password() {
    return this.form.controls.password as FormControl;
  }

  get newPassword() {
    return this.form.controls.newPassword as FormControl;
  }

  get confirmPassword() {
    return this.form.controls.confirmPassword as FormControl;
  }

  passwordCf() {
    if (this.form !== null && this.form !== undefined) {
      const newPass = this.form.controls.newPassword;
      const confirmPass = this.form.controls.confirmPassword;
      return newPass?.value === confirmPass?.value ? null : { passwordNotMatch: true };
    }
    return null;
  }

  passwordChanged(value: string) {
    if (this.form !== null && this.form !== undefined) {
      const repassword = this.form.controls.confirmPassword;
      if (repassword.value !== '' && value !== repassword.value) {
        this.form.controls.confirmPassword.setErrors({ passwordNotMatch: true });
      }
      if (repassword.value !== '' && value === repassword.value) {
        this.form.controls.confirmPassword.setErrors(null);
      }
    }
  }

  hasError(control: AbstractControl, errorName: string) {
    return control && (control.touched || control.dirty) && control.hasError(errorName);
}

  changeInfo() {
    const customerChangePassword = new CliqueCustomerPasswordInput();
    // customerChangePassword.currentPassword = this.password.value;
    customerChangePassword.newPassword = this.newPassword.value;
    this.cliqueCustomerServiceProxy.cliqueCustomerChangePassword(customerChangePassword).subscribe(profile=>{
      this.notify.success(this.l('Your Password Has Changed Successfully'));
    })
  }

}

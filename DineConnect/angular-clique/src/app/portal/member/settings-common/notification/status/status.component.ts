import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ProfileServiceProxy } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent extends AppComponentBase {

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileServiceProxy,
    injector: Injector,
    private router: Router
  ) {
    super(injector);
  }

  goToOrderHistory() {
    this.router.navigate(["/app/portal/member/order-history", 41]);
  }

}

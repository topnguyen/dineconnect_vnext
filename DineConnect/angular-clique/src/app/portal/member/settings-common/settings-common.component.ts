import { DataService } from './../../../shared/common/service/data.service';
import { Subscription } from 'rxjs';
import { Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogLogoutComponent } from '@app/shared/common/dialog-logout/dialog-logout.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings-common',
  templateUrl: './settings-common.component.html',
  styleUrls: ['./settings-common.component.scss']
})
export class SettingsCommonComponent extends AppComponentBase implements OnInit {

  options: FormGroup;
  openTab = 'profile';
  panelOpenState = true;
  panelOpenState2 = false;
  private subscriptions = new Subscription();
  constructor(
    fb: FormBuilder, 
    private dialog: MatDialog,
    injector: Injector, 
    private dataService: DataService,
    private router: Router
  ) {
    super(injector);
    this.options = fb.group({
      bottom: 0,
      fixed: false,
      top: 0,
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(this.dataService.currentData.subscribe(data => {
      if (data[1] == "topUpHere") {
        this.openTab = 'top-up-wallet'
      }
    }))
  }

  //Open profile
  profile() {
    this.openTab = 'profile'
  }

  //Open change password
  changePassword() {
    this.openTab = 'changePassword'
  }

  // Open My Wallet
  myWallet() {
    this.openTab = 'my-wallet'
  }

  // Open Top Up Wallet
  topUpWallet() {
    this.openTab = 'top-up-wallet'
  }

  // Open Top Up Detail
  topUpDetail() {
    this.openTab = 'gui-top-up-wallet'
  }

  // Open Transfer
  transfer() {
    this.openTab = 'transfer'
  }

  //Open Status
  status() {
    this.openTab = 'status'
  }

  //Open Promotions
  promotions() {
    // this.openTab = 'notifications'
  }

  //Logout
  logOut() {
    // this.openTab = 'logout'
    const dialogRef = this.dialog.open(DialogLogoutComponent, {
      width: '529px',
      data: {},
      panelClass: 'dialog-logout'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }
  notification() {
    this.openTab = 'notifications';
  }
  goToHome() {
    this.router.navigate(["/app/portal/member/schedule"]);
  }
}

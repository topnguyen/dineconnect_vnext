import { Component, OnInit, Injector, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../../../../shared/common/service/data.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CliqueWalletServiceProxy,
  CustomerWalletDto,
  CustomerTopupInput,
} from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-gui-top-up-wallet',
  templateUrl: './gui-top-up-wallet.component.html',
  styleUrls: ['./gui-top-up-wallet.component.scss']
})
export class GuiTopUpWalletComponent extends AppComponentBase implements OnInit {

  panelOpenState = true;
  panelOpenState2 = false;
  panelOpenState3 = false;
  topupPrice = 0;
  customerWallet: CustomerWalletDto = new CustomerWalletDto();
  customerTopupInput: CustomerTopupInput = new CustomerTopupInput();

  @Output()
  showStackWallet = new EventEmitter();

  constructor(
    injector: Injector,
    private dataService: DataService,
    private _activatedRoute: ActivatedRoute,
    private _cliqueWalletServiceProxy: CliqueWalletServiceProxy,
    private _router: Router,
  ) {
    super(injector);
   }

  ngOnInit(): void {
    this.dataService.currentData.subscribe(data => {
      if (data[1] == "topupPrice") {
          this.topupPrice = data[0];
      }
    });
    this._activatedRoute.params.subscribe((params) => {
      this._cliqueWalletServiceProxy
        .getCustomerWallet(this.appSession.customerId)
        .subscribe((resultWallet) => {
          this.customerWallet = resultWallet;
        });
    });
  }

  understand() {
    if(this.topupPrice > 0){
      this.customerTopupInput.customerId = this.appSession.customerId;
      this.customerTopupInput.money = this.topupPrice;
      this.customerTopupInput.cardId = this.customerWallet.cardId;

      this._cliqueWalletServiceProxy
        .toptupRecharge(this.customerTopupInput)
        .subscribe(() => {
          this.notify.success(this.l('Topup recharge successfully'));
          this.showStackWallet.emit()
        });
    }
  }
}

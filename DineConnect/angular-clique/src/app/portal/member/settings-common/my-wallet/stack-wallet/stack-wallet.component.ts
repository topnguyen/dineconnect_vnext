import { Component, EventEmitter, OnInit, Output, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../../../shared/common/service/data.service';
import {
  CliqueWalletServiceProxy,
  CustomerWalletDto,
  CustomerTransactionDto,
} from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-stack-wallet',
  templateUrl: './stack-wallet.component.html',
  styleUrls: ['./stack-wallet.component.scss']
})
export class StackWalletComponent extends AppComponentBase implements OnInit {

  @Output()
  showTopUpWallet = new EventEmitter();

  @Output()
  showTransfer = new EventEmitter();

  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _cliqueWalletServiceProxy: CliqueWalletServiceProxy,
    private dataService: DataService,
  ) {
    super(injector);
  }

  customerWallet: CustomerWalletDto = new CustomerWalletDto();
  listTransaction: CustomerTransactionDto[] = new Array<CustomerTransactionDto>();

  public transactionContent: Array<any> = []

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      this._cliqueWalletServiceProxy
        .getCustomerWallet(this.appSession.customerId)
        .subscribe((resultWallet) => {
          this.customerWallet = resultWallet;
        });
      this._cliqueWalletServiceProxy
        .getCustomerTransaction(this.appSession.customerId)
        .subscribe((resultWallet) => {
          this.listTransaction = resultWallet;
          for (var i = 0; i < this.listTransaction.length; i++) {
            var itemTransaction = this.listTransaction[i];

            var item = {
              urlImg: '/assets/image/transaction_payment.png',
              nameWallet: 'Payment',
              menu: itemTransaction.description,
              time: itemTransaction.transactionTime,
              price: '0',
              statusOrder: 'COMPLETE',
            }

            if (itemTransaction.transactionType == 0) {
              item.urlImg = '/assets/image/transaction_topup.png';
              item.nameWallet = 'Top Up';
            }
            else if (itemTransaction.transactionType == 1) {
              item.urlImg = '/assets/image/transaction_transfer.png';
              item.nameWallet = 'Transfer';
            }
            else if (itemTransaction.transactionType == 2) {
              item.urlImg = '/assets/image/transaction_payment.png';
              item.nameWallet = 'Payment';
            }

            if (itemTransaction.debit > 0) {
              item.price = "-$" + itemTransaction.debit.toFixed(2);
            }

            if (itemTransaction.credit > 0) {
              item.price = "+$" + itemTransaction.credit.toFixed(2);
            }

            this.transactionContent.push(item);
          }
        });
    });
  }

  topUpWallet() {
    this.showTopUpWallet.emit()
  }

  goToTransfer() {
    this.showTransfer.emit()
  }

}

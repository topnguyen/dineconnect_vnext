import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, Renderer2 } from '@angular/core';
import { AddonSettingsServiceProxy, CliquePaymentServiceProxy, CliqueWalletServiceProxy, CustomerTopupInput, CustomerWalletDto, GetWheelPaymentMethodForViewDto, TiffinMemberPortalServiceProxy } from '@shared/service-proxies/service-proxies';
import {
  PAYMENT_METHOD,
  SYSTEM_NAME,
  STEP_ORDER,
} from '@app/portal/services/order.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';

declare let OmiseCard: any;
declare let Omise: any;

@Component({
  selector: 'app-top-up-wallet',
  templateUrl: './top-up-wallet.component.html',
  styleUrls: ['./top-up-wallet.component.scss']
})
export class TopUpWalletComponent extends AppComponentBase implements OnInit {

  @Output()
  showGuiTopUp = new EventEmitter()

  @Output()
  showStackWallet = new EventEmitter();

  constructor(
    injector: Injector,
    private _addonSettingsServiceProxy: AddonSettingsServiceProxy,
    private _elRef: ElementRef,
    private _render: Renderer2,
    private _cliquePaymentMethodServiceProxy: CliquePaymentServiceProxy,
    private _portalServiceProxy: TiffinMemberPortalServiceProxy,
    private _cliqueWalletServiceProxy: CliqueWalletServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    super(injector);
  }

  customerWallet: CustomerWalletDto = new CustomerWalletDto();
  customerTopupInput: CustomerTopupInput = new CustomerTopupInput();
  public itemPrices: Array<any> = [
    {
      price: '$25',
      value: 25,
      isSelected: false
    },
    {
      price: '$50',
      value: 50,
      isSelected: false
    },
    {
      price: '$100',
      value: 100,
      isSelected: false
    },
    {
      price: '$150',
      value: 150,
      isSelected: false
    },
    {
      price: '$200',
      value: 200,
      isSelected: false
    },
    {
      price: '$300',
      value: 300,
      isSelected: false
    },
    {
      price: '$400',
      value: 400,
      isSelected: false
    },
    {
      price: '$500',
      value: 500,
      isSelected: false
    },
    {
      price: '$1000',
      value: 1000,
      isSelected: false
    },
  ]
  panelOpenState = true;
  panelOpenState2 = false;
  serviceFeePercentage: number;
  serviceFees = [];
  paymentMethod: "";
  PAYMENT_METHOD = PAYMENT_METHOD;
  omiseConfig = {
    publishKey: '',
    secretKey: '',
    enablePaynowQR: false
  };
  omiseData = {
    label: '',
    omiseToken: '',
    omisePaynowToken: ''
  };
  priceSelected: any;
  isSelecteValue: boolean = false;

  ngOnInit(): void {
    this._getPaymentMethods();
    this._activatedRoute.params.subscribe((params) => {
      this._cliqueWalletServiceProxy
        .getCustomerWallet(this.appSession.customerId)
        .subscribe((resultWallet) => {
          this.customerWallet = resultWallet;
        });
    });
  }

  selectPrice(price) {
    this.itemPrices.forEach(priceItem => {
      if (priceItem.value === price.value) {
        priceItem.isSelected = true;
        this.priceSelected = price;
        this.isSelecteValue = true;
      } else {
        priceItem.isSelected = false;
      }
    })
  }

  // guiTopUp() {
  //   this.showGuiTopUp.emit()
  // }

  private _getServiceCharge(name) {
    this.serviceFeePercentage = 0;
    this._addonSettingsServiceProxy.getServiceCharge(name).subscribe((result) => {
      this.serviceFees = result || [];
      this.serviceFees.map((item) => {
        this.serviceFeePercentage = this.serviceFeePercentage + item.percentage;
      });
    });
  }

  private _getPaymentMethods() {
    this._cliquePaymentMethodServiceProxy.getPaymentMethod(undefined, undefined, undefined, undefined, -1, -1, -1, -1, 1, undefined, 0, 1000).subscribe((result) => {
      this._getSettings(result.items);
      this._initPayment();
    });
  }

  choosepaymentMethod() {
    this._getServiceCharge(this.paymentMethod);

    let els = this._elRef.nativeElement.querySelectorAll('div.payment-method');
    els.forEach((el) => {
      this._render.removeClass(el, 'selected');
    });

    if (this.paymentMethod === PAYMENT_METHOD.Omise || this.paymentMethod === PAYMENT_METHOD.Adyen) {
      els.forEach((el) => {
        this._render.addClass(el, 'selected');
      });
    }
  }

  private _getSettings(items: GetWheelPaymentMethodForViewDto[]) {
    items.map((item) => {
      switch (item.systemName) {
        case SYSTEM_NAME.Omise:
          this.omiseConfig = item.dynamicFieldData;
          break;

        default:
          break;
      }
    });
  }

  private _initPayment() {
    this.omiseData.label = this.l('OnlinePayment') + ' - ' + this.l('Omise');
    if (this.omiseConfig?.publishKey) {
      this.initOmiseElements();
    }
  }

  private initOmiseElements() {
    OmiseCard.configure({
      publicKey: this.omiseConfig.publishKey
    });
    Omise.setPublicKey(this.omiseConfig.publishKey);

    let button = document.querySelector('#checkoutButton');
    if (!button) {
      return;
    }

    button.addEventListener('click', (event) => {
      event.preventDefault();
      if (!this._isValidBillingInformation()) {
        return;
      }

      let self = this;

      OmiseCard.open({
        amount: this.priceSelected.value * 100,
        currency: this.appSession.application.currency,
        defaultPaymentMethod: 'credit_card',
        onCreateTokenSuccess: (nonce) => {
          if (nonce.startsWith('tokn_')) {
            this.omiseData.omiseToken = nonce;

            self.createPayment(PAYMENT_METHOD.Omise);
          }
        }
      });
    });

    let myButton = document.querySelector('#payNow');
    if (!myButton || !this.omiseConfig?.enablePaynowQR) {
      return;
    }

    myButton.addEventListener('click', (event) => {
      event.preventDefault();
      if (!this.isValidBillingInformation()) {
        return;
      }

      this._portalServiceProxy
        .createOmisePaymentNowRequest(this.priceSelected.value * 100, this.appSession.application.currency)
        .subscribe((result) => {
          const response = result ? JSON.parse(result) : null;
        });
    });
  }

  private createPayment(paymentMethod?: string) {
    if (this.priceSelected.value > 0) {
      this.customerTopupInput.customerId = this.appSession.customerId;
      this.customerTopupInput.money = this.priceSelected.value;
      this.customerTopupInput.cardId = this.customerWallet.cardId;
      this.customerTopupInput.paymentType = paymentMethod;
      this.customerTopupInput.callbackUrl = location.origin + '/app/portal/member/settings-common';

      switch (paymentMethod) {
        case PAYMENT_METHOD.Omise:
          {
            this.customerTopupInput.omiseToken = this.omiseData.omiseToken;
          }
          break;
      }

      this._cliqueWalletServiceProxy
        .toptupRecharge(this.customerTopupInput)
        .subscribe(() => {
          if (localStorage.getItem("paymentTopUp")) {
            this.router.navigate(["/app/portal/member/payment-details"]);
            localStorage.removeItem("paymentTopUp");
            return;
          }
          this.notify.success(this.l('Topup recharge successfully'));
          this.showStackWallet.emit()
        });
    }
  }

  private isValidBillingInformation() {
    return true;
  }

  private _isValidBillingInformation() {
    return true;
  }
}

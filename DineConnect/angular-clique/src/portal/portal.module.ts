import { NgModule } from "@angular/core";
import { PortalRoutingModule } from "./portal-routing.module";
import { PortalComponent } from "./portal.component";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { UtilsModule } from "@shared/utils/utils.module";
import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { AppModule } from "@app/app.module";
import { AppCommonModule } from "@app/shared/common/app-common.module";
import { AccountModule } from "@account/account.module";
import { PortalIndexComponent } from "./portal-index/portal-index.component";
import { PortalRouteGuard } from "@app/shared/common/auth/portal-route-guard";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { CommonModule } from "@angular/common";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from "ngx-bootstrap/datepicker";
import { DineConnectCommonModule } from "@shared/common/common.module";
import {MatBadgeModule} from '@angular/material/badge';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import { ViewOffersComponent } from "./view-offers/view-offers.component";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        PortalRoutingModule,
        AccountModule,
        AppCommonModule,
        CommonModule,
        DineConnectCommonModule,
        UtilsModule,
        ServiceProxyModule,
        FormsModule,
        AppModule,
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CarouselModule.forRoot(),
        MatIconModule,
        MatBadgeModule,
        MatMenuModule,
        MatTabsModule,
    ],
    declarations: [PortalComponent, ViewOffersComponent, PortalIndexComponent],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
        PortalRouteGuard,
    ],
})
export class PortalModule {}

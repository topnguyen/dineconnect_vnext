import { RouterModule } from "@angular/router";
import { PortalComponent } from "./portal.component";
import { MenuComponent } from "@app/shared/common/menu/menu.component";
import { TermAndConditionsComponent } from "@app/shared/common/term-and-conditions/term-and-conditions.component";
import { NgModule } from "@angular/core";
import { PrivacyComponent } from "@app/shared/common/privacy/privacy.component";
import { AboutUsComponent } from "@app/shared/common/aboutUs/aboutUs.component";
import { FaqComponent } from "@app/shared/common/faq/faq.component";
import { ContactComponent } from "@app/shared/common/contact/contact.component";
import { FeedbackComponent } from "@app/shared/common/feedback/feedback.component";
import { LoginComponent } from "@account/login/login.component";
import { MemberComponent } from "@account/member/member.component";
import { CreateMemberComponent } from "@account/member/create-member/create-member.component";
import { ForgotPasswordMemberComponent } from "@account/member/forgot-password-member/forgot-password-member.component";
import { PortalRouteGuard } from "@app/shared/common/auth/portal-route-guard";
import { PortalIndexComponent } from "./portal-index/portal-index.component";
import { ResetPasswordComponent } from "@account/password/reset-password.component";
import { ViewOffersComponent } from "./view-offers/view-offers.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "portal",
                component: PortalComponent,
                canActivate: [PortalRouteGuard],
                canActivateChild: [PortalRouteGuard],
                children: [
                    { path: "", component: PortalIndexComponent },
                    { path: "menu", component: MenuComponent },
                    { path: "login", component: MemberComponent },
                    { path: "register", component: CreateMemberComponent },
                    { path: "forgot-password",component: ForgotPasswordMemberComponent},
                    { path: "reset-password", component: ResetPasswordComponent },
                    { path: "aboutUs", component: AboutUsComponent },
                    { path: "faq", component: FaqComponent },
                    { path: "contact", component: ContactComponent },
                    { path: "feedback", component: FeedbackComponent },
                    { path: "privacy", component: PrivacyComponent },
                    { path: "term-and-conditions",component: TermAndConditionsComponent},
                    { path: "view-offers", component: ViewOffersComponent },
                
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class PortalRoutingModule {}

import { RegisterMemberInput } from '@shared/service-proxies/service-proxies';

export class RegisterMemberModel extends RegisterMemberInput {
    public passwordRepeat: string;
}

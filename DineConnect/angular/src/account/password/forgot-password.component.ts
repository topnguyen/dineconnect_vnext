import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AccountServiceProxy, SendPasswordResetCodeInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './forgot-password.component.html',
    styleUrls:['./forgot-password.component.scss'],
    animations: [accountModuleAnimation()]
})
export class ForgotPasswordComponent extends AppComponentBase {
    model = new SendPasswordResetCodeInput({domain: AppConsts.appBaseUrl, emailAddress: ''});
    saving = false;

    constructor (
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _router: Router
        ) {
        super(injector);
    }

    save(): void {
        this.saving = true;
        this._accountService.sendPasswordResetCode(this.model)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.message.success(this.l('PasswordResetMailSentMessage'), this.l('MailSent')).then(() => {
                    this._router.navigate(['account/login']);
                });
            });
    }
}

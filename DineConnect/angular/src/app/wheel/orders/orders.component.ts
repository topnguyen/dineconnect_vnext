import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateWheelTicketShippingOrderInputDto,
    UpdateWheelOrderStatusInput,
    WheelOrderListDto,
    WheelOrderServiceProxy,
    WheelTicketShippingOrderServiceProxy
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/api';
import { DataView } from 'primeng/dataview';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
    animations: [appModuleAnimation()]
})
export class OrdersComponent extends AppComponentBase implements OnInit, OnDestroy {
    @ViewChild('ordersListView', { static: true }) ordersListView: DataView;
    orderList: WheelOrderListDto[] = [];

    filterStatus: number = undefined;
    filterText = '';
    emptyMessage = 'Loading orders...';
    first: number = 0;
    loading: boolean = false;
    sortField: string = 'CreationTime';
    sortOrder: number = -1;
    displayDialog: boolean;
    dateRange: any = [moment().toDate(), moment().toDate()];
    isDisabled = false;
    statusOptions: any[] = [
        { value: '0', label: 'Order Received', color: 'green' },
        { value: '1', label: 'Accepted', color: 'green' },
        { value: '2', label: 'Preparing', color: '#ff8d60' },
        { value: '3', label: 'Prepared', color: '#0cc27e' },
        { value: '4', label: 'Delivering', color: '#1cbcd8' },
        { value: '5', label: 'Delivered', color: '#1cbcd8' }
    ];

    ordersCountPerPage = [
        { name: '20', value: 20 },
        { name: '60', value: 60 },
        { name: '100', value: 100 },
        { name: '200', value: 200 }
    ];

    lalaMoveServiceTypes = [
        { label: 'MOTORCYCLE', value: 'MOTORCYCLE' },
        { label: 'CAR', value: 'CAR' },
        { label: 'MINIVAN', value: 'MINIVAN' },
        { label: 'VAN', value: 'VAN' },
        { label: 'TRUCK330', value: 'TRUCK330' },
        { label: 'TRUCK550', value: 'TRUCK550' }
    ];

    layout: string;

    selectedOrderCounts = this.ordersCountPerPage[0];

    currentPageReportTemplate = '{first} - {last} of {totalRecords}';
    orderTotalCount: number;
    selectedOrder: any;
    polling: any;
    lastCount = -1;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private wheelOrderServiceProxyService: WheelOrderServiceProxy,
        private wheelTicketShippingOrderServiceProxy: WheelTicketShippingOrderServiceProxy,
        private confirmationService: ConfirmationService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.polling = setInterval(() => {
            this.isDisabled = true;
            this.getOrders();
        }, 30000);

        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getOrders();
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
        clearInterval(this.polling);
    }

    loadData(event): void {
        const requestPage = event.first / event.rows;
        const nextPage = requestPage > 0 ? requestPage + 1 : 0;
        if (nextPage < requestPage) {
            return;
        }
        this.getOrders();
    }

    getOrders() {
        this.emptyMessage = 'Loading orders...';
        this.loading = true;
        let sorting = this.ordersListView.sortOrder == 1 ? 'ASC' : 'DESC';

        this.wheelOrderServiceProxyService
            .getAlllWheelOrders(
                this.filterStatus,
                this.dateRange[0],
                this.dateRange[1],
                undefined,
                this.ordersListView.sortField ? this.ordersListView.sortField + ' ' + sorting : '',
                this.ordersListView.rows,
                this.ordersListView.first
            )
            .subscribe((orderListResult) => {
                this.isDisabled = false;
                this.loading = false;
                this.orderList = orderListResult.items;
                this.orderTotalCount = orderListResult.totalCount;

                if (!this.orderList.length) {
                    this.emptyMessage = 'No orders found.';
                    this.lastCount = -1;
                }
                if (this.lastCount >= 0 && orderListResult.totalCount > this.lastCount) {
                    this.playAudio();
                }
                this.lastCount = orderListResult.totalCount;
            });
    }

    changeStatus(event: Event, order: WheelOrderListDto): void {
        var input = new UpdateWheelOrderStatusInput();
        input.invoiceNo = order.invoiceNo;
        input.status = order.orderStatus + 1;

        this.wheelOrderServiceProxyService.updateWheelOrderStatus(input).subscribe(() => {
            order.orderStatus = order.orderStatus + 1;
            this.getOrders();
        });
        event.preventDefault();
        event.stopPropagation();
    }
    changeAcceptStatus(event: Event, order: WheelOrderListDto): void {
        var input = new UpdateWheelOrderStatusInput();
        input.invoiceNo = order.invoiceNo;
        input.status = 2;

        this.wheelOrderServiceProxyService.updateWheelOrderStatus(input).subscribe(() => {
            order.orderStatus = order.orderStatus + 1;
            this.getOrders();
        });
        event.preventDefault();
        event.stopPropagation();
    }
    getOrderStatus(status: any): any {
        return this.statusOptions.find((t) => t.value == status.toString());
    }

    selectOrder(event: Event, order: WheelOrderListDto): void {
        this.selectedOrder = order;
        this.displayDialog = true;
        event.preventDefault();
    }

    onDialogHide(): void {
        this.selectedOrder = null;
    }

    playAudio() {
        this.notify.warn('Have some new orders!');
        let audio = new Audio();
        audio.src = '../../../assets/audio/alarm.wav';
        audio.load();
        audio.play();
    }

    getNewOrderClass(time) {
        let timeTocheck = moment().subtract('seconds', 30);
        return time >= timeTocheck;
    }

    placeDeliveryOrder() {
        var input = new CreateWheelTicketShippingOrderInputDto();
        input.ticketId = this.selectedDeliveryOrder.order.wheelTicketId;
        input.isManual = this.isManual;

        if (this.isManual) {
            input.serviceType = this.selectedLalaMoveServiceType;
            input.manualTime = moment(this.manualTime).format('YYYY/MM/DD HH:mm');
        }
        this.wheelTicketShippingOrderServiceProxy.createWheelTicketShippingOrder(input).subscribe((newId) => {
            this.selectedDeliveryOrder.activeShippingDeliveryOrder = newId;
            this.notify.success('Request Delivery successfully!');
            this.onPlaceShippingOrderDialogHide();
        });
    }

    cancelDelivery(event: Event, order: WheelOrderListDto) {
        this.wheelTicketShippingOrderServiceProxy.cancelWheelTicketShippingOrder(order.order.wheelTicketId).subscribe(() => {
            this.notify.success('Cancel Delivery successfully!');
            order.activeShippingDeliveryOrder = null;
            order.activeShippingDeliveryOrderStatus = null;
        });
        event.preventDefault();
        event.stopPropagation();
    }

    selectedDeliveryOrder: any = null;
    ticketShippingOrderQuotation: any = null;
    displayPlaceShippingOrderDialog: boolean = false;
    isManual: boolean;
    selectedLalaMoveServiceType: string = 'MOTORCYCLE';
    manualTime: Date = new Date();
    requestDelivery(event: Event, order: WheelOrderListDto) {
        this.selectedDeliveryOrder = order;
        this.wheelTicketShippingOrderServiceProxy.getWheelTicketShippingOrderQuotation(order.order.wheelTicketId).subscribe((result) => {
            if (result.shippingFee && result.shippingFeeCurrency) {
                this.ticketShippingOrderQuotation = result;
                this.displayPlaceShippingOrderDialog = true;
            }
        });
        event.preventDefault();
        event.stopPropagation();
    }

    onPlaceShippingOrderDialogHide(): void {
        this.selectedDeliveryOrder = null;
        this.ticketShippingOrderQuotation = null;
        this.displayPlaceShippingOrderDialog = false;
        this.isManual = false;
        this.selectedLalaMoveServiceType = 'MOTORCYCLE';
        this.manualTime = null;
    }

    getShippingProviderName(type) {
        switch (type) {
            case 2:
                return 'LalaMove';
            case 1:
                return 'GOGOX';
            case 3:
                return 'Dunzo';
            default:
                return '';
        }
    }


    refresh() {
        this.clear();
        this.getOrders();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.dateRange = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    apply() {
        this.lastCount = -1;
        this.getOrders();
        this.isShowFilterOptions = false;
    }

}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import {
    WheelScreenMenusServiceProxy,
    WheelScreenMenusListDto,
    WheelScreenMenuReOrderInput,
    UpdatePublishInput
} from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { CdkDrag, CdkDropList, CdkDropListGroup, moveItemInArray } from '@angular/cdk/drag-drop';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { AppConsts } from '@shared/AppConsts';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './wheel-screen-menu.component.html',
    styleUrls: ['./wheel-screen-menu.component.scss'],
    animations: [appModuleAnimation()]
})
export class WheelScreenMenuComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    screenMenus: WheelScreenMenusListDto[] = [];

    @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
    @ViewChild(CdkDropList) placeholder: CdkDropList;
    public target: CdkDropList;
    public targetIndex: number;
    public source: any;
    public sourceIndex: number;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _wheelScreenMenuService: WheelScreenMenusServiceProxy,
        private _localStorageService: LocalStorageService
    ) {
        super(injector);
        this.target = null;
        this.source = null;
    }

    ngOnInit(): void {
        this.getWheelScreenMenus();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelScreenMenus();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    ngAfterViewInit() {
        let phElement = this.placeholder.element.nativeElement;

        phElement.style.display = 'none';
        phElement.parentNode.removeChild(phElement);
    }

    getWheelScreenMenus(): void {
        this.spinnerService.show();
        this._wheelScreenMenuService
            .getAll(this.filterText)
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe((result) => {
                this.screenMenus = result.items;
            });
    }

    createOrEdit(id?: number) {
        this._router.navigate(['/app/wheel/screen-menus/create-or-edit', id ? id : 'null']);
    }

    delete(model: WheelScreenMenusListDto) {
        this.message.confirm(this.l('WheelScreennMenuDeleteWarningMessage', model.name), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._wheelScreenMenuService.deleteWheelScreenMenu(model.id).subscribe(() => {
                    this.getWheelScreenMenus();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    enterChildren(id?: number): void {
        this._localStorageService.setItem(AppConsts.ScreenMenuBreadcrums, [{ sectionId: -1, sectionName: 'Menus' }]);
        this._router.navigate(['/app/wheel/screen-menu-items', id ? id : '']);
    }

    updatePublish(id: number, published: boolean) {
        this._wheelScreenMenuService.updatePublish(new UpdatePublishInput({ id: id, publish: published, type: null })).subscribe(() => {
            this.notify.success(this.l('SavedSuccessfully'));
        });
    }

    drop() {
        if (!this.target) return;

        let phElement = this.placeholder.element.nativeElement;
        let parent = phElement.parentNode;

        phElement.style.display = 'none';

        parent.removeChild(phElement);
        parent.appendChild(phElement);
        parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);

        this.target = null;
        this.source = null;

        if (this.sourceIndex != this.targetIndex) moveItemInArray(this.screenMenus, this.sourceIndex, this.targetIndex);

        this._wheelScreenMenuService
            .reorder(this.screenMenus.map((x, i) => new WheelScreenMenuReOrderInput({ id: x.id, order: i, type: null })))
            .subscribe(() => {});
    }

    enter = (drag: CdkDrag, drop: CdkDropList) => {
        if (drop == this.placeholder) return true;

        let phElement = this.placeholder.element.nativeElement;
        let dropElement = drop.element.nativeElement;

        let dragIndex = __indexOf(dropElement.parentNode.children, drag.dropContainer.element.nativeElement);
        let dropIndex = __indexOf(dropElement.parentNode.children, dropElement);

        if (!this.source) {
            this.sourceIndex = dragIndex;
            this.source = drag.dropContainer;

            let sourceElement = this.source.element.nativeElement;
            phElement.style.width = sourceElement.clientWidth + 'px';
            phElement.style.height = sourceElement.clientHeight + 'px';

            sourceElement.parentNode.removeChild(sourceElement);
        }

        this.targetIndex = dropIndex;
        this.target = drop;

        phElement.style.display = '';
        dropElement.parentNode.insertBefore(phElement, dragIndex < dropIndex ? dropElement.nextSibling : dropElement);

        this.source.start();
        this.placeholder.enter(drag, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);

        return false;
    };

    search(event: string) {
        this.filter$.next(event);
    }
}

function __indexOf(collection, node) {
    return Array.prototype.indexOf.call(collection, node);
}

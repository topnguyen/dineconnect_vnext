import {
    Component,
    OnInit,
    Injector,
    AfterViewInit,
    ViewChild,
    NgZone,
    ElementRef,
    Input,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    CreateOrEditWheelScreenMenuItemInput,
    WheelScreenMenuItemForEditDto,
    WheelScreenMenuItemsServiceProxy,
    MenuItemServiceProxy,
    PagedResultDtoOfMenuItemAndPriceForSelection,
    MenuItemAndPriceForSelection,
    WheelScreenMenuItemlPricesForEditDto,
    PagedResultDtoOfWheelScreenMenuItemForSection,
    WheelScreenMenuItemForSection,
    WheelScreenMenuItemType,
    PagedResultDtoOfProductComboDto,
    ProductComboServiceProxy,
    ProductComboDto,
    TenantSettingsServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";
import Inputmask from "inputmask";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { Observable } from "rxjs";
import { DatePipe } from "@angular/common";
import { WhellScreenMenuTimingSettingComponent } from "./whell-screen-menu-timing-setting.component";
import {
    FormArray,
    FormBuilder,
    FormGroup,
    ReactiveFormsModule,
} from "@angular/forms";
import { AppConsts, MenuItemTagsConst } from "@shared/AppConsts";

@Component({
    templateUrl: "./create-or-edit-wheel-screen-menu-item.component.html",
    styleUrls: ["./create-or-edit-wheel-screen-menu-item.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelScreenItemSectionComponent
    extends AppComponentBase
    implements OnInit, AfterViewInit {
    @ViewChild("selectMenuComboPopupModal", { static: true })
    selectMenuComboPopupModal: SelectPopupComponent;
    @ViewChild("selectMenuItemPopupModal", { static: true })
    selectMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("selectWheelScreenMenuItemPopupModal", { static: true })
    selectWheelScreenMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("timingSettingForm", { static: true })
    timingSettingForm: WhellScreenMenuTimingSettingComponent;
    item: WheelScreenMenuItemForEditDto;
    screenMenuId: number;
    parentSectionId: number;
    itemId: number;
    saving = false;
    selectedPrice;
    currentComponent: any;
    selectRecommendItems: WheelScreenMenuItemForSection[];
    menuItemTags = MenuItemTagsConst;
    currentTypeMenu: any;
    comboName: string = "";
    comboId: number = -1;
    comboPrice = new WheelScreenMenuItemlPricesForEditDto();
    selectedTags: string[];
    isDisabledItem: boolean = false;
    isDisabledCombo: boolean = false;
    widthImage: number;
    heightImage: number;
    uploadUrl = AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImageForScreenMenuItem";

    selectedItemName: string;

    constructor(
        _injector: Injector,
        private _wheelScreenMenuItemService: WheelScreenMenuItemsServiceProxy,
        private _menuitemServiceProxy: MenuItemServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private ngZone: NgZone,
        public datepipe: DatePipe,
        private formBuilder: FormBuilder,
        private _productComboServiceProxy: ProductComboServiceProxy,
        private _tenantSettingsService: TenantSettingsServiceProxy,
    ) {
        super(_injector);
    }

    ngOnInit() {
        this.item = new WheelScreenMenuItemForEditDto();
        this.item.published = true;
        this.item.prices = this.item.prices || [];
        this.comboPrice.name = "NORMAL";
        this.comboPrice.multiplier = 0;
        this.comboPrice.price = 0;
        this._activatedRoute.params.subscribe((params) => {
            this.screenMenuId = +params["id"];
            this.currentTypeMenu = +params["type"];
            this.parentSectionId = +params["parentSectionId"];
            this.itemId = +params["itemId"];

            if (!isNaN(this.itemId)) {


                this._wheelScreenMenuItemService
                    .getWheelScreenMenuItemForEditSection(
                        this.currentTypeMenu,
                        this.itemId
                    )
                    .subscribe((result) => {
                        this.item = result;

                        //menu items
                        if (this.item.seletedMenuItemIdPrice) {
                            this.selectedPrice = {
                                id: this.item.seletedMenuItemIdPrice,
                                name: this.item.selectedMenuIdPriceName,
                            };

                            this.selectedItemName = this.selectedPrice.name;
                        }
                        //combo
                        else if (this.item.productComboName) {

                            this.selectedPrice = {
                                id: this.item.seletedMenuItemIdPrice,
                                name: this.item.selectedMenuIdPriceName,
                            };

                            this.comboName = this.item.productComboName;
                            this.comboId = this.item.productComboId;

                            if (this.item.prices.length > 0) {
                                this.comboPrice = this.item.prices[0];
                            }

                            this.selectedItemName = this.comboName;
                        }

                        if (this.item.tags) {
                            this.selectedTags = this.item.tags.map((x) => x);
                        } else {
                            this.selectedTags = [];
                        }
                        if (this.item.timingSettings) {
                            this.timingSettingForm.defaultTimingSetting(
                                this.item.timingSettings
                            );
                        }
                    });
            }
        });
        this._tenantSettingsService.getOrderSetting().subscribe(result => {
            this.widthImage = result.width;
            this.heightImage = result.height;
        });
    }

    ngAfterViewInit() {
        let priceSelector = document.getElementById("Price");
        if (priceSelector) {
            Inputmask("$ 0,00", {
                numericInput: true,
                placeholder: "0,00",
            }).mask(priceSelector);
        }
    }

    openSelectionPricePopup() {
        this.selectMenuItemPopupModal.show();
    }

    getMenuItemSelection() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfMenuItemAndPriceForSelection> =>
            this._menuitemServiceProxy.getMenuItemAndPriceForSelection(
                filterText,
                sorting,
                maxResultCount,
                skipCount
            );
    }

    getScreenMenuItemSection() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfWheelScreenMenuItemForSection> =>
            this._wheelScreenMenuItemService.getWheelScreenMenuItemForSelection(
                filterText,
                sorting,
                skipCount,
                maxResultCount
            );
    }

    addPrice() {
        let price = new WheelScreenMenuItemlPricesForEditDto();
        price.name = "";
        price.multiplier = 0;
        price.price = 0;
        this.ngZone.run(() => {
            setTimeout(() => {
                this.item.prices.push(price);
            }, 0);
        });
    }

    remove(item: WheelScreenMenuItemlPricesForEditDto) {
        this.item.prices.remove(item);
    }

    close(): void {
        this._router.navigate([
            "/app/wheel/screen-menu-items",
            this.screenMenuId,
        ]);
    }

    save() {
        this.saving = true;
        let timingSetting = this.timingSettingForm.getTimingSetting();
        switch (this.currentTypeMenu) {
            case WheelScreenMenuItemType.Item:
                if (!this.item.seletedMenuItemIdPrice || this.item.seletedMenuItemIdPrice <= 0) {
                    this.notify.error(this.l("MenuItem is requied."));
                    return;
                }
                const model = new CreateOrEditWheelScreenMenuItemInput({
                    id: this.item.id,
                    name: this.item.name,
                    subCategory: this.item.subCategory,
                    parentId: this.parentSectionId || null,
                    screenMenuId: this.screenMenuId,
                    description: this.item.description,
                    note: this.item.note,
                    image: this.item.image,
                    video: this.item.video,
                    displayAs: this.item.displayAs,
                    numberOfColumns: this.item.numberOfColumns,
                    hideGridTitles: this.item.hideGridTitles,
                    markAsNew: this.item.markAsNew,
                    markAsSignature: this.item.markAsSignature,
                    published: this.item.published,
                    displayNutritionFacts: this.item.displayNutritionFacts,
                    recommendedItems: this.item.recommendedItems,
                    prices: this.item.prices,
                    preperationTime: this.item.preperationTime,
                    ingredientWarnings: this.item.ingredientWarnings,
                    seletedMenuItemIdPrice: this.item.seletedMenuItemIdPrice,
                    type: WheelScreenMenuItemType.Item,
                    productComboId: null,
                    tags: this.selectedTags,
                    timingSettings: timingSetting,
                    allowPreOrder: this.item.allowPreOrder,
                });
                this._wheelScreenMenuItemService
                    .createOrEditWheelScreenMenuSection(model)
                    .pipe(finalize(() => (this.saving = false)))
                    .subscribe(() => {
                        this.notify.success(this.l("SavedSuccessfully"));
                        this.close();
                    });
                break;
            case WheelScreenMenuItemType.Combo:
                if (this.currentTypeMenu == WheelScreenMenuItemType.Combo) {
                    if (!this.comboId || this.comboId <= 0) {
                        this.notify.error(this.l("Combo is requied."));
                        return;
                    }
                }
                const modelCombo = new CreateOrEditWheelScreenMenuItemInput({
                    id: this.item.id,
                    name: this.item.name,
                    subCategory: this.item.subCategory,
                    parentId: this.parentSectionId || null,
                    screenMenuId: this.screenMenuId,
                    description: this.item.description,
                    note: this.item.note,
                    image: this.item.image,
                    video: this.item.video,
                    displayAs: this.item.displayAs,
                    numberOfColumns: this.item.numberOfColumns,
                    hideGridTitles: this.item.hideGridTitles,
                    markAsNew: this.item.markAsNew,
                    markAsSignature: this.item.markAsSignature,
                    published: this.item.published,
                    displayNutritionFacts: this.item.displayNutritionFacts,
                    recommendedItems: this.item.recommendedItems,
                    prices: [this.comboPrice],
                    preperationTime: this.item.preperationTime,
                    ingredientWarnings: this.item.ingredientWarnings,
                    seletedMenuItemIdPrice: null,
                    type: WheelScreenMenuItemType.Combo,
                    productComboId: this.comboId,
                    tags: this.selectedTags,
                    timingSettings: timingSetting,
                    allowPreOrder: this.item.allowPreOrder,
                });

                this._wheelScreenMenuItemService
                    .createOrEditWheelScreenMenuSection(modelCombo)
                    .pipe(finalize(() => (this.saving = false)))
                    .subscribe(() => {
                        this.notify.success(this.l("SavedSuccessfully"));
                        this.close();
                    });
                break;
        }

    }
    openSelectionComboPopup() {
        this.selectMenuComboPopupModal.show();
    }

    getMenuComboSelection() {
        return (
            filterText,
            isDeleted,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfProductComboDto> =>
            this._productComboServiceProxy.getAll(
                undefined,
                false,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    selectItem(itemSelect: MenuItemAndPriceForSelection) {
        this.selectedItemName = itemSelect.name;
        this.item.name = this.selectedItemName;
        if (itemSelect.productType == 1) {
            this.pricesAdd(itemSelect);
        }
        else {
            this.changeCombo(itemSelect)
        }
    }

    pricesAdd(itemSelect: MenuItemAndPriceForSelection) {
        this.removeCombo();
        this.currentTypeMenu = WheelScreenMenuItemType.Item;
        if (itemSelect) {
            this.item.prices = itemSelect.prices.map((x) => {
                const data = WheelScreenMenuItemlPricesForEditDto.fromJS(x);
                data.name = x.name;
                data.multiplier = x.multiPlier;
                data.price = x.price;
                return data;
            });

            this.selectedPrice = {
                id: itemSelect.id,
                name: itemSelect.name,
            };

            this.item.seletedMenuItemIdPrice = itemSelect.id;
            this.item.selectedMenuIdPriceName = itemSelect.name;
        }
    }


    changeCombo(itemSelect: MenuItemAndPriceForSelection) {
        this.clearItem();

        this.currentTypeMenu = WheelScreenMenuItemType.Combo;
        this.comboName = itemSelect.name;
        this.comboId = itemSelect.productComboId;
    }

    removeCombo() {
        this.comboName = "";
        this.comboId = -1;
    }

    clearItem() {
        this.item.prices = [];
        this.selectedPrice = null;
        this.item.seletedMenuItemIdPrice = null;
        this.item.selectedMenuIdPriceName = null;
    }
}

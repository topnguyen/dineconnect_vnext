import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { finalize } from "rxjs/operators";
import * as _ from 'lodash';
import {
    WheelScreenMenusServiceProxy,
    WheelScreenMenuItemsServiceProxy,
    WheelScreenMenuItemChildrenDto,
    WheelScreenMenuItemType,
    CreateOrEditWheelScreenMenuInput,
    WheelScreenMenuReOrderInput,
    UpdatePublishInput,
    WheelScreenMenuForEditDto,
    PagedResultDtoOfMenuItemAndPriceForSelection,
    MenuItemServiceProxy,
    ScreenMenuServiceProxy,
    PagedResultDtoOfGetAllScreenMenuDto,
    ThemeLayoutSettingsDto,
} from "@shared/service-proxies/service-proxies";
import { Router, ActivatedRoute } from "@angular/router";
import { forkJoin, Observable } from "rxjs";
import { AppConsts } from "@shared/AppConsts";
import { CdkDrag, CdkDragDrop, CdkDropList, CdkDropListGroup, moveItemInArray } from "@angular/cdk/drag-drop";
import { LocalStorageService } from "@shared/utils/local-storage.service";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";

@Component({
    templateUrl: "./wheel-screen-menu-item.component.html",
    styleUrls: ["./wheel-screen-menu-item.component.scss"],
    animations: [appModuleAnimation()],
})
export class WheelScreenMenuItemComponent
    extends AppComponentBase
    implements OnInit {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("selectScreenMenuCategoryPopupModal", { static: true })
    selectScreenMenuCategoryPopupModal: SelectPopupComponent;
    filter = "";
    screenMenuItems: WheelScreenMenuItemChildrenDto[] = [];
    item: WheelScreenMenuForEditDto = {} as WheelScreenMenuForEditDto;
    screenMenuId: number;
    listSection: { sectionId: number; sectionName: string }[] = [];
    currentSectionId = -1;

    typeItem = WheelScreenMenuItemType.Item;
    typeCombo = WheelScreenMenuItemType.Combo;
    typeSection = WheelScreenMenuItemType.Section;
    typeScreenMenu = null;
    currency = AppConsts.currency;
    selectedSreenMenuCategory;

    isCategory: boolean = true;

    @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
    @ViewChild(CdkDropList) placeholder: CdkDropList;
    public target: CdkDropList;
    public targetIndex: number;
    public source: any;
    public sourceIndex: number;
    

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _wheelScreenMenusService: WheelScreenMenusServiceProxy,
        private _wheelScreenMenuItemsService: WheelScreenMenuItemsServiceProxy,
        private _localStorageService: LocalStorageService,
        private _menuitemServiceProxy: MenuItemServiceProxy,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
    ) {
        super(injector);
        this.target = null;
        this.source = null;
    }

    ngOnInit(): void {
        this._localStorageService.getItem(
            AppConsts.ScreenMenuBreadcrums,
            (err, value) => {
                this.listSection = value;

                this._activatedRoute.params.subscribe((params) => {
                    this.screenMenuId = +params["id"]; // (+) converts string 'id' to a number

                    if (!isNaN(this.screenMenuId)) {
                        // this.listSection.push({ sectionId: -1, sectionName: "Menus" });

                        if (this.listSection.length > 2) {
                            this.isCategory = false;
                            this.getWheelScreenMenuItemFromSection(
                                this.listSection[this.listSection.length - 1]
                                    .sectionId
                            );
                        } else {
                            this.isCategory = true;
                            this.listSection.remove(this.listSection[1]);
                            this.getWheelScreenMenuItem();
                        }
                    } else {
                    }
                });
            }
        );
    }

    ngAfterViewInit() {
        let phElement = this.placeholder.element.nativeElement;

        phElement.style.display = 'none';
        phElement.parentNode.removeChild(phElement);
    }

    search() {
        if (this.currentSectionId === -1) {
            this.getWheelScreenMenuItem(true);
        } else {
            this.getWheelScreenMenuItemFromSection(this.currentSectionId);
        }
    }

    getWheelScreenMenuItem(isSearch: boolean = false): void {
        this.spinnerService.show();
        forkJoin([
            this._wheelScreenMenusService.getWheelScreenMenuForEdit(
                this.screenMenuId
            ),
            this._wheelScreenMenusService.getChildren(
                this.filter,
                false,
                this.screenMenuId
            ),
        ])
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe(([item, children]) => {
                this.isCategory = true;
                this.item = item;
                this.screenMenuItems = children;

                if (!isSearch) {
                    this.listSection.push({
                        sectionId: this.screenMenuId,
                        sectionName: this.item.name,
                    });
                }
            });
    }

    enterSection(sectionId: number, name: string) {
        this.listSection.push({
            sectionId: sectionId,
            sectionName: name,
        });

        this.storeBreadCrum();
        this.getWheelScreenMenuItemFromSection(sectionId);
    }

    getWheelScreenMenuItemFromSection(sectionId: number): void {
        this.currentSectionId = sectionId;
        this.spinnerService.show();
        forkJoin([
            this._wheelScreenMenuItemsService.getWheelScreenMenuItemForEditSection(
                WheelScreenMenuItemType.Section,
                sectionId
            ),
            this._wheelScreenMenuItemsService.getChildren(
                this.filter,
                false,
                sectionId
            ),
        ])
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe(([item, children]) => {
                this.isCategory = false;
                this.item = item;
                this.screenMenuItems = children;
            });
    }

    gotoSection(index: number): void {
        if (index === this.listSection.length - 1) { return; }
        switch (index) {
            case 0:
                this._router.navigate(["/app/wheel/screen-menus"]);
                break;
            case 1:
                this.listSection = this.listSection.slice(0, 1);
                this.currentSectionId = -1;
                this.getWheelScreenMenuItem();
                break;
            default:
                this.listSection = this.listSection.slice(0, index + 1);
                this.getWheelScreenMenuItemFromSection(
                    this.listSection[index].sectionId
                );
                break;
        }

        this.storeBreadCrum();
    }

    createOrEditSection(id?: number) {
        // tslint:disable-next-line:triple-equals
        if (this.currentSectionId != -1) {
            this._router.navigate([
                "/app/wheel/screen-menu-items",
                this.screenMenuId,
                "section",
                this.currentSectionId,
                "create-or-edit",
                id ? id : "null",
            ]);
        } else {
            this._router.navigate([
                "/app/wheel/screen-menu-items",
                this.screenMenuId,
                "create-or-edit",
                id ? id : "null",
            ]);
        }

        this.storeBreadCrum();
    }

    editSection(id?: number) {
        this._router.navigate([
            "/app/wheel/screen-menu-items",
            this.screenMenuId,
            "section",
            this.listSection[this.listSection.length - 2].sectionId,
            "create-or-edit",
            id ? id : "null",
        ]);

        this.storeBreadCrum();
    }

    createOrEditItem(id?: number, type?: number) {
        // tslint:disable-next-line:triple-equals
        if (this.currentSectionId != -1) {
            this._router.navigate([
                "/app/wheel/screen-menu-items",
                this.screenMenuId,
                "section",
                this.currentSectionId,
                "create-or-edit-item",
                id ? id : "null",
                type? type: WheelScreenMenuItemType.Item
            ]);
        } else {
            this._router.navigate([
                "/app/wheel/screen-menu-items",
                this.screenMenuId,
                "create-or-edit-item",
                id ? id : "null",
                type? type: WheelScreenMenuItemType.Item
            ]);
        }

        this.storeBreadCrum();
    }

    save(formValid: boolean) {
        if (formValid) {
            this.spinnerService.show();

            if (this.currentSectionId === -1) {
                const model = new CreateOrEditWheelScreenMenuInput({
                    id: this.item.id,
                    name: this.item.name,
                    description: this.item.description,
                    published: this.item.published,
                });

                this._wheelScreenMenusService
                    .createOrEditWheelScreenMenu(model)
                    .pipe(finalize(() => this.spinnerService.hide()))
                    .subscribe(() => {
                        this.notify.success(this.l("SavedSuccessfully"));
                    });
            } else {
                const model: any = this.item;

                this._wheelScreenMenuItemsService
                    .createOrEditWheelScreenMenuSection(model)
                    .pipe(finalize(() => this.spinnerService.hide()))
                    .subscribe(() => {
                        this.notify.success(this.l("SavedSuccessfully"));
                    });
            }
        }
    }

    updatePublish(id: number, published: boolean, type: WheelScreenMenuItemType) {
        this._wheelScreenMenuItemsService
            .updatePublish(
                new UpdatePublishInput({ id: id, publish: published, type: type })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
            });
    }

    delete(model: WheelScreenMenuItemChildrenDto) {
        this.message.confirm(
            this.l("WheelScreennMenuDeleteWarningMessage", model.name),
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wheelScreenMenuItemsService
                        .deleteWheelScreenMenuSection(model.type, model.id)
                        .subscribe(() => {
                            this.search();
                            this.notify.success(this.l("SuccessfullyDeleted"));
                        });
                }
            }
        );
    }

    // drop(event: CdkDragDrop<string[]>) {
    //     moveItemInArray(
    //         this.screenMenuItems,
    //         event.previousIndex,
    //         event.currentIndex
    //     );

    //     this._wheelScreenMenuItemsService
    //         .reorder(
    //             this.screenMenuItems.map(
    //                 (x, i) =>
    //                     new WheelScreenMenuReOrderInput({ id: x.id, order: i, type: x.type })
    //             )
    //         )
    //         .subscribe(() => { });
    // }
    drop() {
        if (!this.target) return;

        let phElement = this.placeholder.element.nativeElement;
        let parent = phElement.parentNode;

        phElement.style.display = 'none';

        parent.removeChild(phElement);
        parent.appendChild(phElement);
        parent.insertBefore(
            this.source.element.nativeElement,
            parent.children[this.sourceIndex]
        );

        this.target = null;
        this.source = null;

        if (this.sourceIndex != this.targetIndex)
            moveItemInArray(this.screenMenuItems, this.sourceIndex, this.targetIndex);

            this._wheelScreenMenuItemsService
                    .reorder(
                        this.screenMenuItems.map(
                            (x, i) =>
                                new WheelScreenMenuReOrderInput({ id: x.id, order: i, type: x.type })
                        )
                    )
                    .subscribe(() => { });
    }


    storeBreadCrum() {
        this._localStorageService.setItem(
            AppConsts.ScreenMenuBreadcrums,
            this.listSection
        );
    }
    openSelectionMenuSreenCategoryPopup() {
        this.selectScreenMenuCategoryPopupModal.show();
    }

    importScreenMenuItems(itemSelect) {
        console.log('item select',itemSelect);
        this._wheelScreenMenuItemsService.createWheelScreenMenuSectionFromScreenMenuCategory(this.screenMenuId, itemSelect.id)
        .subscribe(result => {
            this.search();
            this.notify.success(this.l("SavedSuccessfully"));
        });
    }
    getMenuItemSelection() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfGetAllScreenMenuDto> =>
            this._screenMenuServiceProxy.getAllScreenMenuCategory(
                filterText,
                sorting,
                maxResultCount,
               skipCount
            );
    }

    getScreenMenuForImport() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfGetAllScreenMenuDto> =>
            this._screenMenuServiceProxy.getAllScreenMenuForImport(
                filterText,
                sorting,
                maxResultCount,
               skipCount
            );
    }
    enter = (drag: CdkDrag, drop: CdkDropList) => {
        if (drop == this.placeholder) return true;

        let phElement = this.placeholder.element.nativeElement;
        let dropElement = drop.element.nativeElement;

        let dragIndex = __indexOf(
            dropElement.parentNode.children,
            drag.dropContainer.element.nativeElement
        );
        let dropIndex = __indexOf(dropElement.parentNode.children, dropElement);

        if (!this.source) {
            this.sourceIndex = dragIndex;
            this.source = drag.dropContainer;

            let sourceElement = this.source.element.nativeElement;
            phElement.style.width = sourceElement.clientWidth + 'px';
            phElement.style.height = sourceElement.clientHeight + 'px';

            sourceElement.parentNode.removeChild(sourceElement);
        }

        this.targetIndex = dropIndex;
        this.target = drop;

        phElement.style.display = '';
        dropElement.parentNode.insertBefore(
            phElement,
            dragIndex < dropIndex ? dropElement.nextSibling : dropElement
        );

        this.source.start();
        this.placeholder.enter(
            drag,
            drag.element.nativeElement.offsetLeft,
            drag.element.nativeElement.offsetTop
        );

        return false;
    };
}
function __indexOf(collection, node) {
    return Array.prototype.indexOf.call(collection, node);
}

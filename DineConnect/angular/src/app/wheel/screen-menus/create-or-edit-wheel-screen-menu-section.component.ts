import { Component, OnInit, Injector, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    CreateOrEditWheelScreenMenuItemInput,
    TenantSettingsServiceProxy,
    WheelDisplayAs,
    WheelScreenMenuItemForEditDto,
    WheelScreenMenuItemsServiceProxy,
    WheelScreenMenuItemType,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";
import { isNumber } from "util";
import { WhellScreenMenuTimingSettingComponent } from "./whell-screen-menu-timing-setting.component";
import { AppConsts } from "@shared/AppConsts";
@Component({
    templateUrl: "./create-or-edit-wheel-screen-menu-section.component.html",
    styleUrls: ["./create-or-edit-wheel-screen-menu-section.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelScreenMenuSectionComponent
    extends AppComponentBase
    implements OnInit
{
    section: WheelScreenMenuItemForEditDto;
    screenMenuId: number;
    parentSectionId: number;
    sectionId: number;
    saving = false;
    displayAsOption: any[];
    @ViewChild("timingSettingForm", { static: true })
    timingSettingForm: WhellScreenMenuTimingSettingComponent;
    numberOfColumnOption = [
        {
            value: 1,
            displayText: "1",
        },
        {
            value: 2,
            displayText: "2",
        },
        {
            value: 3,
            displayText: "3",
        },
        {
            value: 4,
            displayText: "4",
        },
    ];
    widthImage: number;
    heightImage: number;
    uploadUrl = AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImageForScreenMenuItem";

    constructor(
        _injector: Injector,
        private _wheelScreenMenuItemService: WheelScreenMenuItemsServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _tenantSettingsService: TenantSettingsServiceProxy,
    ) {
        super(_injector);

        this.displayAsOption = Object.keys(WheelDisplayAs)
            .filter((key) => isNumber(WheelDisplayAs[key]))
            .map((key) => ({
                value: WheelDisplayAs[key],
                displayText: key,
            }));
    }

    ngOnInit() {
        this.section = new WheelScreenMenuItemForEditDto();
        this.section.published = true;
        
        this._activatedRoute.params.subscribe((params) => {
            this.screenMenuId = +params["id"];
            this.parentSectionId = +params["parentSectionId"];
            this.sectionId = +params["sectionId"];
            if (!isNaN(this.sectionId)) {
                this._wheelScreenMenuItemService
                    .getWheelScreenMenuItemForEditSection(
                        WheelScreenMenuItemType.Section,
                        this.sectionId
                    )
                    .subscribe((result) => {
                        this.section = result;
                        if (this.section.timingSettings) {
                            this.timingSettingForm.defaultTimingSetting(
                                this.section.timingSettings
                            );
                        }
                    });
            } else {
                this.section.displayAs = this.displayAsOption[0].value;
                this.section.numberOfColumns =
                    this.numberOfColumnOption[0].value;
            }
        });
        this._tenantSettingsService.getOrderSetting().subscribe(result => {
            this.widthImage = result.width;
            this.heightImage = result.height;
        });
    }

    close(): void {
        this._router.navigate([
            "/app/wheel/screen-menu-items",
            this.screenMenuId,
        ]);
    }

    save() {
        this.saving = true;
        let timingSetting = this.timingSettingForm.getTimingSetting();
        const model = new CreateOrEditWheelScreenMenuItemInput({
            id: this.section.id,
            name: this.section.name,
            subCategory: '',
            parentId: this.parentSectionId || null,
            screenMenuId: this.screenMenuId,
            description: this.section.description,
            note: this.section.note,
            image: this.section.image,
            displayAs: this.section.displayAs,
            numberOfColumns: this.section.numberOfColumns,
            hideGridTitles: this.section.hideGridTitles,
            markAsNew: this.section.markAsNew,
            markAsSignature: this.section.markAsSignature,
            published: this.section.published,
            video: null,
            preperationTime: 0,
            ingredientWarnings: null,
            displayNutritionFacts: false,
            prices: null,
            recommendedItems: null,
            seletedMenuItemIdPrice: null,
            type: WheelScreenMenuItemType.Section,
            productComboId: null,
            tags: null,
            timingSettings: timingSetting,
            allowPreOrder: null,
        });
        this._wheelScreenMenuItemService
            .createOrEditWheelScreenMenuSection(model)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }
}

import { Component, OnInit, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    WheelScreenMenusServiceProxy,
    WheelScreenMenuForEditDto,
    CreateOrEditWheelScreenMenuInput,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";

@Component({
    templateUrl: "./create-or-edit-wheel-screen-menu.component.html",
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelScreenMenuComponent extends AppComponentBase implements OnInit {
    screenMenu: WheelScreenMenuForEditDto;
    screenMenuId: number;
    saving = false;

    constructor(
        _injector: Injector,
        private _wheelScreenMenuService: WheelScreenMenusServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(_injector);
    }

    ngOnInit() {
        this.screenMenu = new WheelScreenMenuForEditDto();

        this._activatedRoute.params.subscribe((params) => {
            this.screenMenuId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.screenMenuId)) {
                this._wheelScreenMenuService
                    .getWheelScreenMenuForEdit(this.screenMenuId)
                    .subscribe((result) => {
                        this.screenMenu = result;
                    });
            }
        });
    }

    close(): void {
        this._router.navigate(["/app/wheel/screen-menus"]);
    }

    save() {
        this.saving = true;
        const model = new CreateOrEditWheelScreenMenuInput({
            id: this.screenMenu.id,
            name: this.screenMenu.name,
            description: this.screenMenu.description,
            published: this.screenMenu.published,
        });

        this._wheelScreenMenuService
            .createOrEditWheelScreenMenu(model)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }
}

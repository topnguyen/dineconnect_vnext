import { DatePipe } from "@angular/common";
import { Component, Injector, OnInit } from "@angular/core";
import {
    AbstractControl,
    FormArray,
    FormBuilder,
    FormGroup,
} from "@angular/forms";
@Component({
    selector: "app-whell-screen-menu-timing-setting",
    templateUrl: "./whell-screen-menu-timing-setting.component.html",
    styleUrls: ["./whell-screen-menu-timing-setting.component.css"],
})
export class WhellScreenMenuTimingSettingComponent implements OnInit {
    rangeValues: number[] = [480, 720];
    rdoTimeSelected: any;
    radioItems: any[] = [
        { value: "HideUntil", text: "HIDE UNTIL" },
        { value: "ShowOnlyFrom", text: "SHOW ONLY FROM" },
    ];
    dayOfWeekList: any[] = [
        { value: "Mon", text: "MONDAY" },
        { value: "Tue", text: "TUESDAY" },
        { value: "Wed", text: "WEDNESDAY" },
        { value: "Thu", text: "THURSDAY" },
        { value: "Fri", text: "FRIDAY" },
        { value: "Sat", text: "SATURDAY" },
        { value: "Sun", text: "SUNDAY" },
    ];

    selectDayOfWeek: any[];
    futureDate = new Date();
    time = new Date();
    hiddenOnlyFrom = false;
    hiddenHideUntil = false;
    orderForm: FormGroup;
    items: FormArray;
    timeRangeShow: any[] = [];
    timeRangeValue: number[] = [480, 720];
    dayOfWeekSelecteds: any[] = [];

    constructor(
        _injector: Injector,
        public datepipe: DatePipe,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(): void {
        this.orderForm = this.formBuilder.group({
            rdTime: "",
            date: new Date(),
            time: new Date(),
            items: this.formBuilder.array([this.createItem()]),
        });
        this.timeRangeShow.push({
            minTime: this.convertSecondsToHour(this.timeRangeValue[0]),
            maxTime: this.convertSecondsToHour(this.timeRangeValue[1]),
        });
    }

    convertSecondsToHour(seconds) {
        let hours = Math.floor(seconds / 60);
        let minutes = seconds % 60;
        let hourString = hours < 10 ? `0${hours}` : `${hours}`;
        let minuteString = minutes < 10 ? `0${minutes}` : `${minutes}`;
        return `${hourString}:${minuteString}`;
    }

    handleChange(e, i) {
        this.timeRangeShow[i].minTime = this.convertSecondsToHour(e.values[0]);
        this.timeRangeShow[i].maxTime = this.convertSecondsToHour(e.values[1]);
    }
    selectOptionTimeSetting(value, i) {
        switch (value) {
            case "HideUntil":
                this.hiddenHideUntil = true;
                this.hiddenOnlyFrom = false;
                break;
            case "ShowOnlyFrom":
                this.hiddenHideUntil = false;
                this.hiddenOnlyFrom = true;
                break;
        }
    }
    getTimingSetting() {
        let objectTiming: any;
        objectTiming = {
            hiddenOnlyFrom: this.hiddenOnlyFrom,
            hiddenHideUntil: this.hiddenHideUntil,
            days: [],
            rangeTime: [],
            date: this.hiddenHideUntil
                ? this.datepipe.transform(this.getDate, "yyy-MM-dd")
                : null,
            time: this.hiddenHideUntil
                ? this.datepipe.transform(this.getTime, "HH:mm")
                : null,
        };
        if (this.hiddenOnlyFrom) {
            let days = [];
            let rangeTime = [];
            for (let i = 0; i < this.itemsForm.length; i++) {
                days.push(
                    this.dayOfWeekSelecteds
                        .filter((x) => x.index == i)[0]
                        .listDays.join(",")
                );
                rangeTime.push(this.itemsForm[i].controls.sliderTime.value);
            }
            objectTiming.days = days;
            objectTiming.rangeTime = rangeTime;
        }

        return [JSON.stringify(objectTiming)];
    }
    defaultTimingSetting(object) {
        this.items = this.orderForm.get("items") as FormArray;

        let objectItem = JSON.parse(object[0]);
        if (object.length > 0 && objectItem.hasOwnProperty("timefrom")) return;
        this.hiddenHideUntil = objectItem.hiddenHideUntil;
        this.hiddenOnlyFrom = objectItem.hiddenOnlyFrom;
        if (this.hiddenHideUntil) {
            this.initFormByRdHideUntil(objectItem);
        }
        if (this.hiddenOnlyFrom) {
            this.initFormByRdShowOnlyForm(objectItem);
        }
    }
    initFormByRdHideUntil(objectItem) {
        this.orderForm.controls["rdTime"].setValue("HideUntil");
        this.orderForm.controls["date"].setValue(new Date(objectItem.date));
        this.orderForm.controls["time"].setValue(
            new Date(`${objectItem.date} ${objectItem.time}`)
        );
    }

    initFormByRdShowOnlyForm(objectItem) {
        this.orderForm.controls["rdTime"].setValue("ShowOnlyFrom");
        this.removeTitemTiming(0, true);
        let dayArrs = objectItem.days;
        let rangtimeArrs = objectItem.rangeTime;
        for (let i = 0; i < rangtimeArrs.length; i++) {
            this.dayOfWeekSelecteds.push({
                index: i,
                listDays: dayArrs[i] ? dayArrs[i].split(",") : [],
            });
            let itemForm = { days: dayArrs[i], rangTime: rangtimeArrs[i] };
            this.addItem(itemForm);
        }
    }
    addItem(objectItem?) {
        this.items = this.orderForm.get("items") as FormArray;
        this.items.push(this.createItem(objectItem));
        if (objectItem) {
            this.timeRangeShow.push({
                minTime: this.convertSecondsToHour(objectItem.rangTime[0]),
                maxTime: this.convertSecondsToHour(objectItem.rangTime[1]),
            });
        } else {
            this.timeRangeShow.push({
                minTime: this.convertSecondsToHour(this.timeRangeValue[0]),
                maxTime: this.convertSecondsToHour(this.timeRangeValue[1]),
            });
        }
    }
    createItem(objectItem?): FormGroup {
        if (objectItem) {
            return this.formBuilder.group({
                dayOfweek: objectItem.days,
                sliderTime: [objectItem.rangTime],
            });
        }
        return this.formBuilder.group({
            dayOfweek: "",
            sliderTime: [this.timeRangeValue],
        });
    }
    removeTitemTiming(index, isResetForm) {
        if (index == 0 && !isResetForm) return;
        this.items.removeAt(index);
        // this.showList.splice(index, 1);
        this.timeRangeShow.splice(index, 1);
    }

    get itemsForm(): FormArray {
        return this.orderForm.get("items")["controls"] as FormArray;
    }

    getValueDayOfWeek(isChecked, value, index) {
        if (isChecked.checked == true) {
            this.pushItemDay(index, value);
        } else {
            this.deleteItemDay(index, value);
        }
    }

    pushItemDay(index, value) {
        if (
            this.dayOfWeekSelecteds.length === 0 ||
            this.dayOfWeekSelecteds.filter((x) => x.index === index).length ===
                0
        ) {
            this.dayOfWeekSelecteds.push({
                index: index,
                listDays: [value],
            });
        } else {
            let object = this.dayOfWeekSelecteds.filter(
                (x) => x.index == index
            );
            object[0].listDays.push(value);
            let days = object[0].listDays;
            this.dayOfWeekSelecteds.filter(
                (x) => x.index == index
            )[0].listDays = days;
        }
        console.log(this.dayOfWeekSelecteds);
    }

    deleteItemDay(index, value) {
        let object = this.dayOfWeekSelecteds.filter((x) => x.index == index);
        object[0].listDays = object[0].listDays.filter((x) => x !== value);
        if (object[0].listDays.length === 0) {
            this.dayOfWeekSelecteds = this.dayOfWeekSelecteds.filter(
                (x) => x.index != index
            );
        } else {
            this.dayOfWeekSelecteds[index].listDays = object[0].listDays;
        }
    }

    get getDate(): AbstractControl {
        return this.orderForm.controls["date"].value;
    }
    get getTime(): AbstractControl {
        return this.orderForm.controls["time"].value;
    }
}

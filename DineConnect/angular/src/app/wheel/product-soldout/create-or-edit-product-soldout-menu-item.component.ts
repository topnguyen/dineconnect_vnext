import {
    Component,
    OnInit,
    Injector,
    AfterViewInit,
    ViewChild,
    NgZone,
    ElementRef,
    Input,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    MenuItemServiceProxy,
    PagedResultDtoOfMenuItemAndPriceForSelection,
    MenuItemAndPriceForSelection,
    WheelScreenMenuItemlPricesForEditDto,
    WheelScreenMenuItemForSection,
    PagedResultDtoOfLocationDto,
    LocationServiceProxy,
} from "@shared/service-proxies/service-proxies";
import {
    WheelProductSoldOutServiceProxy,
    WheelProductSoldOutListDto
} from "@shared/service-proxies/service-proxies-nswag";

import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";
import Inputmask from "inputmask";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { Observable } from "rxjs";
import { DatePipe } from "@angular/common";
import {
    FormArray,
    FormBuilder,
    FormGroup,
    ReactiveFormsModule,
} from "@angular/forms";
import { AppConsts, MenuItemTagsConst } from "@shared/AppConsts";

@Component({
    templateUrl: "./create-or-edit-product-soldout-menu-item.component.html",
    styleUrls: ["./create-or-edit-product-soldout-menu-item.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditProductSoldoutItemSectionComponent
    extends AppComponentBase
    implements OnInit, AfterViewInit
{
    @ViewChild("selectMenuComboPopupModal", { static: true })
    selectMenuComboPopupModal: SelectPopupComponent;
    @ViewChild("selectMenuItemPopupModal", { static: true })
    selectMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("selectWheelScreenMenuItemPopupModal", { static: true })
    selectWheelScreenMenuItemPopupModal: SelectPopupComponent;
    item: WheelProductSoldOutListDto;
    screenMenuId: number;
    parentSectionId: number;
    itemId: number;
    saving = false;
    selectedPrice;
    currentComponent: any;
    selectRecommendItems: WheelScreenMenuItemForSection[];
    menuItemTags = MenuItemTagsConst;
    comboName: string = "";
    comboId: number = -1;
    comboPrice = new WheelScreenMenuItemlPricesForEditDto();
    selectedTags: string[];
    isDisabledItem: boolean = false;
    isDisabledCombo: boolean = false;
    selectedItemName: string;
    locationName = "";

    constructor(
        _injector: Injector,
        private _service: WheelProductSoldOutServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _menuitemServiceProxy: MenuItemServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        public datepipe: DatePipe
    ) {
        super(_injector);
    }

    ngOnInit() {
        this.item = new WheelProductSoldOutListDto();
        this.item.disable = false;
        this._activatedRoute.params.subscribe((params) => {
            this.itemId = +params["itemId"];
            if (!isNaN(this.itemId)) {
                this._service
                    .getWheelProductSoldOutForEdit(null, null, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, this.itemId)
                    .subscribe((result) => {
                        this.item = result;
                        this.item.id = this.itemId;
                        //menu items
                        if (this.item.menuItemId) {
                            this.selectedPrice = {
                                id: this.item.menuItemId,
                                name: this.item.name,
                            };

                            this.selectedItemName = this.selectedPrice.name;
                        }
                        //location
                        if (this.item.locationId) {
                            this.locationName = this.item.locationName;
                        }
                    });
            }
        });
    }

    close(): void {
        this._router.navigate(["/app/wheel/product-soldout"]);
    }

    ngAfterViewInit() {
        let priceSelector = document.getElementById("Price");
        if (priceSelector) {
            Inputmask("$ 0,00", {
                numericInput: true,
                placeholder: "0,00",
            }).mask(priceSelector);
        }
    }

    openSelectionPricePopup() {
        this.selectMenuItemPopupModal.show();
    }

    getMenuItemSelection() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfMenuItemAndPriceForSelection> =>
            this._menuitemServiceProxy.getMenuItemAndPriceForSelection(
                filterText,
                sorting,
                maxResultCount,
                skipCount
            );
    }

    save() {
        if (!this.selectedItemName) {
            this.notify.error(this.l("MenuItem is requied."));
            return;
        }
        if (!this.locationName) {
            this.notify.error(this.l("Location is requied."));
            return;
        }
        this.saving = true;
        const model = new WheelProductSoldOutListDto({
            id: this.item.id,
            name: undefined,
            disable: this.item.disable,
            menuItemId: this.selectedPrice.id,
            locationId: this.item.locationId,
            menuItemPortionId: undefined,
            lastModificationTime: undefined,
            lastModifierUserId: undefined,
            creationTime: undefined,
            creatorUserId: undefined,
            locationName: undefined,
        });
        this._service
            .createOrEditWheelProductSoldOut(model)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    selectItem(itemSelect: MenuItemAndPriceForSelection) {
        this.selectedItemName = itemSelect.name;

        if (itemSelect) {
            this.selectedPrice = {
                id: itemSelect.id,
                name: itemSelect.name,
            };
        }
    }

    getLocationForCombobox() {
        return (
            filterString,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(
                filterString,
                false,
                0,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    changeLocation(data) {
        this.locationName = data.name;
        this.item.locationId = data.id;
    }
}

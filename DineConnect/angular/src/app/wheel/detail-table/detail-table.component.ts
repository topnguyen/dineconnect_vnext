import { SeatmapService } from './../seatmap/seatmap.service';
import { Component, OnInit, Input } from '@angular/core';
import { Table } from '@app/shared/models/table.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-detail-table',
  templateUrl: './detail-table.component.html',
  styleUrls: ['./detail-table.component.css']
})
export class DetailTableComponent implements OnInit {

  @Input() position: Table = new Table();

  constructor(public activeModal: NgbActiveModal, private setMapService: SeatmapService) { }

  ngOnInit(): void {
  }
  deletePosition() {
    this.activeModal.close({ position: this.position, action: "delete" });
  }
  savePosition() {
    this.setMapService.saveTable(this.position).subscribe(rs=>{
      this.activeModal.dismiss();
    })
  }
}

import { NgModule } from "@angular/core";
import { ServiceFeesComponent } from "./basic-settings/service-fees/service-fees.component";
import { WheelRoutingModule } from "./wheel-routing.module";
import { DeliveryZonesComponent } from "./basic-settings/delivery-zones/delivery-zones.component";
import { OpeningHoursComponent } from "./basic-settings/opening-hours/opening-hours.component";
import { CommonModule, DatePipe } from "@angular/common";
import { FormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AgmCoreModule } from "@agm/core";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { ColorPickerModule } from "ngx-color-picker";
import { PopoverModule } from "ngx-bootstrap/popover";
import { NgxCheckboxModule } from "ngx-checkbox";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { UiSwitchModule } from "ngx-ui-switch";
import { NgSelectModule } from "@ng-select/ng-select";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TemplatesComponent } from "./dynamic-pages/templates.component";
import { CreateOrEditWheelDeliveryDurationModalComponent } from "./basic-settings/delivery-durations/create-or-edit-wheelDeliveryDuration-modal.component";
import { ViewWheelDeliveryDurationModalComponent } from "./basic-settings/delivery-durations/view-wheelDeliveryDuration-modal.component";
import { WheelDeliveryDurationsComponent } from "./basic-settings/delivery-durations/wheelDeliveryDurations.component";
// import { WheelPaymentMethodsComponent } from "./basic-settings/payment-methods/wheelPaymentMethods.component";
import { WheelTaxesComponent } from "./basic-settings/taxes/wheelTaxes.component";
import { ViewWheelTaxModalComponent } from "./basic-settings/taxes/view-wheelTax-modal.component";
import { CreateOrEditWheelTaxModalComponent } from "./basic-settings/taxes/create-or-edit-wheelTax-modal.component";
import { WheelLocationsComponent } from "./locations/wheelLocations.component";
import { WheelDepartmentsComponent } from "./departments/wheelDepartments.component";
import { CreateOrEditWheelDepartmentModalComponent } from "./departments/create-or-edit-wheelDepartment-modal.component";
import { WheelSetupsComponent } from "./wheel-setup/wheelSetups.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { PaginatorModule } from "primeng/paginator";
import { TableModule } from "primeng/table";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { UtilsModule } from "@shared/utils/utils.module";
import { TabsModule } from "ngx-bootstrap/tabs";
import { CreateOrEditTemplateComponent } from "./dynamic-pages/create-or-edit-template.component";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import { AppCommonModule } from "@app/shared/common/app-common.module";
// import { EditWheelPaymentMethodComponent } from "./basic-settings/payment-methods/edit-wheel-payment-method.component";
import { WheelScreenMenuComponent } from "./screen-menus/wheel-screen-menu.component";
import { CreateOrEditWheelScreenMenuComponent } from "./screen-menus/create-or-edit-wheel-screen-menu.component";
import { CreateOrEditWheelScreenMenuSectionComponent } from "./screen-menus/create-or-edit-wheel-screen-menu-section.component";
import { WheelScreenMenuItemComponent } from "./screen-menus/wheel-screen-menu-item.component";
import { CreateOrEditWheelScreenItemSectionComponent } from "./screen-menus/create-or-edit-wheel-screen-menu-item.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { CreateOrEditWheelLocationComponent } from "./locations/create-or-edit-wheelLocation.component";
import { CreateOrEditOpeningHoursComponent } from "./basic-settings/opening-hours/create-or-edit-opening-hours.component";
import { FileUploadModule } from "ng2-file-upload";
import { WheelUiCustomizationComponent } from "./ui-themes/wheel-ui-customization.component";
import { WheelTheme1ThemeUiSettingsComponent } from "./ui-themes/wheel-theme1-theme-ui-settings.component";
import { WheelTheme2ThemeUiSettingsComponent } from "./ui-themes/wheel-theme2-theme-ui-settings.component";
import { WheelTheme3ThemeUiSettingsComponent } from "./ui-themes/wheel-theme3-theme-ui-settings.component";
import { WheelTheme4ThemeUiSettingsComponent } from "./ui-themes/wheel-theme4-theme-ui-settings.component";
import { WheelTheme5ThemeUiSettingsComponent } from "./ui-themes/wheel-theme5-theme-ui-settings.component";
import { WheelTheme6ThemeUiSettingsComponent } from "./ui-themes/wheel-theme6-theme-ui-settings.component";
import { WheelTheme7ThemeUiSettingsComponent } from "./ui-themes/wheel-theme7-theme-ui-settings.component";
import { WheelTheme8ThemeUiSettingsComponent } from "./ui-themes/wheel-theme8-theme-ui-settings.component";
import { WheelTheme9ThemeUiSettingsComponent } from "./ui-themes/wheel-theme9-theme-ui-settings.component";
import { WheelTheme10ThemeUiSettingsComponent } from "./ui-themes/wheel-theme10-theme-ui-settings.component";
import { WheelTheme11ThemeUiSettingsComponent } from "./ui-themes/wheel-theme11-theme-ui-settings.component";
import { WheelTheme12ThemeUiSettingsComponent } from "./ui-themes/wheel-theme12-theme-ui-settings.component";
import { FormlyModule } from "@ngx-formly/core";
import { FormlyBootstrapModule } from "@ngx-formly/bootstrap";
import { WheelTablesComponent } from "./tables/wheelTables.component";
import { CreateOrEditWheelTablesModalComponent } from "./tables/create-or-edit-wheelTables-modal.component";

import { NgxQRCodeModule } from "@techiediaries/ngx-qrcode";
import { ProductComboServiceProxy } from "@shared/service-proxies/service-proxies";
import { CheckboxModule } from "primeng/checkbox";
import { calendar } from "ngx-bootstrap/chronos/moment/calendar";
import { CalendarModule, ConfirmationService, ConfirmDialogModule, SidebarModule } from "primeng";
import { CreateOrEditTimeSlotWheelDepartmentModalComponent } from "./departments/create-or-edit-time-slot-wheel-department-modal.component";
import { OrdersComponent } from "./orders/orders.component";
import { DialogModule } from "primeng/dialog";
import { CardModule } from "primeng/card";
import { DataViewModule } from "primeng/dataview";
import { OrderDetailComponent } from "./orders/order-detail/order-detail.component";
import { OrderSettingsComponent } from "./basic-settings/order-settings/order-settings.component";
import { SliderModule } from "primeng/slider";
import { RadioButtonModule } from "primeng/radiobutton";
import { WhellScreenMenuTimingSettingComponent } from "./screen-menus/whell-screen-menu-timing-setting.component";
import { ShippingProviderSettingComponent } from "./basic-settings/shipping-provider-setting/shipping-provider-setting.component";
import { TiffinModule } from "@app/tiffin/tiffin.module";
import { CreateOrEditWheelSetupComponent } from './wheel-setup/create-or-edit-wheel-setup.component';
import { SeatmapComponent } from './seatmap/seatmap.component';
import { ReservationComponent } from './reservation/reservation.component';
import { DetailTableComponent } from './detail-table/detail-table.component';
import { ReservetionDetailComponent } from './reservetion-detail/reservetion-detail.component';
import { ProductSoldoutMenuComponent } from "./product-soldout/product-soldout-menu.component";
import { CreateOrEditProductSoldoutItemSectionComponent } from "./product-soldout/create-or-edit-product-soldout-menu-item.component";
import { RecommendationMenuComponent } from "./recommendation-menu/recommendation-menu.component";
import { CreateOrEditRecommendationItemSectionComponent } from "./recommendation-menu/create-or-edit-recommendation-menu-item.component";
import { CreateOrEditDayPeriodModalComponent } from "./recommendation-menu/day-period-modal.component";
@NgModule({
    imports: [
        WheelRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TabsModule,
        TooltipModule,
        NgxQRCodeModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        NgSelectModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        AppBsModalModule,
        UtilsModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyCbrgvMPgVlrdwHg7IfHe064oUKEhxma3M",
            libraries: ["drawing"],
        }),
        AccordionModule.forRoot(),
        ColorPickerModule,
        NgxCheckboxModule,
        UiSwitchModule,
        TabsModule,
        DragDropModule,
        FileUploadModule,
        FormlyModule.forRoot({
            validationMessages: [
                { name: "required", message: "This field is required" },
            ],
        }),
        FormlyBootstrapModule,
        CheckboxModule,
        CalendarModule,
        DialogModule,
        DataViewModule,
        CardModule,
        SliderModule,
        RadioButtonModule,
        ReactiveFormsModule,
        TiffinModule,
        ConfirmDialogModule,
        SidebarModule
    ],
    declarations: [
        ServiceFeesComponent,
        DeliveryZonesComponent,
        OpeningHoursComponent,
        TemplatesComponent,
        WheelDeliveryDurationsComponent,
        CreateOrEditWheelDeliveryDurationModalComponent,
        ViewWheelDeliveryDurationModalComponent,
        // WheelPaymentMethodsComponent,
        // EditWheelPaymentMethodComponent,
        WheelTaxesComponent,
        CreateOrEditWheelTaxModalComponent,
        ViewWheelTaxModalComponent,
        WheelLocationsComponent,
        CreateOrEditWheelLocationComponent,
        WheelDepartmentsComponent,
        CreateOrEditWheelDepartmentModalComponent,
        WheelTablesComponent,
        CreateOrEditWheelTablesModalComponent,
        WheelSetupsComponent,
        CreateOrEditTemplateComponent,
        CreateOrEditWheelScreenMenuComponent,
        WheelScreenMenuComponent,
        CreateOrEditWheelScreenMenuSectionComponent,
        WheelScreenMenuItemComponent,
        CreateOrEditWheelScreenItemSectionComponent,
        CreateOrEditOpeningHoursComponent,
        WheelUiCustomizationComponent,
        WheelTheme1ThemeUiSettingsComponent,
        WheelTheme2ThemeUiSettingsComponent,
        WheelTheme3ThemeUiSettingsComponent,
        WheelTheme4ThemeUiSettingsComponent,
        WheelTheme5ThemeUiSettingsComponent,
        WheelTheme6ThemeUiSettingsComponent,
        WheelTheme7ThemeUiSettingsComponent,
        WheelTheme8ThemeUiSettingsComponent,
        WheelTheme9ThemeUiSettingsComponent,
        WheelTheme10ThemeUiSettingsComponent,
        WheelTheme11ThemeUiSettingsComponent,
        WheelTheme12ThemeUiSettingsComponent,
        CreateOrEditTimeSlotWheelDepartmentModalComponent,
        OrdersComponent,
        OrderDetailComponent,
        OrderSettingsComponent,
        WhellScreenMenuTimingSettingComponent,
        ShippingProviderSettingComponent,
        CreateOrEditWheelSetupComponent,
        SeatmapComponent,
        ReservationComponent,
        DetailTableComponent,
        ReservetionDetailComponent,
        ProductSoldoutMenuComponent,
        CreateOrEditProductSoldoutItemSectionComponent,
        RecommendationMenuComponent,
        CreateOrEditRecommendationItemSectionComponent,
        CreateOrEditDayPeriodModalComponent
    ],
    providers: [ProductComboServiceProxy, DatePipe,ConfirmationService],
})
export class WheelModule {}

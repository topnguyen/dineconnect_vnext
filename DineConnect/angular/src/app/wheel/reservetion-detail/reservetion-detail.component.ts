import { result, forEach } from 'lodash';
import { SeatmapService } from './../seatmap/seatmap.service';
import { ReservationService } from './../reservation/reservation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Table } from '@app/shared/models/table.model';

@Component({
  selector: 'app-reservetion-detail',
  templateUrl: './reservetion-detail.component.html',
  styleUrls: ['./reservetion-detail.component.css']
})
export class ReservetionDetailComponent implements OnInit {

  hourList: Array<string> = [];
  minuteList: Array<string> = [];
  table: Table;
  tableList: Array<Table> = [];
  count: number = 0;
  reservationForm: FormGroup;
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {};
  selectTable: any = 2;
  isShow: boolean = true;
  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private reservationService: ReservationService,
    private seatMapService: SeatmapService,
    private router: Router) { }

  ngOnInit(): void {
    this.createForm();
    this.receiveDataFromTable();
    this.getCountryList();
    this.getTables();
    this.dropdownSettings = {
      singleSelection: true,
      textField: 'country',
      allowSearchFilter: true
    };
    const HOUR = 24;
    const MINUTE = 60;
    for (let i = 0; i < HOUR; i++) {
      if (i < 10) {
        this.hourList.push(`0${i}`);
      } else {
        this.hourList.push(i.toString());
      }
    }
    for (let j = 0; j < MINUTE; j += 5) {
      if (j < 10) {
        this.minuteList.push(`0${j}`);
      } else {
        this.minuteList.push(j.toString());
      }
    }
  }

  createForm() {
    this.reservationForm = this.fb.group({
      fromDate: '',
      hourFrom: '',
      minuteFrom: '',
      toDate: '',
      hourTo: '',
      minuteTo: '',
      uniqueId: Math.floor(Math.random() * 1000000),
      status: '',
      depositFee: 50,
      voucherCode: '',
      paymentMethod: '',
      depositPaid: false,
      people: 1,
      tableIds: this.fb.array([]),
      title: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      companyName: '',
      address: '',
      city: '',
      state: '',
      postcode: '',
      country: '',
      note: '',
      id: 0,
      customerId: 0
    });
  }
  get tableIds(): FormArray {
    return this.reservationForm.get('tableIds') as FormArray;
  }
  addTable() {
    const table = new Table();
    table.id = this.count++;
    this.tableIds.push(this.fb.control(table.id));

  }
  removeTable(index: number) {
    this.tableIds.removeAt(index);
  }
  receiveDataFromTable() {
    const RESERVATION_LENGTH = 3; // 3 hour
    this.route.queryParams.subscribe(async (params) => {
      let timeBook;
      let currentDate;
      let toDate;
      let toTimeBook;
      if (params.bookingId > 0) {
        let result = await this.reservationService.getReservationById(params.bookingId).toPromise();
        const tableIds = result.result.reserveTableIds.split(',');
        currentDate = result.result.fromDate.split('T')[0];
        timeBook = result.result.fromDate.split('T')[1].split(':');
        toDate = result.result.toDate.split('T')[0];
        toTimeBook = result.result.toDate.split('T')[1].split(':');
        tableIds.forEach(element => {
          this.tableIds.push(this.fb.control(+element));
        });
        const customer = await this.reservationService.getCustomerById(result.result.customerId).toPromise();

        this.reservationForm.patchValue({
          hourFrom: timeBook[0],
          minuteFrom: timeBook[1],
          hourTo: toTimeBook[0],
          minuteTo: toTimeBook[1],
          fromDate: currentDate,
          toDate: toDate,
          depositFee: result.result.depositFee,
          depositPaid: result.result.depositPay,
          people: result.result.totalPeopleCount,
          id: result.result.id,
          customerId: result.result.customerId,
          firstName: customer.result.name,
          email: customer.result.email
        });

      } else {
        timeBook = this.timeConvert(params.startPoint);
        currentDate = new Date().toISOString().substring(0, 10);
        this.tableIds.push(this.fb.control(+params.tableId));
        this.reservationForm.patchValue({
          hourFrom: timeBook[0],
          minuteFrom: timeBook[1],
          hourTo: (+timeBook[0] + RESERVATION_LENGTH).toString(),
          minuteTo: timeBook[1],
          fromDate: currentDate,
          toDate: currentDate,
        });
      }

    });
  }

  timeConvert(number: number): Array<any> {
    const hour = Math.floor(number / 60);
    const minute = number % 60;
    let hourResult = '';
    let minuteResult = '';
    if (hour < 10) {
      hourResult = `0${hour}`;
    } else {
      hourResult = hour.toString();
    }
    if (minute < 10) {
      minuteResult = `0${minute}`;
    } else {
      minuteResult = minute.toString();
    }
    return [hourResult, minuteResult];
  }
  onSubmit() {
    this.reservationForm.patchValue({
      fromDate: this.reservationForm.value.fromDate + " " + this.reservationForm.value.hourFrom + ":" + this.reservationForm.value.minuteFrom + ":00",
      toDate: this.reservationForm.value.toDate + " " + this.reservationForm.value.hourTo + ":" + this.reservationForm.value.minuteTo + ":00",
    });

    this.reservationService.updateReservation(this.reservationForm.value).subscribe(rs => {
      this.reservationForm.patchValue({
        fromDate: this.reservationForm.value.fromDate.split(" ")[0],
        toDate: this.reservationForm.value.toDate.split(" ")[0],
      });
      this.router.navigate(['/app/wheel/reserve/reservation']);
    })
  }
  getCountryList() {
    this.dropdownList = this.reservationService.getCountryList();
  }

  getTables() {
    this.seatMapService.getTables().subscribe(tables => {
      this.tableList = tables.result;
    })
  }
  onCancel(){
    this.router.navigate(['/app/wheel/reserve/reservation']);
  }

}

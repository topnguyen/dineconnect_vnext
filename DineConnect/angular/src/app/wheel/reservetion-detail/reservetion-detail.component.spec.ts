import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservetionDetailComponent } from './reservetion-detail.component';

describe('ReservetionDetailComponent', () => {
  let component: ReservetionDetailComponent;
  let fixture: ComponentFixture<ReservetionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservetionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservetionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

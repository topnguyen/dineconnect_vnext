import { Component, Injector, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ThemeSettingsDto, WheelCustomizationSettingsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './wheel-theme1-theme-ui-settings.component.html',
    animations: [appModuleAnimation()],
    selector: 'wheel-theme1-theme-ui-settings'
})
export class WheelTheme1ThemeUiSettingsComponent extends AppComponentBase {
    @Input() settings: ThemeSettingsDto;

    constructor(
        injector: Injector,
        private _wheelCustomizationSettingsServiceProxy: WheelCustomizationSettingsServiceProxy
    ) {
        super(injector);
    }

    getCustomizedSetting(settings: ThemeSettingsDto) {
        settings.theme = 'wheelTheme1';

        return settings;
    }

    updateUiManagementSettings(): void {
        this._wheelCustomizationSettingsServiceProxy.updateUiManagementSettings(this.getCustomizedSetting(this.settings)).subscribe(() => {
            this.notify.success(this.l("Successfully"));
        });
    }

    allowAsideMinimizingChange(val): void {
        if (!val) {
            this.settings.menu.defaultMinimizedAside = false;
        }
    }
}

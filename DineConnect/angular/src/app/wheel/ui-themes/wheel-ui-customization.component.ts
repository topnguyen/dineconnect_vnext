import { Component, ViewEncapsulation, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ThemeSettingsDto, WheelCustomizationSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';

@Component({
    templateUrl: './wheel-ui-customization.component.html',
    styleUrls: ['./wheel-ui-customization.component.less'],
    animations: [appModuleAnimation()],
    encapsulation: ViewEncapsulation.None
})
export class WheelUiCustomizationComponent extends AppComponentBase implements OnInit {

    themeSettings: ThemeSettingsDto[];
    currentThemeName = '';

    constructor(
        injector: Injector,
        private _wheelCustomizationSettingsServiceProxy: WheelCustomizationSettingsServiceProxy
    ) {
        super(injector);
    }

    getLocalizedThemeName(str: string): string {
        return this.l('Theme_' + abp.utils.toPascalCase(str));
    }

    ngOnInit(): void {
        this._wheelCustomizationSettingsServiceProxy.getUiManagementSettings().subscribe((settingsResult) => {
            this.themeSettings = _.sortBy(settingsResult, setting => {
                return parseInt(setting.theme.replace('wheelTheme', ''));
            });
            this.currentThemeName = this.themeSettings[0].theme;
        });
    }
}

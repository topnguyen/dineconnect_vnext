import { Component, Injector, Input } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    ThemeSettingsDto,
    UiCustomizationSettingsServiceProxy,
    WheelCustomizationSettingsServiceProxy,
} from "@shared/service-proxies/service-proxies";

@Component({
    templateUrl: "./wheel-theme12-theme-ui-settings.component.html",
    animations: [appModuleAnimation()],
    selector: "wheel-theme12-theme-ui-settings",
})
export class WheelTheme12ThemeUiSettingsComponent extends AppComponentBase {
    @Input() settings: ThemeSettingsDto;

    constructor(
        injector: Injector,
        private _wheelCustomizationSettingsServiceProxy: WheelCustomizationSettingsServiceProxy
    ) {
        super(injector);
    }

    getCustomizedSetting(settings: ThemeSettingsDto) {
        settings.theme = "wheelTheme12";

        return settings;
    }

    updateUiManagementSettings(): void {
        this._wheelCustomizationSettingsServiceProxy
            .updateUiManagementSettings(
                this.getCustomizedSetting(this.settings)
            )
            .subscribe(() => {
                this.notify.success(this.l("Successfully"));
            });
    }

    allowAsideMinimizingChange(val): void {
        if (!val) {
            this.settings.menu.defaultMinimizedAside = false;
        }
    }
}

import { SeatmapService } from './../seatmap/seatmap.service';
import { ReservationService } from './reservation.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Table } from '@app/shared/models/table.model';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {


  booking = [];
  tableList: Array<any> = [];
  tableListResult: Array<any> = [];
  countTable: number = 0;
  dateBooking: string = new Date().toISOString().substring(0, 10);
  noBook = [
    { from: 1200, to: 1380 },
  ];
  workingTime = {
    from: 540,
    to: 1380,
  };
  headerTable: Array<string> = [
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00',
    '22:00',
  ];
  constructor(private router: Router, private reservationService: ReservationService, private seatMapService: SeatmapService) { }
  async ngOnInit() {
    await this.getTableList();
    await this.getReservations();
    this.tableListResult.forEach(element => {
      let itemTable = {
        id: element.id,
        tableDataBook: Array<any>(),
      };
      for (
        let i = 0;
        i < (this.workingTime.to - this.workingTime.from) / 5;
        i++
      ) {
        let itemList = {
          startPoint: this.workingTime.from + i * 5,
          endPoint: this.workingTime.from + (i + 1) * 5,
          content: '+',
          colspan: 1,
          bookingId: 0,
          status:'None'
        };
        let checkPush = false;
        if (this.booking != null && this.booking.length > 0) {
          let tableBook = this.booking.filter((x) => x.tableId == itemTable.id);
          for (let j = 0; j < tableBook.length; j++) {
            if (tableBook[j].booking.from == this.workingTime.from + i * 5) {
              i += (tableBook[j].booking.to - tableBook[j].booking.from) / 5 - 1;
              itemList.endPoint = this.workingTime.from + (i + 1) * 5;
              itemList.content = tableBook[j].name;
              itemList.colspan = (tableBook[j].booking.to - tableBook[j].booking.from) / 5;
              itemList.bookingId = tableBook[j].booking.bookingId;
              itemList.status = tableBook[j].status;
              checkPush = true;
              break;
            }
          }
        }
        if (!checkPush) {
          for (let k = 0; k < this.noBook.length; k++) {
            if (
              this.workingTime.from + i * 5 >= this.noBook[k].from &&
              this.workingTime.from + i * 5 <= this.noBook[k].to
            ) {
              itemList.content = '-';
              break;
            }
          }
        }
        itemTable.tableDataBook.push(itemList);
      }
      this.tableList.push(itemTable);
    });
  }

  navigateReservation(table: Table, item: any) {
    const object = { tableId: table.id, startPoint: item.startPoint, bookingId: item.bookingId };
    this.router.navigate(['/app/wheel/reserve/reservation-detail'], {
      queryParams: object,
    });
  }
  async getReservations() {
    this.booking = [];
    let data = await this.reservationService.getReservation(this.dateBooking).toPromise();
    if (data) {
      this.booking = data.result;
    }
  }

  async getTableList() {
    const data = await this.seatMapService.getTables().toPromise();
    if (data) {
      this.tableListResult = data.result;
    }
  }

}

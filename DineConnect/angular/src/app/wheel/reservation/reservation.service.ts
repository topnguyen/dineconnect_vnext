import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private heroesUrl = AppConsts.remoteServiceBaseUrl + '/api/services/app/Reserve/';
  country = {
    "data": [
      {
        "id": 1,
        "name": "Algeria",
      },
      {
        "id": 2,
        "name": "Angola",
      },
      {
        "id": 3,
        "name": "Benin",
      }
    ]
  }
  constructor(private http: HttpClient) {

  }
  /**
  * get country list
  */
  getCountryList(): any {
    return this.country.data;
  }

  /**
   * get reservation
   */
  getReservation(dateBooking: any): Observable<any> {
    return this.http.get<any>(`${this.heroesUrl}GetReservation?dateBook=${dateBooking}`);
  }
  /**
  * update reservation
  */
  updateReservation(reservation): Observable<any> {
    return this.http.post<any>(`${this.heroesUrl}SaveReservationDetail`, reservation);
  }
  getReservationById(reservationId: number): Observable<any>{
    return this.http.get<any>(`${this.heroesUrl}GetReservationById?id=${reservationId}`);
  }
  getCustomerById(customerId: number): Observable<any>{
    return this.http.get<any>(`${this.heroesUrl}GetCustomerById?id=${customerId}`);
  }

  
}

﻿import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    WheelDepartmentsServiceProxy,
    WheelServiceProxy,
    WheelLocationsServiceProxy,
    WheelTableBaseModel,
    WheelTableServiceProxy
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: "createOrEditWheelTablesModal",
    templateUrl: "./create-or-edit-wheelTables-modal.component.html",
    styleUrls: ["./wheelTables.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelTablesModalComponent
    extends AppComponentBase
    implements OnInit {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    tableName = "";
    active = false;
    saving = false;

    wheelTable: WheelTableBaseModel = new WheelTableBaseModel();
    wheelTableId: number;
    allLocation: any[];
    allDepartment: any[];
    allTableStatus: any[] = [
        {
            value: 0,
            displayText: 'Vacant'
        },
        {
            value: 1,
            displayText: 'Booked'
        }];
    selectedStatus: string;
    selectedLocation: string;
    selectedDepartment: string;

    constructor(
        injector: Injector,
        private _wheelDepartmentsServiceProxy: WheelDepartmentsServiceProxy,
        private _wheelLocationsServiceProxy: WheelLocationsServiceProxy,
        private _wheelTableServiceProxy: WheelTableServiceProxy,
        private _serviceProxy: WheelServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(injector);

        this.getDataForCombobox();
    }

    ngOnInit() {
        this.wheelTable = new WheelTableBaseModel();

        this._activatedRoute.params.subscribe((params) => {
            this.wheelTableId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.wheelTableId)) {
                this._wheelTableServiceProxy
                    .getWheelTableForEdit(this.wheelTableId)
                    .subscribe((result) => {
                        this.wheelTable = result;
                        this.selectedStatus = result.wheelTableStatus.toString();
                        this.selectedLocation = result.wheelLocationId.toString();
                        this.selectedDepartment = result.wheelDepartmentId.toString();
                    });
            }
        });
    }

    getDataForCombobox() {
        this._wheelLocationsServiceProxy
            .getAllLocationForTableDropdown()
            .subscribe((result) => {
                this.allLocation = result.map((x) => {
                    return {
                        value: x.id,
                        displayText: x.displayName
                    };
                });
            });
        this._wheelDepartmentsServiceProxy
            .getAllDepartmentForTableDropdown()
            .subscribe((result) => {
                this.allDepartment = result.map((x) => {
                    return {
                        value: x.id,
                        displayText: x.displayName
                    };
                });
            });
    }

    save(): void {
        this.saving = true;
        this._wheelTableServiceProxy
            .createOrEdit(this.wheelTable)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/wheel/tables"]);
    }

}

﻿import { Component, Injector, ViewEncapsulation, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    WheelDepartmentsServiceProxy,
    WheelLocationsServiceProxy,
    WheelTableStatus,
    WheelTableServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';

@Component({
    templateUrl: './wheelTables.component.html',
    styleUrls: ['./wheelTables.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WheelTablesComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('popupQRCode', { static: true }) popupQRCodeRef: ElementRef;
    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    locationNameFilter = '';
    departmentNameFilter = '';
    wheelTableStatusFilter = -1;
    allTableStatus: any[] = [
        {
            value: 0,
            displayText: 'Vacant'
        },
        {
            value: 1,
            displayText: 'Booked '
        }
    ];
    wheelTableStatus;
    wheelTableLocation;
    wheelTableDepartment;

    elementType = NgxQrcodeElementTypes.URL;
    correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
    qrCodeContentSelected: string = '';
    fileDownloadName: string = '';
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _wheelDepartmentsServiceProxy: WheelDepartmentsServiceProxy,
        private _wheelTableServiceProxy: WheelTableServiceProxy,
        private _wheelLocationsServiceProxy: WheelLocationsServiceProxy,
        private _router: Router
    ) {
        super(injector);
        this.wheelTableStatus = WheelTableStatus;
        // this.getDataForCombobox();
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelTable();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getWheelTable(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelTableServiceProxy
            .getAll(
                this.filterText,
                this.nameFilter,
                this.locationNameFilter,
                this.departmentNameFilter,
                this.wheelTableStatusFilter === -1 ? undefined : this.wheelTableStatusFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.items.length;
                this.primengTableHelper.records = result.items;
            });
    }

    getDataForCombobox() {
        this._wheelLocationsServiceProxy.getAllLocationForTableDropdown().subscribe((result) => {
            this.wheelTableLocation = result.map((x) => {
                return {
                    value: x.id,
                    displayText: x.displayName
                };
            });
        });
        this._wheelDepartmentsServiceProxy.getAllDepartmentForTableDropdown().subscribe((result) => {
            this.wheelTableDepartment = result.map((x) => {
                return {
                    value: x.id,
                    displayText: x.displayName
                };
            });
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createWheelDepartment(id?: number): void {
        this._router.navigate(['/app/wheel/tables/create-or-edit', id ? id : 'null']);
    }

    deleteWheelDepartment(id?: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._wheelTableServiceProxy.delete(id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    generateOTP(id?: number): void {
        this._wheelTableServiceProxy.generateOtp(id).subscribe(() => {
            this.reloadPage();
            this.notify.success(this.l('SuccessfullyDeleted'));
        });
    }

    showQR(tableItem): void {
        this.qrCodeContentSelected = tableItem.qrLink;
        console.log(tableItem);
        console.log(tableItem.qrLink);

        this.fileDownloadName = [tableItem.location, tableItem.department, tableItem.name].join('_');
        this.popupQRCodeRef.nativeElement.style.display = 'flex';
        fromEvent(this.popupQRCodeRef.nativeElement, 'click')
            .pipe(takeUntil(this.destroy$))
            .subscribe((event) => {
                const elementOnClick: HTMLElement = event['target'];
                if (elementOnClick.classList.contains('bg-wrapper-qr') || elementOnClick.classList.contains('btn-outline-secondary')) {
                    this.fileDownloadName = '';
                    this.qrCodeContentSelected = '';
                    this.popupQRCodeRef.nativeElement.style.display = 'none';
                } else if (elementOnClick.classList.contains('btn-primary')) {
                    const imgHtml = document.querySelector('.img-qr').querySelector('img').src;
                    const downloadElement = document.createElement('a');
                    downloadElement.href = imgHtml;
                    downloadElement.download = this.fileDownloadName + '.png';
                    downloadElement.click();
                    downloadElement.remove();
                    this.fileDownloadName = '';
                    this.qrCodeContentSelected = '';
                    this.popupQRCodeRef.nativeElement.style.display = 'none';
                }
            });
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.nameFilter = '';
        this.locationNameFilter = '';
        this.departmentNameFilter = '';
        this.wheelTableStatusFilter = -1;
    }

    apply() {
        this.getWheelTable();
        this.isShowFilterOptions = false;
    }

    exportToExcel(): void {}
}

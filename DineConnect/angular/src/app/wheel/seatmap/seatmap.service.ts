import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';

@Injectable({
  providedIn: 'root'
})
export class SeatmapService {

  private heroesUrl = AppConsts.remoteServiceBaseUrl + '/api/services/app/Reserve/';
  constructor(private http: HttpClient) { }
  getTables(): Observable<any> {
    return this.http.get<any>(`${this.heroesUrl}GetSeatMapList`);
  }
  saveTable(position: any): Observable<any> {
    return this.http.post<any>(`${this.heroesUrl}SaveReserveTable`, position);
  }

  saveSeatMap(position: any): Observable<any> {
    return this.http.post<any>(`${this.heroesUrl}SaveReserveSeatMap`, position);
  }

  saveSeatMapBackgroundUrl(data: any): Observable<any> {
    return this.http.post<any>(`${this.heroesUrl}SaveSeatMapBackgroundUrl`, data);
  }
  
  getSeatMapBackgroundUrl(data: any): Observable<any> {
    return this.http.get<any>(`${this.heroesUrl}GetSeatMapBackgroundUrl?id=`+ data);
  }

  deleteSeatMap(data: any): Observable<any> {
    return this.http.delete<any>(`${this.heroesUrl}DeleteSeatMap?id=`+ data);
  }
}

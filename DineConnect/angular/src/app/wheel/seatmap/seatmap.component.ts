import { DetailTableComponent } from './../detail-table/detail-table.component';
import { SeatmapService } from './seatmap.service';
import { Component, OnInit, ViewChild, TemplateRef, Injector } from '@angular/core';
import { Table } from '@app/shared/models/table.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import { FileUploader } from 'ng2-file-upload';
import { FileUpload } from 'primeng/fileupload';
import { AppConsts } from '@shared/AppConsts';
import { TokenService, IAjaxResponse } from 'abp-ng2-module';
import { DomSanitizer } from '@angular/platform-browser';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-seatmap',
  templateUrl: './seatmap.component.html',
  styleUrls: ['./seatmap.component.css']
})
export class SeatmapComponent extends AppComponentBase implements OnInit {

  @ViewChild('dialogRef') dialogRef!: TemplateRef<any>;
  @ViewChild('showModal') showModal: any;
  @ViewChild('editModal') editModal: TemplateRef<any>;
  @ViewChild('ImageFileUpload', { static: true }) imageFileUpload: FileUpload;

  uploadImageToken: string = null;
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
  seatmapImageUrl = '';

  uploader: FileUploader;
  filePreviewPath: any;
  fileName: any;
  loading = true;

  constructor(
    injector: Injector,
    private modalService: NgbModal,
    private seatMapService: SeatmapService,
    private _tokenService: TokenService,
    private _sanitizer: DomSanitizer
  ) { super(injector); }

  ngOnInit(): void {
    this.getSeats();
    this.uploadInit();
    this.getBackground();
  }
  openModal() {
    this.modalService.open(this.editModal);
  }
  closeModel() {
    this.showModal.nativeElement.className = 'modal hide';
  }
  positionClone: Table;
  position: Table;
  positionList: Array<Table> = [];
  // positionList1: Array<Table> = [];
  x: any;
  y: any;
  count = 0;
  positionSelected: Table = new Table();
  eventImage: any;
  addPoint(event: any) {
    const width = 30;
    const height = 30;
    var elem = document.getElementById('imgSeatMap') as HTMLElement,
      elemLeft = elem.getBoundingClientRect().left,
      elemTop = elem.getBoundingClientRect().top;
    var x = event.pageX - elemLeft,
      y = event.pageY - elemTop;
    this.x = x;
    this.y = y;
    const position = new Table();
    position.positionX = this.x;
    position.positionZ = this.y;
    position.width = width;
    position.height = height;
    position.isDeleted = false;
    position.idElement = this.positionList.length + 1;
    position.name = (this.positionList.length + 1).toString();
    this.positionList.push(position);

  }

  dragEnded($event: any, position: Table) {
    const { offsetLeft, offsetTop } = $event.source.element.nativeElement;
    const { x, y } = $event.distance;
    this.positionClone = cloneDeep(position);
    this.positionClone.positionX = offsetLeft + x;
    this.positionClone.positionZ = offsetTop + y;
    this.positionClone.height = $event.source.element.nativeElement.offsetHeight;
    this.positionClone.width = $event.source.element.nativeElement.offsetWidth;

    this.positionList = this.positionList.filter(positionFilter => positionFilter.idElement != position.idElement)
    this.positionList.push(this.positionClone);
  }
  showDetail(position: Table) {
    const modalRef = this.modalService.open(DetailTableComponent);
    this.positionSelected = position;
    modalRef.componentInstance.position = this.positionSelected;
    modalRef.result.then(
      (result) => {
        const positionResult = result.position;
        const action = result.action;
        switch (action) {
          case 'delete':
            this.positionList.forEach(position => {
              if (position.idElement === positionResult.idElement && !position.isDeleted) {
                position.isDeleted = true;
              }
            })
            break;
          case 'save':
            break;
          default:
            break;
        }
      },
      (dismiss) => {
      }
    );
  }
  getSeats() {
    this.seatMapService.getTables().subscribe((tables) => {
      this.positionList = [];
      tables.result.forEach((element, i) => {
        const item = element;
        item.idElement = i + 1;
        this.positionList.push(item);
      });
    });
  }

  async updateSeats() {
    const positionListSave = [];
    await this.positionList.forEach((position, i) => {
      const widthElement = document.getElementById(`example-box-${i}`)
        ?.offsetWidth as number;
      const heightElement = document.getElementById(`example-box-${i}`)
        ?.offsetHeight as number;
      position.width = widthElement;
      position.height = heightElement;
      position.name = position.name;
      positionListSave.push(position);
    });
    await this.seatMapService.saveSeatMap(positionListSave).subscribe(result => {
      alert("Save success")
    });
  }

  uploadInit() {
    this.loading = true;
    this.uploader = new FileUploader({
      url: this.remoteServiceBaseUrl,
      authToken: 'Bearer ' + this._tokenService.getToken(),
    });

    this.uploader.onAfterAddingFile = f => {
      this.loading = false;
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
      this.filePreviewPath = this._sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(f._file)));
      this.fileName = f._file.name;
      
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <IAjaxResponse>JSON.parse(response);
      if (ajaxResponse.success) {
        this.notify.success(this.l('UploadImageSuccessfully'));
        this.uploadImageToken = ajaxResponse.result.fileToken;
        this.seatmapImageUrl = ajaxResponse.result.url;
        this.seatMapService.saveSeatMapBackgroundUrl({ 'tenantId': this.appSession.tenantId, 'url': this.seatmapImageUrl }).subscribe(result => {
          this.loading = false;
        });
      } else {
        this.loading = false;
        this.message.error(ajaxResponse.error.message);
      }
    };

    this.uploader.onErrorItem = () => {
      this.message.error(this.l('UploadImageFailed'));
    };
  }

  removeFile(item) {
    item.remove();
    this.uploadImageToken = undefined;
    this.fileName = '';
    this.loading = false;
  }

  saveBackground() {
    this.loading = true;
    this.uploader.uploadAll();
  }

  async getBackground() {
   await this.seatMapService.getSeatMapBackgroundUrl(this.appSession.tenantId).subscribe(response => {
      this.seatmapImageUrl = (response.result.url)?response.result.url:'';
    });
  }

  async deleteSeatMap()
  {
    this.seatMapService.deleteSeatMap(this.appSession.tenantId).subscribe((response) => {
      this.seatmapImageUrl = '';
      this.getSeats();
    });
  }
   
}

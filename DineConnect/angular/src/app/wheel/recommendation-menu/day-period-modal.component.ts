import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TimeSlotDto, WheelDepartmentsServiceProxy, WheelOpeningHourServiceType } from '@shared/service-proxies/service-proxies';
import { WheelRecommendationDayPeriodDto, WheelRecommendationMenusServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { OpeningHour } from '../basic-settings/opening-hours/opening-hour';

@Component({
  selector: 'wheel-recommendation-day-period-modal',
  templateUrl: './day-period-modal.component.html',
  styleUrls: ['./day-period-modal.component.css']
})
export class CreateOrEditDayPeriodModalComponent extends AppComponentBase {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  openingHour: OpeningHour = new OpeningHour(WheelOpeningHourServiceType.Default);

  saving = false;
  active = false;
  timeFrom = new Date();
  timeTo = new Date();
  recommendationMenuId: number;
  timeSlot: WheelRecommendationDayPeriodDto = new WheelRecommendationDayPeriodDto();

  constructor(
    injector: Injector,
    private _recommendationService: WheelRecommendationMenusServiceProxy,
    public datepipe: DatePipe

  ) {
    super(injector);
  }

  show(id?: any): void {
    this.active = true;
    if (id != null) {
      this._recommendationService.getDayPeriodById(id).subscribe(result => {
        this.timeSlot = result;
        this.timeFrom = this.formatDate(this.timeSlot.from);
        this.timeTo = this.formatDate(this.timeSlot.to);
        this.modal.show();
      });
    }
    else {
      this.timeSlot = new WheelRecommendationDayPeriodDto();
      this.modal.show();
    }
  }

  save(): void {
    this.saving = true;
    this.timeSlot.from = this.datepipe.transform(this.timeFrom, 'HH:mm');
    this.timeSlot.to = this.datepipe.transform(this.timeTo, 'HH:mm');
    this.timeSlot.recommendationId = this.recommendationMenuId;
    this.timeSlot.openDays = (this.openingHour.toDto()).openDays;
    // this._recommendationService.createOrEditDayPeriod(this.timeSlot).subscribe(result => {
    //   this.modalSave.emit(this.timeSlot);
    //   this.close();
    // })
    this.modalSave.emit(this.timeSlot);
    this.close();
  }

  formatDate(input: string) {
    var datetimeStr = '2020-10-10' + ' ' + input;
    return new Date(datetimeStr);
  }

  close(): void {
    this.active = false;
    this.saving = false;
    this.timeFrom = new Date();
    this.timeTo = new Date();
    this.modal.hide();
  }
}

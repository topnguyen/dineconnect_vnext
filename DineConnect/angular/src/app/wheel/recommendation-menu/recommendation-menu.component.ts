import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { finalize } from "rxjs/operators";
import {
    WheelScreenMenusListDto,
    UpdatePublishInput,
} from "@shared/service-proxies/service-proxies";
import { Router } from "@angular/router";
import {
    CdkDrag,
    CdkDropList,
    CdkDropListGroup,
    moveItemInArray,
} from "@angular/cdk/drag-drop";
import { LocalStorageService } from "@shared/utils/local-storage.service";
import { AppConsts } from "@shared/AppConsts";
import {
    WheelRecommendationMenuDto,
    WheelRecommendationMenusServiceProxy,
} from "@shared/service-proxies/service-proxies-nswag";
import { SingleLocationDto } from "@app/shared/common/select-single-location/select-single-location.component";

@Component({
    templateUrl: "./recommendation-menu.component.html",
    styleUrls: ["./recommendation-menu.component.scss"],
    animations: [appModuleAnimation()],
})
export class RecommendationMenuComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filter = "";
    items: WheelRecommendationMenuDto[] = [];

    @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
    @ViewChild(CdkDropList) placeholder: CdkDropList;
    public target: CdkDropList;
    public targetIndex: number;
    public source: any;
    public sourceIndex: number;
    locationOrGroup = new SingleLocationDto();

    constructor(
        injector: Injector,
        private _router: Router,
        private _service: WheelRecommendationMenusServiceProxy,
        private _localStorageService: LocalStorageService
    ) {
        super(injector);
        this.target = null;
        this.source = null;
    }

    ngOnInit(): void {
        this.getData();
    }

    ngAfterViewInit() {
        let phElement = this.placeholder.element.nativeElement;

        phElement.style.display = "none";
        phElement.parentNode.removeChild(phElement);
        this.locationOrGroup = new SingleLocationDto();
        this.locationOrGroup.locationId = 0;
    }

    getData(): void {
        this.spinnerService.show();
        var locationId = this.locationOrGroup.locationId;

        this._service
            .getAll(this.filter, locationId)
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe((result) => {
                this.items = result.items;
            });
    }

    createOrEdit(id?: number) {
        this._router.navigate([
            "/app/wheel/recommendation-menus/create-or-edit",
            id ? id : "null",
        ]);
    }

    delete(model: WheelScreenMenusListDto) {
        this.message.confirm(
            this.l("WheelScreennMenuDeleteWarningMessage", model.name),
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._service.delete(model.id).subscribe(() => {
                        this.getData();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
                }
            }
        );
    }

    enterChildren(id?: number): void {
        this._localStorageService.setItem(AppConsts.ScreenMenuBreadcrums, [
            { sectionId: -1, sectionName: "Menus" },
        ]);
        this._router.navigate(["/app/wheel/screen-menu-items", id ? id : ""]);
    }

    updateDisable(id: number, published: boolean) {
        this._service
            .updateDisable(
                new UpdatePublishInput({
                    id: id,
                    publish: published,
                    type: null,
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
            });
    }

    drop() {
        if (!this.target) return;

        let phElement = this.placeholder.element.nativeElement;
        let parent = phElement.parentNode;

        phElement.style.display = "none";

        parent.removeChild(phElement);
        parent.appendChild(phElement);
        parent.insertBefore(
            this.source.element.nativeElement,
            parent.children[this.sourceIndex]
        );

        this.target = null;
        this.source = null;

        if (this.sourceIndex != this.targetIndex)
            moveItemInArray(this.items, this.sourceIndex, this.targetIndex);
    }

    enter = (drag: CdkDrag, drop: CdkDropList) => {
        if (drop == this.placeholder) return true;

        let phElement = this.placeholder.element.nativeElement;
        let dropElement = drop.element.nativeElement;

        let dragIndex = __indexOf(
            dropElement.parentNode.children,
            drag.dropContainer.element.nativeElement
        );
        let dropIndex = __indexOf(dropElement.parentNode.children, dropElement);

        if (!this.source) {
            this.sourceIndex = dragIndex;
            this.source = drag.dropContainer;

            let sourceElement = this.source.element.nativeElement;
            phElement.style.width = sourceElement.clientWidth + "px";
            phElement.style.height = sourceElement.clientHeight + "px";

            sourceElement.parentNode.removeChild(sourceElement);
        }

        this.targetIndex = dropIndex;
        this.target = drop;

        phElement.style.display = "";
        dropElement.parentNode.insertBefore(
            phElement,
            dragIndex < dropIndex ? dropElement.nextSibling : dropElement
        );

        this.source.start();
        this.placeholder.enter(
            drag,
            drag.element.nativeElement.offsetLeft,
            drag.element.nativeElement.offsetTop
        );

        return false;
    };
}

function __indexOf(collection, node) {
    return Array.prototype.indexOf.call(collection, node);
}

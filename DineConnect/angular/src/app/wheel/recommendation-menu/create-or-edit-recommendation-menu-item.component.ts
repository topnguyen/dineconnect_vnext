import {
    Component,
    OnInit,
    Injector,
    AfterViewInit,
    ViewChild,
    EventEmitter,
    Output,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    MenuItemServiceProxy,
    PagedResultDtoOfMenuItemAndPriceForSelection,
    MenuItemAndPriceForSelection,
    WheelScreenMenuItemlPricesForEditDto,
    WheelScreenMenuItemForSection,
    PagedResultDtoOfLocationDto,
    LocationServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { Observable } from "rxjs";
import { DatePipe } from "@angular/common";
import { MenuItemTagsConst } from "@shared/AppConsts";
import { CreateOrEditDayPeriodModalComponent } from "./day-period-modal.component";
import {
    CriteriaNotShow,
    RecommendationStatus,
    TriggerApplicable,
    WheelRecommendationItemDto,
    WheelRecommendationMenuDto,
    WheelRecommendationMenusServiceProxy,
} from "@shared/service-proxies/service-proxies-nswag";

@Component({
    templateUrl: "./create-or-edit-recommendation-menu-item.component.html",
    styleUrls: ["./create-or-edit-recommendation-menu-item.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditRecommendationItemSectionComponent
    extends AppComponentBase
    implements OnInit, AfterViewInit
{
    @ViewChild("selectMenuComboPopupModal", { static: true })
    selectMenuComboPopupModal: SelectPopupComponent;
    @ViewChild("selectMenuItemPopupModal", { static: true })
    selectMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("selectWheelScreenMenuItemPopupModal", { static: true })
    selectWheelScreenMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("createOrEditDayPeriod", { static: true })
    createOrEditDayPeriod: CreateOrEditDayPeriodModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    item: WheelRecommendationMenuDto;
    screenMenuId: number;
    parentSectionId: number;
    itemId: number;
    saving = false;
    selectedPrice;
    currentComponent: any;
    selectRecommendItems: WheelScreenMenuItemForSection[];
    menuItemTags = MenuItemTagsConst;
    comboName: string = "";
    comboId: number = -1;
    comboPrice = new WheelScreenMenuItemlPricesForEditDto();
    selectedTags: string[];
    isDisabledItem: boolean = false;
    isDisabledCombo: boolean = false;
    selectedItemName: string;
    locationName = "";
    recommendationStatus = RecommendationStatus;
    criteriaNotShow = CriteriaNotShow;
    triggerApplicable = TriggerApplicable;
    isRecommendAtCheckout: boolean = false;
    isRecommendAtMakePayment: boolean = false;
    isTimerBasedRecommendation: boolean = false;

    constructor(
        _injector: Injector,
        private _service: WheelRecommendationMenusServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _menuitemServiceProxy: MenuItemServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        public datepipe: DatePipe
    ) {
        super(_injector);
    }

    ngOnInit() {
        this.item = new WheelRecommendationMenuDto();
        this.item.disable = false;
        this._activatedRoute.params.subscribe((params) => {
            this.itemId = +params["itemId"];
            if (!isNaN(this.itemId)) {
                this._service
                    .getWheelRecommendationMenuForEdit(this.itemId)
                    .subscribe((result) => {
                        this.item = result;
                        this.item.id = this.itemId;
                        // //menu items
                        // if (this.item.menuItemId) {
                        //     this.selectedPrice = {
                        //         id: this.item.menuItemId,
                        //         name: this.item.name,
                        //     };

                        //     this.selectedItemName = this.selectedPrice.name;
                        // }
                        // //location
                        // if (this.item.locationId) {
                        //     this.locationName = this.item.locationName;
                        // }
                    });
            }
        });
    }

    close(): void {
        this._router.navigate(["/app/wheel/recommendation-menu"]);
    }

    ngAfterViewInit() {}

    openSelectionPricePopup() {
        this.selectMenuItemPopupModal.show();
    }

    getMenuItemSelection() {
        return (
            filterText,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfMenuItemAndPriceForSelection> =>
            this._menuitemServiceProxy.getMenuItemAndPriceForSelection(
                filterText,
                sorting,
                maxResultCount,
                skipCount
            );
    }

    save() {
        if (!this.selectedItemName) {
            this.notify.error(this.l("MenuItem is requied."));
            return;
        }
        if (!this.locationName) {
            this.notify.error(this.l("Location is requied."));
            return;
        }
        this.saving = true;
        const model = new WheelRecommendationMenuDto({
            id: this.item.id,
            name: undefined,
            disable: this.item.disable,
            description: "",
            criteriaNotShow: CriteriaNotShow.Item,
            pax: 0,
            repeatTime: undefined,
            status: RecommendationStatus.Active,
            triggerApplicable: [],
            wheelRecommendationDayPeriods: undefined,
            wheelRecommendationItems: undefined,
        });
        this._service
            .createOrEditWheelRecommendationMenu(model)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    selectItem(itemSelect: MenuItemAndPriceForSelection) {
        if (!this.item.wheelRecommendationItems) {
            this.item.wheelRecommendationItems = [];
        }
        if (itemSelect) {
            let existingItem = this.item.wheelRecommendationItems.find(
                (x) => x.menuItemId == itemSelect.id
            );
            if (!existingItem) {
                var newItem = new WheelRecommendationItemDto({
                    id: undefined,
                    recommendation: undefined,
                    menuItemId: itemSelect.id,
                    recommendationId: this.item.id,
                    menuName: itemSelect.name
                });
                this.item.wheelRecommendationItems.push(newItem);
            }
        }
    }

    removeMenuItem(itemSelect: WheelRecommendationItemDto) {
        let existingItem = this.item.wheelRecommendationItems.find(
            (x) => x.menuItemId == itemSelect.id
        );
        if (existingItem) {
            this.item.wheelRecommendationItems =
                this.item.wheelRecommendationItems.filter(
                    (x) => x.menuItemId != itemSelect.menuItemId
                );
        }
    }

    getLocationForCombobox() {
        return (
            filterString,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(
                filterString,
                false,
                0,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    changeLocation(data) {
        this.locationName = data.name;
    }

    onStatusChange(status) {
        this.item.status = status;
    }

    onCriteriaNotShowChange(criteria) {
        this.item.criteriaNotShow = criteria;
    }

    addEditDayPeriod(id?: number) {
        this.createOrEditDayPeriod.show(id);
        this.createOrEditDayPeriod.modalSave.subscribe((resultData) => {
            let existingItem =
                this.item.wheelRecommendationDayPeriods != null
                    ? this.item.wheelRecommendationDayPeriods.find(
                          (x) => x.id == resultData.id
                      )
                    : null;
            if (existingItem) {
                this.item.wheelRecommendationDayPeriods =
                    this.item.wheelRecommendationDayPeriods.map((x) => {
                        if (x.id == resultData.id) {
                            return resultData;
                        }
                        return x;
                    });
            } else {
                if (!this.item.wheelRecommendationDayPeriods) {
                    this.item.wheelRecommendationDayPeriods = [];
                }
                this.item.wheelRecommendationDayPeriods.push(resultData);
            }
        });
    }
}

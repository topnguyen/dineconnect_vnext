import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TimeSlotDto, WheelDepartmentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-or-edit-time-slot-wheel-department-modal',
  templateUrl: './create-or-edit-time-slot-wheel-department-modal.component.html',
  styleUrls: ['./create-or-edit-time-slot-wheel-department-modal.component.css']
})
export class CreateOrEditTimeSlotWheelDepartmentModalComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  saving = false;
  active = false;
  timeFrom = new Date();
  timeTo = new Date();
  numberOfOrder: number = null;
  wheelDepartmentId: number;
  timeSlot: TimeSlotDto = new TimeSlotDto();

  constructor(
    injector: Injector,
    private _wheelDepartmentsServiceProxy: WheelDepartmentsServiceProxy,
    public datepipe: DatePipe

  ) {
    super(injector);
  }

  show(wheelDepartmentId, id?: any): void {
    this.active = true;
    this.wheelDepartmentId = wheelDepartmentId;
    if (id != null) {
      this._wheelDepartmentsServiceProxy.getTimeSlotById(id).subscribe(result => {
        this.timeSlot = result;
        this.timeFrom = this.formatDate(this.timeSlot.timeFrom);
        this.timeTo = this.formatDate(this.timeSlot.timeTo);
        this.numberOfOrder = this.timeSlot.numberOfOrder;
        this.modal.show();
      });
    }
    else {
      this.timeSlot = new TimeSlotDto();
      this.timeSlot.numberOfOrder = null;
      this.timeSlot.wheelDepartmentId = this.wheelDepartmentId;
      this.modal.show();
    }
  }

  save(): void {

    this.saving = true;
    this.timeSlot.timeFrom = this.datepipe.transform(this.timeFrom, 'HH:mm');
    this.timeSlot.timeTo = this.datepipe.transform(this.timeTo, 'HH:mm');
    this.timeSlot.numberOfOrder = this.numberOfOrder;
    this.timeSlot.wheelDepartmentId = this.wheelDepartmentId;
    this._wheelDepartmentsServiceProxy.createOrEditTimeSlot(this.timeSlot).subscribe(result => {
      this.modalSave.emit();
      this.close();
    })
  }

  formatDate(input: string) {
    var datetimeStr = '2020-10-10' + ' ' + input;
    return new Date(datetimeStr);
  }

  close(): void {
    this.active = false;
    this.saving = false;
    this.timeFrom = new Date();
    this.timeTo = new Date();
    this.numberOfOrder = 0;
    this.modal.hide();
  }
}

﻿import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
    WheelDepartmentsServiceProxy,
    WheelDepartmentDto,
    WheelOrderType,
    WheelScreenMenusServiceProxy,
    WheelServiceProxy,
    WheelOpeningHourServiceType,
    PriceTagsServiceProxy,
    PriceTagsForComboboxDto,
    WheelScreenMenuForComboboxItemDto,
    WheelOpeningHourForComboboxItemDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './wheelDepartments.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ["./wheelDepartments.component.scss"],
    animations: [appModuleAnimation()]
})
export class WheelDepartmentsComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    priceTagFilter = '';
    menuFilter = '';
    openingHoursFilter = '';
    enableFilter = -1;
    orderTypeFilter = -1;
    allPriceTags: PriceTagsForComboboxDto[];
    allScreenMenus: WheelScreenMenuForComboboxItemDto[];
    allOpeningHours: WheelOpeningHourForComboboxItemDto[];
    allOrderTypes: any[] = [];
    isShowFilterOptions = false;

    wheelOrderType = WheelOrderType;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _wheelDepartmentsServiceProxy: WheelDepartmentsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _wheelScreenMenusServiceProxy: WheelScreenMenusServiceProxy,
        private _wheelServiceProxy: WheelServiceProxy,
        private _priceTagsServiceProxy: PriceTagsServiceProxy,
        private _router: Router
    ) {
        super(injector);

        this.getDataForCombobox();
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelDepartments();
        });
    }

    ngOnDestroy(): void {
        this.filter$.unsubscribe();
    }

    getWheelDepartments(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelDepartmentsServiceProxy
            .getAll(
                this.filterText,
                this.nameFilter,
                this.orderTypeFilter == -1 ? undefined : this.orderTypeFilter,
                this.priceTagFilter,
                this.menuFilter,
                this.openingHoursFilter,
                this.enableFilter == -1 ? undefined : this.enableFilter == 1,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    getDataForCombobox() {
        this._wheelScreenMenusServiceProxy.getScreenMenuForCombobox(undefined).subscribe((result) => {
            this.allScreenMenus = result;
        });

        this._wheelServiceProxy
            .getOpeningHourForComboboxByServiceType(WheelOpeningHourServiceType.Default, undefined)
            .subscribe((result) => {
                this.allOpeningHours = result;
            });

        this._priceTagsServiceProxy.getPriceTagsForCombobox(undefined).subscribe((result) => {
            this.allPriceTags = result;
        });

        for (var enumMember in WheelOrderType) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.allOrderTypes.push({
                    displayText: WheelOrderType[enumMember],
                    value: parseInt(enumMember, 10)
                });
            }
        }
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createWheelDepartment(id?: number): void {
        this._router.navigate(['/app/wheel/departments/create-or-edit', id ? id : 'null']);
    }

    deleteWheelDepartment(wheelDepartment: WheelDepartmentDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._wheelDepartmentsServiceProxy.delete(wheelDepartment.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    exportToExcel(): void {
        this._wheelDepartmentsServiceProxy
            .getWheelDepartmentsToExcel(
                this.filterText,
                this.nameFilter,
                this.orderTypeFilter == -1 ? undefined : this.orderTypeFilter,
                this.priceTagFilter,
                this.menuFilter,
                this.openingHoursFilter,
                this.enableFilter == -1 ? undefined : this.enableFilter == 1
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.nameFilter = '';
        this.priceTagFilter = '';
        this.menuFilter = '';
        this.openingHoursFilter = '';
        this.enableFilter = -1;
        this.orderTypeFilter = -1;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }
}

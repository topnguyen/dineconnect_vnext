﻿import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    WheelDepartmentsServiceProxy,
    CreateOrEditWheelDepartmentDto,
    WheelPaymentMethodsServiceProxy,
    WheelServiceProxy,
    WheelOrderType,
    WheelScreenMenusServiceProxy,
    WheelOpeningHourServiceType,
    WheelScreenMenuForComboboxItemDto,
    WheelOpeningHourForComboboxItemDto,
    PriceTagsServiceProxy,
    PriceTagsForComboboxDto,
    WheelPaymentMethodForComboboxDto,
    GetWheelPaymentMethodForViewDto,
    DinePlanTaxesServiceProxy,
    GetDinePlanTaxForComboboxDto,
    TimeSlotDto,
    WheelServiceFeeDto,
    WheelServiceFeeType,
    DinePlanOpeningHoursDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute, Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { CreateOrEditTimeSlotWheelDepartmentModalComponent } from "./create-or-edit-time-slot-wheel-department-modal.component";

@Component({
    selector: "createOrEditWheelDepartmentModal",
    templateUrl: "./create-or-edit-wheelDepartment-modal.component.html",
    styleUrls: ["./wheelDepartments.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelDepartmentModalComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @ViewChild("createOrEditTimeSlot", { static: true })
    createOrEditTimeSlot: CreateOrEditTimeSlotWheelDepartmentModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    departmentName = "";
    priceTagName = "";
    active = false;
    saving = false;

    wheelDepartment: CreateOrEditWheelDepartmentDto =
        new CreateOrEditWheelDepartmentDto();
    wheelDepartmentId: number;
    allPriceTags: PriceTagsForComboboxDto[];
    allOrderTypes: any[] = [];
    selectedNewPaymentMethod: string;

    selectedDinePlanOpeningHours: string;
    selectedServiceFee: string;
    time = new Date();
    lstTimeSlot: TimeSlotDto[] = [];
    dateRange: any[] = [new Date(), new Date()];
    listDateSetting: any[] = [];
    allPaymentMethods: WheelPaymentMethodForComboboxDto[];
    allScreenMenus: WheelScreenMenuForComboboxItemDto[];
    allOpeningHours: WheelOpeningHourForComboboxItemDto[];
    allDinePlanTaxes: GetDinePlanTaxForComboboxDto[];
    allServiceFees: WheelServiceFeeDto[];

    selectedOpeningHour: string;
    selectedMenu: string;
    selectedPriceTag: string;
    isDisable = true;

    constructor(
        injector: Injector,
        private _wheelDepartmentsServiceProxy: WheelDepartmentsServiceProxy,
        private _wheelScreenMenusServiceProxy: WheelScreenMenusServiceProxy,
        private _serviceProxy: WheelServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _priceTagsServiceProxy: PriceTagsServiceProxy,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _dinePlanTaxesServiceProxy: DinePlanTaxesServiceProxy,
        private _router: Router,
        public datepipe: DatePipe
    ) {
        super(injector);
        this.getDataForCombobox();
    }

    ngOnInit() {
        this.wheelDepartment = new CreateOrEditWheelDepartmentDto();
        this.wheelDepartment.isEnableDelivery = true;

        this._activatedRoute.params.subscribe((params) => {
            this.wheelDepartmentId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.wheelDepartmentId)) {
                this.isDisable = false;
                this._wheelDepartmentsServiceProxy
                    .getWheelDepartmentForEdit(this.wheelDepartmentId)
                    .subscribe((result) => {
                        this.wheelDepartment = result;
                        this.wheelDepartment.orderDeliveryTime =
                            this.wheelDepartment.orderDeliveryTime != null
                                ? this.wheelDepartment.orderDeliveryTime
                                : 0;
                        this.wheelDepartment.orderPreparationTime =
                            this.wheelDepartment.orderPreparationTime != null
                                ? this.wheelDepartment.orderPreparationTime
                                : 0;
                        this.selectedMenu = result.menuId.toString();
                        this.selectedPriceTag = result.priceTagId?.toString();
                    });
                this.getAllTimeSlot();
            }
        });
    }

    getDataForCombobox() {
        this._wheelScreenMenusServiceProxy
            .getScreenMenuForCombobox(undefined)
            .subscribe((result) => {
                this.allScreenMenus = result;
            });

        this._serviceProxy
            .getOpeningHourForComboboxByServiceType(
                WheelOpeningHourServiceType.Default,
                undefined
            )
            .subscribe((result) => {
                this.allOpeningHours = result;
            });

        this._priceTagsServiceProxy
            .getPriceTagsForCombobox(undefined)
            .subscribe((result) => {
                this.allPriceTags = result;
            });

        this._wheelPaymentMethodsServiceProxy
            .getAllPaymentMethodForCombobox(undefined)
            .subscribe((result) => {
                this.allPaymentMethods = result;
            });

        this._serviceProxy.getAllWheelServiceFees().subscribe((result) => {
            this.allServiceFees = result;
        });

        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxForCombobox(undefined)
            .subscribe((result) => {
                this.allDinePlanTaxes = result;
            });

        for (var enumMember in WheelOrderType) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.allOrderTypes.push({
                    displayText: WheelOrderType[enumMember],
                    value: parseInt(enumMember, 10),
                });
            }
        }
    }

    resetData() {
        this.selectedNewPaymentMethod = null;
    }

    save(): void {
        this.saving = true;

        this.wheelDepartment.priceTagId = +this.selectedPriceTag;
        // this.wheelDepartment.openingHourId = +this.selectedOpeningHour;
        this.wheelDepartment.menuId = +this.selectedMenu;
        this._wheelDepartmentsServiceProxy
            .createOrEdit(this.wheelDepartment)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/wheel/departments"]);
    }

    addPaymentMethod() {
        if (!this.selectedNewPaymentMethod) return;
        if (!this.wheelDepartment.wheelPaymentMethods)
            this.wheelDepartment.wheelPaymentMethods = [];
        if (
            this.wheelDepartment.wheelPaymentMethods.findIndex(
                (f) => f.systemName == this.selectedNewPaymentMethod
            ) == -1
        ) {
            const data = new GetWheelPaymentMethodForViewDto();
            data.systemName = this.selectedNewPaymentMethod;
            data.friendlyName = this.allPaymentMethods.find(
                (x) => x.value == this.selectedNewPaymentMethod
            ).displayText;
            this.wheelDepartment.wheelPaymentMethods.push(data);
        }
    }

    removePaymentMethod(paymentMethod: GetWheelPaymentMethodForViewDto) {
        this.wheelDepartment.wheelPaymentMethods.remove(paymentMethod);
    }

    addServiceFee() {
        if (!this.selectedServiceFee) return;
        if (!this.wheelDepartment.wheelServiceFees)
            this.wheelDepartment.wheelServiceFees = [];
        if (
            this.wheelDepartment.wheelServiceFees.findIndex(
                (f) => f.id == +this.selectedServiceFee
            ) == -1
        ) {
            const data = new WheelServiceFeeDto();
            data.id = +this.selectedServiceFee;
            data.init(
                this.allServiceFees.find(
                    (x) => x.id == +this.selectedServiceFee
                )
            );
            this.wheelDepartment.wheelServiceFees.push(data);
        }
    }

    removeServiceFee(serviceFee: WheelServiceFeeDto) {
        this.wheelDepartment.wheelServiceFees.remove(serviceFee);
    }

    addDinePlanOpeningHours() {
        if (!this.selectedDinePlanOpeningHours) return;
        if (!this.wheelDepartment.wheelOpeningHours)
            this.wheelDepartment.wheelOpeningHours = [];
        if (
            this.wheelDepartment.wheelOpeningHours.findIndex(
                (f) => f.id == +this.selectedDinePlanOpeningHours
            ) == -1
        ) {
            const data = new DinePlanOpeningHoursDto();
            data.id = +this.selectedDinePlanOpeningHours;
            data.name = this.allOpeningHours.find(
                (x) => x.value == this.selectedDinePlanOpeningHours
            ).displayText;
            this.wheelDepartment.wheelOpeningHours.push(data);
        }
        this.updateOpeningHours();
    }

    removeDinePlanOpeningHours(dinePlanOpeningHours: DinePlanOpeningHoursDto) {
        this.wheelDepartment.wheelOpeningHours.remove(dinePlanOpeningHours);
        this.updateOpeningHours();
    }

    addDateSetting() {
        console.log(this.dateRange);
    }

    addEditTimeSlot(id?: number) {
        this.createOrEditTimeSlot.show(this.wheelDepartmentId, id);
    }

    deleteTimsSlot(id: number) {
        this._wheelDepartmentsServiceProxy
            .deleteTimeSlot(id)
            .subscribe((result) => {
                this.getAllTimeSlot();
                this.notify.success(this.l("Delete time slot"));
            });
    }

    getAllTimeSlot() {
        this._wheelDepartmentsServiceProxy
            .getTimeSlotByDepartmentId(this.wheelDepartmentId)
            .subscribe((result) => {
                this.lstTimeSlot = result;
            });
    }

    getValueDisplayString(fee): string {
        var result: string = `${fee.value}${
            fee.feeType == WheelServiceFeeType.Percentage ? "%" : "$"
        }`;
        return result;
    }

    updateOpeningHours() {
        this._wheelDepartmentsServiceProxy
            .updateOnlyOpeningHours(this.wheelDepartment)
            .subscribe(() => {
                this.notify.success(this.l("DeletedSuccessfully"));
            });
    }
}

import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditWheelSetupDto, WheelCustomizationSettingsServiceProxy, WheelDynamicPageForComboboxItemDto, WheelDynamicPagesServiceProxy, WheelLocationDto, WheelLocationsServiceProxy, WheelPostCodeServiceType, WheelSetupsServiceProxy, WheelThemeForComboboxItemDto } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';
import { countries } from '@shared/common/country/country-data-store';

@Component({
  selector: 'app-create-or-edit-wheel-setup',
  templateUrl: './create-or-edit-wheel-setup.component.html',
  styleUrls: ['./create-or-edit-wheel-setup.component.scss'],
  animations: [appModuleAnimation()],
})
export class CreateOrEditWheelSetupComponent  extends AppComponentBase {

  wheelSetup: CreateOrEditWheelSetupDto = new CreateOrEditWheelSetupDto();
    allLocations: WheelLocationDto[];
    selectedLocation: WheelLocationDto;

    saving: boolean = false;

    logoCompanyUploader: FileUploader;
    welcomeLoadingUploader: FileUploader;
    advertisement1Uploader: FileUploader;
    advertisement2Uploader: FileUploader;

    logoCompanyUrl: string;

    allDynamicPages: WheelDynamicPageForComboboxItemDto[];
    allThemes: WheelThemeForComboboxItemDto[];
    allowEditLogo: boolean = true;
    allowEditWelcome: boolean = true;
    allowEditAdvert1: boolean = true;
    allowEditAdvert2: boolean = true;
    wheelSetupId: number= null;

    postalServices: Array<{ label: string, value: number }> = [
        {
            label: WheelPostCodeServiceType[WheelPostCodeServiceType.GeoApi],
            value: WheelPostCodeServiceType.GeoApi
        },
        {
            label: WheelPostCodeServiceType[WheelPostCodeServiceType.OneMap],
            value: WheelPostCodeServiceType.OneMap
        }
    ]
    public countries:any = countries;

    constructor(
        injector: Injector,
        private _wheelSetupsServiceProxy: WheelSetupsServiceProxy,
        private _tokenService: TokenService,
        private _wheelDynamicPagesServiceProxy: WheelDynamicPagesServiceProxy,
        private _wheelLocationServiceProxy: WheelLocationsServiceProxy,
        private _wheelCustomizationSettingsServiceProxy: WheelCustomizationSettingsServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
    ) {
        super(injector);
        this.initCombobox();
        this.initUploader();
        this._activatedRoute.params.subscribe((params) => {
          this.wheelSetupId = +params["id"]; // (+) converts string 'id' to a number
          if (!isNaN(this.wheelSetupId)) {
              this.getWheelSetup(this.wheelSetupId);
          }
      });
        
    }
    getWheelSetup(id): void {
        this.spinnerService.show();
        this._wheelSetupsServiceProxy
            .getWheelSetupForEdit("",id)
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe((result) => {
                this.checkAllowEdit(result.wheelSetup);
                this.wheelSetup =
                    result.wheelSetup || new CreateOrEditWheelSetupDto();
            });
    }

    initUploader(): void {
        this.logoCompanyUploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                this.wheelSetup.logoCompanyUrl = result.url;
                console.log('logoCompanyUploader', result)
            }
        );
        this.welcomeLoadingUploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                console.log('welcomeLoadingUploader', result)
                this.wheelSetup.welcomeUrl = result.url;
            }
        );
        this.advertisement1Uploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                console.log('advertisement1Uploader', result)
                this.wheelSetup.advertOneUrl = result.url;
            }
        );
        this.advertisement2Uploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                console.log('advertisement2Uploader', result)
                this.wheelSetup.advertTwoUrl = result.url;
            }
        );
    }

    initCombobox(): void {
        this._wheelDynamicPagesServiceProxy
            .getDynamicPageForCombobox(undefined)
            .subscribe((result) => {
                this.allDynamicPages = result;
            });

        this._wheelLocationServiceProxy
            .getAllWheelLocations()
            .subscribe((result) => {
                this.allLocations = result;
            });
    }

    save(): void {
        this.saving = true;

        this._wheelSetupsServiceProxy
            .createOrEdit(this.wheelSetup)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.checkAllowEdit(this.wheelSetup);
                this.close();
            });
    }

    addLocation() {
        if (!this.selectedLocation) return;

        if (!this.wheelSetup.wheelLocations)
            this.wheelSetup.wheelLocations = [];

        if (
            this.wheelSetup.wheelLocations.findIndex(
                (f) => f.id == this.selectedLocation.id
            ) == -1
        ) {
            this.wheelSetup.wheelLocations.push(this.selectedLocation);
        }
    }

    removeLocation(location: WheelLocationDto) {
        this.wheelSetup.wheelLocations.remove(location);
    }

    createUploader(url: string, success?: (result: any) => void): FileUploader {
        const uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + url,
        });

        uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l("SavedSuccessfully"));
                if (success) {
                    success(ajaxResponse.result);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.authToken = "Bearer " + this._tokenService.getToken();
        uploaderOptions.removeAfterUpload = true;
        uploader.setOptions(uploaderOptions);
        return uploader;
    }

    uploadLogoCompany(loaderName): void {
        this[loaderName].uploadAll();
    }

    clearLogo(urlName: string, loaderName: string): void {
        this.wheelSetup[urlName] = '';
        this[loaderName].clearQueue();
    }

    cancel(): void {
       this.close();
    }

    private checkAllowEdit(wheelSetup): void {

        this.allowEditLogo = wheelSetup.logoCompanyUrl ? false
            : true;
        this.allowEditWelcome = wheelSetup.welcomeUrl ? false
            : true;
        this.allowEditAdvert1 = wheelSetup.advertOneUrl ? false
            : true;
        this.allowEditAdvert2 = wheelSetup.advertTwoUrl ? false
            : true;
    }

    close(): void {
      this._router.navigate(["/app/wheel/setup"]);
  }
}

﻿import {
    Component,
    Injector,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import {
    WheelSetupsServiceProxy,
    CreateOrEditWheelSetupDto,
    WheelLocationDto,
    WheelDynamicPageForComboboxItemDto,
    
    WheelThemeForComboboxItemDto,
    WheelPostCodeServiceType,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import * as _ from "lodash";
import { FileUploader } from "ng2-file-upload";
import { Router } from "@angular/router";
import { LazyLoadEvent, Paginator, Table } from "primeng";

@Component({
    templateUrl: "./wheelSetups.component.html",
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class WheelSetupsComponent extends AppComponentBase {

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    
    wheelSetup: CreateOrEditWheelSetupDto = new CreateOrEditWheelSetupDto();
    allLocations: WheelLocationDto[];
    selectedLocation: WheelLocationDto;

    saving: boolean = false;

    logoCompanyUploader: FileUploader;
    welcomeLoadingUploader: FileUploader;
    advertisement1Uploader: FileUploader;
    advertisement2Uploader: FileUploader;

    logoCompanyUrl: string;

    allDynamicPages: WheelDynamicPageForComboboxItemDto[];
    allThemes: WheelThemeForComboboxItemDto[];
    allowEditLogo: boolean = true;
    allowEditWelcome: boolean = true;
    allowEditAdvert1: boolean = true;
    allowEditAdvert2: boolean = true;

    postalServices: Array<{ label: string, value: number }> = [
        {
            label: WheelPostCodeServiceType[WheelPostCodeServiceType.GeoApi],
            value: WheelPostCodeServiceType.GeoApi
        },
        {
            label: WheelPostCodeServiceType[WheelPostCodeServiceType.OneMap],
            value: WheelPostCodeServiceType.OneMap
        }
    ]

    constructor(
        injector: Injector,
        private _wheelSetupsServiceProxy: WheelSetupsServiceProxy,
        private _router: Router,
    ) {
        super(injector);
    }

   

    createOrEditWheelSetup(id?: number): void {
        this._router.navigate(['/app/wheel/setup/create-or-edit', id ? id : 'null']);
    }
    getWheelSetups(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelSetupsServiceProxy.getAll(
            null, //Filter
            null, //HomePageFilter
            null, //CheckOutPageFilter
            null, //ThemeFilter
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            console.log("this.primengTableHelper.records",this.primengTableHelper.records);
        });
    }
    deleteWheelSetup(id?: number){
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wheelSetupsServiceProxy.delete(id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

}

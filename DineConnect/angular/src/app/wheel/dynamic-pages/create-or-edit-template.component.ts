import { Component, OnInit, OnDestroy, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    IpEmailBuilderService,
    Structure,
    TextBlock,
    ImageBlock,
    IPEmail,
} from "ip-email-builder";
import { exhaustMap, takeUntil } from "rxjs/operators";
import { Subject, BehaviorSubject } from "rxjs";
import {
    WheelSetupsServiceProxy,
    WheelDynamicPagesServiceProxy,
    CreateOrEditWheelDynamicPagesInput,
} from "@shared/service-proxies/service-proxies";
import grapesjs from "grapesjs";
import { AppComponentBase } from "@shared/common/app-component-base";
import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min";

import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { AppConsts } from "@shared/AppConsts";
import { Router, ActivatedRoute } from "@angular/router";
import { TokenService, IAjaxResponse } from "abp-ng2-module";

@Component({
    templateUrl: "./create-or-edit-template.component.html",
    styleUrls: ["./create-or-edit-template.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditTemplateComponent
    extends AppComponentBase
    implements OnInit, OnDestroy {
    editor: any;
    dynamicPage: CreateOrEditWheelDynamicPagesInput;
    dynamicPageId: number;
    saving: boolean = false;
    private uploader: FileUploader;

    constructor(
        _injector: Injector,
        private _wheelSetupService: WheelSetupsServiceProxy,
        private _wheelDynamicPagesService: WheelDynamicPagesServiceProxy,
        private _tokenService: TokenService,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(_injector);
    }

    ngOnInit() {
        this.dynamicPage = new CreateOrEditWheelDynamicPagesInput();

        this._activatedRoute.params.subscribe((params) => {
            this.dynamicPageId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.dynamicPageId)) {
                this._wheelDynamicPagesService
                    .getWheelDynamicPageForEdit(this.dynamicPageId)
                    .subscribe((result) => {
                        this.dynamicPage = result;
                        this.editor.setComponents(
                            JSON.parse(this.dynamicPage.components || "")
                        );
                        this.editor.setStyle(
                            JSON.parse(this.dynamicPage.styles || "")
                        );
                        this.editor.AssetManager.add(
                            JSON.parse(this.dynamicPage.assets || "")
                        );
                    });
            }
        });

        this.initUploader();
        this.initEditor();
        this.setUpSaveTemplate();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImage",
            authToken: "Bearer " + this._tokenService.getToken(),
        });

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.removeAfterUpload = true;
        uploaderOptions.autoUpload = true;
        this.uploader.setOptions(uploaderOptions);

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            this.spinnerService.hide();
            if (ajaxResponse.success) {
                this.editor.AssetManager.add(ajaxResponse.result.url);
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };
        this.uploader.onErrorItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);

            this.spinnerService.hide();
            this.notify.error(ajaxResponse.error.message);
        };
    }

    initEditor(): void {
        this.editor = grapesjs.init({
            showOffsets: 1,
            noticeOnUnload: false,
            container: "#gjs",
            fromElement: true,
            // storageManager: { autoload: 0 },
            storeComponents: true,
            storeStyles: true,
            storeHtml: true,
            storeCss: true,
            components: this.dynamicPage.components,
            plugins: ["gjs-preset-webpage"],
            pluginsOpts: {
                "gjs-preset-webpage": {
                    navbarOpts: false,
                    countdownOpts: false,
                    formsOpts: false,
                    blocksBasicOpts: {
                        blocks: [
                            "link-block",
                            "quote",
                            "image",
                            "text",
                            "column1",
                            "column2",
                            "column3",
                        ],
                        flexGrid: false,
                        stylePrefix: "lala-",
                    },
                },
            },
            assetManager: {
                uploadText: "Add image through link or upload image",
                modalTitle: "Select Image",
                openAssetsOnDrop: 1,
                inputPlaceholder: "",
                addBtnText: "Add image",
                uploadFile: (e) => {
                    this.spinnerService.show();
                    const file = e.dataTransfer
                        ? e.dataTransfer.files[0]
                        : e.target.files[0];
                    this.uploader.addToQueue([file]);
                },
                handleAdd: (textFromInput) => {
                    this.editor.AssetManager.add(textFromInput);
                },
            },
            storageManager: {
                autosave: false,
                setStepsBeforeSave: 1,
                //make it true for auto load
                autoload: false,
                //  type: 'remote',
                //  urlStore: 'http://localhost:3000/templateSave'+name,
                //  urlLoad: 'http://localhost:3000/getPath/'+this.TEMPID,
                type: "local",
                urlStore: "",
                urlLoad: "",
                contentTypeJson: true,
            },
        });
    }

    ngOnDestroy() { }

    setUpSaveTemplate() {
        this.editor.on("storage:store", (e) => {
            this.dynamicPage.css = e.css;
            this.dynamicPage.assets = e.assets;
            this.dynamicPage.styles = e.styles;
            this.dynamicPage.components = e.components;
            this.dynamicPage.html = e.html;

            this._wheelDynamicPagesService
                .createOrUpdateWheelDynamicPage(this.dynamicPage)
                .subscribe(() => {
                    this.saving = false;
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        });
    }

    close(): void {
        this._router.navigate(["/app/wheel/dynamic-pages"]);
    }

    save() {
        this.saving = true;
        this.editor.store();
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { debounceTime, finalize } from 'rxjs/operators';
import { WheelDynamicPagesListDto, WheelDynamicPagesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { fromEvent, Subject } from 'rxjs';

@Component({
    templateUrl: './templates.component.html',
    styleUrls: ['./templates.component.scss'],
    animations: [appModuleAnimation()]
})
export class TemplatesComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    wheelDynamicPages: WheelDynamicPagesListDto[] = [];
    filter: string = '';
    filter$ = new Subject();

    constructor(injector: Injector, private _router: Router, private wheelDynamicPagesService: WheelDynamicPagesServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000)).subscribe((data) => {
            console.log(data);
            this.getWheelDynamicPages();
        });
    }

    search(event: KeyboardEvent) {
        this.filter$.next(event);
    }

    getWheelDynamicPages(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this.wheelDynamicPagesService
            .getWheelDynamicPages(
                this.filter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEdit(id?: number) {
        this._router.navigate(['/app/wheel/dynamic-pages/create-or-edit', id ? id : 'null']);
    }

    deleteTenantDatabase(model: WheelDynamicPagesListDto) {
        this.message.confirm(this.l('TenantDatabaseDeleteWarningMessage', model.name), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this.wheelDynamicPagesService.deleteWheelDynamicPage(model.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }
}

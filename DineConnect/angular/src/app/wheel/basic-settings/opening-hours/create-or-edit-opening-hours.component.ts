import { Component, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { OpeningHour } from "./opening-hour";
import { WheelOpeningHourServiceType, WheelServiceProxy } from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { finalize } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: "create-or-edit-opening-hours",
    templateUrl: "./create-or-edit-opening-hours.component.html",
    styleUrls: ['./create-or-edit-opening-hours.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditOpeningHoursComponent extends AppComponentBase {

    openingHour: OpeningHour = new OpeningHour(WheelOpeningHourServiceType.Default);
    openingHourId: number;
    saving: boolean = false;

    constructor(injector: Injector,
        private _serviceProxy: WheelServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router) {
        super(injector);

        this._activatedRoute.params.subscribe((params) => {
            this.openingHourId = +params["id"];

            if (!isNaN(this.openingHourId)) {
                this._serviceProxy.getOpeningHour(this.openingHourId)
                    .subscribe(result => {
                        this.openingHour = OpeningHour.fromDto(result);
                    })
            }
        });
    }

    save() {
        this.saving = true;
        this._serviceProxy.createOrEditOpeningHour(this.openingHour.toDto())
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.cancel();
            });
    }

    cancel() {
        this._router.navigate(['/app/wheel/basic-settings/opening-hours']);
    }
}

import {
    WheelServiceProxy,
    WheelOpeningHourDto,
    WheelOpeningHourServiceType,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";

export class OpeningHour {
    id: number = 0;
    name: string;
    from: string;
    to: string;
    days: Day[];
    isEditMode: boolean;
    serviceType: WheelOpeningHourServiceType;

    constructor(serviceType: WheelOpeningHourServiceType) {
        this.initDefaultValues();
        this.isEditMode = false;
        this.serviceType = serviceType;
    }

    initDefaultValues(): void {
        this.days = [];
        this.days.push(new Day("Monday"));
        this.days.push(new Day("Tuesday"));
        this.days.push(new Day("Wenesday"));
        this.days.push(new Day("Thurday"));
        this.days.push(new Day("Friday"));
        this.days.push(new Day("Saturday"));
        this.days.push(new Day("Sunday"));
    }

    getSelectedDays() {
        var result: string = "";
        for (var i = 0; i < this.days.length; i++) {
            if (this.days[i].isSelected) result += this.days[i].name + ", ";
        }

        result = result.trim();
        if (result.endsWith(","))
            result = result.substring(0, result.length - 1);

        return result;
    }

    isSelected(day: number) {
        return this.days[day].isSelected;
    }

    revert(day: number) {
        this.days[day].revert();
    }

    isNew(): boolean {
        return this.id == 0;
    }

    getSelectedDaysAsNumber(): number {
        var n: number = 0;
        for (var i = 0; i < this.days.length; i++) {
            if (this.days[i].isSelected) {
                var mask = 1 << i;
                n |= mask;
            }
        }

        return n;
    }

    toDto(): WheelOpeningHourDto {
        var openingHourDto = new WheelOpeningHourDto();

        openingHourDto.id = this.id;
        openingHourDto.from = this.from;
        openingHourDto.to = this.to;
        openingHourDto.serviceType = this.serviceType;
        openingHourDto.openDays = this.getSelectedDaysAsNumber();
        openingHourDto.name = this.name;
        return openingHourDto;
    }

    static fromDto(openingHourDto: WheelOpeningHourDto): OpeningHour {
        var openingHour = new OpeningHour(openingHourDto.serviceType);
        openingHour.id = openingHourDto.id;
        openingHour.from = openingHourDto.from;
        openingHour.to = openingHourDto.to;
        openingHour.name = openingHourDto.name;
        for (var i = 0; i < openingHour.days.length; i++) {
            var mask = 1 << i;
            openingHour.days[i].isSelected =
                (openingHourDto.openDays & mask) != 0;
        }

        return openingHour;
    }
}

export class Day {
    name: string;
    isSelected: boolean;

    constructor(name: string) {
        this.name = name;
        this.isSelected = true;
    }

    revert() {
        this.isSelected = !this.isSelected;
    }
}

export class OpeningHourService {
    name: string;
    isDefault: boolean;
    sameWithDefault: boolean;
    openingHours: OpeningHour[];
    newOpeningHour: OpeningHour;
    canAddNew: boolean;
    serviceProxy: WheelServiceProxy;
    serviceType: WheelOpeningHourServiceType;

    constructor(
        name: string,
        serviceType: WheelOpeningHourServiceType,
        serviceProxy: WheelServiceProxy
    ) {
        this.name = name;
        this.openingHours = [];
        this.sameWithDefault = true;
        this.canAddNew = true;
        this.serviceType = serviceType;
        this.serviceProxy = serviceProxy;
        this.newOpeningHour = new OpeningHour(serviceType);
    }

    addNewOpeningHour() {
        this.canAddNew = false;
        this.newOpeningHour = new OpeningHour(this.serviceType);
    }

    addOpeningHour(openingHour: OpeningHour) {
        this.openingHours.push(openingHour);
        this.sameWithDefault = false;
        this.canAddNew = true;
    }

    save(openingHour: OpeningHour) {
        this.canAddNew = true;
        return this.serviceProxy
            .createOrEditOpeningHour(openingHour.toDto())
            .subscribe(() => {});
    }

    delete(openingHour: OpeningHour) {
        this.openingHours.remove(openingHour);
        this.serviceProxy.deleteOpeningHour(openingHour.id).subscribe();
    }

    cancel(openingHour: OpeningHour) {
        this.canAddNew = true;
        if (openingHour.isNew()) {
            this.newOpeningHour = null;
        } else {
            openingHour.isEditMode = false;
        }
    }

    changeSameWithDefaultOption() {
        this.sameWithDefault = !this.sameWithDefault;
        if (this.sameWithDefault) {
            if (this.openingHours.length > 0) {
                this.serviceProxy
                    .deleteOpeningHoursByServiceType(this.serviceType)
                    .subscribe();
            }

            this.openingHours = [];
            this.canAddNew = true;
            this.newOpeningHour = new OpeningHour(this.serviceType);
        }
    }
}

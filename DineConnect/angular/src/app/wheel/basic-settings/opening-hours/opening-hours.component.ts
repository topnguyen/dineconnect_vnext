import { Component, Injector, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { OpeningHour } from "./opening-hour";
import {
    WheelServiceProxy,
    WheelOpeningHourServiceType,
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent } from "primeng/public_api";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { debounceTime, finalize, takeUntil } from "rxjs/operators";
import { Router } from "@angular/router";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Subject } from "rxjs";

@Component({
    selector: "app-oc-hours",
    templateUrl: "./opening-hours.component.html",
    styleUrls: ["./opening-hours.component.scss"],
    animations: [appModuleAnimation()],
})
export class OpeningHoursComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText: string = "";

    
    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _serviceProxy: WheelServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    
    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getOpeningHours();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getOpeningHours(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._serviceProxy
            .getAllOpeningHours(
                this.filterText,
                WheelOpeningHourServiceType.Default,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map((x) =>
                    OpeningHour.fromDto(x)
                );
            });
    }

    createOrEdit(id?: number) {
        this._router.navigate([
            "/app/wheel/basic-settings/opening-hours/create-or-edit",
            id ? id : "null",
        ]);
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    delete(data: OpeningHour) {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._serviceProxy.deleteOpeningHour(data.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l("SuccessfullyDeleted"));
                });
            }
        });
    }

    exportToExcel() {
        this._serviceProxy.getWheelOpeningHoursToExcel(this.filterText, WheelOpeningHourServiceType.Default)
        .subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

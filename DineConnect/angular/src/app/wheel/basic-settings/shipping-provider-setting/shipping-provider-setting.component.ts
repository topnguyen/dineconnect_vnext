import { DatePipe } from "@angular/common";
import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FormlyFieldConfig, FormlyFormOptions } from "@ngx-formly/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppConsts } from "@shared/AppConsts";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    ComboboxItemDto,
    ShippingProvider,
    ShippingProviderDto,
    TenantSettingsEditDto,
    TenantSettingsServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-shipping-provider-setting",
    templateUrl: "./shipping-provider-setting.component.html",
    styleUrls: ["./shipping-provider-setting.component.css"],
    animations: [appModuleAnimation()],
})
export class ShippingProviderSettingComponent
    extends AppComponentBase
    implements OnInit {
    form = new FormGroup({});
    providerType = 1;
    settings: TenantSettingsEditDto = new TenantSettingsEditDto();
    loading = false;
    saving = false;
    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl;

    shippingProviderTypes: ComboboxItemDto[] = [];
    environmentTypes: ComboboxItemDto[] = [];
    ShippingProvider = ShippingProvider;
    // environmentType: string = "DEV";
    webhookUrlLaLaMove: string = `${this.remoteServiceBaseUrl}/api/services/app/WheelTicketShippingOrder/LalaMoveWebhook?tenantId=${this.appSession.tenantId}`
    webhookUrlGoGoVan: string = `${this.remoteServiceBaseUrl}/api/services/app/WheelTicketShippingOrder/GoGoVanWebhook?tenantId=${this.appSession.tenantId}`
    webhookUrlDunzo: string = `${this.remoteServiceBaseUrl}/api/services/app/WheelTicketShippingOrder/DunzoWebhook?tenantId=${this.appSession.tenantId}`
    
    shippingGoGoVanSetting: any = {
        apiIdGoGoVan: "",
        apiSecretKeyGoGoVan: "",
        environmentTypes:""
    };
    
    shippingLaLaMoveSetting: any = {
        apiIdLaLaMove: "",
        apiSecretKeyLaLaMove: "",
        environmentTypes:"DEV"
    };

    shippingDunzoSetting: any = {
        apiIdDunzo: "",
        apiSecretKeyDunzo: "",
        environmentTypes:"DEV"
    };

    shippingGoGoVanConfig = [
        {
            fieldGroupClassName: "row",
            fieldGroup: [
                {
                    className: "col-6",
                    key: "apiIdGoGoVan",
                    type: "input",
                    templateOptions: {
                        label: "API Key",
                        placeholder: "Enter API Key",
                    },
                },
                {
                    className: "col-6",
                    key: "apiSecretKeyGoGoVan",
                    type: "input",
                    templateOptions: {
                        label: "Api Secret Key",
                        placeholder: "Enter Secret Key",
                    },
                },

            ],
        },
    ];

    shippingLaLaMoveConfig = [
        {
            fieldGroupClassName: "row",
            fieldGroup: [
                {
                    className: "col-6",
                    key: "apiIdLaLaMove",
                    type: "input",
                    templateOptions: {
                        label: "API Key",
                        placeholder: "Enter API Key",
                    },
                },
                {
                    className: "col-6",
                    key: "apiSecretKeyLaLaMove",
                    type: "input",
                    templateOptions: {
                        label: "Api Secret Key",
                        placeholder: "Enter Api Secret Key",
                    },
                },

            ],
        },
    ];

    shippingDunzoConfig = [
        {
            fieldGroupClassName: "row",
            fieldGroup: [
                {
                    className: "col-6",
                    key: "apiIdDunzo",
                    type: "input",
                    templateOptions: {
                        label: "Client ID",
                        placeholder: "Enter Client ID",
                    },
                },
                {
                    className: "col-6",
                    key: "apiSecretKeyDunzo",
                    type: "input",
                    templateOptions: {
                        label: "Client Secret",
                        placeholder: "Enter Client Secret",
                    },
                },

            ],
        },
    ];

    constructor(
        injector: Injector,
        private _tenantSettingsService: TenantSettingsServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getSettings();
        this.getShippingProviderTypes();
        this.getEnvironmentTypes();
    }

    getSettings(): void {
        this.loading = true;
        this._tenantSettingsService
            .getAllSettings()
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result: TenantSettingsEditDto) => {
                this.settings = result;
                if (!this.settings.shippingProvider) {
                    this.settings.shippingProvider = new ShippingProviderDto();
                }
                this.bindingShippingGoGoVanSetting();
                this.bindingShippingLaLaMoveSetting();
                this.bindingShippingDunzoSetting();

                this.providerType =
                    this.settings.shippingProvider.shippingProvider;
            });
    }

    saveAll(): void {
        if (!this.form.valid) return;

        this.saving = true;

        this.settings.shippingProvider.shippingGoGoVanSetting = JSON.stringify(
            this.shippingGoGoVanSetting
        );
        
        this.settings.shippingProvider.shippingLaLaMoveSetting = JSON.stringify(
            this.shippingLaLaMoveSetting
        );

        this.settings.shippingProvider.shippingDunzoSetting = JSON.stringify(
            this.shippingDunzoSetting
        );

        this.settings.shippingProvider.shippingProvider = this.providerType;
        this._tenantSettingsService
            .updateAllSettings(this.settings)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
            });
    }

    getShippingProviderTypes() {
        this.shippingProviderTypes = [
            ComboboxItemDto.fromJS({
                value: ShippingProvider.None,
                displayText: this.l("None"),
            }),
            ComboboxItemDto.fromJS({
                value: ShippingProvider.GoGoVan,
                displayText: this.l("GoGoVan"),
            }),
            ComboboxItemDto.fromJS({
                value: ShippingProvider.LalaMove,
                displayText: this.l("LalaMove"),
            }),
            ComboboxItemDto.fromJS({
                value: ShippingProvider.Dunzo,
                displayText: this.l("Dunzo"),
            }),
        ];
    }

    getEnvironmentTypes() {
        this.environmentTypes = [
            ComboboxItemDto.fromJS({
                value: "DEV",
                displayText: this.l("Development"),
            }),
            ComboboxItemDto.fromJS({
                value: "PROD",
                displayText: this.l("Production"),
            }),

        ];
    }

    bindingShippingGoGoVanSetting() {
        var object = this.settings.shippingProvider
        .shippingGoGoVanSetting
        ? JSON.parse(this.settings.shippingProvider.shippingGoGoVanSetting)
        : undefined;

        if (object) {
            this.shippingGoGoVanSetting = {
                apiIdGoGoVan: object.apiIdGoGoVan,
                apiSecretKeyGoGoVan: object.apiSecretKeyGoGoVan,
                environmentTypes: object.environmentTypes ? object.environmentTypes:"DEV"
            };
            
        }
    }

    bindingShippingLaLaMoveSetting() {
        var object = this.settings.shippingProvider
        .shippingLaLaMoveSetting
        ? JSON.parse(this.settings.shippingProvider.shippingLaLaMoveSetting)
        : undefined;

        if (object) {
            this.shippingLaLaMoveSetting = {
                apiIdLaLaMove: object.apiIdLaLaMove,
                apiSecretKeyLaLaMove: object.apiSecretKeyLaLaMove,
                environmentTypes: object.environmentTypes ? object.environmentTypes:"DEV"
            };
            
        }
    }

    bindingShippingDunzoSetting() {
        var object = this.settings.shippingProvider
        .shippingDunzoSetting
        ? JSON.parse(this.settings.shippingProvider.shippingDunzoSetting)
        : undefined;

        if (object) {
            this.shippingDunzoSetting = {
                apiIdDunzo: object.apiIdDunzo,
                apiSecretKeyDunzo: object.apiSecretKeyDunzo,
                environmentTypes: object.environmentTypes ? object.environmentTypes:"DEV"
            };
        }
    }
}

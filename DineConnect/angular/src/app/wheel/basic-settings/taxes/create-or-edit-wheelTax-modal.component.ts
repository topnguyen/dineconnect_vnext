﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { WheelTaxesServiceProxy, CreateOrEditWheelTaxDto, DinePlanTaxLookupDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditWheelTaxModal',
    templateUrl: './create-or-edit-wheelTax-modal.component.html'
})
export class CreateOrEditWheelTaxModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    wheelTax: CreateOrEditWheelTaxDto = new CreateOrEditWheelTaxDto();
    wheelTaxName = '';
    allWheelTaxs: DinePlanTaxLookupDto[];

    constructor(
        injector: Injector,
        private _wheelTaxesServiceProxy: WheelTaxesServiceProxy) {
        super(injector);
    }

    show(wheelTaxId?: number): void {
        if (!wheelTaxId) {
            this.wheelTax = new CreateOrEditWheelTaxDto();
            this.wheelTax.id = wheelTaxId;
            this.wheelTaxName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._wheelTaxesServiceProxy.getWheelTaxForEdit(wheelTaxId).subscribe(result => {
                this.wheelTax = result.wheelTax;

                this.wheelTaxName = result.wheelTaxName;

                this.active = true;
                this.modal.show();
            });
        }

        this._wheelTaxesServiceProxy.getAllDinePlanTaxForLookupTable().subscribe(result => {
            this.allWheelTaxs = result;
        });
    }

    save(): void {
        this.saving = true;


        this._wheelTaxesServiceProxy.createOrEdit(this.wheelTax)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

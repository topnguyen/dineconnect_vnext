﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { WheelTaxesServiceProxy, WheelTaxDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditWheelTaxModalComponent } from './create-or-edit-wheelTax-modal.component';

import { ViewWheelTaxModalComponent } from './view-wheelTax-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './wheelTaxes.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./wheelTaxes.component.scss'],
    animations: [appModuleAnimation()]
})
export class WheelTaxesComponent extends AppComponentBase {

    @ViewChild('createOrEditWheelTaxModal', { static: true }) createOrEditWheelTaxModal: CreateOrEditWheelTaxModalComponent;
    @ViewChild('viewWheelTaxModalComponent', { static: true }) viewWheelTaxModal: ViewWheelTaxModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    nameFilter = '';
    wheelTaxNameFilter = '';
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _wheelTaxesServiceProxy: WheelTaxesServiceProxy,
        private _fileDownloadService: FileDownloadService) {
        
            super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelTaxes();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }


    getWheelTaxes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelTaxesServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.wheelTaxNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createWheelTax(): void {
        this.createOrEditWheelTaxModal.show();
    }

    deleteWheelTax(wheelTax: WheelTaxDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wheelTaxesServiceProxy.delete(wheelTax.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._wheelTaxesServiceProxy.getWheelTaxesToExcel(
            this.filterText,
            this.nameFilter,
            this.wheelTaxNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.nameFilter = '';
        this.wheelTaxNameFilter = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }
}

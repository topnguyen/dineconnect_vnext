import { Component, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ServiceFee } from "./service-fee";
import {
    WheelServiceProxy,
    WheelTaxesServiceProxy,
} from "@shared/service-proxies/service-proxies";

@Component({
    selector: "app-service-fees",
    templateUrl: "./service-fees.component.html",
    styleUrls: ["./service-fees.component.scss"],
    animations: [appModuleAnimation()],
})
export class ServiceFeesComponent extends AppComponentBase {
    newServiceFee: ServiceFee;
    serviceFees: ServiceFee[];
    canAddNew: boolean;
    wheelTaxes = [];

    constructor(
        injector: Injector,
        private _serviceProxy: WheelServiceProxy,
        private _taxesServiceProxy: WheelTaxesServiceProxy
    ) {
        super(injector);
        this.serviceFees = [];
        this.canAddNew = true;
        this.loadFees();
    }

    addNewServiceFee() {
        this.newServiceFee = new ServiceFee();
        this.canAddNew = false;
    }

    save(fee: ServiceFee) {
        console.log(fee);
        if (fee.isNew()) {
            this.serviceFees.push(fee);
            this.newServiceFee = null;

            this._serviceProxy
                .addWheelServiceFee(fee.toDto())
                .subscribe((result) => {
                    fee.id = result.id;
                });
        } else {
            fee.isEditMode = false;
            this._serviceProxy.updateWheelServiceFee(fee.toDto()).subscribe();
        }

        this.canAddNew = true;
    }

    delete(fee: ServiceFee) {
        this.serviceFees.remove(fee);
        this._serviceProxy.deleteWheelServiceFee(fee.id).subscribe();
    }

    cancel(fee: ServiceFee) {
        if (fee.isNew()) {
            this.newServiceFee = null;
        } else {
            fee.isEditMode = false;
        }

        this.canAddNew = true;
    }

    loadFees() {
        this._serviceProxy.getAllWheelServiceFees().subscribe((result) => {
            result.forEach((feeDto) => {
                var fee = ServiceFee.fromDto(feeDto);
                this.serviceFees.push(fee);
            });
        });
        this._taxesServiceProxy.getAllWheelTaxes().subscribe((result) => {
            this.wheelTaxes = result;
        });
    }
}

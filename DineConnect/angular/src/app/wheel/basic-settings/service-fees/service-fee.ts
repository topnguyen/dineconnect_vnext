import {
    WheelServiceFeeType,
    WheelFeeOrderType,
    WheelServiceFeeDto,
    WheelTaxDto,
} from "@shared/service-proxies/service-proxies";
import { Day } from "../opening-hours/opening-hour";

export class ServiceFee {
    id: number = 0;
    name: string;
    feeType: WheelServiceFeeType;
    value: number = 0;
    calculateTax: boolean = false;
    appliedFeeType: AppliedFeeType;
    appliedOrderType: AppliedOrder;
    isEditMode: boolean = false;
    appliedFeeTypes: AppliedFeeType[];
    appliedOrderTypes: AppliedOrder[];
    wheelTaxes: WheelTaxDto[];

    isNew(): boolean {
        return this.id == 0;
    }

    initApplyFeeTypes() {
        this.appliedFeeTypes = [];
        this.appliedFeeTypes.push(
            new AppliedFeeType(1, "Shopping cart value (sub-total)")
        );
        this.appliedFeeTypes.push(
            new AppliedFeeType(2, "Total value of the order placed")
        );

        this.appliedFeeType = this.appliedFeeTypes[0];
    }

    initApplyOrderTypes() {
        this.appliedOrderTypes = [];
        this.appliedOrderTypes.push(new AllOrders());
        this.appliedOrderTypes.push(new AllOrdersCashDiscount());
        this.appliedOrderTypes.push(new OrdersWithDays());
        this.appliedOrderTypes.push(new OrdersWithPaymentMethods());
        this.appliedOrderTypes.push(new OrdersWithTypes());

        this.appliedOrderType = this.appliedOrderTypes[0];
    }

    getValueDisplayString(): string {
        var result: string = `${this.value}${
            this.feeType == WheelServiceFeeType.Percentage ? "%" : "$"
        }`;
        return result;
    }

    changeUnitType(feeType: WheelServiceFeeType) {
        this.feeType = feeType;
    }

    toDto(): WheelServiceFeeDto {
        var serviceFeeDto = new WheelServiceFeeDto();
        serviceFeeDto.id = this.id;
        serviceFeeDto.name = this.name;
        serviceFeeDto.value = this.value;
        serviceFeeDto.feeType = this.feeType;
        serviceFeeDto.calculateTax = this.calculateTax;
        serviceFeeDto.applyOnSubTotal = this.appliedFeeType.id == 2;
        serviceFeeDto.appliedOrderType = this.appliedOrderType
            .id as WheelFeeOrderType;
        serviceFeeDto.appliedOrderInfo = JSON.stringify(this.appliedOrderType);
        serviceFeeDto.wheelTaxes = JSON.stringify(this.wheelTaxes);
        return serviceFeeDto;
    }

    static fromDto(feeDto: WheelServiceFeeDto): ServiceFee {
        var serviceFee = new ServiceFee();
        serviceFee.id = feeDto.id;
        serviceFee.name = feeDto.name;
        serviceFee.value = feeDto.value;
        serviceFee.calculateTax = feeDto.calculateTax;
        serviceFee.feeType = feeDto.feeType;
        serviceFee.appliedFeeType = feeDto.applyOnSubTotal
            ? serviceFee.appliedFeeTypes[1]
            : serviceFee.appliedFeeTypes[0];
        serviceFee.appliedOrderType = AppliedOrder.fromJson(
            feeDto.appliedOrderType,
            JSON.parse(feeDto.appliedOrderInfo)
        );
        serviceFee.wheelTaxes = JSON.parse(feeDto.wheelTaxes);
        return serviceFee;
    }

    constructor() {
        this.initApplyFeeTypes();
        this.initApplyOrderTypes();
        this.feeType = WheelServiceFeeType.Percentage;
    }
}

export class AppliedFeeType {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class AppliedOrder {
    id: number;
    name: string;

    static fromJson(type: WheelFeeOrderType, json: any): AppliedOrder {
        switch (type) {
            case WheelFeeOrderType.AllOrders: {
                return new AllOrders();
            }
            case WheelFeeOrderType.AllOrdersWithDiscount: {
                return new AllOrdersCashDiscount();
            }
            case WheelFeeOrderType.OrderWithType: {
                var ordersWithTypes = new OrdersWithTypes();
                Object.assign(ordersWithTypes, json);
                return ordersWithTypes;
            }
            case WheelFeeOrderType.OrdersWithDays: {
                var ordersWithDays = new OrdersWithDays();
                Object.assign(ordersWithDays, json);

                ordersWithDays.days = [];
                json.days.forEach((d) => {
                    var day = new Day("");
                    Object.assign(day, d);
                    ordersWithDays.days.push(day);
                });

                ordersWithDays.selectedDates = [];
                json.selectedDates.forEach((d) => {
                    var date = new Date(d);
                    ordersWithDays.selectedDates.push(date);
                });

                ordersWithDays.sortSelectedDates();
                return ordersWithDays;
            }
            case WheelFeeOrderType.OrdersWithPaymentMethod: {
                var ordersWithPaymentMethods = new OrdersWithPaymentMethods();
                Object.assign(ordersWithPaymentMethods, json);
                return ordersWithPaymentMethods;
            }
        }
    }
}

export class AllOrders extends AppliedOrder {
    constructor() {
        super();
        this.id = 0;
        this.name = "All orders";
    }
}

export class AllOrdersCashDiscount extends AppliedOrder {
    constructor() {
        super();
        this.id = 1;
        this.name = "All order, but apply cash discount";
    }
}

export class OrdersWithDays extends AppliedOrder {
    days: Day[];
    selectedDates: Date[];

    constructor() {
        super();
        this.id = 2;
        this.name = "Orders with fulfillment on selected days";
        this.initDays();
        this.selectedDates = [];
    }

    initDays() {
        this.days = [];
        this.days.push(new Day("Monday"));
        this.days.push(new Day("Tuesday"));
        this.days.push(new Day("Wenesday"));
        this.days.push(new Day("Thurday"));
        this.days.push(new Day("Friday"));
        this.days.push(new Day("Saturday"));
        this.days.push(new Day("Sunday"));
    }

    isSelected(index: number) {
        return this.days[index].isSelected;
    }

    revert(index: number) {
        this.days[index].revert();
    }

    onSelectedDateChange(date: Date) {
        date.setHours(0, 0, 0, 0);
        var dateIndex = this.findIndex(this.selectedDates, date);
        if (dateIndex > -1) {
            this.selectedDates.splice(dateIndex, 1);
        } else {
            this.selectedDates.push(date);
        }

        this.sortSelectedDates();
    }

    sortSelectedDates() {
        this.selectedDates = this.selectedDates.sort(function (
            a: Date,
            b: Date
        ): any {
            return a.getTime() - b.getTime();
        });
    }

    findIndex(array, date: Date) {
        date.setHours(0, 0, 0, 0);
        return array.findIndex(function (x) {
            return x.valueOf() == date.valueOf();
        });
    }

    removeDate(date: Date) {
        var dateIndex = this.findIndex(this.selectedDates, date);
        if (dateIndex > -1) {
            this.selectedDates.splice(dateIndex, 1);
        }
    }
}

export class PaymentMethod {
    id: number;
    name: string;
    isSelected: boolean = false;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class OrdersWithPaymentMethods extends AppliedOrder {
    paymentMethods: PaymentMethod[];

    constructor() {
        super();
        this.id = 3;
        this.name = "Orders paid with selected payment methods";
        this.initPaymentMethods();
    }

    initPaymentMethods() {
        this.paymentMethods = [];

        this.paymentMethods.push(new PaymentMethod(1, "Cash"));
    }
}

export class OrderType {
    id: number;
    name: string;
    isSelected: boolean = false;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class OrdersWithTypes extends AppliedOrder {
    orderTypes: OrderType[];

    constructor() {
        super();
        this.id = 4;
        this.name = "Orders of selected types";
        this.initOrderTypes();
    }

    initOrderTypes() {
        this.orderTypes = [];

        this.orderTypes.push(new OrderType(1, "Pick Up"));
        this.orderTypes.push(new OrderType(2, "Delivery"));
        this.orderTypes.push(new OrderType(3, "Order Ahead"));
    }
}

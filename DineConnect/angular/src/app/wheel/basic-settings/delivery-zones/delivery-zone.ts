import { WheelDeliveryZoneDto, WheelDeliveryZoneType } from "@shared/service-proxies/service-proxies";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { LatLngLiteral } from "@agm/core";

export class DeliveryZone {
    id: number = 0;
    name: string;
    type: WheelDeliveryZoneType;
    minimumAmount: number = 0;
    deliveryFee: number = 0;
    isEditMode: boolean = false;
    color: string;
    marker: Marker;
    radius: number;

    constructor() {
        this.color = this.randomHSL();
    }

    generateRandomColor(): string {
        return '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
    }

    randomHSL() {
        return `hsla(${~~(360 * Math.random())},80%,50%,1)`
    }

    toDto(): WheelDeliveryZoneDto{
        var zoneDto = new WheelDeliveryZoneDto();
        zoneDto.id = this.id;
        zoneDto.name = this.name;
        zoneDto.minimumTotalAmount = this.minimumAmount;
        zoneDto.deliveryFee = this.deliveryFee;
        zoneDto.color = this.color;
        zoneDto.zoneType = this.type;
        zoneDto.zoneCoordinates = this.marker.getCoordinates();

        return zoneDto;
    }

    isNew(): boolean{
        return this.id == 0;
    }

    static fromDto(zoneDto: WheelDeliveryZoneDto): DeliveryZone{
        var zone = new DeliveryZone();
        zone.id = zoneDto.id;
        zone.name = zoneDto.name;
        zone.minimumAmount = zoneDto.minimumTotalAmount;
        zone.deliveryFee = zoneDto.deliveryFee;
        zone.color = zoneDto.color;
        zone.type = zoneDto.zoneType;
        
        return zone;
    }
}

export class Point {
    lng: number = 0;
    lat: number = 0;

    constructor(lng: number, lat: number) {
        this.lng = lng;
        this.lat = lat;
    }
}

export abstract class Marker {
    color: string;
    strokeWeight: number = 2;
    opacity: number = 0.2;
    isEditable: boolean = false;

    highLight() {
        this.strokeWeight = 3;
        this.opacity = 0.3;
    }

    unHighLight() {
        this.strokeWeight = 2;
        this.opacity = 0.2;
    }

    abstract getCoordinates(): string;
}

export class CircleMarker extends Marker {
    center: Point;
    radius: number;
    subject: Subject<LatLngLiteral> = new Subject();

    constructor(longitude: number, latitude: number, radius: number, color: string) {
        super();
        var centerPoint = new Point(longitude, latitude);
        this.center = centerPoint;
        this.radius = radius;
        this.color = color;

        this.subject.pipe(debounceTime(500)).subscribe(details =>{
            this.center.lat = details.lat;
            this.center.lng = details.lng;
        });
    }

    getCoordinates(): string{
        var coor = {
            center: this.center,
            radius: this.radius
        };

        return JSON.stringify(coor);
    }

    updateCenter(newCoor: LatLngLiteral){
        this.subject.next(newCoor);
    }
}

export class PolygonMarker extends Marker {
    paths: Point[];
    subject: Subject<any> = new Subject();

    getCoordinates(): string{
        var coor = {
            paths: this.paths
        };

        return JSON.stringify(coor);
    }

    updatePaths(newPaths: any){
        this.subject.next(newPaths);
    }

    constructor(color: string, paths: Point[]) {
        super();
        this.color = color;
        this.paths = paths;

        this.subject.pipe(debounceTime(500)).subscribe(details =>{
            this.paths = [];
            details.newArr[0].forEach(coor => {
                this.paths.push(new Point(coor.lng, coor.lat));
            });
        });
    }
}
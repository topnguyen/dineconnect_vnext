import { Component, Injector, OnInit, ViewEncapsulation } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ZoomControlOptions, ControlPosition } from "@agm/core";
import {
    DeliveryZone,
    CircleMarker,
    PolygonMarker,
    Point,
    Marker,
} from "./delivery-zone";
import "../../extension-methods";
import {
    WheelDeliveryZoneDto,
    WheelServiceProxy,
    WheelDeliveryZoneType,
    WheelLocationsServiceProxy,
    TimeSettingsServiceProxy,
    WheelLocationForDeliveryZone,
} from "@shared/service-proxies/service-proxies";

declare const google: any;

@Component({
    selector: "app-delivery-zones",
    templateUrl: "./delivery-zones.component.html",
    styleUrls: ["./delivery-zones.component.scss"],
    animations: [appModuleAnimation()],
})
export class DeliveryZonesComponent extends AppComponentBase implements OnInit {
    lat: number = 1.287953;
    lng: number = 103.851784;
    zoomLevel: number = 13;
    zoomControlOptions: ZoomControlOptions;
    deliveryZones: DeliveryZone[];
    newDeliveryZone: DeliveryZone;
    map: any;
    markers: Marker[];
    drawingManager: any = null;
    locationMarkers: WheelLocationForDeliveryZone[] = [];
    constructor(
        injector: Injector,
        private _deliveryZoneService: WheelServiceProxy,
        private _wheelLocationsServiceProxy: WheelLocationsServiceProxy
    ) {
        super(injector);
    }

    createDefaultMarker(color: string): Marker {
        var marker = new CircleMarker(this.lng, this.lat, 1000, color);
        marker.isEditable = true;

        return marker;
    }

    addNewZone() {
        var newZone = new DeliveryZone();
        newZone.name = "Zone " + (this.deliveryZones.length + 1);
        newZone.type = WheelDeliveryZoneType.Circle;
        newZone.marker = this.createDefaultMarker(newZone.color);
        this.markers.push(newZone.marker);

        this.newDeliveryZone = newZone;
    }

    changeZoneType(zone: DeliveryZone, newType: WheelDeliveryZoneType) {
        if (zone.type == newType) return;

        this.closeDrawingManager();
        this.markers.remove(zone.marker);
        zone.type = newType;
        zone.marker = null;

        if (newType == WheelDeliveryZoneType.Circle) {
            zone.marker = this.createDefaultMarker(zone.color);
            this.markers.push(zone.marker);
        } else if (newType == WheelDeliveryZoneType.Polygon) {
            var drawingOptions = {
                drawingControl: false,
                drawingControlOptions: {
                    drawingModes: ["polygon"],
                },
                polygonOptions: {
                    draggable: true,
                    editable: true,
                    strokeColor: zone.color,
                },
                drawingMode: "polygon",
            };

            this.drawingManager = new google.maps.drawing.DrawingManager(
                drawingOptions
            );
            this.drawingManager.setMap(this.map);

            var self = this;
            google.maps.event.addListener(
                this.drawingManager,
                "polygoncomplete",
                function (polygon) {
                    const len = polygon.getPath().getLength();
                    const polyPaths = [];

                    for (let i = 0; i < len; i++) {
                        const vertex = polygon.getPath().getAt(i);
                        var point = new Point(vertex.lng(), vertex.lat());
                        polyPaths.push(point);
                    }

                    var polygonMarker = new PolygonMarker(
                        zone.color,
                        polyPaths
                    );
                    polygonMarker.isEditable = true;
                    zone.marker = polygonMarker;

                    self.markers.push(polygonMarker);
                    polygon.setMap(null);
                    polygon = null;
                    self.closeDrawingManager();
                }
            );
        }
    }

    deleteZone(zone: DeliveryZone) {
        this.closeDrawingManager();

        if (zone.isNew()) {
            this.newDeliveryZone = null;
        } else {
            this.deliveryZones.remove(zone);
            this._deliveryZoneService.deleteZone(zone.id).subscribe();
        }

        this.markers.remove(zone.marker);
    }

    saveZone(zone: DeliveryZone) {
        this.closeDrawingManager();
        if (zone.marker == null) {
            zone.marker = this.createDefaultMarker(zone.color);
            zone.type = WheelDeliveryZoneType.Circle;
            this.markers.push(zone.marker);
        }

        if (zone.isNew()) {
            this.deliveryZones.push(zone);
            this.onZoneOpenChange(zone, false);
            this.newDeliveryZone = null;

            this._deliveryZoneService
                .addZone(zone.toDto())
                .subscribe((result) => {
                    zone.id = result.id;
                });
        } else {
            zone.isEditMode = false;
            this._deliveryZoneService
                .updateZone(zone.toDto())
                .subscribe(() => {});
        }
    }

    onZoneOpenChange(zone: DeliveryZone, isOpen: boolean) {
        zone.isEditMode = isOpen;
        var marker = this.markers.find((m) => m == zone.marker);

        if (isOpen) {
            if (marker != null) {
                marker.highLight();
                marker.isEditable = true;
            }
        } else {
            if (marker != null) {
                marker.unHighLight();
                marker.isEditable = false;
            }

            this.closeDrawingManager();
            if (zone.marker == null) {
                zone.marker = this.createDefaultMarker(zone.color);
                zone.type = WheelDeliveryZoneType.Circle;
                this.markers.push(zone.marker);
            }
        }
    }

    changeZoneColor(zone: DeliveryZone, color: string) {
        zone.color = color;
        var marker = this.markers.find((m) => m == zone.marker);
        if (marker != null) marker.color = color;
    }

    closeDrawingManager() {
        if (this.drawingManager != null) {
            this.drawingManager.setMap(null);
            this.drawingManager = null;
        }
    }

    isCircleMarker(marker: Marker) {
        return marker instanceof CircleMarker;
    }

    isPolygonMarker(marker: Marker) {
        return marker instanceof PolygonMarker;
    }

    onMapReady(map: any) {
        this.map = map;
    }

    ngOnInit() {
        this.deliveryZones = [];
        this.markers = [];

        this.zoomControlOptions = {
            position: ControlPosition.LEFT_TOP,
        };
        this._wheelLocationsServiceProxy
            .getAllWheelLocationForDeliveryZone()
            .subscribe((result) => {
                this.locationMarkers = result;

                if (this.locationMarkers?.length) {
                    this.lat = parseFloat(
                        this.locationMarkers[0].pickupAddressLat
                    );
                    this.lng = parseFloat(
                        this.locationMarkers[0].pickupAddressLong
                    );
                }
            });
        this.loadAllZones();
    }

    loadAllZones() {
        this._deliveryZoneService
            .getAllDeliveryZones()
            .subscribe((zones: WheelDeliveryZoneDto[]) => {
                zones.forEach((zoneDto) => {
                    var zone = DeliveryZone.fromDto(zoneDto);
                    var coor = JSON.parse(zoneDto.zoneCoordinates);
                    var marker: Marker;

                    if (zone.type == WheelDeliveryZoneType.Circle) {
                        marker = new CircleMarker(
                            coor.center.lng,
                            coor.center.lat,
                            coor.radius,
                            zone.color
                        );
                    } else if (zone.type == WheelDeliveryZoneType.Polygon) {
                        marker = new PolygonMarker(zone.color, coor.paths);
                    }

                    zone.marker = marker;
                    this.markers.push(marker);
                    this.deliveryZones.push(zone);
                });
            });
    }
    clickedMarker(label: string, index: number) {}
    markerDragEnd(m, $event: MouseEvent) {}
    getRadiusKm(input) {
        return (input / 1000).toFixed(2);
    }
}

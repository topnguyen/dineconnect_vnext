import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OrderSettingDto, TenantSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
  selector: 'app-order-settings',
  templateUrl: './order-settings.component.html',
  styleUrls: ['./order-settings.component.css'],
  animations: [appModuleAnimation()],
})
export class OrderSettingsComponent extends AppComponentBase implements OnInit {
  orderSetting = new OrderSettingDto();
  saving = false;
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl;
  menuItemDefaultUploader: FileUploader;
  urlBase = this.remoteServiceBaseUrl + '/TenantCustomization/GetMenuItemDefault?tenantId=' + this.appSession.tenantId;

  width = 0;
  height = 0;
  name: any;

  saveImgForm = false;
  constructor(
    injector: Injector,
    private _tenantSetting: TenantSettingsServiceProxy,
    private _tokenService: TokenService,
    private _tenantSettingsService: TenantSettingsServiceProxy,

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._tenantSetting.getOrderSetting().subscribe(result => {
      this.orderSetting = result;

    });
    this.initUploaders();
  }
  initUploaders() {
    this.menuItemDefaultUploader = this.createUploader('/ImageUploader/UploadMenuItemDefault', (result) => {
      this.appSession.tenant.menuItemDefaultFileType = result.fileType;
      this.appSession.tenant.menuItemDefaultID = result.id;
      this.urlBase = this.remoteServiceBaseUrl + '/TenantCustomization/GetMenuItemDefault?tenantId=' + this.appSession.tenantId + '&random=' + Math.random();
    });
  }
  save() {
    this._tenantSetting.updateOrderSetting(this.orderSetting)
      .subscribe(() => {
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
      });
  }

  saveSetting(){
    this.save();
  }

  uploadMenuItemDefault(): void {
    this.menuItemDefaultUploader.uploadAll();
  }
  clearMenuItemDefault(): void {
    this._tenantSettingsService.clearMenuItemDefault().subscribe(() => {
      this.appSession.tenant.menuItemDefaultFileType = null;
      this.appSession.tenant.menuItemDefaultID = null;
      this.notify.success(this.l('ClearedSuccessfully'));
    });
  }

  createUploader(url: string, success?: (result: any) => void): FileUploader {
    const uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + url });

    uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <IAjaxResponse>JSON.parse(response);
      if (ajaxResponse.success) {
        this.notify.success(this.l('SavedSuccessfully'));
        if (success) {
          success(ajaxResponse.result);
        }
      } else {
        this.message.error(ajaxResponse.error.message);
      }
    };

    const uploaderOptions: FileUploaderOptions = {};
    uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    uploaderOptions.removeAfterUpload = true;
    uploader.setOptions(uploaderOptions);
    return uploader;
  }

  saveImgSize(){
    this._tenantSetting.updateStandImageSize(this.orderSetting)
    .subscribe(() => {
      this.saving = false;
      this.notify.success(this.l('SavedSuccessfully'));
    });
  }
}


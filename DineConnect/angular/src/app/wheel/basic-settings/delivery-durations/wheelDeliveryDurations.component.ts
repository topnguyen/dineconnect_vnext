﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { WheelDeliveryDurationsServiceProxy, WheelDeliveryDurationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditWheelDeliveryDurationModalComponent } from './create-or-edit-wheelDeliveryDuration-modal.component';
import { ViewWheelDeliveryDurationModalComponent } from './view-wheelDeliveryDuration-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './wheelDeliveryDurations.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./wheelDeliveryDurations.component.scss'],
    animations: [appModuleAnimation()]
})
export class WheelDeliveryDurationsComponent extends AppComponentBase {
    @ViewChild('createOrEditWheelDeliveryDurationModal', { static: true })
    createOrEditWheelDeliveryDurationModal: CreateOrEditWheelDeliveryDurationModalComponent;
    @ViewChild('viewWheelDeliveryDurationModalComponent', { static: true })
    viewWheelDeliveryDurationModal: ViewWheelDeliveryDurationModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    maxTenantIdFilter: number;
    maxTenantIdFilterEmpty: number;
    minTenantIdFilter: number;
    minTenantIdFilterEmpty: number;
    maxDeliveryDistanceFilter: number;
    maxDeliveryDistanceFilterEmpty: number;
    minDeliveryDistanceFilter: number;
    minDeliveryDistanceFilterEmpty: number;
    maxDeliveryTimeFilter: number;
    maxDeliveryTimeFilterEmpty: number;
    minDeliveryTimeFilter: number;
    minDeliveryTimeFilterEmpty: number;

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _wheelDeliveryDurationsServiceProxy: WheelDeliveryDurationsServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelDeliveryDurations();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getWheelDeliveryDurations(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelDeliveryDurationsServiceProxy
            .getAll(
                this.filterText,
                this.maxTenantIdFilter == null ? this.maxTenantIdFilterEmpty : this.maxTenantIdFilter,
                this.minTenantIdFilter == null ? this.minTenantIdFilterEmpty : this.minTenantIdFilter,
                this.maxDeliveryDistanceFilter == null ? this.maxDeliveryDistanceFilterEmpty : this.maxDeliveryDistanceFilter,
                this.minDeliveryDistanceFilter == null ? this.minDeliveryDistanceFilterEmpty : this.minDeliveryDistanceFilter,
                this.maxDeliveryTimeFilter == null ? this.maxDeliveryTimeFilterEmpty : this.maxDeliveryTimeFilter,
                this.minDeliveryTimeFilter == null ? this.minDeliveryTimeFilterEmpty : this.minDeliveryTimeFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createWheelDeliveryDuration(): void {
        this.createOrEditWheelDeliveryDurationModal.show();
    }

    deleteWheelDeliveryDuration(wheelDeliveryDuration: WheelDeliveryDurationDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._wheelDeliveryDurationsServiceProxy.delete(wheelDeliveryDuration.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    exportToExcel(): void {
        this._wheelDeliveryDurationsServiceProxy
            .getWheelDeliveryDurationsToExcel(
                this.filterText,
                this.maxTenantIdFilter == null ? this.maxTenantIdFilterEmpty : this.maxTenantIdFilter,
                this.minTenantIdFilter == null ? this.minTenantIdFilterEmpty : this.minTenantIdFilter,
                this.maxDeliveryDistanceFilter == null ? this.maxDeliveryDistanceFilterEmpty : this.maxDeliveryDistanceFilter,
                this.minDeliveryDistanceFilter == null ? this.minDeliveryDistanceFilterEmpty : this.minDeliveryDistanceFilter,
                this.maxDeliveryTimeFilter == null ? this.maxDeliveryTimeFilterEmpty : this.maxDeliveryTimeFilter,
                this.minDeliveryTimeFilter == null ? this.minDeliveryTimeFilterEmpty : this.minDeliveryTimeFilter
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.maxTenantIdFilter = undefined;
        this.maxTenantIdFilterEmpty = undefined;
        this.minTenantIdFilter = undefined;
        this.minTenantIdFilterEmpty = undefined;
        this.maxDeliveryDistanceFilter = undefined;
        this.maxDeliveryDistanceFilterEmpty = undefined;
        this.minDeliveryDistanceFilter = undefined;
        this.minDeliveryDistanceFilterEmpty = undefined;
        this.maxDeliveryTimeFilter = undefined;
        this.maxDeliveryTimeFilterEmpty = undefined;
        this.minDeliveryTimeFilter = undefined;
        this.minDeliveryTimeFilterEmpty = undefined;

    }

    apply() {
        this.getWheelDeliveryDurations();
        this.isShowFilterOptions = false;
    }
}

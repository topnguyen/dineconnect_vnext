﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { WheelDeliveryDurationsServiceProxy, CreateOrEditWheelDeliveryDurationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditWheelDeliveryDurationModal',
    templateUrl: './create-or-edit-wheelDeliveryDuration-modal.component.html'
})
export class CreateOrEditWheelDeliveryDurationModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    wheelDeliveryDuration: CreateOrEditWheelDeliveryDurationDto = new CreateOrEditWheelDeliveryDurationDto();



    constructor(
        injector: Injector,
        private _wheelDeliveryDurationsServiceProxy: WheelDeliveryDurationsServiceProxy
    ) {
        super(injector);
    }

    show(wheelDeliveryDurationId?: number): void {

        if (!wheelDeliveryDurationId) {
            this.wheelDeliveryDuration = new CreateOrEditWheelDeliveryDurationDto();
            this.wheelDeliveryDuration.id = wheelDeliveryDurationId;

            this.active = true;
            this.modal.show();
        } else {
            this._wheelDeliveryDurationsServiceProxy.getWheelDeliveryDurationForEdit(wheelDeliveryDurationId).subscribe(result => {
                this.wheelDeliveryDuration = result.wheelDeliveryDuration;


                this.active = true;
                this.modal.show();
            });
        }
        
    }

    save(): void {
            this.saving = true;

			
            this._wheelDeliveryDurationsServiceProxy.createOrEdit(this.wheelDeliveryDuration)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

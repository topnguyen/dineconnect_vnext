﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetWheelDeliveryDurationForViewDto, WheelDeliveryDurationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewWheelDeliveryDurationModal',
    templateUrl: './view-wheelDeliveryDuration-modal.component.html'
})
export class ViewWheelDeliveryDurationModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetWheelDeliveryDurationForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetWheelDeliveryDurationForViewDto();
        this.item.wheelDeliveryDuration = new WheelDeliveryDurationDto();
    }

    show(item: GetWheelDeliveryDurationForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

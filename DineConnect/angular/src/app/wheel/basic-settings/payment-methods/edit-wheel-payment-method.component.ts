﻿import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    WheelPaymentMethodsServiceProxy,
    EditWheelPaymentMethodDto,
    RecurringPaymentType,
    GetWheelPaymentMethodForEditOutput,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { TokenService, IAjaxResponse } from "abp-ng2-module";
import { AppConsts } from "@shared/AppConsts";

const SYSTEM_NAME = {
    Adyen: "Payments.Adyen",
    Stripe: "Payments.Stripe",
    Omise: "Payments.Omise",
    PayPal: "Payments.PayPal",
};

const FIELD_DEFAULT = {
    Adyen: [
        {
            key: "apiKey",
            type: "input",
            templateOptions: {
                label: "API Key",
            },
        },
        {
            key: "merchantAccount",
            type: "input",
            templateOptions: {
                label: "Merchant Account",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Test", value: 0 },
                    { label: "Live", value: 1 },
                ],
            },
        },
    ],
    Stripe: [
        {
            key: "secretKey",
            type: "input",
            templateOptions: {
                label: "Secret Key",
            },
        },
    ],
    Omise: [
        {
            key: "secretKey",
            type: "input",
            templateOptions: {
                label: "Secret Key",
            },
        },
        {
            key: "publishKey",
            type: "input",
            templateOptions: {
                label: "Publish Key",
            },
        },
    ],
    PayPal: [
        {
            key: "clientId",
            type: "input",
            templateOptions: {
                label: "ClientId",
            },
        },
        {
            key: "clientSecret",
            type: "input",
            templateOptions: {
                label: "Client Secret",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Sand Box", value: 0 },
                    { label: "Live", value: 1 },
                ],
            },
        },
    ],
};

@Component({
    selector: "edit-wheel-payment-method",
    templateUrl: "./edit-wheel-payment-method.component.html",
    styleUrls: ["./edit-wheel-payment-method.component.scss"],
    animations: [appModuleAnimation()],
})
export class EditWheelPaymentMethodComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    uploadImageLoading = false;
    wheelPaymentMethod: GetWheelPaymentMethodForEditOutput = new GetWheelPaymentMethodForEditOutput();
    paymentTypeName = "";
    recurringPaymentTypeOption = [];
    file: any;
    logo: any;
    uploader: FileUploader;

    RecurringPaymentType = RecurringPaymentType;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _router: Router
    ) {
        super(injector);

        this._activatedRoute.params.subscribe((params) => {
            const systemName = params["systemName"];

            if (!systemName) {
                this.wheelPaymentMethod = new GetWheelPaymentMethodForEditOutput();
            } else {
                this._wheelPaymentMethodsServiceProxy.getWheelPaymentMethodForEdit(systemName).subscribe((result) => {
                    this.wheelPaymentMethod = result;

                    this._initDynamicFields();
                });
            }
        });

        this.initUploader();
    }

    private _initDynamicFields() {
        switch (this.wheelPaymentMethod.systemName) {
            case SYSTEM_NAME.Adyen:
                {
                    if (!this.wheelPaymentMethod.dynamicFieldConfig) {
                        this.wheelPaymentMethod.dynamicFieldConfig = FIELD_DEFAULT.Adyen;
                        this.wheelPaymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.wheelPaymentMethod.dynamicFieldData = this.wheelPaymentMethod.dynamicFieldData;
                        this.wheelPaymentMethod.dynamicFieldConfig = this.wheelPaymentMethod.dynamicFieldConfig;
                    }
                }

                break;

            case SYSTEM_NAME.Omise:
                {
                    if (!this.wheelPaymentMethod.dynamicFieldConfig) {
                        this.wheelPaymentMethod.dynamicFieldConfig = FIELD_DEFAULT.Omise;
                        this.wheelPaymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.wheelPaymentMethod.dynamicFieldData = this.wheelPaymentMethod.dynamicFieldData;
                        this.wheelPaymentMethod.dynamicFieldConfig = this.wheelPaymentMethod.dynamicFieldConfig;
                    }
                }

                break;
            case SYSTEM_NAME.PayPal:
                {
                    if (!this.wheelPaymentMethod.dynamicFieldConfig) {
                        this.wheelPaymentMethod.dynamicFieldConfig = FIELD_DEFAULT.PayPal;
                        this.wheelPaymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.wheelPaymentMethod.dynamicFieldData = this.wheelPaymentMethod.dynamicFieldData;
                        this.wheelPaymentMethod.dynamicFieldConfig = this.wheelPaymentMethod.dynamicFieldConfig;
                    }
                }

                break;
            case SYSTEM_NAME.Stripe:
                {
                    if (!this.wheelPaymentMethod.dynamicFieldConfig) {
                        this.wheelPaymentMethod.dynamicFieldConfig = FIELD_DEFAULT.Stripe;
                        this.wheelPaymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.wheelPaymentMethod.dynamicFieldData = this.wheelPaymentMethod.dynamicFieldData;
                        this.wheelPaymentMethod.dynamicFieldConfig = this.wheelPaymentMethod.dynamicFieldConfig;
                    }
                }

                break;

            default:
                {
                    if (!this.wheelPaymentMethod.dynamicFieldConfig) {
                        this.wheelPaymentMethod.dynamicFieldConfig = [];
                        this.wheelPaymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.wheelPaymentMethod.dynamicFieldData = this.wheelPaymentMethod.dynamicFieldData;
                        this.wheelPaymentMethod.dynamicFieldConfig = this.wheelPaymentMethod.dynamicFieldConfig;
                    }
                }
                break;
        }
    }

    ngOnInit() {
        this.logo = new KTImageInput("kt_image_1");

        this.logo.on("change", (imageInput) => {
            this.uploadImageLoading = true;
            this.uploader.addToQueue(imageInput.input.files);
        });

        this.logo.on("cancel", () => {
            this.wheelPaymentMethod.logo = "";
        });

        this.logo.on("remove", () => {
            this.wheelPaymentMethod.logo = "";
        });

        for (var enumMember in RecurringPaymentType) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.recurringPaymentTypeOption.push({
                    label: RecurringPaymentType[enumMember],
                    value: parseInt(enumMember, 10),
                });
            }
        }
    }

    save(): void {
        this.saving = true;

        const input = EditWheelPaymentMethodDto.fromJS(this.wheelPaymentMethod);

        input.dynamicFieldConfig = JSON.stringify(this.wheelPaymentMethod.dynamicFieldConfig);
        input.dynamicFieldData = JSON.stringify(this.wheelPaymentMethod.dynamicFieldData);

        this._wheelPaymentMethodsServiceProxy
            .edit(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.cancel();
            });
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImage",
            authToken: "Bearer " + this._tokenService.getToken(),
        });

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.uploadImageLoading = false;
                this.wheelPaymentMethod.logo = ajaxResponse.result.url;
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.removeAfterUpload = true;
        uploaderOptions.autoUpload = true;
        this.uploader.setOptions(uploaderOptions);
    }

    cancel() {
        this._router.navigate(["/app/wheel/basic-settings/payment-methods"]);
    }
}

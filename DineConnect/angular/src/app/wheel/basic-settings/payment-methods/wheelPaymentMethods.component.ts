﻿import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
    WheelPaymentMethodsServiceProxy,
    WheelPaymentMethodDto,
    RecurringPaymentType,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TokenAuthServiceProxy } from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { NotifyService } from "abp-ng2-module";

@Component({
    templateUrl: "./wheelPaymentMethods.component.html",
    styleUrls: ["./wheelPaymentMethods.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class WheelPaymentMethodsComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    systemNameFilter = "";
    friendlyNameFilter = "";
    supportsCaptureFilter = -1;
    refundFilter = -1;
    partialRefundFilter = -1;
    voidFilter = -1;
    activeFilter = -1;
    recurringSupportFilter = -1;

    allRecurringPaymentType = [];

    RecurringPaymentType = RecurringPaymentType;
    constructor(
        injector: Injector,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);

        for (var enumMember in RecurringPaymentType) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.allRecurringPaymentType.push({
                    displayText: RecurringPaymentType[enumMember],
                    value: parseInt(enumMember, 10),
                });
            }
        }
    }

    getWheelPaymentMethods(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._wheelPaymentMethodsServiceProxy
            .getAll(
                this.filterText,
                this.systemNameFilter,
                this.friendlyNameFilter,
                this.recurringSupportFilter == -1 ? undefined : this.recurringSupportFilter,
                this.supportsCaptureFilter,
                this.refundFilter,
                this.partialRefundFilter,
                this.voidFilter,
                this.activeFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.items.length;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    edit(systemName?: string) {
        this._router.navigate([
            "/app/wheel/basic-settings/payment-methods/edit",
            systemName ? systemName : "null",
        ]);
    }

    deleteWheelPaymentMethod(wheelPaymentMethod: WheelPaymentMethodDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._wheelPaymentMethodsServiceProxy
                    .delete(wheelPaymentMethod.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    refresh() {
        this.filterText = "";
        this.systemNameFilter = "";
        this.friendlyNameFilter = "";
        this.supportsCaptureFilter = -1;
        this.refundFilter = -1;
        this.partialRefundFilter = -1;
        this.voidFilter = -1;
        this.activeFilter = -1;
        this.recurringSupportFilter = -1;

        this.reloadPage();
    }

    exportToExcel(): void {
        this._wheelPaymentMethodsServiceProxy
            .getWheelPaymentMethodsToExcel(
                this.filterText,
                this.systemNameFilter,
                this.friendlyNameFilter
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}

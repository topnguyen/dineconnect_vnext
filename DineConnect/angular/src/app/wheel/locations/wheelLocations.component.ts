﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { WheelLocationsServiceProxy, WheelLocationDto , WheelPostalCodeSearchType } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './wheelLocations.component.html',
    styleUrls: ['./wheelLocations.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class WheelLocationsComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    nameFilter = '';
    enabledFilter = -1;
    taxInclusiveFilter = -1;
    outsideDeliveryZoneFilter = -1;
    wheelPostalCodeSearchTypeFilter = -1;
    locationNameFilter = '';
    screenMenuNameFilter = '';

    wheelPostalCodeSearchType = WheelPostalCodeSearchType;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _wheelLocationsServiceProxy: WheelLocationsServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getWheelLocations();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getWheelLocations(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._wheelLocationsServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.enabledFilter,
            this.taxInclusiveFilter,
            this.outsideDeliveryZoneFilter,
            this.wheelPostalCodeSearchTypeFilter,
            this.locationNameFilter,
            this.screenMenuNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        )
        .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
        .subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createWheelLocation(id?: number): void {
        this._router.navigate(['/app/wheel/locations/create-or-edit', id ? id : 'null'])
    }

    deleteWheelLocation(wheelLocation: WheelLocationDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._wheelLocationsServiceProxy.delete(wheelLocation.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._wheelLocationsServiceProxy.getWheelLocationsToExcel(
        this.filterText,
            this.nameFilter,
            this.enabledFilter,
            this.taxInclusiveFilter,
            this.outsideDeliveryZoneFilter,
            this.wheelPostalCodeSearchTypeFilter,
            this.locationNameFilter,
            this.screenMenuNameFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }

    
    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.nameFilter = '';
        this.enabledFilter = -1;
        this.taxInclusiveFilter = -1;
        this.outsideDeliveryZoneFilter = -1;
        this.wheelPostalCodeSearchTypeFilter = -1;
        this.locationNameFilter = '';
        this.screenMenuNameFilter = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

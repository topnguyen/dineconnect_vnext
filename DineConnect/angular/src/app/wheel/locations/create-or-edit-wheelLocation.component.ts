﻿import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    WheelLocationsServiceProxy,
    CreateOrEditWheelLocationDto,
    WheelLocationLookupTableDto,
    WheelLocationScreenMenuLookupTableDto,
    WheelServiceFeeDto,
    WheelDeliveryZoneDto,
    WheelServiceProxy,
    WheelDepartmentsServiceProxy,
    WheelTaxDto,
    LocationServiceProxy,
    PagedResultDtoOfLocationDto,
    GetWheelDepartmentForViewDto,
    WheelTaxesServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: "createOrEditWheelLocationModal",
    templateUrl: "./create-or-edit-wheelLocation.component.html",
    styleUrls: ["./wheelLocations.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditWheelLocationComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    locationName = "";
    screenMenuName = "";

    wheelLocation: CreateOrEditWheelLocationDto =
        new CreateOrEditWheelLocationDto();
    wheelLocationId: number;
    allLocations: WheelLocationLookupTableDto[];
    allScreenMenus: WheelLocationScreenMenuLookupTableDto[];
    allPostalCodeSearchType: any[];
    allDeliveryZones: WheelDeliveryZoneDto[];
    selectedDeliveryZone: WheelDeliveryZoneDto;
    allDepartments: GetWheelDepartmentForViewDto[];
    selectedDepartment: GetWheelDepartmentForViewDto;
    allServiceFees: WheelServiceFeeDto[];
    selectedNewFee: WheelServiceFeeDto;
    allTaxes: WheelTaxDto[];
    selectedTax: WheelTaxDto;

    constructor(
        injector: Injector,
        private _wheelLocationsServiceProxy: WheelLocationsServiceProxy,
        private _wheelServiceProxy: WheelServiceProxy,
        private _wheelDepartmentServiceProxy: WheelDepartmentsServiceProxy,
        private _serviceProxy: WheelServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _taxServiceProxy: WheelTaxesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(injector);

        this._activatedRoute.params.subscribe((params) => {
            this.wheelLocationId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.wheelLocationId)) {
                this._wheelLocationsServiceProxy
                    .getWheelLocationForEdit(this.wheelLocationId)
                    .subscribe((result) => {
                        this.wheelLocation = result;
                        this.locationName = result.locationName;
                    });
            }
        });

        this.getDataForCombobox();
    }

    getDataForCombobox() {
        this._wheelServiceProxy.getAllDeliveryZones().subscribe((result) => {
            this.allDeliveryZones = result.sort((a, b) =>
                a.name.localeCompare(b.name)
            );
        });

        this._wheelDepartmentServiceProxy
            .getAllWheelDepartments()
            .subscribe((result) => {
                this.allDepartments = result.sort((a, b) =>
                    a.name.localeCompare(b.name)
                );
            });

        this._serviceProxy.getAllWheelServiceFees().subscribe((result) => {
            this.allServiceFees = result;
        });

        this._taxServiceProxy.getAllWheelTaxes().subscribe((result) => {
            this.allTaxes = result;
        });
    }

    resetData() {
        this.selectedDeliveryZone = null;
        this.selectedDepartment = null;
        this.selectedNewFee = null;
        this.selectedTax = null;
    }

    save(): void {
        this.saving = true;
        this._wheelLocationsServiceProxy
            .createOrEdit(this.wheelLocation)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/wheel/locations"]);
    }

    addDeliveryZone() {
        if (!this.selectedDeliveryZone) {
            return;
        }

        if (!this.wheelLocation.wheelDeliveryZones) {
            this.wheelLocation.wheelDeliveryZones = [];
        }

        if (
            this.wheelLocation.wheelDeliveryZones.findIndex(
                (f) => f.id == this.selectedDeliveryZone.id
            ) == -1
        ) {
            this.wheelLocation.wheelDeliveryZones.push(
                this.selectedDeliveryZone
            );
        }
    }

    removeDeliveryZone(zone: WheelDeliveryZoneDto) {
        this.wheelLocation.wheelDeliveryZones.remove(zone);
    }

    addDepartment() {
        if (!this.selectedDepartment) {
            return;
        }

        if (!this.wheelLocation.wheelDepartments) {
            this.wheelLocation.wheelDepartments = [];
        }

        if (
            this.wheelLocation.wheelDepartments.findIndex(
                (f) => f.id == this.selectedDepartment.id
            ) == -1
        ) {
            this.wheelLocation.wheelDepartments.push(this.selectedDepartment);
        }
    }

    removeDepartment(department: GetWheelDepartmentForViewDto) {
        this.wheelLocation.wheelDepartments.remove(department);
    }

    addServiceFee() {
        if (!this.selectedNewFee) {
            return;
        }

        if (!this.wheelLocation.wheelServiceFees) {
            this.wheelLocation.wheelServiceFees = [];
        }

        if (
            this.wheelLocation.wheelServiceFees.findIndex(
                (f) => f.id == this.selectedNewFee.id
            ) == -1
        ) {
            this.wheelLocation.wheelServiceFees.push(this.selectedNewFee);
        }
    }

    removeServiceFee(fee: WheelServiceFeeDto) {
        this.wheelLocation.wheelServiceFees.remove(fee);
    }
    addTax() {
        if (!this.selectedTax) {
            return;
        }

        if (!this.wheelLocation.wheelTaxes) {
            this.wheelLocation.wheelTaxes = [];
        }

        if (
            this.wheelLocation.wheelTaxes.findIndex(
                (f) => f.id == this.selectedTax.id
            ) == -1
        ) {
            this.wheelLocation.wheelTaxes.push(this.selectedTax);
        }
    }

    removeTax(tax: WheelTaxDto) {
        this.wheelLocation.wheelTaxes.remove(tax);
    }

    getLocationForCombobox() {
        return (
            filterString,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(
                filterString,
                false,
                0,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    changeLocation(data) {
        this.locationName = data.name;
        this.wheelLocation.locationId = data.id;
    }
}

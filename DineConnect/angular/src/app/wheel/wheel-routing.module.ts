import { ReservetionDetailComponent } from "./reservetion-detail/reservetion-detail.component";
import { ReservationComponent } from "./reservation/reservation.component";
import { SeatmapComponent } from "./seatmap/seatmap.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ServiceFeesComponent } from "./basic-settings/service-fees/service-fees.component";
import { DeliveryZonesComponent } from "./basic-settings/delivery-zones/delivery-zones.component";
import { OpeningHoursComponent } from "./basic-settings/opening-hours/opening-hours.component";
import { TemplatesComponent } from "./dynamic-pages/templates.component";
import { WheelDeliveryDurationsComponent } from "./basic-settings/delivery-durations/wheelDeliveryDurations.component";
// import { WheelPaymentMethodsComponent } from './basic-settings/payment-methods/wheelPaymentMethods.component';
import { WheelTaxesComponent } from "./basic-settings/taxes/wheelTaxes.component";
import { WheelLocationsComponent } from "./locations/wheelLocations.component";
import { WheelDepartmentsComponent } from "./departments/wheelDepartments.component";
import { WheelSetupsComponent } from "./wheel-setup/wheelSetups.component";
import { CreateOrEditTemplateComponent } from "./dynamic-pages/create-or-edit-template.component";
// import { EditWheelPaymentMethodComponent } from './basic-settings/payment-methods/edit-wheel-payment-method.component';
import { WheelScreenMenuComponent } from "./screen-menus/wheel-screen-menu.component";
import { CreateOrEditWheelScreenMenuComponent } from "./screen-menus/create-or-edit-wheel-screen-menu.component";
import { CreateOrEditWheelScreenMenuSectionComponent } from "./screen-menus/create-or-edit-wheel-screen-menu-section.component";
import { WheelScreenMenuItemComponent } from "./screen-menus/wheel-screen-menu-item.component";
import { CreateOrEditWheelScreenItemSectionComponent } from "./screen-menus/create-or-edit-wheel-screen-menu-item.component";
import { CreateOrEditWheelDepartmentModalComponent } from "./departments/create-or-edit-wheelDepartment-modal.component";
import { CreateOrEditWheelLocationComponent } from "./locations/create-or-edit-wheelLocation.component";
import { CreateOrEditOpeningHoursComponent } from "./basic-settings/opening-hours/create-or-edit-opening-hours.component";
import { WheelUiCustomizationComponent } from "./ui-themes/wheel-ui-customization.component";
import { WheelTablesComponent } from "./tables/wheelTables.component";
import { CreateOrEditWheelTablesModalComponent } from "./tables/create-or-edit-wheelTables-modal.component";
import { OrdersComponent } from "./orders/orders.component";
import { OrderSettingsComponent } from "./basic-settings/order-settings/order-settings.component";
import { FaqsComponent } from "@app/tiffin/faqs/faqs.component";
import { PromotionDynamicComponent } from "@app/tiffin/promotion-dynamic/promotion-dynamic.component";
import { PromotionDynamicCreateEditComponent } from "@app/tiffin/promotion-dynamic/promotion-dynamic-create-edit/promotion-dynamic-create-edit.component";
import { PromotionDynamicViewComponent } from "@app/tiffin/promotion-dynamic/promotion-dynamic-view/promotion-dynamic-view.component";
import { CustomersComponent } from "@app/tiffin/customers/customers.component";
import { CreateOrEditCustomerComponent } from "@app/tiffin/customers/create-or-edit-customer.component";
import { CreateOrEditWheelSetupComponent } from "./wheel-setup/create-or-edit-wheel-setup.component";
import { ProductSoldoutMenuComponent } from "./product-soldout/product-soldout-menu.component";
import { CreateOrEditProductSoldoutItemSectionComponent } from "./product-soldout/create-or-edit-product-soldout-menu-item.component";
import { RecommendationMenuComponent } from "./recommendation-menu/recommendation-menu.component";
import { CreateOrEditRecommendationItemSectionComponent } from "./recommendation-menu/create-or-edit-recommendation-menu-item.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "basic-settings/service-fees",
                        component: ServiceFeesComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "basic-settings/delivery-zones",
                        component: DeliveryZonesComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "basic-settings/opening-hours",
                        component: OpeningHoursComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "basic-settings/opening-hours/create-or-edit/:id",
                        component: CreateOrEditOpeningHoursComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "basic-settings/delivery-durations",
                        component: WheelDeliveryDurationsComponent,
                        data: { permission: "" },
                    },
                    // { path: 'basic-settings/payment-methods', component: WheelPaymentMethodsComponent, data: {permission: ''}},
                    // { path: 'basic-settings/payment-methods/edit/:systemName', component: EditWheelPaymentMethodComponent, data: {permission: ''}},
                    {
                        path: "basic-settings/taxes",
                        component: WheelTaxesComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "basic-settings/order-settings",
                        component: OrderSettingsComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "locations",
                        component: WheelLocationsComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "locations/create-or-edit/:id",
                        component: CreateOrEditWheelLocationComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "dynamic-pages",
                        component: TemplatesComponent,
                        data: { permission: "Pages.WheelDynamicPages" },
                    },
                    {
                        path: "dynamic-pages/create-or-edit/:id",
                        component: CreateOrEditTemplateComponent,
                        data: { permission: "Pages.WheelDynamicPages" },
                    },
                    {
                        path: "screen-menus",
                        component: WheelScreenMenuComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menus/create-or-edit/:id",
                        component: CreateOrEditWheelScreenMenuComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menu-items/:id",
                        component: WheelScreenMenuItemComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menu-items/:id/create-or-edit/:sectionId",
                        component: CreateOrEditWheelScreenMenuSectionComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menu-items/:id/section/:parentSectionId/create-or-edit/:sectionId",
                        component: CreateOrEditWheelScreenMenuSectionComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menu-items/:id/create-or-edit-item/:itemId/:type",
                        component: CreateOrEditWheelScreenItemSectionComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    {
                        path: "screen-menu-items/:id/section/:parentSectionId/create-or-edit-item/:itemId/:type",
                        component: CreateOrEditWheelScreenItemSectionComponent,
                        data: { permission: "Pages.WheelScreenMenus" },
                    },
                    // {
                    //     path: "screen-menu-items/:id/create-or-edit-combo/:itemId/:type",
                    //     // component: CreateOrEditWheelScreenComboItemComponent,
                    //     component: CreateOrEditWheelScreenItemSectionComponent,
                    //     data: { permission: "Pages.WheelScreenMenus" },
                    // },
                    // {
                    //     path: "screen-menu-items/:id/section/:parentSectionId/create-or-edit-combo/:itemId/:type",
                    //     component: CreateOrEditWheelScreenItemSectionComponent,
                    //     data: { permission: "Pages.WheelScreenMenus" },
                    // },
                    {
                        path: "departments",
                        component: WheelDepartmentsComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "departments/create-or-edit/:id",
                        component: CreateOrEditWheelDepartmentModalComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "tables",
                        component: WheelTablesComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "tables/create-or-edit/:id",
                        component: CreateOrEditWheelTablesModalComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "setup",
                        component: WheelSetupsComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "setup/create-or-edit/:id",
                        component: CreateOrEditWheelSetupComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "faqs",
                        component: FaqsComponent,
                        data: { permission: "" },
                    },

                    {
                        path: "themes",
                        component: WheelUiCustomizationComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "orders",
                        component: OrdersComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "manage/promotion-dynamic",
                        component: PromotionDynamicComponent,
                        data: { permission: "Pages.WheelPromotions" },
                    },
                    {
                        path: "manage/promotion-dynamic/create-or-update/:id",
                        component: PromotionDynamicCreateEditComponent,
                        data: { permission: "Pages.WheelPromotions" },
                    },
                    {
                        path: "manage/promotion-dynamic/view/:id",
                        component: PromotionDynamicViewComponent,
                        data: { permission: "Pages.WheelPromotions" },
                    },
                    {
                        path: "customers",
                        component: CustomersComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "customers/create-or-update/:id",
                        component: CreateOrEditCustomerComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "reserve/seat-map",
                        component: SeatmapComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "reserve/reservation",
                        component: ReservationComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "reserve/reservation-detail",
                        component: ReservetionDetailComponent,
                        data: { permission: "" },
                    },
                    {
                        path: "product-soldout",
                        component: ProductSoldoutMenuComponent,
                        data: { permission: "Pages.WheelProductSoldoutMenus" },
                    },
                    {
                        path: "product-soldout/create-or-edit/:itemId",
                        component: CreateOrEditProductSoldoutItemSectionComponent,
                        data: { permission: "Pages.WheelProductSoldoutMenus" },
                    },
                    {
                        path: "recommendation-menus",
                        component: RecommendationMenuComponent,
                        data: { permission: "Pages.WheelRecommendationMenu" },
                    },
                    {
                        path: "recommendation-menus/create-or-edit/:itemId",
                        component: CreateOrEditRecommendationItemSectionComponent,
                        data: { permission: "Pages.WheelRecommendationMenu" },
                    },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class WheelRoutingModule {}

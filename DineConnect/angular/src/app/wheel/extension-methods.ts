interface Array<T> {
    remove(item: T): Array<T>
}

Array.prototype.remove = function (item) {
    var index = this.findIndex(o => o == item);
    if (index > -1)
        this.splice(index, 1);

    return this;
}
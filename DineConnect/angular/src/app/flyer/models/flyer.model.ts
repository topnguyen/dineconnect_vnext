import { FlyerLayout } from "../enum/flyer.enum";

export interface Flyer {
    layout: FlyerLayout,
    flyerQRCode: {
        color: ColorType,
        url: string
    },
    flyerOrderAhead: {
        color: ColorType,
        headline: string;
    },
    flyerOnlineOrdering: {
        color: ColorType,
        frontHeadline: string;
        backHeadline: string;
        orderUrl: string;
        croppedFrontImage: string;
        croppedBackImage: string;
    }
    base64File: string;
}

export interface ColorType {
    id: string;
    color: string;
    name: string;
    file: string;
}
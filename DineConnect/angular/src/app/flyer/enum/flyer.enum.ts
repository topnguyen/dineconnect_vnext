export enum FlyerLayout {
    OnlineOrdering = 'online-ordering',
    OrderAheadImg = 'order-ahead-img',
    DineInFlyer = 'dine-in-flyer'
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AddFlyerComponent } from './components/add-flyer/add-flyer.component';
import { FlyersComponent } from './components/flyers/flyers.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    /* TODO: permission */
                    { path: 'your-flyers', component: FlyersComponent, data: { permission: 'Pages.Tenant.Connect' } },
                    { path: 'add', component: AddFlyerComponent, data: { permission: 'Pages.Tenant.Connect' } },
                    { path: ':id', component: AddFlyerComponent, data: { permission: 'Pages.Tenant.Connect' } },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})

export class FlyerRoutingModule { }

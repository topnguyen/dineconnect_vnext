import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { BACKGROUND_IMAGE } from '@app/flyer/flyer.constant';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: 'app-edit-front-image-modal',
    templateUrl: './edit-front-image-modal.component.html',
    styleUrls: ['./edit-front-image-modal.component.scss'],
    animations: [appModuleAnimation()]
})
export class EditFrontImageModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('modal', { static: true }) modal: ModalDirective;

    @Output() setImage = new EventEmitter<any>();

    imageChangedEvent: any = '';
    imageDefault: any = '';
    croppedImage: any = '';
    side: 'back' | 'front';

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {}

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
        console.log(this.imageChangedEvent);
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }
    imageLoaded(image: HTMLImageElement) {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }

    show(side: 'back' | 'front') {
        this.side = side;
        this.modal.show();
    }

    shown() {
        this.imageDefault = base64ToFile(BACKGROUND_IMAGE);
    }

    save() {
        this.setImage.emit({
            side: this.side,
            croppedImage: this.croppedImage
        });
        this.close();
    }

    close() {
        this.modal.hide();
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlyerLayout } from '@app/flyer/enum/flyer.enum';
import { FLYER_LAYOUT } from '@app/flyer/flyer.constant';
import { Flyer } from '@app/flyer/models/flyer.model';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FlyerServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-flyers',
    templateUrl: './flyers.component.html',
    styleUrls: ['./flyers.component.scss', '../add-flyer/add-flyer.component.scss'],
    animations: [appModuleAnimation()]
})
export class FlyersComponent extends AppComponentBase implements OnInit {
    @ViewChild('flyerTable', { static: true }) flyerTable: Table;
    @ViewChild('flyerPaginator', { static: true }) flyerPaginator: Paginator;

    filterText = '';
    isDeleted = false;

    

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _flyerServiceProxy: FlyerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {}

    getList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.flyerPaginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._flyerServiceProxy
            .getFlyers()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.items?.length || 0;
                this.primengTableHelper.records = result.items || [];
                this.primengTableHelper.records.forEach(item => {
                    item.file = FLYER_LAYOUT.find(flyer => item.content == flyer.id)?.file;
                    item.name = this.getflyerName(item.content);
                    item.flyer = JSON.parse(item.data);
                })
            });
    }

    delete(item: any) {
        this.message.confirm('you want to delete location ' + item.name, this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this.primengTableHelper.isLoading = true;
                this._flyerServiceProxy.deleteFlyer(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));
                    this.primengTableHelper.isLoading = false;
                    this.reloadPage();
                });
            }
        });
    }

    craeteOrUpdate(item?) {
        this._router.navigate(['/app/flyer/', item ? item.id : 'add']);
    }

    reloadPage(): void {
        this.flyerPaginator.changePage(this.flyerPaginator.getPage());
    }

    getflyerName(layout: FlyerLayout): string {
        let name = '';
        switch (layout) {
            case FlyerLayout.DineInFlyer: name = 'Flyer with QR codes for dine-in'
                break;
            case FlyerLayout.OnlineOrdering: name ='Online ordering flyer'
                break;
            case FlyerLayout.OrderAheadImg: name ='Order ahead flyer'
                break;
        }

        return name;
    }


    downloadBase64File(flyer: Flyer, name: string) {
        const downloadLink = document.createElement('a');
        document.body.appendChild(downloadLink);
    
        downloadLink.href = flyer.base64File;
        downloadLink.target = '_self';
        downloadLink.download = name + '.pdf';
        downloadLink.click(); 
    }

}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';

import { jsPDF } from 'jspdf';
import html2canvas, { Options } from 'html2canvas';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { FLYER_QR_CODE, FLYER_LAYOUT, FLYER_ORDER_AHEAD, ONLINE_ORDERING } from '@app/flyer/flyer.constant';
import { FlyerLayout } from '@app/flyer/enum/flyer.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { EditFrontImageModalComponent } from '../edit-front-image-modal/edit-front-image-modal.component';
import { Flyer } from '@app/flyer/models/flyer.model';
import { FlyerCreateDto, FlyerEditDto, FlyerServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-add-flyer',
    templateUrl: './add-flyer.component.html',
    styleUrls: ['./add-flyer.component.scss'],
    animations: [appModuleAnimation()]
})
export class AddFlyerComponent extends AppComponentBase implements OnInit {
    @ViewChild('editFrontImageModal', { static: false }) editFrontImageModal: EditFrontImageModalComponent;

    flyerId = +this._activatedRoute.snapshot.params['id'];
    flyerQRCodeColor = FLYER_QR_CODE[0];
    flyerOrderAheadColor = FLYER_ORDER_AHEAD[0];
    flyerOnlineOrderingColor = ONLINE_ORDERING[0];
    flyerLayout = FlyerLayout.OnlineOrdering;
    url = 'https://dineplan.com';
    headline = 'Want to have the steaming hot food waiting for you?';
    frontHeadline = 'Our food will leave you speechless...';
    backHeadline = '5% off total';
    orderUrl = '';
    side: 'back' | 'front' = 'front';
    croppedFrontImage: any = '';
    croppedBackImage: any = '';
    flyer: Flyer = {} as Flyer;
    loading = false;

    FLYER_QR_CODE = FLYER_QR_CODE;
    FLYER_ORDER_AHEAD = FLYER_ORDER_AHEAD;
    ONLINE_ORDERING = ONLINE_ORDERING;
    FLYER_LAYOUT = FLYER_LAYOUT;
    FlyerLayout = FlyerLayout;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _flyerServiceProxy: FlyerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        if (this.flyerId) {
            this._getFlyer();
        }
    }

    selectLayout() {
        this.flyer.layout = this.flyerLayout;
    }

    saveLayout() {
        this.loading = true;

        if (this.flyer.layout == FlyerLayout.DineInFlyer) {
            this.flyer = {
                ...this.flyer,
                flyerQRCode: {
                    color: this.flyerQRCodeColor,
                    url: this.url
                }
            };

            this.generatePdf(148, 210);
        } else if (this.flyer.layout == FlyerLayout.OrderAheadImg) {
            this.flyer = {
                ...this.flyer,
                flyerOrderAhead: {
                    color: this.flyerOrderAheadColor,
                    headline: this.headline
                }
            };

            this.generatePdf(99, 210);
        } else {
            this.flyer = {
                ...this.flyer,
                flyerOnlineOrdering: {
                    color: this.flyerOnlineOrderingColor,
                    frontHeadline: this.frontHeadline,
                    backHeadline: this.backHeadline,
                    orderUrl: this.orderUrl,
                    croppedFrontImage: this.croppedFrontImage,
                    croppedBackImage: this.croppedBackImage
                }
            };

            this.generatePdf(99, 210);
        }
    }

    back() {
        this._router.navigateByUrl('/app/flyer/your-flyers');
    }

    generatePdf(width: number, height: number) {
        const front = document.getElementById('html2Pdf');
        const options: Partial<Options> = { height: front.clientHeight, width: front.clientWidth };

        html2canvas(front, options).then((canvas1) => {
            if (this.flyerLayout === FlyerLayout.OnlineOrdering) {
                this.side = 'back';
                setTimeout(() => {
                    const back = document.getElementById('html2Pdf2');
                    html2canvas(back, options).then((canvas2) => {
                        this.side = 'front';
                        this.addImageToPDF(width, height, canvas1, canvas2);
                    });
                }, 100);
            } else {
                this.addImageToPDF(width, height, canvas1);
            }
        });
    }

    addImageToPDF(width: number, height: number, canvas1, canvas2?) {
        let doc = new jsPDF('p', 'mm', [width, height], true);
        let imgData = canvas1.toDataURL('image/PNG');
        let imgData2;
        if (canvas2) {
            imgData2 = canvas2.toDataURL('image/PNG');
        }
        doc.addImage(imgData, 'PNG', 0, 0, width, height, '1', 'SLOW');

        if (imgData2) {
            doc.addPage();
            doc.addImage(imgData2, 'PNG', 0, 0, width, height, '2', 'SLOW');
        }

        var reader = new FileReader();
        reader.readAsDataURL(doc.output('blob'));
        reader.onloadend = () => {
            this.flyer.base64File = reader.result.toString();

            this._saveFlyer();
        };
    }

    editImage(side) {
        this.editFrontImageModal.show(side);
    }

    onSetImage(event: { side: 'front' | 'back'; croppedImage: string }) {
        if (event.side == 'front') {
            this.croppedFrontImage = event.croppedImage;
        } else {
            this.croppedBackImage = event.croppedImage;
        }
    }

    private _saveFlyer() {
        
        if (this.flyerId) {
            const body = new FlyerEditDto({
                id: this.flyerId,
                content: this.flyer.layout,
                data: JSON.stringify(this.flyer)
            });

            this._flyerServiceProxy
            .updateFlyer(body)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe((response) => {
                this._router.navigateByUrl('app/flyer/your-flyers');
                this.notify.success(this.l('SuccessfullySaved'));
            });
        } else {
            const body = new FlyerCreateDto({
                content: this.flyer.layout,
                data: JSON.stringify(this.flyer)
            });

            this._flyerServiceProxy
            .createFlyer(body)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe((response) => {
                this._router.navigateByUrl('app/flyer/your-flyers');
                this.notify.success(this.l('SuccessfullySaved'));
            });
        }
    }

    private _getFlyer() {
        this.loading = true;
        this._flyerServiceProxy
            .getFlyerForEdit(this.flyerId)
            .pipe(finalize(() => (this.loading = false)))
            .subscribe((response) => {
                this.flyerLayout = response.content as FlyerLayout;
                this.flyer = JSON.parse(response.data);

                switch (this.flyerLayout) {
                    case FlyerLayout.DineInFlyer:
                        {
                            this.url = this.flyer.flyerQRCode.url;
                            this.flyerQRCodeColor = FLYER_QR_CODE.find((item) => item.id == this.flyer.flyerQRCode.color.id);
                        }
                        break;
                    case FlyerLayout.OrderAheadImg:
                        {
                            this.headline = this.flyer.flyerOrderAhead.headline;
                            this.flyerOrderAheadColor = FLYER_ORDER_AHEAD.find((item) => item.id == this.flyer.flyerOrderAhead.color.id);
                        }
                        break;
                    case FlyerLayout.OnlineOrdering:
                        {
                            this.flyerOnlineOrderingColor = ONLINE_ORDERING.find(
                                (item) => item.id == this.flyer.flyerOnlineOrdering.color.id
                            );
                            this.frontHeadline = this.flyer.flyerOnlineOrdering.frontHeadline;
                            this.backHeadline = this.flyer.flyerOnlineOrdering.backHeadline;
                            this.orderUrl = this.flyer.flyerOnlineOrdering.orderUrl;
                            this.croppedFrontImage = this.flyer.flyerOnlineOrdering.croppedFrontImage;
                            this.croppedBackImage = this.flyer.flyerOnlineOrdering.croppedBackImage;
                        }
                        break;
                }
            });
    }
}

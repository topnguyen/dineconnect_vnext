import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlyersComponent } from './components/flyers/flyers.component';
import { AddFlyerComponent } from './components/add-flyer/add-flyer.component';
import { FlyerRoutingModule } from './flyer-routing.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { PaginatorModule, TableModule } from 'primeng';
import { UtilsModule } from '@shared/utils/utils.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { ImageCropperModule } from 'ngx-image-cropper';
import { EditFrontImageModalComponent } from './components/edit-front-image-modal/edit-front-image-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [FlyersComponent, AddFlyerComponent, EditFrontImageModalComponent],
  imports: [
    CommonModule,
    FlyerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UtilsModule,
    ModalModule,
    AppBsModalModule,
    BsDropdownModule,
    AppCommonModule,
    PaginatorModule,
    TableModule,
    QRCodeModule,
    ImageCropperModule
  ]
})
export class FlyerModule { }

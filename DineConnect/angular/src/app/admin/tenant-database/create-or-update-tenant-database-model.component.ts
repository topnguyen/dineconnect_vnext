import { Component, Injector, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TenantDatabaseDto, TenantDatabaseListDto, TenantDatabaseServiceProxy, CreateOrUpdateTenantDatabaseInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap/modal';


@Component({
    templateUrl: './create-or-update-tenant-database-model.component.html',
    selector: 'createOrEditTenantDatabaseModal',
})

export class CreateOrUpdateTenantDatabaseComponent extends AppComponentBase {
    
    @ViewChild('createModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    tenantDatabase: CreateOrUpdateTenantDatabaseInput;

    constructor(
        injector: Injector,
        private tenantDatabaseService: TenantDatabaseServiceProxy
    ) {
        super(injector);
    }

    show(id?: number) {
        this.tenantDatabase = new CreateOrUpdateTenantDatabaseInput();

        if (!id) {
            this.active = true;
            this.modal.show();
        }
        else {
            this.tenantDatabaseService.getTenantDatabaseForEdit(id)
            .subscribe(res => {
                this.tenantDatabase = res
                this.active = true;
                this.modal.show();
            });
        }
       
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save() {
        this.saving = true;

        this.tenantDatabaseService.createOrUpdateTenantDatabase(this.tenantDatabase)
            .pipe(finalize(() => this.saving = false))
                .subscribe(() => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

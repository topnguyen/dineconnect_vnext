import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TenantDatabaseDto, TenantDatabaseListDto, TenantDatabaseServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateOrUpdateTenantDatabaseComponent } from './create-or-update-tenant-database-model.component';
import { LazyLoadEvent, Table, Paginator } from 'primeng';

@Component({
    templateUrl: './tenant-database.component.html',
    animations: [appModuleAnimation()]
})

export class TenantDatabaseComponent extends AppComponentBase implements OnInit {

    @ViewChild('dataTable', {static: true}) dataTable: Table;
    @ViewChild('paginator', {static: true}) paginator: Paginator;
    @ViewChild('createOrEditTenantDatabaseModal', {static: true}) createOrEditTenantDatabaseModal: CreateOrUpdateTenantDatabaseComponent;

    tenantDatabases: TenantDatabaseListDto[] = [];
    filter: string = ''

    constructor(
        injector: Injector,
        private tenantDatabaseService: TenantDatabaseServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
    }

    getTenantDatabases(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this.tenantDatabaseService.getTenantDatabases(
            this.filter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, event),
            this.primengTableHelper.getSkipCount(this.paginator, event)
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        })
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEdit(id?: number) {
        this.createOrEditTenantDatabaseModal.show();
    }

    deleteTenantDatabase(model: TenantDatabaseListDto) {
        this.message.confirm(
            this.l('TenantDatabaseDeleteWarningMessage', model.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this.tenantDatabaseService.deleteTenantDatabase(model.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
}

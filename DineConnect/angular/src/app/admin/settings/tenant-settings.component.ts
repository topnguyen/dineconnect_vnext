import { Component, Injector, OnInit } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    SettingScopes,
    SendTestEmailInput,
    CompanyLogoInfo,
    ComboboxItemDto,
    ReferralEarningType,
    TiffinPromotionServiceProxy,
    PromotionListDto,
    TiffinProductOffersServiceProxy,
    TiffinProductOfferDto,
    SignUpFormSettingDto,
    CommonServiceProxy,
    ConnectSettingsDto,
    ConnectSyncJobArgs,
    ConnectSetupJobServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { TenantSettingsEditDto, TenantSettingsServiceProxy } from "@shared/service-proxies/service-proxies-nswag";
import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { finalize, map } from "rxjs/operators";
import * as moment from "moment";
import { IAjaxResponse, TokenService } from "abp-ng2-module";
import { forkJoin, Observable } from "rxjs";
import { DatePipe } from "@angular/common";

@Component({
    templateUrl: "./tenant-settings.component.html",
    styleUrls: ["./tenant-settings.component.scss"],
    animations: [appModuleAnimation()],
})
export class TenantSettingsComponent
    extends AppComponentBase
    implements OnInit
{
    saving = false;
    usingDefaultTimeZone = false;
    initialTimeZone: string = null;
    testEmailAddress: string = undefined;
    tContent: string;
    isMultiTenancyEnabled: boolean = this.multiTenancy.isEnabled;
    isTiffinEnabled = this.feature.isEnabled("DinePlan.DineConnect.Tiffins");
    isConnectEnabled = this.feature.isEnabled("DinePlan.DineConnect.Connect");
    isCliqueEnabled = this.feature.isEnabled("DinePlan.DineConnect.Clique");

    showTimezoneSelection: boolean =
        abp.clock.provider.supportsMultipleTimezone;
    activeTabIndex: number = abp.clock.provider.supportsMultipleTimezone
        ? 0
        : 1;
    loading = false;
    settings: TenantSettingsEditDto = undefined;

    logoUploader: FileUploader;
    logoCompanyUploader: FileUploader;
    customCssUploader: FileUploader;
    faviconUploader: FileUploader;
    popupImageUploader: FileUploader;
    hintImageUploader: FileUploader;

    favIcon: HTMLLinkElement = document.querySelector("#appIcon");
    herfIcon = "";
    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl;

    urlFavIcon =
        this.remoteServiceBaseUrl +
        "/TenantCustomization/GetFavicon?tenantId=" +
        this.appSession.tenant.id;
    ReferralEarningType = ReferralEarningType;

    defaultTimezoneScope: SettingScopes = SettingScopes.Tenant;
    orderCutOffTime: Date;
    cliqueOrderCutOffTime: Date;
    referralEarningTypes: ComboboxItemDto[] = [];
    promotions: PromotionListDto[] = [];
    selectedPromotion: PromotionListDto;
    productOffers: TiffinProductOfferDto[] = [];
    selectedProductOffer: TiffinProductOfferDto;

    lstDay: ComboboxItemDto[] = [];
    scname: string = "";
    scStartTime: Date;
    scEndTime: Date;
    schedules: ISchedule[] = [];
    refdays = [
        {
            value: "1",
            displayText: "Monday",
            isSelected: false,
        },
        {
            value: "2",
            displayText: "Tuesday",
            isSelected: false,
        },
        {
            value: "3",
            displayText: "Wednesday",
            isSelected: false,
        },
        {
            value: "4",
            displayText: "Thursday",
            isSelected: false,
        },
        {
            value: "5",
            displayText: "Friday",
            isSelected: false,
        },
        {
            value: "6",
            displayText: "Saturday",
            isSelected: false,
        },
        {
            value: "0",
            displayText: "Sunday",
            isSelected: false,
        },
    ];
    weekDay = [];
    weekEnd = [];
    isAddressObligatory: boolean;
    constructor(
        injector: Injector,
        private _tenantSettingsService: TenantSettingsServiceProxy,
        private _tokenService: TokenService,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
        private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
        private _datePipe: DatePipe,
        private _commonService: CommonServiceProxy,
        private _jobService: ConnectSetupJobServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.testEmailAddress = this.appSession.user.emailAddress;
        this.getReferralEarningType();
        this.getMasterDate();
        this.initUploaders();
        this.getListDay();
    }

    getMasterDate() {
        forkJoin([this.getProductOffers$(), this.getPromotions$()]).subscribe(
            (result) => {
                this.getSettings();
            }
        );
    }

    getListDay() {
        this._commonService.getDays().subscribe((result) => {
            this.lstDay = result;
        });
    }

    getPromotions$(event?: { query: string }): Observable<any> {
        const isAutoAttach = true;
        return this._tiffinPromotionServiceProxy
            .getPromotions(
                event ? event.query : undefined,
                undefined,
                undefined,
                undefined,
                isAutoAttach,
                "name",
                0,
                1000
            )
            .pipe(
                map((result) => {
                    this.promotions = result.items;
                    return result;
                })
            );
    }

    getPromotions(event?: { query: string }) {
        this.getPromotions$(event).subscribe((result) => {
            this.promotions = result.items;
        });
    }

    getProductOffers$(event?: { query: string }): Observable<any> {
        return this._tiffinProductOffersServiceProxy
            .getAll(
                event ? event.query : undefined,
                undefined,
                undefined,
                "name",
                0,
                100
            )
            .pipe(
                map((result) => {
                    this.productOffers = result.items.map(
                        (item) => item.tiffinProductOffer
                    );
                    return result;
                })
            );
    }

    getProductOffers(event?: { query: string }) {
        this.getProductOffers$(event).subscribe((result) => {
            this.productOffers = result.items.map(
                (item) => item.tiffinProductOffer
            );
        });
    }

    getReferralEarningType() {
        this.referralEarningTypes = [
            ComboboxItemDto.fromJS({
                value: ReferralEarningType.ProductOffer,
                displayText: this.l("ProductOffer"),
            }),
            ComboboxItemDto.fromJS({
                value: ReferralEarningType.Promotion,
                displayText: this.l("AutoAttachPromotion"),
            }),
        ];
    }

    getSettings(): void {
        this.loading = true;
        this._tenantSettingsService
            .getAllSettings()
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result: TenantSettingsEditDto) => {
                this.settings = result;
                this.tContent = this.settings.termsAndCondition.content;
                if (
                    this.settings.tenantMealSetting.orderCutOffTime !==
                    undefined
                ) {
                    this.orderCutOffTime =
                        this.settings.tenantMealSetting.orderCutOffTime.toDate();
                }
                if (this.settings.general) {
                    this.initialTimeZone = this.settings.general.timezone;
                    this.usingDefaultTimeZone =
                        this.settings.general.timezoneForComparison ===
                        abp.setting.values["Abp.Timing.TimeZone"];
                }

                if (
                    this.settings.tenantCliqueMealSetting.orderCutOffTime !==
                    undefined
                ) {
                    this.cliqueOrderCutOffTime =
                        this.settings.tenantCliqueMealSetting.orderCutOffTime.toDate();
                }

                if (
                    this.settings.referralEarning.referralEarnType ===
                    ReferralEarningType.ProductOffer
                ) {
                    this.selectedProductOffer = this.productOffers.find(
                        (offer) =>
                            offer.id ===
                            this.settings.referralEarning.referralEarnValue
                    );
                    this.selectedPromotion = null;
                } else if (
                    this.settings.referralEarning.referralEarnType ===
                    ReferralEarningType.Promotion
                ) {
                    this.selectedPromotion = this.promotions.find(
                        (promotion) =>
                            promotion.id ===
                            this.settings.referralEarning.referralEarnValue
                    );
                    this.selectedProductOffer = null;
                }
                if (this.settings.connect) {
                    this.weekDay = JSON.parse(this.settings.connect.weekDay);
                    this.weekEnd = JSON.parse(this.settings.connect.weekEnd);
                    this.schedules = JSON.parse(
                        this.settings.connect.schedules
                    );
                }
            });
    }

    initUploaders(): void {
        this.logoUploader = this.createUploader(
            "/ImageUploader/UploadLogo",
            (result) => {
                console.log("result", result);
                this.appSession.tenant.logoFileType = result.fileType;
                this.appSession.tenant.logoId = result.id;
            }
        );

        this.logoCompanyUploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                let logoInfo = new CompanyLogoInfo();
                logoInfo.uploadedImageToken = result.fileToken;
                logoInfo.name = result.fileName;
                this._tenantSettingsService
                    .createCompanyLogo(logoInfo)
                    .subscribe((url) => {
                        console.log("file", url);
                        this.settings.companySetting.companyLogoUrl = url;
                    });
            }
        );

        this.popupImageUploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                this.settings.tiffinPopup = result.url;
            }
        );

        this.hintImageUploader = this.createUploader(
            "/ImageUploader/UploadImage",
            (result) => {
                if (this.settings.signUpForm) {
                    this.settings.signUpForm.hintImageUrl = result.url;
                } else {
                    this.settings.signUpForm = new SignUpFormSettingDto({
                        hintImageUrl: result.url,
                    });
                }
            }
        );

        this.customCssUploader = this.createUploader(
            "/TenantCustomization/UploadCustomCss",
            (result) => {
                this.appSession.tenant.customCssId = result.id;

                let oldTenantCustomCss =
                    document.getElementById("TenantCustomCss");
                if (oldTenantCustomCss) {
                    oldTenantCustomCss.remove();
                }

                let tenantCustomCss = document.createElement("link");
                tenantCustomCss.setAttribute("id", "TenantCustomCss");
                tenantCustomCss.setAttribute("rel", "stylesheet");
                tenantCustomCss.setAttribute(
                    "href",
                    AppConsts.remoteServiceBaseUrl +
                        "/TenantCustomization/GetCustomCss?tenantId=" +
                        this.appSession.tenant.id
                );
                document.head.appendChild(tenantCustomCss);
            }
        );

        this.faviconUploader = this.createUploader(
            "/ImageUploader/UploadFavicon",
            (result) => {
                this.appSession.tenant.faviconFileType = result.fileType;
                this.appSession.tenant.faviconId = result.id;
                this.herfIcon =
                    this.remoteServiceBaseUrl +
                    "/TenantCustomization/GetFavicon?tenantId=" +
                    this.appSession.tenant.id +
                    "&id=" +
                    this.appSession.tenant.faviconId;
                this.favIcon.href = this.herfIcon;
                this.urlFavIcon = this.urlFavIcon + "&random=" + Math.random();
            }
        );
    }

    createUploader(url: string, success?: (result: any) => void): FileUploader {
        const uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + url,
        });

        uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l("SavedSuccessfully"));
                if (success) {
                    success(ajaxResponse.result);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.authToken = "Bearer " + this._tokenService.getToken();
        uploaderOptions.removeAfterUpload = true;
        uploader.setOptions(uploaderOptions);
        return uploader;
    }

    uploadLogo(): void {
        this.logoUploader.uploadAll();
    }

    uploadCustomCss(): void {
        this.customCssUploader.uploadAll();
    }

    uploadFavicon(): void {
        this.faviconUploader.uploadAll();
    }

    clearLogo(): void {
        this._tenantSettingsService.clearLogo().subscribe(() => {
            this.appSession.tenant.logoFileType = null;
            this.appSession.tenant.logoId = null;
            this.notify.success(this.l("ClearedSuccessfully"));
        });
    }
    uploadLogoCompany(): void {
        this.logoCompanyUploader.uploadAll();
    }

    uploadPopupImage(): void {
        this.popupImageUploader.uploadAll();
    }

    uploadHintImage(): void {
        this.hintImageUploader.uploadAll();
    }

    clearCustomCss(): void {
        this._tenantSettingsService.clearCustomCss().subscribe(() => {
            this.appSession.tenant.customCssId = null;

            let oldTenantCustomCss = document.getElementById("TenantCustomCss");
            if (oldTenantCustomCss) {
                oldTenantCustomCss.remove();
            }

            this.notify.success(this.l("ClearedSuccessfully"));
        });
    }

    clearFavicon(): void {
        /* Property 'clearFavicon' does not exist on type 'TenantSettingsServiceProxy' */
        // this._tenantSettingsService.clearFavicon().subscribe(() => {
        //     this.appSession.tenant.faviconFileType = null;
        //     this.appSession.tenant.faviconId = null;
        //     this.notify.success(this.l('ClearedSuccessfully'));
        //     this.favIcon.href ="favicon.ico";
        // });
    }

    saveAll(): void {
        this.saving = true;
        this.settings.tenantMealSetting.orderCutOffTime = moment(
            this.orderCutOffTime
        );
        this.settings.tenantCliqueMealSetting.orderCutOffTime = moment(
            this.cliqueOrderCutOffTime
        );
        this.settings.termsAndCondition.content = this.tContent;

        // tslint:disable-next-line:triple-equals
        if (
            this.settings.referralEarning.referralEarnType ==
                ReferralEarningType.ProductOffer &&
            this.selectedProductOffer
        ) {
            this.settings.referralEarning.referralEarnValue =
                this.selectedProductOffer.id;
            // tslint:disable-next-line:triple-equals
        } else if (
            this.settings.referralEarning.referralEarnType ==
                ReferralEarningType.Promotion &&
            this.selectedPromotion
        ) {
            this.settings.referralEarning.referralEarnValue =
                this.selectedPromotion.id;
        }

        /* Validation FileDateTimeFormat*/
        if (this.isInValidFormatDate) {
            this.saving = false;
            return;
        }

        if (!this.settings.connect) {
            this.settings.connect = new ConnectSettingsDto();
        }

        this.settings.connect.weekDay = JSON.stringify(this.weekDay);
        this.settings.connect.weekEnd = JSON.stringify(this.weekEnd);
        this.settings.connect.schedules = JSON.stringify(this.schedules);

        this._tenantSettingsService
            .updateAllSettings(this.settings)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));

                if (
                    abp.clock.provider.supportsMultipleTimezone &&
                    this.usingDefaultTimeZone &&
                    this.initialTimeZone !== this.settings.general.timezone
                ) {
                    this.message
                        .info(
                            this.l(
                                "TimeZoneSettingChangedRefreshPageNotification"
                            )
                        )
                        .then(() => {
                            window.location.reload();
                        });
                }
            });
    }

    get isInValidFormatDate(): boolean {
        if (this.settings.fileDateTimeFormat.fileDateTimeFormat) {
            const fileDateTimeFormat = this._datePipe.transform(
                new Date(),
                this.settings.fileDateTimeFormat.fileDateTimeFormat
            );

            if (!(new Date(fileDateTimeFormat) instanceof Date)) {
                this.notify.error(
                    `${this.l("FileDateTimeFormat")} ${this.l(
                        "InvalidPattern"
                    )}`
                );
                return true;
            }
        }

        if (this.settings.fileDateTimeFormat.fileDateFormat) {
            const fileDateFormat = this._datePipe.transform(
                new Date(),
                this.settings.fileDateTimeFormat.fileDateFormat
            );

            if (this._isInvalidDate(fileDateFormat)) {
                this.notify.error(
                    `${this.l("FileDateFormat")} ${this.l("InvalidPattern")}`
                );
                return true;
            }
        }

        if (this.settings.fileDateTimeFormat.fileDayMonthFormat) {
            const fileDayMonthFormat = this._datePipe.transform(
                new Date(),
                this.settings.fileDateTimeFormat.fileDayMonthFormat
            );

            if (this._isInvalidDate(fileDayMonthFormat)) {
                this.notify.error(
                    `${this.l("FileDayMonthFormat")} ${this.l(
                        "InvalidPattern"
                    )}`
                );
                return true;
            }
        }
        return false;
    }

    sendTestEmail(): void {
        const input = new SendTestEmailInput();
        input.emailAddress = this.testEmailAddress;
        this._tenantSettingsService.sendTestEmail(input).subscribe((result) => {
            this.notify.success(this.l("TestEmailSentSuccessfully"));
        });
    }

    private _isInvalidDate(dateString: string) {
        const patt1 = /[a-zA-Z!@#\$%\^\&*\)\(+=.,_]/g;
        const invalidCharacter = dateString.match(patt1);
        return !!invalidCharacter;
    }

    addSchedule() {
        const newSchedule: ISchedule = {
            name: this.scname,
            startHour: this.scStartTime.getHours(),
            endHour: this.scEndTime.getHours(),
            startMinute: this.scStartTime.getMinutes(),
            endMinute: this.scEndTime.getMinutes(),
        };

        if (!newSchedule.name || newSchedule.name.trim().length == 0) {
            return;
        }
        if (
            !newSchedule.startHour ||
            !newSchedule.endHour ||
            newSchedule.startHour > newSchedule.endHour
        ) {
            this.notify.error(this.l("WrongDateRange"));
            return;
        }

        if (!this.schedules) {
            this.schedules = [];
        }

        this.schedules.push(newSchedule);
        this.scname = "";
    }
    syncConnect() {

        const jobArgs = new ConnectSyncJobArgs();

        jobArgs.tenantId =  "1";
        jobArgs.tenantName =  "Cedele";
        jobArgs.tenantPassword =  "123qwe";
        jobArgs.tenantUrl = "http://cedele.dineconnect.net";
        jobArgs.tenantUser = "admin";

        this._jobService.apiSyncDineConnect(jobArgs).subscribe((result) => {
            this.notify.success(this.l("JobStartedSuccessfully"));
        });
    }

    removeSchedule(index) {
        this.schedules.splice(index, 1);
    }
}

interface ISchedule {
    name: string;
    startHour: number;
    endHour: number;
    startMinute: number;
    endMinute: number;
}

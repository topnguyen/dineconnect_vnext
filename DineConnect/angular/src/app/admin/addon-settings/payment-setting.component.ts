import { Component, OnInit, Output, EventEmitter, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { CreateOrEditAddonDto, AddonSettingsServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "payment-setting",
    templateUrl: "./payment-setting.component.html",
    styleUrls: ["./payment-setting.component.scss"],
})
export class PaymentSettingComponent extends AppComponentBase implements OnInit {
    @Output() back: EventEmitter<boolean> = new EventEmitter<boolean>();

    saving = false;
    addonType = 0;

    paymentSettings: CreateOrEditAddonDto[] = [];
    serviceChargeSetting = new CreateOrEditAddonDto();

    serviceChargeName = "";
    serviceChargePercentage: number;

    constructor(injector: Injector, private _addonService: AddonSettingsServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.init();
    }

    init() {
        this.checkNullForSettinng();
        this._addonService.getPaymentSettingForEdit().subscribe((result) => {
            this.paymentSettings = result;
            this.getSettings();
        });
    }

    save() {
        this.serviceChargeSetting.name = "ServiceCharge";
        this.serviceChargeSetting.addonType = this.addonType;

        this.saving = true;
        let input = [];
        input = [this.serviceChargeSetting];

        this._addonService
            .createOrUpdateListAddon(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result) => {
                this.notify.success(this.l("SuccessfullySaved"));
            });
    }

    close() {
        this.back.emit(true);
    }

    addServiceCharge() {
        const charge = {
            name: this.serviceChargeName,
            percentage: this.serviceChargePercentage,
            omisePaymentUse: false,
            stripePaymentUse: false,
            adyenPaymentUse: false,
            paypalPaymentUse: false,
            googlePaymentUse: false,
        }

        if (!this.serviceChargeSetting.setting) {
            this.serviceChargeSetting.setting = [];
        }

        this.serviceChargeSetting.setting.push(charge);

        this.serviceChargeName = "";
        this.serviceChargePercentage = undefined;
    }

    removeServiceCharge(index) {
        this.serviceChargeSetting.setting.splice(index, 1);
    }

    private getSettings() {
        this.paymentSettings.map((item) => {
            switch (item.name) {
                case "ServiceCharge":
                    this.serviceChargeSetting = item;
                    break;

                default:
                    break;
            }
        });
        this.checkNullForSettinng();
    }

    private checkNullForSettinng() {
        if (!this.serviceChargeSetting.setting) {
            this.serviceChargeSetting.setting = [];
        }
    }
}

import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TenantSettingsEditDto, TenantSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { TokenService } from 'abp-ng2-module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addon-settings',
  templateUrl: './addon-settings.component.html',
  styleUrls: ['./addon-settings.component.scss'],
  animations: [appModuleAnimation()]
})
export class AddonSettingsComponent extends AppComponentBase implements OnInit {
  isShowMainScreen = true;

  isShowPayment = false;
  isShowEmail = false;
  isShowImage = false;

  testEmailAddress: string = undefined;

  loading = false;
  settings: TenantSettingsEditDto = undefined;

  ADDON_URL = {
    GRAP: 'app/admin/addons/grap',
    XERO: 'app/admin/addons/xero',
  }


  constructor(
    injector: Injector,
    private _router: Router,
    private _tenantSettingsService: TenantSettingsServiceProxy,
    private _tokenService: TokenService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.testEmailAddress = this.appSession.user.emailAddress;
    this.getSettings();
  }

  getSettings(): void {
    this.loading = true;
    // this._tenantSettingsService.getAllSettings()
    //   .pipe(finalize(() => { this.loading = false; }))
    //   .subscribe((result: TenantSettingsEditDto) => {
    //     this.settings = result;
    //   });
  }


  saveAll(): void {

  }


  viewSetting(type: number) {
    this.isShowMainScreen = false;
    this.isShowPayment = false;
    this.isShowEmail = false;
    this.isShowImage = false;

    switch (type) {
      case 0:
        this.isShowPayment = true;
        break;
      case 1:
        this.isShowImage = true;
        break;
      case 2:
        this.isShowEmail = true;
        break;

      default:
        break;
    }
  }

  navigateTo(url: string) {
    this._router.navigateByUrl(url);
  }

  back() {
    this.isShowMainScreen = true;
  }
}

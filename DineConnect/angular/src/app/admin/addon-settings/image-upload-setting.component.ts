import { Component, OnInit, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditAddonDto, AddonSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'image-upload-setting',
  templateUrl: './image-upload-setting.component.html',
  styleUrls: ['./image-upload-setting.component.scss']
})
export class ImageUploadSettingComponent extends AppComponentBase implements OnInit {
  @Output() saved: EventEmitter<boolean> = new EventEmitter<boolean>();

  saving = false;
  imageUploadSettings = new CreateOrEditAddonDto();
  addonType = 2;

  constructor(injector: Injector,
    private _addonService: AddonSettingsServiceProxy,

  ) {
    super(injector);
  }

  ngOnInit() {
    this.getSettings();
  }


  getSettings() {
    if (!this.imageUploadSettings.setting) {
      this.imageUploadSettings.setting = {};
    }
    this._addonService.getAddonSettingForEdit(this.addonType)
      .subscribe((result) => {
        this.imageUploadSettings = result;
        if (!this.imageUploadSettings.setting) {
          this.imageUploadSettings.setting = {};
        }
      });
  }

  save() {
    this.imageUploadSettings.addonType = this.addonType;
    this.imageUploadSettings.name = 'Image';
    this.saving = true;

    this._addonService.createOrUpdateAddonSetting(this.imageUploadSettings)
      .pipe(finalize(() => this.saving = false))
      .subscribe((result) => {
        this.notify.success(this.l('SuccessfullySaved'));
        this.saved.emit(true);
      });
  }

  close() {
    this.saved.emit(true);
  }
}

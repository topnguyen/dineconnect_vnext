﻿import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    WheelPaymentMethodsServiceProxy,
    EditWheelPaymentMethodDto,
    RecurringPaymentType,
    GetWheelPaymentMethodForEditOutput,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { TokenService, IAjaxResponse } from "abp-ng2-module";
import { AppConsts } from "@shared/AppConsts";

const SYSTEM_NAME = {
    Adyen: "Payments.Adyen",
    Stripe: "Payments.Stripe",
    Omise: "Payments.Omise",
    PayPal: "Payments.PayPal",
    Google: "Payments.Google",
    HitPay: "Payments.HitPay",
};

const FIELD_DEFAULT = {
    Adyen: [
        {
            key: "apiKey",
            type: "input",
            templateOptions: {
                label: "API Key",
            },
        },
        {
            key: "clientKey",
            type: "input",
            templateOptions: {
                label: "Client Key",
            },
        },
        {
            key: "merchantAccount",
            type: "input",
            templateOptions: {
                label: "Merchant Account",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Test", value: "test" },
                    { label: "Live", value: "live" },
                ],
            },
        },
    ],
    Stripe: [
        {
            key: "secretKey",
            type: "input",
            templateOptions: {
                label: "Secret Key",
            },
        },
        {
            key: "publishKey",
            type: "input",
            templateOptions: {
                label: "Publish Key",
            },
        },
    ],
    Omise: [
        {
            key: "secretKey",
            type: "input",
            templateOptions: {
                label: "Secret Key",
            },
        },
        {
            key: "publishKey",
            type: "input",
            templateOptions: {
                label: "Publish Key",
            },
        },
        {
            key: "enablePaynowQR",
            type: "checkbox",
            templateOptions: {
                label: "Enable Paynow QR",
            },
        },
    ],
    PayPal: [
        {
            key: "clientId",
            type: "input",
            templateOptions: {
                label: "ClientId",
            },
        },
        {
            key: "clientSecret",
            type: "input",
            templateOptions: {
                label: "Client Secret",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Sand Box", value: 0 },
                    { label: "Live", value: 1 },
                ],
            },
        },
    ],
    Google: [
        {
            key: "merchantId",
            type: "input",
            templateOptions: {
                label: "Merchant ID",
            },
        },
        {
            key: "merchantName",
            type: "input",
            templateOptions: {
                label: "Merchant Name",
            },
        },
        {
            key: "publicKey",
            type: "input",
            templateOptions: {
                label: "Public Key",
            },
        },
        {
            key: "privateKey",
            type: "input",
            templateOptions: {
                label: "Private Key",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Test", value: "TEST" },
                    { label: "Production", value: "PRODUCTION" },
                ],
            },
        },
    ],
    HitPay: [
        {
            key: "apiKey",
            type: "input",
            templateOptions: {
                label: "API Key",
            },
        },
        {
            key: "environment",
            type: "select",
            templateOptions: {
                label: "Environment",
                options: [
                    { label: "Sand Box", value: 0 },
                    { label: "Live", value: 1 },
                ],
            },
        },
        {
            key: "salt",
            type: "input",
            templateOptions: {
                label: "Salt",
            },
        },
    ],
};

@Component({
    selector: "edit-payment-method-modal",
    templateUrl: "./edit-payment-method.component.html",
    styleUrls: ["./edit-payment-method.component.scss"],
    animations: [appModuleAnimation()],
})
export class EditPaymentMethodComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    loading = false;
    uploadImageLoading = false;
    paymentMethod: GetWheelPaymentMethodForEditOutput =
        new GetWheelPaymentMethodForEditOutput();
    paymentTypeName = "";
    recurringPaymentTypeOption = [];
    file: any;
    logo: any;
    uploader: FileUploader;

    RecurringPaymentType = RecurringPaymentType;

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy
    ) {
        super(injector);
    }

    show(systemName?: string): void {
        this._initVariable();

        if (!systemName) {
            this.paymentMethod = new GetWheelPaymentMethodForEditOutput();
        } else {
            this.loading = true;
            this._wheelPaymentMethodsServiceProxy
                .getWheelPaymentMethodForEdit(systemName)
                .pipe(
                    finalize(() => {
                        this.loading = false;
                    })
                )
                .subscribe((result) => {
                    this.paymentMethod = result;
                    this._initDynamicFields();
                    this.modal.show();
                });
        }
    }

    private _initVariable() {
        this.active = false;
        this.saving = false;
        this.uploadImageLoading = false;
        this.paymentMethod = new GetWheelPaymentMethodForEditOutput();
        this.paymentTypeName = "";
        this.recurringPaymentTypeOption = [];
        this.file = undefined;
        this.logo = undefined;
        this.uploader = undefined;

        this.initUploader();
    }

    private _initDynamicFields() {
        switch (this.paymentMethod.systemName) {
            case SYSTEM_NAME.Adyen:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.Adyen;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            this.paymentMethod.dynamicFieldConfig;
                    }
                }

                break;

            case SYSTEM_NAME.Omise:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.Omise;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.Omise;
                    }
                }

                break;
            case SYSTEM_NAME.PayPal:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.PayPal;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            this.paymentMethod.dynamicFieldConfig;
                    }
                }

                break;
            case SYSTEM_NAME.Google:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.Google;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            this.paymentMethod.dynamicFieldConfig;
                    }
                }

                break;
            case SYSTEM_NAME.Stripe:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.Stripe;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            this.paymentMethod.dynamicFieldConfig;
                    }
                }

                break;
            case SYSTEM_NAME.HitPay:
                {
                    if (!this.paymentMethod.dynamicFieldConfig) {
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.HitPay;
                        this.paymentMethod.dynamicFieldData = {} as any;
                    } else {
                        this.paymentMethod.dynamicFieldData =
                            this.paymentMethod.dynamicFieldData;
                        this.paymentMethod.dynamicFieldConfig =
                            FIELD_DEFAULT.HitPay;
                    }
                }

                break;
        }
    }

    ngOnInit() {
        this.logo = new KTImageInput("kt_image_1");

        this.logo.on("change", (imageInput) => {
            this.uploadImageLoading = true;
            this.uploader.addToQueue(imageInput.input.files);
        });

        this.logo.on("cancel", () => {
            this.paymentMethod.logo = "";
        });

        this.logo.on("remove", () => {
            this.paymentMethod.logo = "";
        });

        for (var enumMember in RecurringPaymentType) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.recurringPaymentTypeOption.push({
                    label: RecurringPaymentType[enumMember],
                    value: parseInt(enumMember, 10),
                });
            }
        }
    }

    save(): void {
        this.saving = true;

        const input = EditWheelPaymentMethodDto.fromJS(this.paymentMethod);

        input.dynamicFieldConfig = JSON.stringify(
            this.paymentMethod.dynamicFieldConfig
        );
        input.dynamicFieldData = JSON.stringify(
            this.paymentMethod.dynamicFieldData
        );

        this._wheelPaymentMethodsServiceProxy
            .edit(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.modalSave.emit(true);
                this.close();
            });
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImage",
            authToken: "Bearer " + this._tokenService.getToken(),
        });

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.uploadImageLoading = false;
                this.paymentMethod.logo = ajaxResponse.result.url;
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.removeAfterUpload = true;
        uploaderOptions.autoUpload = true;
        this.uploader.setOptions(uploaderOptions);
    }

    close(): void {
        this.modal.hide();
    }

    shown(): void {}
}

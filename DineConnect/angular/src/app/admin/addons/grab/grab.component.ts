import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GrabServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-grab',
    templateUrl: './grab.component.html',
    styleUrls: ['./grab.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class GrabComponent extends AppComponentBase implements OnInit {
    clientId = "";
    clientSecret = "";
    merchantID;
    loading = false;
    isConnected = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _grabService: GrabServiceProxy
        
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._checkTokenExists();
    }


    disconnect() {
        this.loading = true;
        this._grabService.removeToken()
        .pipe(
            finalize(() => {
                this.loading = false;
            })
        )
        .subscribe(result => {
            this.isConnected = false;
        })
    };

    connect() {
        this.loading = true;
        this._grabService.connect(this.clientId, this.clientSecret)
        .pipe(
            finalize(() => {
                this.loading = false;
            })
        )
        .subscribe(result => {
            this.isConnected = result;
        })
    };

    syncData() {
        this.loading = true;
        this._grabService.grabSyncData(this.merchantID)
        .pipe(
            finalize(() => {
                this.loading = false;
            })
        )
        .subscribe(result => {
            this.notify.info(this.l("SyncedSuccessfully"));
        })
    };

    cancel() {
        this._router.navigateByUrl('app/admin/addonSettings');
    }

    private _checkTokenExists() {
        this.loading = true;
        this._grabService.isTokenExists()
        .pipe(
            finalize(() => {
                this.loading = false;
            })
        )
        .subscribe(result=> {
            this.isConnected = result;
        })
    }
}

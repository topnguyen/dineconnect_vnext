import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { XeroDepartmentTracking, XeroPaymentTracking, XeroServiceProxy, XeroSetting } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-xero',
    templateUrl: './xero.component.html',
    styleUrls: ['./xero.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class XeroComponent extends AppComponentBase implements OnInit {
    loading = false;
    loginUrl;
    isConnected = false;
    xero = new XeroSetting();

    xeroAccounts = [];
    payments = [];
    xeroContacts = [];
    xeroItems = [];
    departments = [];

    constructor(injector: Injector, private _router: Router, private _xeroService: XeroServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        if (!this.isConnected) {
            this._xeroService.getLoginUrl().subscribe((result) => {
                this.loginUrl = result;
            });
        }

        this._checkTokenExists();
    }

    cancel() {
        this._router.navigateByUrl('app/admin/addonSettings');
    }

    sync() {
        /* TODO: Back-end is not completed yet*/
        // this.loading = true;
        // this._xeroService
        //     .syncNonSales()
        //     .pipe(
        //         finalize(() => {
        //             this.notify.info(this.l('Success'));
        //             this.loading = false;
        //         })
        //     )
        //     .subscribe((result) => {});
    }

    disconnect() {
        this.loading = true;
        this._xeroService
            .removeToken()
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                this.isConnected = false;
            });
    }

    addDepartmentPortion() {
        if (!this.xero.departmentTrackings) {
            this.xero.paymentTrackings = [];
        }
        this.xero.departmentTrackings.push(new XeroDepartmentTracking({ departmentId: 0, saleItem: '' }));
    }

    removeDepartmentPortion(productIndex) {
        this.xero.departmentTrackings.splice(productIndex, 1);
    }

    addPaymentTypePortion() {
        if (!this.xero.paymentTrackings) {
            this.xero.paymentTrackings = [];
        }

        this.xero.paymentTrackings.push(new XeroPaymentTracking({ paymentTypeId: 0, paymentAccount: '' }));
    }

    removePaymentTypePortion(productIndex) {
        this.xero.paymentTrackings.splice(productIndex, 1);
    }

    update() {
        this.loading = true;
        this._xeroService
            .updateXeroSetting(this.xero)
            .pipe(
                finalize(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.loading = false;
                })
            )
            .subscribe((result) => {});
    }

    refresh() {
        this.loading = true;
        this.fillItems();

        this.loading = false;
    }

    fillSetting() {
        this._xeroService.getXeroSetting().subscribe((result) => {
            this.xero = result;
        });
    }

    fillAccounts() {
        this._xeroService.getAccounts().subscribe((result) => {
            this.xeroAccounts = result;
            this.fillSetting();
        });
    }

    fillCustomers() {
        this._xeroService.getCustomers().subscribe((result) => {
            this.xeroContacts = result;
            this.fillAccounts();
        });
    }

    fillItems() {
        this._xeroService.getItems().subscribe((result) => {
            this.xeroItems = result;
            this.fillCustomers();
        });
    }

    private _checkTokenExists() {
        this._xeroService
            .isTokenExists()
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                this.isConnected = result;
                this.refresh();
            });
    }
}

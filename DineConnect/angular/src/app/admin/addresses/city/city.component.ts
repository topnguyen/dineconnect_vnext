import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute } from "@angular/router";
import {
    AddressesServiceProxy,
    CityDto,
} from "@shared/service-proxies/service-proxies";
import { CreateOrUpdateCityComponent } from "./create-or-update-city-modal.component";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    templateUrl: "./city.component.html",
    styleUrls: ["./city.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class CityComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrUpdateCityModal", { static: true })
    createOrUpdateCityModal: CreateOrUpdateCityComponent;

    @ViewChild("cityTable", { static: true }) cityTable: Table;
    @ViewChild("cityPaginator", { static: true }) cityPaginator: Paginator;

    cityDto: CityDto;
    stateLoaded = false;
    loadTable = false;
    countryListDropDown = [];
    stateListDropDown = [];
    countryListDropDownForFilter = [];
    stateListDropDownForFilter = [];
    selectedCountry: number;
    selectedState: number;
    selectedCountryToShowList = 0;
    selectedStateToShowList = 0;
    filterText = "";
    isDeleted = false;

    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
        this.cityDto = new CityDto();
    }
    ngOnInit() {
        this.getCountryList();
    }

    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe((result) => {
                this.countryListDropDown = [];
                this.countryListDropDownForFilter = [];

                result.items.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.countryListDropDown.push(a);
                    this.countryListDropDownForFilter.push(a);
                });
                if (this.countryListDropDownForFilter.length === 1) {
                    this.selectedCountryToShowList =
                        this.countryListDropDownForFilter[0].value;
                } else {
                    this.countryListDropDownForFilter.unshift({
                        label: "None",
                        value: 0,
                    });
                }
                this.getStateListForListDropDown();
            });
    }

    getStateList() {
        this.selectedCountry = this.cityDto.countryId;
        this._addressesServiceProxy
            .getStateListByCountry(this.selectedCountry)
            .subscribe((result) => {
                this.stateListDropDown = [];
                result.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDown.push(a);
                });
                if (this.stateListDropDown.length === 1) {
                    this.cityDto.stateId = this.stateListDropDown[0].value;
                }
                this.stateLoaded = true;
            });
    }

    getStateListForListDropDown() {
        this._addressesServiceProxy
            .getStateListByCountry(this.selectedCountryToShowList)
            .subscribe((result) => {
                this.stateListDropDownForFilter = [];
                result.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDownForFilter.push(a);
                });
                let b = { label: "None", value: 0 };
                this.stateListDropDownForFilter.unshift(b);
                this.selectedStateToShowList = 0;
                this.stateLoaded = true;
                this.loadTable = true;
                this.getCityList();
            });
    }
    getCityList(event?: LazyLoadEvent) {
        if (this.loadTable) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this.cityPaginator.changePage(0);

                return;
            }
            this.primengTableHelper.showLoadingIndicator();
            this._addressesServiceProxy
                .getCityList(
                    this.filterText,
                    this.selectedCountryToShowList,
                    this.selectedStateToShowList,
                    this.isDeleted,
                    this.primengTableHelper.getSorting(this.cityTable),
                    this.primengTableHelper.getMaxResultCount(
                        this.cityPaginator,
                        event
                    ),
                    this.primengTableHelper.getSkipCount(
                        this.cityPaginator,
                        event
                    )
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount =
                        result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    countryChange() {
        this.getStateListForListDropDown();
    }

    deleteCity(city: CityDto) {
        this.message.confirm(
            "you want to delete city " + city.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._addressesServiceProxy
                        .deleteCity(city.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.getCityList();
                        });
                }
            }
        );
    }

    createOrUpdateCity(city: CityDto) {
        if (city == null || !city) {
            this.cityDto = new CityDto();
            if (this.countryListDropDown.length === 1) {
                this.cityDto.countryId = this.countryListDropDown[0].value;
            }
            this.createOrUpdateCityModal.cityDto = this.cityDto;
            this.createOrUpdateCityModal.show();
        } else {
            this.createOrUpdateCityModal.cityDto = city;
            this.createOrUpdateCityModal.show();
        }
    }

    exportToExcel() {
        this._addressesServiceProxy
            .getCityListToExcel(
                this.filterText,
                this.selectedCountryToShowList,
                this.selectedStateToShowList
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    activateItem(city: CityDto) {
        this._addressesServiceProxy
            .activateCity(city.id)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.getCountryList();
            });
    }
}

import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
    Output,
    EventEmitter
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AddressesServiceProxy,
    CityDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrUpdateCity',
    templateUrl: './create-or-update-city-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CreateOrUpdateCityComponent extends AppComponentBase implements OnInit {
    @Output() cityUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('modal', { static: true }) modal: ModalDirective;
    countryListDropDown = [];
    stateListDropDown = [];
    active = false;
    saving = false;
    cityDto: CityDto;
    stateLoaded = false;

    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getCountryList();
    }

    save() {
        this.saving = true;

        this._addressesServiceProxy
            .createorUpdateCity(this.cityDto)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullySaved'));
                this.cityUpdated.emit(true);
                this.modal.hide();
            });
    }

    close() {
        this.modal.hide();
    }

    show(): void {
        this.getStateList();
        this.modal.show();
    }


    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe(result => {
                this.countryListDropDown = [];
                result.items.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.countryListDropDown.push(a);
                });
                if (this.countryListDropDown.length === 1) {
                    this.cityDto.countryId = this.countryListDropDown[0].value;
                }
            });
    }

    getStateList() {
        this._addressesServiceProxy
            .getStateListByCountry(this.cityDto.countryId)
            .subscribe(result => {
                this.stateListDropDown = [];
                result.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDown.push(a);
                });
                if (this.stateListDropDown.length === 1) {
                    this.cityDto.stateId = this.stateListDropDown[0].value;
                }
                this.stateLoaded = true;
            });
    }
}

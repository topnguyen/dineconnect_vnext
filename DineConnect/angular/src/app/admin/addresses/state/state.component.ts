import {
    AfterViewInit,
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute } from "@angular/router";
import {
    AddressesServiceProxy,
    StateDto,
} from "@shared/service-proxies/service-proxies";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { CreateOrUpdateStateComponent } from "./create-or-update-state-modal.component";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { FileDownloadService } from "@shared/utils/file-download.service";
@Component({
    templateUrl: "./state.component.html",
    styleUrls: ["./state.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class StateComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrUpdateStateModal", { static: true })
    createOrUpdateStateModal: CreateOrUpdateStateComponent;

    @ViewChild("stateTable", { static: true }) stateTable: Table;
    @ViewChild("statePaginator", { static: true }) statePaginator: Paginator;

    stateDto: StateDto;
    selectedCountry = 0;
    countryListDropDown = [];
    countryListDropDownForFilter = [];
    loadTable = false;
    filterText = "";
    isDeleted = false;
    modalRef: BsModalRef;
    config = {
        backdrop: true,
        class: "modal-lg",
        ignoreBackdropClick: true,
    };

    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
        this.stateDto = new StateDto();
    }

    ngOnInit() {
        this.getCountryList();
    }

    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe((result) => {
                this.countryListDropDown = [];
                this.countryListDropDownForFilter = [];
                result.items.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.countryListDropDown.push(a);
                    this.countryListDropDownForFilter.push(a);
                });
                if (this.countryListDropDownForFilter.length === 1) {
                    this.selectedCountry =
                        this.countryListDropDownForFilter[0].value;
                } else {
                    this.countryListDropDownForFilter.push({
                        label: "None",
                        value: 0,
                    });
                }
                this.loadTable = true;
                this.getStateList();
            });
    }

    getStateList(event?: LazyLoadEvent) {
        if (this.loadTable) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this.statePaginator.changePage(0);

                return;
            }
            this.primengTableHelper.showLoadingIndicator();
            this._addressesServiceProxy
                .getStateList(
                    this.filterText,
                    this.selectedCountry,
                    this.isDeleted,
                    this.primengTableHelper.getSorting(this.stateTable),
                    this.primengTableHelper.getMaxResultCount(
                        this.statePaginator,
                        event
                    ),
                    this.primengTableHelper.getSkipCount(
                        this.statePaginator,
                        event
                    )
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount =
                        result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    deleteState(state: StateDto) {
        this.message.confirm(
            "you want to delete state " + state.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._addressesServiceProxy
                        .deleteState(state.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.getStateList();
                        });
                }
            }
        );
    }

    createOrUpdateState(state: StateDto) {
        if (state == null || !state) {
            this.stateDto = new StateDto();
            if (this.countryListDropDown.length === 1) {
                this.stateDto.countryId = this.countryListDropDown[0].value;
            }
            this.createOrUpdateStateModal.stateDto = this.stateDto;
            this.createOrUpdateStateModal.show();
        } else {
            this.createOrUpdateStateModal.stateDto = state;
            this.createOrUpdateStateModal.show();
        }
    }

    exportToExcel() {
        this._addressesServiceProxy
            .getStateListToExcel(this.filterText, this.selectedCountry)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    activateItem(state: StateDto) {
        this._addressesServiceProxy
            .activateState(state.id)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.getCountryList();
            });
    }
}

import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
    Output,
    EventEmitter
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AddressesServiceProxy,
    StateDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrUpdateState',
    templateUrl: './create-or-update-state-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CreateOrUpdateStateComponent extends AppComponentBase implements OnInit {
    @Output() stateUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('modal', { static: true }) modal: ModalDirective;
    countryListDropDown = [];
    active = false;
    saving = false;
    stateDto = new StateDto;
    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getCountryList();
    }

    save() {
        this.saving = true;

        this._addressesServiceProxy
            .createorUpdateState(this.stateDto)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullySaved'));
                this.stateUpdated.emit(true);
                this.modal.hide();
            });
    }

    close() {
        this.modal.hide();
    }

    show(): void {
        this.modal.show();
    }

    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe(result => {
                this.countryListDropDown = [];
                result.items.forEach(element => {
                    let a = {
                        label: element.name,
                        value: element.id
                    };
                    this.countryListDropDown.push(a);
                });
                if (this.countryListDropDown.length === 1) {
                    this.stateDto.countryId = this.countryListDropDown[0].value;
                }
            });
    }
}

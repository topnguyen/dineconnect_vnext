import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
    Output,
    EventEmitter
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AddressesServiceProxy,
    CountryDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrUpdateCountry',
    templateUrl: './create-or-update-country-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CreateOrUpdateCountryComponent extends AppComponentBase
    implements OnInit {
    @Output()
    countryUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('modal', { static: true }) modal: ModalDirective;

    active = false;
    saving = false;
    countryDto: CountryDto;
    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy
    ) {
        super(injector);
    }

    save() {
        this.saving = true;

        this._addressesServiceProxy
            .createorUpdateCountry(this.countryDto)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullySaved'));
                this.countryUpdated.emit(true);
                this.modal.hide();
            });
    }
    close() {
        this.modal.hide();
    }

    show(): void {
        this.modal.show();
        //this.selectedLocationList = this.menuItemDto.locationList;
        //   this.getLocationList();
    }

    ngOnInit() {
        //  this.getCategoriesList();
    }
}

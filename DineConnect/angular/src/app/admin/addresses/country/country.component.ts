import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute } from "@angular/router";
import {
    AddressesServiceProxy,
    CountryDto,
} from "@shared/service-proxies/service-proxies";

import { CreateOrUpdateCountryComponent } from "./create-or-update-country-modal.component";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { FileDownloadService } from "@shared/utils/file-download.service";
@Component({
    templateUrl: "./country.component.html",
    styleUrls: ["./country.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class CountryComponent extends AppComponentBase {
    @ViewChild("createOrUpdateCountryModal", { static: true })
    createOrUpdateCountryModal: CreateOrUpdateCountryComponent;

    @ViewChild("countryTable", { static: true }) countryTable: Table;
    @ViewChild("countryPaginator", { static: true })
    countryPaginator: Paginator;

    countryDto: CountryDto;
    filterText = "";
    isDeleted = false;

    constructor(
        injector: Injector,
        private _addressesServiceProxy: AddressesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
        this.countryDto = new CountryDto();
    }

    getCountryList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.countryPaginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._addressesServiceProxy
            .getCountryList(
                this.filterText,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.countryTable),
                this.primengTableHelper.getMaxResultCount(
                    this.countryPaginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(
                    this.countryPaginator,
                    event
                )
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    createOrUpdateCountry(country: CountryDto) {
        if (country == null || country === undefined) {
            this.countryDto = new CountryDto();
            this.createOrUpdateCountryModal.countryDto = this.countryDto;
            this.createOrUpdateCountryModal.show();
        } else {
            this.createOrUpdateCountryModal.countryDto = country;
            this.createOrUpdateCountryModal.show();
        }
    }

    exportToExcel() {
        this._addressesServiceProxy
            .getCountryListToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    deleteCountry(country: CountryDto) {
        this.message.confirm(
            "you want to delete country " + country.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._addressesServiceProxy
                        .deleteCountry(country.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.getCountryList();
                        });
                }
            }
        );
    }

    activateItem(country: CountryDto) {
        this._addressesServiceProxy
            .activateCoutry(country.id)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.getCountryList();
            });
    }
}

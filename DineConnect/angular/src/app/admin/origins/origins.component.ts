import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AllowOriginServiceServiceProxy, GetAllowOriginForViewDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/public_api';
import { Router } from '@angular/router';

@Component({
    templateUrl: './origins.component.html',
    animations: [appModuleAnimation()]
})

export class OriginsComponent extends AppComponentBase implements OnInit {

    @ViewChild('dataTable', {static: true}) dataTable: Table;
    @ViewChild('paginator', {static: true}) paginator: Paginator;

    tenantDatabases: GetAllowOriginForViewDto[] = [];
    filter: string = ''

    constructor(
        injector: Injector,
        private _allowOriginServiceServiceProxy: AllowOriginServiceServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {
    }

    getTenantDatabases(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._allowOriginServiceServiceProxy.getAll(
            this.filter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        })
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEdit(id?: number) {
        this._router.navigate(["/app/admin/origins/create-or-edit", id ? id : 'null']);
    }

    delete(model: GetAllowOriginForViewDto) {
        this.message.confirm(
            this.l('TenantDatabaseDeleteWarningMessage', model.origin),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._allowOriginServiceServiceProxy.delete(model.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
}

import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    AllowOriginServiceServiceProxy,
    GetAllowOriginForViewDto,
    GetAllowOriginForEditDto,
    CreateOrEditAllowOriginInput,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/public_api";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    templateUrl: "./create-or-edit-origin.component.html",
    animations: [appModuleAnimation()],
})
export class CreateOrEditOriginComponent extends AppComponentBase {
    origin: CreateOrEditAllowOriginInput;
    id: number;
    saving: boolean = false;

    constructor(
        injector: Injector,
        private _allowOriginServiceServiceProxy: AllowOriginServiceServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(injector);
        this.origin = new CreateOrEditAllowOriginInput();
        this._activatedRoute.params.subscribe((params) => {
            this.id = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.id)) {
                this.id = undefined;
            } else {
                this._allowOriginServiceServiceProxy
                    .getAllowOriginForEdit(this.id)
                    .subscribe((result) => {
                        this.origin = result;
                    });
            }
        });
    }

    save() {
        this.saving = true;

        this._allowOriginServiceServiceProxy
            .createOrEdit(this.origin)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.cancel();
            });
    }

    cancel() {
        this._router.navigate(["/app/admin/origins"]);
    }
}

import { NgModule } from '@angular/core';
import { NavigationEnd, Router, RouterModule } from '@angular/router';
import { AuditLogsComponent } from './audit-logs/audit-logs.component';
import { HostDashboardComponent } from './dashboard/host-dashboard.component';
import { DemoUiComponentsComponent } from './demo-ui-components/demo-ui-components.component';
import { EditionsComponent } from './editions/editions.component';
import { InstallComponent } from './install/install.component';
import { LanguageTextsComponent } from './languages/language-texts.component';
import { LanguagesComponent } from './languages/languages.component';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import { OrganizationUnitsComponent } from './organization-units/organization-units.component';
import { RolesComponent } from './roles/roles.component';
import { HostSettingsComponent } from './settings/host-settings.component';
import { TenantSettingsComponent } from './settings/tenant-settings.component';
import { InvoiceComponent } from './subscription-management/invoice/invoice.component';
import { SubscriptionManagementComponent } from './subscription-management/subscription-management.component';
import { TenantsComponent } from './tenants/tenants.component';
import { UiCustomizationComponent } from './ui-customization/ui-customization.component';
import { UsersComponent } from './users/users.component';
import { CountryComponent } from './addresses/country/country.component';
import { StateComponent } from './addresses/state/state.component';
import { CityComponent } from './addresses/city/city.component';
import { AddonSettingsComponent } from './addon-settings/addon-settings.component';
import { TenantDatabaseComponent } from './tenant-database/tenant-database.component';
import { OriginsComponent } from './origins/origins.component';
import { CreateOrEditOriginComponent } from './origins/create-or-edit-origin.component';
import { XeroComponent } from './addons/xero/xero.component';
import { GrabComponent } from './addons/grab/grab.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Administration.Users' } },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Administration.Roles' } },
                    { path: 'auditLogs', component: AuditLogsComponent, data: { permission: 'Pages.Administration.AuditLogs' } },
                    { path: 'maintenance', component: MaintenanceComponent, data: { permission: 'Pages.Administration.Host.Maintenance' } },
                    { path: 'hostSettings', component: HostSettingsComponent, data: { permission: 'Pages.Administration.Host.Settings' } },
                    { path: 'editions', component: EditionsComponent, data: { permission: 'Pages.Editions' } },
                    { path: 'languages', component: LanguagesComponent, data: { permission: 'Pages.Administration.Languages' } },
                    { path: 'languages/:name/texts', component: LanguageTextsComponent, data: { permission: 'Pages.Administration.Languages.ChangeTexts' } },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' } },
                    { path: 'organization-units', component: OrganizationUnitsComponent, data: { permission: 'Pages.Administration.OrganizationUnits' } },
                    { path: 'subscription-management', component: SubscriptionManagementComponent, data: { permission: 'Pages.Administration.Tenant.SubscriptionManagement' } },
                    { path: 'invoice/:paymentId', component: InvoiceComponent, data: { permission: 'Pages.Administration.Tenant.SubscriptionManagement' } },
                    { path: 'tenantSettings', component: TenantSettingsComponent, data: { permission: 'Pages.Administration.Tenant.Settings' } },
                    { path: 'hostDashboard', component: HostDashboardComponent, data: { permission: 'Pages.Administration.Host.Dashboard' } },
                    { path: 'demo-ui-components', component: DemoUiComponentsComponent, data: { permission: 'Pages.DemoUiComponents' } },
                    { path: 'install', component: InstallComponent },
                    { path: 'ui-customization', component: UiCustomizationComponent },
                    { path: 'address/country', component: CountryComponent, data: { permission: 'Pages.Tenant.Connect.Address.Countries' } },
                    { path: 'address/state', component: StateComponent, data: { permission: 'Pages.Tenant.Connect.Address.States' } },
                    { path: 'address/city', component: CityComponent, data: { permission: 'Pages.Tenant.Connect.Address.Cities' } },
                    { path: 'addonSettings', component: AddonSettingsComponent, data: { permission: 'Pages.Administration.Tenant.AddonSettings' } },
                    { path: 'addons/xero', component: XeroComponent, data: { permission: 'Pages.Administration.Tenant.AddonSettings' } },
                    { path: 'addons/grap', component: GrabComponent, data: { permission: 'Pages.Administration.Tenant.AddonSettings' } },
                    { path: 'tenantDatabase', component: TenantDatabaseComponent, data: { permission: 'Pages.Administration.Host.TenantDatabase' } },
                    { path: 'origins', component: OriginsComponent, data: { permission: 'Pages.Administration.Host.Origins' } },
                    { path: 'origins/create-or-edit/:id', component: CreateOrEditOriginComponent, data: { permission: 'Pages.Administration.Host.Origins' }}
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {

    constructor(
        private router: Router
    ) {
        router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                window.scroll(0, 0);
            }
        });
    }
}

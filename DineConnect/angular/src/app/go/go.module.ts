import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoRoutingModule } from './go-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UtilsModule } from '@shared/utils/utils.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { CheckboxModule, PaginatorModule, PanelModule, SelectButtonModule, TableModule } from 'primeng';
import { DeviceAddEditComponent } from './device/device-add-edit/device-add-edit.component';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgSelectModule } from '@ng-select/ng-select';
import { FileUploadModule as PrimeNgFileUploadModule } from "primeng/fileupload";
import { FileUploadModule } from 'ng2-file-upload';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrandListComponent } from './brand/brand-list/brand-list.component';
import { BrandAddEditComponent } from './brand/brand-add-edit/brand-add-edit.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentAddEditComponent } from './department/department-add-edit/department-add-edit.component';
import { PaymentTypeListComponent } from './payment-type/payment-type-list/payment-type-list.component';
import { PaymentTypeAddEditComponent } from './payment-type/payment-type-add-edit/payment-type-add-edit.component';
import { ChargeListComponent } from './charge/charge-list/charge-list.component';
import { ChargeAddEditComponent } from './charge/charge-add-edit/charge-add-edit.component';

@NgModule({
    declarations: [
        DeviceAddEditComponent,
        DeviceListComponent,
        BrandListComponent,
        BrandAddEditComponent,
        DepartmentListComponent,
        DepartmentAddEditComponent,
        PaymentTypeListComponent,
        PaymentTypeAddEditComponent,
        ChargeListComponent,
        ChargeAddEditComponent
    ],
    imports: [
        CommonModule,
        GoRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        UtilsModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        CheckboxModule,
        SelectButtonModule,
        PanelModule,
        TabsModule,
        NgSelectModule,
        FileUploadModule ,
        PrimeNgFileUploadModule,
        BsDropdownModule.forRoot(),
    ]
})
export class GoModule {}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDineGoDepartmentInput,
    DepartmentsServiceProxy,
    DineGoChargeEditDto,
    DineGoChargeServiceProxy,
    DineGoDepartmentEditDto,
    DineGoDepartmentServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './department-add-edit.component.html',
  styleUrls: ['./department-add-edit.component.scss'],
  animations: [appModuleAnimation()]
})
export class DepartmentAddEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    department = new DineGoDepartmentEditDto();
    saving = false;
    charges: DineGoChargeEditDto[] = [];
    departments = [];
    selectedCharge: DineGoChargeEditDto;
    dineGoCharges: DineGoChargeEditDto[] = [];

    paging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dineGoDepartmentServiceProxy: DineGoDepartmentServiceProxy,
        private _dineGoChargeServiceProxy: DineGoChargeServiceProxy,
        private _departmentsServiceProxy: DepartmentsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            const departmentId = +params['id'];

            if (departmentId > 0) {
                this.department.id = departmentId;
                this._dineGoDepartmentServiceProxy.getDineGoDepartmentForEdit(this.department.id).subscribe((resp) => {
                    this.department = resp.dineGoDepartment;
                    this.dineGoCharges = resp.dineGoDepartment.dineGoCharges;
                });
            }
        });

        this._getDepartments();
        this._getCharges();
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }
        const input = new CreateOrUpdateDineGoDepartmentInput();
        input.dineGoDepartment = this.department;
        input.dineGoDepartment.dineGoCharges = this.dineGoCharges;

        this.saving = true;
        this._dineGoDepartmentServiceProxy
            .createOrUpdateDineGoDepartment(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/go/department']);
    }

    addCharge() {
        if (this.selectedCharge == null) {
            abp.notify.error('Department is mandatory');
            return;
        }
        let successFlag = false;
        this.dineGoCharges.some( (value, key) => {
            if (value.id == this.selectedCharge.id) {
                abp.notify.error('Record Already Exists');
                successFlag = true;
                return successFlag;
            }
        });
        if (!successFlag) {
            this.dineGoCharges.push(this.selectedCharge);
        }
        this.selectedCharge = null;
    }

    onChangePage(paging) {
        this.paging = paging;
    }

    removeCharge(myObject) {
        for (var i = 0; i < this.dineGoCharges.length; i++) {
            var obj = this.dineGoCharges[i];

            if (obj.id == myObject.id) {
                this.dineGoCharges.splice(i, 1);
            }
        }
    }

    private _getDepartments() {
        this._departmentsServiceProxy.getDepartmentsForCombobox().subscribe((result) => {
            this.departments = result.items;
        });
    }

    private _getCharges() {
        this._dineGoChargeServiceProxy.getAll(undefined, 'id', 1000, 0).subscribe((result) => {
            this.charges = result.items.map(item => {
                return new DineGoChargeEditDto(item as any);
            });
        });
    }

}

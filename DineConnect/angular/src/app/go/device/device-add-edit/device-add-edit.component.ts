import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DeviceThemeSettingsDto,
    DineGoBrandEditDto,
    DineGoBrandListDto,
    DineGoBrandServiceProxy,
    DineGoDeviceDto,
    DineGoDeviceEditDto,
    DineGoDeviceServiceProxy,
    DineGoPaymentTypeEditDto,
    DineGoPaymentTypeListDto,
    DineGoPaymentTypeServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './device-add-edit.component.html',
    styleUrls: ['./device-add-edit.component.scss'],
    animations: [appModuleAnimation()]
})
export class DeviceAddEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    tabIndex = 0;
    device = new DineGoDeviceEditDto();
    saving = false;
    selectedBrand;
    selectedPaymentType;
    brands: DineGoBrandEditDto[] = [];
    paymentTypes: DineGoPaymentTypeEditDto[] = [];
    dineGoBrands: DineGoBrandEditDto[] = [];
    dineGoPaymentTypes: DineGoPaymentTypeEditDto[] = [];

    pagingBrand = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };

    pagingPayment = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };
    idleContents = [];
    bannerContents = [];
    logo = [];
    fileTag = '';
    refreshImage: boolean;
    idleContentUploader: FileUploader;
    bannerContentUploader: FileUploader;
    logoUploader: FileUploader;
    deviceThemeSettings = new DeviceThemeSettingsDto();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dineGoDeviceServiceProxy: DineGoDeviceServiceProxy,
        private _tokenService: TokenService,
        private _dineGoBrandServiceProxy: DineGoBrandServiceProxy,
        private _dineGoPaymentTypeServiceProxy: DineGoPaymentTypeServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            const deviceId = +params['id'];
            if (deviceId > 0) {
                this.device.id = deviceId;
                this._dineGoDeviceServiceProxy.getDineGoDeviceForEdit(this.device.id).subscribe((resp) => {
                    this.device = resp.dineGoDevice;
                    this.dineGoBrands = resp.dineGoDevice.dineGoBrands;
                    this.dineGoPaymentTypes = resp.dineGoDevice.dineGoPaymentTypes;
                    this.deviceThemeSettings = resp.deviceThemeSettings;

                    this.idleContents = this.deviceThemeSettings.idleContents ? JSON.parse(this.deviceThemeSettings.idleContents) : [];
                    this.bannerContents = this.deviceThemeSettings.bannerContents ? JSON.parse(this.deviceThemeSettings.bannerContents) : [];
                    this.logo = this.deviceThemeSettings.logo ? JSON.parse(this.deviceThemeSettings.logo) : [];
                });

                this._getBrands();
                this._getPaymentTypes();
            }
        });

        this.initUploader();
    }

    initUploader() {
        this.idleContentUploader = this.createUploader('/ImageUploader/UploadImage', (result) => {
            if (this.idleContents) {
                this.idleContents.push({
                    fileTag: this.fileTag,
                    fileName: result.fileName,
                    fileSystemName: result.url
                });
            }
        });
        this.bannerContentUploader = this.createUploader('/ImageUploader/UploadImage', (result) => {
            if (this.bannerContents) {
                this.bannerContents.push({
                    fileTag: this.fileTag,
                    fileName: result.fileName,
                    fileSystemName: result.url
                });
            }
        });
        this.logoUploader = this.createUploader('/ImageUploader/UploadImage', (result) => {
            if (this.logo) {
                this.logo.push({
                    fileTag: this.fileTag,
                    fileName: result.fileName,
                    fileSystemName: result.url
                });
            }
        });
    }

    uploadIdleContent(): void {
        this.idleContentUploader.uploadAll();
    }

    uploadBanbnerContent(): void {
        this.bannerContentUploader.uploadAll();
    }

    uploadLogo(): void {
        this.logoUploader.uploadAll();
    }

    createUploader(url: string, success?: (result: any) => void): FileUploader {
        const uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + url
        });

        uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l('SavedSuccessfully'));
                if (success) {
                    success(ajaxResponse.result);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        uploaderOptions.removeAfterUpload = true;
        uploader.setOptions(uploaderOptions);
        return uploader;
    }

    removeBrand(myObject) {
        for (var i = 0; i < this.dineGoBrands.length; i++) {
            var obj = this.dineGoBrands[i];

            if (obj.id == myObject.id) {
                this.dineGoBrands.splice(i, 1);
            }
        }
    }

    addBrand() {
        if (this.selectedBrand == null) {
            this.notify.error('Brand is mandatory');
            return;
        }
        var successFlag = false;
        this.dineGoBrands.some((value) => {
            if (value.id == this.selectedBrand.id) {
                this.notify.error('Record Already Exists');
                successFlag = true;
                return successFlag;
            }
        });
        if (!successFlag) {
            this.dineGoBrands.push(this.selectedBrand);
        }
        this.selectedBrand = null;
    }

    addPaymentType() {
        if (this.selectedPaymentType == null) {
            this.notify.error('Payment Type is mandatory');
            return;
        }
        var successFlag = false;
        this.dineGoPaymentTypes.some((value) => {
            if (value.id == this.selectedPaymentType.id) {
                this.notify.error('Record Already Exists');
                successFlag = true;
                return successFlag;
            }
        });
        if (!successFlag) {
            this.dineGoPaymentTypes.push(this.selectedPaymentType);
        }
        this.selectedPaymentType = null;
    }

    removePaymentType(myObject) {
        for (var i = 0; i < this.dineGoPaymentTypes.length; i++) {
            var obj = this.dineGoPaymentTypes[i];

            if (obj.id == myObject.id) {
                this.dineGoPaymentTypes.splice(i, 1);
            }
        }
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }
        this.saving = true;
        if (this.idleContents) {
            this.deviceThemeSettings.idleContents = JSON.stringify(this.idleContents);
        }

        if (this.bannerContents) {
            this.deviceThemeSettings.bannerContents = JSON.stringify(this.bannerContents);
        }

        if (this.logo) {
            this.deviceThemeSettings.logo = JSON.stringify(this.logo);
        }

        const input = new DineGoDeviceDto();
        input.dineGoDevice = this.device;

        input.deviceThemeSettings = this.deviceThemeSettings;
        input.dineGoDevice.dineGoBrands = this.dineGoBrands;
        input.dineGoDevice.dineGoPaymentTypes = this.dineGoPaymentTypes;

        this._dineGoDeviceServiceProxy
            .createOrUpdateDineGoDevice(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/go/device']);
    }

    onChangePageBrand(paging) {
        this.pagingBrand = paging;
    }

    onChangePagePayment(paging) {
        this.pagingPayment = paging;
    }

    downloader(file) {
        window.open(file.fileSystemName, '_bland');
    }

    removeFile(findex) {
        if (this.fileTag == 'Idle Content') {
            this.idleContents.splice(findex, 1);
        } else if (this.fileTag == 'Banner Content') {
            this.bannerContents.splice(findex, 1);
        } else if (this.fileTag == 'Logo') {
            this.logo.splice(findex, 1);
        }

        this.refreshImage = true;
    }

    private _getBrands() {
        this._dineGoBrandServiceProxy.getAll(undefined, 'id', 1000, 0).subscribe((result) => {
            this.brands = result.items.map(item => {
                return new DineGoBrandEditDto(item as any);
            });
        });
    }

    private _getPaymentTypes() {
        this._dineGoPaymentTypeServiceProxy.getAll(undefined, 'id', 1000, 0).subscribe((result) => {
            this.paymentTypes = result.items.map(item => {
                return new DineGoPaymentTypeEditDto(item as any);
            });
        });
    }
}

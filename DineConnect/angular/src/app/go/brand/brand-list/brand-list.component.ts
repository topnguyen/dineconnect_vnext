import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DineGoBrandListDto, DineGoBrandServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';

@Component({
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class BrandListComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    exporting = false;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _dineGoBrandServiceProxy: DineGoBrandServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._dineGoBrandServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }


    resetFilter() {
        this.filterText = undefined;

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel() {
        this.exporting = true;

        this._dineGoBrandServiceProxy.getAllToExcel(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
            this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
        ).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
            this.exporting = false;
        });
    }

    deleteItem(item: DineGoBrandListDto) {
        this.message.confirm(this.l('DeleteDineGoBrandWarning', item.label), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._dineGoBrandServiceProxy.deleteDineGoBrand(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));
                    this.reloadPage();
                });
            }
        });
    }

    createOrUpdateItem(id?: number) {
        this._router.navigate(['/app/go/brand', id ? id : '0']);
    }
}

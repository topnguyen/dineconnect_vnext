import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
    CreateOrUpdateDineGoBrandInput,
    DineGoBrandEditDto,
    DineGoBrandServiceProxy,
    DineGoDepartmentEditDto,
    DineGoDepartmentListDto,
    DineGoDepartmentServiceProxy,
    LocationServiceProxy,
    ScreenMenuServiceProxy
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './brand-add-edit.component.html',
    styleUrls: ['./brand-add-edit.component.scss'],
    animations: [appModuleAnimation()]
})
export class BrandAddEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    brand = new DineGoBrandEditDto();
    saving = false;
    locations = [];
    screenMenus = [];
    departments: DineGoDepartmentEditDto[] = [];
    selectedDepartment: DineGoDepartmentEditDto;
    dineGoDepartments: DineGoDepartmentEditDto[] = [];

    paging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dineGoBrandServiceProxy: DineGoBrandServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _dineGoDepartmentServiceProxy: DineGoDepartmentServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._getLocations();
        this._getScreenMenu();

        this._activatedRoute.params.subscribe((params) => {
            const brandId = +params['id'];

            if (brandId > 0) {
                this.brand.id = brandId;
                this._dineGoBrandServiceProxy.getDineGoBrandForEdit(this.brand.id).subscribe((resp) => {
                    this.brand = resp.dineGoBrand;
                    this.dineGoDepartments = resp.dineGoBrand.dineGoDepartments;
                });
                this._getDepartments();
            }
        });
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }
        const input = new CreateOrUpdateDineGoBrandInput();
        input.dineGoBrand = this.brand;
        input.dineGoBrand.dineGoDepartments = this.dineGoDepartments;

        this.saving = true;
        this._dineGoBrandServiceProxy
            .createOrUpdateDineGoBrand(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/go/brand']);
    }

    addDepartment() {
        if (this.selectedDepartment == null) {
            abp.notify.error('Department is mandatory');
            return;
        }
        let successFlag = false;
        this.dineGoDepartments.some( (value, key) => {
            if (value.id == this.selectedDepartment.id) {
                abp.notify.error('Record Already Exists');
                successFlag = true;
                return successFlag;
            }
        });
        if (!successFlag) {
            this.dineGoDepartments.push(this.selectedDepartment);
        }
        this.selectedDepartment = null;
    }

    onChangePage(paging) {
        this.paging = paging;
    }

    removeDepartment(myObject) {
        for (var i = 0; i < this.dineGoDepartments.length; i++) {
            var obj = this.dineGoDepartments[i];

            if (obj.id == myObject.id) {
                this.dineGoDepartments.splice(i, 1);
            }
        }
    }

    private _getLocations() {
        this._locationServiceProxy.getList(undefined, undefined, undefined, 'id', 1000, 0).subscribe((result) => {
            this.locations = result.items.map((item) => {
                return new ComboboxItemDto({
                    value: item.id as any,
                    displayText: item.name,
                    isSelected: false
                });
            });
        });
    }

    private _getScreenMenu() {
        this._screenMenuServiceProxy
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'id', 0, 1000)
            .subscribe((result) => {
                this.screenMenus = result.items.map((item) => {
                    return new ComboboxItemDto({
                        value: item.id as any,
                        displayText: item.name,
                        isSelected: false
                    });
                });
            });
    }

    private _getDepartments() {
        this._dineGoDepartmentServiceProxy.getAll( undefined, 'id', 1000, 0).subscribe((result) => {
            this.departments = result.items.map(item => {
                return new DineGoDepartmentEditDto(item as any);
            });
        });
    }
}

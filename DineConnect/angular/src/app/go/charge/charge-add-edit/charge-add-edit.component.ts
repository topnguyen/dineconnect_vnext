import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDineGoChargeInput,
    DineGoChargeEditDto,
    DineGoChargeServiceProxy,
    TransactionTypesServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './charge-add-edit.component.html',
  styleUrls: ['./charge-add-edit.component.scss'],
  animations: [appModuleAnimation()]
})
export class ChargeAddEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    charge = new DineGoChargeEditDto();
    saving = false;
    transactionTypes = [];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dineGoChargeServiceProxy: DineGoChargeServiceProxy,
        private _transactionTypesServiceProxy: TransactionTypesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            const chargeId = +params['id'];

            if (chargeId > 0) {
                this.charge.id = chargeId;
                this._dineGoChargeServiceProxy.getDineGoChargeForEdit(this.charge.id).subscribe((resp) => {
                    this.charge = resp.dineGoCharge;
                });
            }
        });

        this._getTransactionType();
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }
        const input = new CreateOrUpdateDineGoChargeInput();
        input.dineGoCharge = this.charge;

        this.saving = true;
        this._dineGoChargeServiceProxy
            .createOrUpdateDineGoCharge(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/go/charge']);
    }

    private _getTransactionType() {
        this._transactionTypesServiceProxy.getTransactionTypeForLookupTable().subscribe(result => {
            this.transactionTypes = result.items;
        })
    }
}

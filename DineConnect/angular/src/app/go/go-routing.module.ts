﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrandAddEditComponent } from './brand/brand-add-edit/brand-add-edit.component';
import { BrandListComponent } from './brand/brand-list/brand-list.component';
import { ChargeAddEditComponent } from './charge/charge-add-edit/charge-add-edit.component';
import { ChargeListComponent } from './charge/charge-list/charge-list.component';
import { DepartmentAddEditComponent } from './department/department-add-edit/department-add-edit.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DeviceAddEditComponent } from './device/device-add-edit/device-add-edit.component';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { PaymentTypeAddEditComponent } from './payment-type/payment-type-add-edit/payment-type-add-edit.component';
import { PaymentTypeListComponent } from './payment-type/payment-type-list/payment-type-list.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'device', component: DeviceListComponent, data: { permission: '' } },
                    { path: 'device/:id', component: DeviceAddEditComponent, data: { permission: '' } },
                    { path: 'brand', component: BrandListComponent, data: { permission: '' } },
                    { path: 'brand/:id', component: BrandAddEditComponent, data: { permission: '' } },
                    { path: 'department', component: DepartmentListComponent, data: { permission: '' } },
                    { path: 'department/:id', component: DepartmentAddEditComponent, data: { permission: '' } },
                    { path: 'payment-type', component: PaymentTypeListComponent, data: { permission: '' } },
                    { path: 'payment-type/:id', component: PaymentTypeAddEditComponent, data: { permission: '' } },
                    { path: 'charge', component: ChargeListComponent, data: { permission: '' } },
                    { path: 'charge/:id', component: ChargeAddEditComponent, data: { permission: '' } },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class GoRoutingModule { }

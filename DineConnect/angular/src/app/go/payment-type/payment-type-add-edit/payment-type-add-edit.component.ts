import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
    CreateOrUpdateDineGoPaymentTypeInput,
    DineGoPaymentTypeEditDto,
    DineGoPaymentTypeServiceProxy,
    PaymentTypesServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './payment-type-add-edit.component.html',
  styleUrls: ['./payment-type-add-edit.component.scss'],
  animations: [appModuleAnimation()]
})
export class PaymentTypeAddEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    paymentType = new DineGoPaymentTypeEditDto();
    saving = false;
    paymentTypes = [];

    paging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _dineGoPaymentTypeServiceProxy: DineGoPaymentTypeServiceProxy,
        private _paymentTypesServiceProxy: PaymentTypesServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            const paymentTypeId = +params['id'];

            if (paymentTypeId > 0) {
                this.paymentType.id = paymentTypeId;
                this._dineGoPaymentTypeServiceProxy.getDineGoPaymentTypeForEdit(this.paymentType.id).subscribe((resp) => {
                    this.paymentType = resp.dineGoPaymentType;
                });
            }
        });

        this._getPaymentTypes();
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }
        const input = new CreateOrUpdateDineGoPaymentTypeInput();
        input.dineGoPaymentType = this.paymentType;

        this.saving = true;
        this._dineGoPaymentTypeServiceProxy
            .createOrUpdateDineGoPaymentType(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/go/payment-type']);
    }

    private _getPaymentTypes() {
        this._paymentTypesServiceProxy.getPaymentTypeForLookupTable(undefined, undefined, 0, 1000).subscribe((result) => {
            this.paymentTypes = result.items.map((item) => {
                return new ComboboxItemDto({
                    value: item.value as any,
                    displayText: item.name,
                    isSelected: false
                });
            });
        });
    }

}

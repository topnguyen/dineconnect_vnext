import { NgModule } from '@angular/core';
import { NavigationEnd, RouteConfigLoadEnd, RouteConfigLoadStart, Router, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from './shared/common/auth/auth-route-guard';
import { NotificationsComponent } from './shared/layout/notifications/notifications.component';
import { NgxSpinnerService } from 'ngx-spinner';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'app',
                component: AppComponent,
                canActivate: [AppRouteGuard],
                canActivateChild: [AppRouteGuard],
                children: [
                    {
                        path: '',
                        children: [
                            { path: 'notifications', component: NotificationsComponent },
                            { path: '', redirectTo: '/app/main/dashboard', pathMatch: 'full' }
                        ]
                    },
                    {
                        path: 'main',
                        loadChildren: () => import('app/main/main.module').then(m => m.MainModule), //Lazy load main module
                        data: { preload: true }
                    },
                    {
                        path: 'admin',
                        loadChildren: () => import('app/admin/admin.module').then(m => m.AdminModule), //Lazy load admin module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'connect',
                        loadChildren: () => import('app/connect/connect.module').then(m => m.ConnectModule), //Lazy load admin module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'queue',
                        loadChildren: () => import('app/queue/queue.module').then(m => m.QueueModule), //Lazy load queue module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'go',
                        loadChildren: () => import('app/go/go.module').then(m => m.GoModule), //Lazy load queue module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'swipe',
                        loadChildren: () => import('app/swipe/swipe.module').then(m => m.SwipeModule), //Lazy load queue module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'tiffin',
                        loadChildren: () => import('app/tiffin/tiffin.module').then(m => m.TiffinModule), //Lazy load admin module
                        data: { preload: true }
                    },
                    {
                        path: 'wheel',
                        loadChildren: () => import('app/wheel/wheel.module').then(m => m.WheelModule),
                        data: { preload: true }
                    },
                    {
                        path: 'shipment',
                        loadChildren: () => import('app/shipment/shipment.module').then(m => m.ShipmentModule),
                        data: { preload: true }
                    },
                    {
                        path: 'clique',
                        loadChildren: () => import('app/clique/clique.module').then(m => m.CliqueModule),
                        data: { preload: true }
                    },
                    {
                        path: 'flyer',
                        loadChildren: () => import('app/flyer/flyer.module').then(m => m.FlyerModule), //Lazy load queue module
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: 'cluster',
                        loadChildren: () => import('app/cluster/cluster.module').then(m => m.ClusterModule),
                        data: { preload: true },
                        canLoad: [AppRouteGuard]
                    },
                    {
                        path: '**', redirectTo: 'notifications'
                    }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {
    constructor(
        private router: Router,
        private spinnerService: NgxSpinnerService
    ) {
        router.events.subscribe((event) => {

            if (event instanceof RouteConfigLoadStart) {
                spinnerService.show();
            }

            if (event instanceof RouteConfigLoadEnd) {
                spinnerService.hide();
            }

            if (event instanceof NavigationEnd) {
                document.querySelector('meta[property=og\\:url').setAttribute('content', window.location.href);
            }
        });
    }
}

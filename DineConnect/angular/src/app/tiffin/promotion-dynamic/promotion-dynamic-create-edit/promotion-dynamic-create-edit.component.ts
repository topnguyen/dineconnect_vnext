import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import {
    ComboboxItemDto,
    PromotionCodeGenerateDto,
    PromotionCreateDto,
    PromotionEditDto,
    TiffinPromotionServiceProxy,
    TiffinPromotionType
} from '@shared/service-proxies/service-proxies';
import { Paginator, Table } from 'primeng';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup } from '@angular/forms';

interface IPaging { first: number; page: number; pageCount: number; rows: number; }

@Component({
    templateUrl: './promotion-dynamic-create-edit.component.html',
    styleUrls: ['./promotion-dynamic-create-edit.component.scss'],
    animations: [appModuleAnimation()]
})
export class PromotionDynamicCreateEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('tabSet', { static: true }) tabSet: TabsetComponent;
    @ViewChild('generateForm', { static: true }) generateForm: FormGroup;

    tabIndexDefault;

    promotion = new PromotionEditDto();
    promotionCodeGenerate = new PromotionCodeGenerateDto();
    saving = false;
    promotionTypes: ComboboxItemDto[] = [];
    filterText = '';
    totalClaimed = 0;
    paging: IPaging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };
    promotionId: number;
    isDisabledAutoAttach = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
    ) {
        super(injector);

        this.tabIndexDefault = +this._activatedRoute.snapshot.queryParams['t'];
    }

    ngOnInit(): void {
        this._activatedRoute.params
            .subscribe(params => {
                this.promotionId = +params['id'];

                this.promotionId = isNaN(this.promotionId) ? undefined : this.promotionId;

                if (this.promotionId) {
                    this.getPromotion(this.promotionId);
                }

                if (this.tabIndexDefault) {
                    this.tabSet.tabs[this.tabIndexDefault].active = true;
                }
            });

        this.getPromotionTypes();
    }

    getPromotionTypes() {
        this.promotionTypes = [
            ComboboxItemDto.fromJS({
                value: TiffinPromotionType.Percentage.toString(),
                displayText: 'Percentage',
            }),
            ComboboxItemDto.fromJS({
                value: TiffinPromotionType.Value.toString(),
                displayText: 'Value',
            }),
        ];
    }

    getPromotion(promotionId: number) {
        this._tiffinPromotionServiceProxy.getPromotionForEdit(promotionId)
            .subscribe(result => {
                this.promotion = result;
                this.promotionId = result.id;
                this.promotion.validity = moment(result.validity).format('YYYY-MM-DD') as any;
                this.isDisabledAutoAttach = !result.isAutoAttach && !!result.id;

                this.promotionCodeGenerate.prefix = result.generatePrefix;
                this.promotionCodeGenerate.suffix = result.generateSuffix;
                this.promotionCodeGenerate.totalDigits = result.generateTotalDigits;
                this.promotionCodeGenerate.totalVouchers = result.generateTotalVouchers;

                this.getVouchers();
            });
    }

    save(isClose = false) {
        this.saving = true;

        this.promotion.validity = moment(this.promotion.validity).subtract(new Date().getTimezoneOffset() / 60, 'hour');

        this.promotion.generatePrefix = this.promotionCodeGenerate.prefix;
        this.promotion.generateSuffix = this.promotionCodeGenerate.suffix;
        this.promotion.generateTotalDigits = this.promotionCodeGenerate.totalDigits;
        this.promotion.generateTotalVouchers = this.promotionCodeGenerate.totalVouchers;

        if (this.promotionId) {
            const input = PromotionEditDto.fromJS(this.promotion);

            this.updatePromotion(input, isClose);
        } else {
            const input = PromotionCreateDto.fromJS(this.promotion);

            this.createPromotion(input, isClose);
        }
    }

    createPromotion(input: PromotionCreateDto, isClose: boolean) {
        this._tiffinPromotionServiceProxy.createPromotion(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this._router.navigate(['./create-or-update', result ? result : 'null'],);
                this.generateForm.reset();

                this.getPromotion(result);
                if (isClose) {
                    this.close();
                }
            });
    }

    updatePromotion(input: PromotionEditDto, isClose: boolean) {
        this._tiffinPromotionServiceProxy.updatePromotion(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.getPromotion(input.id);
                if (isClose) {
                    this.close();
                }
            });
    }

    generate() {
        if (this.isDisabledGenerate) {
            return;
        }

        this.saving = true;
        this.promotionCodeGenerate.promotionId = this.promotionId;
        this._tiffinPromotionServiceProxy.generatePromotionCode(this.promotionCodeGenerate)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe((result) => {
                this.getPromotion(this.promotionId);
                this.notify.success(this.l('GeneratedSuccessfully'));
            });
    }

    get isDisabledGenerate() {
        return this.generateForm.invalid
            || !this.promotionId
            // tslint:disable-next-line:triple-equals
            || this.promotionCodeGenerate.totalDigits == 0
            // tslint:disable-next-line:triple-equals
            || this.promotionCodeGenerate.totalVouchers == 0;
    }

    close(): void {
        this._router.navigate(['../../../promotion-dynamic'], {relativeTo: this._activatedRoute});
    }

    onChangePage(paging: IPaging) {
        this.paging = paging;
    }

    getVouchers() {
        if (this.promotion.codes) {
            this.primengTableHelper.records = this.promotion.codes.filter(item => item.code ? item.code.includes(this.filterText) : true);
            this.primengTableHelper.totalRecordsCount = this.primengTableHelper.records.length;
            this.totalClaimed = this.promotion.codes.filter(code => code.isClaimed).length;

            this.paging.page = 0;
        }
    }

    get containerClass(): string {
        if (this.appSession.theme.baseSettings.layout.layoutType === 'fluid') {
            return 'container-fluid';
        }

        return 'container';
    }
}

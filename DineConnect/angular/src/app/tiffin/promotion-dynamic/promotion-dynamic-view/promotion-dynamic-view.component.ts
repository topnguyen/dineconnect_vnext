import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PromotionEditDto, TiffinPromotionServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Paginator, Table } from 'primeng';

interface IPaging { first: number; page: number; pageCount: number; rows: number; }

@Component({
  templateUrl: './promotion-dynamic-view.component.html',
  styleUrls: ['./promotion-dynamic-view.component.scss'],
  animations: [appModuleAnimation()]
})
export class PromotionDynamicViewComponent extends AppComponentBase implements OnInit {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  promotion = new PromotionEditDto();
  saving = false;
  filterText = '';
  totalClaimed = 0;
  paging: IPaging = {
    first: 0,
    page: 1,
    rows: this.primengTableHelper.defaultRecordsCountPerPage,
    pageCount: 0
  };

  promotionId: number;

  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
    private _fileDownloadService: FileDownloadService

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._activatedRoute.params
      .subscribe(params => {
        this.promotionId = +params['id'];

        this.promotionId = isNaN(this.promotionId) ? undefined : this.promotionId;

        if (this.promotionId) {
          this.getPromotion(this.promotionId);
        }
      });
  }

  getPromotion(promotionId: number) {
    this.primengTableHelper.showLoadingIndicator();
    this._tiffinPromotionServiceProxy.getPromotionForEdit(promotionId)
      .subscribe(result => {
        this.promotion = result;
        this.promotionId = result.id;
        this.getVouchers();
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  onChangePage(paging: IPaging) {
    this.paging = paging;
  }

  getVouchers() {
    if (this.promotion.codes) {
      this.primengTableHelper.records = this.promotion.codes.filter(item => item.code ? item.code.includes(this.filterText) : true);
      this.primengTableHelper.totalRecordsCount = this.primengTableHelper.records.length;
      this.totalClaimed = this.promotion.codes.filter(code => code.isClaimed).length;

      this.paging.page = 0;
    }
  }

  exportToExcel(): void {
    this._tiffinPromotionServiceProxy.getPromotionCodeToExcel(
      this.promotionId,
    ).subscribe(result => {
      this._fileDownloadService.downloadTempFile(result);
    });
  }

  get containerClass(): string {
    if (this.appSession.theme.baseSettings.layout.layoutType === 'fluid') {
      return 'container-fluid';
    }

    return 'container';
  }
}

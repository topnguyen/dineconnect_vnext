import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

import { ComboboxItemDto, PromotionListDto, TiffinPromotionServiceProxy, TiffinPromotionType } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './promotion-dynamic.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./promotion-dynamic.component.scss'],
    animations: [appModuleAnimation()]
})
export class PromotionDynamicComponent extends AppComponentBase implements OnInit {
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    dateRange: Date[] = [
        moment().toDate(),
        moment().toDate(),
    ];;
    ranges = [];
    promotionTypes: any[] = [];
    promotionType: any;
    sortField = 'name';
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private activeRoute: ActivatedRoute,
        private render: Renderer2,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);

        this.ranges = [{
            value: [new Date(), new Date()],
            label: this.l('Today')
        }, {
            value: [new Date(new Date().setDate(new Date().getDate() + 1)), new Date(new Date().setDate(new Date().getDate() + 1))],
            label: this.l('Tomorrow')
        }, {
            value: [new Date(), new Date(new Date().setDate(new Date().getDate() + 6))],
            label: this.l('Next7Days')
        }, {
            value: [new Date(), new Date(new Date().setDate(new Date().getDate() + 29))],
            label: this.l('Next30Days')
        }, {
            value: [new Date(), moment().endOf('month').toDate()],
            label: this.l('ThisMonth')
        }, {
            value: [new Date(new Date(new Date().setDate(1)).setMonth(new Date().getMonth() + 1)), moment().add(1, 'month').endOf('month').toDate()],
            label: this.l('NextMonth')
        }];
    }

    ngOnInit(): void {
        this.getPromotionTypes();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getPromotions();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getPromotionTypes() {
        this.promotionTypes = [
            ComboboxItemDto.fromJS({
                value: '',
                displayText: 'All',
            }),
            ComboboxItemDto.fromJS({
                value: TiffinPromotionType.Percentage.toString(),
                displayText: 'Percentage',
            }),
            ComboboxItemDto.fromJS({
                value: TiffinPromotionType.Value.toString(),
                displayText: 'Value',
            }),
        ];
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    exportToExcel(): void {
        let promotionTypeId = this.promotionType ? (isNaN(+this.promotionType.value) ? undefined : this.promotionType.value) : undefined;

        this.primengTableHelper.showLoadingIndicator();
        this._tiffinPromotionServiceProxy.getPromotionsToExcel(
            this.filterText,
            (this.dateRange && this.dateRange.length > 0) ? moment(this.dateRange[0]).startOf('day') : undefined,
            (this.dateRange && this.dateRange.length > 0) ? moment(this.dateRange[1]).endOf('day') : undefined,
            promotionTypeId,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable) || this.sortField,
            this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent),
            this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent)
        ).subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    createOrEditPromotion(promotionId?: number): void {
        this._router.navigate(['create-or-update', promotionId ? promotionId : 'null'],{relativeTo: this.activeRoute});
    }

    viewPromotion(promotionId?: number) {
        this._router.navigate(['view', promotionId ? promotionId : 'null'], {relativeTo: this.activeRoute});
    }

    generate(promotionId?: number) {
        this._router.navigate(['create-or-update', promotionId ? promotionId : 'null'], {relativeTo: this.activeRoute,queryParams: {t: 2}});
    }

    getPromotions(event?: LazyLoadEvent) {
        this.lazyLoadEvent = event;

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        let promotionTypeId = this.promotionType ? (isNaN(+this.promotionType.value) ? undefined : this.promotionType.value) : undefined;

        this.primengTableHelper.showLoadingIndicator();

        this._tiffinPromotionServiceProxy.getPromotions(
            this.filterText,
            (this.dateRange && this.dateRange.length > 0) ? moment(this.dateRange[0]).startOf('day') : undefined,
            (this.dateRange && this.dateRange.length > 0) ? moment(this.dateRange[1]).endOf('day') : undefined,
            promotionTypeId,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable) || this.sortField,
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }


    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    deletePromotion(promotion: PromotionListDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tiffinPromotionServiceProxy.deletePromotion(promotion.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.promotionType = undefined;
        this.dateRange = undefined;
        this.daterangePicker.nativeElement.value = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

}

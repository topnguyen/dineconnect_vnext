import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { ProductSetsComponent } from './product-sets/product-sets.component';
import { ProductOffersComponent } from './product-offers/product-offers.component';
import { CreateOrEditProductOfferComponent } from './product-offers/create-or-edit-product-offer.component';
import { CreateOrEditProductSetComponent } from './product-sets/create-or-edit-product-set.component';
import { TemplateEnginesComponent } from './template-engines/template-engines.component';
import { CreateOrEditTemplateEngineComponent } from './template-engines/create-or-edit-template-engine.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { CheduleCalendarComponent } from './schedules/schedule-calendar/schedule-calendar.component';
import { AddOrEditCheduleComponent } from './schedules/add-or-edit-chedule/add-or-edit-chedule.component';
import { AddOrEditCalendarComponent } from './schedules/add-or-edit-calendar/add-or-edit-calendar.component';
import { PrintDemoComponent } from './printing/print-demo.component';
import { CreateOrEditCustomerComponent } from './customers/create-or-edit-customer.component';
import { TiffinProductOfferTypesComponent } from './product-offer-types/tiffin-product-offer-types.component';
import { OrderComponent } from './report/order/order.component';
import { BalanceComponent } from './report/balance/balance.component';
import { PaymentComponent } from './report/payment/payment.component';
import { KitchenComponent } from './report/kitchen/kitchen.component';
import { DriverComponent } from './report/driver/driver.component';
import { CustomerTransactionsComponent } from './customers/customer-transactions/customer-transactions.component';
import { MealTimesComponent } from './meal-times/meal-times.component';
import { TiffinOrderTagsComponent } from './order-tags/order-tags.component';
import { PreparationComponent } from './report/preparation/preparation.component';
import { TiffinTransactionReportComponent } from './report/transaction/tiffin-transaction-report.component';
import { FaqsComponent } from './faqs/faqs.component';
import { HomepageBannerComponent } from './homepage-banner/homepage-banner.component';
import { PromotionDynamicComponent } from './promotion-dynamic/promotion-dynamic.component';
import { PromotionDynamicCreateEditComponent } from './promotion-dynamic/promotion-dynamic-create-edit/promotion-dynamic-create-edit.component';
import { PromotionDynamicViewComponent } from './promotion-dynamic/promotion-dynamic-view/promotion-dynamic-view.component';
import { TimeSlotsComponent } from './time-slots/time-slots.component';
import { EditTimeSlotsComponent } from './time-slots/edit-time-slots/edit-time-slots.component';
import { CustomerReportComponent } from './report/customer-report/customer-report.component';
import { MealPlanReportComponent } from './report/meal-plan-report/meal-plan-report.component';
import { CustomerOrderComponent } from './customers/customer-order/customer-order.component';
import { CollectReportComponent } from './report/collect-report/collect-report.component';
import { CustomerAddressComponent } from './customers/customer-address/customer-address.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'customers', component: CustomersComponent, data: { permission: 'Pages.Tenant.Tiffin.Customer.Customers' } },
                    { path: 'customers/create-or-update/:id', component: CreateOrEditCustomerComponent, data: { permission: 'Pages.Tenant.Tiffin.Customer.Customers.Create' } },
                    { path: 'customerTransactions', component: CustomerTransactionsComponent, data: { permission: 'Pages.Tenant.Tiffin.Customer.Transactions' } },
                    { path: 'customer-order', component: CustomerOrderComponent, data: { permission: 'Pages' } },
                    { path: 'customers/customer-address/:id/:customerId', component: CustomerAddressComponent, data: { permission: 'Pages' } },
                    { path: 'manage/mealTimes', component: MealTimesComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.MealTimes' } },
                    { path: 'manage/time-slots', component: TimeSlotsComponent, data: { permission: 'Pages.Tenant.Tiffin.TimeSlot' } },
                    { path: 'manage/time-slots/edit', component: EditTimeSlotsComponent, data: { permission: 'Pages.Tenant.Tiffin.TimeSlot.Edit' } },
                    { path: 'manage/productSets', component: ProductSetsComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.ProductSets' } },
                    { path: 'manage/productSets/create-or-update/:id', component: CreateOrEditProductSetComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.ProductSets' } },
                    { path: 'manage/productOfferTypes', component: TiffinProductOfferTypesComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.ProductOfferTypes' } },
                    { path: 'manage/productOffers', component: ProductOffersComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.ProductOffers' } },
                    { path: 'manage/productOffers/create-or-update/:id', component: CreateOrEditProductOfferComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.ProductOffers.Create' } },
                    { path: 'templateEngines', component: TemplateEnginesComponent, data: { permission: 'Pages.Administration.Tenant.TemplateEngines' } },
                    { path: 'templateEngines/create-or-update/:id', component: CreateOrEditTemplateEngineComponent, data: { permission: 'Pages.Administration.Tenant.TemplateEngines.Create' } },
                    { path: 'manage/schedules', component: SchedulesComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Schedules' } },
                    { path: 'manage/schedules/schedule-calendar', component: CheduleCalendarComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Schedules' } },
                    { path: 'manage/schedules/schedule', component: AddOrEditCheduleComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Schedules.EditSchedule' } },
                    { path: 'manage/schedules/edit_calendar', component: AddOrEditCalendarComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Schedules.EditCalender' } },
                    { path: 'manage/orderTags', component: TiffinOrderTagsComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.OrderTags' } },
                    { path: 'manage/faqs', component: FaqsComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Faq' } },
                    { path: 'manage/homepage-banner', component: HomepageBannerComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.HomeBanner' } },
                    { path: 'manage/promotion-dynamic', component: PromotionDynamicComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Promotion' } },
                    { path: 'manage/promotion-dynamic/create-or-update/:id', component: PromotionDynamicCreateEditComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Promotion.Edit' } },
                    { path: 'manage/promotion-dynamic/view/:id', component: PromotionDynamicViewComponent, data: { permission: 'Pages.Tenant.Tiffin.Manage.Promotion' } },

                    { path: 'report/order', component: OrderComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.OrderSummary' } },
                    { path: 'report/balance', component: BalanceComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.Balance' } },
                    { path: 'report/customer', component: CustomerReportComponent, data: { permission: 'Pages.Tenant.Tiffin.Report' } },
                    { path: 'report/payment', component: PaymentComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.Payment' } },
                    { path: 'report/kitchen', component: KitchenComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.Kitchen' } },
                    { path: 'report/driver', component: DriverComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.Driver' } },
                    { path: 'report/preparation', component: PreparationComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.Preparation' } },
                    { path: 'report/tiffintransaction', component: TiffinTransactionReportComponent, data: { permission: 'Pages.Tenant.Tiffin.Report.TiffinTransaction' } },
                    { path: 'report/meal-plan', component: MealPlanReportComponent, data: { permission: 'Pages.Tenant.Tiffin.Report' } },
                    { path: 'report/collect', component: CollectReportComponent, data: { permission: 'Pages.Tenant.Tiffin.Report' } },
                    { path: 'print', component: PrintDemoComponent, data: { permission: '' } },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class TiffinRoutingModule { }

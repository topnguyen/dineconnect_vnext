﻿import { Component, ViewChild, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {
    ProductSetsServiceProxy,
    CreateOrEditProductSetDto,
    IdNameDto,
    ProductSetProductDto,
    MenuItemServiceProxy,
    CommonLookupServiceProxy,
    NameValueDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUploader } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DomSanitizer } from '@angular/platform-browser';
import { TokenService, IAjaxResponse } from 'abp-ng2-module';
import { FileUpload } from 'primeng';
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';

@Component({
    selector: 'createOrEditProductSet',
    templateUrl: './create-or-edit-product-set.component.html',
    styleUrls: ['./product-sets.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditProductSetComponent extends AppComponentBase implements OnInit {
    @ViewChild('ImageFileUpload', { static: true }) imageFileUpload: FileUpload;
    @ViewChild('selectProductsLookupModal', { static: true }) selectProductsLookupModal: CommonMultipleLookupModalComponent;

    active = false;
    saving = false;
    productSetId: number;
    productSet: CreateOrEditProductSetDto = new CreateOrEditProductSetDto();
    uploadImageToken: string = null;
    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
    uploader: FileUploader;
    filePreviewPath: any;
    products: [];

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _productSetsServiceProxy: ProductSetsServiceProxy,
        private sanitizer: DomSanitizer,
        private _menuItemService: MenuItemServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.productSetId = +params['id']; // (+) converts string 'id' to a number

            if (isNaN(this.productSetId)) {
                this.productSetId = undefined;
            }
            this._productSetsServiceProxy.getProductSetForEdit(this.productSetId).subscribe((result) => {
                this.productSet = result.productSet;
            });
        });
        this.uploadInit();
        this.configLookup();
    }

    configLookup() {
        this.selectProductsLookupModal.configure({
            title: this.l('SelectProducts'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                return this._menuItemService.getSinglePortionItems(filter, undefined, [], 'name', maxResultCount, skipCount);
            }
        });
    }

    save(): void {
        this.saving = true;

        if (this.uploadImageToken) {
            this.productSet.uploadedImageToken = this.uploadImageToken;
        }

        this._productSetsServiceProxy
            .createOrEdit(this.productSet)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    uploadInit() {
        this.uploader = new FileUploader({
            url: this.remoteServiceBaseUrl,
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader.onAfterAddingFile = (f) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
            this.filePreviewPath = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(f._file));
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l('UploadImageSuccessfully'));
                if (ajaxResponse.result.url != null) this.productSet.images = ajaxResponse.result.url;
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
    }

    removeFile(item) {
        item.remove();
        this.uploadImageToken = undefined;
    }

    addProduct(): void {
        const products = (this.productSet.products || []).map(item => {
            return IdNameDto.fromJS({
                id: item.productId,
                name: item.productName
            }) 
        })
        this.selectProductsLookupModal.show(products);
    }

    addProductToList(event: IdNameDto[]) {
        const lstProduct = event || [];

        this.productSet.products = lstProduct.map(item => {
            return new ProductSetProductDto({
                productId: item.id,
                productName: item.name
            } as any);
        })
    }

    removeProduct(product: ProductSetProductDto) {
        const index: number = this.productSet.products.indexOf(product);
        if (index !== -1) {
            this.productSet.products.splice(index, 1);
        }
    }

    close() {
        this._router.navigate(['/app/tiffin/manage/productSets']);
    }
}

﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetProductSetForViewDto, ProductSetDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewProductSetModal',
    templateUrl: './view-productSet-modal.component.html'
})
export class ViewProductSetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetProductSetForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetProductSetForViewDto();
        this.item.productSet = new ProductSetDto();
    }

    show(item: GetProductSetForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TiffinRoutingModule } from "./tiffin-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/shared/common/app-common.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { CountoModule } from "angular2-counto";
import { EasyPieChartModule } from "ng2modules-easypiechart";
import { EditorModule } from "primeng/editor";
import { FileUploadModule } from "ng2-file-upload";
import { ImageCropperModule } from "ngx-image-cropper";
import { TableModule } from "primeng/table";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxEasypiechartModule } from "ngx-easypiechart";
import { NgxTagsInputModule } from "ngx-tags-input";
import { TagInputModule } from "ngx-chips";
import { QueryBuilderModule } from "angular2-query-builder";
import { ProductSetsComponent } from "@app/tiffin/product-sets/product-sets.component";
import { ViewProductSetModalComponent } from "@app/tiffin/product-sets/view-productSet-modal.component";
import { CreateOrEditProductSetComponent } from "@app/tiffin/product-sets/create-or-edit-product-set.component";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
// import { CreateOrEditCustomerComponent } from './customers/create-or-edit-customer/create-or-edit-customer.component';
import { ProductOffersComponent } from "../tiffin/product-offers/product-offers.component";
import { CreateOrEditProductOfferComponent } from "../tiffin/product-offers/create-or-edit-product-offer.component";
import { SelectItemModalComponent } from "./select-item-modal/select-item-modal.component";
import { TemplateEnginesComponent } from "./template-engines/template-engines.component";
import { CreateOrEditTemplateEngineComponent } from "./template-engines/create-or-edit-template-engine.component";
import { SchedulesComponent } from "./schedules/schedules.component";
import { AddOrEditCheduleComponent } from "./schedules/add-or-edit-chedule/add-or-edit-chedule.component";
import { AddOrEditCalendarComponent } from "./schedules/add-or-edit-calendar/add-or-edit-calendar.component";
import { MultiselectItemModalComponent } from "./schedules/multiselect-item-modal/multiselect-item-modal.component";
import { PrintConfigModalComponent } from "./printing/print-config-modal.component";
import { PrintDemoComponent } from "./printing/print-demo.component";
import { CreateOrEditCustomerComponent } from "./customers/create-or-edit-customer.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService,
} from "ngx-bootstrap/datepicker";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";
import { TiffinProductOfferTypesComponent } from "./product-offer-types/tiffin-product-offer-types.component";
import { CreateOrEditTiffinProductOfferTypeModalComponent } from "./product-offer-types/create-or-edit-product-offer-type-modal.component";
import { OrderComponent } from "./report/order/order.component";
import { BalanceComponent } from "./report/balance/balance.component";
import { PaymentComponent } from "./report/payment/payment.component";
import { OrderExportModalComponent } from "./report/order/order-export-modal/order-export-modal.component";
import { CustomerExportModalComponent } from "./report/balance/customer-export-modal/customer-export-modal.component";
import { KitchenComponent } from "./report/kitchen/kitchen.component";
import { DriverComponent } from "./report/driver/driver.component";
import { ProductSetsInOfferModalComponent } from "./product-offers/product-sets-in-offer-modal.component";
import { DriverExportModalComponent } from "./report/driver/driver-export-modal/driver-export-modal.component";
import { KitchenExportModalComponent } from "./report/kitchen/kitchen-export-modal/kitchen-export-modal.component";
import { CustomerTransactionsComponent } from "./customers/customer-transactions/customer-transactions.component";
import { EditCustomerOfferModalComponent } from "./customers/customer-transactions/edit-balance-modal.component";
import { ChangeHistoryModalComponent } from "./customers/customer-transactions/change-history-modal.component";
import { MealTimesComponent } from "./meal-times/meal-times.component";
import { CreateOrEditMealTimeModalComponent } from "./meal-times/create-or-edit-meal-time-modal.component";
import { CreateOrEditTiffinOrderTagModalComponent } from "./order-tags/create-or-edit-order-tag-modal.component";
import { TiffinOrderTagsComponent } from "./order-tags/order-tags.component";
import { PreparationComponent } from "./report/preparation/preparation.component";
import { EditorModule as TinymceModule } from "@tinymce/tinymce-angular";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import { TextMaskModule } from "angular2-text-mask";
import { CardModule, DataViewModule, PickListModule, SidebarModule } from "primeng";
import { DragulaModule } from "ng2-dragula";

import { AutoCompleteModule } from "primeng/autocomplete";
import { InputMaskModule } from "primeng/inputmask";
import { CheckboxModule } from "primeng/checkbox";
import { CalendarModule } from "primeng/calendar";
import { MultiSelectModule } from "primeng/multiselect";
import { DropdownModule } from "primeng/dropdown";
import { PasswordModule } from "primeng/password";
import { DragDropModule } from "primeng/dragdrop";
import { FileUploadModule as PrimeNgFileUploadModule } from "primeng/fileupload";
import { ChipsModule } from "primeng/chips";

import { PaginatorModule } from "primeng/paginator";
import { CheduleCalendarComponent } from "./schedules/schedule-calendar/schedule-calendar.component";
import { TiffinTransactionReportComponent } from "./report/transaction/tiffin-transaction-report.component";
import { HomepageBannerComponent } from "./homepage-banner/homepage-banner.component";
import { FaqsComponent } from "./faqs/faqs.component";
import { CreateFaqModalComponent } from "./faqs/create-faq-modal.component";
import { EditFaqModalComponent } from "./faqs/edit-faq-modal.component";
import { PromotionDynamicComponent } from "./promotion-dynamic/promotion-dynamic.component";
import { PromotionDynamicCreateEditComponent } from "./promotion-dynamic/promotion-dynamic-create-edit/promotion-dynamic-create-edit.component";
import { PromotionDynamicViewComponent } from "./promotion-dynamic/promotion-dynamic-view/promotion-dynamic-view.component";
import { ImportModalComponent } from "./shared/import-modal/import-modal.component";
import { TimeSlotsComponent } from "./time-slots/time-slots.component";
import { EditTimeSlotsComponent } from "./time-slots/edit-time-slots/edit-time-slots.component";
import { CustomerReportComponent } from "./report/customer-report/customer-report.component";
import { BalancesDetailModalComponent } from "./report/customer-report/balances-detail-modal/balances-detail-modal.component";
import { MealPlanReportComponent } from "./report/meal-plan-report/meal-plan-report.component";
import { EditBalanceDetailModalComponent } from "./report/customer-report/balances-detail-modal/edit-balance-modal/edit-balance-detail-modal.component";
import { AttachedPromotionModalComponent } from "./report/customer-report/attached-promotion-modal/attached-promotion-modal.component";
import { CustomerOrderComponent } from "./customers/customer-order/customer-order.component";
import { CollectReportComponent } from './report/collect-report/collect-report.component';
import { CustomerAddressComponent } from './customers/customer-address/customer-address.component';
@NgModule({
    imports: [
        TiffinRoutingModule,
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        CountoModule,
        AutoCompleteModule,
        EditorModule,
        FileUploadModule,
        PrimeNgFileUploadModule,
        ImageCropperModule,
        InputMaskModule,
        CheckboxModule,
        CalendarModule,
        MultiSelectModule,
        NgSelectModule,
        NgxEasypiechartModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        NgxTagsInputModule,
        TagInputModule,
        TimepickerModule.forRoot(),
        AppBsModalModule,
        UtilsModule,
        QueryBuilderModule,
        ChipsModule,
        TinymceModule,
        DragDropModule,
        PasswordModule,
        TextMaskModule,
        PickListModule,
        DragulaModule.forRoot(),
        DataViewModule,
        CardModule,
        SidebarModule
    ],
    declarations: [
        ViewProductSetModalComponent,
        ProductSetsComponent,
        CreateOrEditProductSetComponent,
        ProductOffersComponent,
        CreateOrEditProductOfferComponent,
        SelectItemModalComponent,
        TemplateEnginesComponent,
        CreateOrEditTemplateEngineComponent,
        SchedulesComponent,
        AddOrEditCheduleComponent,
        AddOrEditCalendarComponent,
        CheduleCalendarComponent,
        MultiselectItemModalComponent,
        PrintConfigModalComponent,
        PrintDemoComponent,
        TiffinProductOfferTypesComponent,
        CreateOrEditTiffinProductOfferTypeModalComponent,
        OrderComponent,
        BalanceComponent,
        PaymentComponent,
        OrderExportModalComponent,
        CustomerExportModalComponent,
        KitchenComponent,
        DriverComponent,
        ProductSetsInOfferModalComponent,
        DriverExportModalComponent,
        KitchenExportModalComponent,
        CustomerTransactionsComponent,
        EditCustomerOfferModalComponent,
        ChangeHistoryModalComponent,
        MealTimesComponent,
        CreateOrEditMealTimeModalComponent,
        TiffinOrderTagsComponent,
        CreateOrEditTiffinOrderTagModalComponent,
        PreparationComponent,
        TiffinTransactionReportComponent,
        HomepageBannerComponent,
        FaqsComponent,
        CreateFaqModalComponent,
        EditFaqModalComponent,
        PromotionDynamicComponent,
        PromotionDynamicCreateEditComponent,
        PromotionDynamicViewComponent,
        ImportModalComponent,
        TimeSlotsComponent,
        EditTimeSlotsComponent,
        CustomerReportComponent,
        BalancesDetailModalComponent,
        MealPlanReportComponent,
        EditBalanceDetailModalComponent,
        AttachedPromotionModalComponent,
        CustomerOrderComponent,
        CollectReportComponent,
        CustomerAddressComponent,
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
    ],
})
export class TiffinModule {}

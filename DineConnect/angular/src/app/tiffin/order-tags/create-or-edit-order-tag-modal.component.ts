import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TiffinOrderTagsServiceProxy, CreateOrEditTiffinOrderTagDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditTiffinOrderTagModal',
    templateUrl: './create-or-edit-order-tag-modal.component.html'
})
export class CreateOrEditTiffinOrderTagModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    tiffinOrderTag: CreateOrEditTiffinOrderTagDto = new CreateOrEditTiffinOrderTagDto();

    constructor(
        injector: Injector,
        private _tiffinOrderTagsServiceProxy: TiffinOrderTagsServiceProxy
    ) {
        super(injector);
    }

    show(tiffinOrderTagId?: number): void {
        if (!tiffinOrderTagId) {
            this.tiffinOrderTag = new CreateOrEditTiffinOrderTagDto();
            this.tiffinOrderTag.id = tiffinOrderTagId;

            this.active = true;
            this.modal.show();
        } else {
            this._tiffinOrderTagsServiceProxy.getTiffinOrderTagForEdit(tiffinOrderTagId)
                .subscribe(result => {
                    this.tiffinOrderTag = result;

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;

        this._tiffinOrderTagsServiceProxy.createOrEdit(this.tiffinOrderTag)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { TiffinOrderTagsServiceProxy, TiffinOrderTagDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditTiffinOrderTagModalComponent } from './create-or-edit-order-tag-modal.component';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './order-tags.component.html',
    styleUrls: ['./order-tags.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TiffinOrderTagsComponent extends AppComponentBase {
    @ViewChild('createOrEditTiffinOrderTagModal', { static: true }) createOrEditTiffinOrderTagModal: CreateOrEditTiffinOrderTagModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _tiffinOrderTagsServiceProxy: TiffinOrderTagsServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getTiffinOrderTags();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getTiffinOrderTags(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._tiffinOrderTagsServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTiffinOrderTag(): void {
        this.createOrEditTiffinOrderTagModal.show();
    }

    deleteTiffinOrderTag(tiffinOrderTag: TiffinOrderTagDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tiffinOrderTagsServiceProxy.delete(tiffinOrderTag.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

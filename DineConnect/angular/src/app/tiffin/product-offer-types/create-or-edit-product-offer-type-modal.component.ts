﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TiffinProductOfferTypesServiceProxy, CreateOrEditTiffinProductOfferTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditTiffinProductOfferTypeModal',
    templateUrl: './create-or-edit-product-offer-type-modal.component.html'
})
export class CreateOrEditTiffinProductOfferTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    tiffinProductOfferType: CreateOrEditTiffinProductOfferTypeDto = new CreateOrEditTiffinProductOfferTypeDto();

    constructor(
        injector: Injector,
        private _tiffinProductOfferTypesServiceProxy: TiffinProductOfferTypesServiceProxy
    ) {
        super(injector);
    }

    show(tiffinProductOfferTypeId?: number): void {
        if (!tiffinProductOfferTypeId) {
            this.tiffinProductOfferType = new CreateOrEditTiffinProductOfferTypeDto();
            this.tiffinProductOfferType.id = tiffinProductOfferTypeId;

            this.active = true;
            this.modal.show();
        } else {
            this._tiffinProductOfferTypesServiceProxy.getTiffinProductOfferTypeForEdit(tiffinProductOfferTypeId).subscribe(result => {
                this.tiffinProductOfferType = result.tiffinProductOfferType;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;

        this._tiffinProductOfferTypesServiceProxy.createOrEdit(this.tiffinProductOfferType)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

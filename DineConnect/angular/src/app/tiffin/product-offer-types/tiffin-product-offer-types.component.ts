import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { TiffinProductOfferTypesServiceProxy, TiffinProductOfferTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditTiffinProductOfferTypeModalComponent } from './create-or-edit-product-offer-type-modal.component';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './tiffin-product-offer-types.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./tiffin-product-offer-types.component.scss'],
    animations: [appModuleAnimation()]
})
export class TiffinProductOfferTypesComponent extends AppComponentBase {
    @ViewChild('createOrEditTiffinProductOfferTypeModal', { static: true }) createOrEditTiffinProductOfferTypeModal: CreateOrEditTiffinProductOfferTypeModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _tiffinProductOfferTypesServiceProxy: TiffinProductOfferTypesServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getTiffinProductOfferTypes();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getTiffinProductOfferTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._tiffinProductOfferTypesServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTiffinProductOfferType(): void {
        this.createOrEditTiffinProductOfferTypeModal.show();
    }

    deleteTiffinProductOfferType(tiffinProductOfferType: TiffinProductOfferTypeDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tiffinProductOfferTypesServiceProxy.delete(tiffinProductOfferType.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._tiffinProductOfferTypesServiceProxy.getTiffinProductOfferTypesToExcel(
            this.filterText,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import { ProductSetsServiceProxy, TiffinProductOffersServiceProxy, MenuItemServiceProxy, ProductSetDto, ScheduleServiceProxy, CreateScheduleDto } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { MultiselectItemModalComponent } from '../multiselect-item-modal/multiselect-item-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
  selector: 'app-add-or-edit-calendar',
  templateUrl: './add-or-edit-calendar.component.html',
  styleUrls: ['./add-or-edit-calendar.component.scss'],
  animations: [appModuleAnimation()]
})
export class AddOrEditCalendarComponent extends AppComponentBase implements OnInit {
  @ViewChild('productSetLookupModal', { static: true }) productSetLookupModal: CommonLookupModalComponent;
  @ViewChild('multiSelectItemModal', { static: true }) multiSelectItemModal: MultiselectItemModalComponent;

  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  filterText = '';
  productSetId: number;
  productId: number;
  dateRange: any;
  startDate: any;
  endDate: any;
  listSchedule: any;
  productSetIdChange: any;
  scheduleIdChange: number;
  lstScheduleChange: CreateScheduleDto[] = [];

  saving = false;

  constructor(
    injector: Injector,
    private _router: Router,
    private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
    private _scheduleService: ScheduleServiceProxy


  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.configLookup();
    // this.getSchedule();
  }

  getSchedule(event?: LazyLoadEvent) {
    if (this.dateRange as any !== undefined) {
      this.startDate = this.dateRange[0];
      this.endDate = this.dateRange[1];
    }
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this._scheduleService
      .getAll(
        undefined,
        undefined,
        this.startDate,
        this.endDate,
        undefined,
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
        this.listSchedule = result.items;
      });
  }
  productSetSelected($event) {
    // check xem no co  change k? neu change thi cap nhat vao
    if (this.productSetIdChange !== $event.id) {
      let index = this.listSchedule.find(x => x.id === this.scheduleIdChange);
      index.productSetId = $event.id;
      index.productSetName = $event.name;
      index.products = [];
      // them phan tu change vao array
      this.lstScheduleChange.push(index);
    }
    this.productSetId = $event.id;
  }

  productSelected($event) {
    this.productId = $event.id;
  }

  showProductSetLookUpModal(schedule): void {
    this.productSetIdChange = schedule.productSetId;
    this.scheduleIdChange = schedule.id;
    console.log('id_ban_dau', schedule.productSetId);
    this.productSetLookupModal.show();
  }
  showProductLookUpModal(product): void {
    this.multiSelectItemModal.show(product);
  }
  getDay(date) {
    let result = '';
    switch (date.day()) {
      case 1:
        result = 'Monday';
        break;
      case 2:
        result = 'Tuesday';
        break;
      case 3:
        result = 'Wednesday';
        break;
      case 4:
        result = 'Thursday';
        break;
      case 5:
        result = 'Friday';
        break;
      case 6:
        result = 'Saturday';
        break;
      case 0:
        result = 'Sunday';
        break;
    }
    return result;
  }
  saveAll() {
    console.log('lstScheduleChange', this.lstScheduleChange);
    this._scheduleService.updateList(this.lstScheduleChange).subscribe(result => {
      this.notify.success(this.l('SavedSuccessfully'));
      this.lstScheduleChange = [];
    });
  }
  close() {
    this.lstScheduleChange = [];
    this._router.navigate(['/app/tiffin/manage/schedules']);
  }
  menuItemSelectedWhenEditSchedule($event) {
    let index = this.listSchedule.find(x => x.id === $event.id);
    index.products = $event.products;
    // kiem tra trong day xem co chua. neu chua co thi them vao
    console.log('this.listSchedule', this.lstScheduleChange);
    let index2 = this.lstScheduleChange.find(x => x.id === index.id);
    if (!index2) {
      this.lstScheduleChange.push(index);
    }

  }
  configLookup() {
    this.productSetLookupModal.configure({
      title: this.l('SelectAProductSet'),
      dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
        return this._tiffinProductOffersServiceProxy.getAllTiffinProductSetForLookupTable(filter, 'name', skipCount, maxResultCount);
      }
    });
  }

}

import { Component, OnInit, Output, EventEmitter, ViewChild, Injector, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PrimengTableHelper } from '@shared/helpers/PrimengTableHelper';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateScheduleDto } from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { isEqual } from 'lodash';
import { ScheduleServiceProxy } from '@shared/service-proxies/service-proxies-nswag';

@Component({
    selector: 'app-multiselect-item-modal',
    templateUrl: './multiselect-item-modal.component.html',
    styleUrls: ['./multiselect-item-modal.component.scss'],
    animations: [appModuleAnimation()]
})
export class MultiselectItemModalComponent extends AppComponentBase {
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
    @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

    @Input() productSetId: number;
    @Output() menuItemSelected: EventEmitter<any> = new EventEmitter<any>();
    @Output() menuItemSelectWhenEditSchedule: EventEmitter<any> = new EventEmitter<any>();

    selectedMenuItems: any;
    selectedMenuItemsStatic: any;
    scheduleDetailId: number;
    scheduleDetail = new CreateScheduleDto();
    filterText = '';
    locationNotInGroupTableHelper: PrimengTableHelper;

    constructor(injector: Injector, private _scheduleServiceProxy: ScheduleServiceProxy) {
        super(injector);
        this.locationNotInGroupTableHelper = new PrimengTableHelper();
    }

    show(product?): void {
        if (product) {
            this.selectedMenuItems = product.products;
            this.selectedMenuItemsStatic = product.products;
            this.scheduleDetailId = product.id;
            this.productSetId = product.productSetId;
        }
        this.locationNotInGroupTableHelper.records = [];
        this.modal.show();
    }

    shown(): void {
        this.getAll();
    }

    getAll(event?: LazyLoadEvent) {
        if (this.locationNotInGroupTableHelper.shouldResetPaging(event)) {
            this.locationPaginator.changePage(0);
            return;
        }
        this.locationNotInGroupTableHelper.showLoadingIndicator();
        this._scheduleServiceProxy
            .getMenuItemForMulti(
                this.filterText,
                this.productSetId,
                this.locationNotInGroupTableHelper.getSorting(this.locationDataTable),
                this.locationNotInGroupTableHelper.getMaxResultCount(this.locationPaginator, event),
                this.locationNotInGroupTableHelper.getSkipCount(this.locationPaginator, event)
            )
            .subscribe((result) => {
                this.locationNotInGroupTableHelper.records = result.items;
                this.locationNotInGroupTableHelper.totalRecordsCount = result.totalCount;
                this.locationNotInGroupTableHelper.records = result.items;
                this.locationNotInGroupTableHelper.hideLoadingIndicator();
            });
    }

    save() {
        if (!isEqual(this.selectedMenuItemsStatic, this.selectedMenuItems)) {
            this.scheduleDetail.products = this.selectedMenuItems;
            this.scheduleDetail.id = this.scheduleDetailId;
            this.menuItemSelectWhenEditSchedule.emit(this.scheduleDetail);
        }

        this.menuItemSelected.emit(this.selectedMenuItems);
        this.close();
    }

    close() {
        this.selectedMenuItems = null;
        this.modal.hide();
    }
}

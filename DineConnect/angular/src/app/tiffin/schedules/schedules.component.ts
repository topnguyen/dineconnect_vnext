import { Component, OnInit, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';

import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';

import {
    TiffinProductOffersServiceProxy,
    MenuItemServiceProxy,
    ScheduleServiceProxy,
    CreateScheduleDto,
    UpdateScheduleStatusDto,
    ScheduleDetailStatus
} from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { ImportModalComponent } from '../shared/import-modal/import-modal.component';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-schedules',
    templateUrl: './schedules.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./schedules.component.scss'],
    animations: [appModuleAnimation()]
})
export class SchedulesComponent extends AppComponentBase implements OnInit {
    @ViewChild('productSetLookupModal', { static: true }) productSetLookupModal: CommonLookupModalComponent;
    @ViewChild('productLookupModal', { static: true }) productLookupModal: CommonLookupModalComponent;
    @ViewChild('importModal', { static: true }) importModal: ImportModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    dateRange: Date[] = [
        new Date(),
        new Date(),
    ];
    filterText = '';
    productSetId: number;
    productId: number;
    startDate: any;
    endDate: any;
    selectedProductName: '';
    selectedProductSetName: '';
    uploadUrl: string;
    ScheduleDetailStatus = ScheduleDetailStatus;
    selectedSchedules: CreateScheduleDto[] = [];

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _scheduleService: ScheduleServiceProxy
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadFile';
    }

    ngOnInit(): void {
        this.configLookup();
        this.configImport();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if ((this.dateRange as any) !== undefined) {
            this.startDate = this.transfromDate(this.dateRange[0]);
            this.endDate = this.transfromDate(this.dateRange[1]);
        }
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._scheduleService
            .getAll(
                this.productSetId,
                this.productId,
                this.startDate,
                this.endDate,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => {
                this.primengTableHelper.hideLoadingIndicator();
            }))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    calender(): void {
        this._router.navigate(['/app/tiffin/manage/schedules/schedule-calendar']);
    }

    schedule(): void {
        this._router.navigate(['/app/tiffin/manage/schedules/schedule']);
    }

    editCalendar(): void {
        this._router.navigate(['/app/tiffin/manage/schedules/edit_calendar']);
    }

    deleteSchedule(scheduleDeltail: CreateScheduleDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._scheduleService.delete(scheduleDeltail.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    deleteSchedules() {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                if (!this.selectedSchedules || this.selectedSchedules.length === 0) {
                    this.notify.info(this.l('PublishSchedules_Required'));
                    return;
                }

                const ids = this.selectedSchedules.map((item) => item.id);

                this._scheduleService.bulkDelete(ids).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));
                    this.selectedSchedules.length = 0;
                    this.dataTable.updateSelectionKeys();
                    this.reloadPage();
                });
            }
        });
    }

    exportToExcel(event?: LazyLoadEvent): void {
        this._scheduleService
            .getScheduleToExcel(
                this.productSetId,
                this.productId,
                this.startDate,
                this.endDate,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    import() {
        this.importModal.show();
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportSchedulesUploadFailed'));
    }

    productSetSelected($event) {
        this.productSetId = $event.id;
        this.selectedProductSetName = $event.name;
    }

    productSelected($event) {
        this.productId = $event.id;
        this.selectedProductName = $event.name;
    }

    configLookup() {
        this.productSetLookupModal.configure({
            title: this.l('SelectAProductSet'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                return this._tiffinProductOffersServiceProxy.getAllTiffinProductSetForLookupTable(filter, 'name', skipCount, maxResultCount);
            }
        });

        this.productLookupModal.configure({
            title: this.l('SelectProduct'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                return this._menuItemServiceProxy.getAllTiffinProductSetForLookupTable(filter, 'name', skipCount, maxResultCount);
            }
        });
    }

    configImport() {
        this.importModal.configure({
            title: this.l('Import'),
            downloadTemplate: (id?: number) => {
                return this._scheduleService.getImportScheduleTemplateToExcel();
            },
            import: (fileToken: string) => {
                return this._scheduleService.importScheduleToDatabase(fileToken);
            }
        });
    }

    showProductSetLookUpModal(): void {
        this.productSetLookupModal.show();
    }
    showProductLookUpModal(): void {
        this.productLookupModal.show();
    }

    publishSchedules(status: ScheduleDetailStatus) {
        if (!this.selectedSchedules || this.selectedSchedules.length === 0) {
            this.notify.info(this.l('PublishSchedules_Required'));
            return;
        }

        const body = UpdateScheduleStatusDto.fromJS({
            ids: this.selectedSchedules.map((item) => item.id),
            newStatus: status
        });

        this._scheduleService.updateStatus(body.toJSON()).subscribe((result) => {
            this.notify.success(this.l('PublishedSuccessfully'));
            this.selectedSchedules.length = 0;
            this.dataTable.updateSelectionKeys();

            this.getAll();
        });
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.productSetId = undefined;
        this.productId = undefined;
        this.dateRange = undefined;
        this.endDate = undefined;
        this.startDate = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

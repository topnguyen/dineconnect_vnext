import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ProductSetDto,
    MenuItemServiceProxy,
    TiffinProductOffersServiceProxy,
    ProductSetsServiceProxy,
    ScheduleServiceProxy,
    CreateScheduleDto,
    ProductSetProductDto,
    CreateOrEditProductSetDto,
    TiffinOrderServiceProxy
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import * as moment from 'moment';
import { MultiselectItemModalComponent } from '../multiselect-item-modal/multiselect-item-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { isEqual } from 'lodash';

@Component({
    selector: 'app-add-or-edit-chedule',
    templateUrl: './add-or-edit-chedule.component.html',
    styleUrls: ['./add-or-edit-chedule.component.scss'],
    animations: [appModuleAnimation()]
})
export class AddOrEditCheduleComponent extends AppComponentBase implements OnInit {
    @ViewChild('productSetLookupModal', { static: true }) productSetLookupModal: CommonLookupModalComponent;
    @ViewChild('multiSelectItemModal', { static: true }) multiSelectItemModal: MultiselectItemModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    listScheduleNew: CreateScheduleDto[] = [];
    lstProduct: ProductSetProductDto[] = [];
    mealOptions = [];
    date: any;
    filterText = '';
    productSetId: number;
    productSetName: string;
    saving = false;
    selectedProductNames = '';
    selectedMealTime = [];

    constructor(
        injector: Injector,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _router: Router,
        private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _scheduleService: ScheduleServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.configLookup();
    }

    getSchedules(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._scheduleService
            .getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => {
                this.primengTableHelper.hideLoadingIndicator();
            }))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    calender(): void {
        this._router.navigate(['/app/tiffin/manage/schedules/schedule-calendar']);
    }

    close() {
        this._router.navigate(['/app/tiffin/manage/schedules']);
        this.productSetName = null;
        this.productSetId = null;
    }

    deleteProductSet(productSet: ProductSetDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._scheduleService.delete(productSet.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }
    deleteItemsInAddingList() {
        this.date = undefined;
        this.selectedMealTime = [];
        this.productSetId = undefined;
        this.lstProduct = [];
    }
    productSetSelected($event) {
        this.productSetName = $event.name;
        this.productSetId = $event.id;
    }

    productSelected($event) {
        this.lstProduct = $event || [];

        let selectedProducts = [];
        this.lstProduct.map((p) => {
            selectedProducts.push(p.productName);
        });

        this.selectedProductNames = selectedProducts.join(', ');
    }

    configLookup() {
        this.productSetLookupModal.configure({
            title: this.l('SelectAProductSet'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                return this._tiffinProductOffersServiceProxy.getAllTiffinProductSetForLookupTable(filter, 'name', skipCount, maxResultCount);
            }
        });

        this._tiffinOrderProxy.getScheduleTimes().subscribe((result) => {
            this.mealOptions = result.items;
        });
    }

    showProductSetLookUpModal(): void {
        this.productSetLookupModal.show();
    }

    showProductLookUpModal(): void {
        this.multiSelectItemModal.show();
    }

    getDates(startDate, stopDate) {
        let dateArray = new Array();
        let currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(new Date(currentDate));
            currentDate = currentDate.add(1, 'days');
        }
        return dateArray;
    }

    addScheduleToList() {
        if (!this.productSetId) {
            this.message.error('Please choose a product set');
        } else if (!this.lstProduct) {
            this.message.error('Please choose a products');
        } else if (!this.date) {
            this.message.error('Please choose a dates');
        } else {

            let start = moment(this.date[0]).hour(0).minute(0).second(0);
            let end = moment(this.date[1]).hour(0).minute(0).second(0);
            let dateRange = this.getDates(start, end);

            dateRange.forEach((element) => {
                this.selectedMealTime.map((time) => {
                    let mealtime = this.mealOptions.find((x) => +x.value === +time);

                    let schedule = new CreateScheduleDto();
                    schedule.tenantId = this.appSession.tenantId;
                    schedule.mealTimeId = +time;
                    schedule.mealTimeName = mealtime.displayText;
                    schedule.productSetName = this.productSetName;
                    schedule.productSetId = this.productSetId;
                    schedule.date = element;
                    let addedMenuItem = new ProductSetProductDto();

                    schedule.products = this.lstProduct;
                    schedule.date = moment(schedule.date).add(-new Date().getTimezoneOffset(), 'minutes');

                    const isExisted = this.listScheduleNew.some((item) => {
                        return (
                            item.productSetId == schedule.productSetId &&
                            item.date.isSame(schedule.date) &&
                            item.mealTimeId == schedule.mealTimeId &&
                            isEqual(item.products, schedule.products)
                        );
                    });
            
                    if (isExisted) {
                        this.message.error('Schedule is existed in List of Schedule New');
                        return;
                    } else {
                        this.listScheduleNew.push(schedule);
                    }
                });
            });

            this.deleteItemsInAddingList();
        }
    }

    removeSchedule(schedule: CreateScheduleDto) {
        const index: number = this.listScheduleNew.indexOf(schedule);
        if (index !== -1) {
            this.listScheduleNew.splice(index, 1);
        }
    }

    saveAll() {
        this.saving = true;

        this._scheduleService
            .createListSchedule(this.listScheduleNew)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.listScheduleNew = [];
                this.getSchedules();
            });
    }

    refresh() {
        this.listScheduleNew = [];
    }
}

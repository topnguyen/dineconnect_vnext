import { Component, OnInit, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { GetScheduleForMenu, ScheduleServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
@Component({
  selector: 'app-schedule-calendar',
  templateUrl: './schedule-calendar.component.html',
  styleUrls: ['./schedule-calendar.component.scss'],
  animations: [appModuleAnimation()]
})
export class CheduleCalendarComponent extends AppComponentBase implements OnInit {
  dateRange: any;
  current = new Date();
  constructor(injector: Injector,
    private _scheduleService: ScheduleServiceProxy
  ) {
    super(injector);
  }
  listschedule: GetScheduleForMenu[] = [];
  ngOnInit() {
    this.reloadPage();
  }
  reloadPage() {
    let first = moment().startOf('isoWeek').toDate();
    let weekend = moment().endOf('isoWeek').toDate();
    this.current = first;
    this.dateRange = [first, weekend];

    this._scheduleService.getScheduleForMenu(undefined, undefined, moment(first), moment(weekend), undefined, undefined, undefined, undefined)
      .subscribe(result => {
        this.listschedule = result;
      });
  }
  getAll() {
    this.current = this.dateRange[0];
    this._scheduleService.getScheduleForMenu(undefined, undefined, moment(this.dateRange[0]), moment(this.dateRange[1]),undefined, undefined, undefined, undefined)
      .subscribe(result => {
        this.listschedule = result;

        this.listschedule.forEach(item => {
            if (item.listSchedule.length > 0) {
                item.listSchedule.unshift(item.listSchedule[item.listSchedule.length - 1]);
                item.listSchedule.splice(item.listSchedule.length - 1, 1);
            }
        })
      });
  }

  previous() {
    let first = moment(this.current).subtract(1, 'weeks').startOf('isoWeek').toDate();
    let weekend = moment(this.current).subtract(1, 'weeks').endOf('isoWeek').toDate();
    this.dateRange = [first, weekend];

    this.current = first;
    this.getAll();
  }

  next() {
    let first = moment(this.current).add(1, 'weeks').startOf('isoWeek').toDate();
    let weekend = moment(this.current).add(1, 'weeks').endOf('isoWeek').toDate();
    this.dateRange = [first, weekend];

    this.current = first;
    this.getAll();
  }
}

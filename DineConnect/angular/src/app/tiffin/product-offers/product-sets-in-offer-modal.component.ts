import { Component, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Table } from 'primeng/table';

@Component({
  selector: 'product-sets-in-offer-modal',
  templateUrl: './product-sets-in-offer-modal.component.html'
})
export class ProductSetsInOfferModalComponent extends AppComponentBase {

  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild('dataTable', { static: true }) dataTable: Table;

  constructor(
    injector: Injector
  ) {
    super(injector);
  }


  show(items): void {
    this.primengTableHelper.records = items;
    this.modal.show();
  }


  close(): void {
    this.modal.hide();
  }

  shown() { }
}

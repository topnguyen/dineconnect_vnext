﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TiffinProductOffersServiceProxy, TiffinProductOfferDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { ProductSetsInOfferModalComponent } from './product-sets-in-offer-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { NotifyService } from 'abp-ng2-module';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './product-offers.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./product-offers.component.scss'],
    animations: [appModuleAnimation()]
})
export class ProductOffersComponent extends AppComponentBase {

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('productSetsModal', { static: true }) productSetsModal: ProductSetsInOfferModalComponent;

    advancedFiltersAreShown = false;
    filterText = '';
    tiffinProductSetNameFilter = '';
    tiffinProductOfferTypeNameFilter = '';

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getTiffinProductOffers();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getTiffinProductOffers(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._tiffinProductOffersServiceProxy.getAll(
            this.filterText,
            this.tiffinProductSetNameFilter,
            this.tiffinProductOfferTypeNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditProductOffer(id?): void {
        this._router.navigate(['/app/tiffin/manage/productOffers/create-or-update', id ? id : '']);

    }

    deleteTiffinProductOffer(tiffinProductOffer: TiffinProductOfferDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tiffinProductOffersServiceProxy.delete(tiffinProductOffer.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._tiffinProductOffersServiceProxy.getTiffinProductOffersToExcel(
            this.filterText,
            this.tiffinProductSetNameFilter,
            this.tiffinProductOfferTypeNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    showProductSets(items) {
        this.productSetsModal.show(items);
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditTiffinProductOfferDto, TiffinProductOffersServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { HttpHeaders, HttpClient, HttpXsrfTokenExtractor } from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';
import { TokenService, IAjaxResponse } from 'abp-ng2-module';
import { FileUpload } from 'primeng';

@Component({
  selector: 'app-create-or-edit-product-offer',
  templateUrl: './create-or-edit-product-offer.component.html',
  styleUrls: ['./create-or-edit-product-offer.component.scss'],
  animations: [appModuleAnimation()]

})
export class CreateOrEditProductOfferComponent extends AppComponentBase implements OnInit {
  @ViewChild('productSetLookupModal', { static: true }) productSetLookupModal: CommonMultipleLookupModalComponent;
  @ViewChild('productOfferTypeLookupModal', { static: true }) productOfferTypeLookupModal: CommonLookupModalComponent;
  @ViewChild('ImageFileUpload', { static: true }) imageFileUpload: FileUpload;

  saving = false;

  tiffinProductOffer = new CreateOrEditTiffinProductOfferDto();
  tiffinProductOfferId: number;

  tiffinProductSetName = '';
  tiffinProductOfferTypeName = '';
  productSets = [];
  tags = [];

  uploadImageToken: string = null;
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';

  uploader: FileUploader;
  filePreviewPath: any;

  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _tokenService: TokenService,
    private _tiffinProductOffersServiceProxy: TiffinProductOffersServiceProxy,
    private sanitizer: DomSanitizer,
  ) {
    super(injector);

  }

  ngOnInit() {
    this._activatedRoute.params
      .subscribe(params => {
        this.tiffinProductOfferId = params['id']; // (+) converts string 'id' to a number

        if (isNaN(this.tiffinProductOfferId) || this.tiffinProductOfferId === 0) {
          this.tiffinProductOfferId = undefined;
        }
        this._tiffinProductOffersServiceProxy.getTiffinProductOfferForEdit(this.tiffinProductOfferId)
          .subscribe(result => {
            this.tiffinProductOffer = result.tiffinProductOffer;
            this.tiffinProductOfferTypeName = result.tiffinProductOfferTypeName;

            if (this.tiffinProductOffer.tag) {
              let array = this.tiffinProductOffer.tag.split(',');
              array.map(a => {
                this.tags.push({ displayValue: a });
              });
            }

            this.productSetSelected(this.tiffinProductOffer.productSets);

          });
      });

    this.configLookup();
    this.uploadInit();
  }

  save(): void {
    this.saving = true;
    this.setTags();
    if (!this.uploadImageToken && this.uploader.queue.length > 0) {
      this.uploader.uploadAll();
    }

    if (this.uploadImageToken) {
      this.tiffinProductOffer.uploadedImageToken = this.uploadImageToken;
    }

    this._tiffinProductOffersServiceProxy.createOrEdit(this.tiffinProductOffer)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.close();
      });
  }

  close(): void {
    this._router.navigate(['/app/tiffin/manage/productOffers']);
  }

  private setTags() {
    let array = [];
    this.tags.map(tag => {
      array.push(tag.displayValue);
    });

    this.tiffinProductOffer.tag = array.join(',');
  }

  configLookup() {
    this.productSetLookupModal.configure({
      title: this.l('SelectAProductSet'),
      dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
        return this._tiffinProductOffersServiceProxy.getAllTiffinProductSetForLookupTable(filter, 'name', skipCount, maxResultCount);
      }
    });

    this.productOfferTypeLookupModal.configure({
      title: this.l('SelectATiffinProductOfferType'),
      dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
        return this._tiffinProductOffersServiceProxy.getAllTiffinProductOfferTypeForLookupTable(filter, 'name', skipCount, maxResultCount);
      }
    });
  }

  showProductSetLookUpModal(): void {
    this.productSetLookupModal.show(this.tiffinProductOffer.productSets);
  }

  showProductOfferTypeLookUpModal(): void {
    this.productOfferTypeLookupModal.show();
  }

  productSetSelected(items) {
    this.productSets = items;
    this.tiffinProductOffer.productSets = items;
    // this.tiffinProductSetName = item.name;
    let names = [];
    items.map((p) => {
      names.push(p.name);
    });

    this.tiffinProductSetName = names.join(', ');
  }

  productOfferTypeSelected(item) {
    this.tiffinProductOffer.tiffinProductOfferTypeId = parseInt(item.value);
    this.tiffinProductOfferTypeName = item.name;

  }

  uploadInit() {
    this.uploader = new FileUploader({
      url: this.remoteServiceBaseUrl,
      authToken: 'Bearer ' + this._tokenService.getToken(),
    });

    this.uploader.onAfterAddingFile = f => {
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
      this.filePreviewPath = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(f._file)));
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <IAjaxResponse>JSON.parse(response);
      if (ajaxResponse.success) {
        this.notify.success(this.l('UploadImageSuccessfully'));
        this.uploadImageToken = ajaxResponse.result.fileToken;
      } else {
        this.message.error(ajaxResponse.error.message);
      }
    };

  }

  removeFile(item) {
    item.remove();
    this.uploadImageToken = undefined;
  }
}

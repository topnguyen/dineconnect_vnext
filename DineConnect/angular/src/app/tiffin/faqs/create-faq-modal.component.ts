import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

import { AppComponentBase } from '@shared/common/app-component-base';

import { FaqCreateDto, TiffinFaqServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'createFaqModal',
    templateUrl: './create-faq-modal.component.html'
})
export class CreateFaqModalComponent extends AppComponentBase {

    @ViewChild('createModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    numberMask = createNumberMask({
        prefix: '',
        allowDecimal: true
    });
    faq: FaqCreateDto = new FaqCreateDto();

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy,
    ) {
        super(injector);
    }

    show(): void {
        this.faq = new FaqCreateDto();
        this.active = true;
        this.modal.show();
    }

    onShown(): void {
        document.getElementById('Questions').focus();
    }

    save(): void {
        let input = new FaqCreateDto();
        input = this.faq;

        this.saving = true;
        this._tiffinFaqServiceProxy.createFaq(input)
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

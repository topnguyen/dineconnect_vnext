import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

import { AppComponentBase } from '@shared/common/app-component-base';

import { FaqEditDto, TiffinFaqServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'editFaqModal',
    templateUrl: './edit-faq-modal.component.html'
})
export class EditFaqModalComponent extends AppComponentBase {

    @ViewChild('editModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    numberMask = createNumberMask({
        prefix: '',
        allowDecimal: true
    });
    faq: FaqEditDto = new FaqEditDto();

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy,
    ) {
        super(injector);
    }

    show(faqId?: number): void {
        this.faq = new FaqEditDto();
        this._tiffinFaqServiceProxy.getFaqForEdit(faqId).subscribe(faqResult => {
            this.faq = faqResult;
            this.active = true;
            this.modal.show();
        });
    }

    onShown(): void {
        document.getElementById('Questions').focus();
    }

    save(): void {
        let input = new FaqEditDto();
        input = this.faq;

        this.saving = true;
        this._tiffinFaqServiceProxy.updateFaq(input)
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

import { Component, Injector, ViewChild } from '@angular/core';

import { finalize } from 'rxjs/operators';
import { Table, Paginator } from 'primeng';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { CreateFaqModalComponent } from './create-faq-modal.component';
import { EditFaqModalComponent } from './edit-faq-modal.component';

import { FaqListDto, TiffinFaqServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './faqs.component.html',
    styleUrls: ['./faqs.component.scss'],
    animations: [appModuleAnimation()]
})
export class FaqsComponent extends AppComponentBase {

    @ViewChild('createFaqModal', {static: true}) createFaqModal: CreateFaqModalComponent;
    @ViewChild('editFaqModal', {static: true}) editFaqModal: EditFaqModalComponent;
    @ViewChild('dataTable', {static: true}) dataTable: Table;
    @ViewChild('paginator', {static: true}) paginator: Paginator;

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy
    ) {
        super(injector);
    }

    getFaqs(): void {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinFaqServiceProxy.getFaqs()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.items.length;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    createFaq(): void {
        this.createFaqModal.show();
    }

    editFaq(faq: FaqListDto): void {
        this.editFaqModal.show(faq.id);
    }

    deleteFaq(faq: FaqListDto): void {
        this.message.confirm(
            this.l('FaqDeleteWarningMessage', faq.questions),
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._tiffinFaqServiceProxy.deleteFaq(faq.id).subscribe(() => {
                        this.getFaqs();
                        this.notify.success(this.l('SuccessfullyDeleted'));
                    });
                }
            }
        );
    }
}

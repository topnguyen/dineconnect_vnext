import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import moment from 'moment';
import { BsCustomDates } from 'ngx-bootstrap/datepicker/themes/bs/bs-custom-dates-view.component';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import {
    TiffinDeliveryTimeSlotType,
    DeliveryTimeSlotServiceProxy,
    TiffinMealTimesServiceProxy,
    TiffinMealTimeDto,
    LocationDto,
    LocationServiceProxy,
    DeliveryTimeSlotDto,
    DayOfWeek,
    ComboboxItemDto
} from '@shared/service-proxies/service-proxies';
import { toArray } from 'lodash';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './time-slots.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./time-slots.component.scss'],
    animations: [appModuleAnimation()]
})
export class TimeSlotsComponent extends AppComponentBase implements OnInit {
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    selectedDeliveryTypeId: TiffinDeliveryTimeSlotType = undefined;
    selectedMealTime: any;
    selectedLocation: any;
    dayOfWeek: DayOfWeek;
    filterText = '';

    mealTimes: TiffinMealTimeDto[] = [];
    listLocation: LocationDto[] = [];
    deliveryTypes: any[] = [];
    ranges: BsCustomDates[] = [];
    dayOfWeeks: ComboboxItemDto[];
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();


    TiffinDeliveryTimeSlotType = TiffinDeliveryTimeSlotType;

    constructor(
        injector: Injector,
        private _router: Router,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
    ) {
        super(injector);

        this.ranges = [
            {
                value: [moment().toDate(), moment().toDate()],
                label: this.l('Today')
            },
            {
                value: [moment().toDate(), moment().add(1, 'day').endOf('date').toDate()],
                label: this.l('Tomorrow')
            },
            {
                value: [moment().toDate(), moment().add(6, 'day').endOf('date').toDate()],
                label: this.l('Next7Days')
            },
            {
                value: [moment().toDate(), moment().add(29, 'day').endOf('date').toDate()],
                label: this.l('Next30Days')
            },
            {
                value: [moment().startOf('month').toDate(), moment().endOf('month').toDate()],
                label: this.l('ThisMonth')
            },
            {
                value: [moment().add(1, 'month').startOf('month').toDate(), moment().add(1, 'month').endOf('month').toDate()],
                label: this.l('NextMonth')
            }
        ];
        this.deliveryTypes = [
            {
                value: undefined,
                displayText: 'All'
            },
            {
                value: TiffinDeliveryTimeSlotType.Delivery,
                displayText: 'Delivery'
            },
            {
                value: TiffinDeliveryTimeSlotType.SelfPickup,
                displayText: 'Self Pickup'
            }
        ];
    }

    ngOnInit(): void {
        this._getLocationList();
        this._getAllMealTime();
        this._getDays();

        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._deliveryTimeSlotServiceProxy
            .getAll(
                this.dayOfWeek,
                this.selectedDeliveryTypeId,
                this.primengTableHelper.getSorting(this.dataTable) || 'id DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                result.items.forEach((item) => {
                    const location = (this.listLocation || []).find((l) => l.id == item.locationId);
                    const address = location
                        ? `${location.address1}, ${location.address2}, ${location.address3}, ${location.cityName}, ${location.countryName}`
                        : null;

                    item.timeSlots = JSON.parse(item.timeSlots.toLowerCase());
                    item.locationName = item.type ? address : null;
                    item.mealTimeName = this.mealTimes.find((mealTime) => mealTime.id == item.mealTimeId)?.name as any;
                });

                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    onChangeDeliveryType(value: number) {
        this.selectedDeliveryTypeId = value;

        if (this.selectedDeliveryTypeId == TiffinDeliveryTimeSlotType.Delivery) {
            this.selectedLocation = undefined;
        } else {
            this.dayOfWeek = undefined;
        }
    }

    editCalendar(): void {
        this._router.navigate(['/app/tiffin/manage/time-slots/edit']);
    }

    deleteTimeSlot(deliveryTimeSlot: DeliveryTimeSlotDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._deliveryTimeSlotServiceProxy.delete(deliveryTimeSlot.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    editTimeSlots() {
        this._router.navigate(['/app/tiffin/manage/time-slots/edit']);
    }

    getDayOfWeekLabel(value: string) {
        const dayOfWeek = (this.dayOfWeeks || []).find((item) => item.value == value);
        return dayOfWeek ? dayOfWeek.displayText : '';
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.selectedDeliveryTypeId = undefined;
        this.dayOfWeek = undefined;
        this.selectedLocation = undefined;
        this.selectedMealTime = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getAllMealTime() {
        this._tiffinMealTimesServiceProxy.getAll(undefined, undefined, 0, 1000).subscribe((result) => {
            this.mealTimes = result.items;
            this.mealTimes.unshift({ id: undefined, name: 'All' } as TiffinMealTimeDto);
        });
    }

    private _getLocationList() {
        this._locationServiceProxy.getList(undefined, false, 0, undefined, 1000, 0).subscribe((result) => {
            this.listLocation = result.items || [];
        });
    }

    private _getDays() {
        const dayOfWeeks = toArray(DayOfWeek).filter(item => typeof item == 'string');
        this.dayOfWeeks = dayOfWeeks.map((day, index) => {
            return {
                value: index.toString(),
                displayText: day,
            } as ComboboxItemDto;
        })
    }
}

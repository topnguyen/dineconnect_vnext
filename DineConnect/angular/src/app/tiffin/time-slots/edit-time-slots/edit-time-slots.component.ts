import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
    DayOfWeek,
    DeliveryTimeSlotDto,
    DeliveryTimeSlotServiceProxy,
    LocationDto,
    LocationServiceProxy,
    PromotionsServiceProxy,
    TiffinDeliveryTimeSlotType,
    TiffinMealTimeDto,
    TiffinMealTimesServiceProxy
} from '@shared/service-proxies/service-proxies';
import { cloneDeep, isEqual, toArray } from 'lodash';
import moment from 'moment';
import { Paginator, Table } from 'primeng';
import { forkJoin, throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';

@Component({
    selector: 'app-edit-time-slots',
    templateUrl: './edit-time-slots.component.html',
    styleUrls: ['./edit-time-slots.component.scss'],
    animations: [appModuleAnimation()]
})
export class EditTimeSlotsComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    saving = false;
    timeSlot = new DeliveryTimeSlotDto();
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    deliveryTypes: any[];
    mealTimes: TiffinMealTimeDto[];
    listLocation: LocationDto[] = [];
    startTime: Date;
    endTime: Date;

    newTimeSlots: DeliveryTimeSlotDto[] = [];
    dayOfWeeks: ComboboxItemDto[];
    paging: IPaging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };

    TiffinDeliveryTimeSlotType = TiffinDeliveryTimeSlotType;

    constructor(
        injector: Injector,
        private _router: Router,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _promotionsServiceProxy: PromotionsServiceProxy
    ) {
        super(injector);
        this.timeSlot.type = TiffinDeliveryTimeSlotType.Delivery;

        this.deliveryTypes = [
            {
                id: TiffinDeliveryTimeSlotType.Delivery,
                displayText: 'Delivery'
            },
            {
                id: TiffinDeliveryTimeSlotType.SelfPickup,
                displayText: 'Self Pickup'
            }
        ];
    }

    ngOnInit(): void {
        this._getDays();
        forkJoin([this._getAllMealTime(), this._getLocationList()]).subscribe((results) => {
            this.getAll();
        });
    }

    save() {
        const body = cloneDeep(this.newTimeSlots);
        body.forEach((item) => {
            item.timeSlots = JSON.stringify(item.timeSlots);
        });

        this._deliveryTimeSlotServiceProxy.upsertList(body).subscribe((result) => {
            this.message.success(this.l('SavedSuccessfully'));
            this.getAll();
            this.newTimeSlots = [];
        });
    }

    removeTimeSlot(newTimeSlot: DeliveryTimeSlotDto) {
        const index: number = this.newTimeSlots.indexOf(newTimeSlot);
        if (index !== -1) {
            this.newTimeSlots.splice(index, 1);
        }
    }

    close() {
        this._router.navigate(['/app/tiffin/manage/time-slots']);
    }

    onChangeDeliveryType(event: { value: any }) {
        this.timeSlot.type = event.value.id;

        if (this.timeSlot.type == TiffinDeliveryTimeSlotType.Delivery) {
            this.timeSlot.locationId = undefined;
        } else {
            this.timeSlot.dayOfWeek = undefined;
        }
    }

    addTimeSlot() {
        this.timeSlot.timeSlots = {
            from: moment(this.startTime).tz(this.timezone).format('HH: mm'),
            to: moment(this.endTime).tz(this.timezone).format('HH: mm'),
        } as any


        /* validation */
        const isExistedInNew = this.newTimeSlots.some((item) => {
            return (
                item.type == this.timeSlot.type &&
                item.mealTimeId == this.timeSlot.mealTimeId &&
                ((this.timeSlot.type == TiffinDeliveryTimeSlotType.Delivery && item.dayOfWeek == this.timeSlot.dayOfWeek) ||
                    (this.timeSlot.type == TiffinDeliveryTimeSlotType.SelfPickup && item.locationId == this.timeSlot.locationId)) &&
                isEqual(item.timeSlots, this.timeSlot.timeSlots)
            );
        });

        if (isExistedInNew) {
            this.message.info('Time Slot is existed in New Time Slots');
            return;
        }

        const isExisted = this.primengTableHelper.records.some((item) => {
            return (
                item.type == this.timeSlot.type &&
                item.mealTimeId == this.timeSlot.mealTimeId &&
                ((this.timeSlot.type == TiffinDeliveryTimeSlotType.Delivery && item.dayOfWeek == this.timeSlot.dayOfWeek) ||
                    (this.timeSlot.type == TiffinDeliveryTimeSlotType.SelfPickup && item.locationId == this.timeSlot.locationId)) &&
                isEqual(item.timeSlots, this.timeSlot.timeSlots)
            );
        });

        if (isExisted) {
            this.message.info('Time Slot is existed in List Time Slots');
            return;
        }

        this.newTimeSlots.push(DeliveryTimeSlotDto.fromJS(this.timeSlot));

        this.newTimeSlots.forEach((item) => {
            const location = (this.listLocation || []).find((l) => l.id == item.locationId);
            const address = location ? `${location.address1}, ${location.address2}, ${location.address3}, ${location.cityName}, ${location.countryName}` : null;

            item.locationName = item.type ? address : null;
            item.mealTimeName = this.mealTimes.find((mealTime) => mealTime.id == item.mealTimeId)?.name as any;
        });

        // this.timeSlot = new DeliveryTimeSlotDto({ type: TiffinDeliveryTimeSlotType.Delivery } as any);
        this.startTime = undefined;
        this.endTime = undefined;
    }

    deleteTimeSlot(deliveryTimeSlot: DeliveryTimeSlotDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._deliveryTimeSlotServiceProxy.delete(deliveryTimeSlot.id).subscribe(() => {
                    this.getAll();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    onChangePage(paging: IPaging) {
        this.paging = paging;
    }

    getAll() {
        this.primengTableHelper.showLoadingIndicator();

        this._deliveryTimeSlotServiceProxy
            .getAll(undefined, undefined, this.primengTableHelper.getSorting(this.dataTable) || 'id DESC', 0, 1000)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                result.items.forEach((item) => {
                    const location = (this.listLocation || []).find((l) => l.id == item.locationId);
                    const address = location
                        ? `${location.address1}, ${location.address2}, ${location.address3}, ${location.cityName}, ${location.countryName}`
                        : null;

                    item.timeSlots = JSON.parse(item.timeSlots.toLowerCase());
                    item.locationName = item.type ? address : null;
                    item.mealTimeName = this.mealTimes.find((mealTime) => mealTime.id == item.mealTimeId)?.name as any;
                });

                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items || [];
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    getDayOfWeekLabel(value: string) {
        const dayOfWeek = (this.dayOfWeeks || []).find((item) => item.value == value);
        return dayOfWeek ? dayOfWeek.displayText : '';
    }

    private _getAllMealTime() {
        return this._tiffinMealTimesServiceProxy.getAll(undefined, undefined, 0, 1000).pipe(
            catchError((err) => throwError(err)),

            map((result) => {
                this.mealTimes = result.items;
                return result;
            })
        );
    }

    private _getLocationList() {
        return this._locationServiceProxy.getList(undefined, false, 0, undefined, 1000, 0).pipe(
            catchError((err) => throwError(err)),

            map((result) => {
                this.listLocation = result.items || [];
                return result;
            })
        );
    }

    private _getDays() {
        const dayOfWeeks = toArray(DayOfWeek).filter(item => typeof item == 'string');
        this.dayOfWeeks = dayOfWeeks.map((day, index) => {
            return {
                value: index.toString(),
                displayText: day,
            } as ComboboxItemDto;
        })
    }
}

interface IPaging {
    first: number;
    page: number;
    pageCount: number;
    rows: number;
}

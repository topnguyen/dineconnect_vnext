import { Component, Injector, OnInit } from '@angular/core';

import { finalize } from 'rxjs/operators';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader } from 'ng2-file-upload';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';

import { HomeBannerEditDto, TiffinHomeBannerServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './homepage-banner.component.html',
    styleUrls: ['./homepage-banner.component.scss'],
    animations: [appModuleAnimation()]
})
export class HomepageBannerComponent extends AppComponentBase implements OnInit {
    saving = false;
    homeBanner = new HomeBannerEditDto();
    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
    uploader: FileUploader;
    bannerList: string[] = [];

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _tiffinHomeBannerServiceProxy: TiffinHomeBannerServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.loadHomeBanner();
        this.uploadInit();
    }

    loadHomeBanner() {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinHomeBannerServiceProxy.getHomeBannerForEdit()
            .subscribe(result => {
                this.homeBanner = result;
                if (this.homeBanner.images) {
                    this.bannerList = this.homeBanner.images.split(',');
                }
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    save(): void {
        this.saving = true;
        if (this.bannerList.length === 0) {
            this.notify.error(this.l('HomeBanner_RequiredLength_Hint'));
            return;
        }
        this.homeBanner.images = this.bannerList.join(',');

        this.primengTableHelper.showLoadingIndicator();
        this._tiffinHomeBannerServiceProxy.updateHomeBanner(this.homeBanner)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    uploadInit() {
        this.uploader = new FileUploader({
            url: this.remoteServiceBaseUrl,
            authToken: 'Bearer ' + this._tokenService.getToken(),
        });

        this.uploader.onAfterAddingFile = fileItem => {
            this.spinnerService.show();

            fileItem.upload();
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            this.spinnerService.hide();
            if (ajaxResponse.success) {
                this.notify.success(this.l('UploadImageSuccessfully'));
                if (ajaxResponse.result.url) {
                    this.bannerList.push(ajaxResponse.result.url);
                } else {
                    this.message.error(ajaxResponse.result.message);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onErrorItem = () => {
            this.message.error(this.l('UploadImageFailed'));
            this.primengTableHelper.hideLoadingIndicator();
        };
    }

    removeFile(index) {
        this.bannerList.splice(index, 1);
    }
}

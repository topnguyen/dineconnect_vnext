import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinCustomerProductOfferDto, TiffinCustomerTransactionsServiceProxy } from '@shared/service-proxies/service-proxies';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-edit-balance-detail-modal',
    templateUrl: './edit-balance-detail-modal.component.html',
    styleUrls: ['./edit-balance-detail-modal.component.scss']
})
export class EditBalanceDetailModalComponent extends AppComponentBase implements OnInit {
    active = false;
    saving = false;

    customerOffer: TiffinCustomerProductOfferDto = new TiffinCustomerProductOfferDto();
    customerOfferId: number;
    /* callback func */
    refresh: Function;

    constructor(injector: Injector,private _bsModalRef: BsModalRef, private _customerOffersServiceProxy: TiffinCustomerTransactionsServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getCustomerProductOfferForEdit();
    }

    getCustomerProductOfferForEdit(): void {
        this._customerOffersServiceProxy.getCustomerProductOfferForEdit(this.customerOfferId).subscribe((result) => {
            this.customerOffer = result;
            this.active = true;
        });
    }

    save(): void {
        this.saving = true;
        this._customerOffersServiceProxy
            .updateCustomerOffer(this.customerOffer)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.refresh(true);
                this.close();
            });
    }

    close(): void {
        this.active = false;
        this._bsModalRef.hide();
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BalanceDetailDto, CustomerReportViewDto, TiffinMemberPortalServiceProxy } from '@shared/service-proxies/service-proxies';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import { EditBalanceDetailModalComponent } from './edit-balance-modal/edit-balance-detail-modal.component';

@Component({
    templateUrl: './balances-detail-modal.component.html',
    styleUrls: ['./balances-detail-modal.component.scss']
})
export class BalancesDetailModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('balanceTable', { static: true }) balanceTable: Table;
    @ViewChild('balancePaginator', { static: true }) balancePaginator: Paginator;

    customerId: number;
    data: CustomerReportViewDto;
    filterText = '';
    isExpiriedOfferFilter = false;

    constructor(
        injector: Injector,
        private _modalService: BsModalService,
        private _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _bsModalRef: BsModalRef
    ) {
        super(injector);
    }

    ngOnInit() {}

    getBalanceList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.balancePaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._portalServiceProxy
            .getBalancesForCustomer(
                this.customerId,
                this.isExpiriedOfferFilter,
                this.filterText,
                this.primengTableHelper.getSorting(this.balanceTable) || undefined,
                this.primengTableHelper.getSkipCount(this.balancePaginator, event),
                this.primengTableHelper.getMaxResultCount(this.balancePaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });

        this.primengTableHelper.hideLoadingIndicator();
    }

    editCustomerOffer(item: BalanceDetailDto) {
        const initialState = {
            customerOfferId: item.customerOfferOrderId
        };
        const bsModalRef = this._modalService.show(EditBalanceDetailModalComponent, { initialState, class: 'modal-lg' });
        bsModalRef.content.refresh = (result) => {
            if (result) {
                this.reloadPage();
            }
        };
    }

    getActiveBalanceList(number: number) {
        switch (number) {
            case 1:
                this.isExpiriedOfferFilter = false;
                break;
            case 2:
                this.isExpiriedOfferFilter = true;
                break;
            case 3:
                this.isExpiriedOfferFilter = undefined;
                break;

            default:
                break;
        }
        this.getBalanceList();
    }

    reloadPage(): void {
        this.balancePaginator.changePage(this.balancePaginator.getPage());
    }

    close() {
        this._bsModalRef.hide();
    }
}

import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LazyLoadEvent } from 'primeng/api';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CustomerReportServiceProxy, CustomerReportInput, CustomerReportViewDto } from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { Table, Paginator } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BalancesDetailModalComponent } from './balances-detail-modal/balances-detail-modal.component';
import { AttachedPromotionModalComponent } from './attached-promotion-modal/attached-promotion-modal.component';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './customer-report.component.html',
    styleUrls: ['./customer-report.component.scss'],
    animations: [appModuleAnimation()]
})
export class CustomerReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('customerTable', { static: true }) customerTable: Table;
    @ViewChild('customerPaginator', { static: true }) customerPaginator: Paginator;

    isActive: boolean;
    isNew: boolean;
    lazyLoadEvent: LazyLoadEvent;
    filterText: string;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _modalService: BsModalService,
        private _customerReportServiceProxy: CustomerReportServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.customerPaginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;

        this.primengTableHelper.showLoadingIndicator();

        this._customerReportServiceProxy
            .getAll(
                this.filterText,
                this.isActive,
                this.isNew,
                this.primengTableHelper.getSorting(this.customerTable),
                this.primengTableHelper.getSkipCount(this.customerPaginator, event),
                this.primengTableHelper.getMaxResultCount(this.customerPaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.customerPaginator.changePage(this.customerPaginator.getPage());
    }

    openBalancesDetail(item: CustomerReportViewDto) {
        const initialState = {
            customerId: item.customerId,
            data: item
        };
        const bsModalRef = this._modalService.show(BalancesDetailModalComponent, { initialState, class: 'modal-xl' });
    }

    exportExcel() {
        const input = new CustomerReportInput();
        input.activeCustomer = this.isActive,
            input.newCustomer = this.isNew,
            input.sorting = this.primengTableHelper.getSorting(this.customerTable) || 'registrationDate';
        input.skipCount = this.primengTableHelper.getSkipCount(this.customerPaginator, this.lazyLoadEvent);
        input.maxResultCount = this.primengTableHelper.getMaxResultCount(this.customerPaginator, this.lazyLoadEvent);

        this.primengTableHelper.showLoadingIndicator();
        this._customerReportServiceProxy
            .exportExcel(input)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    sendEmail(item: CustomerReportViewDto) {
        this._customerReportServiceProxy.sendEmailRemindBuyMealPlan(item.customerId).subscribe(result => {
            this.notify.success('SentEmailSuccessfully');
        })
    }

    sendToNewCustomers() {
        this._customerReportServiceProxy.sendEmailRemindBuyMealPlanToNewCustomers().subscribe(result => {
            this.notify.success('SentEmailToNewCustomersSuccessfully');
        })
    }

    sendPromotionCode(item: CustomerReportViewDto) {
        const initialState = {
            customerReport: item,
        };
        this._modalService.show(AttachedPromotionModalComponent, { initialState, class: 'modal-md' });
    }

    sendPromotionCodeToAllCustomer() {
        const initialState = {
            isSendAll: true
        };
        this._modalService.show(AttachedPromotionModalComponent, { initialState, class: 'modal-md' });
    }

    refresh() {
        this.clear()
        this.customerPaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    apply() {
        this.customerPaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.isActive = undefined;
        this.isNew = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

}

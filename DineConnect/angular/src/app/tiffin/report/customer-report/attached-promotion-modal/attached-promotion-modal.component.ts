import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CustomerReportInput,
    CustomerReportViewDto,
    PromotionListDto,
    TiffinPromotionServiceProxy
} from '@shared/service-proxies/service-proxies';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-attached-promotion-modal',
    templateUrl: './attached-promotion-modal.component.html',
    styleUrls: ['./attached-promotion-modal.component.scss']
})
export class AttachedPromotionModalComponent extends AppComponentBase implements OnInit {
    customerReport: CustomerReportViewDto;
    saving = false;
    promotions: PromotionListDto[] = [];
    selectedPromotion: PromotionListDto;
    isSendAll = false;
    isActiveCustomer = true;
    isNewCustomer = false;

    constructor(injector: Injector, private _bsModalRef: BsModalRef, private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy) {
        super(injector);
    }

    ngOnInit() {}

    close() {
        this._bsModalRef.hide();
    }

    getPromotions$(event?: { query: string }): Observable<any> {
        const isAutoAttach = true;
        return this._tiffinPromotionServiceProxy
            .getPromotions(event ? event.query : undefined, undefined, undefined, undefined, isAutoAttach, 'name', 0, 1000)
            .pipe(
                map((result) => {
                    this.promotions = result.items;
                    return result;
                })
            );
    }

    getPromotions(event?: { query: string }) {
        this.getPromotions$(event).subscribe((result) => {
            this.promotions = result.items;
        });
    }

    send() {
        if (!this.selectedPromotion) {
            this.message.error(this.l('PleaseSelectPromotion'));
        }

        if (this.isSendAll) {
            this.generatePromotionCodeAndSendToAllCustomers()
        } else {
            this.generatePromotionCodeAndSendToCustomers();
        }
    }


    generatePromotionCodeAndSendToCustomers() {
        this._tiffinPromotionServiceProxy.generatePromotionCodeAndSendToCustomers(this.selectedPromotion.id, [this.customerReport.customerId]).subscribe(() => {
            this.notify.success('SentPromotionCodeToCustomerSuccessfully');
        });
    }

    generatePromotionCodeAndSendToAllCustomers() {
        const input = new CustomerReportInput();
        input.newCustomer  = this.isNewCustomer,
        input.activeCustomer  = this.isActiveCustomer

        this._tiffinPromotionServiceProxy.generatePromotionCodeAndSendToAllCustomer(this.selectedPromotion.id, input).subscribe(() => {
            this.notify.success('SentPromotionCodeToCustomerSuccessfully');
        });
    }
}

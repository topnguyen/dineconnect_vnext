import { Component, Injector, OnInit, Renderer2 } from '@angular/core';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import moment from 'moment';
import { every, orderBy } from 'lodash';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { IMealPLanCustomerAcquisitionViewDto, MealPlanReportServiceProxy, TiffinOrderServiceProxy, TimeRangeMode } from '@shared/service-proxies/service-proxies';
import { Subject } from 'rxjs';


@Component({
  templateUrl: './collect-report.component.html',
  styleUrls: ['./collect-report.component.scss'],
  animations: [appModuleAnimation()]
})
export class CollectReportComponent extends AppComponentBase implements OnInit {
    stackedBarchart = {
        data: [],
        view: undefined,
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: true,
        showXAxisLabel: true,
        xAxisLabel: this.l('Dates'),
        showYAxisLabel: true,
        yAxisLabel: this.l('Order Number'),
        animations: true,
        colorScheme: {
            domain: ['#1bc5bd', '#f1c857']
        }
    };
    ranges = this.createDateRangePickerOptions();
    dateRange: Date[] = [moment().startOf('month').add(moment().utcOffset(), 'minute').toDate(), new Date()];
    filterText: string;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private render: Renderer2,
        private _tiffinOrderServiceProxy: TiffinOrderServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getAll();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll() {
        const from = this.dateRange[0] ? moment(this.dateRange[0]) : undefined;
        const to = this.dateRange[1] ? moment(this.dateRange[1]) : undefined;

        this._tiffinOrderServiceProxy
            .getPickUpOrderReport(from, to)
            .subscribe((result) => {
                this.stackedBarchart.data = orderBy(result.items, 'date', 'asc').map((item) => {
                    return {
                        name: item.date.format('DD MMM YYYY'),
                        series: [
                            {
                                name: this.l('Picked up'),
                                value: item.totalPickedUpOrder,
                            },
                            {
                                name: this.l('Not Picked up'),
                                value: item.totalNotPickedUpOrder,
                            }
                        ]
                    } as IDataChartModel;
                });
           
            });
    }

    onSelect(event) {
        console.log(event);
    }

    refresh() {
        this.clear()
        this.getAll()
        this.isShowFilterOptions = false;
    }

    apply() {
        this.getAll()
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [moment().startOf('month').add(moment().utcOffset(), 'minute').toDate(), new Date()];
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

interface IDataChartModel {
    name: string;
    series: ISeries[];
}

interface ISeries {
    name: string;
    value: number;
    extra: {
        code: string;
    };
}

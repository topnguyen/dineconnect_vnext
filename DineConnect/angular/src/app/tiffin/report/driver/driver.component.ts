import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import {
    DriverReportServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
    TiffinOrderServiceProxy,
    CustomersServiceProxy,
    PagedResultDtoOfCustomerListDto,
    ComboboxItemDto,
    DeliveryTimeSlotServiceProxy
} from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { DriverExportModalComponent } from './driver-export-modal/driver-export-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as moment from 'moment';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { unionBy } from 'lodash';

@Component({
    selector: 'app-driver',
    templateUrl: './driver.component.html',
    styleUrls: ['./driver.component.scss'],
    animations: [appModuleAnimation()]
})
export class DriverComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('driverExportModal', { static: true }) driverExportModal: DriverExportModalComponent;

    filterText = '';
    customerName = '';
    dateRange: any = [];
    customers: ERPComboboxItem[] = [];
    selectedCustomer: any;
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    products: ERPComboboxItem[] = [];
    selectedProductSet: any;
    selectedMeal: any;
    mealOptions = [];
    zone: number;
    timeSlots: ComboboxItemDto[];
    timeSlot: string;

    zoneOptions = [
        { text: this.l('North'), value: 1 },
        { text: this.l('NorthWest'), value: 2 },
        { text: this.l('NorthEast'), value: 3 },
        { text: this.l('Central'), value: 4 },
        { text: this.l('West'), value: 5 },
        { text: this.l('East'), value: 6 },
        { text: this.l('South/Cbd'), value: 7 }
    ];

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _driverServiceProxy: DriverReportServiceProxy,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonServiceProxy: CommonServiceProxy,
        private _customerService: CustomersServiceProxy,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    
    ngOnInit(): void {
        this.init();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getList();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    init() {
        this._commonServiceProxy.getAllProductSets().subscribe((result) => {
            this.products = result;
        });
        this._tiffinOrderProxy.getScheduleTimes().subscribe((result) => {
            this.mealOptions = result.items;
        });
        this._getTimeSlots();
    }

    getList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._driverServiceProxy
            .getAll(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.zone,
                undefined,
                this.timeSlot,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map(item => {
                    return {
                        ...item,
                        timeSlot: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null
                    }
                });
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    openKitChenExportModal() {
        this.driverExportModal.show();
    }

    exportToExcel() {
        this._driverServiceProxy
            .getListCustomerForDriverExcel(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.zone,
                undefined,
                this.timeSlot
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    changeCustomer(data) {
        this.customerName = data.name;
        this.selectedCustomer = data.id;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear()
        this.paginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    apply() {
        this.paginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [];
        this.selectedCustomer = undefined;
        this.customerName = '';
        this.selectedProductSet = undefined;
        this.selectedMeal = undefined;
        this.zone = undefined;
        this.timeSlot = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getTimeSlots() {
        this._deliveryTimeSlotServiceProxy
            .getAll(
                undefined,
                undefined,
                'id DESC',
                0,
                1000
            )
            .subscribe((result) => {
                this.timeSlots = [];
                result.items.forEach((item) => {
                    const timeSlots = JSON.parse(item.timeSlots.toLowerCase());

                    this.timeSlots.push(ComboboxItemDto.fromJS({
                        value: item.timeSlots,
                        displayText: `${timeSlots['from']}-${timeSlots['to']}`
                    }))
                });

                this.timeSlots = unionBy(this.timeSlots, item => item.displayText)
            });
    }
}

import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DriverReportServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';

@Component({
  selector: 'driver-export-modal',
  templateUrl: './driver-export-modal.component.html',
  styleUrls: ['./driver-export-modal.component.scss']
})
export class DriverExportModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('driverExportModal', { static: true }) modal: ModalDirective;
  active = false;
  saving = false;
  dateOrder: Date;

  constructor(injector: Injector,
    private _driverServiceProxy: DriverReportServiceProxy) {
    super(injector);
  }

  ngOnInit() {
  }

  show() {
    this.active = true;
    this.dateOrder = undefined;
    this.modal.show();
  }

  close() {
    this.saving = false;
    this.active = false;
    this.modal.hide();
  }

  export() {
    // this._driverServiceProxy.getListCustomerForDriverExcel(moment(this.dateOrder))
    //   .subscribe(() => {
    //   });


    this.saving = false;
    this.active = false;
    this.modal.hide();
  }
}

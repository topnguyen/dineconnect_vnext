import { Component, Injector, OnInit, Renderer2 } from '@angular/core';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import moment from 'moment';
import { every } from 'lodash';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { IMealPLanCustomerAcquisitionViewDto, MealPlanReportServiceProxy, TimeRangeMode } from '@shared/service-proxies/service-proxies';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './meal-plan-report.component.html',
    styleUrls: ['./meal-plan-report.component.scss'],
    animations: [appModuleAnimation()]
})
export class MealPlanReportComponent extends AppComponentBase implements OnInit {
    stackedBarchart = {
        data: [],
        view: undefined,
        showXAxis: true,
        showYAxis: true,
        gradient: false,
        showLegend: true,
        showXAxisLabel: true,
        xAxisLabel: this.l('Dates'),
        showYAxisLabel: true,
        yAxisLabel: this.l('MealPlansBought'),
        animations: true,
        colorScheme: {
            domain: ['#f15757', '#f1c857', '#709231', '#563192', '#317b92', '#666b70', '#7e963f', '#88347d']
        }
    };
    ranges = this.createDateRangePickerOptions();
    dateRange: Date[] = [moment().startOf('month').add(moment().utcOffset(), 'minute').toDate(), new Date()];
    timeRangeMode: any = TimeRangeMode.Day.toString();
    rangerModes: any[] = [
        {
            value: TimeRangeMode.Day.toString(),
            displayText: 'Day'
        },
        {
            value: TimeRangeMode.Month.toString(),
            displayText: 'Month'
        },
        {
            value: TimeRangeMode.Year.toString(),
            displayText: 'Year'
        }
    ];

    pieChart = {
        data: [],
        view: [375, 250],
        gradient: false,
        showLegend: true,
        legendPosition: 'below',
        showLabels: false,
        isDoughnut: false,

        colorScheme: {
            domain: ['#f15757', '#f1c857']
        }
    };
    isNoData = false;
    isShowFilterOptions = false;
    filterText = '';

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(injector: Injector, private render: Renderer2, private _mealPlanReportServiceProxy: MealPlanReportServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });

        this.getAll();
        this.getCustomerAcquisitionReport();
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    reloadPage() {
        this.getAll();
        this.getCustomerAcquisitionReport();
    }

    getAll() {
        this._mealPlanReportServiceProxy
            .getReport(
                this.dateRange[0] ? moment(this.dateRange[0]) : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]) : undefined,
                this.timeRangeMode
            )
            .pipe(finalize(() => {}))
            .subscribe((result) => {
                this.stackedBarchart.data = result.items.map((item) => {
                    return {
                        name: item.date.format(this._formatDate()),
                        series: (item.details || []).map((detail) => {
                            return {
                                name: detail.mealPlanName,
                                value: detail.mealPlanTotalBought,
                                extra: {
                                    code: detail.mealPlanId.toString()
                                }
                            };
                        })
                    } as IDataChartModel;
                });
                this.isNoData = every(this.stackedBarchart.data, (item) => {
                    return item.series.length == 0;
                });
            });
    }

    getCustomerAcquisitionReport() {
        this._mealPlanReportServiceProxy
            .getCustomerAcquisitionReport(
                this.dateRange[0] ? moment(this.dateRange[0]) : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]) : undefined
            )
            .subscribe((result) => {
                this.pieChart.data = result.items.map((item) => {
                    return {
                        ...item,
                        data: this.getDataPieChart(item)
                    };
                });
            });
    }

    exportExcel() {}

    onSelect(event) {
        console.log(event);
    }

    getDataPieChart(item: IMealPLanCustomerAcquisitionViewDto) {
        return [
            {
                name: `New Customer (${((item.mealPlanTotalCountBoughtByNewCustomer * 100) / item.mealPlanTotalBought).toFixed(2)}%)`,
                value: item.mealPlanTotalCountBoughtByNewCustomer
            },
            {
                name: `Existing Customer (${((item.mealPlanTotalCountBoughtByExistingCustomer * 100) / item.mealPlanTotalBought).toFixed(
                    2
                )}%) `,
                value: item.mealPlanTotalCountBoughtByExistingCustomer
            }
        ];
    }

    tooltipText(event) {
        console.log(event);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [moment().startOf('month').add(moment().utcOffset(), 'minute').toDate(), new Date()];
        this.timeRangeMode = TimeRangeMode.Day.toString();
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _formatDate(): string {
        switch (+this.timeRangeMode) {
            case TimeRangeMode.Day:
                return 'DD MMM YYYY';
            case TimeRangeMode.Month:
                return 'MMM YYYY';
            case TimeRangeMode.Year:
                return 'YYYY';

            default:
                return 'DD MMM YYYY';
        }
    }
}

interface IDataChartModel {
    name: string;
    series: ISeries[];
}

interface ISeries {
    name: string;
    value: number;
    extra: {
        code: string;
    };
}

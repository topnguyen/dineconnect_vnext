import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BalanceSummaryServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';

import * as moment from 'moment';

@Component({
  selector: 'customer-export-modal',
  templateUrl: './customer-export-modal.component.html',
  styleUrls: ['./customer-export-modal.component.scss']
})
export class CustomerExportModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('balanceExportModal', { static: true }) modal: ModalDirective;
  active = false;
  saving = false;
  dateOrder: Date[];

  constructor(injector: Injector,
    private _balanceSummaryProxy: BalanceSummaryServiceProxy,
    private _fileDownloadService: FileDownloadService) {
    super(injector);
  }

  ngOnInit() {
  }

  show() {
    this.active = true;
    this.modal.show();
  }

  close() {
    this.saving = false;
    this.active = false;
    this.modal.hide();
  }

  export() {
    this._balanceSummaryProxy.getBalanceSummaryExcel(moment(this.dateOrder[0]), 
    moment(this.dateOrder[1])).subscribe(result => {
      this._fileDownloadService.downloadTempFile(result);
    });

    this.saving = false;
    this.active = false;
    this.modal.hide();
  }
}

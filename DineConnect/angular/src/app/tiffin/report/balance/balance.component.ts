import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LazyLoadEvent } from 'primeng/api';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    BalanceSummaryServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
    PagedResultDtoOfCustomerListDto,
    CustomersServiceProxy
} from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { Table, Paginator } from 'primeng';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './balance.component.html',
    styleUrls: ['./balance.component.scss'],
    animations: [appModuleAnimation()]
})
export class BalanceComponent extends AppComponentBase implements OnInit {
    @ViewChild('balanceTable', { static: true }) balanceTable: Table;
    @ViewChild('balancePaginator', { static: true }) balancePaginator: Paginator;

    filterText = '';
    selectedCustomer: any;
    customerName = '';
    products: ERPComboboxItem[] = [];
    selectedProductSet: any;
    dateRange: any = [];
    zone: number;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    zoneOptions = [
        { text: this.l('North'), value: 1 },
        { text: this.l('NorthWest'), value: 2 },
        { text: this.l('NorthEast'), value: 3 },
        { text: this.l('Central'), value: 4 },
        { text: this.l('West'), value: 5 },
        { text: this.l('East'), value: 6 },
        { text: this.l('South/Cbd'), value: 7 }
    ];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _balanceSummaryProxy: BalanceSummaryServiceProxy,
        private _commonServiceProxy: CommonServiceProxy,
        private _customerService: CustomersServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    ngOnInit(): void {
        this.init();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getBalanceList();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    init() {
        this._commonServiceProxy.getAllProductSets().subscribe((result) => {
            this.products = result;
        });
    }

    getBalanceList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.balancePaginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._balanceSummaryProxy
            .getAll(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.zone,
                this.primengTableHelper.getSorting(this.balanceTable),
                this.primengTableHelper.getSkipCount(this.balancePaginator, event),
                this.primengTableHelper.getMaxResultCount(this.balancePaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.balancePaginator.changePage(this.balancePaginator.getPage());
    }

    openBalanceExportModal() {
        this.primengTableHelper.showLoadingIndicator();
        this._balanceSummaryProxy
            .getBalanceSummaryExcel(this.dateRange[0] ? moment(this.dateRange[0]) : undefined, this.dateRange[1] ? moment(this.dateRange[1]) : undefined)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    changeCustomer(data) {
        this.customerName = data.name;
        this.selectedCustomer = data.id;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear();
        this.balancePaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    apply() {
        this.balancePaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    clear() {
        this.dateRange = [];
        this.filterText = '';
        this.selectedCustomer = undefined;
        this.selectedProductSet = undefined;
        this.zone = undefined;
        this.customerName = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

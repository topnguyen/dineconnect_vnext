import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { KitchenReportServiceProxy, CommonServiceProxy, ERPComboboxItem, PagedResultDtoOfCustomerListDto, CustomersServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Component({
  selector: 'kitchen-export-modal',
  templateUrl: './kitchen-export-modal.component.html',
  styleUrls: ['./kitchen-export-modal.component.scss']
})
export class KitchenExportModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('kitchenExportModal', { static: true }) modal: ModalDirective;
  active = false;
  saving = false;
  customerName = '';
  zone: number;
  meal: number;

  dateRange: any;
  selectedCustomer: any;

  products: ERPComboboxItem[] = [];
  selectedProductSet: any;

  zoneOptions = [
    { text: this.l('North'), value: 1 },
    { text: this.l('NorthWest'), value: 2 },
    { text: this.l('NorthEast'), value: 3 },
    { text: this.l('Central'), value: 4 },
    { text: this.l('West'), value: 5 },
    { text: this.l('East'), value: 6 },
    { text: this.l('South/Cbd'), value: 7 }
  ];

  mealOptions = [
    { text: this.l('BreakFast'), value: 0 },
    { text: this.l('Lunch'), value: 1 },
    { text: this.l('Dinner'), value: 2 }
  ];

  constructor(injector: Injector,
    private _kitchenServiceProxy: KitchenReportServiceProxy,
    private _commonServiceProxy: CommonServiceProxy,
    private _customerService: CustomersServiceProxy
    ) {
    super(injector);
  }

  ngOnInit() {
    this.init();
  }

  init() {
   
    this._commonServiceProxy.getAllProductSets().subscribe(result => {
      this.products = result;
    });
  }

  show() {
    this.active = true;
    this.zone = undefined;
    this.meal = undefined;
    this.dateRange = undefined;
    this.selectedCustomer = undefined;
    this.selectedProductSet = undefined;
    this.modal.show();
  }

  close() {
    this.saving = false;
    this.active = false;
    this.modal.hide();
  }

  export() {
    
    this.saving = false;
    this.active = false;
    this.modal.hide();
  }

  changeCustomer(data) {
    this.customerName = data.name;
    this.selectedCustomer = data.id;
}

getCustomerForComboBox() {
    return (
        filterString,
        sorting,
        maxResultCount,
        skipCount
    ): Observable<PagedResultDtoOfCustomerListDto> => {
        return this._customerService.getAll(
            filterString,null, null,sorting,skipCount,maxResultCount);
    };
}
}

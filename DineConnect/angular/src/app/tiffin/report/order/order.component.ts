import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import {
    OrderSummaryServiceProxy,
    ERPComboboxItem,
    TiffinOrderServiceProxy,
    CommonServiceProxy,
    CustomersServiceProxy,
    PagedResultDtoOfCustomerListDto,
    DeliveryTimeSlotServiceProxy,
    ComboboxItemDto
} from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { OrderExportModalComponent } from './order-export-modal/order-export-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as moment from 'moment';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { unionBy } from 'lodash';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    animations: [appModuleAnimation()]
})
export class OrderComponent extends AppComponentBase implements OnInit {
    @ViewChild('orderTable', { static: true }) orderTable: Table;
    @ViewChild('orderPaginator', { static: true }) orderPaginator: Paginator;
    @ViewChild('orderExportModal', { static: true })
    orderExportModal: OrderExportModalComponent;

    filterText = '';
    customerName = '';
    dateRange: moment.Moment[] = [moment().startOf('day'), moment().endOf('day')];
    customers: ERPComboboxItem[] = [];
    selectedCustomer: any;

    products: ERPComboboxItem[] = [];
    selectedProductSet: any;
    selectedMeal: any;
    mealOptions = [];
    zone: number;
    timeSlots: ComboboxItemDto[];
    timeSlot: string;

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    zoneOptions = [
        { text: this.l('North'), value: 1 },
        { text: this.l('NorthWest'), value: 2 },
        { text: this.l('NorthEast'), value: 3 },
        { text: this.l('Central'), value: 4 },
        { text: this.l('West'), value: 5 },
        { text: this.l('East'), value: 6 },
        { text: this.l('South/Cbd'), value: 7 }
    ];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _orderSummaryProxy: OrderSummaryServiceProxy,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonServiceProxy: CommonServiceProxy,
        private _customerService: CustomersServiceProxy,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    ngOnInit(): void {
        this.init();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getOrderList();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    init() {
        this._commonServiceProxy.getAllProductSets().subscribe((result) => {
            this.products = result;
        });
        this._tiffinOrderProxy.getScheduleTimes().subscribe((result) => {
            this.mealOptions = result.items;
        });
        this._getTimeSlots();
    }

    getOrderList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.orderPaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._orderSummaryProxy
            .getAll(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]) : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]) : undefined,
                this.zone,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                undefined,
                this.timeSlot,
                this.primengTableHelper.getSorting(this.orderTable),
                this.primengTableHelper.getSkipCount(this.orderPaginator, event),
                this.primengTableHelper.getMaxResultCount(this.orderPaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map((item) => {
                    return {
                        ...item,
                        timeSlot: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null
                    };
                });
            });
    }

    reloadPage(): void {
        this.orderPaginator.changePage(this.orderPaginator.getPage());
    }

    exportToExcel() {
        this.primengTableHelper.showLoadingIndicator();
        this._orderSummaryProxy
            .getOrderSummaryExcel(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]) : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]) : undefined,
                this.zone,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                undefined,
                this.timeSlot
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    changeCustomer(data) {
        this.customerName = data.name;
        this.selectedCustomer = data.id;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [];
        this.selectedCustomer = undefined;
        this.selectedProductSet = undefined;
        this.selectedMeal = undefined;
        this.zone = undefined;
        this.customerName = '';
        this.timeSlot = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getTimeSlots() {
        this._deliveryTimeSlotServiceProxy.getAll(undefined, undefined, 'id DESC', 0, 1000).subscribe((result) => {
            this.timeSlots = [];
            result.items.forEach((item) => {
                const timeSlots = JSON.parse(item.timeSlots.toLowerCase());

                this.timeSlots.push(
                    ComboboxItemDto.fromJS({
                        value: item.timeSlots,
                        displayText: `${timeSlots['from']}-${timeSlots['to']}`
                    })
                );
            });

            this.timeSlots = unionBy(this.timeSlots, (item) => item.displayText);
        });
    }
}

import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    PaymentSummaryServiceProxy,
    ERPComboboxItem,
    TiffinOrderServiceProxy,
    CommonServiceProxy,
    PagedResultDtoOfCustomerListDto,
    CustomersServiceProxy
} from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as moment from 'moment';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss'],
    animations: [appModuleAnimation()]
})
export class PaymentComponent extends AppComponentBase implements OnInit {
    @ViewChild('paymentTable', { static: true }) paymentTable: Table;
    @ViewChild('paymentPaginator', { static: true }) paymentPaginator: Paginator;

    filterText = '';
    dateRange: any = [];
    selectedCustomer: any;

    products: ERPComboboxItem[] = [];
    selectedProductSet: any;
    selectedMeal: any;
    mealOptions = [];

    files = [];
    zone: number;

    zoneOptions = [
        { text: this.l('North'), value: 1 },
        { text: this.l('NorthWest'), value: 2 },
        { text: this.l('NorthEast'), value: 3 },
        { text: this.l('Central'), value: 4 },
        { text: this.l('West'), value: 5 },
        { text: this.l('East'), value: 6 },
        { text: this.l('South/Cbd'), value: 7 }
    ];

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _paymentSummaryProxy: PaymentSummaryServiceProxy,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonServiceProxy: CommonServiceProxy,
        private _customerService: CustomersServiceProxy
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    ngOnInit(): void {
        this.init();
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getPaymentList();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    init() {
        this._commonServiceProxy.getAllProductSets().subscribe((result) => {
            this.products = result;
        });
        this._tiffinOrderProxy.getScheduleTimes().subscribe((result) => {
            this.mealOptions = result.items;
        });
    }

    getPaymentList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paymentPaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._paymentSummaryProxy
            .getAll(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.zone,
                this.selectedCustomer ? this.selectedCustomer.id : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.primengTableHelper.getSorting(this.paymentTable),
                this.primengTableHelper.getSkipCount(this.paymentPaginator, event),
                this.primengTableHelper.getMaxResultCount(this.paymentPaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                console.log(result);
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paymentPaginator.changePage(this.paymentPaginator.getPage());
    }

    exportToExcel() {
        this._paymentSummaryProxy
            .getPaymentSummaryExcel(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]) : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]) : undefined,
                this.zone,
                this.selectedCustomer ? this.selectedCustomer.value : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
        // this.notify.warn(this.l('ReportIsExportingInBackground'));
    }

    changeCustomer(data) {
        this.selectedCustomer = data;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear();
        this.paymentPaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    apply() {
        this.paymentPaginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [];
        this.selectedCustomer = undefined;
        this.selectedProductSet = undefined;
        this.selectedMeal = undefined;
        this.zone = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

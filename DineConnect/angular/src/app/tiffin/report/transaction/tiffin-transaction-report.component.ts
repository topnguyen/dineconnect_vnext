import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { ActivatedRoute } from '@angular/router';
import { LazyLoadEvent } from 'primeng/public_api';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    TiffinCustomerTransactionsServiceProxy,
    PagedResultDtoOfCustomerListDto,
    CustomersServiceProxy
} from '@shared/service-proxies/service-proxies';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { FileDownloadService } from '@shared/utils/file-download.service';

import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'app-tiffin-transactions',
    templateUrl: './tiffin-transaction-report.component.html',
    styleUrls: ['./tiffin-transaction-report.component.scss'],
    animations: [appModuleAnimation()]
})
export class TiffinTransactionReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    customerFilter: any;
    paymentFilter = '';
    customerName = '';
    daysExpiryFilter: number;
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    dateRange = [];
    startDate: moment.Moment;
    endDate = undefined;
    selectedCustomer: any;
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _activatedRoute: ActivatedRoute,
        private _tiffinCustomerTransactionsServiceProxy: TiffinCustomerTransactionsServiceProxy,
        private _customerService: CustomersServiceProxy
    ) {
        super(injector);
        this.customerFilter = this._activatedRoute.snapshot.queryParams['customerFilter'] || '';
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    exportToExcel(event?: LazyLoadEvent) {
        this._tiffinCustomerTransactionsServiceProxy
            .getCustomerTransactionsExcel(
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.paymentFilter,
                this.daysExpiryFilter,
                this.startDate,
                this.endDate,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        if ((this.dateRange as any) !== undefined && this.dateRange.length) {
            this.startDate = moment(this.dateRange[0]);
            this.endDate = moment(this.dateRange[1]);
        }
        this._tiffinCustomerTransactionsServiceProxy
            .getCustomerTransactions(
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.paymentFilter,
                this.daysExpiryFilter,
                this.startDate,
                this.endDate,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    returnOnlyNumber(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }

        if (keynum === 11) {
            return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[0123456789]/;
        return charcheck.test(keychar);
    }

    checkEditExpiryDate(expiryDate: moment.Moment): boolean {
        return moment() > expiryDate;
    }

    changeCustomer(data) {
        this.customerName = data.name;
        this.selectedCustomer = data.id;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear()
        this.paginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    apply() {
        this.paginator.changePage(0);
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.paymentFilter = undefined;
        this.daysExpiryFilter = undefined;
        this.dateRange = undefined;
        this.endDate = undefined;
        this.startDate = undefined;
        this.selectedCustomer = undefined;
        this.customerName = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

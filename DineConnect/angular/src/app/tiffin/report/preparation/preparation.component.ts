import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    PreparationReportServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
    TiffinOrderServiceProxy,
    ExportPreparationReportInput,
    PagedResultDtoOfCustomerListDto,
    CustomersServiceProxy,
    ComboboxItemDto,
    DeliveryTimeSlotServiceProxy
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { PrintConfigModalComponent } from '@app/tiffin/printing/print-config-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { unionBy } from 'lodash';

@Component({
    selector: 'app-preparation',
    templateUrl: './preparation.component.html',
    styleUrls: ['./preparation.component.scss'],
    animations: [appModuleAnimation()]
})
export class PreparationComponent extends AppComponentBase implements OnInit {
    @ViewChild('printConfigModal', { static: true }) printConfigModal: PrintConfigModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('tablePaginator', { static: true }) tablePaginator: Paginator;
    filterText = '';
    dateRange: any = [];
    selectedCustomer: any;
    customerName = '';
    products: ERPComboboxItem[] = [];
    selectedProductSet: any;
    selectedMeal: any;
    mealOptions = [];
    timeSlots: ComboboxItemDto[];
    timeSlot: string;
    files = [];
    zone: number;

    zoneOptions = [
        { text: this.l('North'), value: 1 },
        { text: this.l('NorthWest'), value: 2 },
        { text: this.l('NorthEast'), value: 3 },
        { text: this.l('Central'), value: 4 },
        { text: this.l('West'), value: 5 },
        { text: this.l('East'), value: 6 },
        { text: this.l('South/Cbd'), value: 7 }
    ];

    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _preparationService: PreparationReportServiceProxy,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _commonServiceProxy: CommonServiceProxy,
        private _customerService: CustomersServiceProxy,
        private _deliveryTimeSlotServiceProxy: DeliveryTimeSlotServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.init();
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    init() {
        this._commonServiceProxy.getAllProductSets().subscribe((result) => {
            this.products = result;
        });
        this._tiffinOrderProxy.getScheduleTimes().subscribe((result) => {
            this.mealOptions = result.items;
        });
        this._getTimeSlots();
    }

    getAll(event?: LazyLoadEvent) {
        if (this.selectedCustomer === null) {
            this.selectedCustomer = undefined;
        }

        if (this.selectedProductSet === null) {
            this.selectedProductSet = undefined;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.tablePaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._preparationService
            .getAll(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.zone,
                undefined,
                this.timeSlot,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.tablePaginator, event),
                this.primengTableHelper.getMaxResultCount(this.tablePaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map(item => {
                    return {
                        ...item,
                        timeSlot: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null
                    }
                });
            });
    }

    reloadPage(): void {
        this.tablePaginator.changePage(this.tablePaginator.getPage());
    }

    exportToExcel() {
        this.primengTableHelper.showLoadingIndicator();

        this._preparationService
            .getPreparationReportExcel(
                this.filterText,
                this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
                this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
                this.selectedMeal,
                this.selectedCustomer ? this.selectedCustomer : undefined,
                this.selectedProductSet ? this.selectedProductSet.value : undefined,
                this.zone,
                undefined,
                this.timeSlot
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
        // this.notify.warn(this.l('ReportIsExportingInBackground'));
    }

    printListPreparation() {
        let data = {
            fromDate: this.dateRange[0] ? moment(this.dateRange[0]).startOf('day') : undefined,
            toDate: this.dateRange[1] ? moment(this.dateRange[1]).endOf('day') : undefined,
            mealPlan: this.selectedMeal,
            customerId: this.selectedCustomer ? this.selectedCustomer : undefined,
            productSetId: this.selectedProductSet ? this.selectedProductSet.value : undefined
        };
        let exportPdfInput = new ExportPreparationReportInput();
        exportPdfInput.init(data);
        this._preparationService.printPreparationReport(exportPdfInput).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    downloadFile(file) {
        this._fileDownloadService.downloadTempFile(file);
    }

    printOrder(record) {
        this._tiffinOrderProxy.getOrderDetailsForPrintPreparationReport(record.orderIdList).subscribe((result) => {
            let printTemplate = `<html>
          <body style="font-family: monospace; width: 100%;margin: auto; margin-top: 10px;">`;
            result.map((item) => {
                printTemplate =
                    printTemplate +
                    `
            <div style="margin-bottom: 60px">
              <div class="mb-2" style="overflow-wrap: normal">${item.customerName ? item.customerName : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.phoneNumber ? item.phoneNumber : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.deliveryAddress ? item.deliveryAddress : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.postalCode ? item.postalCode : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.zone ? item.zone : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.deliveryDate ? item.deliveryDate.format('YYYY-MM-DD') : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.mealTime ? item.mealTime : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.productSetName ? item.productSetName : ''} X ${item.quantity ? item.quantity : ''}</div>
              <div class="mb-2" style="overflow-wrap: normal">${item.producOfferType ? item.producOfferType : ''}</div>
            </div>`;
            });

            printTemplate = printTemplate + `</body> </html>`;
            this.printConfigModal.show(printTemplate);
        });
    }

    changeCustomer(data) {
        this.customerName = data.name;
        this.selectedCustomer = data.id;
    }

    getCustomerForComboBox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfCustomerListDto> => {
            return this._customerService.getAll(filterString, null, null, sorting, skipCount, maxResultCount);
        };
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = '';
        this.dateRange = [];
        this.selectedCustomer = undefined;
        this.selectedProductSet = undefined;
        this.selectedMeal = undefined;
        this.zone = undefined;
        this.customerName = '';
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getTimeSlots() {
        this._deliveryTimeSlotServiceProxy
            .getAll(
                undefined,
                undefined,
                'id DESC',
                0,
                1000
            )
            .subscribe((result) => {
                this.timeSlots = [];
                result.items.forEach((item) => {
                    const timeSlots = JSON.parse(item.timeSlots.toLowerCase());

                    this.timeSlots.push(ComboboxItemDto.fromJS({
                        value: item.timeSlots,
                        displayText: `${timeSlots['from']}-${timeSlots['to']}`
                    }))
                });

                this.timeSlots = unionBy(this.timeSlots, item => item.displayText)
            });
    }
}

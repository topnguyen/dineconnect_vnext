import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { TiffinMealTimesServiceProxy, TiffinMealTimeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { CreateOrEditMealTimeModalComponent } from './create-or-edit-meal-time-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './meal-times.component.html',
    styleUrls: ['./meal-times.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MealTimesComponent extends AppComponentBase {
    @ViewChild('createOrEditMealTimeModal', { static: true }) createOrEditMealTimeModal: CreateOrEditMealTimeModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getTiffinMealTimes();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getTiffinMealTimes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._tiffinMealTimesServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTiffinMealTime(): void {
        this.createOrEditMealTimeModal.show();
    }

    deleteTiffinMealTime(tiffinMealTime: TiffinMealTimeDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._tiffinMealTimesServiceProxy.delete(tiffinMealTime.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

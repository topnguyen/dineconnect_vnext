import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TiffinMealTimesServiceProxy, CreateOrEditTiffinMealTimeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
  selector: 'create-or-edit-meal-time-modal',
  templateUrl: './create-or-edit-meal-time-modal.component.html',
  styleUrls: ['./create-or-edit-meal-time-modal.component.scss']
})
export class CreateOrEditMealTimeModalComponent extends AppComponentBase {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  tiffinMealTime: CreateOrEditTiffinMealTimeDto = new CreateOrEditTiffinMealTimeDto();

  constructor(
    injector: Injector,
    private _tiffinMealTimesServiceProxy: TiffinMealTimesServiceProxy
  ) {
    super(injector);
  }

  show(tiffinMealTimeId?: number): void {
    if (!tiffinMealTimeId) {
      this.tiffinMealTime = new CreateOrEditTiffinMealTimeDto();
      this.tiffinMealTime.id = tiffinMealTimeId;

      this.active = true;
      this.modal.show();
    } else {
      this._tiffinMealTimesServiceProxy.getTiffinMealTimeForEdit(tiffinMealTimeId)
        .subscribe(result => {
          this.tiffinMealTime = result;

          this.active = true;
          this.modal.show();
        });
    }
  }

  save(): void {
    this.saving = true;

    this._tiffinMealTimesServiceProxy.createOrEdit(this.tiffinMealTime)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}

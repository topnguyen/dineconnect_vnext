import { Component, OnInit, Injector, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { TemplateEnginesServiceProxy, CreateOrEditTemplateEngineDto } from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-or-edit-template-engine',
  templateUrl: './create-or-edit-template-engine.component.html',
  styleUrls: ['./create-or-edit-template-engine.component.scss'],
  animations: [appModuleAnimation()]

})
export class CreateOrEditTemplateEngineComponent extends AppComponentBase implements OnInit {
  @ViewChild('TemplateEngine_Template', { static: true }) templateInput: any;

  saving = false;

  templateEngineId: number;
  templateEngine = new CreateOrEditTemplateEngineDto();

  templateVariables = [];
  templateTypes = [];
  constructor(injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _templateEnginesServiceProxy: TemplateEnginesServiceProxy,
    private render: Renderer2,
    private elRef: ElementRef
  ) {
    super(injector);
  }

  ngOnInit() {
    this._activatedRoute.params
      .subscribe(params => {
        this.templateEngineId = params['id']; // (+) converts string 'id' to a number

        if (isNaN(this.templateEngineId) || this.templateEngineId === 0) {
          this.templateEngineId = undefined;
        }
        this._templateEnginesServiceProxy.getTemplateEngineForEdit(this.templateEngineId)
          .subscribe(result => {
            this.templateEngine = result.templateEngine;
          });
      });

    this.getVariables();
    this.getTypesForCbox();
  }

  save(): void {
    this.saving = true;

    this._templateEnginesServiceProxy.createOrEdit(this.templateEngine)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.close();
      });
  }

  close(): void {
    this._router.navigate(['/app/tiffin/templateEngines']);
  }

  getVariables() {
    this._templateEnginesServiceProxy.getTemplateVariables()
      .subscribe((result) => {
        this.templateVariables = result.items;
      });
  }
  tinymceInit = {
    plugins : [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar : 'styleselect fontselect fontsizeselect | undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify | forecolor backcolor bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap',
    image_advtab : true
  }
  getTypesForCbox() {
    this._templateEnginesServiceProxy.getTemplateTypes()
      .subscribe((result) => {
        this.templateTypes = result.items;
      });
  }

  returnDragValue(item) {
    return `{{${item}}}`;
  }

  getTemplateByModule() {
    this._templateEnginesServiceProxy.getTemplateEngineByTypeForEdit(this.templateEngine.module)
      .subscribe(result => {
        this.templateEngine = result;
      });
  }

  onDropped(event) {
    console.log(event);

  }
}

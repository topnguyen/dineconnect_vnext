﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TiffinCustomerTransactionsServiceProxy, TiffinCustomerProductOfferDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'editBalanceModal',
    templateUrl: './edit-balance-modal.component.html'
})
export class EditCustomerOfferModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    customerOffer: TiffinCustomerProductOfferDto = new TiffinCustomerProductOfferDto();

    constructor(
        injector: Injector,
        private _customerOffersServiceProxy: TiffinCustomerTransactionsServiceProxy
    ) {
        super(injector);
    }

    show(customerOfferId): void {
        this._customerOffersServiceProxy.getCustomerProductOfferForEdit(customerOfferId)
            .subscribe(result => {
                this.customerOffer = result;
                this.active = true;
                this.modal.show();
            });

    }

    save(): void {
        this.saving = true;
        this._customerOffersServiceProxy.updateCustomerOffer(this.customerOffer)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

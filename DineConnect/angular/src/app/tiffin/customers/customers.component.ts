import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { FileDownloadService } from "@shared/utils/file-download.service";
import {
    CustomersServiceProxy,
    CustomerListDto,
} from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router } from "@angular/router";
import { LazyLoadEvent } from "primeng";
import { DTableColumn } from "@app/shared/common/dynamic-table/dynamic-table.model";
import { DynamicTableComponent } from "@app/shared/common/dynamic-table/dynamic-table.component";
import { Subject } from "rxjs";
import { debounceTime, takeUntil } from "rxjs/operators";
@Component({
    templateUrl: "./customers.component.html",
    styleUrls: ["./customers.component.scss"],
    animations: [appModuleAnimation()],
})
export class CustomersComponent extends AppComponentBase implements OnInit {
    @ViewChild("dtable", { static: true }) dtable: DynamicTableComponent;

    filterText = "";

    cols: DTableColumn[] = [
        {
            field: "name",
            header: this.l("Name"),
            type: "string",
        },
        {
            field: "creationTime",
            header: this.l("RegisteredDate"),
            type: "date",
        },
        {
            field: "lastLoginDate",
            header: this.l("LastLoginDate"),
            type: "date",
        },
        {
            field: "emailAddress",
            header: this.l("ContactEmail"),
            type: "string",
        },
        {
            field: "phoneNumber",
            header: this.l("ContactPhone"),
            type: "string",
        },        
        {
            field: "acceptReceivePromotionEmail",
            header: this.l("AcceptReceivePromotionEmail"),
            type: "string",
        },
        {
            field: null,
            header: this.l("Actions"),
            type: "action",
        },
    ];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _customerService: CustomersServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit() {
        setTimeout(() => {
            this.configDtable();
        }, 500);

        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.dtable.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    configDtable() {
        this.dtable.configure({
            cols: this.cols,
            isGrantedEdit: 'Pages.Tenant.Tiffin.Customer.Customers.Edit',
            isGrantedDelete: 'Pages.Tenant.Tiffin.Customer.Customers.Delete',
            dataSource: (
                sorting: string,
                skipCount: number,
                maxResultCount: number
            ) => {
                return this._customerService.getAll(
                    this.filterText,
                    undefined,
                    undefined,
                    sorting,
                    skipCount,
                    maxResultCount
                );
            },
            edit: (item: CustomerListDto) => {
                this.createOrEditCustomer(item.id);
            },
            delete: (item: CustomerListDto) => {
                this.message.confirm(
                    this.l("CustomerDeleteWarningMessage", item.name),
                    this.l("AreYouSure"),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            this._customerService
                                .delete(item.id)
                                .subscribe(() => {
                                    this.reloadPage();
                                    this.notify.success(
                                        this.l("SuccessfullyDeleted")
                                    );
                                });
                        }
                    }
                );
            },
            stateKey: "customer-stated",
        });
    }

    reloadPage(): void {
        this.filterText = undefined;

        this.dtable.paginator.changePage(this.dtable.paginator.getPage());
    }

    createOrEditCustomer(id?) {
        let url = this._router.url;
        if (url.includes("tiffin"))
            this._router.navigate([
                "/app/tiffin/customers/create-or-update",
                id ? id : "",
            ]);
        if (url.includes("wheel"))
            this._router.navigate([
                "/app/wheel/customers/create-or-update",
                id ? id : "",
            ]);
    }

    exportToExcel(event?: LazyLoadEvent): void {
        this._customerService
            .getCustomersToExcel(
                this.filterText,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dtable.dataTable),
                this.primengTableHelper.getSkipCount(
                    this.dtable.paginator,
                    event
                ),
                this.primengTableHelper.getMaxResultCount(
                    this.dtable.paginator,
                    event
                )
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search(event: string) {
        this.filter$.next(event);
    }
    
}

import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    TiffinOrderServiceProxy,
    OrderHistoryDto,
    ChangePickupStatus,
    SendPickUpOrderRemindInput
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Paginator } from 'primeng';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import moment from 'moment';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './customer-order.component.html',
    styleUrls: ['./customer-order.component.scss'],
    animations: [appModuleAnimation()]
})
export class CustomerOrderComponent extends AppComponentBase implements OnInit {
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    term: string = '';
    isPickedUp = false;
    date: moment.Moment = moment(moment.now());
    isShowFilterOptions = false;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(injector: Injector, private _tiffinOrderProxy: TiffinOrderServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000),takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._tiffinOrderProxy
            .getOrderHistory(
                undefined,
                this.isPickedUp,
                this.term,
                this.date ? this.getDate(this.date) : undefined,
                'OrderDate DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map((item) => {
                    return {
                        ...item,
                        addOn: item.addOn ? JSON.parse(item.addOn) : [],
                        timeSlotFormTo: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null,
                        jsonDataObject: item.jsonData ? JSON.parse(item.jsonData) : {}
                    };
                });
            });
    }

    markCollected(id: number, isPickedup = true) {
        const input = new ChangePickupStatus();
        input.isPickup = isPickedup;
        input.orderId = id;
        this._tiffinOrderProxy.changePickupStatus(input).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    remind(id: number) {
        const input = new SendPickUpOrderRemindInput();
        input.orderIds = [id];

        this._tiffinOrderProxy.sendPickUpOrderRemindEmail(input).subscribe((ressult) => {
            this.notify.success(this.l('SendRemindSuccessfully'));
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    isRemind(order: OrderHistoryDto) {
        return !order.pickedUpTime;
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.term = undefined;
        this.date = undefined;
        this.isPickedUp = false;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

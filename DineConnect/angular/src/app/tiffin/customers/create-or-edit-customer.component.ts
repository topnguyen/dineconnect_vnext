import { Component, OnInit, Injector, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
    PasswordComplexitySetting,
    UserEditDto,
    CreateCustomerDto,
    ERPComboboxItem,
    CustomersServiceProxy,
    UserServiceProxy,
    ProfileServiceProxy,
    CommonServiceProxy,
    CreateOrUpdateUserInput,
    TiffinPromotionServiceProxy,
    PromotionCodeDto,
    TenantSettingsServiceProxy,
    UpdateMyAddressInputDto,
    TiffinMemberPortalServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { CustomerPromotionCodeInputDto, CustomerPromotionCodeSendBy } from "@shared/service-proxies/service-proxies-nswag";

@Component({
    selector: "create-or-edit-customer",
    templateUrl: "./create-or-edit-customer.component.html",
    styleUrls: ["./create-or-edit-customer.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditCustomerComponent
    extends AppComponentBase
    implements OnInit {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    saving = false;

    customerId: number;
    passwordComplexitySetting: PasswordComplexitySetting =
        new PasswordComplexitySetting();
    passwordComplexityInfo = "";

    user: UserEditDto = new UserEditDto();
    setRandomPassword = false;

    customer: CreateCustomerDto = new CreateCustomerDto();
    userPasswordRepeat = "";
    lstCity: ERPComboboxItem[] = [];
    lstCountry: ERPComboboxItem[] = [];
    filterText = "";
    sortField = "code";
    promotionCodes: PromotionCodeDto[] = [];
    selectedPromotionCode: PromotionCodeDto;
    grades: string[] = [' 1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'School Staff'];
    sections: string[] = [' A', 'B', 'C', 'D', 'E', 'F', 'CBSE', 'NA'];

    cardID: string;
    grade: string;
    section: string;

    isTiffinEnableStudentInformation = false;

    zoneOptions = [
        { text: this.l("North"), value: 1 },
        { text: this.l("NorthWest"), value: 2 },
        { text: this.l("NorthEast"), value: 3 },
        { text: this.l("Central"), value: 4 },
        { text: this.l("West"), value: 5 },
        { text: this.l("East"), value: 6 },
        { text: this.l("South/Cbd"), value: 7 },
    ];

    listAddress: UpdateMyAddressInputDto[] = [];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _customerService: CustomersServiceProxy,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _commonService: CommonServiceProxy,
        private _tiffinPromotionServiceProxy: TiffinPromotionServiceProxy,
        private _tenantSettingsService: TenantSettingsServiceProxy,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.customerId = params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.customerId) || this.customerId === 0) {
                this.customerId = undefined;
            }
            this.refresh();

        });
        this._tenantSettingsService.getAllSettings().subscribe(result => {
            this.isTiffinEnableStudentInformation = result.isTiffinEnableStudentInformation;
        })

        this.getPromotionCodes();
        this.initForm();
    }

    refresh() {
        this._customerService
            .getCustomerForEdit(this.customerId)
            .subscribe((result) => {
                this.customer = result;
                if (this.customer.jsonData) {
                    const jsonData = JSON.parse(this.customer.jsonData);

                    this.grade = jsonData.grade;
                    this.cardID = jsonData.cardID;
                    this.section = jsonData.section;
                }

                // get user
                if (this.customer.userId) {
                    this._userService
                        .getUserForEdit(this.customer.userId)
                        .subscribe((result1) => {
                            this.user = result1.user;
                        });
                }

                this.listAddress = result.customerAddressDetails;
                this.listAddress.forEach((item) => {
                    item.address2 = this.getAddress2(item);
                });

                this._profileService
                    .getPasswordComplexitySetting()
                    .subscribe((passwordComplexityResult) => {
                        this.passwordComplexitySetting =
                            passwordComplexityResult.setting;
                        this.setPasswordComplexityInfo();
                    });
            });
    }

    viewAddress(id?): void {
        this._router.navigate([
            "/app/tiffin/customers/customer-address",
            id ? id : "null", this.customerId
        ]);
    }

    setDefaultAddress(id: number) {
        this._memberServiceProxy.setDefaultAddress(id).subscribe(() => {
            this.notify.success(this.l("SuccessfullySetDefaultAddress"), "", {
                timer: 500,
            });
            this.refresh();
        });
    }

    deleteAddress(id: number) {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._memberServiceProxy.deleteAddress(id).subscribe(() => {
                    this.notify.success(this.l("SuccessfullyDeleted"), "", {
                        timer: 500,
                    });
                    this.refresh();
                });
            }
        });
    }

    getAddress2(addressDetail: UpdateMyAddressInputDto): string {
        if (addressDetail.address2) {
            let addressSpilit = addressDetail.address2.split(",");
            let floorNo = "";
            let unitNo = "";
            let tower = "";
            if (addressSpilit[0]) {
                floorNo = addressSpilit[0];
            }
            if (addressSpilit[1]) {
                unitNo = addressSpilit[1];
            }
            if (addressSpilit[2]) {
                tower = addressSpilit[2];
            }
            let address2 = [];
            address2.push(floorNo);
            address2.push(unitNo);
            address2.push(tower);
            return address2
                .filter((x) => typeof x === "string" && x.length > 0)
                .join();
        }
    }

    setPasswordComplexityInfo(): void {
        this.passwordComplexityInfo = "<ul>";

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo +=
                "<li>" +
                this.l("PasswordComplexity_RequireDigit_Hint") +
                "</li>";
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo +=
                "<li>" +
                this.l("PasswordComplexity_RequireLowercase_Hint") +
                "</li>";
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo +=
                "<li>" +
                this.l("PasswordComplexity_RequireUppercase_Hint") +
                "</li>";
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo +=
                "<li>" +
                this.l("PasswordComplexity_RequireNonAlphanumeric_Hint") +
                "</li>";
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo +=
                "<li>" +
                this.l(
                    "PasswordComplexity_RequiredLength_Hint",
                    this.passwordComplexitySetting.requiredLength
                ) +
                "</li>";
        }

        this.passwordComplexityInfo += "</ul>";
    }

    changeCountry(id) {
        if (id != "undefined") {
            this._commonService
                .getLookups("Cities", this.appSession.tenantId, id)
                .subscribe((result) => {
                    this.lstCity = result;
                });
        } else {
            this.lstCity = [];
        }
    }

    save() {
        if ((this.customer.zone as any) !== undefined) {
            this.customer.zone = +this.customer.zone;
        } else {
            this.customer.zone = undefined;
        }

        this.customer.emailAddress = this.user.emailAddress;
        this.customer.name = this.user.name;
        this.customer.phoneNumber = this.user.phoneNumber;
        this.customer.jsonData = JSON.stringify({
            grade: this.grade,
            cardID: this.cardID,
            section: this.section,
        });

        this.saving = true;
        this.user.userName = this.user.emailAddress;
        this.user.surname = this.user.name;
        this.user.isActive = true;
        let input = new CreateOrUpdateUserInput();
        input.user = this.user;
        input.sendActivationEmail = false;
        input.assignedRoleNames = ["Customer"];


        this._userService
            .createOrUpdateUser(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                // create
                if (result > 0) {
                    this.customer.userId = result;
                }
                this._customerService
                    .createOrEdit(this.customer)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((result) => {
                        this.notify.success(this.l("SavedSuccessfully"));
                        this.close();
                    });
            });
    }

    close(): void {
        let url = this._router.url;
        if (url.includes("tiffin"))
            this._router.navigate(["/app/tiffin/customers"]);
        if (url.includes("wheel"))
            this._router.navigate(["/app/wheel/customers"]);
    }

    initForm() {
        this._commonService
            .getLookups("Cities", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCity = result;
            });
        this._commonService
            .getLookups("Countries", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCountry = result;
            });
    }

    getAssignedPromotionCodes(event?: LazyLoadEvent) {
        this._tiffinPromotionServiceProxy
            .getPromotionCodes(
                this.filterText,
                this.customerId,
                this.primengTableHelper.getSorting(this.dataTable) ||
                this.sortField,
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                if (result) {
                    this.primengTableHelper.totalRecordsCount =
                        result.totalCount;
                    this.primengTableHelper.records = result.items;
                }
            });
    }

    getPromotionCodes(event?: { query: string }) {
        this._tiffinPromotionServiceProxy
            .getPromotionCodes(
                event ? event.query : undefined,
                undefined,
                this.sortField,
                0,
                100
            )
            .subscribe((result) => {
                this.promotionCodes = result.items;
            });
    }

    deleteCode(voucher: PromotionCodeDto) {
        this.message.confirm(
            this.l("VoucherDeleteWarningMessage", voucher.code),
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tiffinPromotionServiceProxy
                        .removeCustomerToPromotionCode(
                            voucher.id,
                            this.customerId
                        )
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l("SuccessfullyDeleted"));
                        });
                }
            }
        );
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
        this.selectedPromotionCode = null;

        this.getAssignedPromotionCodes();
    }

    addPromotionCodes() {
        if (this.selectedPromotionCode) {
            const body = CustomerPromotionCodeInputDto.fromJS({
                codeId: this.selectedPromotionCode.id,
                customerId: this.customerId,
            });
            this._tiffinPromotionServiceProxy
                .registerCustomerToPromotionCode(body)
                .subscribe((result) => {
                    this.notify.success(this.l("AddedVoucherSuccessfully"));
                    this.reloadPage();
                });
        }
    }

    sendEmail(voucher: PromotionCodeDto) {
        const body = CustomerPromotionCodeInputDto.fromJS({
            codeId: voucher.id,
            customerId: this.customerId,
            sendBy: CustomerPromotionCodeSendBy.Email
        });
        this._sendPromotionCodeToCustomer(body);
    }

    sendWhatsApp(voucher: PromotionCodeDto) {
        const body = CustomerPromotionCodeInputDto.fromJS({
            codeId: voucher.id,
            customerId: this.customerId,
            sendBy: CustomerPromotionCodeSendBy.Whatapp
        });
        this._sendPromotionCodeToCustomer(body);
    }

    private _sendPromotionCodeToCustomer(body: CustomerPromotionCodeInputDto) {
        this._tiffinPromotionServiceProxy
            .sendPromotionCodeToCustomer(body)
            .subscribe((result) => {
                if (body.sendBy === CustomerPromotionCodeSendBy.Email) {
                    this.notify.success(this.l("SentEmailSuccessfully")); 
                } else {
                    this.notify.success(this.l("SentWhatsAppSuccessfully"));
                }
            });
    }

    createTenant() { }
}

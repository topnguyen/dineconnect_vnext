import { Component, OnInit, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import * as qz from 'qz-tray';
import { sha256 } from 'js-sha256';
import { PrintConfigOptionInput } from './print-config-options.model';
import { PrintDemoInput } from './print-demo-input.model';

@Component({
  selector: 'print-config-modal',
  templateUrl: './print-config-modal.component.html',
  styleUrls: ['./print-config-modal.component.scss']
})
export class PrintConfigModalComponent extends AppComponentBase {

  @ViewChild('printModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  printTemplate = '';
  printConfigOptions = new PrintConfigOptionInput();
  printer = '';

  filterPrinter = '';

  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  show(template): void {
    this.printConfigOptions = new PrintConfigOptionInput();
    this.printer = '';
    this.filterPrinter = '';

    qz.api.setSha256Type(data => sha256(data));
    qz.api.setPromiseType(resolver => new Promise(resolver));

    if (!qz.websocket.isActive()) {
      qz.websocket.connect();
    }

    this.printTemplate = template;
    this.active = true;
    this.modal.show();
  }

  print(): void {
    this.saving = true;
    let opts = this.setPrintOptions();

    let printData = [
      {
        type: 'pixel',
        format: 'html',
        flavor: 'plain',
        data: this.printTemplate,
        options: opts
      }
    ];
    let self = this;

    if (!this.printer) {
      this.message.warn(this.l('PleaseSelectPrinterBeforePrint'));
      return;
    }

    // qz.printers.find(this.printer)
    //   .then(function (printer) {
    let cfg = qz.configs.create(this.printer, self.setConfig());       // Create a default config for the found printer
    // Raw ZPL
    return qz.print(cfg, printData).then(function () {
      self.saving = false;
      self.notify.info(self.l('PrintedSuccessfully'));
      self.close();
      self.modalSave.emit(null);
    });
    // }).catch(function (e) { console.error(e); });
  }

  getPrinter() {
    qz.printers.find(this.filterPrinter).then((found) => {
      this.printer = found;
    });
  }

  getDefaultPrinter() {
    qz.printers.getDefault()
      .then((printer) => {
        this.printer = printer;
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
    if (qz.websocket.isActive()) {
      qz.websocket.disconnect();
    }
  }

  resetPixelOptions() {
    this.printConfigOptions = new PrintConfigOptionInput();
  }

  setConfig() {
    let pxlSize = null;
    if (this.printConfigOptions.sizeActive) {
      pxlSize = {
        width: this.printConfigOptions.sizeWidth,
        height: this.printConfigOptions.sizeHeight
      };
    }

    let pxlMargins: any;
    if (this.printConfigOptions.marginsActive) {
      pxlMargins = {
        top: this.printConfigOptions.marginsTop,
        right: this.printConfigOptions.marginsRight,
        bottom: this.printConfigOptions.marginsBottom,
        left: this.printConfigOptions.marginsLeft
      };
    } else {
      pxlMargins = this.printConfigOptions.margins;
    }

    return {
      colorType: this.printConfigOptions.colorType,
      copies: this.printConfigOptions.copies,
      density: this.printConfigOptions.density,
      duplex: this.printConfigOptions.duplex,
      interpolation: this.printConfigOptions.interpolation,
      jobName: this.printConfigOptions.jobName,
      margins: pxlMargins,
      orientation: this.printConfigOptions.orientation,
      paperThickness: this.printConfigOptions.paperThickness,
      printerTray: this.printConfigOptions.printerTray,
      rasterize: this.printConfigOptions.rasterize,
      rotation: this.printConfigOptions.rotation,
      scaleContent: this.printConfigOptions.scale,
      size: pxlSize,
      units: this.printConfigOptions.units
    };
  }

  setPrintOptions() {
    return {
      pageWidth: this.printConfigOptions.renderWidth,
      pageHeight: this.printConfigOptions.renderHeight
    };
  }
}


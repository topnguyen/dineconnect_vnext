export class PrintDemoInput {
  name!: string | undefined;
  number!: string | undefined;
  address!: string | undefined;
  zone!: string | undefined;
  choiceOfMeal!: string | undefined;
  remarks!: string | undefined;

  constructor(data?: IPrintDemoInput) {
    if (data) {
      for (let property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }
}

export interface IPrintDemoInput {
  name: string | undefined;
  number: string | undefined;
  address: string | undefined;
  zone: string | undefined;
  choiceOfMeal: string | undefined;
  remarks: string | undefined;
}




import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as qz from 'qz-tray';
import { sha256 } from 'js-sha256';
import { PrintDemoInput } from './print-demo-input.model';
import { PrintConfigOptionInput } from './print-config-options.model';

@Component({
  selector: 'app-print-demo',
  templateUrl: './print-demo.component.html',
  styleUrls: ['./print-demo.component.scss'],
  animations: [appModuleAnimation()]
})
export class PrintDemoComponent extends AppComponentBase implements OnInit {
  printing = false;

  printInput = new PrintDemoInput();
  printTemplate: string;
  printConfigOptions = new PrintConfigOptionInput();

  firstTab = true;

  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    qz.api.setSha256Type(data => sha256(data));
    qz.api.setPromiseType(resolver => new Promise(resolver));

    qz.websocket.connect();
  }

  next(): void {
    this.firstTab = false;

    this.printTemplate = `<html>
      <body >
        <table style="font-family: monospace; width: 80%; margin: auto;">
          <tr>
            <td>
              <h2>* HTML Sample Print *</h2>
            </td>
          </tr>
        </table>
        <table style="font-family: monospace; width: 80%;margin: auto;">
          <tr>
            <td style="width:20%">Name</td>
            <td style="width:80%">${this.printInput.name ? this.printInput.name : ''}</td>
          </tr>
          <tr>
            <td style="width:20%">Address</td>
            <td style="width:80%">${this.printInput.address ? this.printInput.address : ''}</td>
          </tr>
          <tr>
            <td style="width:20%">Number</td>
            <td style="width:80%">${this.printInput.number ? this.printInput.number : ''}</td>
          </tr>
          <tr>
            <td style="width:20%">Zone</td>
            <td style="width:80%">${this.printInput.zone ? this.printInput.zone : ''}</td>
          </tr>
          <tr>
            <td style="width:20%">ChoiceOfMeal</td>
            <td style="width:80%">${this.printInput.choiceOfMeal ? this.printInput.choiceOfMeal : ''}</td>
          </tr>
          <tr>
            <td style="width:20%">Remarks</td>
            <td style="width:80%">${this.printInput.remarks ? this.printInput.remarks : ''}</td>
          </tr>
        </table>
      </body>
      </html>`;

    if (!qz.websocket.isActive()) {
      qz.websocket.connect();
    }
  }

  clear(): void {
    this.printInput = new PrintDemoInput();
    this.printTemplate = undefined;
    this.resetPixelOptions();
  }

  print(): void {
    this.printing = true;
    let opts = this.setPrintOptions();

    let printData = [
      {
        type: 'pixel',
        format: 'html',
        flavor: 'plain',
        data: this.printTemplate,
        options: opts
      }
    ];
    let self = this;

    qz.printers.getDefault()
      .then(function (printer) {
        let cfg = qz.configs.create(printer, self.setConfig());       // Create a default config for the found printer
        // Raw ZPL
        return qz.print(cfg, printData).then(function () {
          self.printing = false;
          self.notify.info(self.l('PrintedSuccessfully'));
        });
      }).catch(function (e) { console.error(e); });
  }

  back(): void {
    this.firstTab = false;

    if (qz.websocket.isActive()) {
      qz.websocket.disconnect();
    }
    this.resetPixelOptions();

    this.firstTab = true;
  }

  resetPixelOptions() {
    this.printConfigOptions = new PrintConfigOptionInput();
  }

  setConfig() {
    let pxlSize = null;
    if (this.printConfigOptions.sizeActive) {
      pxlSize = {
        width: this.printConfigOptions.sizeWidth,
        height: this.printConfigOptions.sizeHeight
      };
    }

    let pxlMargins: any;
    if (this.printConfigOptions.marginsActive) {
      pxlMargins = {
        top: this.printConfigOptions.marginsTop,
        right: this.printConfigOptions.marginsRight,
        bottom: this.printConfigOptions.marginsBottom,
        left: this.printConfigOptions.marginsLeft
      };
    } else {
      pxlMargins = this.printConfigOptions.margins;
    }


    return {
      colorType: this.printConfigOptions.colorType,
      copies: this.printConfigOptions.copies,
      density: this.printConfigOptions.density,
      duplex: this.printConfigOptions.duplex,
      interpolation: this.printConfigOptions.interpolation,
      jobName: this.printConfigOptions.jobName,
      margins: pxlMargins,
      orientation: this.printConfigOptions.orientation,
      paperThickness: this.printConfigOptions.paperThickness,
      printerTray: this.printConfigOptions.printerTray,
      rasterize: this.printConfigOptions.rasterize,
      rotation: this.printConfigOptions.rotation,
      scaleContent: this.printConfigOptions.scale,
      size: pxlSize,
      units: this.printConfigOptions.units
    };
  }

  setPrintOptions() {
    return {
      pageWidth: this.printConfigOptions.renderWidth,
      pageHeight: this.printConfigOptions.renderHeight
    };
  }
}


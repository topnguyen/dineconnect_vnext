export class PrintConfigOptionInput {
  colorType!: string | undefined;
  copies!: number;
  density!: number;
  duplex!: boolean;
  interpolation!: string | undefined;
  jobName!: string | undefined;
  legacy!: boolean;
  orientation!: string | undefined;
  paperThickness!: number;
  printerTray!: string | undefined;
  rasterize!: boolean;
  rotation!: number;
  units!: string | undefined;
  scale!: boolean;
  marginsActive!: boolean;
  margins!: number;
  marginsTop!: number;
  marginsRight!: number;
  marginsBottom!: number;
  marginsLeft!: number;
  sizeActive!: boolean;
  sizeHeight!: number;
  sizeWidth!: number;
  renderWidth!: number;
  renderHeight!: number;

  constructor(data?: IPrintConfigOptionInput) {
    if (data) {
      for (let property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }
}

export interface IPrintConfigOptionInput {
  colorType: string | undefined;
  copies: number;
  density: number;
  duplex: boolean;
  interpolation: string | undefined;
  jobName: string | undefined;
  legacy: boolean;
  orientation: string | undefined;
  paperThickness: number;
  printerTray: string | undefined;
  rasterize: boolean;
  rotation: number;
  units: string | undefined;
  scale: boolean;
  marginsActive: boolean;
  margins: number;
  marginsTop: number;
  marginsRight: number;
  marginsBottom: number;
  marginsLeft: number;
  sizeActive: boolean;
  sizeHeight: number;
  sizeWidth: number;
  renderWidth: number;
  renderHeight: number;
}


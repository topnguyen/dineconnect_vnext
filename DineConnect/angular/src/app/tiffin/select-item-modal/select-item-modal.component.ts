import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewChild,
    OnInit,
    Input,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    MenuItemServiceProxy,
    IdNameDto,
    PromotionRestrictItemEditDto,
} from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import { PrimengTableHelper } from "shared/helpers/PrimengTableHelper";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    selector: "select-item-modal",
    templateUrl: "./select-item-modal.component.html",
})
export class SelectItemModalComponent
    extends AppComponentBase
    implements OnInit
{
    @Output() itemSelected: EventEmitter<IdNameDto> =
        new EventEmitter<IdNameDto>();

    @ViewChild("modal", { static: true }) modal: ModalDirective;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    selectedMenuItem: IdNameDto;

    constructor(
        injector: Injector,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
        this.primengTableHelper = new PrimengTableHelper();
    }

    show(): void {
        this.primengTableHelper.records = [];
        this.modal.show();
    }

    ngOnInit() {}

    shown(): void {
        this.getMenuItems(null);
    }

    getMenuItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._menuItemServiceProxy
            .getSinglePortionItems(
                this.filterText,
                undefined,
                [],
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    menuItem() {
        this.itemSelected.emit(this.selectedMenuItem);
        this.close();
    }

    close() {
        this.modal.hide();
    }
}

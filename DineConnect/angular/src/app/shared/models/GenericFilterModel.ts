
export interface IGenericFilterModel {
    value: string;
    name: string;
    label: string;
    type:string;
}
export class Table {
    id: number;
    name: string;
    width: number;
    height: number;
    positionX: number;
    positionZ: number;
    capacity: number;
    minimum: number;
    isDeleted: boolean;
    idElement: number;
    constructor(capacity: number = 1, minimum: number = 1) {
        this.capacity = capacity;
        this.minimum = minimum;
    }
}

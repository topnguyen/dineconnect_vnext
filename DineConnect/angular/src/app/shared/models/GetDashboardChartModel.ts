import { IdNameDto, SimpleLocationDto } from "@shared/service-proxies/service-proxies";

export interface GetDashboardChartModel {
    location: number | undefined;
    userId: number | undefined;
    startDate: moment.Moment | undefined;
    endDate: moment.Moment | undefined;
    locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Groups: IdNameDto[] | null | undefined;
    locationGroup_LocationTags: IdNameDto[] | null | undefined;
    locationGroup_NonLocations: SimpleLocationDto[] | null | undefined;
    locationGroup_Group: boolean | undefined;
    locationGroup_LocationTag: boolean | undefined;
    locationGroup_UserId: number | undefined;
    notCorrectDate: boolean | undefined;
    credit: boolean | undefined;
    refund: boolean | undefined;
    ticketNo: string | null | undefined;
}
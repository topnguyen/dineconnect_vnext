import {
    Component,
    EventEmitter,
    Injector,
    OnInit,
    Output,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CommonLocationGroupDto,
    LocationDto,
    LocationGroupServiceProxy,
    LocationServiceProxy,
    LocationTagsServiceProxy,
    SimpleLocationDto,
} from "@shared/service-proxies/service-proxies";
import _ from "lodash";
import { ModalDirective } from "ngx-bootstrap/modal";
import { TabDirective } from "ngx-bootstrap/tabs";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";
import { PrimengTableHelper } from "shared/helpers/PrimengTableHelper";

@Component({
    selector: "select-location-or-group",
    templateUrl: "./select-location-or-group.component.html",
    styleUrls: ["./select-location-or-group.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class SelectLocationOrGroupComponent
    extends AppComponentBase
    implements OnInit
{
    @Output() locationAdded: EventEmitter<CommonLocationGroupDto> =
        new EventEmitter<any>();

    @ViewChild("modal", { static: false }) modal: ModalDirective;

    // --------------------------------------------------

    TYPES = {
        Location: "Location",
        LocationGroup: "LocationGroup",
        LocationTag: "LocationTag",
    };
    TABS = {
        Location: 0,
        NonLocation: 1,
    };
    tabIndex = this.TABS.Location;

    type = this.TYPES.Location;

    isReadonly = false;

    // --

    selected: CommonLocationGroupDto = new CommonLocationGroupDto();

    selectedTags: Array<{
        id: number;
        name: string;
        code?: string;
    }> = [];

    filterText = {
        locations: "",
        locationGroups: "",
        locationTags: "",
        restrictLocations: "",
    };

    @ViewChild("locationDataTable", { static: true }) locationDataTable: Table;
    @ViewChild("locationPaginator", { static: true })
    locationPaginator: Paginator;
    locationDataTableHelper = new PrimengTableHelper();

    @ViewChild("locationGroupDataTable", { static: true })
    locationGroupDataTable: Table;
    @ViewChild("locationGroupPaginator", { static: true })
    locationGroupPaginator: Paginator;
    locationGroupDataTableHelper = new PrimengTableHelper();

    @ViewChild("locationTagDataTable", { static: true })
    locationTagDataTable: Table;
    @ViewChild("locationTagPaginator", { static: true })
    locationTagPaginator: Paginator;
    locationTagDataTableHelper = new PrimengTableHelper();

    @ViewChild("restrictLocationDataTable", { static: true })
    restrictLocationDataTable: Table;
    @ViewChild("restrictLocationPaginator", { static: true })
    restrictLocationPaginator: Paginator;
    restrictLocationDataTableHelper = new PrimengTableHelper();

    // --------------------------------------------------

    get typeLabel() {
        if (this.tabIndex === this.TABS.NonLocation) {
            return this.l("Locations");
        }

        switch (this.type) {
            case this.TYPES.Location:
                return this.l("Locations");

            case this.TYPES.LocationGroup:
                return this.l("LocationGroups");

            case this.TYPES.LocationTag:
                return this.l("LocationTags");
        }
    }

    get selectedCount() {
        const isLocationOrGroupTab = this.tabIndex === 0;
        let result = 0;

        if (isLocationOrGroupTab) {
            switch (this.type) {
                case this.TYPES.Location:
                    result = this.selected?.locations?.length;
                    break;

                case this.TYPES.LocationGroup:
                    result = this.selected?.groups?.length;
                    break;

                case this.TYPES.LocationTag:
                    result = this.selected?.locationTags?.length;
                    break;
            }
        } else {
            result = this.selected?.nonLocations?.length;
        }

        return result || 0;
    }

    // --------------------------------------------------

    constructor(
        injector: Injector,
        private _locationServiceProxy: LocationServiceProxy,
        private _locationGroupServiceProxy: LocationGroupServiceProxy,
        private _locationTagsServiceProxy: LocationTagsServiceProxy
    ) {
        super(injector);

        this.locationDataTableHelper.totalRecordsCount = 0;
        this.locationDataTableHelper.records = [];

        this.locationGroupDataTableHelper.totalRecordsCount = 0;
        this.locationGroupDataTableHelper.records = [];

        this.locationTagDataTableHelper.totalRecordsCount = 0;
        this.locationTagDataTableHelper.records = [];

        this.restrictLocationDataTableHelper.totalRecordsCount = 0;
        this.restrictLocationDataTableHelper.records = [];
    }

    // --------------------------------------------------

    ngOnInit() {}

    // --------------------------------------------------

    onSelectTab(data: TabDirective, index: number) {
        this.tabIndex = index;

        this.updateSelectedTags();
    }

    onChangeType(event) {
        this.type = event.target.value;
        this.updateSelectedTags();
    }

    // --

    reset(clearTable = true) {
        this.tabIndex = this.selected.nonLocations?.length
            ? this.TABS.NonLocation
            : this.TABS.Location;

        if (this.selected.group) {
            this.type = this.TYPES.LocationGroup;
        } else if (this.selected.locationTag) {
            this.type = this.TYPES.LocationTag;
        } else {
            this.type = this.TYPES.Location;
        }

        if (clearTable) {
            this.filterText = {
                locations: "",
                locationGroups: "",
                locationTags: "",
                restrictLocations: "",
            };

            this.locationDataTableHelper.totalRecordsCount = 0;
            this.locationDataTableHelper.records = [];

            this.locationGroupDataTableHelper.totalRecordsCount = 0;
            this.locationGroupDataTableHelper.records = [];

            this.locationTagDataTableHelper.totalRecordsCount = 0;
            this.locationTagDataTableHelper.records = [];

            this.restrictLocationDataTableHelper.totalRecordsCount = 0;
            this.restrictLocationDataTableHelper.records = [];
        }
    }

    show(selected?: CommonLocationGroupDto, isReadonly = false) {
        this.isReadonly = isReadonly;
        this.selected = selected
            ? _.cloneDeep(selected)
            : new CommonLocationGroupDto();

        this.reset();
        this.updateSelectedTags();

        this.modal.show();
    }

    shown() {
        this.getLocationsOrsGroups();
    }

    close() {
        this.modal.hide();
    }

    save() {
        if (!this.isReadonly) {
            this.selected.group = false;
            this.selected.locationTag = false;

            switch (this.type) {
                case this.TYPES.Location:
                    break;
                case this.TYPES.LocationGroup:
                    this.selected.group = true;
                    break;
                case this.TYPES.LocationTag:
                    this.selected.locationTag = true;
                    break;
                default:
                    break;
            }

            this.selected.locations =
                this.type === this.TYPES.Location
                    ? this.selected.locations
                    : [];
            this.selected.groups =
                this.type === this.TYPES.LocationGroup
                    ? this.selected.groups
                    : [];
            this.selected.locationTags =
                this.type === this.TYPES.LocationTag
                    ? this.selected.locationTags
                    : [];
            this.selected.nonLocations =
                this.tabIndex === this.TABS.NonLocation
                    ? this.selected.nonLocations
                    : [];

            this.locationAdded.emit(this.selected);
        }

        this.close();
    }

    clear() {
        this.selected = new CommonLocationGroupDto();
        this.reset(true);

        this.getLocationsOrsGroups();
        this.updateSelectedTags();
    }

    // --

    getLocationsOrsGroups(event?: LazyLoadEvent) {
        this.getLocations(event);
        this.getLocationGroups(event);
        this.getLocationTags(event);
        this.getRestrictLocations(event);
    }

    getLocations(event?: LazyLoadEvent) {
        if (this.locationDataTableHelper.shouldResetPaging(event)) {
            this.locationPaginator.changePage(0);
            return;
        }

        this.locationDataTableHelper.showLoadingIndicator();

        this._locationServiceProxy
            .getSimpleLocations(
                this.filterText.locations,
                false,
                0,
                this.locationDataTableHelper.getSorting(this.locationDataTable),
                this.locationDataTableHelper.getMaxResultCount(
                    this.locationPaginator,
                    event
                ),
                this.locationDataTableHelper.getSkipCount(
                    this.locationPaginator,
                    event
                )
            )
            .pipe(
                finalize(() =>
                    this.locationDataTableHelper.hideLoadingIndicator()
                )
            )
            .subscribe((result) => {
                this.locationDataTableHelper.records = result.items;
                this.locationDataTableHelper.totalRecordsCount =
                    result.totalCount;

                this.locationDataTableHelper.hideLoadingIndicator();
            });
    }

    getLocationGroups(event?: LazyLoadEvent) {
        if (this.locationGroupDataTableHelper.shouldResetPaging(event)) {
            this.locationGroupPaginator.changePage(0);
            return;
        }

        this.locationGroupDataTableHelper.showLoadingIndicator();

        this._locationGroupServiceProxy
            .getSimpleLocationGroups(
                this.filterText.locationGroups,
                this.locationGroupDataTableHelper.getSorting(
                    this.locationGroupDataTable
                ),
                this.locationGroupDataTableHelper.getMaxResultCount(
                    this.locationGroupPaginator,
                    event
                ),
                this.locationGroupDataTableHelper.getSkipCount(
                    this.locationGroupPaginator,
                    event
                )
            )
            .pipe(
                finalize(() =>
                    this.locationGroupDataTableHelper.hideLoadingIndicator()
                )
            )
            .subscribe((result) => {
                this.locationGroupDataTableHelper.records = result.items;
                this.locationGroupDataTableHelper.totalRecordsCount =
                    result.totalCount;
                this.locationGroupDataTableHelper.hideLoadingIndicator();
            });
    }

    getLocationTags(event?: LazyLoadEvent) {
        if (this.locationTagDataTableHelper.shouldResetPaging(event)) {
            this.locationTagPaginator.changePage(0);
            return;
        }

        this.locationTagDataTableHelper.showLoadingIndicator();

        this._locationTagsServiceProxy
            .getSimpleLocationTags(
                this.filterText.locationTags,
                this.locationTagDataTableHelper.getSorting(
                    this.locationTagDataTable
                ),
                this.locationTagDataTableHelper.getMaxResultCount(
                    this.locationTagPaginator,
                    event
                ),
                this.locationTagDataTableHelper.getSkipCount(
                    this.locationTagPaginator,
                    event
                )
            )
            .pipe(
                finalize(() =>
                    this.locationTagDataTableHelper.hideLoadingIndicator()
                )
            )
            .subscribe((result) => {
                this.locationTagDataTableHelper.records = result.items;
                this.locationTagDataTableHelper.totalRecordsCount =
                    result.totalCount;
                this.locationTagDataTableHelper.hideLoadingIndicator();
            });
    }

    getRestrictLocations(event?: LazyLoadEvent) {
        if (this.restrictLocationDataTableHelper.shouldResetPaging(event)) {
            this.restrictLocationPaginator.changePage(0);
            return;
        }

        this.restrictLocationDataTableHelper.showLoadingIndicator();

        this._locationServiceProxy
            .getSimpleLocations(
                this.filterText.restrictLocations,
                false,
                0,
                this.restrictLocationDataTableHelper.getSorting(
                    this.restrictLocationDataTable
                ),
                this.restrictLocationDataTableHelper.getMaxResultCount(
                    this.restrictLocationPaginator,
                    event
                ),
                this.restrictLocationDataTableHelper.getSkipCount(
                    this.restrictLocationPaginator,
                    event
                )
            )
            .pipe(
                finalize(() =>
                    this.restrictLocationDataTableHelper.hideLoadingIndicator()
                )
            )
            .subscribe((result) => {
                this.restrictLocationDataTableHelper.records = result.items;
                this.restrictLocationDataTableHelper.totalRecordsCount =
                    result.totalCount;

                this.restrictLocationDataTableHelper.hideLoadingIndicator();
            });
    }

    updateSelectedTags(e?) {
        setTimeout(() => {
            const isLocationOrGroupTab = this.tabIndex === this.TABS.Location;
            let result: any[] = [];

            if (isLocationOrGroupTab) {
                switch (this.type) {
                    case this.TYPES.Location:
                        result = this.selected?.locations || [];
                        break;

                    case this.TYPES.LocationGroup:
                        result = this.selected?.groups || [];
                        break;

                    case this.TYPES.LocationTag:
                        result = this.selected?.locationTags || [];
                        break;
                }
            } else {
                result = this.selected?.nonLocations || [];
            }

            this.selectedTags = result;
        }, 0);
    }

    removeTag(id: number) {
        const isLocationOrGroupTab = this.tabIndex === this.TABS.Location;
        let result: any[] = [];
        let key = "";

        if (isLocationOrGroupTab) {
            switch (this.type) {
                case this.TYPES.Location:
                    result = this.selected?.locations || [];
                    key = "locations";

                    break;

                case this.TYPES.LocationGroup:
                    result = this.selected?.groups || [];
                    key = "groups";

                    break;

                case this.TYPES.LocationTag:
                    result = this.selected?.locationTags || [];
                    key = "locationTags";

                    break;
            }
        } else {
            result = this.selected?.nonLocations || [];
            key = "nonLocations";
        }
        const index = result.findIndex((x) => x.id === id);
        if (index >= 0) {
            result.splice(index, 1);
            this.selected[key] = [...result];
            this.updateSelectedTags();
        }
    }

    isRestrictLocation(location: SimpleLocationDto) {
        return this.selected.nonLocations.some(
            (record) => record.id === location.id
        );
    }
}

import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    Injector,
    Component,
    forwardRef,
    Input,
    AfterViewInit,
} from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import * as Dropzone from "dropzone";
import { AppConsts } from "@shared/AppConsts";
import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { TokenService, IAjaxResponse } from "abp-ng2-module";
@Component({
    selector: "dropzone",
    templateUrl: "./dropzone.component.html",
    styleUrls: ["./dropzone.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DropzoneComponent),
            multi: true,
        },
    ],
})
export class DropzoneComponent
    extends AppComponentBase
    implements AfterViewInit, ControlValueAccessor
{
    @Input("id") id: string;
    @Input("acceptedFiles") acceptedFiles: string;
    @Input("type") type: string = "image";
    dropzone: any;
    sending: boolean = false;
    private _value: any = "";

    get value(): any {
        return this._value;
    }

    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChange(v);
        }
    }

    progress: number = 0;
    private uploader: FileUploader;

    constructor(injector: Injector, private _tokenService: TokenService) {
        super(injector);
    }

    ngAfterViewInit(): void {
        this.dropzone = new Dropzone(`div#${this.id}`, {
            url:
                AppConsts.remoteServiceBaseUrl +
                (this.type === "video"
                    ? "/ImageUploader/UploadVideo"
                    : "/ImageUploader/UploadImageForScreenMenuItem"),
            uploadMultiple: false,
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedFiles: this.acceptedFiles,
            headers: {
                Authorization: "Bearer " + this._tokenService.getToken(),
            },
        });
        this.initUploader();

        this.dropzone.on("success", (file, response) => {
            setTimeout(() => {
                this.progress = 0;
                this.sending = false;
                this.value = response.result.url;
            }, 1500);
        });

        this.dropzone.on("sending", () => {
            this.sending = true;
            this.progress = 40;
        });

        this.dropzone.on("uploadprogress", (file, progress) => {
            setTimeout(() => {
                this.progress = progress;
            }, 1500);
        });
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url:
                AppConsts.remoteServiceBaseUrl +
                (this.type === "video"
                    ? "/ImageUploader/UploadVideo"
                    : "/ImageUploader/UploadImageForScreenMenuItem"),
            authToken: "Bearer " + this._tokenService.getToken(),
        });

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        const uploaderOptions: FileUploaderOptions = {};
        uploaderOptions.removeAfterUpload = true;
        uploaderOptions.autoUpload = true;
        this.uploader.setOptions(uploaderOptions);

        this.uploader.onProgressItem = (file, progress) => {
            setTimeout(() => {
                this.progress = progress;
            }, 1500);
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            setTimeout(() => {
                this.progress = 0;
                this.sending = false;
                const ajaxResponse = <IAjaxResponse>JSON.parse(response);
                if (ajaxResponse.success) {
                    if (ajaxResponse.result.url) {
                        this.value = ajaxResponse.result.url;
                    } else {
                        this.notify.error(ajaxResponse.result.message);
                    }
                } else {
                    this.notify.error(ajaxResponse.error.message);
                }
            }, 1500);
        };
    }

    onUploadFile(file): void {
        this.sending = true;
        this.progress = 40;
        this.uploader.addToQueue([file]);
    }

    remove(): void {
        this.value = "";
    }

    onChange(value: any) {}

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {}
}

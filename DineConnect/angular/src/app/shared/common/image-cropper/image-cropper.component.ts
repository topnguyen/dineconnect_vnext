import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { base64ToFile, CropperPosition, Dimensions, ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent extends AppComponentBase implements OnInit {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @Input() width: number;
  @Input() height: number;
  @Input() uploadUrl: string;
  @Input("id") id: string;
  @Input("acceptedFiles") acceptedFiles: string;

  @Input() ngModel: string;
  @Output() ngModelChange = new EventEmitter<string>();
  private uploader: FileUploader;
  _uploaderOptions: FileUploaderOptions = {};
  imageChangedEvent: any = '';
  croppedImage: any = '';

  transform: ImageTransform = {};
  scale = 1;
  min = 0.1;
  max = 2;
  step = 0.1;
  containWithinAspectRatio = false;
  widthImage: number;
  heightImage: number;

  canvasRotation = 0;
  rotation = 0;
  showCropper = false;
  loading = false;

  constructor(
    injector: Injector,
    private _tokenService: TokenService) {
    super(injector)
  }

  ngOnInit(): void {
    this.initUploaders();
  }

  initUploaders(): void {
    this.uploader = new FileUploader({ url: this.uploadUrl });
    this._uploaderOptions.autoUpload = true;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    var self = this;
    this.uploader.onSuccessItem = (item, response) => {
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success == true) {
        this.ngModel = resp.result.url;
        this.ngModelChange.emit(this.ngModel);
        self.close();
      }
    };

    this.uploader.onCompleteAll = () => {
      self.loading = false
    }

    this.uploader.setOptions(this._uploaderOptions);
  }

  show() {
    this.modal.show();
  }

  remove(): void {
    this.ngModel = "";
    this.ngModelChange.emit(this.ngModel);
  }

  save() {
    this.loading = true;
    this.uploader.clearQueue();
    this.uploader.addToQueue([<File>base64ToFile(this.croppedImage)]);
  }

  close() {
    this.modal.hide();
  }

  fileChangeEvent(event: any): void {
   
    this.imageChangedEvent = event;

    let image: any = event.target.files[0];
    let fr = new FileReader;
    fr.onload = () => { // when file has loaded
      var img = new Image();

      img.onload = () => {
        this.widthImage = img.width;
        this.heightImage = img.height;
      };
      img.src = fr.result.toString();
    }
    fr.readAsDataURL(image);
    this.show();
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  onChangeScale(event) {
    console.log('this.scale', this.scale);
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

  imageLoaded() {
    this.showCropper = true;
  }

  cropperReady() {
  }

  loadImageFailed() { }
}

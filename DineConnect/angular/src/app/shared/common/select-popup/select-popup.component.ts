import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewChild,
    Input,
    OnInit,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    LocationDto,
    LocationServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import { PrimengTableHelper } from "shared/helpers/PrimengTableHelper";
import { finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { Paginator } from "primeng/paginator";
import { Table } from "primeng/table";
import { LazyLoadEvent } from "primeng/public_api";

@Component({
    selector: "select-popup",
    templateUrl: "./select-popup.component.html",
    animations: [appModuleAnimation()],
})
export class SelectPopupComponent extends AppComponentBase implements OnInit {
    @Input() selectedValue: any | any[];
    @Input() loadData: (
        filterText: string,
        sorting: string,
        maxResultCount: number,
        skipCount: number
    ) => Observable<any>;
    @Input() headers: string[] = [];
    @Input() dataBinding: string[] = [];
    @Input() selectionMode: string;
    @Input() rememberSelect = true;
    @Input() title: string;
    @Input() dataResult: Observable<any>;
    @Output("changeValue") changeValue: EventEmitter<any> =
        new EventEmitter<any>();
    @Output("loadDataResult") loadDataResult: EventEmitter<any> =
        new EventEmitter<any>();
    @ViewChild("modal", { static: true }) modal: ModalDirective;

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";

    constructor(injector: Injector) {
        super(injector);
        this.primengTableHelper = new PrimengTableHelper();
    }

    ngOnInit() {
        if (!this.selectedValue)
            this.selectedValue = this.selectionMode === "multiple" ? [] : {};
        if(this.dataResult){
        this.dataResult.subscribe(result => {
            // deal with asynchronous Observable result
            if(result != null){
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.totalRecordsCount = result.totalCount;
            } else {
                this.primengTableHelper.records = [];
                this.primengTableHelper.totalRecordsCount = 0;
            }
            this.primengTableHelper.hideLoadingIndicator()
        });
        }
    }

    show(seletecdItem?): void {
        this.primengTableHelper.records = [];
        if (seletecdItem) {
            this.selectedValue = seletecdItem;
        }
        this.modal.show();
    }

    shown(): void {
        this.getData();
    }

    getData(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        if (this.loadDataResult) {
            this.loadDataResult.emit({
                filterText: this.filterText,
                sorting: this.primengTableHelper.getSorting(this.dataTable),
                maxResultCount: this.primengTableHelper.getMaxResultCount(this.paginator, event),
                skipCount: this.primengTableHelper.getSkipCount(this.paginator, event)
            });
        }
        if (this.loadData) {
            this.loadData(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
                .pipe(
                    finalize(() => this.primengTableHelper.hideLoadingIndicator())
                )
                .subscribe((result) => {
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                });
        }
    }

    save() {
        this.changeValue.emit(this.selectedValue);
        this.selectedValue =
            this.rememberSelect === true ? this.selectedValue : null;
        this.close();
    }

    close() {
        this.modal.hide();
    }
}

export class SingleLocationDto {
    locationId: number;
    locationName: string;
}

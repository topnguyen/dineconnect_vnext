import { Component, OnInit, Injector } from "@angular/core";
import { DasboardService } from "@app/main/dashboard/dasboard.service";
import { GetDashboardChartModel } from "@app/shared/models/GetDashboardChartModel";
import { GetTopStatsOutput, SimpleLocationDto, TenantDashboardServiceProxy } from "@shared/service-proxies/service-proxies";
import { WidgetComponentBase } from "../widget-component-base";
import *  as moment from 'moment';
@Component({
    selector: "app-widget-top-stats",
    templateUrl: "./widget-top-stats.component.html",
    styleUrls: ["./widget-top-stats.component.scss"],
})
export class WidgetTopStatsComponent
    extends WidgetComponentBase
    implements OnInit {
   
    dashboardTopStats: GetTopStatsOutput = new GetTopStatsOutput();
    dataRange: Date[] = [new Date(), new Date()];
    filterParam: GetDashboardChartModel;
    locationId: number;
    location: SimpleLocationDto[];
    constructor(
        injector: Injector,
        private _dashboardService: DasboardService,
        private _tenantDashboardService: TenantDashboardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.eventListenLocationChange();
        this.eventListenDateRangeChange();
    }

    eventListenLocationChange() {
        this._dashboardService.castDateRange.subscribe((data) => {
            this.dataRange = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getData();
        });
    }

    eventListenDateRangeChange() {
        this._dashboardService.castLocation.subscribe((data) => {
            this.location = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getData();
        });
    }

    getData() {
        this._tenantDashboardService.getTopStats(
            this.locationId,
            this.filterParam.userId,
            this.filterParam.startDate,
            this.filterParam.endDate,
            this.filterParam.locations,
            this.filterParam.locationGroup_Locations,
            this.filterParam.locationGroup_Groups,
            this.filterParam.locationGroup_LocationTags,
            this.filterParam.locationGroup_NonLocations,
            this.filterParam.locationGroup_Group,
            this.filterParam.locationGroup_LocationTag,
            this.filterParam.locationGroup_UserId,
            this.filterParam.notCorrectDate,
            this.filterParam.credit,
            this.filterParam.refund,
            this.filterParam.ticketNo
        ).subscribe(result => {
            this.dashboardTopStats = result;
            console.log('data', this.dashboardTopStats )
        })
    }

    getAll():void {
        
    }
}

import { Injector } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { DasboardService } from "@app/main/dashboard/dasboard.service";
import { GetDashboardChartModel } from "@app/shared/models/GetDashboardChartModel";
import {
    SimpleLocationDto,
    WheelDashboardsAppserviceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { DashboardChartBase } from "../dashboard-chart-base";
import { WidgetComponentBase } from "../widget-component-base";
import * as moment from "moment";
class DashboardTopStats extends DashboardChartBase {
    totalOrders = 0;
    totalOrdersCounter = 0;
    totalSales = 0;
    totalSalesCounter = 0;
    totalOrdersChange = 76;
    totalOrdersChangeCounter = 0;
    totalSalesChange = 85;
    totalSalesChangeCounter = 0;

    init(totalOrders, totalSales) {
        this.totalOrders = totalOrders;
        this.totalSales = totalSales;
        this.hideLoading();
    }
}
@Component({
    selector: "app-widget-total-orders",
    templateUrl: "./widget-total-orders.component.html",
    styleUrls: ["./widget-total-orders.component.scss"],
})
export class WidgetTotalOrdersComponent
    extends WidgetComponentBase
    implements OnInit
{
    dashboardTopStats: DashboardTopStats;
    dataRange: Date[] = [new Date(), new Date()];
    filterParam: GetDashboardChartModel;
    locationId: number;
    location: SimpleLocationDto[];
    constructor(
        injector: Injector,
        private _dashboardService: DasboardService,
        private _wheelDashboardServiceProxy: WheelDashboardsAppserviceServiceProxy
    ) {
        super(injector);
        this.dashboardTopStats = new DashboardTopStats();
    }
    ngOnInit() {
       
        this.eventListenLocationChange();
        this.eventListenDateRangeChange();
    }

    eventListenLocationChange() {
        this._dashboardService.castDateRange.subscribe((data) => {
            this.dataRange = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getTicketDetails();
        });
    }

    eventListenDateRangeChange() {
        this._dashboardService.castLocation.subscribe((data) => {
            this.location = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getTicketDetails();
        });
    }

    getTicketDetails() {
        this._wheelDashboardServiceProxy
            .getWheelOrderDashboard(
                this.locationId,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                if (result != null) {
                    this.dashboardTopStats.init(
                        result.totalOrders,
                        result.totalsales
                    );
                }
            });
    }
}

import { Component, Injector, OnInit } from "@angular/core";
import { DasboardService } from "@app/main/dashboard/dasboard.service";
import { GetDashboardChartModel } from "@app/shared/models/GetDashboardChartModel";
import {
    SimpleLocationDto,
    WheelDashboardsAppserviceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { WidgetComponentBase } from "../widget-component-base";

@Component({
    selector: "app-widget-sale-by-hours",
    templateUrl: "./widget-sale-by-hours.component.html",
    styleUrls: ["./widget-sale-by-hours.component.css"],
})
export class WidgetSaleByHoursComponent
    extends WidgetComponentBase
    implements OnInit
{
    data: any;
    dataRange: Date[] = [new Date(), new Date()];
    filterParam: GetDashboardChartModel;
    locationId: number;
    location: SimpleLocationDto[];
    constructor(
        injector: Injector,
        private _dashboardService: DasboardService,
        private _wheelDashboardServiceProxy: WheelDashboardsAppserviceServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.eventListenLocationChange();

        this.eventListenDateRangeChange();
    }

    eventListenLocationChange() {
        this._dashboardService.castDateRange.subscribe((data) => {
            this.dataRange = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getSaleByHours();
        });
    }

    eventListenDateRangeChange() {
        this._dashboardService.castLocation.subscribe((data) => {
            this.location = data;
            this.filterParam = {
                startDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[0])
                        : undefined,
                endDate:
                    this.dataRange?.length > 0
                        ? moment(this.dataRange[1])
                        : undefined,
                locations: this.location,
            } as GetDashboardChartModel;
            this.getSaleByHours();
        });
    }

    getSaleByHours() {
        this._wheelDashboardServiceProxy
            .getSaleByHoursDashboard(
                this.locationId,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                if (result != null) {
                    this.data = result;
                }
            });
    }
}

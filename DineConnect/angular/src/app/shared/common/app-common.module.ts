import { CommonModule } from "@angular/common";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppLocalizationService } from "@app/shared/common/localization/app-localization.service";
import { AppNavigationService } from "@app/shared/layout/nav/app-navigation.service";
import { DineConnectCommonModule } from "@shared/common/common.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { PaginatorModule } from "primeng/paginator";
import { TableModule } from "primeng/table";
import { AppAuthService } from "./auth/app-auth.service";
import { AppRouteGuard } from "./auth/auth-route-guard";
import { CommonLookupModalComponent } from "./lookup/common-lookup-modal.component";
import { EntityTypeHistoryModalComponent } from "./entityHistory/entity-type-history-modal.component";
import { EntityChangeDetailModalComponent } from "./entityHistory/entity-change-detail-modal.component";
import { DateRangePickerInitialValueSetterDirective } from "./timing/date-range-picker-initial-value.directive";
import { DatePickerInitialValueSetterDirective } from "./timing/date-picker-initial-value.directive";
import { DateTimeService } from "./timing/date-time.service";
import { TimeZoneComboComponent } from "./timing/timezone-combo.component";
import { CustomizableDashboardComponent } from "./customizable-dashboard/customizable-dashboard.component";
import { WidgetGeneralStatsComponent } from "./customizable-dashboard/widgets/widget-general-stats/widget-general-stats.component";
import { DashboardViewConfigurationService } from "./customizable-dashboard/dashboard-view-configuration.service";
import { GridsterModule } from "angular-gridster2";
import { WidgetDailySalesComponent } from "./customizable-dashboard/widgets/widget-daily-sales/widget-daily-sales.component";
import { WidgetEditionStatisticsComponent } from "./customizable-dashboard/widgets/widget-edition-statistics/widget-edition-statistics.component";
import { WidgetHostTopStatsComponent } from "./customizable-dashboard/widgets/widget-host-top-stats/widget-host-top-stats.component";
import { WidgetIncomeStatisticsComponent } from "./customizable-dashboard/widgets/widget-income-statistics/widget-income-statistics.component";
import { WidgetMemberActivityComponent } from "./customizable-dashboard/widgets/widget-member-activity/widget-member-activity.component";
import { WidgetProfitShareComponent } from "./customizable-dashboard/widgets/widget-profit-share/widget-profit-share.component";
import { WidgetRecentTenantsComponent } from "./customizable-dashboard/widgets/widget-recent-tenants/widget-recent-tenants.component";
import { WidgetRegionalStatsComponent } from "./customizable-dashboard/widgets/widget-regional-stats/widget-regional-stats.component";
import { WidgetSalesSummaryComponent } from "./customizable-dashboard/widgets/widget-sales-summary/widget-sales-summary.component";
import { WidgetSubscriptionExpiringTenantsComponent } from "./customizable-dashboard/widgets/widget-subscription-expiring-tenants/widget-subscription-expiring-tenants.component";
import { WidgetTopStatsComponent } from "./customizable-dashboard/widgets/widget-top-stats/widget-top-stats.component";
import { FilterDateRangePickerComponent } from "./customizable-dashboard/filters/filter-date-range-picker/filter-date-range-picker.component";
import { AddWidgetModalComponent } from "./customizable-dashboard/add-widget-modal/add-widget-modal.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { CountoModule } from "angular2-counto";
import { SelectLocationComponent } from "./select-location/select-location.component";
import { PermissionTreeComponent } from "@app/admin/shared/permission-tree.component";
import { TreeModule } from "primeng/tree";
import { SelectSingleLocationComponent } from "./select-single-location/select-single-location.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
} from "ngx-bootstrap/datepicker";
import { CommonMultipleLookupModalComponent } from "./lookup/common-multiple-lookup-modal.component";
import { EditorModule as TinymceModule } from "@tinymce/tinymce-angular";
import { EditorTinyMceComponent } from "./editor-tiny-mce/editor-tiny-mce.component";
import { ToggleComponent } from "./toggle/toggle.component";
import { DropzoneComponent } from "./dropzone/dropzone.component";
import { SelectPopupComponent } from "./select-popup/select-popup.component";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import {
    AutoCompleteModule,
    ListboxModule,
    MultiSelectModule,
    SidebarModule,
    SliderModule,
} from "primeng";
import { SingleLineStringInputTypeComponent } from "./input-types/single-line-string-input-type/single-line-string-input-type.component";
import { ComboboxInputTypeComponent } from "./input-types/combobox-input-type/combobox-input-type.component";
import { MultipleSelectComboboxInputTypeComponent } from "./input-types/multiple-select-combobox-input-type/multiple-select-combobox-input-type.component";
import { PasswordInputWithShowButtonComponent } from "./password-input-with-show-button/password-input-with-show-button.component";
import { KeyValueListManagerComponent } from "./key-value-list-manager/key-value-list-manager.component";
import { SubHeaderComponent } from "./sub-header/sub-header.component";
import { CheckboxInputTypeComponent } from "./input-types/checkbox-input-type/checkbox-input-type.component";
import { SelectLocationOrGroupComponent } from "./select-location-or-group/select-location-or-group.component";
import { NgxTagsInputModule } from "ngx-tags-input";
import { ChipsModule } from "primeng/chips";
import { TagInputModule } from "ngx-chips";
import { DragulaModule } from "ng2-dragula";
import { SortModalComponent } from "./sort-modal/sort-modal.component";
import { DynamicTableComponent } from "./dynamic-table/dynamic-table.component";
import { WidgetTotalOrdersComponent } from "./customizable-dashboard/widgets/widget-total-orders/widget-total-orders.component";
import { WidgetSaleByHoursComponent } from "./customizable-dashboard/widgets/widget-sale-by-hours/widget-sale-by-hours.component";
import { ChartModule } from "primeng/chart";
import { SelectItemPopupComponent } from "./select-item-popup/select-item-popup.component";
import { WidgetTransactionSummaryComponent } from "./customizable-dashboard/widgets/widget-transaction-summary/widget-transaction-summary.component";
import { WidgetTopTenItemsComponent } from "./customizable-dashboard/widgets/widget-top-ten-items/widget-top-ten-items.component";
import { WidgetDepartmentSummaryComponent } from "./customizable-dashboard/widgets/widget-department-summary/widget-department-summary.component";
import { WidgetPaymentSummaryComponent } from "./customizable-dashboard/widgets/widget-payment-summary/widget-payment-summary.component";
import { ImageCropperComponent } from './image-cropper/image-cropper.component';
import { ImageCropperModule } from "ngx-image-cropper";
import { ImageCropperConnectComponent } from "./image-cropper-connect/image-cropper-connect.component";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        UtilsModule,
        DineConnectCommonModule,
        TableModule,
        PaginatorModule,
        ChipsModule,
        GridsterModule,
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        PerfectScrollbarModule,
        CountoModule,
        TreeModule,
        TinymceModule,
        AppBsModalModule,
        AutoCompleteModule,
        NgxTagsInputModule,
        TagInputModule,
        DragulaModule.forRoot(),
        SidebarModule,
        ListboxModule,
        MultiSelectModule,
        ChartModule,
        ImageCropperModule,
        SliderModule
    ],
    declarations: [
        TimeZoneComboComponent,
        CommonLookupModalComponent,
        CommonMultipleLookupModalComponent,
        EntityTypeHistoryModalComponent,
        EntityChangeDetailModalComponent,
        DateRangePickerInitialValueSetterDirective,
        DatePickerInitialValueSetterDirective,
        CustomizableDashboardComponent,
        WidgetGeneralStatsComponent,
        WidgetDailySalesComponent,
        WidgetEditionStatisticsComponent,
        WidgetHostTopStatsComponent,
        WidgetIncomeStatisticsComponent,
        WidgetMemberActivityComponent,
        WidgetProfitShareComponent,
        WidgetRecentTenantsComponent,
        WidgetRegionalStatsComponent,
        WidgetSalesSummaryComponent,
        WidgetSubscriptionExpiringTenantsComponent,
        WidgetTopStatsComponent,
        FilterDateRangePickerComponent,
        AddWidgetModalComponent,
        SelectLocationComponent,
        SelectLocationOrGroupComponent,
        PermissionTreeComponent,
        SelectSingleLocationComponent,
        EditorTinyMceComponent,
        SingleLineStringInputTypeComponent,
        ComboboxInputTypeComponent,
        CheckboxInputTypeComponent,
        MultipleSelectComboboxInputTypeComponent,
        PasswordInputWithShowButtonComponent,
        KeyValueListManagerComponent,
        SubHeaderComponent,
        ToggleComponent,
        DropzoneComponent,
        SelectPopupComponent,
        SortModalComponent,
        DynamicTableComponent,
        WidgetTotalOrdersComponent,
        WidgetSaleByHoursComponent,
        SelectItemPopupComponent,
        WidgetTransactionSummaryComponent,
        WidgetPaymentSummaryComponent,
        WidgetDepartmentSummaryComponent,
        WidgetTopTenItemsComponent,
        ImageCropperComponent,
        ImageCropperConnectComponent,
    ],
    exports: [
        TimeZoneComboComponent,
        CommonLookupModalComponent,
        CommonMultipleLookupModalComponent,
        EntityTypeHistoryModalComponent,
        EntityChangeDetailModalComponent,
        DateRangePickerInitialValueSetterDirective,
        DatePickerInitialValueSetterDirective,
        CustomizableDashboardComponent,
        SelectLocationComponent,
        SelectLocationOrGroupComponent,
        PermissionTreeComponent,
        NgxChartsModule,
        SelectSingleLocationComponent,
        EditorTinyMceComponent,
        PasswordInputWithShowButtonComponent,
        KeyValueListManagerComponent,
        SubHeaderComponent,
        ToggleComponent,
        DropzoneComponent,
        SelectPopupComponent,
        SortModalComponent,
        DynamicTableComponent,
        SelectItemPopupComponent,
        ImageCropperComponent,
        ImageCropperConnectComponent
    ],
    providers: [
        DateTimeService,
        AppLocalizationService,
        AppNavigationService,
        DashboardViewConfigurationService,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
    ],

    entryComponents: [
        WidgetGeneralStatsComponent,
        WidgetDailySalesComponent,
        WidgetEditionStatisticsComponent,
        WidgetHostTopStatsComponent,
        WidgetIncomeStatisticsComponent,
        WidgetMemberActivityComponent,
        WidgetProfitShareComponent,
        WidgetRecentTenantsComponent,
        WidgetRegionalStatsComponent,
        WidgetSalesSummaryComponent,
        WidgetSubscriptionExpiringTenantsComponent,
        WidgetTopStatsComponent,
        FilterDateRangePickerComponent,
        SingleLineStringInputTypeComponent,
        ComboboxInputTypeComponent,
        CheckboxInputTypeComponent,
        MultipleSelectComboboxInputTypeComponent,
        WidgetTransactionSummaryComponent,
        WidgetPaymentSummaryComponent,
        WidgetDepartmentSummaryComponent,
        WidgetTopTenItemsComponent,
    ],
})
export class AppCommonModule {
    static forRoot(): ModuleWithProviders<AppCommonModule> {
        return {
            ngModule: AppCommonModule,
            providers: [AppAuthService, AppRouteGuard],
        };
    }
}

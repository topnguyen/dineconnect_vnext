import { AppComponentBase } from "@shared/common/app-component-base";
import { OnInit, Injector, Component, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
    selector: 'toggle',
    templateUrl: './toggle.componet.html',
    styleUrls: ['./toggle.component.scss'],
    providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => ToggleComponent),
          multi: true
        }
      ]
  })
export class ToggleComponent implements ControlValueAccessor {
    
    private _value: any = '';
    onChange: any

    get value(): any { return this._value; };
  
    set value(v: any) {
      if(v !== undefined && this._value !== v){
        this._value = v;
        }
    }


    writeValue(obj: any): void {
        this.value = obj;
    }   
    
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    changeValue(value) {
      this.value = value;
      this.onChange(value);
    }
  }
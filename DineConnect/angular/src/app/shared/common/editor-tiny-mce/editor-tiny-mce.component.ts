import { AppComponentBase } from "@shared/common/app-component-base";
import { Component, forwardRef, Injector, Input, OnInit } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { LocalStorageService } from "@shared/utils/local-storage.service";
import { FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { AppConsts } from "@shared/AppConsts";
import { TokenService, IAjaxResponse } from "abp-ng2-module";

@Component({
  selector: 'editor-tiny-mce',
  template: `<editor [(ngModel)]="value" id="" apiKey="k17wqq8mqonza5236t8elob9rf1qb0f88kh7moysordtf1b4" [init]="tinymceInit"></editor>`,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EditorTinyMceComponent),
      multi: true
    }
  ]
})
export class EditorTinyMceComponent extends AppComponentBase implements OnInit, ControlValueAccessor {
  id: string;

  private uploader: FileUploader;

  private _value: any = '';
  tinymceInit: any;

  get value(): any { return this._value; };

  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }

  @Input('init') init: any;

  onChange(value: any) {

  };

  constructor(
    injector: Injector,
    private _tokenService: TokenService,
    private _localStorageService: LocalStorageService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    
    this.id = "tcme_" + Date.now.toString();

    this.tinymceInit = {
      ...this.init,
      file_picker_callback: (cb, value, meta) => {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        this.initUploader();

        this.uploader.onSuccessItem = (item, response, status) => {
          const ajaxResponse = <IAjaxResponse>JSON.parse(response);
          if (ajaxResponse.success) {
            // call the callback and populate the Title field with the file name
            cb(ajaxResponse.result.url, { title: ajaxResponse.result.fileName });
          } else {
            this.notify.error(ajaxResponse.error.message);
          }
        };

        input.onchange = (res: any) => {
          var file: File = res.currentTarget.files[0];
          this.uploader.addToQueue([file]);
        };

        input.click();
      }
    }
  }

  initUploader(): void {
    this.uploader = new FileUploader({
      url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
      authToken: 'Bearer ' + this._tokenService.getToken(),
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    const uploaderOptions: FileUploaderOptions = {};
    uploaderOptions.removeAfterUpload = true;
    uploaderOptions.autoUpload = true;
    this.uploader.setOptions(uploaderOptions);
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }
}
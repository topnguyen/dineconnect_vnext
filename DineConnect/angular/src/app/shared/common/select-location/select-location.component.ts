import { Component, Injector, Output, EventEmitter, ViewEncapsulation, ViewChild, OnInit, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SimpleLocationDto, LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'select-location-modal',
    templateUrl: './select-location.component.html',
    styleUrls: ['./select-location.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SelectLocationComponent extends AppComponentBase implements OnInit {
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
    @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

    @Input() multiple = true;

    @Output() locationAdded: EventEmitter<any> = new EventEmitter<any>();

    selectedLocations: any[];
    filterText = '';
    locationNotInGroupTableHelper: PrimengTableHelper;

    constructor(
        injector: Injector, 
        private _locationServiceProxy: LocationServiceProxy
    ) {
        super(injector);
        this.locationNotInGroupTableHelper = new PrimengTableHelper();
    }

    show(locations?): void {
        this.locationNotInGroupTableHelper.records = [];
        this.selectedLocations = _.cloneDeep(locations);

        this.modal.show();
    }

    ngOnInit() {}

    shown(): void {
        this.getLocations();
    }

    getLocations(event?: LazyLoadEvent) {
        if (this.locationNotInGroupTableHelper.shouldResetPaging(event)) {
            this.locationPaginator.changePage(0);
            return;
        }
        this.locationNotInGroupTableHelper.showLoadingIndicator();

        this._locationServiceProxy
            .getSimpleLocations(
                this.filterText,
                false,
                0,
                this.locationNotInGroupTableHelper.getSorting(this.locationDataTable),
                this.locationNotInGroupTableHelper.getMaxResultCount(this.locationPaginator, event),
                this.locationNotInGroupTableHelper.getSkipCount(this.locationPaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.locationNotInGroupTableHelper.records = result.items;
                this.locationNotInGroupTableHelper.totalRecordsCount = result.totalCount;
                this.locationNotInGroupTableHelper.hideLoadingIndicator();
            });
    }

    close() {
        this.modal.hide();
    }

    save() {
        this.locationAdded.emit(this.selectedLocations);
        this.close();
    }
}

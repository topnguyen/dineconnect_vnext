import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { cloneDeep, merge } from 'lodash';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { DTableColumn } from './dynamic-table.model';

export interface IDynamicTableOptions {
    dataSource: (
        sorting: string,
        skipCount: number,
        maxResultCount: number
    ) => Observable<{
        totalCount: number;
        items: any[];
    }>;
    edit?: (item) => void;
    delete?: (item) => void;
    isGrantedEdit?: string;
    isGrantedDelete?: string;
    loadOnStartup?: boolean;
    pageSize?: number;
    cols: DTableColumn[];
    stateKey: string;
}

@Component({
    selector: 'app-dynamic-table',
    templateUrl: './dynamic-table.component.html',
    styleUrls: ['./dynamic-table.component.scss']
})
export class DynamicTableComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    static defaultOptions: IDynamicTableOptions = {
        dataSource: undefined,
        pageSize: AppConsts.grid.defaultPageSize,
        loadOnStartup: true,
        cols: [],
        stateKey: ''
    };

    options: IDynamicTableOptions = merge({});
    display = false;
    selectedCol: DTableColumn[] = [];
    isShown = false;
    isShowPreferences = true;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit() {}

    ngAfterViewInit() {}

    configure(options: IDynamicTableOptions): void {
        this.options = merge({}, DynamicTableComponent.defaultOptions, { title: this.l('SelectAnItem') }, options);
        this.selectedCol = cloneDeep(this.options.cols);
        this.isShown = true;
        this.getAll(null);
        this._updateSelectedCol();
    }

    getAll(event?: LazyLoadEvent): void {
        if (!this.isShown) {
            return;
        }

        if (!this.options.loadOnStartup) {
            return;
        }

        const sorting = this.primengTableHelper.getSorting(this.dataTable);
        const maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        const skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);   

            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this.options
            .dataSource(sorting, skipCount, maxResultCount)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    edit(item) {
        this.options.edit(item);
    }

    delete(item) {
        this.options.delete(item);
    }

    onPreferencesChange(event) {
        this._updateTableState();
    }

    onColReorder(event) {
        console.log(event);
    }

    private _updateSelectedCol() {
        const stateStr = localStorage.getItem(this.options.stateKey);
        let state = {};

        if (stateStr && typeof stateStr == 'string') {
            state = JSON.parse(stateStr);
            this.selectedCol = this.options.cols.filter(col => state['columnOrder'].includes(col.field))
        }
    }

    private _updateTableState() {
        const stateStr = localStorage.getItem(this.options.stateKey);
        let state = {};

        if (stateStr && typeof stateStr == 'string') {
            state = JSON.parse(stateStr);

            state['columnOrder'] = this.selectedCol.map(item => item.field);
            localStorage.setItem(this.options.stateKey, JSON.stringify(state));
        }
    }
}

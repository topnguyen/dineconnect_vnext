export interface DTableColumn {
    field: string;
    header: string;
    type: 'date' | 'number' | 'string' | 'action' | 'money'
}
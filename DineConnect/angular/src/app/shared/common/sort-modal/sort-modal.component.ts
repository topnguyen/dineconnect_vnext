import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';

import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-sort-modal',
    templateUrl: './sort-modal.component.html',
    styleUrls: ['./sort-modal.component.scss']
})
export class SortModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('sortModal', { static: true }) sortModal: ModalDirective;

    @Input() optionLabel = 'name';
    @Input() title: string;
    @Input() dragulaStr: string = "sortModal";
    @Output() hideModal = new EventEmitter<any>();

    saving = false;
    options: any[] = [];

    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit(): void {}

    show(reload$: Observable<any>) {
        reload$.subscribe(result  => {
            this.options =  result.items || [];
        })
        this.sortModal.show();
    }

    close() {
        this.sortModal.hide();
    }

    save() {
        this.hideModal.emit(this.options)
        this.sortModal.hide();
    }
}

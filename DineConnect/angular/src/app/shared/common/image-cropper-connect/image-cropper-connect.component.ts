import { Component, ElementRef, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { base64ToFile, ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-cropper-connect',
  templateUrl: './image-cropper-connect.component.html',
  styleUrls: ['./image-cropper-connect.component.scss']
})
export class ImageCropperConnectComponent extends AppComponentBase implements OnInit {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @Input() ngModel: string;
  @Output() ngModelChange = new EventEmitter<any>();
  @ViewChild('fileInput') fileInput: ElementRef;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  constructor(
    injector: Injector) {
    super(injector)
  }

  ngOnInit(): void { }
  show() {
    this.modal.show();
  }
  save() {
    this.ngModelChange.emit(<File>base64ToFile(this.croppedImage));
    this.close();
  }
  close() {
    this.fileInput.nativeElement.value = "";
    this.modal.hide();
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.show();
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() { }
  cropperReady() { }
  loadImageFailed() { }
}

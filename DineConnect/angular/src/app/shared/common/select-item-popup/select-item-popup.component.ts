import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewChild,
    OnInit,
    Input,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    MenuItemServiceProxy,
    IdNameDto,
} from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import { PrimengTableHelper } from "shared/helpers/PrimengTableHelper";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    selector: "app-select-item-popup",
    templateUrl: "./select-item-popup.component.html",
    styleUrls: ["./select-item-popup.component.css"],
})
export class SelectItemPopupComponent
    extends AppComponentBase
    implements OnInit {
    @Input() backDropZIndex?: number;
    @Output() itemSelected: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild("modal", { static: true }) modal: ModalDirective;

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    categoryId = undefined;
    selectedMenuItem: IdNameDto;

    constructor(
        injector: Injector,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
        this.primengTableHelper = new PrimengTableHelper();
    }

    show(categoryId?, menuItem?): void {
        this.primengTableHelper.records = [];
        if (menuItem) {
        }
        this.categoryId = categoryId;
        if (this.categoryId === null)
            this.categoryId = 0;
        this.getMenuItems();
        this.modal.show();
    }

    ngOnInit() { }

    shown(): void { }

    getMenuItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._menuItemServiceProxy
            .getAll(
                this.filterText,
                false,
                this.categoryId,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    menuItem(record) {
        this.selectedMenuItem = undefined;
        this.itemSelected.emit(record);
        this.close();
    }

    close() {
        this.modal.hide();
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectItemPopupComponent } from './select-item-popup.component';

describe('SelectItemPopupComponent', () => {
  let component: SelectItemPopupComponent;
  let fixture: ComponentFixture<SelectItemPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectItemPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectItemPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injector, Component, OnInit, Inject, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import { UrlHelper } from '@shared/helpers/UrlHelper';
import { DOCUMENT } from '@angular/common';
import { OffcanvasOptions } from '@metronic/app/core/_base/layout/directives/offcanvas.directive';
import { AppConsts } from '@shared/AppConsts';
import { ToggleOptions } from '@metronic/app/core/_base/layout/directives/toggle.directive';
import { UserDefaultLocationDto, DefaultLocationServiceProxy } from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng/public_api';
import { finalize } from 'rxjs/operators';
import { DefaultLocationComponent } from '../default-locations/default-location.component';

@Component({
    templateUrl: './select-location-top-bar.component.html',
    selector: 'select-location-top-bar',
    animations: [appModuleAnimation()]
})
export class SelectLocationComponent extends ThemesLayoutBaseComponent implements OnInit {

    @ViewChild('defaultLocationModal', { static: true }) defaultLocationModal: DefaultLocationComponent;
    userDefaultOrganizationListDto: UserDefaultLocationDto;
    defaultLocationSelected = 0;

    constructor(
        injector: Injector,
        private _defaultLocationServiceProxy: DefaultLocationServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getUserInfo();
    }

    getUserInfo(event?: LazyLoadEvent) {
        this._defaultLocationServiceProxy
            .getUserDefaultLocation()
            .pipe(finalize(() => { }))
            .subscribe(result => {
                this.userDefaultOrganizationListDto = result;
                if (result.defaultLocationId > 0) {
                    this.defaultLocationSelected = result.defaultLocationId;
                }
            });
    }

    setDefaultLocation() {
        this.defaultLocationModal.show();
    }
}

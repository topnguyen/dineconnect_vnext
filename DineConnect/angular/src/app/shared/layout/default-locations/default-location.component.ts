import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    Output,
    EventEmitter
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import {
    BrandDto,
    CategoryDto,
    UserDefaultLocationDto,
    LocationDto,
    AddressesServiceProxy,
    DefaultLocationServiceProxy
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'defaultLocation',
    templateUrl: './default-location.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DefaultLocationComponent extends AppComponentBase {
    @Output()
    defaultLocationUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('modal', { static: true }) modal: ModalDirective; //Use clear name for selector and components!
    @ViewChild('locationBasedOnUserTable', { static: false }) locationBasedOnUserTable: Table; //Use clear name for selector and components!
    @ViewChild('locationBasedOnUserPaginator', { static: false }) locationBasedOnUserPaginator: Paginator; //Use clear name for selector and components!

    userDefaultOrganizationListDto: UserDefaultLocationDto;

    companyList: BrandDto[] = [];
    locationListDropDown = [];

    stateLoaded = false;
    cityLoaded = false;
    countryListDropDown = [];
    cityListDropDown = [];
    stateListDropDown = [];
    companyListDropDown = [];
    defaultLocationSelected = 0;

    companySelected = 0;
    countrySelected = 0;
    stateSelected = 0;
    citySelected = 0;

    categoriesEditDto: CategoryDto;
    filterText = '';
    tableIsShown = false;

    constructor(
        injector: Injector,
        private _defaultLocationServiceProxy: DefaultLocationServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _addressesServiceProxy: AddressesServiceProxy
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams['filterText'] || '';
        this.categoriesEditDto = new CategoryDto();
    }

    show(): void {
        this.getCompanyList();
        this.getUserInfo();
    }

    close(): void {
        this.modal.hide();
    }

    shown(): void {
        this.tableIsShown = true;
        this.getLocationBasedOnUser();
    }

    refresh(): void {
        this.stateSelected = 0;
        this.citySelected = 0;
        this.countrySelected = 0;
        this.companySelected = 0;
        this.filterText = undefined;

        this.getLocationBasedOnUser();
    }

    getUserInfo(event?: LazyLoadEvent) {
        this.getCountryList();
        this.getCompanyList();
        this._defaultLocationServiceProxy
            .getUserDefaultLocation()
            .subscribe(result => {
                this.userDefaultOrganizationListDto = result;
                this.defaultLocationSelected = result.defaultLocationId;
                this.getCityList();
                this.getStateList();
                this.modal.show();
            });
    }

    appendDefaultUserOrganization(location: LocationDto) {
        this._defaultLocationServiceProxy
            .setDefaultLocation(location.id)
            .pipe(finalize(() => { }))
            .subscribe(result => {
                this.modal.hide();
                this.defaultLocationUpdated.emit(true);
            });
    }

    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe(result => {
                this.countryListDropDown = [];
                result.items.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.countryListDropDown.push(a);
                });
                this.countryListDropDown.push({
                    label: 'None',
                    value: 0
                });
            });
    }

    getStateList() {
        this._addressesServiceProxy
            .getStateListByCountry(
                this.countrySelected
            )
            .subscribe(result => {
                this.stateListDropDown = [];
                result.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDown.push(a);
                });
                this.stateListDropDown.push({
                    label: 'None',
                    value: 0
                });
                this.stateLoaded = true;
                this.getCityList();
            });
    }

    getCityList() {
        this._addressesServiceProxy
            .getCityList(
                undefined,
                this.countrySelected,
                this.stateSelected,
                false,
                undefined,
                1000,
                0
            )
            .subscribe(result => {
                this.cityListDropDown = [];
                result.items.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.cityListDropDown.push(a);
                });
                this.cityListDropDown.push({
                    label: 'None',
                    value: 0
                });
                this.cityLoaded = true;
            });
    }

    getLocationBasedOnUser(event?: LazyLoadEvent) {
        if (this.tableIsShown) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this.locationBasedOnUserPaginator.changePage(0);
                return;
            }
            this.primengTableHelper.showLoadingIndicator();
            this._defaultLocationServiceProxy
                .getLocationBasedOnUser(
                    this.filterText,
                    this.stateSelected,
                    this.countrySelected,
                    this.citySelected,
                    this.companySelected,
                    this.primengTableHelper.getSorting(
                        this.locationBasedOnUserTable
                    ),
                    this.primengTableHelper.getMaxResultCount(
                        this.locationBasedOnUserPaginator,
                        event
                    ),
                    this.primengTableHelper.getSkipCount(
                        this.locationBasedOnUserPaginator,
                        event
                    )
                )
                .pipe(finalize(() => { this.primengTableHelper.hideLoadingIndicator(); }))
                .subscribe(result => {
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                });
        }
    }

    getCompanyList() {
        this._defaultLocationServiceProxy
            .getBrandBasedOnUser()
            .subscribe(result => {
                this.companyListDropDown = [];
                result.forEach(element => {
                    let a = { label: element.name, value: element.id };
                    this.companyListDropDown.push(a);
                });
                this.companyListDropDown.push({
                    label: 'None',
                    value: 0
                });
            });
    }

    setCountry() {
        this.getStateList();
    }
    setState() {
        this.getCityList();
    }
}

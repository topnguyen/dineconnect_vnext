﻿import { AppSessionService } from "@shared/common/session/app-session.service";

import { Injectable } from "@angular/core";
import { AppMenu } from "./app-menu";
import { AppMenuItem } from "./app-menu-item";
import {
    PermissionCheckerService,
    FeatureCheckerService,
} from "abp-ng2-module";

@Injectable()
export class AppNavigationService {
    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService,
        private _featureSessionService: FeatureCheckerService
    ) { }

    getMenu(): AppMenu {
        return new AppMenu("MainMenu", "MainMenu", [
            new AppMenuItem(
                "Dashboard",
                "Pages.Administration.Host.Dashboard",
                "flaticon-line-graph",
                "/app/admin/hostDashboard"
            ),
            new AppMenuItem(
                "Dashboard",
                "Pages.Tenant.Dashboard",
                "flaticon-line-graph",
                "/app/main/dashboard"
            ),
            new AppMenuItem(
                "Tenants",
                "Pages.Tenants",
                "flaticon-list-3",
                "/app/admin/tenants"
            ),
            new AppMenuItem(
                "Editions",
                "Pages.Editions",
                "flaticon-app",
                "/app/admin/editions"
            ),
            //#region "Connect"
            new AppMenuItem(
                "Connect",
                "Pages.Tenant.Connect",
                "flaticon-squares-3",
                "",
                [],
                [
                    new AppMenuItem(
                        "Location",
                        "Pages.Tenant.Connect.Location",
                        "flaticon-placeholder-1",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Brand",
                                "Pages.Tenant.Connect.Location.Brands",
                                "flaticon2-send-1",
                                "/app/connect/master/organization"
                            ),
                            new AppMenuItem(
                                "Location",
                                "Pages.Tenant.Connect.Location.Locations",
                                "flaticon2-location",
                                "/app/connect/master/locations"
                            ),
                            new AppMenuItem(
                                "LocationGroup",
                                "Pages.Tenant.Connect.Location.LocationGroups",
                                "flaticon2-resize",
                                "/app/connect/master/locationGroups"
                            ),
                            new AppMenuItem(
                                "LocationTag",
                                "Pages.Tenant.Connect.Location.LocationTags",
                                "flaticon2-tag",
                                "/app/connect/master/locationTags"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Master",
                        "Pages.Tenant.Connect.Master",
                        "flaticon2-dashboard",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "PaymentType",
                                "Pages.Tenant.Connect.Master.PaymentTypes",
                                "flaticon2-telegram-logo",
                                "/app/connect/master/paymentTypes"
                            ),
                            new AppMenuItem(
                                "TransactionType",
                                "Pages.Tenant.Connect.Master.TransactionTypes",
                                "flaticon2-files-and-folders",
                                "/app/connect/master/transactionTypes"
                            ),
                            new AppMenuItem(
                                "Terminal",
                                "Pages.Tenant.Connect.Master.Terminals",
                                "flaticon2-ui",
                                "/app/connect/master/terminals"
                            ),
                            new AppMenuItem(
                                "DepartmentGroup",
                                "Pages.Tenant.Connect.Master.DepartmentGroup",
                                "flaticon2-drop",
                                "/app/connect/master/department-group"
                            ),
                            new AppMenuItem(
                                "Department",
                                "Pages.Tenant.Connect.Master.Departments",
                                "flaticon2-protected",
                                "/app/connect/master/departments"
                            ),
                            new AppMenuItem(
                                "Numerator",
                                "Pages.Tenant.Connect.Master.Numerator",
                                "flaticon2-wifi",
                                "/app/connect/master/numerators"
                            ),
                            new AppMenuItem(
                                "TicketType",
                                "Pages.Tenant.Connect.Master.TicketType",
                                "flaticon2-crisp-icons",
                                "/app/connect/master/ticket-types"
                            ),
                            new AppMenuItem(
                                "ForeignCurrency",
                                "Pages.Tenant.Connect.Master.ForeignCurrencies",
                                "flaticon2-cd",
                                "/app/connect/master/foreignCurrencies"
                            ),
                            new AppMenuItem(
                                "Reason",
                                "Pages.Tenant.Connect.Master.PlanReasons",
                                "flaticon2-help",
                                "/app/connect/master/planReasons"
                            ),
                            new AppMenuItem(
                                "TillAccount",
                                "Pages.Tenant.Connect.Master.TillAccounts",
                                "flaticon2-layers-1",
                                "/app/connect/master/tillAccounts"
                            ),
                            new AppMenuItem(
                                "Calculation",
                                "Pages.Tenant.Connect.Master.Calculations",
                                "flaticon2-list-2",
                                "/app/connect/master/calculations"
                            ),
                            new AppMenuItem(
                                "Tax",
                                "Pages.Tenant.Connect.Master.DinePlanTaxes",
                                "flaticon2-cube-1",
                                "/app/connect/master/dinePlanTaxes"
                            ),
                            new AppMenuItem(
                                "FutureData",
                                "Pages.Tenant.Connect.Master.FutureDateInformations",
                                "flaticon2-box-1",
                                "/app/connect/master/future-data"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Address",
                        "Pages.Tenant.Connect.Address",
                        "flaticon-placeholder-1",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Country",
                                "Pages.Tenant.Connect.Address.Countries",
                                "flaticon-globe",
                                "/app/admin/address/country"
                            ),
                            new AppMenuItem(
                                "State",
                                "Pages.Tenant.Connect.Address.States",
                                "flaticon-layers",
                                "/app/admin/address/state"
                            ),
                            new AppMenuItem(
                                "City",
                                "Pages.Tenant.Connect.Address.Cities",
                                "flaticon2-architecture-and-city",
                                "/app/admin/address/city"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Menu",
                        "Pages.Tenant.Connect.Menu",
                        "flaticon2-gear",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "ProductGroup",
                                "Pages.Tenant.Connect.Menu.ProductGroups",
                                "flaticon-more",
                                "/app/connect/menu/productGroups"
                            ),
                            new AppMenuItem(
                                "Category",
                                "Pages.Tenant.Connect.Menu.Categories",
                                "flaticon2-indent-dots",
                                "/app/connect/menu/category"
                            ),
                            new AppMenuItem(
                                "MenuItem",
                                "Pages.Tenant.Connect.Menu.MenuItems",
                                "flaticon-menu-button",
                                "/app/connect/menu/menuItem"
                            ),
                            new AppMenuItem(
                                "ScreenMenu",
                                "Pages.Tenant.Connect.Menu.ScreenMenus",
                                "flaticon2-list-2",
                                "/app/connect/menu/screen-menus"
                            ),
                            new AppMenuItem(
                                "ComboGroup",
                                "Pages.Tenant.Connect.Menu.ComboGroups",
                                "flaticon2-cube-1",
                                "/app/connect/menu/combo-groups"
                            ),
                            new AppMenuItem(
                                "ProductCombo",
                                "Pages.Tenant.Connect.Menu.ComboGroups",
                                "flaticon2-cube-1",
                                "/app/connect/menu/product-combo"
                            ),
                            new AppMenuItem(
                                "LocationPrice",
                                "Pages.Tenant.Connect.Menu.LocationPrices",
                                "flaticon2-open-text-book",
                                "/app/connect/menu/location-prices"
                            ),
                            new AppMenuItem(
                                "LocationMenu",
                                "Pages.Tenant.Connect.Menu.LocationMenus",
                                "flaticon2-setup",
                                "/app/connect/menu/location-menus"
                            ),
                        ]
                    ),

                    new AppMenuItem(
                        "Print",
                        "Pages.Tenant.Connect.Print",
                        "flaticon2-indent-dots",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Printer",
                                "Pages.Tenant.Connect.Print.Printers",
                                "flaticon2-open-text-book",
                                "/app/connect/print/printers"
                            ),
                            new AppMenuItem(
                                "Template",
                                "Pages.Tenant.Connect.Print.PrintTemplates",
                                "flaticon2-soft-icons-1",
                                "/app/connect/print/print-templates"
                            ),
                            new AppMenuItem(
                                "Job",
                                "Pages.Tenant.Connect.Print.PrintJobs",
                                "flaticon2-wifi",
                                "/app/connect/print/print-jobs"
                            ),
                            new AppMenuItem(
                                "Condition",
                                "Pages.Tenant.Connect.Print.PrintConditions",
                                "flaticon2-open-text-book",
                                "/app/connect/print/print-conditions"
                            ),
                            new AppMenuItem(
                                "Device",
                                "Pages.Tenant.Connect.Display.DineDevice",
                                "flaticon2-trash",
                                "/app/connect/print/print-devices"
                            ),
                        ]
                    ),

                    new AppMenuItem(
                        "ProgramSetting",
                        "Pages.Tenant.Connect.ProgramSetting",
                        "flaticon2-paper",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Templates",
                                "Pages.Tenant.Connect.ProgramSetting.Templates",
                                "flaticon2-paper",
                                "/app/connect/programSetting/templates"
                            ),
                        ]
                    ),

                    new AppMenuItem(
                        "Tag",
                        "Pages.Tenant.Connect.Tag",
                        "flaticon-interface-9",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "TicketTag",
                                "Pages.Tenant.Connect.Tag.TicketTagGroups",
                                "flaticon2-medical-records-1",
                                "/app/connect/tags/ticketTagGroups"
                            ),
                            new AppMenuItem(
                                "PriceTag",
                                "Pages.Tenant.Connect.Tag.PriceTags",
                                "flaticon-price-tag",
                                "/app/connect/tags/priceTags"
                            ),
                            new AppMenuItem(
                                "OrderTagGroup",
                                "Pages.Tenant.Connect.Tag.OrderTags",
                                "flaticon2-graph-1",
                                "/app/connect/tags/orderTagGroups"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Promotion",
                        "Pages.Tenant.Connect.Promotions",
                        "flaticon-list-3",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Category",
                                "Pages.Tenant.Connect.PromotionCategory",
                                "flaticon-list-3",
                                "/app/connect/discount/categorys"
                            ),
                            new AppMenuItem(
                                "Promotion",
                                "Pages.Tenant.Connect.Promotions",
                                "flaticon-infinity",
                                "/app/connect/discount/promotions"
                            ),

                        ]
                    ),

                    new AppMenuItem(
                        "User",
                        "Pages.Tenant.Connect.User",
                        "flaticon-user-add",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Roles",
                                "Pages.Tenant.Connect.User.DinePlanUserRole",
                                "flaticon-grid-menu",
                                "/app/connect/user/roles"
                            ),
                            new AppMenuItem(
                                "Users",
                                "Pages.Tenant.Connect.User.DinePlanUser",
                                "flaticon-user-add",
                                "/app/connect/user/users"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Card",
                        "Pages.Tenant.Connect.Card",
                        "flaticon2-menu-4",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Category",
                                "Pages.Tenant.Connect.Card.ConnectCardTypeCategory",
                                "flaticon2-pie-chart-4",
                                "/app/connect/card/categorys"
                            ),
                            new AppMenuItem(
                                "CardType",
                                "flaticon2-download-2",
                                "flaticon2-shelter",
                                "/app/connect/card/types"
                            ),
                            new AppMenuItem(
                                "Cards",
                                "Pages.Tenant.Connect.Card.Cards",
                                "flaticon2-architecture-and-city",
                                "/app/connect/card/cards"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Report",
                        "Pages.Tenant.Connect.Report",
                        "flaticon2-layers-1",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Ticket",
                                "Pages.Tenant.Connect.Report.Item",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Ticket",
                                        "Pages.Tenant.Connect.Report.Ticket",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/ticket"
                                    ),
                                    new AppMenuItem(
                                        "Sync",
                                        "Pages.Tenant.Connect.Report.TicketSync",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/ticket-sync"
                                    ),
                                    new AppMenuItem(
                                        "Hourly",
                                        "Pages.Tenant.Connect.Report.TicketHourlySales",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/hourly-sales"

                                    ),
                                ]
                            ),

                            new AppMenuItem(
                                "Order",
                                "Pages.Tenant.Connect.Report.Order",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Order",
                                        "Pages.Tenant.Connect.Report.Order",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/order"
                                    ),
                                    new AppMenuItem(
                                        "OrderTagReport",
                                        "Pages.Tenant.Connect.Tag.OrderTags",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/order-tag-group"
                                    ),
                                    new AppMenuItem(
                                        "OrderExchange",
                                        "Pages.Tenant.Connect.Tag.OrderTags",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/order-exchange"
                                    ),
                                    new AppMenuItem(
                                        "OrderReturn",
                                        "Pages.Tenant.Connect.Tag.OrderTags",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/order-return"
                                    ),
                                ]
                            ),
                            
                            new AppMenuItem(
                                "Other",
                                "Pages.Tenant.Connect.Report.Other",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Till Transaction",
                                        "Pages.Tenant.Connect.Report.TillTransaction",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/till-transaction"
                                    ),
                                    new AppMenuItem(
                                        "Log",
                                        "Pages.Tenant.Connect.Report.Log",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/log-other"
                                    ),
                                    new AppMenuItem(
                                        "Summary",
                                        "Pages.Tenant.Connect.Report.SummaryOther",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/summaryOther"
                                    ),
                                    new AppMenuItem(
                                        "Tender",
                                        "Pages.Tenant.Connect.Report.Tender",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/tender-report"
                                    ),
                                    new AppMenuItem(
                                        "Payment Excess",
                                        "Pages.Tenant.Connect.Report.PaymentExcess",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/payment-excess"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Department",
                                "Pages.Tenant.Connect.Report.Department",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Summary",
                                        "Pages.Tenant.Connect.Report.Department.Summary",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/department-summary"
                                    ),
                                    new AppMenuItem(
                                        "Item",
                                        "Pages.Tenant.Connect.Report.Department.Items",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/department-item"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Group",
                                "Pages.Tenant.Connect.Report.Group",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Sumary",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/group"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Category",
                                "Pages.Tenant.Connect.Report.Category",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Sumary",
                                        "Pages.Tenant.Connect.Report.Category",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/category"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Item",
                                "Pages.Tenant.Connect.Report.Item",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Sumary",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/item-sales"
                                    ),
                                    new AppMenuItem(
                                        "Hourly",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/hourly"
                                    ),
                                    new AppMenuItem(
                                        "Top Down",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/top-down"
                                    ),
                                    new AppMenuItem(
                                        "Tag",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/item-tag"
                                    ),
                                ]
                            ),


                            new AppMenuItem(
                                "Promotion",
                                "Pages.Tenant.Connect.Report.Promotion",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "OrderPromotion",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/order-promotion"
                                    ),
                                    new AppMenuItem(
                                        "TicketPromotion",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/ticket-promotion"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Schedules",
                                "Pages.Tenant.Connect.Report.ScheduleReport",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Items",
                                        "Pages.Tenant.Connect.Report.ScheduleItems",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/schedule-items"
                                    ),
                                    new AppMenuItem(
                                        "Location",
                                        "Pages.Tenant.Connect.Report.ScheduleLocations",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/schedule-locations"
                                    ),
                                ]
                            ),
                            new AppMenuItem(
                                "Sale",
                                "Pages.Tenant.Connect.Report.Sale",
                                "flaticon2-layers-1",
                                "",
                                [],
                                [
                                    new AppMenuItem(
                                        "Summary",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/summary-sale"
                                    ),
                                    new AppMenuItem(
                                        "WorkDay",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/workday"
                                    ),
                                    new AppMenuItem(
                                        "WeekDay",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/weekday"
                                    ),
                                    new AppMenuItem(
                                        "WeekEnd",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/weekend"
                                    ),
                                    new AppMenuItem(
                                        "PlanDay",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/planday"
                                    ),
                                    new AppMenuItem(
                                        "CashSummaryReport",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/cash-summary"
                                    ),
                                    new AppMenuItem(
                                        "CollectionReport",
                                        "Pages.Tenant.Connect",
                                        "flaticon2-layers-1",
                                        "/app/connect/report/collection-report"
                                    ),
                                ]
                            ),
                        ]
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Connect"
                    );
                }
            ),
            //#endregion
            //#region "Wheel"
            new AppMenuItem(
                "Wheel",
                "Pages.Tenant.Wheel",
                "flaticon2-rocket-1",
                "",
                [],
                [
                    new AppMenuItem(
                        "Setup",
                        "Pages.WheelSetups",
                        "flaticon2-soft-icons",
                        "/app/wheel/setup",
                        []
                    ),
                    new AppMenuItem(
                        "Dynamic Page",
                        "Pages.WheelDynamicPages",
                        "flaticon2-console",
                        "/app/wheel/dynamic-pages",
                        []
                    ),
                    new AppMenuItem(
                        "Table",
                        "Pages.WheelTables",
                        "flaticon2-document",
                        "/app/wheel/tables",
                        []
                    ),
                    new AppMenuItem(
                        "General Setting",
                        "Pages.Wheel.GeneralSettings",
                        "flaticon2-open-text-book",
                        "/app/wheel/basic-settings/order-settings",
                        []
                    ),
                    new AppMenuItem(
                        "Manage",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Location",
                                "Pages.WheelLocations",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/locations",
                                []
                            ),
                            new AppMenuItem(
                                "Department",
                                "Pages.WheelDepartments",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/departments",
                                []
                            ),
                            new AppMenuItem(
                                "Screen Menu",
                                "Pages.WheelScreenMenus",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/screen-menus",
                                []
                            ),
                            new AppMenuItem(
                                "Soldouts",
                                "Pages.WheelProductSoldoutMenus",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/product-soldout",
                                []
                            ),
                            new AppMenuItem(
                                "Recommendation",
                                "Pages.WheelRecommendationMenu",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/recommendation-menus",
                                []
                            ),
                            new AppMenuItem(
                                "Delivery Zone",
                                "Pages.WheelDeliveryZone",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/basic-settings/delivery-zones",
                                []
                            ),
                            new AppMenuItem(
                                "Delivery Duration",
                                "Pages.WheelDeliveryDurations",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/basic-settings/delivery-durations",
                                []
                            ),
                            new AppMenuItem(
                                "Service Fee",
                                "Pages.WheelServiceFee",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/basic-settings/service-fees",
                                []
                            ),
                            new AppMenuItem(
                                "Opening Hour",
                                "Pages.WheelOpeningHour",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/basic-settings/opening-hours",
                                []
                            ),
                            new AppMenuItem(
                                "Tax",
                                "Pages.WheelTaxes",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/basic-settings/taxes",
                                []
                            ),
                            new AppMenuItem(
                                "Faq",
                                "Pages.Wheel.Faq",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/faqs",
                                []
                            ),

                            new AppMenuItem(
                                "Promotion",
                                "Pages.WheelPromotions",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/manage/promotion-dynamic"
                            ),
                            new AppMenuItem(
                                "Flyers",
                                "Pages.Tenant.Flyer.Manage.Flyer",
                                "flaticon2-crisp-icons-1",
                                "/app/flyer/your-flyers"
                            ),
                        ]
                    ),

                    new AppMenuItem(
                        "Queue",
                        "DinePlan.DineConnect.Queue",
                        "flaticon2-shelter",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "QueueLocations",
                                "DinePlan.DineConnect.Queue",
                                "flaticon2-crisp-icons-1",
                                "/app/queue/queue-locations"
                            ),
                            new AppMenuItem(
                                "ManageQueues",
                                "DinePlan.DineConnect.Queue",
                                "flaticon2-mail-1",
                                "/app/queue/manage-queue"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Reserve",
                        "Pages.Wheel.Reserve",
                        "flaticon2-shelter",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "SeatMap",
                                "Pages.Wheel.Reserve.SeatMap",
                                "flaticon2-crisp-icons-1",
                                "/app/wheel/reserve/seat-map"
                            ),
                            new AppMenuItem(
                                "Reservation",
                                "Pages.Wheel.Reserve.Reservation",
                                "flaticon2-mail-1",
                                "/app/wheel/reserve/reservation"
                            ),
                        ]
                    ),


                    new AppMenuItem(
                        "Report",
                        "Pages.Wheel.Report",
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Customer",
                                null,
                                "flaticon2-gear",
                                "/app/wheel/customers",
                                []
                            ),
                            new AppMenuItem(
                                "Order",
                                null,
                                "flaticon2-gear",
                                "/app/wheel/orders",
                                []
                            ),
                        ]
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Wheel"
                    );
                }
            ),

            //#endregion
            //#region "Clique"
            new AppMenuItem(
                "Clique",
                "Pages",
                "flaticon2-cd",
                "",
                [],
                [

                    new AppMenuItem(
                        "Manage",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Company",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/manage/company",
                                []
                            ),
                            new AppMenuItem(
                                "Brand",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/manage/brand",
                                []
                            ),
                            new AppMenuItem(
                                "Meal Time",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/manage/clique-meal-time",
                                []
                            ),
                            new AppMenuItem(
                                "Schedule",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/manage/schedule",
                                []
                            ),

                            new AppMenuItem(
                                "Product",
                                null,
                                "flaticon2-gear",
                                null,
                                [],
                                [

                                    new AppMenuItem(
                                        "Group",
                                        null,
                                        "flaticon2-medical-records",
                                        "/app/clique/manage/product/group",
                                        []
                                    ),
                                    new AppMenuItem(
                                        "Category",
                                        null,
                                        "flaticon2-medical-records",
                                        "/app/clique/manage/product/category",
                                        []
                                    ),
                                    new AppMenuItem(
                                        "Product",
                                        null,
                                        "flaticon2-medical-records",
                                        "/app/clique/manage/product/product",
                                        []
                                    )
                                ]
                            ),
                            new AppMenuItem(
                                "Menu",
                                null,
                                "flaticon2-menu-4",
                                null,
                                [],
                                [
                                    new AppMenuItem(
                                        "Tag",
                                        null,
                                        "flaticon2-medical-records",
                                        "/app/clique/manage/menu/tag",
                                        []
                                    ),
                                    new AppMenuItem(
                                        "Menu",
                                        null,
                                        "flaticon2-medical-records",
                                        "/app/clique/manage/menu/screenmenu",
                                        []
                                    ),

                                ]
                            ),

                        ]
                    ),
                    new AppMenuItem(
                        "Customer",
                        "",
                        "flaticon2-tag",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Customer",
                                "",
                                "flaticon-customer",
                                "/app/clique/customer/customers"
                            ),
                            new AppMenuItem(
                                "Transaction",
                                "",
                                "flaticon-customer",
                                "/app/clique/customer/customerTransactions"
                            ),
                            new AppMenuItem(
                                "Collect",
                                "Pages",
                                "flaticon-customer",
                                "/app/clique/customer/customer-order"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Reports",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "BrandUser",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/brand-user",
                                []
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Administration",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "BrandUser",
                                null,
                                "flaticon2-medical-records",
                                "/app/clique/brand-user",
                                []
                            ),
                        ]
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Clique"
                    );
                }
            ),

            //#endregion
            //#region "Cluster"
            new AppMenuItem(
                "Cluster",
                "Pages",
                "flaticon2-cd",
                "",
                [],
                [

                    new AppMenuItem(
                        "Location",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Group",
                                null,
                                "flaticon2-gear",
                                '/app/cluster/location-group',
                            ),
                            new AppMenuItem(
                                "Location",
                                null,
                                "flaticon2-gear",
                                '/app/cluster/location',
                            ),
                            new AppMenuItem(
                                "Mapping",
                                null,
                                "flaticon2-gear",
                                '/app/cluster/location-mapping',
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Product",
                        "",
                        "flaticon2-tag",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Group",
                                "",
                                "flaticon-customer",
                                "/app/cluster/item-group"
                            ),
                            new AppMenuItem(
                                "Category",
                                "",
                                "flaticon-customer",
                                "/app/cluster/category"
                            ),
                            new AppMenuItem(
                                "Item",
                                "Pages",
                                "flaticon-customer",
                                "/app/cluster/item"
                            ),
                            new AppMenuItem(
                                "Item Activation",
                                "Pages",
                                "flaticon-customer",
                                "/app/cluster/item-activation"
                            ),
                            new AppMenuItem(
                                "Status",
                                "Pages",
                                "flaticon-customer",
                                "/app/cluster/status"
                            ),
                            new AppMenuItem(
                                "Tax",
                                "Pages",
                                "flaticon-customer",
                                "/app/cluster/tax"
                            ),
                            new AppMenuItem(
                                "Charge",
                                "Pages",
                                "flaticon-customer",
                                "/app/cluster/charge"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Modifier",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Variant Group ",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/variant-group",
                                []
                            ),
                            new AppMenuItem(
                                "Variant ",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/variant",
                                []
                            ),
                            new AppMenuItem(
                                "Modifier Group ",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/modifier-group",
                                []
                            ),
                            new AppMenuItem(
                                "Modifier",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/modifier",
                                []
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Manage",
                        null,
                        "flaticon2-gear",
                        null,
                        [],
                        [
                            new AppMenuItem(
                                "Timing Group",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/timing-group",
                                []
                            ),
                            new AppMenuItem(
                                "Language",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/language",
                                []
                            ),
                            new AppMenuItem(
                                "Image",
                                null,
                                "flaticon2-medical-records",
                                "/app/cluster/image",
                                []
                            ),
                        ]
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Clique"
                    );
                }
            ),
            //#endregion
            //#region "Tiffin"
            new AppMenuItem(
                "Tiffin",
                "Pages.Tenant.Tiffin",
                "flaticon2-sort-down",
                "",
                [],
                [
                    new AppMenuItem(
                        "Customer",
                        "Pages.Tenant.Tiffin.Customer",
                        "flaticon2-tag",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "Customer",
                                "Pages.Tenant.Tiffin.Customer.Customers",
                                "flaticon-customer",
                                "/app/tiffin/customers"
                            ),
                            new AppMenuItem(
                                "Transactions",
                                "Pages.Tenant.Tiffin.Customer.Transactions",
                                "flaticon-customer",
                                "/app/tiffin/customerTransactions"
                            ),
                            new AppMenuItem(
                                "Collect",
                                "Pages",
                                "flaticon-customer",
                                "/app/tiffin/customer-order"
                            ),
                        ]
                    ),

                    new AppMenuItem(
                        "Manage",
                        "Pages.Tenant.Tiffin.Manage",
                        "flaticon2-tag",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "ProductOffer",
                                "Pages.Tenant.Tiffin.Manage.ProductOffers",
                                "flaticon-more",
                                "/app/tiffin/manage/productOffers"
                            ),
                            new AppMenuItem(
                                "ProductSet",
                                "Pages.Tenant.Tiffin.Manage.ProductSets",
                                "flaticon-more",
                                "/app/tiffin/manage/productSets"
                            ),
                            new AppMenuItem(
                                "TiffinProductOfferType",
                                "Pages.Tenant.Tiffin.Manage.ProductOfferTypes",
                                "flaticon-more",
                                "/app/tiffin/manage/productOfferTypes"
                            ),
                            new AppMenuItem(
                                "MealTime",
                                "Pages.Tenant.Tiffin.Manage.MealTimes",
                                "flaticon-more",
                                "/app/tiffin/manage/mealTimes"
                            ),
                            new AppMenuItem(
                                "OrderTag",
                                "Pages.Tenant.Tiffin.Manage.OrderTags",
                                "flaticon-more",
                                "/app/tiffin/manage/orderTags"
                            ),
                            new AppMenuItem(
                                "TimeSlot",
                                "Pages.Tenant.Tiffin.TimeSlot",
                                "flaticon-more",
                                "/app/tiffin/manage/time-slots"
                            ),
                            new AppMenuItem(
                                "Faqs",
                                "Pages.Tenant.Tiffin.Manage.Faq",
                                "flaticon-more",
                                "/app/tiffin/manage/faqs"
                            ),
                            new AppMenuItem(
                                "HomePageBanner",
                                "Pages.Tenant.Tiffin.Manage.HomeBanner",
                                "flaticon-more",
                                "/app/tiffin/manage/homepage-banner"
                            ),
                            new AppMenuItem(
                                "TiffinPromotion",
                                "Pages.Tenant.Tiffin.Manage.Promotion",
                                "flaticon-more",
                                "/app/tiffin/manage/promotion-dynamic"
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Schedule",
                        "Pages.Tenant.Tiffin.Manage.Schedules",
                        "flaticon-more",
                        "/app/tiffin/manage/schedules"
                    ),
                    new AppMenuItem(
                        "Report",
                        "Pages.Tenant.Tiffin.Report",
                        "flaticon-interface-8",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "OrderSummary",
                                "Pages.Tenant.Tiffin.Report.OrderSummary",
                                "flaticon-user",
                                "/app/tiffin/report/order"
                            ),
                            new AppMenuItem(
                                "Preparation",
                                "Pages.Tenant.Tiffin.Report.Preparation",
                                "flaticon-user",
                                "/app/tiffin/report/preparation"
                            ),
                            new AppMenuItem(
                                "Balance",
                                "Pages.Tenant.Tiffin.Report.Balance",
                                "flaticon-suitcase",
                                "/app/tiffin/report/balance"
                            ),
                            /* TODO: permission */
                            new AppMenuItem(
                                "Customer",
                                "Pages.Tenant.Tiffin.Report",
                                "flaticon-suitcase",
                                "/app/tiffin/report/customer"
                            ),
                            new AppMenuItem(
                                "MealPlan",
                                "Pages.Tenant.Tiffin.Report",
                                "flaticon-suitcase",
                                "/app/tiffin/report/meal-plan"
                            ),
                            new AppMenuItem(
                                "Collect",
                                "Pages.Tenant.Tiffin.Report",
                                "flaticon-suitcase",
                                "/app/tiffin/report/collect"
                            ),
                            new AppMenuItem(
                                "Payment",
                                "Pages.Tenant.Tiffin.Report.Payment",
                                "flaticon-users",
                                "/app/tiffin/report/payment"
                            ),
                            new AppMenuItem(
                                "Kitchen",
                                "Pages.Tenant.Tiffin.Report.Kitchen",
                                "flaticon2-supermarket",
                                "/app/tiffin/report/kitchen"
                            ),
                            new AppMenuItem(
                                "Driver ",
                                "Pages.Tenant.Tiffin.Report.Driver",
                                "flaticon2-lorry",
                                "/app/tiffin/report/driver"
                            ),
                            new AppMenuItem(
                                "Transaction ",
                                "Pages.Tenant.Tiffin.Report.TiffinTransaction",
                                "flaticon2-lorry",
                                "/app/tiffin/report/tiffintransaction"
                            ),
                        ]
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Tiffins"
                    );
                }
            ),
            //#endregion
            //#region "Go"
            new AppMenuItem(
                "Go",
                "Pages",
                "flaticon2-cd",
                "",
                [],
                [
                    new AppMenuItem(
                        "Device",
                        "Pages",
                        "flaticon2-cd",
                        "/app/go/device"
                    ),
                    new AppMenuItem(
                        "Brand",
                        "Pages",
                        "flaticon2-cd", "/app/go/brand"
                    ),
                    new AppMenuItem(
                        "Department",
                        "Pages",
                        "flaticon2-cd", "/app/go/department"
                    ),
                    new AppMenuItem(
                        "PaymentType",
                        "Pages",
                        "flaticon2-cd", "/app/go/payment-type"
                    ),
                    new AppMenuItem(
                        "Charges",
                        "Pages",
                        "flaticon2-cd", "/app/go/charge"
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Go"
                    );
                }
            ),
            //#endregion
            //#region "Swipe"
            new AppMenuItem(
                "Swipe",
                "Pages",
                "flaticon2-hourglass",
                "",
                [],
                [
                    new AppMenuItem(
                        "Manage",
                        "Pages",
                        "flaticon2-files-and-folders",
                        "",
                        [],
                        [
                            new AppMenuItem(
                                "CardType",
                                "Pages",
                                "flaticon2-open-text-book",
                                "/app/swipe/master/card-type",
                                []
                            ),
                            new AppMenuItem(
                                "Card",
                                "Pages",
                                "flaticon2-add-square",
                                "/app/swipe/master/card",
                                []
                            ),
                        ]
                    ),
                    new AppMenuItem(
                        "Transaction",
                        "Pages",
                        "flaticon2-tag",
                        "/app/swipe/transaction",
                        []
                    ),
                ],
                undefined,
                undefined,
                () => {
                    return this._featureSessionService.isEnabled(
                        "DinePlan.DineConnect.Swipe"
                    );
                }
            ),
            //#endregion
            /**
                      //#region "Track"
                      new AppMenuItem(
                          "Track",
                          "Pages",
                          "flaticon-interface-8",
                          "",
                          [],
                          [
                              new AppMenuItem(
                                  "Directories",
                                  "Pages",
                                  "flaticon2-soft-icons-1",
                                  "",
                                  [],
                                  [
                                      new AppMenuItem(
                                          "Clients",
                                          "Pages",
                                          "far fa-address-card",
                                          "/app/shipment/clients"
                                      ),
                                      new AppMenuItem(
                                          "Addresses",
                                          "Pages",
                                          "fas fa-map-marker-alt",
                                          "/app/shipment/addresses"
                                      ),
                                      new AppMenuItem(
                                          "Depots",
                                          "Pages",
                                          "flaticon2-soft-icons-1",
                                          "/app/shipment/depots"
                                      ),
                                      new AppMenuItem(
                                          "Drivers",
                                          "Pages",
                                          "flaticon-user",
                                          "/app/shipment/drivers"
                                      ),
                                  ]
                              ),
                              new AppMenuItem(
                                  "Analytics",
                                  "Pages",
                                  "flaticon2-delivery-truck",
                                  "/app/shipment/analytics"
                              ),
                              new AppMenuItem(
                                  "Routes",
                                  "Pages",
                                  "flaticon2-delivery-truck",
                                  "/app/shipment/routes"
                              ),
                          ],
                          undefined,
                          undefined,
                          () => {
                              return this._featureSessionService.isEnabled(
                                  "DinePlan.DineConnect.Track"
                              );
                          }
                      ),
                      //#endregion

                      */

            //#region Admin
            new AppMenuItem(
                "Administration",
                "",
                "flaticon-interface-8",
                "",
                [],
                [
                    new AppMenuItem(
                        "AddOns",
                        "Pages.Administration.Tenant.AddonSettings",
                        "flaticon-settings",
                        "/app/admin/addonSettings"
                    ),
                    new AppMenuItem(
                        "TemplateEngine",
                        "Pages.Administration.Tenant.TemplateEngines",
                        "flaticon-more",
                        "/app/tiffin/templateEngines"
                    ),
                    new AppMenuItem(
                        "Languages",
                        "Pages.Administration.Languages",
                        "flaticon-tabs",
                        "/app/admin/languages"
                    ),

                    new AppMenuItem(
                        "Roles",
                        "Pages.Administration.Roles",
                        "flaticon-suitcase",
                        "/app/admin/roles"
                    ),
                    new AppMenuItem(
                        "Users",
                        "Pages.Administration.Users",
                        "flaticon-users",
                        "/app/admin/users"
                    ),
                    new AppMenuItem(
                        "UserLocation",
                        "Pages.Administration.UserLocation",
                        "flaticon-user",
                        "/app/connect/user-locations"
                    ),

                    new AppMenuItem(
                        "AuditLogs",
                        "Pages.Administration.AuditLogs",
                        "flaticon-folder-1",
                        "/app/admin/auditLogs"
                    ),
                    new AppMenuItem(
                        "Background Jobs",
                        "Pages",
                        "flaticon2-correct",
                        "/app/admin/background-jobs"
                    ),
                    new AppMenuItem(
                        "Maintenance",
                        "Pages.Administration.Host.Maintenance",
                        "flaticon-lock",
                        "/app/admin/maintenance"
                    ),
                    new AppMenuItem(
                        "Subscription",
                        "Pages.Administration.Tenant.SubscriptionManagement",
                        "flaticon-refresh",
                        "/app/admin/subscription-management"
                    ),
                    new AppMenuItem(
                        "TenantDatabase",
                        "Pages.Administration.Host.TenantDatabase",
                        "flaticon-tool",
                        "/app/admin/tenantDatabase"
                    ),
                    new AppMenuItem(
                        "Origins",
                        "Pages.Administration.Host.Origins",
                        "flaticon-more",
                        "/app/admin/origins"
                    ),
                    new AppMenuItem(
                        "Settings",
                        "Pages.Administration.Host.Settings",
                        "flaticon-settings",
                        "/app/admin/hostSettings"
                    ),
                    new AppMenuItem(
                        "Settings",
                        "Pages.Administration.Tenant.Settings",
                        "flaticon-settings",
                        "/app/admin/tenantSettings"
                    ),
                    new AppMenuItem(
                        "VisualSettings",
                        "Pages.Administration.UiCustomization",
                        "flaticon-medical",
                        "/app/admin/ui-customization"
                    ),
                ]
            ),
            //#endregion
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {
        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (
                subMenuItem.permissionName === "" ||
                subMenuItem.permissionName === null
            ) {
                if (subMenuItem.route) {
                    return true;
                }
            } else if (
                this._permissionCheckerService.isGranted(
                    subMenuItem.permissionName
                )
            ) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                let isAnyChildItemActive =
                    this.checkChildMenuItemPermission(subMenuItem);
                if (isAnyChildItemActive) {
                    return true;
                }
            }
        }
        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (
            menuItem.permissionName ===
            "Pages.Administration.Tenant.SubscriptionManagement" &&
            this._appSessionService.tenant &&
            !this._appSessionService.tenant.edition
        ) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (
            menuItem.permissionName &&
            !this._permissionCheckerService.isGranted(menuItem.permissionName)
        ) {
            hideMenuItem = true;
        }

        if (
            this._appSessionService.tenant ||
            !abp.multiTenancy.ignoreFeatureCheckForHostUsers
        ) {
            if (
                menuItem.hasFeatureDependency() &&
                !menuItem.featureDependencySatisfied()
            ) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }

    /**
     * Returns all menu items recursively
     */
    getAllMenuItems(): AppMenuItem[] {
        let menu = this.getMenu();
        let allMenuItems: AppMenuItem[] = [];
        menu.items.forEach((menuItem) => {
            allMenuItems = allMenuItems.concat(
                this.getAllMenuItemsRecursive(menuItem)
            );
        });
        return allMenuItems;
    }

    private getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {
        if (!menuItem.items) {
            return [menuItem];
        }

        let menuItems = [menuItem];
        menuItem.items.forEach((subMenu) => {
            menuItems = menuItems.concat(
                this.getAllMenuItemsRecursive(subMenu)
            );
        });

        return menuItems;
    }
}

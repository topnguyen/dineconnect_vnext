import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {
    ReportBackgroundServiceProxy,
    BackgroundReportOutput,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'reportNotificationModal',
    templateUrl: './report-notifications-modal.component.html',
})
export class ReportNotificationModalComponent extends AppComponentBase {
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    saving = false;
    backgroundReport: BackgroundReportOutput[] = [];

    constructor(
        injector: Injector,
        private _reportBackgroundService: ReportBackgroundServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }
    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._reportBackgroundService
            .getAll(
                [],
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    show() {
        this.reloadPage();
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    downloadReport(id: number) {
        this._reportBackgroundService.getFileDto(id).subscribe((result) => {
            if (result) {
                this._fileDownloadService.downloadTempFile(result);
            }
        });
    }
}

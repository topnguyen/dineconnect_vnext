import { IThemeAssetContributor } from '../ThemeAssetContributor';

export class Theme12ThemeAssetContributor implements IThemeAssetContributor {
    getAdditionalBodyStle(): string {
        return '';
    }
    getMenuWrapperStyle(): string {
        return '';
    }
    getFooterStyle(): string {
        return '';
    }
    getAssetUrls(): string[] {
        return [''];
    }
}

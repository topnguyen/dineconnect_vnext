import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QueueRoutingModule } from './queue-routing.module';
import { QueueLocationsComponent } from './components/queue-locations/queue-locations.component';
import { ManageQueueComponent } from './components/manage-queue/manage-queue.component';
import { AddQueueOptionModalComponent } from './components/queue-locations/add-queue-option-modal/add-queue-option-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { CalendarModule, CheckboxModule, PaginatorModule, TableModule } from 'primeng';
import { UtilsModule } from '@shared/utils/utils.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { CustomerQueueComponent } from './components/manage-queue/customer-queue/customer-queue.component';
import { AddQueueLocationComponent } from './components/queue-locations/add-queue-location/add-queue-location.component';
import { SelectButtonModule } from 'primeng/selectbutton';

@NgModule({
    declarations: [QueueLocationsComponent, ManageQueueComponent, AddQueueOptionModalComponent, CustomerQueueComponent, AddQueueLocationComponent],
    imports: [
        CommonModule,
        QueueRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        UtilsModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        CheckboxModule,
        CalendarModule,
        SelectButtonModule
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        }
    ]
})
export class QueueModule {}

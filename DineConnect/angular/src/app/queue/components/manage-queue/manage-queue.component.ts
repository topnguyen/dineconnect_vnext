import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { SingleLocationDto } from '@app/shared/common/select-single-location/select-single-location.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DefaultLocationServiceProxy,
    DineQueueServiceProxy,
    LocationServiceProxy,
    QueueLocationDto,
    QueueLocationOptionDto,
    UserDefaultLocationDto
} from '@shared/service-proxies/service-proxies';
import { cloneDeep, isEqual } from 'lodash';
import { Paginator, Table } from 'primeng';
import { interval, Subscription } from 'rxjs';``
import { concatMap } from 'rxjs/operators';

@Component({
  templateUrl: './manage-queue.component.html',
  styleUrls: ['./manage-queue.component.scss'],
  animations: [appModuleAnimation()]
})
export class ManageQueueComponent  extends AppComponentBase implements OnInit, OnDestroy {
    @ViewChild('queueTable', { static: true }) dataTable: Table;
    @ViewChild('queuePaginator', { static: true }) paginator: Paginator;
    @ViewChild('selectLocation', { static: true }) selectLocation: SelectLocationComponent;

    queueLocation = new QueueLocationDto();
    location = new SingleLocationDto();
    defaultLocation = new UserDefaultLocationDto();
    queueLocationOptions: QueueLocationOptionDto[] = [];

    private _inteval$: Subscription;

    constructor(
        injector: Injector,
        private _dineQueueServiceProxy: DineQueueServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _defaultLocationServiceProxy: DefaultLocationServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getUserInfo();
    }

    ngOnDestroy(): void {
        if (this._inteval$) {
            this._inteval$.unsubscribe();
        }
    }

    getQueueLocationFullData() {
        this._dineQueueServiceProxy.getQueueLocationFullDataByLocationId(this.location.locationId).subscribe((result) => {
            this.queueLocation = cloneDeep(result);
            this.queueLocationOptions = (this.queueLocation.queueLocationOptions || []).filter((option) => {
                return option.active;
            });

            if (!this._inteval$) {
                this._inteval$ = interval(30000)
                .pipe(concatMap(() => this._dineQueueServiceProxy.getQueueLocationFullDataByLocationId(this.location.locationId)))
                .subscribe((result) => {
                    if (result) {
                        this.queueLocation = cloneDeep(result);
                        const queueLocationOptions = (this.queueLocation.queueLocationOptions || []).filter((option) => {
                            return option.active;
                        });

                        if (!isEqual(this.queueLocationOptions, queueLocationOptions)) {
                            this.queueLocationOptions = queueLocationOptions;
                        }
                    }
                });
            }
        });
    }

    openLocationsModal() {
        this.selectLocation.show(this.location);
    }

    setLocation(event: SingleLocationDto) {
        this.location = event;
        this.getQueueLocationFullData();
    }

    private _getUserInfo() {
        this._defaultLocationServiceProxy
            .getUserDefaultLocation()
            .pipe(
                concatMap((defaultLocation) => {
                    this.defaultLocation = defaultLocation;
                    return this._locationServiceProxy.getSimpleLocations(undefined, false, 0, undefined, 1000, 0);
                })
            )
            .subscribe((result) => {
                this.location = new SingleLocationDto();
                const location = (result.items || []).find((item) => item.id == this.defaultLocation.defaultLocationId);
                if (location) {
                    this.location.locationId = location.id;
                    this.location.locationName = location.name;
                }

                this.getQueueLocationFullData();
            });
    }
}
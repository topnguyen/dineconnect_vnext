import { Component, Injector, Input, Output, OnInit, ViewChild, EventEmitter, OnChanges, SimpleChanges, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CustomerQueueDto, DineQueueServiceProxy, QueueLocationDto, QueueLocationOptionDto } from '@shared/service-proxies/service-proxies';
import { orderBy, maxBy, findIndex, cloneDeep } from 'lodash';
import moment from 'moment';
import { Paginator, Table } from 'primeng';

@Component({
    selector: 'app-customer-queue',
    templateUrl: './customer-queue.component.html',
    styleUrls: ['./customer-queue.component.scss'],
    animations: [appModuleAnimation()]
})
export class CustomerQueueComponent extends AppComponentBase implements OnInit, OnChanges, AfterViewInit {
    @ViewChild('queueTable', { static: true }) dataTable: Table;
    @ViewChild('queuePaginator', { static: true }) paginator: Paginator;

    @Input() queueLocation: QueueLocationDto;
    @Input() option: QueueLocationOptionDto;

    @Output() reload: EventEmitter<any> = new EventEmitter<any>();

    paging: IPaging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };
    isCompleted = false;
    isInQueue = true;

    currentQueue = new CustomerQueueDto();
    nextQueue = new CustomerQueueDto();

    constructor(injector: Injector, private _dineQueueServiceProxy: DineQueueServiceProxy, private _cdr: ChangeDetectorRef) {
        super(injector);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.option) {
            this.refresh();
        }
    }

    ngOnInit(): void {}

    refresh() {
        this._updateCurrentQueue();
        this.primengTableHelper.records = cloneDeep(this.option.customerQueues).filter((item) => {
            if (this.isInQueue && this.isCompleted) {
                return true;
            } else if (this.isInQueue && !this.isCompleted) {
                return !item.finishTime;
            } else if (!this.isInQueue && this.isCompleted) {
                return item.finishTime;
            } else {
                return false;
            }
        });
        this.primengTableHelper.hideLoadingIndicator();
        this._cdr.detectChanges();
    }

    ngAfterViewInit() {}

    onChangePage(paging: IPaging) {
        this.paging = paging;
    }

    noShow(item: CustomerQueueDto) {
        item.seated = false;
        item.finishTime = moment(moment.now());

        this._createOrUpdateCustomerQueue(item);
    }

    seated(item: CustomerQueueDto) {
        item.seated = true;
        item.finishTime = moment(moment.now());

        this._createOrUpdateCustomerQueue(item);
    }

    callNow(item: CustomerQueueDto) {
        item.callTime = moment(moment.now());

        this._createOrUpdateCustomerQueue(item);
    }

    recall(item: CustomerQueueDto) {
        item.callTime = moment(moment.now());
        item.finishTime = null;
        item.seated = false;

        this._createOrUpdateCustomerQueue(item);
    }

    callNext() {
        this.nextQueue = this.option.customerQueues.find((item) => !item.callTime && !item.finishTime);

        if (this.nextQueue && this.nextQueue.id) {
            this.nextQueue.callTime = moment(moment.now());

            this._createOrUpdateCustomerQueue(this.nextQueue);
        }
    }

    callDuration(callTime: moment.Moment, finishTime: moment.Moment) {
        const duration = callTime && finishTime ? moment.duration(finishTime.diff(callTime)) : null;

        if (duration) {
            if (duration.asHours() > 1) {
                return Math.round(duration.asHours()) + 'h';
            } else if (duration.asMinutes() > 1) {
                return Math.round(duration.asMinutes()) + 'm';
            } else if (duration.asSeconds() > 1) {
                return Math.round(duration.asSeconds()) + 's';
            }
        } else {
            return '';
        }
    }

    delete(item: CustomerQueueDto) {
        this._dineQueueServiceProxy.deleteCustomerQueue(item.id).subscribe((result) => {
            this.notify.info(this.l('DeletedSuccessfully'));
            this.reload.emit(true);
        });
    }

    private _updateCurrentQueue() {
        this.option.customerQueues = orderBy(this.option.customerQueues, ['queueTime'], 'asc');

        this.currentQueue = maxBy(this.option.customerQueues, 'callTime');
    }

    private _createOrUpdateCustomerQueue(input: CustomerQueueDto) {
        this.primengTableHelper.showLoadingIndicator();

        this._dineQueueServiceProxy.createOrUpdateCustomerQueue(input).subscribe((result) => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.reload.emit(true);
        });
    }

    get isDisabledCallNext() {
        if (this.currentQueue) {
            return !this.currentQueue.finishTime || !this.queueLocation?.enableQueue || !this.option.active;
        } else {
            return false;
        }
    }
}

interface IPaging {
    first: number;
    page: number;
    pageCount: number;
    rows: number;
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrEditWheelSetupDto,
    DineQueueServiceProxy,
    LocationServiceProxy,
    SimpleLocationDto,
    TenantSettingsEditDto,
    WheelSetupsServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './queue-locations.component.html',
    styleUrls: ['./queue-locations.component.scss'],
    animations: [appModuleAnimation()]
})
export class QueueLocationsComponent extends AppComponentBase implements OnInit {
    @ViewChild('queueTable', { static: true }) dataTable: Table;
    @ViewChild('queuePaginator', { static: true }) paginator: Paginator;

    paging: IPaging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };
    locations: SimpleLocationDto[] = [];
    wheelDomain: string;

    constructor(
        injector: Injector,
        private _router: Router,
        private _dineQueueServiceProxy: DineQueueServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _wheelSetupsServiceProxy: WheelSetupsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getSettings()
    }

    getQueueLocationList() {
        this.primengTableHelper.showLoadingIndicator();
        this._dineQueueServiceProxy
            .getQueueLocationList()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.records = result.items;

                this.primengTableHelper.records.forEach((item) => {
                    item.locationNames = item.locationIds.map((id) => {
                        const location = this.locations.find((l) => l.id == id);
                        return location ? location.name : '';
                    });
                });

                this.primengTableHelper.totalRecordsCount = result.items.length;
                this.paging.page = 0;
            });
    }

    onChangePage(paging: IPaging) {
        this.paging = paging;
    }

    createOrEditQueueLocation(id?: number) {
        this._router.navigate(['app/queue/queue-locations/', id || 'null']);
    }

    deleteQueueLocation(id: number) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._dineQueueServiceProxy.deleteQueueLocation(id).subscribe(() => {
                    this.getQueueLocationList();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    private _getLocations() {
        this._locationServiceProxy.getSimpleLocations(undefined, undefined, undefined, undefined, 1000, 0).subscribe((result) => {
            this.locations = result.items;
            this.getQueueLocationList();
        });
    }

    private _getSettings() {
        this._wheelSetupsServiceProxy.getWheelSetup().subscribe(result => {
            this.wheelDomain = result.wheelSetup.clientUrl || `https://${this.appSession.tenancyName?.toLowerCase()}.edine.xyz`;

            this._getLocations();
        });
    }
}

interface IPaging {
    first: number;
    page: number;
    pageCount: number;
    rows: number;
}

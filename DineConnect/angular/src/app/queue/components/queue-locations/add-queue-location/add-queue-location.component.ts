import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { SingleLocationDto } from '@app/shared/common/select-single-location/select-single-location.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DineQueueServiceProxy,
    LocationServiceProxy,
    QueueLocationDto,
    QueueLocationOptionDto,
    SimpleLocationDto,
    UserDefaultLocationDto
} from '@shared/service-proxies/service-proxies';
import { cloneDeep } from 'lodash';
import moment from 'moment';
import { Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import { AddQueueOptionModalComponent } from '../add-queue-option-modal/add-queue-option-modal.component';

@Component({
    selector: 'app-add-queue-location',
    templateUrl: './add-queue-location.component.html',
    styleUrls: ['./add-queue-location.component.scss'],
    animations: [appModuleAnimation()]
})
export class AddQueueLocationComponent extends AppComponentBase implements OnInit {
    @ViewChild('queueTable', { static: true }) dataTable: Table;
    @ViewChild('queuePaginator', { static: true }) paginator: Paginator;
    @ViewChild('queueOptionModal', { static: true }) queueOptionModal: AddQueueOptionModalComponent;
    @ViewChild('selectLocationModal', { static: false }) selectLocationModal: SelectLocationComponent;

    queueLocation = new QueueLocationDto();
    startTime: Date;
    endTime: Date;
    location = new SingleLocationDto();
    defaultLocation = new UserDefaultLocationDto();
    paging: IPaging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0
    };
    queueLocationId = +this._activatedRoute.snapshot.params['id'];
    selectedLocations: SimpleLocationDto[] = [];
    stateOptions = [{label: 'ON', value: true}, {label: 'OFF', value: false}];
    enableQueue = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _locationServiceProxy: LocationServiceProxy,
        private _dineQueueServiceProxy: DineQueueServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        if (!isNaN(this.queueLocationId)) {
            this.getQueueLocationFullDataById();
        } else {
            this.queueLocation = new QueueLocationDto();
            this.queueLocation.enableQueue = true;
        }
    }

    getQueueLocationFullDataById() {
        this.primengTableHelper.showLoadingIndicator();
        this._dineQueueServiceProxy
            .getQueueLocationFullDataById(this.queueLocationId)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.queueLocation = cloneDeep(result);
                this.startTime = this.getTime(result.startTime);
                this.endTime = this.getTime(result.endTime);

                this._getLocations();

                this.primengTableHelper.records = this.queueLocation.queueLocationOptions;
                this.primengTableHelper.totalRecordsCount = this.primengTableHelper.records.length;
                this.paging.page = 0;
            });
    }

    getTime(time: string) {
        const hour = +time.substr(0, 2);
        const minute = +time.substr(3, 2);
        return new Date(new Date().setHours(hour, minute, 0, 0));
    }

    deleteQueueOption(id: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._dineQueueServiceProxy.deleteQueueOption(id).subscribe(() => {
                    this.getQueueLocationFullDataById();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    createOrEditQueueOption(option = new QueueLocationOptionDto()) {
        if (!this.queueLocation.id) {
            this.message.error('PleaseCreateQueueLocationFirst');
            return;
        }
        option.queueLocationId = this.queueLocation.id;
        this.queueOptionModal.show(option);
    }

    pauseOption(option: QueueLocationOptionDto, active: boolean) {
        option.active = active;

        this._dineQueueServiceProxy.createOrUpdateQueueOption(option).subscribe(() => {
            this.notify.info(this.l('SavedSuccessfully'));
        });
    }

    onChangePage(paging: IPaging) {
        this.paging = paging;
    }

    save(queueForm: NgForm) {
        if (queueForm.invalid) {
            return;
        }

        this.queueLocation.startTime = moment(this.startTime).format('HH:mm:ss');
        this.queueLocation.endTime = moment(this.endTime).format('HH:mm:ss');
        this.queueLocation.locationIds = this.selectedLocations.map(item => item.id);

        this._dineQueueServiceProxy.createOrUpdateQueueLocation(this.queueLocation).subscribe((result) => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.back();
        });
    }

    back() {
        this._router.navigate(['app/queue/queue-locations']);
    }

    selectLocation() {
        this.selectLocationModal.show(this.selectedLocations);
    }

    onLocationAdded(event: SimpleLocationDto[]) {
        this.selectedLocations = event;
    }

    private _getLocations() {
        this._locationServiceProxy
            .getSimpleLocations(undefined, undefined, undefined, undefined, 1000, 0)
            .subscribe((result) => {
                this.selectedLocations = result.items.filter((item) => {
                    return this.queueLocation.locationIds.includes(item.id);
                });
            });
    }
}

interface IPaging {
    first: number;
    page: number;
    pageCount: number;
    rows: number;
}

import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DineQueueServiceProxy, QueueLocationOptionDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-add-queue-option-modal',
    templateUrl: './add-queue-option-modal.component.html',
    styleUrls: ['./add-queue-option-modal.component.scss']
})
export class AddQueueOptionModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    option: QueueLocationOptionDto;

    constructor(injector: Injector, private _dineQueueServiceProxy: DineQueueServiceProxy) {
        super(injector);
    }

    show(option: QueueLocationOptionDto): void {
        this.option = option;
        this.active = true;
        this.modal.show();
    }

    save(form: NgForm): void {
        if (form.invalid) {
            return;
        }

        this.saving = true;
        const body = this.option;

        this._dineQueueServiceProxy
            .createOrUpdateQueueOption(body)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modalSave.emit(true)
                this.close();
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    ngOnInit(): void {}
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QueueLocationsComponent } from './components/queue-locations/queue-locations.component';
import { ManageQueueComponent } from './components/manage-queue/manage-queue.component';
import { AddQueueLocationComponent } from './components/queue-locations/add-queue-location/add-queue-location.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    /* TODO: permission */
                    { path: 'queue-locations', component: QueueLocationsComponent, data: { permission: 'Pages.Tenant.Connect' } },
                    { path: 'queue-locations/:id', component: AddQueueLocationComponent, data: { permission: 'Pages.Tenant.Connect' } },
                    { path: 'manage-queue', component: ManageQueueComponent, data: { permission: 'Pages.Tenant.Connect' } },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})

export class QueueRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClusterRoutingModule } from './cluster-routing.module';
import { ClusterLocationGroupComponent } from './components/cluster-location-group/cluster-location-group.component';
import { ClusterLocationComponent } from './components/cluster-location/cluster-location.component';
import { ClusterLocationMappingComponent } from './components/cluster-location-mapping/cluster-location-mapping.component';
import { MultiSelectModule, PaginatorModule, SidebarModule, TableModule } from 'primeng';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddUpdateClusterLocationGroupComponent } from './components/cluster-location-group/add-update-cluster-location-group/add-update-cluster-location-group.component';
import { AddEditClusterLocationComponent } from './components/cluster-location/add-edit-cluster-location/add-edit-cluster-location.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FileUploadModule } from 'ng2-file-upload';
import { ClusterItemGroupComponent } from './components/cluster-item-group/cluster-item-group.component';
import { AddEditClusterItemGroupComponent } from './components/cluster-item-group/add-edit-cluster-item-group/add-edit-cluster-item-group.component';
import { AddEditClusterCategoryComponent } from './components/cluster-category/add-edit-cluster-category/add-edit-cluster-category.component';
import { ClusterCategoryComponent } from './components/cluster-category/cluster-category.component';
import { ClusterItemComponent } from './components/cluster-item/cluster-item.component';
import { AddEditClusterItemComponent } from './components/cluster-item/add-edit-cluster-item/add-edit-cluster-item.component';
import { ClusterLanguageDescriptionModalComponent } from './components/cluster-language-description-modal/cluster-language-description-modal.component';
import { ClusterTimingGroupComponent } from './components/cluster-timming-group/cluster-timing-group.component';
import { AddEditClusterTimingGroupComponent } from './components/cluster-timming-group/add-edit-cluster-timing-group/add-edit-cluster-timing-group.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ImportModalComponent } from './shared/import-modal/import-modal.component';
import { ClusterChargeComponent } from './components/cluster-charge/cluster-charge.component';
import { AddEditClusterChargeComponent } from './components/cluster-charge/add-edit-cluster-charge/add-edit-cluster-charge.component';
import { ClusterTaxComponent } from './components/cluster-tax/cluster-tax.component';
import { AddEditClusterTaxComponent } from './components/cluster-tax/add-edit-cluster-tax/add-edit-cluster-tax.component';
import { ClusterItemActivationComponent } from './components/cluster-item-activation/cluster-item-activation.component';
import { AddEditClusterItemActivationComponent } from './components/cluster-item-activation/add-edit-cluster-item-activation/add-edit-cluster-item-activation.component';
import { ClusterStatusComponent } from './components/cluster-status/cluster-status.component';
import { AddEditClusterStatusComponent } from './components/cluster-status/add-edit-cluster-status/add-edit-cluster-status.component';
import { ClusterVariantGroupComponent } from './components/cluster-variant-group/cluster-variant-group.component';
import { ClusterVariantComponent } from './components/cluster-variant/cluster-variant.component';
import { ClusterModifierGroupComponent } from './components/cluster-modifier-group/cluster-modifier-group.component';
import { ClusterModifierComponent } from './components/cluster-modifier/cluster-modifier.component';
import { AddEditClusterVariantGroupComponent } from './components/cluster-variant-group/add-edit-cluster-variant-group/add-edit-cluster-variant-group.component';
import { AddEditClusterVariantComponent } from './components/cluster-variant/add-edit-cluster-variant/add-edit-cluster-variant.component';
import { AddEditClusterVariantModalComponent } from './components/cluster-variant-group/add-edit-cluster-variant-modal/add-edit-cluster-variant-modal.component';
import { AddEditClusterModifierGroupComponent } from './components/cluster-modifier-group/add-edit-cluster-modifier-group/add-edit-cluster-modifier-group.component';
import { AddEditClusterModifierComponent } from './components/cluster-modifier/add-edit-cluster-modifier/add-edit-cluster-modifier.component';
import { AddEditClusterModifierModalComponent } from './components/cluster-modifier-group/add-edit-cluster-modifier-modal/add-edit-cluster-modifier-modal.component';
import { ClusterImageComponent } from './components/cluster-image/cluster-image.component';
import { AddEditClusterImageComponent } from './components/cluster-image/add-edit-cluster-image/add-edit-cluster-image.component';

@NgModule({
    declarations: [
        ClusterLocationGroupComponent,
        ClusterLocationComponent,
        ClusterLocationMappingComponent,
        AddUpdateClusterLocationGroupComponent,
        AddEditClusterLocationComponent,
        ClusterItemGroupComponent,
        AddEditClusterItemGroupComponent,
        AddEditClusterCategoryComponent,
        ClusterCategoryComponent,
        ClusterItemComponent,
        AddEditClusterItemComponent,
        ClusterLanguageDescriptionModalComponent,
        ClusterTimingGroupComponent,
        AddEditClusterTimingGroupComponent,
        ImportModalComponent,
        ClusterChargeComponent,
        AddEditClusterChargeComponent,
        ClusterTaxComponent,
        AddEditClusterTaxComponent,
        ClusterItemActivationComponent,
        AddEditClusterItemActivationComponent,
        ClusterStatusComponent,
        AddEditClusterStatusComponent,
        ClusterVariantGroupComponent,
        ClusterVariantComponent,
        ClusterModifierGroupComponent,
        ClusterModifierComponent,
        AddEditClusterVariantGroupComponent,
        AddEditClusterVariantComponent,
        AddEditClusterVariantModalComponent,
        AddEditClusterModifierGroupComponent,
        AddEditClusterModifierComponent,
        AddEditClusterModifierModalComponent,
        ClusterImageComponent,
        AddEditClusterImageComponent
    ],
    imports: [
        CommonModule,
        ClusterRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        UtilsModule,
        ModalModule,
        AppBsModalModule,
        BsDropdownModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        SidebarModule,
        TabsModule,
        FileUploadModule,
        TimepickerModule.forRoot(),
        MultiSelectModule
    ]
})
export class ClusterModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEditClusterLocationMappingComponent } from '@app/cluster/components/cluster-location-mapping/add-edit-cluster-location-mapping/add-edit-cluster-location-mapping.component';
import { AddEditClusterCategoryComponent } from './components/cluster-category/add-edit-cluster-category/add-edit-cluster-category.component';
import { ClusterCategoryComponent } from './components/cluster-category/cluster-category.component';
import { AddEditClusterChargeComponent } from './components/cluster-charge/add-edit-cluster-charge/add-edit-cluster-charge.component';
import { ClusterChargeComponent } from './components/cluster-charge/cluster-charge.component';
import { AddEditClusterImageComponent } from './components/cluster-image/add-edit-cluster-image/add-edit-cluster-image.component';
import { ClusterImageComponent } from './components/cluster-image/cluster-image.component';
import { AddEditClusterItemActivationComponent } from './components/cluster-item-activation/add-edit-cluster-item-activation/add-edit-cluster-item-activation.component';
import { ClusterItemActivationComponent } from './components/cluster-item-activation/cluster-item-activation.component';
import { AddEditClusterItemGroupComponent } from './components/cluster-item-group/add-edit-cluster-item-group/add-edit-cluster-item-group.component';
import { ClusterItemGroupComponent } from './components/cluster-item-group/cluster-item-group.component';
import { AddEditClusterItemComponent } from './components/cluster-item/add-edit-cluster-item/add-edit-cluster-item.component';
import { ClusterItemComponent } from './components/cluster-item/cluster-item.component';
import { AddUpdateClusterLocationGroupComponent } from './components/cluster-location-group/add-update-cluster-location-group/add-update-cluster-location-group.component';
import { ClusterLocationGroupComponent } from './components/cluster-location-group/cluster-location-group.component';
import { ClusterLocationMappingComponent } from './components/cluster-location-mapping/cluster-location-mapping.component';
import { AddEditClusterLocationComponent } from './components/cluster-location/add-edit-cluster-location/add-edit-cluster-location.component';
import { ClusterLocationComponent } from './components/cluster-location/cluster-location.component';
import { AddEditClusterModifierGroupComponent } from './components/cluster-modifier-group/add-edit-cluster-modifier-group/add-edit-cluster-modifier-group.component';
import { ClusterModifierGroupComponent } from './components/cluster-modifier-group/cluster-modifier-group.component';
import { AddEditClusterModifierComponent } from './components/cluster-modifier/add-edit-cluster-modifier/add-edit-cluster-modifier.component';
import { ClusterModifierComponent } from './components/cluster-modifier/cluster-modifier.component';
import { AddEditClusterStatusComponent } from './components/cluster-status/add-edit-cluster-status/add-edit-cluster-status.component';
import { ClusterStatusComponent } from './components/cluster-status/cluster-status.component';
import { AddEditClusterTaxComponent } from './components/cluster-tax/add-edit-cluster-tax/add-edit-cluster-tax.component';
import { ClusterTaxComponent } from './components/cluster-tax/cluster-tax.component';
import { AddEditClusterTimingGroupComponent } from './components/cluster-timming-group/add-edit-cluster-timing-group/add-edit-cluster-timing-group.component';
import { ClusterTimingGroupComponent } from './components/cluster-timming-group/cluster-timing-group.component';
import { AddEditClusterVariantGroupComponent } from './components/cluster-variant-group/add-edit-cluster-variant-group/add-edit-cluster-variant-group.component';
import { ClusterVariantGroupComponent } from './components/cluster-variant-group/cluster-variant-group.component';
import { AddEditClusterVariantComponent } from './components/cluster-variant/add-edit-cluster-variant/add-edit-cluster-variant.component';
import { ClusterVariantComponent } from './components/cluster-variant/cluster-variant.component';

const routes: Routes = [
    {
        path: '',
        children: [
            /* TODO: permission */
            {
                path: 'location-group',
                component: ClusterLocationGroupComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },
            {
                path: 'location-group/:id',
                component: AddUpdateClusterLocationGroupComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'location', component: ClusterLocationComponent, data: { permission: 'Pages.Tenant.Connect' } },
            {
                path: 'location/:id',
                component: AddEditClusterLocationComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            {
                path: 'location-mapping',
                component: ClusterLocationMappingComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },
            {
                path: 'location-mapping/:id',
                component: AddEditClusterLocationMappingComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'item-group', component: ClusterItemGroupComponent, data: { permission: 'Pages.Tenant.Connect' } },
            {
                path: 'item-group/:id',
                component: AddEditClusterItemGroupComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'category', component: ClusterCategoryComponent, data: { permission: 'Pages.Tenant.Connect' } },
            {
                path: 'category/:id',
                component: AddEditClusterCategoryComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'item', component: ClusterItemComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'item/:id', component: AddEditClusterItemComponent, data: { permission: 'Pages.Tenant.Connect' } },

            {
                path: 'timing-group',
                component: ClusterTimingGroupComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },
            {
                path: 'timing-group/:id',
                component: AddEditClusterTimingGroupComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'charge', component: ClusterChargeComponent, data: { permission: 'Pages.Tenant.Connect' } },
            {
                path: 'charge/:id',
                component: AddEditClusterChargeComponent,
                data: { permission: 'Pages.Tenant.Connect' }
            },

            { path: 'tax', component: ClusterTaxComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'tax/:id', component: AddEditClusterTaxComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'item-activation', component: ClusterItemActivationComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'item-activation/:id', component: AddEditClusterItemActivationComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'status', component: ClusterStatusComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'status/:id', component: AddEditClusterStatusComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'variant-group', component: ClusterVariantGroupComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'variant-group/:id', component: AddEditClusterVariantGroupComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'variant', component: ClusterVariantComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'variant/:id', component: AddEditClusterVariantComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'modifier-group', component: ClusterModifierGroupComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'modifier-group/:id', component: AddEditClusterModifierGroupComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'modifier', component: ClusterModifierComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'modifier/:id', component: AddEditClusterModifierComponent, data: { permission: 'Pages.Tenant.Connect' } },

            { path: 'image', component: ClusterImageComponent, data: { permission: 'Pages.Tenant.Connect' } },
            { path: 'image/:id', component: AddEditClusterImageComponent, data: { permission: 'Pages.Tenant.Connect' } }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClusterRoutingModule {}

import { Component, Injector, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, MenuItemServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggVariantInput,
    DelAggImageEditDto,
    DelAggLocMappingServiceProxy,
    DelAggVariantEditDto,
    DelAggVariantGroupListDto,
    DelAggVariantGroupServiceProxy,
    DelAggVariantServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';

@Component({
    selector: 'app-add-edit-cluster-variant',
    templateUrl: './add-edit-cluster-variant.component.html',
    styleUrls: ['./add-edit-cluster-variant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterVariantComponent extends AppComponentBase implements OnInit {
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;
    @ViewChild('selectMenuItemPortionModal', { static: true })
    selectMenuItemPortionModal: SelectPopupComponent;

    @Input() isModal: boolean;

    id: number;
    isLoading: boolean;
    delAggVariant = new DelAggVariantEditDto();
    delAggImage: DelAggImageEditDto[] = [];
    delAggImageItem = new DelAggImageEditDto();
    delaggVariantGroups: DelAggVariantGroupListDto[] = [];

    delAggTypes: ComboboxItemDto[];
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    closeModal: (isSaved) => void;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggVariantGroupServiceProxy: DelAggVariantGroupServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _delAggVariantServiceProxy: DelAggVariantServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.id = this.isModal ? this.id : +this._activatedRoute.snapshot.params['id']

        this._getDelAggType();
        this._getVariantGroups();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggVariantInput();
        body.delAggVariant = this.delAggVariant;
        body.delAggImage = this.delAggImage;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/variant']);
    }

    getMenuItemPortion() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<any> => {
            return this._menuItemServiceProxy.getAllPortionItems(
                filterString,
                undefined,
                undefined,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    openMenuItemPortion() {
        this.selectMenuItemPortionModal.show({ id: this.delAggVariant.menuItemPortionId });
    }

    clearMenuItemPortion() {
        this.delAggVariant.menuItemPortionId = undefined;
        this.delAggVariant.menuItemPortionName = undefined;
    }

    changeMenuItemPortion(data) {
        this.delAggVariant.menuItemPortionId = data.id;
        this.delAggVariant.menuItemPortionName = data.name;
    }

    remove(index: number) {
        this.delAggImage.splice(index, 1);
    }

    download(file: DelAggImageEditDto) {
        location.href = JSON.parse(file.imagePath).url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        file.upload();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.delAggImageItem.imagePath = JSON.stringify({
                    fileTag: 'Image',
                    fileName: ajaxResponse.result.fileName,
                    fileType: ajaxResponse.result.fileType,
                    fileToken: ajaxResponse.result.fileToken,
                    url: ajaxResponse.result.url
                });
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: false
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    addImage() {
        if (this.delAggTypeRef) {
            const isExist = this.delAggImage.some(
                (item) => item.delAggTypeRefId == +this.delAggTypeRef.value
            );
            if (isExist) {
                this.notify.error('Del Agg Type existed');
                return;
            }

            this.delAggImageItem.delAggTypeRefId = +this.delAggTypeRef.value;
            this.delAggImageItem.delAggTypeRefName = this.delAggTypeRef.displayText;

            this.delAggImage.push(new DelAggImageEditDto(this.delAggImageItem));
        }
    }

    toJSON(data: string) {
        return JSON.parse(data);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.delAggVariant.id,
            language: this.delAggVariant.name,
            languageDescriptionType: 5
        };

        this.clusterLanguageDescriptionModal.show(data);
    }

    private _create(body: CreateOrUpdateDelAggVariantInput) {
        this.isLoading = true;

        this._delAggVariantServiceProxy
            .createOrUpdateDelAggVariant(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));

                if (!this.isModal) {
                    this.back();
                } else {
                    this.closeModal(true);
                }
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggVariantServiceProxy
            .getDelAggVariantForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggVariant = result.delAggVariant;
                this.delAggImage = result.delAggImage;
            });
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }

    private _getVariantGroups() {
        this._delAggVariantGroupServiceProxy
            .getNames()
            .subscribe((result) => (this.delaggVariantGroups = result.items));
    }
}

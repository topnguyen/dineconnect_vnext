import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, LocationDto, LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    DelAggLocationListDto,
    DelAggLocMappingServiceProxy,
    PagedResultDtoOfLocationDto
} from '@shared/service-proxies/service-proxies-nswag';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-location-mapping.component.html',
    styleUrls: ['./cluster-location-mapping.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterLocationMappingComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectPopupComponent;

    filterText;
    isDeleted: boolean;
    selectedLocation: LocationDto; 
    delAggTypeRef: ComboboxItemDto;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;

    delAggTypes: ComboboxItemDto[];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _locationServiceProxy: LocationServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getDelAggType();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggLocMappingServiceProxy
            .getAll(
                this.filterText,
                this.delAggTypeRef?.value as any,
                this.selectedLocation?.id,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/location-mapping/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggLocMappingServiceProxy.deleteDelAggLocMapping(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggLocMappingServiceProxy
            .getAllToExcel(
                this.filterText,
                this.delAggTypeRef?.value as any,
                this.selectedLocation?.id,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    openLocation() {
        this.selectLocationModal.show();
    }

    clearLocation() {
        this.selectedLocation = undefined;
    }

    changeLocation(data) {
        this.selectedLocation = data;
    }

    getLocationForCombobox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(filterString, false, undefined, sorting, maxResultCount, skipCount);
        };
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.isDeleted = undefined;
        this.selectedLocation = undefined;
        this.delAggTypeRef = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy.getDelAggTypeForCombobox().subscribe((result) => (this.delAggTypes = result.items));
    }
}

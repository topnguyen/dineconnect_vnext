import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, GrabServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggLocMappingInput,
    DelAggLocationListDto,
    DelAggLocMappingEditDto,
    DelAggLocMappingServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './add-edit-cluster-location-mapping.component.html',
    styleUrls: ['./add-edit-cluster-location-mapping.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterLocationMappingComponent extends AppComponentBase implements OnInit {
    id: number;
    isLoading: boolean;
    locationMapping = new DelAggLocMappingEditDto();
    delAggTypes: ComboboxItemDto[];
    locations: DelAggLocationListDto[];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _grabServiceProxy: GrabServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        if (this.id != NaN && this.id > 0) {
            this._get();
        }

        this._getDelAggLocation();
        this._getDelAggType();
    }

    submit() {
        const body = new CreateOrUpdateDelAggLocMappingInput();
        body.delAggLocMapping = this.locationMapping;

        this._create(body);
    }

    syncData() {
        this._grabServiceProxy.grabSyncData(this.locationMapping.remoteCode).subscribe((result) => {
            if (result) {
                this.notify.success(this.l('Done'));
            }
        });
    }

    back() {
        this._router.navigate(['app/cluster/location-mapping']);
    }

    private _create(body: CreateOrUpdateDelAggLocMappingInput) {
        this.isLoading = true;

        this._delAggLocMappingServiceProxy
            .createOrUpdateDelAggLocMapping(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggLocMappingServiceProxy
            .getDelAggLocMappingForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.locationMapping = result.delAggLocMapping;
            });
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy.getDelAggTypeForCombobox().subscribe((result) => {
            this.delAggTypes = result.items;
        });
    }

    private _getDelAggLocation() {
        this._delAggLocMappingServiceProxy.getAggLocationNames().subscribe((result) => {
            this.locations = result.items;
        });
    }
}

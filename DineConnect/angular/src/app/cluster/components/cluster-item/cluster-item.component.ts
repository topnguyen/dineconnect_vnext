import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MenuItemServiceProxy, PagedResultDtoOfMenuItemListDto } from '@shared/service-proxies/service-proxies';
import {
    DelAggCategoryListDto,
    DelAggCategoryServiceProxy,
    DelAggItemGroupListDto,
    DelAggItemGroupServiceProxy,
    DelAggItemServiceProxy,
    DelAggLocationGroupListDto,
    DelAggLocationListDto,
    DelAggTaxListDto,
    DelAggTaxServiceProxy,
} from '@shared/service-proxies/service-proxies-nswag';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { EMPTY, Observable, Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
import { ImportModalComponent } from '@app/cluster/shared/import-modal/import-modal.component';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './cluster-item.component.html',
    styleUrls: ['./cluster-item.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterItemComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectMenuItemModal', { static: true }) selectMenuItemModal: SelectPopupComponent;
    @ViewChild('selectMenuItemPortionModal', { static: true }) selectMenuItemPortionModal: SelectPopupComponent;
    @ViewChild('importModal', { static: true }) importModal: ImportModalComponent;

    filterText;
    selectedCategory;
    selectedTax;
    menuItem;
    menuItemPortion;
    selectedItemGroup: DelAggLocationGroupListDto;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;

    itemGroups: DelAggItemGroupListDto[] = [];
    categories: DelAggCategoryListDto[] = [];
    taxes: DelAggTaxListDto[] = [];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggItemServiceProxy: DelAggItemServiceProxy,
        private _delAggCategoryServiceProxy: DelAggCategoryServiceProxy,
        private _delAggItemGroupServiceProxy: DelAggItemGroupServiceProxy,
        private _delTaxServiceProxy: DelAggTaxServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.configImport();
        this._getItemGroups();
        this._getCategorys();
        this._getTaxes();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    configImport() {
        this.importModal.configure({
            title: this.l('Item'),
            remoteServiceBaseUrl: AppConsts.remoteServiceBaseUrl + "ClusterImport/ImportDelAggItemData",
             
            downloadTemplate: () => {
                return EMPTY;
            },
            import: (fileToken: string) => {
                return EMPTY;
            }
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggItemServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.selectedCategory?.id,
                this.selectedItemGroup?.id,
                this.selectedTax?.id,
                this.menuItem?.id,
                this.menuItemPortion?.id,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/item/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggItemServiceProxy.deleteDelAggItem(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggItemServiceProxy
            .getAllToExcel(
                this.filterText || undefined,
                undefined,
                this.selectedCategory?.id,
                this.selectedItemGroup?.id,
                this.selectedTax?.id,
                this.menuItem?.id,
                this.menuItemPortion?.id,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getMenuItem() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfMenuItemListDto> => {
            return this._menuItemServiceProxy.getAll(
                filterString,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                sorting,
                skipCount,
                maxResultCount
            );
        };
    }

    getMenuItemPortion() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<any> => {
            return this._menuItemServiceProxy.getAllPortionItems(
                filterString,
                undefined,
                this.menuItem?.id,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    openMenuItem() {
        this.selectMenuItemModal.show();
    }

    clearMenuItem() {
        this.menuItem = undefined;
    }

    changeMenuItem(data) {
        this.menuItem = data;
    }

    openMenuItemPortion() {
        this.selectMenuItemPortionModal.show();
    }

    clearMenuItemPortion() {
        this.menuItemPortion = undefined;
    }

    changeMenuItemPortion(data) {
        this.menuItemPortion = data;
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.selectedItemGroup = undefined;
        this.selectedCategory = undefined;
        this.selectedTax = undefined;
        this.menuItem = undefined;
        this.menuItemPortion = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    cloneDelAggItem() {
        this._delAggItemServiceProxy.initiateBackGroundClone().subscribe(() => {
            this.getAll();

            this.notify.success(this.l('Job is running in Background'));
        });
    }

    openHangFire() {
        var url = '';
        url = 'http://' + window.location.host + '/hangfire';
        window.open(url);
    }

    importDelAggItem() {
        this.importModal.show();
    }

    onModalSave() {
        setTimeout(() => {
            this.getAll();
        }, 3000);
    }

    private _getItemGroups() {
        this._delAggItemGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.itemGroups = result.items));
    }

    private _getCategorys() {
        this._delAggCategoryServiceProxy
            .getAll(undefined, undefined, undefined, undefined, undefined)
            .subscribe((result) => (this.categories = result.items));
    }

    private _getTaxes() {
        this._delTaxServiceProxy
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined)
            .subscribe((result) => (this.taxes = result.items));
    }
}

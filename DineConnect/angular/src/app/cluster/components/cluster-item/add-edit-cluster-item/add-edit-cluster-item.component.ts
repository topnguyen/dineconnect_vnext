import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
    MenuItemServiceProxy,
    PagedResultDtoOfMenuItemListDto
} from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggItemInput,
    DelAggCategoryServiceProxy,
    DelAggImageEditDto,
    DelAggItemEditDto,
    DelAggItemGroupServiceProxy,
    DelAggItemServiceProxy,
    DelAggLocMappingServiceProxy,
    DelAggModifierGroupItemEditDto,
    DelAggTaxServiceProxy,
    DelAggVariantEditDto,
    DelAggVariantGroupEditDto
} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';

@Component({
    templateUrl: './add-edit-cluster-item.component.html',
    styleUrls: ['./add-edit-cluster-item.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterItemComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectMenuItemModal', { static: true }) selectMenuItemModal: SelectPopupComponent;
    @ViewChild('selectMenuItemPortionModal', { static: true }) selectMenuItemPortionModal: SelectPopupComponent;
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;

    id: number;
    catId: number;
    isLoading: boolean;
    delAggItem = new DelAggItemEditDto();
    delAggImage: DelAggImageEditDto[] = [];
    delAggImageItem = new DelAggImageEditDto();

    itemgroups: any[] = [];
    categories: any[] = [];
    taxes: any[] = [];
    modifiergroups: any[] = [];

    delAggModifierGroupItem: DelAggModifierGroupItemEditDto[];
    delAggVariantGroup = new DelAggVariantGroupEditDto();
    delAggVariants: DelAggVariantEditDto[] = [];
    selecteddelAggVariant: DelAggVariantEditDto;

    delAggTypes: ComboboxItemDto[];
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggItemServiceProxy: DelAggItemServiceProxy,
        private _delAggItemGroupServiceProxy: DelAggItemGroupServiceProxy,
        private _delAggCategoryServiceProxy: DelAggCategoryServiceProxy,
        private _delTaxServiceProxy: DelAggTaxServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
        this.catId = +this._activatedRoute.snapshot.queryParams['catId'];
    }

    ngOnInit(): void {
        this._getItemGroup();
        this._getCategorys();
        this._getTaxes();
        this._getDelAggType();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggItemInput();
        body.delAggItem = this.delAggItem;
        body.delAggImage = this.delAggImage;
        body.delAggModifierGroupItem = this.delAggModifierGroupItem;
        body.delAggVariantGroup = this.delAggVariantGroup;
        body.delAggVariant = this.delAggVariants;
        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/item']);
    }

    openforMenuItem() {
        this.selectMenuItemModal.show();
    }

    clearMenuItem() {
        this.delAggItem.menuItemId = undefined;
        this.delAggItem.menuItemName = undefined;
        this.delAggVariantGroup.name = undefined;
    }

    changeMenuItem(result) {
        this.delAggItem.menuItemId = result.id;
        this.delAggItem.menuItemName = result.name;
        this.delAggVariantGroup.name = this.delAggItem.menuItemId + ' - ' + this.delAggItem.menuItemName;
    }

    getMenuItem() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfMenuItemListDto> => {
            return this._menuItemServiceProxy.getAll(
                filterString,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                sorting,
                skipCount,
                maxResultCount
            );
        };
    }

    openforMenuItemPortions(data: DelAggVariantEditDto) {
        this.selecteddelAggVariant = data;
        this.selectMenuItemPortionModal.show();
    }

    clearMenuItemPortion() {
        this.selecteddelAggVariant.menuItemPortionId = undefined;
        this.selecteddelAggVariant.menuItemPortionName = undefined;
        this.selecteddelAggVariant.name = undefined
    }

    changeMenuItemPortion(result) {
        this.selecteddelAggVariant.menuItemPortionId = result.id;
        this.selecteddelAggVariant.menuItemPortionName = result.name;
        this.selecteddelAggVariant.name =
            this.selecteddelAggVariant.menuItemPortionId + ' - ' + this.selecteddelAggVariant.menuItemPortionName;    
    }

    getMenuItemPortion() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<any> => {
            return this._menuItemServiceProxy.getAllPortionItems(
                filterString,
                undefined,
                this.delAggItem.menuItemId,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    addPortion() {
        const row: DelAggVariantEditDto = DelAggVariantEditDto.fromJS({
            name: null,
            code: null,
            delAggVariantGroupId: 0,
            menuItemPortionId: 0,
            menuItemPortionName: null,
            menuItemId: this.delAggItem.menuItemId,
            salesPrice: 0,
            markupPrice: 0,
            sortOrder: 1
        });

        this.delAggVariants.push(row);
    }

    removeRow(index) {
        this.delAggVariants.splice(index, 1);
    }

    remove(index: number) {
        this.delAggImage.splice(index, 1);
    }

    download(file: DelAggImageEditDto) {
        location.href = JSON.parse(file.imagePath).url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        file.upload();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.delAggImageItem.imagePath = JSON.stringify({
                    fileTag: 'Image',
                    fileName: ajaxResponse.result.fileName,
                    fileType: ajaxResponse.result.fileType,
                    fileToken: ajaxResponse.result.fileToken,
                    url: ajaxResponse.result.url
                });
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: false
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    addImage() {
        if (this.delAggTypeRef) {
            const isExist = this.delAggImage.some((item) => item.delAggTypeRefId == +this.delAggTypeRef.value);
            if (isExist) {
                this.notify.error('Del Agg Type existed');
                return;
            }

            this.delAggImageItem.delAggTypeRefId = +this.delAggTypeRef.value;
            this.delAggImageItem.delAggTypeRefName = this.delAggTypeRef.displayText;

            this.delAggImage.push(new DelAggImageEditDto(this.delAggImageItem));
        }
    }

    toJSON(data: string) {
        return JSON.parse(data);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.delAggItem.id,
            language: this.delAggItem.name,
            languageDescriptionType: 2
        };

        this.clusterLanguageDescriptionModal.show(data);
    }

    private _create(body: CreateOrUpdateDelAggItemInput) {
        this.isLoading = true;

        this._delAggItemServiceProxy
            .createOrUpdateDelAggItem(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggItemServiceProxy
            .getDelAggItemForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggItem = result.delAggItem;
                this.delAggImage = result.delAggImage;
                this.delAggVariants = result.delAggVariant;
                this.delAggVariantGroup = result.delAggVariantGroup;
                this.delAggModifierGroupItem = result.delAggModifierGroupItem;

                if (this.delAggVariants.length == 0) {
                    this.addPortion();
                }
            });
    }

    private _getItemGroup() {
        this._delAggItemGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.itemgroups = result.items));
    }

    private _getCategorys() {
        this._delAggCategoryServiceProxy
            .getAll(undefined, undefined, undefined, undefined, undefined)
            .subscribe((result) => {
                this.categories = result.items

                if (this.catId) {
                    this.delAggItem.delAggCatId = this.catId
                }
            });
    }

    private _getTaxes() {
        this._delTaxServiceProxy
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined)
            .subscribe((result) => (this.taxes = result.items));
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }
}

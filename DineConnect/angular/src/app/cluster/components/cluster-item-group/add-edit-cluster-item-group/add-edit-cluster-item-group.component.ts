import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDelAggItemGroupInput,
    DelAggItemGroupEditDto,
    DelAggItemGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';

@Component({
    templateUrl: './add-edit-cluster-item-group.component.html',
    styleUrls: ['./add-edit-cluster-item-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterItemGroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;

    id: number;
    isLoading: boolean;
    itemGroup = new DelAggItemGroupEditDto();

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggItemGroupServiceProxy: DelAggItemGroupServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggItemGroupInput();
        body.delAggItemGroup = this.itemGroup;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/item-group']);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.itemGroup.id,
            language: this.itemGroup.name,
            languageDescriptionType: 1
        }

        this.clusterLanguageDescriptionModal.show(data);
    }

    private _create(body: CreateOrUpdateDelAggItemGroupInput) {
        this.isLoading = true;

        this._delAggItemGroupServiceProxy
            .createOrUpdateDelAggItemGroup(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggItemGroupServiceProxy
            .getDelAggItemGroupForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.itemGroup = result.delAggItemGroup;
            });
    }
}

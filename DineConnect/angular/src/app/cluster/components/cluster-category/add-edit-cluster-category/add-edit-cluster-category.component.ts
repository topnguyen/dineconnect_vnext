import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDelAggCategoryInput,
    DelAggCategoryEditDto,
    DelAggCategoryServiceProxy,
    DelTimingGroupListDto,
    DelTimingGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';

@Component({
    templateUrl: './add-edit-cluster-category.component.html',
    styleUrls: ['./add-edit-cluster-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;

    id: number;
    isLoading: boolean;
    category = new DelAggCategoryEditDto();
    timingGroups: DelTimingGroupListDto[];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggCategoryServiceProxy: DelAggCategoryServiceProxy,
        private _delTimingGroupServiceProxy: DelTimingGroupServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        if (this.id != NaN && this.id > 0) {
            this._get();
        }

        this._getTimingGroup();
    }

    submit() {
        const body = new CreateOrUpdateDelAggCategoryInput();
        body.delAggCategory = this.category;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/category']);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.category.id,
            language: this.category.name,
            languageDescriptionType: 1
        };

        this.clusterLanguageDescriptionModal.show(data);
    }

    private _create(body: CreateOrUpdateDelAggCategoryInput) {
        this.isLoading = true;

        this._delAggCategoryServiceProxy
            .createOrUpdateDelAggCategory(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggCategoryServiceProxy
            .getDelAggCategoryForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.category = result.delAggCategory;
            });
    }

    private _getTimingGroup() {
        this._delTimingGroupServiceProxy.getTimingGroupNames().subscribe((result) => {
            this.timingGroups = result.items;
        });
    }
}

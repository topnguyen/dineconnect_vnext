import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SortModalComponent } from '@app/shared/common/sort-modal/sort-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DelAggCategoryListDto,
    DelAggCategoryServiceProxy,
    DelAggItemListDto
} from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-category.component.html',
    styleUrls: ['./cluster-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('sortModal', { static: true }) sortModal: SortModalComponent;

    categories: DelAggCategoryListDto[] = [];
    currentCate: DelAggCategoryListDto;
    subItems: DelAggItemListDto[] = [];
    currentTag;
    permissions;
    spanFlag: boolean;
    loading: boolean;

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggCategoryServiceProxy: DelAggCategoryServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getAll();
    }

    getAll() {
        this.primengTableHelper.showLoadingIndicator();
        this._delAggCategoryServiceProxy
            .getDelAggCategories(
                undefined,
                undefined,
                undefined,
                'All',
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.categories = result.items;
            });
    }

    addOrEdit(item?: DelAggCategoryListDto) {
        this._router.navigate(['app/cluster/category/', item ? item.id : 'new']);
    }

    sort() {
        this.sortModal.show(
            this._delAggCategoryServiceProxy.getAll(undefined, undefined, undefined, undefined, undefined)
        );
    }

    saveSort(options) {
        const ids = options.map((x) => x.id);

        this._delAggCategoryServiceProxy.saveSortDelAggCategories(ids).subscribe((result) => {
            this.notify.success(this.l('SavedSuccessfully'));
            this.getAll();
        });
    }

    refresh() {
        this.getAll();
    }

    editCategory(item) {
        this._router.navigate(['app/cluster/category', item.id]);
    }

    createItem(item) {
        this._router.navigate(['app/cluster/item', 'new'], { queryParams: { catId: item.id } });
    }

    loadItem(cate: DelAggCategoryListDto) {
        var colCount = cate.categoryColumnCount === 0 ? 3 : cate.categoryColumnCount;
        this.currentCate = cate;
        this.getItemsForCategoryTag('');
    }

    getItemsForCategoryTag(tag: string) {
        this._delAggCategoryServiceProxy
            .getItemForVisualScreen(
                undefined,
                this.currentCate.id,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .pipe(
                finalize(() => {
                    this.currentTag = tag;
                })
            )
            .subscribe((result) => {
                this.subItems = result.delAggItems;
                this.spanFlag = true;
            });
    }

    getCateName(item: DelAggCategoryListDto) {
        return item.name.replace('\n', '<br/>').replace('/r', '<br/>').replace('\r', '<br/>');
    }

    getItemName(name) {
        return name.replace('\n', '<br/>').replace('/r', '<br/>').replace('\r', '<br/>');
    }

    getTagName(name) {
        return name.split(',').pop().trim();
    }
}

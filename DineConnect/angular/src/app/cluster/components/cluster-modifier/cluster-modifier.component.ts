import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MenuItemServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    DelAggLocationListDto,
    DelAggModifierGroupListDto,
    DelAggModifierGroupServiceProxy,
    DelAggModifierServiceProxy,
    OrderTagDto,
    OrderTagGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-modifier.component.html',
    styleUrls: ['./cluster-modifier.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterModifierComponent extends AppComponentBase implements OnInit {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectMenuItemPortionModal', { static: true }) selectMenuItemPortionModal: SelectPopupComponent;

    filterText;
    menuItemPortion;
    selectedModifierGroup: DelAggModifierGroupListDto;
    selectedOrderTag: any;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;
    modifierGroups: DelAggModifierGroupListDto[] = [];
    orderTags: OrderTagDto[] = [];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggModifierServiceProxy: DelAggModifierServiceProxy,
        private _delAggModifierGroupServiceProxy: DelAggModifierGroupServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getModifierGroups();
        this._getOrderTags();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggModifierServiceProxy
            .getAll(
                this.filterText,
                this.selectedModifierGroup?.id,
                this.menuItemPortion?.id,
                undefined,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/modifier/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggModifierServiceProxy.deleteDelAggModifier(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggModifierServiceProxy
            .getAllToExcel(
                this.filterText,
                this.selectedModifierGroup?.id,
                this.menuItemPortion?.id,
                undefined,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getMenuItemPortion() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<any> => {
            return this._menuItemServiceProxy.getAllPortionItems(
                filterString,
                undefined,
                undefined,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    openMenuItemPortion() {
        this.selectMenuItemPortionModal.show();
    }

    clearMenuItemPortion() {
        this.menuItemPortion = undefined;
    }

    changeMenuItemPortion(data) {
        this.menuItemPortion = data;
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.selectedModifierGroup = undefined;
        this.menuItemPortion = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getModifierGroups() {
        this._delAggModifierGroupServiceProxy
            .getAll(undefined, undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.modifierGroups = result.items));
    }

    private _getOrderTags() {
        this._orderTagGroupServiceProxy
            .getOrderTagNames()
            .subscribe((result) => (this.orderTags = result.items));
    }
}

import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, CommonServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelTimingGroupInput,
    DelTimingDetailEditDto,
    DelTimingGroupEditDto,
    DelTimingGroupServiceProxy,
    IDelTimingDetailEditDto
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    templateUrl: './add-edit-cluster-timing-group.component.html',
    styleUrls: ['./add-edit-cluster-timing-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterTimingGroupComponent extends AppComponentBase implements OnInit {
    id: number;
    isLoading: boolean;
    delTimingGroup = new DelTimingGroupEditDto({
        delTimingDetails: []
    } as any);
    refdays: ComboboxItemDto[] = [];
    refday: ComboboxItemDto[] = [];
    delTimingDetail: Partial<IDelTimingDetailEditDto & { startTime?: Date; endTime?: Date }> = {
        startTime: moment('06:30', 'HH:mm').toDate(),
        endTime: moment('11:30', 'HH:mm').toDate()
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delTimingGroupServiceProxy: DelTimingGroupServiceProxy,
        private _commonServiceProxy: CommonServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getDays();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelTimingGroupInput();
        body.delTimingGroup = this.delTimingGroup;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/timing-group']);
    }

    addDetail() {
        const item = new DelTimingDetailEditDto({
            startHour: this.delTimingDetail.startTime.getHours(),
            startMinute: this.delTimingDetail.startTime.getMinutes(),
            endHour: this.delTimingDetail.endTime.getHours(),
            endMinute: this.delTimingDetail.endTime.getMinutes(),
            allDays: this.refday,
            days: this.refday.map((item) => item.value).join(',')
        } as any);
        this.delTimingGroup.delTimingDetails.push(item);

        this.refday = [];
        this.delTimingDetail.startTime = undefined;
        this.delTimingDetail.endTime = undefined;
    }

    remove(index) {
        this.delTimingGroup.delTimingDetails.splice(index, 1);
    }

    private _create(body: CreateOrUpdateDelTimingGroupInput) {
        this.isLoading = true;

        this._delTimingGroupServiceProxy
            .createOrUpdateDelTimingGroup(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delTimingGroupServiceProxy
            .getDelTimingGroupForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delTimingGroup = result.delTimingGroup;
            });
    }

    private _getDays() {
        this._commonServiceProxy.getDays().subscribe((result) => (this.refdays = result));
    }
}

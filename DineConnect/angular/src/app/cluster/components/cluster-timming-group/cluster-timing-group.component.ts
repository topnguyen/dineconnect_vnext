import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DelTimingGroupListDto, DelTimingGroupServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './cluster-timing-group.component.html',
  styleUrls: ['./cluster-timing-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class ClusterTimingGroupComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    lazyLoadEvent: LazyLoadEvent;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delTimingGroupServiceProxy: DelTimingGroupServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delTimingGroupServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelTimingGroupListDto) {
        this._router.navigate(['app/cluster/timing-group/', item ? item.id : 'new']);
    }

    delete(item: DelTimingGroupListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delTimingGroupServiceProxy.deleteDelTimingGroup(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delTimingGroupServiceProxy
            .getAllToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DynamicTableComponent } from '@app/shared/common/dynamic-table/dynamic-table.component';
import { DTableColumn } from '@app/shared/common/dynamic-table/dynamic-table.model';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ApplicationLanguageListDto, LanguageServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggLanguageInput,
    DelAggLanguageEditDto,
    DelAggLanguageListDto,
    DelAggLanguageServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-cluster-language-description-modal',
    templateUrl: './cluster-language-description-modal.component.html',
    styleUrls: ['./cluster-language-description-modal.component.scss']
})
export class ClusterLanguageDescriptionModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('delAggLanguageModal', { static: true }) modal: ModalDirective;
    @ViewChild('dtable', { static: true }) dtable: DynamicTableComponent;

    id: number;
    language: string;
    delAggLanguage = new DelAggLanguageEditDto();
    reflanguageCode: ApplicationLanguageListDto[];
    saving = false;
    loading = false;
    filterText = '';
    languageDescriptionType;

    cols: DTableColumn[] = [
        {
            field: 'language',
            header: this.l('Language'),
            type: 'string'
        },
        {
            field: 'languageValue',
            header: this.l('LanguageValue'),
            type: 'string'
        },
        {
            field: null,
            header: this.l('Actions'),
            type: 'action'
        }
    ];

    constructor(
        injector: Injector,
        private _delAggLanguageServiceProxy: DelAggLanguageServiceProxy,
        private _languageService: LanguageServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getLanguageCode();
    }

    configDtable() {
        this.dtable.isShowPreferences = false;
        this.dtable.configure({
            cols: this.cols,
            isGrantedEdit: 'Pages.Tenant.Connect',
            isGrantedDelete: 'Pages.Tenant.Connect',
            dataSource: (sorting: string, skipCount: number, maxResultCount: number) => {
                return this._delAggLanguageServiceProxy.getAll(
                    this.filterText,
                    this.id,
                    undefined,
                    this.languageDescriptionType,
                    sorting,
                    maxResultCount,
                    skipCount
                );
            },
            edit: (item: DelAggLanguageListDto) => {
                this._delAggLanguageServiceProxy.getDelAggLanguageForEdit(item.id).subscribe((result) => {
                    this.delAggLanguage = {
                        ...result.delAggLanguage,
                        language: Number(result.delAggLanguage.language) as any
                    } as DelAggLanguageEditDto;
                });
            },
            delete: (item: DelAggLanguageListDto) => {
                this.message.confirm(this.l('DelAggLanguageWarningMessage'), this.l('AreYouSure'), (isConfirmed) => {
                    if (isConfirmed) {
                        this._delAggLanguageServiceProxy.deleteDelAggLanguage(item.id).subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                    }
                });
            },
            stateKey: 'cluster'
        });
    }

    reloadPage() {
        this.filterText = undefined;
        this.dtable.paginator.changePage(0);
    }

    search() {
        this.dtable.getAll();
    }

    show(data) {
        this.id = data.id;
        this.language = data.language;
        this.languageDescriptionType = data.languageDescriptionType;

        this.configDtable();
        this.modal.show();
    }

    onShown() {}

    close() {
        this.modal.hide();
    }

    clear() {
        this.delAggLanguage = new DelAggLanguageEditDto();
        this.filterText = undefined;
    }

    save(argSaveOption) {
        this.saving = true;
        this.delAggLanguage.referenceId = this.id;
        this.delAggLanguage.languageDescriptionType = this.languageDescriptionType;

        const body = new CreateOrUpdateDelAggLanguageInput({
            delAggLanguage: this.delAggLanguage
        });

        this._delAggLanguageServiceProxy
            .createOrUpdateDelAggLanguage(body)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                if (argSaveOption == 2) {
                    this.close();
                }
                this.clear();
                this.reloadPage();
            });
    }

    private _getLanguageCode() {
        this._languageService.getLanguages().subscribe((result) => {
            this.reflanguageCode = result.items;
        });
    }
}

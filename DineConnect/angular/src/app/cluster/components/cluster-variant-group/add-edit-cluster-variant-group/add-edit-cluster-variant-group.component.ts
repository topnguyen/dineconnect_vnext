import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggVariantGroupInput,
    DelAggImageEditDto,
    DelAggLocMappingServiceProxy,
    DelAggVariantGroupEditDto,
    DelAggVariantGroupServiceProxy,
    DelAggVariantListDto,
    DelAggVariantServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';
import { AddEditClusterVariantModalComponent } from '../add-edit-cluster-variant-modal/add-edit-cluster-variant-modal.component';

@Component({
    templateUrl: './add-edit-cluster-variant-group.component.html',
    styleUrls: ['./add-edit-cluster-variant-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterVariantGroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;
    @ViewChild('addEditClusterVariantModal', { static: true })
    addEditClusterVariantModal: AddEditClusterVariantModalComponent;

    id: number;
    isLoading: boolean;
    delAggVariantGroup = new DelAggVariantGroupEditDto();
    delAggImage: DelAggImageEditDto[] = [];
    delAggImageItem = new DelAggImageEditDto();
    delaggVariantItems: DelAggVariantListDto[] = [];

    delAggTypes: ComboboxItemDto[];
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggVariantGroupServiceProxy: DelAggVariantGroupServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _delAggVariantServiceProxy: DelAggVariantServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getDelAggType();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggVariantGroupInput();
        body.delAggVariantGroup = this.delAggVariantGroup;
        body.delAggImage = this.delAggImage;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/variant-group']);
    }

    addVariant() {
        this.addEditClusterVariantModal.show({
            id: 0,
            delAggVariantGroupId: this.delAggVariantGroup.id
        });
    }

    deleteVariant(item) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggVariantServiceProxy.deleteDelAggVariant(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this._getVariants();
                });
            }
        });
    }

    editVariant(id) {
        this.addEditClusterVariantModal.show({
            id: id,
            delAggVariantGroupId: this.delAggVariantGroup.id
        });
    }

    remove(index: number) {
        this.delAggImage.splice(index, 1);
    }

    download(file: DelAggImageEditDto) {
        location.href = JSON.parse(file.imagePath).url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        file.upload();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.delAggImageItem.imagePath = JSON.stringify({
                    fileTag: 'Image',
                    fileName: ajaxResponse.result.fileName,
                    fileType: ajaxResponse.result.fileType,
                    fileToken: ajaxResponse.result.fileToken,
                    url: ajaxResponse.result.url
                });
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: false
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    addImage() {
        if (this.delAggTypeRef) {
            const isExist = this.delAggImage.some(
                (item) => item.delAggTypeRefId == +this.delAggTypeRef.value
            );
            if (isExist) {
                this.notify.error('Del Agg Type existed');
                return;
            }

            this.delAggImageItem.delAggTypeRefId = +this.delAggTypeRef.value;
            this.delAggImageItem.delAggTypeRefName = this.delAggTypeRef.displayText;

            this.delAggImage.push(new DelAggImageEditDto(this.delAggImageItem));
        }
    }

    toJSON(data: string) {
        return JSON.parse(data);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.delAggVariantGroup.id,
            language: this.delAggVariantGroup.name,
            languageDescriptionType: 4
        };

        this.clusterLanguageDescriptionModal.show(data);
    }

    onSavedVariant() {
        this._getVariants();
    }

    private _create(body: CreateOrUpdateDelAggVariantGroupInput) {
        this.isLoading = true;

        this._delAggVariantGroupServiceProxy
            .createOrUpdateDelAggVariantGroup(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggVariantGroupServiceProxy
            .getDelAggVariantGroupForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggVariantGroup = result.delAggVariantGroup;
                this.delAggImage = result.delAggImage;

                this._getVariants();
            });
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }

    private _getVariants() {
        this._delAggVariantServiceProxy
            .getAll(
                undefined,
                this.delAggVariantGroup.id,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .subscribe((result) => (this.delaggVariantItems = result.items));
    }
}

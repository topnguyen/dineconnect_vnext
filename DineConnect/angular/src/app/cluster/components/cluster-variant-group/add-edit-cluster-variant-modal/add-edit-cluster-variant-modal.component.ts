import { ChangeDetectorRef, Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AddEditClusterVariantComponent } from '../../cluster-variant/add-edit-cluster-variant/add-edit-cluster-variant.component';

@Component({
    selector: 'app-add-edit-cluster-variant-modal',
    templateUrl: './add-edit-cluster-variant-modal.component.html',
    styleUrls: ['./add-edit-cluster-variant-modal.component.scss']
})
export class AddEditClusterVariantModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('delAggLanguageModal', { static: true }) modal: ModalDirective;
    @ViewChild('addEditClusterVariant', { static: false })
    addEditClusterVariant: AddEditClusterVariantComponent;

    data = {
        id: 0,
        delAggVariantGroupId: 0
    };
    isActive = false;

    @Output() saved = new EventEmitter<any>();

    constructor(injector: Injector, private changeDetector: ChangeDetectorRef) {
        super(injector);
    }

    ngOnInit(): void {}

    show(data) {
        this.data = data;
        this.isActive = true;
        this.changeDetector.detectChanges();

        setTimeout(() => {
            this.addEditClusterVariant.id = data.id;
            this.addEditClusterVariant.delAggVariant.delAggVariantGroupId = data.delAggVariantGroupId;
            this.addEditClusterVariant.ngOnInit();
            this.addEditClusterVariant.closeModal = (isSaved) => {
                if (isSaved) {
                    this.saved.emit();
                }
                this.close();
            };

            this.modal.show();
        }, 100);
    }

    onShown() {}

    close() {
        this.isActive = false;
        this.modal.hide();
    }
}

import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DelAggItemListDto, DelAggItemServiceProxy, DelAggLocationItemServiceProxy, DelAggLocationListDto, DelAggLocMappingListDto, DelAggLocMappingServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-item-activation.component.html',
    styleUrls: ['./cluster-item-activation.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterItemActivationComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;
    delAggLocMappingId;
    delAggItemId;
    delAggVariantId;
    delAggModifierId;
    delAggPriceTypeRefId;
    inActive;

    refLocMap: DelAggLocMappingListDto[] = []
    refItem: DelAggItemListDto[] = []

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggLocationItemServiceProxy: DelAggLocationItemServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _delAggItemServiceProxy: DelAggItemServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getLocaionMapping();
        this._getItem();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggLocationItemServiceProxy
            .getAll(
                this.filterText,
                this.delAggLocMappingId,
                this.delAggItemId,
                this.delAggVariantId,
                this.delAggModifierId,
                this.delAggPriceTypeRefId,
                this.inActive,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/item-activation/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggLocationItemServiceProxy.deleteDelAggLocationItem(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggLocationItemServiceProxy
            .getAllToExcel(
                this.filterText,
                this.delAggLocMappingId,
                this.delAggItemId,
                this.delAggVariantId,
                this.delAggModifierId,
                this.delAggPriceTypeRefId,
                this.inActive,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.delAggLocMappingId = undefined;
        this.delAggItemId = undefined;
        this.delAggVariantId = undefined;
        this.delAggModifierId = undefined;
        this.delAggPriceTypeRefId = undefined;
        this.inActive = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getLocaionMapping() {
        this._delAggLocMappingServiceProxy.getAggLocationMapNames().subscribe( (result) => {
            this.refLocMap = result.items;
        });
    }

    private _getItem() {
        this._delAggItemServiceProxy.getNames().subscribe( (result) => {
            this.refItem = result.items;
        });
    }
}

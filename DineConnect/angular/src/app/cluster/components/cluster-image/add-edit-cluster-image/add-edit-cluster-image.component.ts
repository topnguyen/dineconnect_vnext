import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
} from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggImageInput,
    DelAggImageEditDto,
    DelAggImageServiceProxy,
    DelAggLocMappingServiceProxy,

} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './add-edit-cluster-image.component.html',
    styleUrls: ['./add-edit-cluster-image.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterImageComponent extends AppComponentBase implements OnInit {

    id: number;
    isLoading: boolean;
    delAggImage = new DelAggImageEditDto();
    imagePaths: any[] = [];

    delAggImageTypes: ComboboxItemDto[] = [];
    delAggTypes: ComboboxItemDto[];
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggImageServiceProxy: DelAggImageServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getDelAggType();
        this._getDelAggImageType();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggImageInput();
        body.delAggImage = this.delAggImage;
        body.delAggImage.imagePath = JSON.stringify(this.imagePaths);

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/image']);
    }

    remove(index: number) {
        this.imagePaths.splice(index, 1);
    }

    download(file: any) {
        location.href = file.url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.imagePaths.push(
                    {
                        fileTag: 'Image',
                        fileName: ajaxResponse.result.fileName,
                        fileType: ajaxResponse.result.fileType,
                        fileToken: ajaxResponse.result.fileToken,
                        url: ajaxResponse.result.url
                    }
                );
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: true
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    private _create(body: CreateOrUpdateDelAggImageInput) {
        this.isLoading = true;

        this._delAggImageServiceProxy
            .createOrUpdateDelAggImage(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggImageServiceProxy
            .getDelAggImageForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggImage = result.delAggImage;
                this.imagePaths = JSON.parse(result.delAggImage.imagePath);
            });
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }

    private _getDelAggImageType() {
        this._delAggLocMappingServiceProxy
            .getDelAggImageTypeForCombobox()
            .subscribe((result) => (this.delAggImageTypes = result.items));
    }
}

import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DelAggLocationListDto,
    DelAggImageListDto,
    DelAggImageServiceProxy,
    DelAggLocMappingServiceProxy,
    ComboboxItemDto
} from '@shared/service-proxies/service-proxies-nswag';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';
@Component({
    selector: 'app-cluster-image',
    templateUrl: './cluster-image.component.html',
    styleUrls: ['./cluster-image.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterImageComponent extends AppComponentBase implements OnInit {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectMenuItemPortionModal', { static: true }) selectMenuItemPortionModal: SelectPopupComponent;

    filterText;
    delAggTypeRefId;
    delAggImageTypeRefId;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;
    variantGroups: DelAggImageListDto[] = [];
    delAggTypes: ComboboxItemDto[] = [];
    delAggImageTypes: ComboboxItemDto[] = [];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggImageServiceProxy: DelAggImageServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getDelAggType();
        this._getDelAggImageType();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggImageServiceProxy
            .getAll(
                this.filterText,
                this.delAggTypeRefId,
                this.delAggImageTypeRefId,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/image/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggImageServiceProxy.deleteDelAggImage(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggImageServiceProxy
            .getAllToExcel(
                this.filterText,
                this.delAggTypeRefId,
                this.delAggImageTypeRefId,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    downloader(record) {
        const imagePath = record.argFile.imagePath ? JSON.parse(record.argFile.imagePath) : [];
        location.href =
            abp.appPath +
            'FileUpload/DownloadFile?fileName=' +
            imagePath[0].fileName +
            '&url=' +
            imagePath[0].fileSystemName;
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.delAggTypeRefId = undefined;
        this.delAggImageTypeRefId = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }

    private _getDelAggImageType() {
        this._delAggLocMappingServiceProxy
            .getDelAggImageTypeForCombobox()
            .subscribe((result) => (this.delAggImageTypes = result.items));
    }
}

import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto } from '@shared/service-proxies/service-proxies';
import {
    DelAggLocationListDto,
    DelAggTaxServiceProxy,
} from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-tax.component.html',
    styleUrls: ['./cluster-tax.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterTaxComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;

    taxTypes: ComboboxItemDto[];
    selectedTax: ComboboxItemDto;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggTaxServiceProxy: DelAggTaxServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getTaxType();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggTaxServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.selectedTax?.value as any,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/tax/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggTaxServiceProxy.deleteDelAggTax(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggTaxServiceProxy
            .getAllToExcel(
                this.filterText,
                undefined,
                this.selectedTax?.value as any,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.selectedTax = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getTaxType() {
        this._delAggTaxServiceProxy.getDelAggTaxTypeForCombobox().subscribe((result) => (this.taxTypes = result.items));
    }
}

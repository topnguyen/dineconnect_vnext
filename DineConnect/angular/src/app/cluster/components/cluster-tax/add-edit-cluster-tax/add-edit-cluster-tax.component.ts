import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggTaxInput,
    DelAggTaxEditDto,
    DelAggTaxMappingListDto,
    DelAggTaxServiceProxy,
    DelAggItemGroupListDto,
    DelAggItemGroupServiceProxy,
    DelAggLocationGroupListDto,
    DelAggLocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './add-edit-cluster-tax.component.html',
    styleUrls: ['./add-edit-cluster-tax.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterTaxComponent extends AppComponentBase implements OnInit {
    id: number;
    isLoading: boolean;
    tax = new DelAggTaxEditDto();
    delAggTaxMapping: DelAggTaxMappingListDto[] = [];
    taxTypes: ComboboxItemDto[] = [];
    locationGroups: DelAggLocationGroupListDto[];
    itemGroups: DelAggItemGroupListDto[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggTaxServiceProxy: DelAggTaxServiceProxy,
        private _delAggLocationGroupServiceProxy: DelAggLocationGroupServiceProxy,
        private _delAggItemGroupServiceProxy: DelAggItemGroupServiceProxy
    ) {
        super(injector);
        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getLocationGroup();
        this._getItemGroup();
        this._getTaxType();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }

        if (this.delAggTaxMapping.length == 0) {
            this.addMapping();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggTaxInput();
        body.delAggTax = this.tax;
        body.delAggTaxMapping = this.delAggTaxMapping;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/tax']);
    }

    addMapping() {
        const newRow = new DelAggTaxMappingListDto({
            delAggTaxId: 0,
            delAggLocationGroupId: undefined,
            delItemGroupId: undefined
        } as any);

        this.delAggTaxMapping.push(newRow);
    }

    removeMapping(index) {
        this.delAggTaxMapping.splice(index, 1);
    }

    private _create(body: CreateOrUpdateDelAggTaxInput) {
        this.isLoading = true;

        this._delAggTaxServiceProxy
            .createOrUpdateDelAggTax(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggTaxServiceProxy
            .getDelAggTaxForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.tax = result.delAggTax;
                this.delAggTaxMapping = result.delAggTaxMapping || [];
            });
    }

    private _getTaxType() {
        this._delAggTaxServiceProxy.getDelAggTaxTypeForCombobox().subscribe((result) => (this.taxTypes = result.items));
    }

    private _getLocationGroup() {
        this._delAggLocationGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.locationGroups = result.items));
    }


    private _getItemGroup() {
        this._delAggItemGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.itemGroups = result.items));
    }
}

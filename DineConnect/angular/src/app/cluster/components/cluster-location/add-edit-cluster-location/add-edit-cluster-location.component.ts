import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggLocationInput,
    DelAggImageEditDto,
    DelAggLocationEditDto,
    DelAggLocationGroupListDto,
    DelAggLocationGroupServiceProxy,
    DelAggLocationServiceProxy,
    DelAggLocMappingServiceProxy,
    PagedResultDtoOfLocationDto
} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
declare const $: any;

@Component({
    templateUrl: './add-edit-cluster-location.component.html',
    styleUrls: ['./add-edit-cluster-location.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterLocationComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectPopupComponent;

    id: number;
    isLoading: boolean;
    location = new DelAggLocationEditDto({
        group: false,
        locationTag: false,
        locations: null,
        nonLocations: null
    } as any);
    delAggImage: DelAggImageEditDto[];
    locationGroup: DelAggLocationGroupListDto[];
    delAggTypes: ComboboxItemDto[];
    delAggImageItem = new DelAggImageEditDto();
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggLocationServiceProxy: DelAggLocationServiceProxy,
        private _delAggLocationGroupServiceProxy: DelAggLocationGroupServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getLocationGroup();
        this._getDelAggType();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggLocationInput();
        body.delAggLocation = this.location;
        body.delAggImage = this.delAggImage;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/location']);
    }

    openLocation() {
        this.selectLocationModal.show();
    }

    clearLocation() {
        this.location.locationId = undefined;
        this.location.locationName = undefined;
    }

    changeLocation(data) {
        this.location.locationId = data.id;
        this.location.locationName = data.name;
    }

    getLocationForCombobox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(filterString, false, undefined, sorting, maxResultCount, skipCount);
        };
    }

    remove(index: number) {
        this.delAggImage.splice(index, 1);
    }

    download(file: DelAggImageEditDto) {
        location.href = JSON.parse(file.imagePath).url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        file.upload();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.delAggImageItem.imagePath = JSON.stringify({
                    fileTag: 'Image',
                    fileName: ajaxResponse.result.fileName,
                    fileType: ajaxResponse.result.fileType,
                    fileToken: ajaxResponse.result.fileToken,
                    url: ajaxResponse.result.url
                });
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: false
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    addImage() {
        if (this.delAggTypeRef) {
            const isExist = this.delAggImage.some(item => item.delAggTypeRefId == +this.delAggTypeRef.value);
            if (isExist) {
                this.notify.error('Del Agg Type existed');
                return;
            }

            this.delAggImageItem.delAggTypeRefId = +this.delAggTypeRef.value;
            this.delAggImageItem.delAggTypeRefName = this.delAggTypeRef.displayText;

            this.delAggImage.push(new DelAggImageEditDto(this.delAggImageItem));
        }
    }

    toJSON(data: string) {
        return JSON.parse(data);
    }

    private _create(body: CreateOrUpdateDelAggLocationInput) {
        this.isLoading = true;

        this._delAggLocationServiceProxy
            .createOrUpdateDelAggLocation(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggLocationServiceProxy
            .getDelAggLocationForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.location = result.delAggLocation;
                this.delAggImage = result.delAggImage;
            });
    }

    private _getLocationGroup() {
        this._delAggLocationGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.locationGroup = result.items));
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy.getDelAggTypeForCombobox().subscribe((result) => (this.delAggTypes = result.items));
    }
}

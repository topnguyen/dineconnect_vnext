import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import {
    DelAggLocationGroupListDto,
    DelAggLocationGroupServiceProxy,
    DelAggLocationListDto,
    DelAggLocationServiceProxy,
    PagedResultDtoOfLocationDto
} from '@shared/service-proxies/service-proxies-nswag';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable, Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './cluster-location.component.html',
    styleUrls: ['./cluster-location.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterLocationComponent extends AppComponentBase {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectPopupComponent;

    filterText;
    isDeleted: boolean;
    selectedLocation;
    selectedLocationGroup: DelAggLocationGroupListDto;
    lazyLoadEvent: LazyLoadEvent;
    isShowFilterOptions = false;

    locationGroup: DelAggLocationGroupListDto[];

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggLocationServiceProxy: DelAggLocationServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _locationServiceProxy: LocationServiceProxy,
        private _delAggLocationGroupServiceProxy: DelAggLocationGroupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._getLocationGroup();

        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggLocationServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                undefined,
                this.selectedLocation?.id,
                this.selectedLocationGroup?.id,
                undefined,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggLocationListDto) {
        this._router.navigate(['app/cluster/location/', item ? item.id : 'new']);
    }

    delete(item: DelAggLocationListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggLocationServiceProxy.deleteDelAggLocation(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggLocationServiceProxy.getAllToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    openLocation() {
        this.selectLocationModal.show();
    }

    clearLocation() {
        this.selectedLocation = undefined;
    }

    changeLocation(data) {
        this.selectedLocation = data;
    }

    getLocationForCombobox() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<PagedResultDtoOfLocationDto> => {
            return this._locationServiceProxy.getList(filterString, false, undefined, sorting, maxResultCount, skipCount);
        };
    }

    reloadPage() {
        this.paginator.changePage(0);
    }

    refresh() {
        this.clear();
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    apply() {
        this.reloadPage();
        this.isShowFilterOptions = false;
    }

    clear() {
        this.filterText = undefined;
        this.isDeleted = undefined;
        this.selectedLocation = undefined;
        this.selectedLocationGroup = undefined;
    }

    filter() {
        this.isShowFilterOptions = true;
    }

    search(event: string) {
        this.filter$.next(event);
    }

    private _getLocationGroup() {
        this._delAggLocationGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.locationGroup = result.items));
    }
}

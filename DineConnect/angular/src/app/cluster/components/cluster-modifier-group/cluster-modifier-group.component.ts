import { Component, Injector, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DelAggLocationListDto,
    DelAggModifierGroupServiceProxy,
    OrderTagGroupEditDto,
    OrderTagGroupServiceProxy,
} from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './cluster-modifier-group.component.html',
  styleUrls: ['./cluster-modifier-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class ClusterModifierGroupComponent extends AppComponentBase {
  @ViewChild('table', { static: true }) table: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  filterText;
  lazyLoadEvent: LazyLoadEvent;
  isShowFilterOptions = false;

  orderTagGroups: OrderTagGroupEditDto[];
  orderTagGroupId: number;

  private filter$ = new Subject();
  private destroy$: Subject<boolean> = new Subject();

  constructor(
      injector: Injector,
      private _router: Router,
      private _delAggModifierGroupServiceProxy: DelAggModifierGroupServiceProxy,
      private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy,
      private _fileDownloadService: FileDownloadService
  ) {
      super(injector);
  }

  ngOnInit(): void {
      this._getTagGroups();

      this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
          this.getAll();
      });
  }

  ngOnDestroy(): void {
      this.destroy$.next(true);
  }

  getAll(event?: LazyLoadEvent) {
      if (this.primengTableHelper.shouldResetPaging(event)) {
          this.paginator.changePage(0);

          return;
      }
      this.lazyLoadEvent = event;
      this.primengTableHelper.showLoadingIndicator();
      this._delAggModifierGroupServiceProxy
          .getAll(
              this.filterText,
              this.orderTagGroupId,
              this.primengTableHelper.getSorting(this.table),
              this.primengTableHelper.getMaxResultCount(this.paginator, event),
              this.primengTableHelper.getSkipCount(this.paginator, event)
          )
          .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
          .subscribe((result) => {
              this.primengTableHelper.totalRecordsCount = result.totalCount;
              this.primengTableHelper.records = result.items;
          });
  }
  addOrEdit(item?: DelAggLocationListDto) {
      this._router.navigate(['app/cluster/modifier-group/', item ? item.id : 'new']);
  }

  delete(item: DelAggLocationListDto) {
      this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
          if (isConfirmed) {
              this._delAggModifierGroupServiceProxy
                  .deleteDelAggModifierGroup(item.id)
                  .subscribe((result) => {
                      this.notify.success(this.l('SuccessfullyRemoved'));
                      this.getAll();
                  });
          }
      });
  }

  export() {
      this._delAggModifierGroupServiceProxy
          .getAllToExcel(
              this.filterText,
              this.orderTagGroupId,
              this.primengTableHelper.getSorting(this.table),
              this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
              this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
          )
          .subscribe((result) => {
              this._fileDownloadService.downloadTempFile(result);
          });
  }

  reloadPage() {
      this.paginator.changePage(0);
  }

  refresh() {
      this.clear();
      this.reloadPage();
      this.isShowFilterOptions = false;
  }

  apply() {
      this.reloadPage();
      this.isShowFilterOptions = false;
  }

  clear() {
      this.filterText = undefined;
      this.orderTagGroupId = undefined;
  }

  filter() {
      this.isShowFilterOptions = true;
  }

  search(event: string) {
      this.filter$.next(event);
  }

  private _getTagGroups() {
      this._orderTagGroupServiceProxy
          .getOrderTagGroupNames()
          .subscribe((result) => (this.orderTagGroups = result.items));
  }
}

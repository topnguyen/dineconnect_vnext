import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, OrderTagGroupListDto } from '@shared/service-proxies/service-proxies';
import {
    CreateOrUpdateDelAggItemInput,
    CreateOrUpdateDelAggModifierGroupInput,
    DelAggImageEditDto,
    DelAggItemListDto,
    DelAggItemServiceProxy,
    DelAggLocMappingServiceProxy,
    DelAggModifierGroupEditDto,
    DelAggModifierGroupItemEditDto,
    DelAggModifierGroupServiceProxy,
    DelAggModifierListDto,
    DelAggModifierServiceProxy,
    OrderTagGroupEditDto,
    OrderTagGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';
import { ClusterLanguageDescriptionModalComponent } from '../../cluster-language-description-modal/cluster-language-description-modal.component';
import { AddEditClusterModifierModalComponent } from '../add-edit-cluster-modifier-modal/add-edit-cluster-modifier-modal.component';

@Component({
    templateUrl: './add-edit-cluster-modifier-group.component.html',
    styleUrls: ['./add-edit-cluster-modifier-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterModifierGroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('clusterLanguageDescriptionModal', { static: true })
    clusterLanguageDescriptionModal: ClusterLanguageDescriptionModalComponent;
    @ViewChild('addEditClusterModifierModal', { static: true })
    addEditClusterModifierModal: AddEditClusterModifierModalComponent;

    id: number;
    isLoading: boolean;
    delAggModifierGroup = new DelAggModifierGroupEditDto();
    delAggImage: DelAggImageEditDto[] = [];
    delAggImageItem = new DelAggImageEditDto();
    delAggModifierGroupItems: DelAggModifierGroupItemEditDto[] = [];
    delAggItems: DelAggItemListDto[] = [];
    orderTagGroups: OrderTagGroupEditDto[];
    delaggModifierItems: DelAggModifierListDto[] = [];

    delAggTypes: ComboboxItemDto[];
    delAggTypeRef: ComboboxItemDto;
    uploader: FileUploader;
    uploadLoading = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _delAggModifierGroupServiceProxy: DelAggModifierGroupServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _delAggModifierServiceProxy: DelAggModifierServiceProxy,
        private _delAggItemServiceProxy: DelAggItemServiceProxy,
        private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getDelAggType();
        this._getDelAggItems();
        this._getTagGroups();
        this.initUploader();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggModifierGroupInput();
        body.delAggModifierGroup = this.delAggModifierGroup;
        body.delAggImage = this.delAggImage;
        body.delAggModifierGroupItems = this.delAggModifierGroupItems;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/modifier-group']);
    }

    addItem() {
        this.delAggModifierGroupItems.push(
            new DelAggModifierGroupItemEditDto({
                delAggModifierGroupId: this.delAggModifierGroup.id,
                delAggItemId: 0
            } as any)
        );
    }

    deleteItem(index) {
        this.delAggModifierGroupItems.splice(index, 1);
    }

    addModifier() {
        this.addEditClusterModifierModal.show({
            id: 0,
            delAggModifierGroupId: this.delAggModifierGroup.id
        });
    }

    deleteModifier(item) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggModifierServiceProxy.deleteDelAggModifier(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this._getModifiers();
                });
            }
        });
    }

    editModifier(id) {
        this.addEditClusterModifierModal.show({
            id: id,
            delAggModifierGroupId: this.delAggModifierGroup.id
        });
    }

    remove(index: number) {
        this.delAggImage.splice(index, 1);
    }

    download(file: DelAggImageEditDto) {
        location.href = JSON.parse(file.imagePath).url;
    }

    upload(file: FileItem) {
        this.uploadLoading = true;
        file.upload();
    }

    initUploader(): void {
        this.uploader = new FileUploader({
            url: AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage',
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader._fileTypeFilter = (item: FileLikeObject) => {
            return true;
        };

        this.uploader.onBeforeUploadItem = (fileItem) => {
            fileItem.formData.push({ fileTag: 'Image' });
        };

        this.uploader.onCompleteAll = () => {
            this.uploadLoading = false;
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse: IAjaxResponse = JSON.parse(response);

            if (ajaxResponse?.success) {
                this.delAggImageItem.imagePath = JSON.stringify({
                    fileTag: 'Image',
                    fileName: ajaxResponse.result.fileName,
                    fileType: ajaxResponse.result.fileType,
                    fileToken: ajaxResponse.result.fileToken,
                    url: ajaxResponse.result.url
                });
            } else {
                this.notify.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onAfterAddingFile = (file) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
        };

        const uploaderOptions: FileUploaderOptions = {
            removeAfterUpload: false,
            autoUpload: false
        };
        this.uploader.setOptions(uploaderOptions);
    }

    removeFile(item) {
        item.remove();
    }

    addImage() {
        if (this.delAggTypeRef) {
            const isExist = this.delAggImage.some((item) => item.delAggTypeRefId == +this.delAggTypeRef.value);
            if (isExist) {
                this.notify.error('Del Agg Type existed');
                return;
            }

            this.delAggImageItem.delAggTypeRefId = +this.delAggTypeRef.value;
            this.delAggImageItem.delAggTypeRefName = this.delAggTypeRef.displayText;

            this.delAggImage.push(new DelAggImageEditDto(this.delAggImageItem));
        }
    }

    toJSON(data: string) {
        return JSON.parse(data);
    }

    openLanguageDescriptionModal() {
        const data = {
            id: this.delAggModifierGroup.id,
            language: this.delAggModifierGroup.name,
            languageDescriptionType: 4
        };

        this.clusterLanguageDescriptionModal.show(data);
    }

    onSavedModifier() {}

    private _create(body: CreateOrUpdateDelAggModifierGroupInput) {
        this.isLoading = true;

        this._delAggModifierGroupServiceProxy
            .createOrUpdateDelAggModifierGroup(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggModifierGroupServiceProxy
            .getDelAggModifierGroupForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggModifierGroup = result.delAggModifierGroup;
                this.delAggImage = result.delAggImage;
                this.delAggModifierGroupItems = result.delAggModifierGroupItem;

                this._getModifiers();
            });
    }

    private _getDelAggType() {
        this._delAggLocMappingServiceProxy
            .getDelAggTypeForCombobox()
            .subscribe((result) => (this.delAggTypes = result.items));
    }

    private _getDelAggItems() {
        this._delAggItemServiceProxy.getNames().subscribe((result) => {
            this.delAggItems = result.items;
        });
    }

    private _getTagGroups() {
        this._orderTagGroupServiceProxy
            .getOrderTagGroupNames()
            .subscribe((result) => (this.orderTagGroups = result.items));
    }

    private _getModifiers() {
        this._delAggModifierServiceProxy
            .getAll(undefined, this.delAggModifierGroup.id, undefined, undefined, undefined, undefined, undefined)
            .subscribe((result) => (this.delaggModifierItems = result.items));
    }
}

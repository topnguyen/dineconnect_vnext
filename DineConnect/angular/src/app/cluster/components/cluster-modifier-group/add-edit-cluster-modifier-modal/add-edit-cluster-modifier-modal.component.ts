import { ChangeDetectorRef, Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AddEditClusterModifierComponent } from '../../cluster-modifier/add-edit-cluster-modifier/add-edit-cluster-modifier.component';

@Component({
    selector: 'app-add-edit-cluster-modifier-modal',
    templateUrl: './add-edit-cluster-modifier-modal.component.html',
    styleUrls: ['./add-edit-cluster-modifier-modal.component.scss']
})
export class AddEditClusterModifierModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('delAggLanguageModal', { static: true }) modal: ModalDirective;
    @ViewChild('addEditClusterModifier', { static: false })
    addEditClusterModifier: AddEditClusterModifierComponent;

    data = {
        id: 0,
        delAggModifierGroupId: 0
    };
    isActive = false;

    @Output() saved = new EventEmitter<any>();

    constructor(injector: Injector, private changeDetector: ChangeDetectorRef) {
        super(injector);
    }

    ngOnInit(): void {}

    show(data) {
        this.data = data;
        this.isActive = true;
        this.changeDetector.detectChanges();

        setTimeout(() => {
            this.addEditClusterModifier.id = data.id;
            this.addEditClusterModifier.delAggModifier.delAggModifierGroupId = data.delAggModifierGroupId;
            this.addEditClusterModifier.ngOnInit();
            this.addEditClusterModifier.closeModal = (isSaved) => {
                if (isSaved) {
                    this.saved.emit();
                }
                this.close();
            };

            this.modal.show();
        }, 100);
    }

    onShown() {}

    close() {
        this.isActive = false;
        this.modal.hide();
    }
}

import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DelAggChargeListDto, DelAggChargeServiceProxy } from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Subject } from 'rxjs';
import { debounceTime, finalize, takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './cluster-charge.component.html',
  styleUrls: ['./cluster-charge.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})

export class ClusterChargeComponent extends AppComponentBase implements OnInit {
    @ViewChild('table', { static: true }) table: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    lazyLoadEvent: LazyLoadEvent;

    private filter$ = new Subject();
    private destroy$: Subject<boolean> = new Subject();

    constructor(
        injector: Injector,
        private _router: Router,
        private _delAggChargeServiceProxy: DelAggChargeServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.filter$.pipe(debounceTime(1000), takeUntil(this.destroy$)).subscribe((data) => {
            this.getAll();
        });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._delAggChargeServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEdit(item?: DelAggChargeListDto) {
        this._router.navigate(['app/cluster/charge/', item ? item.id : 'new']);
    }

    delete(item: DelAggChargeListDto) {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._delAggChargeServiceProxy.deleteDelAggCharge(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyRemoved'));
                    this.getAll();
                });
            }
        });
    }

    export() {
        this._delAggChargeServiceProxy
            .getAllToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.table),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search(event: string) {
        this.filter$.next(event);
    }
}

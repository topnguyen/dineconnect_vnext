import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDelAggChargeInput,
    DelAggChargeEditDto,
    DelAggChargeMappingListDto,
    DelAggChargeServiceProxy,
    DelAggItemGroupListDto,
    DelAggItemGroupServiceProxy,
    DelAggLocationGroupListDto,
    DelAggLocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-add-edit-cluster-charge',
    templateUrl: './add-edit-cluster-charge.component.html',
    styleUrls: ['./add-edit-cluster-charge.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddEditClusterChargeComponent extends AppComponentBase implements OnInit {
    id: number;
    isLoading: boolean;
    charge = new DelAggChargeEditDto();
    delAggChargeMapping: DelAggChargeMappingListDto[] = [];
    locationGroups: DelAggLocationGroupListDto[];
    itemGroups: DelAggItemGroupListDto[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggChargeServiceProxy: DelAggChargeServiceProxy,
        private _delAggLocationGroupServiceProxy: DelAggLocationGroupServiceProxy,
        private _delAggItemGroupServiceProxy: DelAggItemGroupServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getLocationGroup();
        this._getItemGroup();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }

        if (this.delAggChargeMapping.length == 0 ) {
            this.addMapping();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggChargeInput();
        body.delAggCharge = this.charge;
        body.delAggChargeMapping = this.delAggChargeMapping;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/charge']);
    }

    addMapping() {
        const newRow = new DelAggChargeMappingListDto({
            delAggChargeId: 0,
            delAggLocationGroupId: undefined,
            delItemGroupId: undefined
        } as any);

        this.delAggChargeMapping.push(newRow);
    }

    removeMapping(index) {
        this.delAggChargeMapping.splice(index, 1);
    }

    private _create(body: CreateOrUpdateDelAggChargeInput) {
        this.isLoading = true;

        this._delAggChargeServiceProxy
            .createOrUpdateDelAggCharge(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggChargeServiceProxy
            .getDelAggChargeForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.charge = result.delAggCharge;
                this.delAggChargeMapping = result.delAggChargeMapping || [];
            });
    }

    private _getLocationGroup() {
        this._delAggLocationGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.locationGroups = result.items));
    }


    private _getItemGroup() {
        this._delAggItemGroupServiceProxy
            .getAll(undefined, 'name ASC', undefined, undefined)
            .subscribe((result) => (this.itemGroups = result.items));
    }
}

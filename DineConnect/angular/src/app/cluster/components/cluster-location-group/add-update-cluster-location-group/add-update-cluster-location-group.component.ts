import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateDelAggLocationGroupInput,
    DelAggLocationGroupEditDto,
    DelAggLocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './add-update-cluster-location-group.component.html',
    styleUrls: ['./add-update-cluster-location-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddUpdateClusterLocationGroupComponent extends AppComponentBase implements OnInit {
    id: number;
    isLoading: boolean;
    locationGroup = new DelAggLocationGroupEditDto();

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggLocationGroupServiceProxy: DelAggLocationGroupServiceProxy
    ) {
        super(injector);

        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggLocationGroupInput();
        body.delAggLocationGroup = this.locationGroup;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/location-group']);
    }

    private _create(body: CreateOrUpdateDelAggLocationGroupInput) {
        this.isLoading = true;

        this._delAggLocationGroupServiceProxy
            .createOrUpdateDelAggLocationGroup(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggLocationGroupServiceProxy
            .getDelAggLocationGroupForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.locationGroup = result.delAggLocationGroup;
            });
    }
}

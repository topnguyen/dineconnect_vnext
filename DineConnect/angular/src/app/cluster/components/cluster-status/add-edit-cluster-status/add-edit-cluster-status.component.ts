import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto } from '@shared/service-proxies/service-proxies';
import {
    DelAggLocationItemEditDto,
    DelAggItemServiceProxy,
    DelAggLocMappingServiceProxy,
    DelAggLocationItemServiceProxy,
    ListResultDtoOfDelAggItemListDto,
    DelAggLocMappingListDto,
    CreateOrUpdateDelAggLocationItemInput,
    DelAggItemListDto,
    DelAggVariantListDto,
    DelAggModifierListDto,
    DelAggVariantServiceProxy,
    DelAggModifierServiceProxy
} from '@shared/service-proxies/service-proxies-nswag';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './add-edit-cluster-status.component.html',
  styleUrls: ['./add-edit-cluster-status.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class AddEditClusterStatusComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectItemModal', { static: true }) selectItemModal: SelectPopupComponent;

    id: number;
    isLoading: boolean;
    delAggLocationItem = new DelAggLocationItemEditDto();

    refLocMap: DelAggLocMappingListDto[] = []
    refItem: DelAggItemListDto[] = []
    refVariant: DelAggVariantListDto[] = [];
    refModifier: DelAggModifierListDto[] = [];
    delpriceTypes: ComboboxItemDto[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _delAggLocationItemServiceProxy: DelAggLocationItemServiceProxy,
        private _delAggLocMappingServiceProxy: DelAggLocMappingServiceProxy,
        private _delAggItemServiceProxy: DelAggItemServiceProxy,
        private _delAggVariantServiceProxy: DelAggVariantServiceProxy,
        private _delAggModifierServiceProxy: DelAggModifierServiceProxy,
    ) {
        super(injector);
        this.id = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit(): void {
        this._getLocaionMapping();
        this._getVariant();
        this._getModifier();
        this._getPriceType();

        if (this.id != NaN && this.id > 0) {
            this._get();
        }
    }

    submit() {
        const body = new CreateOrUpdateDelAggLocationItemInput();
        body.delAggLocationItem = this.delAggLocationItem;

        this._create(body);
    }

    back() {
        this._router.navigate(['app/cluster/status']);
    }

    openforItem() {
        this.selectItemModal.show();
    }

    clearItem() {
        this.delAggLocationItem.delAggItemId = undefined;
        this.delAggLocationItem.delAggItemName = undefined;
    }

    changeItem(result) {
        this.delAggLocationItem.delAggItemId = result.id;
        this.delAggLocationItem.delAggItemName = result.name;
    }

    getItem() {
        return (filterString, sorting, maxResultCount, skipCount): Observable<ListResultDtoOfDelAggItemListDto> => {
            return this._delAggItemServiceProxy.getAll(
                filterString,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                sorting,
                maxResultCount,
                skipCount
            );
        };
    }

    private _create(body: CreateOrUpdateDelAggLocationItemInput) {
        this.isLoading = true;

        this._delAggLocationItemServiceProxy
            .createOrUpdateDelAggLocationItem(body)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.back();
            });
    }

    private _get() {
        this.isLoading = true;

        this._delAggLocationItemServiceProxy
            .getDelAggLocationItemForEdit(this.id)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((result) => {
                this.delAggLocationItem = result.delAggLocationItem;
            });
    }

    private _getLocaionMapping() {
        this._delAggLocMappingServiceProxy.getAggLocationMapNames().subscribe((result) => {
            this.refLocMap = result.items;
        });
    }

    private _getVariant() {
        this._delAggVariantServiceProxy.getNames().subscribe( (result) => {
            this.refVariant = result.items;
        });
    }

    private _getModifier() {
        this._delAggModifierServiceProxy.getNames().subscribe( (result) => {
            this.refModifier = result.items;
        });
    }

    private _getPriceType() {
        this._delAggLocMappingServiceProxy.getDelPriceTypeForCombobox().subscribe( (result) => {
            this.delpriceTypes = result.items;
        });
    }
}

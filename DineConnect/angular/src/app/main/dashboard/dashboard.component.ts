import {
    Component,
    ElementRef,
    Injector,
    OnInit,
    Renderer2,
    ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { DashboardCustomizationConst } from "@app/shared/common/customizable-dashboard/DashboardCustomizationConsts";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import {
    IdNameDto,
    SimpleLocationDto,
    TicketServiceProxy,
} from "@shared/service-proxies/service-proxies";
import moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import * as Highcharts from "highcharts";
import { BsModalService } from "ngx-bootstrap/modal";
import { TicketModalComponent } from "./ticket-modal/ticket-modal.component";
import { CustomizableDashboardComponent } from "@app/shared/common/customizable-dashboard/customizable-dashboard.component";
import { DasboardService } from "./dasboard.service";
import { GetDashboardChartModel } from "@app/shared/models/GetDashboardChartModel";

@Component({
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"],
    animations: [appModuleAnimation()],
})
export class DashboardComponent extends AppComponentBase implements OnInit {
    @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
    @ViewChild("selectLocationModal", { static: true })
    selectLocationModal: SelectLocationComponent;
    @ViewChild("customizableDashboard", { static: true })
    customizableDashboard: CustomizableDashboardComponent;

    dashboardName =
        DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;
    dateRangeModel: Date[];
    ranges = [];
    allLocation: boolean = false;
    autoRefresh: boolean;
    location = new SimpleLocationDto();

    currencyText = this.appSession.application.currencySign;
    totalTicketAmount: number = 0;
    totalTickets: number = 0;
    totalOrders: number = 0;
    totalItems: number = 0;
    totalAverage: number = 0;
    filterParam: GetDashboardChartModel;

    isPaymentsChartFullScreen = false;
    isTransactionChartFullScreen = false;
    isDepartmentChartFullScreen = false;
    isItemChartFullScreen = false;

    editModeEnabled = false;

    constructor(
        injector: Injector,
        private render: Renderer2,
        private _modalService: BsModalService,
        private ticketServiceProxy: TicketServiceProxy,
        private _dashboardService: DasboardService
    ) {
        super(injector);
        this.ranges = this.createDateRangePickerOptions();
    }

    ngOnInit() {
        this.refresh();
    }

    refresh() {
        if (this.allLocation) {
            this.location = new SimpleLocationDto();
        }

        this.filterParam = {
            startDate:
                this.dateRangeModel?.length > 0
                    ? moment(this.dateRangeModel[0])
                    : undefined,
            endDate:
                this.dateRangeModel?.length > 0
                    ? moment(this.dateRangeModel[1])
                    : undefined,
            location: this.location.id || undefined,
        } as GetDashboardChartModel;

        this.getTicketDetails();
        this.paymentsChart();
        this.transactionChart();
        this.departmentChart();
        this.itemChart();
    }

    getTicketDetails() {
        this.ticketServiceProxy
        
            .getDashboardChart(
                this.filterParam.location,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                if (result != null) {
                    this.totalTicketAmount = result.dashBoardDto.totalAmount;
                    this.totalTickets = result.dashBoardDto.totalTicketCount;
                    this.totalOrders = result.dashBoardDto.totalOrderCount;
                    this.totalItems = result.dashBoardDto.totalItemSold;
                    this.totalAverage = result.dashBoardDto.averageTicketAmount;
                }
            });
    }

    paymentsChart() {
        this.ticketServiceProxy
            .getPaymentChart(
                this.filterParam.location,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                Highcharts.chart(
                    "payment_stats",
                    {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: "pie",
                        },
                        title: {
                            text: "",
                        },
                        tooltip: {
                            pointFormat: "{point.percentage:.1f} %",
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: "pointer",
                                dataLabels: {
                                    enabled: true,
                                    format: "{point.name}: {point.y} <br/>{point.percentage:.1f} %",
                                    style: {
                                        color:
                                            (Highcharts["theme"] &&
                                                Highcharts["theme"]
                                                    .contrastTextColor) ||
                                            "black",
                                    },
                                },
                            },
                        },
                        series: [
                            {
                                name: this.l("Payment"),
                                colorByPoint: true,
                                data: result, // [{"name":"CASH","y":1093.800},{"name":"MASTER","y":680.130},{"name":"VISA","y":3825.240}]
                            },
                        ],
                    },
                    () => {}
                );
            });
    }

    transactionChart() {
        this.ticketServiceProxy
            .getTransactionChart(
                this.filterParam.location,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                Highcharts.chart(
                    "transaction_stats",
                    {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: "pie",
                        },
                        title: {
                            text: "",
                        },
                        tooltip: {
                            pointFormat: "{point.percentage:.1f} %",
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: "pointer",
                                dataLabels: {
                                    enabled: true,
                                    format: "{point.name}: {point.y} <br/>{point.percentage:.1f} %",
                                    style: {
                                        color:
                                            (Highcharts["theme"] &&
                                                Highcharts["theme"]
                                                    .contrastTextColor) ||
                                            "black",
                                    },
                                },
                            },
                        },
                        series: [
                            {
                                name: this.l("Payment"),
                                colorByPoint: true,
                                data: result, // [{"name":"DISCOUNT","y":-1.6},{"name":"PAYMENT","y":5599.17},{"name":"ROUNDING","y":-0.15},{"name":"SALE","y":5234.6},{"name":"TAX","y":366.32}]
                            },
                        ],
                    },
                    () => {}
                );
            });
    }

    departmentChart() {
        this.ticketServiceProxy
            .getDepartmentChart(
                this.filterParam.location,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                Highcharts.chart(
                    "department_stats",
                    {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: "pie",
                        },
                        title: {
                            text: "",
                        },
                        tooltip: {
                            pointFormat: "{point.percentage:.1f} %",
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: "pointer",
                                dataLabels: {
                                    enabled: true,
                                    format: "{point.name}: {point.y} <br/>{point.percentage:.1f} %",
                                    style: {
                                        color:
                                            (Highcharts["theme"] &&
                                                Highcharts["theme"]
                                                    .contrastTextColor) ||
                                            "black",
                                    },
                                },
                            },
                        },
                        series: [
                            {
                                name: this.l("Department"),
                                colorByPoint: true,
                                data: result, // [{"name":"HALL A","y":1677.980},{"name":"HALL B","y":3921.190}]
                            },
                        ],
                    },
                    () => {}
                );
            });
    }

    itemChart() {
        this.ticketServiceProxy
            .getItemChart(
                this.filterParam.location,
                this.filterParam.userId,
                this.filterParam.startDate,
                this.filterParam.endDate,
                this.filterParam.locations,
                this.filterParam.locationGroup_Locations,
                this.filterParam.locationGroup_Groups,
                this.filterParam.locationGroup_LocationTags,
                this.filterParam.locationGroup_NonLocations,
                this.filterParam.locationGroup_Group,
                this.filterParam.locationGroup_LocationTag,
                this.filterParam.locationGroup_UserId,
                this.filterParam.notCorrectDate,
                this.filterParam.credit,
                this.filterParam.refund,
                this.filterParam.ticketNo
            )
            .subscribe((result) => {
                Highcharts.chart(
                    "item_stats",
                    {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: "pie",
                        },
                        title: {
                            text: "",
                        },
                        tooltip: {
                            pointFormat: "{point.percentage:.1f} %",
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: "pointer",
                                dataLabels: {
                                    enabled: true,
                                    format: "{point.name}: {point.y}",
                                    style: {
                                        color:
                                            (Highcharts["theme"] &&
                                                Highcharts["theme"]
                                                    .contrastTextColor) ||
                                            "black",
                                    },
                                },
                            },
                        },
                        series: [
                            {
                                name: this.l("Department"),
                                colorByPoint: true,
                                data: result, //[{"name":"小料","y":103.000},{"name":"王老吉凉茶","y":23.000},{"name":"极品鲜毛肚","y":22.000},{"name":"手工虾滑","y":17.000},{"name":"美国肥牛","y":17.000},{"name":"白米饭","y":16.000},{"name":"黑猪五花肉","y":16.000},{"name":"功夫青笋","y":15.000},{"name":"青岛啤酒 640ml","y":15.000},{"name":"大骨鸳鸯","y":13.000}]
                            },
                        ],
                    },
                    () => {}
                );
            });
    }

    autoRefreshScreen() {}

    openModalTickets() {
        const initialState = {
            locationId: this.location.id,
        };
        const bsModalRef = this._modalService.show(TicketModalComponent, {
            initialState,
            class: "modal-xl",
        });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;

        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        let customDatepickerEl = document.querySelector(
            ".bs-datepicker.custom-datepicker"
        );
        self.render.removeClass(el, "show");
        self.render.removeClass(customDatepickerEl, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
            self.render.addClass(customDatepickerEl, "show");
        });
    }

    openLocation() {
        this.selectLocationModal.show(this.location);
    }

    getLocations(event) {
        this.location = event || new SimpleLocationDto();
        this._dashboardService.changeLocation(event);
    }

    changeEditMode(): void {
        this.customizableDashboard.changeEditMode();
    }
    changeDaterange() {
        this._dashboardService.changeDateRange(this.dateRangeModel);
    }
}



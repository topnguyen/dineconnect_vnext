import { Component, Injector, OnInit, Renderer2 } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TicketServiceProxy } from '@shared/service-proxies/service-proxies';
import moment from 'moment';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    templateUrl: './ticket-modal.component.html',
    styleUrls: ['./ticket-modal.component.scss']
})
export class TicketModalComponent extends AppComponentBase implements OnInit {
    totalTicketAmount = 0;
    totalTickets = 0;
    totalOrders = 0;
    totalItems = 0;
    totalAverage = 0;

    ctotalTicketAmount = 0;
    ctotalTickets = 0;
    ctotalOrders = 0;
    ctotalItems = 0;
    ctotalAverage = 0;

    down = false;
    display = false;
    downTickets = false;
    downOrders = false;
    downItems = false;
    downAvg = false;

    dateRangeModel: Date[];
    cdateRangeModel: Date[];
    ranges = [];
    locationId: number;

    constructor(injector: Injector, private _bsModalRef: BsModalRef, private _render: Renderer2, private _ticketServiceProxy: TicketServiceProxy) {
        super(injector);
        this.ranges = this.createDateRangePickerOptions();
    }

    ngOnInit() {}

    close(): void {
        this._bsModalRef.hide();
    }

    refresh() {
        this._ticketServiceProxy
            .getDashboardChart(
                this.locationId,
                undefined,
                this.dateRangeModel?.length > 0 ? moment(this.dateRangeModel[0]) : undefined,
                this.dateRangeModel?.length > 0 ? moment(this.dateRangeModel[1]) : undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .subscribe((result) => {
                if (result != null) {
                    this.totalTicketAmount = result.dashBoardDto.totalAmount;
                    this.totalTickets = result.dashBoardDto.totalTicketCount;
                    this.totalOrders = result.dashBoardDto.totalOrderCount;
                    this.totalItems = result.dashBoardDto.totalItemSold;
                    this.totalAverage = result.dashBoardDto.averageTicketAmount;
                }
            });

    }

    crefresh() {
        this._ticketServiceProxy
            .getDashboardChart(
                this.locationId,
                undefined,
                this.cdateRangeModel?.length > 0 ? moment(this.cdateRangeModel[0]) : undefined,
                this.cdateRangeModel?.length > 0 ? moment(this.cdateRangeModel[1]) : undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .subscribe((result) => {
                if (result != null) {
                    this.ctotalTicketAmount = result.dashBoardDto.totalAmount;
                    this.ctotalTickets = result.dashBoardDto.totalTicketCount;
                    this.ctotalOrders = result.dashBoardDto.totalOrderCount;
                    this.ctotalItems = result.dashBoardDto.totalItemSold;
                    this.ctotalAverage = result.dashBoardDto.averageTicketAmount;

                    this.display = true;

                    if (this.ctotalTicketAmount > this.totalTicketAmount) {
                        this.down = true;
                    } else {
                        this.down = false;
                    }

                    if (this.ctotalTickets > this.totalTickets) {
                        this.downTickets = true;
                    } else {
                        this.downTickets = false;
                    }

                    if (this.ctotalOrders > this.totalOrders) {
                        this.downOrders = true;
                    } else {
                        this.downOrders = false;
                    }

                    if (this.ctotalItems > this.totalItems) {
                        this.downItems = true;
                    } else {
                        this.downItems = false;
                    }

                    if (this.ctotalAverage > this.totalAverage) {
                        this.downAvg = true;
                    } else {
                        this.downAvg = false;
                    }
                }
            });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;

        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        let customDatepickerEl = document.querySelector('.bs-datepicker.custom-datepicker');
        self._render.removeClass(el, 'show');
        self._render.removeClass(customDatepickerEl, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self._render.addClass(el, 'show');
            self._render.addClass(customDatepickerEl, 'show');
        });
    }
}

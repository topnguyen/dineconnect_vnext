import { Injectable} from '@angular/core';
import { SimpleLocationDto } from '@shared/service-proxies/service-proxies';
import {BehaviorSubject} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DasboardService {

  constructor() { }
  private dateRange = new BehaviorSubject<Date[]>([new Date(), new Date()]);
  private location = new BehaviorSubject<SimpleLocationDto[]>([]);
   castDateRange = this.dateRange.asObservable();
   castLocation = this.location.asObservable();
   
   changeDateRange(daterange: Date[]){
     this.dateRange.next(daterange); 
   }

   changeLocation(location: SimpleLocationDto[]){
    this.location.next(location); 
  }
}

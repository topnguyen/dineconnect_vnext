import { ShipmentAnalyticServiceProxy } from './../../../shared/service-proxies/service-proxies';
import { EventEmitter, Output } from "@angular/core";
import {
    Component,
    Injector,
    ViewChild,
} from "@angular/core";
import {
    ShipmentAddressServiceProxy, ShipmentRouteServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table, Paginator, LazyLoadEvent, MenuItem } from "primeng";
import { CreateOrEditAddressModalComponent } from "../addresses/create-or-edit-address-modal.component";

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
  animations: [appModuleAnimation()],
})
export class AnalyticsComponent extends AppComponentBase{
    @ViewChild("createOrEditAddressModal", { static: true })
    createOrEditAddress: CreateOrEditAddressModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    selectedOrders: any;
    dateRange: Date[] = [
        new Date(new Date().setDate(new Date().getDate() - 7)),
        new Date(new Date().setDate(new Date().getDate() + 7)),
    ];
    startDate: any;
    endDate: any;
    constructor(
        injector: Injector,
        private _shipmentRouteSiteServiceProxy: ShipmentRouteServiceProxy,
        private _shipmentAnalyticServiceProxy: ShipmentAnalyticServiceProxy,
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if ((this.dateRange as any) !== undefined) {
            this.startDate = this.transfromDate(this.dateRange[0]);
            this.endDate = this.transfromDate(this.dateRange[1]);
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._shipmentAnalyticServiceProxy
            .getAll(
                this.filterText,
                this.startDate,
                this.endDate,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createAddress(): void {
        this.createOrEditAddress.show();
    }

    clickOrder(record) {
        this.primengTableHelper.records.forEach((element) => {
            element.isActive = false;
        });
        record.isActive = true;
    }
    onRowExpand(event) {
        this._shipmentRouteSiteServiceProxy.getOrderItemsList(event.data.id, "").subscribe(result => {
          event.data.goods = result;
        });
      }
}

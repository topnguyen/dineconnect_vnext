import { Component, Injector , ViewChild } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { ShipmentClientServiceProxy, ShipmentDepotServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { NotifyService } from 'abp-ng2-module';
import { CreateOrEditClientModalComponent } from './create-or-edit-client-modal.component';
@Component({
    selector: 'app-shipment-clients',
    templateUrl: './clients.component.html',
    animations: [appModuleAnimation()]
})
export class ClientsComponent extends AppComponentBase {
    @ViewChild('createOrEditClientModal', { static: true }) createOrEditClient: CreateOrEditClientModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';

    constructor(
        injector: Injector,
        private _clientServiceProxy: ShipmentClientServiceProxy,
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._clientServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createClient(): void {
        this.createOrEditClient.show();
    }

    deleteClient(depot): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._clientServiceProxy.delete(depot.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
    getPriority(input) {
        let result = "";
        switch (input) {
            case 1:
                result = "High";
                break;
            case 2:
                result = "Normal";
                break;
            case 3:
                result = "Low";
                break;
            default:
                result = "";
        }
        return result;
    }
}

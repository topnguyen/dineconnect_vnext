import { result } from "lodash";
import {
    CreateOrEditShipmentClientAddressDto,
    LinkedAddressDto,
    ShipmentClientAddressServiceProxy,
} from "./../../../shared/service-proxies/service-proxies";
import { OnInit } from "@angular/core";
import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    CreateOrEditShipmentClientDto,
    ShipmentClientServiceProxy,
    ERPComboboxItem,
    CommonServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { forkJoin } from "rxjs";
@Component({
    selector: "app-create-or-edit-client-modal",
    templateUrl: "./create-or-edit-client-modal.component.html",
    styleUrls: ["./create-or-edit-client-modal.component.scss"],
})
export class CreateOrEditClientModalComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    client: CreateOrEditShipmentClientDto = new CreateOrEditShipmentClientDto();
    lstShipmentTimes: ERPComboboxItem[] = [];
    lstPriority = [
        {name: 'High', value: 1},
        {name: 'Normal', value: 2},
        {name: 'Low', value: 3},
    ];
    address = null;
    lstAddress: LinkedAddressDto[] = [];
    lstAddressLinked: LinkedAddressDto[] = [];
    constructor(
        injector: Injector,
        private _clientServiceProxy: ShipmentClientServiceProxy,
        private _clientAddressServiceProxy: ShipmentClientAddressServiceProxy,
        private _commonServiceProxy: CommonServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonServiceProxy
            .getLookups("ShipmentTimes", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstShipmentTimes = result;
            });
    }
    show(id?: number): void {
        if (!id) {
            this.client = new CreateOrEditShipmentClientDto();
            this.client.id = id;
            this.active = true;
            this.modal.show();
        } else {
            this._clientServiceProxy
                .getShipmentClientEdit(id)
                .subscribe((result) => {
                    this.client = result;
                    this.active = true;
                    this.modal.show();
                });
            this.getAddressIsNotInClient(id);
            this.getLinkedAddressByClient(id);
        }
    }
    getAddressIsNotInClient(id) {
        this._clientAddressServiceProxy
            .getAddressesIsNotInClientId(id)
            .subscribe((result) => {
                this.lstAddress = result;
            });
    }
    getLinkedAddressByClient(id) {
        this._clientAddressServiceProxy
            .getLinkedAddressByClientId(id)
            .subscribe((result) => {
                this.lstAddressLinked = result;
            });
    }

    save(): void {
        this.saving = true;
        if ((this.client.priority as any) == "undefined") {
            this.client.priority = undefined;
        }
        if ((this.client.deliveryTimeFrom as any) == "undefined") {
            this.client.deliveryTimeFrom = undefined;
        }
        if ((this.client.deliveryTimeTo as any) == "undefined") {
            this.client.deliveryTimeTo = undefined;
        }
        this._clientServiceProxy
            .createOrEdit(this.client)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
    addAddress() {
        if (this.address) {
            let input = new CreateOrEditShipmentClientAddressDto();
            input.clientId = this.client.id;
            input.addressId = this.address;
            input.tenantId = this.appSession.tenantId;
            this._clientAddressServiceProxy.create(input).subscribe(() => {
                this.getAddressIsNotInClient(this.client.id);
                this.getLinkedAddressByClient(this.client.id);
                this.notify.success(this.l("SuccessfullySaved"));
            });
        }
    }
    deleteClientAddress(record) {
        this._clientAddressServiceProxy.delete(record.id).subscribe(() => {
            this.getAddressIsNotInClient(this.client.id);
            this.getLinkedAddressByClient(this.client.id);
            this.notify.success(this.l("SuccessfullyDeleted"));
        });
        this.address = null;
    }
}

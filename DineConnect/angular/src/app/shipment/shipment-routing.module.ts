import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DepotsComponent } from '../shipment/depots/depots.component';
import { DriversComponent } from '../shipment/drivers/drivers.component';
import { AddressessComponent } from './addresses/addresses.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ClientsComponent } from './clients/clients.component';
import { ManageRoutesComponent } from './routes/manage-routes.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'depots', component: DepotsComponent, data: { permission: '' } },
                    { path: 'drivers', component: DriversComponent, data: { permission: '' } },
                    { path: 'routes', component: ManageRoutesComponent, data: { permission: '' } },
                    { path: 'clients', component: ClientsComponent, data: { permission: '' } },
                    { path: 'addresses', component: AddressessComponent, data: { permission: '' } },
                    { path: 'analytics', component: AnalyticsComponent, data: { permission: '' } },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class ShipmentRoutingModule { }

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ShipmentService {
  routeId = new BehaviorSubject<number>(0);
  routeIdAsObservable = this.routeId.asObservable();

  siteId = new BehaviorSubject<number>(0);
  siteIdAsObservable = this.siteId.asObservable();
  
  
  numberSite = new BehaviorSubject<number>(0);
  changeNumberSiteAsObservable = this.numberSite.asObservable();



  constructor() { }

  changeSite(siteId: number) {
    this.siteId.next(siteId);
  }

  changeRoute(routeId: number) {
    this.routeId.next(routeId);
  }
  
  changeNumberSite(numberSite: number) {
    this.numberSite.next(numberSite);
  }
}

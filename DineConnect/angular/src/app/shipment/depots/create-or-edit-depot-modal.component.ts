import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ShipmentDepotServiceProxy, CreateOrEditShipmentDepotDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ZoomControlOptions } from '@agm/core';
import { MouseEvent } from '@agm/core';
@Component({
    selector: 'app-create-or-edit-depot-modal',
    templateUrl: './create-or-edit-depot-modal.component.html',
    styleUrls: ['./create-or-edit-depot-modal.component.scss']
})
export class CreateOrEditDepotModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    lat: number = 1.287953;
    lng: number = 103.851784;
    zoomLevel: number = 13;
    zoomControlOptions: ZoomControlOptions;
    map: any;
    markers: any[] = [];

    depot: CreateOrEditShipmentDepotDto = new CreateOrEditShipmentDepotDto();

    constructor(
        injector: Injector,
        private _depotServiceProxy: ShipmentDepotServiceProxy
    ) {
        super(injector);
    }
    onMapReady(map: any) {
        this.map = map;
    }
    mapClicked($event: MouseEvent) {
        this.markers = [];
        this.depot.latitude = $event.coords.lat;
        this.depot.longitude = $event.coords.lng;

        this.markers.push({
            lat: $event.coords.lat,
            lng: $event.coords.lng,
            draggable: true
        });
    }
    clickedMarker(label: string, index: number) { }
    changeMap() {
        this.markers = [];
        this.markers.push({
            lat: this.depot.latitude,
            lng: this.depot.longitude,
            draggable: true
        });
    }
    show(tiffinOrderTagId?: number): void {
        if (!tiffinOrderTagId) {
            this.depot = new CreateOrEditShipmentDepotDto();
            this.depot.id = tiffinOrderTagId;

            this.active = true;
            this.modal.show();
        } else {
            this._depotServiceProxy.getTrackDepotEdit(tiffinOrderTagId)
                .subscribe(result => {
                    this.depot = result;
                    this.active = true;
                    this.modal.show();
                    this.changeMap();
                });
        }
    }

    save(): void {
        this.saving = true;

        this._depotServiceProxy.createOrEdit(this.depot)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}


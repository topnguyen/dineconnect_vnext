import { Component, Injector , ViewChild } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { ShipmentDepotServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { NotifyService } from 'abp-ng2-module';
import { CreateOrEditDepotModalComponent } from './create-or-edit-depot-modal.component';
@Component({
    selector: 'app-depots',
    templateUrl: './depots.component.html',
    animations: [appModuleAnimation()]
})
export class DepotsComponent extends AppComponentBase {
    @ViewChild('createOrEditDepotModal', { static: true }) createOrEditDepot: CreateOrEditDepotModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';

    constructor(
        injector: Injector,
        private _depotServiceProxy: ShipmentDepotServiceProxy,
    ) {
        super(injector);
    }

    getDepots(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._depotServiceProxy.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createDepot(): void {
        this.createOrEditDepot.show();
    }

    deleteDepot(depot): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._depotServiceProxy.delete(depot.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }
}

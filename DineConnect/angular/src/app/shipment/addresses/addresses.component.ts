import { result } from "lodash";
import { Component, Injector, ViewChild } from "@angular/core";
import {
    ShipmentAddressServiceProxy,
    ShipmentClientServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { CreateOrEditAddressModalComponent } from "./create-or-edit-address-modal.component";
@Component({
    selector: "app-addresses-clients",
    templateUrl: "./addresses.component.html",
    animations: [appModuleAnimation()],
})
export class AddressessComponent extends AppComponentBase {
    @ViewChild("createOrEditAddressModal", { static: true })
    createOrEditAddress: CreateOrEditAddressModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";

    constructor(
        injector: Injector,
        private _addressServiceProxy: ShipmentAddressServiceProxy
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._addressServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createAddress(): void {
        this.createOrEditAddress.show();
    }

    deleteAddress(depot): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._addressServiceProxy.delete(depot.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l("SuccessfullyDeleted"));
                });
            }
        });
    }
    getPriority(input) {
        let result = "";
        switch (input) {
            case 1:
                result = "High";
                break;
            case 2:
                result = "Normal";
                break;
            case 3:
                result = "Low";
                break;
            default:
                result = "";
        }
        return result;
    }
}

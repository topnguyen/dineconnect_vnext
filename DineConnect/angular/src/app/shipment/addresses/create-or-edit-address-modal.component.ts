import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ShipmentDepotServiceProxy, CreateOrEditShipmentDepotDto, ShipmentAddressServiceProxy, CreateOrEditShipmentAddressDto, LinkedClientDto, ShipmentClientAddressServiceProxy, CreateOrEditShipmentClientAddressDto, CommonServiceProxy, ERPComboboxItem } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ZoomControlOptions } from '@agm/core';
import { MouseEvent } from '@agm/core';

@Component({
    selector: 'app-create-or-edit-address-modal',
    templateUrl: './create-or-edit-address-modal.component.html',
    styleUrls: ['./create-or-edit-address-modal.component.scss']
})
export class CreateOrEditAddressModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    lat: number = 1.287953;
    lng: number = 103.851784;
    zoomLevel: number = 13;
    zoomControlOptions: ZoomControlOptions;
    map: any;
    markers: any[] = [];
    address: CreateOrEditShipmentAddressDto = new CreateOrEditShipmentAddressDto();
    lstPriority = [
        {name: 'High', value: 1},
        {name: 'Normal', value: 2},
        {name: 'Low', value: 3},
    ];
    client: number;
    lstClient: LinkedClientDto[] = [];
    lstClientLinked: LinkedClientDto[] = [];
    lstShipmentTimes: ERPComboboxItem[] = [];

    constructor(
        injector: Injector,
        private _addressServiceProxy: ShipmentAddressServiceProxy,
        private _clientAddressServiceProxy: ShipmentClientAddressServiceProxy,
        private _commonServiceProxy: CommonServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonServiceProxy
        .getLookups("ShipmentTimes", this.appSession.tenantId, undefined)
        .subscribe((result) => {
            this.lstShipmentTimes = result;
        });
    }
    onMapReady(map: any) {
        this.map = map;
    }
    mapClicked($event: MouseEvent) {
        this.markers = [];
        this.address.gpsLat = $event.coords.lat.toString();
        this.address.gpsLon = $event.coords.lng.toString();

        this.markers.push({
            lat: $event.coords.lat,
            lng: $event.coords.lng,
            draggable: true
        });
    }
    clickedMarker(label: string, index: number) { }
    changeMap() {
        this.markers = [];
        this.markers.push({
            lat: +this.address.gpsLat,
            lng: +this.address.gpsLon,
            draggable: true
        });
    }
    show(id?: number): void {
        if (!id) {
            this.address = new CreateOrEditShipmentAddressDto();
            this.address.id = id;

            this.active = true;
            this.modal.show();
        } else {
            this._addressServiceProxy.getShipmentAddressEdit(id)
                .subscribe(result => {
                    this.address = result;
                    this.active = true;
                    this.modal.show();
                    this.changeMap();
                    this.getClientIsNotInAddress(this.address.id);
                    this.getLinkedClientByAddress(this.address.id);
                });
        }
    }
    getClientIsNotInAddress(id) {
        this._clientAddressServiceProxy
            .getClientesIsNotInAddressId(id)
            .subscribe((result) => {
                this.lstClient = result;
            });
    }
    getLinkedClientByAddress(id) {
        this._clientAddressServiceProxy
            .getLinkedClientByAddressId(id)
            .subscribe((result) => {
                this.lstClientLinked = result;
            });
    }
    save(): void {
        this.saving = true;

        this._addressServiceProxy.createOrEdit(this.address)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    addClient() {
        if (this.address) {
            let input = new CreateOrEditShipmentClientAddressDto();
            input.clientId = this.client;
            input.addressId = this.address.id;
            input.tenantId = this.appSession.tenantId;
            this._clientAddressServiceProxy.create(input).subscribe(() => {
                this.getClientIsNotInAddress(this.address.id);
                this.getLinkedClientByAddress(this.address.id);
                this.notify.success(this.l("SuccessfullySaved"));
            });
        }
    }
    deleteClientAddress(record) {
        this._clientAddressServiceProxy.delete(record.id).subscribe(() => {
            this.getClientIsNotInAddress(this.address.id);
            this.getLinkedClientByAddress(this.address.id);
            this.notify.success(this.l("SuccessfullyDeleted"));
        });
        this.client = null;
    }

}


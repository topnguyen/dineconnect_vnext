import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ShipmentDriverServiceProxy, CreateOrEditShipmentDriverDto, CommonServiceProxy, ERPComboboxItem } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'app-create-or-edit-driver-modal',
    templateUrl: './create-or-edit-driver-modal.component.html',
    styleUrls: ['./create-or-edit-driver-modal.component.scss']

})
export class CreateOrEditDriverModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    driver: CreateOrEditShipmentDriverDto = new CreateOrEditShipmentDriverDto();
    lstTrackDepot: ERPComboboxItem[] = [];
    constructor(
        injector: Injector,
        private _driverServiceProxy: ShipmentDriverServiceProxy,
        private _commonServiceProxy: CommonServiceProxy
    ) {
        super(injector);
    }

    show(driverId?: number): void {
        if (!driverId) {
            this.driver = new CreateOrEditShipmentDriverDto();
            this.driver.id = driverId;

            this.active = true;
            this.modal.show();
        } else {
            this._driverServiceProxy.getDriverForEdit(driverId)
                .subscribe(result => {
                    this.driver = result;

                    this.active = true;
                    this.modal.show();
                });
        }
        this._commonServiceProxy.getLookups("ShipmentDepots", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstTrackDepot = result;
        });
    }

    save(): void {
        this.saving = true;
        if (this.driver.trackDepotId as any == "undefined") {
            this.driver.trackDepotId = undefined;
        }
        this._driverServiceProxy.createOrEdit(this.driver)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

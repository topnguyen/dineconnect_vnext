import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/shared/common/app-common.module";
import { UtilsModule } from "@shared/utils/utils.module";
import { CountoModule } from "angular2-counto";
import { EasyPieChartModule } from "ng2modules-easypiechart";
import { EditorModule } from "primeng/editor";
import { FileUploadModule } from "ng2-file-upload";
import { ImageCropperModule } from "ngx-image-cropper";
import { TableModule } from "primeng/table";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxEasypiechartModule } from "ngx-easypiechart";
import { NgxTagsInputModule } from "ngx-tags-input";
import { TagInputModule } from "ngx-chips";
import { QueryBuilderModule } from "angular2-query-builder";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { AngularSplitModule } from 'angular-split';
import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService,
} from "ngx-bootstrap/datepicker";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { PopoverModule } from "ngx-bootstrap/popover";
import { EditorModule as TinymceModule } from "@tinymce/tinymce-angular";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import { TextMaskModule } from "angular2-text-mask";
import { CardModule, DataViewModule, PickListModule } from "primeng";
import { DragulaModule } from "ng2-dragula";
import { AutoCompleteModule } from "primeng/autocomplete";
import { InputMaskModule } from "primeng/inputmask";
import { CheckboxModule } from "primeng/checkbox";
import { CalendarModule } from "primeng/calendar";
import { MultiSelectModule } from "primeng/multiselect";
import { DropdownModule } from "primeng/dropdown";
import { PasswordModule } from "primeng/password";
import { DragDropModule } from "primeng/dragdrop";
import { FileUploadModule as PrimeNgFileUploadModule } from "primeng/fileupload";
import { ChipsModule } from "primeng/chips";
import { PaginatorModule } from "primeng/paginator";
import { CreateOrEditDepotModalComponent } from "../shipment/depots/create-or-edit-depot-modal.component";
import { DriversComponent } from "../shipment/drivers/drivers.component";
import { CreateOrEditDriverModalComponent } from "../shipment/drivers/create-or-edit-driver-modal.component";
import { RoutesComponent } from "../shipment/routes/routes.component";
import { CreateOrEditRouteModalComponent } from "../shipment/routes/create-or-edit-route-modal.component";
import { AgmCoreModule } from "@agm/core";
import { ShipmentService } from "./shipment.service";
import { ShipmentRoutingModule } from "./shipment-routing.module";
import { DeliveriesComponent } from "./routes/deliveries.component";
import { SitesComponent } from "./routes/sites.component";
import { MapsComponent } from './routes/maps.component';
import { DepotsComponent } from "./depots/depots.component";
import { AgmDirectionModule } from "agm-direction";
import { OrdersComponent } from "./routes/orders.component";
import { CreateOrEditOrderModalComponent } from './routes/create-or-edit-order-modal.component';
import { ClientsComponent } from "./clients/clients.component";
import { CreateOrEditClientModalComponent } from "./clients/create-or-edit-client-modal.component";
import { AddressessComponent } from "./addresses/addresses.component";
import { CreateOrEditAddressModalComponent } from "./addresses/create-or-edit-address-modal.component";
import { ManageRoutesComponent } from './routes/manage-routes.component';
import { ContextMenuModule } from 'primeng/contextmenu';
import { MatMenuModule } from "@angular/material/menu";
import { DuplicateRouteModalComponent } from './routes/duplicate-route-modal.component';
import { AnalyticsComponent } from './analytics/analytics.component';

@NgModule({
    imports: [
        ShipmentRoutingModule,
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        CountoModule,
        AutoCompleteModule,
        EditorModule,
        FileUploadModule,
        PrimeNgFileUploadModule,
        ImageCropperModule,
        InputMaskModule,
        CheckboxModule,
        CalendarModule,
        MultiSelectModule,
        NgSelectModule,
        NgxEasypiechartModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        NgxTagsInputModule,
        TagInputModule,
        TimepickerModule.forRoot(),
        AppBsModalModule,
        UtilsModule,
        QueryBuilderModule,
        ChipsModule,
        TinymceModule,
        DragDropModule,
        PasswordModule,
        TextMaskModule,
        PickListModule,
        DragulaModule.forRoot(),
        DataViewModule,
        CardModule,
        AgmCoreModule.forRoot({
            // apiKey: "AIzaSyCbrgvMPgVlrdwHg7IfHe064oUKEhxma3M",
            apiKey: "AIzaSyAS7ZOFV3rqGjbd5vwzAqmYDLNzUc72LlE",
        }),
        AgmDirectionModule  ,
        AngularSplitModule,
        ContextMenuModule,
       MatMenuModule,
    ],
    declarations: [
        CreateOrEditDepotModalComponent,
        DriversComponent,
        CreateOrEditDriverModalComponent,
        RoutesComponent,
        CreateOrEditRouteModalComponent,
        SitesComponent,
        DepotsComponent,
        DeliveriesComponent,
        MapsComponent,
        OrdersComponent,
        CreateOrEditOrderModalComponent,
        ClientsComponent,
        CreateOrEditClientModalComponent,
        AddressessComponent,
        CreateOrEditAddressModalComponent,
        ManageRoutesComponent,
        DuplicateRouteModalComponent,
        AnalyticsComponent
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
        ShipmentService,
    ],
})
export class ShipmentModule {}

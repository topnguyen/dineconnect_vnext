import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AddOrdersToRouteInputDto, ShipmentRouteServiceProxy, UpdateRoutePointsOrderInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ShipmentService } from '../shipment.service';

@Component({
  selector: 'app-shipment-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent extends AppComponentBase implements OnInit {
  lstSite: any[] = [];
  loading = false;
  routeId: number;
  isFullScreen= false;
  @Input()
  currentState: any;

  @Output() orderAdd: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    injector: Injector,
    private _shipmentRouteServiceProxy: ShipmentRouteServiceProxy,
    private _trackPodService: ShipmentService
  ) {
    super(injector);
  }


  ngOnInit(): void {
    this._trackPodService.routeIdAsObservable.subscribe(result => {
      this.routeId = result == 0 ? undefined : result;
      if (this.routeId) {
        this.getSites();
      }
    });
  }

  getSites(siteId?) {
    this.loading = true;
    this._shipmentRouteServiceProxy.getRoutePointList(
      this.routeId,
      undefined,
    )
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(result => {
        this.lstSite = result;
        this.lstSite.forEach(element => {
          element.isActive = false;
        });
        this.loading = true;

        if (siteId) {
          var row = this.lstSite.find(t => t.id == siteId);
          this.clickSite(row);
        }
        else if (this.lstSite.length) {
          this.clickSite(this.lstSite[0]);
        }
      });
      this._trackPodService.changeNumberSite(siteId);
  }

  clickSite(record) {
    this.lstSite.forEach(element => {
      element.isActive = false;
    });
    record.isActive = true;
    this._trackPodService.changeSite(record.id);
  }

  onRowReorder(event) {
    let ids = [];

    var dragedId = this.lstSite[event.dropIndex].id;

    for (let i = 0; i < this.lstSite.length; i++) {
      if (this.lstSite[i].type == 0)
        ids.push(this.lstSite[i].id);
    }


    var body = new UpdateRoutePointsOrderInput();
    body.routePointIds = ids;

    this._shipmentRouteServiceProxy.updateRoutePointsOrder(body).subscribe(t => {
      this.getSites(dragedId);
    });
  }

  drop(event) {
    if (this.currentState.draggedOrder && this.routeId) {
      let body: AddOrdersToRouteInputDto = new AddOrdersToRouteInputDto();

      body.orderIds = [this.currentState.draggedOrder.id];
      body.routeId = this.currentState.routeId;

      this._shipmentRouteServiceProxy.addOrdersFromRoute(body).subscribe(t => {
        this.orderAdd.emit(t);
      });
    }
  }
  onChangeFullScreen(){
      this.isFullScreen = !this.isFullScreen;
  }
}

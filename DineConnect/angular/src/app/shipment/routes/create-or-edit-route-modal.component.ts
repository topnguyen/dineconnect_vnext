import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonServiceProxy, ERPComboboxItem, CreateOrEditShipmentRouteDto, ShipmentRouteServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
@Component({
    selector: 'app-create-or-edit-route-modal',
    templateUrl: './create-or-edit-route-modal.component.html',
    styleUrls: ['./create-or-edit-route-modal.component.scss']

})
export class CreateOrEditRouteModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    date: moment.Moment = moment(moment.now());
    time = new Date;

    active = false;
    saving = false;

    shipmentRoute: CreateOrEditShipmentRouteDto = new CreateOrEditShipmentRouteDto();
    
    lstShipmentDepot: ERPComboboxItem[] = [];
    lstShipmentDriver: ERPComboboxItem[] = [];

    constructor(
        injector: Injector,
        private _shipmentRouteServiceProxy: ShipmentRouteServiceProxy,
        private _commonServiceProxy: CommonServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonServiceProxy.getLookups("ShipmentDepots", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentDepot = result;
        });
        this._commonServiceProxy.getLookups("ShipmentDrivers", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentDriver = result;
        });
    }

    show(shipmentDriverId?: number): void {
        if (!shipmentDriverId) {
            this.shipmentRoute = new CreateOrEditShipmentRouteDto();
            this.shipmentRoute.id = shipmentDriverId;
            this.date = moment(moment.now());
            this.time = new Date;
            this.active = true;
            this.modal.show();
        } else {
            this._shipmentRouteServiceProxy.getRouteForEdit(shipmentDriverId)
                .subscribe(result => {
                    this.shipmentRoute = result;
                    this.date = this.shipmentRoute.time;
                    this.time = moment(this.shipmentRoute.time).toDate();
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.shipmentRoute.dateInput = this.date;
        this.shipmentRoute.time = moment(this.time);
        if (this.shipmentRoute.driverId as any == "undefined") {
            this.shipmentRoute.driverId = undefined;
        }
        if (this.shipmentRoute.shipFromId as any == "undefined") {
            this.shipmentRoute.shipFromId = undefined;
        }
        this._shipmentRouteServiceProxy.createOrEditRoute(this.shipmentRoute)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

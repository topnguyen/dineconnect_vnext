import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ShipmentRoutePointDeliveryDto, ShipmentRouteServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateOrEditOrderModalComponent } from './create-or-edit-order-modal.component';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { FileUpload } from 'primeng';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
  selector: 'app-shipping-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class OrdersComponent extends AppComponentBase implements OnInit {
  @Input() currentState: any;
  @ViewChild('createOrEditOrderModal', { static: true }) createOrEditOrder: CreateOrEditOrderModalComponent;
  @ViewChild('ordersFileInput') ordersFileInput: ElementRef;
  lstOrder: any[] = [];
  siteId: number;
  loading = false;
  loadingGoods = false;
  uploadUrl = AppConsts.remoteServiceBaseUrl + '/Import/ImportShipmentOrders';
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/Import/ImportShipmentOrders';
  _uploaderOptions: FileUploaderOptions = {};
  uploader: FileUploader;
  expandedRows: {} = {};
  isFullScreen= false;


  constructor(
    injector: Injector,
    private _shipmentRouteSiteServiceProxy: ShipmentRouteServiceProxy,
    private _tokenService: TokenService,
    private _fileDownloadService: FileDownloadService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getOrders();
    this.initUploaders();
  }
  importOrders() {
    if (this.ordersFileInput) {
      this.ordersFileInput.nativeElement.click();
    }
  }
  initUploaders(): void {
    this.uploader = new FileUploader({ url: this.uploadUrl });
    this._uploaderOptions.autoUpload = true;
    this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
    this._uploaderOptions.removeAfterUpload = true;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onSuccessItem = (item, response) => {
      const resp = <IAjaxResponse>JSON.parse(response);
      if (resp.success == true) {
        this.notify.success(this.l('ImportSuccessfully'));
        this.getOrders();
        this.ordersFileInput.nativeElement.value = "";
      }
    };
    this.uploader.setOptions(this._uploaderOptions);
  }

  getOrders() {
    this.expandedRows = {};
    this.loading = true;
    this._shipmentRouteSiteServiceProxy.getFreeOrderList(
      this.siteId,
      ""
    )
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(result => {
        this.lstOrder = result;
        this.lstOrder.forEach(element => {
          element.isActive = false;
        });
        this.loading = true;
      });
  }

  clickOrder(record) {
    this.lstOrder.forEach(element => {
      element.isActive = false;
    });
    record.isActive = true;
  }

  onRowExpand(event) {
    let orderId = event.data.id;
    this.loadingGoods = true
    this._shipmentRouteSiteServiceProxy.getOrderItemsList(
      orderId,
      ""
    )
      .pipe(finalize(() => { this.loadingGoods = false; }))
      .subscribe(result => {
        event.data.goods = result;
        this.loadingGoods = true;
      });
  }

  createOrder() {
    this.createOrEditOrder.show();
  }

  dragStart(event, order: any) {
    this.currentState.draggedOrder = order;
  }

  dragEnd(event) {
    this.currentState.draggedOrder = null;
  }
  downloadTemp() {
    this._shipmentRouteSiteServiceProxy.getImportTemplateToExcel()
      .subscribe((result) => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }
  deleteOrder(order): void {
    this.message.confirm(
      '',
      this.l('AreYouSure'),
      (isConfirmed) => {
        if (isConfirmed) {
          this._shipmentRouteSiteServiceProxy.deleteOrder(order.id)
            .subscribe(() => {
              this.notify.success(this.l('SuccessfullyDeleted'));
              this.getOrders();
            });
        }
      }
    );
  }
  onChangeFullScreen(){
    this.isFullScreen = !this.isFullScreen;
}
}

import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";
import {
    EntityDto,
    ShipmentRouteServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { CreateOrEditRouteModalComponent } from "./create-or-edit-route-modal.component";
import { ShipmentService } from "../shipment.service";
import { OrdersComponent } from "@app/wheel/orders/orders.component";
import { MapsComponent } from "./maps.component";
import { DeliveriesComponent } from "./deliveries.component";
import { SitesComponent } from "./sites.component";
import { RoutesComponent } from "./routes.component";

@Component({
    selector: "app-manage-routes",
    templateUrl: "./manage-routes.component.html",
    styleUrls: ["./manage-routes.component.scss"],
    animations: [appModuleAnimation()],
})
export class ManageRoutesComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditRouteModal", { static: true })
    createOrEditRoute: CreateOrEditRouteModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("routes", { static: true }) routes: RoutesComponent;
    @ViewChild("sites", { static: true }) sites: SitesComponent;
    @ViewChild("deliveries", { static: true }) deliveries: DeliveriesComponent;
    @ViewChild("map", { static: true }) map: MapsComponent;
    @ViewChild("orders", { static: true }) orders: OrdersComponent;

    currentState: any = {
        draggedOrder: null,
    };

    advancedFiltersAreShown = false;
    filterText = "";
    dateRange: Date[] = [
        new Date(new Date().setDate(new Date().getDate() - 7)),
        new Date(new Date().setDate(new Date().getDate() + 7)),
    ];
    startDate: any;
    endDate: any;
    constructor(
        injector: Injector,
        private _shipmentRouteServiceProxy: ShipmentRouteServiceProxy,
        private _shipmentService: ShipmentService
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._shipmentService.routeIdAsObservable.subscribe((result) => {
            this.currentState.routeId = result;
        });
    }

    getRoutes(event?: LazyLoadEvent) {
        if ((this.dateRange as any) !== undefined) {
            this.startDate = this.transfromDate(this.dateRange[0]);
            this.endDate = this.transfromDate(this.dateRange[1]);
        }

        this.primengTableHelper.showLoadingIndicator();

        this._shipmentRouteServiceProxy
            .getRouteList(
                this.filterText,
                this.startDate,
                this.endDate,
                this.primengTableHelper.getSorting(this.dataTable)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.length;
                this.primengTableHelper.records = result;
                this.primengTableHelper.hideLoadingIndicator();

                this.primengTableHelper.records.forEach((element) => {
                    element.isActive = false;
                });
            });
    }

    createRoute(): void {
        this.createOrEditRoute.show();
    }

    deleteRoute(route): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._shipmentRouteServiceProxy
                    .deleteRoute(route.id)
                    .subscribe(() => {
                        this.getRoutes();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    searchRoutes() {
        this.getRoutes();
        this._shipmentService.changeRoute(0);
    }

    clickRoute(record) {
        this.primengTableHelper.records.forEach((element) => {
            element.isActive = false;
        });

        record.isActive = true;

        this.currentState.routeId = record.id;
        this._shipmentService.changeRoute(record.id);
    }

    orderAdded(id): void {
        this.sites.getSites(id);
        this.deliveries.getDeliveries();
        this.orders.getOrders();
    }

    ordersRemoved(id): void {
        this.sites.getSites();
        this.orders.getOrders();
    }

    emptyRoute(event){
        this.orders.getOrders();
    }
}

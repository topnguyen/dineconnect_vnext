import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CommonServiceProxy, ERPComboboxItem, ShipmentRouteServiceProxy, CreateOrUpdateOrderDto, ShipmentItemListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
@Component({
    selector: 'app-create-or-edit-shipment-order-modal',
    templateUrl: './create-or-edit-order-modal.component.html',
    styleUrls: ['./create-or-edit-order-modal.component.scss']

})
export class CreateOrEditOrderModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    date: moment.Moment = moment(moment.now());
    deliveryTimeFrom = new Date;
    deliveryTimeTo = new Date;
    active = false;
    saving = false;
    order: CreateOrUpdateOrderDto = new CreateOrUpdateOrderDto();
    orderItems: any[] = [];
    orderItem: ShipmentItemListDto = new ShipmentItemListDto();
    lstShipmentDepot: ERPComboboxItem[] = [];
    lstShipmentDriver: ERPComboboxItem[] = [];
    lstShipmentTypes: ERPComboboxItem[] = [];
    lstShipmentTimes: ERPComboboxItem[] = [];
    constructor(
        injector: Injector,
        private _shipmentRouteServiceProxy: ShipmentRouteServiceProxy,
        private _commonServiceProxy: CommonServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonServiceProxy.getLookups("ShipmentDepots", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentDepot = result;
        });
        this._commonServiceProxy.getLookups("ShipmentDrivers", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentDriver = result;
        });
        this._commonServiceProxy.getLookups("ShipmentTypes", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentTypes = result;
        });
        this._commonServiceProxy.getLookups("ShipmentTimes", this.appSession.tenantId, undefined).subscribe(result => {
            this.lstShipmentTimes = result;
        });
    }

    show(shipmentDriverId?: number): void {
        if (!shipmentDriverId) {
            this.order = new CreateOrUpdateOrderDto();
            this.order.id = shipmentDriverId;
            this.date = moment(moment.now());
            this.active = true;
            this.modal.show();
        } else {
            this._shipmentRouteServiceProxy.getOrderById(shipmentDriverId)
                .subscribe(result => {
                    this.order = result;
                    this.active = true;
                    this.date = moment(this.order.date);
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.order.date = moment(this.date);
        if (this.order.shipmentDepotId as any == undefined)
            this.order.shipmentDepotId = undefined

        this.order.gpsLat = null;
        this.order.gpsLon = null;
        this._shipmentRouteServiceProxy.createOrUpdateOrder(this.order)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.orderItem = new ShipmentItemListDto();
        this.modal.hide();
    }
    addOrderItem() {
        if (this.orderItem.name != null) {
            if (!this.order.items)
                this.order.items = [];
            let newOrderItem = Object.assign(new ShipmentItemListDto, this.orderItem);
            this.order.items.push(newOrderItem);
            this.orderItem.name = null;
        }
    }
    deleteOrderItem(index) {
        this.order.items.splice(index, 1);
    }
}


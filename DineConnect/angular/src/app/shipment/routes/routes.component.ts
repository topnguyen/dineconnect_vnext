import { EventEmitter, Output } from "@angular/core";
import {
    Component,
    Injector,
    ViewChild,
} from "@angular/core";
import {
    EntityDto,
    ShipmentRouteServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table, Paginator, LazyLoadEvent, MenuItem } from "primeng";
import { CreateOrEditRouteModalComponent } from "./create-or-edit-route-modal.component";
import { ShipmentService } from "../shipment.service";
import { MatMenuTrigger } from "@angular/material/menu";
import { DuplicateRouteModalComponent } from "./duplicate-route-modal.component";
@Component({
    selector: "app-routes",
    templateUrl: "./routes.component.html",
    styleUrls: ["./routes.component.scss"],
    animations: [appModuleAnimation()],
})
export class RoutesComponent extends AppComponentBase {
    @ViewChild("createOrEditRouteModal", { static: true })
    createOrEditRoute: CreateOrEditRouteModalComponent;

    @ViewChild("duplicateRouteModal", { static: true })
    duplicateRouteModal: DuplicateRouteModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild(MatMenuTrigger) contextMenu: MatMenuTrigger;
    @Output() emptyRouteEmitter: EventEmitter<any> = new EventEmitter<any>();

    advancedFiltersAreShown = false;
    filterText = "";
    dateRange: Date[] = [
        new Date(new Date().setDate(new Date().getDate() - 7)),
        new Date(new Date().setDate(new Date().getDate() + 7)),
    ];
    startDate: any;
    endDate: any;
    isFullScreen = false;
    items: MenuItem[];
    contextMenuPosition = { x: "0px", y: "0px" };
    constructor(
        injector: Injector,
        private _shipmentRouteServiceProxy: ShipmentRouteServiceProxy,
        private _shipmentService: ShipmentService
    ) {
        super(injector);
    }

    getRoutes(event?: LazyLoadEvent) {
        if ((this.dateRange as any) !== undefined) {
            this.startDate = this.transfromDate(this.dateRange[0]);
            this.endDate = this.transfromDate(this.dateRange[1]);
        }

        this.primengTableHelper.showLoadingIndicator();

        this._shipmentRouteServiceProxy
            .getRouteList(
                this.filterText,
                this.startDate,
                this.endDate,
                this.primengTableHelper.getSorting(this.dataTable)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.length;
                this.primengTableHelper.records = result;
                this.primengTableHelper.hideLoadingIndicator();

                this.primengTableHelper.records.forEach((element) => {
                    element.isActive = false;
                });
            });
    }

    createRoute(): void {
        this.createOrEditRoute.show();
    }

    deleteRoute(route): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._shipmentRouteServiceProxy
                    .deleteRoute(route.id)
                    .subscribe(() => {
                        this.getRoutes();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    searchRoutes() {
        this.getRoutes();
        this._shipmentService.changeRoute(0);
    }

    clickRoute(record) {
        this.primengTableHelper.records.forEach((element) => {
            element.isActive = false;
        });
        record.isActive = true;
        this._shipmentService.changeRoute(record.id);
    }
    emptyRoute(route): void {
        var body = new EntityDto();
        body.id = route.id;
        this._shipmentRouteServiceProxy.emptyRoute(body).subscribe(() => {
            this._shipmentService.changeRoute(route.id);
            this.emptyRouteEmitter.emit();
        });
    }

    onChangeFullScreen() {
        this.isFullScreen = !this.isFullScreen;
    }
    onContextMenu(event: MouseEvent, item: any) {
        this.clickRoute(item);
        event.preventDefault();
        this.contextMenuPosition.x = event.clientX + "px";
        this.contextMenuPosition.y = event.clientY + "px";
        this.contextMenu.menuData = { item: item };
        this.contextMenu.menu.focusFirstItem("mouse");
        this.contextMenu.openMenu();
    }

    editRoute(item) {
        this.createOrEditRoute.show(item.id);
    }
    duplicateRoute(item){
       this.duplicateRouteModal.show(item.id);
    }
    closeRoute(item){
        console.log('item', item);
        this._shipmentRouteServiceProxy.closeRoute(item.id).subscribe(result => {
            this.getRoutes();
            this.notify.success('SuccessfullySaved')
        })
    }
}

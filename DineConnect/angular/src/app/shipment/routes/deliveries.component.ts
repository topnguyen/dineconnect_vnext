import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ShipmentRouteServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ShipmentService } from '../shipment.service';
import { CreateOrEditOrderModalComponent } from './create-or-edit-order-modal.component';

@Component({
  selector: 'app-shipment-deliveries',
  templateUrl: './deliveries.component.html',
  styleUrls: ['./deliveries.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})
export class DeliveriesComponent extends AppComponentBase implements OnInit {
  @ViewChild('createOrEditOrderModal', { static: true }) createOrEditOrder: CreateOrEditOrderModalComponent;

  lstDelivery: any[] = [];
  siteId: number;
  routeId: number;
  loading = false;

  @Input() currentState: any;
  @Output() ordersRemove: EventEmitter<any> = new EventEmitter<any>();

  selectedOrders: any[] = [];
  expandedRows = {};
  isFullScreen= false;

  constructor(
    injector: Injector,
    private _shipmentRouteSiteServiceProxy: ShipmentRouteServiceProxy,
    private _shipmentServiceService: ShipmentService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._shipmentServiceService.siteIdAsObservable.subscribe(result => {
      this.siteId = result == 0 ? undefined : result;
      this.getDeliveries();
    });

    this._shipmentServiceService.routeIdAsObservable.subscribe(result => {
      this.routeId = result == 0 ? undefined : result;
    });
  }

  getDeliveries() {
    this.expandedRows = {};
    this.loading = true;
    this._shipmentRouteSiteServiceProxy.getOrderList(this.siteId, "")
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(result => {
        this.lstDelivery = result;
        this.selectedOrders = [];
        this.loading = false;
      });
  }

  removeOrders(): void {
    if (this.selectedOrders.length) {
      let orderIds = this.selectedOrders.map(t => t.id);
      this.loading = true;
      this._shipmentRouteSiteServiceProxy.removeOrdersFromRoute(orderIds, this.routeId).subscribe(t => {
        this.loading = false;
        this.ordersRemove.emit();
        this.getDeliveries();
      });
    }
  }

  onRowExpand(event) {
    this._shipmentRouteSiteServiceProxy.getOrderItemsList(event.data.id, "").subscribe(result => {
      event.data.goods = result;
    });
  }
  onChangeFullScreen(){
    this.isFullScreen = !this.isFullScreen;
}
}

import { ZoomControlOptions } from '@agm/core';
import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ShipmentRouteServiceProxy } from '@shared/service-proxies/service-proxies';
import _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { ShipmentService } from '../shipment.service';

@Component({
  selector: 'app-shipment-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent extends AppComponentBase implements OnInit {
  siteId: number;
  routeId: number;
  active = false;
  saving = false;
  lat: number = 1.287953;
  lng: number = 103.851784;
  zoomLevel: number = 13;
  zoomControlOptions: ZoomControlOptions;
  map: any;
  markers: any[] = [];
  directions: any[] = [];
  loading = false;
  lstAddress: any[] = [];
  isShowMark = false;
  isShowDirection = false;
  constructor(injector: Injector,
    private _shipmentRouteSiteServiceProxy: ShipmentRouteServiceProxy,
    private _shipmentService: ShipmentService
  ) {
    super(injector)
  }

  ngOnInit(): void {
    this._shipmentService.routeIdAsObservable.subscribe(result => {
      this.routeId = result == 0 ? undefined : result;
      this.getDeliveries();
      this.getMarkers();
    });
    this._shipmentService.changeNumberSiteAsObservable.subscribe(result => {
      if (result > 0) {
        this.getDeliveries();
        this.getMarkers();
      }
    });
  }
  getDeliveries() {
    this.loading = true;
    this._shipmentRouteSiteServiceProxy.getOrderListOnMap(this.routeId, "")
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(result => {
        this.lstAddress = result;
        console.log(' this.lstAddress', this.lstAddress);
        this.getMarkers();
        this.getDirections();
        this.loading = false;
      });
  }
  onMapReady(map: any) {
    this.map = map;
  }

  getMarkers() {
    this.markers = [];
    this.lstAddress.forEach(address => {
      let mark = {
        lat: address.latitude,
        lng: address.longitude,
        name: address.address,
        // label: address.label
        label: { color: 'white', text: address.label }
      }
      this.markers.push(mark);
    });
    console.log('markers', this.markers);
  }
  getDirections() {
    this.directions = [];
    if (this.lstAddress.length >= 2) {
      for (let i = 0; i < this.lstAddress.length - 1; i++) {
        let direction = {
          origin: { lat: +this.lstAddress[i].latitude, lng: +this.lstAddress[i].longitude, label: this.lstAddress[i].label },
          destination: { lat: +this.lstAddress[i + 1].latitude, lng: +this.lstAddress[i + 1].longitude, label: this.lstAddress[i + 1].label }
        }
        this.directions.push(direction);
      }
    }
    console.log('directions', this.directions);
  }

}

import {
  Component,
  Injector,
  Output,
  EventEmitter,
  ViewChild,
  Input
} from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
  LocationDto,
  LocationServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
  selector: 'select-location',
  templateUrl: './select-location-modal.component.html',
})

export class SelectLocationModalComponent extends AppComponentBase {
  @Input() selectedLocation: LocationDto;
  @Output() locationAdded: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
  @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

  filterText = '';

  constructor(
    injector: Injector,
    private _locationServiceProxy: LocationServiceProxy
  ) {
    super(injector);
    this.primengTableHelper = new PrimengTableHelper();
  }

  show(): void {
    this.primengTableHelper.records = [];
    this.modal.show();
  }


  shown(): void {
    this.getLocations(null);
  }

  getLocations(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    this._locationServiceProxy.getList(
      this.filterText,
      false,
      0,
      this.primengTableHelper.getSorting(this.locationDataTable),
      this.primengTableHelper.getMaxResultCount(this.locationPaginator, event),
      this.primengTableHelper.getSkipCount(this.locationPaginator, event)
    )
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe(result => {
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.totalRecordsCount = result.totalCount;

        if (result.items) {
          this.selectedLocation = result.items[0];
          this.locationAdded.emit(this.selectedLocation);
        }
      });
  }

  save() {
    this.locationAdded.emit(this.selectedLocation);
    this.close();
  }

  close() {
    this.modal.hide();
  }
}


import { SelectLocationOrGroupComponent } from "./../../../shared/common/select-location-or-group/select-location-or-group.component";
import {
    ChangeDetectorRef,
    Component,
    Injector,
    OnInit,
    ViewChild,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CategoryServiceProxy,
    CommonLocationGroupDto,
    CreateOrEditDinePlanTaxDto,
    CreateOrUpdateDinePlanTaxLocationInput,
    DepartmentsServiceProxy,
    DinePlanTaxesServiceProxy,
    DinePlanTaxLocationEditDto,
    DinePlanTaxMappingEditDto,
    MenuItemServiceProxy,
    TransactionTypesServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-create-or-edit-dine-plan-tax",
    templateUrl: "./create-or-edit-dine-plan-tax.component.html",
    styleUrls: ["./create-or-edit-dine-plan-tax.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditDinePlanTaxComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("dinePlanTaxForm", { static: true }) dinePlanTaxForm: NgForm;
    @ViewChild("dinePlanTaxMappingForm", { static: false })
    dinePlanTaxMappingForm: NgForm;

    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("selectFilterLocationOrGroup", { static: true })
    selectFilterLocationOrGroup: SelectLocationOrGroupComponent;

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    // --

    saving = false;
    openTabMapping = false;

    // --

    transactionTypes = [];
    departments = [];
    categories = [];
    menuItems = [];
    allMenuItems = [];

    // --

    dinePlanTaxId: number;
    dinePlanTax = new CreateOrEditDinePlanTaxDto();

    filter: {
        locations?: CommonLocationGroupDto;
    } = {};

    // --

    taxMappingTaxLocation = new DinePlanTaxLocationEditDto();
    taxMappingLocationGroup: CommonLocationGroupDto;
    taxMappingTaxMappings: DinePlanTaxMappingEditDto[] = [];

    // --

    currentIndex = 0;

    // --

    get isEdit() {
        return !!this.dinePlanTaxId;
    }

    get isEditTaxMapping() {
        return !!this.taxMappingTaxLocation?.id;
    }

    get formIsValid() {
        if (this.openTabMapping) {
            if (!this.taxMappingTaxMappings.length) {
                return false;
            }

            for (const taxMapping of this.taxMappingTaxMappings) {
                if (
                    !taxMapping.categoryId ||
                    !taxMapping.departmentId ||
                    !taxMapping.menuItemId
                ) {
                    return false;
                }
            }

            if (!this.taxMappingLocationGroup) {
                return false;
            }

            if (this.taxMappingLocationGroup.group) {
                if (!this.taxMappingLocationGroup.groups?.length) {
                    return false;
                }
            } else if (this.taxMappingLocationGroup.locationTag) {
                if (!this.taxMappingLocationGroup.locationTags?.length) {
                    return false;
                }
            } else if (
                !this.taxMappingLocationGroup.locations?.length &&
                !this.taxMappingLocationGroup.nonLocations?.length
            ) {
                return false;
            }

            return this.dinePlanTaxMappingForm.form.valid;
        } else {
            return this.dinePlanTaxForm.form.valid;
        }
    }

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _dinePlanTaxesServiceProxy: DinePlanTaxesServiceProxy,
        private _transactionTypesServiceProxy: TransactionTypesServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _departmentServiceProxy: DepartmentsServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);

        this.primengTableHelper.totalRecordsCount = 0;
        this.primengTableHelper.records = [];
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.dinePlanTaxId = +params["id"]; // (+) converts string 'id' to a number

            this._changeDetector.detectChanges();

            this.getDataForCombobox();

            if (isNaN(this.dinePlanTaxId)) {
                this.dinePlanTax = new CreateOrEditDinePlanTaxDto();
                this.dinePlanTaxId = this.dinePlanTax.id;
            } else {
                this._dinePlanTaxesServiceProxy
                    .getDinePlanTaxForEdit(this.dinePlanTaxId)
                    .subscribe((result) => {
                        this.dinePlanTax = result.dinePlanTax;
                    });

                this.getLocationTaxes();
            }
        });
    }

    save() {
        this.saving = true;

        if (this.openTabMapping) {
            const input = new CreateOrUpdateDinePlanTaxLocationInput();

            input.dinePlanTaxLocation = this.taxMappingTaxLocation;
            input.dinePlanTaxLocation.dinePlanTaxRefId = this.dinePlanTaxId;
            input.dinePlanTaxLocation.locationGroup =
                this.taxMappingLocationGroup;
            input.taxTemplateMapping = this.taxMappingTaxMappings;

            this._dinePlanTaxesServiceProxy
                .createOrUpdateTaxLocation(input)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.openTabMapping = false;

                    this.taxMappingTaxLocation =
                        new DinePlanTaxLocationEditDto();
                    this.taxMappingLocationGroup = undefined;
                    this.taxMappingTaxMappings = [];

                    this.dinePlanTaxMappingForm.resetForm();

                    this.notify.success(this.l("SavedSuccessfully"));
                    this.reloadPage();
                });
        } else {
            this._dinePlanTaxesServiceProxy
                .createOrEdit(this.dinePlanTax)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        }
    }

    close() {
        this._router.navigate(["/app/connect/master/dinePlanTaxes"]);
    }

    // --

    getDataForCombobox() {
        this._transactionTypesServiceProxy
            .getTransactionTypeForLookupTable()
            .subscribe((result) => {
                this.transactionTypes = result.items;
            });

        if (this.isEdit) {
            this._categoryServiceProxy
                .getCategoriesForCombobox()
                .subscribe((result) => {
                    this.categories = result.items.map((item) => {
                        return {
                            value: +item.value,
                            displayText: item.displayText,
                        };
                    });
                });

            this._departmentServiceProxy
                .getDepartmentsForCombobox()
                .subscribe((result) => {
                    this.departments = result.items.map((item) => {
                        return {
                            value: +item.value,
                            displayText: item.displayText,
                        };
                    });
                });

            this.getMenuItems();
        }
    }

    getMenuItems(categoryId?, index?) {
        this._menuItemServiceProxy
            .getMenuItemForCategoryCombobox(categoryId)
            .subscribe((result) => {
                this.currentIndex = index || 0;
                this.menuItems = result.items.map((item) => {
                    return { value: +item.id, displayText: item.name };
                });

                if (!categoryId) {
                    this.allMenuItems = result.items.map((item) => {
                        return { value: +item.id, displayText: item.name };
                    });
                }
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.taxMappingLocationGroup);
    }

    getLocations(event) {
        this.taxMappingLocationGroup = event;
    }

    // --

    getLocationTaxes(event?: LazyLoadEvent) {
        if (this.isEdit) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this.paginator.changePage(0);
                return;
            }

            this.primengTableHelper.showLoadingIndicator();

            let locationIds = "";

            if (this.filter.locations?.group) {
                locationIds = this.filter.locations.groups
                    .map((x) => x.id)
                    .join(",");
            } else if (this.filter.locations?.locationTag) {
                locationIds = this.filter.locations.locationTags
                    .map((x) => x.id)
                    .join(",");
            } else if (this.filter.locations?.nonLocations?.length) {
                locationIds = this.filter.locations.nonLocations
                    .map((x) => x.id)
                    .join(",");
            } else if (this.filter.locations?.locations?.length) {
                locationIds = this.filter.locations.locations
                    .map((x) => x.id)
                    .join(",");
            }

            this._dinePlanTaxesServiceProxy
                .getLocationTaxesForGivenTax(
                    this.dinePlanTaxId,
                    this.primengTableHelper.getSorting(this.dataTable),
                    this.primengTableHelper.getMaxResultCount(
                        this.paginator,
                        event
                    ),
                    this.primengTableHelper.getSkipCount(this.paginator, event),
                    locationIds,
                    this.filter.locations?.group,
                    this.filter.locations?.locationTag,
                    !!this.filter.locations?.nonLocations?.length
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount =
                        result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    openFilterSelectLocationOrGroupModal() {
        this.selectFilterLocationOrGroup.show(this.filter.locations);
    }

    setFilterSelectLocations(event: CommonLocationGroupDto) {
        this.filter.locations = event;
        this.getLocationTaxes();
    }

    clearFilter() {
        this.filter = {};

        this.taxMappingTaxLocation = new DinePlanTaxLocationEditDto();
        this.taxMappingLocationGroup = undefined;
        this.taxMappingTaxMappings = [];

        this.dinePlanTaxMappingForm.resetForm();

        this.paginator.changePage(0);
    }

    // --

    getTaxLocationForEdit(taxLocationId: number, isClone?: boolean) {
        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxLocationForEdit(taxLocationId)
            .subscribe((result) => {
                if (isClone) {
                    this.taxMappingTaxLocation = new DinePlanTaxLocationEditDto(
                        result.dinePlanTaxLocation
                    );
                    this.taxMappingTaxLocation.id = undefined;
                    this.taxMappingTaxLocation.locationGroup = undefined;
                    this.taxMappingLocationGroup = new CommonLocationGroupDto();

                    result.taxTemplateMapping.forEach((x) => {
                        x.id = undefined;
                        x.dinePlanTaxLocationId = undefined;
                    });
                } else {
                    this.taxMappingTaxLocation = result.dinePlanTaxLocation;
                    this.taxMappingLocationGroup =
                        this.taxMappingTaxLocation.locationGroup;
                }

                this.taxMappingTaxMappings = result.taxTemplateMapping;
            });
    }

    viewTaxLocation(item) {
        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxLocationForEdit(item.id)
            .subscribe((result) => {
                this.selectLocationOrGroup.show(
                    result.dinePlanTaxLocation.locationGroup,
                    true
                );
            });
    }

    addItem() {
        const item = new DinePlanTaxMappingEditDto({
            dinePlanTaxLocationId: 0,
            departmentId: undefined,
            categoryId: undefined,
            menuItemId: undefined,
        } as any);

        this.taxMappingTaxMappings.push(item);
    }

    removeItem(index) {
        this.taxMappingTaxMappings.splice(index, 1);
    }

    editLocationTax(data) {
        this.getTaxLocationForEdit(data.id);
        this.openTabMapping = true;
    }

    cloneLocationTax(data) {
        this.getTaxLocationForEdit(data.id, true);
        this.openTabMapping = true;
    }

    deleteLocationTax(data) {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._dinePlanTaxesServiceProxy
                    .deleteDinePlanTaxLocation(data.id)
                    .pipe(
                        finalize(() => {
                            this.reloadPage();
                        })
                    )
                    .subscribe(() => {
                        this.notify.success(this.l("DeletedSuccessfully"));
                    });
            }
        });
    }
}

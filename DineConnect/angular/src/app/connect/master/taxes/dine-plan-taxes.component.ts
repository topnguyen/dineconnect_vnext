import {
    Component,
    Injector,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CommonLocationGroupDto,
    DinePlanTaxDto,
    DinePlanTaxesServiceProxy,
    EntityDto,
    LocationDto,
    TokenAuthServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { NotifyService } from "abp-ng2-module";
import _ from "lodash";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";

@Component({
    templateUrl: "./dine-plan-taxes.component.html",
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class DinePlanTaxesComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;

    advancedFiltersAreShown = false;
    isDeleted = false;
    filterText = "";
    filterLocationsInput = new CommonLocationGroupDto();
    filterLocations = new CommonLocationGroupDto();

    constructor(
        injector: Injector,
        private _dinePlanTaxesServiceProxy: DinePlanTaxesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getDinePlanTaxes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        let locationIds = "";

        if (this.filterLocations.group) {
            locationIds = this.filterLocations.groups
                .map((x) => x.id)
                .join(",");
        } else if (this.filterLocations.locationTag) {
            locationIds = this.filterLocations.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (this.filterLocations.nonLocations?.length) {
            locationIds = this.filterLocations.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (this.filterLocations.locations?.length) {
            locationIds = this.filterLocations.locations
                .map((x) => x.id)
                .join(",");
        }

        this._dinePlanTaxesServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                locationIds,
                this.filterLocations.group,
                this.filterLocations.locationTag,
                !!this.filterLocations.nonLocations?.length,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    search() {
        this.filterLocations = _.cloneDeep(this.filterLocationsInput);
        this.getDinePlanTaxes();
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.filterLocationsInput);
    }

    selectLocations(event) {
        this.filterLocationsInput = event;
    }

    reset() {
        this.filterText = "";
        this.filterLocationsInput = new CommonLocationGroupDto();
        this.filterLocations = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    // --

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditDinePlanTax(promotionId?: number): void {
        this._router.navigate([
            "/app/connect/master/dinePlanTaxes/create-or-update",
            promotionId ? promotionId : "null",
        ]);
    }

    deleteDinePlanTax(dinePlanTax: DinePlanTaxDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._dinePlanTaxesServiceProxy
                    .delete(dinePlanTax.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(dinePlanTax: DinePlanTaxDto) {
        const body = EntityDto.fromJS({ id: dinePlanTax.id });
        this._dinePlanTaxesServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    exportToExcel(): void {
        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxesToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}

import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ComboboxItemDto,
    CreateOrUpdateNumeratorInput,
    NumeratorEditDto,
    NumeratorServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-update-numerator',
  templateUrl: './create-update-numerator.component.html',
  styleUrls: ['./create-update-numerator.component.scss'],
  animations: [appModuleAnimation()]
})
export class CreateUpdateNumeratorComponent extends AppComponentBase implements OnInit {
    saving = false;
    numeratorId: number;
    numerator = new NumeratorEditDto();
    resetNumeratorTypes: ComboboxItemDto[] = [];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _numeratorServiceProxy: NumeratorServiceProxy
    ) {
        super(injector);
        this.numeratorId = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this._init();
    }

    close() {
        this._router.navigate(['/app/connect/master/numerators']);
    }

    save(): void {
        const input = new CreateOrUpdateNumeratorInput();
        input.numerator = this.numerator;

        this.saving = true;
        this._numeratorServiceProxy
            .createOrUpdateNumerator(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    private _init() {
        this._numeratorServiceProxy.getResetNumeratorTypes().subscribe((result) => {
            this.resetNumeratorTypes = result.items;
        });

        if (this.numeratorId && this.numeratorId != NaN) {
            this._numeratorServiceProxy.getNumeratorForEdit(this.numeratorId).subscribe((result) => {
                this.numerator = result.numerator;
            }); 
        } else {
            this.numerator.numberFormat = "#";
            this.numerator.outputFormat = "[Number]"
        }
    }
}
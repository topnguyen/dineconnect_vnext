import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { EntityDto, NumeratorListDto, NumeratorServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-numerator',
    templateUrl: './numerator.component.html',
    styleUrls: ['./numerator.component.scss'],
    animations: [appModuleAnimation()]
})
export class NumeratorComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _numeratorServiceProxy: NumeratorServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {}

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._numeratorServiceProxy
            .getAll(
                this.filterText,
                undefined,
                undefined,
                this.isDeleted,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditNumerator(id?: number): void {
        this._router.navigate(['/app/connect/master/numerators/create-or-update', id ? id : 'null']);
    }

    deleteNumerator(numerator: NumeratorListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._numeratorServiceProxy.deleteNumerator(numerator.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(numerator: NumeratorListDto) {
        const body = EntityDto.fromJS({ id: numerator.id });
        this._numeratorServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    exportToExcel(): void {
        this._numeratorServiceProxy.getAllToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }
}

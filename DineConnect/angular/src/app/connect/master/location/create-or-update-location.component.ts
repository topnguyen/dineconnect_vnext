import { Component, Injector, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    LocationDto,
    CompanyServiceProxy,
    AddressesServiceProxy,
    LocationServiceProxy,
    LocationScheduleEditDto,
    LocationBranchEditDto,
    DineQueueServiceProxy,
    QueueLocationDto,
    SimpleLocationDto,
    LocationGroupServiceProxy,
    LocationGroupDto,
    LocationTagDto,
    LocationTagsServiceProxy,
    CreateOrEditLocationTagDto
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrUpdateLocation',
    styleUrls: ['./create-or-update-location.component.scss'],
    templateUrl: './create-or-update-location.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrUpdateLocationModalComponent extends AppComponentBase implements OnInit {
    @Output() locationUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    active = false;
    saving = false;

    locationEditDto: LocationDto = new LocationDto();
    companyListDto: LocationDto[] = [];
    companyListDropDown = [];
    filterText = '';
    transactionDate = new Date();
    startDate = new Date();
    endDate = new Date();
    locations: SimpleLocationDto[] = [];
    requestedLocations: any[] = [];

    stateLoaded = false;
    cityLoaded = false;
    countryListDropDown = [];
    cityListDropDown = [];
    stateListDropDown = [];
    queueLocations: QueueLocationDto[] = [];
    locationGroups: LocationGroupDto[] = [];
    locationTags: LocationTagDto[] = [];

    id: number;
    scname: string = '';
    scStartTime: Date;
    scEndTime: Date;
    locationBranch = new LocationBranchEditDto();
    datachangeallowed = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _companyServiceProxy: CompanyServiceProxy,
        private _addressesServiceProxy: AddressesServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _dineQueueServiceProxy: DineQueueServiceProxy,
        private _locationGroupServiceProxy: LocationGroupServiceProxy,
        private _locationTagsServiceProxy: LocationTagsServiceProxy
    ) {
        super(injector);
    }

    save() {
        this.saving = true;
        this.locationEditDto.branch = this.locationBranch;
        this.locationEditDto.requestedLocations = JSON.stringify(this.requestedLocations);

        if (this.locationEditDto.locationGroups) {
            this.locationEditDto.locationGroups = this.locationEditDto.locationGroups.map(item => {
                return new LocationGroupDto(item);
            })
        }

        if (this.locationEditDto.locationTags) {
            this.locationEditDto.locationTags = this.locationEditDto.locationTags.map(item => {
                let tmp = new LocationTagDto();
                tmp.init( new CreateOrEditLocationTagDto(item))
                return tmp;
            })
        }

        this._locationServiceProxy
            .createOrUpdte(this.locationEditDto)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.saving = false;
                this.notify.success(this.l('SuccessfullySaved'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/connect/master/locations']);
    }

    ngOnInit() {
        this.getCompanyList();
        this.getQueueLocations()
        this.getLocations();
        this._getLocationGroups();
        this._getLocationTags();

        this._activatedRoute.params.subscribe((params) => {
            this.id = +params['id']; // (+) converts string 'id' to a number

            if (!isNaN(this.id)) {
                this._locationServiceProxy.get(this.id).subscribe((result) => {
                    this.locationEditDto = result;
                    this.locationEditDto.countryId = this.locationEditDto.countryId || undefined;
                    this.locationEditDto.stateId = this.locationEditDto.stateId || undefined;
                    this.locationEditDto.cityId = this.locationEditDto.cityId || undefined;
                    this.locationEditDto.schedules = this.locationEditDto.schedules || [];
                    this.locationBranch = new LocationBranchEditDto(this.locationEditDto.branch);
                    this.datachangeallowed = !this.locationEditDto.houseTransactionDate;
                    this.requestedLocations = this.locationEditDto.requestedLocations ? JSON.parse(this.locationEditDto.requestedLocations) : [];

                    this.getCountryList();
                    this.getStateList(this.locationEditDto.countryId);
                    this.getCityList(this.locationEditDto.countryId, this.locationEditDto.stateId);
                });
            } else {
                this.getCountryList();
                this.locationEditDto = new LocationDto();
            }
        });
    }

    getCompanyList() {
        this._companyServiceProxy.getAll(undefined, false, undefined, 1000, 0).subscribe((result) => {
            this.companyListDropDown = [];
            result.items.forEach((element) => {
                let a = { label: element.name, value: element.id };
                this.companyListDropDown.push(a);
            });
        });
    }

    getCountryList() {
        this.countryListDropDown = [];

        this._addressesServiceProxy.getCountryList(undefined, false, undefined, 1000, 0).subscribe((result) => {
            result.items.forEach((element) => {
                let a = { label: element.name, value: element.id };
                this.countryListDropDown.push(a);
            });
        });
    }

    getStateList(countryId: number) {
        this.stateListDropDown = [];
        this.cityListDropDown = [];
        if (+this.locationEditDto.countryId) {
            this._addressesServiceProxy.getStateListByCountry(countryId).subscribe((result) => {
                result.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDown.push(a);
                });

                this.stateLoaded = true;
            });
        }
    }

    getCityList(countryId: number, stateId: number) {
        this.cityListDropDown = [];
        if (+countryId && +stateId) {
            this._addressesServiceProxy.getCityList(undefined, countryId, stateId, false, undefined, 1000, 0).subscribe((result) => {
                result.items.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.cityListDropDown.push(a);
                });

                this.cityLoaded = true;
            });
        }
    }

    getQueueLocations() {
        this._dineQueueServiceProxy.getQueueLocationList().subscribe(result => {
            this.queueLocations = result.items;
        })
    }

    setCountry() {
        this.locationEditDto.stateId = undefined;
        this.locationEditDto.cityId = undefined;
        this.getStateList(this.locationEditDto.countryId);
    }

    setState() {
        this.locationEditDto.cityId = undefined;
        this.getCityList(this.locationEditDto.countryId, this.locationEditDto.stateId);
    }

    // setBrandCountry() {
    //     this.locationBranch.state = undefined;
    //     this.locationBranch.city = undefined;
    //     this.getStateList(+this.locationBranch.country);
    // }

    // setBrandState() {
    //     this.locationBranch.city  = undefined;
    //     this.getCityList(+this.locationBranch.country, +this.locationBranch.state);
    // }

    addSchedule() {
        const newSchedule = new LocationScheduleEditDto({
            name: this.scname,
            startHour: this.scStartTime.getHours(),
            endHour: this.scEndTime.getHours(),
            startMinute: this.scStartTime.getMinutes(),
            endMinute: this.scEndTime.getMinutes(),
            locationId: this.locationEditDto.id
        } as LocationScheduleEditDto);

        if (!this.scname || this.scname.trim().length == 0) {
            return;
        }
        if (!this.scStartTime || !this.scEndTime || this.scStartTime.getTime() > this.scEndTime.getTime()) {
            this.notify.error(this.l('WrongDateRange'));
            return;
        }
        if (!this.locationEditDto.schedules) {
            this.locationEditDto.schedules = [];
        }

        this.locationEditDto.schedules.push(newSchedule);
        this.scname = '';
    }

    removeSchedule(index) {
        this.locationEditDto.schedules.splice(index, 1);
    }

    getLocations() {
        this._locationServiceProxy.getSimpleLocations(undefined, undefined, undefined, 'name ASC', 1000, 0).subscribe((result) => {
            this.locations = result.items.filter(item => item.id != this.locationEditDto.id);
        });
    };

    private _getLocationGroups() {
        this._locationGroupServiceProxy.getAllGroup(undefined, undefined, 1000, 0).subscribe(result => {
            this.locationGroups = result.items;
        })
    }

    private _getLocationTags() {
        this._locationTagsServiceProxy.getAllTags(undefined, undefined, 1000, 0).subscribe(result => {
            this.locationTags = result.items;
        })
    }

    get requestLocations() {
        if (this.requestedLocations && this.requestedLocations.length > 0) {
            return (this.locations || []).filter(l => this.requestedLocations.includes(l.id));
        } else {
            return [];
        }
    }
}

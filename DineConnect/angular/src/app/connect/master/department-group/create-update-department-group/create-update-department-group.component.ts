import { Component, ViewChild, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {
    DepartmentsServiceProxy,
    CreateOrUpdateDepartmentGroupInput,
    DepartmentGroupEditDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'app-create-update-department-group',
    templateUrl: './create-update-department-group.component.html',
    styleUrls: ['./create-update-department-group.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateUpdateDepartmentGroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectLocationComponent;

    saving = false;

    departmentGroup: DepartmentGroupEditDto = new DepartmentGroupEditDto();

    priceTags = [];

    departmentGroupId: number;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _departmentsServiceProxy: DepartmentsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.departmentGroupId = +params['id']; // (+) converts string 'id' to a number

            if (!isNaN(this.departmentGroupId)) {
                this._departmentsServiceProxy.getDepartmentGroupForEdit(this.departmentGroupId).subscribe((result) => {
                    this.departmentGroup = result.departmentGroup;
                });
            }
        });
    }

    save(): void {
        this.saving = true;
        const input = new CreateOrUpdateDepartmentGroupInput();
        input.departmentGroup = this.departmentGroup;
    
        this._departmentsServiceProxy
            .createOrUpdateDepartmentGroup(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(['/app/connect/master/department-group']);
    }
}

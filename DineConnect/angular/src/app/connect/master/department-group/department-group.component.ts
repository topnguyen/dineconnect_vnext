import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DepartmentsServiceProxy, DepartmentDto, DepartmentGroup, EntityDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';

@Component({
  templateUrl: './department-group.component.html',
  styleUrls: ['./department-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class DepartmentGroupComponent extends AppComponentBase {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;

    constructor(
        injector: Injector,
        private _departmentsServiceProxy: DepartmentsServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._departmentsServiceProxy.getAllDepartmentGroup(
            this.filterText,
            undefined,
            this.isDeleted,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getMaxResultCount(this.paginator, event),
            this.primengTableHelper.getSkipCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditDepartmentGroup(id?: number): void {
        this._router.navigate(['/app/connect/master/department-group/create-or-update', id ? id : 'null']);
    }

    deleteDepartmentGroup(departmentGroup: DepartmentGroup): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._departmentsServiceProxy.deleteDepartmentGroup(departmentGroup.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    activateItem(departmentGroup: DepartmentGroup) {
        const body = EntityDto.fromJS({ id: departmentGroup.id });
        this._departmentsServiceProxy.activateDepartmentGroupItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    exportToExcel(): void {
        this._departmentsServiceProxy.getAllDepartmentGroupToExcel()
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

}

import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    ComboboxItemDto,
    CommonLocationGroupDto,
    CreateOrUpdateTicketTypeInput,
    CreateTicketTypeConfigurationInput,
    NumeratorServiceProxy,
    ScreenMenuServiceProxy,
    TicketTypeConfigurationEditDto,
    TicketTypeConfigurationServiceProxy,
    TicketTypeEditDto,
    TicketTypeServiceProxy,
    TransactionTypesServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    templateUrl: "./create-update-ticket-type.component.html",
    styleUrls: ["./create-update-ticket-type.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateUpdateTicketTypeComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("selectLocationOrGroupFilter", { static: true })
    selectLocationOrGroupFilter: SelectLocationOrGroupComponent;

    saving = false;
    ticketTypeConfiguration = new TicketTypeConfigurationEditDto();
    ticketTypeConfigurationId: number;
    ticketType = new TicketTypeEditDto();
    filterLocationGroup: CommonLocationGroupDto = new CommonLocationGroupDto();
    locationGroup: CommonLocationGroupDto = new CommonLocationGroupDto();
    numerators: ComboboxItemDto[] = [];
    transactionTypes: ComboboxItemDto[] = [];
    screenMenus: ComboboxItemDto[] = [];
    TAB = {
        TICKET_TYPE: 1,
        MAP: 2,
    };
    selectedTab: number = this.TAB.TICKET_TYPE;

    paging = {
        first: 0,
        page: 1,
        rows: this.primengTableHelper.defaultRecordsCountPerPage,
        pageCount: 0,
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _ticketTypeServiceProxy: TicketTypeServiceProxy,
        private _numeratorServiceProxy: NumeratorServiceProxy,
        private _transactionTypesServiceProxy: TransactionTypesServiceProxy,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _ticketTypeConfigurationServiceProxy: TicketTypeConfigurationServiceProxy
    ) {
        super(injector);
        this.ticketTypeConfigurationId =
            +this._activatedRoute.snapshot.params["id"];
    }

    ngOnInit() {
        this._init();
    }

    selectFilterLocations(result) {
        this.filterLocationGroup = result;
        this._init();
    }

    clearFilterLocation() {
        this.filterLocationGroup = new CommonLocationGroupDto();
        this._init();
    }

    openFilterLocation() {
        this.selectLocationOrGroupFilter.show(this.filterLocationGroup);
    }

    openLocation() {
        this.selectLocationOrGroup.show(this.locationGroup);
    }

    selectLocations(result) {
        this.locationGroup = result;
    }

    close() {
        this._router.navigate(["/app/connect/master/ticket-types"]);
    }

    save(): void {
        const input = new CreateTicketTypeConfigurationInput();
        input.ticketTypeConfiguration = this.ticketTypeConfiguration;

        this.saving = true;
        this._ticketTypeConfigurationServiceProxy
            .createOrUpdateTicketTypeConfiguration(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    onChangePage(paging) {
        this.paging = paging;
    }

    saveTicketType() {
        this.ticketType.ticketTypeConfigurationId =
            this.ticketTypeConfiguration.id;
        this.ticketType.locationGroup = this.locationGroup;

        const input = new CreateOrUpdateTicketTypeInput();
        input.ticketType = this.ticketType;
        this.saving = true;

        this._ticketTypeServiceProxy
            .addOrEditTicketType(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                    this.locationGroup = new CommonLocationGroupDto();
                    this._init();
                    this.selectedTab = this.TAB.TICKET_TYPE;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l("SavedSuccessfully"));
            });
    }

    removePortion(ticketType: TicketTypeEditDto) {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._ticketTypeServiceProxy
                    .deleteTicketTypeMapping(ticketType.id)
                    .subscribe(() => {
                        this._init();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    editPortion(ticketType: TicketTypeEditDto) {
        console.log(ticketType);
        this.ticketType = ticketType;
        this.locationGroup = ticketType.locationGroup;
        this.selectedTab = this.TAB.MAP;
    }
    clonePortion(ticketType: TicketTypeEditDto) {
        this.ticketType = ticketType;
        this.ticketType.id = null;
        this.locationGroup = new CommonLocationGroupDto();
        this.selectedTab = this.TAB.MAP;
    }
    viewPortion(ticketType: TicketTypeEditDto) {
        this.locationGroup = ticketType.locationGroup;
        this.openLocation();
    }

    onChangeTab(tab: number) {
        if (tab == this.TAB.TICKET_TYPE) {
            this.ticketType = new TicketTypeEditDto();
        }

        this.selectedTab = tab;
    }

    private _init() {
        this._numeratorServiceProxy
            .getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                1000,
                0
            )
            .subscribe((result) => {
                this.numerators = result.items.map((item) => {
                    return ComboboxItemDto.fromJS({
                        value: item.id,
                        displayText: item.name,
                    });
                });

                if (
                    this.ticketTypeConfigurationId &&
                    this.ticketTypeConfigurationId != NaN
                ) {
                    this.primengTableHelper.isLoading = true;
                    this._ticketTypeServiceProxy
                        .getTicketTypeForEdit(
                            this.ticketTypeConfigurationId,
                            undefined,
                            this.filterLocationGroup.locations,
                            this.filterLocationGroup.groups,
                            this.filterLocationGroup.locationTags,
                            this.filterLocationGroup.nonLocations,
                            this.filterLocationGroup.group,
                            this.filterLocationGroup.locationTag,
                            undefined
                        )
                        .pipe(
                            finalize(
                                () =>
                                    (this.primengTableHelper.isLoading = false)
                            )
                        )
                        .subscribe((result) => {
                            this.ticketTypeConfiguration =
                                result.ticketTypeConfiguration;
                            this.primengTableHelper.records =
                                this.ticketTypeConfiguration.ticketTypes;
                            this.primengTableHelper.records.forEach((item) => {
                                item.ticketNumeratorName = (
                                    this.numerators.find(
                                        (n) => n.value == item.ticketNumeratorId
                                    ) || {}
                                ).displayText;
                                item.orderNumeratorName = (
                                    this.numerators.find(
                                        (n) => n.value == item.orderNumeratorId
                                    ) || {}
                                ).displayText;
                                item.saleTransactionTypeName = (
                                    this.transactionTypes.find(
                                        (n) =>
                                            n.value ==
                                            item.saleTransactionType_Id
                                    ) || {}
                                ).displayText;
                            });
                            this.primengTableHelper.totalRecordsCount =
                                this.ticketTypeConfiguration.ticketTypes.length;

                            this.paging.page = 0;
                        });
                }
            });

        this._transactionTypesServiceProxy
            .getTransactionTypeForLookupTable()
            .subscribe((result) => {
                this.transactionTypes = result.items;
            });
        this._screenMenuServiceProxy
            .getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                0,
                1000
            )
            .subscribe((result) => {
                this.screenMenus = result.items.map((item) => {
                    return ComboboxItemDto.fromJS({
                        value: item.id,
                        displayText: item.name,
                    });
                });
            });
    }
}

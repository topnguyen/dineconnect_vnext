import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import {
    PaymentTypesServiceProxy,
    PaymentTypeDto,
    CommonLocationGroupDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import { finalize } from "rxjs/operators";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";

@Component({
    templateUrl: "./payment-types.component.html",
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class PaymentTypesComponent extends AppComponentBase {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    isAcceptChange = undefined;
    filterText = "";
    nameFilter = "";
    isDeleted: boolean;
    locationNameFilter = "";
    locationOrGroup: CommonLocationGroupDto;

    constructor(
        injector: Injector,
        private _paymentTypesServiceProxy: PaymentTypesServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router
    ) {
        super(injector);
    }

    getPaymentTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._paymentTypesServiceProxy
            .getAll(
                this.filterText,
                this.nameFilter,
                this.locationNameFilter,
                this.isAcceptChange,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditPaymentType(paymentTypeId?: number): void {
        this._router.navigate([
            "/app/connect/master/paymentTypes/create-or-update",
            paymentTypeId ? paymentTypeId : "null",
        ]);
    }

    deletePaymentType(paymentType: PaymentTypeDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._paymentTypesServiceProxy
                    .delete(paymentType.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(paymentType: PaymentTypeDto) {
        const body = EntityDto.fromJS({ id: paymentType.id });
        this._paymentTypesServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    exportToExcel(): void {
        this._paymentTypesServiceProxy
            .getPaymentTypesToExcel(
                this.filterText,
                this.nameFilter,
                this.locationNameFilter,
                this.isAcceptChange
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.isAcceptChange = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getPaymentTypes();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

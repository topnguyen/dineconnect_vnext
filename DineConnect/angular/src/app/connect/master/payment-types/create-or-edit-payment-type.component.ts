import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    PaymentTypesServiceProxy,
    CreateOrEditPaymentTypeDto,
    UpdatePaymentTypeImageInput,
    ProfileServiceProxy,
    CommonLocationGroupDto,
    DepartmentsServiceProxy,
    ComboboxItemDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Router, ActivatedRoute } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileUploader, FileUploaderOptions, FileItem } from "ng2-file-upload";
import { AppConsts } from "@shared/AppConsts";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { DomSanitizer } from "@angular/platform-browser";
import { TokenService, IAjaxResponse } from "abp-ng2-module";
import { CreateOrEditLanguageDescriptionModalComponent } from "@app/connect/menu/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-payment-type",
    templateUrl: "./create-or-edit-payment-type.component.html",
    styleUrls: ["./create-or-edit-payment-type.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditPaymentTypeComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;

    saving = false;

    paymentType: CreateOrEditPaymentTypeDto = new CreateOrEditPaymentTypeDto();

    locationName = "";
    paymentTags = [];
    paymentProcessors = [];
    paymentSetting: any = {};
    departments = [];
    selectedDepartments: ComboboxItemDto[];
    paymentTenderTypes = [];
    id: number;

    public uploader: FileUploader;
    remoteServiceBaseUrl =
        AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImage";
    filePreviewPath: any;

    private sub: any;

    constructor(
        injector: Injector,
        private _paymentTypesServiceProxy: PaymentTypesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _profileService: ProfileServiceProxy,
        private _tokenService: TokenService,
        private _sanitizer: DomSanitizer,
        private _departmentsServiceProxy: DepartmentsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getDepartmentForSelection();
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.id = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.id)) {
                this.paymentType = new CreateOrEditPaymentTypeDto();
                this.locationName = "";
                this.paymentType.locationGroup = new CommonLocationGroupDto();
                this.paymentType.paymentTenderType = 0;
            } else {
                this._paymentTypesServiceProxy
                    .getPaymentTypeForEdit(this.id)
                    .subscribe((result) => {
                        this.paymentType = result.paymentType;
                        if (this.paymentType.departments)
                            this.selectedDepartments = JSON.parse(
                                this.paymentType.departments
                            );

                        this.locationName = result.locationName;
                        this.getPaymentTags();
                        this.getProcessor();
                    });
            }

            this.getProcessorsForCbo();
        });

        this.initFileUploader();
    }

    save(): void {
        this.saving = true;
        this.setPaymentTags();
        this.setProcessors();

        this.paymentType.departments = JSON.stringify(this.selectedDepartments);

        this._paymentTypesServiceProxy
            .createOrEdit(this.paymentType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.uploader.uploadAll();

                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.paymentType.locationGroup);
    }

    setLocationIdNull() {
        this.paymentType.locationId = null;
        this.locationName = "";
    }

    getLocations(e) {
        this.paymentType.locationGroup = e;
    }

    close(): void {
        this._router.navigate(["/app/connect/master/paymentTypes"]);
    }

    private getPaymentTags() {
        this.paymentTags = [];
        if (this.paymentType.paymentTag) {
            let tags = this.paymentType.paymentTag.split(",");

            tags.forEach((tag, index) => {
                this.paymentTags.push({ display: tag, value: index });
            });
        }
    }

    private setPaymentTags() {
        let tags = [];
        this.paymentTags.forEach((tag) => {
            tags.push(tag.display);
        });

        this.paymentType.paymentTag = tags.join(",");
    }

    private getProcessorsForCbo() {
        this._paymentTypesServiceProxy
            .getPaymentProcessors()
            .subscribe((result) => {
                this.paymentProcessors = result.items;
            });
        this._paymentTypesServiceProxy
            .getPaymentTenderType()
            .subscribe((result) => {
                this.paymentTenderTypes = result.items;
            });
    }

    private setProcessors() {
        this.paymentType.processors = JSON.stringify(this.paymentSetting);
    }

    private getProcessor() {
        this.paymentSetting = {};
        if (this.paymentType.processors) {
            this.paymentSetting = JSON.parse(this.paymentType.processors);
        }
    }

    initFileUploader() {
        this.uploader = new FileUploader({
            url: this.remoteServiceBaseUrl,
            authToken: "Bearer " + this._tokenService.getToken(),
        });

        this.uploader.onAfterAddingFile = (f) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
            this.filePreviewPath = this._sanitizer.bypassSecurityTrustUrl(
                window.URL.createObjectURL(f._file)
            );
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l("UploadImageSuccessfully"));
                this.paymentType.uploadedImageToken =
                    ajaxResponse.result.fileToken;
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
    }

    removeFile(item) {
        item.remove();
        this.paymentType.uploadedImageToken = undefined;
    }

    getDepartmentForSelection() {
        this._departmentsServiceProxy
            .getDepartmentsForCombobox()
            .subscribe((result) => {
                this.departments = result.items;
            });
    }

    showLanguages() {
        this.createOrEditLanguageDescriptionModal.show(
            "PaymentType",
            this.id,
            this.id + " - " + this.paymentType.name
        );
    }
}

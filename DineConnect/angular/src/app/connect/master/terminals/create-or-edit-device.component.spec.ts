import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CreateOrEditDeviceModalComponent } from "./create-or-edit-device.component";

describe("CreateOrEditDeviceComponent", () => {
    let component: CreateOrEditDeviceModalComponent;
    let fixture: ComponentFixture<CreateOrEditDeviceModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CreateOrEditDeviceModalComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateOrEditDeviceModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});

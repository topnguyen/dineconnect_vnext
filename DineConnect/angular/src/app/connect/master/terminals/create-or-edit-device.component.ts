import {
    Component,
    EventEmitter,
    Injector,
    Output,
    ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ConnectCardServiceProxy } from "@shared/service-proxies/service-proxies";
import { Guid } from "guid-typescript";
import { ModalDirective } from "ngx-bootstrap/modal";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-create-or-edit-device",
    templateUrl: "./create-or-edit-device.component.html",
    styleUrls: ["./create-or-edit-device.component.css"],
})
export class CreateOrEditDeviceModalComponent extends AppComponentBase {
    @ViewChild("modal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    isShown = false;
    filterText = "";
    saving = false;
    allDeviceNames = [
        "Generic Modem",
        "Artech",
        "Call Center Client",
        "Mettler Toledo",
        "Custom Scale",
    ];
    deviceNames = [];
    deviceName = "Generic Modem";
    dataModal = {};
    isDisable: boolean;
    deviceNameSelected = [];

    constructor(
        injector: Injector,
        private _connectCardServiceProxy: ConnectCardServiceProxy
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {}

    show(data?, selectedDevices?): void {
        this.deviceNames = this.allDeviceNames;
        this.deviceNameSelected = selectedDevices;
        this.dataModal = data;
        if (data && data.DeviceName) {
            this.deviceName = data.DeviceName;
            this.isDisable = true;
        } else {
            var dataNotSelected = [];
            this.deviceNames.forEach((value, key) => {
                if (!this.deviceNameSelected.includes(value)) {
                    dataNotSelected.push(value);
                }
            });
            this.deviceNames = dataNotSelected;
            this.deviceName = this.deviceNames[0];
            this.isDisable = false;
        }
        this.modal.show();
    }

    close(): void {
        this.isShown = false;
        this.dataModal = {};
        this.modal.hide();
    }

    shown(): void {
        this.isShown = true;
    }

    save() {
        let key = Guid.create();
        (this.dataModal as any).DeviceName = this.deviceName;
        (this.dataModal as any).Key =
            (this.dataModal as any).Key ?? key.toString();
        this.modalSave.emit(this.dataModal);
        this.close();
    }
}

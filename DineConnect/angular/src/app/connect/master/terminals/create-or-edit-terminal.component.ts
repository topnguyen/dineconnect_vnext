import {
    Component,
    ViewChild,
    Injector,
    OnInit,
    ElementRef,
} from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    TerminalsServiceProxy,
    CreateOrEditTerminalDto,
    CommonLocationGroupDto,
    AutoDayCloseDto,
    TicketTypeServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute, Router } from "@angular/router";
import { CreateOrEditDeviceModalComponent } from "./create-or-edit-device.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { result } from "lodash";
import { AppConsts } from "@shared/AppConsts";
import { FileUploader } from "ng2-file-upload";
import { SafeUrl } from "@angular/platform-browser";
import { IAjaxResponse, TokenService } from "abp-ng2-module";

@Component({
    selector: "app-create-or-edit-terminal",
    templateUrl: "./create-or-edit-terminal.component.html",
    styleUrls: ["./create-or-edit-terminal.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditTerminalComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;

    @ViewChild("createOrEditDevice", { static: true })
    createOrEditDevice: CreateOrEditDeviceModalComponent;

    saving = false;

    terminal: CreateOrEditTerminalDto = new CreateOrEditTerminalDto();
    terminalId: number;
    settings: any = {};
    private sub: any;
    autoDayCloses = [];
    settingName = "";
    devices = [];
    deviceIndex = -1;
    ticketTypes = [];
    uploadUrl = AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadImage";
    uploader: FileUploader;
    uploaderDineMenuImage: FileUploader;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _terminalsServiceProxy: TerminalsServiceProxy,
        private _ticketTypeService: TicketTypeServiceProxy,
        private _tokenService: TokenService,
        private el: ElementRef
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.terminalId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.terminalId)) {
                this.terminal = new CreateOrEditTerminalDto();
                this.terminal.locationGroup = new CommonLocationGroupDto();
            } else {
                this._terminalsServiceProxy
                    .getTerminalForEdit(this.terminalId)
                    .subscribe((result) => {
                        this.terminal = result;
                        if (this.terminal.settings) {
                            this.settings = JSON.parse(this.terminal.settings);
                            this.devices = [];
                            if (this.settings.AdditionalDevices) {
                                this.devices = JSON.parse(
                                    this.settings.AdditionalDevices
                                );
                            }
                        }
                        this.autoDayCloses = this.terminal.autoDayCloses;
                        this.autoDayCloses.map((item) => {
                            item.closeTime = new Date(item.closeTime);
                        });
                    });
            }
        });

        this.getTicketTypes();
        this.initUpload();
    }

    save(): void {
        this.saving = true;
        this.settings.AdditionalDevices = JSON.stringify(this.devices);
        this.terminal.settings = JSON.stringify(this.settings);
        this.terminal.autoDayCloses = this.autoDayCloses;

        this._terminalsServiceProxy
            .createOrEdit(this.terminal)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/terminals"]);
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.terminal.locationGroup);
    }

    getLocations(event) {
        this.terminal.locationGroup = event;
    }

    private initUpload() {
        this.uploader = new FileUploader({
            url: this.uploadUrl,
            authToken: "Bearer " + this._tokenService.getToken(),
            queueLimit: 1,
        });

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                if (ajaxResponse.result.message == null) {
                    this.settings.logoPath = ajaxResponse.result.url;
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
        this.uploaderDineMenuImage = new FileUploader({
            url: this.uploadUrl,
            authToken: "Bearer " + this._tokenService.getToken(),
            queueLimit: 1,
        });

        this.uploaderDineMenuImage.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                if (ajaxResponse.result.message == null) {
                    this.settings.dineMenuImagePath = ajaxResponse.result.url;
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
    }

    onFileSelected() {
        this.uploader.uploadAll();
    }

    onDineMenuImageFileSelected() {
        this.uploaderDineMenuImage.uploadAll();
    }

    returnNumberWithComma(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }

        if (keynum === 11) {
            return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[0123456789,]/;
        return charcheck.test(keychar);
    }

    addAutoDayClose() {
        if (!this.autoDayCloses) {
            this.autoDayCloses = [];
        }
        let newInput = new AutoDayCloseDto();
        newInput.notifyType = 5;
        this.autoDayCloses.push(newInput);
    }

    removeAutoDayClose(index) {
        this.autoDayCloses.splice(index, 1);
    }

    itemSelected(name) {
        this.settingName = name;
        console.log(name);
    }

    showDeviceModal(data, index?) {
        let deviceNameSelected = [];
        this.devices.forEach((el) => {
            deviceNameSelected.push(el.DeviceName);
        });
        this.createOrEditDevice.show(data, deviceNameSelected);

        this.deviceIndex = index;
    }

    addDevice(event) {
        if (this.deviceIndex > -1) this.devices[this.deviceIndex] = event;
        else this.devices.push(event);
    }

    removeDevice(index) {
        this.devices.splice(index, 1);
    }

    private getTicketTypes() {
        this._ticketTypeService
            .getAllTicketTypeForLookupTable()
            .subscribe((result) => {
                this.ticketTypes = result.items;
                this.ticketTypes.unshift({
                    value: "",
                    displayText: this.l("NotAssigned"),
                });
            });
    }
}

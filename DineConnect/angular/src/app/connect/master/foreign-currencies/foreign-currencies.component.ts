import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
    EntityDto,
    ForeignCurrenciesServiceProxy,
    ForeignCurrencyDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TokenAuthServiceProxy } from "@shared/service-proxies/service-proxies";
import { CreateOrEditForeignCurrencyModalComponent } from "./create-or-edit-foreign-currency-modal.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { EntityTypeHistoryModalComponent } from "@app/shared/common/entityHistory/entity-type-history-modal.component";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    selector: "app-foreign-currencies",
    templateUrl: "./foreign-currencies.component.html",
    styleUrls: ["./foreign-currencies.component.scss"],
    animations: [appModuleAnimation()],
})
export class ForeignCurrenciesComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditForeignCurrencyModal", { static: true })
    createOrEditForeignCurrencyModal: CreateOrEditForeignCurrencyModalComponent;
    @ViewChild("entityTypeHistoryModal", { static: true })
    entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    isDeleted;
    paymentTypeNameFilter = "";

    _entityTypeFullName =
        "DinePlan.DineConnect.Connect.Master.ForeignCurrencies.ForeignCurrency";
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _foreignCurrenciesServiceProxy: ForeignCurrenciesServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return (
            customSettings.EntityHistory &&
            customSettings.EntityHistory.isEnabled &&
            _.filter(
                customSettings.EntityHistory.enabledEntities,
                (entityType) => entityType === this._entityTypeFullName
            ).length === 1
        );
    }

    getForeignCurrencies(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._foreignCurrenciesServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.paymentTypeNameFilter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createForeignCurrency(): void {
        this.createOrEditForeignCurrencyModal.show();
    }

    showHistory(foreignCurrency: ForeignCurrencyDto): void {
        this.entityTypeHistoryModal.show({
            entityId: foreignCurrency.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: "",
        });
    }

    deleteForeignCurrency(foreignCurrency: ForeignCurrencyDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._foreignCurrenciesServiceProxy
                    .delete(foreignCurrency.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    exportToExcel(): void {
        this._foreignCurrenciesServiceProxy
            .getForeignCurrenciesToExcel(
                this.filterText,
                this.paymentTypeNameFilter
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    activateItem(foreignCurrency: ForeignCurrencyDto) {
        const body = EntityDto.fromJS({ id: foreignCurrency.id });
        this._foreignCurrenciesServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }
}

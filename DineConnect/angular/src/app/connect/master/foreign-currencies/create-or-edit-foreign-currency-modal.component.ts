import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ForeignCurrenciesServiceProxy, CreateOrEditForeignCurrencyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'createOrEditForeignCurrencyModal',
  templateUrl: './create-or-edit-foreign-currency-modal.component.html'
})
export class CreateOrEditForeignCurrencyModalComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  foreignCurrency: CreateOrEditForeignCurrencyDto = new CreateOrEditForeignCurrencyDto();
  paymentTypes = [];

  constructor(
    injector: Injector,
    private _foreignCurrenciesServiceProxy: ForeignCurrenciesServiceProxy
  ) {
    super(injector);
  }

  show(foreignCurrencyId?: number): void {

    if (!foreignCurrencyId) {
      this.foreignCurrency = new CreateOrEditForeignCurrencyDto();

      this.active = true;
      this.modal.show();
    } else {
      this._foreignCurrenciesServiceProxy.getForeignCurrencyForEdit(foreignCurrencyId)
        .subscribe(result => {
          this.foreignCurrency = result.foreignCurrency;

          this.active = true;
          this.modal.show();
        });
    }
    this.getPaymentTypes();
  }

  save(): void {
    this.saving = true;


    this._foreignCurrenciesServiceProxy.createOrEdit(this.foreignCurrency)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  setPaymentTypeIdNull() {
    this.foreignCurrency.paymentTypeId = null;
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  private getPaymentTypes() {
    this._foreignCurrenciesServiceProxy.getAllPaymentTypeForLookupTable(undefined)
      .subscribe((result) => {
        this.paymentTypes = result.items;
      });
  }
}

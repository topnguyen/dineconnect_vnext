import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    FutureDateInformationsServiceProxy,
    CreateOrEditFutureDateInformationDto,
    CommonLocationGroupDto,
    ComboboxItemDto,
    PriceTagsServiceProxy,
    DinePlanTaxesServiceProxy,
    ScreenMenuServiceProxy,
    MenuItemServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute, Router } from "@angular/router";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-future-date",
    templateUrl: "./create-or-edit-future-date.component.html",
    styleUrls: ["./create-or-edit-future-date.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditFutureDateComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;

    saving = false;
    futureDateInformation: CreateOrEditFutureDateInformationDto =
        new CreateOrEditFutureDateInformationDto();
    futureDateInformationId: number;
    futureDateTypes: ComboboxItemDto[] = [];
    minDate = new Date();
    priceTags: ComboboxItemDto[] = [];
    taxes: ComboboxItemDto[] = [];
    screenMenus: ComboboxItemDto[] = [];
    menuItems: ComboboxItemDto[] = [];

    FUTURE_DATE_TYPE = {
        PRICE_TAG: 2,
        TAX: 3,
        SCREEN_MENU: 4,
        MENU_ITEM: 1,
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _futureDateInformationsServiceProxy: FutureDateInformationsServiceProxy,
        private _priceTagsServiceProxy: PriceTagsServiceProxy,
        private _dinePlanTaxesServiceProxy: DinePlanTaxesServiceProxy,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
        this._init();
    }

    ngOnInit() {
        this.minDate = new Date();

        this._activatedRoute.params.subscribe((params) => {
            this.futureDateInformationId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.futureDateInformationId)) {
                this.futureDateInformation =
                    new CreateOrEditFutureDateInformationDto();
                this.futureDateInformation.locationGroup =
                    new CommonLocationGroupDto();
            } else {
                this._futureDateInformationsServiceProxy
                    .getFutureDateInformationForEdit(
                        this.futureDateInformationId
                    )
                    .subscribe((result) => {
                        this.futureDateInformation = result;
                    });
            }
        });
        this.getFutureDateTypes();
    }

    save(): void {
        this.saving = true;

        this._futureDateInformationsServiceProxy
            .createOrEdit(this.futureDateInformation)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/future-data"]);
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.futureDateInformation.locationGroup);
    }

    getLocations(event) {
        this.futureDateInformation.locationGroup = event;
    }

    getFutureDateTypes() {
        this._futureDateInformationsServiceProxy
            .getFutureDateTypeForLookupTable()
            .subscribe((result) => {
                this.futureDateTypes = result.items;
            });
    }

    private _init() {
        this._priceTagsServiceProxy
            .getPriceTagsForCombobox(undefined)
            .subscribe((result) => {
                this.priceTags = result;
            });
        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxForCombobox(undefined)
            .subscribe((result) => {
                this.taxes = result;
            });
        this._screenMenuServiceProxy
            .getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                0,
                1000
            )
            .subscribe((result) => {
                this.screenMenus = result.items.map((item) => {
                    return ComboboxItemDto.fromJS({
                        value: item.id,
                        displayText: item.name,
                    });
                });
            });

        this._menuItemServiceProxy
            .getAllItems(
                undefined,
                undefined,
                undefined,
                1000,
                0,
                undefined,
                undefined
            )
            .subscribe((result) => {
                this.menuItems = result.items.map((item) => {
                    return ComboboxItemDto.fromJS({
                        value: item.id,
                        displayText: item.name,
                    });
                });
            });
    }
}

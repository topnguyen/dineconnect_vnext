import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";
import { Router } from "@angular/router";
import {
    FutureDateInformationsServiceProxy,
    FutureDateInformationDto,
    CommonLocationGroupDto,
    ComboboxItemDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";

@Component({
    templateUrl: "./future-dates.component.html",
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class FutureDatesComponent extends AppComponentBase implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    isDeleted = false;
    locationOrGroup: CommonLocationGroupDto;
    futureDateTypes: ComboboxItemDto[] = [];
    type: number;
    futureDate: moment.Moment;

    FUTURE_DATE_TYPE = {
        PRICE_TAG: 2,
        TAX: 3,
        SCREEN_MENU: 4,
        MENU_ITEM: 1,
    };

    constructor(
        injector: Injector,
        private _futureDatesServiceProxy: FutureDateInformationsServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit() {
        this._futureDatesServiceProxy
            .getFutureDateTypeForLookupTable()
            .subscribe((result) => {
                this.futureDateTypes = result.items;
            });
    }

    getAlls(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._futureDatesServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.futureDate,
                this.type,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditFutureDate(id?: number): void {
        this._router.navigate([
            "/app/connect/master/future-data/create-or-update",
            id ? id : "null",
        ]);
    }

    deleteFutureDate(futureDate: FutureDateInformationDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._futureDatesServiceProxy
                    .delete(futureDate.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(futureDate: FutureDateInformationDto): void {
        const body = EntityDto.fromJS({ id: futureDate.id });
        this._futureDatesServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l("Successfully"));
            this.reloadPage();
        });
    }

    openTypePage(data: FutureDateInformationDto) {
        // if (data.status == 'Updated' && data.entityId > 0) {
        //     this.notify.error('Cant able to Open, Future Data is already updated');
        //     return;
        // }
        switch (data.futureDateType) {
            case this.FUTURE_DATE_TYPE.MENU_ITEM:
                if (data.contents)
                    this._router.navigate(
                        [
                            "/app/connect/menu/menuItem/create-or-update",
                            data.contents,
                        ],
                        {
                            queryParams: {
                                futureDataId: data.id,
                            },
                        }
                    );
                else this._router.navigate(["/app/connect/menu/menuItem"]);
                break;
            case this.FUTURE_DATE_TYPE.PRICE_TAG:
                if (data.contents)
                    this._router.navigate(
                        [
                            "/app/connect/tags/priceTags/edit-price-for-tag",
                            data.contents,
                        ],
                        {
                            queryParams: {
                                futureDataId: data.id,
                            },
                        }
                    );
                else this._router.navigate(["/app/connect/tags/priceTags"]);
                break;
            case this.FUTURE_DATE_TYPE.TAX:
                if (data.contents)
                    this._router.navigate(
                        [
                            "/app/connect/master/dinePlanTaxes/create-or-update",
                            data.contents,
                        ],
                        {
                            queryParams: {
                                futureDataId: data.id,
                            },
                        }
                    );
                else
                    this._router.navigate([
                        "/app/connect/master/dinePlanTaxes",
                    ]);
                break;
            case this.FUTURE_DATE_TYPE.SCREEN_MENU:
                if (data.contents)
                    this._router.navigate(
                        [
                            "/app/connect/menu/screen-menus/create-or-update",
                            data.contents,
                        ],
                        {
                            queryParams: {
                                id: data.contents,
                                futureDataId: data.id,
                            },
                        }
                    );
                else this._router.navigate(["/app/connect/menu/screen-menus"]);
                break;

            default:
                break;
        }
    }

    exportToExcel(): void {
        this._futureDatesServiceProxy
            .getFutureDateInformationsToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.futureDate = undefined;
        this.type = undefined;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getAlls();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }
}

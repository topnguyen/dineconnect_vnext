import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditTillAccountComponent } from './create-or-edit-till-account.component';

describe('CreateOrEditTillAccountComponent', () => {
  let component: CreateOrEditTillAccountComponent;
  let fixture: ComponentFixture<CreateOrEditTillAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditTillAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditTillAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injector, ViewChild, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
    TillAccountsServiceProxy,
    TillAccountDto,
    CommonLocationGroupDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";

@Component({
    selector: "app-till-accounts",
    templateUrl: "./till-accounts.component.html",
    styleUrls: ["./till-accounts.component.scss"],
    animations: [appModuleAnimation()],
})
export class TillAccountsComponent extends AppComponentBase implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    isDeleted;
    locationOrGroup: CommonLocationGroupDto;
    tillAccountTypeId = undefined;

    tillAccountTypes = [];

    constructor(
        injector: Injector,
        private _tillAccountsServiceProxy: TillAccountsServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getTillAccountTypes();
    }

    private getTillAccountTypes() {
        this._tillAccountsServiceProxy
            .getAccountTypeForLookupTable()
            .subscribe((result) => {
                this.tillAccountTypes = result.items;
            });
    }

    getTillAccounts(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._tillAccountsServiceProxy
            .getAll(
                this.filterText,
                this.tillAccountTypeId,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditTillAccount(id?: number): void {
        this._router.navigate([
            "/app/connect/master/tillAccounts/create-or-update",
            id ? id : "null",
        ]);
    }

    deleteTillAccount(tillAccount: TillAccountDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._tillAccountsServiceProxy
                    .delete(tillAccount.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(tillAccount: TillAccountDto) {
        const body = EntityDto.fromJS({ id: tillAccount.id });
        this._tillAccountsServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    exportToExcel(): void {
        this._tillAccountsServiceProxy
            .getTillAccountsToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.tillAccountTypeId = undefined;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getTillAccounts();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

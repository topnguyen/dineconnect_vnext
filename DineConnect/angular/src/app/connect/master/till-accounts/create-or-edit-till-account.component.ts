import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CreateOrEditTillAccountDto,
    TillAccountsServiceProxy,
    CommonLocationGroupDto,
} from "@shared/service-proxies/service-proxies";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-till-account",
    templateUrl: "./create-or-edit-till-account.component.html",
    styleUrls: ["./create-or-edit-till-account.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditTillAccountComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;

    saving = false;

    tillAccount: CreateOrEditTillAccountDto = new CreateOrEditTillAccountDto();

    tillAccountTypes = [];
    tillAccountId: number;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _tillAccountsServiceProxy: TillAccountsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.tillAccountId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.tillAccountId)) {
                this.tillAccount = new CreateOrEditTillAccountDto();
                this.tillAccount.locationGroup = new CommonLocationGroupDto();
            } else {
                this._tillAccountsServiceProxy
                    .getTillAccountForEdit(this.tillAccountId)
                    .subscribe((result) => {
                        this.tillAccount = result;
                    });
            }
        });

        this.getTillAccountTypes();
    }

    save(): void {
        this.saving = true;

        this._tillAccountsServiceProxy
            .createOrEdit(this.tillAccount)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/tillAccounts"]);
    }

    private getTillAccountTypes() {
        this._tillAccountsServiceProxy
            .getAccountTypeForLookupTable()
            .subscribe((result) => {
                this.tillAccountTypes = result.items;
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.tillAccount.locationGroup);
    }

    getLocations(event) {
        this.tillAccount.locationGroup = event;
    }
}

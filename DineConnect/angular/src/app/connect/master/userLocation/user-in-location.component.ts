import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    Input,
    OnInit,
    OnChanges
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserLocationServiceProxy, LocationDto } from '@shared/service-proxies/service-proxies';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { AddUserModalComponent } from './add-user-modal.component';
import { DefaultLocationUpdateService } from './services/defaultLocationUpdateService';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'userInLocation',
    templateUrl: './user-in-location.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserInLocationComponent extends AppComponentBase
    implements OnInit, OnChanges {
    @Input() selectedLocation: LocationDto;

    @ViewChild('addUserModal', { static: true }) addUserModal: AddUserModalComponent;
    @ViewChild('usersInLocationDataTable', { static: true }) usersInLocationDataTable: Table;
    @ViewChild('usersInLocationPaginator', { static: true }) usersInLocationPaginator: Paginator;

    filterTextForUserTable = '';
    usersInLocationTableHelper: PrimengTableHelper;
    organizationListDropDown = [];
    organizationSelected: number;
    //selectedLocation: LocationDto;
    loadUserInLocation = false;

    constructor(
        injector: Injector,
        private _userLocationServiceProxy: UserLocationServiceProxy,
        private _defaultLocationUpdateService: DefaultLocationUpdateService
    ) {
        super(injector);
        this.usersInLocationTableHelper = new PrimengTableHelper();
    }

    ngOnInit() {
        // console.log("From user in location ng on init");
        // this.getUsersInLocation();
    }

    ngOnChanges() {
        this.getUsersInLocation();
    }

    getUsersInLocation(event?: LazyLoadEvent) {
        if (
            this.usersInLocationTableHelper.getMaxResultCount(
                this.usersInLocationPaginator,
                event
            )
        ) {
            if (this.usersInLocationTableHelper.shouldResetPaging(event)) {
                this.usersInLocationPaginator.changePage(0);
                return;
            }
            this.usersInLocationTableHelper.showLoadingIndicator();
            this._userLocationServiceProxy
                .getUsersOfLocation(
                    this.selectedLocation.id,
                    this.usersInLocationTableHelper.getSorting(
                        this.usersInLocationDataTable
                    ),
                    this.usersInLocationTableHelper.getMaxResultCount(
                        this.usersInLocationPaginator,
                        event
                    ),
                    this.usersInLocationTableHelper.getSkipCount(
                        this.usersInLocationPaginator,
                        event
                    )
                )
                .subscribe(result => {
                    this.usersInLocationTableHelper.records = result.items;
                    this.usersInLocationTableHelper.totalRecordsCount =
                        result.totalCount;
                    this.usersInLocationTableHelper.records = result.items;
                    this.usersInLocationTableHelper.hideLoadingIndicator();
                });
        }
    }

    openAddModal() {
        this.addUserModal.locationId = this.selectedLocation.id;
        this.addUserModal.show();
    }

    removeUserFromLocation(userLocationId: number) {
        this._userLocationServiceProxy
            .removeUserFromLocation(userLocationId)
            .subscribe(result => {
                this._defaultLocationUpdateService.updateDetails(false);
                this.notify.success(this.l('SuccessfullyRemoved'));
                this.getUsersInLocation();
            });
        this._defaultLocationUpdateService.updateDetails(true);
    }
}

import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewEncapsulation,
    ViewChild,
    OnInit
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    UserLocationServiceProxy,
    LocationDto,
    UserLocationDto,
    FindUserLocationUsersInput
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { DefaultLocationUpdateService } from './services/defaultLocationUpdateService';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'addUserModal',
    templateUrl: './add-user-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddUserModalComponent extends AppComponentBase implements OnInit {
    locationId = 0;

    @Output() userAdded: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('addUserToLocationDataTable', { static: true }) addUserToLocationDataTable: Table;
    @ViewChild('addUserToLocationPaginator', { static: true })
    addUserToLocationPaginator: Paginator;

    modalRef: BsModalRef;
    config = {
        backdrop: true,
        class: 'modal-lg',
        ignoreBackdropClick: true
    };
    filterTextForUserTable = '';

    UsersNotInLocationTableHelper: PrimengTableHelper;

    organizationListDropDown = [];
    organizationSelected: number;
    selectedLocation: LocationDto;
    loadUserInLocation = false;

    constructor(
        injector: Injector,
        private _userLocationServiceProxy: UserLocationServiceProxy,
        private _defaultLocationUpdateService: DefaultLocationUpdateService
    ) {
        super(injector);
        this.UsersNotInLocationTableHelper = new PrimengTableHelper();
    }

    show(): void {
        this.UsersNotInLocationTableHelper.records = [];
        this.modal.show();
    }

    ngOnInit() { }
    shown(): void {
        this.getUserToAdd(null);
    }
    getUserToAdd(event?: LazyLoadEvent) {
        if (this.UsersNotInLocationTableHelper.shouldResetPaging(event)) {
            this.addUserToLocationPaginator.changePage(0);
            return;
        }

        this.UsersNotInLocationTableHelper.showLoadingIndicator();
        let findUserLocationUsersInput = new FindUserLocationUsersInput();
        findUserLocationUsersInput.filter = this.filterTextForUserTable;

        findUserLocationUsersInput.maxResultCount = this.UsersNotInLocationTableHelper.getMaxResultCount(
            this.addUserToLocationPaginator,
            event
        );

        findUserLocationUsersInput.skipCount = this.UsersNotInLocationTableHelper.getSkipCount(
            this.addUserToLocationPaginator,
            event
        );
        findUserLocationUsersInput.locationId = this.locationId;

        this._userLocationServiceProxy
            .findUsers(findUserLocationUsersInput)
            .subscribe(result => {
                this.UsersNotInLocationTableHelper.records = result.items;
                this.UsersNotInLocationTableHelper.totalRecordsCount =
                    result.totalCount;
                this.UsersNotInLocationTableHelper.records = result.items;
                this.UsersNotInLocationTableHelper.hideLoadingIndicator();
            });
    }

    addUserToLocation(userId: number) {
        let userLocation = new UserLocationDto();
        userLocation.locationId = this.locationId;
        userLocation.userId = userId;
        this._userLocationServiceProxy
            .addUserToLocation(userLocation)
            .subscribe(result => {
                this._defaultLocationUpdateService.updateDetails(true);
                this.notify.success(this.l('SuccessfullyAdded'));
                this.userAdded.emit(true);
                this.modal.hide();
            });

        this._defaultLocationUpdateService.updateDetails(false);
    }

    close() {
        this.modal.hide();
    }
}

import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import {
    CompanyServiceProxy,
    LocationServiceProxy,
    LocationDto
} from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'userLocation',
    templateUrl: './user-location.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class UserLocationComponent extends AppComponentBase implements OnInit {
    @ViewChild('locationDataTable', { static: true }) locationDataTable: Table;
    @ViewChild('locationPaginator', { static: true }) locationPaginator: Paginator;

    filterText = '';

    organizationListDropDown = [];
    organizationSelected: number;
    selectedLocation: LocationDto;
    loadUserInLocation = false;
    constructor(
        injector: Injector,
        private _companyServiceProxy: CompanyServiceProxy,
        private _locationServiceProxy: LocationServiceProxy,
        private _activatedRoute: ActivatedRoute,
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    ngOnInit() {
        this.getOrganizationList();
    }

    getLocationList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.locationPaginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._locationServiceProxy
            .getList(
                this.filterText,
                false,
                this.organizationSelected,
                this.primengTableHelper.getSorting(this.locationDataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.locationPaginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(
                    this.locationPaginator,
                    event
                )
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    getOrganizationList() {
        this.organizationListDropDown = [];
        this._companyServiceProxy
            .getAll(undefined, false, undefined, 1000, 0)
            .subscribe(result => {
                result.items.forEach(element => {
                    this.organizationListDropDown.push({
                        label: element.name,
                        value: element.id
                    });
                });
                this.organizationListDropDown.push({
                    label: 'All organization',
                    value: 0
                });
            });
    }

    createTenant() {}

    selectLocation(location: LocationDto) {
        this.selectedLocation = location;
        this.loadUserInLocation = true;
    }
}

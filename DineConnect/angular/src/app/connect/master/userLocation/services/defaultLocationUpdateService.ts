import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class DefaultLocationUpdateService {
    private isDefaultLocationRemoved = new Subject<boolean>();

    currentIsDefaultLocationRemoved$ = this.isDefaultLocationRemoved.asObservable();

    updateDetails(customerOrderId: boolean) {
        console.log(customerOrderId);
        this.isDefaultLocationRemoved.next(customerOrderId);
    }
}

import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CreateOrEditPlanReasonDto,
    PlanReasonsServiceProxy,
    CommonLocationGroupDto,
} from "@shared/service-proxies/service-proxies";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize } from "rxjs/operators";
import { CreateOrEditLanguageDescriptionModalComponent } from "@app/connect/menu/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-plan-reason",
    templateUrl: "./create-or-edit-plan-reason.component.html",
    styleUrls: ["./create-or-edit-plan-reason.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditPlanReasonComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;

    saving = false;

    planReason: CreateOrEditPlanReasonDto = new CreateOrEditPlanReasonDto();

    reasonGroups = [];
    planReasonId: number;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _planReasonsServiceProxy: PlanReasonsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.planReasonId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.planReasonId)) {
                this.planReason = new CreateOrEditPlanReasonDto();
                this.planReason.locationGroup = new CommonLocationGroupDto();
            } else {
                this._planReasonsServiceProxy
                    .getPlanReasonForEdit(this.planReasonId)
                    .subscribe((result) => {
                        this.planReason = result;
                    });
            }
        });

        this.getReasonGroups();
    }

    save(): void {
        this.saving = true;

        this._planReasonsServiceProxy
            .createOrEdit(this.planReason)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/planReasons"]);
    }

    private getReasonGroups() {
        this._planReasonsServiceProxy
            .getReasonGroupForLookupTable()
            .subscribe((result) => {
                this.reasonGroups = result.items;
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.planReason.locationGroup);
    }

    getLocations(event) {
        this.planReason.locationGroup = event;
    }

    showLanguages() {
        this.createOrEditLanguageDescriptionModal.show(
            "Reason",
            this.planReasonId,
            this.planReasonId + " - " + this.planReason.reason
        );
    }

    checkboxChanged() {
        if (this.planReason.reasonGroupCheckBox) {
            this.planReason.reasonGroup = "";
        }
    }
}

import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Injector,
    Output,
    ViewChild
} from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    LocationTagsServiceProxy,
    LocationTagDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

export interface IOrganizationUnitOnEdit {
    id?: number;
    parentId?: number;
    displayName?: string;
}

@Component({
    selector: 'create-or-edit-tag-modal',
    templateUrl: './create-or-edit-tag-modal.component.html'
})

export class CreateOrEditLocationTagModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output()
    tagAddedOrUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    active = false;
    saving = false;

    locationTag: LocationTagDto;

    constructor(
        injector: Injector,
        private _locationTagServiceProxy: LocationTagsServiceProxy,
        private _changeDetector: ChangeDetectorRef
    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('LocationTag_Code').focus();
    }

    show(tag: LocationTagDto): void {
        if (!tag || tag == null) {
            tag = new LocationTagDto();
        }
        this.locationTag = tag;
        this.active = true;
        this.modal.show();
        this._changeDetector.detectChanges();

    }

    save(): void {
        this.saving = true;
        this._locationTagServiceProxy
            .createOrEdit(this.locationTag)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.tagAddedOrUpdated.emit(true);
            });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}

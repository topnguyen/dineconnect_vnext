import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewEncapsulation,
    ViewChild,
    OnInit
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import {
    LocationDto,
    AddLocationToTagDto,
    LocationTagsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'add-location-to-tag-modal',
    templateUrl: './add-location-to-tag-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddLocationToTagModalComponent extends AppComponentBase {

    @Output() locationAdded: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('addLocationToTagDataTable', { static: true }) addLocationToTagDataTable: Table;
    @ViewChild('addLocationToTagPaginator', { static: true }) addLocationToTagPaginator: Paginator;

    selectedLocations: LocationDto[];
    tagId = null;

    filterTextForUserTable = '';
    primengTableHelper: PrimengTableHelper;
    selectedLocation: LocationDto;

    constructor(
        injector: Injector,
        private _userLocationServiceProxy: LocationTagsServiceProxy
    ) {
        super(injector);
        this.primengTableHelper = new PrimengTableHelper();
    }

    show(): void {
        this.primengTableHelper.records = [];
        this.modal.show();
        this.getLocationToAdd();
    }

    shown(): void {
    }

    getLocationToAdd(event?: LazyLoadEvent) {
        if (!this.tagId) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.addLocationToTagPaginator.changePage(0);
            return;
        }


        this.primengTableHelper.showLoadingIndicator();
        this._userLocationServiceProxy
            .getLocationNotInTag(
                this.filterTextForUserTable,
                this.tagId,
                this.primengTableHelper.getSorting(this.addLocationToTagDataTable),
                this.primengTableHelper.getMaxResultCount(this.addLocationToTagPaginator, event),
                this.primengTableHelper.getSkipCount(this.addLocationToTagPaginator, event)
            )
            .pipe(finalize(() => (this.primengTableHelper.hideLoadingIndicator())))
            .subscribe(result => {
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.totalRecordsCount = result.totalCount;
            });
    }

    addLocationToTag() {
        let a = new AddLocationToTagDto();
        a.locationTagId = this.tagId;
        a.locationIds = _.map(this.selectedLocations,
            selectedLocations => selectedLocations.id
        );

        this._userLocationServiceProxy
            .addLocationToTag(a)
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullyAdded'));
                this.locationAdded.emit(true);
                this.modal.hide();
            });
    }

    close() {
        this.modal.hide();
    }
}

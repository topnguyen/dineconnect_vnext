import {
    Component,
    Injector,
    OnChanges,
    Input,
    ViewChild
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    LocationTagDto,
    LocationTagsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AddLocationToTagModalComponent } from './add-location-to-tag-modal.component';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';

@Component({
    selector: 'location-in-tag',
    templateUrl: './locations-in-tag.component.html',
    animations: [appModuleAnimation()]
})
export class LocationsInTagComponent extends AppComponentBase implements OnChanges {
    @ViewChild('addLocationModal', { static: true }) addLocationModal: AddLocationToTagModalComponent;
    @Input() tag: LocationTagDto;

    @ViewChild('locationInTagTable', { static: true }) locationInTagTable: Table;
    @ViewChild('locationInTagPaginator', { static: true }) locationInTagPaginator: Paginator;

    filterText = '';

    constructor(
        injector: Injector,
        private _locationTagServiceProxy: LocationTagsServiceProxy
    ) {
        super(injector);
    }

    ngOnChanges() {
        this.getLocationsInTag();
    }

    getLocationsInTag(event?: LazyLoadEvent) {
        const pageSize = 10;

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.locationInTagPaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._locationTagServiceProxy
            .getAllLocationOfTag(
                this.filterText,
                this.tag.id,
                this.primengTableHelper.getSorting(this.locationInTagTable),
                this.primengTableHelper.getMaxResultCount(this.locationInTagPaginator, event) || pageSize,
                this.primengTableHelper.getSkipCount(this.locationInTagPaginator, event)
            )
            .pipe(finalize(() => (this.primengTableHelper.hideLoadingIndicator())))
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    openAddModal() {
        this.addLocationModal.tagId = this.tag.id;
        this.addLocationModal.show();
    }

    removeLocationFromTag(locationTagId: number) {
        this._locationTagServiceProxy
            .deleteLoctionTagLocation(locationTagId)
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullyRemoved'));
                this.getLocationsInTag();
            });
    }
}

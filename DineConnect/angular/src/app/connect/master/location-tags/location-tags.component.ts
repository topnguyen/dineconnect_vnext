import { Component, Injector, ViewChild, Renderer2, ElementRef, ViewEncapsulation } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocationTagDto, LocationTagsServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditLocationTagModalComponent } from './create-or-edit-tag-modal.component';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './location-tags.component.html',
    styleUrls: ['./location-tags.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LocationTagsComponent extends AppComponentBase {
    @ViewChild('createOrEditTagModal', { static: true })
    createOrEditTagModal: CreateOrEditLocationTagModalComponent;

    @ViewChild('tagTable', { static: true }) tagTable: Table;
    @ViewChild('tagPaginator', { static: true }) tagPaginator: Paginator;

    filterText = '';
    selectedTag: LocationTagDto;
    constructor(
        injector: Injector,
        private _locationTagServiceProxy: LocationTagsServiceProxy,
        private render: Renderer2,
        private elRef: ElementRef,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getTags(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.tagPaginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._locationTagServiceProxy
            .getAllTags(
                this.filterText,
                this.primengTableHelper.getSorting(this.tagTable),
                this.primengTableHelper.getMaxResultCount(this.tagPaginator, event),
                this.primengTableHelper.getSkipCount(this.tagPaginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }
    addOrEditTag(tag: LocationTagDto) {
        this.createOrEditTagModal.show(tag);
    }

    deleteTag(tag: LocationTagDto) {
        this._locationTagServiceProxy.deleteTag(tag.id).subscribe((result) => {
            this.selectedTag = null;
            this.notify.success(this.l('SuccessfullyRemoved'));
            this.getTags();
        });
    }

    selectTag(event, tag: LocationTagDto) {
        this.selectedTag = tag;

        let els = this.elRef.nativeElement.querySelectorAll('tr.ng-star-inserted');
        els.forEach((el) => {
            this.render.removeClass(el, 'selected');
        });
        let element = event.currentTarget.parentElement.parentElement;
        this.render.addClass(element, 'selected');
    }

    exportTag() {
        this._locationTagServiceProxy.getLocationTagsToExcel(this.filterText).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
}

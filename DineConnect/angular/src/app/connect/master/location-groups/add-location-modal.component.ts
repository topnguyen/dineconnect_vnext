import {
    Component,
    Injector,
    Output,
    EventEmitter,
    ViewEncapsulation,
    ViewChild,
    OnInit
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import {
    LocationDto,
    AddLocationGroupInput,
    LocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
@Component({
    selector: 'addLocationModal',
    templateUrl: './add-location-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddLocationModalComponent extends AppComponentBase implements OnInit {
    groupId = 0;

    @Output()
    locationAdded: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild('modal', { static: true }) modal: ModalDirective;

    @ViewChild('addLocationToGroupDataTable', { static: true })
    addLocationToGroupDataTable: Table;
    @ViewChild('addLocationToGroupPaginator', { static: true })
    addLocationToGroupPaginator: Paginator;

    selectedLocations: LocationDto[];

    filterTextForUserTable = '';
    locationNotInGroupTableHelper: PrimengTableHelper;
    organizationListDropDown = [];
    organizationSelected: number;
    selectedLocation: LocationDto;
    loadUserInLocation = false;
    constructor(
        injector: Injector,
        private _userLocationServiceProxy: LocationGroupServiceProxy
    ) {
        super(injector);
        this.locationNotInGroupTableHelper = new PrimengTableHelper();
    }

    show(): void {
        this.locationNotInGroupTableHelper.records = [];
        this.modal.show();
    }

    ngOnInit() { }
    shown(): void {
        this.getLocationToAdd(null);
    }
    getLocationToAdd(event?: LazyLoadEvent) {
        if (this.locationNotInGroupTableHelper.shouldResetPaging(event)) {
            this.addLocationToGroupPaginator.changePage(0);
            return;
        }
        this.locationNotInGroupTableHelper.showLoadingIndicator();
        this._userLocationServiceProxy
            .getLocationNotInGroup(
                this.filterTextForUserTable,
                0,
                this.groupId,
                this.locationNotInGroupTableHelper.getSorting(
                    this.addLocationToGroupDataTable
                ),
                this.locationNotInGroupTableHelper.getMaxResultCount(
                    this.addLocationToGroupPaginator,
                    event
                ),
                this.locationNotInGroupTableHelper.getSkipCount(
                    this.addLocationToGroupPaginator,
                    event
                )
            )
            .subscribe(result => {
                this.locationNotInGroupTableHelper.records = result.items;
                this.locationNotInGroupTableHelper.totalRecordsCount =
                    result.totalCount;
                this.locationNotInGroupTableHelper.records = result.items;
                this.locationNotInGroupTableHelper.hideLoadingIndicator();
            });
    }

    addLocationToGroup() {
        let a = new AddLocationGroupInput();
        a.groupId = this.groupId;
        a.locations = _.map(
            this.selectedLocations,
            selectedLocations => selectedLocations.id
        );

        this._userLocationServiceProxy
            .addLocationToGroup(a)
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullyAdded'));
                this.locationAdded.emit(true);
                this.modal.hide();
            });
    }

    close() {
        this.modal.hide();
    }
}

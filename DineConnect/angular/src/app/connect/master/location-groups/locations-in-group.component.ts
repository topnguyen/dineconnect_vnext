import {
    Component,
    Injector,
    OnChanges,
    Input,
    ViewChild
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    GroupDto,
    LocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { CreateOrEditGroupModalComponent } from './create-or-edit-group-modal.component';
import { AddLocationModalComponent } from './add-location-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
@Component({
    selector: 'locationInGroup',
    templateUrl: './locations-in-group.component.html',
    animations: [appModuleAnimation()]
})
export class LocationsInGroupComponent extends AppComponentBase
    implements OnChanges {
    @ViewChild('addLocationModal', { static: true }) addLocationModal: AddLocationModalComponent;
    @ViewChild('createOrEditGroupModal', { static: true })
    createOrEditGroupModal: CreateOrEditGroupModalComponent;
    @Input() group: GroupDto;

    @ViewChild('locationInGroupTable', { static: true }) locationInGroupTable: Table;
    @ViewChild('locationInGroupPaginator', { static: true }) locationInGroupPaginator: Paginator;
    filterText = '';

    constructor(
        injector: Injector,
        private _locationGroupServiceProxy: LocationGroupServiceProxy
    ) {
        super(injector);
    }
    ngOnChanges() {
        this.getLocationsInGroup();
    }
    getLocationsInGroup(event?: LazyLoadEvent) {
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.locationInGroupPaginator,
            event
        );
        if (!maxResultCount) { maxResultCount = 10; }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.locationInGroupPaginator.changePage(0);

            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._locationGroupServiceProxy
            .getAllLocationGroup(
                this.group.id,
                this.primengTableHelper.getSorting(this.locationInGroupTable),
                maxResultCount,
                this.primengTableHelper.getSkipCount(
                    this.locationInGroupPaginator,
                    event
                )
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    openAddModal() {
        this.addLocationModal.groupId = this.group.id;
        this.addLocationModal.show();
    }

    removeLocationFromGroup(locationGroupId: number) {
        this._locationGroupServiceProxy
            .removeLocationFromGroup(locationGroupId)
            .subscribe(result => {
                this.notify.success(this.l('SuccessfullyRemoved'));
                this.getLocationsInGroup();
            });
    }
}

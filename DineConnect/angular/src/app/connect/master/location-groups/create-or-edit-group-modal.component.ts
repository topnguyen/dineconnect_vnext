import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Injector,
    Output,
    ViewChild
} from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    LocationGroupServiceProxy,
    GroupDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

export interface IOrganizationUnitOnEdit {
    id?: number;
    parentId?: number;
    displayName?: string;
}

@Component({
    selector: 'createOrEditGroupModal',
    templateUrl: './create-or-edit-group-modal.component.html'
})

export class CreateOrEditGroupModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output()
    groupAddedOrUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    active = false;
    saving = false;

    organizationUnit: GroupDto;

    constructor(
        injector: Injector,
        private _locationGroupServiceProxy_: LocationGroupServiceProxy,
        private _changeDetector: ChangeDetectorRef
    ) {
        super(injector);
    }

    show(organizationUnit: GroupDto): void {
        if (!organizationUnit || organizationUnit == null) {
            organizationUnit = new GroupDto();
        }
        this.organizationUnit = organizationUnit;
        this.active = true;
        this.modal.show();
        this._changeDetector.detectChanges();

    }

    save(): void {
        this.createUnit();
    }

    createUnit() {
        const createInput = new GroupDto();
        createInput.id = this.organizationUnit.id;
        createInput.name = this.organizationUnit.name;

        this.saving = true;
        this._locationGroupServiceProxy_
            .createOrUpdateGroup(this.organizationUnit)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.groupAddedOrUpdated.emit(true);
            });
    }

    updateUnit() {
        // const updateInput = new UpdateOrganizationUnitInput();
        // updateInput.id = this.organizationUnit.id;
        // updateInput.displayName = this.organizationUnit.displayName;
        // this.saving = true;
        // this._locationGroupServiceProxy_
        //     .updateOrganizationUnit(updateInput)
        //     .pipe(finalize(() => (this.saving = false)))
        //     .subscribe((result: OrganizationUnitDto) => {
        //         this.notify.success(this.l("SavedSuccessfully"));
        //         this.close();
        //         this.groupAddedOrUpdated.emit(result);
        //     });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    onShown(): void {
        document.getElementById('OrganizationUnitCode').focus();
    }

}

import { Component, Injector, ViewChild, Renderer2, ElementRef, ViewEncapsulation } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    GroupDto,
    LocationGroupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { CreateOrEditGroupModalComponent } from './create-or-edit-group-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './location-group.component.html',
    styleUrls: ['./location-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class LocationGroupComponent extends AppComponentBase {
    @ViewChild('createOrEditGroupModal', { static: true })
    createOrEditGroupModal: CreateOrEditGroupModalComponent;

    @ViewChild('groupTable', { static: true }) groupTable: Table;
    @ViewChild('groupPaginator', { static: true }) groupPaginator: Paginator;

    filterText = '';
    selectedGroup: GroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _locationGroupServiceProxy: LocationGroupServiceProxy,
        private render: Renderer2,
        private elRef: ElementRef,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getGroups(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.groupPaginator.changePage(0);

            return;
        }
        this.lazyLoadEvent = event;
        this.primengTableHelper.showLoadingIndicator();
        this._locationGroupServiceProxy
            .getAllGroup(
                this.filterText,
                this.primengTableHelper.getSorting(this.groupTable),
                this.primengTableHelper.getMaxResultCount(
                    this.groupPaginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.groupPaginator, event)
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
    addOrEditGroup(group: GroupDto) {
        this.createOrEditGroupModal.show(group);
    }

    deleteGroup(group: GroupDto) {
        this._locationGroupServiceProxy
            .deleteGroup(group.id)
            .subscribe(result => {
                this.selectedGroup = null;
                this.notify.success(this.l('SuccessfullyRemoved'));
                this.getGroups();
            });
    }

    selectGroup(event, group: GroupDto) {
        this.selectedGroup = group;

        let els = this.elRef.nativeElement.querySelectorAll('tr.ng-star-inserted');
        els.forEach(el => {
            this.render.removeClass(el, 'selected');
        });
        let element = event.currentTarget.parentElement.parentElement;
        this.render.addClass(element, 'selected');
    }

    exportGroup() {
       this._locationGroupServiceProxy.getLocationGroupsToExcel(
        this.filterText,
        this.primengTableHelper.getSorting(this.groupTable),
        this.primengTableHelper.getMaxResultCount(
            this.groupPaginator,
            this.lazyLoadEvent
        ),
        this.primengTableHelper.getSkipCount(this.groupPaginator, this.lazyLoadEvent)
       ).subscribe(result => {
        this._fileDownloadService.downloadTempFile(result);
       })
    }
}

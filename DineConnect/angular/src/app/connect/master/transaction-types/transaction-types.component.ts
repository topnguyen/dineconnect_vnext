import { Component, Injector, ViewChild, OnInit } from "@angular/core";
import {
    TransactionTypesServiceProxy,
    TransactionTypeDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { CreateOrEditTransactionTypeModalComponent } from "./create-or-edit-transaction-type-modal.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { EntityTypeHistoryModalComponent } from "@app/shared/common/entityHistory/entity-type-history-modal.component";
import * as _ from "lodash";
import { finalize } from "rxjs/operators";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    selector: "app-transaction-types",
    templateUrl: "./transaction-types.component.html",
    styleUrls: ["./transaction-types.component.scss"],
    animations: [appModuleAnimation()],
})
export class TransactionTypesComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditTransactionTypeModal", { static: true })
    createOrEditTransactionTypeModal: CreateOrEditTransactionTypeModalComponent;
    @ViewChild("entityTypeHistoryModal", { static: true })
    entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = "";
    isDeleted;

    _entityTypeFullName =
        "DinePlan.DineConnect.Connect.Master.TransactionTypes.TransactionType";
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _transactionTypesServiceProxy: TransactionTypesServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return (
            customSettings.EntityHistory &&
            customSettings.EntityHistory.isEnabled &&
            _.filter(
                customSettings.EntityHistory.enabledEntities,
                (entityType) => entityType === this._entityTypeFullName
            ).length === 1
        );
    }

    getTransactionTypes(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._transactionTypesServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTransactionType(): void {
        this.createOrEditTransactionTypeModal.show();
    }

    showHistory(transactionType: TransactionTypeDto): void {
        this.entityTypeHistoryModal.show({
            entityId: transactionType.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: "",
        });
    }

    deleteTransactionType(transactionType: TransactionTypeDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._transactionTypesServiceProxy
                    .delete(transactionType.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(transactionType: TransactionTypeDto): void {
        const body = EntityDto.fromJS({ id: transactionType.id });
        this._transactionTypesServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    exportToExcel(): void {
        this._transactionTypesServiceProxy
            .getTransactionTypesToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}

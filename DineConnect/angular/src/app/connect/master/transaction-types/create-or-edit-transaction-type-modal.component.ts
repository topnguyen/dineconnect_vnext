import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TransactionTypesServiceProxy, CreateOrEditTransactionTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
  selector: 'createOrEditTransactionTypeModal',
  templateUrl: './create-or-edit-transaction-type-modal.component.html'
})
export class CreateOrEditTransactionTypeModalComponent extends AppComponentBase {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  transactionType: CreateOrEditTransactionTypeDto = new CreateOrEditTransactionTypeDto();

  constructor(
    injector: Injector,
    private _transactionTypesServiceProxy: TransactionTypesServiceProxy
  ) {
    super(injector);
  }

  show(transactionTypeId?: number): void {

    if (!transactionTypeId) {
      this.transactionType = new CreateOrEditTransactionTypeDto();
      this.transactionType.id = transactionTypeId;

      this.active = true;
      this.modal.show();
    } else {
      this._transactionTypesServiceProxy.getTransactionTypeForEdit(transactionTypeId).subscribe(result => {
        this.transactionType = result;

        this.active = true;
        this.modal.show();
      });
    }
  }

  save(): void {
    this.saving = true;

    this._transactionTypesServiceProxy.createOrEdit(this.transactionType)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}

import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    CalculationsServiceProxy,
    CreateOrEditCalculationDto,
    CommonLocationGroupDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router, ActivatedRoute } from "@angular/router";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-calculation",
    templateUrl: "./create-or-edit-calculation.component.html",
    styleUrls: ["./create-or-edit-calculation.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditCalculationComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;

    saving = false;

    calculation: CreateOrEditCalculationDto = new CreateOrEditCalculationDto();

    transactionTypes = [];
    calculationTypes = [];
    confirmationTypes = [];
    departments = [];

    selectedDepartments = [];
    calculationId: number;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _calculationsServiceProxy: CalculationsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.calculationId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.calculationId)) {
                this.calculation = new CreateOrEditCalculationDto();
                this.calculation.locationGroup = new CommonLocationGroupDto();
            } else {
                this._calculationsServiceProxy
                    .getCalculationForEdit(this.calculationId)
                    .subscribe((result) => {
                        this.calculation = result;
                        this.selectedDepartments = this.calculation.departments
                            ? this.calculation.departments.split(",")
                            : [];
                    });
            }
        });

        this.getTransactionTypes();
        this.getCalculationTypes();
        this.getConfirmationTypes();
    }

    save(): void {
        this.saving = true;
        this.calculation.departments = this.selectedDepartments.join(",");

        this._calculationsServiceProxy
            .createOrEdit(this.calculation)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/calculations"]);
    }

    private getTransactionTypes() {
        this._calculationsServiceProxy
            .getAllTransactionTypeForLookupTable(undefined)
            .subscribe((result) => {
                this.transactionTypes = result.items;
            });
    }

    private getCalculationTypes() {
        this._calculationsServiceProxy
            .getCalculationTypeForLookupTable(undefined)
            .subscribe((result) => {
                this.calculationTypes = result.items;
            });
    }

    private getConfirmationTypes() {
        this._calculationsServiceProxy
            .getConfirmationTypeForLookupTable(undefined)
            .subscribe((result) => {
                this.confirmationTypes = result.items;
            });

        this._calculationsServiceProxy
            .getDepartmentForCombobox()
            .subscribe((result) => {
                this.departments = result.items.map((item) => {
                    return { label: item.displayText, value: item.value };
                });
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.calculation.locationGroup);
    }

    getLocations(event) {
        this.calculation.locationGroup = event;
    }
}

import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IGenericFilterModel } from '@app/shared/models/GenericFilterModel';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BrandDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-organization-filter',
    templateUrl: './organization-filter.component.html',
    styleUrls: ['./organization-filter.component.scss']
})
export class OrganizationFilterComponent extends AppComponentBase {
    @Input() childArrayListForFilter: Array<IGenericFilterModel>;
    @Output() filterationEmitter: EventEmitter<string> = new EventEmitter<string>();
    @Output() close = new EventEmitter<any>();

    customFilter: IGenericFilterModel;
    organizationUnit: BrandDto;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {}

    applyFilter() {
        let arrayGenericObject: Array<IGenericFilterModel> = [];
        document.querySelectorAll('.filterationMainClass .fliterationFieldClass').forEach( (item, index) => {
            let obj: IGenericFilterModel = {
                name: item.getAttribute('data-filtername'),
                type: item.getAttribute('data-type'),
                value: item['value'],
                label: ''
            };
            arrayGenericObject.push(obj);
        });
        this.filterationEmitter.emit(JSON.stringify(arrayGenericObject));
        this.close.emit();
    }

    clear() {
        document.querySelectorAll('.filterationMainClass .fliterationFieldClass').forEach( (item, index) => {
            item['value'] = '';
        });
    }

    refresh() {
        this.clear();
        this.filterationEmitter.emit('');
        this.close.emit();
    }
}

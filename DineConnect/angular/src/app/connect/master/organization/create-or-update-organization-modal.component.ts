import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    Output,
    EventEmitter,
    AfterViewInit
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CompanyServiceProxy,
    AddressesServiceProxy,
    BrandDto,
    AddressDto,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { ModalDirective } from "ngx-bootstrap/modal";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { FileItem, FileUploader, FileUploaderOptions } from "ng2-file-upload";
import { base64ToFile, ImageCroppedEvent } from "ngx-image-cropper";
import { AppConsts } from "@shared/AppConsts";
import { IAjaxResponse, TokenService } from "abp-ng2-module";

@Component({
    selector: "createOrUpdateOrganization",
    templateUrl: "./create-or-update-organization-modal.component.html",
    styleUrls: ["./create-or-update-organization-modal.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class CreateOrUpdateOrganizationModalComponent extends AppComponentBase implements AfterViewInit {
    @Output()
    organizationUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild("modal", { static: true }) modal: ModalDirective;
    @ViewChild('tab') tab: TabsetComponent;

    active = false;
    saving = false;

    companyEditDto: BrandDto = new BrandDto();
    companyListDto: BrandDto[] = [];
    companyListDropDown = [];
    filterText = "";
    transactionDate = new Date();
    startDate = new Date();
    endDate = new Date();
    locationListForDropDown = [];

    stateLoaded = false;
    cityLoaded = false;
    countryListDropDown = [];
    cityListDropDown = [];
    stateListDropDown = [];
    selectedCountry: number | string;
    selectedState: number | string;
    uploader: FileUploader;
    imageChangedEvent: any = '';

    id: number;
    private sub: any;
    private _uploaderOptions: FileUploaderOptions = {};

    constructor(
        injector: Injector,
        private _companyServiceProxy: CompanyServiceProxy,
        private _addressesServiceProxy: AddressesServiceProxy,
        private _tokenService: TokenService
    ) {
        super(injector);
    }

    ngAfterViewInit() {
        this.modal.onShow.subscribe(() => {
            setTimeout(() => {
                this.tab.tabs[0].active = true;
            }, 0);
        });
    }

    save() {
        this.companyEditDto.address.countryId = this.companyEditDto.address.countryId || null;
        this.companyEditDto.address.stateId = this.companyEditDto.address.stateId || null;
        this.companyEditDto.address.cityId = this.companyEditDto.address.cityId || null;

        if ((this.companyEditDto.address.address1 || this.companyEditDto.address.address2 || this.companyEditDto.address.address3) && !this.companyEditDto.address.cityId) {
            this.notify.error('City is required. Please add city or remove address line');
            return;
        }

        this.saving = true;
        this._companyServiceProxy
            .createOrUpdate(this.companyEditDto)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.saving = false;
                this.notify.success(this.l("SuccessfullySaved"));
                this.organizationUpdated.emit(true);
                this.modal.hide();
            });
    }

    close() {
        this.countryListDropDown = [];
        this.stateListDropDown = [];
        this.cityListDropDown = [];

        this.modal.hide();
    }

    show(organizationId?: number): void {
        this.active = true;
        this.modal.show();
        this.companyEditDto = new BrandDto();
        this.companyEditDto.address = new AddressDto();

        if (!isNaN(organizationId)) {
            this._companyServiceProxy
                .getCompanyForEdit(organizationId)
                .subscribe((result) => {
                    this.companyEditDto = result;
                    this.companyEditDto.address = this.companyEditDto.address || new AddressDto();
                    this.companyEditDto.address.countryId = this.companyEditDto.address.countryId || undefined;
                    this.companyEditDto.address.stateId = this.companyEditDto.address.stateId || undefined;
                    this.companyEditDto.address.cityId = this.companyEditDto.address.cityId || undefined;
                    this.getCountryList();
                    this.getStateList();
                    this.getCityList();

                    this.initFileUploader();
                });
        } else {
            this.getCountryList();
        }
    }

    getCompanyList() {
        this._companyServiceProxy
            .getAll(undefined, false, undefined, 1000, 0)
            .subscribe((result) => {
                this.companyListDropDown = [];
                result.items.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.companyListDropDown.push(a);
                });
            });
    }

    getCountryList() {
        this._addressesServiceProxy
            .getCountryList(undefined, false, undefined, 1000, 0)
            .subscribe((result) => {
                this.countryListDropDown = result.items.map((element) => {
                    let a = { label: element.name, value: element.id };
                    return a;
                });
            });
    }

    getStateList() {
        this.stateListDropDown = [];
        this.cityListDropDown = [];
        if (
            !this.companyEditDto.address.countryId ||
            this.companyEditDto.address.countryId.toString() === "undefined"
        ) {
            return;
        }

        this._addressesServiceProxy
            .getStateListByCountry(this.companyEditDto.address.countryId)
            .subscribe((result) => {
                result.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.stateListDropDown.push(a);
                });
            });
    }

    getCityList() {
        this.cityListDropDown = [];

        if (
            !this.companyEditDto.address.stateId ||
            this.companyEditDto.address.stateId.toString() === "undefined" ||
            !this.companyEditDto.address.countryId ||
            this.companyEditDto.address.countryId.toString() === "undefined"
        ) {
            return;
        }

        this._addressesServiceProxy
            .getCityList(
                undefined,
                this.companyEditDto.address.countryId,
                this.companyEditDto.address.stateId,
                false,
                undefined,
                1000,
                0
            )
            .subscribe((result) => {
                result.items.forEach((element) => {
                    let a = { label: element.name, value: element.id };
                    this.cityListDropDown.push(a);
                });
            });
    }
    setCountry() {
        this.companyEditDto.address.stateId = undefined;
        this.companyEditDto.address.cityId = undefined;
        this.getStateList();
    }
    setState() {
        this.companyEditDto.address.cityId = undefined;
        this.getCityList();
    }

    changePicture(companyId: number) {

    }

    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', 5));
            return;
        }

        this.imageChangedEvent = event;
    }

    imageCroppedFile(event: ImageCroppedEvent) {
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>base64ToFile(event.base64)]);
    }

    initFileUploader(): void {
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Image/UploadProfilePicture' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', 'ProfilePicture');
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                /* TODO: update logo */
                // this.updateProfilePicture(resp.result.fileToken);
            } else {
                this.message.error(resp.error.message);
            }
        };

        this.uploader.setOptions(this._uploaderOptions);
    }

    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
}

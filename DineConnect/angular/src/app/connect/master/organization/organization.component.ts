import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { CompanyServiceProxy, BrandDto, EntityDto } from '@shared/service-proxies/service-proxies';

import { CreateOrUpdateOrganizationModalComponent } from './create-or-update-organization-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { IGenericFilterModel } from '@app/shared/models/GenericFilterModel';
@Component({
    templateUrl: './organization.component.html',
    styleUrls: ['./organization.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class OrganizationComponent extends AppComponentBase {
    @ViewChild('createOrUpdateOrganizationModal', { static: true }) createOrUpdateOrganizationModal: CreateOrUpdateOrganizationModalComponent;
    @ViewChild('organizationTable', { static: true }) organizationTable: Table;
    @ViewChild('organizationPaginator', { static: true }) organizationPaginator: Paginator;

    companyEditDto: BrandDto;
    selectedGroup: BrandDto;
    filterText = '';
    genericFilterValue = '';
    isDeleted = false;
    parentArrayListForFilter: Array<IGenericFilterModel> = [
        { name: 'Id', value: '', type: 'number', label: 'Id' },
        { name: 'Code', value: '', type: 'string', label: 'Code' },
        { name: 'Name', value: '', type: 'string', label: 'Name' },
        { name: 'Address.Address1', value: '', type: 'string', label: 'Address' },
        { name: 'Address.City.State.Name', value: '', type: 'string', label: 'State' },
        { name: 'Address.City.Name', value: '', type: 'string', label: 'City' },
        { name: 'PhoneNumber', value: '', type: 'string', label: 'Phone Number' }
    ];
    isShowFilterOptions = false;
    constructor(
        injector: Injector,
        private _companyServiceProxy: CompanyServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
        this.companyEditDto = new BrandDto();
    }

    getCompanyList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.organizationPaginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._companyServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.organizationTable),
                this.primengTableHelper.getMaxResultCount(this.organizationPaginator, event),
                this.primengTableHelper.getSkipCount(this.organizationPaginator, event),
                this.genericFilterValue
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
    showFilterOptions() {
        this.isShowFilterOptions = true;
    }
    deleteOrganization(organization: BrandDto) {
        this.message.confirm('You want to delete Brand ' + organization.name, this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._companyServiceProxy.delete(organization.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));
                    this.reloadPage();
                });
            }
        });
    }

    activateItem(organization: BrandDto) {
        const body = EntityDto.fromJS({ id: organization.id });
        this._companyServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    craeteOrUpdateOrganization(organizationId?: number) {
        this.createOrUpdateOrganizationModal.show(organizationId);
    }

    exportToExcel() {
        this._companyServiceProxy.getBrandsToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    reloadPage(): void {
        this.organizationPaginator.changePage(this.organizationPaginator.getPage());
    }

    parentFilterationEmitterReceiver(e) {
        this.genericFilterValue = e;
        this.getCompanyList();
    }
}

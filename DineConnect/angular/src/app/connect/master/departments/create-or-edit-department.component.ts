import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    DepartmentsServiceProxy,
    CreateOrEditDepartmentDto,
    CommonLocationGroupDto,
    TicketTypeServiceProxy,
    ComboboxItemDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router, ActivatedRoute } from "@angular/router";
import { CreateOrEditLanguageDescriptionModalComponent } from "@app/connect/menu/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-department",
    templateUrl: "./create-or-edit-department.component.html",
    styleUrls: ["./create-or-edit-department.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditDepartmentComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;

    saving = false;

    department: CreateOrEditDepartmentDto = new CreateOrEditDepartmentDto();

    priceTags: ComboboxItemDto[] = [];
    ticketTypes: ComboboxItemDto[] = [];
    departmentGroups: any[] = [];

    departmentId: number;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _departmentsServiceProxy: DepartmentsServiceProxy,
        private _ticketTypeServiceProxy: TicketTypeServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.departmentId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.departmentId)) {
                this.department = new CreateOrEditDepartmentDto();
                this.department.locationGroup = new CommonLocationGroupDto();
            } else {
                this._departmentsServiceProxy
                    .getDepartmentForEdit(this.departmentId)
                    .subscribe((result) => {
                        this.department = result;
                    });
            }
        });

        this._init();
    }

    save(): void {
        this.saving = true;

        this._departmentsServiceProxy
            .createOrEdit(this.department)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/master/departments"]);
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.department.locationGroup);
    }

    getLocations(event) {
        this.department.locationGroup = event;
    }

    private _init() {
        this._departmentsServiceProxy
            .getAllPriceTagForLookupTable(undefined)
            .subscribe((result) => {
                this.priceTags = result.items;
            });

        this._ticketTypeServiceProxy
            .getAllTicketTypeForLookupTable()
            .subscribe((result) => {
                this.ticketTypes = result.items;
            });

        this._departmentsServiceProxy
            .getAllDepartmentGroup(
                undefined,
                undefined,
                undefined,
                undefined,
                1000,
                0
            )
            .subscribe((result) => {
                this.departmentGroups = result.items;
                // this.departmentGroups.forEach((item) => {
                //     item.name == `${item.name} - ${item.code}`;
                // });
            });
    }

    showLanguages() {
        this.createOrEditLanguageDescriptionModal.show(
            "DepartmentName",
            this.departmentId,
            this.departmentId + " - " + this.department.name
        );
    }
}

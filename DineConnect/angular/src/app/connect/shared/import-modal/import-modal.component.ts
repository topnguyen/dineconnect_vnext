import {
    Component,
    EventEmitter,
    Injector,
    Output,
    ViewChild,
    OnInit,
} from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { AppComponentBase } from "@shared/common/app-component-base";
import { FileDto } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import * as _ from "lodash";
import { finalize } from "rxjs/operators";
import { FileUploader } from "ng2-file-upload";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { TokenService, IAjaxResponse } from "abp-ng2-module";

export interface IImportModalOptions {
    title?: string;
    downloadTemplate: (id?: number, tenantId?: number) => Observable<FileDto>;
    import: (fileToken: string) => Observable<any>;
    remoteServiceBaseUrl?: string;
    formData?: any;
    filters?: any;
    queueLimit?: any;
    templateUrl?: string;
}

@Component({
    selector: "import-modal",
    templateUrl: "./import-modal.component.html",
    styleUrls: ["./import-modal.component.scss"],
})
export class ImportModalComponent extends AppComponentBase implements OnInit {
    @ViewChild("modal", { static: true }) modal: ModalDirective;
    @ViewChild("fileInput", { static: true }) fileInput: any;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    static defaultOptions: IImportModalOptions = {
        downloadTemplate: undefined,
        import: undefined,
        remoteServiceBaseUrl: undefined,
        formData: undefined,
        filters: [],
    };

    options: IImportModalOptions = _.merge({});
    uploader: FileUploader;

    isShown = false;
    isInitialized = false;
    loading = false;
    saving = false;
    tenantId?: number;
    fileToken: any;
    remoteServiceBaseUrl =
        AppConsts.remoteServiceBaseUrl + "/ImageUploader/UploadFile";

    constructor(
        injector: Injector,
        private _tokenService: TokenService,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.initUploaders();
    }

    configure(options: IImportModalOptions): void {
        this.options = _.merge(
            {},
            ImportModalComponent.defaultOptions,
            { title: this.l("SelectAnItem") },
            options
        );
    }

    show(): void {
        if (!this.options) {
            throw Error(
                "Should call ImportModalComponent.configure once before ImportModalComponent.show!"
            );
        }

        this.modal.show();
    }

    close(): void {
        this.saving = false;
        this.loading = false;
        this.fileToken = undefined;
        this.modal.hide();
    }

    shown(): void {
        this.isShown = true;
        this.fileInput.nativeElement.value = "";
    }

    initUploaders(): void {
        this.uploader = new FileUploader({
            url: this.options.remoteServiceBaseUrl
                ? this.options.remoteServiceBaseUrl
                : this.remoteServiceBaseUrl,
            authToken: "Bearer " + this._tokenService.getToken(),
            filters: this.options.filters || [],
            additionalParameter: this.options.formData
                ? this.options.formData
                : undefined,
        });

        this.uploader.onAfterAddingFile = (f) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
                this.fileToken = undefined;
            }
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.fileToken = ajaxResponse.result.fileToken;

                this.import();
            } else {
                this.saving = false;
                this.message.error(ajaxResponse.error.message);
            }
        };
        this.uploader.onCompleteAll = () => {
            this.fileToken = undefined;
            this.fileInput.nativeElement.value = "";
        };
    }

    private import() {
        this.options
            .import(this.fileToken)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.info(this.l("Importing"));
                setTimeout(() => {
                    this.close();
                    this.modalSave.emit(true);
                }, 1000);
            });
    }

    downloadTemplate() {
        this.loading = true;
        this.options
            .downloadTemplate()
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    save() {
        this.saving = true;
        if (this.uploader.queue.length > 0) {
            this.uploader.uploadAll();
        }
    }
}

import { Component, ElementRef, Injector, OnInit, ViewChild ,Renderer2} from '@angular/core';
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, CommonLocationGroupDto, DepartmentsServiceProxy, ExportType, PaymentTypesServiceProxy, PromotionListDto, SimpleLocationDto, SimpleLocationGroupDto, SimpleLocationTagDto, TicketHourlySalesReportServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import { HourlySalesExportModalComponent } from '../hourly-sales-export-modal/hourly-sales-export-modal.component';
import _ from "lodash";
import { map, sumBy, unionBy } from "lodash";
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
  selector: 'app-hourly-sales',
  templateUrl: './hourly-sales.component.html',
  styleUrls: ['./hourly-sales.component.scss'],
  animations: [appModuleAnimation()],
})
export class HourlySalesComponent extends AppComponentBase implements OnInit {
  @ViewChild("commonMultipleLookupModal", { static: true }) commonMultipleLookupModal: CommonMultipleLookupModalComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  // @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("exportModal", { static: false }) exportModal: HourlySalesExportModalComponent;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("selectLocationOrGroup", { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;


  lookupName: string;
  paymentTypes = [];
  departments = [];
  requestParams: IRequestParams = {
    exportOutputType: ExportType.Excel,
  } as IRequestParams;
  dataChart: any;

  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  public dateRange: Date[] = [moment().toDate(), moment().toDate()];
  constructor(injector: Injector
    , private _departmentServiceProxy: DepartmentsServiceProxy
    , private _paymentTypeServiceProxy: PaymentTypesServiceProxy
    , private _ticketHourlySalesServiceProxy: TicketHourlySalesReportServiceProxy
    , private _fileDownloadService: FileDownloadService
    , private render: Renderer2
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }
  showedDateRangePicker(): void {
    let button = document.querySelector(
        "bs-daterangepicker-container .bs-datepicker-predefined-btns"
    ).lastElementChild;
    let self = this;
    let el = document.querySelector(
        "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
    );
    self.render.removeClass(el, "show");

    button.addEventListener("click", (event) => {
        event.preventDefault();

        self.render.addClass(el, "show");
    });
}

  showLookupModal(name) {
    this.lookupName = name;
    switch (name) {
      case "Department":
        this.commonMultipleLookupModal.configure({
          title: this.l("SelectDepartment"),
          dataSource: (
            skipCount: number,
            maxResultCount: number,
            filter: string
          ) => {
            return this._departmentServiceProxy.getDepartmentsForLookup(
              filter,
              "name",
              skipCount,
              maxResultCount
            );
          },
        });

        this.commonMultipleLookupModal.show(this.departments);
        break;
      case "Payment":
        this.commonMultipleLookupModal.configure({
          title: this.l("Select") + " " + this.l("Payments"),
          dataSource: (
            skipCount: number,
            maxResultCount: number,
            filter: string
          ) => {
            return this._paymentTypeServiceProxy.getPaymentTypeForLookupTable(
              filter,
              "name",
              skipCount,
              maxResultCount
            );
          },
        });

        this.commonMultipleLookupModal.show(this.paymentTypes);
        break;
      default:
        break;
    }
  }

  itemsSelected(event) {
    switch (this.lookupName) {
      case "Department":
        this.departments = event;
        break;
      case "Payment":
        this.paymentTypes = event;

        break;
      case "Transaction":
        break;

      default:
        break;
    }
  }
  get getSumTotal() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.total;
    });
  }
  get getSumTicket() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.tickets;
    });
  }
  clearLookup(name) {
    switch (name) {
      case "Department":
        this.departments = [];
        break;
      case "Payment":
        this.paymentTypes = [];
        break;
      case "Transaction":
        break;
      default:
        break;
    }
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }

  private setRequestParams() {
    this.requestParams.departments = [];
    this.requestParams.payments = [];

    this.departments.forEach((item) => {
      let newItem = new ComboboxItemDto();
      newItem.value = item.value;
      newItem.displayText = item.name;
      this.requestParams.departments.push(newItem);
    });
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });

    this.requestParams.locationGroup_Locations =
      this.locationOrGroup.locations;
    this.requestParams.locationGroup_Groups = this.locationOrGroup.groups;
    this.requestParams.locationGroup_LocationTags =
      this.locationOrGroup.locationTags;
    this.requestParams.locationGroup_NonLocations =
      this.locationOrGroup.nonLocations;
    this.requestParams.locationGroup_Group = this.locationOrGroup.group;
    this.requestParams.locationGroup_LocationTag =
      this.locationOrGroup.locationTag;
    this.requestParams.locationGroup_UserId = this.locationOrGroup.userId;

    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;

    this.requestParams.sorting = this.primengTableHelper.getSorting(
      this.dataTable
    );
    this.requestParams.maxResultCount = 24;
    this.requestParams.skipCount = 0;
  }

  showExportModal() {
    this.exportModal.show();
  }

  clear() {
    this.departments = [];
    this.paymentTypes = [];
  }

  export($event) {
    this.requestParams.byLocation = $event.byLocation;
    this.requestParams.exportOutput = $event.exportOutput;
    this.requestParams.duration = $event.duration;
    this.setRequestParams();
    console.log(this.requestParams);
    this.primengTableHelper.showLoadingIndicator();
    this._ticketHourlySalesServiceProxy.getAllToExcel(
      this.requestParams.exportOutput,
      this.requestParams.duration,
      this.requestParams.byLocation,
      this.requestParams.promotions,
      this.requestParams.paymentTags,
      this.requestParams.payments,
      this.requestParams.transactions,
      this.requestParams.departments,
      this.requestParams.ticketTags,
      this.requestParams.department,
      this.requestParams.terminalName,
      this.requestParams.lastModifiedUserName,
      this.requestParams.outputType,
      this.requestParams.startDate,
      this.requestParams.endDate,
      this.requestParams.location,
      this.requestParams.userId,
      this.requestParams.locations,
      this.requestParams.locationGroup_Locations,
      this.requestParams.locationGroup_Groups,
      this.requestParams.locationGroup_LocationTags,
      this.requestParams.locationGroup_NonLocations,
      this.requestParams.locationGroup_Group,
      this.requestParams.locationGroup_LocationTag,
      this.requestParams.locationGroup_UserId,
      this.requestParams.notCorrectDate,
      this.requestParams.credit,
      this.requestParams.refund,
      this.requestParams.ticketNo,
      this.requestParams.exportOutputType,
      undefined,
      this.requestParams.dynamicFilter,
      undefined,
      undefined,
      this.requestParams.sorting,
      this.requestParams.maxResultCount,
      this.requestParams.skipCount
    ).pipe(
      finalize(() => this.primengTableHelper.hideLoadingIndicator())
    ).subscribe((result) => {
      this._fileDownloadService.downloadTempFile(result);
    });
  }

  getTickets(event?: LazyLoadEvent) {

    this.primengTableHelper.showLoadingIndicator();
    this.setRequestParams();
    this._ticketHourlySalesServiceProxy
      .getHourlySalesSummary(
        this.requestParams.promotions,
        this.requestParams.paymentTags,
        this.requestParams.payments,
        this.requestParams.transactions,
        this.requestParams.departments,
        this.requestParams.ticketTags,
        this.requestParams.department,
        this.requestParams.terminalName,
        this.requestParams.lastModifiedUserName,
        this.requestParams.outputType,
        this.requestParams.startDate,
        this.requestParams.endDate,
        this.requestParams.location,
        this.requestParams.userId,
        this.requestParams.locations,
        this.requestParams.locationGroup_Locations,
        this.requestParams.locationGroup_Groups,
        this.requestParams.locationGroup_LocationTags,
        this.requestParams.locationGroup_NonLocations,
        this.requestParams.locationGroup_Group,
        this.requestParams.locationGroup_LocationTag,
        this.requestParams.locationGroup_UserId,
        this.requestParams.notCorrectDate,
        this.requestParams.credit,
        this.requestParams.refund,
        this.requestParams.ticketNo,
        this.requestParams.exportOutputType,
        undefined,
        this.requestParams.dynamicFilter,
        undefined,
        undefined,
        this.requestParams.sorting,
        this.requestParams.maxResultCount,
        this.requestParams.skipCount
      )
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = 24
        result.hourList[0].total;
        this.primengTableHelper.records = result.hourList[0].hours;
        let labels = _.range(0, 24, 1);
        this.dataChart = {
          labels: labels,
          datasets: [
            {
              data: result.chartTicket,
              label: this.l("Ticket"),
              backgroundColor: "#42A5F5",
              borderColor: "#1E88E5",

            },
            {
              data: result.chartTotal,
              label: this.l("Total"),
              backgroundColor: "#2e3033",
              borderColor: "#1E88E5",
            },

          ]
        };

      });
  }

  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

}

interface IRequestParams {
  promotions: PromotionListDto[] | null | undefined;
  paymentTags: string[] | null | undefined;
  payments: number[] | null | undefined;
  transactions: ComboboxItemDto[] | null | undefined;
  departments: ComboboxItemDto[] | null | undefined;
  ticketTags: ComboboxItemDto[] | null | undefined;
  department: string | null | undefined;
  terminalName: string | null | undefined;
  lastModifiedUserName: string | null | undefined;
  outputType: string | null | undefined;
  startDate: moment.Moment | undefined;
  endDate: moment.Moment | undefined;
  location: number | undefined;
  userId: number | undefined;
  locations: SimpleLocationDto[] | null | undefined;
  locationGroup_Locations: SimpleLocationDto[] | null | undefined;
  locationGroup_Groups: SimpleLocationGroupDto[] | null | undefined;
  locationGroup_LocationTags: SimpleLocationTagDto[] | null | undefined;
  locationGroup_NonLocations: SimpleLocationDto[] | null | undefined;
  locationGroup_Group: boolean | undefined;
  locationGroup_LocationTag: boolean | undefined;
  locationGroup_UserId: number | undefined;
  notCorrectDate: boolean | undefined;
  credit: boolean | undefined;
  refund: boolean | undefined;
  ticketNo: string | null | undefined;
  exportOutputType: ExportType;
  dynamicFilter: string | null | undefined;
  sorting: string | null | undefined;
  maxResultCount: number | undefined;
  skipCount: number | undefined;
  onlystatus: boolean | undefined;
  byLocation: boolean | false;
  duration: string | null | undefined;
  exportOutput: string | null | undefined;
}



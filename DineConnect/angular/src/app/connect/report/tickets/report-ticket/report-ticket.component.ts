import { Component, Injector, ViewEncapsulation, ViewChild, Renderer2, ElementRef, OnInit } from '@angular/core';
import {
    CommonLocationGroupDto,
    PromotionListDto,
    ComboboxItemDto,
    SimpleLocationDto,
    ExportType,
    TicketReportServiceProxy,
    SimpleLocationGroupDto,
    SimpleLocationTagDto
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { sumBy } from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { QueryBuilderComponent } from 'angular2-query-builder';
import { finalize } from 'rxjs/operators';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { OrderReportDetailModalComponent } from '../../order/order-report-detail-modal.component';
import { FilterModalComponent } from './filter-modal/filter-modal.component';

@Component({
    templateUrl: './report-ticket.component.html',
    styleUrls: ['./report-ticket.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ReportTicketComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    @ViewChild('queryBuilder', { static: false })
    queryBuilder: QueryBuilderComponent;
    @ViewChild('detailmodal', { static: false })
    detailmodal: OrderReportDetailModalComponent;
    @ViewChild('filterModal', { static: false })
    filterModal: FilterModalComponent;

    locationOrGroup = new CommonLocationGroupDto();
    // dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    totalTicketAmount;
    totalTickets;
    totalOrders;
    totalItems;
    totalAverage;
    requestParams: IRequestParams = {
        exportOutputType: ExportType.Excel
    } as IRequestParams;

    decimals = 2;
    loading = false;
    isShowFilterOptions = false;

    disableExport = true;
    ExportType = ExportType;

    constructor(
        injector: Injector,
        private _ticketReportServiceProxy: TicketReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2
    ) {
        super(injector);
    }

    ngOnInit() {}

    getTickets(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup_Locations = this.locationOrGroup.locations;
        this.requestParams.locationGroup_Groups = this.locationOrGroup.groups;
        this.requestParams.locationGroup_LocationTags = this.locationOrGroup.locationTags;
        this.requestParams.locationGroup_NonLocations = this.locationOrGroup.nonLocations;
        this.requestParams.locationGroup_Group = this.locationOrGroup.group;
        this.requestParams.locationGroup_LocationTag = this.locationOrGroup.locationTag;
        this.requestParams.locationGroup_UserId = this.locationOrGroup.userId;

        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

        this._ticketReportServiceProxy
            .getTickets(
                this.requestParams.promotions,
                this.requestParams.paymentTags,
                this.requestParams.payments,
                this.requestParams.transactions,
                this.requestParams.departments,
                this.requestParams.ticketTags,
                this.requestParams.department,
                this.requestParams.terminalName,
                this.requestParams.lastModifiedUserName,
                this.requestParams.outputType,
                this.requestParams.startDate,
                this.requestParams.endDate,
                this.requestParams.location,
                this.requestParams.userId,
                this.requestParams.locations,
                this.requestParams.locationGroup_Locations,
                this.requestParams.locationGroup_Groups,
                this.requestParams.locationGroup_LocationTags,
                this.requestParams.locationGroup_NonLocations,
                this.requestParams.locationGroup_Group,
                this.requestParams.locationGroup_LocationTag,
                this.requestParams.locationGroup_UserId,
                this.requestParams.notCorrectDate,
                this.requestParams.credit,
                this.requestParams.refund,
                this.requestParams.ticketNo,
                this.requestParams.exportOutputType,
                undefined,
                this.requestParams.dynamicFilter,
                undefined,
                undefined,
                this.requestParams.sorting,
                this.requestParams.maxResultCount,
                this.requestParams.skipCount,
                this.requestParams.onlystatus
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.ticketViewList.totalCount;
                this.primengTableHelper.records = result.ticketViewList.items;
                this.totalTicketAmount = result.dashBoardDto.totalAmount;
                this.totalTickets = result.dashBoardDto.totalTicketCount;
                this.totalOrders = result.dashBoardDto.totalOrderCount;
                this.totalItems = result.dashBoardDto.totalItemSold;
                this.totalAverage = result.dashBoardDto.averageTicketAmount;
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        this._ticketReportServiceProxy
            .getAllToExcel(
                this.requestParams.promotions,
                this.requestParams.paymentTags,
                this.requestParams.payments,
                this.requestParams.transactions,
                this.requestParams.departments,
                this.requestParams.ticketTags,
                this.requestParams.department,
                this.requestParams.terminalName,
                this.requestParams.lastModifiedUserName,
                this.requestParams.outputType,
                this.requestParams.startDate,
                this.requestParams.endDate,
                this.requestParams.location,
                this.requestParams.userId,
                this.requestParams.locations,
                this.requestParams.locationGroup_Locations,
                this.requestParams.locationGroup_Groups,
                this.requestParams.locationGroup_LocationTags,
                this.requestParams.locationGroup_NonLocations,
                this.requestParams.locationGroup_Group,
                this.requestParams.locationGroup_LocationTag,
                this.requestParams.locationGroup_UserId,
                this.requestParams.notCorrectDate,
                this.requestParams.credit,
                this.requestParams.refund,
                this.requestParams.ticketNo,
                exportOutputType || this.requestParams.exportOutputType,
                undefined,
                this.requestParams.dynamicFilter,
                undefined,
                undefined,
                this.requestParams.sorting,
                this.requestParams.maxResultCount,
                this.requestParams.skipCount
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    showDetails(record) {
        this.detailmodal.show(record.id);
    }

    onFilter(data) {
        this._setRequestParams(data);

        this.getTickets();
    }

    private _setRequestParams(data) {
        this.requestParams.departments = [];
        this.requestParams.payments = [];
        this.requestParams.transactions = [];
        data.departments.forEach((item) => {
            let newItem = new ComboboxItemDto();
            newItem.value = item.value;
            newItem.displayText = item.name;
            this.requestParams.departments.push(newItem);
        });
        data.paymentTypes.forEach((item) => {
            this.requestParams.payments.push(+item.value);
        });
        data.transactionTypes.forEach((item) => {
            let newItem = new ComboboxItemDto();
            newItem.value = item.value;
            newItem.displayText = item.name;
            this.requestParams.transactions.push(newItem);
        });

        this.requestParams.startDate =
            data.dateRangeModel && data.dateRangeModel.length > 0
                ? moment(data.dateRangeModel[0]).startOf('day').add(moment().utcOffset(), 'minute')
                : undefined;
        this.requestParams.endDate =
            data.dateRangeModel && data.dateRangeModel.length > 0
                ? moment(data.dateRangeModel[1]).endOf('day').add(moment().utcOffset(), 'minute')
                : undefined;
        
        this.requestParams.lastModifiedUserName = data.lastModifiedUserName;
        this.requestParams.credit = data.credit;
        this.requestParams.refund = data.refund;
        this.requestParams.ticketNo = data.ticketNo;
        this.requestParams.ticketTags = data.ticketTags;
    }

    clear() {
        this.filterModal.clear();
        this.filterModal.apply()
        this.selectLocationOrGroup.clear();
        this.locationOrGroup = new CommonLocationGroupDto();
    }

    refresh() {
        this.clear()
        this.getTickets();
    }

    openFilterModal() {
        this.isShowFilterOptions =  true;
    }

    get getAggregationValue() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.totalAmount;
        });
    }
}

interface IRequestParams {
    promotions: PromotionListDto[] | null | undefined;
    paymentTags: string[] | null | undefined;
    payments: number[] | null | undefined;
    transactions: ComboboxItemDto[] | null | undefined;
    departments: ComboboxItemDto[] | null | undefined;
    ticketTags: ComboboxItemDto[] | null | undefined;
    department: string | null | undefined;
    terminalName: string | null | undefined;
    lastModifiedUserName: string | null | undefined;
    outputType: string | null | undefined;
    startDate: moment.Moment | undefined;
    endDate: moment.Moment | undefined;
    location: number | undefined;
    userId: number | undefined;
    locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Groups: SimpleLocationGroupDto[] | null | undefined;
    locationGroup_LocationTags: SimpleLocationTagDto[] | null | undefined;
    locationGroup_NonLocations: SimpleLocationDto[] | null | undefined;
    locationGroup_Group: boolean | undefined;
    locationGroup_LocationTag: boolean | undefined;
    locationGroup_UserId: number | undefined;
    notCorrectDate: boolean | undefined;
    credit: boolean | undefined;
    refund: boolean | undefined;
    ticketNo: string | null | undefined;
    exportOutputType: ExportType;
    dynamicFilter: string | null | undefined;
    sorting: string | null | undefined;
    maxResultCount: number | undefined;
    skipCount: number | undefined;
    onlystatus: boolean | undefined;
}

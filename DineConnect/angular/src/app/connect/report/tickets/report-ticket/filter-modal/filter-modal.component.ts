import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DepartmentsServiceProxy, PaymentTypesServiceProxy, TransactionTypesServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-filter-modal',
    templateUrl: './filter-modal.component.html',
    styleUrls: ['./filter-modal.component.scss']
})
export class FilterModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('commonMultipleLookupModal', { static: true }) commonMultipleLookupModal: CommonMultipleLookupModalComponent;

    @Output() applyFilter = new EventEmitter<any>();
    @Output() close = new EventEmitter<any>();

    dateRangeModel: any[] = [new Date(), new Date()];
    lookupName: string;
    transactionTypes = [];
    paymentTypes = [];
    departments = [];
    ticketTags: string;
    lastModifiedUserName; string;
    credit: boolean;
    refund: boolean;
    ticketNo: string;

    constructor(
        injector: Injector,
        private _departmentServiceProxy: DepartmentsServiceProxy,
        private _paymentTypeServiceProxy: PaymentTypesServiceProxy,
        private _transactionTypeServiceProxy: TransactionTypesServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {}

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        this.departments = [];
        this.paymentTypes = [];
        this.transactionTypes = [];
        this.lastModifiedUserName = '';
        this.ticketNo = '';
        this.ticketTags = '';
        this.credit = false;
        this.refund = false;
    }

    apply() {
        const data = {
            dateRangeModel: this.dateRangeModel,
            departments: this.departments,
            paymentTypes: this.paymentTypes,
            transactionTypes: this.transactionTypes,
            lastModifiedUserName: this.lastModifiedUserName,
            credit: this.credit,
            refund: this.refund,
            ticketNo: this.ticketNo,
            ticketTags: this.ticketTags
        }
        this.applyFilter.emit(data);
        this.close.emit()
    }

    onRefresh() {
        this.clear();
        this.apply();
    }

    showLookupModal(name) {
        this.lookupName = name;

        switch (name) {
            case 'Department':
                this.commonMultipleLookupModal.configure({
                    title: this.l('SelectDepartment'),
                    dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                        return this._departmentServiceProxy.getDepartmentsForLookup(filter, 'name', skipCount, maxResultCount);
                    }
                });

                this.commonMultipleLookupModal.show(this.departments);
                break;
            case 'Payment':
                this.commonMultipleLookupModal.configure({
                    title: this.l('Select') + ' ' + this.l('Payments'),
                    dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                        return this._paymentTypeServiceProxy.getPaymentTypeForLookupTable(filter, 'name', skipCount, maxResultCount);
                    }
                });

                this.commonMultipleLookupModal.show(this.paymentTypes);
                break;
            case 'Transaction':
                this.commonMultipleLookupModal.configure({
                    title: this.l('Select') + ' ' + this.l('Transactions'),
                    dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                        return this._transactionTypeServiceProxy.getTransactionTypeForLookupTablePaging(
                            filter,
                            'name',
                            skipCount,
                            maxResultCount
                        );
                    }
                });

                this.commonMultipleLookupModal.show(this.transactionTypes);
                break;

            default:
                break;
        }
    }

    itemsSelected(event) {
        switch (this.lookupName) {
            case 'Department':
                this.departments = event;
                break;
            case 'Payment':
                this.paymentTypes = event;

                break;
            case 'Transaction':
                this.transactionTypes = event;
                break;

            default:
                break;
        }
    }

    clearLookup(name) {
        switch (name) {
            case 'Department':
                this.departments = [];
                break;
            case 'Payment':
                this.paymentTypes = [];
                break;
            case 'Transaction':
                this.transactionTypes = [];
                break;

            default:
                break;
        }
    }
}

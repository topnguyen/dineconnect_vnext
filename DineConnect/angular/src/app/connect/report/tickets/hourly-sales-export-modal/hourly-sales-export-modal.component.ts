import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TicketHourlySalesReportServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

type NewType = EventEmitter<any>;

@Component({
  selector: 'app-hourly-sales-export-modal',
  templateUrl: './hourly-sales-export-modal.component.html',
  styleUrls: ['./hourly-sales-export-modal.component.css']
})
export class HourlySalesExportModalComponent extends AppComponentBase {
  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @Output() modalSave: NewType = new EventEmitter<any>();

  
  durations = [
    { id: "D", label: "Date" },
    { id: "S", label: "Summary" },
    
  ];
  duration: string = this.durations[0].id;  
  exportOutputs = [
    { id: "T", label: "TotalAmount" },
    { id: "I", label: "Tickets" },
    { id: "B", label: "Both" }
  ];
  exportOutput: string = this.exportOutputs[0].id;
  
  byLocation: boolean = false;
  constructor(
    injector: Injector,

  ) {
    super(injector);
  }

  ngOnInit(): void {
  }

  show(): void {
    this.modal.show();
  }

  close() {
    this.modal.hide();
  }
  export() {
    this.modalSave.emit({duration: this.duration,exportOutput:  this.exportOutput, byLocation: this.byLocation});
    this.close();
  }

}

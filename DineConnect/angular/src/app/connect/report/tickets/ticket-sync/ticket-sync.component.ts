import { Component, Injector, ViewEncapsulation, ViewChild, Renderer2, ElementRef, OnInit } from '@angular/core';
import {
    CommonLocationGroupDto,
    SimpleLocationDto,
    IdNameDto,
    ExportType,
    TicketSyncReportServiceProxy,
    SimpleLocationTagDto,
    SimpleLocationGroupDto
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
    templateUrl: './ticket-sync.component.html',
    styleUrls: ['./ticket-sync.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TicketSyncComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;

    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    requestParams: IRequestParams = {
        exportOutputType: ExportType.Excel
    } as IRequestParams;
    loading = false;
    disableExport = true;
    ExportType = ExportType;

    constructor(
        injector: Injector,
        private _ticketSyncReportServiceProxy: TicketSyncReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
    ) {
        super(injector);
    }

    ngOnInit() {}

    getTicketSyncs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup_Locations = this.locationOrGroup.locations;
        this.requestParams.locationGroup_Groups = this.locationOrGroup.groups;
        this.requestParams.locationGroup_LocationTags = this.locationOrGroup.locationTags;
        this.requestParams.locationGroup_NonLocations = this.locationOrGroup.nonLocations;
        this.requestParams.locationGroup_Group = this.locationOrGroup.group;
        this.requestParams.locationGroup_LocationTag = this.locationOrGroup.locationTag;
        this.requestParams.locationGroup_UserId = this.locationOrGroup.userId;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

        this._ticketSyncReportServiceProxy
            .getTicketSyncs(
                this.requestParams.startDate,
                this.requestParams.endDate,
                this.requestParams.location,
                this.requestParams.locations,
                this.requestParams.locationGroup_Locations,
                this.requestParams.locationGroup_Groups,
                this.requestParams.locationGroup_LocationTags,
                this.requestParams.locationGroup_NonLocations,
                this.requestParams.locationGroup_Group,
                this.requestParams.locationGroup_LocationTag,
                this.requestParams.locationGroup_UserId,
                this.requestParams.userId,
                this.requestParams.notCorrectDate,
                this.requestParams.credit,
                this.requestParams.refund,
                this.requestParams.ticketNo,
                this.requestParams.exportOutputType,
                this.requestParams.sorting,
                this.requestParams.maxResultCount,
                this.requestParams.skipCount
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        this._ticketSyncReportServiceProxy
            .getTicketSyncToExcel(
                this.requestParams.startDate,
                this.requestParams.endDate,
                this.requestParams.location,
                this.requestParams.locations,
                this.requestParams.locationGroup_Locations,
                this.requestParams.locationGroup_Groups,
                this.requestParams.locationGroup_LocationTags,
                this.requestParams.locationGroup_NonLocations,
                this.requestParams.locationGroup_Group,
                this.requestParams.locationGroup_LocationTag,
                this.requestParams.locationGroup_UserId,
                this.requestParams.userId,
                this.requestParams.notCorrectDate,
                this.requestParams.credit,
                this.requestParams.refund,
                this.requestParams.ticketNo,
                exportOutputType || this.requestParams.exportOutputType,
                this.requestParams.sorting,
                this.requestParams.maxResultCount,
                this.requestParams.skipCount
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

interface IRequestParams {
    startDate: moment.Moment | undefined;
    endDate: moment.Moment | undefined;
    location: number | undefined;
    locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Groups: SimpleLocationGroupDto[] | null | undefined;
    locationGroup_LocationTags: SimpleLocationTagDto[] | null | undefined;
    locationGroup_NonLocations: SimpleLocationDto[] | null | undefined;
    locationGroup_Group: boolean | undefined;
    locationGroup_LocationTag: boolean | undefined;
    locationGroup_UserId: number | undefined;
    userId: number | undefined;
    notCorrectDate: boolean | undefined;
    credit: boolean | undefined;
    refund: boolean | undefined;
    ticketNo: string | null | undefined;
    exportOutputType: ExportType;
    sorting: string | null | undefined;
    maxResultCount: number | undefined;
    skipCount: number | undefined;
}

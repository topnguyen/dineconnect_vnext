import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TenantSettingsServiceProxy, TicketListDto, TicketServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-order-report-detail-modal',
  templateUrl: './order-report-detail-modal.component.html',
  styleUrls: ['./order-report-detail-modal.component.css']
})
export class OrderReportDetailModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  isShown = false;
  loading = false;
  ticketId: number;
  ticket = new TicketListDto();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  constructor(injector: Injector,
    private _ticketService: TicketServiceProxy,
    private _settingService: TenantSettingsServiceProxy

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }

  show(id?) {
    this.ticketId = id;
    this.getOrders();
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }

  getOrders() {
    this.loading = true;
    this._ticketService.getInputTicket(this.ticketId)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe((result) => {
        this.ticket = result;
      });
  }

}

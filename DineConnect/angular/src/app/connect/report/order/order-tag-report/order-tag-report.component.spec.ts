import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTagReportComponent } from './order-tag-report.component';

describe('OrderTagReportComponent', () => {
  let component: OrderTagReportComponent;
  let fixture: ComponentFixture<OrderTagReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTagReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTagReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

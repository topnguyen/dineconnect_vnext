import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ExportType,
  OrderTagGroupServiceProxy,
  GetOrderTagReportInput,
} from "@shared/service-proxies/service-proxies-nswag";
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { OrderReportDetailModalComponent } from "../../order/order-report-detail-modal.component";
import { SelectItem } from "primeng/api";

@Component({
  selector: 'app-order-tag-report',
  templateUrl: './order-tag-report.component.html',
  styleUrls: ['./order-tag-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})

export class OrderTagReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;
  @ViewChild("detailmodal", { static: false })
  detailmodal: OrderReportDetailModalComponent;
  orderTagList =[];
  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  totalAmount;
  totalOrders;
  totalItems;
  selectedOrderTag = [];
  requestParams: GetOrderTagReportInput = {
      exportOutputType: ExportType.Excel,
  } as GetOrderTagReportInput;
  query: any;
  config: QueryBuilderConfig = {
      fields: {},
  };
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;

  constructor(
      injector: Injector,
      private _orderTagReportServiceProxy: OrderTagGroupServiceProxy,
      private _fileDownloadService: FileDownloadService,
      private render: Renderer2,
  ) {
      super(injector);
  }

  ngOnInit() {
      this.getComboBoxData();
      this._createBuilder();
  }

    getComboBoxData() {
        forkJoin([
            this._orderTagReportServiceProxy.getIds(),
        ]).subscribe((results) => {
            this.orderTagList = [];
            for (const key in results[0]) {
                if (Object.prototype.hasOwnProperty.call(results[0], key)) {
                    const element = results[0][key];

                    this.orderTagList.push(...element);
                }
            }
            this.orderTagList = results[0].items.map((item) => {
                return {
                    value: item.id,
                    name: item.name,
                };
            });
        });
    }
    getOrders(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;

        this.requestParams.orderTagGroupIds = this.selectedOrderTag.map( item => {
            return item.value;
        });
        this.requestParams.sorting = this.primengTableHelper.getSorting(
            this.dataTable
        );
        this.requestParams.maxResultCount =
            this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );

      if (this.query) {
          this.requestParams.dynamicFilter = JSON.stringify(
              this.mapQuery(this.query, this.config)
          );
      }
      this._orderTagReportServiceProxy
          .buildOrderTagReport(this.requestParams)
          .pipe(
              finalize(() => this.primengTableHelper.hideLoadingIndicator())
          )
          .subscribe((result) => {
              console.log("data test", result);
              this.primengTableHelper.totalRecordsCount = result.data.totalCount;
              this.primengTableHelper.records = result.data.items;
              this.totalAmount = result.dashBoardOrderDto.totalAmount;
              this.totalOrders = result.dashBoardOrderDto.totalOrderCount;
              this.totalItems = result.dashBoardOrderDto.totalItemSold;
          });
  }

  showedDateRangePicker(): void {
      let button = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-predefined-btns"
      ).lastElementChild;
      let self = this;
      let el = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
      );
      self.render.removeClass(el, "show");

      button.addEventListener("click", (event) => {
          event.preventDefault();

          self.render.addClass(el, "show");
      });
  }

  reloadPage(): void {
      this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
      
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.orderTagGroupIds = this.selectedOrderTag;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;
    if (this.query) {
        this.requestParams.dynamicFilter = JSON.stringify(
            this.mapQuery(this.query, this.config)
        );
    }
      this._orderTagReportServiceProxy
          .buildOrderTagsExcel(this.requestParams)
          .subscribe((result) => {
              this._fileDownloadService.downloadFromFilePath(result);
          });
  }

  openSelectLocationModal() {
      this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
      this.locationOrGroup = event;
  }

  showDetails(record) {
      this.detailmodal.show(record.ticketId);
  }

  clear() {
      this.dateRangeModel = [new Date(), new Date()];
      this.requestParams = {
          exportOutputType: ExportType.Excel,
      } as GetOrderTagReportInput;
      this.query = null;
      this.getOrders();
  }

  private _createBuilder() {
      this.config.fields = {
        TicketNo: {
              name: this.l("TicketNo"),
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
              type: "string",
          },
          LocationCode: {
              name: this.l("Location"),
              operators: [
                "equal",
                "not_equal",
                "in",
                "not_in",
                "begins_with",
                "not_begins_with",
                "contains",
                "not_contains",
                "ends_with",
                "not_ends_with",
                "is_empty",
                "is_not_empty",
                "is_null",
                "is_not_null",
              ],
              type: "string",
          },        
          OrderId: {
              name: this.l("OrderId"),
              type: "string",
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          OrderTagGroup: {
              name: this.l("OrderTagGroup"),
              type: "string",
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          OrderTagName: {
              name: this.l("OrderTagName"),
              type: "string",
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          Quantity: {
              name: this.l("Quantity"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          Price: {
              name: this.l("Price"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          Total: {
              name: this.l("Total"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          User: {
              name: this.l("User"),
              type: "string",
              operators: ["equal", "not_equal", "is_null", "is_not_null"],
          },          
      };
  }

  get getAggregationQuantityValue() {
      return sumBy(this.primengTableHelper.records, (o) => {
          return o.quantity;
      });
  }
  get getAggregationPriceValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.price;
    });
  }
  get getAggregationTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.total;
    });
  }
}

interface OrderTagCombo {
  name: string,
  code: number
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderReturnReportComponent } from './order-return-report.component';

describe('OrderReturnReportComponent', () => {
  let component: OrderReturnReportComponent;
  let fixture: ComponentFixture<OrderReturnReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderReturnReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReturnReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ExportType,
  OrderTagGroupServiceProxy,
  GetOrderExchangeReportInput,
} from "@shared/service-proxies/service-proxies-nswag";
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { OrderReportDetailModalComponent } from "../../order/order-report-detail-modal.component";

@Component({
  selector: 'app-order-return-report',
  templateUrl: './order-return-report.component.html',
  styleUrls: ['./order-return-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})

export class OrderReturnReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;
  @ViewChild("detailmodal", { static: false })
  detailmodal: OrderReportDetailModalComponent;

  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  totalAmount;
  totalOrders;
  totalItems;
  requestParams: GetOrderExchangeReportInput = {
      exportOutputType: ExportType.Excel,
  } as GetOrderExchangeReportInput;
  query: any;
  config: QueryBuilderConfig = {
      fields: {},
  };
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;

  constructor(
      injector: Injector,
      private _orderTagReportServiceProxy: OrderTagGroupServiceProxy,
      private _fileDownloadService: FileDownloadService,
      private render: Renderer2,
  ) {
      super(injector);
  }

  ngOnInit() {
      this.getComboBoxData();
      this._createBuilder();
  }

  getComboBoxData() {
     
  }
  getOrders(event?: LazyLoadEvent) {
      if (this.primengTableHelper.shouldResetPaging(event)) {
          this.paginator.changePage(0);
          return;
      }
      this.primengTableHelper.showLoadingIndicator();

      this.requestParams.locationGroup = this.locationOrGroup;

      this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
      this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

      this.requestParams.sorting = this.primengTableHelper.getSorting(
          this.dataTable
      );
      this.requestParams.maxResultCount =
          this.primengTableHelper.getMaxResultCount(this.paginator, event);
      this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
          this.paginator,
          event
      );

      if (this.query) {
          this.requestParams.dynamicFilter = JSON.stringify(
              this.mapQuery(this.query, this.config)
          );
      }
      this._orderTagReportServiceProxy
          .buildReturnProductReport(this.requestParams)
          .pipe(
              finalize(() => this.primengTableHelper.hideLoadingIndicator())
          )
          .subscribe((result) => {
              console.log("data test", result);
              this.primengTableHelper.totalRecordsCount = result.data.totalCount;
              this.primengTableHelper.records = result.data.items;
              this.totalAmount = result.dashBoardOrderDto.totalAmount;
              this.totalOrders = result.dashBoardOrderDto.totalOrderCount;
              this.totalItems = result.dashBoardOrderDto.totalItemSold;
          });
  }

  showedDateRangePicker(): void {
      let button = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-predefined-btns"
      ).lastElementChild;
      let self = this;
      let el = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
      );
      self.render.removeClass(el, "show");

      button.addEventListener("click", (event) => {
          event.preventDefault();

          self.render.addClass(el, "show");
      });
  }

  reloadPage(): void {
      this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    this.requestParams.locationGroup = this.locationOrGroup;

    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;

      this._orderTagReportServiceProxy
          .buildReturnProductExcel( this.requestParams)
          .subscribe((result) => {
              this._fileDownloadService.downloadFromFilePath(result);
          });
  }

  openSelectLocationModal() {
      this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
      this.locationOrGroup = event;
  }

  showDetails(record) {
      this.detailmodal.show(record.ticketId);
  }

  clear() {
      this.dateRangeModel = [new Date(), new Date()];
      this.requestParams = {
          exportOutputType: ExportType.Excel,
      } as GetOrderExchangeReportInput;
      this.query = null;
      this.getOrders();
  }

  private _createBuilder() {
      this.config.fields = {
          TicketId: {
              name: this.l("Ticket"),
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
              type: "number",
          },
          TicketNo: {
              name: this.l("TicketNo"),
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
              type: "string",
          },
          DepartmentName: {
              name: this.l("DepartmentName"),
              type: "category",
              options: this.departments,
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          MenuItemId: {
              name: this.l("MenuItem"),
              type: "category",
              options: this.menuItems,
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          OrderNumber: {
              name: this.l("OrderNumber"),
              type: "string",
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          PortionName: {
              name: this.l("Portion"),
              type: "string",
              operators: [
                  "equal",
                  "not_equal",
                  "in",
                  "not_in",
                  "begins_with",
                  "not_begins_with",
                  "contains",
                  "not_contains",
                  "ends_with",
                  "not_ends_with",
                  "is_empty",
                  "is_not_empty",
                  "is_null",
                  "is_not_null",
              ],
          },
          Quantity: {
              name: this.l("Quantity"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          Price: {
              name: this.l("Price"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          PromotionAmount: {
              name: this.l("PromotionAmount"),
              type: "number",
              operators: [
                  "equal",
                  "less",
                  "less_or_equal",
                  "greater",
                  "greater_or_equal",
              ],
          },
          LastModifiedUserName: {
              name: this.l("User"),
              type: "string",
              operators: ["equal", "not_equal", "is_null", "is_not_null"],
          },
          Gift: {
              name: this.l("Gift"),
              type: "string",
              operators: ["equal", "not_equal", "is_null", "is_not_null"],
          },
          Comp: {
              name: this.l("Comp"),
              type: "string",
              operators: ["equal", "not_equal", "is_null", "is_not_null"],
          },
      };
  }

  get getAggregationQuantityValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.quantity;
    });
  }
  get getAggregationAmountValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.amount;
    });
  }
}


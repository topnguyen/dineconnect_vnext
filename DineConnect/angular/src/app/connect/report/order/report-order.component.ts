import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    Renderer2,
    ElementRef,
    OnInit,
} from "@angular/core";
import {
    CommonLocationGroupDto,
    PromotionListDto,
    SimpleLocationDto,
    IdNameDto,
    ConnectLookupServiceProxy,
    ExportType,
    OrderReportServiceProxy,
    MenuItemServiceProxy,
    GetItemInput,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
    QueryBuilderComponent,
    QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { OrderReportDetailModalComponent } from "./order-report-detail-modal.component";

@Component({
    templateUrl: "./report-order.component.html",
    styleUrls: ["./report-order.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ReportOrderComponent extends AppComponentBase implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
    @ViewChild("queryBuilder", { static: false })
    queryBuilder: QueryBuilderComponent;
    @ViewChild("detailmodal", { static: false })
    detailmodal: OrderReportDetailModalComponent;

    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    totalAmount;
    totalOrders;
    totalItems;
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel,
    } as GetItemInput;
    query: any;
    config: QueryBuilderConfig = {
        fields: {},
    };
    decimals = 2;
    loading = false;
    menuItems = [];
    departments = [];
    disableExport = true;
    ExportType = ExportType;
    advancedFiltersAreShown = false;
    terminalName: string;

    constructor(
        injector: Injector,
        private _orderReportServiceProxy: OrderReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getComboBoxData();
        this._createBuilder();
    }

    getComboBoxData() {
        forkJoin([
            this._menuItemServiceProxy.getMenuItemForComboBox(),
            this._connectLookupServiceProxy.getDepartments(),
        ]).subscribe((results) => {
            this.menuItems = [];
            for (const key in results[0]) {
                if (Object.prototype.hasOwnProperty.call(results[0], key)) {
                    const element = results[0][key];

                    this.menuItems.push(...element);
                }
            }

            this.menuItems = unionBy(this.menuItems, "id").map((item) => {
                return {
                    value: item.value,
                    name: item.displayText,
                };
            });
            this.departments = results[1].items.map((item) => {
                return {
                    value: item.value,
                    name: item.displayText,
                };
            });
        });
    }
    getOrders(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

        this.requestParams.sorting = this.primengTableHelper.getSorting(
            this.dataTable
        );
        this.requestParams.maxResultCount =
            this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );

        if (this.query) {
            this.requestParams.dynamicFilter = JSON.stringify(
                this.mapQuery(this.query, this.config)
            );
        }

        var input = new GetItemInput();
        input.init(this.requestParams);
        input.terminalName = this.terminalName;

        this._orderReportServiceProxy
            .buildOrderReport(input)
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount =
                    result.orderViewList.totalCount;
                this.primengTableHelper.records = result.orderViewList.items;
                this.totalAmount = result.dashBoardDto.totalAmount;
                this.totalOrders = result.dashBoardDto.totalOrderCount;
                this.totalItems = result.dashBoardDto.totalItemSold;
            });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;
        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        self.render.removeClass(el, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        var input = new GetItemInput();
        input.init(this.requestParams);
        input.terminalName = this.terminalName;
        input.exportOutputType = exportOutputType;

        this._orderReportServiceProxy
            .buildOrderReportExcel(input)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    showDetails(record) {
        this.detailmodal.show(record.ticketId);
    }

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        this.requestParams = {
            exportOutputType: ExportType.Excel,
        } as GetItemInput;
        this.query = null;
        this.getOrders();
    }

    private _createBuilder() {
        this.config.fields = {
            TicketId: {
                name: this.l("Ticket"),
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
                type: "number",
            },
            TicketNo: {
                name: this.l("TicketNo"),
                operators: [
                    "equal",
                    "not_equal",
                    "in",
                    "not_in",
                    "begins_with",
                    "not_begins_with",
                    "contains",
                    "not_contains",
                    "ends_with",
                    "not_ends_with",
                    "is_empty",
                    "is_not_empty",
                    "is_null",
                    "is_not_null",
                ],
                type: "string",
            },
            DepartmentName: {
                name: this.l("DepartmentName"),
                type: "category",
                options: this.departments,
                operators: [
                    "equal",
                    "not_equal",
                    "in",
                    "not_in",
                    "begins_with",
                    "not_begins_with",
                    "contains",
                    "not_contains",
                    "ends_with",
                    "not_ends_with",
                    "is_empty",
                    "is_not_empty",
                    "is_null",
                    "is_not_null",
                ],
            },
            MenuItemId: {
                name: this.l("MenuItem"),
                type: "category",
                options: this.menuItems,
                operators: [
                    "equal",
                    "not_equal",
                    "in",
                    "not_in",
                    "begins_with",
                    "not_begins_with",
                    "contains",
                    "not_contains",
                    "ends_with",
                    "not_ends_with",
                    "is_empty",
                    "is_not_empty",
                    "is_null",
                    "is_not_null",
                ],
            },
            OrderNumber: {
                name: this.l("OrderNumber"),
                type: "string",
                operators: [
                    "equal",
                    "not_equal",
                    "in",
                    "not_in",
                    "begins_with",
                    "not_begins_with",
                    "contains",
                    "not_contains",
                    "ends_with",
                    "not_ends_with",
                    "is_empty",
                    "is_not_empty",
                    "is_null",
                    "is_not_null",
                ],
            },
            PortionName: {
                name: this.l("Portion"),
                type: "string",
                operators: [
                    "equal",
                    "not_equal",
                    "in",
                    "not_in",
                    "begins_with",
                    "not_begins_with",
                    "contains",
                    "not_contains",
                    "ends_with",
                    "not_ends_with",
                    "is_empty",
                    "is_not_empty",
                    "is_null",
                    "is_not_null",
                ],
            },
            Quantity: {
                name: this.l("Quantity"),
                type: "number",
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
            },
            Price: {
                name: this.l("Price"),
                type: "number",
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
            },
            PromotionAmount: {
                name: this.l("PromotionAmount"),
                type: "number",
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
            },
            LastModifiedUserName: {
                name: this.l("User"),
                type: "string",
                operators: ["equal", "not_equal", "is_null", "is_not_null"],
            },
            Gift: {
                name: this.l("Gift"),
                type: "string",
                operators: ["equal", "not_equal", "is_null", "is_not_null"],
            },
            Comp: {
                name: this.l("Comp"),
                type: "string",
                operators: ["equal", "not_equal", "is_null", "is_not_null"],
            },
        };
    }

    get getAggregationValue() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.totalAmount;
        });
    }
}

export interface IRequestParams {
    includeTax: boolean | undefined;
    filterByUser: string[] | null | undefined;
    filterByTerminal: string[] | null | undefined;
    filterByDepartment: string[] | null | undefined;
    filterByCategory: number[] | null | undefined;
    filterByGroup: number[] | null | undefined;
    filterByTag: string[] | null | undefined;
    isItemSalesReport: boolean | undefined;
    isSummary: boolean | undefined;
    lastModifiedUserName: string | null | undefined;
    gift: boolean | undefined;
    void: boolean | undefined;
    comp: boolean | undefined;
    sales: boolean | undefined;
    exchange: boolean | undefined;
    noSales: boolean | undefined;
    stats: boolean | undefined;
    byLocation: boolean | undefined;
    duration: string | null | undefined;
    workPeriodId: number | undefined;
    outputType: string | null | undefined;
    promotions: PromotionListDto[] | null | undefined;
    startDate: moment.Moment | undefined;
    endDate: moment.Moment | undefined;
    location: number | undefined;
    userId: number | undefined;
    notCorrectDate: boolean | undefined;
    locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Locations: SimpleLocationDto[] | null | undefined;
    locationGroup_Groups: IdNameDto[] | null | undefined;
    locationGroup_LocationTags: IdNameDto[] | null | undefined;
    locationGroup_NonLocations: SimpleLocationDto[] | null | undefined;
    locationGroup_Group: boolean | undefined;
    locationGroup_LocationTag: boolean | undefined;
    locationGroup_UserId: number | undefined;
    credit: boolean | undefined;
    refund: boolean | undefined;
    ticketNo: string | null | undefined;
    exportOutputType: ExportType;
    menuItemIds: number[] | null | undefined;
    menuItemPortionIds: number[] | null | undefined;
    isExport: boolean | undefined;
    reportDescription: string | null | undefined;
    takeNumber: number | undefined;
    takeType: string | null | undefined;
    ranking: string | null | undefined;
    takeString: string | null | undefined;
    dynamicFilter: string | null | undefined;
    sorting: string | null | undefined;
    maxResultCount: number | undefined;
    skipCount: number | undefined;
}

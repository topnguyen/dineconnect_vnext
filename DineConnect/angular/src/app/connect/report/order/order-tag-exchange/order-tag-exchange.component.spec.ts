import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTagExchangeComponent } from './order-tag-exchange.component';

describe('OrderTagExchangeComponent', () => {
  let component: OrderTagExchangeComponent;
  let fixture: ComponentFixture<OrderTagExchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTagExchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTagExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TicketListDto, TicketServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-order-promotion-report-detail-modal',
  templateUrl: './order-promotion-report-detail-modal.component.html',
  styleUrls: ['./order-promotion-report-detail-modal.component.css']
})
export class OrderPromotionReportDetailModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  isShown = false;
  loading = false;
  ticket = new TicketListDto();

  constructor(injector: Injector, private _ticketAppservice: TicketServiceProxy) {
    super(injector);
  }

  ngOnInit(): void {
  }

  show(data) {
    this._ticketAppservice.getInputTicket(data).subscribe(result => {
      this.ticket = result;
    })
    this.ticket = data;
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }
}

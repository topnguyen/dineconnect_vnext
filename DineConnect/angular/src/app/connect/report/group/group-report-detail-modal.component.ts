import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GroupReportDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-group-report-detail-modal',
  templateUrl: './group-report-detail-modal.component.html',
  styleUrls: ['./group-report-detail-modal.component.css']
})
export class GroupReportDetailModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  isShown = false;
  loading = false;
  data = new GroupReportDto();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
  }

  show(data) {
    this.data = data;
    console.log('data', this.data);
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }
}

import { Component, Injector, ViewEncapsulation, ViewChild, Renderer2, ElementRef, OnInit } from '@angular/core';
import {
    CommonLocationGroupDto,
    ConnectLookupServiceProxy,
    ExportType,
    MenuItemServiceProxy,
    GetItemInput,
    GroupReportServiceProxy
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { unionBy } from 'lodash';
import { forkJoin } from 'rxjs';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { QueryBuilderComponent, QueryBuilderConfig } from 'angular2-query-builder';
import { finalize } from 'rxjs/operators';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { GroupReportDetailModalComponent } from './group-report-detail-modal.component';
import _ from 'lodash';

@Component({
    templateUrl: './group-report.component.html',
    styleUrls: ['./group-report.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class GroupReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    @ViewChild('queryBuilder', { static: false }) queryBuilder: QueryBuilderComponent;
    @ViewChild('detailmodal', { static: false }) detailmodal: GroupReportDetailModalComponent;

    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    totalAmount;
    totalOrders;
    totalItems;
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel
    } as GetItemInput;
    query: any;
    config: QueryBuilderConfig = {
        fields: {},
        allowEmptyRulesets: true
    };
    decimals = 2;
    loading = false;
    menuItems = [];
    departments = [];
    disableExport = true;
    ExportType = ExportType;
    advancedFiltersAreShown = false;
    dynamicBuilder: any;
    constructor(
        injector: Injector,
        private _groupReportServiceProxy: GroupReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
    }
    data: any;
    ngOnInit() {
        this.getComboBoxData();
        this._createBuilder();
    }



    getComboBoxData() {
        forkJoin([
            this._menuItemServiceProxy.getMenuItemForComboBox(),
            this._connectLookupServiceProxy.getDepartments()
        ]).subscribe((results) => {
            this.menuItems = [];
            for (const key in results[0]) {
                if (Object.prototype.hasOwnProperty.call(results[0], key)) {
                    const element = results[0][key];

                    this.menuItems.push(...element);
                }
            }

            this.menuItems = unionBy(this.menuItems, 'id').map((item) => {
                return {
                    value: item.value,
                    name: item.displayText
                };
            });
            this.departments = results[1].items.map((item) => {
                return {
                    value: item.value,
                    name: item.displayText
                };
            });

        });
    }

    getData(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);
        if(this.query) {
            this.requestParams.dynamicFilter = JSON.stringify(this.mapQuery(this.query, this.config));
        }

        var input = new GetItemInput();
        input.init(this.requestParams);
        this._groupReportServiceProxy
            .buildGroupSalesReport(input)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.groupList.totalCount;
                this.primengTableHelper.records = result.groupList.items;
                this.buildChart(result);
            });
    }

    buildChart(input){
        let dataChart = _.uniq(_.flatten(input.chartOutput.map(x=>x.data)));
        this.data = {
            labels: input.groups,
            datasets: [
                {
                    label: this.l('Total'),
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: dataChart
                }
            ]
        }
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {

        var input = new GetItemInput();
        input.init(this.requestParams);
        input.exportOutputType = exportOutputType;

        this._groupReportServiceProxy
            .buildGroupExcel(input)
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    showDetails(record) {
        this.detailmodal.show(record)
    }

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        this.requestParams = {
            exportOutputType: ExportType.Excel
        } as GetItemInput;
        this.query = null;
        this.getData();
    }

    private _createBuilder() {
        this.config.fields = {
            groupName: {
                name: this.l('Group'),
                operators: [
                    'equal',
                    'not_equal',
                    'in',
                    'not_in',
                    'begins_with',
                    'not_begins_with',
                    'contains',
                    'not_contains',
                    'ends_with',
                    'not_ends_with',
                    'is_empty',
                    'is_not_empty',
                    'is_null',
                    'is_not_null'
                ],
                type: 'string'
            },
            Quantity: {
                name: this.l('Quantity'),
                type: 'number',
                operators: ['equal', 'less', 'less_or_equal', 'greater', 'greater_or_equal']
            },
            Total: {
                name: this.l('Total'),
                type: 'number',
                operators: ['equal', 'less', 'less_or_equal', 'greater', 'greater_or_equal']
            }
        };
    }
}

import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CategoryReportDto, GroupReportDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-category-report-detail-modal',
  templateUrl: './category-report-detail-modal.component.html',
  styleUrls: ['./category-report-detail-modal.component.css']
})
export class CategoryReportDetailModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  isShown = false;
  loading = false;
  data = new CategoryReportDto();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
  }

  show(data) {
    this.data = data;
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }
}

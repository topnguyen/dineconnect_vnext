import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, ExportType, GetItemInput, GetTicketInput, IdNameDto, SaleSummaryReportServiceProxy, SimpleLocationDto, TicketSyncReportServiceProxy, WorkDayDetailSummaryOutput, WorkPeriodServiceProxy, WorkPeriodSummaryInput, WorkPeriodSummaryOutput } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import _ from 'lodash';
@Component({
    selector: 'app-week-end-report',
    templateUrl: './week-end-report.component.html',
    styleUrls: ['./week-end-report.component.css'],
    animations: [appModuleAnimation()]
})
export class WeekEndReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    requestParams: WorkPeriodSummaryInput = {
        exportOutputType: ExportType.Excel
    } as WorkPeriodSummaryInput;
    loading = false;
    disableExport = true;
    ExportType = ExportType;
    data: any;
    sumTotalTicketCount: number;
    sumTotal: number;
    dataPrint: any;
    lstweekday:WorkDayDetailSummaryOutput[] =[];
    sumtTotalTickets: number;
    sumtSales: number;
    sumtAverage: number;
    sumtDiscount: number;
    constructor(
        injector: Injector,
        private _workperiodServiceProxy: WorkPeriodServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getData();
     }

    getData() {
        this.requestParams.locationGroup = this.locationOrGroup;
        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this._workperiodServiceProxy
            .buildWeekEndSales(
                this.requestParams
            )
            .subscribe((result) => {
                this.lstweekday = result;
                this.sumtTotalTickets =_.sumBy(this.lstweekday, item => { return item.totalTickets;  });
                this.sumtSales =_.sumBy(this.lstweekday, item => { return item.sales;  });
                this.sumtAverage = _.sumBy(this.lstweekday, item => { return item.average;  });
                this.sumtDiscount = _.sumBy(this.lstweekday, item => { return item.discount;  });
                this.buildChart(result);
            });
    }
    buildChart(input) {
        let labels = input.map(x=> x.date);
        let data = input.map(x=> x.sales);
        this.data = {
            labels: labels,
            datasets: [
                {
                    label: this.l('Total'),
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: data,
                    fill: false,
                }
            ]
        }
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        var input = new WorkPeriodSummaryInput();
        input.init(this.requestParams);
        input.exportOutputType = exportOutputType;
        input.locationGroup = this.locationOrGroup;
        input.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf('day') : undefined;
        input.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf('day') : undefined;
        this._workperiodServiceProxy
            .buildWeekEndSalesExport(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    exportItemSales(id): void {
        var input = new GetItemInput();
        input.init(this.requestParams);
        input.exportOutputType =ExportType.Excel ;
        input.location = id;
        this._workperiodServiceProxy
            .buildItemSalesForWorkPeriodExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

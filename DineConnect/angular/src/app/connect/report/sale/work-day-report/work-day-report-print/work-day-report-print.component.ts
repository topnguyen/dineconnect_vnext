import { Component, Injector, Input, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AbpSessionService } from 'abp-ng2-module';

@Component({
  selector: 'app-work-day-report-print',
  templateUrl: './work-day-report-print.component.html',
  styleUrls: ['./work-day-report-print.component.scss']
})
export class WorkDayPrintPrintComponent extends AppComponentBase implements OnInit {
  @Input() data: any;
  tenantName: string;
  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit(): void {
    this.tenantName = this.appSession.tenancyName;
  }
}

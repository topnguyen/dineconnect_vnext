import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, ExportType, GetItemInput, GetTicketInput, IdNameDto, SaleSummaryReportServiceProxy, SimpleLocationDto, TicketSyncReportServiceProxy, WorkPeriodServiceProxy, WorkPeriodSummaryInput, WorkPeriodSummaryOutput } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import _ from 'lodash';
@Component({
    selector: 'app-work-day-report',
    templateUrl: './work-day-report.component.html',
    styleUrls: ['./work-day-report.component.css'],
    animations: [appModuleAnimation()]
})
export class WorkDayReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
    totalTicketAmount;
    totalTickets;
    totalOrders;
    totalItems;
    totalAverage;
    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    requestParams: WorkPeriodSummaryInput = {
        exportOutputType: ExportType.Excel
    } as WorkPeriodSummaryInput;
    loading = false;
    disableExport = true;
    ExportType = ExportType;
    data: any;
    sumTotalTicketCount: number;
    sumTotal: number;
    dataPrint: any;
    lstworkday:WorkPeriodSummaryOutput[] =[];
    constructor(
        injector: Injector,
        private _workperiodServiceProxy: WorkPeriodServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getData();
     }

    getData() {
        this.requestParams.locationGroup = this.locationOrGroup;
        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

        this._workperiodServiceProxy
            .buildSummaryReport(
                this.requestParams
            )
            .subscribe((result) => {
                this.lstworkday = result.periods;
                this.totalTicketAmount = result.dashBoard.totalSales;
                this.totalTickets = result.dashBoard.totalTickets,
                this.totalAverage = result.dashBoard.average;
            });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportDaySummaryToExcel(exportOutputType: ExportType): void {
        var input = new WorkPeriodSummaryInput();
        input.init(this.requestParams);
        input.exportOutputType = exportOutputType;
        input.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf('day') : undefined;
        input.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf('day') : undefined;
        this._workperiodServiceProxy
            .buildSummaryExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }
    exportDayDetailsToExcel(exportOutputType: ExportType): void {
        var input = new WorkPeriodSummaryInput();
        input.init(this.requestParams);
        input.exportOutputType = exportOutputType;
        input.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf('day') : undefined;
        input.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf('day') : undefined;

        this._workperiodServiceProxy
            .buildDetailExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }
    exportItemSales(id): void {
        var input = new GetItemInput();
        input.init(this.requestParams);
        input.exportOutputType =ExportType.Excel ;
        input.location = id;
        this._workperiodServiceProxy
            .buildItemSalesForWorkPeriodExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
    print(){
        window.print();
    }
}

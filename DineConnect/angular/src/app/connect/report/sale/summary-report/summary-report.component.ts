import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, ExportType, GetTicketInput, IdNameDto, SaleSummaryReportServiceProxy, SimpleLocationDto, TicketSyncReportServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import _ from 'lodash';
@Component({
    selector: 'app-summary-report',
    templateUrl: './summary-report.component.html',
    styleUrls: ['./summary-report.component.css'],
    animations: [appModuleAnimation()]
})
export class SummaryReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('drp', { static: false }) daterangePicker: ElementRef;

    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    requestParams: GetTicketInput = {
        exportOutputType: ExportType.Excel
    } as GetTicketInput;
    loading = false;
    disableExport = true;
    ExportType = ExportType;
    data: any;
    sumTotalTicketCount: number;
    sumTotal: number;
    dataPrint: any;
    constructor(
        injector: Injector,
        private _saleSummaryReportServiceProxy: SaleSummaryReportServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
    ) {
        super(injector);
    }

    ngOnInit() { }

    getTicketSyncs(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.requestParams.locationGroup = this.locationOrGroup;
        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

        this._saleSummaryReportServiceProxy
            .buildSaleSummaryReport(
                this.requestParams
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.sale.totalCount;
                this.primengTableHelper.records = result.sale.items;
                this.sumTotalTicketCount = _.sumBy(result.sale.items, function (item) {
                    return item.totalTicketCount;
                });
                this.sumTotal = _.sumBy(result.sale.items, function (item) {
                    return item.total;
                });
                this.buildChart(result.dashboard);
            });
    }
    buildChart(input) {
        this.data = {
            labels: input.dates,
            datasets: [
                {
                    label: this.l('Total'),
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: input.totals,
                    fill: false,
                }
            ]
        }
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        this.primengTableHelper.showLoadingIndicator();

        var input = new GetTicketInput();
        input.init(this.requestParams);
        input.exportOutputType = exportOutputType;

        this._saleSummaryReportServiceProxy
            .buildSaleSummaryExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
                this.primengTableHelper.hideLoadingIndicator();
            });

    }
    exportToExcelByLocation() {
        var input = new GetTicketInput();
        input.init(this.requestParams);
        input.exportOutputType = ExportType.Excel;

        this._saleSummaryReportServiceProxy
            .buildLocationComparisonReportToExcel(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }
    exportConsolidateToExcel() {
        var input = new GetTicketInput();
        input.init(this.requestParams);
        input.exportOutputType = ExportType.Excel;

        this._saleSummaryReportServiceProxy
            .buildConsolidatedExport(
                input
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadFromFilePath(result);
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
    print() {
        window.print();
    }
}

import { Component, Injector, Input, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AbpSessionService } from 'abp-ng2-module';

@Component({
  selector: 'app-sale-summary-print',
  templateUrl: './sale-summary-print.component.html',
  styleUrls: ['./sale-summary-print.component.scss']
})
export class SaleSummaryPrintComponent extends AppComponentBase implements OnInit {
  @Input() data: any;
  tenantName: string;
  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit(): void {
    this.tenantName = this.appSession.tenancyName;
  }
}

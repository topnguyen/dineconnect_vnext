import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {CentralTerminalExportInput, CentralShiftExportInput,WorkShiftDto,TenantSettingsServiceProxy,ExportType,CentralShiftReportInput, CentralExportInput,PaymentTypeByWorkTimeRowData,CentralDayReportData,WorkPeriodSummaryOutput, PlanDayReportServiceProxy,CentralTerminalReportInput,TicketTypeReportData,WorkTimePaTable,UserInfoReportData 
 } from '@shared/service-proxies/service-proxies';
 import { FileDownloadService } from '@shared/utils/file-download.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { NotifyService } from "abp-ng2-module";
@Component({
  selector: 'app-terminal-report',
  templateUrl: './terminal-report.component.html',
  styleUrls: ['./terminal-report.component.css']
})
export class TerminalReportComponent  extends AppComponentBase implements OnInit{

  @ViewChild('modal', { static: true }) modal: ModalDirective;
  isShown = false;
  loading = false;
  ExportType = ExportType;
  workPeriodId: number;
  terminal: string[]=[];
  uniqueId:any;
  filterText:'';
  workShifts:WorkShiftDto[]=[];
  dataTranferShift = new WorkPeriodSummaryOutput();
  planTerminalResult = new CentralDayReportData();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  constructor(injector: Injector,
    private _planDayReportServiceProxy: PlanDayReportServiceProxy,
    private _settingService: TenantSettingsServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _notifyService: NotifyService,

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }
  planTerminalResultticketTypeReportDatasubTotal:any;
  planTerminalResultticketTypeReportDatatax:any;
  planTerminalResultticketTypeReportDatatotalSales:any;
  planTerminalResultticketTypeReportDataticketCount:any;
  planTerminalResultticketTypeReportDataaverage:any;
  planTerminalResultticketTypeReportDatatransactions:any;
  planTerminalResultdepartments:any;
  planTerminalResultpaymentTypeReportDatadetails:any;
  planTerminalResultcashReportData:any;
  planTerminalResultworkTimePaTabledetails:PaymentTypeByWorkTimeRowData[] =[];
  planTerminalResultticketsPromotionReportDatadetails:any;
  planTerminalResultticketsPromotionReportDatatotalCount:any;
  planTerminalResultticketsPromotionReportDatatotalSum:any;
  planTerminalResultitemsData:any;
  planTerminalResultitemsDatatotalPaidItems:any;
  planTerminalResultitemsDatatotalPaidItemTotal:any;
  planTerminalResultitemsDataitemCount:any;
  planTerminalResultordersReportDatatotalPaidItemsorderCount:any;
  planTerminalResultordersReportDatatotalPaidItemsorderValue:any;
  planTerminalResultordersReportDataorderCount:any;
  planTerminalResultordersReportDataorderStateReports:any;
  planTerminalResultticketsReportDataticketStateReports:any;
  planTerminalResultticketsReportDatafirstTicketNumber:any;
  planTerminalResultticketsReportDatalastTicketNumber:any;
  planTerminalResultticketTagsReportData:any;
  planTerminalResultownersReportDatadetails:any;
  planTerminalResultownersReportDatatotalOwnersAmount:any;
  planTerminalResultusersIncomeReportData:any;

  planTerminalResultcashReportDatacashIn:any;
  planTerminalResultcashReportDataallIncomeAmount:any;
  planTerminalResultcashReportDatacashOut:any;
  planTerminalResultcashReportDataallExpAmount:any;
  planTerminalResultcashReportDatacashInDrawer:any;
  disableExport = true;
  selectedItemShift:any;
  show(data:any) {
    this.dataTranferShift = data;
    this.workPeriodId = data.wid;
    this.workShifts = this.dataTranferShift.workShifts;
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }
  
  getShiftDayData() {
    this.loading = true;
    this.terminal= [];
    this.terminal.push(this.filterText);
   var input = new CentralTerminalReportInput();
   input.workPeriodId = this.dataTranferShift.wid;
   input.terminals = this.terminal;

    this._planDayReportServiceProxy.buildCentralTerminalReport(input)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe((result) => {
        this.planTerminalResult = result;
        this.planTerminalResultticketTypeReportDatasubTotal = this.planTerminalResult.ticketTypeReportData.subTotal;
        this.planTerminalResultticketTypeReportDatatax = result.ticketTypeReportData.tax;
        this.planTerminalResultticketTypeReportDatatotalSales= result.ticketTypeReportData.totalSales;
        this.planTerminalResultticketTypeReportDataticketCount= result.ticketTypeReportData.ticketCount;
        this.planTerminalResultticketTypeReportDataaverage= result.ticketTypeReportData.average;
        this.planTerminalResultticketTypeReportDatatransactions = result.ticketTypeReportData.transactions;
        this.planTerminalResultdepartments = result.departments; 
        this.planTerminalResultpaymentTypeReportDatadetails = result.paymentTypeReportData.details;
        this.planTerminalResultcashReportData = result.cashReportData;
        this.planTerminalResultworkTimePaTabledetails = result.workTimePaTable.details;
        this.planTerminalResultticketsPromotionReportDatadetails = result.ticketsPromotionReportData.details;
        this.planTerminalResultticketsPromotionReportDatatotalCount = result.ticketsPromotionReportData.totalCount;     
        this.planTerminalResultticketsPromotionReportDatatotalSum = result.ticketsPromotionReportData.totalSum;
        this.planTerminalResultitemsData = result.itemsData;
        this.planTerminalResultitemsDatatotalPaidItems = result.itemsData.totalPaidItems;
        this.planTerminalResultitemsDatatotalPaidItemTotal = result.itemsData.totalPaidItemTotal;
        this.planTerminalResultitemsDataitemCount = result.itemsData.itemCount;
        this.planTerminalResultordersReportDatatotalPaidItemsorderCount = result.ordersReportData.totalPaidItemsorderCount;
        this.planTerminalResultordersReportDatatotalPaidItemsorderValue = result.ordersReportData.totalPaidItemsorderValue;
        this.planTerminalResultordersReportDataorderCount = result.ordersReportData.orderCount;
        this.planTerminalResultordersReportDataorderStateReports = result.ordersReportData.orderStateReports;
        this.planTerminalResultticketsReportDataticketStateReports = result.ticketsReportData.ticketStateReports;
        this.planTerminalResultticketsReportDatafirstTicketNumber = result.ticketsReportData.firstTicketNumber;
        this.planTerminalResultticketsReportDatalastTicketNumber = result.ticketsReportData.lastTicketNumber;
        this.planTerminalResultticketTagsReportData = result.ticketTagsReportData;
        this.planTerminalResultownersReportDatadetails = result.ownersReportData.details;
        this.planTerminalResultownersReportDatatotalOwnersAmount = result.ownersReportData.totalOwnersAmount;
        this.planTerminalResultusersIncomeReportData = result.usersIncomeReportData;
        this.planTerminalResultcashReportDatacashIn = result.cashReportData.cashIn;
        this.planTerminalResultcashReportDataallIncomeAmount= result.cashReportData.allIncomeAmount;
        this.planTerminalResultcashReportDatacashOut= result.cashReportData.cashOut;
        this.planTerminalResultcashReportDataallExpAmount= result.cashReportData.allExpAmount;
        this.planTerminalResultcashReportDatacashInDrawer= result.cashReportData.cashInDrawer;
      });
  }
  sum = function (items, prop) {
    if (items == null) {
        return 0;
    }
    return items.reduce(function (a:any, b:any) {
        return b[prop] == null ? +a : +a + +b[prop];
    },
        0);
};
  exportToExcel(exportOutputType: ExportType): void {
    this.terminal= [];
    this.terminal.push(this.filterText);
    var input = new CentralTerminalExportInput();
    var inputShift = new CentralTerminalReportInput();
    var wId = this.workPeriodId;
    inputShift.workPeriodId = wId;
    inputShift.terminals = this.terminal;
    
    input.centralTerminalReportInput = inputShift;
    input.dateReport = this.dataTranferShift.reportStartDay;
    input.exportOutputType = exportOutputType;
    this._planDayReportServiceProxy
      .buildCentralTerminalExport(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
}

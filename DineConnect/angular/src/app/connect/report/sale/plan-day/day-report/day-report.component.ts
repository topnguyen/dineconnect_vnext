import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TenantSettingsServiceProxy,ExportType, CentralExportInput,PaymentTypeByWorkTimeRowData,CentralDayReportData,WorkPeriodSummaryOutput, PlanDayReportServiceProxy,CentralTerminalReportInput,TicketTypeReportData,WorkTimePaTable,UserInfoReportData 
 } from '@shared/service-proxies/service-proxies';
 import { FileDownloadService } from '@shared/utils/file-download.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-day-report',
  templateUrl: './day-report.component.html',
  styleUrls: ['./day-report.component.css']
})
export class DayReportComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;
  disableExport = true;
  isShown = false;
  loading = false;
  ExportType = ExportType;
  workPeriodId: number;
  planDayResultticketTypeReportDatasubTotal:any;
  planDayResultticketTypeReportDatatax:any;
  planDayResultticketTypeReportDatatotalSales:any;
  planDayResultticketTypeReportDataticketCount:any;
  planDayResultticketTypeReportDataaverage:any;
  planDayResultticketTypeReportDatatransactions:any;
  planDayResultdepartments:any;
  planDayResultpaymentTypeReportDatadetails:any;
  planDayResultcashReportData:any;
  planDayResultworkTimePaTabledetails:PaymentTypeByWorkTimeRowData[] =[];
  planDayResultticketsPromotionReportDatadetails:any;
  planDayResultticketsPromotionReportDatatotalCount:any;
  planDayResultticketsPromotionReportDatatotalSum:any;
  planDayResultitemsData:any;
  planDayResultitemsDatatotalPaidItems:any;
  planDayResultitemsDatatotalPaidItemTotal:any;
  planDayResultitemsDataitemCount:any;
  planDayResultordersReportDatatotalPaidItemsorderCount:any;
  planDayResultordersReportDatatotalPaidItemsorderValue:any;
  planDayResultordersReportDataorderCount:any;
  planDayResultordersReportDataorderStateReports:any;
  planDayResultticketsReportDataticketStateReports:any;
  planDayResultticketsReportDatafirstTicketNumber:any;
  planDayResultticketsReportDatalastTicketNumber:any;
  planDayResultticketTagsReportData:any;
  planDayResultownersReportDatadetails:any;
  planDayResultownersReportDatatotalOwnersAmount:any;
  planDayResultusersIncomeReportData:any;
  dataTranfer = new WorkPeriodSummaryOutput();
  planDayResult = new CentralDayReportData();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  constructor(injector: Injector,
    private _planDayReportServiceProxy: PlanDayReportServiceProxy,
    private _settingService: TenantSettingsServiceProxy,
    private _fileDownloadService: FileDownloadService,

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }

  show(data:any) {
    this.dataTranfer = data;
    this.dataTranfer.reportStartDay = data.reportStartDay;
    this.dataTranfer.locationCode = data.locationCode;
    this.workPeriodId = data.wid;
    this.getPlanDay();
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }

  getPlanDay() {
    this.loading = true;
    this._planDayReportServiceProxy.buildCentralPlanDayReport( this.workPeriodId)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe((result) => {
        this.planDayResult = result;
        this.planDayResultticketTypeReportDatasubTotal = this.planDayResult.ticketTypeReportData.subTotal;
        this.planDayResultticketTypeReportDatatax = result.ticketTypeReportData.tax;
        this.planDayResultticketTypeReportDatatotalSales= result.ticketTypeReportData.totalSales;
        this.planDayResultticketTypeReportDataticketCount= result.ticketTypeReportData.ticketCount;
        this.planDayResultticketTypeReportDataaverage= result.ticketTypeReportData.average;
        this.planDayResultticketTypeReportDatatransactions = result.ticketTypeReportData.transactions;
        this.planDayResultdepartments = result.departments; 
        this.planDayResultpaymentTypeReportDatadetails = result.paymentTypeReportData.details;
        this.planDayResultcashReportData = result.cashReportData;
        this.planDayResultworkTimePaTabledetails = result.workTimePaTable.details;
        this.planDayResultticketsPromotionReportDatadetails = result.ticketsPromotionReportData.details;
        this.planDayResultticketsPromotionReportDatatotalCount = result.ticketsPromotionReportData.totalCount;     
        this.planDayResultticketsPromotionReportDatatotalSum = result.ticketsPromotionReportData.totalSum;
        this.planDayResultitemsData = result.itemsData;
        this.planDayResultitemsDatatotalPaidItems = result.itemsData.totalPaidItems;
        this.planDayResultitemsDatatotalPaidItemTotal = result.itemsData.totalPaidItemTotal;
        this.planDayResultitemsDataitemCount = result.itemsData.itemCount;
        this.planDayResultordersReportDatatotalPaidItemsorderCount = result.ordersReportData.totalPaidItemsorderCount;
        this.planDayResultordersReportDatatotalPaidItemsorderValue = result.ordersReportData.totalPaidItemsorderValue;
        this.planDayResultordersReportDataorderCount = result.ordersReportData.orderCount;
        this.planDayResultordersReportDataorderStateReports = result.ordersReportData.orderStateReports;
        this.planDayResultticketsReportDataticketStateReports = result.ticketsReportData.ticketStateReports;
        this.planDayResultticketsReportDatafirstTicketNumber = result.ticketsReportData.firstTicketNumber;
        this.planDayResultticketsReportDatalastTicketNumber = result.ticketsReportData.lastTicketNumber;
        this.planDayResultticketTagsReportData = result.ticketTagsReportData;
        this.planDayResultownersReportDatadetails = result.ownersReportData.details;
        this.planDayResultownersReportDatatotalOwnersAmount = result.ownersReportData.totalOwnersAmount;
        this.planDayResultusersIncomeReportData = result.usersIncomeReportData;
      });
  }
  
  exportToExcel(exportOutputType: ExportType): void {
    var input = new CentralExportInput();
    input.exportOutputType = exportOutputType;
    input.workPeriodId = this.dataTranfer.wid,
    input.exportOutputType = exportOutputType,
    input.dateReport =  this.dataTranfer.reportStartDay,
    input.runInBackground = false;
    this._planDayReportServiceProxy
      .buildCentralPlanDayExport(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
  sum = function (items, prop) {
    if (items == null) {
        return 0;
    }
    return items.reduce(function (a:any, b:any) {
        return b[prop] == null ? +a : +a + +b[prop];
    },
        0);
};
}

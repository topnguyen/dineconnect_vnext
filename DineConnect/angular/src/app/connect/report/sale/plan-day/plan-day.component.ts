import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, ExportType,TenantSettingsServiceProxy, GetItemInput, IdNameDto, PlanDayReportServiceProxy, SimpleLocationDto, WorkPeriodSummaryOutput, WorkPeriodSummaryInputWithPaging } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DayReportComponent } from './day-report/day-report.component';
import { ShiftReportComponent } from './shift-report/shift-report.component';
import { TerminalReportComponent } from './terminal-report/terminal-report.component';
import _ from 'lodash';
@Component({
  selector: 'app-plan-day',
  templateUrl: './plan-day.component.html',
  styleUrls: ['./plan-day.component.css'],
  animations: [appModuleAnimation()]
})
export class PlanDayComponent extends AppComponentBase implements OnInit {
  @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
  @ViewChild('dayReportModal', { static: false }) dayReportModal: DayReportComponent;
  @ViewChild('shiftReportModal', { static: false }) shiftReportModal: ShiftReportComponent;
  @ViewChild('terminalReportModal', { static: false }) terminalReportModal: TerminalReportComponent;
  locationOrGroup = new CommonLocationGroupDto();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  requestParams: WorkPeriodSummaryInputWithPaging = {
    exportOutputType: ExportType.Excel
  } as WorkPeriodSummaryInputWithPaging;
  loading = false;
  disableExport = true;
  ExportType = ExportType;
  data: any;
  planDayResult: WorkPeriodSummaryOutput[]=[];
   listCashSummary : any;
  totalAmount: number;
  constructor( injector: Injector,
    private _planDayReportServiceProxy: PlanDayReportServiceProxy ,
    private _fileDownloadService: FileDownloadService,
    private _settingService: TenantSettingsServiceProxy,
    private render: Renderer2,) {  
      super(injector);
  }
  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }
  getData() {
    this.requestParams.locationGroup = this.locationOrGroup;

    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;

    this._planDayReportServiceProxy
      .buildWorkPeriodByLocationForDatesWithPaging(
        this.requestParams
      )
      .subscribe((result) => {
        console.log("data result", result);
        this.planDayResult = result;
      });
  }
  showedDateRangePicker(): void {
    let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
    let self = this;
    let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
    self.render.removeClass(el, 'show');

    button.addEventListener('click', (event) => {
      event.preventDefault();

      self.render.addClass(el, 'show');
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    var input = new WorkPeriodSummaryInputWithPaging();
    input.init(this.requestParams);
    input.exportOutputType = exportOutputType;

    input.locationGroup = this.locationOrGroup;
    input.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf('day') : undefined;
    input.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf('day') : undefined;

    this._planDayReportServiceProxy
      .buildWorkPeriodByLocationForDatesWithPagingToExcel(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }

  showDayReport(record) {
    console.log('showDetailsDay', record);
    this.dayReportModal.show(record);
}
showShiftReport(record) {
  console.log('showDetailsShift', record);
  this.shiftReportModal.show(record);
}
showTerminalReport(record) {
  console.log('showDetailsTerminal', record);
  this.terminalReportModal.show(record);
}
  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }
}

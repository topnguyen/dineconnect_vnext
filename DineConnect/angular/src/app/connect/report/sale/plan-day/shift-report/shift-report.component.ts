import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CentralShiftExportInput,WorkShiftDto,TenantSettingsServiceProxy,ExportType,CentralShiftReportInput, CentralExportInput,PaymentTypeByWorkTimeRowData,CentralDayReportData,WorkPeriodSummaryOutput, PlanDayReportServiceProxy,CentralTerminalReportInput,TicketTypeReportData,WorkTimePaTable,UserInfoReportData 
 } from '@shared/service-proxies/service-proxies';
 import { FileDownloadService } from '@shared/utils/file-download.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { NotifyService } from "abp-ng2-module";
@Component({
  selector: 'app-shift-report',
  templateUrl: './shift-report.component.html',
  styleUrls: ['./shift-report.component.css']
})
export class ShiftReportComponent  extends AppComponentBase implements OnInit{

  @ViewChild('modal', { static: true }) modal: ModalDirective;
  isShown = false;
  loading = false;
  ExportType = ExportType;
  workPeriodId: number;
  uniqueId:any;
  workShifts:WorkShiftDto[]=[];
  dataTranferShift = new WorkPeriodSummaryOutput();
  planShiftResult = new CentralDayReportData();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  constructor(injector: Injector,
    private _planDayReportServiceProxy: PlanDayReportServiceProxy,
    private _settingService: TenantSettingsServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _notifyService: NotifyService,

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }
  planShiftResultticketTypeReportDatasubTotal:any;
  planShiftResultticketTypeReportDatatax:any;
  planShiftResultticketTypeReportDatatotalSales:any;
  planShiftResultticketTypeReportDataticketCount:any;
  planShiftResultticketTypeReportDataaverage:any;
  planShiftResultticketTypeReportDatatransactions:any;
  planShiftResultdepartments:any;
  planShiftResultpaymentTypeReportDatadetails:any;
  planShiftResultcashReportData:any;
  planShiftResultworkTimePaTabledetails:PaymentTypeByWorkTimeRowData[] =[];
  planShiftResultticketsPromotionReportDatadetails:any;
  planShiftResultticketsPromotionReportDatatotalCount:any;
  planShiftResultticketsPromotionReportDatatotalSum:any;
  planShiftResultitemsData:any;
  planShiftResultitemsDatatotalPaidItems:any;
  planShiftResultitemsDatatotalPaidItemTotal:any;
  planShiftResultitemsDataitemCount:any;
  planShiftResultordersReportDatatotalPaidItemsorderCount:any;
  planShiftResultordersReportDatatotalPaidItemsorderValue:any;
  planShiftResultordersReportDataorderCount:any;
  planShiftResultordersReportDataorderStateReports:any;
  planShiftResultticketsReportDataticketStateReports:any;
  planShiftResultticketsReportDatafirstTicketNumber:any;
  planShiftResultticketsReportDatalastTicketNumber:any;
  planShiftResultticketTagsReportData:any;
  planShiftResultownersReportDatadetails:any;
  planShiftResultownersReportDatatotalOwnersAmount:any;
  planShiftResultusersIncomeReportData:any;

  planShiftResultcashReportDatacashIn:any;
  planShiftResultcashReportDataallIncomeAmount:any;
  planShiftResultcashReportDatacashOut:any;
  planShiftResultcashReportDataallExpAmount:any;
  planShiftResultcashReportDatacashInDrawer:any;
  disableExport = true;
  selectedItemShift:any;
  show(data:any) {
    this.dataTranferShift = data;
    this.workPeriodId = data.wid;
    this.workShifts = this.dataTranferShift.workShifts;
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }
  selectWorkShiftChange(event)
  {
    this.selectedItemShift = event.target.value;
  }
  getShiftDayData() {
    this.loading = true;
   var input = new CentralShiftReportInput();
   input.workPeriodId = this.dataTranferShift.wid;
   input.workShiftId = this.selectedItemShift;
   if(input.workShiftId == undefined)
   {
    this.message.error('Must Select WorkShift','Notification');
    return;
   }
    this._planDayReportServiceProxy.buildCentralShiftReport(input)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe((result) => {
        this.planShiftResult = result;
        this.planShiftResultticketTypeReportDatasubTotal = this.planShiftResult.ticketTypeReportData.subTotal;
        this.planShiftResultticketTypeReportDatatax = result.ticketTypeReportData.tax;
        this.planShiftResultticketTypeReportDatatotalSales= result.ticketTypeReportData.totalSales;
        this.planShiftResultticketTypeReportDataticketCount= result.ticketTypeReportData.ticketCount;
        this.planShiftResultticketTypeReportDataaverage= result.ticketTypeReportData.average;
        this.planShiftResultticketTypeReportDatatransactions = result.ticketTypeReportData.transactions;
        this.planShiftResultdepartments = result.departments; 
        this.planShiftResultpaymentTypeReportDatadetails = result.paymentTypeReportData.details;
        this.planShiftResultcashReportData = result.cashReportData;
        this.planShiftResultworkTimePaTabledetails = result.workTimePaTable.details;
        this.planShiftResultticketsPromotionReportDatadetails = result.ticketsPromotionReportData.details;
        this.planShiftResultticketsPromotionReportDatatotalCount = result.ticketsPromotionReportData.totalCount;     
        this.planShiftResultticketsPromotionReportDatatotalSum = result.ticketsPromotionReportData.totalSum;
        this.planShiftResultitemsData = result.itemsData;
        this.planShiftResultitemsDatatotalPaidItems = result.itemsData.totalPaidItems;
        this.planShiftResultitemsDatatotalPaidItemTotal = result.itemsData.totalPaidItemTotal;
        this.planShiftResultitemsDataitemCount = result.itemsData.itemCount;
        this.planShiftResultordersReportDatatotalPaidItemsorderCount = result.ordersReportData.totalPaidItemsorderCount;
        this.planShiftResultordersReportDatatotalPaidItemsorderValue = result.ordersReportData.totalPaidItemsorderValue;
        this.planShiftResultordersReportDataorderCount = result.ordersReportData.orderCount;
        this.planShiftResultordersReportDataorderStateReports = result.ordersReportData.orderStateReports;
        this.planShiftResultticketsReportDataticketStateReports = result.ticketsReportData.ticketStateReports;
        this.planShiftResultticketsReportDatafirstTicketNumber = result.ticketsReportData.firstTicketNumber;
        this.planShiftResultticketsReportDatalastTicketNumber = result.ticketsReportData.lastTicketNumber;
        this.planShiftResultticketTagsReportData = result.ticketTagsReportData;
        this.planShiftResultownersReportDatadetails = result.ownersReportData.details;
        this.planShiftResultownersReportDatatotalOwnersAmount = result.ownersReportData.totalOwnersAmount;
        this.planShiftResultusersIncomeReportData = result.usersIncomeReportData;
        this.planShiftResultcashReportDatacashIn = result.cashReportData.cashIn;
        this.planShiftResultcashReportDataallIncomeAmount= result.cashReportData.allIncomeAmount;
        this.planShiftResultcashReportDatacashOut= result.cashReportData.cashOut;
        this.planShiftResultcashReportDataallExpAmount= result.cashReportData.allExpAmount;
        this.planShiftResultcashReportDatacashInDrawer= result.cashReportData.cashInDrawer;
      });
  }
  sum = function (items, prop) {
    if (items == null) {
        return 0;
    }
    return items.reduce(function (a:any, b:any) {
        return b[prop] == null ? +a : +a + +b[prop];
    },
        0);
};
  exportToExcel(exportOutputType: ExportType): void {
    
    var input = new CentralShiftExportInput();
    var inputShift = new CentralShiftReportInput();
    var wId = this.workPeriodId;
    inputShift.workPeriodId = wId;
    inputShift.workShiftId = this.selectedItemShift;
    if(inputShift.workShiftId == undefined)
    {
     this.message.error('Must Select WorkShift','Notification');
     return;
    }
    input.centralShiftReportInput = inputShift;
    input.workPeriod = this.dataTranferShift;
    input.dateReport = this.dataTranferShift.reportStartDay;
    input.exportOutputType = exportOutputType;
    this._planDayReportServiceProxy
      .buildCentralShiftExport(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashSummaryReportComponent } from './cash-summary-report.component';

describe('CashSummaryReportComponent', () => {
  let component: CashSummaryReportComponent;
  let fixture: ComponentFixture<CashSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

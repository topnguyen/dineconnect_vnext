import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, ExportType, GetItemInput, IdNameDto, CashSummaryReportServiceProxy, SimpleLocationDto, GetCashSummaryReportOutput, GetCashSummaryReportByDate, GetCashSummaryReportByItem, GetCashSummaryReportByPlant, GetCashSummaryReportByShift, GetCashSummaryReportInput } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import _ from 'lodash';
@Component({
  selector: 'app-cash-summary-report',
  templateUrl: './cash-summary-report.component.html',
  styleUrls: ['./cash-summary-report.component.css'],
  animations: [appModuleAnimation()]
})
export class CashSummaryReportComponent extends AppComponentBase implements OnInit {
  @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  requestParams: GetCashSummaryReportInput = {
    exportOutputType: ExportType.Excel
  } as GetCashSummaryReportInput;
  loading = false;
  disableExport = true;
  ExportType = ExportType;
  data: any;
  CashSummaryResult: GetCashSummaryReportOutput;
   listCashSummary : any;
  totalAmount: number;
  constructor(
    injector: Injector,
    private _CashSummaryReportServiceProxy: CashSummaryReportServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() {

  }

  getData() {
    this.requestParams.locationGroup = this.locationOrGroup;

    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;

    this._CashSummaryReportServiceProxy
      .buildCashSummaryReport(
        this.requestParams
      )
      .subscribe((result) => {
        console.log("data result", result);
        this.CashSummaryResult = result;
        this.listCashSummary = result.listItem;
        this.totalAmount = result.totalAmount;
      });
  }
  showedDateRangePicker(): void {
    let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
    let self = this;
    let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
    self.render.removeClass(el, 'show');

    button.addEventListener('click', (event) => {
      event.preventDefault();

      self.render.addClass(el, 'show');
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    var input = new GetCashSummaryReportInput();
    input.init(this.requestParams);
    input.exportOutputType = exportOutputType;

    input.locationGroup = this.locationOrGroup;
    input.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf('day') : undefined;
    input.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf('day') : undefined;

    this._CashSummaryReportServiceProxy
      .buildCashSummaryReportToExport(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }


  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }
}

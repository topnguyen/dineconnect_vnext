import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollecttionReportComponent } from './collecttion-report.component';

describe('CollecttionReportComponent', () => {
  let component: CollecttionReportComponent;
  let fixture: ComponentFixture<CollecttionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollecttionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollecttionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

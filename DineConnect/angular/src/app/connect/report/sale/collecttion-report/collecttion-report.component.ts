import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ConnectLookupServiceProxy,
  ExportType,
  OrderReportServiceProxy,
  MenuItemServiceProxy,
  GetItemInput,GetTicketInput
} from "@shared/service-proxies/service-proxies";
import {
CollectionReportServiceProxy,TicketTypeReportData
} from "@shared/service-proxies/service-proxies-nswag";
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent, InputMaskModule } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
  selector: 'app-collecttion-report',
  templateUrl: './collecttion-report.component.html',
  styleUrls: ['./collecttion-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})
export class CollecttionReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;

  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  totalAmount;
  totalOrders;
  totalItems;
  requestParams: GetTicketInput = {
      exportOutputType: ExportType.Excel,
  } as GetTicketInput;
  query: any;
  config: QueryBuilderConfig = {
      fields: {},
  };
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;

  constructor(
      injector: Injector,
      private _collectionReportServiceProxy: CollectionReportServiceProxy,
      private _fileDownloadService: FileDownloadService,
      private render: Renderer2,
  ) {
      super(injector);
  }

  ngOnInit() {
  }

  getCollection(event?: LazyLoadEvent) {
      if (this.primengTableHelper.shouldResetPaging(event)) {
          this.paginator.changePage(0);
          return;
      }
      this.primengTableHelper.showLoadingIndicator();

      this.requestParams.locationGroup = this.locationOrGroup;

      this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

      this.requestParams.sorting = this.primengTableHelper.getSorting(
          this.dataTable
      );
      this.requestParams.maxResultCount =
          this.primengTableHelper.getMaxResultCount(this.paginator, event);
      this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
          this.paginator,
          event
      );

      if (this.query) {
          this.requestParams.dynamicFilter = JSON.stringify(
              this.mapQuery(this.query, this.config)
          );
      }

      var input = new GetTicketInput();
      input.init(this.requestParams);
      input.terminalName = this.terminalName;

      this._collectionReportServiceProxy
          .buildCollectionReport(input)
          .pipe(
              finalize(() => this.primengTableHelper.hideLoadingIndicator())
          )
          .subscribe((result) => {
              this.primengTableHelper.totalRecordsCount =
                  result.totalCount;
              this.primengTableHelper.records = result.items;
          });
  }

  showedDateRangePicker(): void {
      let button = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-predefined-btns"
      ).lastElementChild;
      let self = this;
      let el = document.querySelector(
          "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
      );
      self.render.removeClass(el, "show");

      button.addEventListener("click", (event) => {
          event.preventDefault();

          self.render.addClass(el, "show");
      });
  }

  reloadPage(): void {
      this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
      var input = new GetTicketInput();
      input.init(this.requestParams);
      input.terminalName = this.terminalName;
      input.exportOutputType = exportOutputType;

      this._collectionReportServiceProxy
          .buildCollectionReportForExport(input)
          .subscribe((result) => {
              this._fileDownloadService.downloadTempFile(result);
          });
  }

  openSelectLocationModal() {
      this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
      this.locationOrGroup = event;
  }
  clear() {
      this.dateRangeModel = [new Date(), new Date()];
      this.requestParams = {
          exportOutputType: ExportType.Excel,
      } as GetTicketInput;
      this.query = null;
      this.getCollection();
  }

  get getAggregationitemSalesValue() {
      return sumBy(this.primengTableHelper.records, (o) => {
          return o.itemSales;
      });
  }
  get getAggregationticketDiscountTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.ticketDiscountTotal;
    });
}
get getAggregationitemDiscountTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.itemDiscountTotal;
    });
}
get getAggregationsubTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.subTotal;
    });
}
get getAggregationtaxValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.tax;
    });
}
get getAggregationtotalSalesValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
        return o.totalSales;
    });
}
}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleItemsComponent } from './schedule-items.component';

describe('ScheduleItemsComponent', () => {
  let component: ScheduleItemsComponent;
  let fixture: ComponentFixture<ScheduleItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

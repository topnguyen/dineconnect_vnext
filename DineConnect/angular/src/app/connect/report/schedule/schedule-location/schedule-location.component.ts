import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto,MenuItemServiceProxy,PagedResultDtoOfMenuItemListDto, ExportType, GetItemInput, IdNameDto, SimpleLocationDto, GetCashSummaryReportOutput, GetCashSummaryReportByDate, GetCashSummaryReportByItem, GetCashSummaryReportByPlant, GetCashSummaryReportByShift, GetCashSummaryReportInput } from '@shared/service-proxies/service-proxies';
import {ScheduleAllList ,ScheduleReportOutput,ScheduleSalesLocationReportServiceProxy} from '@shared/service-proxies/service-proxies-nswag';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {MessageService} from 'abp-ng2-module';
import _ from 'lodash';
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { map, sumBy, unionBy } from "lodash";
import { Observable } from "rxjs";
@Component({
  selector: 'app-schedule-location',
  templateUrl: './schedule-location.component.html',
  styleUrls: ['./schedule-location.component.css'],
  animations: [appModuleAnimation()]
})
export class ScheduleLocationComponent extends AppComponentBase implements OnInit {
  @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('drp', { static: false }) daterangePicker: ElementRef;
  @ViewChild("selectMenuItemPopupModal", { static: true }) selectMenuItemPopupModal: SelectPopupComponent;
  locationOrGroup = new CommonLocationGroupDto({
    locations: [],
      groups: [],
      locationTags: [],
      nonLocations: [],
      group: false,
      locationTag: false,
      userId: 0
  });
  selectedMenuItems = [];
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  requestParams: GetItemInput = {
    exportOutputType: ExportType.Excel
  } as GetItemInput;
  loading = false;
  disableExport = true;
  ExportType = ExportType;
  data: any;
  scheduleItemResult = new ScheduleAllList();
   listCashSummary : any;
  totalAmount: number;
  constructor(
    injector: Injector,
    private _scheduleService: ScheduleSalesLocationReportServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _messageService: MessageService,
    private _menuItemServiceProxy: MenuItemServiceProxy,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() {
  }
  clear()
  {
    this.dateRangeModel = [new Date(), new Date()];
    this.requestParams = {
        exportOutputType: ExportType.Excel,
    } as GetItemInput;
    this.scheduleItemResult = new ScheduleAllList();
    this.locationOrGroup = new CommonLocationGroupDto({
      locations: [],
        groups: [],
        locationTags: [],
        nonLocations: [],
        group: false,
        locationTag: false,
        userId: 0
    });
  }
  openforMenuItem() {
    this.selectMenuItemPopupModal.show(this.selectedMenuItems);
}
resetSelectedItem() {
    this.selectedMenuItems = [];
}
addMenuItems(itemSelect) {
  this.selectedMenuItems = itemSelect;
}
getMenuItemSelection() {
  return (
      filterText: string,
      sorting: string,
      maxResultCount: number,
      skipCount: number
  ): Observable<PagedResultDtoOfMenuItemListDto> =>
      this._menuItemServiceProxy.getAll(
          filterText,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          sorting,
          skipCount,
          maxResultCount
      );
}

  getData() {
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    let ids: number[] =[];
    this.selectedMenuItems.map((item) => {
      ids.push(+item.id);
    });
    this.requestParams.menuItemIds = ids;
    if(this.locationOrGroup?.locations?.length == 0) {
      this._messageService.error("Must Select Location");
      return;
    }
    this._scheduleService
      .buildScheduleLocationReport(
        this.requestParams
      )
      .subscribe((result) => {
        
        this.scheduleItemResult = result;
        console.log("data result", result);
        console.log("data result 0", result.listResult[0]);
        console.log("data result schedule", result.listSchedule);
      });
  }
  showedDateRangePicker(): void {
    let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
    let self = this;
    let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
    self.render.removeClass(el, 'show');

    button.addEventListener('click', (event) => {
      event.preventDefault();

      self.render.addClass(el, 'show');
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    var input = new GetItemInput();
    input.init(this.requestParams);
    input.exportOutputType = exportOutputType;
    let ids: number[] =[];
    this.selectedMenuItems.map((item) => {
      ids.push(+item.id);
    });
    this.requestParams.menuItemIds = ids;
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    if(this.locationOrGroup?.locations?.length == 0) {
      this._messageService.error("Must Select Location");
      return;
    }
    this._scheduleService
      .buildLocationScheduleReportExcel(
        input
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }

  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }
}



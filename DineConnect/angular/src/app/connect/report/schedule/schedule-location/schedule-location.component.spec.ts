import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleLocationComponent } from './schedule-location.component';

describe('ScheduleLocationComponent', () => {
  let component: ScheduleLocationComponent;
  let fixture: ComponentFixture<ScheduleLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentExcessReportComponent } from './payment-excess-report.component';

describe('PaymentExcessReportComponent', () => {
  let component: PaymentExcessReportComponent;
  let fixture: ComponentFixture<PaymentExcessReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentExcessReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentExcessReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

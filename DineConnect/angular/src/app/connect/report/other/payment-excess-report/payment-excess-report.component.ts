import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ExportType,
  OrderTagGroupServiceProxy,
  GetOrderTagReportInput,
  PaymentExcessReportServiceProxy,
  PaymentExcessDetailDto,PaymentExcessSummaryDto,
  ListResultDtoOfComboboxItemDto,
  ComboboxItemDto,
  GetTicketInput,PaymentTypesServiceProxy} from "@shared/service-proxies/service-proxies-nswag";
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { LogDetailComponent } from "../../other/log-report/log-detail/log-detail.component";
import { SelectItem } from "primeng/api";

@Component({
  selector: 'app-payment-excess-report',
  templateUrl: './payment-excess-report.component.html',
  styleUrls: ['./payment-excess-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})

export class PaymentExcessReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;
  orderTagList = [];
  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  userName: any;
  inComplete:any;
  paymentTags:any;
  paymentTypes = [];
  selectedEvent= [];
  listAccountId:number[];
  requestParams: GetTicketInput = {
    exportOutputType: ExportType.Excel,
  } as GetTicketInput;
  query: any;
  config: QueryBuilderConfig = {
    fields: {},
  };
  comboAccInput: ComboboxItemDto[];
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  lookupName: string;
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;
  listEvent :ComboboxItemDto[];
  constructor(
    injector: Injector,
    private _paymentExcessReportServiceProxy: PaymentExcessReportServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() {
  }


  getOrders(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();

    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.sorting = this.primengTableHelper.getSorting(
      this.dataTable
    );
    this.requestParams.paymentTags = [];
    if(this.paymentTags != undefined)
    {
      this.requestParams.paymentTags.push(this.paymentTags);
    }
    
    this.requestParams.payments = [];
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });
    this.requestParams.maxResultCount =
      this.primengTableHelper.getMaxResultCount(this.paginator, event);
    this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );

    if (this.query) {
      this.requestParams.dynamicFilter = JSON.stringify(
        this.mapQuery(this.query, this.config)
      );
    }
    this._paymentExcessReportServiceProxy
      .buildPaymentExcessSummary(this.requestParams)
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        console.log("data test", result);
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  showedDateRangePicker(): void {
    let button = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-predefined-btns"
    ).lastElementChild;
    let self = this;
    let el = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
    );
    self.render.removeClass(el, "show");

    button.addEventListener("click", (event) => {
      event.preventDefault();

      self.render.addClass(el, "show");
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }
  exportToExcel(exportOutputType: ExportType): void {
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;
    this.requestParams.paymentTags = [];
    if(this.paymentTags!= undefined)
    {
      this.requestParams.paymentTags.push(this.paymentTags);
    }
    this.requestParams.payments = [];
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });
    this._paymentExcessReportServiceProxy
      .buildPaymentExcessExport(this.requestParams)
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
  exportToExcelDetail(exportOutputType: ExportType): void {
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;
    
    this.requestParams.paymentTags = [];
    if(this.paymentTags != undefined )
    {
      this.requestParams.paymentTags.push(this.paymentTags);
    }
    this.requestParams.payments = [];
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });
    this._paymentExcessReportServiceProxy
      .buildPaymentExcessDetailExport(this.requestParams)
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }

  clear() {
    this.dateRangeModel = [new Date(), new Date()];
    this.requestParams = {
      exportOutputType: ExportType.Excel,
    } as GetTicketInput;
    this.query = null;
    this.getOrders();
  }

  get getAggregationActualValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.actual;
    });
  }
  get getAggregationTenderValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.tendered;
    });
  }
  get getAggregationExcessValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.excess;
    });
  }
}

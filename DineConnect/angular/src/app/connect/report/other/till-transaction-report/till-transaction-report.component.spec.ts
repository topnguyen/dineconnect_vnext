import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TillTransactionReportComponent } from './till-transaction-report.component';

describe('TillTransactionReportComponent', () => {
  let component: TillTransactionReportComponent;
  let fixture: ComponentFixture<TillTransactionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TillTransactionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TillTransactionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

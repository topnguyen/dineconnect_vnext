import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ExportType,
  OrderTagGroupServiceProxy,
  GetOrderTagReportInput,
  PlanLoggerServiceProxy,
  GetPlanLoggerInput,
  ListResultDtoOfComboboxItemDto,
  ComboboxItemDto
} from "@shared/service-proxies/service-proxies-nswag";
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { LogDetailComponent } from "../../other/log-report/log-detail/log-detail.component";
import { SelectItem } from "primeng/api";

@Component({
  selector: 'app-log-report',
  templateUrl: './log-report.component.html',
  styleUrls: ['./log-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})

export class LogReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;
  @ViewChild("detailmodal", { static: false })detailmodal: LogDetailComponent;
  orderTagList = [];
  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  userName: any;
  inComplete:any;
  selectedEvent= [];
  listAccountId:number[];
  requestParams: GetPlanLoggerInput = {
    exportOutputType: ExportType.Excel,
  } as GetPlanLoggerInput;
  query: any;
  config: QueryBuilderConfig = {
    fields: {},
  };
  comboAccInput: ComboboxItemDto[];
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;
  listEvent :ComboboxItemDto[];
  constructor(
    injector: Injector,
    private _planLoggerServiceProxy: PlanLoggerServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getEvent();
  }

  getEvent()
  {
    this._planLoggerServiceProxy.getLogTypes()
      .subscribe((result) => {
        console.log("data event", result);
        this.listEvent = result.items;
      });
  }
  getOrders(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();

    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.events = this.selectedEvent;
    this.requestParams.unfinished = this.inComplete;
    this.requestParams.sorting = this.primengTableHelper.getSorting(
      this.dataTable
    );
    this.requestParams.maxResultCount =
      this.primengTableHelper.getMaxResultCount(this.paginator, event);
    this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );

    if (this.query) {
      this.requestParams.dynamicFilter = JSON.stringify(
        this.mapQuery(this.query, this.config)
      );
    }
    this._planLoggerServiceProxy
      .buildGetAll(this.requestParams)
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        console.log("data test", result);
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  showedDateRangePicker(): void {
    let button = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-predefined-btns"
    ).lastElementChild;
    let self = this;
    let el = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
    );
    self.render.removeClass(el, "show");

    button.addEventListener("click", (event) => {
      event.preventDefault();

      self.render.addClass(el, "show");
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;
    this.requestParams.events = this.selectedEvent;
    this.requestParams.unfinished = this.inComplete;
    this._planLoggerServiceProxy
      .buildGetExcel(this.requestParams)
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }

  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }

  showDetails(record) {
    this.detailmodal.show(record);
  }

  clear() {
    this.dateRangeModel = [new Date(), new Date()];
    this.requestParams = {
      exportOutputType: ExportType.Excel,
    } as GetPlanLoggerInput;
    this.query = null;
    this.getOrders();
  }

  get getAggregationTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.ticketTotal;
    });
  }
}
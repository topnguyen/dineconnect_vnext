import { DatePipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TenantSettingsServiceProxy, PlanLoggerServiceProxy,TimeLineOutput , EventOutput,NameValueDto} from '@shared/service-proxies/service-proxies-nswag';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-log-detail',
  templateUrl: './log-detail.component.html',
  styleUrls: ['./log-detail.component.css']
})
export class LogDetailComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;

  events: EventOutput[];
  bgColor:'#673AB7';
  log;
  pairs: NameValueDto[];
  icon ='pi pi-check';
  isShown = false;
  loading = false;
  logId: number;
  logResult = new TimeLineOutput();
  dateTimeFormat = 'dd-MM-yyyy hh:mm:ss';
  constructor(injector: Injector,
    private _planLoggerServiceProxy: PlanLoggerServiceProxy,
    private _settingService: TenantSettingsServiceProxy

  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._settingService.getAllSettings().subscribe(result => {
      this.dateTimeFormat = result.fileDateTimeFormat.fileDateTimeFormat;
    })
  }

  show(event:any) {
    this.logId = event.id;
    this.log = event;
    this.getOrders();
    this.modal.show();
  }

  shown() {
    this.isShown = true;
  }

  close() {
    this.loading = false;
    this.modal.hide();
  }

  getOrders() {
    this.loading = true;
    this._planLoggerServiceProxy.buildTimelineLogs(this.logId)
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe((result) => {
        this.events = result.eventOutputs;
        this.pairs = result.descriptions;
        console.log("events",this.events);
        console.log("pairs",this.pairs);
      });
  }
}

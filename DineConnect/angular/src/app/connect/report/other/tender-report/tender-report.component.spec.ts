import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenderReportComponent } from './tender-report.component';

describe('TenderReportComponent', () => {
  let component: TenderReportComponent;
  let fixture: ComponentFixture<TenderReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenderReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenderReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

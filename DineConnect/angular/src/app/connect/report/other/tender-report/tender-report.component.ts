import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
  Renderer2,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  CommonLocationGroupDto,
  PromotionListDto,
  SimpleLocationDto,
  IdNameDto,
  ExportType,
  OrderTagGroupServiceProxy,
  GetOrderTagReportInput,
  TenderReportServiceProxy,
  GetPlanLoggerInput,
  ListResultDtoOfComboboxItemDto,
  ComboboxItemDto,
  GetTicketInput} from "@shared/service-proxies/service-proxies-nswag";
  import {PaymentTypesServiceProxy} from "@shared/service-proxies/service-proxies";
import { CommonMultipleLookupModalComponent } from '@app/shared/common/lookup/common-multiple-lookup-modal.component';
import * as moment from "moment";
import { sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import {
  QueryBuilderComponent,
  QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { LogDetailComponent } from "../../other/log-report/log-detail/log-detail.component";
import { SelectItem } from "primeng/api";

@Component({
  selector: 'app-tender-report',
  templateUrl: './tender-report.component.html',
  styleUrls: ['./tender-report.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
})

export class TenderReportComponent extends AppComponentBase implements OnInit {
  @ViewChild("commonMultipleLookupModal", { static: true }) commonMultipleLookupModal: CommonMultipleLookupModalComponent;
  @ViewChild("selectLocationOrGroup", { static: true })
  selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
  @ViewChild("queryBuilder", { static: false })
  queryBuilder: QueryBuilderComponent;
  orderTagList = [];
  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  userName: any;
  inComplete:any;
  paymentTypes = [];
  selectedEvent= [];
  listAccountId:number[];
  requestParams: GetTicketInput = {
    exportOutputType: ExportType.Excel,
  } as GetTicketInput;
  query: any;
  config: QueryBuilderConfig = {
    fields: {},
  };
  comboAccInput: ComboboxItemDto[];
  decimals = 2;
  loading = false;
  menuItems = [];
  departments = [];
  lookupName: string;
  disableExport = true;
  ExportType = ExportType;
  advancedFiltersAreShown = false;
  terminalName: string;
  listEvent :ComboboxItemDto[];
  constructor(
    injector: Injector,
    private _tenderReportServiceProxy: TenderReportServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _paymentTypeServiceProxy: PaymentTypesServiceProxy,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() {
  }

  showLookupModal(name) {
    this.lookupName = name;
    switch (name) {      
      case "Payment":
        this.commonMultipleLookupModal.configure({
          title: this.l("Select") + " " + this.l("Payments"),
          dataSource: (
            skipCount: number,
            maxResultCount: number,
            filter: string
          ) => {
            return this._paymentTypeServiceProxy.getPaymentTypeForLookupTable(
              filter,
              "name",
              skipCount,
              maxResultCount
            );
          },
        });

        this.commonMultipleLookupModal.show(this.paymentTypes);
        break;
      default:
        break;
    }
  }

  getOrders(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();

    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.sorting = this.primengTableHelper.getSorting(
      this.dataTable
    );
    this.requestParams.payments = [];
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });
    this.requestParams.maxResultCount =
      this.primengTableHelper.getMaxResultCount(this.paginator, event);
    this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );

    if (this.query) {
      this.requestParams.dynamicFilter = JSON.stringify(
        this.mapQuery(this.query, this.config)
      );
    }
    this._tenderReportServiceProxy
      .buildTenderDetails(this.requestParams)
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        console.log("data test", result);
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  showedDateRangePicker(): void {
    let button = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-predefined-btns"
    ).lastElementChild;
    let self = this;
    let el = document.querySelector(
      "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
    );
    self.render.removeClass(el, "show");

    button.addEventListener("click", (event) => {
      event.preventDefault();

      self.render.addClass(el, "show");
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }
  clearLookup(name) {
    switch (name) {
      case "Department":
        this.departments = [];
        break;
      case "Payment":
        this.paymentTypes = [];
        break;
      case "Transaction":
        break;
      default:
        break;
    }
  }
  exportToExcel(exportOutputType: ExportType): void {
    this.requestParams.locationGroup = this.locationOrGroup;
    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.exportOutputType = exportOutputType;
    this.requestParams.payments = [];
    this.paymentTypes.forEach((item) => {
      this.requestParams.payments.push(+item.value);
    });
    this._tenderReportServiceProxy
      .buildTenderDetailsExport(this.requestParams)
      .subscribe((result) => {
        this._fileDownloadService.downloadFromFilePath(result);
      });
  }
  itemsSelected(event) {
    switch (this.lookupName) {
      case "Department":
        this.departments = event;
        break;
      case "Payment":
        this.paymentTypes = event;

        break;
      case "Transaction":
        break;

      default:
        break;
    }
  }
  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }

  clear() {
    this.dateRangeModel = [new Date(), new Date()];
    this.requestParams = {
      exportOutputType: ExportType.Excel,
    } as GetTicketInput;
    this.query = null;
    this.getOrders();
  }

  get getAggregationSubTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.subTotal;
    });
  }
  get getAggregationTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.total;
    });
  }
  get getAggregationDiscountTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.discount;
    });
  }
  get getAggregationServicechargeTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.serviceCharge;
    });
  }
  get getAggregationVatTotalValue() {
    return sumBy(this.primengTableHelper.records, (o) => {
      return o.vat;
    });
  }
}
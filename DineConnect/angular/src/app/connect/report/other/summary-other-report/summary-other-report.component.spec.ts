import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryOtherReportComponent } from './summary-other-report.component';

describe('SummaryOtherReportComponent', () => {
  let component: SummaryOtherReportComponent;
  let fixture: ComponentFixture<SummaryOtherReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryOtherReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryOtherReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

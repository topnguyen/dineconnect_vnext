import {
    Component,
    Injector,
    ViewChild,
    Renderer2,
    ElementRef,
    OnInit,
} from "@angular/core";
import {
    CommonLocationGroupDto,
    ConnectLookupServiceProxy,
    ExportType,
    MenuItemServiceProxy,
    GetItemInput,
    ItemReportServiceProxy,
    PagedResultDtoOfCategoryDto,
    CategoryServiceProxy,
    PagedResultDtoOfMenuItemListDto,
    CommonLookupServiceProxy,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { map, sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent, TreeNode } from "primeng";
import {
    QueryBuilderComponent,
    QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { Observable } from "rxjs";
import { DecimalPipe } from "@angular/common";
import { ItemReportExportModalComponent } from "./item-report-export-modal/item-report-export-modal.component";

@Component({
    selector: "app-item-report",
    templateUrl: "./item-report.component.html",
    styleUrls: ["./item-report.component.css"],
    animations: [appModuleAnimation()],
})
export class ItemReportComponent extends AppComponentBase implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
    @ViewChild("queryBuilder", { static: false })
    queryBuilder: QueryBuilderComponent;
    @ViewChild("selectCategoryPopupModal", { static: true })
    selectCategoryPopupModal: SelectPopupComponent;
    @ViewChild("selectMenuItemPopupModal", { static: true })
    selectMenuItemPopupModal: SelectPopupComponent;
    @ViewChild("exportModal", { static: true })
    exportModal: ItemReportExportModalComponent;

    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel,
    } as GetItemInput;
    query: any;
    config: QueryBuilderConfig = {
        fields: {},
    };
    decimals = 2;
    loading = false;
    menuItems = [];
    departments = [];
    productGroups = [];
    disableExport = true;
    ExportType = ExportType;
    advancedFiltersAreShown = false;
    ticketTags = "";
    selectedDepartments = [];
    selectedProductGroups = [];
    selectedCategory = [];
    selectedMenuItems = [];
    topQtyData: any;
    topValueData = {};
    chartOption = {};
    treeData: TreeNode[];

    totalQuantity = 0;
    totalAmount = 0;

    isItemExporting = false;
    isItemComboExporting = false;
    isPriceExporting: boolean = false;
    isPortionExporting: boolean;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _itemReportServiceProxy: ItemReportServiceProxy,
        private _categoryService: CategoryServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _decimalPipe: DecimalPipe
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getComboBoxData();
        this._createBuilder();
        this.initChart();
    }

    initChart() {
        let self = this;
        this.chartOption = {
            legend: {
                position: "bottom",
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        let value =
                            data["datasets"][0]["data"][tooltipItem.index];
                        let percent = self.getPercentForChart(
                            data["datasets"][0]["data"],
                            +value
                        );
                        return (
                            data["labels"][tooltipItem.index] +
                            ": " +
                            value +
                            "(" +
                            percent +
                            "%)"
                        );
                    },
                },
            },
        };
    }

    getPercentForChart(datas: any[], value) {
        let total = datas.reduce((a, b) => a + b, 0);
        let percent = (value * 100) / total;
        return this._decimalPipe.transform(percent, "1.0-2", "en");
    }

    getComboBoxData() {
        forkJoin([
            this._menuItemServiceProxy.getMenuItemForComboBox(),
            this._connectLookupServiceProxy.getDepartments(),
            this._commonLookupServiceProxy.getProductGroupsForCombobox(""),
        ]).subscribe((results) => {
            this.menuItems = [];
            for (const key in results[0]) {
                if (Object.prototype.hasOwnProperty.call(results[0], key)) {
                    const element = results[0][key];

                    this.menuItems.push(...element);
                }
            }

            this.menuItems = unionBy(this.menuItems, "id").map((item) => {
                return {
                    value: item.value,
                    name: item.displayText,
                };
            });
            this.departments = results[1].items;
            this.productGroups = results[2].items;
        });
    }

    getOrders(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.setRepuestParams();
        this.requestParams.sorting = this.primengTableHelper.getSorting(
            this.dataTable
        );
        this.requestParams.maxResultCount =
            this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        this.requestParams.isSummary = true;

        this._itemReportServiceProxy
            .buildItemSalesReport(this.requestParams)
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount =
                    result.menuList.totalCount;
                this.primengTableHelper.records = result.menuList.items;
                this.totalAmount = result.totalResult;
                this.totalQuantity = result.totalQuantity;

                this.getDataForPieChart(result.quantities, 0);
                this.getDataForPieChart(result.totals, 1);
            });
    }

    private setRepuestParams() {
        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
        this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

        this.requestParams.dynamicFilter = JSON.stringify(this.query);
        this.requestParams.menuItemIds = [];
        this.requestParams.filterByCategory = [];
        this.requestParams.filterByDepartment = [];
        this.requestParams.filterByGroup = [];

        this.selectedMenuItems.map((item) => {
            this.requestParams.menuItemIds.push(+item.id);
        });
        this.selectedCategory.map((item) => {
            this.requestParams.filterByCategory.push(+item.id);
        });
        this.selectedDepartments.map((item) => {
            this.requestParams.filterByDepartment.push(item.displayText);
        });
        this.selectedProductGroups.map((item) => {
            this.requestParams.filterByGroup.push(+item.value);
        });

        if (this.query) {
            this.requestParams.dynamicFilter = JSON.stringify(
                this.mapQuery(this.query, this.config)
            );
        }
    }

    private getDataForPieChart(items, type) {
        if (items) {
            let data = {};
            let labels = [];
            let values = [];
            var coloR = [];

            items.forEach((element) => {
                labels.push(element.name);
                values.push(element.y);
                coloR.push(this.dynamicColors());
            });

            data = {
                labels: labels,
                datasets: [
                    {
                        data: values,
                        backgroundColor: coloR,
                    },
                ],
            };
            switch (type) {
                case 0:
                    this.topQtyData = data;
                    break;
                case 1:
                    this.topValueData = data;
                    break;

                default:
                    break;
            }
        }
    }

    private dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }

    isNode(node) {
        return node.node.children?.length;
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;
        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        self.render.removeClass(el, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        this.requestParams = {
            exportOutputType: ExportType.Excel,
        } as GetItemInput;
        this.query = null;
        this.getOrders();
    }

    openforMenuItem() {
        this.selectMenuItemPopupModal.show(this.selectedMenuItems);
    }
    resetSelectedItem() {
        this.selectedMenuItems = [];
    }

    selectCategory() {
        this.selectCategoryPopupModal.show(this.selectedCategory);
    }

    getCategorySelection() {
        return (
            filterText: string,
            sorting: string,
            maxResultCount: number,
            skipCount: number
        ): Observable<PagedResultDtoOfCategoryDto> =>
            this._categoryService.getAll(
                filterText,
                undefined,
                sorting,
                maxResultCount,
                skipCount
            );
    }

    getMenuItemSelection() {
        return (
            filterText: string,
            sorting: string,
            maxResultCount: number,
            skipCount: number
        ): Observable<PagedResultDtoOfMenuItemListDto> =>
            this._menuItemServiceProxy.getAll(
                filterText,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                sorting,
                skipCount,
                maxResultCount
            );
    }

    addMenuItems(itemSelect) {
        this.selectedMenuItems = itemSelect;
    }

    addCategory(itemSelect) {
        this.selectedCategory = itemSelect;
    }

    exportPrice() {
        this.isPriceExporting = true;
        this.setRepuestParams();
        if (!this.requestParams.runInBackground) {
            this._itemReportServiceProxy
                .exportItemSalesByPriceToExcel(this.requestParams)
                .pipe(finalize(() => (this.isPriceExporting = false)))
                .subscribe((result) => {
                    this._fileDownloadService.downloadTempFile(result);
                });
        }
    }
    exportPortion() {
        this.isPortionExporting = true;
        this.setRepuestParams();
        if (!this.requestParams.runInBackground) {
            this._itemReportServiceProxy
                .exportItemsPortionToExcel(this.requestParams)
                .pipe(finalize(() => (this.isPortionExporting = false)))
                .subscribe((result) => {
                    this._fileDownloadService.downloadTempFile(result);
                });
        }
    }

    exportItem() {
        this.isItemExporting = true;
        this.setRepuestParams();
        if (!this.requestParams.runInBackground) {
            this._itemReportServiceProxy
                .exportItemsAndOrderTagToExcel(this.requestParams)
                .pipe(finalize(() => (this.isItemExporting = false)))
                .subscribe((result) => {
                    this._fileDownloadService.downloadTempFile(result);
                });
        }
    }
    exportItemCombo() {
        this.isItemComboExporting = true;
        this.setRepuestParams();
        if (!this.requestParams.runInBackground) {
            this._itemReportServiceProxy
                .exportItemComboSalesToExcel(this.requestParams)
                .pipe(finalize(() => (this.isItemComboExporting = false)))
                .subscribe((result) => {
                    this._fileDownloadService.downloadTempFile(result);
                });
        }
    }

    private _createBuilder() {
        this.config.fields = {
            groupName: {
                name: this.l("TypeCode"),
                type: "string",
            },
            categoryName: {
                name: this.l("Category"),
                type: "string",
            },
            menuItemName: {
                name: this.l("MenuItem"),
                type: "string",
            },
            menuItemPortionName: {
                name: this.l("Portion"),
                type: "string",
            },
            price: {
                name: this.l("AvgPrice"),
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
                type: "number",
                // validator: {
                //     min: 0,
                //     step: 0.01,
                // },
            },
            Quantity: {
                name: this.l("Quantity"),
                type: "number",
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
            },
            TotalAmount: {
                name: this.l("TotalAmount"),
                type: "number",
                operators: [
                    "equal",
                    "less",
                    "less_or_equal",
                    "greater",
                    "greater_or_equal",
                ],
            },
        };
    }

    exportModalShow() {
        this.setRepuestParams();
        this.exportModal.show(this.requestParams);
    }
}

import {
    Component,
    Injector,
    ViewChild,
    Renderer2,
    ElementRef,
    OnInit,
} from "@angular/core";
import {
    CommonLocationGroupDto,
    ConnectLookupServiceProxy,
    ExportType,
    MenuItemServiceProxy,
    GetItemInput,
    ItemReportServiceProxy,
    PagedResultDtoOfCategoryDto,
    CategoryServiceProxy,
    PagedResultDtoOfMenuItemListDto,
    CommonLookupServiceProxy,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { map, sumBy, unionBy } from "lodash";
import { forkJoin } from "rxjs";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent, TreeNode } from "primeng";
import {
    QueryBuilderComponent,
    QueryBuilderConfig,
} from "angular2-query-builder";
import { finalize } from "rxjs/operators";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { Observable } from "rxjs";
import { DecimalPipe } from "@angular/common";

@Component({
    selector: "app-top-down-report",
    templateUrl: "./top-down-report.component.html",
    styleUrls: ["./top-down-report.component.scss"],
    animations: [appModuleAnimation()],
})
export class TopDownReportComponent extends AppComponentBase implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
    @ViewChild("queryBuilder", { static: false })
    queryBuilder: QueryBuilderComponent;
    @ViewChild("selectCategoryPopupModal", { static: true })
    selectCategoryPopupModal: SelectPopupComponent;
    @ViewChild("selectMenuItemPopupModal", { static: true })
    selectMenuItemPopupModal: SelectPopupComponent;

    filterText = '';
    locationOrGroup = new CommonLocationGroupDto();
    dateRangeModel: any[] = [new Date(), new Date()];
    ranges = this.createDateRangePickerOptions();
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel,
        takeNumber: 10,
        ranking: "Quantity",
        takeType: "desc",
    } as GetItemInput;

    decimals = 2;
    loading = false;
    menuItems = [];
    departments = [];
    productGroups = [];
    disableExport = true;
    ExportType = ExportType;
    advancedFiltersAreShown = false;
    ticketTags = "";
    selectedDepartments = [];
    selectedProductGroups = [];
    selectedCategory = [];
    selectedMenuItems = [];
    topQtyData: any;
    topValueData = {};
    chartOption = {};
    treeData: TreeNode[];

    totalQuantity = 0;
    totalAmount = 0;

    isExporting = false;
    isShowFilterOptions = false;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _itemReportServiceProxy: ItemReportServiceProxy,
        private _categoryService: CategoryServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _decimalPipe: DecimalPipe
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getComboBoxData();
    }

    getReportData(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.setRepuestParams();
        this.requestParams.sorting = this.primengTableHelper.getSorting(
            this.dataTable
        );
        this.requestParams.maxResultCount =
            this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );

        this.requestParams.isSummary = true;

        this._itemReportServiceProxy
            .buldTopOrBottomItemSales(this.requestParams)
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount =
                    result.menuList.totalCount;
                this.primengTableHelper.records = result.menuList.items;
                this.totalAmount = result.totalResult;
                this.totalQuantity = result.totalQuantity;
            });
    }

    exportToExcel(exportOutputType: ExportType): void {
        this.isExporting = true;
        this.setRepuestParams();
        this.requestParams.exportOutputType = exportOutputType;

        this._itemReportServiceProxy
            .exportTopOrBottomItemSalesToExcel(this.requestParams)
            .pipe(finalize(() => (this.isExporting = false)))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        this.selectedMenuItems = [];
        this.selectedCategory = [];
        this.selectedDepartments = [];
        this.selectedProductGroups = [];
        this.requestParams = {
            exportOutputType: ExportType.Excel,
            takeNumber: 10,
            ranking: "Quantity",
            takeType: "desc",
        } as GetItemInput;
    }

    openMenuItemModal() {
        this.selectMenuItemPopupModal.show(this.selectedMenuItems);
    }

    selectCategory() {
        this.selectCategoryPopupModal.show(this.selectedCategory);
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    getCategorySelection() {
        return (): Observable<PagedResultDtoOfCategoryDto> =>
            this._categoryService.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    getMenuItemSelection() {
        return (): Observable<PagedResultDtoOfMenuItemListDto> =>
            this._menuItemServiceProxy.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    addMenuItems(itemSelect) {
        this.selectedMenuItems = itemSelect;
    }

    addCategory(itemSelect) {
        this.selectedCategory = itemSelect;
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;
        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        self.render.removeClass(el, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
        });
    }

    refresh() {
        this.clear();
        this.getReportData();
    }

    apply() {
        this.getReportData();
        this.isShowFilterOptions = false;
    }

    private setRepuestParams() {
        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]) .startOf("day") .add(moment().utcOffset(), "minute") : undefined;
this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]) .endOf("day") .add(moment().utcOffset(), "minute") : undefined;

        this.requestParams.menuItemIds = [];
        this.requestParams.filterByCategory = [];
        this.requestParams.filterByDepartment = [];
        this.requestParams.filterByGroup = [];

        this.selectedMenuItems.map((item) => {
            this.requestParams.menuItemIds.push(+item.id);
        });
        this.selectedCategory.map((item) => {
            this.requestParams.filterByCategory.push(+item.id);
        });
        this.selectedDepartments.map((item) => {
            this.requestParams.filterByDepartment.push(item.displayText);
        });
        this.selectedProductGroups.map((item) => {
            this.requestParams.filterByGroup.push(+item.value);
        });
    }

    private getComboBoxData() {
        forkJoin([
            this._menuItemServiceProxy.getMenuItemForComboBox(),
            this._connectLookupServiceProxy.getDepartments(),
            this._commonLookupServiceProxy.getProductGroupsForCombobox(""),
        ]).subscribe((results) => {
            this.menuItems = [];
            for (const key in results[0]) {
                if (Object.prototype.hasOwnProperty.call(results[0], key)) {
                    const element = results[0][key];

                    this.menuItems.push(...element);
                }
            }

            this.menuItems = unionBy(this.menuItems, "id").map((item) => {
                return {
                    value: item.value,
                    name: item.displayText,
                };
            });
            this.departments = results[1].items;
            this.productGroups = results[2].items;
        });
    }
}

import {
    Component,
    EventEmitter,
    Injector,
    Output,
    Renderer2,
    ViewChild,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CategoryServiceProxy,
    CommonLookupServiceProxy,
    ConnectLookupServiceProxy,
    FaqCreateDto,
    GetItemInput,
    ItemReportServiceProxy,
    MenuItemServiceProxy,
    TiffinFaqServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { DecimalPipe } from "@angular/common";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    selector: "app-item-report-export-modal",
    templateUrl: "./item-report-export-modal.component.html",
    styleUrls: ["./item-report-export-modal.component.css"],
})
export class ItemReportExportModalComponent extends AppComponentBase {
    @ViewChild("createModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    duration = "D";
    exportOutput = "T";
    bylocation = false;

    active = false;
    saving = false;
    requestParams = new GetItemInput();

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _itemReportServiceProxy: ItemReportServiceProxy,
        private _categoryService: CategoryServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _decimalPipe: DecimalPipe
    ) {
        super(injector);
    }

    show(requestParams: GetItemInput): void {
        this.requestParams = requestParams;
        this.active = true;
        this.modal.show();
    }

    onShown(): void {
        document.getElementById("Durations").focus();
    }

    export(): void {
        this.saving = true;
        this.requestParams.byLocation = this.bylocation;
        this.requestParams.exportOutput = this.exportOutput;
        this.requestParams.duration = this.duration;

        if (!this.requestParams.runInBackground) {
            this._itemReportServiceProxy
                .exportItemsExcel(this.requestParams)
                .pipe(finalize(() => (this.saving = false)))
                .subscribe((result) => {
                    this._fileDownloadService.downloadTempFile(result);
                    this.close();
                });
        }
    }

    close(): void {
        this.requestParams = new GetItemInput();
        this.duration = "D";
        this.exportOutput = "T";
        this.bylocation = false;
        this.active = false;
        this.modal.hide();
    }
}

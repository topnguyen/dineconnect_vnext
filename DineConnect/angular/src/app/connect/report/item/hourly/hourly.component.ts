import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import {
    CommonLocationGroupDto,
    ExportType,
    MenuItemServiceProxy,
    GetItemInput,
    ItemReportServiceProxy,
    PagedResultDtoOfMenuItemListDto,
} from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { sumBy } from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-hourly',
    templateUrl: './hourly.component.html',
    styleUrls: ['./hourly.component.scss'],
    animations: [appModuleAnimation()]
})
export class HourlyComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectMenuItemPopupModal', { static: true }) selectMenuItemPopupModal: SelectPopupComponent;

    filterText;
    dateRangeModel: any[] = [new Date(), new Date()];
    selectedMenuItems = [];
    locationOrGroup = new CommonLocationGroupDto();
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel
    } as GetItemInput;
    loading = false;
    disableExport = false;
    isShowFilterOptions = false;
    ExportType = ExportType;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _itemReportServiceProxy: ItemReportServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {}

    getOrders(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.setRepuestParams();
        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

        this._itemReportServiceProxy
            .buildItemHourlySales(this.requestParams)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.menuList.totalCount;
                this.primengTableHelper.records = result.menuList.items;
            });
    }

    getMenuItemSelection() {
        return (): Observable<PagedResultDtoOfMenuItemListDto> =>
            this._menuItemServiceProxy.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    addMenuItems(itemSelect) {
        this.selectedMenuItems = itemSelect;
    }

    clearSelectedItem() {
        this.selectedMenuItems = [];
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(exportOutputType: ExportType): void {
        this.setRepuestParams();
        this.requestParams.exportOutputType = exportOutputType;
        this._itemReportServiceProxy.buildItemHourlyReportExcel(this.requestParams).subscribe((result) => {
            this._fileDownloadService.downloadFromFilePath(result);
        });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    clear() {
        this.filterText = '';
        this.dateRangeModel = [new Date(), new Date()];
        this.selectedMenuItems = [];
        this.requestParams = {
            exportOutputType: ExportType.Excel
        } as GetItemInput;
    }

    refresh() {
        this.clear();
        this.getOrders();
    }

    onShowFilter() {
        this.isShowFilterOptions = true;
    }

    openforMenuItem() {
        this.selectMenuItemPopupModal.show(this.selectedMenuItems);
    }

    apply() {
        this.getOrders();
        this.isShowFilterOptions = false;
    }

    private setRepuestParams() {
        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate =
            this.dateRangeModel && this.dateRangeModel.length > 0
                ? moment(this.dateRangeModel[0]).startOf('day').add(moment().utcOffset(), 'minute')
                : undefined;
        this.requestParams.endDate =
            this.dateRangeModel && this.dateRangeModel.length > 0
                ? moment(this.dateRangeModel[1]).endOf('day').add(moment().utcOffset(), 'minute')
                : undefined;

        this.requestParams.filterByCategory = [];
        this.requestParams.filterByDepartment = [];
        this.requestParams.filterByGroup = [];
        this.requestParams.menuItemIds = this.selectedMenuItems.map((item) => {
            return +item.id;
        });
    }

    get getSumTotal() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.total;
        });
    }

    get getSumQuantity() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.quantity;
        });
    }
}

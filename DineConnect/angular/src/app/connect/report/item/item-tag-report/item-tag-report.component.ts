import { Component, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ItemReportServiceProxy, GetItemInput, ExportType, CommonLocationGroupDto } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { sumBy } from 'lodash';
import _ from 'lodash';

@Component({
    selector: 'app-item-tag-report',
    templateUrl: './item-tag-report.component.html',
    styleUrls: ['./item-tag-report.component.scss'],
    animations: [appModuleAnimation()]
})
export class ItemTagReportComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('selectLocationOrGroup', { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;

    dateRangeModel: any[] = [new Date(), new Date()];
    locationOrGroup = new CommonLocationGroupDto();
    requestParams: GetItemInput = {
        exportOutputType: ExportType.Excel
    } as GetItemInput;
    ExportType = ExportType;
    advancedFiltersAreShown = false;
    data: any;
    isExporting = false;
    isShowFilterOptions =  false;
    filterText= '';
    disableExport = false;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2,
        private _itemReportServiceProxy: ItemReportServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {}

    getReportData(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this.setRepuestParams();
        this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
        this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
        this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);
        this.requestParams.isSummary = true;

        this._itemReportServiceProxy
            .buildItemTagSalesReport(this.requestParams)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.itemTagList.totalCount;
                this.primengTableHelper.records = result.itemTagList.items;
                this.buildChart(result);
            });
    }

    buildChart(input) {
        let dataChart = _.uniq(_.flatten(input.chartOutput.map((x) => x.data)));
        this.data = {
            labels: input.itemTags,
            datasets: [
                {
                    label: this.l('Total'),
                    backgroundColor: '#42A5F5',
                    borderColor: '#1E88E5',
                    data: dataChart
                }
            ]
        };
    }

    clear() {
        this.dateRangeModel = [new Date(), new Date()];
        
        this.requestParams = {
            exportOutputType: ExportType.Excel
        } as GetItemInput;
        this.getReportData();
    }

    refresh() {
        this.clear();
        this.getReportData();
    }

    apply() {
        this.getReportData();
        this.isShowFilterOptions = false;
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    exportToExcel(exportOutputType: ExportType): void {
        this.isExporting = true;
        this.setRepuestParams();
        this.requestParams.exportOutputType = exportOutputType;

        this._itemReportServiceProxy
            .exportItemTagSalesToExcel(this.requestParams)
            .pipe(finalize(() => (this.isExporting = false)))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    private setRepuestParams() {
        this.requestParams.locationGroup = this.locationOrGroup;

        this.requestParams.startDate =
            this.dateRangeModel && this.dateRangeModel.length > 0
                ? moment(this.dateRangeModel[0]).startOf('day').add(moment().utcOffset(), 'minute')
                : undefined;
        this.requestParams.endDate =
            this.dateRangeModel && this.dateRangeModel.length > 0
                ? moment(this.dateRangeModel[1]).endOf('day').add(moment().utcOffset(), 'minute')
                : undefined;

        this.requestParams.menuItemIds = [];
        this.requestParams.filterByCategory = [];
        this.requestParams.filterByDepartment = [];
        this.requestParams.filterByGroup = [];
    }

    get getSumTotal() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.total;
        });
    }

    get getSumQuantity() {
        return sumBy(this.primengTableHelper.records, (o) => {
            return o.quantity;
        });
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }
}

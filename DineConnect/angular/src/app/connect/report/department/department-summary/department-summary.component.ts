import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, DepartmentSummaryServiceProxy, ExportType, GetItemInput, GetTicketInput, SimpleLocationDto, SimpleLocationGroupDto, SimpleLocationTagDto, TicketSyncReportServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-department-summary',
  templateUrl: './department-summary.component.html',
  styleUrls: ['./department-summary.component.scss'],
  animations: [appModuleAnimation()],
})
export class DepartmentSummaryComponent extends AppComponentBase implements OnInit {

  @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('drp', { static: false }) daterangePicker: ElementRef;

  locationOrGroup = new CommonLocationGroupDto();
  dateRangeModel: any[] = [new Date(), new Date()];
  ranges = this.createDateRangePickerOptions();
  requestParams: GetItemInput = {
    exportOutputType: ExportType.Excel,
  } as GetItemInput;
  loading = false;
  disableExport = true;
  ExportType = ExportType;

  constructor(
    injector: Injector,
    private _departmentReportServiceProxy: DepartmentSummaryServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private render: Renderer2,
  ) {
    super(injector);
  }

  ngOnInit() { }

  getTicketSyncs(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();

    this.requestParams.locationGroup = this.locationOrGroup;

    this.requestParams.startDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[0]).startOf("day").add(moment().utcOffset(), "minute") : undefined;
    this.requestParams.endDate = this.dateRangeModel && this.dateRangeModel.length > 0 ? moment(this.dateRangeModel[1]).endOf("day").add(moment().utcOffset(), "minute") : undefined;

    this.requestParams.sorting = this.primengTableHelper.getSorting(this.dataTable);
    this.requestParams.maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);
    this.requestParams.skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);

    this._departmentReportServiceProxy
      .buildDepartmentSales(
        this.requestParams
      )
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.departmentList.totalCount;
        this.primengTableHelper.records = result.departmentList.items;
      });
  }

  showedDateRangePicker(): void {
    let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
    let self = this;
    let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
    self.render.removeClass(el, 'show');

    button.addEventListener('click', (event) => {
      event.preventDefault();

      self.render.addClass(el, 'show');
    });
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel(exportOutputType: ExportType): void {
    this._departmentReportServiceProxy.buildDepartmentSummaryExport(this.requestParams)
      .subscribe((result) => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }

  openSelectLocationModal() {
    this.selectLocationOrGroup.show(this.locationOrGroup);
  }

  setLocations(event: CommonLocationGroupDto) {
    this.locationOrGroup = event;
  }
}
import { Component, Injector, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { CommonLocationGroupDto, ConnectCardTypeServiceProxy, EntityDto } from '@shared/service-proxies/service-proxies';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { CardsModalComponent } from '../cards/cards-modal/cards-modal.component';

@Component({
    templateUrl: './card-type.component.html',
    styleUrls: ['./card-type.component.scss'],
    animations: [appModuleAnimation()]
})
export class CardTypeComponent extends AppComponentBase {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;
    @ViewChild('cardsModal', { static: true }) cardsModal: CardsModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted = false;
    location: string;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _connectCardTypeServiceProxy: ConnectCardTypeServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        const locationOrGroup = this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocation = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._connectCardTypeServiceProxy
            .getCardTypes(
                this.filterText,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocation,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditConnectCardType(id?: number): void {
        this._router.navigate(['/app/connect/card/types/create-or-update', id ? id : 'null']);
    }

    deleteConnectCardType(id: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._connectCardTypeServiceProxy.deleteConnectCardType(id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(id: number) {
        const body = EntityDto.fromJS({ id: id });
        this._connectCardTypeServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._connectCardTypeServiceProxy
            .getAllToExcel()
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search() {
        this.getAll();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    openCards(id: number) {
        this._openCardsModal(id, undefined);
    };

    openRedemption(id: number) {
        this._openCardsModal(id, true);
    };

    private _openCardsModal(id: number, redeemed: boolean) {
        this.cardsModal.show({
            cardTypeId: id,
            redeemed: redeemed,
            title: redeemed ? this.l('Redemption') : this.l('Cards')
        })
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = '';

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(',');
            this.location = locationOrGroup.groups[0].name;
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags.map((x) => x.id).join(',');
            this.location = locationOrGroup.locationTags[0].name;
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations.map((x) => x.id).join(',');
            this.location = null;
        } else if (locationOrGroup.locations?.length) {
            this.location = locationOrGroup.locations[0].name;
            locationIds = locationOrGroup.locations.map((x) => x.id).join(',');
        }
        return locationIds;
    }
}

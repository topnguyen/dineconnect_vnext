import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    ConnectCardTypeCategoryEditDto,
    CreateOrUpdateConnectCardTypeCategoryInput,
    CommonLocationGroupDto,
    ConnectCardTypeServiceProxy,
    ConnectCardTypeEditDto,
    CreateOrUpdateConnectCardTypeInput,
    ConnectCardTypeCategoryServiceProxy,
    ComboboxItemDto,
    ConnectCardServiceProxy,
    GenerateCardsInputDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Router, ActivatedRoute } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { Observable } from "rxjs/internal/Observable";
import { AppConsts } from "@shared/AppConsts";

@Component({
    templateUrl: "./create-or-update-card-type.component.html",
    styleUrls: ["./create-or-update-card-type.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrUpdateCardTypeComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("importModal", { static: true })
    importModal: ImportModalComponent;

    saving = false;
    connectCardType: ConnectCardTypeEditDto = new ConnectCardTypeEditDto();
    cardTypeId: number;
    selectedTab: number = 1;
    cardTypeCategorys = [];
    cardList = [];

    prefix;
    suffix;
    cardNumberOfDigits;
    cardStartingNumber;
    cardCount;

    TAB = {
        CARD_TYPE: 1,
        GENERATE_NEW_CARDS: 2,
        CARDS: 3,
        IMPORT: 4,
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _connectCardTypeServiceProxy: ConnectCardTypeServiceProxy,
        private _connectCardTypeCategoryServiceProxy: ConnectCardTypeCategoryServiceProxy,
        private _connectCardServiceProxy: ConnectCardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.cardTypeId = +params["id"]; // (+) converts string 'id' to a number

            if (!isNaN(this.cardTypeId)) {
                this._init();
                this.configImport();
            }
        });

        this._getCardTypeCategories();
    }

    save(): void {
        this.saving = true;
        const input = new CreateOrUpdateConnectCardTypeInput();
        input.connectCardType = this.connectCardType;

        this._connectCardTypeServiceProxy
            .createOrUpdateConnectCardType(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    configImport() {
        this.importModal.configure({
            title: this.l("CardType"),
            downloadTemplate: () => {
                return this._connectCardTypeServiceProxy.getImportCardsTemplateToExcel();
            },
            import: (fileToken: string) => {
                return this._connectCardTypeServiceProxy.importCardsFromExcel(
                    this.cardTypeId,
                    fileToken
                );
            },
            filters: [
                {
                    name: "imageFilter",
                    fn: (item, options) => {
                        var type =
                            "|" +
                            item.type.slice(item.type.lastIndexOf("/") + 1) +
                            "|";
                        if (
                            "|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|".indexOf(
                                type
                            ) === -1
                        ) {
                            this.message.warn(
                                this.l("ImportTemplate_Warn_FileType")
                            );
                            return false;
                        }
                        return true;
                    },
                },
            ],
        });
    }

    onChangeTab(tab: number) {
        this.selectedTab = tab;
    }

    close(): void {
        this._router.navigate(["/app/connect/card/types"]);
    }

    generateCards() {
        const body = GenerateCardsInputDto.fromJS({
            connectCardTypeId: this.connectCardType.id,
            prefix: this.prefix,
            suffix: this.suffix,
            cardNumberOfDigits: this.cardNumberOfDigits,
            cardStartingNumber: this.cardStartingNumber,
            cardCount: this.cardCount,
        });

        this._connectCardServiceProxy
            .generateConnectCards(body)
            .subscribe((result) => {
                this.notify.info(
                    this.cardCount +
                        " " +
                        this.l("CardsAreBeingGeneratedInBackground")
                );
                this.connectCardType.id = null;
                this.prefix = null;
                this.suffix = null;
                this.cardNumberOfDigits = null;
                this.cardStartingNumber = null;
                this.cardCount = null;
                this.close();
            });
    }

    import() {
        this.importModal.show();
    }

    onModalSave() {
        this._init();
    }

    private _getCardTypeCategories() {
        this._connectCardTypeCategoryServiceProxy
            .getConnectCardTypeCategorys()
            .subscribe((result) => {
                this.cardTypeCategorys = result.items.map((item) => {
                    return ComboboxItemDto.fromJS({
                        value: item.id,
                        displayText: item.name + " - " + item.code,
                    });
                });
            });
    }

    private _init() {
        this._connectCardTypeServiceProxy
            .getConnectCardTypeForEdit(this.cardTypeId)
            .subscribe((result) => {
                this.connectCardType = result.connectCardType;
                this.cardList = result.cardList;
            });
    }
}

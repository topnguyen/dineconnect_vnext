import { Component, Injector, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { ConnectCardTypeCategoryServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './card-category.component.html',
  styleUrls: ['./card-category.component.scss'],
  animations: [appModuleAnimation()]
})
export class CardCategoryComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _connectCardTypeCategoryServiceProxy: ConnectCardTypeCategoryServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._connectCardTypeCategoryServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createConnectCardTypeCategory(id?: number): void {
        this._router.navigate(['/app/connect/card/categorys/create-or-update', id ? id : 'null']);
    }

    deleteConnectCardTypeCategory(id?: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._connectCardTypeCategoryServiceProxy.deleteConnectCardTypeCategory(id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    refresh() {
        this.filterText = undefined;
        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._connectCardTypeCategoryServiceProxy
            .getAllToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search() {
        this.getAll();
    }
}

import { Component, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {
    ConnectCardTypeCategoryServiceProxy,
    ConnectCardTypeCategoryEditDto,
    CreateOrUpdateConnectCardTypeCategoryInput
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  templateUrl: './create-or-update-card-category.component.html',
  styleUrls: ['./create-or-update-card-category.component.scss'],
  animations: [appModuleAnimation()]
})
export class CreateOrUpdateCardCategoryComponent extends AppComponentBase implements OnInit {

    saving = false;
    category: ConnectCardTypeCategoryEditDto = new ConnectCardTypeCategoryEditDto();
    categoryId: number;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _connectCardTypeCategoryServiceProxy: ConnectCardTypeCategoryServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.categoryId = +params['id']; // (+) converts string 'id' to a number

            if (!isNaN(this.categoryId)) {
                this._connectCardTypeCategoryServiceProxy.getConnectCardTypeCategoryForEdit(this.categoryId).subscribe((result) => {
                    this.category = result.connectCardTypeCategory;
                });
            }
        });
    }

    save(): void {
        this.saving = true;
        const input = new CreateOrUpdateConnectCardTypeCategoryInput();
        input.connectCardTypeCategory = this.category;
    
        this._connectCardTypeCategoryServiceProxy
            .createOrUpdateConnectCardTypeCategory(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(['/app/connect/card/categorys']);
    }
}
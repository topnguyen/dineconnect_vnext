import { Component, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConnectCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

export interface ICardsModalOptions {
    title?: string;
    cardTypeId: number;
    redeemed: boolean;
}

@Component({
    selector: 'app-cards-modal',
    templateUrl: './cards-modal.component.html',
    styleUrls: ['./cards-modal.component.scss']
})
export class CardsModalComponent extends AppComponentBase {
    @ViewChild('modal', { static: true }) modal: ModalDirective;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    isShown = false;
    filterText = '';
    options: ICardsModalOptions = {} as ICardsModalOptions;

    constructor(injector: Injector, private _connectCardServiceProxy: ConnectCardServiceProxy) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._connectCardServiceProxy
            .getAllConnectCard(
                this.filterText,
                this.options.cardTypeId,
                this.options.redeemed,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    show(options : ICardsModalOptions): void {
        this.options = options;
        this.getAll();
        this.modal.show();
    }

    refreshTable(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    close(): void {
        this.isShown = false;
        this.modal.hide();
    }

    shown(): void {
        this.isShown = true;
    }
}

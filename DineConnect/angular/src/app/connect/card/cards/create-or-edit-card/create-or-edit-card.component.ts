import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ConnectCardEditDto, ConnectCardServiceProxy, ConnectCardTypeListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-create-or-edit-card',
  templateUrl: './create-or-edit-card.component.html',
  styleUrls: ['./create-or-edit-card.component.scss']
})
export class CreateOrEditCardComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    active = false;
    connectCard: ConnectCardEditDto = new ConnectCardEditDto();
    connectCardTypes: ConnectCardTypeListDto[] = [];

    constructor(
        injector: Injector,
        private _connectCardServiceProxy: ConnectCardServiceProxy
    ) {
        super(injector);
    }

    show(connectCardId?: number): void {
        this.active = true;

        if (!connectCardId) {
            this.modal.show();
        } else {
            this._connectCardServiceProxy.getConnectCardForEdit(connectCardId).subscribe(result => {
                this.connectCard = result;
                this.modal.show();
            });
        }

        this._connectCardServiceProxy.getConnectCardTypes().subscribe(result => {
            this.connectCardTypes = result;
        });
    }

    save(): void {
        this.saving = true;

        this._connectCardServiceProxy.createOrUpdateConnectCard(this.connectCard)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { finalize } from "rxjs/operators";
import {
    ConnectCardServiceProxy,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { CreateOrEditCardComponent } from "./create-or-edit-card/create-or-edit-card.component";
import { ImportCardModalComponent } from "./import-card-modal/import-card-modal.component";

@Component({
    templateUrl: "./cards.component.html",
    styleUrls: ["./cards.component.scss"],
    animations: [appModuleAnimation()],
})
export class CardsComponent extends AppComponentBase implements OnInit {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("createOrEditCardModal", { static: true })
    createOrEditCardModal: CreateOrEditCardComponent;
    @ViewChild("importModal", { static: true })
    importModal: ImportCardModalComponent;

    filterText = "";
    selectedCards = [];

    constructor(
        injector: Injector,
        private _connectCardServiceProxy: ConnectCardServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit() {
        this.configImport();
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._connectCardServiceProxy
            .getAllConnectCard(
                this.filterText,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    configImport() {
        // this.importModal.configure({
        //     title: this.l('Card'),
        //     downloadTemplate: () => {
        //         return this._connectCardServiceProxy.getImportTemplateToExcel();
        //     },
        //     import: (fileToken: string) => {
        //         return this._connectCardServiceProxy.importFromExcel(fileToken);
        //     },
        //     filters: [
        //         {
        //             name: 'excelFilter',
        //             fn: (item, options) => {
        //                 var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        //                 if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
        //                     this.message.warn(this.l('ImportTemplate_Warn_FileType'));
        //                     return false;
        //                 }
        //                 return true;
        //             }
        //         }
        //     ]
        // });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditCard(id?: number): void {
        this.createOrEditCardModal.show(id);
    }

    deleteCard(id?: number): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._connectCardServiceProxy
                    .deleteConnectCard(id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    deleteMutilCard() {
        let input = this.selectedCards.map((item) => {
            return +item.id;
        });
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._connectCardServiceProxy
                    .deleteMultiConnectCard(input)
                    .subscribe(() => {
                        this.reloadPage();
                        this.selectedCards = [];
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    refresh() {
        this.filterText = undefined;
        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._connectCardServiceProxy.getAllToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    import() {
        this.importModal.show();
    }

    search() {
        this.getAll();
    }

    inActiveOrActiveCard(record) {
        let input = new EntityDto();
        input.id = record.id;

        this._connectCardServiceProxy
            .activeOrInactiveConnectCard(input)
            .subscribe(() => {
                record.active = !record.active;
                this.notify.success(this.l("Successfully"));
            });
    }
}

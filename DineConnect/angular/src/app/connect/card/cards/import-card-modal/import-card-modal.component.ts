import { Component, Injector, ViewChild, OnInit, EventEmitter, Output } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConnectCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader } from 'ng2-file-upload';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'import-card-modal',
  templateUrl: './import-card-modal.component.html',
  styleUrls: ['./import-card-modal.component.css']
})
export class ImportCardModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('fileInput', { static: true }) fileInput: any;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  isShown = false;
  filterText = '';
  options = {};
  downloading = false;
  saving = false;
  uploader: FileUploader;
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/Import/ImportConnectCardData';
  active: boolean;
  jobName = '';

  constructor(injector: Injector,
    private _connectCardServiceProxy: ConnectCardServiceProxy,
    private _tokenService: TokenService,
    private _fileDownloadService: FileDownloadService) {
    super(injector);
  }

  ngOnInit(): void {
    this.initUploaders();
  }

  show(): void {
    // this.options = options;
    this.getAll();
    this.modal.show();
  }

  close(): void {
    if (this.uploader.queue.length > 1) {
      this.uploader.queue.forEach(element => {
        this.uploader.removeFromQueue(element);
      });
    }
    this.isShown = false;
    this.saving = false;
    this.active = undefined;
    this.jobName = '';
    this.fileInput.nativeElement.value = '';
    this.modal.hide();
    this.modalSave.emit();
  }

  shown(): void {
    this.isShown = true;
  }

  initUploaders(): void {
    this.uploader = new FileUploader({
      url: this.remoteServiceBaseUrl,
      authToken: 'Bearer ' + this._tokenService.getToken(),
      queueLimit: 1,
      filters: [],
      additionalParameter: undefined
    });

    this.uploader.onAfterAddingFile = (f) => {
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <IAjaxResponse>JSON.parse(response);
      this.saving = false;
      if (ajaxResponse.success) {
        this.notify.success(this.l('Successfully'));
        this.close();
      } else {
        this.message.error(ajaxResponse.error.message);
      }
    };

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('Active', this.active); //note comma separating key and value
      form.append('JobName', this.jobName);
    };

    this.uploader.onErrorItem = (item, response, status, headers) => { this.saving = false; };
  }


  getAll(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    this._connectCardServiceProxy
      .getImportCardDataDetails(
        this.filterText,
        undefined,
        undefined,
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getMaxResultCount(this.paginator, event),
        this.primengTableHelper.getSkipCount(this.paginator, event)
      )
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe((result) => {
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  downloadTemplate() {
    this.downloading = true;
    this._connectCardServiceProxy.getImportTemplateToExcel()
      .pipe(
        finalize(() => {
          this.downloading = false;
        })
      )
      .subscribe((result) => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }

  save() {
    this.saving = true;
    if (this.uploader.queue.length > 0) {


      this.uploader.uploadAll();
    }
  }

  exportToExcel() {

    this._connectCardServiceProxy
      .getImportCardDataDetailToExport()
      .subscribe((result) => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }
}

import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import {
    CommonLocationGroupDto,
    PromotionCategoryServiceProxy,
    PromotionCategoryListDto
} from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import * as _ from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './promotion-categorys.component.html',
    styleUrls: ['./promotion-categorys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PromotionCategorysComponent extends AppComponentBase {

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    location: string;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _promotionCategoryServiceProxy: PromotionCategoryServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._promotionCategoryServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditCategory(id: number): void {
        this._router.navigate(['/app/connect/discount/categorys/create-or-update', id ? id : 'null']);
    }

    deleteCagegory(category: PromotionCategoryListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._promotionCategoryServiceProxy.deletePromotionCategory(category.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = '';

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(',');
            this.location = locationOrGroup.groups[0].name;
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags.map((x) => x.id).join(',');
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations.map((x) => x.id).join(',');
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(',');
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;

        this.paginator.changePage(0);
    }

    exportToExcel() {

        this._promotionCategoryServiceProxy
            .getAllToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search() {
        this.getAll();
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CommonLocationGroupDto,
    CreateOrUpdatePromotionCategoryInput,
    PromotionCategoryEditDto,
    PromotionCategoryServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './create-update-promotion-category.component.html',
    styleUrls: ['./create-update-promotion-category.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateUpdatePromotionCategoryComponent extends AppComponentBase implements OnInit {
    saving = false;
    prCateId: number;
    promotionCategory = new PromotionCategoryEditDto();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _promotionCategoryServiceProxy: PromotionCategoryServiceProxy,
    ) {
        super(injector);
        this.prCateId = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this._init();
    }

    close() {
        this._router.navigate(['/app/connect/discount/categorys']);
    }

    save(): void {
        const input = new CreateOrUpdatePromotionCategoryInput();
        input.promotionCategory = this.promotionCategory;

        this.saving = true;
        this._promotionCategoryServiceProxy
            .createOrUpdatePromotionCategory(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    private _init() {
        if (this.prCateId && this.prCateId != NaN) {
            this._promotionCategoryServiceProxy.getPromotionCategoryForEdit(this.prCateId)
                .subscribe((result) => {
                    this.promotionCategory = result.promotionCategory;
                });
        }
    }
}

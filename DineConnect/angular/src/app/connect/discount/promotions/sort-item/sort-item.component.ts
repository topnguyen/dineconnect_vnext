import { Component, Injector, Output, EventEmitter, ViewChild, OnInit, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MenuItemServiceProxy, IdNameDto, PromotionRestrictItemEditDto, PromotionsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';
import { CdkDragDrop, CDK_DRAG_CONFIG, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
const DragConfig = {
  dragStartThreshold: 0,
  pointerDirectionChangeThreshold: 5,
  zIndex: 10000
};
@Component({
  selector: 'app-sort-item-promotion',
  templateUrl: './sort-item.component.html',
  styleUrls: ['./sort-item.component.css'],
  providers: [{ provide: CDK_DRAG_CONFIG, useValue: DragConfig }]
})

export class SortItemComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  loading = false;
  promotions:any[]=[];
  constructor(
    injector: Injector,
    private _promotionsServiceProxy: PromotionsServiceProxy,
  ) {
    super(injector);
  }

  show(): void {
    this.getPromotions(null);
    this.modal.show();
  }

  ngOnInit() { }

  shown(): void {}

  getPromotions(event?: LazyLoadEvent) {
    this.loading = true;
    this._promotionsServiceProxy.getAll(
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      0,
      1000
  )
  .pipe(
    finalize(() => {
        this.loading = false;
    })
)
  .subscribe(result => {
      this.loading = true;
      this.promotions = result.items;
    })
  }
  save(){
    let ids = this.promotions.map(el => el.id);
    console.log('id', ids);
    this._promotionsServiceProxy.saveSortOrder(ids).subscribe(result => {
      this.close();
      this.modalSave.emit(null);
    });
  }
  close() {
    this.modal.hide();
  }
 
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
}

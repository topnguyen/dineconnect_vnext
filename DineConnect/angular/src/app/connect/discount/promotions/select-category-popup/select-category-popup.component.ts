import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PrimengTableHelper } from '@shared/helpers/PrimengTableHelper';
import { CategoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'select-category-popup',
  templateUrl: './select-category-popup.component.html',
  styleUrls: ['./select-category-popup.component.css']
})
export class SelectCategoryPopupComponent extends AppComponentBase implements OnInit {
  @Input() selectedValue: any | any[];
  @Output("changeValue") changeValue: EventEmitter<any> =
      new EventEmitter<any>();

  @ViewChild("modal", { static: true }) modal: ModalDirective;

  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;

  filterText = "";
  productGroupId?:number = null;

  constructor(injector: Injector,
    private _categoryService: CategoryServiceProxy,
    ) {
      super(injector);
      this.primengTableHelper = new PrimengTableHelper();
  }

  ngOnInit() {
    
  }


  show(id?: number, seletecdItem?:number): void {
    this.productGroupId = id;
    if (seletecdItem) {
        this.selectedValue = seletecdItem;
    }
    this.modal.show();
    this.getCategoriesByGroup();
  }

  save() {
      this.changeValue.emit(this.selectedValue);
      this.selectedValue = null;
      this.close();
  }

  close() {
      this.modal.hide();
  }

  getCategoriesByGroup(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    if(this.productGroupId){
      this._categoryService.getCategoryByPgroductGroupId(
        this.filterText,
        this.productGroupId,
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getMaxResultCount(this.paginator, event),
        this.primengTableHelper.getSkipCount(this.paginator, event)
      ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
        .subscribe(result => {
          this.primengTableHelper.totalRecordsCount = result.totalCount;
          this.primengTableHelper.records = result.items;
          this.primengTableHelper.hideLoadingIndicator();
        });
    }
    else{
      this._categoryService.getAll(
        this.filterText,
        false,
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getMaxResultCount(this.paginator, event),
        this.primengTableHelper.getSkipCount(this.paginator, event)
      ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
        .subscribe(result => {
          this.primengTableHelper.totalRecordsCount = result.totalCount;
          this.primengTableHelper.records = result.items;
          this.primengTableHelper.hideLoadingIndicator();
        });
    }
  }
}

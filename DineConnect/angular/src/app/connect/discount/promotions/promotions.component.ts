﻿import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
    ElementRef,
    Renderer2,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
    PromotionsServiceProxy,
    PromotionsDto,
    CommonLocationGroupDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { TokenAuthServiceProxy } from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import * as moment from "moment";
import { finalize } from "rxjs/operators";
import { BsDaterangepickerDirective } from "ngx-bootstrap/datepicker";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { NotifyService } from "abp-ng2-module";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { CheckboxModule } from "primeng/checkbox";
import { SortItemComponent } from "./sort-item/sort-item.component";
import { result } from "lodash";

@Component({
    templateUrl: "./promotions.component.html",
    encapsulation: ViewEncapsulation.None,
    styleUrls: ["./promotions.component.scss"],
    animations: [appModuleAnimation()],
})
export class PromotionsComponent extends AppComponentBase implements OnInit {
    @ViewChild("drp", { static: false }) daterangePicker: ElementRef;
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("sortItem", { static: true })
    sortItem: SortItemComponent;


    advancedFiltersAreShown = false;
    filterText = "";
    isDeleted;
    locationOrGroup: CommonLocationGroupDto;
    promotionType: any;
    noFilter: boolean = false;

    ranges = [];
    range = [];
    public promotionTypes = [];
    public dateRange: Date[] = [moment().toDate(), moment().toDate()];

    constructor(
        injector: Injector,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService,
        private render: Renderer2
    ) {
        super(injector);

        this.ranges = [
            {
                value: [new Date(), new Date()],
                label: this.l("Today"),
            },
            {
                value: [
                    new Date(new Date().setDate(new Date().getDate() + 1)),
                    new Date(new Date().setDate(new Date().getDate() + 1)),
                ],
                label: this.l("Tomorrow"),
            },
            {
                value: [
                    new Date(),
                    new Date(new Date().setDate(new Date().getDate() + 6)),
                ],
                label: this.l("Next7Days"),
            },
            {
                value: [
                    new Date(),
                    new Date(new Date().setDate(new Date().getDate() + 29)),
                ],
                label: this.l("Next30Days"),
            },
            {
                value: [new Date(), moment().endOf("month").toDate()],
                label: this.l("ThisMonth"),
            },
            {
                value: [
                    new Date(
                        new Date(new Date().setDate(1)).setMonth(
                            new Date().getMonth() + 1
                        )
                    ),
                    moment().add(1, "month").endOf("month").toDate(),
                ],
                label: this.l("NextMonth"),
            },
        ];
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;
        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        self.render.removeClass(el, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
        });
    }

    ngOnInit(): void {
        this.getPromotionTypes();
    }

    getPromotions(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        let promotionTypeId = this.promotionType
            ? isNaN(+this.promotionType.value)
                ? undefined
                : this.promotionType.value
            : undefined;

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);
        this._promotionsServiceProxy
            .getAll(
                this.filterText,
                this.dateRange && this.dateRange.length > 0
                    ? moment(this.dateRange[0]).endOf("day")
                    : undefined,
                this.dateRange && this.dateRange.length > 0
                    ? moment(this.dateRange[1]).endOf("day")
                    : undefined,
                promotionTypeId,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    deletePromotion(promotion: PromotionsDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._promotionsServiceProxy
                    .delete(promotion.id)
                    .subscribe(() => {
                        this.getPromotions();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    exportToExcel(): void {
        this._promotionsServiceProxy
            .getPromotionsToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    createOrEditPromotion(promotionId?: number): void {
        this._router.navigate([
            "/app/connect/discount/promotions/create-or-update",
            promotionId ? promotionId : "null",
        ]);
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    refresh() {
        this.noFilter = false;
        this.filterText = undefined;
        this.isDeleted = false;
        this.promotionType = undefined;
        this.dateRange = [moment().toDate(), moment().toDate()];
        if (this.daterangePicker) {
            this.daterangePicker.nativeElement.value = "";
        }
        this.locationOrGroup = new CommonLocationGroupDto();
        this.paginator.changePage(0);
    }

    search() {
        this.getPromotions();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    private getPromotionTypes() {
        this._promotionsServiceProxy
            .getPromotionTypesForLookupTable()
            .subscribe((result) => {
                this.promotionTypes = result.items;
            });
    }

    changeFilter() {
        if (this.noFilter) {
            this.dateRange = [];
            this.promotionType = undefined;
        } else {
            this.dateRange = [moment().toDate(), moment().toDate()];
            this.promotionType = undefined;
            this.paginator.changePage(0);
        }
    }
    sortPromotion(){
     this.sortItem.show();
    }
    getPromotionAllDeleted(){
        this.noFilter = true;
        this.changeFilter();
        this.getPromotions();
    }

    revertPromotion(record){
        const body = EntityDto.fromJS({ id: record.id });
        this._promotionsServiceProxy.activateItem(body).subscribe(result=>{
            this.getPromotions();
            this.notify.success(this.l("Revert Succesfully"));
        });
    }
}

import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PrimengTableHelper } from '@shared/helpers/PrimengTableHelper';
import { CategoryServiceProxy, MenuItemServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'select-menu-item-popup',
  templateUrl: './select-menu-item-popup.component.html',
  styleUrls: ['./select-menu-item-popup.component.css']
})
export class SelectMenuItemPopupComponent extends AppComponentBase implements OnInit {
  @Input() selectedValue: any | any[];
  @Output("changeValue") changeValue: EventEmitter<any> =
      new EventEmitter<any>();

  @ViewChild("modal", { static: true }) modal: ModalDirective;

  @ViewChild("dataTable", { static: true }) dataTable: Table;
  @ViewChild("paginator", { static: true }) paginator: Paginator;

  filterText = "";
  categoryId?:number = undefined;

  constructor(injector: Injector,
    private _menuItemService: MenuItemServiceProxy
    ) {
      super(injector);
      this.primengTableHelper = new PrimengTableHelper();
  }

  ngOnInit() {
    
  }


  show(id?: number, seletecdItem?:number): void {
    this.categoryId = id;
    if (seletecdItem) {
        this.selectedValue = seletecdItem;
    }
    this.modal.show();
    this.getMenuItems();
  }

  save() {
      this.changeValue.emit(this.selectedValue);
      this.selectedValue = null;
      this.close();
  }

  close() {
      this.modal.hide();
  }

  getMenuItems(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    
    this._menuItemService
        .getAll(
            this.filterText,
            false,
            this.categoryId,
            undefined,
            null,
            false,
            false,
            false,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
           
        ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
        .subscribe((result) => {
          this.primengTableHelper.totalRecordsCount = result.totalCount;
          this.primengTableHelper.records = result.items;
          this.primengTableHelper.hideLoadingIndicator();
      });
    }
       
}
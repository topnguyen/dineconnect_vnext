﻿import {
    Component,
    ViewChild,
    Injector,
    OnInit,
    Renderer2,
    ChangeDetectorRef,
} from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    PromotionsServiceProxy,
    CreateOrEditPromotionDto,
    PromotionScheduleEditDto,
    CommonLookupServiceProxy,
    PromotionRestrictItemEditDto,
    CategoryServiceProxy,
    DepartmentsServiceProxy,
    MenuItemServiceProxy,
    GetPromotionForEditOutput,
    FixedPromotionEditDto,
    FreePromotionEditDto,
    FreeItemPromotionExecutionEditDto,
    FreePromotionExecutionEditDto,
    FreeValuePromotionEditDto,
    FreeValuePromotionExecutionEditDto,
    DemandPromotionEditDto,
    DemandPromotionExecutionEditDto,
    TicketDiscountPromotionDto,
    FreeItemPromotionEditDto,
    CommonLocationGroupDto,
    ConnectCardServiceProxy,
    PagedResultDtoOfPromotionsDto,
    PromotionCategoryServiceProxy,
    PromotionQuotaDto,
    ComboboxItemDto,
    BuyXAndYAtZValuePromotionEditDto,
    BuyXAndYAtZValuePromotionExecutionEditDto,
    StepDiscountPromotionEditDto,
    StepDiscountPromotionExecutionEditDto,
    StepDiscountPromotionMappingExecutionEditDto,
    PromotionTypes,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { ActivatedRoute, Router } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { QueryBuilderConfig } from "angular2-query-builder";
import { SelectItemModalComponent } from "../select-item-modal/select-item-modal.component";
import { BehaviorSubject, Observable } from "rxjs";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { DatePipe } from "@angular/common";
import { SelectProductGroupModalComponent } from "./select-product-group-modal/select-product-group-modal.component";
import { SelectCategoryPopupComponent } from "./select-category-popup/select-category-popup.component";
import { SelectMenuItemPopupComponent } from "./select-menu-item-popup/select-menu-item-popup.component";
import { SelectPortionPopupComponent } from "./select-portion-popup/select-portion-popup.component";
import { forEach } from "lodash";
import { FlexAlignStyleBuilder } from "@angular/flex-layout";
@Component({
    selector: "create-or-edit-promotion",
    templateUrl: "./create-or-edit-promotion.component.html",
    styleUrls: ["./create-or-edit-promotion.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditPromotionComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("selectItemModal", { static: true })
    selectItemModal: SelectItemModalComponent;
    @ViewChild("selectPromotionModal", { static: true })
    selectPromotionModal: SelectPopupComponent;
    @ViewChild("selectProductGroupsModal", { static: true })
    selectProductGroupsModal: SelectProductGroupModalComponent;
    @ViewChild("selectCategoriesModal", { static: true })
    selectCategoriesModal: SelectCategoryPopupComponent;
    @ViewChild("selectMenuItemModal", { static: true })
    selectMenuItemModal: SelectMenuItemPopupComponent;
    @ViewChild("selectPortionsModal", { static: true })
    selectPortionsModal: SelectPortionPopupComponent;

    saving = false;
    currentIndex = undefined;
    isPromotionFreeFrom: boolean = false;
    restTypeList: ComboboxItemDto[] = [];
    dayOfWeekList: ComboboxItemDto[] = [];
    applyTypes: ComboboxItemDto[] = [];
    locationOrGroup: CommonLocationGroupDto;
    plantLocationOrGroup: CommonLocationGroupDto;
    currentIndexQuota: Number = null;

    promotion: CreateOrEditPromotionDto = new CreateOrEditPromotionDto();
    timerPromotion: any;
    fixedPromotionPer = [];
    fixedPromotion = [];
    distributions = [];
    priceTags = [];
    promotionTypeSelected: any;
    promotionCategorySelected: any;
    promotionCardTypeSelected: any;
    promotionPositionSelected: any;
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    promotionId: number;
    promotionTypes = [];
    dateRange: Date[] = [
        moment().endOf("day").toDate(),
        moment().endOf("day").toDate(),
    ];
    days = [];
    months = [];

    startTime = moment("00:00", "HH:mm").tz(this.timezone, true).toDate();
    endTime = moment("23:59", "HH:mm").tz(this.timezone, true).toDate();
    startDate: Date = moment().endOf("day").toDate();
    endDate: Date = moment().endOf("day").toDate();
    dayOfWeek = [];
    monthSchedules = [];

    query: any;

    config: QueryBuilderConfig = {
        fields: {},
    };

    isShowFilter = false;
    fromCountLocked = false;
    fromCountValueLocked = false;
    disabledSave = false;
    disabledSaveforLastTab = false;

    refpromotions = [];
    promotionValueTypes = [];
    departments = [];
    categories = [];
    groupProducts: ComboboxItemDto[] = [];
    stepDiscountTypes: ComboboxItemDto[] = [];
    portionMenuItemList = [];
    allMenuItems = [];
    cardTypes = [];
    promotionPositions = [];
    promotionCategories = [];
    freePromotion = new FreePromotionEditDto();
    freeValuePromotion = new FreeValuePromotionEditDto();
    freeItemPromotion = new FreeItemPromotionEditDto();
    demandPromotion = new DemandPromotionEditDto();
    ticketDiscountPromotion = new TicketDiscountPromotionDto();
    buyXAndYAtZValuePromotion = new BuyXAndYAtZValuePromotionEditDto();
    stepDiscountPromotion = new StepDiscountPromotionEditDto();
    stepDiscountExecutions = new StepDiscountPromotionExecutionEditDto();
    stepDiscountPromotionMapping =
        new StepDiscountPromotionMappingExecutionEditDto();

    minDate = moment().toDate();

    ranges = [];
    combinePros = "";
    combineProList = [];
    isFirstLoad = true;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _commonServiceProxy: CommonLookupServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _connectCardServiceProxy: ConnectCardServiceProxy,
        private render: Renderer2,
        private _datePipe: DatePipe,
        private cdr: ChangeDetectorRef
    ) {
        super(injector);
        this.getFilters();

        this.ranges = [
            {
                value: [new Date(), new Date()],
                label: this.l("Today"),
            },
            {
                value: [
                    new Date(new Date().setDate(new Date().getDate() + 1)),
                    new Date(new Date().setDate(new Date().getDate() + 1)),
                ],
                label: this.l("Tomorrow"),
            },
            {
                value: [
                    new Date(),
                    new Date(new Date().setDate(new Date().getDate() + 6)),
                ],
                label: this.l("Next7Days"),
            },
            {
                value: [
                    new Date(),
                    new Date(new Date().setDate(new Date().getDate() + 29)),
                ],
                label: this.l("Next30Days"),
            },
            {
                value: [
                    new Date(),
                    moment().tz(this.timezone, true).endOf("month").toDate(),
                ],
                label: this.l("ThisMonth"),
            },
            {
                value: [
                    moment()
                        .add(1, "month")
                        .tz(this.timezone, true)
                        .startOf("month")
                        .toDate(),
                    moment()
                        .add(1, "month")
                        .tz(this.timezone, true)
                        .endOf("month")
                        .toDate(),
                ],
                label: this.l("NextMonth"),
            },
        ];
    }
    ngAfterViewChecked() {
        //your code to update the model
        this.cdr.detectChanges();
    }
    ngOnInit() {
        this.renderMonthsLookup();
        this._activatedRoute.params.subscribe((params) => {
            this.promotionId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.promotionId)) {
                this.promotionId = undefined;
            }
            this._promotionsServiceProxy
                .getPromotionForEdit(this.promotionId)
                .pipe(
                    finalize(() => {
                        setTimeout(() => {
                            this.isFirstLoad = false;
                        }, 300);
                    })
                )
                .subscribe((result) => {
                    this.promotion = result.promotion;
                    this.getDataForPromotion();
                    this.getPromotionTypes(result);
                    this.getDataForCombobox(result);
                    this.getComboboxForSchedule(result);
                    this.timerPromotion = result.timerPromotion;
                    this.fixedPromotionPer = result.fixPromotionsPer;

                    this.fixedPromotion = result.fixPromotions;
                    this.distributions = result.distributions;
                    this.freePromotion = result.freePromotion
                        ? result.freePromotion
                        : new FreePromotionEditDto();
                    this.freeValuePromotion = result.freeValuePromotion
                        ? result.freeValuePromotion
                        : new FreeValuePromotionEditDto();
                    this.demandPromotion = result.demandPromotion
                        ? result.demandPromotion
                        : new DemandPromotionEditDto();
                    this.freeItemPromotion = result.freeItemPromotion
                        ? result.freeItemPromotion
                        : new FreeItemPromotionEditDto();
                    this.ticketDiscountPromotion =
                        result.ticketDiscountPromotion
                            ? result.ticketDiscountPromotion
                            : new TicketDiscountPromotionDto();

                    this.buyXAndYAtZValuePromotion =
                        result.buyXAndYAtZValuePromotion
                            ? result.buyXAndYAtZValuePromotion
                            : new BuyXAndYAtZValuePromotionEditDto();
                    this.stepDiscountPromotion = result.stepDiscountPromotion
                        ? result.stepDiscountPromotion
                        : new StepDiscountPromotionEditDto();
                    this.setScheduleDatetime();
                    this.getComboboxByPromotionType(
                        this.promotion.promotionTypeId
                    );
                });
        });
    }

    private getDataForPromotion() {
        this.query = JSON.parse(this.promotion.filter);
        if (!this.promotionId) {
            this.promotion.startDate = moment();
            this.promotion.endDate = moment();
            this.promotion.promotionSchedules = [];
            this.query = {
                condition: "and",
                rules: [
                    {
                        field: "Total",
                        operator: "greater_or_equal",
                        value: "0",
                    },
                ],
            };
        }
        if (this.promotion.id) {
            this.getDateTime();
        }

        this.promotion.locationGroup = this.promotion.locationGroup
            ? this.promotion.locationGroup
            : new CommonLocationGroupDto();

        if (this.promotion.combineProValues) {
            if (this.promotion.combineProValues == "*") {
                this.combinePros = this.promotion.combineProValues;
            } else {
                let names = [];
                this.combineProList = JSON.parse(
                    this.promotion.combineProValues
                );

                this.combineProList.forEach((cp) => {
                    names.push(cp.id + " - " + cp.name);
                });
                this.combinePros = names.join(", ");
            }
        }
    }

    showedDateRangePicker(): void {
        let button = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-predefined-btns"
        ).lastElementChild;
        let self = this;
        let el = document.querySelector(
            "bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation"
        );
        self.render.removeClass(el, "show");

        button.addEventListener("click", (event) => {
            event.preventDefault();

            self.render.addClass(el, "show");
        });
    }
    validFrom() {
        let isValid = true;
        isValid = this.validQuoataPromotion();
        if (!isValid) {
            return isValid;
        }
        if (this.promotion.promotionTypeId == PromotionTypes.TicketDiscount) {
            let isValidTotal = this.checkValidDistributions();
            if (!isValidTotal) {
                this.notify.error(this.l("Total Percentage should be 100"));
                isValid = false;
            }
        }
        return isValid;
    }

    save(): void {
        if (!this.validFrom()) {
            return;
        }
        this.saving = true;
        this.promotion.filter = JSON.stringify(this.query);
        this.promotion.combineProValues =
            this.combineProList && this.combineProList.length > 0
                ? JSON.stringify(this.combineProList)
                : null;
        this.setDateTime();

        this.promotion.connectCardTypeId =
            this.promotion.connectCardTypeId > 0
                ? this.promotion.connectCardTypeId
                : null;
        let input = new GetPromotionForEditOutput();
        input.promotion = this.promotion;
        input.timerPromotion = this.timerPromotion;
        input.fixPromotionsPer = this.fixedPromotionPer;
        input.fixPromotions = this.fixedPromotion;
        input.freePromotion = this.freePromotion;
        input.freeValuePromotion = this.freeValuePromotion;
        input.demandPromotion = this.demandPromotion;
        input.freeItemPromotion = this.freeItemPromotion;
        input.ticketDiscountPromotion = this.ticketDiscountPromotion;
        input.buyXAndYAtZValuePromotion = this.buyXAndYAtZValuePromotion;
        input.stepDiscountPromotion = this.stepDiscountPromotion;
        input.distributions = this.distributions;

        this._promotionsServiceProxy
            .createOrEdit(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/discount/promotions"]);
    }

    private checkPromotionDateValid() {
        let currentDate = this._datePipe.transform(
            moment().toDate(),
            "yyy-MM-dd"
        );
        let startDate = this._datePipe.transform(
            this.promotion.startDate.toDate(),
            "yyy-MM-dd"
        );
        if (startDate <= currentDate) {
            return false;
        }
        return true;
    }

    private getPromotionTypes(data) {
        this.promotionTypes = data.promotionTypes.items;
        if (this.promotion.id) {
            this.promotionTypeSelected = this.promotionTypes.find(
                (x) => x.value == this.promotion.promotionTypeId
            );
        }
        this.promotionCategories = data.promotionCategories.items;
        if (this.promotion.id) {
            this.promotionCategorySelected = this.promotionCategories.find(
                (x) => x.id == this.promotion.promotionCategoryId
            );
        }
        // this.refpromotions = data.portions.items;
        this.promotionValueTypes = data.promotionValueTypes.items;

        this.promotionPositions = data.promotionPositions.items;
        if (this.promotion.id) {
            this.promotionPositionSelected = this.promotionPositions.find(
                (x) => x.value == this.promotion.promotionPosition
            );
        }

        this.days = data.dayOfWeeks.items;

        this.cardTypes = data.cardTypes.items;
        if (this.promotion.id) {
            this.promotionCardTypeSelected = this.cardTypes.find(
                (x) => x.value == this.promotion.connectCardTypeId
            );
        }
    }

    getComboboxForSchedule(data) {
        this.dayOfWeekList = data.dayOfWeeks.items.map((item) =>
            ComboboxItemDto.fromJS({
                value: +item.value,
                displayText: item.displayText,
                isSelected: false,
            })
        );
        this.restTypeList = data.restTypeList.items.map((item) =>
            ComboboxItemDto.fromJS({
                value: +item.value,
                displayText: item.displayText,
                isSelected: false,
            })
        );
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.promotion.locationGroup);
    }

    openSelectPromotionModal() {
        this.selectPromotionModal.show(this.combineProList);
    }

    getPromotions() {
        return (
            filterText,
            isDeleted,
            productComboSelectionIds,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfPromotionsDto> =>
            this._promotionsServiceProxy.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    promotionsChange(items) {
        this.combineProList = items;
        this.promotion.combineProValues = JSON.stringify(this.combineProList);
        let names = [];
        this.combineProList.forEach((cp) => {
            names.push(cp.id + " - " + cp.name);
        });
        this.combinePros = names.join(", ");
    }

    getLocations(event) {
        this.locationOrGroup = event;
        this.promotion.locationGroup = this.locationOrGroup;
        if (this.plantLocationOrGroup != null) {
            this.plantLocationOrGroup = event;
            this.promotion.promotionQuotas[this.currentIndex].locationGroup =
                this.plantLocationOrGroup;
            let plants = this.plantLocationOrGroup.locations
                .map((item) => {
                    return item.name;
                })
                .join(", ");
            this.promotion.promotionQuotas[this.currentIndex].plants = plants;
            this.promotion.promotionQuotas[this.currentIndex].locations =
                JSON.stringify(this.plantLocationOrGroup.locations);
        }
    }

    private getDateTime() {
        this.dateRange = [
            this.promotion.startDate.toDate(),
            this.promotion.endDate.toDate(),
        ];
    }

    private setDateTime() {
        this.promotion.startDate = moment(this.dateRange[0]).endOf("day");
        this.promotion.endDate = moment(this.dateRange[1]).startOf("day");
    }

    removeSchedule(index) {
        this.promotion.promotionSchedules.splice(index, 1);
    }

    addSchedule() {
        let schedule = Object.assign(new PromotionScheduleEditDto(), {
            startDate: this.startDate,
            endDate: this.endDate,
            startHour: moment(this.startTime).tz(this.timezone).format("HH"),
            startMinute: moment(this.startTime).tz(this.timezone).format("mm"),
            endHour: moment(this.endTime).tz(this.timezone).format("HH"),
            endMinute: moment(this.endTime).tz(this.timezone).format("mm"),
            days: this.dayOfWeek
                .map((item) => {
                    return item.value;
                })
                .join(","),
            dayNames: this.dayOfWeek
                .map((item) => {
                    return item.displayText;
                })
                .join(","),
            monthDays: this.monthSchedules
                .map((item) => {
                    return item.value;
                })
                .join(","),
        });

        this.promotion.promotionSchedules.push(schedule);

        this.initSchedule();
    }

    addRestrictItems() {
        this.promotion.promotionRestrictItems.push(
            Object.assign(new PromotionRestrictItemEditDto(), {
                id: undefined,
                menuItemId: undefined,
                name: "",
            })
        );
        this.isDisabledSave();
    }

    removeRestrictItem(index) {
        this.promotion.promotionRestrictItems.splice(index, 1);
        this.isDisabledSave();
    }

    openforMenuItem(promotionItem) {
        this.selectItemModal.promotionRestrictItem = promotionItem;
        this.selectItemModal.id = promotionItem.menuItemId;
        this.selectItemModal.show(this.promotion.promotionRestrictItems);
    }
   
    itemSelected(event) {
        event.id = 0;
        this.promotion.promotionRestrictItems[this.currentIndex] = event;
        this.isDisabledSave();
    }
    private getFilters() {
        this._commonServiceProxy.getConditionFilter().subscribe((result) => {
            this.config = {
                fields: JSON.parse(result),
            };
        });
    }

    addFixProsPer() {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer.push(
                    Object.assign(new FixedPromotionEditDto(), {
                        promotionValueType: 1,
                        promotionValue: 0,
                    })
                );
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions.push(
                    Object.assign(new FixedPromotionEditDto(), {
                        promotionValueType: 1,
                        promotionValue: 0,
                    })
                );
                break;
        }
    }

    removeFixProsPer(productIndex) {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer.splice(productIndex, 1);
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions.splice(productIndex, 1);
                break;
        }
    }

    addFixPros() {
        this.fixedPromotion.push(
            Object.assign(new FixedPromotionEditDto(), {
                promotionValueType: 2,
                promotionValue: 0,
            })
        );
    }

    removeFixPros(productIndex) {
        this.fixedPromotion.splice(productIndex, 1);
    }

    addBuyXAndYAtZValue() {
        this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions.push(
            Object.assign(new BuyXAndYAtZValuePromotionExecutionEditDto(), {
                promotionValueType: 11,
            })
        );
        this.disabledForLastTab(
            this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions
        );
    }

    removeBuyXAndYAtZValue(productIndex) {
        this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions.splice(
            productIndex,
            1
        );
        this.disabledForLastTab(
            this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions
        );
    }

    getDataForCombobox(data) {
        this._categoryServiceProxy
            .getCategoriesForCombobox()
            .subscribe((result) => {
                this.categories = result.items;
            });

        this._connectCardServiceProxy
            .getConnectCardTypes()
            .subscribe((result) => {
                this.cardTypes = result;
                if (this.promotion.id) {
                    this.promotionCardTypeSelected = this.cardTypes.find(
                        (x) => x.value == this.promotion.connectCardTypeId
                    );
                }
            });

        this.getMenuItems();
    }

    getMenuItems(categoryId?, index?) {
        if (this.currentIndex != index) {
            this.currentIndex = index || 0;
        }
    }

    changeFromCount(status) {
        this.fromCountLocked = status;
        if (status) {
            this.freePromotion.fromCount =
                this.freePromotion.from.length === 0
                    ? 1
                    : this.freePromotion.from.length;
        }
    }

    changeFromValueCount(status) {
        if (status) {
            if (this.freeValuePromotion.from.length === 0) {
                this.freeValuePromotion.fromCount = 1;
            } else {
                this.freeValuePromotion.fromCount =
                    this.freeValuePromotion.from.length;
            }
            this.fromCountValueLocked = true;
        } else {
            this.fromCountValueLocked = false;
        }
    }

    addFreeItem() {
        this.freeItemPromotion.executions.push(
            Object.assign(new FreeItemPromotionExecutionEditDto(), {
                // menuItemPortionId: undefined,
            })
        );
        this.disabledForLastTab(this.freeItemPromotion.executions);
    }

    removeFreeItem(productIndex) {
        this.freeItemPromotion.executions.splice(productIndex, 1);
        this.disabledForLastTab(this.freeItemPromotion.executions);
    }

    addFreeFrom() {
        this.freePromotion.from.push(
            Object.assign(new FreePromotionExecutionEditDto(), {
                menuItemPortionId: undefined,
                from: true,
            })
        );
        this.disabledForLastTab(this.freePromotion.from);
    }

    removeFreeFrom(productIndex) {
        this.freePromotion.from.splice(productIndex, 1);
        this.disabledForLastTab(this.freePromotion.from);
    }

    addFreeTo() {
        this.freePromotion.to.push(
            Object.assign(new FreePromotionExecutionEditDto(), {
                menuItemPortionId: undefined,
                from: false,
            })
        );
        this.disabledForLastTab(this.freePromotion.to);
    }

    removeFreeTo(productIndex) {
        this.freePromotion.to.splice(productIndex, 1);
        this.disabledForLastTab(this.freePromotion.to);
    }

    addFreeValueFrom() {
        this.freeValuePromotion.from.push(
            Object.assign(new FreeValuePromotionExecutionEditDto(), {
                menuItemPortionId: undefined,
                from: true,
            })
        );
        this.disabledForLastTab(this.freeValuePromotion.from);
    }

    removeFreeValueFrom(productIndex) {
        this.freeValuePromotion.from.splice(productIndex, 1);
        this.disabledForLastTab(this.freeValuePromotion.from);
    }

    addFreeValueTo() {
        this.freeValuePromotion.to.push(
            Object.assign(new FreeValuePromotionExecutionEditDto(), {
                menuItemPortionId: undefined,
                from: false,
            })
        );
        this.disabledForLastTab(this.freeValuePromotion.to);
    }

    removeFreeValueTo(productIndex) {
        this.freeValuePromotion.to.splice(productIndex, 1);
        this.disabledForLastTab(this.freeValuePromotion.to);
    }

    addDemandPromosExes() {
        this.demandPromotion.demandPromotionExecutions.push(
            Object.assign(new DemandPromotionExecutionEditDto(), {
                // categoryId: "",
                // menuItemId: "",
            })
        );
    }

    removeDemandPromosExes(productIndex) {
        this.demandPromotion.demandPromotionExecutions.splice(productIndex, 1);
    }

    promotionTypeChanged() {
        PromotionTypes;
        this.promotion.promotionTypeId = this.promotionTypeSelected
            ? +this.promotionTypeSelected.value
            : 0;

        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.BuyXItemGetYItemFree:
                this.freePromotion.from = [];
                this.freePromotion.to = [];
                this.addFreeFrom();
                this.addFreeTo();
                break;
            case PromotionTypes.BuyXItemGetYatZValue:
                this.freeValuePromotion.from = [];
                this.freeValuePromotion.to = [];
                this.addFreeValueFrom();
                this.addFreeValueTo();
                break;
            case PromotionTypes.FreeItems:
                this.freeItemPromotion.executions = [];
                this.addFreeItem();
                break;
            case PromotionTypes.TicketDiscount:
            case PromotionTypes.PaymentTicketDiscount:
                this.ticketDiscountPromotion = new TicketDiscountPromotionDto();
                break;
            case PromotionTypes.OrderDiscount:
            case PromotionTypes.PaymentOrderDiscount:
                this.demandPromotion = new DemandPromotionEditDto();
                this.demandPromotion.demandPromotionExecutions = [];
                break;
            case PromotionTypes.PaymentTicketDiscount:
                break;
            case PromotionTypes.BuyXAndYAtZValue:
                break;
            case PromotionTypes.StepDiscount:
                break;
            default:
                break;
        }
        this.getComboboxByPromotionType(this.promotion.promotionTypeId);
    }

    isDisabledSave() {
        this.disabledSave = false;
        this.promotion.promotionRestrictItems.map((item) => {
            if (!item.menuItemId) {
                this.disabledSave = true;
            }
        });
    }

    disabledForLastTab(items: any[]) {
        this.disabledSaveforLastTab = false;
        this.disabledSaveforLastTab = !items.length;
    }
    get formIsValid() {
        const promotionTypeId = Number(this.promotion.promotionTypeId);
        if (this.promotionTypes.length === 0 || isNaN(promotionTypeId)) {
            return false;
        }
        if (promotionTypeId == PromotionTypes.NotAssigned) {
            return false;
        }

        if (promotionTypeId === PromotionTypes.HappyHour) {
            // HappyHour
            const priceTagId = Number(this.timerPromotion.priceTagId);
            return !!priceTagId;
        }
        return true;
    }
    promotionCardTypeChanged() {
        this.promotion.connectCardTypeId =
            +this.promotionCardTypeSelected?.value;
    }
    promotionCategoryChanged() {
        this.promotion.promotionCategoryId = this.promotionCategorySelected?.id;
    }
    promotionPositionChanged() {
        this.promotion.promotionPosition =
            +this.promotionPositionSelected?.value;
    }

    clearPromotion() {
        this.combinePros = "";
        this.combineProList = [];
    }
    clearPromitionPositon(value) {
        if (!value) {
            this.promotionPositionSelected = null;
            this.promotion.promotionPosition = null;
        }
    }
    renderMonthsLookup() {
        for (let i = 1; i <= 31; i++) {
            this.months.push({
                displayText: i,
                value: i,
            });
        }
    }
    changeDaterange() {
        if (!this.isFirstLoad) {
            this.promotion.promotionSchedules = [];
        }
        this.setScheduleDatetime();
    }

    setScheduleDatetime() {
        this.startDate = moment(this.dateRange[0]).toDate();
        this.endDate = moment(this.dateRange[1]).toDate();
    }
    changeDateSchedules(type: string) {
        var startDateFormat = this.formatDateToString(
            this.startDate,
            "yyyy-MM-dd"
        );
        var endDateFormat = this.formatDateToString(this.endDate, "yyyy-MM-dd");
        var startDatePromotionFormat = this.formatDateToString(
            moment(this.dateRange[0]).toDate(),
            "yyyy-MM-dd"
        );
        var endDatePromotionFormat = this.formatDateToString(
            moment(this.dateRange[1]).toDate(),
            "yyyy-MM-dd"
        );
        switch (type) {
            case "start":
                if (
                    startDateFormat < startDatePromotionFormat ||
                    startDateFormat > endDatePromotionFormat
                ) {
                    this.notify.error(
                        this.l(
                            "Schedule Start Date should within a Promotion Date Range"
                        )
                    );
                    this.startDate = null;
                }
                break;
            case "end":
                if (
                    endDateFormat < startDatePromotionFormat ||
                    endDateFormat > endDatePromotionFormat
                ) {
                    this.notify.error(
                        this.l(
                            "Schedule End Date should within a Promotion Date Range"
                        )
                    );
                    this.endDate = null;
                }
                break;
        }
    }

    private formatDateToString(value, type) {
        return this._datePipe.transform(value, type);
    }

    private initSchedule() {
        this.startTime = moment("00:00", "HH:mm")
            .tz(this.timezone, true)
            .toDate();
        this.endTime = moment("23:59", "HH:mm")
            .tz(this.timezone, true)
            .toDate();
        this.dayOfWeek = [];
        this.monthSchedules = [];
        this.setScheduleDatetime();
    }
    addRowQuota() {
        if (!this.validQuoataPromotion()) {
            return;
        }
        let quota = new PromotionQuotaDto();
        this.promotion.promotionQuotas.push(quota);
    }

    deleteQuota(rowIndex: any) {
        this.promotion.promotionQuotas.splice(rowIndex, 1);
    }

    openPlantSelectLocationsModal(index) {
        if (this.currentIndex != index) {
            this.currentIndex = index;
            this.plantLocationOrGroup = new CommonLocationGroupDto();
        }

        this.selectLocationOrGroup.show(
            this.promotion.promotionQuotas[this.currentIndex].locationGroup
        );
    }
    getComboboxByPromotionType(type) {
        this._promotionsServiceProxy
            .getComboboxByPromotionType(type)
            .subscribe((result) => {
                switch (type) {
                    case PromotionTypes.HappyHour:
                        this.priceTags = result.priceTags.items;
                        break;
                    case PromotionTypes.OrderDiscount:
                    case PromotionTypes.PaymentOrderDiscount:
                    case PromotionTypes.StepDiscount:
                        this.applyTypes = result.applyTypes.items;
                        if (type == 12) {
                            this.promotionValueTypes =
                                result.promotionValueTypes.items;
                            this.stepDiscountTypes =
                                result.stepDiscountTypes.items;
                        }
                        break;
                    case PromotionTypes.TicketDiscount:
                        this.applyTypes = result.applyTypes.items;
                        break;
                }
            });
    }
    addStepDiscountPromotion() {
        if (!this.stepDiscountPromotion.stepDiscountPromotionExecutions) {
            this.stepDiscountPromotion.stepDiscountPromotionExecutions = [];
        }

        this.stepDiscountPromotion.stepDiscountPromotionExecutions.push(
            this.stepDiscountExecutions
        );
        this.stepDiscountExecutions =
            new StepDiscountPromotionExecutionEditDto();
    }
    removeStepDiscountPromotion(index) {
        this.stepDiscountPromotion.stepDiscountPromotionExecutions.splice(
            index,
            1
        );
    }

    addStepDiscountPromotionMappingExecution() {
        if (
            !this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions
        ) {
            this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions =
                [];
        }
        this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions.push(
            this.stepDiscountPromotionMapping
        );
        this.stepDiscountPromotionMapping =
            new StepDiscountPromotionMappingExecutionEditDto();
    }
    removeStepDiscountPromotionMappingExecution(index) {
        this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions.splice(
            index,
            1
        );
    }

    openProductGroups() {
        this.selectProductGroupsModal.show();
    }
    opencategories(id?: number, seletecdItem?: number) {
        id = id ? id : undefined;
        this.selectCategoriesModal.show(id, seletecdItem);
    }
    openMenuItems(id?: number, seletecdItem?: number) {
        id = id ? id : undefined;
        this.selectMenuItemModal.show(id, seletecdItem);
    }
    openPortions(id?: number, seletecdItem?: number) {
        id = id ? id : undefined;
        this.selectPortionsModal.show(id, seletecdItem);
    }

    getProductGroups(event) {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.NotAssigned:
                break;
            case PromotionTypes.HappyHour:
                break;
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer[this.currentIndex].productGroupId =
                    event.id;
                this.fixedPromotionPer[this.currentIndex].productGroupName =
                    event.name;
                break;
            case PromotionTypes.FixedDiscountValue:
                this.fixedPromotion[this.currentIndex].productGroupId =
                    event.id;
                this.fixedPromotion[this.currentIndex].productGroupName =
                    event.name;
                break;
            case PromotionTypes.BuyXItemGetYItemFree:
                if (this.isPromotionFreeFrom) {
                    this.freePromotion.from[this.currentIndex].productGroupId =
                        event.id;
                    this.freePromotion.from[
                        this.currentIndex
                    ].productGroupName = event.name;
                } else {
                    this.freePromotion.to[this.currentIndex].productGroupId =
                        event.id;
                    this.freePromotion.to[this.currentIndex].productGroupName =
                        event.name;
                }
                break;
            case PromotionTypes.BuyXItemGetYatZValue:
                if (this.isPromotionFreeFrom) {
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].productGroupId = event.id;
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].productGroupName = event.name;
                } else {
                    this.freeValuePromotion.to[
                        this.currentIndex
                    ].productGroupId = event.id;
                    this.freeValuePromotion.to[
                        this.currentIndex
                    ].productGroupName = event.name;
                }
                break;
            case PromotionTypes.OrderDiscount:
            case PromotionTypes.PaymentOrderDiscount:
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].productGroupId = event.id;
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].productGroupName = event.name;
                break;
            case PromotionTypes.FreeItems:
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions[this.currentIndex].productGroupId = event.id;
                this.distributions[this.currentIndex].productGroupName =
                    event.name;
                break;
            case PromotionTypes.PaymentTicketDiscount:
                break;
            case PromotionTypes.BuyXAndYAtZValue:
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].productGroupId = event.id;
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].productGroupName = event.name;
                break;
            case PromotionTypes.StepDiscount:
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].productGroupId = event.id;
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].productGroupName = event.name;
                break;
        }
    }
    getCategories(event) {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.NotAssigned:
                break;
            case PromotionTypes.HappyHour:
                break;
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer[this.currentIndex].categoryId = event.id;
                this.fixedPromotionPer[this.currentIndex].categoryName =
                    event.name;
                break;
            case PromotionTypes.FixedDiscountValue:
                this.fixedPromotion[this.currentIndex].categoryId = event.id;
                this.fixedPromotion[this.currentIndex].categoryName =
                    event.name;
                break;
            case PromotionTypes.BuyXItemGetYItemFree:
                if (this.isPromotionFreeFrom) {
                    this.freePromotion.from[this.currentIndex].categoryId =
                        event.id;
                    this.freePromotion.from[this.currentIndex].categoryName =
                        event.name;
                } else {
                    this.freePromotion.to[this.currentIndex].categoryId =
                        event.id;
                    this.freePromotion.to[this.currentIndex].categoryName =
                        event.name;
                }
                break;
            case PromotionTypes.BuyXItemGetYatZValue:
                if (this.isPromotionFreeFrom) {
                    this.freeValuePromotion.from[this.currentIndex].categoryId =
                        event.id;
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].categoryName = event.name;
                } else {
                    this.freeValuePromotion.to[this.currentIndex].categoryId =
                        event.id;
                    this.freeValuePromotion.to[this.currentIndex].categoryName =
                        event.name;
                }
                break;
            case PromotionTypes.OrderDiscount:
            case PromotionTypes.PaymentOrderDiscount:
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].categoryId = event.id;
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].categoryName = event.name;
                break;
            case PromotionTypes.FreeItems:
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions[this.currentIndex].categoryId = event.id;
                this.distributions[this.currentIndex].categoryName = event.name;
                break;
            case PromotionTypes.PaymentTicketDiscount:
                break;
            case PromotionTypes.BuyXAndYAtZValue:
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].categoryId = event.id;
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].categoryName = event.name;
                break;
            case PromotionTypes.StepDiscount:
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].categoryId = event.id;
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].categoryName = event.name;
                break;
        }
    }

    getMenuItem(event) {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.NotAssigned:
                break;
            case PromotionTypes.HappyHour:
                break;
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer[this.currentIndex].menuItemId = event.id;
                this.fixedPromotionPer[this.currentIndex].menuItemName =
                    event.name;
                break;
            case PromotionTypes.FixedDiscountValue:
                this.fixedPromotion[this.currentIndex].menuItemId = event.id;
                this.fixedPromotion[this.currentIndex].menuItemName =
                    event.name;
                break;
            case PromotionTypes.BuyXItemGetYItemFree:
                if (this.isPromotionFreeFrom) {
                    this.freePromotion.from[this.currentIndex].menuItemId =
                        event.id;
                    this.freePromotion.from[this.currentIndex].menuItemName =
                        event.name;
                } else {
                    this.freePromotion.to[this.currentIndex].menuItemId =
                        event.id;
                    this.freePromotion.to[this.currentIndex].menuItemName =
                        event.name;
                }
                break;
            case PromotionTypes.BuyXItemGetYatZValue:
                if (this.isPromotionFreeFrom) {
                    this.freeValuePromotion.from[this.currentIndex].menuItemId =
                        event.id;
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].menuItemName = event.name;
                } else {
                    this.freeValuePromotion.to[this.currentIndex].menuItemId =
                        event.id;
                    this.freeValuePromotion.to[this.currentIndex].menuItemName =
                        event.name;
                }
                break;
            case PromotionTypes.OrderDiscount:
            case PromotionTypes.PaymentOrderDiscount:
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].menuItemId = event.id;
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].menuItemName = event.name;
                break;
            case PromotionTypes.FreeItems:
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions[this.currentIndex].menuItemId = event.id;
                this.distributions[this.currentIndex].menuItemName = event.name;
                break;
            case PromotionTypes.PaymentTicketDiscount:
                break;
            case PromotionTypes.BuyXAndYAtZValue:
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].menuItemId = event.id;
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].menuItemName = event.name;
                break;
            case PromotionTypes.StepDiscount:
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].menuItemId = event.id;
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].menuItemName = event.name;
                break;
        }
    }
    getPortions(event) {
        switch (this.promotion.promotionTypeId) {
            case PromotionTypes.NotAssigned:
                break;
            case PromotionTypes.HappyHour:
                break;
            case PromotionTypes.FixedDiscountPercentage:
                this.fixedPromotionPer[this.currentIndex].menuItemPortionId =
                    event.id;
                this.fixedPromotionPer[this.currentIndex].portionName =
                    event.name;
                break;
            case PromotionTypes.FixedDiscountValue:
                this.fixedPromotion[this.currentIndex].menuItemPortionId =
                    event.id;
                this.fixedPromotion[this.currentIndex].portionName = event.name;
                break;
            case PromotionTypes.BuyXItemGetYItemFree:
                if (this.isPromotionFreeFrom) {
                    this.freePromotion.from[
                        this.currentIndex
                    ].menuItemPortionId = event.id;
                    this.freePromotion.from[this.currentIndex].portionName =
                        event.name;
                } else {
                    this.freePromotion.to[this.currentIndex].menuItemPortionId =
                        event.id;
                    this.freePromotion.to[this.currentIndex].portionName =
                        event.name;
                }
                break;
            case PromotionTypes.BuyXItemGetYatZValue:
                if (this.isPromotionFreeFrom) {
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].menuItemPortionId = event.id;
                    this.freeValuePromotion.from[
                        this.currentIndex
                    ].portionName = event.name;
                } else {
                    this.freeValuePromotion.to[
                        this.currentIndex
                    ].menuItemPortionId = event.id;
                    this.freeValuePromotion.to[this.currentIndex].portionName =
                        event.name;
                }
                break;
            case PromotionTypes.OrderDiscount:
            case PromotionTypes.PaymentOrderDiscount:
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].menuItemPortionId = event.id;
                this.demandPromotion.demandPromotionExecutions[
                    this.currentIndex
                ].portionName = event.name;
                break;
            case PromotionTypes.FreeItems:
                this.freeItemPromotion.executions[
                    this.currentIndex
                ].menuItemPortionId = event.id;
                this.freeItemPromotion.executions[
                    this.currentIndex
                ].portionName = event.name;
                break;
            case PromotionTypes.TicketDiscount:
                this.distributions[this.currentIndex].menuItemPortionId =
                    event.id;
                this.distributions[this.currentIndex].portionName = event.name;
                break;
            case PromotionTypes.PaymentTicketDiscount:
                break;
            case PromotionTypes.BuyXAndYAtZValue:
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].menuItemPortionId = event.id;
                this.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions[
                    this.currentIndex
                ].portionName = event.name;
                break;
            case PromotionTypes.StepDiscount:
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].menuItemPortionId = event.id;
                this.stepDiscountPromotion.stepDiscountPromotionMappingExecutions[
                    this.currentIndex
                ].portionName = event.name;
                break;
        }
    }
    checkValidDistributions() {
        if (this.distributions.length > 0) {
            let total = this.getTotalDistribution();
            if (total != 100) {
                return false;
            }
        }
        return true;
    }

    private getTotalDistribution() {
        let total = 0;

        this.distributions.forEach((item) => {
            total += Number(item.promotionValue);
        });
        return total;
    }

    validQuoataPromotion() {
        let isValid = true;
        if (
            this.promotion.promotionQuotas != null &&
            this.promotion.promotionQuotas.length > 0
        ) {
            for (let i = 0; i < this.promotion.promotionQuotas.length; i++) {
                let item = this.promotion.promotionQuotas[i];
                if (item.resetType == undefined) {
                    this.notify.error(this.l("Invalid Quota data"));
                    isValid = false;
                    break;
                }
                if (
                    (item.resetType == 2 || item.resetType == 3) &&
                    item.resetMonthValue == null
                ) {
                    this.notify.error(this.l("Invalid Quota Reset Value"));
                    isValid = false;
                    break;
                }
            }
        }
        return isValid;
    }
}

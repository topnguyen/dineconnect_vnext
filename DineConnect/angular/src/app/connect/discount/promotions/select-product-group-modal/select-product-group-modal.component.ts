import {
    Component,
    EventEmitter,
    Injector,
    OnInit,
    Output,
    ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CategoryServiceProxy,
    ProductGroupsServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { ArrayToTreeConverterService } from "@shared/utils/array-to-tree-converter.service";
import { TreeDataHelperService } from "@shared/utils/tree-data-helper.service";
import { ModalDirective } from "ngx-bootstrap/modal";
import { TreeNode, MenuItem, Paginator, Table, LazyLoadEvent } from "primeng";

@Component({
    selector: "select-product-group-modal",
    templateUrl: "./select-product-group-modal.component.html",
    styleUrls: ["./select-product-group-modal.component.css"],
})
export class SelectProductGroupModalComponent
    extends AppComponentBase
    implements OnInit
{
    @Output() selectedProductGroup: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("modal", { static: false }) modal: ModalDirective;
    treeData: any;
    selectedGroup: TreeNode;

    filterText = "";
    isDeleted;
    selectedCategory = [];

    items: MenuItem[];
    active = false;
    saving = false;
    constructor(
        injector: Injector,
        private _productGroupsServiceProxy: ProductGroupsServiceProxy,
        private _arrayToTreeConverterService: ArrayToTreeConverterService,
        private _treeDataHelperService: TreeDataHelperService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getProductGroups();
    }

    getProductGroups() {
        this._productGroupsServiceProxy.getAll().subscribe((result) => {
            let self = this;

            this.treeData = this._arrayToTreeConverterService.createTree(
                result.items,
                "parentId",
                "id",
                null,
                "children",
                [
                    {
                        target: "styleClass",
                        value: "category-tree-node",
                    },
                    {
                        target: "label",
                        targetFunction(item) {
                            return item.name;
                        },
                    },
                    {
                        target: "selectable",
                        value: true,
                    },
                    {
                        target: "expandedIcon",
                        value: "fa fa-folder-open m--font-warning",
                    },
                    {
                        target: "collapsedIcon",
                        value: "fa fa-folder m--font-warning",
                    },
                    {
                        target: "expanded",
                        value: true,
                    },
                    {
                        target: "categoriesCount",
                        targetFunction(item) {
                            return self.setCategoriesCount(item);
                        },
                    },
                    {
                        target: "categories",
                        targetFunction(item) {
                            return item.categories;
                        },
                    },
                ]
            );

            if (this.selectedGroup) {
                this.selectedGroup = this._treeDataHelperService.findNode(
                    this.treeData,
                    { data: { id: this.selectedGroup?.data.id } }
                );
            }
        });
    }

    setCategoriesCount(item) {
        return item.categories?.length;
    }

    nodeSelect(event: any) {
        console.log("selectedGroup", this.selectedGroup.data);
    }

    show() {
        this.active = true;
        this.modal.show();
    }

    close() {
        this.modal.hide();
    }
    save() {
        this.saving = true;
        this.active = false;
        this.selectedProductGroup.emit(this.selectedGroup.data);
        this.saving = false;
        this.selectedGroup = null;
        this.close();
    }
}

import {
    Component,
    EventEmitter,
    Injector,
    Input,
    OnInit,
    Output,
    ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { PrimengTableHelper } from "@shared/helpers/PrimengTableHelper";
import { MenuItemServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";

@Component({
    selector: "select-portion-popup",
    templateUrl: "./select-portion-popup.component.html",
    styleUrls: ["./select-portion-popup.component.css"],
})
export class SelectPortionPopupComponent
    extends AppComponentBase
    implements OnInit
{
    @Input() selectedValue: any | any[];
    @Output("changeValue") changeValue: EventEmitter<any> =
        new EventEmitter<any>();

    @ViewChild("modal", { static: true }) modal: ModalDirective;

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    menuItemId?: any;

    constructor(
        injector: Injector,
        private _menuItemService: MenuItemServiceProxy
    ) {
        super(injector);
        this.primengTableHelper = new PrimengTableHelper();
    }
    ngOnInit() {}
    show(id?: number, seletecdItem?: number): void {
        this.menuItemId = id;
        if (seletecdItem) {
            this.selectedValue = seletecdItem;
        }
        this.modal.show();
        this.getPortions();
    }

    save() {
        this.changeValue.emit(this.selectedValue);
        this.selectedValue = null;
        this.close();
    }

    close() {
        this.modal.hide();
    }

    getPortions(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._menuItemService
            .getAllPortionItems(
                this.filterText,
                false,
                this.menuItemId,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
}

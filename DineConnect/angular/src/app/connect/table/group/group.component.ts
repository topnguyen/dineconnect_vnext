import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import {
    CommonLocationGroupDto,
    EntityDto,
    ConnectTableGroupServiceProxy,
    ConnectTableListDto,
    ConnectTableGroupListDto
} from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import * as _ from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ImportModalComponent } from '@app/connect/shared/import-modal/import-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './group.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class GroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;
    @ViewChild('importModal', { static: true }) importModal: ImportModalComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _connectTableGroupServiceProxy: ConnectTableGroupServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit() {
        this.configImport();
    }

    configImport() {
        // this.importModal.configure({
        //     title: this.l('SelectATiffinProductOfferType'),
        //     downloadTemplate: () => {
        //         return this._connectTableGroupServiceProxy.getTemplateExcelToImport();
        //     },
        //     import: (fileToken: string) => {
        //         return this._connectTableGroupServiceProxy.importLocationFromExcel(fileToken);
        //     }
        // });
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        const locationOrGroup = this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._connectTableGroupServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrUpdateGroup(id?: number): void {
        this._router.navigate(['/app/connect/table/group/create-or-update', id ? id : 'null']);
    }

    deleteTableGroup(group: ConnectTableGroupListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._connectTableGroupServiceProxy.deleteConnectTableGroup(group.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(table: ConnectTableListDto) {
        const body = EntityDto.fromJS({ id: table.id });
        this._connectTableGroupServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    exportToExcel() {
        // this._connectTableGroupServiceProxy.getAllToExcel().subscribe((result) => {
        //     this._fileDownloadService.downloadTempFile(result);
        // });
    }

    import() {
        this.importModal.show();
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = '';

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(',');
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags.map((x) => x.id).join(',');
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations.map((x) => x.id).join(',');
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(',');
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    onModalSave() {
        setTimeout(() => {
            this.getAll();
        }, 3000);
    }
}

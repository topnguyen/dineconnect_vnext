import { Component, ViewChild, Injector, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {
    CommonLocationGroupDto,
    ConnectTableGroupServiceProxy,
    ConnectTableGroupEditDto,
    CreateOrUpdateConnectTableGroupInput
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss'],
  animations: [appModuleAnimation()]
})
export class CreateGroupComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectLocationComponent;

    saving = false;
    tableGroup = new ConnectTableGroupEditDto();
    locationGroup = new CommonLocationGroupDto();
    tableGroupId: number;
    groupOptions = {
        items: [],
        selectedItems: []
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _connectTableGroupServiceProxy: ConnectTableGroupServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.tableGroupId = +params['id']; // (+) converts string 'id' to a number

            if (!isNaN(this.tableGroupId)) {
                this._connectTableGroupServiceProxy.getConnectTableGroupForEdit(this.tableGroupId).subscribe((result) => {
                    this.tableGroup = result.connectTableGroup;
                    this.locationGroup = result.locationGroup;

                    this.groupOptions.selectedItems = this.tableGroup.connectTables;
                    this.groupOptions.items = this.tableGroup.allTables;
                });
            }
        });
    }

    save(): void {
        this.saving = true;
        const input = new CreateOrUpdateConnectTableGroupInput();
        input.connectTableGroup = this.tableGroup;
        input.connectTableGroup.connectTables = this.groupOptions.selectedItems;
        input.locationGroup = this.locationGroup;

        this._connectTableGroupServiceProxy
            .createOrUpdateConnectTableGroup(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(['/app/connect/table/group']);
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.locationGroup);
    }

    getLocations(event) {
        this.locationGroup = event;
    }
}
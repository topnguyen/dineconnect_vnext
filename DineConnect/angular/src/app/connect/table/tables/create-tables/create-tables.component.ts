import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConnectTableEditDto, ConnectTableGroupServiceProxy, CreateOrUpdateConnectTableInput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-tables',
  templateUrl: './create-tables.component.html',
  styleUrls: ['./create-tables.component.scss']
})
export class CreateTablesComponent extends AppComponentBase {
    @ViewChild('createTablesModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    table: ConnectTableEditDto;
    newName = null;
    tableId: number;

    constructor(injector: Injector, private _connectTableGroupServiceProxy: ConnectTableGroupServiceProxy) {
        super(injector);
    }

    show(tableId: number): void {
        this.tableId = tableId;
        this.saving = false;
        this.modal.show();
        
        this.init();
    }

    save(): void {
        this.saving = true;

        const table = ConnectTableEditDto.fromJS(this.table);
        const body = CreateOrUpdateConnectTableInput.fromJS({
            table: table
        })

        this._connectTableGroupServiceProxy
            .createOrUpdateConnectTable(body)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    init() {
        if (this.tableId) {
            this._connectTableGroupServiceProxy.getConnectTableForEdit(this.tableId).subscribe((result) => {
                this.table = result.table;
            });
        } else {
            this.table = new ConnectTableEditDto({pax: 0} as any);
        }        
    }

    close(): void {
        this.modal.hide();
        this.modalSave.emit(null);
    }
}

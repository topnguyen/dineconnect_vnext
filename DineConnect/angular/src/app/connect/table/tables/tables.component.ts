import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import {
    CommonLocationGroupDto,
    EntityDto,
    ConnectTableGroupServiceProxy,
    ConnectTableListDto
} from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import * as _ from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { finalize } from 'rxjs/operators';
import { CreateTablesComponent } from './create-tables/create-tables.component';

@Component({
    templateUrl: './tables.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TablesComponent extends AppComponentBase {
    @ViewChild('createOrEditTable', { static: true }) createOrEditTable: CreateTablesComponent;
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    location: string;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _connectTableGroupServiceProxy: ConnectTableGroupServiceProxy,
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        const locationOrGroup = this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._connectTableGroupServiceProxy
            .getAllTables(
                this.filterText,
                undefined,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createTables(): void {
        this.createOrEditTable.show(null);
    }


    editTables(id?: number): void {
        this.createOrEditTable.show(id);
    }

    deleteTableGroup(table: ConnectTableListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._connectTableGroupServiceProxy.deleteConnectTable(table.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(table: ConnectTableListDto) {
        const body = EntityDto.fromJS({ id: table.id });
        this._connectTableGroupServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = '';

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(',');
            this.location = locationOrGroup.groups[0].name;
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags.map((x) => x.id).join(',');
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations.map((x) => x.id).join(',');
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(',');
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

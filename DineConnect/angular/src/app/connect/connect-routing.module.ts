import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { OrganizationComponent } from "./master/organization/organization.component";
import { LocationComponent } from "./master/location/location.component";
import { CreateOrUpdateLocationModalComponent } from "./master/location/create-or-update-location.component";
import { LocationGroupComponent } from "./master/location-groups/location-group.component";
import { MenuItemComponent } from "./menu/menuItem/menuItem.component";
import { CategoriesComponent } from "./menu/categories/categories.component";
import { UserLocationComponent } from "./master/userLocation/user-location.component";
import { ProductGroupsComponent } from "./menu/product-groups/product-groups.component";
import { PaymentTypesComponent } from "./master/payment-types/payment-types.component";
import { CreateOrEditPaymentTypeComponent } from "./master/payment-types/create-or-edit-payment-type.component";
import { ForeignCurrenciesComponent } from "./master/foreign-currencies/foreign-currencies.component";
import { PlanReasonsComponent } from "./master/plan-reasons/plan-reasons.component";
import { TransactionTypesComponent } from "./master/transaction-types/transaction-types.component";
import { TillAccountsComponent } from "./master/till-accounts/till-accounts.component";
import { CalculationsComponent } from "./master/calculations/calculations.component";
import { CreateOrEditCalculationComponent } from "./master/calculations/create-or-edit-calculation.component";
import { CreateOrEditTerminalComponent } from "./master/terminals/create-or-edit-terminal.component";
import { TerminalsComponent } from "./master/terminals/terminals.component";
import { TicketTagGroupsComponent } from "./tags/ticket-tags/ticket-tag-groups.component";
import { CreateOrEditTicketTagGroupComponent } from "./tags/ticket-tags/create-or-edit-ticket-tag-group.component";
import { PriceTagsComponent } from "./tags/price-tags/price-tags.component";
import { CreateOrEditPriceTagComponent } from "./tags/price-tags/create-or-edit-price-tag.component";
import { DepartmentsComponent } from "./master/departments/departments.component";
import { CreateOrEditDepartmentComponent } from "./master/departments/create-or-edit-department.component";
import { LocationTagsComponent } from "./master/location-tags/location-tags.component";
import { PromotionsComponent } from "./discount/promotions/promotions.component";
import { CreateOrEditPromotionComponent } from "./discount/promotions/create-or-edit-promotion.component";
import { CreateOrEditDinePlanTaxComponent } from "./master/taxes/create-or-edit-dine-plan-tax.component";
import { CreateOrEditCategoryComponent } from "./menu/categories/create-or-edit-category.component";
import { CreateOrEditMenuitemComponent } from "./menu/menuItem/create-or-edit-menuitem.component";
import { DinePlanTaxesComponent } from "./master/taxes/dine-plan-taxes.component";
import { CreateOrEditTillAccountComponent } from "./master/till-accounts/create-or-edit-till-account.component";
import { CreateOrEditPlanReasonComponent } from "./master/plan-reasons/create-or-edit-plan-reason.component";
import { EditPriceForPriceTagComponent } from "./tags/price-tags/edit-price-for-price-tag/edit-price-for-price-tag.component";
import { ProgramSettingTemplatesComponent } from "./program-setting/program-setting-templates/program-setting-templates.component";
import { CreateOrEditProgramSettingTemplateComponent } from "./program-setting/program-setting-templates/create-or-edit-program-setting-template.component";
import { PrintPrintersComponent } from "./print/printers/printers.component";
import { CreateOrEditPrinterComponent } from "./print/printers/create-or-edit-printer.component";
import { CreateOrEditPrintTemplateComponent } from "./print/templates/create-or-edit-print-template.component";
import { PrintTemplateComponent } from "./print/templates/print-template.component";
import { PrintJobComponent } from "./print/jobs/print-job.component";
import { CreateOrEditPrintJobComponent } from "./print/jobs/create-or-edit-print-job.component";
import { PrintConditionComponent } from "./print/conditions/print-condition.component";
import { CreateOrEditPrintConditionComponent } from "./print/conditions/create-or-edit-print-condition.component";
import { LocationPricesComponent } from "./menu/location-prices/location-prices.component";
import { ComboGroupsComponent } from "./menu/combo-groups/combo-groups.component";
import { CreateOrEditComboGroupComponent } from "./menu/combo-groups/create-or-edit-combo-group.component";
import { LocationMenusComponent } from "./menu/location-menus/location-menus.component";
import { ScreenMenusComponent } from "./menu/screen-menus/screen-menus.component";
import { CreateOrEditScreenMenuComponent } from "./menu/screen-menus/create-or-edit-screen-menu.component";
import { ScreenMenuCategoriesComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-categories.component";
import { EditScreenMenuCategoryComponent } from "./menu/screen-menus/screen-menu-categories/edit-screen-menu-category.component";
import { ScreenMenuMenuitemComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem.component";
import { ScreenMenuMenuitemEditComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-edit/screen-menu-menuitem-edit.component";
import { ScreenMenuMenuitemAddComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-add/screen-menu-menuitem-add.component";
import { ScreenMenuVirtualscreenComponent } from "./menu/screen-menus/screen-menu-virtualscreen/screen-menu-virtualscreen.component";
import { OrderTagGroupsComponent } from "./tags/order-tag-groups/order-tag-groups.component";
import { CreateOrEditOrderTagGroupComponent } from "./tags/order-tag-groups/create-or-edit-order-tag-group.component";
import { GroupComponent } from "./table/group/group.component";
import { TablesComponent } from "./table/tables/tables.component";
import { ConnectUserRolesComponent } from "./user/user-roles/user-roles.component";
import { CreateUpdateConnectUserRoleComponent } from "./user/user-roles/create-update-user-role/create-update-user-role.component";
import { ConnectUsersComponent } from "./user/users/users.component";
import { CreateUpdateConnectUserComponent } from "./user/users/create-update-user/create-update-user.component";
import { PromotionCategorysComponent } from "./discount/promotion-categorys/promotion-categorys.component";
import { CreateUpdatePromotionCategoryComponent } from "./discount/promotion-categorys/create-update-promotion-category/create-update-promotion-category.component";
import { NumeratorComponent } from "./master/numerator/numerator.component";
import { CreateUpdateNumeratorComponent } from "./master/numerator/create-update-numerator/create-update-numerator.component";
import { TicketTypeComponent } from "./master/ticket-type/ticket-type.component";
import { CreateUpdateTicketTypeComponent } from "./master/ticket-type/create-update-ticket-type/create-update-ticket-type.component";
import { DepartmentGroupComponent } from "./master/department-group/department-group.component";
import { CreateUpdateDepartmentGroupComponent } from "./master/department-group/create-update-department-group/create-update-department-group.component";
import { FutureDatesComponent } from "./master/future-date/future-dates.component";
import { CreateOrEditFutureDateComponent } from "./master/future-date/create-or-edit-future-date.component";
import { CreateGroupComponent } from "./table/group/create-group/create-group.component";
import { PrintDevicesComponent } from "./print/devices/devices.component";
import { CreateOrEditDeviceComponent } from "./print/devices/create-or-edit-device.component";
import { CardCategoryComponent } from "./card/card-category/card-category.component";
import { CreateOrUpdateCardCategoryComponent } from "./card/card-category/create-or-update-card-category/create-or-update-card-category.component";
import { CardTypeComponent } from "./card/card-type/card-type.component";
import { CreateOrUpdateCardTypeComponent } from "./card/card-type/create-or-update-card-type/create-or-update-card-type.component";
import { CardsComponent } from "./card/cards/cards.component";
import { ReportTicketComponent } from "./report/tickets/report-ticket/report-ticket.component";
import { TicketSyncComponent } from "./report/tickets/ticket-sync/ticket-sync.component";
import { ReportOrderComponent } from "./report/order/report-order.component";
import { PriceTagLocationPriceComponent } from "./tags/price-tags/price-tag-location-price.component";
import { ProductComboComponent } from "./menu/product-combo/product-combo.component";
import { CreateOrEditProductComboComponent } from "./menu/product-combo/create-or-edit-product-combo.component";
import { ItemReportComponent } from "./report/item/item-report.component";
import { HourlyComponent } from "./report/item/hourly/hourly.component";
import { OrderTagLocationPriceComponent } from "./tags/order-tag-groups/order-tag-location-price/order-tag-location-price.component";
import { GroupReportComponent } from "./report/group/group-report.component";
import { CategoryReportComponent } from "./report/category/category-report.component";
import { OrderPromotionReportComponent } from "./report/promotion/order-promotion/order-promotion-report.component";
import { ItemTagReportComponent } from "./report/item/item-tag-report/item-tag-report.component";
import { SummaryReportComponent } from "./report/sale/summary-report/summary-report.component";
import { TopDownReportComponent } from "./report/item/top-down-report/top-down-report.component";
import { WorkDayReportComponent } from "./report/sale/work-day-report/work-day-report.component";
import { WeekDayReportComponent } from "./report/sale/week-day-report/week-day-report.component";
import { WeekEndReportComponent } from "./report/sale/week-end-report/week-end-report.component";
import { HourlySalesComponent } from "./report/tickets/hourly-sales/hourly-sales.component";
import { DepartmentSummaryComponent } from "./report/department/department-summary/department-summary.component";
import { DepartmentItemsComponent } from "./report/department/department-items/department-items.component";
import { PlanDayComponent } from "./report/sale/plan-day/plan-day.component";

import { CashSummaryReportComponent } from './report/sale/cash-summary-report/cash-summary-report.component';
import { OrderTagReportComponent } from './report/order/order-tag-report/order-tag-report.component';
import { OrderTagExchangeComponent } from './report/order/order-tag-exchange/order-tag-exchange.component';
import { OrderReturnReportComponent } from './report/order/order-return-report/order-return-report.component';
import { CollecttionReportComponent  } from './report/sale/collecttion-report/collecttion-report.component';
import { ScheduleItemsComponent } from "./report/schedule/schedule-items/schedule-items.component";
import { ScheduleLocationComponent } from "./report/schedule/schedule-location/schedule-location.component";

import { TillTransactionReportComponent } from "./report/other/till-transaction-report/till-transaction-report.component";
import { LogReportComponent } from "./report/other/log-report/log-report.component";
import { PaymentExcessReportComponent } from "./report/other/payment-excess-report/payment-excess-report.component";
import { SummaryOtherReportComponent } from "./report/other/summary-other-report/summary-other-report.component";
import { TenderReportComponent } from "./report/other/tender-report/tender-report.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "master/organization",
                        component: OrganizationComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Location.Brands",
                        },
                    },
                    {
                        path: "master/locations",
                        component: LocationComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Location.Locations",
                        },
                    },
                    {
                        path: "master/locations/create-or-update/:id",
                        component: CreateOrUpdateLocationModalComponent,
                    },
                    {
                        path: "master/locationGroups",
                        component: LocationGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Location.LocationGroups",
                        },
                    },
                    {
                        path: "master/locationTags",
                        component: LocationTagsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Location.LocationTags",
                        },
                    },
                    {
                        path: "master/paymentTypes",
                        component: PaymentTypesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.PaymentTypes",
                        },
                    },
                    {
                        path: "master/paymentTypes/create-or-update/:id",
                        component: CreateOrEditPaymentTypeComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.PaymentTypes.Create",
                        },
                    },
                    {
                        path: "master/foreignCurrencies",
                        component: ForeignCurrenciesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.ForeignCurrencies",
                        },
                    },
                    {
                        path: "master/planReasons",
                        component: PlanReasonsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.PlanReasons",
                        },
                    },
                    {
                        path: "master/planReasons/create-or-update/:id",
                        component: CreateOrEditPlanReasonComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.PlanReasons",
                        },
                    },
                    {
                        path: "master/transactionTypes",
                        component: TransactionTypesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.TransactionTypes",
                        },
                    },
                    {
                        path: "master/tillAccounts",
                        component: TillAccountsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.TillAccounts",
                        },
                    },
                    {
                        path: "master/tillAccounts/create-or-update/:id",
                        component: CreateOrEditTillAccountComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.TillAccounts",
                        },
                    },
                    {
                        path: "master/calculations",
                        component: CalculationsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.Calculations",
                        },
                    },
                    {
                        path: "master/calculations/create-or-update/:id",
                        component: CreateOrEditCalculationComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.Calculations",
                        },
                    },
                    {
                        path: "master/terminals",
                        component: TerminalsComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Master.Terminals",
                        },
                    },
                    {
                        path: "master/terminals/create-or-update/:id",
                        component: CreateOrEditTerminalComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Master.Terminals",
                        },
                    },
                    {
                        path: "master/future-data",
                        component: FutureDatesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.FutureDateInformations",
                        },
                    },
                    {
                        path: "master/future-data/create-or-update/:id",
                        component: CreateOrEditFutureDateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.FutureDateInformations.Create",
                        },
                    },
                    {
                        path: "master/departments",
                        component: DepartmentsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.Departments",
                        },
                    },
                    {
                        path: "master/departments/create-or-update/:id",
                        component: CreateOrEditDepartmentComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.Departments.Create",
                        },
                    },
                    {
                        path: "master/dinePlanTaxes",
                        component: DinePlanTaxesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.DinePlanTaxes",
                        },
                    },
                    {
                        path: "master/dinePlanTaxes/create-or-update/:id",
                        component: CreateOrEditDinePlanTaxComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.DinePlanTaxes.Create",
                        },
                    },
                    {
                        path: "master/numerators",
                        component: NumeratorComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Master.Numerator",
                        },
                    },
                    {
                        path: "master/numerators/create-or-update/:id",
                        component: CreateUpdateNumeratorComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.Numerator.Create",
                        },
                    },
                    {
                        path: "master/ticket-types",
                        component: TicketTypeComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.TicketType",
                        },
                    },
                    {
                        path: "master/ticket-types/create-or-update/:id",
                        component: CreateUpdateTicketTypeComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.TicketType.Create",
                        },
                    },
                    {
                        path: "master/department-group",
                        component: DepartmentGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.DepartmentGroup",
                        },
                    },
                    {
                        path: "master/department-group/create-or-update/:id",
                        component: CreateUpdateDepartmentGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Master.DepartmentGroup.Create",
                        },
                    },

                    {
                        path: "menu/menuItem",
                        component: MenuItemComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.MenuItems",
                        },
                    },
                    {
                        path: "menu/menuItem/create-or-update/:id",
                        component: CreateOrEditMenuitemComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.MenuItems.Create",
                        },
                    },
                    {
                        path: "user-locations",
                        component: UserLocationComponent,
                        data: {
                            permission: "Pages.Administration.UserLocation",
                        },
                    },
                    {
                        path: "menu/productGroups",
                        component: ProductGroupsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ProductGroups",
                        },
                    },
                    {
                        path: "menu/category",
                        component: CategoriesComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.Categories",
                        },
                    },
                    {
                        path: "menu/category/create-or-update/:id",
                        component: CreateOrEditCategoryComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.Categories.Create",
                        },
                    },

                    // ScreenMenus
                    {
                        path: "menu/screen-menus",
                        component: ScreenMenusComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ScreenMenus",
                        },
                    },
                    {
                        path: "menu/screen-menus/create-or-update",
                        component: CreateOrEditScreenMenuComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ScreenMenus.Create",
                        },
                    },
                    {
                        path: "menu/screen-menus/create-or-update/:id",
                        component: CreateOrEditScreenMenuComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ScreenMenus.Edit",
                        },
                    },
                    {
                        path: "menu/screen-menus/:id/screen-menu-categories",
                        component: ScreenMenuCategoriesComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ScreenMenus",
                        },
                    },
                    {
                        path: "menu/screen-menus/:screenMenuId/screen-menu-categories/edit/:id",
                        component: EditScreenMenuCategoryComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ScreenMenus",
                        },
                    },
                    {
                        path: "menu/screen-menus/:screenMenuId/screen-menu-categories/:id/screen-menu-menuitems",
                        component: ScreenMenuMenuitemComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ScreenMenus",
                        },
                    },
                    {
                        path: "menu/screen-menus/:screenMenuId/screen-menu-categories/:screenMenuCategoryId/screen-menu-menuitems/add",
                        component: ScreenMenuMenuitemAddComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ScreenMenus.Edit",
                        },
                    },
                    {
                        path: "menu/screen-menus/:screenMenuId/screen-menu-categories/:screenMenuCategoryId/screen-menu-menuitems/edit/:id",
                        component: ScreenMenuMenuitemEditComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ScreenMenus.Edit",
                        },
                    },

                    {
                        path: "menu/screen-menus/:id/screen-menu-virtualscreen",
                        component: ScreenMenuVirtualscreenComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ScreenMenus.Edit",
                        },
                    },

                    // ProductCombo
                    {
                        path: "menu/product-combo",
                        component: ProductComboComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ComboGroups",
                        },
                    },
                    {
                        path: "menu/product-combo/create-or-update",
                        component: CreateOrEditProductComboComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ComboGroups.Create",
                        },
                    },
                    {
                        path: "menu/product-combo/create-or-update/:id",
                        component: CreateOrEditProductComboComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ComboGroups.Edit",
                        },
                    },

                    // ComboGroups
                    {
                        path: "menu/combo-groups",
                        component: ComboGroupsComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Menu.ComboGroups",
                        },
                    },
                    {
                        path: "menu/combo-groups/create-or-update",
                        component: CreateOrEditComboGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ComboGroups.Create",
                        },
                    },
                    {
                        path: "menu/combo-groups/create-or-update/:id",
                        component: CreateOrEditComboGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.ComboGroups.Edit",
                        },
                    },

                    // LocationMenus
                    {
                        path: "menu/location-menus",
                        component: LocationMenusComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.LocationMenus",
                        },
                    },

                    // LocationPrices
                    {
                        path: "menu/location-prices",
                        component: LocationPricesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.LocationPrices",
                        },
                    },
                    {
                        path: "menu/location-prices/:locationId",
                        component: LocationPricesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Menu.LocationPrices",
                        },
                    },

                    {
                        path: "table/group",
                        component: GroupComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Table.Group",
                        },
                    },
                    {
                        path: "table/group/create-or-update/:id",
                        component: CreateGroupComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Table.Group",
                        },
                    },
                    {
                        path: "table/tables",
                        component: TablesComponent,
                        data: { permission: "Pages.Tenant.Connect.Tables" },
                    },

                    {
                        path: "tags/ticketTagGroups",
                        component: TicketTagGroupsComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Tag.TicketTagGroups",
                        },
                    },
                    {
                        path: "tags/ticketTagGroups/create-or-update/:id",
                        component: CreateOrEditTicketTagGroupComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Tag.TicketTagGroups",
                        },
                    },
                    {
                        path: "tags/priceTags",
                        component: PriceTagsComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.PriceTags",
                        },
                    },
                    {
                        path: "tags/priceTags/create-or-update/:id",
                        component: CreateOrEditPriceTagComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.PriceTags",
                        },
                    },
                    {
                        path: "tags/priceTags/edit-price-for-tag/:id",
                        component: EditPriceForPriceTagComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.PriceTags",
                        },
                    },
                    {
                        path: "tags/priceTags/location-price/:id/:priceId",
                        component: PriceTagLocationPriceComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.PriceTags",
                        },
                    },
                    {
                        path: "tags/orderTagGroups",
                        component: OrderTagGroupsComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.OrderTags",
                        },
                    },
                    {
                        path: "tags/orderTagGroups/create-or-update/:id",
                        component: CreateOrEditOrderTagGroupComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.OrderTags",
                        },
                    },
                    {
                        path: "tags/orderTagGroups/location-price/:id/:priceId",
                        component: OrderTagLocationPriceComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Tag.OrderTags",
                        },
                    },

                    {
                        path: "discount/promotions",
                        component: PromotionsComponent,
                        data: { permission: "Pages.Tenant.Connect.Promotions" },
                    },
                    {
                        path: "discount/promotions/create-or-update/:id",
                        component: CreateOrEditPromotionComponent,
                        data: { permission: "Pages.Tenant.Connect.Promotions" },
                    },
                    {
                        path: "discount/categorys",
                        component: PromotionCategorysComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.PromotionCategory",
                        },
                    },
                    {
                        path: "discount/categorys/create-or-update/:id",
                        component: CreateUpdatePromotionCategoryComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.PromotionCategory.Create",
                        },
                    },

                    {
                        path: "print/printers",
                        component: PrintPrintersComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Print.Printers",
                        },
                    },
                    {
                        path: "print/printers/create-or-update",
                        component: CreateOrEditPrinterComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.Printers.Create",
                        },
                    },
                    {
                        path: "print/printers/create-or-update/:id",
                        component: CreateOrEditPrinterComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.Printers.Edit",
                        },
                    },

                    {
                        path: "print/print-templates",
                        component: PrintTemplateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintTemplates",
                        },
                    },
                    {
                        path: "print/print-templates/create-or-update",
                        component: CreateOrEditPrintTemplateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintTemplates.Create",
                        },
                    },
                    {
                        path: "print/print-templates/create-or-update/:id",
                        component: CreateOrEditPrintTemplateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintTemplates.Edit",
                        },
                    },

                    {
                        path: "print/print-jobs",
                        component: PrintJobComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Print.PrintJobs",
                        },
                    },
                    {
                        path: "print/print-jobs/create-or-update",
                        component: CreateOrEditPrintJobComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintJobs.Create",
                        },
                    },
                    {
                        path: "print/print-jobs/create-or-update/:id",
                        component: CreateOrEditPrintJobComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintJobs.Edit",
                        },
                    },

                    {
                        path: "print/print-devices",
                        component: PrintDevicesComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Print.PrintJobs",
                        },
                    },
                    {
                        path: "print/print-devices/create-or-update",
                        component: CreateOrEditDeviceComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintJobs.Create",
                        },
                    },
                    {
                        path: "print/print-devices/create-or-update/:id",
                        component: CreateOrEditDeviceComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Print.PrintJobs.Edit",
                        },
                    },

                    {
                        path: "print/print-conditions",
                        component: PrintConditionComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Display.DineDevice",
                        },
                    },
                    {
                        path: "print/print-conditions/create-or-update",
                        component: CreateOrEditPrintConditionComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Display.DineDevice.Create",
                        },
                    },
                    {
                        path: "print/print-conditions/create-or-update/:id",
                        component: CreateOrEditPrintConditionComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Display.DineDevice.Edit",
                        },
                    },

                    {
                        path: "programSetting/templates",
                        component: ProgramSettingTemplatesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.ProgramSetting.Templates",
                        },
                    },
                    {
                        path: "programSetting/templates/create-or-update",
                        component: CreateOrEditProgramSettingTemplateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.ProgramSetting.Templates.Create",
                        },
                    },
                    {
                        path: "programSetting/templates/create-or-update/:id",
                        component: CreateOrEditProgramSettingTemplateComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.ProgramSetting.Templates.Edit",
                        },
                    },

                    // { path: 'discount/program-settings/templates/create-or-update/:id', component: CreateOrEditPromotionComponent, data: { permission: 'Pages.Tenant.Connect.Promotions' } },

                    {
                        path: "user/users",
                        component: ConnectUsersComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.User.DinePlanUser",
                        },
                    },
                    {
                        path: "user/users/create-or-update/:id",
                        component: CreateUpdateConnectUserComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.User.DinePlanUser.Create",
                        },
                    },
                    {
                        path: "user/roles",
                        component: ConnectUserRolesComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.User.DinePlanUserRole",
                        },
                    },

                    // { path: 'fullTax/members', component: FullTaxMembersComponent, data: { permission: 'Pages.FullTaxMembers' } },
                    // { path: 'fullTax/members/create-or-update/:id', component: CreateOrEditFullTaxMemberComponent, data: { permission: 'Pages.FullTaxMembers.Create' } },

                    {
                        path: "card/categorys",
                        component: CardCategoryComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Card.ConnectCardTypeCategory",
                        },
                    },
                    {
                        path: "card/categorys/create-or-update/:id",
                        component: CreateOrUpdateCardCategoryComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Create",
                        },
                    },
                    {
                        path: "card/types",
                        component: CardTypeComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Card.CardType",
                        },
                    },
                    {
                        path: "card/types/create-or-update/:id",
                        component: CreateOrUpdateCardTypeComponent,
                        data: {
                            permission:
                                "Pages.Tenant.Connect.Card.CardType.Create",
                        },
                    },
                    {
                        path: "card/cards",
                        component: CardsComponent,
                        data: { permission: "Pages.Tenant.Connect.Card.Cards" },
                    },

                    /* TODO: permission */
                    {
                        path: "report/ticket",
                        component: ReportTicketComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/ticket-sync",
                        component: TicketSyncComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/hourly-sales",
                        component: HourlySalesComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/order",
                        component: ReportOrderComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/item-sales",
                        component: ItemReportComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/item-tag",
                        component: ItemTagReportComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/top-down",
                        component: TopDownReportComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/hourly",
                        component: HourlyComponent,
                        data: { permission: "Pages.Tenant.Connect" },
                    },
                    {
                        path: "report/group",
                        component: GroupReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Group",
                        },
                    },
                    {
                        path: "report/category",
                        component: CategoryReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Category",
                        },
                    },
                    {
                        path: "report/category",
                        component: CategoryReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Category",
                        },
                    },
                    {
                        path: "report/order-promotion",
                        component: OrderPromotionReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Promotion",
                        },
                    },
                    {
                        path: "report/ticket-promotion",
                        component: OrderPromotionReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Promotion",
                        },
                    },
                    {
                        path: "report/summary-sale",
                        component: SummaryReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/workday",
                        component: WorkDayReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/weekday",
                        component: WeekDayReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/weekend",
                        component: WeekEndReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/planday",
                        component: PlanDayComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/cash-summary",
                        component: CashSummaryReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/collection-report",
                        component: CollecttionReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Sale",
                        },
                    },
                    {
                        path: "report/department-summary",
                        component: DepartmentSummaryComponent,
                        data: { permission: "Pages.Tenant.Connect.Report.Department.Summary" },
                    },
                    {
                        path: "report/department-item",
                        component: DepartmentItemsComponent,
                        data: { permission: "Pages.Tenant.Connect.Report.Department.Items" },
                    },
                    {
                        path: "report/order-tag-group",
                        component: OrderTagReportComponent,
                        data: { permission: "Pages.Tenant.Connect.Tag.OrderTags" },
                    },
                    {
                        path: "report/order-exchange",
                        component: OrderTagExchangeComponent,
                        data: { permission: "Pages.Tenant.Connect.Tag.OrderTags" },
                    },
                    {
                        path: "report/order-return",
                        component: OrderReturnReportComponent,
                        data: { permission: "Pages.Tenant.Connect.Tag.OrderTags" },
                    },
                    {
                        path: "report/schedule-items",
                        component: ScheduleItemsComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.ScheduleItems",
                        },
                    },
                    {
                        path: "report/schedule-locations",
                        component: ScheduleLocationComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.ScheduleLocations",
                        },
                    },
                    {
                        path: "report/till-transaction",
                        component: TillTransactionReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.TillTransaction",
                        },
                    },
                    {
                        path: "report/log-other",
                        component: LogReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Log",
                        },
                    },
                    {
                        path: "report/summaryOther",
                        component: SummaryOtherReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.SummaryOther",
                        },
                    },
                    {
                        path: "report/payment-excess",
                        component: PaymentExcessReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.PaymentExcess",
                        },
                    },
                    {
                        path: "report/tender-report",
                        component: TenderReportComponent,
                        data: {
                            permission: "Pages.Tenant.Connect.Report.Tender",
                        },
                    },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class ConnectRoutingModule {}

import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductGroupsServiceProxy, ProductGroupDto, CategoryServiceProxy, PagedResultDtoOfCategoryDto, UpdateProductgroupCategoryInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditProductGroupModalComponent } from './create-or-edit-productGroup-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent, TreeNode, MenuItem } from 'primeng';
import { TreeDataHelperService } from '@shared/utils/tree-data-helper.service';
import { ArrayToTreeConverterService } from '@shared/utils/array-to-tree-converter.service';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { Observable } from 'rxjs';
import { ItemsList } from '@ng-select/ng-select/lib/items-list';
import { CreateOrEditLanguageDescriptionModalComponent } from '../create-or-edit-language-description-modal/create-or-edit-language-description-modal.component';

@Component({
  selector: 'app-product-groups',
  templateUrl: './product-groups.component.html',
  styleUrls: ['./product-groups.component.css'],
  animations: [appModuleAnimation()]
})
export class ProductGroupsComponent extends AppComponentBase implements OnInit {

  @ViewChild('createOrEditProductGroupModal', { static: true }) createOrEditProductGroupModal: CreateOrEditProductGroupModalComponent;
  @ViewChild('selectCategoryPopupModal', { static: true }) selectCategoryPopupModal: SelectPopupComponent;
  @ViewChild('createOrEditLanguageDescriptionModal', { static: true }) createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  treeData: any;
  selectedGroup: TreeNode;

  filterText = '';
  isDeleted;
  selectedCategory = [];

  items: MenuItem[];

  constructor(
    injector: Injector,
    private _productGroupsServiceProxy: ProductGroupsServiceProxy,
    private _arrayToTreeConverterService: ArrayToTreeConverterService,
    private _treeDataHelperService: TreeDataHelperService,
    private _categoryService: CategoryServiceProxy
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.items = [
      { label: this.l('Edit'), icon: 'pi pi-pencil', command: (event) => this.editGroup(this.selectedGroup.data) },
      { label: 'Add sub unit', icon: 'pi pi-plus', command: (event) => this.addSubUnit(this.selectedGroup.data) },
      { label: this.l('Delete'), icon: 'pi pi-times', command: (event) => this.deleteProductGroup(this.selectedGroup.data) }
    ];

    this.getProductGroups();
  }

  getProductGroups() {
    this._productGroupsServiceProxy.getAll()
      .subscribe(result => {
        let self = this;

        this.treeData = this._arrayToTreeConverterService.createTree(
          result.items,
          'parentId',
          'id',
          null,
          'children',
          [
            {
              target: 'styleClass',
              value: 'category-tree-node',
            },
            {
              target: 'label',
              targetFunction(item) {
                return item.name;
              },
            },
            {
              target: 'selectable',
              value: true,
            },
            {
              target: 'expandedIcon',
              value: 'fa fa-folder-open m--font-warning'
            },
            {
              target: 'collapsedIcon',
              value: 'fa fa-folder m--font-warning'
            },
            {
              target: 'expanded',
              value: true
            },
            {
              target: 'categoriesCount',
              targetFunction(item) {
                return self.setCategoriesCount(item)
              },
            },
            {
              target: 'categories',
              targetFunction(item) {
                return item.categories
              },
            },
          ]
        );

        if (this.selectedGroup) {
          this.selectedGroup = this._treeDataHelperService.findNode(
            this.treeData,
            { data: { id: this.selectedGroup?.data.id } }
          );
        }
      });
  }

  getCategoriesByGroup(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    this.selectedCategory = this.selectedGroup?.data.categories;
    this._categoryService.getCategoryByPgroductGroupId(
      this.filterText,
      this.selectedGroup?.data.id,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getMaxResultCount(this.paginator, event),
      this.primengTableHelper.getSkipCount(this.paginator, event)
    ).pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  setCategoriesCount(item) {
    return item.categories?.length;
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  createProductGroup(): void {
    this.createOrEditProductGroupModal.show();
  }

  editGroup(productGroup: ProductGroupDto) {
    this.createOrEditProductGroupModal.show(productGroup.id)
  }

  addSubUnit(productGroup: ProductGroupDto) {
    this.createOrEditProductGroupModal.show(undefined, productGroup.id);
  }

  selectCategory() {
    this.selectCategoryPopupModal.show(this.selectedCategory);
  }

  deleteProductGroup(productGroup: ProductGroupDto): void {
    this.message.confirm(
      this.l('AreYouSure'),
      this.l('Group') + ' ' + productGroup.name + ' ' + this.l('will be deleted.'),
      (isConfirmed) => {
        if (isConfirmed) {
          this._productGroupsServiceProxy.delete(productGroup.id)
            .subscribe(() => {
              this.getProductGroups();
              this.notify.success(this.l('SuccessfullyDeleted'));
            });
        }
      }
    );
  }

  nodeSelect(event: any) {
    this.getCategoriesByGroup();
  }

  getCategorySelection() {
    return (): Observable<PagedResultDtoOfCategoryDto> =>
      this._categoryService.getAll(
        undefined,
        undefined,
        undefined,
        undefined,
        undefined
      );
  }

  addCategory(itemSelect) {
    this.selectedCategory = itemSelect;

    let ids = itemSelect.map(item => { return item.id; });
    let input = new UpdateProductgroupCategoryInput();

    input.productGroupId = this.selectedGroup?.data.id;
    input.categoryIds = ids;
    this._categoryService.updateCategoryProductgroupId(input)
      .subscribe(result => {
        this.getCategoriesByGroup();
        this.getProductGroups();
      });
  }


  showLanguages() {
    this.createOrEditLanguageDescriptionModal.show('ProductGroup', this.selectedGroup?.data.id);
  }
}

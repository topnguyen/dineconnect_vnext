﻿import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import {
    LanguageDescriptionsServiceProxy,
    CreateOrEditLanguageDescriptionDto,
    LanguageDescriptionDto,
    LanguageServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    selector: "createOrEditLanguageDescriptionModal",
    templateUrl: "./create-or-edit-language-description-modal.component.html",
})
export class CreateOrEditLanguageDescriptionModalComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    active = false;
    saving = false;

    languageDescription: CreateOrEditLanguageDescriptionDto =
        new CreateOrEditLanguageDescriptionDto();
    languages = [];

    refId = undefined;
    type = "";
    name = "";

    constructor(
        injector: Injector,
        private _languageDescriptionsServiceProxy: LanguageDescriptionsServiceProxy,
        private _languageService: LanguageServiceProxy
    ) {
        super(injector);
    }

    show(type, refId?: number, name?: string): void {
        this.languageDescription = new CreateOrEditLanguageDescriptionDto();
        this.refId = refId;
        this.type = type;
        this.getLaguages();
        this.getLanguageDescriptions();
        this.active = true;
        this.modal.show();
        this.name = name;
    }

    getLaguages() {
        this._languageService.getLanguages().subscribe((result) => {
            this.languages = result.items;
        });
    }

    save(close?): void {
        this.saving = true;
        this.languageDescription.referenceId = this.refId;
        this.languageDescription.type = this.type;

        this._languageDescriptionsServiceProxy
            .createOrEdit(this.languageDescription)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.clear();
                this.getLanguageDescriptions();
                if (close) this.close();
            });
    }

    saveAndClose() {
        this.save(true);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    clear(): void {
        this.languageDescription = new CreateOrEditLanguageDescriptionDto();
    }

    ngOnInit(): void {}

    getLanguageDescriptions(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._languageDescriptionsServiceProxy
            .getAll(
                this.filterText,
                this.refId,
                undefined,
                this.type,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    editLanguageDescription(id): void {
        this._languageDescriptionsServiceProxy
            .getLanguageDescriptionForEdit(id)
            .subscribe((result) => {
                this.languageDescription = result.languageDescription;
            });
    }

    deleteLanguageDescription(
        languageDescription: LanguageDescriptionDto
    ): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._languageDescriptionsServiceProxy
                    .delete(languageDescription.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }
}

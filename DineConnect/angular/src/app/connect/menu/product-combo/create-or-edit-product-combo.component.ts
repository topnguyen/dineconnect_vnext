import {
    ChangeDetectorRef,
    Component,
    Injector,
    OnInit,
    ViewChild,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CreateEditProductComboItemDto,
    CreateOrEditComboInputDto,
    PagedResultDtoOfProductComboGroupEditDto,
    ProductComboDto,
    ProductComboGroupDto,
    ProductComboGroupServiceProxy,
    ProductComboServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { Paginator, Table } from "primeng";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { ComboItemsModalComponent } from "../combo-groups/combo-items-modal/combo-items-modal.component";

@Component({
    selector: "app-create-or-edit-product-combo",
    templateUrl: "./create-or-edit-product-combo.component.html",
    styleUrls: ["./create-or-edit-product-combo.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditProductComboComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("form", { static: true }) form: NgForm;
    @ViewChild("selectProductComboGroupPopupModal", { static: true })
    selectProductComboGroupPopupModal: SelectPopupComponent;

    @ViewChild("dataTable") dataTable: Table;
    @ViewChild("paginator") paginator: Paginator;
    @ViewChild("comboItemsmodal") comboItemsmodal: ComboItemsModalComponent;
    // @ViewChild('createComboItemModal') createComboItemModal: CreateComboItemModalComponent;
    // @ViewChild('createOrEditLanguageDescriptionModal', { static: true }) createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;

    // --

    comboGroupId = 0;

    productCombo = new ProductComboDto();

    itemIndex: number;

    saving = false;

    get isEdit() {
        return !!this.comboGroupId;
    }

    get formIsValid() {
        return this.form.form.valid;
    }

    get productItems() {
        return this.productCombo?.productComboGroupDtos || [];
    }

    set productItems(value) {
        if (this.productCombo) {
            this.productCombo.productComboGroupDtos = value;
        }
    }

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _productComboServiceProxy: ProductComboServiceProxy,
        private _productComboGroupServiceProxy: ProductComboGroupServiceProxy
    ) {
        super(injector);

        this.productCombo.productComboGroupDtos = [];
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.comboGroupId = +params["id"];
            this._changeDetector.detectChanges();

            if (this.comboGroupId > 0) {
                this._productComboServiceProxy
                    .getForEdit(this.comboGroupId)
                    .subscribe((resp) => {
                        this.productCombo = resp;

                        if (!this.productCombo.productComboGroupDtos) {
                            this.productCombo.productComboGroupDtos = [];
                        }
                    });
            }
        });
    }

    save() {
        if (!this.formIsValid) {
            return;
        }

        this.saving = true;

        const body = new CreateOrEditComboInputDto();
        body.name = this.productCombo.name;
        body.addPriceToOrderPrice = this.productCombo.addPriceToOrderPrice;
        body.productComboGroupIds = this.productCombo.productComboGroupDtos.map(
            (comboGroup) => {
                return comboGroup["id"];
            }
        );
        console.log(this.productCombo.productComboGroupDtos);

        if (this.comboGroupId > 0) {
            body.id = this.comboGroupId;
            this._productComboServiceProxy
                .createOrEdit(body)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        } else {
            this._productComboServiceProxy
                .createOrEdit(body)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        }
    }

    close() {
        this._router.navigate(["/app/connect/menu/product-combo"]);
    }

    // --

    addItem() {
        this.itemIndex = -1;
        this.selectProductComboGroupPopupModal.show();
    }

    removeItem(index: number) {
        this.productItems.splice(index, 1);
    }

    getComboGroups() {
        return (
            filterText,
            comboGroupSelectionIds,
            isDeleted,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfProductComboGroupEditDto> =>
            this._productComboGroupServiceProxy.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    productComboChange(items: ProductComboGroupDto[]): void {
        this.productCombo.productComboGroupDtos = [...items];
    }
    showComboItems(comboGroupId) {
        this.comboItemsmodal.show(comboGroupId);
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboItemsModalComponent } from './combo-items-modal.component';

describe('ComboItemsModalComponent', () => {
  let component: ComboItemsModalComponent;
  let fixture: ComponentFixture<ComboItemsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboItemsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboItemsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

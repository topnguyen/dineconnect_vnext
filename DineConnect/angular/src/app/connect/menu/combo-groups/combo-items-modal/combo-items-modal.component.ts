import {
  Component,
  Injector,
  ViewChild,
  OnInit
} from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
  ProductComboGroupServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { Table, LazyLoadEvent } from 'primeng';
import { PrimengTableHelper } from '@shared/helpers/PrimengTableHelper';

@Component({
  selector: 'app-combo-items-modal',
  templateUrl: './combo-items-modal.component.html',
  styleUrls: ['./combo-items-modal.component.css']
})
export class ComboItemsModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild('itemDataTable', { static: true }) itemDataTable: Table;
  groupId = undefined;
  active = false;
  itemsTableHelper: PrimengTableHelper;

  constructor(
    injector: Injector,
    private _productComboGroupServiceProxy: ProductComboGroupServiceProxy
  ) {
    super(injector);
    this.itemsTableHelper = new PrimengTableHelper();
  }

  show(groupId): void {
    this.groupId = groupId;
    this.active = true;
    // this.getItems();
    this.modal.show();
  }

  ngOnInit() { }

  shown(): void {
    if (this.groupId)
      this.getItems();
  }

  getItems(event?: LazyLoadEvent) {
    // if (this.itemsTableHelper.shouldResetPaging(event)) {
    //   return;
    // }

    if (this.groupId) {
      this.itemsTableHelper.showLoadingIndicator();

      this._productComboGroupServiceProxy.getComboItemsByComboGroupId(this.groupId)
        .pipe(finalize(() => this.itemsTableHelper.hideLoadingIndicator()))
        .subscribe(result => {
          this.itemsTableHelper.records = result;
          this.itemsTableHelper.totalRecordsCount = result?.length;
          this.itemsTableHelper.hideLoadingIndicator();
        });
    }
  }

  close() {
    this.modal.hide();
    this.active = false;

  }
}


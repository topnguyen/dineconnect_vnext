import {
    ChangeDetectorRef,
    Component,
    Injector,
    OnInit,
    ViewChild,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator, Table } from "primeng";
import { finalize } from "rxjs/operators";
import {
    CreateEditProductComboGroupInput,
    CreateEditProductComboItemDto,
    ProductComboGroupServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { CreateComboItemModalComponent } from "../menuItem/create-combo-item-modal/create-combo-item-modal.component";
import { CreateOrEditLanguageDescriptionModalComponent } from "../create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";

@Component({
    selector: "app-create-or-edit-combo-group",
    templateUrl: "./create-or-edit-combo-group.component.html",
    styleUrls: ["./create-or-edit-combo-group.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditComboGroupComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("form", { static: true }) form: NgForm;

    @ViewChild("dataTable") dataTable: Table;
    @ViewChild("paginator") paginator: Paginator;

    @ViewChild("createComboItemModal")
    createComboItemModal: CreateComboItemModalComponent;
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;

    // --

    comboGroupId = 0;

    comboGroup = new CreateEditProductComboGroupInput();

    itemIndex: number;

    saving = false;

    get isEdit() {
        return !!this.comboGroupId;
    }

    get formIsValid() {
        return this.form.form.valid;
    }

    get comboItems() {
        return this.comboGroup?.comboItems || [];
    }

    set comboItems(value) {
        if (this.comboGroup) {
            this.comboGroup.comboItems = value;
        }
    }

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _comboGroupServiceProxy: ProductComboGroupServiceProxy
    ) {
        super(injector);

        this.comboGroup.comboItems = [];
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.comboGroupId = +params["id"];
            this._changeDetector.detectChanges();

            if (this.comboGroupId > 0) {
                this._comboGroupServiceProxy
                    .getForEdit(this.comboGroupId)
                    .subscribe((resp) => {
                        this.comboGroup = resp;

                        if (!this.comboGroup.comboItems) {
                            this.comboGroup.comboItems = [];
                        }
                    });
            }
        });
    }

    save() {
        if (!this.formIsValid) {
            return;
        }

        this.saving = true;

        if (this.comboGroupId > 0) {
            this._comboGroupServiceProxy
                .edit(this.comboGroup)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        } else {
            this._comboGroupServiceProxy
                .create(this.comboGroup)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                });
        }
    }

    close() {
        this._router.navigate(["/app/connect/menu/combo-groups"]);
    }

    // --

    addItem() {
        this.itemIndex = -1;
        this.createComboItemModal.show();
    }

    editItem(index: number) {
        this.itemIndex = index;
        const item = new CreateEditProductComboItemDto(
            this.comboItems[this.itemIndex]
        );
        this.createComboItemModal.show(item);
    }

    removeItem(index: number) {
        this.comboItems.splice(index, 1);
    }

    addComboItem(item: CreateEditProductComboItemDto) {
        if (this.itemIndex < 0) {
            this.comboItems.push(item);
        } else {
            this.comboItems[this.itemIndex] = item;
        }
    }

    showLanguages() {
        this.createOrEditLanguageDescriptionModal.show(
            "ComboGroup",
            this.comboGroupId,
            this.comboGroupId + " - " + this.comboGroup.name
        );
    }
}

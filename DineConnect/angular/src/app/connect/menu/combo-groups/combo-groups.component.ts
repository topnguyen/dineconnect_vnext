import {
    Component,
    Injector,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    EntityDto,
    GetAllPrinterDto,
    PagedResultDtoOfProductComboGroupEditDto,
    ProductComboDto,
    ProductComboGroupDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { Observable } from "rxjs";
import { ProductComboGroupServiceProxy } from "./../../../../shared/service-proxies/service-proxies";
import { ComboItemsModalComponent } from "./combo-items-modal/combo-items-modal.component";

@Component({
    templateUrl: "./combo-groups.component.html",
    styleUrls: ["./combo-groups.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ComboGroupsComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("comboItemsmodal") comboItemsmodal: ComboItemsModalComponent;
    @ViewChild("selectProductComboGroupPopupModal", { static: true })
    selectProductComboGroupPopupModal: SelectPopupComponent;
    comboGroupSelectionIds = null;
    filterText = "";
    isDeleted;

    exporting = false;
    productCombo = new ProductComboDto();
    constructor(
        injector: Injector,
        private _productComboGroupServiceProxy: ProductComboGroupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._productComboGroupServiceProxy
            .getAll(
                this.filterText,
                this.comboGroupSelectionIds,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    resetFilter() {
        this.filterText = "";
        this.isDeleted = false;
        this.paginator.changePage(0);
    }

    search() {
        this.getItems();
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    // --

    exportToExcel() {
        this.exporting = true;

        this._productComboGroupServiceProxy
            .exportToExcel()
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);

                this.exporting = false;
            });
    }

    // --

    deleteItem(item: GetAllPrinterDto) {
        this.message.confirm(
            this.l("DeleteComboGroupWarning") + " " + item.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._productComboGroupServiceProxy
                        .delete(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }
    revertItem(item: GetAllPrinterDto) {
        const body = EntityDto.fromJS({ id: item.id });
        this._productComboGroupServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    // --

    createOrUpdateItem(id?: number) {
        this._router.navigate([
            "/app/connect/menu/combo-groups/create-or-update",
            id ? id : "",
        ]);
    }

    showComboItems(comboGroupId) {
        this.comboItemsmodal.show(comboGroupId);
    }
    refresh() {
        this.filterText = "";
        this.comboGroupSelectionIds = "";
        this.paginator.changePage(0);
    }
    openFilterSelectComboGroupModal() {
        this.selectProductComboGroupPopupModal.show();
    }
    getComboGroups() {
        return (
            filterText,
            comboGroupSelectionIds,
            isDeleted,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfProductComboGroupEditDto> =>
            this._productComboGroupServiceProxy.getAll(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }
    productComboChange(items: ProductComboGroupDto[]): void {
        this.productCombo.productComboGroupDtos = [...items];
        this.comboGroupSelectionIds = this.productCombo.productComboGroupDtos
            .map((el) => el.id)
            .join(",");
    }
}

import { Component, OnInit, Injector, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    CategoryDto,
    ProfileServiceProxy,
    CategoryServiceProxy,
    CommonLookupServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize } from "rxjs/operators";
import { TokenService } from "abp-ng2-module";
import { CreateOrEditLanguageDescriptionModalComponent } from "../create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";

@Component({
    selector: "app-create-or-edit-category",
    templateUrl: "./create-or-edit-category.component.html",
    styleUrls: ["./create-or-edit-category.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditCategoryComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;
    groups = [];
    id: number;

    categoryEditDto = new CategoryDto();
    saving = false;

    private sub: any;

    constructor(
        injector: Injector,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _commonLookupService: CommonLookupServiceProxy,
        private _tokenService: TokenService
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.id = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.id)) {
                this.id = undefined;
            }
            this._categoryServiceProxy
                .getCategoryForEdit(this.id)
                .subscribe((result) => {
                    this.categoryEditDto = result;
                });

            this.init();
        });
    }

    save() {
        this.saving = true;

        this._categoryServiceProxy
            .createOrUpdate(this.categoryEditDto)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/menu/category"]);
    }

    init(): void {
        this._commonLookupService
            .getProductGroupsForCombobox(undefined)
            .subscribe((result) => {
                this.groups = result.items;
            });
    }

    showCategory() {
        this.createOrEditLanguageDescriptionModal.show(
            "Category",
            this.id,
            this.id + " - " + this.categoryEditDto.name
        );
    }
}

import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
    CategoryServiceProxy,
    CategoryDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    templateUrl: "./categories.component.html",
    styleUrls: ["./categories.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class CategoriesComponent extends AppComponentBase {
    @ViewChild("categoryTable", { static: true }) categoryTable: Table;
    @ViewChild("categoryPaginator", { static: true })
    categoryPaginator: Paginator;

    filterText = "";
    isDeleted = false;

    constructor(
        injector: Injector,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
    }

    getCategoryList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.categoryPaginator.changePage(0);

            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this._categoryServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.categoryTable),
                this.primengTableHelper.getMaxResultCount(
                    this.categoryPaginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(
                    this.categoryPaginator,
                    event
                )
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    deleteCategory(category: CategoryDto) {
        this.message.confirm(
            "You want to delete category " + category.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._categoryServiceProxy
                        .delete(category.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    revertCategory(category: CategoryDto) {
        const body = EntityDto.fromJS({ id: category.id });
        this._categoryServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l("Successfully"));
            this.reloadPage();
        });
    }

    craeteOrUpdateCategory(id?: number): void {
        this._router.navigate([
            "/app/connect/menu/category/create-or-update",
            id ? id : "null",
        ]);
    }

    reloadPage(): void {
        this.categoryPaginator.changePage(this.categoryPaginator.getPage());
    }

    exportToExcel(): void {
        this._categoryServiceProxy
            .getCategoryToExcel(this.filterText, false, "", 10, 0)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}

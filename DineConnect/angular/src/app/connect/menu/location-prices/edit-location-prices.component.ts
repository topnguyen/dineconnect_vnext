import { LocationPriceInput, MenuItemServiceProxy } from '@shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ProgramSettingTemplatesServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-edit-location-prices',
  templateUrl: './edit-location-prices.component.html',
  styleUrls: ['./edit-location-prices.component.scss'],
  animations: [appModuleAnimation()]
})
export class EditLocationPricesComponent extends AppComponentBase implements OnInit {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @ViewChild('form', { static: true }) form: NgForm;

  @Input() locationId: number;

  saving = false;

  data: any[] = [];
  itemIndex = 0;

  model: any = {};

  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _changeDetector: ChangeDetectorRef,
    private _menuItemServiceProxy: MenuItemServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit() { }

  onShown() { }

  show(data: any[], index: number) {
    this.data = data;
    this.itemIndex = index;

    this.model = _.cloneDeep(this.data[this.itemIndex]);

    this.modal.show();
  }

  save(next = false) {
    if (this.saving) {
      return;
    }

    this.saving = true;

    const input = new LocationPriceInput();
    input.portionId = this.model.id;
    input.price = this.model.locationPrice;
    input.locationId = this.locationId;

    this._menuItemServiceProxy.updateLocationPrice([input]).pipe(
      finalize(() => {
        this.saving = false;
      })
    )
      .subscribe((result) => {
        this.notify.success(this.l('SavedSuccessfully'));

        this.data[this.itemIndex] = this.model;
        if (next) {
          this.next();
        } else {
          this.close();
        }
      });
  }

  close() {
    this.modal.hide();
    this.saving = false;
  }

  prev() {
    if (this.itemIndex > 0) {
      this.itemIndex--;
      this.model = _.cloneDeep(this.data[this.itemIndex]);
    }
  }

  next() {
    if (this.itemIndex < this.data.length - 1) {
      this.itemIndex++;
      this.model = _.cloneDeep(this.data[this.itemIndex]);
    }
  }
}

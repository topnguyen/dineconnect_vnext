import {
    Component,
    Injector,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import {
    SelectSingleLocationComponent,
    SingleLocationDto,
} from "@app/shared/common/select-single-location/select-single-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CommonLocationGroupDto,
    LocationServiceProxy,
    MenuItemServiceProxy,
    SimpleLocationDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import _ from "lodash";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { GetAllProgramSettingTemplateDto } from "../../../../shared/service-proxies/service-proxies";
import { EditLocationPricesComponent } from "./edit-location-prices.component";

@Component({
    templateUrl: "./location-prices.component.html",
    styleUrls: ["./location-prices.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class LocationPricesComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("importModal", { static: true })
    importModal: ImportModalComponent;
    @ViewChild("selectLocationModal", { static: true })
    selectLocationModal: SelectSingleLocationComponent;
    @ViewChild("editModal", { static: true })
    editModal: EditLocationPricesComponent;

    filterText = "";

    importing = false;

    location = new SingleLocationDto();

    // --

    locationId = 0;

    // --

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _loactionServiceProxy: LocationServiceProxy
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
        this.locationId = Number(
            this._activatedRoute.snapshot.params["locationId"]
        );

        this._router.events.subscribe((e) => {
            if (e instanceof NavigationEnd) {
                const locationId = Number(
                    this._activatedRoute.snapshot.params["locationId"]
                );

                if (locationId !== this.locationId) {
                    this.locationId = locationId;
                    this.resetFilter();
                }
            }
        });
        if (!isNaN(this.locationId))
            this._loactionServiceProxy
                .get(this.locationId)
                .subscribe((result) => {
                    this.location.locationName = result.name;
                });
    }

    ngOnInit() {
        this.configImport();
    }

    configImport() {
        this.importModal.configure({
            title: this.l("LocationPrice"),
            downloadTemplate: (id?: number) => {
                return this._menuItemServiceProxy.getMenuPriceTemplate(
                    this.locationId
                );
            },
            import: (fileToken: string) => {
                return this._menuItemServiceProxy.importLocationMenuItemPricesToDatabase(
                    fileToken,
                    this.locationId
                );
            },
        });
    }

    getItems(event?: LazyLoadEvent) {
        if (isNaN(this.locationId) || !this.locationId) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._menuItemServiceProxy
            .getLocationMenuItemPrices(
                this.filterText,
                this.locationId,
                undefined,
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getSorting(this.dataTable)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    openSelectLocationModal() {
        this.selectLocationModal.show();
    }

    resetFilter() {
        this.filterText = "";
        this.paginator.changePage(0);
    }

    search() {
        this.paginator.changePage(0);
    }

    setLocation(location: SingleLocationDto) {
        if (location) {
            this._router.navigate([
                "/app/connect/menu/location-prices",
                location.locationId,
            ]);
            this.location = new SingleLocationDto();
        }
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    // --

    import() {
        this.importModal.show();
    }

    editItem(index: number) {
        this.editModal.show(this.primengTableHelper.records, index);
    }
}

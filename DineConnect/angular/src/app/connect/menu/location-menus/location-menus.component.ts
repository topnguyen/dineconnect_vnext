import {
    Component,
    Injector,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import {
    SelectSingleLocationComponent,
    SingleLocationDto,
} from "@app/shared/common/select-single-location/select-single-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { PrimengTableHelper } from "@shared/helpers/PrimengTableHelper";
import {
    CommonLocationGroupDto,
    LocationMenuItemInput,
    MenuItemListDto,
    MenuItemServiceProxy,
    SimpleLocationDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import _ from "lodash";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { map } from "rxjs/operators";
import { GetAllProgramSettingTemplateDto } from "../../../../shared/service-proxies/service-proxies";

@Component({
    templateUrl: "./location-menus.component.html",
    styleUrls: ["./location-menus.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class LocationMenusComponent extends AppComponentBase implements OnInit {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    locationMenuItemPrimengTableHelper = new PrimengTableHelper();
    @ViewChild("locationMenuItemDataTable", { static: true })
    locationMenuItemDataTable: Table;
    @ViewChild("locationMenuItemPaginator", { static: true })
    locationMenuItemPaginator: Paginator;

    @ViewChild("importModal", { static: true })
    importModal: ImportModalComponent;
    @ViewChild("selectLocationModal", { static: true })
    selectLocationModal: SelectSingleLocationComponent;

    // --

    filterInput: {
        menuItemText?: string;
        locationMenuText?: string;
    } = {};

    filter: {
        menuItemText?: string;
        locationMenuText?: string;
    } = {};

    exporting = false;
    importing = false;

    location = new SingleLocationDto();

    // --

    locationId = 0;

    // --

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterInput.menuItemText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";

        this.primengTableHelper.records = [];
        this.primengTableHelper.totalRecordsCount = 0;

        this.locationMenuItemPrimengTableHelper.records = [];
        this.locationMenuItemPrimengTableHelper.totalRecordsCount = 0;
    }

    ngOnInit() {
        this.configImport();
    }

    configImport() {
        this.importModal.configure({
            title: this.l("LocationMenu"),
            downloadTemplate: (id?: number) => {
                return this._menuItemServiceProxy.getLocationMenuItemTemplate();
            },
            import: (fileToken: string) => {
                return this._menuItemServiceProxy
                    .importLocationMenuItems(fileToken, this.locationId)
                    .pipe(
                        map((x) => {
                            this.locationMenuItemPaginator.changePage(0);
                            return x;
                        })
                    );
            },
        });
    }

    getMenuItems(event?: LazyLoadEvent) {
        if (isNaN(this.locationId) || !this.locationId) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._menuItemServiceProxy
            .getAll(
                this.filter.menuItemText,
                false,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),                
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    getLocationMenuItems(event?: LazyLoadEvent) {
        if (isNaN(this.locationId) || !this.locationId) {
            return;
        }

        if (this.locationMenuItemPrimengTableHelper.shouldResetPaging(event)) {
            this.locationMenuItemPaginator.changePage(0);
            return;
        }

        this.locationMenuItemPrimengTableHelper.showLoadingIndicator();

        this._menuItemServiceProxy
            .getLocationMenuItems(
                this.locationId,
                this.filter.locationMenuText,
                this.locationMenuItemPrimengTableHelper.getSorting(
                    this.locationMenuItemDataTable
                ),
                this.locationMenuItemPrimengTableHelper.getMaxResultCount(
                    this.locationMenuItemPaginator,
                    event
                ),
                this.locationMenuItemPrimengTableHelper.getSkipCount(
                    this.locationMenuItemPaginator,
                    event
                )
            )
            .subscribe((result) => {
                this.locationMenuItemPrimengTableHelper.totalRecordsCount =
                    result.totalCount;
                this.locationMenuItemPrimengTableHelper.records = result.items;
                this.locationMenuItemPrimengTableHelper.hideLoadingIndicator();
            });
    }

    openSelectLocationModal() {
        this.selectLocationModal.show();
    }

    searchMenuItems() {
        this.filter.menuItemText = this.filterInput.menuItemText;
        this.paginator.changePage(0);
    }

    searchLocationMenuItems() {
        this.filter.locationMenuText = this.filterInput.locationMenuText;
        this.locationMenuItemPaginator.changePage(0);
    }

    setLocation(location: SingleLocationDto) {
        if (location) {
            this.location = location;
            this.locationId = location.locationId;

            this.resetFilter();
        }
    }

    resetFilter() {
        this.filterInput = {};
        this.filter = {};
        this.paginator.changePage(0);
        this.locationMenuItemPaginator.changePage(0);
    }

    // --

    import() {
        this.importModal.show();
    }

    export() {
        this.exporting = true;

        this._menuItemServiceProxy
            .exportLocationMenuItems(this.locationId)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);

                this.exporting = false;
            });
    }

    // --

    addLocationMenuItem(item: MenuItemListDto) {
        const input = new LocationMenuItemInput();
        input.locationId = this.locationId;
        input.menuItemId = item.id;

        this._menuItemServiceProxy
            .createLocationMenuItem(input)
            .subscribe((result) => {
                this.locationMenuItemPaginator.changePage(0);
            });
    }

    removeLocationMenuItem(item: MenuItemListDto) {
        this._menuItemServiceProxy
            .deleteLocationMenuItem(
                this.locationId,
                item.id,
                undefined,
                undefined,
                undefined
            )
            .subscribe((result) => {
                this.locationMenuItemPaginator.changePage(0);
            });
    }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, CreateOrUpdateScreenMenuItem, MenuItemEditDto, MenuItemServiceProxy, ScreenMenuItemEditDto, ScreenMenuItemListDto, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './screen-menu-menuitem-edit.component.html',
    styleUrls: ['./screen-menu-menuitem-edit.component.scss'],
    animations: [appModuleAnimation()]
})
export class ScreenMenuMenuitemEditComponent extends AppComponentBase implements OnInit {
    @ViewChild('menuItemLookupModal', { static: true }) menuItemLookupModal: CommonLookupModalComponent;
    @ViewChild('menuItemForm', { static: true }) form: NgForm;

    screenMenuId = 0;
    screenMenuCategoryId = 0;
    locationId = 0;

    saving = false;
    menuItem = new ScreenMenuItemEditDto();
    copyToAll = false;
    confirmationTypes: ComboboxItemDto[] = [];
    isLoading = false;
    menuItemId = 0;
    selectedMenuItem: MenuItemEditDto;
    replyFiles: any[] = [];
    tags = [];
    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
    uploader: FileUploader;
    refreshImage = false;
    tagText = '';
    menuItems: MenuItemEditDto[] = [];

    errorMessages = {
        'minlength': 'The tag should be at least 3 characters.',
    };

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _tokenService: TokenService,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['screenMenuId'];
            this.screenMenuCategoryId = +params['screenMenuCategoryId'];
            this.screenMenuCategoryId = +params['screenMenuCategoryId'];
            this.menuItemId = +params['id'];
            this.getConfirmationTypes();
            this.configLookup();

            this.getMenuItems();
        });
        this.locationId = +this._activatedRoute.snapshot.queryParams['location'];
        this.uploadInit();
    }

    getMenuItems() {
        this._menuItemServiceProxy
            .getAllItems(
                undefined,
                undefined,
                this.locationId,
                1000,
                0,
                undefined,
                undefined,
            )
            .subscribe(result => {
                this.menuItems = result.items;

                this.getMenuItem();
            });
    }

    configLookup() {
        this.menuItemLookupModal.configure({
            title: this.l('SelectMenuItem'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string) => {
                return this._menuItemServiceProxy.getAll(filter, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'name ASC', skipCount, maxResultCount) as any;
            }
        });
    }

    openforMenuItem() {
        this.menuItemLookupModal.show();
    }

    menuItemSelected(event) {
        this.selectedMenuItem = event;
    }

    getMenuItem() {
        this._screenMenuServiceProxy.getMenuItemForEdit(this.menuItemId).subscribe(result => {
            this.menuItem = result.menuItem;

            if (this.menuItem.subMenuTag != null) {
                this.getSubMenuTags(this.menuItem.subMenuTag);
            }

            this.selectedMenuItem =  this.menuItems.find(menuItem => menuItem.id === result.menuItemId);
            this.replyFiles = this.menuItem.files ? JSON.parse(this.menuItem.files) : [];
        });
    }

    getSubMenuTags(tag) {
        const tags = tag.split(',');

        (tags || []).forEach((item) => {
            if (item) {
                this.tags.push({ value: item, display: item });
            }
        });
    }

    setSubMenuTags() {
        const tags = [];
        (this.tags || []).forEach((item) => {
            tags.push(item.value);
        });
        this.menuItem.subMenuTag = tags.join(',');
    }

    save() {
        this.saving = true;
        this.setSubMenuTags();
        this.menuItem.refreshImage = this.refreshImage;
        this.menuItem.files = JSON.stringify(this.replyFiles);

        const input = CreateOrUpdateScreenMenuItem.fromJS({
            menuItem: this.menuItem,
            copyToAll: this.copyToAll,
            menuItemId: this.selectedMenuItem.id,
            categoryId: this.screenMenuCategoryId,
        });

        this._screenMenuServiceProxy.createOrUpdateMenuItem(input).subscribe(result => {
            this.notify.success(this.l("SavedSuccessfully"));
            this.saving = false;
            this.close();
        });
    }

    close() {
        this._router.navigate(['/app/connect/menu/screen-menus', this.screenMenuId, 'screen-menu-categories', this.screenMenuCategoryId, 'screen-menu-menuitems'], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    minlength(control: FormControl) {
        if (control.value.length < 3) {
            return {
                'minlength': true
            };
        }

        return null;
    }

    uploadInit() {
        this.uploader = new FileUploader({
            url: this.remoteServiceBaseUrl,
            authToken: 'Bearer ' + this._tokenService.getToken(),
        });

        this.uploader.onAfterAddingFile = fileItem => {
            this.primengTableHelper.showLoadingIndicator();

            fileItem.upload();
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            this.primengTableHelper.hideLoadingIndicator();
            if (ajaxResponse.success) {
                this.notify.success(this.l('UploadImageSuccessfully'));
                if (ajaxResponse.result.url) {
                    this.replyFiles.push(ajaxResponse.result.url);
                    this.refreshImage = true;
                } else {
                    this.message.error(ajaxResponse.result.message);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };

        this.uploader.onErrorItem = () => {
            this.message.error(this.l('UploadImageFailed'));
            this.primengTableHelper.hideLoadingIndicator();
        };
    }

    removeFile(findex) {
        this.replyFiles.splice(findex, 1);
        this.refreshImage = true;
    }


    getConfirmationTypes() {
        this.isLoading = true;
        this._screenMenuServiceProxy.getConfirmationTypes()
            .pipe(finalize(() => {
                this.isLoading = false;
            }))
            .subscribe(result => {
                this.confirmationTypes = result.items || [];
            });
    }

    removeSubMenuTags(index) {
        if (index > -1) {
            this.tags.splice(index, 1);
        }
    }

    onTextChange(event) {
        this.tagText = event;
    }

    get formIsValid() {
        return this.form.form.valid && (!this.tagText || (this.tagText.length >= 3));
    }

}

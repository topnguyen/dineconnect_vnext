import {
    Component,
    EventEmitter,
    Injector,
    OnInit,
    Output,
    ViewChild, ViewEncapsulation
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateCategoryInput, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'add-screen-menu-category',
    templateUrl: './add-screen-menu-category.component.html',
    styleUrls: ['./add-screen-menu-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AddScreenMenuCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('createModal', { static: true }) modal: ModalDirective;
    @ViewChild('form', { static: true }) form: NgForm;

    @Output() saved: EventEmitter<any> = new EventEmitter<any>();

    screenMenuId = 0;

    saving = false;
    categoryInput = new CreateCategoryInput();

    constructor(
        injector: Injector,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _activatedRoute: ActivatedRoute,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['id'];
        });
    }

    showModal(screenMenuId?) {
        if (screenMenuId) {
            this.screenMenuId = screenMenuId;
        }

        this.categoryInput = new CreateCategoryInput();
        this.modal.show();
    }

    closeModal() {
        this.form.form.reset();
        this.modal.hide();
    }

    saveCategory() {
        if (this.saving) {
            return;
        }

        this.categoryInput.screenMenuId = this.screenMenuId;

        this._screenMenuServiceProxy.createCategory(this.categoryInput).pipe(
            finalize(() => { this.saving = false; })
        ).subscribe((result) => {
            this.notify.success(this.l('SavedSuccessfully'));
            this.categoryInput = new CreateCategoryInput();
            this.closeModal();

            this.saved.emit();
        });
    }
}

import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ScreenMenuItemListDto, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './screen-menu-menuitem.component.html',
    styleUrls: ['./screen-menu-menuitem.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ScreenMenuMenuitemComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('sortModal', { static: true }) sortModal: ModalDirective;

    screenMenuId = 0;
    screenMenuCategoryId = 0;
    locationId = 0;
    saving = false;

    filter: {
        text?: string,
    } = {};

    menuItems: ScreenMenuItemListDto[];

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['screenMenuId'];
            this.screenMenuCategoryId = +params['id'];
            this.getMenuItems();
        });
        this.locationId = +this._activatedRoute.snapshot.queryParams['location'];
    }

    create() {
        this._router.navigate(['/app/connect/menu/screen-menus', this.screenMenuId, 'screen-menu-categories', this.screenMenuCategoryId, 'screen-menu-menuitems', 'add'], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    edit(menuItem: ScreenMenuItemListDto) {
        this._router.navigate(['/app/connect/menu/screen-menus', this.screenMenuId, 'screen-menu-categories', this.screenMenuCategoryId, 'screen-menu-menuitems', 'edit', menuItem.id], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    deleteItem(menuItem: ScreenMenuItemListDto) {
        this.message.confirm(this.l('DeleteScreenMenuItemWarning') + ' ' + menuItem.name,
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._screenMenuServiceProxy
                        .deleteScreenMenuItem(menuItem.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    closeSortModal() {
        this.saving = false;
        this.sortModal.hide();
    }

    saveSortMenuItem() {
        const ids = this.menuItems.map(x => x.id);

        this.saving = true;

        this._screenMenuServiceProxy
            .saveSortSortItems(ids)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.closeSortModal();
                this.reloadPage();
            });
    }

    reloadPage() {
        this.getMenuItems();
    }

    getMenuItems(event?: LazyLoadEvent) {
        if (!this.screenMenuId || this.screenMenuId < 1) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._screenMenuServiceProxy
            .getMenuItems(
                this.filter.text,
                undefined,
                this.screenMenuId,
                this.screenMenuCategoryId,
                undefined,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable) || undefined,
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => {
                this.primengTableHelper.hideLoadingIndicator();
            }))
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
            });
    }

    resetFilter() {
        this.filter = {};
        this.paginator.changePage(0);
    }

    search() {
        this.getMenuItems();
    }

    showSortModal() {
        this.menuItems = [];

        this._screenMenuServiceProxy
            .getMenuItems(
                undefined,
                undefined,
                this.screenMenuId,
                this.screenMenuCategoryId,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                0,
                1000,
            )
            .subscribe(result => {
                this.menuItems = result.items;
            });

        this.sortModal.show();
    }

    back() {
        this._router.navigate(['/app/connect/menu/screen-menus', this.screenMenuId, 'screen-menu-categories']);
    }

}

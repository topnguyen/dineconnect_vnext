import {
    Component,
    Injector,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import * as _ from "lodash";
import { LazyLoadEvent, Paginator, Table } from "primeng";

import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { CommonLocationGroupDto, EntityDto } from "@shared/service-proxies/service-proxies";
import {
    GetAllScreenMenuDto,
    ScreenMenuServiceProxy,
} from "./../../../../shared/service-proxies/service-proxies";
import { AddScreenMenuCategoryComponent } from "./screen-menu-categories/add-screen-menu-category.component";

@Component({
    templateUrl: "./screen-menus.component.html",
    styleUrls: ["./screen-menus.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ScreenMenusComponent extends AppComponentBase implements OnInit {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("importModal", { static: true })
    importModal: ImportModalComponent;
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("createCategoryModal", { static: true })
    createCategoryModal: AddScreenMenuCategoryComponent;

    isDeleted?: boolean;
    filterText?: string;
    locationOrGroup?: CommonLocationGroupDto;

    importing = false;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
    }

    ngOnInit() {
        this.configImport();
    }

    configImport() {
        this.importModal.configure({
            title: this.l("ScreenMenu"),
            downloadTemplate: (id?: number) => {
                return this._screenMenuServiceProxy.getImportTemplate();
            },
            import: (fileToken: string) => {
                return this._screenMenuServiceProxy.import(fileToken);
            },
        });
    }

    getItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._screenMenuServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                undefined,
                undefined,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable) || undefined,
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    refresh() {
        this.filterText = '';
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getItems();
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    import() {
        this.importModal.show();
    }

    showVitualScreen(item: GetAllScreenMenuDto) {
        this._router.navigate(
            [
                "/app/connect/menu/screen-menus",
                item.id,
                "screen-menu-virtualscreen",
            ],
            {
                queryParams: {
                    location: item.refLocationId,
                },
            }
        );
    }

    generateItem(item: GetAllScreenMenuDto) {
        this.primengTableHelper.showLoadingIndicator();

        this._screenMenuServiceProxy
            .generate(item.id, false)
            .subscribe((result) => {
                this.notify.success(this.l("Done"));
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    cloneItem(item: GetAllScreenMenuDto) {
        this.message.confirm(
            this.l("CloneScreenMenuWarning") + item.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._screenMenuServiceProxy
                        .clone(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    deleteItem(item: GetAllScreenMenuDto) {
        this.message.confirm(
            this.l("DeleteScreenMenuWarning") + item.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._screenMenuServiceProxy
                        .delete(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    listCategories(item: GetAllScreenMenuDto) {
        this._router.navigate(
            [
                "/app/connect/menu/screen-menus",
                item.id,
                "screen-menu-categories",
            ],
            {
                queryParams: {
                    location: item.refLocationId,
                },
            }
        );
    }

    revertItem(item: GetAllScreenMenuDto) {
        const body = new EntityDto({id: item.id});
        this._screenMenuServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('SuccessfullyReverted'));
            this.reloadPage();
        });
    }

    showCreateCategoryModal(item) {
        this.createCategoryModal.showModal(item.id);
    }

    // --

    createOrUpdateItem(id?: number) {
        this._router.navigate([
            "/app/connect/menu/screen-menus/create-or-update",
            id ? id : "",
        ]);
    }
}

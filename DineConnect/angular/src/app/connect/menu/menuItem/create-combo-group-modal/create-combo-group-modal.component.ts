import { Component, ViewChild, Injector, Output, EventEmitter, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ProductComboGroupEditDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { Table, Paginator } from 'primeng';

@Component({
  selector: 'create-combo-group-modal',
  templateUrl: './create-combo-group-modal.component.html',
})
export class CreateComboGroupModalComponent extends AppComponentBase {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<ProductComboGroupEditDto> = new EventEmitter<ProductComboGroupEditDto>();

  active = false;
  saving = false;
  isEdit = false;
  comboGroup: ProductComboGroupEditDto = new ProductComboGroupEditDto();
  filterText = '';

  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  show(combo?: ProductComboGroupEditDto): void {

    this.comboGroup = combo || new ProductComboGroupEditDto();
    this.isEdit = this.comboGroup.name ? true : false;

    this.active = true;
    this.modal.show();

  }

  save(): void {
    this.saving = true;

    this.modalSave.emit(this.comboGroup);
    this.close();
  }

  close(): void {
    this.active = false;
    this.saving = false;
    this.modal.hide();
  }
}

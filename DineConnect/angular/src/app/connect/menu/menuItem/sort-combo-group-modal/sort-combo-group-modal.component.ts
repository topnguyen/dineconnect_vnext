import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
} from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";
import {
    ProductComboGroupServiceProxy,
    ProductComboItemEditDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Table, Paginator } from "primeng";
import { finalize } from "rxjs/operators";

@Component({
    selector: "app-sort-combo-group-modal",
    templateUrl: "./sort-combo-group-modal.component.html",
    styleUrls: ["./sort-combo-group-modal.component.css"],
})
export class SortComboGroupModalComponent extends AppComponentBase {
    @ViewChild("sortModal", { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> =
        new EventEmitter<any>();

    active = false;
    saving = false;
    comboGroups = [];

    constructor(
        injector: Injector,
        private _productComboGroupServiceProxy: ProductComboGroupServiceProxy
    ) {
        super(injector);
    }

    show(combos?: ProductComboItemEditDto[]): void {
        this.comboGroups = [];
        if (combos && combos.length) {
            combos.map((item) => {
                if (item.id) {
                    this.comboGroups.push(item);
                }
            });
        }

        this.active = true;
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        let input = [];
        this.comboGroups.map((item, index) => {
            item.sortOrder = index;
            input.push(item);
        });

        this._productComboGroupServiceProxy
            .sortComboGroups(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                    this.modalSave.emit(this.comboGroups);
                    this.close();
                })
            )
            .subscribe(() => {
                this.notify.success("Successfully");
            });
        
    }

    close(): void {
        this.active = false;
        this.saving = false;
        this.modal.hide();
    }
}

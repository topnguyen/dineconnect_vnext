import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortComboGroupModalComponent } from './sort-combo-group-modal.component';

describe('SortComboGroupModalComponent', () => {
  let component: SortComboGroupModalComponent;
  let fixture: ComponentFixture<SortComboGroupModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortComboGroupModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortComboGroupModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

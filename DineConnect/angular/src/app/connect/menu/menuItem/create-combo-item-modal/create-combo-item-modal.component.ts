import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateEditProductComboItemDto, IdNameDto, ProductComboItemEditDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SelectItemModalComponent } from '@app/connect/discount/select-item-modal/select-item-modal.component';
import { Table, Paginator } from 'primeng';

@Component({
  selector: 'create-combo-item-modal',
  templateUrl: './create-combo-item-modal.component.html',
})
export class CreateComboItemModalComponent extends AppComponentBase {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @ViewChild('selectItemModal', { static: true }) selectItemModal: SelectItemModalComponent;

  @Output() modalSave: EventEmitter<CreateEditProductComboItemDto> = new EventEmitter<CreateEditProductComboItemDto>();

  active = false;
  saving = false;
  isEdit = false;
  comboItem: CreateEditProductComboItemDto = new CreateEditProductComboItemDto();
  filterText = '';

  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  show(combo?: CreateEditProductComboItemDto): void {
    if (!combo) {
      this.comboItem = new CreateEditProductComboItemDto();
      this.comboItem.sortOrder = 0;
      this.comboItem.count = 1;
      this.comboItem.price = 0;
      this.comboItem.buttonColor = '#E6E784';
    } else {
      this.comboItem = combo;
    }
    this.isEdit = this.comboItem.name ? true : false;

    this.active = true;
    this.modal.show();

  }

  save(): void {
    this.saving = true;

    this.modalSave.emit(this.comboItem);
    this.close();
  }

  openforMenuItem(comboItem: CreateEditProductComboItemDto) {
    //this.selectItemModal.promotionRestrictItem = comboItem;
    this.selectItemModal.show();
  }

  selectMenuItem(newItem: IdNameDto) {
    this.comboItem.menuItemPortionId = newItem.id;
    this.comboItem.menuItemId = newItem.menuItemId;
    this.comboItem.name = newItem.name;
  }

  removeComboItem() {
    this.comboItem = new CreateEditProductComboItemDto();
  }

  close(): void {
    this.active = false;
    this.saving = false;
    this.modal.hide();
  }
}

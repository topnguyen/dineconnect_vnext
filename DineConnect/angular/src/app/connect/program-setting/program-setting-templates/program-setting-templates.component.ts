import { GetAllProgramSettingTemplateDto, ProgramSettingTemplatesServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryServiceProxy, CategoryDto, CommonLocationGroupDto } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import _ from 'lodash';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';


@Component({
    templateUrl: './program-setting-templates.component.html',
    styleUrls: ['./program-setting-templates.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProgramSettingTemplatesComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;

    // --

    filterIsDeleted = false;

    filter: {
        text?: string,
        locations?: CommonLocationGroupDto,
    } = {};

    exporting = false;

    constructor(
        injector: Injector,
        private _programSettingTemplatesServiceProxy: ProgramSettingTemplatesServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filter.text = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }

    getItems(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        let locationIds = '';
        const filterLocations = this.filter.locations ?? new CommonLocationGroupDto();

        if (filterLocations.group) {
            locationIds = filterLocations.groups.map(x => x.id).join(',');
        } else if (filterLocations.locationTag) {
            locationIds = filterLocations.locationTags.map(x => x.id).join(',');
        } else if (filterLocations.nonLocations?.length) {
            locationIds = filterLocations.nonLocations.map(x => x.id).join(',');
        } else if (filterLocations.locations?.length) {
            locationIds = filterLocations.locations.map(x => x.id).join(',');
        }

        this._programSettingTemplatesServiceProxy
            .getAll(
                this.filter.text,
                this.filterIsDeleted,
                locationIds,
                filterLocations.group,
                filterLocations.locationTag,
                !!filterLocations.nonLocations?.length,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.filter.locations);
    }

    resetFilter() {
        this.filter = {};
        this.paginator.changePage(0);
    }

    search() {
        this.getItems();
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.filter.locations = event;
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel() {
        this.exporting = true;

        this._programSettingTemplatesServiceProxy.exportToExcel()
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);

                this.exporting = false;
            });
    }

    // --

    deleteItem(item: GetAllProgramSettingTemplateDto) {
        this.message.confirm(this.l('DeleteSettingTemplateWarning') + item.name,
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._programSettingTemplatesServiceProxy
                        .delete(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    // --

    createOrUpdateItem(id?: number) {
        this._router.navigate(['/app/connect/programSetting/templates/create-or-update', id ? id : '']);
    }
}

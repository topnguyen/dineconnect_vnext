import { LocationHelper } from './../../../../shared/helpers/LocationHelper';
import { CommonLocationGroupDto, CreateOrEditProgramSettingValueInput, GetAllProgramSettingValueDto } from '@shared/service-proxies/service-proxies';
import { CreateProgramSettingTemplateInput, EditProgramSettingTemplateInput, ProgramSettingTemplatesServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
  selector: 'app-create-or-edit-program-setting-template',
  templateUrl: './create-or-edit-program-setting-template.component.html',
  styleUrls: ['./create-or-edit-program-setting-template.component.scss'],
  animations: [appModuleAnimation()]
})
export class CreateOrEditProgramSettingTemplateComponent extends AppComponentBase implements OnInit {
  @ViewChild('form', { static: true }) form: NgForm;
  @ViewChild('valueForm', { static: true }) valueForm: NgForm;

  @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
  @ViewChild('filterSelectLocationOrGroup', { static: true }) filterSelectLocationOrGroup: SelectLocationOrGroupComponent;

  @ViewChild('valueDataTable', { static: true }) valueDataTable: Table;
  @ViewChild('valuePaginator', { static: true }) valuePaginator: Paginator;

  templateId = 0;

  template = new EditProgramSettingTemplateInput();
  valueInput = new CreateOrEditProgramSettingValueInput();

  saving = false;
  savingValue = false;

  valueFilter = {
    locationGroup: new CommonLocationGroupDto()
  };

  get isEdit() {
    return !!this.templateId;
  }

  get formIsValid() {
    return this.form.form.valid;
  }

  get valueFormIsValid() {
    return this.valueForm.form.valid;
  }

  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _changeDetector: ChangeDetectorRef,
    private _programSettingTemplatesServiceProxy: ProgramSettingTemplatesServiceProxy,
  ) {
    super(injector);

    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
  }

  ngOnInit() {
    this._activatedRoute.params
      .subscribe(params => {
        this.templateId = params['id'] ? +params['id'] : 0;
        this._changeDetector.detectChanges();

        if (this.templateId > 0) {
          this._programSettingTemplatesServiceProxy
            .getForEdit(this.templateId)
            .subscribe(resp => {
              this.template = resp;
            });
          this.getValues();
        } else {

        }
      });
  }

  save() {
    if (!this.formIsValid) {
      return;
    }

    this.saving = true;

    if (this.templateId > 0) {
      this._programSettingTemplatesServiceProxy.edit(this.template)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe((result) => {
          this.notify.success(this.l('SavedSuccessfully'));
          this.close();
        });
    } else {
      const data = new CreateProgramSettingTemplateInput(this.template);
      this._programSettingTemplatesServiceProxy.create(data)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe((result) => {
          this.notify.success(this.l('SavedSuccessfully'));
          this.close();
        });
    }
  }

  close() {
    this._router.navigate(['/app/connect/programSetting/templates']);
  }

  clear() {
    this.valueInput = new CreateOrEditProgramSettingValueInput();
    this.valueFilter = {
      locationGroup: new CommonLocationGroupDto()
    };

    this.getValues();
  }

  // --

  openValueSelectLocationOrGroup() {
    this.selectLocationOrGroup.show(this.valueInput.locationGroup);
  }

  openFilterSelectLocationOrGroup() {
    this.filterSelectLocationOrGroup.show(this.valueFilter.locationGroup);
  }

  setValueLocations(event: CommonLocationGroupDto) {
    this.valueInput.locationGroup = event;
  }

  setValueFilterLocations(event: CommonLocationGroupDto) {
    this.valueFilter.locationGroup = event;
    this.getValues();
  }

  // --

  getValues(event?: LazyLoadEvent) {
    if (!this.templateId) {
      return;
    }

    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.valuePaginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    const locationFilter = LocationHelper.getFilter(this.valueFilter.locationGroup);

    this._programSettingTemplatesServiceProxy
      .getAllValues(
        this.templateId,
        locationFilter.locationIds,
        locationFilter.locationsGroup,
        locationFilter.locationTag,
        locationFilter.nonLocations,
        this.primengTableHelper.getSorting(this.valueDataTable),
        this.primengTableHelper.getSkipCount(this.valuePaginator, event),
        this.primengTableHelper.getMaxResultCount(this.valuePaginator, event)
      )
      .subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  saveValue() {
    if (!this.valueFormIsValid) {
      return;
    }

    this.savingValue = true;

    this.valueInput.programSettingTemplateId = this.templateId;

    this._programSettingTemplatesServiceProxy.createOrUpdateValue(this.valueInput)
      .pipe(finalize(() => { this.savingValue = false; }))
      .subscribe((result) => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.valueInput = new CreateOrEditProgramSettingValueInput();
        this.valueForm.reset();

        this.valuePaginator.changePage(this.valuePaginator.getPage());
      });
  }

  viewValue(item: GetAllProgramSettingValueDto) {
    this._programSettingTemplatesServiceProxy.getValueForEdit(item.id)
      .subscribe((result) => {
        this.selectLocationOrGroup.show(result.locationGroup, true);
      });
  }

  editValue(item: GetAllProgramSettingValueDto) {
    this._programSettingTemplatesServiceProxy.getValueForEdit(item.id)
      .subscribe((result) => {
        this.valueForm.reset();
        this.valueInput = result;
      });
  }

  deleteValue(item: GetAllProgramSettingValueDto) {
    this.message.confirm(this.l('DeleteSettingValueWarning') + item.actualValue,
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this._programSettingTemplatesServiceProxy
            .deleteValue(item.id)
            .subscribe((result) => {
              this.notify.success(this.l('SuccessfullyDeleted'));

              this.valuePaginator.changePage(this.valuePaginator.getPage());
            });
        }
      }
    );
  }
}

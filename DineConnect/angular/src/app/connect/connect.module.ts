import { CommonModule, DecimalPipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppCommonModule } from "@app/shared/common/app-common.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import {
    AddressesServiceProxy,
    CategoryReportServiceProxy,
    CategoryServiceProxy,
    CommonServiceProxy,
    CompanyServiceProxy,
    ConnectCardServiceProxy,
    ConnectCardTypeCategoryServiceProxy,
    ConnectCardTypeServiceProxy,
    ConnectLookupServiceProxy,
    ConnectTableGroupServiceProxy,
    DineDeviceServiceProxy,
    DinePlanUserRoleServiceProxy,
    DinePlanUserServiceProxy,
    GroupReportServiceProxy,
    LocationGroupServiceProxy,
    LocationServiceProxy,
    MenuItemServiceProxy,
    NumeratorServiceProxy,
    OrderPromotionReportServiceProxy,
    OrderReportServiceProxy,
    ProductComboServiceProxy,
    ProductGroupsServiceProxy,
    PromotionCategoryServiceProxy,
    SaleSummaryReportServiceProxy,
    TicketReportServiceProxy,
    TicketSyncReportServiceProxy,
    TicketTypeConfigurationServiceProxy,
    TicketTypeServiceProxy,
    UserLocationServiceProxy,
    WorkPeriodServiceProxy,
    PlanDayReportServiceProxy,
    TicketHourlySalesReportServiceProxy,
    CashSummaryReportServiceProxy
} from "@shared/service-proxies/service-proxies";
import {
    OrderTagGroupServiceProxy,
    CollectionReportServiceProxy,
    ScheduleSalesItemsReportServiceProxy,
    ScheduleSalesLocationReportServiceProxy, 
    TillAccountsServiceProxy,
    PlanLoggerServiceProxy,   
    SaleTargetServiceProxy,
    TenderReportServiceProxy,
    PaymentExcessReportServiceProxy
} from "@shared/service-proxies/service-proxies-nswag";
import { UtilsModule } from "@shared/utils/utils.module";
import { CountoModule } from "angular2-counto";
import { QueryBuilderModule } from "angular2-query-builder";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { DragulaModule } from "ng2-dragula";
import { FileUploadModule } from "ng2-file-upload";
import {
    BsDatepickerConfig,
    BsDatepickerModule,
    BsDaterangepickerConfig,
    BsLocaleService,
} from "ngx-bootstrap/datepicker";
import { MatExpansionModule } from "@angular/material/expansion";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { PopoverModule } from "ngx-bootstrap/popover";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TagInputModule } from "ngx-chips";
import { NgxEasypiechartModule } from "ngx-easypiechart";
import { ImageCropperModule } from "ngx-image-cropper";
import { NgxTagsInputModule } from "ngx-tags-input";
import {
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    ChipsModule,
    EditorModule,
    FileUploadModule as PrimeNgFileUploadModule,
    InputMaskModule,
    MultiSelectModule,
    PickListModule,
    PanelModule,
    TreeModule,
    ContextMenuModule,
    ChartModule,
    OrderListModule,
    TreeTableModule,
    SidebarModule,
} from "primeng";
import { TextMaskModule } from "angular2-text-mask";
import { PaginatorModule } from "primeng/paginator";
import { TableModule } from "primeng/table";
import { ConnectRoutingModule } from "./connect-routing.module";
import { CreateOrEditPromotionComponent } from "./discount/promotions/create-or-edit-promotion.component";
import { PromotionsComponent } from "./discount/promotions/promotions.component";
import { SelectItemModalComponent } from "./discount/select-item-modal/select-item-modal.component";
import { CalculationsComponent } from "./master/calculations/calculations.component";
import { CreateOrEditCalculationComponent } from "./master/calculations/create-or-edit-calculation.component";
import { CreateOrEditDepartmentComponent } from "./master/departments/create-or-edit-department.component";
import { DepartmentsComponent } from "./master/departments/departments.component";
import { CreateOrEditForeignCurrencyModalComponent } from "./master/foreign-currencies/create-or-edit-foreign-currency-modal.component";
import { ForeignCurrenciesComponent } from "./master/foreign-currencies/foreign-currencies.component";
import { CreateOrEditFutureDateComponent } from "./master/future-date/create-or-edit-future-date.component";
import { FutureDatesComponent } from "./master/future-date/future-dates.component";
import { AddLocationModalComponent } from "./master/location-groups/add-location-modal.component";
import { CreateOrEditGroupModalComponent } from "./master/location-groups/create-or-edit-group-modal.component";
import { LocationGroupComponent } from "./master/location-groups/location-group.component";
import { LocationsInGroupComponent } from "./master/location-groups/locations-in-group.component";
import { AddLocationToTagModalComponent } from "./master/location-tags/add-location-to-tag-modal.component";
import { CreateOrEditLocationTagModalComponent } from "./master/location-tags/create-or-edit-tag-modal.component";
import { LocationTagsComponent } from "./master/location-tags/location-tags.component";
import { LocationsInTagComponent } from "./master/location-tags/locations-in-tag.component";
import { CreateOrUpdateLocationModalComponent } from "./master/location/create-or-update-location.component";
import { LocationComponent } from "./master/location/location.component";
import { CreateOrUpdateOrganizationModalComponent } from "./master/organization/create-or-update-organization-modal.component";
import { OrganizationComponent } from "./master/organization/organization.component";
import { CreateOrEditPaymentTypeComponent } from "./master/payment-types/create-or-edit-payment-type.component";
import { PaymentTypesComponent } from "./master/payment-types/payment-types.component";
import { CreateOrEditPlanReasonComponent } from "./master/plan-reasons/create-or-edit-plan-reason.component";
import { PlanReasonsComponent } from "./master/plan-reasons/plan-reasons.component";
import { CreateOrEditDinePlanTaxComponent } from "./master/taxes/create-or-edit-dine-plan-tax.component";
import { DinePlanTaxesComponent } from "./master/taxes/dine-plan-taxes.component";
import { SelectLocationModalComponent } from "./master/taxes/select-location/select-location-modal.component";
import { CreateOrEditTerminalComponent } from "./master/terminals/create-or-edit-terminal.component";
import { TerminalsComponent } from "./master/terminals/terminals.component";
import { CreateOrEditTillAccountComponent } from "./master/till-accounts/create-or-edit-till-account.component";
import { TillAccountsComponent } from "./master/till-accounts/till-accounts.component";
import { CreateOrEditTransactionTypeModalComponent } from "./master/transaction-types/create-or-edit-transaction-type-modal.component";
import { TransactionTypesComponent } from "./master/transaction-types/transaction-types.component";
import { AddUserModalComponent } from "./master/userLocation/add-user-modal.component";
import { UserInLocationComponent } from "./master/userLocation/user-in-location.component";
import { UserLocationComponent } from "./master/userLocation/user-location.component";
import { CategoriesComponent } from "./menu/categories/categories.component";
import { CreateOrEditCategoryComponent } from "./menu/categories/create-or-edit-category.component";
import { ComboGroupsComponent } from "./menu/combo-groups/combo-groups.component";
import { CreateOrEditComboGroupComponent } from "./menu/combo-groups/create-or-edit-combo-group.component";
import { LocationMenusComponent } from "./menu/location-menus/location-menus.component";
import { EditLocationPricesComponent } from "./menu/location-prices/edit-location-prices.component";
import { LocationPricesComponent } from "./menu/location-prices/location-prices.component";
import { CreateComboGroupModalComponent } from "./menu/menuItem/create-combo-group-modal/create-combo-group-modal.component";
import { CreateComboItemModalComponent } from "./menu/menuItem/create-combo-item-modal/create-combo-item-modal.component";
import { CreateOrEditMenuitemComponent } from "./menu/menuItem/create-or-edit-menuitem.component";
import { MenuItemComponent } from "./menu/menuItem/menuItem.component";
import { CreateOrEditProductGroupModalComponent } from "./menu/product-groups/create-or-edit-productGroup-modal.component";
import { ProductGroupsComponent } from "./menu/product-groups/product-groups.component";
import { CreateOrEditScreenMenuComponent } from "./menu/screen-menus/create-or-edit-screen-menu.component";
import { AddScreenMenuCategoryComponent } from "./menu/screen-menus/screen-menu-categories/add-screen-menu-category.component";
import { EditScreenMenuCategoryComponent } from "./menu/screen-menus/screen-menu-categories/edit-screen-menu-category.component";
import { ScreenMenuCategoriesComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-categories.component";
import { ScreenMenusComponent } from "./menu/screen-menus/screen-menus.component";
import { CreateOrEditPrintConditionComponent } from "./print/conditions/create-or-edit-print-condition.component";
import { PrintConditionComponent } from "./print/conditions/print-condition.component";
import { CreateOrEditPrintJobComponent } from "./print/jobs/create-or-edit-print-job.component";
import { PrintJobComponent } from "./print/jobs/print-job.component";
import { CreateOrEditPrinterComponent } from "./print/printers/create-or-edit-printer.component";
import { PrintPrintersComponent } from "./print/printers/printers.component";
import { CreateOrEditPrintTemplateComponent } from "./print/templates/create-or-edit-print-template.component";
import { PrintTemplateComponent } from "./print/templates/print-template.component";
import { CreateOrEditProgramSettingTemplateComponent } from "./program-setting/program-setting-templates/create-or-edit-program-setting-template.component";
import { ProgramSettingTemplatesComponent } from "./program-setting/program-setting-templates/program-setting-templates.component";
import { ImportModalComponent } from "./shared/import-modal/import-modal.component";
import { CreateOrEditPriceTagComponent } from "./tags/price-tags/create-or-edit-price-tag.component";
import { EditPriceForPriceTagComponent } from "./tags/price-tags/edit-price-for-price-tag/edit-price-for-price-tag.component";
import { EditTagPriceModalComponent } from "./tags/price-tags/edit-price-for-price-tag/edit-tag-price-modal.component";
import { PriceTagsComponent } from "./tags/price-tags/price-tags.component";
import { CreateOrEditTicketTagGroupComponent } from "./tags/ticket-tags/create-or-edit-ticket-tag-group.component";
import { TicketTagGroupsComponent } from "./tags/ticket-tags/ticket-tag-groups.component";
import { ScreenMenuMenuitemComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem.component";
import { ScreenMenuMenuitemAddComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-add/screen-menu-menuitem-add.component";
import { ScreenMenuMenuitemEditComponent } from "./menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-edit/screen-menu-menuitem-edit.component";
import { ScreenMenuVirtualscreenComponent } from "./menu/screen-menus/screen-menu-virtualscreen/screen-menu-virtualscreen.component";
import { OrderTagGroupsComponent } from "./tags/order-tag-groups/order-tag-groups.component";
import { CreateOrEditOrderTagGroupComponent } from "./tags/order-tag-groups/create-or-edit-order-tag-group.component";
import { OrderTagGroupCloneComponent } from "./tags/order-tag-groups/order-tag-group-clone/order-tag-group-clone.component";
import { GroupComponent } from "./table/group/group.component";
import { TablesComponent } from "./table/tables/tables.component";
import { CreateTablesComponent } from "./table/tables/create-tables/create-tables.component";
import { ConnectUserRolesComponent } from "./user/user-roles/user-roles.component";
import { CreateUpdateConnectUserRoleComponent } from "./user/user-roles/create-update-user-role/create-update-user-role.component";
import { ConnectUsersComponent } from "./user/users/users.component";
import { CreateUpdateConnectUserComponent } from "./user/users/create-update-user/create-update-user.component";
import { PromotionCategorysComponent } from "./discount/promotion-categorys/promotion-categorys.component";
import { CreateUpdatePromotionCategoryComponent } from "./discount/promotion-categorys/create-update-promotion-category/create-update-promotion-category.component";
import { NumeratorComponent } from "./master/numerator/numerator.component";
import { CreateUpdateNumeratorComponent } from "./master/numerator/create-update-numerator/create-update-numerator.component";
import { TicketTypeComponent } from "./master/ticket-type/ticket-type.component";
import { CreateUpdateTicketTypeComponent } from "./master/ticket-type/create-update-ticket-type/create-update-ticket-type.component";
import { DepartmentGroupComponent } from "./master/department-group/department-group.component";
import { CreateUpdateDepartmentGroupComponent } from "./master/department-group/create-update-department-group/create-update-department-group.component";
import { FormlyModule } from "@ngx-formly/core";
import { CreateGroupComponent } from "./table/group/create-group/create-group.component";
import { PrintDevicesComponent } from "./print/devices/devices.component";
import { CreateOrEditDeviceComponent } from "./print/devices/create-or-edit-device.component";
import { RepeatSection } from "./print/devices/repeat-section/repeat-section.component";
import { CardCategoryComponent } from "./card/card-category/card-category.component";
import { CardTypeComponent } from "./card/card-type/card-type.component";
import { CardsComponent } from "./card/cards/cards.component";
import { CreateOrUpdateCardCategoryComponent } from "./card/card-category/create-or-update-card-category/create-or-update-card-category.component";
import { CreateOrUpdateCardTypeComponent } from "./card/card-type/create-or-update-card-type/create-or-update-card-type.component";
import { CardsModalComponent } from "./card/cards/cards-modal/cards-modal.component";
import { CreateOrEditCardComponent } from "./card/cards/create-or-edit-card/create-or-edit-card.component";
import { ReportTicketComponent } from "./report/tickets/report-ticket/report-ticket.component";
import { TicketSyncComponent } from "./report/tickets/ticket-sync/ticket-sync.component";
import { ReportOrderComponent } from "./report/order/report-order.component";
import { ComboItemsModalComponent } from "./menu/combo-groups/combo-items-modal/combo-items-modal.component";
import { CreateOrEditLanguageDescriptionModalComponent } from "./menu/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
import { FormlyBootstrapModule } from "@ngx-formly/bootstrap";
import { PriceTagLocationPriceComponent } from "./tags/price-tags/price-tag-location-price.component";
import { ProductComboComponent } from "./menu/product-combo/product-combo.component";
import { CreateOrEditProductComboComponent } from "./menu/product-combo/create-or-edit-product-combo.component";
import { UploadFileModalComponent } from "./shared/upload-file-modal/upload-file-modal.component";
import { OrderReportDetailModalComponent } from "./report/order/order-report-detail-modal.component";
import { ItemReportComponent } from "./report/item/item-report.component";
import { ImportCardModalComponent } from "./card/cards/import-card-modal/import-card-modal.component";
import { HourlyComponent } from "./report/item/hourly/hourly.component";
import { OrderTagLocationPriceComponent } from "./tags/order-tag-groups/order-tag-location-price/order-tag-location-price.component";
import { SortComboGroupModalComponent } from "./menu/menuItem/sort-combo-group-modal/sort-combo-group-modal.component";
import { CreateOrEditDeviceModalComponent } from "./master/terminals/create-or-edit-device.component";
import { GroupReportDetailModalComponent } from "./report/group/group-report-detail-modal.component";
import { GroupReportComponent } from "./report/group/group-report.component";
import { CategoryReportDetailModalComponent } from "./report/category/category-report-detail-modal.component";
import { CategoryReportComponent } from "./report/category/category-report.component";
import { OrderPromotionReportComponent } from "./report/promotion/order-promotion/order-promotion-report.component";
import { OrderPromotionReportDetailModalComponent } from "./report/promotion/order-promotion/order-promotion-report-detail-modal.component";
import { ItemReportExportModalComponent } from "./report/item/item-report-export-modal/item-report-export-modal.component";
import { SummaryReportComponent } from "./report/sale/summary-report/summary-report.component";
import { ItemTagReportComponent } from "./report/item/item-tag-report/item-tag-report.component";
import { SaleSummaryPrintComponent } from "./report/sale/summary-report/sale-summary-print/sale-summary-print.component";
import { TopDownReportComponent } from "./report/item/top-down-report/top-down-report.component";
import { DatePipe } from "@angular/common";
import { WorkDayReportComponent } from "./report/sale/work-day-report/work-day-report.component";
import { WorkDayPrintPrintComponent } from "./report/sale/work-day-report/work-day-report-print/work-day-report-print.component";
import { WeekDayReportComponent } from "./report/sale/week-day-report/week-day-report.component";
import { WeekEndReportComponent } from "./report/sale/week-end-report/week-end-report.component";
import { SelectProductGroupModalComponent } from './discount/promotions/select-product-group-modal/select-product-group-modal.component';
import { SelectCategoryPopupComponent } from './discount/promotions/select-category-popup/select-category-popup.component';
import { SelectMenuItemPopupComponent } from './discount/promotions/select-menu-item-popup/select-menu-item-popup.component';
import { SelectPortionPopupComponent } from './discount/promotions/select-portion-popup/select-portion-popup.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { SortItemComponent } from './discount/promotions/sort-item/sort-item.component';
import { HourlySalesComponent } from './report/tickets/hourly-sales/hourly-sales.component';
import { HourlySalesExportModalComponent } from './report/tickets/hourly-sales-export-modal/hourly-sales-export-modal.component';
import { DepartmentSummaryComponent } from './report/department/department-summary/department-summary.component';
import { PlanDayComponent } from "./report/sale/plan-day/plan-day.component";
import { DayReportComponent } from "./report/sale/plan-day/day-report/day-report.component";
import { ShiftReportComponent } from "./report/sale/plan-day/shift-report/shift-report.component";
import { TerminalReportComponent } from "./report/sale/plan-day/terminal-report/terminal-report.component";
import { CashSummaryReportComponent } from './report/sale/cash-summary-report/cash-summary-report.component';
import { DepartmentItemsComponent } from './report/department/department-items/department-items.component';
import { FilterModalComponent } from './report/tickets/report-ticket/filter-modal/filter-modal.component';
import { OrderTagReportComponent } from './report/order/order-tag-report/order-tag-report.component';
import { OrderTagExchangeComponent } from './report/order/order-tag-exchange/order-tag-exchange.component';
import { OrderReturnReportComponent } from './report/order/order-return-report/order-return-report.component';
import { CollecttionReportComponent } from './report/sale/collecttion-report/collecttion-report.component';
import { OrganizationFilterComponent } from "./master/organization/organization-filter/organization-filter.component";
import { ScheduleItemsComponent } from './report/schedule/schedule-items/schedule-items.component';
import { ScheduleLocationComponent } from './report/schedule/schedule-location/schedule-location.component';
import { TillTransactionReportComponent } from './report/other/till-transaction-report/till-transaction-report.component';
import { LogReportComponent } from './report/other/log-report/log-report.component';
import { SummaryOtherReportComponent } from './report/other/summary-other-report/summary-other-report.component';
import { TenderReportComponent } from './report/other/tender-report/tender-report.component';
import { PaymentExcessReportComponent } from './report/other/payment-excess-report/payment-excess-report.component';
import { LogDetailComponent } from './report/other/log-report/log-detail/log-detail.component';
@NgModule({
    imports: [
        CommonModule,
        ConnectRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        CountoModule,
        AutoCompleteModule,
        EditorModule,
        FileUploadModule,
        PrimeNgFileUploadModule,
        ImageCropperModule,
        InputMaskModule,
        TextMaskModule,
        PaginatorModule,
        TableModule,
        CheckboxModule,
        PickListModule,
        CalendarModule,
        MultiSelectModule,
        NgSelectModule,
        NgxEasypiechartModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        NgxTagsInputModule,
        TagInputModule,
        TimepickerModule.forRoot(),
        QueryBuilderModule,
        ChipsModule,
        EditorModule,
        AppBsModalModule,
        DragulaModule.forRoot(),
        MatExpansionModule,
        PanelModule,
        TreeModule,
        TreeTableModule,
        ContextMenuModule,
        FormlyModule.forRoot({
            validationMessages: [
                { name: "required", message: "This field is required" },
            ],
            types: [{ name: "repeatSection", component: RepeatSection }],
        }),
        FormlyBootstrapModule,
        ChartModule,
        OrderListModule,
        DragDropModule,
        SidebarModule
    ],
    declarations: [
        OrganizationComponent,
        CreateOrEditProductComboComponent,
        ProductComboComponent,
        LocationComponent,
        MenuItemComponent,
        CategoriesComponent,
        UserLocationComponent,
        AddUserModalComponent,
        UserInLocationComponent,
        LocationGroupComponent,
        CreateOrEditGroupModalComponent,
        LocationsInGroupComponent,
        AddLocationModalComponent,
        CreateOrUpdateLocationModalComponent,
        CreateOrUpdateOrganizationModalComponent,
        ProductGroupsComponent,
        CreateOrEditProductGroupModalComponent,
        PaymentTypesComponent,
        CreateOrEditPaymentTypeComponent,
        ForeignCurrenciesComponent,
        CreateOrEditForeignCurrencyModalComponent,
        PlanReasonsComponent,
        TransactionTypesComponent,
        CreateOrEditTransactionTypeModalComponent,
        TillAccountsComponent,
        CalculationsComponent,
        CreateOrEditCalculationComponent,
        TerminalsComponent,
        CreateOrEditTerminalComponent,
        FutureDatesComponent,
        CreateOrEditFutureDateComponent,
        ShiftReportComponent,
        TicketTagGroupsComponent,
        CreateOrEditTicketTagGroupComponent,
        PriceTagsComponent,
        CreateOrEditPriceTagComponent,
        OrderTagGroupsComponent,
        CreateOrEditOrderTagGroupComponent,
        DepartmentsComponent,
        CreateOrEditDepartmentComponent,
        LocationTagsComponent,
        CreateOrEditLocationTagModalComponent,
        AddLocationToTagModalComponent,
        LocationsInTagComponent,
        PromotionsComponent,
        CreateOrEditPromotionComponent,
        SelectItemModalComponent,
        DinePlanTaxesComponent,
        CreateOrEditDinePlanTaxComponent,
        SelectLocationModalComponent,
        CreateOrEditCategoryComponent,
        CreateOrEditMenuitemComponent,
        CreateComboGroupModalComponent,
        CreateComboItemModalComponent,
        CreateOrEditTillAccountComponent,
        CreateOrEditPlanReasonComponent,
        EditPriceForPriceTagComponent,
        EditTagPriceModalComponent,
        ImportModalComponent,
        DayReportComponent,
        // --
        TerminalReportComponent,
        //#region Print
        PrintPrintersComponent,
        CreateOrEditPrinterComponent,

        PrintTemplateComponent,
        CreateOrEditPrintTemplateComponent,

        PrintJobComponent,
        CreateOrEditPrintJobComponent,

        PrintConditionComponent,
        CreateOrEditPrintConditionComponent,

        //#endregion

        //#region ProgramSetting
        ProgramSettingTemplatesComponent,
        CreateOrEditProgramSettingTemplateComponent,
        //#endregion

        LocationPricesComponent,
        EditLocationPricesComponent,

        ComboGroupsComponent,
        CreateOrEditComboGroupComponent,
        CreateComboItemModalComponent,

        LocationMenusComponent,

        ScreenMenusComponent,
        CreateOrEditScreenMenuComponent,
        ScreenMenuCategoriesComponent,
        AddScreenMenuCategoryComponent,
        EditScreenMenuCategoryComponent,
        ScreenMenuMenuitemComponent,
        ScreenMenuMenuitemAddComponent,
        ScreenMenuMenuitemEditComponent,
        ScreenMenuVirtualscreenComponent,
        OrderTagGroupCloneComponent,
        GroupComponent,
        TablesComponent,
        CreateTablesComponent,
        ConnectUserRolesComponent,
        CreateUpdateConnectUserRoleComponent,
        ConnectUsersComponent,
        CreateUpdateConnectUserComponent,
        PromotionCategorysComponent,
        CreateUpdatePromotionCategoryComponent,
        NumeratorComponent,
        CreateUpdateNumeratorComponent,
        TicketTypeComponent,
        CreateUpdateTicketTypeComponent,
        DepartmentGroupComponent,
        CreateUpdateDepartmentGroupComponent,
        CreateGroupComponent,
        PrintDevicesComponent,
        CreateOrEditDeviceComponent,
        RepeatSection,
        CardCategoryComponent,
        CardTypeComponent,
        CardsComponent,
        CreateOrUpdateCardCategoryComponent,
        CreateOrUpdateCardTypeComponent,
        CardsModalComponent,
        CreateOrEditCardComponent,
        ReportTicketComponent,
        TicketSyncComponent,
        ReportOrderComponent,
        ComboItemsModalComponent,
        CreateOrEditLanguageDescriptionModalComponent,
        PriceTagLocationPriceComponent,
        UploadFileModalComponent,
        OrderReportDetailModalComponent,
        ItemReportComponent,
        ImportCardModalComponent,
        HourlyComponent,
        OrderTagLocationPriceComponent,
        SortComboGroupModalComponent,
        CreateOrEditDeviceModalComponent,
        GroupReportComponent,
        GroupReportDetailModalComponent,
        CategoryReportComponent,
        CategoryReportDetailModalComponent,
        ItemReportExportModalComponent,
        OrderPromotionReportComponent,
        OrderPromotionReportDetailModalComponent,
        SummaryReportComponent,
        ItemTagReportComponent,
        SaleSummaryPrintComponent,
        TopDownReportComponent,
        WorkDayReportComponent,
        WorkDayPrintPrintComponent,
        WeekDayReportComponent,
        WeekEndReportComponent,
        SelectProductGroupModalComponent,
        SelectCategoryPopupComponent,
        SelectMenuItemPopupComponent,
        SelectPortionPopupComponent,
        SortItemComponent,
        HourlySalesComponent,
        HourlySalesExportModalComponent,
        DepartmentSummaryComponent,
        PlanDayComponent,
        CashSummaryReportComponent,
        DepartmentItemsComponent,
        FilterModalComponent,
        OrderTagReportComponent,
        OrderTagExchangeComponent,
        OrderReturnReportComponent,
        CollecttionReportComponent,
        OrganizationFilterComponent,
        ScheduleItemsComponent,
        ScheduleLocationComponent,
        TillTransactionReportComponent,
        LogReportComponent,
        SummaryOtherReportComponent,
        TenderReportComponent,
        PaymentExcessReportComponent,
        LogDetailComponent,
    ],
    providers: [
        DecimalPipe,
        CategoryServiceProxy,
        CompanyServiceProxy,
        MenuItemServiceProxy,
        LocationServiceProxy,
        LocationGroupServiceProxy,
        UserLocationServiceProxy,
        AddressesServiceProxy,
        ProductGroupsServiceProxy,
        CommonServiceProxy,
        OrderTagGroupServiceProxy,
        ConnectTableGroupServiceProxy,
        DinePlanUserRoleServiceProxy,
        DinePlanUserServiceProxy,
        PromotionCategoryServiceProxy,
        NumeratorServiceProxy,
        TicketTypeServiceProxy,
        TicketTypeConfigurationServiceProxy,
        DineDeviceServiceProxy,
        ConnectCardTypeCategoryServiceProxy,
        ConnectCardTypeServiceProxy,
        ConnectCardServiceProxy,
        TicketReportServiceProxy,
        TicketHourlySalesReportServiceProxy,
        ProductComboServiceProxy,
        ConnectLookupServiceProxy,
        TicketSyncReportServiceProxy,
        OrderReportServiceProxy,
        GroupReportServiceProxy,
        CategoryReportServiceProxy,
        OrderPromotionReportServiceProxy,
        SaleSummaryReportServiceProxy,
        PlanDayReportServiceProxy,
        CashSummaryReportServiceProxy,
        WorkPeriodServiceProxy,
        CollectionReportServiceProxy,
        ScheduleSalesItemsReportServiceProxy,
        ScheduleSalesLocationReportServiceProxy,
        TillAccountsServiceProxy,
        PlanLoggerServiceProxy,
        SaleTargetServiceProxy,
        TenderReportServiceProxy,
        PaymentExcessReportServiceProxy,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
        DatePipe,
    ],
})
export class ConnectModule {}

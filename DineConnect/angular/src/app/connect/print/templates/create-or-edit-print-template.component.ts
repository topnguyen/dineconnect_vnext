import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CommonLocationGroupDto,
    PrintTemplateServiceProxy,
    CreatePrintTemplateInput,
    EditPrintTemplateInput,
    CreateOrEditPrintTemplateMappingInput,
    GetAllPrintTemplateMappingDto
} from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileUploader } from 'ng2-file-upload';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LocationHelper } from '../../../../shared/helpers/LocationHelper';

@Component({
    selector: 'app-create-or-edit-print-template',
    templateUrl: './create-or-edit-print-template.component.html',
    styleUrls: ['./create-or-edit-print-template.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditPrintTemplateComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    @ViewChild('mappingForm') set mappingForm(mappingForm: NgForm) {
        if (mappingForm) {
            this._mappingForm = mappingForm;
        }
    }

    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('filterSelectLocationOrGroup', { static: true }) filterSelectLocationOrGroup: SelectLocationOrGroupComponent;

    @ViewChild('dataTable') set dataTable(dataTable: Table) {
        if (dataTable) {
            this._dataTable = dataTable;
        }
    }
    paginator$ = new Subject<Paginator>();
    @ViewChild('paginator') set paginator(paginator: Paginator) {
        if (paginator) {
            this._paginator = paginator;
            this.paginator$.next(paginator);
            this.paginator$.complete();
        }
    }

    tabIndex = 0;
    printerId = 0;
    printer = new EditPrintTemplateInput();
    mapping = new CreateOrEditPrintTemplateMappingInput();
    saving = false;
    printTemplateFilter = {
        locationGroup: new CommonLocationGroupDto()
    };
    files = [];

    private _paginator: Paginator;
    private _dataTable: Table;
    _mappingForm: NgForm;
    uploadUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
    uploader: FileUploader;
    filePreviewPath: SafeUrl;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _tokenService: TokenService,
        private _printTemplateServiceProxy: PrintTemplateServiceProxy
    ) {
        super(injector);

        this.primengTableHelper.totalRecordsCount = 0;
        this.primengTableHelper.records = [];

        this.paginator$.subscribe((x) => {
            this._changeDetector.detectChanges();
            this.getMappings();
        });
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.printerId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.printerId > 0) {
                this._printTemplateServiceProxy.getForEdit(this.printerId).subscribe((resp) => {
                    this.printer = resp;
                });

                this.getMappings();
            }
        });
        this.initUpload();
    }

    initUpload() {
        this.uploader = new FileUploader({
            url: this.uploadUrl,
            authToken: 'Bearer ' + this._tokenService.getToken(),
        });

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l('UploadImageSuccessfully'));
                if (ajaxResponse.result.message == null) {
                    this.files.push(ajaxResponse.result);
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
    }

    onFileSelected() {
        this.uploader.uploadAll();
    }

    removeImage(item) {
        item.remove();
    }

    save() {
        if (!this.formIsValid) {
            return;
        }

        const isPrintTemplateTab = this.tabIndex === 0;

        if (isPrintTemplateTab) {
            this.saving = true;

            if (this.printerId > 0) {
                this._printTemplateServiceProxy
                    .edit(this.printer)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((result) => {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.close();
                    });
            } else {
                const data = new CreatePrintTemplateInput(this.printer);
                this._printTemplateServiceProxy
                    .create(data)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((result) => {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.close();
                    });
            }
        } else {
            this.saving = true;
            this.mapping.printConfigurationId = this.printerId;

            if (typeof this.mapping.mergeLines !== 'boolean') {
                this.mapping.mergeLines = false;
            }
            this.mapping.files = '';
            if (this.files.length) {
                this.mapping.files = JSON.stringify(this.files);
            }
            this._printTemplateServiceProxy
                .createOrUpdateMapping(this.mapping)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.tabIndex = 0;
                    this.mapping = new CreateOrEditPrintTemplateMappingInput();
                    this._mappingForm.resetForm();
                    this.notify.success(this.l('SavedSuccessfully'));
                    this._paginator.changePage(0);
                });
        }
    }

    close() {
        this._router.navigate(['/app/connect/print/print-templates']);
    }

    clear() {
        this.mapping = new CreateOrEditPrintTemplateMappingInput();
        this.printTemplateFilter = {
            locationGroup: new CommonLocationGroupDto()
        };

        this.getMappings();
    }

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.mapping.locationGroup);
    }

    openFilterSelectLocationOrGroup() {
        this.filterSelectLocationOrGroup.show(this.printTemplateFilter.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.mapping.locationGroup = event;
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.printTemplateFilter.locationGroup = event;
        this.getMappings();
    }

    getMappings(event?: LazyLoadEvent) {
        if (this._dataTable && this._paginator) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this._paginator.changePage(0);
                return;
            }

            this.primengTableHelper.showLoadingIndicator();
            const locationFilter = LocationHelper.getFilter(this.printTemplateFilter.locationGroup);

            this._printTemplateServiceProxy
                .getAllMappings(
                    this.printerId,
                    locationFilter.locationIds,
                    locationFilter.locationsGroup,
                    locationFilter.locationTag,
                    locationFilter.nonLocations,
                    this.primengTableHelper.getSorting(this._dataTable),
                    this.primengTableHelper.getSkipCount(this._paginator, event),
                    this.primengTableHelper.getMaxResultCount(this._paginator, event)
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    viewMapping(item: GetAllPrintTemplateMappingDto) {
        this._printTemplateServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.selectLocationOrGroup.show(result.locationGroup, true);
        });
    }

    editMapping(item: GetAllPrintTemplateMappingDto) {
        this.tabIndex = 1;
        this._mappingForm.reset();

        this._printTemplateServiceProxy.getMappingForEdit(item.id)
            .subscribe((result) => {
                this.mapping = result;
                this.files = [];
                if (this.mapping.files) {
                    this.files = JSON.parse(this.mapping.files);
                }
            });
    }

    deleteMapping(item: GetAllPrintTemplateMappingDto) {
        this.message.confirm(this.l('DeletePrintTemplateWarning'), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._printTemplateServiceProxy.deleteMapping(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));

                    if (this.mapping.id === item.id) {
                        this.mapping = new CreateOrEditPrintTemplateMappingInput();
                        this.printTemplateFilter = {
                            locationGroup: new CommonLocationGroupDto()
                        };
                    }

                    this._paginator.changePage(this._paginator.getPage());
                });
            }
        });
    }

    cloneMapping(item: GetAllPrintTemplateMappingDto) {
        this.tabIndex = 1;
        this._mappingForm.reset();

        this._printTemplateServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.mapping = result;
            this.mapping.locationGroup = undefined;
            this.mapping.id = undefined;
        });
    }

    get isEdit() {
        return !!this.printerId;
    }

    get formIsValid() {
        const isPrintTemplateTab = this.tabIndex === 0;

        if (isPrintTemplateTab) {
            return this.form.form.valid;
        } else {
            return this._mappingForm?.form?.valid;
        }
    }

    get isEditMapping() {
        return !!this.mapping?.id;
    }

    removeFile(index) {
        this.files.splice(index);
    }
}

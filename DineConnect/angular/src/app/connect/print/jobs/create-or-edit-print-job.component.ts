import { DepartmentsServiceProxy, PagedResultDtoOfCategoryDto, PagedResultDtoOfDepartmentDto, PagedResultDtoOfMenuItemListDto, PrintersServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CommonLocationGroupDto,
    PrintJobServiceProxy,
    CreatePrintJobInput,
    EditPrintJobInput,
    CreateOrEditPrintJobMappingInput,
    GetAllPrintJobMappingDto,
    CreateOrEditPrinterMapInput,
    CategoryServiceProxy,
    MenuItemServiceProxy
} from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { BehaviorSubject, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LocationHelper } from '../../../../shared/helpers/LocationHelper';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-create-or-edit-print-job',
    templateUrl: './create-or-edit-print-job.component.html',
    styleUrls: ['./create-or-edit-print-job.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditPrintJobComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;
    @ViewChild('mappingForm') set mappingForm(mappingForm: NgForm) {
        if (mappingForm) {
            this._mappingForm = mappingForm;
        }
    }
    @ViewChild('selectDepartmentPopupModal', { static: true }) selectDepartmentPopupModal: SelectPopupComponent;
    @ViewChild('selectCategoryPopupModal', { static: true }) selectCategoryPopupModal: SelectPopupComponent;
    @ViewChild('selectMenuItemPopupModal', { static: true }) selectMenuItemPopupModal: SelectPopupComponent;
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('filterSelectLocationOrGroup', { static: true }) filterSelectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('dataTable') set dataTable(dataTable: Table) {
        if (dataTable) {
            this._dataTable = dataTable;
        }
    }
    @ViewChild('paginator') set paginator(paginator: Paginator) {
        if (paginator) {
            this._paginator = paginator;
            this.paginator$.next(paginator);
            this.paginator$.complete();
        }
    }
    tabIndex = 0;
    printerId = 0;
    saving = false;
    printer = new EditPrintJobInput();
    mapping = new CreateOrEditPrintJobMappingInput();
    mappingItems: CreateOrEditPrinterMapInput[] = [];    
    whatToPrints = [];
    departmentResult$ : any = new BehaviorSubject<any>(null);
    categoryResult$ : any = new BehaviorSubject<any>(null);
    menuItemResult$ : any = new BehaviorSubject<any>(null);
    printers = [];
    templates = [];
    printJobFilter = {
        locationGroup: new CommonLocationGroupDto()
    };

    paginator$ = new Subject<Paginator>();
    selectedDepartment: any;
    selectedCategory: any;
    selectedMenuItem: any;
    _mappingForm: NgForm;
    private _dataTable: Table;
    private _paginator: Paginator;

    mappingIndex: number;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _printJobServiceProxy: PrintJobServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _departmentServiceProxy: DepartmentsServiceProxy,
        private _printersServiceProxy: PrintersServiceProxy
    ) {
        super(injector);

        this.primengTableHelper.totalRecordsCount = 0;
        this.primengTableHelper.records = [];

        this.paginator$.subscribe((x) => {
            this._changeDetector.detectChanges();
            this.getMappings();
        });
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.printerId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.printerId > 0) {
                this._printJobServiceProxy.getForEdit(this.printerId).subscribe((resp) => {
                    this.printer = resp;
                });

                this.getMappings();
            }

            this.getComboBoxData();
        });
    }

    getComboBoxData() {
        if (this.isEdit) {
            this._printJobServiceProxy.getWhatToPrints().subscribe((result) => {
                this.whatToPrints = result.items || [];

                this.mapping.whatToPrint = (this.whatToPrints || []).length > 0 ? this.whatToPrints[0].value : '0';
            });

            // this._categoryServiceProxy.getCategoriesForCombobox().subscribe((result) => {
            //     this.categories = result.items;
            // });

            // this._menuItemServiceProxy.getMenuItemForComboBox().subscribe((result) => {
            //     this.menuItems = result;
            // });

            this._printersServiceProxy.getPrintersForComboBox().subscribe((result) => {
                this.printers = result.items;
            });

            this._printersServiceProxy.getPrinterTemplatesForComboBox().subscribe((result) => {
                this.templates = result.items;
            });
        }
    }

    save() {
        if (!this.formIsValid) {
            return;
        }

        const isPrintJobTab = this.tabIndex === 0;

        if (isPrintJobTab) {
            this.saving = true;

            if (this.printerId > 0) {
                this._printJobServiceProxy
                    .edit(this.printer)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((result) => {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.close();
                    });
            } else {
                const data = new CreatePrintJobInput(this.printer);
                this._printJobServiceProxy
                    .create(data)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((result) => {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.close();
                    });
            }
        } else {
            this.saving = true;

            this.mapping.printConfigurationId = this.printerId;
            this.mapping.printerMapInputs = this.mappingItems;

            const keys = ['negative', 'excludeTax'];
            keys.forEach((x) => {
                if (typeof this.mapping[x] !== 'boolean') {
                    this.mapping[x] = false;
                }
            });

            this._printJobServiceProxy
                .createOrUpdateMapping(this.mapping)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.tabIndex = 0;

                    this.mapping = new CreateOrEditPrintJobMappingInput();
                    this.mappingItems = [];

                    this._mappingForm.resetForm();

                    this.notify.success(this.l('SavedSuccessfully'));
                    this._paginator.changePage(0);
                });
        }
    }

    close() {
        this._router.navigate(['/app/connect/print/print-jobs']);
    }

    clear() {
        this.mapping = new CreateOrEditPrintJobMappingInput();
        this.mappingItems = [];

        this.printJobFilter = {
            locationGroup: new CommonLocationGroupDto()
        };

        this.getMappings();
    }

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.mapping.locationGroup);
    }

    openFilterSelectLocationOrGroup() {
        this.filterSelectLocationOrGroup.show(this.printJobFilter.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.mapping.locationGroup = event;
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.printJobFilter.locationGroup = event;
        this.getMappings();
    }

    getMappings(event?: LazyLoadEvent) {
        if (this._dataTable && this._paginator) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this._paginator.changePage(0);
                return;
            }

            this.primengTableHelper.showLoadingIndicator();
            const locationFilter = LocationHelper.getFilter(this.printJobFilter.locationGroup);

            this._printJobServiceProxy
                .getAllMappings(
                    this.printerId,
                    locationFilter.locationIds,
                    locationFilter.locationsGroup,
                    locationFilter.locationTag,
                    locationFilter.nonLocations,
                    this.primengTableHelper.getSorting(this._dataTable),
                    this.primengTableHelper.getSkipCount(this._paginator, event),
                    this.primengTableHelper.getMaxResultCount(this._paginator, event)
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    viewMapping(item: GetAllPrintJobMappingDto) {
        this._printJobServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.selectLocationOrGroup.show(result.locationGroup, true);
        });
    }

    editMapping(item: GetAllPrintJobMappingDto, isClone?: boolean) {
        this._mappingForm.reset();
        this._printJobServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.mapping = result;

            this.mappingItems = this.mapping.printerMapInputs;
            this.mappingItems.forEach((x) => {
                const keys = ['printerId', 'printTemplateId', 'categoryId', 'menuItemId'];

                keys.forEach((key) => {
                    if (x[key] === 0) {
                        x[key] = undefined;
                    }
                });
            });
            if (isClone) {
                this.mapping.locationGroup = undefined;
                this.mapping.id = undefined;
            }
            this.tabIndex = 1;
        });
    }

    deleteMapping(item: GetAllPrintJobMappingDto) {
        this.message.confirm(this.l('DeletePrintJobWarning'), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._printJobServiceProxy.deleteMapping(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));

                    if (this.mapping.id === item.id) {
                        this.mapping = new CreateOrEditPrintJobMappingInput();
                        this.printJobFilter = {
                            locationGroup: new CommonLocationGroupDto()
                        };
                    }

                    this._paginator.changePage(this._paginator.getPage());
                });
            }
        });
    }

    cloneMapping(item: GetAllPrintJobMappingDto) {
        this.editMapping(item, true);
    }

    addItem() {
        const item = new CreateOrEditPrinterMapInput();
        this.mappingItems.push(item);
    }

    removeItem(index: number) {
        this.mappingItems.splice(index, 1);
    }

    // getMenuItemSelections(categoryId: number) {
    //     return this.menuItems[categoryId] || [];
    // }

    get isEdit() {
        return !!this.printerId;
    }

    get formIsValid() {
        const isPrintJobTab = this.tabIndex === 0;

        if (isPrintJobTab) {
            return this.form.form.valid;
        } else {
            if (!this.mappingItems?.length) {
                return false;
            }

            for (const mappingItem of this.mappingItems) {
                if (!mappingItem.printerId || !mappingItem.printTemplateId) {
                    return false;
                }
            }

            return this._mappingForm?.form?.valid;
        }
    }

    get isEditMapping() {
        return !!this.mapping?.id;
    }


    addDepartment(itemSelect) {
        this.selectedDepartment = itemSelect;
        this.mappingItems[this.mappingIndex].departmentId = itemSelect.id;
        this.mappingItems[this.mappingIndex].departmentName = itemSelect.name;

    }

    selectDepartment(index) {
        this.mappingIndex = index;
        this.selectDepartmentPopupModal.show();
    }

    getDepartmentSelection($event: any) {
        let filterText = $event.filterText;
        let sorting = $event.sorting;
        let skipCount = $event.skipCount;
        let maxResultCount = $event.maxResultCount;
        this._departmentServiceProxy.getAll(
                filterText,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                sorting,
                skipCount,
                maxResultCount
            ).subscribe(result =>  {
                this.departmentResult$.next(result);
            });
    }

    addCategory(itemSelect) {
        this.selectedCategory = itemSelect;
        this.mappingItems[this.mappingIndex].categoryId = itemSelect.id;
        this.mappingItems[this.mappingIndex].categoryName = itemSelect.name;
    }

    selectCategory(index) {
        this.mappingIndex = index;
        this.selectCategoryPopupModal.show();
    }

    getCategorySelection($event: any) {
        let filterText = $event.filterText;
        let sorting = $event.sorting;
        let skipCount = $event.skipCount;
        let maxResultCount = $event.maxResultCount;
        this._categoryServiceProxy.getAll(filterText, false, sorting, maxResultCount, skipCount).subscribe(result =>  {
            this.categoryResult$.next(result);
        });
    }

    addMenuItem(itemSelect) {
        this.selectedMenuItem = itemSelect;
        this.mappingItems[this.mappingIndex].menuItemId = itemSelect.id;
        this.mappingItems[this.mappingIndex].menuItemName = itemSelect.name;
    }

    selectMenuItem(index) {
        this.mappingIndex = index;
        this.selectMenuItemPopupModal.show();
    }

    getMenuItemSelection($event: any): any {
        let filterText = $event.filterText;
        let sorting = $event.sorting;
        let skipCount = $event.skipCount;
        let maxResultCount = $event.maxResultCount;

        if(!this.mappingItems[this.mappingIndex] ) {
            this.menuItemResult$.next([]); ; 
        }
        let categoryId = this.mappingItems[this.mappingIndex].categoryId;
         this._menuItemServiceProxy.getAll(filterText
                , false
                , categoryId
                , undefined
                , undefined
                , undefined
                , undefined
                , undefined
                , sorting
                , skipCount
                , maxResultCount).subscribe(result =>  {
                    this.menuItemResult$.next(result);
                });
    }
}

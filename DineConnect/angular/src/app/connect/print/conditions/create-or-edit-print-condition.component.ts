import { ChangeDetectorRef, Component, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CommonLocationGroupDto,
    CommonLookupServiceProxy,
    CreateOrEditPrintConditionMappingInput,
    CreatePrintConditionInput,
    EditPrintConditionInput,
    GetAllPrintConditionMappingDto,
    PrintConditionMappingScheduleEditDto,
    PrintConditionServiceProxy,
    PromotionsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { QueryBuilderConfig } from 'angular2-query-builder';
import moment from 'moment';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LocationHelper } from '../../../../shared/helpers/LocationHelper';

@Component({
    selector: 'app-create-or-edit-print-condition',
    templateUrl: './create-or-edit-print-condition.component.html',
    styleUrls: ['./create-or-edit-print-condition.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditPrintConditionComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    _mappingForm: NgForm;
    @ViewChild('mappingForm') set mappingForm(mappingForm: NgForm) {
        if (mappingForm) {
            this._mappingForm = mappingForm;
        }
    }

    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('filterSelectLocationOrGroup', { static: true }) filterSelectLocationOrGroup: SelectLocationOrGroupComponent;

    private _dataTable: Table;
    dataTable$ = new Subject<Table>();
    @ViewChild('dataTable') set dataTable(dataTable: Table) {
        if (dataTable) {
            this._dataTable = dataTable;
            this.dataTable$.next(dataTable);
            this.dataTable$.complete();
        }
    }
    private _paginator: Paginator;
    paginator$ = new Subject<Paginator>();
    @ViewChild('paginator') set paginator(paginator: Paginator) {
        if (paginator) {
            this._paginator = paginator;
            this.paginator$.next(paginator);
            this.paginator$.complete();
        }
    }

    tabIndex = 0;
    mappingTabIndex = 0;
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    printerId = 0;
    printer = new EditPrintConditionInput();
    mapping = new CreateOrEditPrintConditionMappingInput();
    saving = false;
    dateRange = [moment().startOf('day').toDate(), moment().endOf('day').toDate()];
    days = [];
    months = [];
    ranges = [
        {
            value: [new Date(), new Date()],
            label: this.l('Today')
        },
        {
            value: [new Date(new Date().setDate(new Date().getDate() + 1)), new Date(new Date().setDate(new Date().getDate() + 1))],
            label: this.l('Tomorrow')
        },
        {
            value: [new Date(), new Date(new Date().setDate(new Date().getDate() + 6))],
            label: this.l('Next7Days')
        },
        {
            value: [new Date(), new Date(new Date().setDate(new Date().getDate() + 29))],
            label: this.l('Next30Days')
        },
        {
            value: [new Date(), moment().tz(this.timezone, true).endOf('month').toDate()],
            label: this.l('ThisMonth')
        },
        {
            value: [
                moment().add(1, 'month').tz(this.timezone, true).startOf('month').toDate(),
                moment().add(1, 'month').tz(this.timezone, true).endOf('month').toDate()
            ],
            label: this.l('NextMonth')
        }
    ];

    startTime = moment('12:00', 'HH:mm').tz(this.timezone, true).toDate();
    endTime = moment('17:00', 'HH:mm').tz(this.timezone, true).toDate();
    dayOfWeek = [];
    monthSchedules = [];
    minDate = moment().toDate();
    query: any;
    isShowConditionJson = false;
    config: QueryBuilderConfig = {
        fields: {}
    };
    printJobFilter = {
        locationGroup: new CommonLocationGroupDto()
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _printConditionServiceProxy: PrintConditionServiceProxy,
        private render: Renderer2,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _commonServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);

        this.primengTableHelper.totalRecordsCount = 0;
        this.primengTableHelper.records = [];

        this.resetMappingForm(false);

        this.paginator$.subscribe((x) => {
            this._changeDetector.detectChanges();
            this.getMappings();
        });

        for (let i = 1; i <= 12; i++) {
            this.months.push({
                displayText: i,
                value: i
            });
        }
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.printerId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.printerId > 0) {
                this._printConditionServiceProxy.getForEdit(this.printerId).subscribe((resp) => {
                    this.printer = resp;
                });

                this.getMappings();
            }

            this.getComboBoxData();
        });
    }

    getComboBoxData() {
        if (this.isEdit) {
            this._promotionsServiceProxy.getDaysForLookupTable().subscribe((result) => {
                this.days = result.items;
            });

            this._commonServiceProxy.getConditionFilter().subscribe((result) => {
                this.config = {
                    fields: JSON.parse(result)
                };
            });
        }
    }

    saveMapping() {
        this.saving = true;

        this.mapping.printConfigurationId = this.printerId;

        this.mapping.startDate = moment(this.dateRange[0]).startOf('day');
        this.mapping.endDate = moment(this.dateRange[1]).endOf('day');
        this.mapping.conditionSchedules = JSON.stringify(this.mapping.conditionScheduleList);
        this.mapping.filter = JSON.stringify(this.query);

        this._printConditionServiceProxy
            .createOrUpdateMapping(this.mapping)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.tabIndex = 0;

                this.resetMappingForm();
                this._mappingForm.resetForm();

                this.notify.success(this.l('SavedSuccessfully'));
                this._paginator.changePage(0);
            });
    }

    save() {
        this.saving = true;

        if (this.printerId > 0) {
            this._printConditionServiceProxy
                .edit(this.printer)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                });
        } else {
            const data = new CreatePrintConditionInput(this.printer);
            this._printConditionServiceProxy
                .create(data)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                });
        }
    }

    close() {
        this._router.navigate(['/app/connect/print/print-conditions']);
    }

    resetMappingForm(initialized = true) {
        this.mappingTabIndex = 0;

        this.mapping = new CreateOrEditPrintConditionMappingInput();
        this.mapping.conditionScheduleList = [];
        this.dateRange = [moment().startOf('day').toDate(), moment().endOf('day').toDate()];
        this.startTime = moment('12:00', 'HH:mm').tz(this.timezone, true).toDate();
        this.endTime = moment('17:00', 'HH:mm').tz(this.timezone, true).toDate();
        this.dayOfWeek = [];
        this.monthSchedules = [];

        if (initialized) {
            this.query = {
                condition: 'and',
                rules: [{ field: 'Total', operator: 'equal', value: '0' }]
            };
        }
    }

    clear() {
        this.printJobFilter = {
            locationGroup: new CommonLocationGroupDto()
        };

        this.resetMappingForm();
        this.getMappings();

        this._mappingForm.reset();
    }

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.mapping.locationGroup);
    }

    openFilterSelectLocationOrGroup() {
        this.filterSelectLocationOrGroup.show(this.printJobFilter.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.mapping.locationGroup = event;
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.printJobFilter.locationGroup = event;
        this.getMappings();
    }

    getMappings(event?: LazyLoadEvent) {
        if (this._dataTable && this._paginator) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this._paginator.changePage(0);
                return;
            }

            this.primengTableHelper.showLoadingIndicator();
            const locationFilter = LocationHelper.getFilter(this.printJobFilter.locationGroup);

            this._printConditionServiceProxy
                .getAllMappings(
                    this.printerId,
                    locationFilter.locationIds,
                    locationFilter.locationsGroup,
                    locationFilter.locationTag,
                    locationFilter.nonLocations,
                    this.primengTableHelper.getSorting(this._dataTable),
                    this.primengTableHelper.getSkipCount(this._paginator, event),
                    this.primengTableHelper.getMaxResultCount(this._paginator, event)
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    viewMapping(item: GetAllPrintConditionMappingDto) {
        this._printConditionServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.selectLocationOrGroup.show(result.locationGroup, true);
        });
    }

    editMapping(item: GetAllPrintConditionMappingDto) {
        this._printConditionServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.resetMappingForm();

            if (result.conditionSchedules) {
                try {
                    const conditionScheduleList: any[] = JSON.parse(this.mapping.conditionSchedules);
                    result.init({ ...result, conditionScheduleList });
                } catch (err) {}
            }

            this.mapping = result;

            try {
                this.query = this.mapping.filter ? JSON.parse(this.mapping.filter) : {};
            } catch (err) {
                this.query = {
                    condition: 'and',
                    rules: [{ field: 'Total', operator: 'equal', value: '0' }]
                };
            }

            this.tabIndex = 1;
            this.mappingTabIndex = 0;
        });
    }

    deleteMapping(item: GetAllPrintConditionMappingDto) {
        this.message.confirm(this.l('DeletePrintConditionWarning'), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._printConditionServiceProxy.deleteMapping(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));

                    if (this.mapping.id === item.id) {
                        this.resetMappingForm();
                    }

                    this._paginator.changePage(this._paginator.getPage());
                });
            }
        });
    }

    cloneMapping(item: GetAllPrintConditionMappingDto) {
        this._printConditionServiceProxy.getMappingForEdit(item.id).subscribe((result) => {
            this.resetMappingForm();

            if (result.conditionSchedules) {
                try {
                    const conditionScheduleList: any[] = JSON.parse(this.mapping.conditionSchedules);
                    result.init({ ...result, conditionScheduleList });
                } catch (err) {}
            }

            this.mapping = result;

            this.mapping.locationGroup = undefined;
            this.mapping.id = undefined;
            this.mapping.conditionScheduleList = [];

            try {
                this.query = this.mapping.filter ? JSON.parse(this.mapping.filter) : {};
            } catch (err) {
                this.query = {
                    condition: 'and',
                    rules: [{ field: 'Total', operator: 'equal', value: '0' }]
                };
            }

            this.tabIndex = 1;
            this.mappingTabIndex = 0;
        });
    }

    shownDateRangePicker() {
        const button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        const self = this;
        const el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }

    addSchedule() {
        const schedule = new PrintConditionMappingScheduleEditDto();
        schedule.init({
            startHour: moment(this.startTime).tz(this.timezone).format('HH'),
            startMinute: moment(this.startTime).tz(this.timezone).format('mm'),
            endHour: moment(this.endTime).tz(this.timezone).format('HH'),
            endMinute: moment(this.endTime).tz(this.timezone).format('mm'),
            days: this.dayOfWeek
                .map((item) => {
                    return item.value;
                })
                .join(','),
            dayNames: this.dayOfWeek
                .map((item) => {
                    return item.displayText;
                })
                .join(','),
            monthDays: this.monthSchedules
                .map((item) => {
                    return item.value;
                })
                .join(',')
        });

        this.mapping.conditionScheduleList.push(schedule);

        this.startTime = moment('12:00', 'HH:mm').tz(this.timezone, true).toDate();
        this.endTime = moment('17:00', 'HH:mm').tz(this.timezone, true).toDate();
        this.dayOfWeek = [];
        this.monthSchedules = [];
    }

    removeSchedule(index) {
        this.mapping.conditionScheduleList.splice(index, 1);
    }

    get isEdit() {
        return !!this.printerId;
    }

    get formIsValid() {
        const isPrintConditionTab = this.tabIndex === 0;

        if (isPrintConditionTab) {
            return this.form.form.valid;
        } else {
            return this._mappingForm?.form?.valid;
        }
    }

    get isEditMapping() {
        return !!this.mapping?.id;
    }
}

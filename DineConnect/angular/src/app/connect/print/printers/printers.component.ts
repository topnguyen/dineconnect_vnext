import {
    Component,
    Injector,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CommonLocationGroupDto,
    EntityDto,
    GetAllPrinterDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { LazyLoadEvent, Paginator, Table } from "primeng";
import { PrintersServiceProxy } from "./../../../../shared/service-proxies/service-proxies";

@Component({
    templateUrl: "./printers.component.html",
    styleUrls: ["./printers.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class PrintPrintersComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;

    filterText = "";
    isDeleted = false;
    locationOrGroup = new CommonLocationGroupDto();
    location: string;
    locationIds: string;

    exporting = false;

    constructor(
        injector: Injector,
        public _printersServiceProxy: PrintersServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;

        this._printersServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.locationIds,
                locationOrGroup.group,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    resetFilter() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();
        this.location = undefined;
        this.locationIds = undefined;

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;

        if (this.locationOrGroup.group) {
            this.locationIds = this.locationOrGroup.groups
                .map((x) => x.id)
                .join(",");
            this.location = this.locationOrGroup.groups[0].name;
        } else if (this.locationOrGroup.locationTag) {
            this.locationIds = this.locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
            this.location = this.locationOrGroup.locationTags[0].name;
        } else if (this.locationOrGroup.nonLocations?.length) {
            this.locationIds = this.locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
            this.location = null;
        } else if (this.locationOrGroup.locations?.length) {
            this.location = this.locationOrGroup.locations[0].name;
            this.locationIds = this.locationOrGroup.locations
                .map((x) => x.id)
                .join(",");
        }
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel() {
        this.exporting = true;

        this._printersServiceProxy.exportToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);

            this.exporting = false;
        });
    }

    deleteItem(item: GetAllPrinterDto) {
        this.message.confirm(
            this.l("DeletePrinterWarning") + " " + item.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._printersServiceProxy
                        .delete(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    activateItem(item: GetAllPrinterDto) {
        const body = EntityDto.fromJS({ id: item.id });
        this._printersServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l("Successfully"));
            this.reloadPage();
        });
    }

    createOrUpdateItem(id?: number) {
        this._router.navigate([
            "/app/connect/print/printers/create-or-update",
            id ? id : "",
        ]);
    }
}

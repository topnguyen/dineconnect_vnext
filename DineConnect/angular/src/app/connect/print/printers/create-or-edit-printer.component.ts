import { LocationHelper } from '../../../../shared/helpers/LocationHelper';
import { CommonLocationGroupDto, CreateOrEditPrinterMappingInput, FormlyType, GetAllPrinterMappingDto } from '@shared/service-proxies/service-proxies';
import { CreatePrinterInput, EditPrinterInput, PrintersServiceProxy } from '../../../../shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
    selector: 'app-create-or-edit-printer',
    templateUrl: './create-or-edit-printer.component.html',
    styleUrls: ['./create-or-edit-printer.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditPrinterComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('filterSelectLocationOrGroup', { static: true }) filterSelectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('printerTemplateForm')
    set printerTemplateForm(form: NgForm) {
        if (form) {
            this._printerTemplateForm = form;
        }
    }
    get printerTemplateForm() {
        return this._printerTemplateForm;
    }
    @ViewChild('dataTable')
    set dataTable(table: Table) {
        if (table) {
            this._dataTable = table;
        }
    }
    get dataTable() {
        return this._dataTable;
    }
    @ViewChild('paginator')
    set paginator(paginator: Paginator) {
        if (paginator) {
            this._paginator = paginator;
        }
    }
    get paginator() {
        return this._paginator;
    }

    tabIndex = 0;
    printerId = 0;
    printer = new EditPrinterInput();
    printerTemplate = new CreateOrEditPrinterMappingInput();
    filterLocationGroup = new CommonLocationGroupDto();
    printerTypes = [];
    fallBackPrinters = [];
    customPrinters = [];
    saving = false;
    loading = false;
    fields: FormlyType[] = [];
    printerProcessor: any = {};

    private _printerTemplateForm: NgForm;
    private _dataTable: Table;
    private _paginator: Paginator;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _printersServiceProxy: PrintersServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.printerId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.printerId > 0) {
                this._printersServiceProxy.getForEdit(this.printerId).subscribe((resp) => {
                    this.printer = resp;
                });

                this.getPrinterTemplates();
            }

            this.getComboBoxData();
        });
    }

    saveMapping() {
        if (!this.printerTemplateForm.form.valid) {
            return;
        }

        this.saving = true;

        if (this.printerProcessor) {
            this.printerTemplate.customPrinterData = JSON.stringify(this.printerProcessor);
        }

        this.printerTemplate.printConfigurationId = this.printerId;
        this._printersServiceProxy
            .createOrUpdatePrinterTemplate(this.printerTemplate)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.tabIndex = 0;
                this.printerTemplate = new CreateOrEditPrinterMappingInput();
                this.printerTemplateForm.resetForm();
                this.notify.success(this.l('SavedSuccessfully'));
                this._paginator.changePage(0);
            });
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }

        this.saving = true;

        if (this.printerId > 0) {
            this._printersServiceProxy
                .edit(this.printer)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                });
        } else {
            const data = new CreatePrinterInput(this.printer);
            this._printersServiceProxy
                .create(data)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((result) => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                });
        }
    }

    close() {
        this._router.navigate(['/app/connect/print/printers']);
    }

    clear() {
        this.printerTemplate = new CreateOrEditPrinterMappingInput();
        this.filterLocationGroup = new CommonLocationGroupDto();

        this.getPrinterTemplates();
    }

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.printerTemplate.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.printerTemplate.locationGroup = event;
    }

    openFilterSelectLocationOrGroup() {
        this.filterSelectLocationOrGroup.show(this.filterLocationGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.filterLocationGroup = event;
        this.getPrinterTemplates();
    }

    getPrinterTemplates(event?: LazyLoadEvent) {
        if (this._dataTable && this._paginator) {
            if (this.primengTableHelper.shouldResetPaging(event)) {
                this._paginator.changePage(0);
                return;
            }

            this.primengTableHelper.showLoadingIndicator();
            const locationFilter = LocationHelper.getFilter(this.filterLocationGroup);

            this._printersServiceProxy
                .getAllPrinterTemplates(
                    this.printerId,
                    locationFilter.locationIds,
                    locationFilter.locationsGroup,
                    locationFilter.locationTag,
                    locationFilter.nonLocations,
                    this.primengTableHelper.getSorting(this._dataTable),
                    this.primengTableHelper.getSkipCount(this._paginator, event),
                    this.primengTableHelper.getMaxResultCount(this._paginator, event)
                )
                .subscribe((result) => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = result.items;
                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    getComboBoxData() {
        if (this.isEdit) {
            this._printersServiceProxy.getPrinterTypes().subscribe((result) => {
                this.printerTypes = result.items.map((x) => ({ ...x, value: +x.value }));
            });

            this._printersServiceProxy.getFallBackPrinters(this.printerId).subscribe((result) => {
                this.fallBackPrinters = result.items.map((x) => ({ ...x, value: +x.value }));
            });

            this._printersServiceProxy.getPrinterProcessors().subscribe((result) => {
                this.customPrinters = result.items;
            });
        }
    }

    onChangeProcessor() {
        this.fields = [];
        if (this.printerTemplate.customPrinterName) {
            this.loading = true;
            this._printersServiceProxy
                .getPrinterProcessorSetting(this.printerTemplate.customPrinterName)
                .pipe(finalize(() => (this.loading = false)))
                .subscribe((result) => {
                    this.fields = result;

                    this.fields.forEach((field) => {
                        if (field.type == 'checkbox') {
                            field.templateOptions = {
                                label: field.templateOptions.label
                            } as any;
                        }
                    });
                });
        }
    }

    viewPrinterTemplate(item: GetAllPrinterMappingDto) {
        this._printersServiceProxy.getPrinterTemplateForEdit(item.id).subscribe((result) => {
            this.selectLocationOrGroup.show(result.locationGroup, true);
        });
    }

    editPrinterTemplate(item: GetAllPrinterMappingDto) {
        this.tabIndex = 1;
        this.printerTemplateForm.reset();
        this._printersServiceProxy.getPrinterTemplateForEdit(item.id).subscribe((result) => {
            this.printerTemplate = result;
            if (this.printerTemplate.customPrinterData) {
                this.printerProcessor = JSON.parse(this.printerTemplate.customPrinterData);
            }
            this.onChangeProcessor();
        });
    }

    deletePrinterTemplate(item: GetAllPrinterMappingDto) {
        this.message.confirm(this.l('DeletePrinterWarning'), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._printersServiceProxy.deletePrinterTemplate(item.id).subscribe((result) => {
                    this.notify.success(this.l('SuccessfullyDeleted'));

                    if (this.printerTemplate.id === item.id) {
                        this.printerTemplate = new CreateOrEditPrinterMappingInput();
                        this.filterLocationGroup = new CommonLocationGroupDto();
                    }

                    this._paginator.changePage(this._paginator.getPage());
                });
            }
        });
    }

    clonePrinterTemplate(item: GetAllPrinterMappingDto) {
        this.tabIndex = 1;
        this.printerTemplateForm.reset();
        this._printersServiceProxy.getPrinterTemplateForEdit(item.id).subscribe((result) => {
            this.printerTemplate = result;
            this.printerTemplate.locationGroup = undefined;
            this.printerTemplate.id = undefined;

            if (this.printerTemplate.customPrinterData) {
                this.printerProcessor = JSON.parse(this.printerTemplate.customPrinterData);
            }
            this.onChangeProcessor();
        });
    }

    get isEdit() {
        return !!this.printerId;
    }

    get isEditPrinterTemplate() {
        return !!this.printerTemplate?.id;
    }
}

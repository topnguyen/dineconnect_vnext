import {
    CommonLocationGroupDto,
    CreateOrUpdateDineDeviceInput,
    DineDeviceEditDto,
    DineDeviceServiceProxy,
    FormlyType
} from '@shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
    templateUrl: './create-or-edit-device.component.html',
    styleUrls: ['./create-or-edit-device.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditDeviceComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;

    tabIndex = 0;
    deviceId = 0;
    device = new DineDeviceEditDto();
    deviceTypes = [];
    saving = false;
    loading = false;
    dineDeviceFields: FormlyType[] = [];
    locationGroup = new CommonLocationGroupDto();
    dinedeviceType = {} as any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _devicesServiceProxy: DineDeviceServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.deviceId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.deviceId > 0) {
                this._devicesServiceProxy.getDineDeviceForEdit(this.deviceId).subscribe((resp) => {
                    this.device = resp.dineDevice;
                    this.locationGroup = resp.locationGroup;

                    if (this.device.settings) {
                        this.dinedeviceType = JSON.parse(this.device.settings) || {};

                        if (this.dinedeviceType.TicketSettingData) {
                            if ((this.dinedeviceType.TicketSettingData.TicketStates || []).length > 0) {
                                this.dinedeviceType.TicketStates = this.dinedeviceType.TicketSettingData.TicketStates;
                                delete this.dinedeviceType.TicketSettingData.TicketStates;
                            } else {
                                this.dinedeviceType.TicketStates = [];
                            }
                        }

                        this.onChangeDineDeviceType();
                    }
                });
            }

            this.getDineDeviceTypes();
        });
    }

    save() {
        if (!this.form.form.valid) {
            return;
        }

        if ((this.dinedeviceType.TicketStates || []).length > 0) {
            if (this.dinedeviceType.TicketSettingData) {
                this.dinedeviceType.TicketSettingData.TicketStates = this.dinedeviceType.TicketStates;
                delete this.dinedeviceType.TicketStates;
            } else {
                this.message.error('Page Size Field is required');
                return;
            }
        } else {
            if (this.dinedeviceType.TicketSettingData) {
                this.dinedeviceType.TicketSettingData.TicketStates =  [];
            }
        }

        this.device.settings = JSON.stringify(this.dinedeviceType);
        this.saving = true;

        const input = new CreateOrUpdateDineDeviceInput();
        input.dineDevice = this.device;
        input.locationGroup = this.locationGroup;

        this._devicesServiceProxy
            .createOrUpdateDineDevice(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/connect/print/print-devices']);
    }

    clear() {
        this.device = new DineDeviceEditDto();
        this.locationGroup = new CommonLocationGroupDto();
    }

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationGroup = event;
    }

    getDineDeviceTypes() {
        this._devicesServiceProxy.getDineDeviceTypes().subscribe((result) => {
            this.deviceTypes = result.items.map((x) => ({ ...x, value: +x.value }));
        });
    }

    onChangeDineDeviceType() {
        this.loading = true;
        this._devicesServiceProxy
            .getDineDeviceTypeSetting(this.device.deviceType)
            .pipe(
                finalize(() => {
                    this.loading = false;
                })
            )
            .subscribe((result) => {
                this.dineDeviceFields = result || [];

                this.dineDeviceFields.forEach((field) => {
                    if (field.type == 'checkbox') {
                        field.templateOptions = {
                            label: field.templateOptions.label
                        } as any;
                    }
                });
            });
    }
}
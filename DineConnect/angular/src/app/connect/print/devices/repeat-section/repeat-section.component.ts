import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { cloneDeep, forEach } from 'lodash';

@Component({
    selector: 'app-repeat-section',
    templateUrl: './repeat-section.component.html'
})
export class RepeatSection extends FieldType {
    unique = 1;

    copyFields(fields) {
        fields = cloneDeep(fields);
        this.addRandomIds(fields);
        return fields;
    }

    addNew() {
        this.model[this.options['key']] = this.model[this.options['key']] || [];
        var repeatsection = this.model[this.options['key']];
        var lastSection = repeatsection[repeatsection.length - 1];
        var newsection = {};
        if (lastSection) {
            newsection = cloneDeep(lastSection);
            this.empty(newsection);
        }
        repeatsection.push(newsection);
    }

    empty(object) {
        Object.keys(object).forEach((k) => {
            if (object[k] && typeof object[k] === 'object') {
                return this.empty(object[k]);
            }
            object[k] = '';
        });
    }
    addRandomIds(fields) {
        this.unique ++;

        forEach(fields, (field, index) => {
            if (field.fieldGroup) {
                this.addRandomIds(field.fieldGroup);
                return; // fieldGroups don't need an ID
            }

            if (field.templateOptions && field.templateOptions.fields) {
                this.addRandomIds(field.templateOptions.fields);
            }

            field.id = field.id || field.key + '_' + index + '_' + this.unique + this.getRandomInt(0, 9999);
        });
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

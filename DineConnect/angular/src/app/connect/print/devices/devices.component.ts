import {
    Component,
    Injector,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CommonLocationGroupDto,
    DineDeviceListDto,
    DineDeviceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { LazyLoadEvent, Paginator, Table } from "primeng";

@Component({
    templateUrl: "./devices.component.html",
    styleUrls: ["./devices.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class PrintDevicesComponent extends AppComponentBase {
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    exporting = false;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _devicesServiceProxy: DineDeviceServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._devicesServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    event
                ),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    resetFilter() {
        this.filterText = undefined;

        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel() {
        this.exporting = true;

        this._devicesServiceProxy
            .getAllToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(
                    this.paginator,
                    this.lazyLoadEvent
                ),
                this.primengTableHelper.getSkipCount(
                    this.paginator,
                    this.lazyLoadEvent
                )
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
                this.exporting = false;
            });
    }

    deleteItem(item: DineDeviceListDto) {
        this.message.confirm(
            this.l("DeleteDineGoDeviceWarning", item.name),
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._devicesServiceProxy
                        .deleteDineDevice(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    createOrUpdateItem(id?: number) {
        this._router.navigate([
            "/app/connect/print/print-devices/create-or-update",
            id ? id : "",
        ]);
    }
}

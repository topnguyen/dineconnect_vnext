import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { CommonLocationGroupDto, EntityDto, DinePlanUserRoleServiceProxy, DinePlanUserRoleListDto } from '@shared/service-proxies/service-proxies';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import * as _ from 'lodash';

import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { finalize } from 'rxjs/operators';
import { CreateUpdateConnectUserRoleComponent } from './create-update-user-role/create-update-user-role.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    templateUrl: './user-roles.component.html',
    styleUrls: ['./user-roles.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ConnectUserRolesComponent extends AppComponentBase {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;
    @ViewChild('createOrEditRoleModal', { static: true }) createOrEditRoleModal: CreateUpdateConnectUserRoleComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    location: string;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(injector: Injector, private _dinePlanUserRoleServiceProxy: DinePlanUserRoleServiceProxy, private _fileDownloadService: FileDownloadService) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._dinePlanUserRoleServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.location,
                this.isDeleted,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createDinePlanUserRole(): void {
        this.createOrEditRoleModal.show();
    }

    editUserRole(id?: number): void {
        this.createOrEditRoleModal.show(id);
    }

    deleteUserRole(role: DinePlanUserRoleListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._dinePlanUserRoleServiceProxy.deleteDinePlanUserRole(role.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(role: DinePlanUserRoleListDto) {
        const body = EntityDto.fromJS({ id: role.id });
        this._dinePlanUserRoleServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._dinePlanUserRoleServiceProxy.getAllToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    search() {
        this.getAll();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;

        if (event.locations.length > 0) {
            this.location = event.locations[0].name;
        } else if (event.groups.length > 0) {
            this.location = event.groups[0].name;
        } else if (event.locationTags.length > 0) {
            this.location = event.locationTags[0].name;
        } else {
            this.location = null;
        }
    }
}

import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { PermissionTreeComponent } from '@app/admin/shared/permission-tree.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, CommonLookupServiceProxy, CreateOrUpdateDinePlanUserRoleInput, DinePlanUserRoleEditDto, DinePlanUserRoleServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-create-update-user-role',
    templateUrl: './create-update-user-role.component.html',
    styleUrls: ['./create-update-user-role.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateUpdateConnectUserRoleComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('permissionTree', { static: false }) permissionTree: PermissionTreeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    departments: ComboboxItemDto[] = [];
    requestDepartments: [];
    role: DinePlanUserRoleEditDto = new DinePlanUserRoleEditDto();

    constructor(
        injector: Injector,
        private _dinePlanUserRoleServiceProxy: DinePlanUserRoleServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._getDepartments();
    }

    show(roleId?: number): void {
        this.active = true;

        this._dinePlanUserRoleServiceProxy.getDinePlanUserRoleForEdit(roleId).subscribe((result) => {
            this.role = result.dinePlanUserRole;
            this.permissionTree.editData = result;

            this.modal.show();
        });
    }

    onShown(): void {
        document.getElementById('RoleDisplayName').focus();
    }

    save(): void {
        const input = new CreateOrUpdateDinePlanUserRoleInput();
        input.dinePlanUserRole = this.role;
        input.grantedPermissionNames = this.permissionTree.getGrantedPermissionNames();

        this.saving = true;
        this._dinePlanUserRoleServiceProxy
            .createOrUpdateDinePlanUserRole(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    private _getDepartments() {
        this._commonLookupServiceProxy.getDepartmentForLookupTable().subscribe((result) => {
            this.departments = result.items || [];
        });
    }
}

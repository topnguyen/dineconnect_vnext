import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CommonLocationGroupDto,
    CommonLookupServiceProxy,
    CreateOrUpdateDinePlanUserInput,
    DinePlanUserEditDto,
    DinePlanUserRoleListDto,
    DinePlanUserRoleServiceProxy,
    DinePlanUserServiceProxy,
    FindUsersInput,
    NameValueDto,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './create-update-user.component.html',
    styleUrls: ['./create-update-user.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateUpdateConnectUserComponent extends AppComponentBase implements OnInit {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild('userLookupModal', { static: true }) userLookupModal: CommonLookupModalComponent;

    saving = false;
    userId: number;
    user = new DinePlanUserEditDto();
    userRoles: DinePlanUserRoleListDto[] = [];
    locationGroup: CommonLocationGroupDto;
    languages = abp.localization.languages;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _dinePlanUserRoleServiceProxy: DinePlanUserRoleServiceProxy,
        private _dinePlanUserServiceProxy: DinePlanUserServiceProxy
    ) {
        super(injector);
        this.userId = +this._activatedRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.configLookup();
        this._init();
    }

    openLocation() {
        this.selectLocationOrGroup.show(this.locationGroup);
    }

    selectLocations(result) {
        this.locationGroup = result;
    }

    configLookup() {
        this.userLookupModal.configure({
            title: this.l('SelectAUser'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string, tenantId?: number) => {
                let input = new FindUsersInput();
                input.filter = filter;
                input.excludeCurrentUser = true;
                input.maxResultCount = maxResultCount;
                input.skipCount = skipCount;
                input.tenantId = tenantId;

                return this._commonLookupServiceProxy.findUsers(input);
            }
        });
    }

    openUser() {
        this.userLookupModal.show();
    }

    userSelected(selectedItem: NameValueDto) {
        this.user.name = selectedItem.name;
        this.user.userId = +selectedItem.value;
    }

    cancelUser() {
        this.user.userId = null;
        this.user.name = null;
    }
    
    close() {
        this._router.navigate(['/app/connect/user/users']);
    }

    save(): void {
        const input = new CreateOrUpdateDinePlanUserInput();
        input.locationGroup = this.locationGroup;
        input.dinePlanUser = this.user;

        this.saving = true;
        this._dinePlanUserServiceProxy
            .createOrUpdateDinePlanUser(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    private _init() {
        this._dinePlanUserRoleServiceProxy.getAll(undefined, undefined, undefined, undefined, undefined, 1000, 0).subscribe((result) => {
            this.userRoles = result.items;
            this.userRoles.unshift({ id: 0, name: this.l('NotAssigned') } as any);
        });

        if (this.userId && this.userId != NaN) {
            this._dinePlanUserServiceProxy.getDinePlanUserForEdit(this.userId).subscribe((result) => {
                this.user = result.dinePlanUser;
                this.locationGroup = result.locationGroup;
            }); 
        }
    }
}

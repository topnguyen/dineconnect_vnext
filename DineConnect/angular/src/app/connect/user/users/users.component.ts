import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
} from "@angular/core";
import {
    CommonLocationGroupDto,
    EntityDto,
    DinePlanUserServiceProxy,
    DinePlanUserListDto,
} from "@shared/service-proxies/service-proxies";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import * as _ from "lodash";

import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { finalize } from "rxjs/operators";
import { Router } from "@angular/router";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    templateUrl: "./users.component.html",
    styleUrls: ["./users.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ConnectUsersComponent extends AppComponentBase {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;

    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    isDeleted;
    location: string;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _dinePlanUserServiceProxy: DinePlanUserServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._dinePlanUserServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditDinePlanUser(id): void {
        this._router.navigate([
            "/app/connect/user/users/create-or-update",
            id ? id : "null",
        ]);
    }

    deleteUser(user: DinePlanUserListDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._dinePlanUserServiceProxy
                    .deleteDinePlanUser(user.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    activateItem(user: DinePlanUserListDto) {
        const body = EntityDto.fromJS({ id: user.id });
        this._dinePlanUserServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
            this.location = locationOrGroup.groups[0].name;
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._dinePlanUserServiceProxy.getAllToExcel().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    search() {
        this.getAll();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }
}

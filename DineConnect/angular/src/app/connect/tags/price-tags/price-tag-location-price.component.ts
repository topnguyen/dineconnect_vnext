import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryServiceProxy, CreateOrEditPriceTagDto, LocationServiceProxy, PagedResultDtoOfSimpleLocationDto, PriceTagLocationPriceEditDto, PriceTagsServiceProxy, SimpleLocationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { ImportModalComponent } from '@app/connect/shared/import-modal/import-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable } from 'rxjs';
import { SelectPopupComponent } from '@app/shared/common/select-popup/select-popup.component';
import {MessageService} from 'abp-ng2-module';
@Component({
  selector: 'app-price-tag-location-price',
  templateUrl: './price-tag-location-price.component.html',
  styleUrls: ['./price-tag-location-price.component.css'],
  animations: [appModuleAnimation()]
})
export class PriceTagLocationPriceComponent extends AppComponentBase implements OnInit {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('selectLocationModal', { static: true }) selectLocationModal: SelectPopupComponent;

  filterText = '';
  tagId: number;
  pricetagDefinitionId: number;
  futuredataId: number;

  allItems = [];
  location: any;
  saving = false;
  selectedLocation: any;
  priceId: number;

  locationPriceEditDto = new PriceTagLocationPriceEditDto();

  constructor(
    injector: Injector,
    private _priceTagsServiceProxy: PriceTagsServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private _locationServiceProxy: LocationServiceProxy,
    private _categoryService: CategoryServiceProxy,
    private _messageService: MessageService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((params) => {
      this.tagId = +params['id'];
      this.futuredataId = this._activatedRoute.snapshot.queryParams['futuredataId'];

      this.priceId = +params['priceId'];
    });

    this.locationPriceEditDto.price = 0;
  }

  loadMenu(event?: LazyLoadEvent) {
    if (isNaN(this.priceId)) {
      return;
    }

    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();

    this._priceTagsServiceProxy
      .getAllPriceTagLocationPrice(
        this.filterText,
        this.tagId,
        this.priceId, // menu portion id
        undefined,
        this.primengTableHelper.getSorting(this.dataTable),
        this.primengTableHelper.getSkipCount(this.paginator, event),
        this.primengTableHelper.getMaxResultCount(this.paginator, event)
      )
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
      .subscribe((result) => {
        this.allItems = result.items;
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
        this._priceTagsServiceProxy
            .getPriceTagDefinition(null, this.tagId, this.priceId, undefined, undefined, 0, 1)
            .subscribe(result => {                
                  this.pricetagDefinitionId = result.id;
            });
      });

      
  }

  isDuplicateLocation(locationId: number) : boolean {
    if(this.allItems.find(x => x.locationId == locationId)) return true;
    return false;
  }

  reloadPage(): void {
    this.paginator.changePage(this.paginator.getPage());
  }

  save(): void {
    this.saving = true;
    this.locationPriceEditDto.priceTagDefinitionId = this.pricetagDefinitionId;
    this.locationPriceEditDto.locationId = this.selectedLocation.id;

    if(this.isDuplicateLocation(this.selectedLocation.id)) {
      this._messageService.error("Duplicate location");
      this.saving = false;
      return;
    }

    this._priceTagsServiceProxy.createPriceTagLocationPrice(this.locationPriceEditDto)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.notify.success(this.l('SavedSuccessfully'));
        this.loadMenu();
        this.clear();
      });
  }

  clear() {
    this.selectedLocation = undefined;
    this.location = undefined;
    this.locationPriceEditDto = new PriceTagLocationPriceEditDto();
    this.locationPriceEditDto.price = 0;
  }

  close() {
    this._router.navigate(['/app/connect/tags/priceTags/create-or-update', this.tagId]);
  }

  openFilterSelectLocationsModal() {
    this.selectLocationModal.show();
  }

  getCategorySelection() {
    return (): Observable<any> =>
    this._categoryService.getAll(
      undefined,
      undefined,
      undefined,
      undefined,
      undefined
  );
    }

  getLocationSelection() {
    return (): Observable<PagedResultDtoOfSimpleLocationDto> =>
        this._locationServiceProxy.getSimpleLocations(
            undefined,
            false,
            0,
            undefined,
            undefined,
            undefined
        );
  }

  changeLocation(event) {
    this.selectedLocation = event;
    this.location = this.selectedLocation.name;
    if (this.primengTableHelper.records.length)
      this.checkAvailableLocationForSave();
  }

  checkAvailableLocationForSave() {
    this.locationPriceEditDto.priceTagDefinitionId = this.priceId;
    this.locationPriceEditDto.locationId = this.selectedLocation.id;

    this._priceTagsServiceProxy.checkExistLocationPrice(this.locationPriceEditDto)
      .subscribe((result) => {
        if (result) {
          this.message.error('Location ' + this.location + ' already exists. To avoid duplicate, please delete the already exists record.', 'Location already exists.')
            .then(() => {
              this.clear();
            })
        }
      });
  }

  editPrice(record) {
    this.locationPriceEditDto = Object.assign(record);

    this.selectedLocation = new SimpleLocationDto();
    this.selectedLocation.id = this.locationPriceEditDto.locationId;
    this.selectedLocation.name = this.locationPriceEditDto.locationName;
    this.location = this.selectedLocation.name;
  }

  deleteLocationPrice(record) {
    this.message.confirm(
      '',
      this.l('AreYouSure'),
      (isConfirmed) => {
        if (isConfirmed) {
          this._priceTagsServiceProxy.deletePriceTagLocationPrice(record.id)
            .subscribe(() => {
              this.reloadPage();
              this.clear();
              this.notify.success(this.l('SuccessfullyDeleted'));
            });
        }
      }
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceTagLocationPriceComponent } from './price-tag-location-price.component';

describe('PriceTagLocationPriceComponent', () => {
  let component: PriceTagLocationPriceComponent;
  let fixture: ComponentFixture<PriceTagLocationPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceTagLocationPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceTagLocationPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateOrEditPriceTagDto, PriceTagsServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { EditTagPriceModalComponent } from './edit-tag-price-modal.component';
import { ImportModalComponent } from '@app/connect/shared/import-modal/import-modal.component';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
@Component({
    selector: 'app-edit-price-for-price-tag',
    templateUrl: './edit-price-for-price-tag.component.html',
    styleUrls: ['./edit-price-for-price-tag.component.scss'],
    animations: [appModuleAnimation()]
})
export class EditPriceForPriceTagComponent extends AppComponentBase implements OnInit {
    @ViewChild('importModal', { static: true }) importModal: ImportModalComponent;
    @ViewChild('editTagPrice', { static: true }) editTagPrice: EditTagPriceModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    tagId: number;
    priceTag = new CreateOrEditPriceTagDto();
    futuredataId: number;

    allItems = [];

    constructor(
        injector: Injector,
        private _priceTagsServiceProxy: PriceTagsServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _activatedRoute: ActivatedRoute
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe((params) => {
            this.tagId = +params['id'];
            this.futuredataId = this._activatedRoute.snapshot.queryParams['futuredataId'];
        });

        this._getPriceTag(this.tagId);
        this.configImport();
    }

    configImport() {
        this.importModal.configure({
            title: this.l('SelectATiffinProductOfferType'),
            downloadTemplate: (id?: number) => {
                return this._priceTagsServiceProxy.getImportTemplate(this.tagId);
            },
            import: (fileToken: string) => {
                return this._priceTagsServiceProxy.importPriceTagToDatabase(fileToken, this.futuredataId);
            }
        });
    }

    loadMenu(event?: LazyLoadEvent) {
        if (isNaN(this.tagId)) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._priceTagsServiceProxy
            .getMenuPricesForTag(
                this.filterText,
                this.tagId,
                undefined,
                this.futuredataId,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.allItems = result.items;
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    import() {
        this.importModal.show();
    }

    exportToExcel(): void {
        this.primengTableHelper.showLoadingIndicator();

        this._priceTagsServiceProxy
            .getMenuPricesForTagToExcel(this.tagId)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    editPrice(price) {
        this.editTagPrice.show(this.tagId, this.allItems, price, this.futuredataId);
    }

    private _getPriceTag(priceTagId: number) {
        this._priceTagsServiceProxy.getPriceTagForEdit(priceTagId).subscribe(result => {
            this.priceTag = result;
        })
    }


    openLocationPrice(priceId) {
        this._router.navigate(['/app/connect/tags/priceTags/location-price', this.tagId, priceId]);
    }
}

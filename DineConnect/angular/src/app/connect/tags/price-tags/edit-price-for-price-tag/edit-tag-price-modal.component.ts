import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TagMenuItemPortionDto, PriceTagsServiceProxy, TagPriceInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'edit-tag-price-modal',
    templateUrl: './edit-tag-price-modal.component.html',
    styleUrls: ['./edit-tag-price-modal.component.scss']
})
export class EditTagPriceModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    allItems = [];
    currentItem: TagMenuItemPortionDto;
    editingItem: TagMenuItemPortionDto;
    tagId: number;
    futureDataId: number;
    currentIndex = 0;

    constructor(injector: Injector, private _priceTagsServiceProxy: PriceTagsServiceProxy) {
        super(injector);
    }

    show(tagId: number, allItems: TagMenuItemPortionDto[], currentItem: TagMenuItemPortionDto, futuredataId: number): void {
        this.tagId = tagId;
        this.allItems = allItems;
        this.currentItem = currentItem;
        this.editingItem = Object.assign({}, currentItem);
        this.futureDataId = futuredataId;

        this.currentIndex = this.allItems.findIndex((item) => item === this.currentItem);

        this.active = true;
        this.modal.show();
        this.saving = false;
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    saveAndNext() {
        this.save(false);
    }

    saveAndClose() {
        this.save(true);
    }

    previous() {
        if (this.currentIndex <= 0) {
            return;
        }

        this.currentIndex--;
        this.currentItem = this.allItems[this.currentIndex];
        this.editingItem = Object.assign({}, this.currentItem);
    }

    save(close) {
        if (this.currentItem.tagPrice === this.editingItem.tagPrice) {
            this.executeAfterAction(close);
            return;
        }

        this.saving = true;
        let input = new TagPriceInput();
        input.tagId = this.tagId;
        input.price = this.editingItem.tagPrice;
        input.portionId = this.editingItem.id;
        input.futureDataId = this.futureDataId;

        this._priceTagsServiceProxy
            .updateTagPrice(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result) => {
                abp.notify.info(this.l('SavedSuccessfully'));
                this.currentItem.tagPrice = this.editingItem.tagPrice;
                this.executeAfterAction(close);
            });
    }

    executeAfterAction(close) {
        if (close) {
            this.close();
        } else {
            //Go to next

            this.currentIndex++;
            console.log(this.currentIndex);
            if (this.allItems.length <= this.currentIndex) {
                this.close();
                return;
            }

            this.currentItem = this.allItems[this.currentIndex];
            this.editingItem = Object.assign({}, this.currentItem);
        }
    }
}

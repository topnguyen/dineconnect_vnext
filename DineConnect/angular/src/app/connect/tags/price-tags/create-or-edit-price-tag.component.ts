import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    PriceTagsServiceProxy,
    CreateOrEditPriceTagDto,
    CommonLocationGroupDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router, ActivatedRoute } from "@angular/router";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-price-tag",
    templateUrl: "./create-or-edit-price-tag.component.html",
    styleUrls: ["./create-or-edit-price-tag.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditPriceTagComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;

    saving = false;

    priceTag: CreateOrEditPriceTagDto = new CreateOrEditPriceTagDto();

    priceTagTypes = [];
    departments = [];

    selectedDepartments = [];
    priceTagId: number;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _priceTagsServiceProxy: PriceTagsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.priceTagId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.priceTagId)) {
                this.priceTag = new CreateOrEditPriceTagDto();
                this.priceTag.locationGroup = new CommonLocationGroupDto();
            } else {
                this._priceTagsServiceProxy
                    .getPriceTagForEdit(this.priceTagId)
                    .subscribe((result) => {
                        this.priceTag = result;
                        this.selectedDepartments = this.priceTag.departments
                            ? this.priceTag.departments.split(",")
                            : [];
                    });
            }
        });
    }

    save(): void {
        this.saving = true;
        this.priceTag.departments = this.selectedDepartments.join(",");

        this._priceTagsServiceProxy
            .createOrEdit(this.priceTag)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/connect/tags/priceTags"]);
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.priceTag.locationGroup);
    }

    getLocations(event) {
        this.priceTag.locationGroup = event;
    }
}

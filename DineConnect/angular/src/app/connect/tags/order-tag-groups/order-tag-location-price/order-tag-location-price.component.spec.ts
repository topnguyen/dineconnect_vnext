import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTagLocationPriceComponent } from './order-tag-location-price.component';

describe('OrderTagLocationPriceComponent', () => {
  let component: OrderTagLocationPriceComponent;
  let fixture: ComponentFixture<OrderTagLocationPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTagLocationPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTagLocationPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
    PriceTagsServiceProxy,
    PriceTagDto,
    CommonLocationGroupDto,
    OrderTagGroupServiceProxy,
    OrderTagGroupListDto,
    EntityDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { SelectLocationComponent } from '@app/shared/common/select-location/select-location.component';
import { OrderTagGroupCloneComponent } from './order-tag-group-clone/order-tag-group-clone.component';

@Component({
    templateUrl: './order-tag-groups.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class OrderTagGroupsComponent extends AppComponentBase {
    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationComponent;
    @ViewChild('orderTagGroupClone', { static: true }) orderTagGroupClone: OrderTagGroupCloneComponent;

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    isDeleted;
    location: string;
    locationOrGroup: CommonLocationGroupDto;
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getOrderTagGroups(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        const locationOrGroup = this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._orderTagGroupServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.location,
                this.isDeleted,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                locationIds,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditOrderTag(id?: number): void {
        this._router.navigate(['/app/connect/tags/orderTagGroups/create-or-update', id ? id : 'null']);
    }

    deleteOrderTag(orderTag: OrderTagGroupListDto): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._orderTagGroupServiceProxy.deleteOrderTagGroup(orderTag.id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    activateItem(orderTag: OrderTagGroupListDto) {
        const body = EntityDto.fromJS({ id: orderTag.id });
        this._orderTagGroupServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    cloneOrderTag(orderTag: PriceTagDto): void {
        this.orderTagGroupClone.show(orderTag.id);
    }

    exportToExcel(): void {
        const locationOrGroup = this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._orderTagGroupServiceProxy
            .getToExcel(
                this.filterText,
                undefined,
                this.location,
                this.isDeleted,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                locationIds,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = '';

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(',');
            this.location = locationOrGroup.groups[0].name;
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags.map((x) => x.id).join(',');
            this.location = locationOrGroup.locationTags[0].name;
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations.map((x) => x.id).join(',');
            this.location = null;
        } else if (locationOrGroup.locations?.length) {
            this.location = locationOrGroup.locations[0].name;
            locationIds = locationOrGroup.locations.map((x) => x.id).join(',');
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getOrderTagGroups();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }


}

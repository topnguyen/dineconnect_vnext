import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    TicketTagGroupsServiceProxy,
    CreateOrEditTicketTagGroupDto,
    TicketTagDto,
    CommonLocationGroupDto,
    TicketTagGroupType,
    ComboboxItemDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router, ActivatedRoute } from "@angular/router";
import { StringValidationHelper } from "@shared/helpers/StringValidationHelper";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    selector: "app-create-or-edit-ticket-tag-group",
    templateUrl: "./create-or-edit-ticket-tag-group.component.html",
    styleUrls: ["./create-or-edit-ticket-tag-group.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditTicketTagGroupComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;

    saving = false;

    ticketTagGroup: CreateOrEditTicketTagGroupDto =
        new CreateOrEditTicketTagGroupDto();

    ticketTagGroupTypes = [];
    departments = [];

    selectedDepartments: ComboboxItemDto[];
    ticketTagGroupId: number;

    ticketTagGroupType = TicketTagGroupType;

    private sub: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _ticketTagGroupsServiceProxy: TicketTagGroupsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.ticketTagGroupId = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.ticketTagGroupId)) {
                this.ticketTagGroup = new CreateOrEditTicketTagGroupDto();
                this.ticketTagGroup.locationGroup =
                    new CommonLocationGroupDto();
                this.ticketTagGroup.tags = [];
            } else {
                this._ticketTagGroupsServiceProxy
                    .getTicketTagGroupForEdit(this.ticketTagGroupId)
                    .subscribe((result) => {
                        this.ticketTagGroup = result;
                        this.selectedDepartments = this.ticketTagGroup
                            .departments
                            ? JSON.parse(this.ticketTagGroup.departments)
                            : [];
                    });
            }
        });

        this.getTransactionTypes();
        this.getDepartments();
    }

    save(): void {
        this.saving = true;

        if (!this.checkValidationTags()) {
            this.saving = false;
            this.notify.warn(
                `${this.l(
                    "NameTagIsIncorrectFormat"
                )} ${this.ticketTagGroupType[
                    this.ticketTagGroup.dataType
                ].toString()}`
            );
            return;
        }

        this.ticketTagGroup.departments = JSON.stringify(
            this.selectedDepartments
        );

        this._ticketTagGroupsServiceProxy
            .createOrEdit(this.ticketTagGroup)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    checkValidationTags(): boolean {
        switch (+this.ticketTagGroup.dataType) {
            case TicketTagGroupType.Integer:
                return this.checkTags(
                    this.ticketTagGroup.tags,
                    StringValidationHelper.IsInteger
                );
            case TicketTagGroupType.Alphanumeric:
                return this.checkTags(
                    this.ticketTagGroup.tags,
                    StringValidationHelper.IsAlphanumeric
                );
            case TicketTagGroupType.Decimal:
                return this.checkTags(
                    this.ticketTagGroup.tags,
                    StringValidationHelper.IsDecimal
                );
            default:
                return true;
        }
    }

    checkTags(tags: TicketTagDto[], validate) {
        for (const tag of tags) {
            if (!validate(tag.name)) {
                return false;
            }
        }
        return true;
    }

    close(): void {
        this._router.navigate(["/app/connect/tags/ticketTagGroups"]);
    }

    private getTransactionTypes() {
        this._ticketTagGroupsServiceProxy
            .getTicketTagGroupTypeForLookupTable()
            .subscribe((result) => {
                this.ticketTagGroupTypes = result.items;
            });
    }

    private getDepartments() {
        this._ticketTagGroupsServiceProxy
            .getDepartmentForCombobox()
            .subscribe((result) => {
                this.departments = result.items;
            });
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.ticketTagGroup.locationGroup);
    }

    getLocations(event) {
        this.ticketTagGroup.locationGroup = event;
    }

    addTag() {
        this.ticketTagGroup.tags.push(
            Object.assign(new TicketTagDto(), {
                id: 0,
                name: "",
                AlternateName: "",
            })
        );
    }

    removeTag(index) {
        this.ticketTagGroup.tags.splice(index, 1);
    }
}

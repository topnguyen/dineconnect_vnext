import { Component, ViewChild, Injector } from "@angular/core";
import { Router } from "@angular/router";
import {
    TicketTagGroupsServiceProxy,
    TicketTagGroupDto,
    CommonLocationGroupDto,
    EntityDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { FileDownloadService } from "@shared/utils/file-download.service";
import * as _ from "lodash";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { SelectLocationComponent } from "@app/shared/common/select-location/select-location.component";
import { finalize } from "rxjs/operators";

@Component({
    selector: "ticket-tag-groups",
    templateUrl: "./ticket-tag-groups.component.html",
    styleUrls: ["./ticket-tag-groups.component.scss"],
    animations: [appModuleAnimation()],
})
export class TicketTagGroupsComponent extends AppComponentBase {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    filterText = "";
    isDeleted;
    locationOrGroup: CommonLocationGroupDto;

    constructor(
        injector: Injector,
        private _ticketTagGroupsServiceProxy: TicketTagGroupsServiceProxy,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getTicketTagGroups(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this._ticketTagGroupsServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditTicketTagGroup(id?: number): void {
        this._router.navigate([
            "/app/connect/tags/ticketTagGroups/create-or-update",
            id ? id : "null",
        ]);
    }

    deleteTicketTagGroup(ticketTagGroup: TicketTagGroupDto): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._ticketTagGroupsServiceProxy
                    .delete(ticketTagGroup.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    exportToExcel(): void {
        this._ticketTagGroupsServiceProxy
            .getTicketTagGroupsToExcel(this.filterText)
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }

    refresh() {
        this.filterText = undefined;
        this.isDeleted = false;
        this.locationOrGroup = new CommonLocationGroupDto();

        this.paginator.changePage(0);
    }

    search() {
        this.getTicketTagGroups();
    }

    openFilterSelectLocationsModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    setFilterLocations(event: CommonLocationGroupDto) {
        this.locationOrGroup = event;
    }

    activateItem(tag: TicketTagGroupDto) {
        const body = EntityDto.fromJS({ id: tag.id });
        this._ticketTagGroupsServiceProxy
            .activateItem(body)
            .subscribe((result) => {
                this.notify.success(this.l("Successfully"));
                this.reloadPage();
            });
    }
}

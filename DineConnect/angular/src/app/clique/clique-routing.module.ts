import { CreateOrUpdateCliqueMenuModalComponent } from './manage/clique-menu/create-or-update-clique-menu.component';
import { ScreenMenusComponent } from './manage/menu/screen-menus/screen-menus.component';
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BrandComponent } from "./manage/brand/brand.component";
import { CreateOrUpdateBrandModalComponent } from "./manage/brand/create-or-update-brand.component";
import { CategoriesComponent } from "./manage/categories/categories.component";
import { CreateOrEditCategoryComponent } from "./manage/categories/create-or-edit-category.component";
import { CompanyComponent } from "./manage/company/company.component";
import { CreateOrUpdateCompanyComponent } from "./manage/company/create-or-update-company.component";
import { ProductGroupsComponent } from "./manage/product-groups/product-groups.component";
import { CreateOrEditMenuitemComponent } from "./manage/product/create-or-edit-menuitem.component";
import { MenuItemComponent } from "./manage/product/menuItem.component";
import { CreateOrEditOrderTagGroupComponent } from "./manage/tags/create-or-edit-order-tag-group.component";
import { OrderTagGroupsComponent } from "./manage/tags/order-tag-groups.component";
import { CreateOrEditScreenMenuComponent } from './manage/menu/screen-menus/create-or-edit-screen-menu.component';
import { ScreenMenuCategoriesComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-categories.component';
import { EditScreenMenuCategoryComponent } from './manage/menu/screen-menus/screen-menu-categories/edit-screen-menu-category.component';
import { ScreenMenuMenuitemComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem.component';
import { ScreenMenuMenuitemEditComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-edit/screen-menu-menuitem-edit.component';
import { ScreenMenuVirtualscreenComponent } from './manage/menu/screen-menus/screen-menu-virtualscreen/screen-menu-virtualscreen.component';
import { ScheduleComponent } from './manage/schedule/schedule.component';
import { CreateOrUpdateScheduleComponent } from './manage/schedule/create-or-update-schedule.component';
import { CliqueMealTimeComponent } from "./manage/clique-meal-time/clique-meal-time.component";
import { CliqueMenuComponent } from "./manage/clique-menu/clique-menu.component";
import { CreateOrUpdateCliqueMealTimeModalComponent } from './manage/clique-meal-time/create-or-update-clique-meal-time.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerOrderComponent } from './customers/customer-order/customer-order.component';
import { CustomerTransactionsComponent } from './customers/customer-transactions/customer-transactions.component';
import { CreateOrEditCustomerComponent } from './customers/create-or-edit-customer.component';
import { CustomerAddressComponent } from './customers/customer-address/customer-address.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "manage/brand",
                        component: BrandComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/brand/create-or-update/:id",
                        component: CreateOrUpdateBrandModalComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/company",
                        component: CompanyComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/clique-meal-time",
                        component: CliqueMealTimeComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/clique-meal-time/create-or-update/:id",
                        component: CreateOrUpdateCliqueMealTimeModalComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/schedule",
                        component: CliqueMenuComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/schedule/create-or-update/:id",
                        component: CreateOrUpdateCliqueMenuModalComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/company/create-or-update/:id",
                        component: CreateOrUpdateCompanyComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/product/group",
                        component: ProductGroupsComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/product/category/create-or-update/:id",
                        component: CreateOrEditCategoryComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/product/category",
                        component: CategoriesComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/product/product",
                        component: MenuItemComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/product/product/create-or-update/:id",
                        component: CreateOrEditMenuitemComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/menu/tag",
                        component: OrderTagGroupsComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/menu/tag/create-or-update/:id",
                        component: CreateOrEditOrderTagGroupComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/menu/screenmenu",
                        component: ScreenMenusComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/menu/screenmenu/create-or-update/:id",
                        component: CreateOrEditScreenMenuComponent,
                        data: { permission: "" }
                    },
                    {
                        path: "manage/menu/screenmenu/:id/screen-menu-categories",
                        component: ScreenMenuCategoriesComponent,
                        data: {
                            permission: "",
                        },
                    },
                    {
                        path: "manage/menu/screenmenu/:screenMenuId/screen-menu-categories/create-or-update/:id",
                        component: EditScreenMenuCategoryComponent,
                        data: {
                            permission: "",
                        },
                    },
                    {
                        path: "manage/menu/screenmenu/:screenMenuId/screen-menu-categories/:id/screen-menu-menuitems",
                        component: ScreenMenuMenuitemComponent,
                        data: {
                            permission: "",
                        },
                    },
                    {
                        path: "manage/menu/screenmenu/:screenMenuId/screen-menu-categories/:screenMenuCategoryId/screen-menu-menuitems/create-or-update/:id",
                        component: ScreenMenuMenuitemEditComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "manage/menu/screenmenu/:id/screen-menu-virtualscreen",
                        component: ScreenMenuVirtualscreenComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "manage/menu/schedule",
                        component: ScheduleComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "manage/menu/schedule/create-or-update/:id",
                        component: CreateOrUpdateScheduleComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "customer/customers",
                        component: CustomersComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    { 
                        path: 'customer/customers/customer-address/:id/:customerId', 
                        component: CustomerAddressComponent,
                        data: { permission: 'Pages' } 
                    },
                    {
                        path: "customer/customers/create-or-update/:id",
                        component: CreateOrEditCustomerComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "customer/customerTransactions",
                        component: CustomerTransactionsComponent,
                        data: {
                            permission:
                                "",
                        },
                    },
                    {
                        path: "customer/customer-order",
                        component: CustomerOrderComponent,
                        data: {
                            permission:
                                "",
                        },
                    }
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class CliqueRoutingModule { }
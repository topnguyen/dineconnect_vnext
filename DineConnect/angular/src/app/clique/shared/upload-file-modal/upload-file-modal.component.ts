import { Component, EventEmitter, Injector, Output, ViewChild, OnInit, Input } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { FileUploader } from 'ng2-file-upload';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { TokenService, IAjaxResponse } from 'abp-ng2-module';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-upload-file-modal',
  templateUrl: './upload-file-modal.component.html',
  styleUrls: ['./upload-file-modal.component.css']
})
export class UploadFileModalComponent extends AppComponentBase implements OnInit {
  @Input() loadData: (id: number) => Observable<any>;
  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild('fileInput', { static: true }) fileInput: any;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  uploader: FileUploader;

  isShown = false;
  loading = false;
  saving = false;
  uploadUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
  files = [];
  id: number;

  constructor(injector: Injector, private _tokenService: TokenService, private _fileDownloadService: FileDownloadService) {
    super(injector);
  }

  ngOnInit(): void {
    this.initUploaders();
  }

  show(id?: number): void {
    this.id = id;
    this.modal.show();
  }

  close(): void {
    this.saving = false;
    this.loading = false;
    this.modal.hide();
  }

  shown(): void {
    this.isShown = true;
    this.fileInput.nativeElement.value = '';
    if (this.id) {
      this.files = [];
      this.loadData(this.id).subscribe(result => {
        if (result) {
          this.files = JSON.parse(result);
        }
      });
    }
  }

  initUploaders(): void {
    this.uploader = new FileUploader({
      url: this.uploadUrl,
      authToken: 'Bearer ' + this._tokenService.getToken(),
      filters: [],
      additionalParameter: undefined
    });

    this.uploader.onAfterAddingFile = (f) => {
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);;
      }
    };

    this.uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <IAjaxResponse>JSON.parse(response);
      if (ajaxResponse.success) {
        this.files.push(ajaxResponse.result);
      } else {
        this.saving = false;
        this.message.error(ajaxResponse.error.message);
      }
      this.loading = false;

    };
    this.uploader.onCompleteAll = () => {
      this.fileInput.nativeElement.value = '';
    };
  }

  save() {
    let input = { id: this.id, files: JSON.stringify(this.files) }
    this.modalSave.emit(input);
    this.close();
  }

  onFileSelected() {
    this.loading = true;

    this.uploader.uploadAll();
  }

  removeFile(index) {
    this.files.splice(index);
  }
}

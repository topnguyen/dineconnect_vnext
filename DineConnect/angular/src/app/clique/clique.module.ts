import { AutoCompleteModule } from 'primeng/autocomplete';
import { CustomerAddressComponent } from './customers/customer-address/customer-address.component';
import { CustomerOrderComponent } from './customers/customer-order/customer-order.component';
import { EditCustomerOfferModalComponent } from './customers/customer-transactions/edit-balance-modal.component';
import { CreateOrUpdateCliqueMenuModalComponent } from './manage/clique-menu/create-or-update-clique-menu.component';
import { CliqueMenuComponent } from './manage/clique-menu/clique-menu.component';
import { CreateOrUpdateCliqueMealTimeModalComponent } from './manage/clique-meal-time/create-or-update-clique-meal-time.component';
import { CliqueMealTimeComponent } from './manage/clique-meal-time/clique-meal-time.component';
import { EditScreenMenuCategoryComponent } from './manage/menu/screen-menus/screen-menu-categories/edit-screen-menu-category.component';
import { AddScreenMenuCategoryComponent } from './manage/menu/screen-menus/screen-menu-categories/add-screen-menu-category.component';
import { CategoryServiceProxy, CliqueScreenMenuServiceProxy, CompanyServiceProxy, ConnectLookupServiceProxy, OrderTagGroupServiceProxy, ScreenMenuServiceProxy, MenuItemScheduleProxy, CliqueMealTimeServiceProxy, CliqueMenuServiceProxy, CliqueCustomerServiceProxy } from './../../shared/service-proxies/service-proxies';
import { ImportModalComponent } from './shared/import-modal/import-modal.component';
import { ConfirmationService, ContextMenuModule, MultiSelectModule, PickListModule, TreeModule } from 'primeng';
import { CliqueRoutingModule } from './clique-routing.module';
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrandComponent } from "./manage/brand/brand.component";
import { ProductComboServiceProxy } from "@shared/service-proxies/service-proxies";
import { CommonModule, DatePipe } from "@angular/common";
import { AgmCoreModule } from "@agm/core";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { ColorPickerModule } from "ngx-color-picker";
import { PopoverModule } from "ngx-bootstrap/popover";
import { NgxCheckboxModule } from "ngx-checkbox";
import { TimepickerModule } from "ngx-bootstrap/timepicker";
import { UiSwitchModule } from "ngx-ui-switch";
import { NgSelectModule } from "@ng-select/ng-select";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { ModalModule } from "ngx-bootstrap/modal";
import { PaginatorModule } from "primeng/paginator";
import { TableModule } from "primeng/table";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { UtilsModule } from "@shared/utils/utils.module";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { AppBsModalModule } from "@shared/common/appBsModal/app-bs-modal.module";
import { AppCommonModule } from "@app/shared/common/app-common.module";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { FileUploadModule } from "ng2-file-upload";
import { FormlyModule } from "@ngx-formly/core";
import { FormlyBootstrapModule } from "@ngx-formly/bootstrap";

import { NgxQRCodeModule } from "@techiediaries/ngx-qrcode";
import { CheckboxModule } from "primeng/checkbox";
import { CalendarModule, ConfirmDialogModule } from "primeng";
import { DialogModule } from "primeng/dialog";
import { CardModule } from "primeng/card";
import { DataViewModule } from "primeng/dataview";
import { SliderModule } from "primeng/slider";
import { RadioButtonModule } from "primeng/radiobutton";
import { TiffinModule } from "@app/tiffin/tiffin.module";
import { CreateOrUpdateBrandModalComponent } from './manage/brand/create-or-update-brand.component';
import { CompanyComponent } from './manage/company/company.component';
import { CreateOrUpdateCompanyComponent } from './manage/company/create-or-update-company.component';
import { CreateOrEditLanguageDescriptionModalComponent } from './manage/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component';
import { CreateOrEditProductGroupModalComponent } from './manage/product-groups/create-or-edit-productGroup-modal.component';
import { ProductGroupsComponent } from './manage/product-groups/product-groups.component';
import { CreateOrEditCategoryComponent } from './manage/categories/create-or-edit-category.component';
import { CategoriesComponent } from './manage/categories/categories.component';
import { CreateOrEditMenuitemComponent } from './manage/product/create-or-edit-menuitem.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MenuItemComponent } from './manage/product/menuItem.component';
import { OrderTagGroupsComponent } from './manage/tags/order-tag-groups.component';
import { CreateOrEditOrderTagGroupComponent } from './manage/tags/create-or-edit-order-tag-group.component';
import { OrderTagGroupCloneComponent } from './manage/tags/order-tag-group-clone/order-tag-group-clone.component';
import { UploadFileModalComponent } from './shared/upload-file-modal/upload-file-modal.component';
import { TextMaskModule } from 'angular2-text-mask';
import { ScreenMenusComponent } from './manage/menu/screen-menus/screen-menus.component';
import { CreateOrEditScreenMenuComponent } from './manage/menu/screen-menus/create-or-edit-screen-menu.component';
import { ScreenMenuCategoriesComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-categories.component';
import { DragulaModule } from 'ng2-dragula';
import { TagInputModule } from 'ngx-chips';
import { ScreenMenuVirtualscreenComponent } from './manage/menu/screen-menus/screen-menu-virtualscreen/screen-menu-virtualscreen.component';
import { ScreenMenuMenuitemComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem.component';
import { ScreenMenuMenuitemEditComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-edit/screen-menu-menuitem-edit.component';
import { ScreenMenuMenuitemAddComponent } from './manage/menu/screen-menus/screen-menu-categories/screen-menu-menuitem/screen-menu-menuitem-add/screen-menu-menuitem-add.component';
import { ScheduleComponent } from './manage/schedule/schedule.component';
import { CreateOrUpdateScheduleComponent } from './manage/schedule/create-or-update-schedule.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerTransactionsComponent } from './customers/customer-transactions/customer-transactions.component';
import { ChangeHistoryModalComponent } from './customers/customer-transactions/change-history-modal.component';
import { CreateOrEditCustomerComponent } from './customers/create-or-edit-customer.component';
@NgModule({
    imports: [
        FormsModule,
        CliqueRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TabsModule,
        TooltipModule,
        NgxQRCodeModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        NgSelectModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        AppBsModalModule,
        UtilsModule,
        AccordionModule.forRoot(),
        ColorPickerModule,
        NgxCheckboxModule,
        UiSwitchModule,
        TabsModule,
        DragDropModule,
        FileUploadModule,
        FormlyModule.forRoot({
            validationMessages: [
                { name: "required", message: "This field is required" },
            ],
        }),
        FormlyBootstrapModule,
        CheckboxModule,
        CalendarModule,
        DialogModule,
        DataViewModule,
        CardModule,
        SliderModule,
        RadioButtonModule,
        TiffinModule,
        ConfirmDialogModule,
        TreeModule,
        ContextMenuModule,
        MatCheckboxModule,
        MultiSelectModule,
        TextMaskModule,
        DragulaModule.forRoot(),
        TagInputModule,
        PickListModule,
        AutoCompleteModule
    ],
    declarations: [
        BrandComponent,
        CreateOrUpdateBrandModalComponent,
        ImportModalComponent,
        CompanyComponent,
        CreateOrUpdateCompanyComponent,
        ProductGroupsComponent,
        CreateOrEditLanguageDescriptionModalComponent,
        CreateOrEditProductGroupModalComponent,
        CreateOrEditCategoryComponent,
        CategoriesComponent,
        CreateOrEditMenuitemComponent,
        MenuItemComponent,
        OrderTagGroupsComponent,
        CreateOrEditOrderTagGroupComponent,
        OrderTagGroupCloneComponent,
        UploadFileModalComponent,
        ScreenMenusComponent,
        CreateOrEditScreenMenuComponent,
        ScreenMenuCategoriesComponent,
        AddScreenMenuCategoryComponent,
        EditScreenMenuCategoryComponent,
        ScreenMenuVirtualscreenComponent,
        ScreenMenuMenuitemComponent,
        ScreenMenuMenuitemEditComponent,
        ScreenMenuMenuitemAddComponent,
        ScheduleComponent,
        CreateOrUpdateScheduleComponent,
        CliqueMealTimeComponent,
        CreateOrUpdateCliqueMealTimeModalComponent,
        CliqueMenuComponent,
        CreateOrUpdateCliqueMenuModalComponent,
        CustomersComponent,
        CustomerTransactionsComponent,
        ChangeHistoryModalComponent,
        EditCustomerOfferModalComponent,
        CreateOrEditCustomerComponent,
        CustomerOrderComponent,
        CustomerAddressComponent
    ],
    providers: [ProductComboServiceProxy, 
        DatePipe,
        ConfirmationService, 
        CompanyServiceProxy, 
        CategoryServiceProxy, 
        OrderTagGroupServiceProxy,
        ConnectLookupServiceProxy,
        CliqueScreenMenuServiceProxy,
        MenuItemScheduleProxy,
        CliqueMealTimeServiceProxy,
		CliqueMenuServiceProxy,
        CliqueCustomerServiceProxy,
        CliqueCustomerServiceProxy
    ],
})
export class CliqueModule {}

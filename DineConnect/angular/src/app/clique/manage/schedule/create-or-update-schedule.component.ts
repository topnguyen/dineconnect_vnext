import { WheelOpeningHourServiceType } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { PagedResultDtoOfLocationDto } from './../../../../shared/service-proxies/service-proxies';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  LocationServiceProxy,
  PagedResultDtoOfMenuItemListDto,
  MenuItemServiceProxy,
  MenuItemScheduleProxy,
  MenuItemScheduleInputDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, Injector } from '@angular/core';
import moment from 'moment';
import { OpeningHour } from './opening-hour';

@Component({
  selector: 'createOrUpdateSchedule',
  styleUrls: ['./create-or-update-schedule.component.scss'],
  templateUrl: './create-or-update-schedule.component.html',
  animations: [appModuleAnimation()]
})
export class CreateOrUpdateScheduleComponent extends AppComponentBase implements OnInit {
  isAllDays: boolean = false;
  isFixedMenu: boolean = false;
  locationName = '';
  selectedLocation: any;
  menuItemName = '';
  selectedMenuItem: any;
  menuItemScheduleInputDto: MenuItemScheduleInputDto = new MenuItemScheduleInputDto();
  saving = false;
  openingHour: OpeningHour = new OpeningHour(WheelOpeningHourServiceType.Default);
  public itemDays = 
  [
    {text: '', value: ''}, 
    {text: 'Monday', value: 'MON'}, 
    {text: 'TuesDay', value: 'TUE'}, 
    {text: 'Wednesday', value: 'WED'}, 
    {text: 'Thursday', value: 'THU'}, 
    {text: 'Friday', value: 'FRI'}, 
    {text: 'Saturday', value: 'SAT'}, 
    {text: 'Sunday', value: 'SUN'}
  ]
  isValid: boolean = false;
  constructor(
    injector: Injector,
    private _router: Router,
    private _locationServiceProxy: LocationServiceProxy,
    private _menuItemServiceProxy: MenuItemServiceProxy,
    private _activeRouter: ActivatedRoute,
    private _menuItemScheduleProxy: MenuItemScheduleProxy,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._activeRouter.params.subscribe((params) => {
      const idMenuItemSchedule = +params['id'];
      if (idMenuItemSchedule > 0) {
        this.getMenuItemScheduleById(idMenuItemSchedule);

      }
    });
    
  }

  selectAllDays(event) {
    this.isAllDays = event;

  }

  selectFixedMenu(event) {
    this.isFixedMenu = event;
  }

  save() {
    this.saving = true;
    this.menuItemScheduleInputDto.startHour = this.openingHour.from ? moment(this.openingHour.from,'HH:mm').get('hour') : 0;
    this.menuItemScheduleInputDto.startMinute = this.openingHour.from ? moment(this.openingHour.from,'HH:mm').get('minutes') : 0;
    this.menuItemScheduleInputDto.endHour = this.openingHour.to ? moment(this.openingHour.to,'HH:mm').get('hour') : 23;
    this.menuItemScheduleInputDto.endMinute = this.openingHour.to ? moment(this.openingHour.to,'HH:mm').get('minutes') : 59;
    this.menuItemScheduleInputDto.days = "";
    this.openingHour.days.filter(day=> day.isSelected).forEach(dayChild => this.menuItemScheduleInputDto.days += dayChild.name + ",");
    
    this._menuItemScheduleProxy
      .createOrUpdateMenuItemSchedule(this.menuItemScheduleInputDto)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe((result) => {
        this.saving = false;
        this.notify.success(this.l('SuccessfullySaved'));
        this.close();
      });
  }

  close() {
    this._router.navigate(["/app/clique/manage/menu/schedule"]);
  }

  getLocationForCombobox() {
    return (
      filterString,
      sorting,
      maxResultCount,
      skipCount
    ): Observable<PagedResultDtoOfLocationDto> => {
      return this._locationServiceProxy.getList(
        filterString,
        false,
        0,
        sorting,
        maxResultCount,
        skipCount
      );
    };
  }

  changeLocation(data) {
    this.locationName = data.name;
    this.selectedLocation = data.id;
  }

  getMenuItemForCombobox() {
    return (
      filterString,
      sorting,
      maxResultCount,
      skipCount
    ): Observable<PagedResultDtoOfMenuItemListDto> => {
      return this._menuItemServiceProxy.getAll(
        filterString,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        sorting,
        skipCount,
        maxResultCount
      );
    };
  }

  changeMenuItem(data) {
    this.menuItemName = data.name;
    this.selectedMenuItem = data.id;
    this.menuItemScheduleInputDto.menuItemId = data.id;
    this.menuItemScheduleInputDto.menuItemName = data.name;
  }

  getMenuItemScheduleById(menuItemScheduleId: number) {
    this._menuItemScheduleProxy.getMenuItemScheduleById(menuItemScheduleId).subscribe(menuItemSchedule => {
      this.menuItemScheduleInputDto = menuItemSchedule;
      this.openingHour.from = `${this.menuItemScheduleInputDto.startHour}:${this.menuItemScheduleInputDto.startMinute}`;
      this.openingHour.to = `${this.menuItemScheduleInputDto.endHour}:${this.menuItemScheduleInputDto.endMinute}`;
      let daySchedules = this.menuItemScheduleInputDto.days.split(",");
      daySchedules.pop();
      this.openingHour.days.forEach(day=>day.isSelected = false);
      daySchedules.forEach(day=>{
        this.openingHour.days.find(dayOpening=>dayOpening.name == day).isSelected = true;
      })
    })
  }

  selectCalendar() {
    if(!moment(this.openingHour.from, 'h:mma').isBefore(moment(this.openingHour.to, 'h:mma')) || !this.openingHour.from || !this.openingHour.to) {
      this.isValid = true;
    }else {
      this.isValid = false;
    }
  }

}

import {
  Component,
  Injector,
  ViewChild,
  OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
  MenuItemScheduleProxy,
  CommonLocationGroupDto,
  GetAllMenuItemScheduleDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent extends AppComponentBase implements OnInit {
  @ViewChild("scheduleTable", { static: true }) scheduleTable: Table;
  @ViewChild("categoryPaginator", { static: true }) categoryPaginator: Paginator;
  locationOrGroup?: CommonLocationGroupDto;

  constructor(
    injector: Injector,
    private _menuItemScheduleProxy: MenuItemScheduleProxy,
    private _router: Router
  ) {
    super(injector);
  }

  ngOnInit() {
  }

  getItems(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.categoryPaginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();

    const locationOrGroup =
      this.locationOrGroup ?? new CommonLocationGroupDto();
    const locationGroup = locationOrGroup.group;
    const locationTag = locationOrGroup.locationTag;
    const nonLocations = !!locationOrGroup.nonLocations?.length;
    const locationIds = this._getLocationIds(locationOrGroup);

    this._menuItemScheduleProxy
      .getAll(
        this.primengTableHelper.getSorting(this.scheduleTable) || undefined,
        this.primengTableHelper.getSkipCount(this.categoryPaginator, event),
        this.primengTableHelper.getMaxResultCount(this.categoryPaginator, event)
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();

      });
  }

  private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
    let locationIds = "";

    if (locationOrGroup.group) {
      locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
    } else if (locationOrGroup.locationTag) {
      locationIds = locationOrGroup.locationTags
        .map((x) => x.id)
        .join(",");
    } else if (locationOrGroup.nonLocations?.length) {
      locationIds = locationOrGroup.nonLocations
        .map((x) => x.id)
        .join(",");
    } else if (locationOrGroup.locations?.length) {
      locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
    }
    return locationIds;
  }

  deleteSchedule(getAllMenuItemScheduleDto: GetAllMenuItemScheduleDto) {
    this.message.confirm(
      "you want to delete menu item schedule ",
      this.l("AreYouSure"),
      (isConfirmed) => {
        if (isConfirmed) {
          this._menuItemScheduleProxy
            .deleteMenuItemSchedule(getAllMenuItemScheduleDto.id)
            .subscribe((result) => {
              this.notify.success(this.l("SuccessfullyDeleted"));
              this.reloadPage();
            });
        }
      }
    );
  }

  reloadPage(): void {
    this.categoryPaginator.changePage(this.categoryPaginator.getPage());
  }

  createOrUpdateSchedule(id?: number) {
    this._router.navigate([
      "/app/clique/manage/menu/schedule/create-or-update",
      id ? id : "null",
    ]);
  }
  convertTime(time: number): string{
    if (time < 10) {
      return `0${time}`;
    }
    return `${time}`;
  }
}

import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLocationGroupDto, CreateOrUpdateOrderTagGroupInput, OrderTagGroupEditDto, OrderTagGroupServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-order-tag-group-clone',
    templateUrl: './order-tag-group-clone.component.html',
    styleUrls: ['./order-tag-group-clone.component.scss']
})
export class OrderTagGroupCloneComponent extends AppComponentBase {
    @ViewChild('cloneOrderTagModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    tag: OrderTagGroupEditDto;
    locationGroup: CommonLocationGroupDto = new CommonLocationGroupDto();
    newName = null;
    oldOrderTagId: number;

    constructor(injector: Injector, private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy) {
        super(injector);
    }

    show(oldOrderTagId: number): void {
        this.oldOrderTagId = oldOrderTagId;
        this.saving = false;
        this.modal.show();
        this.init();
    }

    save(): void {
        this.saving = true;
        this.tag.name = this.newName;
        delete this.tag.id
        this.tag.tags.forEach(item => delete item.id);
        this.tag.maps.forEach(item => delete item.id);

        const body = CreateOrUpdateOrderTagGroupInput.fromJS({
            orderTagGroup: this.tag,
            locationGroup: this.locationGroup,
        });

        this._orderTagGroupServiceProxy
            .createOrUpdateOrderTagGroup(body)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    init() {
        this._orderTagGroupServiceProxy.getTagForEdit(this.oldOrderTagId).subscribe((result) => {
            this.tag = result.orderTagGroup;
            this.locationGroup = result.locationGroup;
        });
    }

    close(): void {
        this.modal.hide();
        this.modalSave.emit(null);
    }
}

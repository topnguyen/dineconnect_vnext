import { Component, ViewChild, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import {
    CategoryServiceProxy,
    ComboboxItemDto,
    CommonLocationGroupDto,
    CommonLookupServiceProxy,
    CreateOrUpdateOrderTagGroupInput,
    GetOrderTagGroupForEditOutput,
    MenuItemServiceProxy,
    OrderMapDto,
    OrderTagDto,
    OrderTagGroupServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as moment from "moment";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonLookupModalComponent } from "@app/shared/common/lookup/common-lookup-modal.component";
import { CreateOrEditLanguageDescriptionModalComponent } from "@app/connect/menu/create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
import { UploadFileModalComponent } from "@app/connect/shared/upload-file-modal/upload-file-modal.component";
import { Observable } from "rxjs";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { SelectItemPopupComponent } from "@app/shared/common/select-item-popup/select-item-popup.component";

@Component({
    selector: "app-create-or-edit-order-tag-group",
    templateUrl: "./create-or-edit-order-tag-group.component.html",
    styleUrls: ["./create-or-edit-order-tag-group.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditOrderTagGroupComponent
    extends AppComponentBase
    implements OnInit
{
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationModal: SelectLocationOrGroupComponent;
    @ViewChild("productLookupModal", { static: true })
    productLookupModal: SelectItemPopupComponent;
    @ViewChild("categoryLookupModal", { static: true })
    categoryLookupModal: CommonLookupModalComponent;
    @ViewChild("createOrEditLanguageDescriptionModal", { static: true })
    createOrEditLanguageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;
    @ViewChild("uploadFileModal", { static: true })
    uploadFileModal: UploadFileModalComponent;

    tagId: number;
    saving = false;
    orderTagGroupInput: CreateOrUpdateOrderTagGroupInput =
        new CreateOrUpdateOrderTagGroupInput();
    departments = [];
    tag = this.orderTagGroupInput.orderTagGroup;
    locationGroup: CommonLocationGroupDto = new CommonLocationGroupDto();
    requestDepartments = [];
    selectedRow: any;
    uploadImageData: any;
    isUploadForTag = false;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _orderTagGroupServiceProxy: OrderTagGroupServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.tagId = +params["id"];

            this.getDepartments();
            this.configLookup();

            if (!isNaN(this.tagId)) {
                this._orderTagGroupServiceProxy
                    .getTagForEdit(this.tagId)
                    .subscribe((result) => {
                        this.tag = result.orderTagGroup;
                        this.locationGroup = result.locationGroup;
                        this.requestDepartments = result.departments;
                    });
            } else {
                this.tag.tags = [];
                this.tag.maps = [];
                this.tag.minSelectedItems = 0;
                this.tag.maxSelectedItems = 0;
            }
        });

        this.getDepartments();
    }

    save(): void {
        this.saving = true;
        const body = CreateOrUpdateOrderTagGroupInput.fromJS({
            orderTagGroup: this.tag,
            locationGroup: this.locationGroup,
            departments: this.requestDepartments,
        });
        console.log(body);

        this._orderTagGroupServiceProxy
            .createOrUpdateOrderTagGroup(body)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/clique/manage/menu/tag"]);
    }

    openforMenuItem(tag: OrderTagDto) {
        this.selectedRow = tag;
        this.productLookupModal.show(tag.categoryId);
    }

    openforCategoryItem(map: OrderMapDto) {
        this.selectedRow = map;
        this.categoryLookupModal.show();
    }

    configLookup() {
        this.categoryLookupModal.configure({
            title: this.l("Select"),
            dataSource: (
                skipCount: number,
                maxResultCount: number,
                filter: string
            ) => {
                return this._categoryServiceProxy.getAll(
                    filter,
                    false,
                    "name",
                    maxResultCount,
                    skipCount
                ) as any;
            },
        });
    }

    productSelected(event) {
        this.selectedRow.menuItemId = event.id;
        this.selectedRow.menuItemGroupCode = event.name;
        this.selectedRow = null;
    }

    categorySelected(event) {
        this.selectedRow.categoryId = event.id;
        this.selectedRow.categoryName = event.name;
        this.selectedRow.menuItemId = null;
        this.selectedRow.menuItemGroupCode = null;
        this.selectedRow = null;
    }

    openSelectLocationModal() {
        this.selectLocationModal.show(this.locationGroup);
    }

    getLocations(event) {
        this.locationGroup = event;
    }

    addTag() {
        this.tag.tags.push({
            id: null,
            name: "",
            alternateName: "",
            menuItemId: null,
            maxQuantity: 1,
            price: 0,
            sortOrder: 0,
        } as OrderTagDto);
    }

    removeTag(index) {
        this.tag.tags.splice(index, 1);
    }

    addMap() {
        this.tag.maps.push({
            categoryName: "",
            menuItemId: null,
            categoryId: null,
        } as OrderMapDto);
    }

    removeMap(productIndex) {
        this.tag.maps.splice(productIndex, 1);
    }

    private getDepartments() {
        this._commonLookupServiceProxy
            .getDepartmentForLookupTable()
            .subscribe((result) => {
                this.departments = result.items;
            });
    }

    showLanguages() {
        this.createOrEditLanguageDescriptionModal.show(
            "OrderTagGroup",
            this.tagId,
            this.tagId + " - " + this.tag.name
        );
    }

    openOrderTagLanguages(orderTag) {
        this.createOrEditLanguageDescriptionModal.show(
            "OrderTag",
            orderTag.id,
            orderTag.id + " - " + orderTag.alternateName
        );
    }

    showUploadImages() {
        this.uploadImageData = (): Observable<string> =>
            this._orderTagGroupServiceProxy.getFilesOrderTagGroupById(
                this.tagId
            );
        this.isUploadForTag = false;
        (): Observable<void> =>
            this._orderTagGroupServiceProxy.updateFilesOrderTagGroup(
                this.tagId,
                undefined
            );
        this.uploadFileModal.show(this.tagId);
    }

    showUploadImagesForTag(id) {
        this.uploadImageData = (): Observable<string> =>
            this._orderTagGroupServiceProxy.getFilesOrderTagById(id);
        this.isUploadForTag = true;
        this.uploadFileModal.show(id);
    }

    saveFiles(event) {
        if (this.isUploadForTag) {
            this._orderTagGroupServiceProxy
                .updateFilesOrderTag(event.id, event.files)
                .subscribe(() => {
                    this.notify.success(this.l("SavedSuccessfully"));
                });
        } else {
            this._orderTagGroupServiceProxy
                .updateFilesOrderTagGroup(event.id, event.files)
                .subscribe(() => {
                    this.notify.success(this.l("SavedSuccessfully"));
                });
        }
    }
    openLocationPrice(priceId) {
        this._router.navigate([
            "/app/connect/tags/orderTagGroups/location-price",
            this.tagId,
            priceId,
        ]);
    }
}

import { CreateOrUpdateCompanyComponent } from './create-or-update-company.component';
import {
  Component,
  Injector,
  ViewEncapsulation,
  ViewChild,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute } from "@angular/router";
import {
  CompanyServiceProxy,
  BrandDto,
  EntityDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { finalize } from "rxjs/operators";
import { Table, Paginator, LazyLoadEvent } from "primeng";
@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
  
})
export class CompanyComponent extends AppComponentBase {
  @ViewChild("createOrUpdateOrganizationModal", { static: true })
  createOrUpdateOrganizationModal: CreateOrUpdateCompanyComponent;
  @ViewChild("organizationTable", { static: true }) organizationTable: Table;
  @ViewChild("organizationPaginator", { static: true })
  organizationPaginator: Paginator;

  companyEditDto: BrandDto;
  filterText = "";
  isDeleted = false;

  constructor(
    injector: Injector,
    private _companyServiceProxy: CompanyServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _fileDownloadService: FileDownloadService
  ) {
    super(injector);
    this.filterText =
      this._activatedRoute.snapshot.queryParams["filterText"] || "";
    this.companyEditDto = new BrandDto();
  }

  getCompanyList(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.organizationPaginator.changePage(0);
      return;
    }

    this.primengTableHelper.showLoadingIndicator();
    this._companyServiceProxy
      .getAll(
        this.filterText,
        this.isDeleted,
        this.primengTableHelper.getSorting(this.organizationTable),
        this.primengTableHelper.getMaxResultCount(
          this.organizationPaginator,
          event
        ),
        this.primengTableHelper.getSkipCount(
          this.organizationPaginator,
          event
        )
      )
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  deleteOrganization(organization: BrandDto) {
    this.message.confirm(
      "You want to delete Company " + organization.name,
      this.l("AreYouSure"),
      (isConfirmed) => {
        if (isConfirmed) {
          this._companyServiceProxy
            .delete(organization.id)
            .subscribe((result) => {
              this.notify.success(this.l("SuccessfullyDeleted"));
              this.reloadPage();
            });
        }
      }
    );
  }

  activateItem(organization: BrandDto) {
    const body = EntityDto.fromJS({ id: organization.id });
    this._companyServiceProxy.activateItem(body).subscribe((result) => {
      this.notify.success(this.l("Successfully"));
      this.reloadPage();
    });
  }

  craeteOrUpdateOrganization(organizationId?: number) {
    this.createOrUpdateOrganizationModal.show(organizationId);
  }

  exportToExcel() {
    this._companyServiceProxy.getBrandsToExcel().subscribe((result) => {
      this._fileDownloadService.downloadTempFile(result);
    });
  }

  reloadPage(): void {
    this.organizationPaginator.changePage(
      0
    );
  }
}

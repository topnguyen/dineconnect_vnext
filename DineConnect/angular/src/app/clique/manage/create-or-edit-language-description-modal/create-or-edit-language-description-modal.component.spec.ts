import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditLanguageDescriptionModalComponent } from './create-or-edit-language-description-modal.component';

describe('CreateOrEditLanguageDescriptionModalComponent', () => {
  let component: CreateOrEditLanguageDescriptionModalComponent;
  let fixture: ComponentFixture<CreateOrEditLanguageDescriptionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditLanguageDescriptionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditLanguageDescriptionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

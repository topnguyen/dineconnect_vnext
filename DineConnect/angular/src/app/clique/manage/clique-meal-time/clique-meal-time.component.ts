import {
  Component,
  Injector,
  ViewChild,
  OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
  LocationServiceProxy,
  LocationDto,
  EntityDto,
  CliqueMealTimeServiceProxy,
  CliqueMealTimeDto,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";

@Component({
  selector: 'app-clique-meal-time',
  templateUrl: './clique-meal-time.component.html',
  styleUrls: ['./clique-meal-time.component.scss']
})
export class CliqueMealTimeComponent extends AppComponentBase implements OnInit {
  filterText = "";
  isDeleted = false;

  @ViewChild("locationTable", { static: true }) locationTable: Table;
  @ViewChild("locationPaginator", { static: true })
  locationPaginator: Paginator;
  @ViewChild("importModal", { static: true })
  importModal: ImportModalComponent;

  constructor(
    injector: Injector,
    private _locationServiceProxy: LocationServiceProxy,
    private _cliqueMealTimeServiceProxy: CliqueMealTimeServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _fileDownloadService: FileDownloadService,
    private _router: Router
  ) {
    super(injector);
    this.filterText =
      this._activatedRoute.snapshot.queryParams["filterText"] || "";
  }

  ngOnInit() {
    // this.configImport();
  }

  configImport() {
    this.importModal.configure({
      title: this.l("Location"),
      downloadTemplate: () => {
        return this._locationServiceProxy.getTemplateExcelToImport();
      },
      import: (fileToken: string) => {
        return this._locationServiceProxy.importLocationFromExcel(
          fileToken
        );
      },
    });
  }

  getLocationList(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    this._cliqueMealTimeServiceProxy
      .getCliqueMealtime(
        this.filterText,
        this.primengTableHelper.getSorting(this.locationTable),
        this.primengTableHelper.getMaxResultCount(this.locationPaginator,event),
        this.primengTableHelper.getSkipCount(
          this.locationPaginator,
          event
        )
      )
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  deleteLocation(cliqueMealTime: CliqueMealTimeDto) {
    this.message.confirm(
      "you want to delete clique meal time " + cliqueMealTime.name,
      this.l("AreYouSure"),
      (isConfirmed) => {
        if (isConfirmed) {
          this._cliqueMealTimeServiceProxy
            .deleteCliqueMealTime(cliqueMealTime.id)
            .subscribe((result) => {
              this.notify.success(this.l("SuccessfullyDeleted"));
              this.reloadPage();
            });
        }
      }
    );
  }

  activateItem(location: LocationDto) {
    const body = EntityDto.fromJS({ id: location.id });
    this._locationServiceProxy.activateItem(body).subscribe((result) => {
      this.notify.success(this.l("Successfully"));
      this.reloadPage();
    });
  }

  craeteOrUpdateLocation(cliqueMealTime: CliqueMealTimeDto) {
    this._router.navigate([
      "/app/clique/manage/clique-meal-time/create-or-update",
      cliqueMealTime ? cliqueMealTime.id : "null",
    ]);
  }

  reloadPage(): void {
    this.locationPaginator.changePage(this.locationPaginator.getPage());
  }

}

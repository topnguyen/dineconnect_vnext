import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AddressesServiceProxy, LocationBranchEditDto, CreateOrEditLocationTagDto, CompanyServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { 
LocationDto, 
LocationGroupDto, 
LocationTagDto, 
LocationServiceProxy, 
CliqueMealTimeInputDto,
CliqueMealTimeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, Injector } from '@angular/core';

@Component({
  selector: 'createOrUpdateCliqueMealTime',
  styleUrls: ['./create-or-update-clique-meal-time.component.scss'],
  templateUrl: './create-or-update-clique-meal-time.component.html',
  animations: [appModuleAnimation()]
})
export class CreateOrUpdateCliqueMealTimeModalComponent extends AppComponentBase implements OnInit {

  locationEditDto: LocationDto = new LocationDto();
  stateListDropDown = [];
  cityListDropDown = [];
  countryListDropDown = [];
  cityLoaded = false;
  stateLoaded = false;
  saving = false;
  locationBranch = new LocationBranchEditDto();
  requestedLocations: any[] = [];
  companyListDropDown = [];
  cliqueMealTimeInputDto: CliqueMealTimeInputDto = new CliqueMealTimeInputDto();
  constructor(
    injector: Injector,
    private _addressesServiceProxy: AddressesServiceProxy,
    private _router: Router,
    private _locationServiceProxy: LocationServiceProxy,
    private _companyServiceProxy: CompanyServiceProxy,
    private _activeRouter: ActivatedRoute,
    private _cliqueMealTimeServiceProxy: CliqueMealTimeServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit(): void {

    this._activeRouter.params.subscribe((params) => {
        const idCliqueMealTime = +params['id'];
        if (idCliqueMealTime > 0) {
            this.getCliqueMealTimeById(idCliqueMealTime);
        }
    });
  }


  getCityList(countryId: number, stateId: number) {
    this.cityListDropDown = [];
    if (+countryId && +stateId) {
      this._addressesServiceProxy.getCityList(undefined, countryId, stateId, false, undefined, 1000, 0).subscribe((result) => {
        result.items.forEach((element) => {
          let a = { label: element.name, value: element.id };
          this.cityListDropDown.push(a);
        });
        this.cityLoaded = true;
      });
    }
  }

  close() {
    this._router.navigate(['/app/clique/manage/clique-meal-time']);
  }

  save() {
    this.saving = true;
    this._cliqueMealTimeServiceProxy
      .createOrUpdateCliqueMealTime(this.cliqueMealTimeInputDto)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe((result) => {
        this.saving = false;
        this.notify.success(this.l('SuccessfullySaved'));
        this.close();
      });
  }

  getCliqueMealTimeById(cliqueMealTimeId: number) {
    this._cliqueMealTimeServiceProxy.getCliqueMealtimeById(cliqueMealTimeId).subscribe(cliqueMealTime=>{
      this.cliqueMealTimeInputDto = cliqueMealTime;
    })
  }
}

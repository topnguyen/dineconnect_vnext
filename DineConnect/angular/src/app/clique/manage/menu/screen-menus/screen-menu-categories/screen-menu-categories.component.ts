import {
    Component,
    Injector,
    OnInit,
    Renderer2,
    ViewChild, ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ScreenMenuCategoryListDto, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LazyLoadEvent, Paginator, Table } from 'primeng';
import { finalize } from 'rxjs/operators';
import { AddScreenMenuCategoryComponent } from './add-screen-menu-category.component';
import { DragulaService } from 'ng2-dragula';
import moment from 'moment';

@Component({
    templateUrl: './screen-menu-categories.component.html',
    styleUrls: ['./screen-menu-categories.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ScreenMenuCategoriesComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('createModal', { static: true }) createModal: AddScreenMenuCategoryComponent;
    @ViewChild('sortModal', { static: true }) sortModal: ModalDirective;

    ranges = this.createDateRangePickerOptions();
    dateRange: Date[];
    isDateRange: boolean = false;
    // --

    screenMenuId = 0;
    locationId = 0;

    filterInput: {
        text?: string,
    } = {};

    filter: {
        text?: string,
    } = {};

    saving = false;
    categories: ScreenMenuCategoryListDto[] = [];

    constructor(
        injector: Injector,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private render: Renderer2,
    ) {
        super(injector);
        this.filterInput.text = this._activatedRoute.snapshot.queryParams['filterText'] || '';


        // this._dragulaService.createGroup('ScreenMenuCategory', {});
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['id'];
            this.getItems();
        });
        this.locationId = +this._activatedRoute.snapshot.queryParams['location'];
    }

    getItems(event?: LazyLoadEvent) {
        if (!this.screenMenuId || this.screenMenuId < 1) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._screenMenuServiceProxy
            .getCategories(
                this.filter.text,
                this.screenMenuId,
                this.isDateRange,
                this.dateRange ? moment(this.dateRange[0]) : undefined,
                this.dateRange ? moment(this.dateRange[1]) : undefined,
                this.primengTableHelper.getSorting(this.dataTable) || undefined,
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    resetFilter() {
        this.filterInput = {};
        this.filter = {};
        this.paginator.changePage(0);
    }

    search() {
        this.filter = {
            ...this.filter,
            ...this.filterInput
        };
        this.getItems();
    }

    reloadPage() {
        this.paginator.changePage(this.paginator.getPage());
    }

    // --

    editItem(item) {
        this._router.navigateByUrl(['/app/clique/manage/menu/screenmenu', this.screenMenuId, 'screen-menu-categories/create-or-update', item.id].join('/'));
    }

    deleteItem(item) {
        this.message.confirm(this.l('Delete Screen Menu Category') + ' ' + item.name,
            this.l('AreYouSure'),
            isConfirmed => {
                if (isConfirmed) {
                    this._screenMenuServiceProxy
                        .deleteCategory(item.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            this.reloadPage();
                        });
                }
            }
        );
    }

    // --

    showCreateModal() {
        // this.createModal.showModal();
        this._router.navigateByUrl(['/app/clique/manage/menu/screenmenu', this.screenMenuId, 'screen-menu-categories/create-or-update', "null"].join('/'));
 
    }

    showSortModal() {
        this.categories = [];

        this._screenMenuServiceProxy
            .getCategories(
                undefined,
                this.screenMenuId,
                this.isDateRange,
                this.dateRange ? moment(this.dateRange[0]) : undefined,
                this.dateRange ? moment(this.dateRange[1]) : undefined,
                undefined,
                0,
                1000
            )
            .subscribe(result => {
                this.categories = result.items;
            });

        this.sortModal.show();
    }

    closeSortModal() {
        this.saving = false;
        this.sortModal.hide();
    }

    saveSortCategory() {
        const ids = this.categories.map(x => x.id);

        this.saving = true;

        this._screenMenuServiceProxy
            .saveSortCategories(ids)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.closeSortModal();
                this.reloadPage();
            });
    }

    listMenuItem(category: ScreenMenuCategoryListDto) {
        this._router.navigate(['/app/clique/manage/menu/screenmenu', this.screenMenuId, 'screen-menu-categories', category.id, 'screen-menu-menuitems' ], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    back() {
        this._router.navigate(['/app/clique/manage/menu/screenmenu'])
    }

    showedDateRangePicker(): void {
        let button = document.querySelector('bs-daterangepicker-container .bs-datepicker-predefined-btns').lastElementChild;
        let self = this;
        let el = document.querySelector('bs-daterangepicker-container .bs-datepicker-container.ng-trigger-datepickerAnimation');
        self.render.removeClass(el, 'show');

        button.addEventListener('click', (event) => {
            event.preventDefault();

            self.render.addClass(el, 'show');
        });
    }
}

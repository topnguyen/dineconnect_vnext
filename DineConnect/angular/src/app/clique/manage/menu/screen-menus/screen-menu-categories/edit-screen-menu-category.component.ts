import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConnectLookupServiceProxy, ScreenMenuCategoryEditInput, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { cloneDeep } from 'lodash';
import * as moment from 'moment';
import { FileUploader } from 'ng2-file-upload';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'edit-screen-menu-category',
    templateUrl: './edit-screen-menu-category.component.html',
    styleUrls: ['./edit-screen-menu-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EditScreenMenuCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl + '/ImageUploader/UploadImage';
    screenMenuId = 0;
    screenMenuCategoriesId = 0;

    saving = false;
    model = new ScreenMenuCategoryEditInput();

    uploader: FileUploader;
    filePreviewPath: SafeUrl;
    now = new Date();
    keypads = [];
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    schedules: ISchedule[] = [];
    dayOfWeeks = ['Monday', 'Tuesday', 'Wednesday', 'Thurday', 'Friday', 'Saturday', 'Sunday'];
    dayOfMonths = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
        '24',
        '25',
        '26',
        '27',
        '28',
        '29',
        '30',
        '31'
    ];
    schedule: ISchedule = this.getDefaultDate();

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private sanitizer: DomSanitizer,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
        private _connectLookupServiceProxy: ConnectLookupServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.screenMenuId = +params['screenMenuId'];
            this.screenMenuCategoriesId = +params['id'];
            if (!isNaN(this.screenMenuCategoriesId)) {
                this._screenMenuServiceProxy.getCategoryForEdit(this.screenMenuCategoriesId).subscribe((result) => {
                    this.model = result;
                    this.schedules = this.model.screenMenuCategorySchedule ? JSON.parse(this.model.screenMenuCategorySchedule) : [];
                });
            }
        });

        this.getScreenMenuCategoryKeyBoardTypes();

        this.initUpload();
    }

    initUpload() {
        this.uploader = new FileUploader({
            url: this.remoteServiceBaseUrl,
            authToken: 'Bearer ' + this._tokenService.getToken()
        });

        this.uploader.onAfterAddingFile = (f) => {
            if (this.uploader.queue.length > 1) {
                this.uploader.removeFromQueue(this.uploader.queue[0]);
            }
            this.filePreviewPath = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(f._file));
        };

        this.uploader.onSuccessItem = (item, response, status) => {
            const ajaxResponse = <IAjaxResponse>JSON.parse(response);
            if (ajaxResponse.success) {
                this.notify.success(this.l('UploadImageSuccessfully'));
                if (ajaxResponse.result.url != null) {
                    this.model.imagePath = ajaxResponse.result.url;
                }
            } else {
                this.message.error(ajaxResponse.error.message);
            }
        };
    }
    addUploadFile(event){
        this.uploader.addToQueue([event]);
    }

    removeImage(item) {
        item.remove();
    }

    close() {
        window.history.back();
    }

    save() {
        if (!this.isValid || this.saving) {
            return;
        }

        this.saving = true;
        this.model.screenMenuCategorySchedule = JSON.stringify(this.schedules);
        this.model.screenMenuId = +this.screenMenuId; 
        this._screenMenuServiceProxy
            .updateCategory(this.model)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.close();
            });
    }

    addSchedule() {
        if (
            !this.schedule.startDate ||
            !this.schedule.endDate ||
            !this.schedule.startTime ||
            !this.schedule.endTime
        ) {
            return;
        }

        const schedule = cloneDeep(this.schedule);
        //schedule.startTime = moment(schedule.startTime).format('hh:mm');
        //schedule.endTime = moment(schedule.endTime).format('hh:mm');

        this.schedules.push(schedule);
        //this.schedule = {} as ISchedule;
    }

    removeSchedule(index) {
        this.schedules.splice(index, 1);
    }

    get isValid() {
        return this.form.form.valid;
    }

    private getScreenMenuCategoryKeyBoardTypes() {
        this._connectLookupServiceProxy
            .getScreenMenuCategoryKeyBoardTypes()
            .subscribe((result) => {
                this.keypads = result.items;
            });
    }

    private getDefaultDate() : ISchedule {
        return {
            startDate: moment().format(),
            endDate: moment().format(),
            startTime: moment().set({"hour": 7, "minute": 30}).format(),
            endTime: moment().set({"hour": 0, "minute": 30}).format(),
        } as ISchedule;
    }
}

interface ISchedule {
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
    dayOfWeek: string;
    dayOfMonth: string;
}

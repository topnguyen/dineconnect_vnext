import {
    ComboboxItemDto,
    CreateOrUpdateScreenMenuInput,
    ScreenMenuEditDto,
    ScreenMenuServiceProxy
} from './../../../../../shared/service-proxies/service-proxies';
import { CommonLocationGroupDto, DepartmentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { SelectLocationOrGroupComponent } from '@app/shared/common/select-location-or-group/select-location-or-group.component';

@Component({
    selector: 'app-create-or-edit-screen-menu',
    templateUrl: './create-or-edit-screen-menu.component.html',
    styleUrls: ['./create-or-edit-screen-menu.component.scss'],
    animations: [appModuleAnimation()]
})
export class CreateOrEditScreenMenuComponent extends AppComponentBase implements OnInit {
    @ViewChild('form', { static: true }) form: NgForm;

    @ViewChild('selectLocationOrGroup', { static: true }) selectLocationOrGroup: SelectLocationOrGroupComponent;

    screenMenuId = 0;

    screenMenu = new ScreenMenuEditDto();
    locationGroup = new CommonLocationGroupDto();

    departments: ComboboxItemDto[] = [];
    departmentTags: ComboboxItemDto[] = [];

    saving = false;
    savingValue = false;

    get isEdit() {
        return !!this.screenMenuId;
    }

    get formIsValid() {
        return this.form.form.valid;
    }

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _changeDetector: ChangeDetectorRef,
        private _screenMenusServiceProxy: ScreenMenuServiceProxy,
        private _departmentsServiceProxy: DepartmentsServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.screenMenuId = +params['id'];
            this._changeDetector.detectChanges();

            if (this.screenMenuId > 0) {
                this._screenMenusServiceProxy.getForEdit(this.screenMenuId).subscribe((response) => {
                    this.screenMenu = response.screenMenu;
                    this.locationGroup = response.locationGroup;
                    this.departmentTags = response.departments || [];
                });
            }
        });

        this.getComboBoxData();
    }

    getComboBoxData() {
        this._departmentsServiceProxy.getDepartmentsForCombobox().subscribe((r) => {
            this.departments = r.items;
        });
    }

    save() {
        if (!this.formIsValid || this.saving) {
            return;
        }

        this.saving = true;

        const input = new CreateOrUpdateScreenMenuInput();
        input.screenMenu = this.screenMenu;
        input.screenMenu.type = 2;
        input.locationGroup = this.locationGroup;
        input.departments = (this.departmentTags || []).map((x) => new ComboboxItemDto(x));
        console.log("111: ",input);
        
        this._screenMenusServiceProxy
            .createOrUpdateScreenMenu(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    close() {
        this._router.navigate(['/app/clique/manage/menu/screenmenu']);
    }

    clear() {}

    // --

    openSelectLocationOrGroup() {
        this.selectLocationOrGroup.show(this.locationGroup);
    }

    setLocations(event: CommonLocationGroupDto) {
        this.locationGroup = event;
    }

    removeDepartment(value) {
        const index = this.departmentTags.findIndex((x) => x.value === value);
        if (index > -1) {
            this.departmentTags.splice(index, 1);
        }
    }
}

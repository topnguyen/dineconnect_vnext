import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ComboboxItemDto, CreateScreenMenuItemInput, ScreenMenuItemEditDto, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './screen-menu-menuitem-add.component.html',
    styleUrls: ['./screen-menu-menuitem-add.component.scss'],
    animations: [appModuleAnimation()]
})
export class ScreenMenuMenuitemAddComponent extends AppComponentBase implements OnInit {
    @ViewChild('menuItemForm', { static: true }) form: NgForm;

    screenMenuId = 0;
    screenMenuCategoryId = 0;
    locationId = 0;

    saving = false;
    menuItem = new ScreenMenuItemEditDto({fontSize: 14, buttonColor: '#000000'} as any);
    subMenuTags: any[] = [];
    confirmationTypes: ComboboxItemDto[] = [];
    groupOptions = {
        items: [],
        selectedItems: []
    };
    isLoading = false;
    errorMessages = {
        'minlength': 'The tag should be at least 3 characters.',
    };
    tagText = '';

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['screenMenuId'];
            this.screenMenuCategoryId = +params['screenMenuCategoryId'];

            this.getScreenItemsForLocation();
            this.getConfirmationTypes();
        });
        this.locationId = +this._activatedRoute.snapshot.queryParams['location'];
    }

    setSubMenuTags() {
        const tags = [];
        (this.subMenuTags || []).forEach((item) => {
            tags.push(item.value);
        });
        this.menuItem.subMenuTag = tags.join(',');
    }

    save() {
        this.saving = true;
        this.setSubMenuTags();
        const input = CreateScreenMenuItemInput.fromJS({
            categoryId: this.screenMenuCategoryId,
            items: this.groupOptions.selectedItems,
            menuItem: this.menuItem,
        });

        this._screenMenuServiceProxy.createMenuItem(input)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close() {
        window.history.back();
    }

    getConfirmationTypes() {
        this.isLoading = true;
        this._screenMenuServiceProxy.getConfirmationTypes()
            .pipe(finalize(() => {
                this.isLoading = false;
            }))
            .subscribe(result => {
                this.confirmationTypes = result.items || [];
            });
    }

    removeSubMenuTags(index) {
        if (index > -1) {
            this.subMenuTags.splice(index, 1);
        }
    }

    getScreenItemsForLocation() {
        this.isLoading = true;
        this._screenMenuServiceProxy.getScreenItemsForLocation(
            this.locationId || undefined,
            this.screenMenuId,
            this.screenMenuCategoryId,
            undefined,
            undefined,
            undefined,
            'name ASC',
            1000,
            0,
        )
            .pipe(finalize(() => {
                this.isLoading = false;
            }))
            .subscribe(result => {
                this.groupOptions.selectedItems = result.selectedItems;
                this.groupOptions.items = result.items;
            });
    }

    minlength(control: FormControl) {
        if (control.value.length < 3) {
            return {
                'minlength': true
            };
        }

        return null;
    }

    onTextChange(event) {
        this.tagText = event;
    }

    get formIsValid() {
        return this.form.form.valid && (!this.tagText || (this.tagText.length >= 3));
    }

}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

import { appModuleAnimation } from '@shared/animations/routerTransition';

import { AppComponentBase } from '@shared/common/app-component-base';
import { AddScreenMenuCategoryComponent } from '../screen-menu-categories/add-screen-menu-category.component';

import { ComboboxItemDto, LocationGroupDto, ScreenMenuCategoryListDto, ScreenMenuServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './screen-menu-virtualscreen.component.html',
    styleUrls: ['./screen-menu-virtualscreen.component.scss'],
    animations: [appModuleAnimation()]
})
export class ScreenMenuVirtualscreenComponent extends AppComponentBase implements OnInit {
    @ViewChild('createModal', { static: true }) createModal: AddScreenMenuCategoryComponent;
    @ViewChild('sortModal', { static: true }) sortModal: ModalDirective;

    locationId = 0;
    screenMenuId = 0;
    screenMenu: any;
    requestdepartments: ComboboxItemDto[];
    locationGroup: LocationGroupDto;
    cateWidth: any;
    categories: ScreenMenuCategoryListDto[];
    categoriesToSort: ScreenMenuCategoryListDto[];

    subMenuItems = [];
    categoryTags = [];
    currentTag = '';
    backButtonTag = '';
    backButtonCaption = '';
    currentCate: ScreenMenuCategoryListDto = null;

    loadingMenu = false;
    menuWidth = '';
    menuHeight = '';
    menuWrapText = false;
    saving = false;
    loading = false;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _screenMenuServiceProxy: ScreenMenuServiceProxy
    ) {
        super(injector);
    }

    reloadPage() {
        this.getScreenMenuForEdit();
        this.getCategories();
    }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(params => {
            this.screenMenuId = +params['id'];

            this.getScreenMenuForEdit();
            this.getCategories();
        });
        this.locationId = +this._activatedRoute.snapshot.queryParams['location'];
    }

    getScreenMenuForEdit() {
        this.loading = true;
        this._screenMenuServiceProxy.getScreenMenuForEdit(
            this.screenMenuId,
            undefined,
            undefined,
            undefined,
            undefined
        )
        .pipe(finalize(() => {
            this.loading = false;
        }))
        .subscribe((result) => {
            this.screenMenu = result.screenMenu;
            this.requestdepartments = result.departments;
            this.locationGroup = result.locationGroup;

            const colCount = this.screenMenu.categoryColumnCount === 0 ? 1 : this.screenMenu.categoryColumnCount;
            this.cateWidth = 100 / colCount + '%';
        });
    }

    getCateName(item) {
        return (item.name || '').replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>");
    }

    getItemName(name) {
        name = (name || '').replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>");
        if (this.menuWrapText) {
            return (name || '').replace(" ", "<br/>");
        }

        return name;
    }

    getTagName(name) {
        name = (name || '').split(',').pop().trim();
        return name;
    }

    getCategories() {
        this.loading = true;
        this._screenMenuServiceProxy.getCategories(
            undefined,
            this.screenMenuId,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        )
        .pipe(finalize(() => {
            this.loading = false;
        })).subscribe(result => {
            this.categories = result.items;
        });
    }

    loadMenuItem(cate: ScreenMenuCategoryListDto) {
        const colCount = cate.columnCount === 0 ? 3 : cate.columnCount;

        this.menuWidth = 100 / colCount + '%';
        this.menuHeight = cate.menuItemButtonHeight + 'px';
        this.menuWrapText = cate.wrapText;

        this.currentCate = cate;
        this.getMenuItemsForTag('');
    }

    getMenuItemsForTag(tag: string) {
        this.loadingMenu = true;
        this._screenMenuServiceProxy.getMenuItemForVisualScreen(
            undefined,
            undefined,
            this.currentCate.id,
            undefined,
            undefined,
            tag,
            undefined,
            1000,
            0
        )
        .pipe(finalize(() => {
            this.loadingMenu = false;
        }))
        .subscribe(result => {
            this.subMenuItems = result.menuItems;
            this.categoryTags = result.categoryTags;

            this.currentTag = tag;
            this.backButtonTag = tag.replace(tag.split(',').pop(), '').slice(0, -1).trim();
            this.backButtonCaption = this.backButtonTag.split(',').pop().trim();
        });
    }

    editCategory(category: ScreenMenuCategoryListDto, event) {
        event.stopPropagation();
        this._router.navigate(['/app/clique/manage/menu/screenmenu', this.screenMenuId, 'screen-menu-categories', 'create-or-update', category.id], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    showCreateModal() {
        this.createModal.showModal();
    }

    createMenuItem(category: ScreenMenuCategoryListDto, event: MouseEvent) {
        event.stopPropagation();
        this._router.navigate(['/app/clique/manage/menu/screenmenu', this.screenMenuId, 'screen-menu-categories', category.id, 'screen-menu-menuitems', 'create-or-update','null'], {
            queryParams: {
                location: this.locationId
            }
        });
    }

    showSortModal() {
        this.categoriesToSort = [];

        this._screenMenuServiceProxy
            .getCategories(
                undefined,
                this.screenMenuId,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            )
            .subscribe(result => {
                this.categoriesToSort = result.items;
            });

        this.sortModal.show();
    }

    closeSortModal() {
        this.sortModal.hide();
    }

    saveSortCategory() {
        const ids = this.categoriesToSort.map(x => x.id);
        this.saving = true;

        this._screenMenuServiceProxy
            .saveSortCategories(ids)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.closeSortModal();
                this.reloadPage();
            });
    }

}

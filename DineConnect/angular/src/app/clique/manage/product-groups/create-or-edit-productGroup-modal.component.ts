﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { ProductGroupsServiceProxy, CreateOrEditProductGroupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { Table, Paginator } from 'primeng';

@Component({
    selector: 'createOrEditProductGroupModal',
    templateUrl: './create-or-edit-productGroup-modal.component.html'
})
export class CreateOrEditProductGroupModalComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    productGroup: CreateOrEditProductGroupDto = new CreateOrEditProductGroupDto();
    filterText = '';

    parentId = undefined;

    constructor(
        injector: Injector,
        private _productGroupsServiceProxy: ProductGroupsServiceProxy
    ) {
        super(injector);
    }

    show(productGroupId?: number, parentId?: number): void {
        this.parentId = parentId;

        if (!productGroupId) {
            this.productGroup = new CreateOrEditProductGroupDto();
            this.productGroup.id = productGroupId;

            this.active = true;
            this.modal.show();
        } else {
            this._productGroupsServiceProxy.getProductGroupForEdit(productGroupId).subscribe(result => {
                this.productGroup = result.productGroup;
                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;

        this.productGroup.parentId = this.parentId;
        this._productGroupsServiceProxy.createOrEdit(this.productGroup)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }




    getProductGroups(event?) { }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

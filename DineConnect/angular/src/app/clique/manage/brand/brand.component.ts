import {
  Component,
  Injector,
  ViewChild,
  OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
  LocationServiceProxy,
  LocationDto,
  EntityDto,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent extends AppComponentBase implements OnInit {
  filterText = "";
  isDeleted = false;

  @ViewChild("locationTable", { static: true }) locationTable: Table;
  @ViewChild("locationPaginator", { static: true })
  locationPaginator: Paginator;
  @ViewChild("importModal", { static: true })
  importModal: ImportModalComponent;

  constructor(
    injector: Injector,
    private _locationServiceProxy: LocationServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _fileDownloadService: FileDownloadService,
    private _router: Router
  ) {
    super(injector);
    this.filterText =
      this._activatedRoute.snapshot.queryParams["filterText"] || "";
  }

  ngOnInit() {
    this.configImport();
  }

  configImport() {
    this.importModal.configure({
      title: this.l("Location"),
      downloadTemplate: () => {
        return this._locationServiceProxy.getTemplateExcelToImport();
      },
      import: (fileToken: string) => {
        return this._locationServiceProxy.importLocationFromExcel(
          fileToken
        );
      },
    });
  }

  getLocationList(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    this._locationServiceProxy
      .getList(
        this.filterText,
        this.isDeleted,
        0,
        this.primengTableHelper.getSorting(this.locationTable),
        this.primengTableHelper.getMaxResultCount(
          this.locationPaginator,
          event
        ),
        this.primengTableHelper.getSkipCount(
          this.locationPaginator,
          event
        )
      )
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  deleteLocation(location: LocationDto) {
    this.message.confirm(
      "you want to delete location " + location.name,
      this.l("AreYouSure"),
      (isConfirmed) => {
        if (isConfirmed) {
          this._locationServiceProxy
            .delete(location.id)
            .subscribe((result) => {
              this.notify.success(this.l("SuccessfullyDeleted"));
              this.reloadPage();
            });
        }
      }
    );
  }

  activateItem(location: LocationDto) {
    const body = EntityDto.fromJS({ id: location.id });
    this._locationServiceProxy.activateItem(body).subscribe((result) => {
      this.notify.success(this.l("Successfully"));
      this.reloadPage();
    });
  }

  craeteOrUpdateLocation(location: LocationDto) {
    this._router.navigate([
      "/app/clique/manage/brand/create-or-update",
      location ? location.id : "null",
    ]);
  }

  exportToExcel() {
    this._locationServiceProxy.getAllToExcel().subscribe((result) => {
      this._fileDownloadService.downloadTempFile(result);
    });
  }

  showImport() {
    this.importModal.show();
  }

  onModalSave() {
    setTimeout(() => {
      this.getLocationList();
    }, 3000);
  }

  reloadPage(): void {
    this.locationPaginator.changePage(this.locationPaginator.getPage());
  }
}

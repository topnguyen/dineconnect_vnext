import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AddressesServiceProxy, LocationBranchEditDto, CreateOrEditLocationTagDto, CompanyServiceProxy } from './../../../../shared/service-proxies/service-proxies';
import { LocationDto, LocationGroupDto, LocationTagDto, LocationServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, Injector } from '@angular/core';

@Component({
    selector: 'createOrUpdateBrand',
    styleUrls: ['./create-or-update-brand.component.scss'],
    templateUrl: './create-or-update-brand.component.html',
    animations: [appModuleAnimation()]
})
export class CreateOrUpdateBrandModalComponent extends AppComponentBase implements OnInit {
    locationEditDto: LocationDto = new LocationDto();
    stateListDropDown = [];
    cityListDropDown = [];
    countryListDropDown = [];
    cityLoaded = false;
    stateLoaded = false;
    saving = false;
    locationBranch = new LocationBranchEditDto();
    requestedLocations: any[] = [];
    companyListDropDown = [];
    id: number;
    constructor(
      injector: Injector,
      private _addressesServiceProxy: AddressesServiceProxy,
      private _router: Router,
      private _locationServiceProxy: LocationServiceProxy,
      private _companyServiceProxy: CompanyServiceProxy,
      private _activatedRoute: ActivatedRoute
    ) {
      super(injector);
    }
  
    ngOnInit(): void {
      this.getCompanyList();
      this.getCountryList();

        this._activatedRoute.params.subscribe((params) => {
            this.id = +params['id']; // (+) converts string 'id' to a number
            if (!isNaN(this.id)) {
                this._locationServiceProxy.get(this.id).subscribe((result) => {
                    this.locationEditDto = result;
                    this.locationEditDto.countryId = this.locationEditDto.countryId || undefined;
                    this.locationEditDto.stateId = this.locationEditDto.stateId || undefined;
                    this.locationEditDto.cityId = this.locationEditDto.cityId || undefined;
                    this.locationEditDto.schedules = this.locationEditDto.schedules || [];
                    this.locationBranch = new LocationBranchEditDto(this.locationEditDto.branch);
                    this.getStateList(this.locationEditDto.countryId);
                    this.getCityList(this.locationEditDto.countryId, this.locationEditDto.stateId);
                });
            } else {
                this.locationEditDto = new LocationDto();
            }
        });
    }
  
    setCountry() {
      this.locationEditDto.stateId = undefined;
      this.locationEditDto.cityId = undefined;
      this.getStateList(this.locationEditDto.countryId);
    }
  
    getStateList(countryId: number) {
      this.stateListDropDown = [];
      this.cityListDropDown = [];
      if (+this.locationEditDto.countryId) {
        this._addressesServiceProxy.getStateListByCountry(countryId).subscribe((result) => {
          result.forEach((element) => {
            let a = { label: element.name, value: element.id };
            this.stateListDropDown.push(a);
          });
  
          this.stateLoaded = true;
        });
      }
    }
  
    getCountryList() {
      this.countryListDropDown = [];
  
      this._addressesServiceProxy.getCountryList(undefined, false, undefined, 1000, 0).subscribe((result) => {
        result.items.forEach((element) => {
          let a = { label: element.name, value: element.id };
          this.countryListDropDown.push(a);
        });
      });
    }
  
    setState() {
      this.locationEditDto.cityId = undefined;
      this.getCityList(this.locationEditDto.countryId, this.locationEditDto.stateId);
    }
  
    getCityList(countryId: number, stateId: number) {
      this.cityListDropDown = [];
      if (+countryId && +stateId) {
        this._addressesServiceProxy.getCityList(undefined, countryId, stateId, false, undefined, 1000, 0).subscribe((result) => {
          result.items.forEach((element) => {
            let a = { label: element.name, value: element.id };
            this.cityListDropDown.push(a);
          });
          this.cityLoaded = true;
        });
      }
    }
    
    close() {
      this._router.navigate(['/app/clique/manage/brand']);
    }

    save() {
      this.saving = true;
      this.locationEditDto.branch = this.locationBranch;
      this.locationEditDto.requestedLocations = JSON.stringify(this.requestedLocations);

      if (this.locationEditDto.locationGroups) {
          this.locationEditDto.locationGroups = this.locationEditDto.locationGroups.map(item => {
              return new LocationGroupDto(item);
          })
      }

      if (this.locationEditDto.locationTags) {
          this.locationEditDto.locationTags = this.locationEditDto.locationTags.map(item => {
              let tmp = new LocationTagDto();
              tmp.init( new CreateOrEditLocationTagDto(item))
              return tmp;
          })
      }

      this._locationServiceProxy
          .createOrUpdte(this.locationEditDto)
          .pipe(
              finalize(() => {
                  this.saving = false;
              })
          )
          .subscribe((result) => {
              this.saving = false;
              this.notify.success(this.l('SuccessfullySaved'));
              this.close();
          });
  }
  getCompanyList() {
    this._companyServiceProxy.getAll(undefined, false, undefined, 1000, 0).subscribe((result) => {
        this.companyListDropDown = [];
        result.items.forEach((element) => {
            let a = { label: element.name, value: element.id };
            this.companyListDropDown.push(a);
        });
    });
}
}

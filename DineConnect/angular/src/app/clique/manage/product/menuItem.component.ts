import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild,
    OnInit,
} from "@angular/core";

import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
    MenuItemServiceProxy,
    LocationDto,
    CategoryServiceProxy,
    ComboboxItemDto,
    EntityDto,
    CommonLocationGroupDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";

@Component({
    templateUrl: "./menuItem.component.html",
    styleUrls: ["./menuItem.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class MenuItemComponent extends AppComponentBase implements OnInit {
    @ViewChild("menuItemTable", { static: true }) menuItemTable: Table;
    @ViewChild("menuItemPaginator", { static: true })
    menuItemPaginator: Paginator;
    @ViewChild("importModal", { static: true })
    importModal: ImportModalComponent;
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    notAssigned = {
        value: undefined,
        displayText: this.l("NotAssigned"),
    } as ComboboxItemDto;

    filterText = "";
    isDeleted = false;
    locationOrGroup: CommonLocationGroupDto;

    categoriesDropDown: ComboboxItemDto[] = [];
    selectedCategory: ComboboxItemDto = this.notAssigned;
    productTypesDropDown: ComboboxItemDto[] = [];
    selectedProductType: ComboboxItemDto = this.notAssigned;

    constructor(
        injector: Injector,
        private _router: Router,
        private _menuitemServiceProxy: MenuItemServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.filterText =
            this._activatedRoute.snapshot.queryParams["filterText"] || "";
    }

    ngOnInit(): void {
        this.configImport();
        this.getDataForComboBox();
    }

    configImport() {
        this.importModal.configure({
            title: this.l("MenuItem"),
            downloadTemplate: (id?: number) => {
                return this._menuitemServiceProxy.getImportMenuItemTemplateToExcel();
            },
            import: (fileToken: string) => {
                return this._menuitemServiceProxy.importMenuItemToDatabase(
                    fileToken
                );
            },
            templateUrl: "/assets/sampleFiles/ImportMenuItemTemplate.xlsx",
        });
    }

    getDataForComboBox() {
        this._categoryServiceProxy
            .getCategoriesForCombobox()
            .subscribe((result) => {
                this.categoriesDropDown = result.items;
                this.categoriesDropDown.unshift(this.notAssigned);
            });

        this._menuitemServiceProxy.getProductTypes().subscribe((result) => {
            this.productTypesDropDown = result.items;
            this.productTypesDropDown.unshift(this.notAssigned);
        });
    }

    reset() {
        this.filterText = "";
        this.isDeleted = false;
        this.selectedProductType = this.notAssigned;
        this.locationOrGroup = undefined;
        this.selectedCategory = this.notAssigned;

        this.getMenuItemList();
    }

    getMenuItemList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.menuItemPaginator.changePage(0);

            return;
        }

        const locationOrGroup =
            this.locationOrGroup ?? new CommonLocationGroupDto();
        const locationGroup = locationOrGroup.group;
        const locationTag = locationOrGroup.locationTag;
        const nonLocations = !!locationOrGroup.nonLocations?.length;
        const locationIds = this._getLocationIds(locationOrGroup);

        this.primengTableHelper.showLoadingIndicator();
        this._menuitemServiceProxy
            .getAll(
                this.filterText,
                this.isDeleted,
                this.selectedCategory.value as any,
                this.selectedProductType.value as any,
                locationIds,
                locationGroup,
                locationTag,
                nonLocations,
                this.primengTableHelper.getSorting(this.menuItemTable),
                this.primengTableHelper.getSkipCount(
                    this.menuItemPaginator,
                    event
                ),
                this.primengTableHelper.getMaxResultCount(
                    this.menuItemPaginator,
                    event
                )
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    deleteMenuItem(menuItem) {
        this.message.confirm(
            "you want to delete menu Item " + menuItem.name,
            this.l("AreYouSure"),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._menuitemServiceProxy
                        .delete(menuItem.id)
                        .subscribe(() => {
                            this.notify.success(this.l("SuccessfullyDeleted"));
                            this.getMenuItemList();
                        });
                }
            }
        );
    }

    activateItem(menuItem) {
        const body = EntityDto.fromJS({ id: menuItem.id });
        this._menuitemServiceProxy.activateItem(body).subscribe((result) => {
            this.notify.success(this.l("Successfully"));
            this.getMenuItemList();
        });
    }

    craeteOrUpdateMenuItem(id?: number) {
        this._router.navigate([
            "/app/clique/manage/product/product/create-or-update",
            id ? id : "null",
        ]);
    }

    exportToExcel(): void {
        this._menuitemServiceProxy
            .getAllMenuItemToExcel()
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    import() {
        this.importModal.show();
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.locationOrGroup);
    }

    selectLocations(event) {
        this.locationOrGroup = event;
    }

    private _getLocationIds(locationOrGroup: CommonLocationGroupDto) {
        let locationIds = "";

        if (locationOrGroup.group) {
            locationIds = locationOrGroup.groups.map((x) => x.id).join(",");
        } else if (locationOrGroup.locationTag) {
            locationIds = locationOrGroup.locationTags
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.nonLocations?.length) {
            locationIds = locationOrGroup.nonLocations
                .map((x) => x.id)
                .join(",");
        } else if (locationOrGroup.locations?.length) {
            locationIds = locationOrGroup.locations.map((x) => x.id).join(",");
        }
        return locationIds;
    }
}

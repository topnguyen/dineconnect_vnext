import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SelectItemModalComponent } from "@app/connect/discount/select-item-modal/select-item-modal.component";
import { ImportModalComponent } from "@app/connect/shared/import-modal/import-modal.component";
import { SelectLocationOrGroupComponent } from "@app/shared/common/select-location-or-group/select-location-or-group.component";
import { SelectPopupComponent } from "@app/shared/common/select-popup/select-popup.component";
import { SelectSingleLocationComponent } from "@app/shared/common/select-single-location/select-single-location.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    CategoryServiceProxy,
    ComboboxItemDto,
    CommonLocationGroupDto,
    CreateEditProductComboItemDto,
    CreateOrUpdateMenuItemInput,
    DinePlanTaxesServiceProxy,
    LocationDto,
    LocationServiceProxy,
    MenuBarCodeEditDto,
    MenuItemEditDto,
    MenuItemLocationPriceDto,
    MenuItemPortionDto,
    MenuItemPortionEditDto,
    MenuItemServiceProxy,
    PagedResultDtoOfCategoryDto,
    PagedResultDtoOfProductComboGroupEditDto,
    ProductComboGroupEditDto,
    ProductComboGroupServiceProxy,
    PromotionsServiceProxy,
    UpMenuItemEditDto,
} from "@shared/service-proxies/service-proxies";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { TokenService } from "abp-ng2-module";
import * as _ from "lodash";
import * as moment from "moment";
import { FileUploader } from "ng2-file-upload";
import { Dropdown } from "primeng";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { CreateOrEditLanguageDescriptionModalComponent } from "../create-or-edit-language-description-modal/create-or-edit-language-description-modal.component";
@Component({
    selector: "app-create-or-edit-menuitem",
    templateUrl: "./create-or-edit-menuitem.component.html",
    styleUrls: ["./create-or-edit-menuitem.component.scss"],
    animations: [appModuleAnimation()],
})
export class CreateOrEditMenuitemComponent
    extends AppComponentBase
    implements OnInit {
    @ViewChild("selectLocationOrGroup", { static: true })
    selectLocationOrGroup: SelectLocationOrGroupComponent;
    @ViewChild("selectSingleLocationModal", { static: true })
    selectSingleLocationModal: SelectSingleLocationComponent;
    @ViewChild("selectItemModal", { static: true })
    selectItemModal: SelectItemModalComponent;
    @ViewChild("importDescriptionsModal", { static: true })
    importDescriptionsModal: ImportModalComponent;
    @ViewChild("selectProductComboGroupPopupModal", { static: true })
    selectProductComboGroupPopupModal: SelectPopupComponent;
    @ViewChild("languageDescriptionModal", { static: true })
    languageDescriptionModal: CreateOrEditLanguageDescriptionModalComponent;
    @ViewChild('selectCategoryPopupModal', { static: true })
    selectCategoryPopupModal: SelectPopupComponent;

    saving = false;
    categories = [];
    productTypes = [];
    foodTypes = [];
    transactionTypes = [];
    allComboItems = [];
    locationPrices = [];
    locationList: LocationDto[];
    menuItemDto = new MenuItemEditDto();
    selectedLocationList = [];
    combos = [];
    comboItems = [];
    languages = [];
    tags: Array<{ displayValue?: string }> = [];
    id: number;
    sub: any;
    days = [];
    timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    startTime = moment("12:00", "HH:mm").tz(this.timezone, true).toDate();
    endTime = moment("17:00", "HH:mm").tz(this.timezone, true).toDate();
    dayOfWeek = [];
    monthSchedules = [];
    isDisableSave = false;
    selectedBarcodes = [];
    barcode = "";
    showComboItems = false;
    selectedComboIndex = 0;
    selectedComboGroupIndex = undefined;
    selectedComboItemIndex = undefined;
    gstenabledforIndia = false;
    dinePlanTaxes: ComboboxItemDto[] = [];
    selectedDinePlanTax: ComboboxItemDto;
    taxDetail = {
        dinePlanTaxId: 0,
        isChangeValue: false,
        taxValue: null,
    };
    menuItemTaxDetails: any[] = [];
    uploader: FileUploader;
    nutritionInfo = {
        servingSize: 0,
        calories: 0,
        caloriesFromFat: 0,
        totalFat: 0,
        totalFatPercent: 0,
        saturatedFat: 0,
        saturatedFatPercent: 0,
        transFat: 0,
        transFatPercent: 0,
        cholesterol: 0,
        cholesterolPercent: 0,
        sodium: 0,
        sodiumPercent: 0,
        totalCarbonhydrate: 0,
        totalCarbonhydratePercent: 0,
        dietaryFiber: 0,
        dietaryFiberPercent: 0,
        sugars: 0,
        sugarsPercent: 0,
        protein: 0,
        proteinPercent: 0,
        vitaminA: 0,
        vitaminAPercent: 0,
        vitaminC: 0,
        vitaminCPercent: 0,
        calcium: 0,
        calciumPercent: 0,
        iron: 0,
        ironPercent: 0,
    };
    selectedCategory : any;
    selectedUpItem: any;

    get isEdit() {
        return !!this.menuItemDto?.id;
    }
    allergyList = Object.values(Allergy);
    categoryList: any;
    allergicList = [];
    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _locationServiceProxy: LocationServiceProxy,
        private _categoryServiceProxy: CategoryServiceProxy,
        private _promotionsServiceProxy: PromotionsServiceProxy,
        private _menuItemServiceProxy: MenuItemServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _dinePlanTaxesServiceProxy: DinePlanTaxesServiceProxy,
        private _productComboGroupServiceProxy: ProductComboGroupServiceProxy,
        private _tokenService: TokenService,
        private _categoryService: CategoryServiceProxy
    ) {
        super(injector);
    }

    show(): void {
        this.getLocationList();
    }

    ngOnInit() {
        this.sub = this._activatedRoute.params.subscribe((params) => {
            this.id = +params["id"]; // (+) converts string 'id' to a number

            if (isNaN(this.id)) {
                this.id = undefined;
            }
            this.getMenuItem();
        });

        this.getDataForCombobox();
        this.getCategorySelection();
    }

    save() {
        this.saving = true;
        let input = new CreateOrUpdateMenuItemInput();
        input.menuItem = this.menuItemDto;
        input.productType = this.menuItemDto.productType;
        input.menuItemLocationPrices = this.locationPrices;
        input.combos = this.combos;
        if(this.allergicList?.length > 0) {
            input.menuItem.allergicInfo = this.allergicList.toString();
        }
        input.menuItem.tag = this.tags.map((x) => x.displayValue).join("\n");
        (input.menuItem.nutritionInfo = JSON.stringify(this.nutritionInfo)),
        // this.menuItemDto.locationList = this.selectedLocationList;
        this._menuItemServiceProxy
            .createOrUpdateMenuItem(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((result) => {
                this.notify.success(this.l("SavedSuccessfully"));
                this.close();
            });
    }

    close(): void {
        this._router.navigate(["/app/clique/manage/product/product"]);
    }

    getMenuItem() {
        this._menuItemServiceProxy
            .getMenuItemForEdit(this.id)
            .subscribe((result) => {
                this.menuItemDto = result.menuItem;
                this.combos = result.combos;
                if (!this.menuItemDto.locationGroup) {
                    this.menuItemDto.locationGroup =
                        new CommonLocationGroupDto();
                }
                this.isDisableSave = this.menuItemDto.portions.length <= 0;
                this.getDays();
                this.getSelectedCatetory(this.menuItemDto.categoryId);
                if (this.menuItemDto.tag) {
                    this.tags = this.menuItemDto.tag
                        .split("\n")
                        .map((x) => ({ displayValue: x }));
                }

                if (this.menuItemDto.nutritionInfo) {
                    this.nutritionInfo = JSON.parse(
                        result.menuItem.nutritionInfo
                    );
                }
                this.allergicList = this.menuItemDto?.allergicInfo?.split(',');
                if (!this.allergicList) {
                    this.allergicList = [];
                }
            });
    }

    getDataForCombobox() {
        this.languages = _.filter(
            abp.localization.languages,
            (l) => (<any>l).isDisabled === false
        );

        this._categoryServiceProxy
            .getCategoriesForCombobox()
            .subscribe((result) => {
                this.categories = result.items;
            });

        this._menuItemServiceProxy.getProductTypes().subscribe((result) => {
            this.productTypes = result.items;
        });

        this._menuItemServiceProxy.getFoodTypes().subscribe((result) => {
            this.foodTypes = result.items;
        });

        this._menuItemServiceProxy
            .getTransactionTypesForCombobox()
            .subscribe((result) => {
                this.transactionTypes = result.items;
            });

        this._menuItemServiceProxy.getComboItems().subscribe((result) => {
            this.allComboItems = result.items;
        });

        this._menuItemServiceProxy
            .getLocationPricesForMenu("", undefined, this.id, 0, 1, "")
            .subscribe((result) => {
                this.locationPrices = result;
            });

        this.getDinePlanTaxes();
    }

    private getDays() {
        this._promotionsServiceProxy
            .getDaysForLookupTable()
            .subscribe((result) => {
                this.days = result.items;
            });
    }

    private getSelectedCatetory(categoryId: number) {
        if(categoryId){
        this._categoryService
            .getCategoryForEdit(categoryId)
            .subscribe((result) => {
                this.selectedCategory = result;
            });
        }
    }

    getLocationList() {
        this.primengTableHelper.showLoadingIndicator();
        this._locationServiceProxy
            .getList(undefined, false, 0, undefined, 1000, 0)
            .subscribe((result) => {
                this.locationList = result.items;
            });
    }

    addPortion() {
        this.menuItemDto.portions.push(
            Object.assign(new MenuItemPortionEditDto(), {
                id: 0,
                name: "NORMAL",
                multiPlier: 1,
                price: 0,
                barcode: null,
            })
        );
        this.isDisableSave = this.menuItemDto.portions.length <= 0;
    }

    removePortion(index) {
        this.menuItemDto.portions.splice(index, 1);
        this.isDisableSave = this.menuItemDto.portions.length <= 0;
    }

    addUpItems() {
        this.menuItemDto.upSingleMenuItems.push(
            Object.assign(new UpMenuItemEditDto(), {
                id: 0,
                refMenuItemId: 0,
                name: "",
                addBaseProductPrice: false,
            })
        );
    }

    upItemSelected(item) {
        this.selectedUpItem.refMenuItemId = item.menuItemId;
        this.selectedUpItem.name = item.name;
    }

    removeUpItem(productIndex) {
        this.menuItemDto.upSingleMenuItems.splice(productIndex, 1);
    }

    addUpCombo() {
        this.menuItemDto.upMenuItems.push(
            Object.assign(new UpMenuItemEditDto(), {
                id: 0,
                refMenuItemId: 0,
                name: "",
                addBaseProductPrice: false,
                addQuantity: 1,
            })
        );
    }

    removeUpCombo(productIndex) {
        this.menuItemDto.upMenuItems.splice(productIndex, 1);
    }

    addBarCode() {
        let code = Object.assign(new MenuBarCodeEditDto(), {
            id: 0,
            barcode: this.barcode,
        });
        let idx = -1;
        this.menuItemDto.barcodes.map((item, index) => {
            if (item.barcode === this.barcode) {
                idx = index;
            }
        });

        if (idx >= 0) {
            this.notify.info(this.l("AlreadyLocalExists_F", this.barcode));
            return;
        } else {
            this._menuItemServiceProxy
                .isBarcodeExists(this.barcode)
                .pipe(finalize(() => { }))
                .subscribe((result) => {
                    if (!result) {
                        this.menuItemDto.barcodes.push(code);
                        this.barcode = "";
                    } else {
                        this.notify.info(
                            this.l("AlreadyExists_F", this.barcode)
                        );
                    }
                });
        }
    }

    deleteBarCode() {
        this.selectedBarcodes.map((item) => {
            let idx = this.menuItemDto.barcodes.indexOf(item);
            this.menuItemDto.barcodes.splice(idx, 1);
        });
    }

    readBarCodes() { }

    openforMenuItem(promotionItem) {
        this.selectedUpItem = promotionItem;
        let menuItems = this.menuItemDto.upSingleMenuItems.map((x) => {return {menuItemId: x.refMenuItemId};});
        this.selectItemModal.show(menuItems, true, 1);
    }

    addComboGroup() {
        var newCombo = new ProductComboGroupEditDto();
        newCombo.minimum = newCombo.maximum = 1;
        var maxOrder = this.getMaxOrder(this.combos, o => o.sortOrder) ;
        newCombo.sortOrder = maxOrder + 1;
        this.addorEditGroup(newCombo);
    }

    editComboGroup(index, data) {
        this.selectedComboGroupIndex = index;
        this.addorEditGroup(data);
    }

    deleteComboGroup(productIndex) {
        this.combos.splice(productIndex, 1);
    }

    listComboItems(productIndex, data) {
        var $this = this;
        $this.selectedComboIndex = productIndex;
        if(!data.id){
            this.updateSelectedCombo(productIndex);
            return;
        }
        this._productComboGroupServiceProxy
            .getForEdit(data.id)
            .subscribe((resp) => {
                $this.combos[productIndex].comboItems = resp.comboItems;
                this.updateSelectedCombo(productIndex);
            });
    }

    private updateSelectedCombo(productIndex: number) {
        if (!this.combos[productIndex].comboItems) {
            this.combos[productIndex].comboItems = [];
        }
        this.comboItems = this.combos[productIndex].comboItems;
        this.showComboItems = true;
    }

    addComboItem(productIndex, data) {
        this.listComboItems(productIndex, data);
        var newComboItem = new CreateEditProductComboItemDto();
        var maxOrder = this.getMaxOrder(this.comboItems, o => o.sortOrder);
        newComboItem.sortOrder = maxOrder + 1;
        newComboItem.count = 1;
        newComboItem.price = 0;
        newComboItem.buttonColor = '#E6E784';
        this.addorEditItem(newComboItem);
    }

    private getMaxOrder(datas: any, callbackFn: (value: any) => any ): number {
        var maxOrder = Math.max.apply(Math, datas.map(callbackFn));
        maxOrder = Number.isFinite(maxOrder) ? maxOrder: -1;
        return maxOrder;
    }

    editComboItem(productIndex, data) {
        this.selectedComboItemIndex = productIndex;
        // this.addorEditItem(data);
    }

    deleteComboItem(productIndex) {
        this.comboItems.splice(productIndex, 1);
    }

    addorEditGroup(data?) {
        // this.createComboGroupModal.show(data);
    }

    addorEditItem(data?) {
        // this.createComboItemModal.show(data);
    }

    openSelectLocationModal() {
        this.selectLocationOrGroup.show(this.menuItemDto.locationGroup);
    }

    getLocations(event) {
        this.menuItemDto.locationGroup = event;
    }

    addLocationPrices() {
        this.locationPrices.push(
            Object.assign(new MenuItemLocationPriceDto(), {
                locationId: 0,
                menuItemId: this.id,
            })
        );
    }

    removeLocationPrice(index) {
        this.locationPrices.splice(index, 1);
    }

    openSelectSingleLocation(locationPrice) {
        this.selectSingleLocationModal.location = locationPrice;
        let excludedLocationIds = this.locationPrices.map(x => x.locationId);
        this.selectSingleLocationModal.show(excludedLocationIds);
    }

    updateLocationPrices(event) {
        let locationPrice = Object.assign(
            new MenuItemLocationPriceDto(),
            event
        );
        locationPrice.menuItemPortions = [];
        let currentLp = this.locationPrices.find(
            (page) => page.locationId === locationPrice.locationId
        );
        currentLp.menuItemPortions = [];

        this.menuItemDto.portions.map((item) => {
            let portion = Object.assign(new MenuItemPortionDto(), {
                id: item.id,
                locationId: locationPrice.locationId,
                menuItemId: this.id,
                price: item.price,
                portionName: item.name,
                locationPrice: 0,
                barcode: item.barcode,
            });
            currentLp.menuItemPortions.push(portion);
        });
    }

    comboGroupAdded(event) {
        let ind = this.combos.findIndex((cb) => cb.name === event.name);
        if (ind < 0) {
            this.combos.push(event);
        }
        if (this.selectedComboGroupIndex >= 0) {
            this.combos[this.selectedComboGroupIndex] = event;
        }

        this.selectedComboGroupIndex = undefined;
    }

    comboItemAdded(event) {
        if (this.selectedComboIndex >= 0) {
            let ind = this.comboItems.findIndex((cb) => cb.name === event.name);
            if (ind < 0) {
                this.comboItems.push(event);
            }

            if (this.selectedComboItemIndex >= 0) {
                this.comboItems[this.selectedComboItemIndex] = event;
            }

            this.combos[this.selectedComboIndex].comboItems = this.comboItems;

            this.selectedComboItemIndex = undefined;
        }
    }

    addTax(dinePlanTaxDropdown: Dropdown) {
        if (!this.selectedDinePlanTax || !this.selectedDinePlanTax.value) {
            this.notify.info(this.l("ChooseTax"));
            return;
        }

        this.taxDetail.dinePlanTaxId = +this.selectedDinePlanTax.value;

        this.menuItemTaxDetails.push(this.taxDetail);

        this.selectedDinePlanTax = undefined;
        dinePlanTaxDropdown.clear(undefined);

        this.taxDetail = {
            dinePlanTaxId: 0,
            isChangeValue: false,
            taxValue: null,
        };
    }

    removeTax(taxIndex) {
        this.menuItemTaxDetails.splice(taxIndex, 1);
    }

    getDinePlanTaxes() {
        this._dinePlanTaxesServiceProxy
            .getDinePlanTaxForCombobox(undefined)
            .subscribe((result) => {
                this.dinePlanTaxes = result;
            });
    }

    dinePlanTaxName(dinePlanTaxId: number) {
        if (dinePlanTaxId) {
            const dinePlanTax = (this.dinePlanTaxes || []).find(
                (item) => +item.value === +dinePlanTaxId
            );
            return dinePlanTax ? dinePlanTax.displayText : "";
        } else {
            return "";
        }
    }

    getComboGroups() {
        return (
            filterText,
            comboGroupSelectionIds,
            isDeleted,
            sorting,
            maxResultCount,
            skipCount
        ): Observable<PagedResultDtoOfProductComboGroupEditDto> =>
            this._productComboGroupServiceProxy.getAll(
                filterText,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined
            );
    }

    openComboGroupModal() {
        this.selectProductComboGroupPopupModal.show(this.combos);
    }

    selectComboGroup(event) {        
        this.combos = event.sort(x => x.sortOrder);
    }

    showLanguages() {
        this.languageDescriptionModal.show(
            "MenuItem",
            this.id,
            this.id + " - " + this.menuItemDto.name
        );
    }

    selectCategory() {
        this.selectCategoryPopupModal.show(this.selectedCategory);
      }

    getCategorySelection() {
            this._categoryService.getCategoriesForCombobox().subscribe(result=>{
                this.categoryList = result.items;
            });
    }

    addCategory(itemSelect) {
        // this.selectedCategory = itemSelect;
        this.menuItemDto.categoryId = +itemSelect;   
        this.menuItemDto.productType = 0;     
    }
    
    selectItem(event,value) {
        const valueAllergic = value.source._elementRef.nativeElement.innerText;
        
        if (event) {
            this.allergicList.push(valueAllergic);
        } else {
            this.allergicList = this.allergicList.filter(allergic => allergic !== valueAllergic);
        }
        
    }
}
export enum Allergy {
    MILK = "Milk",
    EGG = "Egg",
    PEANUTS = "Peanuts"
}
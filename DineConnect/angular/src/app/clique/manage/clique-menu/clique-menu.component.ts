import {
  Component,
  Injector,
  ViewChild,
  OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import { ActivatedRoute, Router } from "@angular/router";
import {
  LocationServiceProxy,
  LocationDto,
  EntityDto,
  CliqueMenuServiceProxy,
  CliqueMenuDto
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { FileDownloadService } from "@shared/utils/file-download.service";
import { Table, Paginator, LazyLoadEvent } from "primeng";
import moment from 'moment';
@Component({
  selector: 'app-clique-menu',
  templateUrl: './clique-menu.component.html',
  styleUrls: ['./clique-menu.component.scss']
})
export class CliqueMenuComponent extends AppComponentBase implements OnInit {
  filterText = "";
  isDeleted = false;

  @ViewChild("locationTable", { static: true }) locationTable: Table;
  @ViewChild("locationPaginator", { static: true })
  locationPaginator: Paginator;
  constructor(
    injector: Injector,
    private _router: Router,
    private _cliqueMenuService: CliqueMenuServiceProxy
  ) {
    super(injector);
  }
  fixedMenu: boolean = false;
  fromDateSearch: string;
  toDateSearch: string;
  publishStatus: number = 1;
  ngOnInit() {
  }

  createOrUpdateCliqueMealTime(id?: number) {
    this._router.navigate([
      "/app/clique/manage/schedule/create-or-update",
      id ? id : "null",
    ]);
  }

  getCliqueMenuList(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.locationPaginator.changePage(0);
      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    this._cliqueMenuService
      .getCliqueMenu(
        this.fixedMenu,
        this.publishStatus,
        this.fromDateSearch ? 
        moment(this.fromDateSearch).format("YYYY-MM-DD") : undefined,
        this.toDateSearch ?
        moment(this.toDateSearch).format("YYYY-MM-DD") : undefined,
        this.primengTableHelper.getSorting(this.locationTable),
        this.primengTableHelper.getMaxResultCount(this.locationPaginator,event),
        this.primengTableHelper.getSkipCount(
          this.locationPaginator,
          event
        )
      )
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe((result) => {
        console.log(result);
        
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
      });
  }

  reloadPage(): void {
    this.locationPaginator.changePage(this.locationPaginator.getPage());
  }

  craeteOrUpdateCliqueMenu(cliqueMenu: CliqueMenuDto) {
    this._router.navigate([
      "/app/clique/manage/schedule/create-or-update",
      cliqueMenu ? cliqueMenu.id : "null",
    ]);
  }

  deleteCliqueMenu(cliqueMenu: CliqueMenuDto) {
    this.message.confirm(
      "you want to delete schedule ",
      this.l("AreYouSure"),
      (isConfirmed) => {
        if (isConfirmed) {
          this._cliqueMenuService
            .deleteCliqueMenu(cliqueMenu.id)
            .subscribe((result) => {
              this.notify.success(this.l("SuccessfullyDeleted"));
              this.reloadPage();
            });
        }
      }
    );
  }

  reset() {
    this.fixedMenu = false;
    this.fromDateSearch = undefined;
    this.toDateSearch = undefined;

    this.getCliqueMenuList();
}
}

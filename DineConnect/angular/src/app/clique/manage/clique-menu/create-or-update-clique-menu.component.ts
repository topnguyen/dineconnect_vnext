import { WheelOpeningHourServiceType } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, Injector } from '@angular/core';
import {
  LocationServiceProxy,
  PagedResultDtoOfLocationDto,
  PagedResultDtoOfGetAllScreenMenuDto,
  CliqueScreenMenuServiceProxy,
  CliqueMenuServiceProxy,
  CliqueMenuDto,
  CliqueMealTimeServiceProxy,
  PagedResultDtoOfGetCliqueMealTimeDto
} from '@shared/service-proxies/service-proxies';
import { Observable } from 'rxjs';
import moment from 'moment';
import { OpeningHour } from '../schedule/opening-hour';

@Component({
  selector: 'createOrUpdateCliqueMenu',
  styleUrls: ['./create-or-update-clique-menu.component.scss'],
  templateUrl: './create-or-update-clique-menu.component.html',
  animations: [appModuleAnimation()]
})
export class CreateOrUpdateCliqueMenuModalComponent extends AppComponentBase implements OnInit {

  isAllDays: boolean = false;
  isFixedMenu: boolean = false;
  locationName = '';
  selectedLocation: any;
  menuItemName = '';
  selectedMenuItem: any;
  cliqueMealTimeName = '';
  selectedCliqueMealTime: any;
  cliqueMenuDto: CliqueMenuDto = new CliqueMenuDto();
  saving = false;
  isValid: boolean = false;
  openingHour: OpeningHour = new OpeningHour(WheelOpeningHourServiceType.Default);
  dateRange = [];
  constructor(
    injector: Injector,
    private _router: Router,
    private _locationServiceProxy: LocationServiceProxy,
    private _cliqueScreenMenuServiceProxy: CliqueScreenMenuServiceProxy,
    private _activeRouter: ActivatedRoute,
    private _cliqueMenuService: CliqueMenuServiceProxy,
    private _cliqueMealTimeServiceProxy: CliqueMealTimeServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._activeRouter.params.subscribe((params) => {
        const idCliqueMenu = +params['id'];
        if (idCliqueMenu > 0) {
            this.getCliqueMenuById(idCliqueMenu);
        }
        else {
          this.cliqueMenuDto.publishStatus = 1;
        }
    });
  }

  selectAllDays(event) {
    this.cliqueMenuDto.allDay = event;

  }
  selectFixedMenu(event) {
    this.cliqueMenuDto.fixedMenu = event;
    this.selectCutOfDate();
  }

  close() {
    this._router.navigate(["/app/clique/manage/schedule"]);
  }

  getLocationForCombobox() {
    return (
      filterString,
      sorting,
      maxResultCount,
      skipCount
    ): Observable<PagedResultDtoOfLocationDto> => {
      return this._locationServiceProxy.getList(
        filterString,
        false,
        0,
        sorting,
        maxResultCount,
        skipCount
      );
    };
  }

  changeLocation(data) {
    this.locationName = data.name;
    this.selectedLocation = data.id;
    this.cliqueMenuDto.locationId = data.id;
    this.cliqueMenuDto.locationName = data.name;

  }

  getMenuItemForCombobox() {
    return (
      filterString,
      sorting,
      maxResultCount,
      skipCount
    ): Observable<PagedResultDtoOfGetAllScreenMenuDto> => {
      return this._cliqueScreenMenuServiceProxy.getAll(
        filterString,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        sorting,
        skipCount,
        maxResultCount
      );
    };
  }

  changeMenuItem(data) {
    this.menuItemName = data.name;
    this.selectedMenuItem = data.id;
    this.cliqueMenuDto.screenMenuId = data.id;
    this.cliqueMenuDto.screenMenuName = data.name;

  }


  getCliqueMealTimeForCombobox() {
    return (
      filterString,
      sorting,
      maxResultCount,
      skipCount
    ): Observable<PagedResultDtoOfGetCliqueMealTimeDto> => {
      return this._cliqueMealTimeServiceProxy.getCliqueMealtime(
        filterString,
        sorting,
        maxResultCount,
        skipCount
      );
    };
  }

  changeCliqueMealTime(data) {
    this.cliqueMealTimeName = data.name;
    this.selectedCliqueMealTime = data.id;
    this.cliqueMenuDto.cliqueMealTimeId = data.id;
    this.cliqueMenuDto.cliqueMealTimeName = data.name;
  }

  getCliqueMenuById(cliqueMenuId: number){
    this._cliqueMenuService.getCliqueMenuById(cliqueMenuId).subscribe(cliqueMenu=>{
      this.cliqueMenuDto = cliqueMenu;
      this.dateRange = [moment(this.cliqueMenuDto.fromDate).toDate(), moment(this.cliqueMenuDto.toDate).toDate()];
      if(this.cliqueMenuDto.cutOffDate){
        this.cliqueMenuDto.cutOffDate = moment(this.cliqueMenuDto.cutOffDate).toDate()
      }
      let daySchedules = this.cliqueMenuDto.days.split(",");
      daySchedules.pop();
      this.openingHour.days.forEach(day=>day.isSelected = false);
      daySchedules.forEach(day=>{
        if(this.openingHour.days.find(dayOpening=>dayOpening.name == day)){
          this.openingHour.days.find(dayOpening=>dayOpening.name == day).isSelected = true;
        }
      })
    })
  }

  save() {
    this.saving = true;

    this.cliqueMenuDto.fromDate = moment(this.dateRange[0]).toDate();
    this.cliqueMenuDto.toDate = moment(this.dateRange[1]).toDate();
    if(this.cliqueMenuDto.cutOffDate){
      this.cliqueMenuDto.cutOffDate = moment(this.cliqueMenuDto.cutOffDate).toDate();
    }
    this.cliqueMenuDto.days = "";
    this.openingHour.days.filter(day=> day.isSelected).forEach(dayChild => this.cliqueMenuDto.days += dayChild.name + ",");
    this._cliqueMenuService
      .createOrUpdateCliqueMenu(this.cliqueMenuDto)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe((result) => {
        this.saving = false;
        this.notify.success(this.l('SuccessfullySaved'));
        this.close();
      });
  }

  selectCutOfDate() {
    if(!this.cliqueMenuDto.fixedMenu && this.cliqueMenuDto.cutOffDate && this.dateRange[0] 
      && !moment(this.cliqueMenuDto.cutOffDate).isBefore(moment(this.dateRange[0]))) {
      this.isValid = true;
    }else {
      this.isValid = false;
    }
  }
}

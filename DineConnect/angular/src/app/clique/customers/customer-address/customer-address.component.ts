import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
  UpdateMyAddressInputDto,
  ERPComboboxItem,
  TiffinMemberPortalServiceProxy,
  CommonServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { Router, ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-customer-address',
  templateUrl: './customer-address.component.html',
  styleUrls: ['./customer-address.component.scss']
})
export class CustomerAddressComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  addressId: number;
  addressDetail: UpdateMyAddressInputDto = new UpdateMyAddressInputDto();

  lstCity: ERPComboboxItem[] = [];
  countryName = "";
  countryId = 0;
  customerId = 0;

  floorNo = "";
  unitNo = "";
  tower = "";

  disableIfDefault = false;

  FloorNo = new FormControl('', Validators.required);


  propertyOptions = [
    { text: this.l("HDB"), value: 1 },
    { text: this.l("Condominium"), value: 2 },
    { text: this.l("LandedProperty"), value: 3 },
    { text: this.l("OfficeBuilding"), value: 5 },
    { text: this.l("ShopHouse"), value: 6 },
    { text: this.l("University/School"), value: 4 },
  ];

  constructor(
    injector: Injector,
    private _router: Router,
    private _memberServiceProxy: TiffinMemberPortalServiceProxy,
    private _commonService: CommonServiceProxy,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit() {

    this._activatedRoute.params.subscribe((params) => {
      this.addressId = +params["id"]; // (+) converts string 'id' to a number
      this.customerId = +params["customerId"];
      this.init();
      if (isNaN(this.addressId)) {
        this.addressId = undefined;
      }
      if (this.addressId === undefined) {
        this.addressDetail = new UpdateMyAddressInputDto();
        this.disableIfDefault = false;
      } else {
        this._memberServiceProxy
          .getMyAddress(this.addressId)
          .subscribe((result) => {
            this.addressDetail = result;
            if (!this.addressDetail.cityId) {
              this.addressDetail.cityId = undefined;
            }
            this.disableIfDefault = result.isDefault;
            this.getAddressSpilit();
          });
      }
    });
  }
  floorNoValidator() {
    // let FloorNo = control.value;
    // if ( !FloorNo) {
    //     return null;
    // }
    if (this.floorNo == null) {
      return '*If not applicable please enter 0';
    } else if (this.floorNo != null) {
      return null;
    }
  }

  save() {
    this.saving = true;

    this.addressDetail.address2 = this.getAddressJoin();
    this.addressDetail.customerId = this.customerId;
    this._memberServiceProxy
      .addOrUpdateMyAddress(this.addressDetail)
      .pipe(finalize(() => (this.saving = false)))
      .subscribe(() => {
        this.notify.success(this.l("SavedSuccessfully"));
        this.back();
      });
  }

  getAddressJoin(): string {
    let address2 = [];
    address2.push(this.floorNo);
    address2.push(this.unitNo);
    address2.push(this.tower);
    return address2.join();
  }

  getAddressSpilit() {
    if (this.addressDetail.address2) {
      let addressSpilit = this.addressDetail.address2.split(",");
      if (addressSpilit[0]) {
        this.floorNo = addressSpilit[0];
      }
      if (addressSpilit[1]) {
        this.unitNo = addressSpilit[1];
      }
      if (addressSpilit[2]) {
        this.tower = addressSpilit[2];
      }
    }
  }

  getListCity() {
    this._commonService
      .getLookups("Cities", this.appSession.tenantId, this.countryId)
      .subscribe((result) => {
        this.lstCity = result;
      });
  }

  getCountry() {
    this._commonService
      .getCountryName(this.countryId)
      .subscribe((result) => {
        this.countryName = result;
      });
  }

  getCallingCode() {
    this._commonService
      .getCountryCallCode(this.countryId)
      .subscribe((result) => {
        this.addressDetail.callingCode = result;
      });
  }

  init() {
    this._commonService
      .getCountryIdForCustomer(this.customerId)
      .subscribe((result) => {
        this.countryId = result;
        this.countryId = this.countryId || 0;
        this.getListCity();
        this.getCountry();
        this.getCallingCode();
      });
  }

  returnOnlyNumber(e) {
    let keynum;
    let keychar;
    let charcheck;

    if (window.event) {
      keynum = e.keyCode;
    } else if (e.which) {
      keynum = e.which;
    }

    if (keynum === 11) {
      return false;
    }
    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789]/;
    return charcheck.test(keychar);
  }

  back() {
    this._router.navigate([
      "/app/clique/customer/customers/create-or-update",
      this.customerId,
    ]);
  }
}

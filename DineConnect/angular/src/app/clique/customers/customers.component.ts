import { Component, OnInit, ViewChild, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { FileDownloadService } from "@shared/utils/file-download.service";
import {
    CustomersServiceProxy,
    CustomerListDto,
    CliqueCustomerServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Router } from "@angular/router";
import { LazyLoadEvent } from "primeng";
import { DTableColumn } from "@app/shared/common/dynamic-table/dynamic-table.model";
import { DynamicTableComponent } from "@app/shared/common/dynamic-table/dynamic-table.component";
@Component({
    templateUrl: "./customers.component.html",
    styleUrls: ["./customers.component.scss"],
    animations: [appModuleAnimation()],
})
export class CustomersComponent extends AppComponentBase implements OnInit {
    @ViewChild("dtable", { static: true }) dtable: DynamicTableComponent;

    filterText = "";

    cols: DTableColumn[] = [
        {
            field: null,
            header: this.l("Actions"),
            type: "action",
        },
        {
            field: "name",
            header: this.l("Name"),
            type: "string",
        },
        {
            field: "creationTime",
            header: this.l("RegisteredDate"),
            type: "date",
        },
        {
            field: "lastLoginDate",
            header: this.l("LastLoginDate"),
            type: "date",
        },
        {
            field: "emailAddress",
            header: this.l("ContactEmail"),
            type: "string",
        },
        {
            field: "phoneNumber",
            header: this.l("ContactPhone"),
            type: "string",
        },        
        {
            field: "acceptReceivePromotionEmail",
            header: this.l("AcceptReceivePromotionEmail"),
            type: "string",
        },
        {
            field: "balance",
            header: this.l("Balance"),
            type: "money",
        },
    ];

    constructor(
        injector: Injector,
        private _customerService: CustomersServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _router: Router,
        private _cliqueCustomerService: CliqueCustomerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        setTimeout(() => {
            this.configDtable();
        }, 500);
    }

    configDtable() {
        this.dtable.configure({
            cols: this.cols,
            isGrantedEdit: 'Pages.Tenant.Tiffin.Customer.Customers.Edit',
            isGrantedDelete: 'Pages.Tenant.Tiffin.Customer.Customers.Delete',
            dataSource: (
                sorting: string,
                skipCount: number,
                maxResultCount: number
            ) => {
                return this._cliqueCustomerService.getAll(
                    this.filterText,
                    undefined,
                    undefined,
                    sorting,
                    skipCount,
                    maxResultCount
                );
            },
            edit: (item: CustomerListDto) => {
                this.createOrEditCustomer(item.id);
            },
            delete: (item: CustomerListDto) => {
                this.message.confirm(
                    this.l("CustomerDeleteWarningMessage", item.name),
                    this.l("AreYouSure"),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            this._customerService
                                .delete(item.id)
                                .subscribe(() => {
                                    this.reloadPage();
                                    this.notify.success(
                                        this.l("SuccessfullyDeleted")
                                    );
                                });
                        }
                    }
                );
            },
            stateKey: "clique-customer-stated",
        });
    }

    reloadPage(): void {
        this.filterText = undefined;

        this.dtable.paginator.changePage(this.dtable.paginator.getPage());
    }

    createOrEditCustomer(id?) {
        let url = this._router.url;
        if (url.includes("clique"))
            this._router.navigate([
                "/app/clique/customer/customers/create-or-update",
                id ? id : "null",
            ]);
        if (url.includes("wheel"))
            this._router.navigate([
                "/app/wheel/customers/create-or-update",
                id ? id : "",
            ]);
    }

    exportToExcel(event?: LazyLoadEvent): void {
        this._customerService
            .getCustomersToExcel(
                this.filterText,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dtable.dataTable),
                this.primengTableHelper.getSkipCount(
                    this.dtable.paginator,
                    event
                ),
                this.primengTableHelper.getMaxResultCount(
                    this.dtable.paginator,
                    event
                )
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}

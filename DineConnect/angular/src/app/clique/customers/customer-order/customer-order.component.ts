import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { 
TiffinOrderServiceProxy,
OrderHistoryDto, 
ChangePickupStatus, 
SendPickUpOrderRemindInput,
CliqueCustomerServiceProxy 
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import moment from 'moment';
import { Table, Paginator, LazyLoadEvent } from "primeng";

@Component({
    templateUrl: './customer-order.component.html',
    styleUrls: ['./customer-order.component.scss'],
    animations: [appModuleAnimation()]
})
export class CustomerOrderComponent extends AppComponentBase implements OnInit {
    @ViewChild("locationTable", { static: true }) ordersListView: Table;
    @ViewChild('collectPaginator', { static: true }) paginator: Paginator;

    term: string = '';
    isPickedUp = false;
    date: string ="";

    constructor(
    injector: Injector,
    private _tiffinOrderProxy: TiffinOrderServiceProxy,
    private _cliqueCustomerProxy: CliqueCustomerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {}

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._cliqueCustomerProxy
            .getCustomerCliqueOrder(
                this.term,
                !this.date ? "" :
                moment(this.date).local().format("MM-DD-YYYY"),
                this.primengTableHelper.getSorting(this.ordersListView),
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items; 
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    markCollected(id: number, isPickedup = true) {
        const input = new ChangePickupStatus();
        input.isPickup = isPickedup;
        input.orderId = id;
        this._tiffinOrderProxy.changePickupStatus(input).subscribe((result) => {
            this.notify.success(this.l('Successfully'));
            this.reloadPage();
        });
    }

    remind(id: number) {
        const input = new SendPickUpOrderRemindInput();
        input.orderIds = [id];

        this._tiffinOrderProxy.sendPickUpOrderRemindEmail(input).subscribe((ressult) => {
            this.notify.success(this.l('SendRemindSuccessfully'));
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    refresh() {
        this.term = undefined;
        this.date = "";
        this.isPickedUp = false;
        this.paginator.changePage(0);
    }

    isRemind(order: OrderHistoryDto) {
        return !order.pickedUpTime;
    }
}

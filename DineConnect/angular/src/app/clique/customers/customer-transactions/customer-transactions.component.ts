import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinCustomerTransactionsServiceProxy, PagedResultDtoOfCustomerListDto, CustomersServiceProxy, CliqueCustomerServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { EditCustomerOfferModalComponent } from './edit-balance-modal.component';
import { ChangeHistoryModalComponent } from './change-history-modal.component';
import * as moment from 'moment';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './customer-transactions.component.html',
  styleUrls: ['./customer-transactions.component.scss'],
  animations: [appModuleAnimation()]
})
export class CustomerTransactionsComponent extends AppComponentBase implements OnInit {
  @ViewChild('balanceTable', { static: true }) balanceTable: Table;
  @ViewChild('balancePaginator', { static: true }) balancePaginator: Paginator;

  @ViewChild('editBalanceModal', { static: true }) editBalanceModal: EditCustomerOfferModalComponent;
  @ViewChild('historyModal', { static: true }) historyModal: ChangeHistoryModalComponent;

  customerFilter: any;
  paymentFilter = '';
  customerName = '';
  daysExpiryFilter: number;
  public dateRange = [];
  startDate: moment.Moment;
  endDate = undefined;
  selectedCustomer: any;
  constructor(
    injector: Injector,
    private _activatedRoute: ActivatedRoute,
    private _customerService: CustomersServiceProxy,
    private _tiffinCustomerTransactionsServiceProxy: TiffinCustomerTransactionsServiceProxy,
    private _cliqueCustomerProxy: CliqueCustomerServiceProxy
  ) {
    super(injector);
    this.customerFilter = this._activatedRoute.snapshot.queryParams['customerFilter'] || '';
  }

  ngOnInit() {
   
  }

  getBalanceList(event?: LazyLoadEvent) {
    if (this.dateRange as any !== undefined && this.dateRange.length) {
      this.startDate = moment(this.dateRange[0]).startOf('day');
      this.endDate = moment(this.dateRange[1]).endOf('day');
    }

    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.balancePaginator.changePage(0);

      return;
    }
    this.primengTableHelper.showLoadingIndicator();
    this._cliqueCustomerProxy.getCustomerTransactionAdmin(
      !this.startDate ? '' :
      moment(this.startDate).format("MM-DD-YYYY HH:mm:ss"),
      !this.endDate ? '' :
      moment(this.endDate).format("MM-DD-YYYY HH:mm:ss"),
      this.selectedCustomer || undefined,
      this.paymentFilter,
      this.daysExpiryFilter,
      this.primengTableHelper.getSorting(this.balanceTable),
      this.primengTableHelper.getMaxResultCount(this.balancePaginator, event),
      this.primengTableHelper.getSkipCount(this.balancePaginator, event))
    .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
    .subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
    });

    this.primengTableHelper.hideLoadingIndicator();
  }

  reloadPage(): void {
    this.balancePaginator.changePage(this.balancePaginator.getPage());
  }


  editCustomerOffer(record) {
    this.editBalanceModal.show(record.customerOfferId);
  }

  showHistory(record) {
    this.historyModal.show(record.customerOfferId);
  }

  refresh() {
    this.customerFilter = undefined;
    this.paymentFilter = undefined;
    this.daysExpiryFilter = undefined;
    this.dateRange = undefined;
    this.endDate = undefined;
    this.startDate = undefined;
    this.selectedCustomer = undefined;
    this.customerName ='';
    this.reloadPage();
  }


  returnOnlyNumber(e) {
    let keynum;
    let keychar;
    let charcheck;

    if (window.event) {
      keynum = e.keyCode;
    } else if (e.which) {
      keynum = e.which;
    }

    if (keynum === 11) {
      return false;
    }
    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789]/;
    return charcheck.test(keychar);
  }

  

  checkEditExpiryDate(expiryDate: moment.Moment): boolean {
    return moment() > expiryDate;
  }

    changeCustomer(data) {
      this.customerName = data.name;
      this.selectedCustomer = data.id;
  }

  getCustomerForComboBox() {
      return (
          filterString,
          sorting,
          maxResultCount,
          skipCount
      ): Observable<PagedResultDtoOfCustomerListDto> => {
          return this._customerService.getAll(
              filterString,null, null,sorting,skipCount,maxResultCount);
      };
  }
}

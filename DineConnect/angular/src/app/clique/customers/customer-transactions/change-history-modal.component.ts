import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { TiffinCustomerTransactionsServiceProxy, TiffinCustomerProductOfferDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { Table } from 'primeng/table';

@Component({
  selector: 'change-history-modal',
  templateUrl: './change-history-modal.component.html',
  styleUrls: ['./change-history-modal.component.scss']
})
export class ChangeHistoryModalComponent extends AppComponentBase {

  @ViewChild('modal', { static: true }) modal: ModalDirective;
  @ViewChild('dataTable', { static: true }) dataTable: Table;


  active = false;
  saving = false;

  customerOffer: TiffinCustomerProductOfferDto = new TiffinCustomerProductOfferDto();

  constructor(
    injector: Injector,
    private _customerOffersServiceProxy: TiffinCustomerTransactionsServiceProxy
  ) {
    super(injector);
  }

  show(customerOfferId): void {
    this._customerOffersServiceProxy.getEditHistories(customerOfferId)
      .subscribe(result => {
        this.primengTableHelper.records = result.items;
        this.active = true;
        this.modal.show();
      });

  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

  shown() { }
}

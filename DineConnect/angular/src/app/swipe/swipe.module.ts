import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwipeRoutingModule } from './swipe-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { CalendarModule, CheckboxModule, PaginatorModule, TableModule } from 'primeng';
import { UtilsModule } from '@shared/utils/utils.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SwipeCardTypeComponent } from './components/master/swipe-card-type/swipe-card-type.component';
import { SwipeCardTypeAddComponent } from './components/master/swipe-card-type/swipe-card-type-add/swipe-card-type-add.component';
import { TextMaskModule } from 'angular2-text-mask';
import { SwipeCardComponent } from './components/master/swipe-card/swipe-card.component';
import { SwipeCardAddModalComponent } from './components/master/swipe-card/swipe-card-add-modal/swipe-card-add-modal.component';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SwipeTransactionComponent } from './components/transaction/swipe-transaction.component';
import { IssueNewCardComponent } from './components/transaction/issue-new-card/issue-new-card.component';
import { SwipeBalanceComponent } from './components/transaction/swipe-balance/swipe-balance.component';

@NgModule({
    declarations: [SwipeCardTypeComponent, SwipeCardTypeAddComponent, SwipeCardComponent, SwipeCardAddModalComponent, SwipeTransactionComponent, IssueNewCardComponent, SwipeBalanceComponent],
    imports: [
        CommonModule,
        SwipeRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        UtilsModule,
        AppCommonModule,
        PaginatorModule,
        TableModule,
        TabsModule,
        CheckboxModule,
        CalendarModule,
        SelectButtonModule,
        BsDropdownModule,
        TextMaskModule,
        AppBsModalModule,
        NgSelectModule
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        }
    ]
})
export class SwipeModule { }

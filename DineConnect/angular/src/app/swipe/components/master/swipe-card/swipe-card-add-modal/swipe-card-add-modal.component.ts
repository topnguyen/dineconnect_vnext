import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { SwipeCardDto, SwipeCardServiceProxy, SwipeCardTypeDto, SwipeCardTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-swipe-card-add-modal',
  templateUrl: './swipe-card-add-modal.component.html',
  styleUrls: ['./swipe-card-add-modal.component.scss']
})
export class SwipeCardAddModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    active = false;
    swipeCard = new SwipeCardDto();
    cardTypes: SwipeCardTypeDto[] = [];

    constructor(
        injector: Injector,
        private _swipeCardServiceProxy: SwipeCardServiceProxy,
        private _swipeCardTypeServiceProxy: SwipeCardTypeServiceProxy
    ) {
        super(injector);
    }

    show(cardId?: number): void {
        this.active = true;

        if (!cardId) {
            this.modal.show();
        } else {
            this._swipeCardServiceProxy.get(cardId).subscribe(result => {
                this.swipeCard = result;
                this.modal.show();
            });
        }

        this._swipeCardTypeServiceProxy.getAll(undefined, 'id', 0, 1000).subscribe(result => {
            this.cardTypes = result.items;
        });
    }

    save(): void {
        this.saving = true;

        this._swipeCardServiceProxy.addOrUpdate(this.swipeCard)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

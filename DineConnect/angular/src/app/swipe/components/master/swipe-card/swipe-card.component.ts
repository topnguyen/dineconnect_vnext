import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { finalize } from 'rxjs/operators';
import { SwipeCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { SwipeCardAddModalComponent } from './swipe-card-add-modal/swipe-card-add-modal.component';

@Component({
  templateUrl: './swipe-card.component.html',
  styleUrls: ['./swipe-card.component.scss'],
  animations: [appModuleAnimation()]
})
export class SwipeCardComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('createOrEditCardModal', { static: true }) createOrEditCardModal: SwipeCardAddModalComponent;

    filterText = '';
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _swipeCardServiceProxy: SwipeCardServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit() {
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;
        this._swipeCardServiceProxy
            .getAll(
                this.filterText,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable) || 'id',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditCard(id?: number): void {
        this.createOrEditCardModal.show(id);
    }

    deleteCard(id?: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._swipeCardServiceProxy.delete(id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    refresh() {
        this.filterText = undefined;
        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._swipeCardServiceProxy
            .getToExcel(
                this.filterText,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable) || 'id',
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search() {
        this.getAll();
    }
}

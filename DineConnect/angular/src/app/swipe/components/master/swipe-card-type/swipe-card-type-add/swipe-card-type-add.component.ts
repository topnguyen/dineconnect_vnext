import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ImportModalComponent } from '@app/connect/shared/import-modal/import-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SwipeCardServiceProxy, SwipeCardTypeDto, SwipeCardTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './swipe-card-type-add.component.html',
    styleUrls: ['./swipe-card-type-add.component.scss'],
    animations: [appModuleAnimation()]
})
export class SwipeCardTypeAddComponent extends AppComponentBase implements OnInit {
    @ViewChild('importModal', { static: true }) importModal: ImportModalComponent;

    saving = false;
    swipeCardType = new SwipeCardTypeDto();
    cardTypeId: number;
    selectedTab: number = 1;
    cardList = [];
    // autoActivationFlag = false;

    prefix;
    suffix;
    cardNumberOfDigits;
    cardStartingNumber;
    cardCount;

    TAB = {
        CARD_TYPE: 1,
        GENERATE_NEW_CARDS: 2,
        CARDS: 3,
        IMPORT: 4
    };

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _swipeCardTypeServiceProxy: SwipeCardTypeServiceProxy,
        private _swipeCardServiceProxy: SwipeCardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params) => {
            this.cardTypeId = +params['id']; // (+) converts string 'id' to a number

            if (!isNaN(this.cardTypeId)) {
                this._init();
                this.configImport();
            }
        });
    }

    save(): void {
        this.saving = true;

        this._swipeCardTypeServiceProxy
            .addOrUpdate(this.swipeCardType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    configImport() {
        // this.importModal.configure({
        //     title: this.l('CardType'),
        //     downloadTemplate: () => {
        //         return this._connectCardTypeServiceProxy.getImportCardsTemplateToExcel();
        //     },
        //     import: (fileToken: string) => {
        //         return this._connectCardTypeServiceProxy.importCardsFromExcel(this.cardTypeId, fileToken);
        //     },
        //     filters: [
        //         {
        //             name: 'imageFilter',
        //             fn: (item, options) => {
        //                 var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        //                 if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
        //                     this.message.warn(this.l('ImportTemplate_Warn_FileType'));
        //                     return false;
        //                 }
        //                 return true;
        //             }
        //         }
        //     ]
        // });
    }

    onChangeTab(tab: number) {
        this.selectedTab = tab;
    }

    close(): void {
        this._router.navigate(['/app/swipe/master/card-type']);
    }

    generateCards() {
        // const body = Gene.fromJS({
        //     connectCardTypeId: this.connectCardType.id,
        //     prefix: this.prefix,
        //     suffix: this.suffix,
        //     cardNumberOfDigits: this.cardNumberOfDigits,
        //     cardStartingNumber: this.cardStartingNumber,
        //     cardCount: this.cardCount
        // });
        // this._swipeCardServiceProxy.(body).subscribe((result) => {
        //     this.notify.info(this.cardCount + ' ' + this.l('CardsAreBeingGeneratedInBackground'));
        //     this.connectCardType.id = null;
        //     this.prefix = null;
        //     this.suffix = null;
        //     this.cardNumberOfDigits = null;
        //     this.cardStartingNumber = null;
        //     this.cardCount = null;
        //     this.close();
        // });
    }

    import() {
        this.importModal.show();
    }

    cardTransactionDataShown(id) {}

    onModalSave() {
        this._init();
    }

    private _init() {
        this._swipeCardTypeServiceProxy.get(this.cardTypeId).subscribe((result) => {
            this.swipeCardType = result;

            // if (this.swipeCardType['autoActivateRegExpression']?.length > 0) {
            //     this.autoActivationFlag = true;
            // }

            this._getCardList();
        });
    }

    private _getCardList() {
        this._swipeCardServiceProxy.getAll(undefined, this.swipeCardType.id, 'id DESC', 0, 1000).subscribe((result) => {
            this.cardList = result.items
        });
    }
}

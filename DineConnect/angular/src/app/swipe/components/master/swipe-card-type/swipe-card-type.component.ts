import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { SwipeCardTypeServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './swipe-card-type.component.html',
    styleUrls: ['./swipe-card-type.component.scss'],
    animations: [appModuleAnimation()]
})
export class SwipeCardTypeComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    filterText = '';
    lazyLoadEvent: LazyLoadEvent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _swipeCardTypeServiceProxy: SwipeCardTypeServiceProxy,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;

        this._swipeCardTypeServiceProxy
            .getAll(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable) || 'id',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createOrEditCardType(id?: number): void {
        this._router.navigate(['/app/swipe/master/card-type/create-or-update', id ? id : 'null']);
    }

    deleteCardType(id: number): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._swipeCardTypeServiceProxy.delete(id).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'));
                });
            }
        });
    }

    refresh() {
        this.filterText = undefined;
        this.paginator.changePage(0);
    }

    exportToExcel() {
        this._swipeCardTypeServiceProxy
            .getToExcel(
                this.filterText,
                this.primengTableHelper.getSorting(this.dataTable) || 'id',
                this.primengTableHelper.getSkipCount(this.paginator, this.lazyLoadEvent),
                this.primengTableHelper.getMaxResultCount(this.paginator, this.lazyLoadEvent)
            )
            .subscribe((result) => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    search() {
        this.getAll();
    }

    ngOnInit(): void {}
}

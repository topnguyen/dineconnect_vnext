import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonLookupModalComponent } from '@app/shared/common/lookup/common-lookup-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AddTransactionDto, CustomerListDto, CustomersServiceProxy, PagedResultDtoOfCustomerListDto, SwipeCardDto, SwipeCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { SwipeBalanceComponent } from '../swipe-balance/swipe-balance.component';

@Component({
    selector: 'app-issue-new-card',
    templateUrl: './issue-new-card.component.html',
    animations: [appModuleAnimation()]
})
export class IssueNewCardComponent extends AppComponentBase implements OnInit {
    @ViewChild('SwipeBalanceModal', { static: true }) swipeBalanceComponent: SwipeBalanceComponent;
    @ViewChild('userLookupModal', { static: true }) userLookupModal: CommonLookupModalComponent;

    swiperTransaction = new AddTransactionDto();
    swipeCard = new SwipeCardDto();
    topupFlag = false;
    refundFlag = false;
    customer: CustomerListDto;

    constructor(
        injector: Injector,
        private _router: Router,
        private _customersServiceProxy: CustomersServiceProxy,
        private _swipeCardServiceProxy: SwipeCardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.init();
    }

    init(): void {
        this.userLookupModal.configure({
            title: this.l('SelectACustomer'),
            dataSource: (skipCount: number, maxResultCount: number, filter: string): Observable<any> => {
                return this._customersServiceProxy.getAll(filter, undefined, undefined, 'name', skipCount, maxResultCount);
            }
        });
    }

    showCommonLookupModal(): void {
        this.userLookupModal.show();
    }
    
    userSelected(customer: CustomerListDto): void {
        this.customer = customer
        this.swipeCard.customerId = customer.id;
        this.swipeCard.customerName = customer.name;
        this.swiperTransaction.customerId = customer.id;
        console.log(customer);
    }

    save() {
        if (!this.topupFlag && !this.refundFlag) {
            this.notify.error('Please choose Topup or Refund');
            return;
        }
        this.swiperTransaction.paymentType = 'Cash';


        const input= cloneDeep(this.swiperTransaction);
        if (!this.topupFlag && this.refundFlag) {
            input.amount = input.amount * -1;
        }

        this._swipeCardServiceProxy.addTransaction(input).subscribe((result) => {
            this.swiperTransaction.amount = 0;
            this.notify.success(this.l('SavedSuccessfully'));
            this.checkCard(false);
        });
    }

    topup() {
        this.topupFlag = true;
        this.refundFlag = false;
    }

    refund() {
        this.refundFlag = true;
        this.topupFlag = false;
    }

    clear() {
        this.topupFlag = false;
        this.refundFlag = false;
        this.swipeCard = new SwipeCardDto();
        this.swiperTransaction = new AddTransactionDto();
    }

    close() {
        this._router.navigate(['app/swipe/transaction']);
    }

    viewBalance() {
        this.swipeBalanceComponent.show(this.swipeCard.id);
    }

    checkCard(isShowMessage = true) {
        if (this.swiperTransaction.cardNumber) {
            this._swipeCardServiceProxy.checkCard(this.swiperTransaction.cardNumber).subscribe((result) => {
                if (result) {
                    this.swipeCard = result;

                    this.swiperTransaction.customerId = this.swipeCard.customerId;

                    if (isShowMessage) {
                        this.notify.success('Valid Card');
                    }
                }
            });
        }
    }
}

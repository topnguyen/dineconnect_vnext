import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table, Paginator, LazyLoadEvent } from 'primeng';
import { finalize } from 'rxjs/operators';
import { SwipeCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { SwipeBalanceComponent } from './swipe-balance/swipe-balance.component';
import { Router } from '@angular/router';

@Component({
    templateUrl: './swipe-transaction.component.html',
    styleUrls: ['./swipe-transaction.component.scss'],
    animations: [appModuleAnimation()]
})
export class SwipeTransactionComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('SwipeBalanceModal', { static: true }) swipeBalanceComponent: SwipeBalanceComponent;

    filterText = '';
    customerId: number;
    customerName: string;
    cardNumber: string;
    lazyLoadEvent: LazyLoadEvent;

    constructor(injector: Injector, private _router: Router, private _swipeCardServiceProxy: SwipeCardServiceProxy) {
        super(injector);
    }

    ngOnInit() {}

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.lazyLoadEvent = event;
        this._swipeCardServiceProxy
            .getAllCustomerCard(
                this.customerId,
                this.customerName,
                this.cardNumber,
                this.primengTableHelper.getSorting(this.dataTable) || 'id DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    topupRefund(): void {
        this._router.navigate(['app/swipe/transaction/topup-refund']);
    }

    viewTransaction(cardId: number) {
        this.swipeBalanceComponent.show(cardId);
    }

    refresh() {
        this.filterText = undefined;
        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SwipeCardTypeAddComponent } from './components/master/swipe-card-type/swipe-card-type-add/swipe-card-type-add.component';
import { SwipeCardTypeComponent } from './components/master/swipe-card-type/swipe-card-type.component';
import { SwipeCardAddModalComponent } from './components/master/swipe-card/swipe-card-add-modal/swipe-card-add-modal.component';
import { SwipeCardComponent } from './components/master/swipe-card/swipe-card.component';
import { IssueNewCardComponent } from './components/transaction/issue-new-card/issue-new-card.component';
import { SwipeTransactionComponent } from './components/transaction/swipe-transaction.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    /* TODO: permission */
                    { path: 'master/card-type', component: SwipeCardTypeComponent, data: { permission: 'Pages' } },
                    { path: 'master/card-type/create-or-update/:id', component: SwipeCardTypeAddComponent, data: { permission: 'Pages' } },
                    { path: 'master/card', component: SwipeCardComponent, data: { permission: 'Pages' } },
                    { path: 'master/card/create-or-update/:id', component: SwipeCardAddModalComponent, data: { permission: 'Pages' } },
                    { path: 'transaction', component: SwipeTransactionComponent, data: { permission: 'Pages' } },
                    { path: 'transaction/topup-refund', component: IssueNewCardComponent, data: { permission: 'Pages' } },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})

export class SwipeRoutingModule { }

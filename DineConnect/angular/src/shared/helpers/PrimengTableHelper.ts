import { LazyLoadEvent, SortEvent } from 'primeng/public_api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import * as rtlDetect from 'rtl-detect';

export class PrimengTableHelper {
    predefinedRecordsCountPerPage = [5, 10, 25, 50, 100, 250, 500];

    defaultRecordsCountPerPage = 10;

    isResponsive = true;

    resizableColumns: false;

    totalRecordsCount = 0;

    records: any[];

    isLoading = false;

    showLoadingIndicator(): void {
        setTimeout(() => {
            this.isLoading = true;
        }, 0);
    }

    hideLoadingIndicator(): void {
        setTimeout(() => {
            this.isLoading = false;
        }, 0);
    }

    getSorting(table: Table): string {
        let sorting = '';

        if (table) {
            if (table.sortMode === 'multiple') {
                if (table.multiSortMeta) {
                    for (let i = 0; i < table.multiSortMeta.length; i++) {
                        const element = table.multiSortMeta[i];
                        if (i > 0) {
                            sorting += ',';
                        }
                        sorting += element.field;
                        if (element.order === 1) {
                            sorting += ' ASC';
                        } else if (element.order === -1) {
                            sorting += ' DESC';
                        }
                    }
                }
            } else {
                if (table.sortField) {
                    sorting = table.sortField;
                    if (table.sortOrder === 1) {
                        sorting += ' ASC';
                    } else if (table.sortOrder === -1) {
                        sorting += ' DESC';
                    }
                }
    
            }
        }

        return sorting;
    }

    getMaxResultCount(paginator: Paginator, event: LazyLoadEvent): number {
        if (paginator) {
            if (paginator.rows) {
                return paginator.rows;
            }
    
            if (!event) {
                return 10;
            }
        }

        return event.rows || 10;
    }

    getSkipCount(paginator: Paginator, event: LazyLoadEvent): number {
        if (paginator) {
            if (paginator.first) {
                return paginator.first;
            }
    
            if (!event) {
                return 0;
            }   
        }

        return event.first;
    }

    shouldResetPaging(event: LazyLoadEvent): boolean {
        if (!event /*|| event.sortField*/) { // if you want to reset after sorting, comment out parameter
            return true;
        }

        return false;
    }

    adjustScroll(table: Table) {
        const rtl = rtlDetect.isRtlLang(abp.localization.currentLanguage.name);
        if (!rtl) {
            return;
        }

        const body: HTMLElement = table.el.nativeElement.querySelector('.ui-table-scrollable-body');
        const header: HTMLElement = table.el.nativeElement.querySelector('.ui-table-scrollable-header');
        body.addEventListener('scroll', () => {
            header.scrollLeft = body.scrollLeft;
        });
    }

    customSort(event: SortEvent) {
        event.data.sort((data1, data2) => {
            let value1 = data1[event.field];
            let value2 = data2[event.field];
            let result = null;

            if (value1 == null && value2 != null) {
                result = -1;
            } else if (value1 != null && value2 == null) {
                result = 1;
            } else if (value1 == null && value2 == null) {
                result = 0;
            } else if (typeof value1 === 'string' && typeof value2 === 'string') {
                result = value1.localeCompare(value2);
            } else {
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
            }

            return (event.order * result);
        });
    }
}

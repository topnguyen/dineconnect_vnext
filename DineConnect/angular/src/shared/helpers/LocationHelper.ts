import { CommonLocationGroupDto } from '@shared/service-proxies/service-proxies';

export class LocationHelper {

  static getFilter(value: CommonLocationGroupDto) {
    let locationIds = '';
    const filterLocations = value ?? new CommonLocationGroupDto();

    if (filterLocations.group) {
      locationIds = filterLocations.groups.map(x => x.id).join(',');
    } else if (filterLocations.locationTag) {
      locationIds = filterLocations.locationTags.map(x => x.id).join(',');
    } else if (filterLocations.nonLocations?.length) {
      locationIds = filterLocations.nonLocations.map(x => x.id).join(',');
    } else if (filterLocations.locations?.length) {
      locationIds = filterLocations.locations.map(x => x.id).join(',');
    }

    return {
      locationIds,
      locationsGroup: !!filterLocations.group,
      locationTag: !!filterLocations.locationTag,
      nonLocations: !!filterLocations.nonLocations?.length,
    };
  }

  static checkValid(value: CommonLocationGroupDto) {
    if (!value) {
      return false;
    }

    if (value.group) {
      if (!value.groups?.length) {
        return false;
      }
    } else if (value.locationTag) {
      if (!value.locationTags?.length) {
        return false;
      }
    } else if (!value.locations?.length && !value.nonLocations?.length) {
      return false;
    }

    return true;
  }
}

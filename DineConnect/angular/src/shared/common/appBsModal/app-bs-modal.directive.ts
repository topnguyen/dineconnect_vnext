import { Directive, Input } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";

@Directive({
    selector: "[appBsModal]",
    exportAs: "bs-modal",
})
export class AppBsModalDirective extends ModalDirective {
    @Input() backDropZIndex?: number;

    showElement(): void {
        super.showElement();
        this.setZIndexes();
        const firstFoucs = this["_element"].nativeElement.querySelector(
            "[autoFocus]"
        );
        if (!!firstFoucs) {
          setTimeout(() => {
            firstFoucs.focus();
          }, 0);
        }
    }

    setZIndexes(): void {
        let newZIndex = this.setAndGetModalZIndex();
        this.setBackDropZIndex(typeof this.backDropZIndex === 'number' ? this.backDropZIndex : newZIndex - 1);
    }

    setAndGetModalZIndex(): number {
        let modalBaseZIndex = 1050;
        let modalsLength = document.querySelectorAll(".modal.fade.show").length;

        let newZIndex = modalBaseZIndex + modalsLength * 2;

        (this as any)._element.nativeElement.style.zIndex = newZIndex.toString();
        return newZIndex;
    }

    setBackDropZIndex(zindex: number): void {
        let modalBackdrops = document.querySelectorAll(
            ".modal-backdrop.fade.show"
        );
        const modalBackdrop = modalBackdrops[modalBackdrops.length - 1] as HTMLElement;
        if (modalBackdrop) {
            modalBackdrop.style.zIndex = zindex.toString();
        }
    }
}

import { Component, Injector } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { AppUrlService } from "@shared/common/nav/app-url.service";
import { AppSessionService } from "@shared/common/session/app-session.service";
import { AppUiCustomizationService } from "@shared/common/ui/app-ui-customization.service";
import { PrimengTableHelper } from "shared/helpers/PrimengTableHelper";
import { UiCustomizationSettingsDto } from "@shared/service-proxies/service-proxies";
import { NgxSpinnerService } from "ngx-spinner";
import { NgxSpinnerTextService } from "@app/shared/ngx-spinner-text.service";
import {
    LocalizationService,
    PermissionCheckerService,
    FeatureCheckerService,
    NotifyService,
    SettingService,
    MessageService,
    AbpMultiTenancyService,
} from "abp-ng2-module";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import * as moment from "moment";
import {
    QueryBuilderClassNames,
    QueryBuilderConfig,
} from "angular2-query-builder";
import { join } from "lodash";

export abstract class AppComponentBase {
    localizationSourceName =
        AppConsts.localization.defaultLocalizationSourceName;

    localization: LocalizationService;
    permission: PermissionCheckerService;
    feature: FeatureCheckerService;
    notify: NotifyService;
    setting: SettingService;
    message: MessageService;
    multiTenancy: AbpMultiTenancyService;
    appSession: AppSessionService;
    primengTableHelper: PrimengTableHelper;
    ui: AppUiCustomizationService;
    appUrlService: AppUrlService;
    spinnerService: NgxSpinnerService;
    private ngxSpinnerTextService: NgxSpinnerTextService;
    defaultColor = abp.setting.get("App.Theme.Color");
    numberMask = createNumberMask({
        prefix: "",
        allowDecimal: false,
    });

    decimalMask = createNumberMask({
        prefix: "",
        allowDecimal: true,
    });
    intMask = createNumberMask({
        prefix: "",
        allowDecimal: false,
        includeThousandsSeparator: false,
    });

    DATE_FORMAT = {
        yyyyMMdd_hhmmss: "YYYY-MM-DD HH:mm:ss",
    };

    constructor(injector: Injector) {
        this.localization = injector.get(LocalizationService);
        this.permission = injector.get(PermissionCheckerService);
        this.feature = injector.get(FeatureCheckerService);
        this.notify = injector.get(NotifyService);
        this.setting = injector.get(SettingService);
        this.message = injector.get(MessageService);
        this.multiTenancy = injector.get(AbpMultiTenancyService);
        this.appSession = injector.get(AppSessionService);
        this.ui = injector.get(AppUiCustomizationService);
        this.appUrlService = injector.get(AppUrlService);
        this.primengTableHelper = new PrimengTableHelper();
        this.spinnerService = injector.get(NgxSpinnerService);
        this.ngxSpinnerTextService = injector.get(NgxSpinnerTextService);
    }

    flattenDeep(array) {
        return array.reduce(
            (acc, val) =>
                Array.isArray(val)
                    ? acc.concat(this.flattenDeep(val))
                    : acc.concat(val),
            []
        );
    }

    l(key: string, ...args: any[]): string {
        args.unshift(key);
        args.unshift(this.localizationSourceName);
        return this.ls.apply(this, args);
    }

    ls(sourcename: string, key: string, ...args: any[]): string {
        let localizedText = this.localization.localize(key, sourcename);

        if (!localizedText) {
            localizedText = key;
        }

        if (!args || !args.length) {
            return localizedText;
        }

        args.unshift(localizedText);
        return abp.utils.formatString.apply(this, this.flattenDeep(args));
    }

    isGranted(permissionName: string): boolean {
        return this.permission.isGranted(permissionName);
    }

    isGrantedAny(...permissions: string[]): boolean {
        if (!permissions) {
            return false;
        }

        for (const permission of permissions) {
            if (this.isGranted(permission)) {
                return true;
            }
        }

        return false;
    }

    s(key: string): string {
        return abp.setting.get(key);
    }

    appRootUrl(): string {
        return this.appUrlService.appRootUrl;
    }

    get currentTheme(): UiCustomizationSettingsDto {
        return this.appSession.theme;
    }

    get containerClass(): string {
        if (this.appSession.theme.baseSettings.layout.layoutType === "fluid") {
            return "container-fluid";
        }

        return "container";
    }

    showMainSpinner(text?: string): void {
        this.ngxSpinnerTextService.currentText = text;
        this.spinnerService.show();
    }

    hideMainSpinner(text?: string): void {
        this.spinnerService.hide();
    }

    returnOnlyNumber(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }

        if (keynum === 11) {
            return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[0123456789]/;
        return charcheck.test(keychar);
    }

    enterPhoneNumber(e) {
        let keynum;
        let keychar;
        let charcheck;

        if (window.event) {
            keynum = e.keyCode;
        } else if (e.which) {
            keynum = e.which;
        }

        if (keynum === 11) {
            return false;
        }
        keychar = String.fromCharCode(keynum);
        charcheck = /[+0123456789]/;
        return charcheck.test(keychar);
    }

    getBackgroundColor(color) {
        let result = color;
        if (!color) {
            result = this.defaultColor;
        }

        return result;
    }

    log(value) {
        console.log(value);
    }

    getDate(date) {
        return date
            ? moment(date).add(-new Date().getTimezoneOffset(), "minutes")
            : undefined;
    }

    transfromDate(date: Date) {
        return moment(date).add(moment().tz(Intl.DateTimeFormat().resolvedOptions().timeZone).format('z'), 'hour');
    }

    createDateRangePickerOptions() {
        return [
            {
                value: [new Date(), new Date()],
                label: this.l("Today"),
            },
            {
                value: [
                    new Date(new Date().setDate(new Date().getDate() - 1)),
                    new Date(new Date().setDate(new Date().getDate() - 1)),
                ],
                label: this.l("Yesterday"),
            },
            {
                value: [
                    new Date(new Date().setDate(new Date().getDate() - 6)),
                    new Date(),
                ],
                label: this.l("Last7Days"),
            },
            {
                value: [
                    new Date(new Date().setDate(new Date().getDate() - 30)),
                    new Date(),
                ],
                label: this.l("Last30Days"),
            },
            {
                value: [
                    moment()
                        .startOf("month")
                        .add(moment().utcOffset(), "minute")
                        .toDate(),
                    new Date(),
                ],
                label: this.l("ThisMonth"),
            },
            {
                value: [
                    moment()
                        .startOf("month")
                        .subtract(1, "month")
                        .add(moment().utcOffset(), "minute")
                        .toDate(),
                    moment().endOf("month").subtract(1, "month").toDate(),
                ],
                label: this.l("LastMonth"),
            },
            {
                value: [
                    moment()
                        .startOf("month")
                        .subtract(2, "month")
                        .add(moment().utcOffset(), "minute")
                        .toDate(),
                    moment().endOf("month").subtract(1, "month").toDate(),
                ],
                label: this.l("TwoMonthsAgo"),
            },
        ];
    }

    queryBuilderClassNames: QueryBuilderClassNames = {
        // removeIcon: 'fa fa-minus',
        // addIcon: 'fa fa-plus',
        arrowIcon: "fa fa-chevron-right px-2",
        button: "btn btn-sm btn-outline-success",
        buttonGroup: "btn-group",
        removeButton: "btn-outline-danger",
        rightAlign: "order-12 ml-auto",
        switchRow: "d-flex px-2 rules-group-container",
        switchGroup: "d-flex align-items-center",
        // switchRadio: 'custom-control-input',
        // switchLabel: 'custom-control-label',
        // switchControl: 'custom-control custom-radio custom-control-inline',
        row: "row p-2 m-1",
        rule: "border",
        ruleSet: "border",
        invalidRuleSet: "alert alert-danger",
        emptyWarning: "text-danger mx-auto",
        operatorControl: "form-control",
        operatorControlSize: "col-auto pr-0",
        fieldControl: "form-control",
        fieldControlSize: "col-auto pr-0",
        entityControl: "form-control",
        entityControlSize: "col-auto pr-0",
        inputControl: "form-control",
        inputControlSize: "col-auto",
    };

    getLocationsSelected(locationOrGroup): any {
        if (!locationOrGroup) {
            return 0;
        }
        if (locationOrGroup.group) {
            return join(locationOrGroup.groups.map(item => item.name), ', ');
        } else if (locationOrGroup.locationTag) {
            return join(locationOrGroup.locationTags.map(item => item.name), ', ');
        } else if (locationOrGroup.nonLocations?.length) {
            return join(locationOrGroup.nonLocations.map(item => item.name), ', ');
        } else if (locationOrGroup.locations?.length) {
            return join(locationOrGroup.locations.map(item => item.name), ', ');
        }
    }

    // Build query builder with type
    mapQuery(originQuery, config) {
        return {
            condition: originQuery.condition,
            rules: (originQuery.rules || []).map((rule) => {
                if (rule.field) {
                    return {
                        field: rule.field,
                        operator: rule.operator,
                        type: this.getQueryType(rule, config),
                        value: rule.value,
                    };
                } else {
                    return this.mapQuery(rule, config);
                }
            }),
        };
    }

    getQueryType(rule: any, config: QueryBuilderConfig) {
        let queryType:
            | "double"
            | "string"
            | "boolean"
            | "datetime"
            | "category" = null;
        const type = config.fields ? config.fields[rule.field].type : rule.type;

        switch (type) {
            case "number":
                queryType = "double";
                break;
            case "string":
                queryType = "string";
                break;
            case "boolean":
                queryType = "boolean";
                break;
            case "datetime":
                queryType = "datetime";
                break;
            case "category":
                queryType = "double";
                break;
            default:
                queryType = "string";
                break;
        }

        return queryType;
    }
}

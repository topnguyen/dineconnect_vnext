import { Injector, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'statuscolor'
})
export class StatusColorPipe implements PipeTransform {

    transform(status: String): String {
        
        var color = '#0a5114';
        (status == 'None') ? color = '#0a5114' : color = color;
        (status == 'Cancelled') ? color = '#d1d1d1' : color = color;
        (status == 'Pending') ? color = '#fbc994' : color = color;
        (status == 'Confirmed') ? color = '#5ac5b6' : color = color;
        (status == 'Enquiry') ? color = '#85cae7' : color = color;

        return color;
    }
}

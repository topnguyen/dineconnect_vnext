import { Directive, Self, Output, EventEmitter, Input, SimpleChanges, OnDestroy, OnChanges } from '@angular/core';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as moment from 'moment';
import compare from 'just-compare';

///this directive ensures that the date value will always be the moment.
@Directive({
    selector: '[onlyDatePickerMomentModifier]'
})
export class OnlyDatePickerMomentModifierDirective implements OnDestroy, OnChanges {
    subscribe: Subscription;
    lastDate: Date = null;

    @Output('ngModelChange') update = new EventEmitter();

    constructor(@Self() private bsDatepicker: BsDatepickerDirective) {
        this.subscribe = bsDatepicker.bsValueChange
            .pipe(filter(date => !!(date && date instanceof Date && !compare(this.lastDate, date) && date.toString() !== 'Invalid Date')))
            .subscribe((date: Date) => {
                this.lastDate = this.removeTimeZone(date);
                this.update.emit(this.lastDate);
            });
    }

    ngOnDestroy() {
        this.subscribe.unsubscribe();
    }

    ngOnChanges({ date }: SimpleChanges) {
        if (date && date.currentValue && !compare(date.currentValue, date.previousValue)) {
            var lastDate =  this.removeTimeZone(new Date(date.currentValue));
            setTimeout(() => this.bsDatepicker.bsValue = lastDate, 0);
        }
    }

    removeTimeZone(date: Date) {
        date.setUTCFullYear(date.getFullYear(), date.getMonth(), date.getDate());
        date.setUTCHours(0, 0, 0, 0);
        
        return date;
    }
}

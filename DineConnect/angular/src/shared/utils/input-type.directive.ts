import { Directive, HostListener, Input, ElementRef } from "@angular/core";
import { TicketTagGroupType } from "@shared/service-proxies/service-proxies";
import { StringValidationHelper } from "@shared/helpers/StringValidationHelper";

@Directive({
    selector: "[inputType]",
})
export class InputTypeDirective {
    @Input('ticketTagGroupType') type: TicketTagGroupType;

    constructor(private el: ElementRef) {

    }

    @HostListener("keypress", ['$event']) onKeyDown(e: KeyboardEvent) {
        switch (this.type) {
            case TicketTagGroupType.Integer:
                if (!StringValidationHelper.IsInteger(e.key))
                    e.preventDefault();
                break;
            case TicketTagGroupType.Alphanumeric:
                if(!StringValidationHelper.IsAlphanumeric(e.key))
                    e.preventDefault();
                break;
            default:
                break;
        }
    }

    @HostListener("keyup", ['$event']) onKeyUp(e: KeyboardEvent) {
        if (this.type === TicketTagGroupType.Decimal) {
            const str = (this.el.nativeElement as HTMLInputElement).value;
            (this.el.nativeElement as HTMLInputElement).value = str.replace(/[^\d.]|\.(?=.*\.)/g, '');
        }
    }
}

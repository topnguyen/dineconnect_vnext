export class AppConsts {

    static readonly tenancyNamePlaceHolderInUrl = '{TENANCY_NAME}';

    static remoteServiceBaseUrl: string;
    static remoteServiceBaseUrlFormat: string;
    static appBaseUrl: string;
    static appBaseHref: string; // returns angular's base-href parameter value if used during the publish
    static appBaseUrlFormat: string;
    static recaptchaSiteKey: string;
    static subscriptionExpireNootifyDayCount: number;
    static currency: string;
    static localeMappings: any = [];

    static readonly userManagement = {
        defaultAdminUserName: 'admin'
    };

    static readonly localization = {
        defaultLocalizationSourceName: 'DineConnect'
    };

    static readonly authorization = {
        encrptedAuthTokenName: 'enc_auth_token'
    };

    static readonly grid = {
        defaultPageSize: 10
    };

    static readonly MinimumUpgradePaymentAmount = 1;

    /// <summary>
    /// Gets current version of the application.
    /// It's also shown in the web page.
    /// </summary>
    static readonly WebAppGuiVersion = '9.0.1.0';

    static readonly ScreenMenuBreadcrums: string = "ScreenMenuBreadcrums";
}
export const MenuItemTagsConst: any[] =  [
    { value: "HOT", src: "../../../assets/image/markItemAs/hot.svg" },
    { value: "VEGETARIAN", src: "../../../assets/image/markItemAs/vegetarian.svg" },
    { value: "HALAI", icon: "", src: "../../../assets/image/markItemAs/halal.svg" },
    { value: "HOTPROMOTION", src: "../../../assets/image/markItemAs/hot-promotions.svg" },
    { value: "VEGAN", icon: "",src: "../../../assets/image/markItemAs/vegan.svg" },
    { value: "BESTSELLER", src: "../../../assets/image/markItemAs/best-seller.svg" },
    { value: "GLUTENFEE", icon: "",src: "../../../assets/image/markItemAs/gluten.svg" },
  ]

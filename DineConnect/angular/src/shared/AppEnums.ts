export class AppEditionExpireAction {
    static DeactiveTenant = 'DeactiveTenant';
    static AssignToAnotherEdition = 'AssignToAnotherEdition';
}
export enum PromotionTypes
{
    NotAssigned = 0,
    HappyHour = 1,
    FixedDiscountPercentage = 2,
    FixedDiscountValue = 3,
    BuyXItemGetYItemFree = 4,
    BuyXItemGetYatZValue = 5,
    OrderDiscount = 6,
    FreeItems = 7,
    TicketDiscount = 8,
    PaymentOrderDiscount = 9,
    PaymentTicketDiscount = 10,
    BuyXAndYAtZValue = 11,
    StepDiscount = 12
};

﻿import { AbpHttpInterceptor, RefreshTokenService, AbpHttpConfigurationService } from 'abp-ng2-module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import * as ApiServiceProxies from './service-proxies';
import * as ApiNswagServiceProxies from './service-proxies-nswag';
import { ZeroRefreshTokenService } from '@account/auth/zero-refresh-token.service';
import { ZeroTemplateHttpConfigurationService } from './zero-template-http-configuration.service';

@NgModule({
    providers: [
        ApiServiceProxies.ItemReportServiceProxy,
        ApiServiceProxies.TicketServiceProxy,
        ApiServiceProxies.LanguageDescriptionsServiceProxy,
        ApiServiceProxies.TiffinOrderServiceProxy,
        ApiServiceProxies.TiffinOrderTagsServiceProxy,
        ApiServiceProxies.WheelSetupsServiceProxy,
        ApiServiceProxies.WheelDepartmentsServiceProxy,
        ApiServiceProxies.WheelLocationsServiceProxy,
        ApiServiceProxies.WheelTaxesServiceProxy,
        ApiServiceProxies.WheelPaymentMethodsServiceProxy,
        ApiServiceProxies.WheelDeliveryDurationsServiceProxy,
        ApiServiceProxies.TiffinMealTimesServiceProxy,
        ApiServiceProxies.AddonSettingsServiceProxy,
        ApiServiceProxies.TiffinCustomerTransactionsServiceProxy,
        ApiServiceProxies.TiffinProductOfferTypesServiceProxy,
        ApiServiceProxies.TemplateEnginesServiceProxy,
        ApiServiceProxies.TiffinProductOffersServiceProxy,
        ApiServiceProxies.DinePlanTaxesServiceProxy,
        // ApiServiceProxies.ConnectCardsServiceProxy,
        // ApiServiceProxies.ConnectCardTypesServiceProxy,
        // ApiServiceProxies.FullTaxMembersServiceProxy,
        // ApiServiceProxies.DinePlanUsersServiceProxy,
        // ApiServiceProxies.DinePlanUserRolesServiceProxy,
        ApiServiceProxies.PromotionsServiceProxy,
        ApiServiceProxies.DepartmentsServiceProxy,
        ApiServiceProxies.PriceTagsServiceProxy,
        ApiServiceProxies.TicketTagGroupsServiceProxy,
        ApiServiceProxies.FutureDateInformationsServiceProxy,
        ApiServiceProxies.TerminalsServiceProxy,
        //ApiServiceProxies.PrintTemplatesServiceProxy,
        ApiServiceProxies.CalculationsServiceProxy,
        ApiServiceProxies.TillAccountsServiceProxy,
        ApiServiceProxies.TransactionTypesServiceProxy,
        ApiServiceProxies.PlanReasonsServiceProxy,
        ApiServiceProxies.ForeignCurrenciesServiceProxy,
        ApiServiceProxies.PaymentTypesServiceProxy,
        ApiServiceProxies.LocationTagsServiceProxy,
        ApiServiceProxies.ProductGroupsServiceProxy,
        ApiServiceProxies.AuditLogServiceProxy,
        ApiServiceProxies.CachingServiceProxy,
        ApiServiceProxies.ChatServiceProxy,
        ApiServiceProxies.CommonLookupServiceProxy,
        ApiServiceProxies.EditionServiceProxy,
        ApiServiceProxies.FriendshipServiceProxy,
        ApiServiceProxies.HostSettingsServiceProxy,
        ApiServiceProxies.InstallServiceProxy,
        ApiServiceProxies.LanguageServiceProxy,
        ApiServiceProxies.NotificationServiceProxy,
        ApiServiceProxies.OrganizationUnitServiceProxy,
        ApiServiceProxies.PermissionServiceProxy,
        ApiServiceProxies.ProfileServiceProxy,
        ApiServiceProxies.RoleServiceProxy,
        ApiServiceProxies.SessionServiceProxy,
        ApiServiceProxies.TenantServiceProxy,
        ApiServiceProxies.TenantDashboardServiceProxy,
        ApiServiceProxies.TenantSettingsServiceProxy,
        ApiServiceProxies.TimingServiceProxy,
        ApiServiceProxies.UserServiceProxy,
        ApiServiceProxies.UserLinkServiceProxy,
        ApiServiceProxies.UserLoginServiceProxy,
        ApiServiceProxies.WebLogServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.TokenAuthServiceProxy,
        ApiServiceProxies.TenantRegistrationServiceProxy,
        ApiServiceProxies.HostDashboardServiceProxy,
        ApiServiceProxies.PaymentServiceProxy,
        ApiServiceProxies.DemoUiComponentsServiceProxy,
        ApiServiceProxies.InvoiceServiceProxy,
        ApiServiceProxies.SubscriptionServiceProxy,
        ApiServiceProxies.InstallServiceProxy,
        ApiServiceProxies.UiCustomizationSettingsServiceProxy,
        ApiServiceProxies.PayPalPaymentServiceProxy,
        ApiServiceProxies.StripePaymentServiceProxy,
        ApiServiceProxies.DashboardCustomizationServiceProxy,
        ApiServiceProxies.WheelServiceProxy,
        ApiServiceProxies.TenantDatabaseServiceProxy,
        ApiServiceProxies.WheelDynamicPagesServiceProxy,
        ApiServiceProxies.WebhookEventServiceProxy,
        ApiServiceProxies.WebhookSubscriptionServiceProxy,
        ApiServiceProxies.WebhookSendAttemptServiceProxy,
        ApiServiceProxies.UserDelegationServiceProxy,
        ApiServiceProxies.DynamicParameterServiceProxy,
        ApiServiceProxies.DynamicEntityParameterDefinitionServiceProxy,
        ApiServiceProxies.EntityDynamicParameterServiceProxy,
        ApiServiceProxies.DynamicParameterValueServiceProxy,
        ApiServiceProxies.EntityDynamicParameterValueServiceProxy,
        ApiServiceProxies.WheelScreenMenusServiceProxy,
        ApiServiceProxies.WheelScreenMenuItemsServiceProxy,
        ApiServiceProxies.MenuItemServiceProxy,
        ApiServiceProxies.ProductComboGroupServiceProxy,
        ApiServiceProxies.LocationServiceProxy,
        ApiServiceProxies.ScreenMenuServiceProxy,
        ApiServiceProxies.PrintersServiceProxy,
        ApiServiceProxies.PrintTemplateServiceProxy,
        ApiServiceProxies.PrintJobServiceProxy,
        ApiServiceProxies.PrintConditionServiceProxy,
        ApiServiceProxies.WheelCustomizationSettingsServiceProxy,
        ApiServiceProxies.AllowOriginServiceServiceProxy,
        ApiServiceProxies.ProgramSettingTemplatesServiceProxy,
        ApiServiceProxies.TiffinFaqServiceProxy,
        ApiServiceProxies.TiffinHomeBannerServiceProxy,
        ApiServiceProxies.WheelTableServiceProxy,
        ApiServiceProxies.ProductSetsServiceProxy,
        ApiServiceProxies.CustomersServiceProxy,
        ApiServiceProxies.CommonServiceProxy,
        ApiServiceProxies.MenuItemServiceProxy,
        ApiServiceProxies.ScheduleServiceProxy,
        ApiServiceProxies.LocationGroupServiceProxy,
        ApiServiceProxies.OrderSummaryServiceProxy,
        ApiServiceProxies.PaymentSummaryServiceProxy,
        ApiServiceProxies.BalanceSummaryServiceProxy,
        ApiServiceProxies.KitchenReportServiceProxy,
        ApiServiceProxies.DriverReportServiceProxy,
        ApiServiceProxies.PreparationReportServiceProxy,
        ApiServiceProxies.TiffinOrderServiceProxy,
        ApiServiceProxies.TiffinPromotionServiceProxy,
        ApiServiceProxies.DeliveryTimeSlotServiceProxy,
        ApiServiceProxies.CustomerReportServiceProxy,
        ApiServiceProxies.TiffinMemberPortalServiceProxy,
        ApiServiceProxies.MealPlanReportServiceProxy,
        ApiServiceProxies.DineQueueServiceProxy,
        ApiServiceProxies.TicketServiceProxy,
        ApiServiceProxies.WheelOrderServiceProxy,
        ApiServiceProxies.WheelTicketShippingOrderServiceProxy,
        ApiServiceProxies.SwipeCardTypeServiceProxy,
        ApiServiceProxies.SwipeCardServiceProxy,
        ApiServiceProxies.WheelDashboardsAppserviceServiceProxy,
        ApiServiceProxies.DineGoDeviceServiceProxy,
        ApiServiceProxies.DineGoBrandServiceProxy,
        ApiServiceProxies.DineGoDepartmentServiceProxy,
        ApiServiceProxies.DineGoPaymentTypeServiceProxy,
        ApiServiceProxies.DineGoChargeServiceProxy,
        ApiServiceProxies.GrabServiceProxy,
        ApiServiceProxies.XeroServiceProxy,
        ApiServiceProxies.ShipmentDepotServiceProxy,
        ApiServiceProxies.ShipmentDriverServiceProxy,
        ApiServiceProxies.ShipmentRouteServiceProxy,
        ApiServiceProxies.ShipmentClientServiceProxy,
        ApiServiceProxies.ShipmentAddressServiceProxy,
        ApiServiceProxies.ShipmentClientAddressServiceProxy,
        ApiServiceProxies.ShipmentAnalyticServiceProxy,
        ApiServiceProxies.ConnectSetupJobServiceProxy,
        ApiServiceProxies.DepartmentSummaryServiceProxy,
        ApiServiceProxies.PlanDayReportServiceProxy,
        ApiServiceProxies.CashSummaryReportServiceProxy,

        ApiServiceProxies.OrderTagGroupServiceProxy,
        /* TODO: Remove after fixing nswag is completed */
        ApiNswagServiceProxies.WheelProductSoldOutServiceProxy,
        ApiNswagServiceProxies.ScheduleServiceProxy,
        ApiNswagServiceProxies.TemplateEnginesServiceProxy,
        ApiNswagServiceProxies.TenantSettingsServiceProxy,
        ApiNswagServiceProxies.FlyerServiceProxy,
        ApiNswagServiceProxies.WheelRecommendationMenusServiceProxy,
        ApiNswagServiceProxies.DelAggLocationGroupServiceProxy,
        ApiNswagServiceProxies.DelAggLocationServiceProxy,
        ApiNswagServiceProxies.DelAggLocMappingServiceProxy,
        ApiNswagServiceProxies.DelAggItemGroupServiceProxy,
        ApiNswagServiceProxies.DelAggLanguageServiceProxy,
        ApiNswagServiceProxies.DelAggCategoryServiceProxy,
        ApiNswagServiceProxies.DelTimingGroupServiceProxy,
        ApiNswagServiceProxies.DelAggItemGroupServiceProxy,
        ApiNswagServiceProxies.DelAggItemServiceProxy,
        ApiNswagServiceProxies.DelAggTaxServiceProxy,
        ApiNswagServiceProxies.DelAggChargeServiceProxy,
        ApiNswagServiceProxies.DelAggLocationItemServiceProxy,
        ApiNswagServiceProxies.DelAggVariantServiceProxy,
        ApiNswagServiceProxies.DelAggVariantGroupServiceProxy,
        ApiNswagServiceProxies.DelAggModifierServiceProxy,
        ApiNswagServiceProxies.DelAggModifierGroupServiceProxy,
        ApiNswagServiceProxies.DelAggImageServiceProxy,


        ApiNswagServiceProxies.OrderTagGroupServiceProxy,
        ApiNswagServiceProxies.ScheduleSalesItemsReportServiceProxy,
        ApiNswagServiceProxies.ScheduleSalesLocationReportServiceProxy,
        ApiNswagServiceProxies.TillAccountsServiceProxy,
        ApiNswagServiceProxies.PlanLoggerServiceProxy,
        ApiNswagServiceProxies.SaleTargetServiceProxy,
        ApiNswagServiceProxies.TenderReportServiceProxy,
        ApiNswagServiceProxies.PaymentExcessReportServiceProxy,
        { provide: RefreshTokenService, useClass: ZeroRefreshTokenService },
        { provide: AbpHttpConfigurationService, useClass: ZeroTemplateHttpConfigurationService },
        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceProxyModule { }

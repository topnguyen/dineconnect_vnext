import { Component, OnInit, Injector } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinMemberPortalServiceProxy, GetAllTiffinProductOffersInput, TiffinMemberProductOfferDto } from '@shared/service-proxies/service-proxies';
import { ProductService } from '@app/shared/services/product.service';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-buy-offer',
    templateUrl: './buy-offer.component.html',
    styleUrls: ['./buy-offer.component.scss']
})
export class BuyOfferComponent extends AppComponentBase implements OnInit {
    productOffers: TiffinMemberProductOfferDto[];
    isCollapsed = new Array(100).fill(true);
    constructor(
        injector: Injector,
        public _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private productService: ProductService,
        private _location: Location
    ) {
        super(injector);
    }

    ngOnInit() {
        this.spinnerService.show();
        const input = new GetAllTiffinProductOffersInput();
        this._portalServiceProxy
            .getActiveOffers(input)
            .pipe(finalize(() => this.spinnerService.hide()))
            .subscribe((result) => {
                this.productOffers = result.items;
            });
    }

    addToCart(product: TiffinMemberProductOfferDto) {
        this.productService.addToCart(() => {
            abp.notify.success(`${product.name} added to Cart!`, '', {
                timer: 1000
            });
        }, product);
    }

    back() {
        this._location.back();
    }
}

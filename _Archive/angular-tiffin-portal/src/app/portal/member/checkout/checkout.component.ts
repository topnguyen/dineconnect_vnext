import {
    Component,
    ViewChild,
    ElementRef,
    Injector,
    OnInit,
} from "@angular/core";

import { AppComponentBase } from "@shared/common/app-component-base";
import {
    TiffinMemberProductOfferDto,
    BillingInfo,
    TiffinMemberPortalServiceProxy,
    CommonServiceProxy,
    ERPComboboxItem,
    TiffinOrderServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { ProductService } from "@app/shared/services/product.service";
import { CartCalculatorComponent } from "../cart-calculator/cart-calculator.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";

@Component({
    selector: "app-checkout",
    templateUrl: "./checkout.component.html",
    styleUrls: ["./checkout.component.scss"],
    animations: [appModuleAnimation()],
})
export class CheckoutComponent extends AppComponentBase implements OnInit {
    @ViewChild("cartCalculatorComponent", { static: true })
    cartCalculatorComponent: CartCalculatorComponent;
    @ViewChild("stripeCardElement", { static: false }) cardelement: ElementRef;

    lstCity: ERPComboboxItem[] = [];
    lstCountry: ERPComboboxItem[] = [];

    billingInfo: BillingInfo = new BillingInfo();

    addressId: any;
    addresses: any;
    cartProducts: TiffinMemberProductOfferDto[];

    constructor(
        injector: Injector,
        private _productService: ProductService,
        private _commonService: CommonServiceProxy,
        private _memberServiceProxy: TiffinMemberPortalServiceProxy,
        private _orderService: TiffinOrderServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this._commonService
            .getLookups("Cities", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCity = result;
            });

        this._commonService
            .getLookups("Countries", this.appSession.tenantId, undefined)
            .subscribe((result) => {
                this.lstCountry = result;
            });

        this._memberServiceProxy.getMyInfo().subscribe((myInfo) => {
            this.billingInfo.init(myInfo);
            this.billingInfo.address1 = "";
            this.billingInfo.address2 = "";
            this.billingInfo.address3 = "";
        });

        this._orderService.getCustomerAddresses().subscribe((result) => {
            this.addresses = result;
        });

        this.getCartProduct();
    }

    getCartProduct() {
        this.cartProducts = this._productService.getLocalCartProducts();
    }

    changeCountry(id) {
        this._commonService
            .getLookups("Cities", this.appSession.tenantId, id)
            .subscribe((result) => {
                this.lstCity = result;
            });
    }

    addressChanged() {
        if (this.addressId !== undefined) {
            this._orderService
                .getCustomerAddressesById(this.addressId)
                .subscribe((result) => {
                    this.billingInfo.address1 = result.address1;
                    this.billingInfo.address2 = result.address2;
                    this.billingInfo.address3 = result.address3;
                });
        } else {
            this.billingInfo.address1 = "";
            this.billingInfo.address2 = "";
            this.billingInfo.address3 = "";
        }
    }
}

import { Component, OnInit, Injector, ViewChild, NgZone } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocationDto, LocationServiceProxy, OrderHistoryDto, TiffinOrderServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileDownloadService } from '@shared/utils/services/file-download.service';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { EditOrderModalComponent } from '../orders/edit-order-modal/edit-order-modal.component';

@Component({
    selector: 'app-order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('editOrderModal', { static: true }) editOrderModal: EditOrderModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    orderHistory: OrderHistoryDto[] = [];
    listLocation: LocationDto[] = [];
    currentDate = moment();
    cutOffTime = moment(abp.setting.get('App.MealSetting.OrderCutOffTime').replace(/"/g, ''));
    isTimeEdit = false;
    allowDate = moment();

    constructor(
        injector: Injector,
        private zone: NgZone,
        private _tiffinOrderProxy: TiffinOrderServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _locationServiceProxy: LocationServiceProxy
    ) {
        super(injector);

        this.zone.runOutsideAngular(() => {
            setInterval(() => {
                this.disabledEdit();
            }, 1000);
        });
    }

    ngOnInit() {
        this._getSelfPickupLocation();
    }

    private _getSelfPickupLocation() {
        this._locationServiceProxy
            .getList(undefined, false, 0, undefined, 1000, 0)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.listLocation = result.items || [];
                this.getAll();
            });
    }

    getAll(event?) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();

        this._tiffinOrderProxy
            .getOrderHistory(
                this.appSession.customerId,
                undefined,
                undefined,
                undefined,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(
                finalize(() => {
                    this.primengTableHelper.hideLoadingIndicator();
                })
            )
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items.map((item) => {
                    const location = (this.listLocation || []).find((l) => l.id == item.selfPickupLocationId);
                    const address = location
                        ? `${location.address1}, ${location.address2}, ${location.address3}, ${location.cityName}, ${location.countryName}`
                        : null;

                    return {
                        ...item,
                        addOn: item.addOn ? JSON.parse(item.addOn) : [],
                        timeSlotFormTo: item.timeSlotFormTo ? JSON.parse(item.timeSlotFormTo) : null,
                        locationName: address
                    };
                });
                this.primengTableHelper.hideLoadingIndicator();
            });

        this.disabledEdit();
    }

    exportExcel() {
        this._tiffinOrderProxy.exportOrderHistory().subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
            this.notify.success(this.l('DownloadedSuccessfully'));
        });
    }

    printOrder(record) {
        this._tiffinOrderProxy.printOrderHistory(record.orderId).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }

    getCutOffTime() {
        this._tiffinOrderProxy.getCutOffTime().subscribe((result) => {
            this.cutOffTime = result;
            this.disabledEdit();
        });
    }

    disabledEdit() {
        let currentDate = moment().add(1, 'second');
        this.isTimeEdit = currentDate.format('HH:mm') < this.cutOffTime.format('HH:mm');

        if (!this.isTimeEdit) {
            this.allowDate = currentDate.add(2, 'days').startOf('day');
        }
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    editOrder(item) {
        this.editOrderModal.show(item.orderId);
    }

    deleteOrder(record) {
        this.message.confirm(this.l('OrderDeleteWarningMessage', record.choiceOfSet), this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._tiffinOrderProxy.deleteOrder(record.orderId).subscribe(() => {
                    this.reloadPage();
                    this.notify.success(this.l('SuccessfullyDeleted'), '', { timer: 700 });
                });
            }
        });
    }
}

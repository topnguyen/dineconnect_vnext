import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { OrderItem } from '@app/portal/services/order.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AddonListDto } from '@shared/service-proxies/service-proxies';
import { cloneDeep } from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-addon-dialog',
    templateUrl: './addon-dialog.component.html',
    styleUrls: ['./addon-dialog.component.scss']
})
export class AddonDialogComponent extends AppComponentBase {

    @ViewChild('addonModal', { static: true }) modal: ModalDirective;

    @Output() onClose = new EventEmitter<{item: OrderItem, addOn: AddonListDto[]}>();

    item: OrderItem;
    addons: AddonListDto[]

    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    close(): void {
        this.hide();
    }

    add(): void {
        const addons = (this.addons || []).filter(a => a.quantity > 0);

        this.onClose.emit({
            item: this.item,
            addOn: addons
        });
        this.hide();
    }

    show(item: OrderItem, addons): void {
        this.item = item;
        this.addons = cloneDeep(addons);
        this.addons.forEach(addon => {

            const index = (this.item.addOn || []).findIndex(e => e.value == addon.value);
            addon.quantity = index >= 0 ? this.item.addOn[index].quantity : addon.quantity;
        })

        this.modal.show();
    }

    hide(): void {
        this.addons = [];
        this.item = undefined;

        this.modal.hide();
    }

    increaseQuantity(addon: AddonListDto) {
        addon.quantity = addon.quantity + 1;
    }

    decreaseQuantity(addon: AddonListDto) {
        if (addon.quantity > 0) {
            addon.quantity = addon.quantity - 1;
        }
    }
}

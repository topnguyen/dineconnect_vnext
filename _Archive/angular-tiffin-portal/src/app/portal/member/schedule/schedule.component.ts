import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  animations: [appModuleAnimation()]
})
export class ScheduleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

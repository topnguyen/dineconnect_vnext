import { Component, Injector, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TiffinMemberPortalServiceProxy, UpdateMyInfoDto, MyInfoDto } from '@shared/service-proxies/service-proxies';
import { CardIdGuiderModalComponent } from '@app/shared/common/card-id-guide-modal/card-id-guider-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-detail-profile',
    templateUrl: './detail-profile.component.html',
    styleUrls: ['./detail-profile.component.scss']
})
export class DetailProfileComponent extends AppComponentBase implements OnChanges {
    @ViewChild('CardIdGuiderModal') CardIdGuiderModal: CardIdGuiderModalComponent;

    @Input() myInfo: MyInfoDto;

    active = false;
    saving = false;
    cardID: string;
    grade: string;

    grades: string[] = [' 1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'School Staff'];

    constructor(injector: Injector, private _memberServiceProxy: TiffinMemberPortalServiceProxy) {
        super(injector);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.myInfo && this.myInfo?.jsonData) {
            const jsonData = JSON.parse(this.myInfo.jsonData);

            this.grade = jsonData.grade;
            this.cardID = jsonData.cardID;
        }
    }

    showGetCardIdGuide() {
        this.CardIdGuiderModal.show();
    }

    save(): void {
        this.saving = true;
        let updateInfo = new UpdateMyInfoDto();
        updateInfo.init(this.myInfo);
        updateInfo.jsonData = JSON.stringify({
            grade: this.grade,
            cardID: this.cardID
        });

        this._memberServiceProxy
            .updateMyBasicInfo(updateInfo)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.appSession.user.name = this.myInfo.name;
                this.notify.info(this.l('SavedSuccessfully'));
            });
    }
}

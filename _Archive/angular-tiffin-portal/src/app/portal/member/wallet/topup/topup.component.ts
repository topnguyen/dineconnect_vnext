import { Component, ElementRef, Injector, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AddonSettingsServiceProxy,
    GetWheelPaymentMethodForViewDto,
    OmisePaymentResponseDto,
    SwipeCardServiceProxy,
    SwipeCardTopUpDto,
    TiffinMemberPortalServiceProxy,
    WheelPaymentMethodsServiceProxy
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import AdyenCheckout from '@adyen/adyen-web';
import '@adyen/adyen-web/dist/adyen.css';
import { AdyenState, PAYMENT_METHOD, SYSTEM_NAME } from '@app/portal/services/order.service';
import { ReadyToPayChangeResponse } from '@google-pay/button-angular';
import { DataApproved, PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { StripeHelper } from '@shared/helpers/StripeHelper';
import { QRCodeDialogComponent } from '../../cart-calculator/qr-code-dialog/qr-code-dialog.component';

declare let Stripe: any;
declare let OmiseCard: any;
declare let Omise: any;

@Component({
    selector: 'app-topup',
    templateUrl: './topup.component.html',
    styleUrls: ['./topup.component.scss', './payment-method.component.scss']
})
export class TopupComponent extends AppComponentBase implements OnInit {
    @ViewChild('QRCodeModal') QRCodeModal: QRCodeDialogComponent;

    topup = new SwipeCardTopUpDto();
    saving: boolean;
    omiseConfig = {
        publishKey: '',
        secretKey: '',
        enablePaynowQR: false
    };
    stripeConfig = {
        secretKey: '',
        publishKey: ''
    };
    adyenConfig = {
        apiKey: '',
        merchantAccount: '',
        environment: '',
        clientKey: ''
    };
    paypalConfig = {
        clientId: '',
        clientSecret: '',
        environment: 0
    };
    googlePayConfig = {
        merchantId: '',
        merchantName: '',
        publicKey: '',
        environment: ''
    };
    omiseData = {
        label: '',
        omiseToken: '',
        omisePaynowToken: ''
    };
    stripeData = {
        label: '',
        stripeToken: ''
    };
    paypalData = {
        label: '',
        orderId: null
    };
    adyenData = {
        label: '',
        adyenEncryptedCardNumber: '',
        adyenEncryptedExpiryMonth: '',
        adyenEncryptedExpiryYear: '',
        adyenEncryptedSecurityCode: ''
    };
    googlePayData = {
        label: '',
        googlePayToken: null
    };
    paymentRequest: any;
    payPalButtonId: 'payPalButton';
    checkoutAdyen: AdyenCheckout;
    customCard;
    stripe: any;
    paymentMethod: string;
    serviceFeePercentage: number;
    serviceFees = [];
    paynowToken = '';

    PAYMENT_METHOD = PAYMENT_METHOD;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _ngZone: NgZone,
        private _render: Renderer2,
        private _elRef: ElementRef,
        private _router: Router,
        private _swipeCardServiceProxy: SwipeCardServiceProxy,
        private _addonSettingsServiceProxy: AddonSettingsServiceProxy,
        private _wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
        private _paypalScriptService: PaypalScriptService,
        private _tiffinMemberPortalServiceProxy: TiffinMemberPortalServiceProxy
    ) {
        super(injector);

        this.topup.cardNumber = this._activatedRoute.snapshot.params['cardNumber'];
    }

    ngOnInit(): void {
        this._getPaymentMethods();
    }

    back() {
        this._router.navigate(['/app/portal/member/wallet']);
    }

    choosepaymentMethod() {
        this._getServiceCharge(this.paymentMethod);

        let els = this._elRef.nativeElement.querySelectorAll('div.payment-method');
        els.forEach((el) => {
            this._render.removeClass(el, 'selected');
        });

        if (this.paymentMethod === PAYMENT_METHOD.Omise || this.paymentMethod === PAYMENT_METHOD.Adyen) {
            els.forEach((el) => {
                this._render.addClass(el, 'selected');
            });
        }
    }

    payWithAdyen() {
        this._createTransaction(PAYMENT_METHOD.Adyen);
    }

    onLoadGooglePaymentData = (event: CustomEvent<google.payments.api.PaymentData>): void => {
        console.log('load payment data', event.detail);
        this.googlePayData.googlePayToken = event?.detail?.paymentMethodData?.tokenizationData?.token;

        this._createTransaction(PAYMENT_METHOD.Google);
    };

    onGooglePayError = (event: ErrorEvent): void => {
        console.error('error', event.error);
    };

    onGooglePaymentDataAuthorized: google.payments.api.PaymentAuthorizedHandler = (paymentData) => {
        console.log('payment authorized', paymentData);

        return {
            transactionState: 'SUCCESS'
        };
    };

    onReadyToPayChange = (event: CustomEvent<ReadyToPayChangeResponse>): void => {
        console.log('ready to pay change', event.detail);
    };

    onCloseQRCodeModal(response: OmisePaymentResponseDto) {
        if (response.isSuccess) {
            this.omiseData.omisePaynowToken = response.paymentId;
            this._createTransaction(PAYMENT_METHOD.Paynow);
        }
    }

    private _getPaymentMethods() {
        this._wheelPaymentMethodsServiceProxy.getAll(undefined, undefined, undefined, undefined, -1, -1, -1, -1, 1, undefined, 0, 1000).subscribe((result) => {
            this._getSettings(result.items);
            this._initPayment();
        });
    }

    private _getSettings(items: GetWheelPaymentMethodForViewDto[]) {
        items.map((item) => {
            switch (item.systemName) {
                case SYSTEM_NAME.Omise:
                    this.omiseConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Stripe:
                    this.stripeConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.Adyen:
                    this.adyenConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.PayPal:
                    this.paypalConfig = item.dynamicFieldData;
                    break;
                case SYSTEM_NAME.GooglePay:
                    this.googlePayConfig = item.dynamicFieldData;
                    break;

                default:
                    break;
            }
        });
    }

    private _initPayment() {
        this.stripeData.label = this.l('OnlinePayment') + ' - ' + this.l('Stripe');
        this.omiseData.label = this.l('OnlinePayment') + ' - ' + this.l('Omise');
        this.adyenData.label = this.l('OnlinePayment') + ' - ' + this.l('Adyen');
        this.paypalData.label = this.l('OnlinePayment') + ' - ' + this.l('PayPal');
        this.googlePayData.label = this.l('OnlinePayment') + ' - ' + this.l('GooglePay');

        if (this.stripeConfig?.publishKey) {
            this.initStripeElements();
        }
        if (this.omiseConfig?.publishKey) {
            this.initOmiseElements();
        }
        if (this.adyenConfig?.apiKey) {
            this.initAdyenElements();
        }
        if (this.paypalConfig?.clientId) {
            this.initPayPalElements();
        }
        if (this.googlePayConfig?.merchantId) {
            this.initGooglePayElements();
        }
    }

    private initPayPalElements() {
        const config = {
            clientId: this.paypalConfig.clientId,
            currency: this.appSession.application.currency,
            locale: 'en_SG',
            components: ['buttons', 'funding-eligibility']
        };
        this._paypalScriptService.registerScript(config, (payPal: any) => {
            this.initPayPalConfig(payPal);
        });
    }

    private initPayPalConfig(payPal) {
        const self = this;
        this._ngZone.runOutsideAngular(() => {
            payPal
                .Buttons({
                    fundingSource: payPal.FUNDING.PAYPAL,
                    createOrder: (data: any, actions: { order: any }) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    amount: {
                                        currency_code: this.appSession.application.currency,
                                        value: this.topup.paidAmount
                                    }
                                }
                            ]
                        });
                    },
                    onApprove: (data: DataApproved, actions: any) => {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function (details) {
                            // This function shows a transaction success message to your buyer.

                            self.paypalData.orderId = data.orderID;
                            self._ngZone.run(() => {
                                self._createTransaction(PAYMENT_METHOD.PayPal);
                            });
                        });
                    }
                })
                .render(`#${this.payPalButtonId}`);
        });
    }

    private initGooglePayElements() {
        this.paymentRequest = {
            environment: this.googlePayConfig.environment,
            buttonType: 'buy',
            buttonColor: 'default',
            apiVersion: 2,
            apiVersionMinor: 0,
            allowedPaymentMethods: [
                {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
                        allowedCardNetworks: ['MASTERCARD', 'VISA']
                    },
                    tokenizationSpecification: {
                        type: 'DIRECT',
                        parameters: {
                            protocolVersion: 'ECv2',
                            publicKey: this.googlePayConfig.publicKey
                        }
                    }
                }
            ],
            merchantInfo: {
                merchantId: this.googlePayConfig.merchantId,
                merchantName: this.googlePayConfig.merchantName
            }
        };
    }

    private initStripeElements() {
        if (this.stripeConfig.publishKey) {
            this.stripe = Stripe(this.stripeConfig.publishKey);

            let elements = this.stripe.elements({
                fonts: [
                    {
                        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro'
                    }
                ]
            });

            let elementStyles = {
                base: {
                    color: '#32325D',
                    fontWeight: 500,
                    fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                    fontSize: '16px',
                    fontSmoothing: 'antialiased',

                    '::placeholder': {
                        color: '#CFD7DF'
                    },
                    ':-webkit-autofill': {
                        color: '#e39f48'
                    }
                },
                invalid: {
                    color: '#E25950',

                    '::placeholder': {
                        color: '#FFCCA5'
                    }
                }
            };

            let elementClasses = {
                focus: 'focused',
                empty: 'empty',
                invalid: 'invalid'
            };

            let cardNumber = elements.create('cardNumber', {
                style: elementStyles,
                classes: elementClasses
            });
            cardNumber.mount('#stripe-card-number');

            let cardExpiry = elements.create('cardExpiry', {
                style: elementStyles,
                classes: elementClasses
            });
            cardExpiry.mount('#stripe-card-expiry');

            let cardCvc = elements.create('cardCvc', {
                style: elementStyles,
                classes: elementClasses
            });

            cardCvc.mount('#stripe-card-cvc');

            let self = this;
            StripeHelper.registerElements(this.stripe, [cardNumber, cardExpiry, cardCvc], 'stripe-check-out-container', function (token) {
                self.stripeData.stripeToken = token.id;
                self._createTransaction(PAYMENT_METHOD.Stripe);
            });
        }
    }

    private initOmiseElements() {
        OmiseCard.configure({
            publicKey: this.omiseConfig.publishKey
        });
        Omise.setPublicKey(this.omiseConfig.publishKey);

        let button = document.querySelector('#checkoutButton');
        if (!button) {
            return;
        }

        button.addEventListener('click', (event) => {
            event.preventDefault();

            let self = this;

            OmiseCard.open({
                amount: this.topup.paidAmount * 100,
                currency: this.appSession.application.currency,
                defaultPaymentMethod: 'credit_card',
                onCreateTokenSuccess: (nonce) => {
                    if (nonce.startsWith('tokn_')) {
                        this.omiseData.omiseToken = nonce;

                        self._createTransaction(PAYMENT_METHOD.Omise);
                    }
                }
            });
        });

        let myButton = document.querySelector('#payNow');
        if (!myButton || !this.omiseConfig?.enablePaynowQR) {
            return;
        }

        myButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (!this.isValidBillingInformation()) {
                return;
            }

            this._tiffinMemberPortalServiceProxy
                .createOmisePaymentNowRequest(this.topup.paidAmount * 100, this.appSession.application.currency)
                .subscribe((result) => {
                    console.log(result);
                    const response = result ? JSON.parse(result) : null;

                    if (response && response.source && response.source.scannable_code && response.source.scannable_code.image) {
                        this.QRCodeModal.show(response.source.scannable_code.image.download_uri, response.id, this.topup.paidAmount);
                    }
                });
        });
    }

    private isValidBillingInformation() {
        return true;
    }

    private initAdyenElements() {
        const configuration = {
            clientKey: this.adyenConfig.clientKey,
            locale: 'en-US',
            environment: this.adyenConfig.environment,
            amount: {
                currency: this.appSession.application.currency,
                value: this.topup.paidAmount * 100
            },
            countryCode: 'SG'
        };

        this.checkoutAdyen = new AdyenCheckout(configuration);
        this.customCard = this.checkoutAdyen
            .create('securedfields', {
                // Optional configuration
                type: 'card',
                brands: ['mc', 'visa', 'amex', 'bcmc', 'maestro'],
                styles: {
                    base: {
                        color: 'black',
                        fontSize: '14px',
                        fontFamily: 'Helvetica'
                    },
                    error: {
                        color: 'red'
                    },
                    validated: {
                        color: 'green'
                    },
                    placeholder: {
                        color: '#d8d8d8'
                    }
                },
                ariaLabels: {
                    lang: 'en-GB',
                    encryptedCardNumber: {
                        label: 'Credit or debit card number field'
                    }
                },
                // Events
                onChange: (state: AdyenState, component) => {
                    const data = state.data.paymentMethod;

                    if (state?.data?.paymentMethod) {
                        this.adyenData.adyenEncryptedCardNumber = data.encryptedCardNumber;
                        this.adyenData.adyenEncryptedExpiryMonth = data.encryptedExpiryMonth;
                        this.adyenData.adyenEncryptedExpiryYear = data.encryptedExpiryYear;
                        this.adyenData.adyenEncryptedSecurityCode = data.encryptedSecurityCode;
                    }
                }
            })
            .mount('#customCard-container');
    }

    private _createTransaction(paymentOption?: string) {
        this.saving = true;

        this.topup.paymentOption = paymentOption || null;
        this.topup.source = '101';

        switch (paymentOption) {
            case PAYMENT_METHOD.Omise:
                {
                    this.topup.omiseToken = this.omiseData.omiseToken;
                }

                break;
            case PAYMENT_METHOD.Paynow:
                {
                    this.topup.omiseToken = this.omiseData.omisePaynowToken;
                }

                break;
            case PAYMENT_METHOD.Stripe:
                {
                    this.topup.stripeToken = this.stripeData.stripeToken;
                }

                break;
            case PAYMENT_METHOD.Adyen:
                {
                    this.topup.adyenEncryptedCardNumber = this.adyenData.adyenEncryptedCardNumber;
                    this.topup.adyenEncryptedExpiryMonth = this.adyenData.adyenEncryptedExpiryMonth;
                    this.topup.adyenEncryptedExpiryYear = this.adyenData.adyenEncryptedExpiryYear;
                    this.topup.adyenEncryptedSecurityCode = this.adyenData.adyenEncryptedSecurityCode;
                }

                break;
            case PAYMENT_METHOD.PayPal:
                {
                    this.topup.paypalOrderId = this.paypalData.orderId;
                }

                break;
            case PAYMENT_METHOD.Google:
                {
                    this.topup.googlePayToken = this.googlePayData.googlePayToken;
                }

                break;
        }
        this._swipeCardServiceProxy.topUp(this.topup)
                .pipe(finalize(() => (this.saving = false)))
                .subscribe((result) => {
                    if (result) {
                        this.message.success(this.l('Topup Successfully'), '', {
                            timer: 1500
                        });

                        setTimeout(() => {
                            this._router.navigate(['/app/portal/member/wallet']);
                        }, 700);
                    }
                });
    }

    private _getServiceCharge(name) {
        this.serviceFeePercentage = 0;
        this._addonSettingsServiceProxy.getServiceCharge(name).subscribe((result) => {
            this.serviceFees = result || [];
            this.serviceFees.map((item) => {
                this.serviceFeePercentage = this.serviceFeePercentage + item.percentage;
            });
        });
    }
}

import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CustomerCardDto, SwipeCardServiceProxy } from '@shared/service-proxies/service-proxies';
import { TransactionComponent } from './transaction/transaction.component';

@Component({
    templateUrl: './wallet.component.html',
    styleUrls: ['./wallet.component.scss'],
    animations: [appModuleAnimation()]
})
export class WalletComponent extends AppComponentBase implements OnInit {
    @ViewChild('TransactionModal', { static: true }) transactionComponent: TransactionComponent;

    cardList: CustomerCardDto[] = [];

    constructor(injector: Injector, private _router: Router, private _swipeCardServiceProxy: SwipeCardServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this._getWalletList();
    }

    topup(cardNumber: string) {
        this._router.navigate(['app/portal/member/topup', cardNumber]);
    }

    viewTransaction(cardId: number) {
        this.transactionComponent.show(cardId);
    }

    private _getWalletList() {
        this._swipeCardServiceProxy.getAllCustomerCard(this.appSession.customerId, undefined, undefined, 'id', 0, 1000).subscribe((result) => {
            this.cardList = result.items;
        });
    }
}

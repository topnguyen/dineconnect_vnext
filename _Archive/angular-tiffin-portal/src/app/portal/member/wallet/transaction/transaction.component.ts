import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { SwipeCardDto, SwipeCardServiceProxy, SwipeCardTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { LazyLoadEvent } from 'primeng';


@Component({
    selector: 'app-transaction-modal',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent extends AppComponentBase {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('balanceModal', { static: true }) balanceModal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    swipeCardId: number;
    swipeCard = new SwipeCardDto();
    cardTypes: SwipeCardTypeDto[] = [];

    constructor(
        injector: Injector,
        private _swipeCardServiceProxy: SwipeCardServiceProxy,
    ) {
        super(injector);
    }

    show(cardId?: number): void {
        if (cardId) {
            this.swipeCardId = cardId;
            this.getAll();
            this._getCard();
            this.balanceModal.show();
        }
    }

    getAll(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._swipeCardServiceProxy
            .getAllTransaction(
                this.swipeCardId,
                this.primengTableHelper.getSorting(this.dataTable) || 'id DESC',
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    refresh() {
        this.paginator.changePage(0);
    }

    search() {
        this.getAll();
    }

    private _getCard(){
        this._swipeCardServiceProxy.get(this.swipeCardId).subscribe(result => {
            this.swipeCard = result;
        })
    }

    close(): void {
        this.balanceModal.hide();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortalRoutingModule } from './portal-routing.module';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';

import {
    CustomersServiceProxy,
    CommonServiceProxy,
    ProductSetsServiceProxy,
    TiffinMemberPortalServiceProxy,
    ScheduleServiceProxy,
    TiffinOrderServiceProxy,
    EmailNotificationBackgroundJobServiceServiceProxy,
    TiffinPromotionServiceProxy,
    WheelPaymentMethodsServiceProxy,
    TiffinMealTimesServiceProxy,
    DeliveryTimeSlotServiceProxy,
    LocationServiceProxy,
    WheelServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { LoginService } from 'portal/services/login.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { BuyOfferComponent } from './member/buy-offer/buy-offer.component';
import { PaymentCompletedComponent } from './member/payment/payment-completed.component';
import { PaymentOrderCompletedComponent } from './member/payment/payment-order-completed.component';
import { CheckboxModule, TableModule, PaginatorModule, RadioButtonModule, DataViewModule, CardModule, ButtonModule } from 'primeng';

import { ProductService } from '@app/shared/services/product.service';
import { CartProductsComponent } from './member/cart-products/cart-products.component';
import { CartCalculatorComponent } from './member/cart-calculator/cart-calculator.component';
import { CheckoutComponent } from './member/checkout/checkout.component';
import { ProfileComponent } from './member/profile/profile.component';
import { BalanceComponent } from './member/balance/balance.component';
import { ChangePasswordComponent } from './member/profile/change-password/change-password.component';
import { DetailProfileComponent } from './member/profile/detail-profile/detail-profile.component';
import { OrderHistoryComponent } from './member/order-history/order-history.component';
import { PaymentHistoryComponent } from './member/payment-history/payment-history.component';

import { AddressProfileComponent } from './member/profile/address-profile/address-profile.component';
import { PaymentDetailComponent } from './member/payment-history/payment-detail.component';
import { ScheduleComponent } from './member/schedule/schedule.component';
import { CreateAddressProfileComponent } from './member/profile/address-profile/create-address-profile/create-address-profile.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FileUploadModule } from 'ng2-file-upload';
import { AgmCoreModule } from "@agm/core";
import { AddPromoCodeDialogComponent } from './member/cart-calculator/add-promo-code-dialog/add-promo-code-dialog.component';
import { ScriptLoadingService } from '@app/shared/services/script-loading.service';
import { PaypalScriptService } from '@app/shared/services/paypal-script.service';
import { AddonDialogComponent } from './member/orders/addon-dialog/addon-dialog.component';
import { AppConsts } from '@shared/AppConsts';
import { GooglePayButtonModule } from '@google-pay/button-angular';
import { OrderComponent } from './member/orders/order.component';
import { EditOrderModalComponent } from './member/orders/edit-order-modal/edit-order-modal.component';
import { QRCodeDialogComponent } from './member/cart-calculator/qr-code-dialog/qr-code-dialog.component';
import { WalletComponent } from './member/wallet/wallet.component';
import { TopupComponent } from './member/wallet/topup/topup.component';
import { TransactionComponent } from './member/wallet/transaction/transaction.component';
import { CollectComponent } from './admin/collect/collect.component';

@NgModule({
    declarations: [
        BalanceComponent,
        BuyOfferComponent,
        PaymentCompletedComponent,
        PaymentOrderCompletedComponent,
        CartProductsComponent,
        CartCalculatorComponent,
        CheckoutComponent,
        ProfileComponent,
        ChangePasswordComponent,
        DetailProfileComponent,
        OrderHistoryComponent,
        PaymentHistoryComponent,
        OrderComponent,
        EditOrderModalComponent,
        AddressProfileComponent,
        PaymentDetailComponent,
        ScheduleComponent,
        CreateAddressProfileComponent,
        AddPromoCodeDialogComponent,
        AddonDialogComponent,
        QRCodeDialogComponent,
        WalletComponent,
        TopupComponent,
        TransactionComponent,
        CollectComponent
    ],
    imports: [
        CommonModule,
        PortalRoutingModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        FileUploadModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        AppBsModalModule,
        TimepickerModule.forRoot(),
        OAuthModule.forRoot(),
        CollapseModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: AppConsts.GOOGLE_MAP_KEY,
            libraries: ["drawing"],
        }),
        GooglePayButtonModule,
        TableModule,
        PaginatorModule,
        CheckboxModule,
        RadioButtonModule,
        DataViewModule,
        CardModule,
        ButtonModule
    ],
    providers: [
        CustomersServiceProxy,
        LoginService,
        CommonServiceProxy,
        ProductSetsServiceProxy,
        TiffinMemberPortalServiceProxy,
        ProductService,
        ScheduleServiceProxy,
        TiffinOrderServiceProxy,
        TiffinPromotionServiceProxy,
        EmailNotificationBackgroundJobServiceServiceProxy,
        WheelPaymentMethodsServiceProxy,
        ScriptLoadingService,
        PaypalScriptService,
        TiffinMealTimesServiceProxy,
        DeliveryTimeSlotServiceProxy,
        LocationServiceProxy,
        WheelServiceProxy,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig,
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig,
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale,
        },
    ],
})
export class PortalModule {}

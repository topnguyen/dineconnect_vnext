import { Component, OnInit } from '@angular/core';

import { TenantSettingsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-term-and-conditions',
    templateUrl: './term-and-conditions.component.html',
    styleUrls: ['./term-and-conditions.component.scss']
})
export class TermAndConditionsComponent implements OnInit {
    tContent: string;

    constructor(
        private _tenantSettingsServiceProxy: TenantSettingsServiceProxy
    ) { }

    ngOnInit() {
        this._tenantSettingsServiceProxy.getAllSettings().subscribe(settings => {
            this.tContent = settings.termsAndCondition.content;
        });
    }
}

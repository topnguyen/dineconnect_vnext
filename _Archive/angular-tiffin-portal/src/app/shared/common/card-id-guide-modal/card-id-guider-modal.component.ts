import { Component, ViewChild } from '@angular/core';
import { TenantSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-card-id-guider-modal',
    templateUrl: './card-id-guider-modal.component.html',
    styleUrls: ['./card-id-guider-modal.component.scss']
})
export class CardIdGuiderModalComponent  {
    @ViewChild('CardIdGuiderModal', { static: true }) modal: ModalDirective;

    url: string;
    constructor(
        private _tenantSettingsServiceProxy: TenantSettingsServiceProxy
    ) {
        this._tenantSettingsServiceProxy.getAllSettings().subscribe(settings => {
            this.url = settings.signUpForm?.hintImageUrl;
        });
    }

    close(): void {
        this.modal.hide();
    }

    show(): void {
        this.modal.show();
    }
}


import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    CanLoad,
    Router,
    RouterStateSnapshot,
} from "@angular/router";
import { AppSessionService } from "@shared/common/session/app-session.service";
import { UrlHelper } from "@shared/helpers/UrlHelper";
import { Observable } from "@node_modules/rxjs/internal/Observable";
import { of, Subject } from "rxjs";
import { PermissionCheckerService, RefreshTokenService } from "abp-ng2-module";

@Injectable()
export class PortalRouteGuard
    implements CanActivate, CanActivateChild, CanLoad {
    constructor(
        private _permissionChecker: PermissionCheckerService,
        private _router: Router,
        private _sessionService: AppSessionService,
        private _refreshTokenService: RefreshTokenService
    ) {}

    canActivateInternal(
        data: any,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        if (!this._sessionService.user) {
            return of(true);
        }

        this._router.navigate([this.selectBestRoute()]);
        return of(false);
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.canActivateInternal(route.data, state);
    }

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.canActivate(route, state);
    }

    canLoad(route: any): Observable<boolean> | Promise<boolean> | boolean {
        return this.canActivateInternal(route.data, null);
    }

    selectBestRoute(): string {
        return "/app/portal/member/buy-offer";
    }
}

import { Injector, Component, OnInit, ViewEncapsulation, TemplateRef, AfterViewChecked } from '@angular/core';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';
import { AppConsts } from '@shared/AppConsts';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import {
    ChangeUserLanguageDto,
    LinkedUserDto,
    ProfileServiceProxy,
    UserLinkServiceProxy,
    UserDefaultLocationDto,
    DefaultLocationServiceProxy
} from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ProductService } from '../services/product.service';
import { NavigationEnd, Router } from '@angular/router';
import { AbpSessionService, AbpMultiTenancyService } from 'abp-ng2-module';

declare let $ : any;

@Component({
    selector: 'topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TopBarComponent extends ThemesLayoutBaseComponent implements OnInit, AfterViewChecked {
    isHost = false;
    languages: abp.localization.ILanguageInfo[];
    currentLanguage: abp.localization.ILanguageInfo;
    isImpersonatedLogin = false;
    isMultiTenancyEnabled = false;
    shownLoginName = '';
    tenancyName = '';
    userName = '';
    profilePicture = AppConsts.appBaseUrl + '/assets/common/images/default-profile-picture.png';
    defaultLogo = AppConsts.appBaseUrl + '/assets/common/images/app-logo-on-' + this.currentTheme.baseSettings.menu.asideSkin + '.svg';
    recentlyLinkedUsers: LinkedUserDto[];
    unreadChatMessageCount = 0;
    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;
    chatConnected = false;
    isQuickThemeSelectEnabled: boolean = this.setting.getBoolean('App.UserManagement.IsQuickThemeSelectEnabled');
    userDefaultOrganizationListDto: UserDefaultLocationDto;
    defaultLocationSelected = 0;
    subscription: Subscription;
    cartProductsCount = 0;
    collapse = false;
    showSub = false;
    isBackgroudYellow = false;

    constructor(
        injector: Injector,
        private _router: Router,
        private _abpSessionService: AbpSessionService,
        private _abpMultiTenancyService: AbpMultiTenancyService,
        private _profileServiceProxy: ProfileServiceProxy,
        private _userLinkServiceProxy: UserLinkServiceProxy,
        private _authService: AppAuthService,
        private _defaultLocationServiceProxy: DefaultLocationServiceProxy,
        public productService: ProductService
    ) {
        super(injector);
    }

    ngOnInit() {
        this.isBackgroudYellow = this._router.url.includes('/order') || this._router.url.includes('/buy-offer') ;

        this._router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.isBackgroudYellow = this._router.url.includes('/order') || this._router.url.includes('/buy-offer');
            }
        });

        this.isHost = !this._abpSessionService.tenantId;
        this.isMultiTenancyEnabled = this._abpMultiTenancyService.isEnabled;
        this.languages = _.filter(this.localization.languages, (l) => l.isDisabled === false);
        this.currentLanguage = this.localization.currentLanguage;
        this.isImpersonatedLogin = this._abpSessionService.impersonatorUserId > 0;
        this.setCurrentLoginInformations();
        this.getProfilePicture();
        this.getRecentlyLinkedUsers();
        this.cartProductsCount = this.productService.getLocalCartProducts().reduce((count, p) => count + p.quantity, 0);

        this.registerToEvents();
        this.getUserInfo();
    }

    ngAfterViewChecked() {
        $('.nav-link').click(function (e) {
            if (window.innerWidth <= 768) {
                $('.navbar-toggler-icon').trigger('click');
            }
        });
    }

    registerToEvents() {
        abp.event.on('profilePictureChanged', () => {
            this.getProfilePicture();
        });

        abp.event.on('app.chat.unreadMessageCountChanged', (messageCount) => {
            this.unreadChatMessageCount = messageCount;
        });

        abp.event.on('app.chat.connected', () => {
            this.chatConnected = true;
        });

        abp.event.on('app.getRecentlyLinkedUsers', () => {
            this.getRecentlyLinkedUsers();
        });

        abp.event.on('app.onMySettingsModalSaved', () => {
            this.onMySettingsModalSaved();
        });

        abp.event.on('app.chat.cartProductsCountChanged', (cartProductsCount) => {
            this.cartProductsCount = cartProductsCount;
        });

        this._router.events.subscribe(() => (this.collapse = false));
    }

    changeLanguage(languageName: string): void {
        const input = new ChangeUserLanguageDto();
        input.languageName = languageName;

        this._profileServiceProxy.changeLanguage(input).subscribe(() => {
            abp.utils.setCookieValue(
                'Abp.Localization.CultureName',
                languageName,
                new Date(new Date().getTime() + 5 * 365 * 86400000), //5 year
                abp.appPath
            );

            window.location.reload();
        });
    }

    setCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
    }

    getShownUserName(linkedUser: LinkedUserDto): string {
        if (!this._abpMultiTenancyService.isEnabled) {
            return linkedUser.username;
        }

        return (linkedUser.tenantId ? linkedUser.tenancyName : '.') + '\\' + linkedUser.username;
    }

    getProfilePicture(): void {
        this._profileServiceProxy.getProfilePicture().subscribe((result) => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            }
        });
    }

    getRecentlyLinkedUsers(): void {
        this._userLinkServiceProxy.getRecentlyUsedLinkedUsers().subscribe((result) => {
            this.recentlyLinkedUsers = result.items;
        });
    }

    showLoginAttempts(): void {
        abp.event.trigger('app.show.loginAttemptsModal');
    }

    showLinkedAccounts(): void {
        abp.event.trigger('app.show.linkedAccountsModal');
    }

    changePassword(): void {
        abp.event.trigger('app.show.changePasswordModal');
    }

    changeProfilePicture(): void {
        abp.event.trigger('app.show.changeProfilePictureModal');
    }

    changeMySettings(): void {
        abp.event.trigger('app.show.mySettingsModal');
    }

    logout(): void {
        this._authService.logout();
        this.productService.removeAllLocalCartProducts();
    }

    onMySettingsModalSaved(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
    }

    backToMyAccount(): void {}

    downloadCollectedData(): void {
        this._profileServiceProxy.prepareCollectedData().subscribe(() => {
            this.message.success(this.l('GdprDataPrepareStartedNotification'));
        });
    }

    getUserInfo() {
        this._defaultLocationServiceProxy
            .getUserDefaultLocation()
            .pipe(finalize(() => {}))
            .subscribe((result) => {
                this.userDefaultOrganizationListDto = result;
                if (result.defaultLocationId > 0) {
                    this.defaultLocationSelected = result.defaultLocationId;
                }
            });
    }
    setDefaultLocation(template: TemplateRef<any>, id?: number) {}

    mouseLeave() {
        this.collapse = false;
    }

    openWhatsApp() {
        window.open('https://wa.me/+6598588841'); 
    }
}

import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ReCaptchaV3Service } from 'ngx-captcha';
import { AppConsts } from "@shared/AppConsts";
import {
    LoginService,
    ExternalLoginProvider,
} from "portal/services/login.service";
import { Router } from "@angular/router";
import { AbpSessionService } from "abp-ng2-module";
import {
    SessionServiceProxy,
    UpdateUserSignInTokenOutput,
    TenantSettingsEditDto,
    TenantSettingsServiceProxy,
    TiffinMemberPortalServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { UrlHelper } from "@shared/helpers/UrlHelper";
import * as CryptoJS from "crypto-js";

@Component({
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    animations: [appModuleAnimation()],
})
export class LoginComponent extends AppComponentBase implements OnInit {

    submitting = false;
    isMultiTenancyEnabled: boolean = this.multiTenancy.isEnabled;
    recaptchaSiteKey: string = AppConsts.recaptchaSiteKey;
    captchaResponse?: string;
    active = true;
    saving = false;
    settings: TenantSettingsEditDto = undefined;
    urlLogo: string;
    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
        private _sessionService: AbpSessionService,
        private _sessionAppService: SessionServiceProxy,
        private _tenantSettingsService: TenantSettingsServiceProxy,
        public _portalServiceProxy: TiffinMemberPortalServiceProxy,
        private _reCaptchaV3Service: ReCaptchaV3Service
    ) {
        super(injector);
    }

    get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }

    get isTenantSelfRegistrationAllowed(): boolean {
        return this.setting.getBoolean(
            "App.TenantManagement.AllowSelfRegistration"
        );
    }

    get isSelfRegistrationAllowed(): boolean {
        if (!this._sessionService.tenantId) {
            return false;
        }

        return this.setting.getBoolean(
            "App.UserManagement.AllowSelfRegistration"
        );
    }

    ngOnInit(): void {
        this.getSettings();
        if (
            this._sessionService.userId > 0 &&
            UrlHelper.getReturnUrl() &&
            UrlHelper.getSingleSignIn()
        ) {
            this._sessionAppService
                .updateUserSignInToken()
                .subscribe((result: UpdateUserSignInTokenOutput) => {
                    const initialReturnUrl = UrlHelper.getReturnUrl();
                    const returnUrl =
                        initialReturnUrl +
                        (initialReturnUrl.indexOf("?") >= 0 ? "&" : "?") +
                        "accessToken=" +
                        result.signInToken +
                        "&userId=" +
                        result.encodedUserId +
                        "&tenantId=" +
                        result.encodedTenantId;

                    location.href = returnUrl;
                });
        }

        let state = UrlHelper.getQueryParametersUsingHash().state;
        if (state && state.indexOf("openIdConnect") >= 0) {
            this.loginService.openIdConnectLoginCallback({});
        }

        this.getUserInfor();
    }

    getUserInfor() {
        let data = localStorage.getItem("tiffin");
        if (data) {
            let infor = JSON.parse(data);
            this.loginService.authenticateModel.userNameOrEmailAddress = CryptoJS.AES.decrypt(
                infor.a.trim(),
                "123qwe"
            ).toString(CryptoJS.enc.Utf8);
            this.loginService.authenticateModel.password = CryptoJS.AES.decrypt(
                infor.b.trim(),
                "123qwe"
            ).toString(CryptoJS.enc.Utf8);
            this.loginService.rememberMe = true;
        }
    }

    getSettings(): void {
        this._tenantSettingsService
            .getCompanyLogoForCustomerSetting()
            .subscribe((result) => {
                this.urlLogo = result.companyLogoUrl;
            });
    }

    login(): void {
        const initialReturnUrl = UrlHelper.getReturnUrl2();
        let recaptchaCallback = (token: string) => {
            this.showMainSpinner();

            this.submitting = true;
            this.loginService.authenticate(
                () => {
                    this.submitting = false;
                    this.hideMainSpinner();
                },
                initialReturnUrl || "/app/portal/member/buy-offer",
                token
            );
        };

        if (this.useCaptcha) {
            this._reCaptchaV3Service.execute(this.recaptchaSiteKey, 'login', (token) => {
                recaptchaCallback(token);
            });
        } else {
            recaptchaCallback(null);
        }
    }

    externalLogin(provider: ExternalLoginProvider) {
        this.loginService.externalAuthenticate(provider);
    }

    get useCaptcha(): boolean {
        return this.setting.getBoolean("App.UserManagement.UseCaptchaOnLogin");
    }

    captchaResolved(captchaResponse: string): void {
        this.captchaResponse = captchaResponse;
    }

    createAccount() {
        this._router.navigate(['/portal/register'])
    }
}

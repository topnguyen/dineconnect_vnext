import { Component, Injector } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    SendPasswordResetCodeInput,
    AccountServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { Router } from "@angular/router";
import { finalize } from "rxjs/operators";
import { AppConsts } from "@shared/AppConsts";

@Component({
    templateUrl: "./forgot-password.component.html",
    styleUrls: ["./forgot-password.component.scss"],
    animations: [appModuleAnimation()],
})
export class ForgotPasswordComponent extends AppComponentBase {
    model = new SendPasswordResetCodeInput({domain: AppConsts.appBaseUrl, emailAddress: ''});
    saving = false;

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    save(): void {
        this.saving = true;
        this._accountService
            .sendPasswordResetCodeTiffin(this.model)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.message
                    .success(
                        this.l("PasswordResetMailSentMessage"),
                        this.l("MailSent")
                    )
                    .then(() => {
                        this._router.navigate(["portal/login"]);
                    });
            });
    }
}

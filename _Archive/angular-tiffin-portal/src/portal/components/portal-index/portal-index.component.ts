import { AfterViewChecked, AfterViewInit, Component, Injector, OnInit } from '@angular/core';

import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { finalize } from 'rxjs/operators';

import { AppComponentBase } from '@shared/common/app-component-base';

import { FaqListDto, TiffinFaqServiceProxy, TiffinHomeBannerServiceProxy } from '@shared/service-proxies/service-proxies';

declare const $: any;

@Component({
    selector: 'app-portal-index',
    templateUrl: './portal-index.component.html',
    styleUrls: ['./portal-index.component.scss'],
    providers: [
        {
            provide: CarouselConfig,
            useValue: { interval: 5000, noPause: true, showIndicators: false },
        },
    ],
})
export class PortalIndexComponent extends AppComponentBase implements OnInit, AfterViewChecked {
    questions: FaqListDto[];
    bannerList: string[] = [];

    constructor(
        injector: Injector,
        private _tiffinFaqServiceProxy: TiffinFaqServiceProxy,
        private _tiffinHomeBannerServiceProxy: TiffinHomeBannerServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getFaqs();
        this.getHomeBanner();
    }

    ngAfterViewChecked() {
        const slider = $('.index-slider');
        if (!slider.hasClass('slick-initialized')) {
            return slider.slick({
                dots: true,
                arrows: false,
                infinite: false,
                dotsClass: 'slick-custom-dots'
            });
        }
        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
             $(".slick-custom-dots li").removeClass("active");
             $(".slick-custom-dots li").eq(nextSlide).prevAll().addClass("active");
      });
    }

    getFaqs(): void {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinFaqServiceProxy.getFaqs()
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.questions = result.items.filter(item => item.isDisplayAtHomePage);
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    getHomeBanner() {
        this.primengTableHelper.showLoadingIndicator();
        this._tiffinHomeBannerServiceProxy.getHomeBannerForEdit()
            .subscribe(result => {
                if (result.images) {
                    this.bannerList = result.images.split(',');
                }
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
}

import { NgModule } from '@angular/core';
import { PortalRoutingModule } from './portal-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { UtilsModule } from '@shared/utils/utils.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { AppModule } from '@app/app.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { PortalRouteGuard } from '@app/shared/common/auth/portal-route-guard';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { DineConnectCommonModule } from '@shared/common/common.module';
import { PortalComponent } from './components/portal/portal.component';
import { ViewOffersComponent } from './components/view-offers/view-offers.component';
import { PortalIndexComponent } from './components/portal-index/portal-index.component';
import { LoginComponent } from './components/login/login.component';
import { CreateMemberComponent } from './components/create-member/create-member.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ReCaptchaV3Service, ScriptService } from 'ngx-captcha';
import { LoginService } from './services/login.service';
import { CommonServiceProxy, CustomersServiceProxy } from '@shared/service-proxies/service-proxies';
import { AccountRouteGuard } from '@shared/auth/account-route-guard';
import { PasswordModule } from 'primeng';

@NgModule({
    imports: [
        PortalRoutingModule,
        AppCommonModule,
        CommonModule,
        DineConnectCommonModule,
        UtilsModule,
        ServiceProxyModule,
        ReactiveFormsModule,
        FormsModule,
        AppModule,
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CarouselModule.forRoot(),
        PasswordModule
    ],
    declarations: [
        PortalComponent,
        ViewOffersComponent,
        PortalIndexComponent,
        LoginComponent,
        CreateMemberComponent,
        ResetPasswordComponent,
        ForgotPasswordComponent,
    ],
    providers: [
        ScriptService,
        ReCaptchaV3Service,
        LoginService,
        CustomersServiceProxy,
        CommonServiceProxy,
        AccountRouteGuard,
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        },
        PortalRouteGuard
    ]
})
export class PortalModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AutoFocusDirective } from './directives/auto-focus.directive';
import { FileDownloadService } from './services/file-download.service';
import { FriendProfilePictureComponent } from './components/friend-profile-picture.component';
import { MomentFormatPipe } from './pipes/moment-format.pipe';
import { ValidationMessagesComponent } from './validation-messages.component';
import { EqualValidator } from './validation/equal-validator.directive';
import { PasswordComplexityValidator } from './validation/password-complexity-validator.directive';
import { LocalizePipe } from '@shared/common/pipes/localize.pipe';
import { PermissionPipe } from '@shared/common/pipes/permission.pipe';
import { PermissionAnyPipe } from '@shared/common/pipes/permission-any.pipe';
import { PermissionAllPipe } from '@shared/common/pipes/permission-all.pipe';
import { FeatureCheckerPipe } from '@shared/common/pipes/feature-checker.pipe';
import { SafePipe } from '@shared/common/pipes/safe-html.pipe';
import { LocalStorageService } from './services/local-storage.service';
import { ScriptLoaderService } from './services/script-loader.service';
import { StyleLoaderService } from './services/style-loader.service';
import { ButtonBusyDirective } from './directives/button-busy.directive';
import { BusyIfDirective } from './directives/busy-if.directive';
import { MomentFromNowPipe } from './pipes/moment-from-now.pipe';
import { NullDefaultValueDirective } from './directives/null-value.directive';
import { DatePickerMomentModifierDirective } from './directives/date-picker-moment-modifier.directive';
import { DateRangePickerMomentModifierDirective } from './directives/date-range-picker-moment-modifier.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        FileDownloadService,
        LocalStorageService,
        ScriptLoaderService,
        StyleLoaderService,
    ],
    declarations: [
        EqualValidator,
        PasswordComplexityValidator,
        ButtonBusyDirective,
        AutoFocusDirective,
        BusyIfDirective,
        FriendProfilePictureComponent,
        MomentFormatPipe,
        MomentFromNowPipe,
        ValidationMessagesComponent,
        NullDefaultValueDirective,
        LocalizePipe,
        PermissionPipe,
        PermissionAnyPipe,
        FeatureCheckerPipe,
        DatePickerMomentModifierDirective,
        DateRangePickerMomentModifierDirective,
        SafePipe,
        PermissionAllPipe    
    ],
    exports: [
        EqualValidator,
        PasswordComplexityValidator,
        ButtonBusyDirective,
        AutoFocusDirective,
        BusyIfDirective,
        FriendProfilePictureComponent,
        MomentFormatPipe,
        MomentFromNowPipe,
        ValidationMessagesComponent,
        NullDefaultValueDirective,
        LocalizePipe,
        PermissionPipe,
        PermissionAnyPipe,
        FeatureCheckerPipe,
        DatePickerMomentModifierDirective,
        DateRangePickerMomentModifierDirective,
        SafePipe,
        PermissionAllPipe   
    ]
})
export class UtilsModule { }

$(document).ready(function () {
    // quantity increaser
    var minVal = 1, maxVal = 20; // Set Max and Min values
    // Increase product quantity on cart page
    $(".increaseQty").on('click', function (e) {
        e.preventDefault();
        var $parentElm = $(this).parents(".qtySelector");
        $(this).addClass("clicked");
        setTimeout(function () {
            $(".clicked").removeClass("clicked");
        }, 100);
        var value = $parentElm.find(".qtyValue").val();
        if (value < maxVal) {
            value++;
        }
        $parentElm.find(".qtyValue").val(value);
    });
    // Decrease product quantity on cart page
    $(".decreaseQty").on('click', function (e) {
        e.preventDefault();
        var $parentElm = $(this).parents(".qtySelector");
        $(this).addClass("clicked");
        setTimeout(function () {
            $(".clicked").removeClass("clicked");
        }, 100);
        var value = $parentElm.find(".qtyValue").val();
        if (value > 1) {
            value--;
        }
        $parentElm.find(".qtyValue").val(value);
    });

    var selfpickup = $(".selfpickup__main__body");
    var delivery = $(".delivery__main__body");

    // console.log(selfpickup);
    // console.log(delivery);

    // if (delivery.hasClass("location__state_active")) {
    //     console.log("succes");
    // }
    var deliveryCheck = $(".deliveryCheckbox");
    var selfpickupCheck = $(".selfPickupCheckbox");

    $(selfpickupCheck).click(function (e) {
        $(selfpickupCheck).addClass("selfPickupCheckbox___active");
        $(deliveryCheck).removeClass("deliveryCheckbox___active");
        $(deliveryCheck).find('input').removeAttr('checked');
        $(this).find('input').attr('checked', true);
        $(delivery).removeClass("location__state_active");
        $(selfpickup).addClass("location__state_active");
    });

    $(deliveryCheck).click(function (e) {
        $(deliveryCheck).addClass("deliveryCheckbox___active");
        $(selfpickupCheck).removeClass("selfPickupCheckbox___active");
        $(selfpickupCheck).find('input').removeAttr('checked');
        $(this).find('input').attr('checked', true);
        $(selfpickup).removeClass("location__state_active");
        $(delivery).addClass("location__state_active");
    });

    $(".addNewCardbtn").click(function (e) {
        e.preventDefault();
        $(".addnewcard").addClass("addnewcard__active");
    });

    $(".cancelcard").click(function (e) {
        e.preventDefault();
        $(".addnewcard").removeClass("addnewcard__active");
    });


    var addNewCard = document.getElementById('addNewCard');
    if (addNewCard != null && addNewCard.length > 1) {
        $("#addNewCard").validate({
            rules: {
                cardnumber: {
                    required: true,
                    maxlength: 16
                },
                cardname: {
                    required: true,

                },
                month: {
                    required: true,

                },
                year: {
                    required: true,
                    maxlength: 4

                },
                cvv: {
                    required: true,

                }
            },
            messages: {
                cardnumber: {
                    required: "Please enter a valid card number"
                },
                cardname: {
                    required: "Please enter your name"
                },
                month: {
                    required: "expiry month"
                },
                year: {
                    required: "expiry year"
                },
                cvv: {
                    required: "Please enter you CVV"
                }
            }
        });
    }



    $(".notification__icon").click(function (e) {
        e.preventDefault();
        $(this).parents('.notification__container').addClass('notification__active');
        console.log($(this).parents('.notification__container'));
    });

    $(".notification__container .notification__content__outer").click(function (e) {
        e.preventDefault();
        $(this).parents('.notification__container').removeClass('notification__active');
    });


    $(".week__menu__days .date_text").click(function (e) {
        e.preventDefault();
        $(".week__menu__days .date_text").removeClass("active");
        $(this).addClass("active");

    });

    $('.scroll-down[data-target]').on('click', function (event) {

        var target = $(this.getAttribute('data-target'));
        if (target.length) {
            event.preventDefault();

            if ($(window).width() > 992) {
                $('html, body').stop().animate({
                    scrollTop: target.offset().top - 100
                }, 1000);
                return
            }
            $(target).siblings().addClass('d-none d-lg-block');
            target.removeClass('d-none d-lg-block');
            return 

        }

    });

    $('#selectMealPlan').change(function () {
        $('.dashboard__select__dropdown').addClass("display__none__md");
    });



});

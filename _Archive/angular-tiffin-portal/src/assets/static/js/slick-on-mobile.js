$(document).ready(function () {
    // Index page
    var $slick_slider = $('.index-slider');
    var settings_slider = {
        dots: true,
        arrows: false,
        infinite: false,
        dotsClass: 'slick-custom-dots'
        // more settings
    };
    slick_index($slick_slider, settings_slider);

    //Delivery page
    var $slick_slider = $('.locations-slider');
    var settings_slider = {
        dots: true,
        arrows: false,
        infinite: false,
        dotsClass: 'slick-custom-dots'
        // more settings
    };
    slick_on_mobile($slick_slider, settings_slider);

    //Select meals page
    var $week_slider = $('.week-slider');
    var $week_content_slider = $('.week-content-slider');
    var settings_week = {
        dots: false,
        arrows: true,
        infinite: false,
        asNavFor: $week_content_slider,
        draggable: false,
        swipe: false,
        prevArrow: $('.week-slider__arrow-prev'),
        nextArrow: $('.week-slider__arrow-next')
    };
    var settings_week_content = {
        dots: false,
        arrows: false,
        infinite: false,
        draggable: false,
        swipe: false
    };
    slick_on_mobile($week_slider, settings_week);
    slick_on_mobile($week_content_slider, settings_week_content);

    // console.log(slick())
    // slick on mobile
    function slick_on_mobile(slider, settings) {
        $(window).on('load resize', function () {
            if ($(window).width() > 992) {
                if (slider.hasClass('slick-initialized')) {
                    slider.slick('unslick');
                }
                return;
            }
            if (!slider.hasClass('slick-initialized')) {
                return slider.slick(settings);
            }
        });
    }

    function slick_index(slider, settings) {
        $(window).on('load resize', function () {
            if (!slider.hasClass('slick-initialized')) {
                return slider.slick(settings);
            }
            return;
        });
    }
});

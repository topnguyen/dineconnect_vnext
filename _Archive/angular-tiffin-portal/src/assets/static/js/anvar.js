
//Delivery page
var locationWrapper = document.querySelector('#location-wrapper');
var deliveryButton = document.querySelector('.deliveryCheckbox');
var selfPickUpButton = document.querySelector('.selfPickupCheckbox');
var goToButtons = document.getElementsByClassName('goTo');


const deliverPckupButtonsCLickndler = function () {
    let dClass = 'deliveriPckup-visible';
    let hasClass = locationWrapper.classList.contains(dClass);
    if (hasClass) locationWrapper.classList.remove(dClass);
    else {
        locationWrapper.classList.add(dClass);
        locationWrapper.classList.remove('self-pickup-visible');
        locationWrapper.classList.remove('delivery-visible');
        locationWrapper.classList.remove('delivery-summery-visible');

    }


}

const selfPickUpButtonCLickHandler = function () {

    //Slef pick slider
    //Fix onload style
    var slider = $('.locations-slider');
    slider.slick('refresh');
    // slider.slick('slickGoTo', 0);

    //Show Self pick up belongins
    locationWrapper.classList.remove('delivery-visible');
    locationWrapper.classList.add('self-pickup-visible');
    deliverPckupButtonsCLickndler();
}

const deliveryButtonCLickHandler = function () {

    //Show Self pick up belongins
    locationWrapper.classList.remove('self-pickup-visible');
    locationWrapper.classList.add('delivery-visible');
    deliverPckupButtonsCLickndler();
}

const deliverNextButtonClickndler = function () {
    locationWrapper.classList.remove('self-pickup-visible');
    locationWrapper.classList.remove('delivery-visible');
    locationWrapper.classList.add('delivery-summery-visible');
    // let dClass = 'deliveriPckup-visible';
    // let hasClass = locationWrapper.classList.contains(dClass);
    // if (hasClass) locationWrapper.classList.remove(dClass);
    // else {locationWrapper.classList.add(dClass);
    //     locationWrapper.classList.remove('self-pickup-visible');
    //     locationWrapper.classList.remove('delivery-visible');
    // }


}



//Attaching click eent to selfPickUpButton
if (locationWrapper) {
    selfPickUpButton.addEventListener('click', selfPickUpButtonCLickHandler);
    deliveryButton.addEventListener('click', deliveryButtonCLickHandler);
    // toDeliveryButton.addEventListener('click', goTofunction, "as");



}



//Payment page
var paymentWrapper = document.querySelector('#payment-wrapper');
const makePaymentButtonClickHandler = function () {
    paymentWrapper.classList.remove('payment-summery-visible');
    paymentWrapper.classList.add('payment-card-visible');
}

const toSummeryClickHandler = function () {
    paymentWrapper.classList.remove('payment-card-visible');
    paymentWrapper.classList.add('payment-summery-visible');
}

//Select Meals page
var selectMealsWrapper = document.querySelector('#select-meals-wrapper');
var selectMealPlanDropDown = document.querySelector('#selectMealPlan');

const showSelectMealSummery = function () {
    selectMealsWrapper.classList.remove('select-meals-visible');
    selectMealsWrapper.classList.add('selectmeal-summery-visible');
}

const showSelectMeals = function () {
    selectMealsWrapper.classList.remove('selectmeal-summery-visible');
    selectMealsWrapper.classList.add('select-meals-visible');
}

const selectMealsDropDownOnChangeHandler = function () {
    showSelectMeals();
    if ($(window).width() <= 992) {
        var slider = $('.week-slider');
        slider.slick('refresh');
    }
}

if (selectMealsWrapper) {
    selectMealPlanDropDown.addEventListener('change', selectMealsDropDownOnChangeHandler)
}


//Common
const functionMap = {
    'deliveriPckup-visible': deliverPckupButtonsCLickndler,
    'delivery-summery-visible': deliverNextButtonClickndler,
    'payment-card-visible': makePaymentButtonClickHandler,
    'payment-summery-visible': toSummeryClickHandler,
    'selectmeal-summery-visible': showSelectMealSummery,
    'select-meals-visible': showSelectMeals
}

const goTofunction = function (e) {
    var to = this.getAttribute('page-to');
    functionMap[to]();
}

//Attaching to goTO buttons
for (let el of goToButtons) {
    el.addEventListener('click', goTofunction);
}





//For scrolling
(function () {
    // The debounce function receives our function as a parameter
    const debounce = (fn) => {

        // This holds the requestAnimationFrame reference, so we can cancel it if we wish
        let frame;

        // The debounce function returns a new function that can receive a variable number of arguments
        return (...params) => {

            // If the frame variable has been defined, clear it now, and queue for next frame
            if (frame) {
                cancelAnimationFrame(frame);
            }

            // Queue our function call for the next frame
            frame = requestAnimationFrame(() => {

                // Call our function and pass any params we received
                fn(...params);
            });

        }
    };


    // Reads out the scroll position and stores it in the data attribute
    // so we can use it in our stylesheets
    const storeScroll = () => {
        document.documentElement.dataset.scroll = window.scrollY>100?window.scrollY:0;
    }

    // Listen for new scroll events, here we debounce our `storeScroll` function
    document.addEventListener('scroll', debounce(storeScroll), { passive: true });

    // Update scroll position for first time
    storeScroll();
})();
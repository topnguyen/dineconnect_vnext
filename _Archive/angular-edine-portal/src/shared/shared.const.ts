export const LOGO_URL: string = 'LOGO_URL';
export const CHOOSE_TABLE_MANUAL: string = 'CHOOSE_TABLE_MANUAL';
export const SCAN_QR_CODE: string = 'SCAN_QR_CODE';
export const SELECTED_LOCATION: string = 'SELECTED_LOCATION';
export const SELECTED_DEPARTMENT: string = 'SELECTED_DEPARTMENT';
export const SELECTED_TABLE: string = 'SELECTED_TABLE';
export const SELECTED_FEE: string = 'SELECTED_FEE';
export const CHOOSE_TABLE_BY_QR: string = 'CHOOSE_TABLE_BY_QR';
export const LANGUAGE_DISPLAY: string = 'LANGUAGE_DISPLAY';
export const POSTAL_CODE_ONEMAP: string = 'POSTAL_CODE_ONEMAP';
export const POSTAL_CODE: string = 'POSTAL_CODE';
export const DATE_TIME_DELIVERY: string = 'DATE_TIME_DELIVERY';
export const TIME_SLOT_DELEVERY: string = 'TIME_SLOT_DELEVERY';
export const SETTING_TENANT: string = 'SETTING_TENANT';
export const SAVED_OTP: string = 'SAVED_OTP';

export const GOOGLE_MAP_KEY = 'AIzaSyCbrgvMPgVlrdwHg7IfHe064oUKEhxma3M';

export enum StatusDineMessage {
  Success = 'Success',
  Error = 'Error',
}
export enum PaymentMethodsSytemsNames {
  Cash = 'Payments.Cash',
  Omise = 'Payments.Omise',
  Stripe = 'Payments.Stripe'
}

export enum PaymentMethods {
  Cash = 'Cash',
  Omise = 'Omise',
  Stripe = 'Stripe'
}

export const languageItemList: Array<{
  label: string;
  value: string;
  icon: string;
  active?: boolean;
}> = [
    {
      label: 'ENGLISH',
      value: 'EN',
      icon: 'assets/image/en-icon.png',
    },
  ];
export const MenuItemTagsConst: any[] = [
  { value: 'HOT', src: '../../../assets/image/markItemAs/hot.svg' },
  {
    value: 'VEGETARIAN',
    src: '../../../assets/image/markItemAs/vegetarian.svg',
  },
  {
    value: 'HALAI',
    icon: '',
    src: '../../../assets/image/markItemAs/halal.svg',
  },
  {
    value: 'HOTPROMOTION',
    src: '../../../assets/image/markItemAs/hot-promotions.svg',
  },
  {
    value: 'VEGAN',
    icon: '',
    src: '../../../assets/image/markItemAs/vegan.svg',
  },
  {
    value: 'BESTSELLER',
    src: '../../../assets/image/markItemAs/best-seller.svg',
  },
  {
    value: 'GLUTENFEE',
    icon: '',
    src: '../../../assets/image/markItemAs/gluten.svg',
  },
];

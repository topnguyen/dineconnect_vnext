import { WheelThemeSettingsService } from './../../../service/wheel-theme-settings.service';
import { map } from 'rxjs/operators';
import { AppSession, OrderSetting, wheelDataSetups } from './../../../constant/constant';
import { WheelSetupsService } from './../../../service/wheel-setups.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtService } from '../../authenticate/jwt.service';
import { AppConsts } from 'src/shared/constant';
import { TenantService } from 'src/app/tenant/tenant.service';
import { XmlHttpRequestHelper } from 'src/shared/helper/XmlHttpRequestHelper';
import { SubdomainTenancyNameFinder } from 'src/shared/helper/SubdomainTenancyNameFinder';
import { PlatformLocation } from '@angular/common';
import { CommonService } from '../../common-service/common-service.service';
import { CookieService } from 'ngx-cookie-service';
import { AccountService } from 'src/shared/service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StartUpService {
  constructor(
    private jwtService: JwtService,
    private _cookieService: CookieService,
    private tenantService: TenantService,
    private wheelSetupsService: WheelSetupsService,
    private wheelThemeSettingsService: WheelThemeSettingsService,
    private commonService: CommonService,
    private _accountService: AccountService,
  ) { }

  checkTenantAvailable(): Promise<any> {
    const tenantAvailableInput = {
      tenancyName: AppConsts.tenancyName ? AppConsts.tenancyName : 'Default',
    };
    return this.tenantService
      .isTenantAvailable(tenantAvailableInput)
      .toPromise();
  }

  getApplicationConfig(appRootUrl: string, callback: () => void) {
    let type = 'GET';
    let url = appRootUrl + 'assets/config/' + environment.appConfig;

    XmlHttpRequestHelper.ajax(type, url, null, null, (result) => {
      const subdomainTenancyNameFinder = new SubdomainTenancyNameFinder();
      const tenancyName =
        subdomainTenancyNameFinder.getCurrentTenancyNameOrNull(
          result.appBaseUrl
        );

      AppConsts.appBaseUrlFormat = result.appBaseUrl;
      AppConsts.remoteServiceBaseUrlFormat = result.remoteServiceBaseUrl;
      AppConsts.tenancyName = result.tenancyName;

      if (tenancyName == null) {
        AppConsts.appBaseUrl = result.appBaseUrl.replace(
          AppConsts.tenancyNamePlaceHolderInUrl + '.',
          ''
        );
        AppConsts.remoteServiceBaseUrl = result.remoteServiceBaseUrl.replace(
          AppConsts.tenancyNamePlaceHolderInUrl + '.',
          ''
        );
      } else {
        AppConsts.tenancyName = tenancyName;
        AppConsts.appBaseUrl = result.appBaseUrl.replace(
          AppConsts.tenancyNamePlaceHolderInUrl,
          tenancyName
        );
        AppConsts.remoteServiceBaseUrl = result.remoteServiceBaseUrl.replace(
          AppConsts.tenancyNamePlaceHolderInUrl,
          tenancyName
        );
      }

      callback();
    });
  }

  getWheelSetups(id, baseUrl) {
    return this.wheelSetupsService.getWheelSetups(id, baseUrl).pipe(
      map((resp) => {
        if (!resp.result.wheelSetup) {
          this.commonService.isWheelSetupsAvailable = false;
          return;
        }

        this.commonService.isWheelSetupsAvailable = true;
        const {
          homePageId,
          logoCompanyUrl,
          checkOutPageId,
          themeName,
          welcomeUrl,
          advertOneUrl,
          advertTwoUrl,
          title,
          welcomeMessage,
          termsAndConditions,
          wheelPostCodeServiceType,
          wheelDynamicPageEditDto,
          isDisplayHomePage
        } = resp.result.wheelSetup;

        wheelDataSetups.logoCompanyUrl = logoCompanyUrl;
        wheelDataSetups.homePageId = homePageId;
        wheelDataSetups.welcomeUrl = welcomeUrl;
        wheelDataSetups.advertOneUrl = advertOneUrl;
        wheelDataSetups.advertTwoUrl = advertTwoUrl;
        wheelDataSetups.checkOutPageId = checkOutPageId;
        wheelDataSetups.currency = resp.result.currency;
        wheelDataSetups.title = title || 'eDine';
        wheelDataSetups.welcomeMessage = welcomeMessage;
        wheelDataSetups.termsAndCondtions = termsAndConditions;
        wheelDataSetups.wheelPostCodeServiceType = wheelPostCodeServiceType;
        wheelDataSetups.homePageObject = wheelDynamicPageEditDto;
        wheelDataSetups.isDisplayHomePage = isDisplayHomePage;

        OrderSetting.accepted = resp.result.orderSetting.accepted;
        OrderSetting.width = resp.result.orderSetting.width;
        OrderSetting.height = resp.result.orderSetting.height;
        OrderSetting.autoRequestDeliver = resp.result.orderSetting.autoRequestDeliver;
        OrderSetting.askTheMobileNumberFirst = resp.result.orderSetting.askTheMobileNumberFirst;
        OrderSetting.signUp = resp.result.orderSetting.signUp;

        this.wheelThemeSettingsService
          .getWheelThemeSettings(themeName)
          .subscribe((resp) => {
            this.commonService.$themeSettings.next(resp.result);
          });
      })
    );
  }

  getCurrentLoginInformations(): Observable<any> {
    return this._accountService.getCurrentLoginInformations().pipe(
      map((resp) => {
        AppSession.user = resp.result;

        this._accountService.userSubject.next(AppSession.user);

        if (AppSession.user.customerGuid) {
          this._cookieService.set("Abp-CustomerId", AppSession.user.customerGuid, new Date(new Date().getTime() + 5 * 365 * 86400000), "/");
        }
        else {
          this._cookieService.delete("Abp-CustomerId", "/");
        }
      }));
  }

  getWheelTokenOneMap() {
    return this.wheelSetupsService.getAddressByPostCode();
  }

  setCookieToken(name, value) {
    this._cookieService.set(name, value);
  }
}

function getPathName() {
  return document.location.pathname;
}

function getHost() {
  return document.location.host;
}

function getDocumentOrigin() {
  if (!document.location.origin) {
    return (
      document.location.protocol +
      '//' +
      document.location.hostname +
      (document.location.port ? ':' + document.location.port : '')
    );
  }
  return document.location.origin;
}

function getBaseHref(platformLocation: PlatformLocation): string {
  let baseUrl = platformLocation.getBaseHrefFromDOM();
  if (baseUrl) {
    return baseUrl;
  }
  return '/';
}

export function StartUpFactory(
  startUpService: StartUpService,
  platformLocation: PlatformLocation
) {
  return () => {
    return new Promise<boolean>((resolve, reject) => {
      AppConsts.appBaseHref = getBaseHref(platformLocation);

      //window.localStorage.clear();
      //window.sessionStorage.clear();

      // window.history.pushState('', '', getDocumentOrigin() + AppConsts.appBaseHref);
      startUpService.getApplicationConfig(
        getDocumentOrigin() + AppConsts.appBaseHref,
        () => {
          startUpService
            .checkTenantAvailable()
            .then((result) => {
              let pathName = getPathName();
              let id = 0;
              if (pathName && pathName.length > 1) {
                let params = pathName.split('/');
                id = !isNaN(+params[1]) ? +params[1] : 0;
                if (!isNaN(+params[1])) {
                  window.history.pushState('', '', getDocumentOrigin() + AppConsts.appBaseHref);
                }
              }
              startUpService.getCurrentLoginInformations().subscribe(() => {
                startUpService.getWheelSetups(id, getHost()).subscribe(() => {
                  resolve(true);
                });
              });
            })
            .catch((err) => reject(err));
        }
      );
    });
  };
}

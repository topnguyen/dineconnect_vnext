import { LocalStorageService } from './../local-storage/local-storage.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IAuthenticateModel, IAuthenticateResultModel, IUser } from 'src/model';
import { HttpClientService } from './../config/http-client/http-client.service';
import { IPageResult } from './../../../model';
import { HttpOptions } from '..';
import { CONSTANT } from 'src/shared/constant';
import {
  IExternalAuthenticateModel,
  IExternalAuthenticateResultModel,
} from 'src/shared/service-proxies/service-proxies';

import {
  TokenService,
} from 'abp-ng2-module';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  constructor(
    private http: HttpClientService,
    private _tokenService: TokenService,
    private localStorageService: LocalStorageService
  ) {}

  authenticate(
    body: IAuthenticateModel | undefined,
    metadataAuthenticate = '',
    forceReload = false
  ): Observable<IPageResult<IAuthenticateResultModel>> {
    let url_ = `/api/TokenAuth/AuthenticatePortal`;
    url_ = url_.replace(/[?&]$/, '');

    const postContent: HttpOptions = {
      url: url_,
      body: body,
      cacheMins: 10,
      specificCacheKey: metadataAuthenticate,
      forceReload: forceReload,
    };

    return this.http.post<IPageResult<IAuthenticateResultModel>>(postContent);
  }

  externalAuthenticate(
    body: IExternalAuthenticateModel | undefined,
    metadataAuthenticate = '',
    forceReload = false
  ): Observable<IExternalAuthenticateResultModel> {
    let url_ = `/api/TokenAuth/ExternalAuthenticateMember`;
    url_ = url_.replace(/[?&]$/, '');

    const postContent: HttpOptions = {
      url: url_,
      body: body,
      cacheMins: 10,
      specificCacheKey: metadataAuthenticate,
      forceReload: forceReload,
    };

    return this.http.post<IExternalAuthenticateResultModel>(postContent);
  }

  public getUserToken(): string {
    return this._tokenService.getToken();
  }

  logOut(): Observable<void> {
    let url_ = '/api/TokenAuth/LogOut';
    url_ = url_.replace(/[?&]$/, '');

    const content: HttpOptions = {
      url: url_,
    };

    return this.http.get(content);
  }

  refreshToken(metadataAuthenticate: string = ''): any {
    const body = {
      userNameOrEmailAddress: environment.serviceUserAccount,
      password: environment.servicePasswordAccount,
      rememberClient: false,
      returnUrl: null,
      singleSignIn: false,
      twoFactorRememberClientToken: null,
    };
    return this.authenticate(body, metadataAuthenticate);
  }
}

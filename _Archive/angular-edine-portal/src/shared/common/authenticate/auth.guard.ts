import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { NavigationService } from 'src/shared/navigation';
import { AccountService } from 'src/shared/service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private accountService: AccountService,
    private navigationService: NavigationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const user = this.accountService.userValue;
    // if (user) {
    //   // If user authorised then return true
    //   return true;
    // }

    return true;

    this.router.navigate(['']).then(() => this.navigationService.navigateToLogin().then(() => false));
  }
}

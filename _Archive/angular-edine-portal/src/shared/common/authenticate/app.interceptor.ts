import { ToastService } from 'src/shared/toast';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { TenantService } from 'src/app/tenant/tenant.service';
import { JwtService } from './jwt.service';
import { switchMap, catchError, tap, finalize } from 'rxjs/operators';
import { CONSTANT, AppConsts } from 'src/shared/constant';
import { AccountService } from 'src/shared/service';
import { CommonService } from '../common-service/common-service.service';
import { DineMessageService } from '../common-service/message-top-down.service';
import { CookieService } from 'ngx-cookie-service';
import {
  TokenService,
} from 'abp-ng2-module';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  baseUrl = AppConsts.remoteServiceBaseUrl;
  requestCounter = 0;

  constructor(
    public auth: JwtService,
    private tenantService: TenantService,
    private accountService: AccountService,
    private commonService: CommonService,
    private dineMessageService: DineMessageService ,
    private _tokenService: TokenService,
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.beginRequest();
    if (
      !request.url.includes('www') &&
      !request.url.includes('googleapis') &&
      !request.url.includes('http') &&
      !request.url.includes('onemap')
    ) {
      request = this.addAuthHeader(request);
      let requestUrl = '';
      requestUrl = `${this.baseUrl ? this.baseUrl : ''}${request.url}`;

      request = request.clone({
        url: requestUrl,
      });
    }

    return next.handle(request).pipe(
      finalize(() => {
        this.endRequest();
      }),
      catchError((error) => {
        return this.handleResponseError(error, request, next);
      })
    );
  }

  beginRequest() {
    this.requestCounter = Math.max(this.requestCounter, 0) + 1;

    if (this.requestCounter === 1) {
      this.commonService.setIsBusy(true);
    }
  }

  endRequest() {
    this.requestCounter = Math.max(this.requestCounter, 1) - 1;

    if (this.requestCounter === 0) {
      this.commonService.setIsBusy(false);
    }
  }

  addAuthHeader(request) {
    let tokenValue = this._tokenService.getToken();    
    const tenant = this.tenantService.getTenantAvailable();

    let setHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Abp-TenantId': tenant && tenant.tenantId ? tenant.tenantId.toString() : '1',
      'Abp-CustomerId': this.accountService.customerId
    };
    
    const authenHeader = request.headers.get('Authorization');

    if (authenHeader && !authenHeader.includes('null')) {
      setHeaders['Authorization'] = authenHeader;
    } else if (tokenValue) {
      setHeaders['Authorization'] = `Bearer ${tokenValue}`;
    }

    return request.clone({
      setHeaders: setHeaders,
    });
  }

  handleResponseError(
    error: HttpErrorResponse,
    request?,
    next?
  ): Observable<HttpEvent<any>> {
    switch (error.status) {
      case 401:
        // return this.auth.refreshToken(CONSTANT.META_DATA_TOKEN_KEY).pipe(
        //   switchMap(() => {
        //     request = this.addAuthHeader(request);
        //     return next.handle(request);
        //   }),
        //   catchError((e) => {
        //     if (e.status !== 401) {
        //       return this.handleResponseError(e);
        //     } else {
        //       this.auth.logOut();
        //     }
        //   })
        // );
        return throwError(error);

      // // Access denied error
      // case 403:
      //   break;

      // // Server error
      // case 500:
      //   break;

      // // Maintenance error
      // case 503:
      //   break;

      default:
        let message = error?.error?.error?.message;
        this.dineMessageService.showError(message ?? error?.message);
    }
  }
}

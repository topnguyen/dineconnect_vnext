import { ICartModel } from 'src/model/cart.model';
import { CONSTANT } from 'src/shared/constant';
import { IUser } from './../../../model/user.model';
import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { BehaviorSubject } from 'rxjs';
import { ISimpleLocation } from 'src/model';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import * as localForage from 'localforage';

const STORAGE_KEY = CONSTANT.STORAGE_KEY;
const DELIVERY_LOCALTION_KEY = CONSTANT.DELIVERY_LOCALTION_KEY;
const USERKEY = CONSTANT.USERKEY;
const ADDRESS_DETAIL = CONSTANT.ADDRESS_DETAIL;
@Injectable()
export class LocalStorageService {
  $orderListLocalStorage = new BehaviorSubject<CustomMenuItemData[]>(
    this.getOrderItemsOnLocalStorage()
  );

  $deliveryLocation = new BehaviorSubject<ISimpleLocation>(
    this.getDeliveryLocation()
  );

  $orderListModelStorage = new BehaviorSubject<ICartModel[]>(
    this.getCartItem()
  );

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {}

  storeOrderItemsOnLocalStorage(items: any): void {
    let currentList = this.storage.get(STORAGE_KEY) || [];
    currentList = [...items];
    // insert updated array to local storage
    this.storage.set(STORAGE_KEY, currentList);
    this.$orderListLocalStorage.next(items);
  }

  getOrderItemsOnLocalStorage() {
    const items = this.storage.get(STORAGE_KEY) || [];
    return items;
  }

  clearOrderList() {
    this.storage.remove(STORAGE_KEY);
    this.$orderListLocalStorage.next([]);
  }

  setCartItem(carts: ICartModel[]) {
    this.storage.set(CONSTANT.CART_ORDER, carts);
    this.$orderListModelStorage.next(carts);
  }

  getCartItem() {
    return this.storage.get(CONSTANT.CART_ORDER) || [];
  }

  removeCartItem() {
    this.storage.remove(CONSTANT.CART_ORDER);
  }

  saveDeliveryLocation(location: ISimpleLocation) {
    if (location) {
      this.storage.set(DELIVERY_LOCALTION_KEY, location);
      this.$deliveryLocation.next(location);
    }
  }
  
  getDeliveryLocation() {
    const items = this.storage.get(DELIVERY_LOCALTION_KEY);
    return items;
  }

  saveUser(user: IUser) {
    if (user) {
      this.storage.set(USERKEY, user);
    }
  }

  getUser() {
    const items = this.storage.get(USERKEY);
    return items;
  }
  removeByKey(key) {
    this.storage.remove(key);
  }
  getByKey(key) {
    return this.storage.get(key);
  }
  saveAddress(addressDetail) {
    if (addressDetail) {
      this.storage.set(ADDRESS_DETAIL, addressDetail);
    }
  }
  getAddress() {
    const items = this.storage.get(ADDRESS_DETAIL);
    return items;
  }

  getItem(key: string, callback: any): void {
    if (!localForage) {
      return;
    }

    localForage.getItem(key, callback);
  }

  setItem(key, value, callback?: any): void {
    if (!localForage) {
      return;
    }

    if (value === null) {
      value = undefined;
    }

    localForage.setItem(key, value, callback);
  }

  removeItem(key, callback?: any): void {
    if (!localForage) {
      return;
    }

    localForage.removeItem(key, callback);
  }
}

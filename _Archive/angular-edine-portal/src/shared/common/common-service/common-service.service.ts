import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  $isWelComePage = new BehaviorSubject<boolean>(false);
  $currentStepName = new BehaviorSubject<string>('');
  $isBusy = new BehaviorSubject<boolean>(false);
  $userEmail = new BehaviorSubject<string>('');
  $themeSettings = new BehaviorSubject<Object>({});
  $isHideHeaderSubject = new Subject<boolean>();
  $isHideFooterSubject = new Subject<boolean>();
  $isWith70Subject = new Subject<boolean>();
  $isMenuSubject = new Subject<boolean>();

  private _isWheelSetupsAvailable: boolean = true;

  constructor() { }

  setIsWelComePage(bool: boolean) {
    this.$isWelComePage.next(bool);
  }

  setIsBusy(bool: boolean) {
    this.$isBusy.next(bool);
  }

  get isWheelSetupsAvailable() {
    return this._isWheelSetupsAvailable;
  }

  set isWheelSetupsAvailable(bool: boolean) {
    this._isWheelSetupsAvailable = bool;
  }
}

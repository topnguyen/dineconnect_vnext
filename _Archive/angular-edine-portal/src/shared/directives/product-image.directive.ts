import {
  Directive,
  HostListener,
  ElementRef,
  Input,
  HostBinding,
  Injector,
  EventEmitter,
  Output,
} from '@angular/core';
import { TenantService } from 'src/app/tenant/tenant.service';
import { AppConsts } from '../constant';

@Directive({
  selector: '[appProductImage]',
  host: {
    '(error)': 'updateUrl()',
    '(load)': 'load()',
    '[src]': 'src',
  },
})
export class ProductImageDirective {
  @Input() src: string;
  @Output() imageErrorCallBack: EventEmitter<string> =
    new EventEmitter<string>();
  @HostBinding('class') className;

  constructor(injector: Injector, private tenantService: TenantService) {}

  updateUrl() {
    this.imageErrorCallBack.emit(this.src);
    this.className = 'not-available';
    this.src = this.fallbackProductImageUrl();
  }

  load() {
    if (!this.src) {
      this.src = this.fallbackProductImageUrl();
    }
  }

  fallbackProductImageUrl(): string {
    let tenant = this.tenantService.getTenantAvailable();
    const tenantId = tenant?.tenantId ?? 1;
    return (
      AppConsts.remoteServiceBaseUrl +
      '/TenantCustomization/GetMenuItemDefault?tenantId=' +
      tenantId
    );
  }
}

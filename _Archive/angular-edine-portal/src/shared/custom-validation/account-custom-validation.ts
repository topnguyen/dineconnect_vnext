import {
  AbstractControl,
  ValidatorFn,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';

export function phoneEmailValidators(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = nameRe.test(control.value);
    return !forbidden ? { forbiddenName: { value: control.value } } : null;
  };
}

export function emailCustomValidators(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = nameRe.test(control.value);
    return !forbidden ? { email: { value: control.value } } : null;
  };
}

export const confirmPasswordValidators: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const newPass = control.get('newPassword');
  const confirmPass = control.get('confirmPassword');
  return newPass && confirmPass && newPass.value !== confirmPass.value
    ? { invalidConfirm: true }
    : null;
};

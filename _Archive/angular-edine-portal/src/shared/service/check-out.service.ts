import { IPageResultList } from './../../model/page-result.model';
import { Observable } from 'rxjs';
import { IOrderCheckout } from './../../model/order-checkout.model';
import { Injectable } from '@angular/core';
import { HttpClientService, HttpOptions } from '../common';
import { IGetOrderDetailForView } from 'src/model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { GOOGLE_MAP_KEY } from '../shared.const';
import { Loader } from '@googlemaps/js-api-loader';

@Injectable({
  providedIn: 'root',
})
export class CheckOutService {
  constructor(
    private http: HttpClientService,
    private httpClient: HttpClient
  ) { }

  chargedOrder(orderDetails: IOrderCheckout) {
    let url_ = '/api/services/app/OrderDetails/CreateOrEdit';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(orderDetails);
    const options: HttpOptions = {
      url: url_,
      body: content_,
    };
    return this.http.post(options);
  }

  getPastOrder(): Observable<IPageResultList<IGetOrderDetailForView>> {
    let url_ = '/api/services/app/OrderDetails/GetAll';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      forceReload: true,
    };
    return this.http.get(options);
  }

  createPaymentForOrder(body: any): Observable<any> {
    let url_ = '/api/services/app/WheelOrder/CreatePaymentForOrder';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body,
    };
    return this.http.post(options);
  }
  searchLocation(body: string): Observable<any> {
    let url_ =
      'https://maps.googleapis.com/maps/api/geocode/json?key=' + GOOGLE_MAP_KEY;
    if (body) {
      url_ += '&address=' + encodeURIComponent(body);
    }
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };
    return this.httpClient.get(url_);
  }

  checkSlotOrder(body: any): Observable<any> {
    let url_ = '/api/services/app/WheelOrder/CheckSlotOrder';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body,
    };
    return this.http.post(options);
  }

  loadScript = async (): Promise<void> => {
    const loader = new Loader({
      apiKey: GOOGLE_MAP_KEY,
    });
    await loader.load();
  };

  async AddressToGeoCode(address: string, callback: (res: any) => void) {
    await this.loadScript();
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address }, (res) => {
      console.log(res);
      if (callback) {
        callback(res);
      }
    });
  }

  CheckDeliveryZone(body: any): Observable<any> {
    let url_ = '/api/services/app/WheelOrder/CheckDeliveryZone';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body,
    };
    return this.http.post(options);
  }

  checkMinOrderByDeliveryZone(body: any): Observable<any> {
    let url_ = '/api/services/app/WheelOrder/CheckMinOrderByDeliveryZone';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body,
    };
    return this.http.post(options);
  }

  payOrderWithHitPay(body: any): Observable<any> {
    let url_ = '/api/services/app/WheelOrder/PlaceOrderWithHitPay';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body,
    };
    return this.http.post(options);
  }

  saveCustomerSessionData(key: string, value: string): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/SaveCustomerSessionData';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body: { key, value },
    };
    return this.http.post(options);
  }

  isLocationSetup(locationId: number): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/IsLocationSetup?locationId=' + locationId;
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };
    return this.http.post(options);
  }

  isLocationInDeliveryZone(locationId: number): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/IsLocationInDeliveryZone?locationId=' + locationId;;
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };
    return this.http.post(options);
  }

  checkMinOrder(): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/CheckMinOrderRequired';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_
    };
    return this.http.post(options);
  }

  getCustomerSessionData(key: string): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/GetCustomerSessionData?key=' + key;
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };
    return this.http.get(options);
  }

  getCustomerCheckoutData(): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/GetCustomerCheckoutData';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };
    return this.http.get(options);
  }

  resetCheckout(): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/ResetCheckout';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_
    };
    return this.http.post(options);
  }

  initQROrdering(body:any): Observable<any> {
    let url_ = '/api/services/app/WheelCheckout/InitQROrdering';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body
    };
    return this.http.post(options);
  }
}

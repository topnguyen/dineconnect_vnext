import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPageResult } from 'src/model/page-result.model';
import { HttpClientService } from '../common/config/http-client/http-client.service';
import { HttpOptions, TokenType } from '../common/config/http-client/http-options.model';

const url = '/api/services/app/WheelDynamicPages';

@Injectable({
  providedIn: 'root'
})

export class WheelDynamicPagesService {

  constructor(
    private http: HttpClientService,
  ) { }

  getWheelDynamicPage(id: number): Observable<IPageResult<any>> {
    const options: HttpOptions = {
      url: `${url}/GetWheelDynamicPageForEdit?Id=${id}`,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
}

export interface ICreateOrEditWheelDynamicPagesInput {
  name: string;
  html: string | undefined;
  css: string | undefined;
  assets: string | undefined;
  styles: string | undefined;
  components: string | undefined;
  id: number;
}

export class CreateOrEditWheelDynamicPagesInput implements ICreateOrEditWheelDynamicPagesInput {
  name!: string;
  html!: string | undefined;
  css!: string | undefined;
  assets!: string | undefined;
  styles!: string | undefined;
  components!: string | undefined;
  id!: number;

  constructor(data?: ICreateOrEditWheelDynamicPagesInput) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(_data?: any) {
      if (_data) {
          this.name = _data["name"];
          this.html = _data["html"];
          this.css = _data["css"];
          this.assets = _data["assets"];
          this.styles = _data["styles"];
          this.components = _data["components"];
          this.id = _data["id"];
      }
  }

  static fromJS(data: any): CreateOrEditWheelDynamicPagesInput {
      data = typeof data === 'object' ? data : {};
      let result = new CreateOrEditWheelDynamicPagesInput();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["name"] = this.name;
      data["html"] = this.html;
      data["css"] = this.css;
      data["assets"] = this.assets;
      data["styles"] = this.styles;
      data["components"] = this.components;
      data["id"] = this.id;
      return data; 
  }
}

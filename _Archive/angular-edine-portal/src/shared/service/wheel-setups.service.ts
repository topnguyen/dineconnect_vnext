import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPageResult } from 'src/model/page-result.model';
import { HttpClientService } from '../common/config/http-client/http-client.service';
import {
  HttpOptions,
  TokenType,
} from '../common/config/http-client/http-options.model';

@Injectable({
  providedIn: 'root',
})
export class WheelSetupsService {
  constructor(
    private http: HttpClientService,
    private httpClient: HttpClient
  ) {}
  // getPageToBack(pageId, wheelDepartermentId): Observable<IPageResult<number>> {
  //   let url_ = '/api/services/app/WheelOrder/GetPageGoToBack';
  //   url_ += `?currentPage=${encodeURIComponent(
  //     '' + pageId
  //   )}&wheelDepatermentId=${encodeURIComponent('' + wheelDepartermentId)}&`;
  //   url_ = url_.replace(/[?&]$/, '');
  //   const options: HttpOptions = {
  //     url: url_,
  //     typeToken: TokenType.META_TOKEN,
  //   };
  //   return this.http.get(options);
  // }
  getWheelSetups(id, baseUrl): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/WheelSetups/GetWheelSetupForEdit?id='+id+'&baseUrl='+baseUrl+'&';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    console.log("options", options);
    return this.http.get(options);
  }
  getFaqs(): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/TiffinFaq/GetFaqs';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
  getAddressByPostCode(): Observable<any> {
    const url =
      'https://developers.onemap.sg/commonapi/search?searchVal=189970&returnGeom=Y&getAddrDetails=Y';
    // const body = JSON.stringify({ email: 'shawn@lfsolutions.net', password: 'level5DP' });
    HttpOptions;
    return this.httpClient.get(url, {
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
      },
    });
  }

  isOpeningHour(id: number | undefined): Observable<any> {
    let urlString = '/api/services/app/WheelOrder/IsOpeningHour?';
    if (id === null) {
      throw new Error("The parameter 'id' cannot be null.");
    } else if (id !== undefined) {
      urlString += 'wheelDepartmentId=' + encodeURIComponent('' + id) + '&';
    }
    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.post<any>(options);
  }
  getDefaultDateFuture(id: number | undefined): Observable<any> {
    let urlString = '/api/services/app/WheelOrder/GetDefaultDateFuture?';
    if (id === null) {
      throw new Error("The parameter 'id' cannot be null.");
    } else if (id !== undefined) {
      urlString += 'wheelDepartmentId=' + encodeURIComponent('' + id) + '&';
    }
    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get<any>(options);
  }
  calculateDeliveryTime(
    locationId: number | undefined,
    customerAddressLng: number | undefined,
    customerAddressLat: number | undefined
  ): Observable<any> {
    let urlString = '/api/services/app/WheelOrder/CalculateDeliveryTime?';
    if (!locationId)
      throw new Error("The parameter 'departmentId' cannot be null.");
    else urlString += 'locationId=' + encodeURIComponent('' + locationId) + '&';
    if (!customerAddressLng)
      throw new Error("The parameter 'customerAddressLng' cannot be null.");
    else
      urlString +=
        'customerAddressLng=' +
        encodeURIComponent('' + customerAddressLng) +
        '&';
    if (!customerAddressLat)
      throw new Error("The parameter 'customerAddressLat' cannot be null.");
    else
      urlString +=
        'customerAddressLat=' +
        encodeURIComponent('' + customerAddressLat) +
        '&';

    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.post<any>(options);
  }
}

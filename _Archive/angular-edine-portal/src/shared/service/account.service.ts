import { TokenType } from './../common/config/http-client/http-options.model';
import { IPageResult } from './../../model/page-result.model';
import { LocalStorageService } from 'src/shared/common';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  IUser,
  IUserForEditOutput,
  ICreateOrUpdateUserInput,
} from './../../model/user.model';
import { JwtService } from './../common/authenticate/jwt.service';
import {
  IPageResultList,
  IResetPasswordInput,
  IResetPasswordOutput,
  ISendPasswordResetCodeInput,
  IResolveTenantIdInput,
} from 'src/model';
import { HttpClientService } from './../common/config/http-client/http-client.service';
import { HttpOptions } from '../common';
import { AppSession, CONSTANT } from '../constant/constant';
import {
  CreateCustomerDto,
  GetCurrentLoginInformationsOutput,
  RegisterMemberInput,
  RegisterOutput,
} from '../service-proxies/service-proxies';
import { HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  $isOpenAccountComponent = new BehaviorSubject(false);
  $changePassword = new BehaviorSubject(null);
  tempInfomationUser: IUser;
  returnUrl = '';

  userSubject: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(
    this.getUserFromLocal()
  );

  user: Observable<IUser>;

  constructor(
    private _cookieService: CookieService,
    private http: HttpClientService,
    private auth: JwtService,
    private localStorageService: LocalStorageService
  ) {
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): IUser {
    return this.userSubject.value;
  }

  public get userToken() {
    return this.auth.getUserToken();
  }

  public get customerId() {
    return this._cookieService.get("Abp-CustomerId");
  }

  openAccountComponent(value) {
    this.$isOpenAccountComponent.next(value);
  }

  getUserForEdit(
    id: number | null | undefined
  ): Observable<IPageResult<IUserForEditOutput>> {
    let url_ = '/api/services/app/User/GetUserForEdit?';
    if (id !== undefined) url_ += 'Id=' + encodeURIComponent('' + id) + '&';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getCustomerForEdit(): Observable<any> {
    let url_ = '/api/services/app/Customers/GetCurrentCustomerForEdit';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  createOrUpdateUser(
    body: ICreateOrUpdateUserInput | undefined
  ): Observable<IPageResult<number>> {
    let url_ = '/api/services/app/User/CreateOrUpdateUser';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);
    const options: HttpOptions = {
      url: url_,
      body: content_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.post(options);
  }

  registerCustomer(
    body: RegisterMemberInput | undefined
  ): Observable<RegisterOutput> {
    let url_ = '/api/services/app/Account/RegisterWheelCustomer';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);

    const options: HttpOptions = {
      url: url_,
      body: content_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.post(options);
  }

  getUsers(
    filter?: string | null | undefined,
    permissions?: string[] | null | undefined,
    role?: number | null | undefined,
    sorting?: string | null | undefined,
    onlyLockedUsers = false,
    maxResultCount = 10,
    skipCount = 0
  ): Observable<IPageResultList<IUser>> {
    let url_ = '/api/services/app/User/GetUsers?';
    if (filter) url_ += 'Filter=' + encodeURIComponent('' + filter) + '&';
    if (permissions)
      permissions &&
        permissions.forEach((item) => {
          url_ += 'Permissions=' + encodeURIComponent('' + item) + '&';
        });
    if (role) url_ += 'Role=' + encodeURIComponent('' + role) + '&';
    if (onlyLockedUsers === null)
      throw new Error("The parameter 'onlyLockedUsers' cannot be null.");
    else if (onlyLockedUsers)
      url_ +=
        'OnlyLockedUsers=' + encodeURIComponent('' + onlyLockedUsers) + '&';
    if (sorting) url_ += 'Sorting=' + encodeURIComponent('' + sorting) + '&';
    if (maxResultCount === null)
      throw new Error("The parameter 'maxResultCount' cannot be null.");
    else if (maxResultCount)
      url_ += 'MaxResultCount=' + encodeURIComponent('' + maxResultCount) + '&';
    if (skipCount === null)
      throw new Error("The parameter 'skipCount' cannot be null.");
    else if (skipCount)
      url_ += 'SkipCount=' + encodeURIComponent('' + skipCount) + '&';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  isValidUserByEmail(email?: string | null | undefined): Observable<IUser> {
    let url_ = '/api/services/app/User/IsValidUserByEmail?';
    if (email) url_ += 'email=' + encodeURIComponent('' + email) + '&';

    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.post(options);
  }

  resetPassword(
    body: IResetPasswordInput | undefined
  ): Observable<IPageResult<IResetPasswordOutput>> {
    let url_ = '/api/services/app/Account/ResetPassword';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);

    const options: HttpOptions = {
      body: content_,
      url: url_,
    };

    return this.http.post(options);
  }

  signIn(userNameOrEmailAddress, password, forceReload = false) {
    const body = {
      userNameOrEmailAddress,
      password,
      rememberClient: false,
      returnUrl: null,
      singleSignIn: false,
      twoFactorRememberClientToken: null,
      // shouldChangePasswordOnNextLogin: true,
    };
    return this.auth.authenticate(body, CONSTANT.USER_TOKEN_KEY, forceReload);
  }

  logOut() {
    return this.auth.logOut();
  }

  saveUser(user: IUser) {
    if (user) {
      this.localStorageService.saveUser(user);
      this.userSubject.next(user);
    }
  }

  savePhoneNumber(phoneNumber: string, prefix: string) {
    if (phoneNumber) {
      AppSession.user.phoneNumber = phoneNumber;
      AppSession.user.phonePrefix = prefix;

      this.userSubject.next(null);
    }
  }

  getUserFromLocal() {
    return AppSession.user;
  }

  callBackLogOutEvent() {
    this.userSubject.next(null);
  }

  sendPasswordResetCode(
    body: ISendPasswordResetCodeInput | undefined
  ): Observable<void> {
    let url_ = '/api/services/app/Account/SendPasswordResetCode';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);
    const options: HttpOptions = {
      body: content_,
      url: url_,
    };

    return this.http.post(options);
  }

  resolveTenantId(
    body: IResolveTenantIdInput | undefined
  ): Observable<IPageResult<number>> {
    let url_ = '/api/services/app/Account/ResolveTenantId';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);
    const options: HttpOptions = {
      body: content_,
      url: url_,
    };

    return this.http.post(options);
  }

  updateCustomer(
    body: CreateCustomerDto | undefined
  ): Observable<IPageResult<number>> {
    let url_ = '/api/services/app/Customers/CreateOrEdit';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);
    console.log('body', body);
    const options: HttpOptions = {
      url: url_,
      body: content_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.post(options);
  }

  getCurrentLoginInformations(): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/Session/GetWheelLoginInformations';

    var customerId = this._cookieService.get("Abp-CustomerId");

    if (customerId) {
      url_ += '?customerId=' + customerId;
    }

    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<any>>(options);
  }
}

import { IPageResult } from 'src/model/page-result.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../common/config/http-client/http-client.service';
import { HttpOptions, TokenType } from '../common';

@Injectable({
  providedIn: 'root'
})
export class WheelLocationsService {

  constructor(
    private http: HttpClientService,
  ) { }

  getWheelLocationsByTenant(): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/WheelSetups/GetWheelSetupLocationsByTenant';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getWheelLocationForEdit(locationId: number): Observable<IPageResult<any>> {
    let url_ = `/api/services/app/WheelLocations/GetWheelLocationForEdit?Id=${locationId}`;
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getAllWheelTableForTableDropdown(wheelLocationId: number | undefined, wheelDepartmentId: number | undefined): Observable<IPageResult<any>> {
    let url_ = `/api/services/app/WheelTable/GetAllWheelTableForTableDropdown?wheelLocationId=${wheelLocationId}&wheelDepartmentId=${wheelDepartmentId}`
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getWheelDepartments(): Observable<IPageResult<any>> {
    let url_ = "/api/services/app/WheelDepartments/GetWheelDepartments";
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getWheelDepartment(departmentId: number): Observable<IPageResult<any>> {
    let url_ = "/api/services/app/WheelDepartments/GetWheelDepartment?departmentId=" + departmentId;
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService, HttpOptions, TokenType } from '../common';
import { ERPComboboxItem } from '../service-proxies/service-proxies';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private http: HttpClientService,
  ) { }
  getLookups(
    type: string | null | undefined, tenantId: number | null | undefined, parentId: number | null | undefined
  ): Observable<any> {
    let url_ = '/api/services/app/Common/GetLookups?';

    if (type !== undefined && type !== null)
      url_ += "type=" + encodeURIComponent("" + type) + "&";
    if (tenantId !== undefined && tenantId !== null)
      url_ += "tenantId=" + encodeURIComponent("" + tenantId) + "&";
    if (parentId !== undefined && parentId !== null)
      url_ += "parentId=" + encodeURIComponent("" + parentId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
}

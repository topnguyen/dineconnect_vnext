import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { IPageResult } from 'src/model';
import { HttpClientService, HttpOptions, TokenType } from '../common';
import { WheelCartItemDto, WheelGetCartOutputDto } from '../service-proxies/service-proxies';

@Injectable({
  providedIn: 'root',
})
export class WheelOrderService {
  private prodCount: number = 0;
  public prodCountCountChange: Subject<number> = new Subject();

  updateCount(count: number = 0): void {
    this.prodCount = count;
    this.prodCountCountChange.next(this.prodCount);
  }

  constructor(
    private http: HttpClientService,
    private httpClient: HttpClient
  ) { }

  getWheelOrdersByUser(userId): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/WheelOrder/GetAlllWheelOrders';
    url_ += '?customerId=' + encodeURIComponent('' + userId) + '&';

    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getPageToBack(pageId, wheelDepartermentId, isGenerateQRCode): Observable<IPageResult<number>> {
    if (isGenerateQRCode == undefined) {
      isGenerateQRCode = true;
    }
    let url_ = '/api/services/app/WheelOrder/GetPageGoToBack';
    url_ += `?currentPage=${encodeURIComponent(
      '' + pageId
    )}&wheelDepatermentId=${encodeURIComponent('' + wheelDepartermentId)}&isGenerateQRCode=${isGenerateQRCode}`;
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getCart(includeCartTotal?: boolean): Observable<IPageResult<any>> {

    let url_ = '/api/services/app/WheelCheckout/GetCartItems';

    if (includeCartTotal) {
      url_ += "?includeCartTotal=true"
    }

    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  generateKey(item: any, isComboItem: boolean = false): string {
    let keys = ['menuId-' + item.screenMenuItemId];

    if (item.portion) {
      keys.push('size-' + item.portion?.id);
    }

    if (isComboItem) {
      keys.push('quantity-' + (item.quantity ?? 1));
    }

    if (item.tags) {
      item.tags.forEach((tag) => {
        keys.push('tag-' + tag.id + '-quantity-' + (tag.quantity ?? 1));
      });
    }

    return keys.join('-');
  }

  generateComboKey(item: any): string {
    let keys = [
      this.generateKey(item)
    ];
  
    if (item.subItems) {
      item.subItems.forEach((subItem) => {
        keys.push(this.generateKey(subItem, true));
      });
    }

    return keys.join('-');
  }

  addOrderItemToCart(item: any, isCombo: boolean = false) {
    let input = {
      key: isCombo ? this.generateComboKey(item) : this.generateKey(item),
      screenMenuItemId: item.screenMenuItemId,
      quantity: item.quantity,
      tags: item.tags,
      portion: item.portion,
      note: item.note,
      subItems: item.subItems
    };

    let url_ = '/api/services/app/WheelCheckout/AddMenuItemToCart';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body: input,
    };

    return this.http.post<IPageResult<number>>(options).subscribe(t => {
      this.updateCount(t.result);
    });
  }

  updateOrderItemToCart(item: any) {
    let url_ = '/api/services/app/WheelCheckout/AddMenuItemToCart';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      body: item,
    };

    return this.http.post<IPageResult<number>>(options).subscribe(t => {
      this.updateCount(t.result);
    });
  }

  removeOrderItemFromCart(item: any, callback?) {
    let url_ = '/api/services/app/WheelCheckout/RemoveItemFromCart?screenMenuItemId=' + item.screenMenuItemId + '&key=' + item.key;
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
    };

    return this.http.delete(options).subscribe(t => {
      if (callback)
        callback();
    });
  }
}

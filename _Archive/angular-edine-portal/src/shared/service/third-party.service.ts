import { debounceTime } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IPostalCodeResponse } from './../../model/third-party-postal-code.model';

@Injectable({
  providedIn: 'root',
})
export class ThirdPartyService {
  apiPostalCodeUrl = environment.postalCodeUrl;
  apiKey = environment.postalCodeApiKey;
  apiSecret = environment.posttalCodeApiSecret;

  constructor(private http: HttpClient) {}

  searchPostalCode(postcode: string) {
    // buildingBlock  need to provide at least 1 character
    // streetName  need to provide at least 3 character
    const body = {
      APIKey: this.apiKey,
      APISecret: this.apiSecret,
      Postcode: postcode,
    };
    const url = this.apiPostalCodeUrl;
    const formData: any = new FormData();
    formData.append('APIKey', body.APIKey);
    formData.append('APISecret', body.APISecret);
    formData.append('Postcode', body.Postcode);

    return this.http
      .post<IPostalCodeResponse>(url, formData)
      .pipe(debounceTime(800));
  }
}

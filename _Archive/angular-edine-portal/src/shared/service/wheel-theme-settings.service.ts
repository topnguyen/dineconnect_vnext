import { IPageResult } from 'src/model/page-result.model';
import { Injectable } from '@angular/core';
import { HttpClientService } from '../common/config/http-client/http-client.service';
import { HttpOptions, TokenType } from '../common';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WheelThemeSettingsService {

  constructor(
    private http: HttpClientService,
  ) { }

  getWheelThemeSettings(theme: string): Observable<IPageResult<any>> {
    let url_ = `/api/services/app/WheelCustomizationSettings/GetUiManagementSetting?themeId=${theme}`;
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
}

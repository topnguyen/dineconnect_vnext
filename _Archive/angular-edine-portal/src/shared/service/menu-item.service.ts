import {
  AddItemToCartInputDto,
  NameValue,
  OrderSettingDto,
  WheelCartItemDto,
} from './../service-proxies/service-proxies';
import { map, retry } from 'rxjs/operators';
import { IPageResult } from './../../model/page-result.model';
import { TokenType } from './../common/config/http-client/http-options.model';
import { ICategory } from './../../model/category.model';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import {
  IPageResultList,
  IGetMenuItemForEditOutput,
  MenuItemData,
  WheelMenuCombos,
} from 'src/model';
import { LocalStorageService, HttpOptions } from '../common';
import { HttpClientService } from './../common/config/http-client/http-client.service';
import {
  IWheelScreenMenusListDto,
  WheelScreenMenuForEditDto,
} from '../service-proxies/service-proxies';
import { LeftMenu } from 'src/model/left-menu.model';
import { OrderModel } from 'src/model/order.model';
import {
  CustomMenuItemData,
  IMenuItem,
} from 'src/model/custom-menu-item.model';
import * as _ from 'lodash';
import { ICartModel, IOrderItem, IOrderItemTag } from 'src/model/cart.model';
import { pullAt } from 'lodash';
import { TypeScriptEmitter } from '@angular/compiler';
import { WheelOrderService } from './wheel-order.service';
@Injectable({
  providedIn: 'root',
})
export class MenuItemService {
  $menuItemSubject = new Subject<MenuItemData[]>();
  $firstSectionSubject = new Subject<MenuItemData[]>();
  $commonMenuIDSubject = new Subject();
  $menuIDSubject = new Subject();
  $leftMenuDataSubject = new Subject<LeftMenu[]>();
  $closeLeftMenuSubject = new Subject<boolean>();
  orderList: CustomMenuItemData[] = [];

  constructor(
    private localStorageService: LocalStorageService,
    private orderService: WheelOrderService,
    private http: HttpClientService
  ) { }

  getAllCategory(
    filter?: string | null,
    sorting?: string | null,
    maxResultCount?: number | undefined,
    skipCount?: number | undefined
  ): Observable<IPageResultList<ICategory>> {
    let url_ = '/api/services/app/Category/GetAll?';
    if (filter !== undefined) {
      url_ += 'Filter=' + encodeURIComponent('' + filter) + '&';
    }
    if (sorting !== undefined) {
      url_ += 'Sorting=' + encodeURIComponent('' + sorting) + '&';
    }
    if (maxResultCount === null) {
      throw new Error("The parameter 'maxResultCount' cannot be null.");
    } else if (maxResultCount !== undefined) {
      url_ += 'MaxResultCount=' + encodeURIComponent('' + maxResultCount) + '&';
    }
    if (skipCount === null) {
      throw new Error("The parameter 'skipCount' cannot be null.");
    } else if (skipCount !== undefined) {
      url_ += 'SkipCount=' + encodeURIComponent('' + skipCount) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get<IPageResultList<ICategory>>(options);
  }

  getAllMenuItems(
    filter?: string | null,
    sorting?: string | null,
    maxResultCount?: number | undefined,
    skipCount?: number | undefined
  ): Observable<IPageResultList<IMenuItem>> {
    let url_ = '/api/services/app/MenuItem/GetAll?';
    if (filter !== undefined) {
      url_ += 'Filter=' + encodeURIComponent('' + filter) + '&';
    }
    if (sorting !== undefined) {
      url_ += 'Sorting=' + encodeURIComponent('' + sorting) + '&';
    }
    if (maxResultCount === null) {
      throw new Error("The parameter 'maxResultCount' cannot be null.");
    } else if (maxResultCount !== undefined) {
      url_ += 'MaxResultCount=' + encodeURIComponent('' + maxResultCount) + '&';
    }
    if (skipCount === null) {
      throw new Error("The parameter 'skipCount' cannot be null.");
    } else if (skipCount !== undefined) {
      url_ += 'SkipCount=' + encodeURIComponent('' + skipCount) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get<IPageResultList<IMenuItem>>(options);
  }

  getAll(
    filter?: string | null | undefined
  ): Observable<IPageResultList<IWheelScreenMenusListDto>> {
    let urlString = '/api/services/app/WheelScreenMenus/GetAll?';
    if (filter !== undefined && filter !== null) {
      urlString += 'Filter=' + encodeURIComponent('' + filter) + '&';
    }
    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResultList<IWheelScreenMenusListDto>>(options);
  }

  getChildren(id: number | undefined): Observable<IPageResult<MenuItemData[]>> {
    let urlString = '/api/services/app/WheelScreenMenus/GetChildren?';
    if (id === null) {
      throw new Error("The parameter 'id' cannot be null.");
    } else if (id !== undefined) {
      urlString += 'Id=' + encodeURIComponent('' + id) + '&';
    }
    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<MenuItemData[]>>(options);
  }

  getLefMenu(id: number): Observable<IPageResult<LeftMenu[]>> {
    let urlString = '/api/services/app/WheelScreenMenus/GetSectionChildren?';
    urlString += 'menuId=' + encodeURIComponent('' + id) + '&';
    urlString = urlString.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: urlString,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<LeftMenu[]>>(options);
  }

  getMenuItemChildren(
    filter: string | null | undefined,
    id: number | undefined
  ): Observable<IPageResult<MenuItemData[]>> {
    let urlString = '/api/services/app/WheelScreenMenuItems/GetChildren?';
    if (filter !== undefined && filter !== null) {
      urlString += 'Filter=' + encodeURIComponent('' + filter) + '&';
    }
    if (id === null) {
      throw new Error("The parameter 'id' cannot be null.");
    } else if (id !== undefined) {
      urlString += 'Id=' + encodeURIComponent('' + id) + '&';
    }
    urlString = urlString.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: urlString,
      cacheMins: 5,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<MenuItemData[]>>(options);
  }

  getMenuItemForEdit(
    id: number | null | undefined
  ): Observable<IPageResult<IGetMenuItemForEditOutput>> {
    let url_ = '/api/services/app/MenuItem/GetMenuItemForEdit?';
    if (id !== undefined) {
      url_ += 'Id=' + encodeURIComponent('' + id) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      cacheMins: 3,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }

  getWheelScreenMenuForEdit(
    id: number | undefined
  ): Observable<IPageResultList<WheelScreenMenuForEditDto>> {
    let url_ = '/api/services/app/WheelScreenMenus/GetWheelScreenMenuForEdit?';
    if (id === null) {
      throw new Error("The parameter 'id' cannot be null.");
    } else if (id !== undefined) {
      url_ += 'Id=' + encodeURIComponent('' + id) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      cacheMins: 3,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get<IPageResultList<WheelScreenMenuForEditDto>>(options);
  }

  createMaxIdentical(items: IMenuItem[]) {
    let maxIdentical = 0;
    items.map((x) => {
      maxIdentical =
        maxIdentical > x.tempOrderId ? maxIdentical : x.tempOrderId;
    });
    return maxIdentical + 1;
  }

  getWheelOrder(invoiceNo: string): Observable<IPageResult<OrderModel>> {
    let url_ = '/api/services/app/WheelOrder/GetWheelOrder?';
    if (invoiceNo !== undefined) {
      url_ += 'invoiceNo=' + encodeURIComponent('' + invoiceNo) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      cacheMins: 1,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<OrderModel>>(options);
  }

  getSubCategories(
    menuId: number,
    categoryId: number
  ): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/WheelScreenMenus/GetSubCategories?';
    if (menuId !== undefined) {
      url_ += 'menuId=' + encodeURIComponent('' + menuId) + '&';
    }
    if (categoryId !== undefined) {
      url_ += 'categoryId=' + encodeURIComponent('' + categoryId) + '&';
    }
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<any>>(options);
  }

  getMenuItemsData(
    menuId: number,
    categoryId: number,
    subCategory: string,
    pageSize: number,
    pageNumber: number,
    allowPreOrder
  ): Observable<IPageResult<any>> {
    let url_ = '/api/services/app/WheelScreenMenus/GetSectionMenu?';
    if (menuId !== undefined) {
      url_ += 'menuId=' + encodeURIComponent('' + menuId) + '&';
    }
    if (categoryId !== undefined) {
      url_ += 'categoryId=' + encodeURIComponent('' + categoryId) + '&';
    }
    if (subCategory !== undefined) {
      url_ += 'subCategory=' + encodeURIComponent('' + subCategory) + '&';
    }
    if (pageSize !== undefined) {
      url_ += 'pageSize=' + pageSize + '&';
    }
    if (pageSize !== undefined) {
      url_ += 'pageNumber=' + pageNumber + '&';
    }
    if (allowPreOrder !== undefined) {
      url_ += 'allowPreOrder=' + allowPreOrder;
    }
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get<IPageResult<any>>(options);
  }

  generateKey(item: CustomMenuItemData): string {
    const edges = ['menuId-' + item.id, 'size-' + item.portions.id];
    const tagIds = item.tags.map((tag) => {
      return tag.id;
    });
    const keys = [...edges, tagIds].sort();
    return keys.join('-');
  }

  // totalItem is null => calculate the price per item
  // totalItem have value => calculate the price by total items
  calculatePrice(
    basePrice: number,
    orderItems: IOrderItem[],
    totalItems?: number
  ): number {
    let addOnPrice = 0;
    orderItems.forEach((e) => {
      addOnPrice += e.totalPrice;
    });
    return (basePrice + addOnPrice) * (totalItems ?? 1);
  }

  generateComboKey(item: WheelMenuCombos): string {
    const edges = [
      'menuId-' + item.screenMenuId,
      'categoryId-' + item.screenCategoryId,
      'comboId-' + item.id,
    ];
    let orderItem = [];
    item.wheelMenuComboItems.map((e) => {
      orderItem = [...orderItem, ...e.itemSelect];
    });
    let keys = orderItem.map((e) => 'orderItem-' + e.id);

    for (const iterator of orderItem) {
      const tagIds = iterator.orderTags.map((orderTag) => {
        const tagChecked = orderTag.tags
          .filter((x) => x.checked)
          .map((e) => e.id + e.name);
        return tagChecked;
      });
      const mergedTagIds = Array.prototype.concat.apply([], tagIds);
      if (mergedTagIds.length > 0) {
        keys = [...keys, ...mergedTagIds];
      }
    }

    const finalKeys = [...edges, ...keys].sort();
    return finalKeys.join('-');
  }

  generateComboItemKey(comboId: number, item: CustomMenuItemData): string {
    const edges = ['combo-' + comboId, 'menuId-' + item.id];
    const tagIds = item.tags.map((tag) => {
      return tag.id;
    });
    const keys = [...edges, tagIds].sort();
    return keys.join('-');
  }
}
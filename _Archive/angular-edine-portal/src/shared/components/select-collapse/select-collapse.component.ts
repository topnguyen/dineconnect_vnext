import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectCollapseData } from 'src/shared/constant';
import { AccountService } from 'src/shared/service';
@Component({
  selector: 'app-select-collapse',
  templateUrl: './select-collapse.component.html',
  styleUrls: ['./select-collapse.component.scss'],
})
export class SelectCollapseComponent
  implements OnInit, OnDestroy
{
  @ViewChild('selectTitleRef', { static: false }) selectTitleRef: ElementRef;
  @ViewChild('selectContentRef', { static: false })  selectContentRef: ElementRef;

  @Input() selectMenuTitle: string = '';
  @Input() selectMenuData: SelectCollapseData[] = [];
  @Output() menuItemSelected = new EventEmitter<SelectCollapseData>();
  urlActived: string;
  collapsed: boolean = true;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(private readonly accountService: AccountService,
    private router: Router) {}

  ngOnInit(): void {
    this.urlActived =  this.router.url;
  }

  handleOnClick(item: any) {
    this.menuItemSelected.emit(item);
    if(item.url){
      this.router.navigateByUrl(item.url);
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
  }

  toggleStatus(): void {
    this.collapsed = !this.collapsed;
  }
}

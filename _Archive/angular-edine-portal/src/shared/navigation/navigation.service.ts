import { CommonService } from 'src/shared/common';
import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(
    private router: Router,
    private location: Location,
    private commonService: CommonService
  ) { }

  navigateToLocation() {
    this.router.navigate(["location"]);
  }

  navigateToLogin() {
    return this.router.navigate(["sign-in"]);
  }

  navigateToSignUp(userEmail?: string) {
    this.router.navigate(["sign-up"]).then(() => {
      this.commonService.$userEmail.next(userEmail);
    });
  }

  navigateToPrevious() {
    this.location.back();
  }
}

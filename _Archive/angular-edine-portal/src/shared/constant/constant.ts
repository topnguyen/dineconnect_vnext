import { UserLoginInfoDto, TenantLoginInfoDto, ApplicationInfoDto, UiCustomizationSettingsDto } from '../service-proxies/service-proxies';
import { StartUpService } from './../common/config/start-up/start-up.service';
export enum CONSTANT {
  TEXT = 1,
  STORAGE_KEY = 'orderItems',
  DELIVERY_LOCALTION_KEY = 'deliveryLocation',
  USERKEY = 'validUser',
  META_DATA_TOKEN_KEY = 'metadataAuthenticate',
  USER_TOKEN_KEY = 'userAuthenticate',
  TENANT_AVAILABLE_KEY = 'tenantAvailable',
  ADDRESS_DETAIL = 'addressDetail',
  CART_ORDER = 'CART_ORDER',
}

export enum PAYMENT_METHOD {
  CREDIT_CARD = 'creditCard',
  CASH_ON_DELIVERY = 'cashOnDelivery',
  CARD_ON_DELIVERY = 'cardOnDelivery',
}
export enum PAGE {
  Depaterment = 0,
  PostalCode = 1,
  Location = 2,
  PhoneNumber = 3,
  Language = 4,
  MenuOrder = 5,
  Payment = 6,
  PaymentInfor = 7,
  DateOrderFututer = 8,
  VerifyCode = 9
}

export class AppConsts {
  static readonly tenancyNamePlaceHolderInUrl = '{TENANCY_NAME}';
  static tenancyName: string;
  static remoteServiceBaseUrl: string;
  static remoteServiceBaseUrlFormat: string;
  static appBaseUrl: string;
  static appBaseUrlFormat: string;
  static appBaseHref: string;
  static tenantId: number;

  static readonly localization = {
    defaultLocalizationSourceName: 'DineConnect',
  };

  static readonly authorization = {
    encrptedAuthTokenName: 'enc_auth_token',
  };

  static readonly UserInfocation
}

export class wheelDataSetups {
  static checkOutPageId: number;
  static homePageId: number;
  static id: number;
  static logoCompanyUrl: string;
  static welcomeUrl: string;
  static advertOneUrl: string;
  static advertTwoUrl: string;
  static locations: Array<Object>;
  static homePageObject: Object;
  static currency: string;
  static oneMapToken: string;
  static title: string;
  static welcomeMessage: string;
  static termsAndCondtions: string;
  static wheelPostCodeServiceType: number;
  static isDisplayHomePage: boolean;
}

export class StepName {
  static chooseLocation = 'Choose Location';
  static chooseOrderType = 'Choose Order Type';
  static menu = 'Menu';
  static combo = 'Combo';
  static myOrder = 'My Order';
  static login = 'Customer Login';
  static signUp = 'Sign Up';
  static myProfile = 'My Profile';
  static guestInfo = 'Guest Information';
  static payment = 'Payment';
  static orderStatus = 'Order Status';
}

export class AnimationRouteItems {
  static home = 'Home';
  static location = 'Location';
  static orderType = 'OrderType';
  static menuItems = 'MenuItems';
  static combo = 'Combo';
  static login = 'SignIn';
  static signUp = 'SignUp';
  static myProfile = 'MyProfile';
}

export class RegexPatterns {
  static emailRegex =
    /^([A-Za-z0-9_\-\.]){1,}\@([A-Za-z0-9_\-\.]){1,}\.([A-Za-z]){2,4}$/;
}

export interface SelectCollapseData {
  label: string;
  value: string;
  icon: string;
  active?: boolean;
  url?: string;
}


export class AppSession {
  static user: any;
}

export class OrderSetting {
  static accepted: boolean;
  static width: number;
  static height: number;
  static autoRequestDeliver: boolean;
  static askTheMobileNumberFirst: boolean;
  static signUp: boolean;
}
import { ICategory } from './../../model/category.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myCategoryFilter',
  pure: false,
})
export class MyCategoryFilterPipe implements PipeTransform {
  transform(items: ICategory[], filter: string): ICategory[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter(
      (item) =>
        item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );
  }
}

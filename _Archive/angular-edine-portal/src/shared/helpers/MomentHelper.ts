import { CommonLocationGroupDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';

export class MomentHelper {
  static readonly timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  static newMoment(inp?: moment.MomentInput, format?: moment.MomentFormatSpecification, language?: string, strict?: boolean): moment.Moment {
    return moment(inp, format, language, strict).tz(MomentHelper.timezone, true);
  }
}

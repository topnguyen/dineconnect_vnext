export class StringValidationHelper {
    static IsInteger(value: string) {
        return /^\d+$/.test(value);
    }

    static IsAlphanumeric(value: string) {
        return /^[0-9a-zA-Z]+$/.test(value);
    }

    static IsDecimal(value: string) {
        return /^[0-9]*\.?[0-9]*$/.test(value);
    }
} 
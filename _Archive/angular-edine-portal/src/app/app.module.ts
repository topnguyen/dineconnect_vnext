import {
  NgModule,
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { StorageServiceModule } from 'ngx-webstorage-service';
import {
  StartUpService,
  StartUpFactory,
} from './../shared/common/config/start-up/start-up.service';
import { MainModule } from './main/main.module';
import { AppComponent } from './app.component';
import { LocationService } from 'src/shared/service';
import { AppRoutingModule } from './app-routing.module';
import { LocalStorageService, HttpClientService } from '../shared/common';
import { AppInterceptor } from 'src/shared/common/authenticate/app.interceptor';
import { PlatformLocation } from '@angular/common';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BlockUIModule } from 'primeng/blockui';
import { SidebarModule } from 'primeng/sidebar';
import { ServiceProxiesModule } from 'src/shared/service-proxies/service-proxies.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { CookieService } from 'ngx-cookie-service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    StorageServiceModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    ProgressSpinnerModule,
    BlockUIModule,
    AppRoutingModule,
    MainModule,
    SidebarModule,
    ServiceProxiesModule,
    NgxSpinnerModule,
    ButtonModule,
    RippleModule,
    NgxIntlTelInputModule,
    DataViewModule,
    DialogModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    CookieService,
    StartUpService,
    LocalStorageService,
    HttpClientService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: StartUpFactory,
      deps: [StartUpService, PlatformLocation],
      multi: true,
    },
    LocationService,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}

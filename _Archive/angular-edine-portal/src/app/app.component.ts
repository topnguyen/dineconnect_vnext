import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectorRef, Component } from '@angular/core';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { PrimeNGConfig } from 'primeng/api';
import { CommonService, MenuItemService } from 'src/shared/common';
import { slideInAnimation } from './main/animations';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts, wheelDataSetups } from './../shared/constant/constant';
import * as $ from 'jquery';
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import { LOGO_URL } from 'src/shared/shared.const';
import { timer } from 'rxjs';
import { TenantService } from './tenant/tenant.service';
import { WheelDynamicPageEditDto } from 'src/shared/service-proxies/service-proxies';
import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation],
})
export class AppComponent {
  formGroup: FormGroup;
  isWheelSetupsAvailable: boolean;
  isBusy: boolean;
  totalPrice: number;
  logoUrl: string;
  isHideHeader = false;
  isHideFooter = false;
  isWith70 = false;
  isReady = false;

  isDisplayAd = false;
  isDisplayWelcome = false;

  isLogoAvailable = false;

  adTimer: string = '';
  srcAdLoading: string = '';
  srcAds: string[] = [];
  favIcon: HTMLLinkElement = document.querySelector('#appIcon');
  remoteServiceBaseUrl = AppConsts.remoteServiceBaseUrl;
  herfIcon = '';
  tenant: any;
  homePage: any = null;
  isDisplayHomePage = false;

  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
  errPhoneMsg = 'Phone number is missing.';

  constructor(
    private router: Router,
    private commonService: CommonService,
    private primengConfig: PrimeNGConfig,
    private menuItemService: MenuItemService,
    private cdRef: ChangeDetectorRef,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private tenantService: TenantService,
    private deviceService: DeviceDetectorService
  ) {
    this.formGroup = this.fb.group({
      phoneNumber: [null, [Validators.required]],
    });
    document.title = wheelDataSetups.title;
    this.isDisplayHomePage = wheelDataSetups.isDisplayHomePage;
    //show welcome Screen =>> home page >> logo company
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/') {
          this.isReady = false;
          let timeDelay = 3;
          if (wheelDataSetups.logoCompanyUrl) {
            this.logoUrl = wheelDataSetups.logoCompanyUrl;
            this.isLogoAvailable = true;
          }
          if (wheelDataSetups.homePageId && wheelDataSetups.isDisplayHomePage) {
            this.homePage = wheelDataSetups.homePageObject;
            this.appendCss(this.homePage.css);
          }
          window.sessionStorage.setItem(
            LOGO_URL,
            wheelDataSetups.logoCompanyUrl
          );
          if (this.logoUrl && this.isLogoAvailable) {
            this.spinner.show();
          } else if (wheelDataSetups.isDisplayHomePage && !this.isLogoAvailable) {
            this.isDisplayHomePage = wheelDataSetups.isDisplayHomePage;
          }
          else {
            this.spinner.hide();
            this.isReady = true;
          }
          this.commonService.setIsWelComePage(true);
          //show hompage
          if (this.logoUrl && this.isLogoAvailable) {
            setTimeout(() => {
              this.spinner.hide();

              this.isLogoAvailable = false;
              if (this.isDisplayHomePage == false) {
                this.isReady = true;
              }
            }, timeDelay * 1000);
          }
        } else {
          this.isDisplayHomePage = false;
          if (this.isLogoAvailable) {
            this.spinner.show();
            timer(2000).subscribe(() => {
              this.spinner.hide();
            });
          }
          this.isReady = true;

          this.commonService.setIsWelComePage(false);
        }

      }
    });
    if (this.isDisplayHomePage == false) {
      this.isDisplayWelcome = false;
      setTimeout(() => {
        this.isReady = true;
      }, 1000);
    }
    this.isWheelSetupsAvailable = this.commonService.isWheelSetupsAvailable;
  }

  isShowNotificationPWA: boolean = false;
  isShowNotificationIOS: boolean = false;
  isShowNotificationAndroid: boolean = false;

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.tenant = this.tenantService.getTenantAvailable();
    this.tenant.tenantId = this.tenant.tenantId ? this.tenant.tenantId : 1;
    this.herfIcon =
      this.remoteServiceBaseUrl +
      '/TenantCustomization/GetFavicon?tenantId=' +
      this.tenant.tenantId +
      '&id=' +
      Date.now().toString();
    this.favIcon.href = this.herfIcon;
    // check divice and mode 
    if (!(window.matchMedia('(display-mode: standalone)').matches)) {
      if (this.deviceService.isMobile()) {
        this.isShowNotificationPWA = true;
        if (this.deviceService.device == 'iPhone') {
          this.isShowNotificationIOS = true;
        } else {
          this.isShowNotificationAndroid = true;
        }
      }
    }

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.commonService.$isBusy.subscribe((isBusy) => {
        this.isBusy = isBusy;
        this.cdRef.detectChanges();
      });
      this.menuItemService.$totalPrice.subscribe((totalPrice) => {
        this.totalPrice = totalPrice;
      });
      this.commonService.$themeSettings.subscribe((settings) => {
        this.initialTheme(settings);
      });
      this.commonService.$isHideHeaderSubject.subscribe((data: boolean) => {
        this.isHideHeader = data;
        this.cdRef.detectChanges();
      });
      this.commonService.$isHideFooterSubject.subscribe((data: boolean) => {
        this.isHideFooter = data;
        this.cdRef.detectChanges();
      });
      this.commonService.$isWith70Subject.subscribe((data: boolean) => {
        this.isWith70 = data;
        this.cdRef.detectChanges();
      });
    }, 0);
  }

  initialTheme(settings) {
    const { header, footer, content } = settings;

    let headerHeight = getComputedStyle(
      document.documentElement
    ).getPropertyValue('--height-header');
    let footerHeight = getComputedStyle(
      document.documentElement
    ).getPropertyValue('--height-footer');

    // --Header--
    document.documentElement.style.setProperty('--btn-dine-primary', '#fa8c16');
    document.documentElement.style.setProperty('--app-dine-bg', '#fff');
    document.documentElement.style.setProperty(
      '--header-background',
      header?.color
    );
    if (header?.desktopFixedHeader) {
      document.documentElement.style.setProperty('--header-position', 'fixed');
      document.documentElement.style.setProperty(
        '--margin-content-top',
        headerHeight
      );
    }
    // --End Header--

    // --Content--
    document.documentElement.style.setProperty(
      '--content-background',
      content?.color
    );
    // --End Content--

    // --Footer--
    document.documentElement.style.setProperty(
      '--footer-background',
      footer?.color
    );
    if (footer?.fixedFooter) {
      document.documentElement.style.setProperty('--footer-position', 'fixed');
      document.documentElement.style.setProperty(
        '--margin-content-bottom',
        footerHeight
      );
    }
    // --End Footer--
  }

  prepareRoute(outlet: RouterOutlet) {
    return (
      outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation
    );
  }

  private displayAD(): void {
    let timerInterval = null;
    const timeLimit = 6;
    let timeLeft: number = 5;
    let timePassed: number = 0;
    let srcAdIndex: number = 0;
    this.srcAdLoading = this.srcAds[srcAdIndex];
    this.isDisplayAd = true;

    timerInterval = setInterval(() => {
      ++timePassed;
      timeLeft = timeLimit - timePassed;
      if (timeLeft > 0) {
        this.adTimer = timeLeft.toString();
      } else {
        ++srcAdIndex;
        timeLeft = 0;
        timePassed = 0;
        this.adTimer = timeLeft.toString();
      }
      this.srcAdLoading = this.srcAds[srcAdIndex];
    }, 1000);
    setTimeout(() => {
      clearInterval(timerInterval);
      this.isDisplayAd = false;
    }, 6000 * this.srcAds.length);
  }
  appendCss(customData) {
    $(document).ready(function () {
      $('style').append(customData);
    });
  }
  openPageOrder() {
    if (wheelDataSetups.logoCompanyUrl) {
      //show logoCompany
      this.isDisplayWelcome = false;
      this.displayAD();
      this.logoUrl = wheelDataSetups.logoCompanyUrl;
      this.isLogoAvailable = true;
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
        this.isReady = true;
      }, 6000);
    } else {
      this.isDisplayWelcome = false;
      this.isDisplayHomePage = false;
      this.isReady = true;
    }
  }

  hideNotification() {
    this.isShowNotificationPWA = false;
  }
}

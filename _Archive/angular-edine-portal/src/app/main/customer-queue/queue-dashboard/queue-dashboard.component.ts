import { Component, OnDestroy, OnInit } from '@angular/core';
import {
    CustomerQueueDto,
    QueueLocationDto,
} from 'src/shared/service-proxies/service-proxies';
import { cloneDeep, orderBy } from 'lodash';
import { interval, Subscription } from 'rxjs';``
import { concatMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { DineQueueService } from 'src/shared/service/dine-queue.service';

@Component({
  templateUrl: './queue-dashboard.component.html',
  styleUrls: ['./queue-dashboard.component.scss']
})
export class QueueDashboardComponent implements OnInit, OnDestroy {
    locationId = this.activatedRoute.snapshot.params['id'];
    queueLocation = new QueueLocationDto();

    private _inteval$: Subscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private _dineQueueService: DineQueueService,
    ) {
    }

    ngOnInit(): void {
        this.getQueueLocationFullData();
    }

    ngOnDestroy(): void {
        if (this._inteval$) {
            this._inteval$.unsubscribe();
        }
    }

    getQueueLocationFullData() {
        this._dineQueueService.getQueueLocationFullDataByLocationId(this.locationId).subscribe((result) => {
            this.queueLocation = cloneDeep(result);
            this.queueLocation.queueLocationOptions = (this.queueLocation.queueLocationOptions || []).filter((option) => {
                return option.active;
            });

            if (!this._inteval$) {
                this._inteval$ = interval(30000)
                .pipe(concatMap(() => this._dineQueueService.getQueueLocationFullDataByLocationId(this.locationId)))
                .subscribe((result) => {
                    if (result) {
                        this.queueLocation = cloneDeep(result);
                        this.queueLocation.queueLocationOptions = (this.queueLocation.queueLocationOptions || []).filter((option) => {
                            return option.active;
                        });
                    }
                });
            }
        });
    }

    currentQueueIndex(customerQueues: CustomerQueueDto[] = []) {
        customerQueues = orderBy(customerQueues, ['queueTime'], 'asc');

        /* get first */
        const index = customerQueues.findIndex((item) => !item.finishTime);

        if (index >= 0) {
            return index;
        } else {
            return null;
        }
    }
}
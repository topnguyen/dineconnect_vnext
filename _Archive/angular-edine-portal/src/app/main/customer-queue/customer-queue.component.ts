
import { ActivatedRoute, Router } from '@angular/router';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import * as moment from 'moment';
import { cloneDeep, orderBy, findLastIndex, maxBy, findIndex } from 'lodash';
import { SearchCountryField, TooltipLabel, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';

import { SELECTED_DEPARTMENT, SELECTED_LOCATION } from './../../../shared/shared.const';
import { LOGO_URL } from 'src/shared/shared.const';

import { CustomerQueueDto, QueueLocationDto, QueueLocationOptionDto } from 'src/shared/service-proxies/service-proxies';
import { DineMessageService } from 'src/shared/common/common-service/message-top-down.service';
import { LocalStorageService } from 'src/shared/common/local-storage/local-storage.service';
import { interval, Subscription } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { DineQueueService } from 'src/shared/service/dine-queue.service';
import { wheelDataSetups } from 'src/shared/constant';
import { IUser } from './../../../model/user.model';
import { DomSanitizer } from '@angular/platform-browser';
import { DecimalPipe } from '@angular/common';

@Component({
    templateUrl: './customer-queue.component.html',
    styleUrls: ['./customer-queue.component.scss']
})
export class CustomerQueueComponent implements OnInit, OnDestroy, AfterViewInit {
    logoUrl: string = '';
    separateDialCode = false;
    preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
    errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
    errPhoneMsg = 'Phone number is missing.';
    locationId = this.activatedRoute.snapshot.params['id'];
    queueLocation = new QueueLocationDto();
    selectedQueueOption = new QueueLocationOptionDto();
    customerName: string;
    phoneNumber: {
        e164Number: string;
        dialCode: string;
    };
    userQueue: CustomerQueueDto;
    userQueueOption: QueueLocationOptionDto;
    isShowShareQueueMethod = false;
    queueFormGroup = new FormGroup({
        queueOption: new FormControl(undefined, [Validators.required]),
        phoneNumber: new FormControl(undefined, [Validators.required]),
        customerName: new FormControl(undefined, [Validators.required])
    });

    SearchCountryField = SearchCountryField;
    TooltipLabel = TooltipLabel;
    CountryISO = CountryISO;
    PhoneNumberFormat = PhoneNumberFormat;
    WheelDataSetups = wheelDataSetups;

    private _inteval$: Subscription;

    constructor(
        private router: Router,
        private decimalPipe: DecimalPipe,
        private domSanitizer: DomSanitizer,
        private activatedRoute: ActivatedRoute,
        private dineQueueService: DineQueueService,
        private dineMessageService: DineMessageService,
        private localStorageService: LocalStorageService,
        
    ) {
        this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
    }

    ngOnInit() {
        this._getQueueLocationFullData();
    }

    ngAfterViewInit() {
        const user: IUser = this.localStorageService.getUser();
        setTimeout(() => {
            this.queueFormGroup.controls['phoneNumber'].setValue(user ? user.phoneNumber.substr(3, user.phoneNumber.length) : null)
        }, 100);
    }

    ngOnDestroy(): void {
        this._inteval$.unsubscribe();
    }

    onSubmit(event): void {
        this.queueFormGroup.markAllAsTouched();
        this.queueFormGroup.updateValueAndValidity();
        if (this.queueFormGroup.invalid || !this.selectedQueueOption.id) {
            return;
        }
        this._createCustomerQueue();
    }

    onClickBack(): void {
        this.router.navigateByUrl('/location');
    }

    currentQueueIndex(customerQueues: CustomerQueueDto[] = []) {
        const currentQueue = maxBy(customerQueues, 'callTime');

        if (currentQueue) {
            return findIndex(customerQueues, (item) => item.id == currentQueue.id);
        } else {
            return null;
        }
    }

    order(): void {
        let selectedDepartment = JSON.parse(window.localStorage.getItem(SELECTED_DEPARTMENT));
        if (!selectedDepartment) {
            this.dineMessageService.showError('Please select Order Type');
            return;
        }

        const location = selectedDepartment.wheelLocationOutputs.find(item => item.locationId == this.locationId);
        this.router.navigateByUrl(`/location/${location.id}/type/${selectedDepartment.id}`);
    }

    shareQueue() {
        this.isShowShareQueueMethod = true;
    }

    removeQueue() {
        this.userQueue.seated = false;
        this.userQueue.finishTime = moment(moment.now());

        this.dineQueueService.createOrUpdateCustomerQueue(this.userQueue).subscribe((result) => {
            this.dineMessageService.showSuccess('Removed Successfully');
            this._getQueueLocationFullData();
        });
    }

    shareBywhatsapp() {
        const uri_enc = encodeURIComponent(`
            Store Name: ${this.queueLocation.name} 
            Queue Option: ${this.userQueueOption.name} 
            Ticket NO.: ${this.userQueue.queueNo} 
        `);

        var win = window.open(`https://wa.me?text=${uri_enc}`, '_blank');
        win.focus();
    }

    shareByemail() {
        const uri_enc = encodeURIComponent(`
            Store Name: ${this.queueLocation.name} 
            Queue Option : ${this.userQueueOption.name} 
            Ticket NO.: ${this.userQueue.queueNo} 
        `);

        const subject=`${this.queueLocation.name}`

        var win = window.open(`mailto:?subject=${subject}&body=${uri_enc}`, '_blank');
        win.focus();
    }

    shareBySMS() {
        const uri_enc = encodeURIComponent(`
            Store Name: ${this.queueLocation.name} 
            Queue Option : ${this.userQueueOption.name} 
            Ticket NO.: ${this.userQueue.queueNo} 
        `);

        const url = this.domSanitizer.bypassSecurityTrustResourceUrl(`sms:?body=${uri_enc}`) as any; 

        var win = window.open(url, '_blank');
        win.focus();
    }

    queueAgain() {
        this.userQueue.callTime = null;
        this.userQueue.finishTime = null;

        this.dineQueueService.createOrUpdateCustomerQueue(this.userQueue).subscribe((result) => {
            this.dineMessageService.showSuccess('Queue Again Successfull');
            this._getQueueLocationFullData();
        });
    }

    expectedQueue(option: QueueLocationOptionDto) {
        if (option.customerQueues.length == 0) {
            return option.prefix + '001'
        }

        const maxQueueNo = +option.customerQueues[option.customerQueues.length -1].queueNo.match(/\d+/g);
        return option.prefix + this.decimalPipe.transform(maxQueueNo + 1, '3.0');
    }

    private _getQueueLocationFullData() {
        this.dineQueueService.getQueueLocationFullDataByLocationId(this.locationId).subscribe((result) => {
            this.queueLocation = cloneDeep(result);
            this._userQueueQueue(); 
        });

        if (!this._inteval$) {
            this._inteval$ = interval(30000)
            .pipe(concatMap(() => this.dineQueueService.getQueueLocationFullDataByLocationId(this.locationId)))
            .subscribe((result) => {
                if (result) {
                    this.queueLocation = cloneDeep(result);
                    this._userQueueQueue();
                }
            });
        }
    }

    private _userQueueQueue() {
        this.userQueue = undefined;
        this.queueLocation.queueLocationOptions = (this.queueLocation.queueLocationOptions || []).filter((option) => {
            option.customerQueues.forEach((queue) => {
                const user: IUser = this.localStorageService.getUser();
                if (user && queue.customerMobile == user.phoneNumber) {

                    this.userQueueOption = option;
                    this.userQueue = queue || undefined;
                }
            });

            return option.active;
        });
    }

    private _createCustomerQueue() {
        const input = new CustomerQueueDto();
        input.queueTime = moment(moment.now());
        input.customerName = this.customerName;
        input.queueLocationOptionId = this.selectedQueueOption.id;
        input.customerMobile = this.phoneNumber.e164Number;

        this.dineQueueService.createOrUpdateCustomerQueue(input).subscribe((result) => {
            const user: IUser = this.localStorageService.getUser() || {};

            user.phoneNumber = this.phoneNumber.e164Number;
            user.phonePrefix = this.phoneNumber.dialCode;

            this.localStorageService.saveUser(user);
            this.dineMessageService.showSuccess('Created Queue Successfully');
            this._getQueueLocationFullData();
        });
    }

    get nowQueue() {
        return this.userQueueOption?.customerQueues[this.currentQueueIndex(this.userQueueOption?.customerQueues)];
    }
}

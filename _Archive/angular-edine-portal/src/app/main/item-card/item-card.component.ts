import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/shared/common';
import { CONSTANT, OrderSetting, wheelDataSetups } from 'src/shared/constant';
import { AccountService, MenuItemService } from 'src/shared/service';
import { OrderSettingDto } from 'src/shared/service-proxies/service-proxies';
import { MenuItemTagsConst } from 'src/shared/shared.const';
import { AppSession } from 'src/shared/constant';
@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss'],
})
export class ItemCardComponent implements OnInit {
  private _itemData: any;
  get itemData(): any {
    return this._itemData;
  }
  @Input() set itemData(value: any) {
    this._itemData = { ...value, calories: parseFloat(value.calories) };
  }
  @Input() isListStyle = true;
  @Input() sizeImg = new OrderSettingDto();
  @Input() isCombo = false;
  @Output() choseItem: EventEmitter<any> = new EventEmitter();
  @Output() showCalorie: EventEmitter<any> = new EventEmitter();
  @Input() colSetting = '';
  systemCurrency = wheelDataSetups.currency;
  userId = undefined;
  menuItemTags = MenuItemTagsConst;
  menuItemTagSelected: any[] = [];
  displayDialog: boolean;
  signUp: boolean = false;
  colImage = 'col-md-6';
  colDescription = 'col-md-6';
  constructor(
    public accountService: AccountService,
    private menuItemService: MenuItemService,
    private router: Router
  ) {
    
    if (AppSession && AppSession.user) {
        this.userId = AppSession.user.userId;
    }
  }
  orderSetting: any;
  ngOnInit(): void {
    this.menuItemTagSelected = this.menuItemTags.filter((el) => {
      if (this.itemData.tags != '' && this.itemData.tags != null) {
        let index = this.itemData.tags.includes(el.value);
        if (index) return el;
      }
    });
    this.calculatorColImage(this.colSetting);
  }

  calculatorColImage(input) {
    if (input == 'col-md-12' || input == 'col-md-6') {
      this.colImage = 'col-md-3';
      this.colDescription = 'col-md-9';
    } else if (input == 'col-md-4' || input == 'col-md-3') {
      this.colImage = 'col-md-12';
      this.colDescription = 'col-md-12';
    }
  }

  addItem(): void {
    if (OrderSetting) {
      if (!this.userId && OrderSetting.signUp) {
        this.router.navigateByUrl('/sign-in');
        return;
      }
      this.choseItem.emit(this.itemData);
    } else {
      this.choseItem.emit(this.itemData);
    }
  }

  displayVideo(event: Event) {
    this.displayDialog = true;
    event.preventDefault();
  }

  onDialogHide(): void {
    // this.selectedOrder = null;
  }
  onClickCalories() {
    if (this.itemData.nutritionInfo) {
      this.showCalorie.emit(this.itemData.nutritionInfo);
    }
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MenuItemData } from 'src/model';

@Component({
  selector: 'app-menu-content',
  templateUrl: './menu-content.component.html',
  styleUrls: ['./menu-content.component.scss'],
})
export class MenuContentComponent implements OnInit {
  @Input()
  set InputWheelMenuSection(data: string[]) {
    if (data) {
      this.wheelMenuSection = data;
    }
  }

  @Output() changeSubMenu = new EventEmitter<string>();

  wheelMenuSection: string[] = [];
  isShowSection = false;
  itemActivated: string;

  constructor() { }

  ngOnInit() {

  }

  selectionClick(item: string): void {
    if (this.itemActivated != item) {
      this.itemActivated = item;
      this.changeSubMenu.emit(item);
    }
  }
}

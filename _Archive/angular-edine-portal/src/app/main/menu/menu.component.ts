import { Component, OnInit, ViewChild } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IPageResult, ISimpleLocation, MenuItemData } from 'src/model';
import { CommonService, LocalStorageService } from 'src/shared/common';
import { MenuItemService, WheelLocationsService } from 'src/shared/service';
import { OrderSetting, PAGE, wheelDataSetups } from 'src/shared/constant';
import {
  DATE_TIME_DELIVERY,
  LOGO_URL,
  SCAN_QR_CODE,
  SELECTED_DEPARTMENT,
  SELECTED_FEE,
} from 'src/shared/shared.const';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';
import { Location, PlatformLocation } from '@angular/common';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { MenuContentComponent } from './components/menu-content/menu-content.component';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import _ from 'lodash';
import { CheckOutService } from 'src/shared/service/check-out.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    trigger('popupState', [
      state('default', style({})),
      state(
        'open',
        style({
          bottom: 0,
        })
      ),
      transition('default <=> open', animate(500)),
    ]),
    trigger('menuState', [
      state('hide', style({})),
      state(
        'show',
        style({
          bottom: 0,
        })
      ),
      transition('default <=> open', animate(500)),
    ]),
  ],
})
export class MenuComponent implements OnInit {
  @ViewChild('leftMenuRef', { static: false })
  leftSideMenuComponent: LeftSideMenuComponent;
  @ViewChild('subMenu', { static: false }) subMenu: MenuContentComponent;

  filterText: string = null;
  logoUrl: string = '';
  categories = [];
  departmentMenuID: number;
  menuActiveId: number;
  viewType: number;
  isDisplaySearchBox: boolean = false;
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  orderItems: number;
  orderList: CustomMenuItemData[] = [];
  cartModel: any[] = [];
  isOpenOrder = false;
  isShowMenu = false;
  state = 'default';
  menuState = 'hide';
  isViewDetailsSubtotal = true;
  subTotalPrice: number = 0;
  totalPrice: number = 0;
  systemCurrency = wheelDataSetups.currency;
  orderItem: any;
  deliveryLocation: ISimpleLocation;
  currencyPipe: '';
  isShowSection = true;

  OutputWheelMenuSection: MenuItemData[] = [];

  menuItems: MenuItemData[] = [];

  isShowSubMenu: boolean = true;
  allMenuItemsTags: any[] = [];
  selectedTag: string = '';
  topMenuItemData: MenuItemData;

  numberOfColumns: number;
  subCategories: string[] = [];

  currentSubCategory: string;
  pageSize: number = 4;
  currentPage = 0;

  throttle = 1000;
  scrollDistance = 1;
  scrollUpDistance = 2;
  isPreOrder = false;

  cartCount: number = 0;

  departmentId: number;
  department: any;
  tableNo: string;

  constructor(
    private route: ActivatedRoute,
    private wheelLocationsService: WheelLocationsService,
    private commonService: CommonService,
    private menuItemService: MenuItemService,
    private messageService: MessageService,
    private router: Router,
    private wheelOrderService: WheelOrderService,
    private spinner: NgxSpinnerService,
    private orderService: WheelOrderService,
    private checkOutService: CheckOutService
  ) {

  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap) => {
      this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

      this.checkOutService.getCustomerCheckoutData().subscribe(res => {
        if (res.result.table) {
          this.tableNo = res.result.table.name;
        }
      });

      let routeParams = paramMap["params"];
      this.departmentId = routeParams['departmentId'];

      if (!this.departmentId) {
        this.router.navigateByUrl(``);
        return;
      }

      this.wheelLocationsService.getWheelDepartment(this.departmentId).subscribe(res => {
        this.department = res.result;
        if (!this.department) {
          this.router.navigateByUrl(``);
        } else {
          this.isPreOrder = (!this.department.isOpeningHour && this.department.allowPreOrder) || (this.department.wheelOrderType === 1 && window.localStorage.getItem(DATE_TIME_DELIVERY)?.split(' ').shift() > moment().format('YYYY-MM-DD'));

          this.departmentMenuID = this.department.menuId;

          this.getChildren(this.departmentMenuID);

          this.getDataForOrder();

          this.commonService.$isHideFooterSubject.next(true);
          this.commonService.$isMenuSubject.next(true);

          this.orderService.prodCountCountChange.subscribe(
            newProdCount => this.cartCount = newProdCount
          );
        }
      })
    });
  }

  OnDestroy(): void {
    this.commonService.$isHideFooterSubject.next(false);
    this.commonService.$isMenuSubject.next(false);
  }

  onToggleSearchBox(): void {
    this.isDisplaySearchBox = !this.isDisplaySearchBox;
    if (!this.isDisplaySearchBox) {
      this.filterText = null;
    }
  }

  onChangeSubMenu(subCategory: string) {
    if (subCategory) {
      this.currentSubCategory = subCategory;
      this.currentPage = 0;
      this.menuItems = [];
      this.getMenuItems();
    }
  }

  getDataForOrder() {
    this.orderService.getCart().subscribe(t => {
      this.cartModel = t.result.cartItems;
      this.cartCount = t.result.cartItems.length;
    })
  }

  selectMenu(target: MenuItemData) {
    if (target.id !== this.menuActiveId) {
      this.menuActiveId = target.id;
      this.getSubCategories();
    }
  }

  openOrderPopup() {
    this.orderService.getCart().subscribe(t => {
      this.cartModel = t.result.cartItems;

      if (this.cartModel && this.cartModel.length) {
        this.isOpenOrder = true;
        this.state === 'default'
          ? (this.state = 'open')
          : (this.state = 'default');
      }
    });
  }

  closeOrderPopup() {
    this.state === 'default' ? (this.state = 'open') : (this.state = 'default');
    this.isOpenOrder = false;
  }

  openLeftMenu() {
    this.isShowMenu = true;
    this.menuState === 'hide'
      ? (this.menuState = 'show')
      : (this.menuState = 'hide');
  }

  closeLeftMenu() {
    this.menuState === 'hide'
      ? (this.menuState = 'show')
      : (this.menuState = 'hide');
    this.isShowMenu = false;
  }

  onChangeView(type): void {
    if (this.viewType === type) {
      return;
    }
    this.viewType = type;
  }

  incItems(item) {
    if (item.totalItems >= 50) {
      this.messageService.clear();
      this.messageService.add({
        severity: 'warn',
        summary: 'Service Message',
        detail: 'Limitation is 50 for each item',
      });
      return;
    }

    item.quantity += 1;
    item.subTotal = item.unitPrice * item.quantity;

    this.orderService.updateOrderItemToCart(item);
  }

  decItems(item) {
    item.quantity -= 1;
    item.subTotal = item.unitPrice * item.quantity;

    if (item.quantity < 1) {
      this.orderService.removeOrderItemFromCart(item, () => {
        _.remove(this.cartModel, t => t.key == item.key);
        this.cartCount = this.cartModel.length;

        if (!this.cartModel.length) {
          this.closeOrderPopup();
        }
      });
    }
    else {
      this.orderService.updateOrderItemToCart(item);
    }
  }

  openSide() {
    if (this.isViewDetailsSubtotal) {
      document.getElementById('subtotal').style.height = '0';
      this.isViewDetailsSubtotal = false;
    } else {
      document.getElementById('subtotal').style.height = '11rem';
      this.isViewDetailsSubtotal = true;
    }
  }

  continueToDelivery() {
    this.router.navigate(['/payment']);
  }

  private getChildren(id): void {
    this.menuItemService
      .getChildren(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: IPageResult<MenuItemData[]>) => {
        this.categories = res.result.filter((i) => i.type === 0);
        this.menuActiveId = this.categories[0].id;
        this.getSubCategories();
      });
  }

  getSubCategories(): void {
    this.spinner.show();
    this.menuItemService
      .getSubCategories(this.departmentMenuID, this.menuActiveId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: IPageResult<any>) => {
        this.numberOfColumns = res.result.numberOfColumns;
        this.subCategories = res.result.subCategories;
        this.pageSize = 20;
        this.isShowSubMenu = this.subCategories.length > 0;

        if (this.isShowSection) {
          this.subMenu.selectionClick(this.subCategories[0]);
        }
        this.spinner.hide();
      });
  }

  onScrollDown(): void {
    console.log('scrolled down!!', event);
    this.currentPage += 1;
    this.getMenuItems();
  }

  getMenuItems(): void {
    this.spinner.show();
    this.menuItemService
      .getMenuItemsData(
        this.departmentMenuID,
        this.menuActiveId,
        this.currentSubCategory,
        this.pageSize,
        this.currentPage,
        this.isPreOrder
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: IPageResult<any>) => {
        this.allMenuItemsTags = [];
        if (res && res.result) {
          this.menuItems.push(...res.result.items ?? []);

          let allMenuItemsTags = [{ label: '-- ALL --', value: '' }];

          this.menuItems
            .filter((x) => x.tags)
            .forEach((x) => {
              this.addMenuItemTags(x, allMenuItemsTags);
            });

          this.allMenuItemsTags = allMenuItemsTags;
          this.selectedTag = '';
        }
        this.spinner.hide();
      });
  }

  showMenu(): void {
    this.leftSideMenuComponent.show();
  }

  addMenuItemTags(x: MenuItemData, allMenuItemsTags: any[]) {
    x.tags.forEach((tag) => {
      if (
        allMenuItemsTags.findIndex(
          (v) => v.value.toUpperCase() == tag.toUpperCase()
        ) < 0
      ) {
        allMenuItemsTags.push({
          label: tag.toUpperCase(),
          value: tag.toUpperCase(),
        });
      }
    });
  }

  onClickBack() {

    let qrCode = JSON.parse(window.sessionStorage.getItem(SCAN_QR_CODE));

    this.wheelOrderService
      .getPageToBack(PAGE.MenuOrder, this.departmentId, qrCode ? true : false)
      .subscribe(
        (res: any) => {
          switch (res.result) {
            case PAGE.Location:
              this.router.navigateByUrl('/location');
              break;
            case PAGE.PhoneNumber:
              this.router.navigateByUrl('/phone-number');
              break;
            case PAGE.Depaterment:
              this.router.navigateByUrl('/');
              break;
            case PAGE.DateOrderFututer:
              this.router.navigateByUrl('/select-date-future');
              break;
            case PAGE.PostalCode:
              this.router.navigateByUrl('/postal-code');
              break;
          }
        },
        (error) => { }
      );
  }
}

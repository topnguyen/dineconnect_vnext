import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  DATE_TIME_DELIVERY,
  GOOGLE_MAP_KEY,
  LANGUAGE_DISPLAY,
  LOGO_URL,
  POSTAL_CODE,
  POSTAL_CODE_ONEMAP,
  SCAN_QR_CODE,
  SELECTED_LOCATION,
  TIME_SLOT_DELEVERY,
} from 'src/shared/shared.const';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  DineMessageService,
  HttpClientService,
  HttpOptions,
  TokenType,
} from 'src/shared/common';
import { Observable } from 'rxjs/internal/Observable';
import {
  CheckDeliveryZoneAndFeeInput,
  TimeSlotDto,
  WheelOrderTrackingDto,
} from 'src/shared/service-proxies/service-proxies';
import { OrderSetting, PAGE, wheelDataSetups } from 'src/shared/constant';
import { CheckOutService } from 'src/shared/service/check-out.service';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SelectItem } from 'primeng/api';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';
import { AccountService, WheelLocationsService } from 'src/shared/service';
import { UrlHelper } from 'src/shared/helpers/UrlHelper';
import { ScriptLoaderService } from 'src/shared/common/common-service/script-loader.service';

@Component({
  selector: 'app-input-postal-code',
  templateUrl: './input-postal-code.component.html',
  styleUrls: ['./input-postal-code.component.scss'],
})
export class InputPostalCodeComponent implements OnInit {
  @ViewChild('leftMenuRef', { static: false })
  leftSideMenu: LeftSideMenuComponent;
  displayPopupLocation = false;
  enableContinueBtn: boolean = false;
  logoUrl = '';
  placeHolderText = 'Search here...';

  errInvalidPostalCodeMsg =
    'Invalid value. Please enter a valid email address.';
  errPostalCodeMsg = 'Please enter postal code.';
  errDateDeliveryMsg = 'Please choose date delivery.';
  errTimeDeliveryMsg = 'Please choose time delivery.';
  errAddressMsg = 'Address is missing.';
  formGroup: FormGroup = new FormGroup({});
  postalCodeFormGroup: FormGroup = new FormGroup({});
  timeValueDefault: Date = new Date();
  dateDefaultForOnlyTime: Date = new Date();
  selectedAddress = '';
  markers: google.maps.Marker[] = [];
  googleMap: google.maps.Map;
  maxDate: Date = null;
  listAddress: Array<{ name: string; address: string; value: any }> = [];
  lastMarker: any;
  timeSlots: SelectItem[] = [];
  timeSlotsInit: any[] = [];
  timeslotSelected: string = null;
  isTimeSlots = false;
  postalServiceType: number = wheelDataSetups.wheelPostCodeServiceType;
  googleMapSrc =
    'https://maps.googleapis.com/maps/api/js?key=' +
    GOOGLE_MAP_KEY +
    '&libraries=places';
  lstLocation: any;

  languages: Array<{ code: string; icon: string; name: string }> = [
    {
      code: 'EN',
      icon: 'assets/image/en-icon.png',
      name: 'ENGLISH',
    },
  ];

  departmentId: number;
  department: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dineMessageService: DineMessageService,
    private checkOutService: CheckOutService,
    private wheelLocationsService: WheelLocationsService,
    private http: HttpClientService,
    public accountService: AccountService,
    private datePipe: DatePipe,
    private scriptLoaderService: ScriptLoaderService
  ) {
    this.formGroup = this.createForm();
    this.postalCodeFormGroup = this.createPostalForm();
  }

  get postalCode(): AbstractControl {
    return this.postalCodeFormGroup.controls.postalCode;
  }

  get dateDelivery(): AbstractControl {
    return this.formGroup.controls.dateDelivery;
  }

  get timeDelivery(): AbstractControl {
    return this.formGroup.controls.timeDelivery;
  }

  get timeSlotId(): AbstractControl {
    return this.formGroup.controls.timeSlotId;
  }

  defaultDate: Date = new Date();

  ngOnInit(): void {
    this.scriptLoaderService.loadScript(this.googleMapSrc).then(() => {});

    this.route.paramMap.subscribe((paramMap) => {
      let routeParams = paramMap['params'];
      this.departmentId = routeParams['departmentId'];

      if (!this.departmentId) {
        this.router.navigateByUrl(``);
      }

      this.wheelLocationsService
        .getWheelDepartment(this.departmentId)
        .subscribe((res) => {
          this.department = res.result;

          if (!this.department) {
            this.router.navigateByUrl(``);
          } else {
            if (this.department.wheelOrderType !== 1) {
              this.router.navigateByUrl(this.departmentId + '/location');
              return;
            }

            let today = new Date();

            if (
              (!this.department.isEnableDelivery ||
                !this.department.isOpeningHour) &&
              this.department.allowPreOrder &&
              this.department.dateDefaultOrderFututer != null
            ) {
              this.timeValueDefault = moment(
                this.department.dateDefaultOrderFututer
              ).toDate();

              this.defaultDate = moment(
                this.department.dateDefaultOrderFututer
              ).toDate();
            }
            if (
              this.department.futureOrderDaysLimit != null &&
              this.department.futureOrderDaysLimit >= 0
            ) {
              let result = new Date(today);
              result.setDate(
                today.getDate() + this.department.futureOrderDaysLimit
              );
              this.maxDate = result;
            }

            this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

            this.isTimeSlots = this.department.isTimeSlotMode;

            this.timeSlotsInit = this.department.timeSlots ?? [];

            this.timeSlots = this.mapTimeSlot(
              this.timeValueDefault,
              this.timeValueDefault
            );

            this.lstLocation = this.department.wheelLocationOutputs || [];
          }
        });
    });
  }

  onClickBack(): void {
    this.router.navigateByUrl('/');
  }

  searchPostalCode(): void {
    this.postalCode.markAsTouched();
    if (this.postalCode.value.length === 0) {
      this.getUserLocation();
      return;
    }

    this.getAddressByPostCode(this.postalCode.value).subscribe(
      (res: any) => {
        if (res.result.length === 0) {
          this.dineMessageService.showError('Postalcode is not found');
        } else {
          if (res.result && res.result.length) {
            const address = res.result[0];
            if (address) {
              this.selectedAddress = address.address;
              this.checkOutService
                .saveCustomerSessionData(POSTAL_CODE, address.postal)
                .subscribe();
              this.checkOutService
                .saveCustomerSessionData(
                  POSTAL_CODE_ONEMAP,
                  JSON.stringify(address)
                )
                .subscribe();
              this.initMap(address);
            }
          }
        }
      },
      (error) => {
        this.dineMessageService.showError('Postalcode is not found');
      }
    );
  }

  private getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const longitude = position.coords.longitude;
        const latitude = position.coords.latitude;
        this.codeLatLng(latitude, longitude);

        this.initMap({ latitude: latitude, longitude: longitude });
      });
    } else {
      console.log('User not allow');
    }
  }

  private codeLatLng(lat, lng) {
    let self = this;
    let geocoder = new google.maps.Geocoder();

    let latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ location: latlng }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results.length) {
          //formatted address
          const addressValue = results[0];

          let oneMapadd = {
            address: addressValue.formatted_address,
            latitude: lat,
            longitude: lng,
            longtitude: lng,
            searchval: addressValue.formatted_address,
          };
          if (addressValue) {
            this.checkOutService
              .saveCustomerSessionData(
                POSTAL_CODE_ONEMAP,
                JSON.stringify(oneMapadd)
              )
              .subscribe();

            let postCode =
              addressValue.address_components.find(
                (x) => x.types.indexOf('postal_code') >= 0
              )?.short_name ||
              addressValue.address_components[
                addressValue.address_components.length - 1
              ].short_name;

            this.checkOutService
              .saveCustomerSessionData(POSTAL_CODE, postCode)
              .subscribe();

            this.postalCode.setValue(postCode);

            this.selectedAddress = addressValue.formatted_address;
          }
        }
      }
    });
  }

  onSubmit(): void {
    if (!this.selectedAddress) {
      this.dineMessageService.showError('Invalid input');
      return;
    }

    if (this.isTimeSlots && !this.timeslotSelected) {
      this.dineMessageService.showError('Invalid Time Slot');
      return;
    }

    let preUrl = UrlHelper.initialUrl;

    if (preUrl.includes('verify')) {
      this.checkOutService
        .getCustomerSessionData(SELECTED_LOCATION)
        .subscribe((res) => {
          let location = JSON.parse(res.result);
          this.router.navigateByUrl(
            `${this.department.id}/location/${location.id}`
          );
        });
    } else {
      let locations = this.department.wheelLocationOutputs || [];

      if (locations.length === 1) {
        this.checkOutService
          .isLocationInDeliveryZone(locations[0].id)
          .subscribe((result) => {
            let checkDeliveryResult = result.result;
            if (
              !checkDeliveryResult.allowOutOfDeliveryZones &&
              checkDeliveryResult.outOfAllDeliveryZones
            ) {
              this.dineMessageService.showError(
                `Your address is out of delivery zones. Please select other location`
              );
            } else {
              this.checkSlotOrder(this.department);
            }
          });
      } else if (locations.length > 1) {
        this.checkSlotOrder(this.department);
      }
    }
  }

  checkSlotOrder(department) {
    if (this.isTimeSlots) {
      this.timeSlotId.markAsDirty();
    } else {
      this.timeDelivery.markAsDirty();
    }

    if (this.formGroup.invalid) {
      return;
    }

    let timeSlot = department.timeSlots.find(
      (x) => x.id.toString() == this.timeslotSelected
    );

    if (this.isTimeSlots) {
      let input: any = {};
      input.wheelDepartmentTimeSlotId = +this.timeslotSelected;
      input.orderDate = moment(this.dateDelivery.value).format('YYYY-MM-DD');

      this.checkOutService.checkSlotOrder(input).subscribe((res) => {
        if (res.result) {
          this.submitOrder(timeSlot);
        } else {
          this.dineMessageService.showError(
            `Really Sorry !!! Maximum Orders are reached in the Selected Time Slot. Please select other time slot from the List`,
            5000
          );
          return;
        }
      });
    } else {
      this.submitOrder(timeSlot);
    }
  }

  submitOrder(timeSlot: TimeSlotDto) {
    let dateDalivery = moment(this.dateDelivery.value).format('YYYY-MM-DD');
    let dateTimeDelivery = '';

    if (timeSlot) {
      this.checkOutService
        .saveCustomerSessionData(TIME_SLOT_DELEVERY, JSON.stringify(timeSlot))
        .subscribe();
      dateTimeDelivery =
        dateDalivery + ' ' + timeSlot.timeFrom + '-' + timeSlot.timeTo;
    } else {
      this.checkOutService
        .saveCustomerSessionData(TIME_SLOT_DELEVERY, null)
        .subscribe();
      let timeDelivery = this.timeDelivery.value
        ? moment(this.timeDelivery.value).format('HH:mm')
        : 'ASAP';
      dateTimeDelivery =
        timeDelivery == 'ASAP' ? 'ASAP' : dateDalivery + ' ' + timeDelivery;
    }
    this.checkOutService
      .saveCustomerSessionData(DATE_TIME_DELIVERY, dateTimeDelivery)
      .subscribe();

    let locations = this.department.wheelLocationOutputs || [];

    if (locations.length === 1) {
      this.checkOutService
        .saveCustomerSessionData(
          SELECTED_LOCATION,
          JSON.stringify(locations[0])
        )
        .subscribe();

      if (OrderSetting.askTheMobileNumberFirst) {
        this.router.navigateByUrl('/phone-number');
      } else {
        const scanData = JSON.parse(
          window.sessionStorage.getItem(SCAN_QR_CODE)
        );

        if (scanData) {
          if (this.department.isRequireOtp) {
            this.router.navigateByUrl(
              `/verify/${scanData.locationID}/${scanData.departmentId}/${scanData.tableID}`
            );
          } else {
            this.onChooseLanguage(locations[0]);
          }
        } else {
          this.onChooseLanguage(locations[0]);
        }
      }
    } else {
      this.router.navigateByUrl(this.departmentId + '/location');
    }
  }

  onChooseLanguage(location): void {
    this.checkOutService
      .saveCustomerSessionData(LANGUAGE_DISPLAY, this.languages[0].code)
      .subscribe();

    if (
      this.department.wheelOrderType !== 1 &&
      this.department.allowPreOrder &&
      !this.department.isOpeningHour &&
      this.department.dateDefaultOrderFututer != null
    ) {
      this.router.navigateByUrl(`/select-date-future`);
    } else {
      this.router.navigateByUrl(
        `${this.department.id}/location/${location.id}`
      );
    }
  }

  searchPlace(): void {
    if (this.postalCode.value.toString().trim().length < 1) {
      return;
    }
    let service: google.maps.places.PlacesService;
    const uluru = { lat: 1.354916, lng: 103.867728 };

    this.googleMap = new google.maps.Map(
      document.getElementById('googleMap') as HTMLElement,
      {
        center: uluru,
        zoom: 4,
      }
    );
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.googleMap,
    });
    this.markers.push(marker);
    const request = {
      query: this.postalCode.value,
      fields: ['name', 'geometry'],
    };

    service = new google.maps.places.PlacesService(this.googleMap);
    service.textSearch(
      request,
      (
        results: google.maps.places.PlaceResult[],
        status: google.maps.places.PlacesServiceStatus
      ) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          this.listAddress = [];
          if (results.length > 9) {
            results = results.slice(0, 10);
          }
          results.forEach((item) => {
            this.listAddress.push({
              name: item.name,
              address: item.formatted_address,
              value: item,
            });
          });
        }
      }
    );
  }

  onChooseDeliveryPlace(event) {
    this.addressToGeoCode(event.value);
  }

  private addressToGeoCode(address) {
    this.checkOutService.AddressToGeoCode(address.formatted_address, (res) => {
      if (res && res.length) {
        const addressValue = res[0];

        if (addressValue) {
          let postCode = addressValue.address_components.find(
            (x) => x.types.indexOf('postal_code') >= 0
          )?.short_name;
          var latitude = address.geometry.location.lat();
          var longitude = address.geometry.location.lng();

          let postalCodeOneMap = JSON.stringify({
            address: addressValue.formatted_address,
            postal: postCode,
            latitude: latitude,
            longitude: longitude,
          });

          this.checkOutService
          .saveCustomerSessionData(
            POSTAL_CODE_ONEMAP,
            postalCodeOneMap
          )
          .subscribe();
          this.checkOutService
            .saveCustomerSessionData(POSTAL_CODE, postCode)
            .subscribe();

          this.selectedAddress = addressValue.formatted_address;

          this.createMarker(
            addressValue.geometry as google.maps.places.PlaceGeometry,
            addressValue.formatted_address
          );
        }
      } else {
        this.dineMessageService.showError(
          `Somethings went wrong with the System. Please contact the team`,
          5000
        );
      }
    });
  }

  private getComponentByTypes(result: any, type: string): any {
    return result.address_components.find((data) => data.types.includes(type));
  }

  private clearMarkers() {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
  }

  private createMarker(
    geometry: google.maps.places.PlaceGeometry,
    name: string
  ): void {
    this.clearMarkers();
    this.googleMap = new google.maps.Map(
      document.getElementById('googleMap') as HTMLElement,
      {
        center: geometry.location,
        zoom: 15,
      }
    );
    const marker = new google.maps.Marker({
      position: geometry.location,
      map: this.googleMap,
    });

    this.markers.push(marker);
    this.lastMarker = geometry.location;
  }

  private numberic(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const controlValue = control.value;
      if (typeof controlValue !== 'number' || isNaN(controlValue)) {
        return {
          numberic: { value: controlValue },
        };
      }
      return null;
    };
  }

  private initMap(data): void {
    // The location of Uluru
    const uluru = {
      lat: parseFloat(data ? data.latitude : 1.354916),
      lng: parseFloat(data ? data.longitude : 103.867728),
    };
    // The map, centered at Uluru
    this.googleMap = new google.maps.Map(
      document.getElementById('googleMap') as HTMLElement,
      {
        zoom: 15,
        center: uluru,
      }
    );

    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.googleMap,
    });

    this.initLocationsInMap();
    this.markers.push(marker);
  }

  initLocationsInMap() {
    this.lstLocation.forEach((location) => {
      if (
        location.pickupAddressLat != null &&
        location.pickupAddressLong != null
      ) {
        let item = {
          lat: parseFloat(location.pickupAddressLat),
          lng: parseFloat(location.pickupAddressLong),
        };

        let marker = new google.maps.Marker({
          position: item,
          map: this.googleMap,
          title: location.name,
          label: {
            color: 'black',
            text: location.name,
          },
        });
        this.markers.push(marker);
      }
    });
  }

  private getAddressByPostCode(postcode: string): Observable<any> {
    let url_ = '/api/services/app/WheelLocations/GetAddressByPostCode?';
    if (postcode !== undefined && postcode !== null)
      url_ += 'postcode=' + encodeURIComponent('' + postcode) + '&';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get(options);
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      dateDelivery: [this.timeValueDefault, [Validators.required]],
      timeDelivery: [null],
      timeSlotId: [0, this.isTimeSlots ? [Validators.required] : []],
    });
  }
  private createPostalForm(): FormGroup {
    return this.formBuilder.group({
      postalCode: ['', [Validators.required]],
    });
  }

  formatDate(date: Date, time: string) {
    var datetimeStr = this.datePipe.transform(date, 'yyyy-MM-dd') + ' ' + time;
    return new Date(datetimeStr);
  }

  onClick(disabled: boolean) {
    if (disabled) {
      event.stopPropagation();
    }
  }

  private getTimeSlots(): Observable<any> {
    let url_ = '/api/services/app/WheelDepartments/GetTimeSlots?';

    let orderDate = moment(this.dateDelivery.value).format('YYYY/MM/DD');
    url_ += 'departmentId=' + encodeURIComponent('' + this.departmentId) + '&';
    url_ += 'date=' + encodeURIComponent('' + orderDate) + '&';

    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get(options);
  }

  changeDate(event) {
    if (this.isTimeSlots) {
      this.getTimeSlots().subscribe(
        (res: any) => {
          if (res.result.length === 0) {
            this.dineMessageService.showError(
              'No Time Slot available for selected date.'
            );
          }

          this.timeSlotsInit = res.result ?? [];

          this.timeSlots = this.mapTimeSlot(
            this.timeValueDefault,
            this.timeValueDefault
          );

          this.updateTimeSlots();
        },
        (error) => {
          this.dineMessageService.showError(
            'Cannot get Time Slots for selected date'
          );
        }
      );
    }
  }

  updateTimeSlots() {
    this.timeSlots = this.mapTimeSlot(
      this.dateDelivery.value,
      this.timeValueDefault
    );

    if (this.isTimeSlots) return;
    this.dateDefaultForOnlyTime = new Date();

    this.formGroup.controls['timeDelivery'].setValue(null);
    if (
      this.dateDelivery.value.getDate() > this.dateDefaultForOnlyTime.getDate()
    ) {
      this.dateDefaultForOnlyTime.setDate(
        this.dateDefaultForOnlyTime.getDate() - 1
      );
      this.formGroup.controls['timeDelivery'].setValidators(
        Validators.required
      );
      this.formGroup.controls['timeDelivery'].updateValueAndValidity();
    } else {
      this.formGroup.controls['timeDelivery'].clearValidators();
      this.formGroup.controls['timeDelivery'].updateValueAndValidity();
    }
  }

  mapTimeSlot(datechange, datedefault) {
    let timeSlotMap: SelectItem[];
    timeSlotMap = this.timeSlotsInit.map((t) => {
      let defaultDateHasTime = this.datePipe.transform(
        datedefault,
        'yyyy-MM-dd HH:mm'
      );
      let fromDateHasTime =
        this.datePipe.transform(datechange, 'yyyy-MM-dd') + ' ' + t.timeTo;
      let currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      let dateFrom = this.datePipe.transform(datechange, 'yyyy-MM-dd');

      if (currentDate < dateFrom || fromDateHasTime > defaultDateHasTime) {
        return {
          label: t.timeFrom + ' - ' + t.timeTo,
          value: t.id.toString(),
          disabled: false,
        };
      } else {
        return {
          label: t.timeFrom + ' - ' + t.timeTo,
          value: t.id.toString(),
          disabled: true,
        };
      }
    });
    return timeSlotMap;
  }
}

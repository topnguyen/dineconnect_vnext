import { MenuItemService } from './../../../shared/service/menu-item.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AccountService } from 'src/shared/service/account.service';
import { Router } from '@angular/router';
import { LocalStorageService, CommonService } from 'src/shared/common';
import { CONSTANT, wheelDataSetups } from 'src/shared/constant';
import { NavigationService } from 'src/shared/navigation';
import { ToastService } from 'src/shared/toast';
import { LeftMenu } from 'src/model/left-menu.model';

import { AppConsts } from 'src/shared/constant';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { TokenService } from 'abp-ng2-module';

@Component({
  selector: 'header-ui',
  templateUrl: './header-ui.component.html',
  styleUrls: ['./header-ui.component.scss'],
})
export class HeaderUiComponent implements OnInit {
  phoneNumber = environment.phoneNumber;
  isOpenAccount = true;
  logoCompanyUrl = wheelDataSetups.logoCompanyUrl;
  leftMenuData = [];
  isWheelSetupsAvailable: boolean;
  isWelComePage: boolean;
  selectItemID: number;
  stepName: string;
  orders: number = 0;
  itemActiveId: number;
  InputSectionChildren: LeftMenu;
  isOpenMenu = false;
  isMenu = false;
  constructor(
    private accountService: AccountService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private commonService: CommonService,
    private navigationService: NavigationService,
    private toastService: ToastService,
    private menuItemService: MenuItemService,
    private cdRef: ChangeDetectorRef,
    private checkOutService: CheckOutService,
    private _tokenService: TokenService
  ) { }

  ngOnInit(): void {
    this.isWheelSetupsAvailable = this.commonService.isWheelSetupsAvailable;
    this.commonService.$isWelComePage.subscribe(isWelComePage => {
      this.isWelComePage = isWelComePage;
    });
    this.localStorageService.$orderListLocalStorage.subscribe(() => {
      const listOrders = this.menuItemService.orderList.length ? this.menuItemService.orderList :
        this.localStorageService.getOrderItemsOnLocalStorage();
      this.orders = listOrders.reduce((arr, prev) => {
        return arr += prev.total;
      }, 0);
    });

    this.menuItemService.$leftMenuDataSubject.subscribe(items => {
      this.leftMenuData = items;
    });

    this.menuItemService.$closeLeftMenuSubject.subscribe(isClose => {
      if (isClose) {
        this.closeNav();
      }
    });

    this.commonService.$isMenuSubject.subscribe(data => {
      this.isMenu = data;
      this.cdRef.detectChanges();
    });

  }

  ngAfterViewChecked(): void {
    this.commonService.$currentStepName.subscribe(currentStepName => {
      this.stepName = currentStepName;
      this.cdRef.detectChanges();
    });
  }

  openAccountComponent() {
    // this.accountService.openAccountComponent(this.isOpenAccount);
    this.navigationService.navigateToLogin();
  }

  public get user() {
    return this.accountService.userValue;
  }

  logOut() {
    this.checkOutService.resetCheckout().subscribe(() => {
      // remove user from local storage and set current user to null
      this.accountService.logOut().subscribe(() => {

        abp.auth.clearToken();
        abp.auth.clearRefreshToken();
        abp.utils.setCookieValue(AppConsts.authorization.encrptedAuthTokenName, undefined, undefined, abp.appPath);
        this.accountService.callBackLogOutEvent();
        this.toastService.clear();
        this.toastService.showSuccess('Log out Successful');
        location.href = '/';
      });
    });
  }

  goToLocation() {
    this.navigationService.navigateToLocation();
  }

  goToPreviousRoute() {
    this.navigationService.navigateToPrevious();
  }

  clickMenuItem(item: LeftMenu): void {
    if (!this.selectItemID) {
      this.selectItemID = item.id;
      this.InputSectionChildren = item;
    } else {
      this.selectItemID = null;
    }
    if (!item.sectionChildren) {
      this.closeNav();
    }
    this.menuItemService.$commonMenuIDSubject.next(item);
    this.itemActiveId = item.id;
  }

  openNav() {
    document.getElementById('LeftMenuSideNav').style.width = '70%';
    this.isOpenMenu = true;
  }

  closeNav() {
    document.getElementById('LeftMenuSideNav').style.width = '0';
    this.isOpenMenu = false;
  }
}

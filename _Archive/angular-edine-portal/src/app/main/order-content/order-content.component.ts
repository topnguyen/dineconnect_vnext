import { wheelDataSetups } from './../../../shared/constant/constant';
import { FormControl } from '@angular/forms';
import { ISimpleLocation } from '../../../model/simple-location.model';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MenuItemService, LocationService } from 'src/shared/service';
import { CommonService, LocalStorageService } from 'src/shared/common';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import { SELECTED_FEE } from 'src/shared/shared.const';

@Component({
  selector: 'app-order-content',
  templateUrl: './order-content.component.html',
  styleUrls: ['./order-content.component.scss'],
})
export class OrderContentComponent implements OnInit, OnDestroy {
  subTotalPrice: number = 0;
  totalPrice: number = 0;
  currencyPipe: '';
  orderList: CustomMenuItemData[] = [];
  isFullViewCart = false;
  deliveryLocation: ISimpleLocation;
  orderItem: any;
  noteFormControl: FormControl;
  systemCurrency = wheelDataSetups.currency;
  isViewDetailsSubtotal = true;
  wheelServiceFees = [];
  @Input() isOrderState: boolean = true;

  constructor(
    private menuItemService: MenuItemService,
    private localStorageService: LocalStorageService,
    private locationService: LocationService,
    private router: Router,
    private commonService: CommonService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.createForm();
    this.wheelServiceFees = JSON.parse(window.localStorage.getItem(SELECTED_FEE));

    this.localStorageService.$orderListLocalStorage.subscribe(() =>
      this.loadItemsOrder()
    );
    this.localStorageService.$deliveryLocation.subscribe((value) => {
      if (value) {
        this.deliveryLocation = value;
        if (this.orderItem) {
          this.order(this.orderItem);
          this.orderItem = null;
        }
      }
    });

  }
  ngOnDestroy(): void {
    this.commonService.$isHideHeaderSubject.next(false);
  }

  createForm() {
    this.noteFormControl = new FormControl();
  }

  getNoteValue() {
    return this.noteFormControl.value;
  }

  getTotalPrice() {
    const totalPrice = this.countSubTotal();
    return totalPrice;
  }

  order(item) {
    
  }

  incItems(item) {
    if (item.total >= 50) {
      this.messageService.clear();
      this.messageService.add({
        severity: 'warn',
        summary: 'Service Message',
        detail: 'Limitation is 50 for each item',
      });
      return;
    }
    //this.menuItemService.incItems(item);
  }

  decItems(item) {
    //this.menuItemService.decItems(item);
    if (!this.orderList.length) {
      this.fullViewCart(false);
    }
  }

  loadItemsOrder() {
    this.menuItemService.orderList = this.localStorageService.getOrderItemsOnLocalStorage();
    this.orderList = [...this.menuItemService.orderList];
    // this.loadTagItem();
    this.subTotalPrice = this.countSubTotal();
    this.totalPrice = this.countTotal();
  }

  loadTagItem(): void {
    this.orderList.forEach((items) => {
      items.orderTags.forEach((orderTagItem) => {
        orderTagItem.isShowOrderTags = orderTagItem.tags.some((tags) => tags.checked === true);
      });
    });
  }

  fullViewCart(isFullView = true) {
    if (isFullView) {
      this.commonService.$isHideHeaderSubject.next(true);
    } else {
      this.commonService.$isHideHeaderSubject.next(false);
    }
    let body = document.getElementsByTagName('body')[0] as any;
    if (isFullView) {
      body.className = 'order-active';
    } else {
      body.className = '';
    }
    this.isFullViewCart = isFullView;
  }

  countSubTotal() {
    let totalPrice = 0;
    this.orderList.map((item) => {
      totalPrice += item.totalPricePerItem;
    });

    return totalPrice;
  }

  countTotal() {
    let totalPrice = this.subTotalPrice;

    this.wheelServiceFees.map((item) => {
      totalPrice += item.feeType ? item.value : this.subTotalPrice * (item.value / 100);
    });
    return totalPrice;
  }

  openLocation() {
    this.locationService.openLocationPopUp();
  }

  get totalAmountsItem() {
    let totalAmounts = this.getTotalAmountsItem();
    return totalAmounts;
  }

  getTotalAmountsItem() {
    let totalAmounts = 0;
    this.orderList.map((x) => {
      totalAmounts += x.total;
    });
    return totalAmounts;
  }

  continueToDelivery() {
    this.router.navigate(['/checkout']);
  }

  openSide() {
    if (this.isViewDetailsSubtotal) {
      document.getElementById('subtotal').style.height = '0';
      this.isViewDetailsSubtotal = false;
    } else {
      document.getElementById('subtotal').style.height = '11rem';
      this.isViewDetailsSubtotal = true;
    }
  }
}

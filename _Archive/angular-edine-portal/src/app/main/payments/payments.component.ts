import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { DatePipe, Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { MenuItemData, Tag, WheelMenuComboItems } from 'src/model';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import {
  DineMessageService,
  HttpClientService,
  HttpOptions,
  LocalStorageService,
  TokenType,
} from 'src/shared/common';
import { AppConsts, CONSTANT, OrderSetting, wheelDataSetups } from 'src/shared/constant';
import {
  AccountService,
  MenuItemService,
  WheelSetupsService,
} from 'src/shared/service';
import {
  CheckDeliveryZoneAndFeeInput,
  ICalculatePromotionDiscountPaymentInputDto,
  PromotionDiscountPaymentDto,
  WheelCartItemDto,
  WheelOrderTrackingDto,
} from 'src/shared/service-proxies/service-proxies';
import { CheckOutService } from 'src/shared/service/check-out.service';
import {
  GOOGLE_MAP_KEY,
  PaymentMethods,
  POSTAL_CODE,
  POSTAL_CODE_ONEMAP,
  PaymentMethodsSytemsNames,
} from 'src/shared/shared.const';
import { OrderContentComponent } from '../order-content/order-content.component';
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import { catchError, switchMap } from 'rxjs/operators';
import { ScriptLoaderService } from 'src/shared/common/common-service/script-loader.service';
import { HttpParams } from '@angular/common/http';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { AppSession } from 'src/shared/constant';

declare var OmiseCard: any;

@Component({
  selector: 'payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
  animations: [
    trigger('popupState', [
      state('default', style({})),
      state(
        'open',
        style({
          bottom: 0,
        })
      ),
      transition('default <=> open', animate(500)),
    ]),
    trigger('comboItemState', [
      state('hide', style({})),
      state(
        'show',
        style({
          bottom: 0,
        })
      ),
      transition('hide <=> show', animate(500)),
    ]),
  ],
})
export class PaymentsComponent implements OnInit {
  orderContentComponent: OrderContentComponent;

  headerLbl: string = 'Order Summary';
  orderLbl: string = 'Order Details';
  recommentLbl: string = 'Recommended Items';
  deliveryLbl: string = 'Delivery Information';
  paymentLbl: string = 'Payment Selection';

  paymentMethodSelected: string = 'cod';

  omiseLabel: string = '';
  omisePaymentGatewayPublicKey: string = '';
  omiseConfig = '';

  stripePublicKey: string = '';

  deliveryFee: number = 0;
  totalPrice: number = 0;

  serviceFee: number = 0;
  subTotalPrice: number = 0;
  subTotalPriceWithoutDiscount: number = 0;

  checkoutSuccessful: boolean = false;
  isDeliveryType: boolean = false;
  isPickupPlateNumber: boolean = false;

  selectedLocation: any = null;
  selectedDepartment: any = null;

  isASAP: boolean = true;
  isSubmitted: boolean = false;

  timeValueDefault: Date = new Date();

  dateOrder: Date = new Date();
  timeOrder: Date = new Date();

  timeDelivery = 'Order Now';
  address: string = '';
  timeSlot: any;

  fromDate: Date = new Date();
  toDate: Date = new Date();

  isTimeSlot = false;

  orderDateTime: Date = null;

  lastMarker: any;
  wheelServiceFees = [];

  orderedList: any[] = [];
  orderList: any[] = [];
  orderTotal: any;

  listAddress: Array<{ name: string; address: string; value: any }> = [];
  markers: google.maps.Marker[] = [];
  googleMap: google.maps.Map;
  googleMapSrc =
    'https://maps.googleapis.com/maps/api/js?key=' +
    GOOGLE_MAP_KEY +
    '&libraries=places';

  promotionDiscountPayment: PromotionDiscountPaymentDto;

  items = [];
  selectedItemCalendar: { label: string; icon: string } = {
    label: 'Order Now',
    icon: 'pi pi-clock',
  };
  formGroup: FormGroup = null;
  errInvalidEmailMsg = 'Invalid value. Please enter a valid email address.';
  errUsernameMsg = 'Name is missing.';
  plateNumberUsernameMsg = 'Plate Number is missing.';
  errEmailMsg = 'Email is missing.';
  errAddress1Msg = 'Address1 is missing.';
  errAddressMsg = 'Address is missing.';
  blockunitMsg = 'Block/Floor/Unit is missing.';
  errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
  errPhoneMsg = 'Phone number is missing.';

  paymentMethodRequired = 'Payment method is required.';

  selectItem;
  isOpenAddon: boolean = false;
  state = 'default';
  priceItem: number = 0;
  wheelMenuComboItems: WheelMenuComboItems;
  totalItemsAdd = 1;
  selectedPortion: any = null;

  systemCurrency = wheelDataSetups.currency;

  recommendItems: any[];
  oneMapValue: any;
  postCode: string;
  infoMinimumOrder: string = null;

  checkDeliveryResult: any = null;
  deliveryExpectdTime: any;
  taxAmount = 0;
  dateTimeDelivery: string;
  askTheMobileNumberFirst: boolean = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  lstLocation: any;

  tableId: any;
  tableName: any;
  postalServiceType: number = wheelDataSetups.wheelPostCodeServiceType;
  constructor(
    private http: HttpClientService,
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location,
    private checkOutService: CheckOutService,
    private localStorageService: LocalStorageService,
    private readonly accountService: AccountService,
    private menuItemService: MenuItemService,
    private dineMessageService: DineMessageService,
    private datePipe: DatePipe,
    private wheelSetupsService: WheelSetupsService,
    private orderService: WheelOrderService
  ) {

  }

  ngOnInit(): void {
    this.askTheMobileNumberFirst = OrderSetting.askTheMobileNumberFirst;

    this.checkOutService.getCustomerCheckoutData().subscribe(res => {

      if (res.result && res.result.table) {
        this.tableId = res.result.table.id;
        this.tableName = res.result.table.name;
      }

      if (this.tableName) {
        this.headerLbl = 'Table No ' + this.tableName + " - " + this.headerLbl;
      }

      this.selectedDepartment = res.result.department;
      this.lstLocation = this.selectedDepartment.wheelLocationOutputs || [];
      this.isDeliveryType = this.selectedDepartment.wheelOrderType === 1;
      this.isPickupPlateNumber = this.selectedDepartment.wheelOrderType === 3;
      this.selectedLocation = res.result.location;
      this.oneMapValue = res.result.postalCodeOneMap ?? null;
      this.isTimeSlot = this.isDeliveryType && this.selectedDepartment.timeSlots?.length > 0;
      this.postCode = res.result.postalCode;
      this.address = this.oneMapValue?.address.split('SINGAPORE')[0];

      this.formGroup = this.createAddressForm();

      const omise = this.selectedDepartment.wheelPaymentMethods.find(
        (method) => method.systemName === PaymentMethodsSytemsNames.Omise
      );

      if (omise) {
        this.omisePaymentGatewayPublicKey = omise.dynamicFieldData?.publishKey;
        this.omiseConfig = omise.dynamicFieldData;
      }

      const stripe = this.selectedDepartment.wheelPaymentMethods.find(
        (method) => method.systemName === PaymentMethodsSytemsNames.Stripe
      );

      if (stripe) {
        this.stripePublicKey = stripe.dynamicFieldData?.publishKey;
      }

      if (
        this.isDeliveryType ||
        (this.selectedDepartment.allowPreOrder &&
          this.selectedDepartment.dateDefaultOrderFututer != null)
      ) {
        this.dateTimeDelivery = res.result.dateTimeDelivery;
      } else {
        this.dateTimeDelivery = '';
      }

      this.wheelServiceFees = this.selectedDepartment.wheelServiceFees || [];

      this.timeSlot = res.result.timeSlotDelivery;


      if (this.isDeliveryType) {
        this.loadItemsOrder();
        this.wheelSetupsService
          .calculateDeliveryTime(
            this.selectedLocation.id,
            this.oneMapValue.longitude,
            this.oneMapValue.latitude
          )
          .subscribe((res) => {
            if ((res.result as any) == 0) {
              this.deliveryExpectdTime = 'Not Available';
            } else {
              this.deliveryExpectdTime = res.result + ' mins';
            }
          });

          this.searchPostalCode()
      }
      else {
        this.loadItemsOrder();
      }

      if (!OrderSetting.askTheMobileNumberFirst) {
        const phoneNumer = AppSession.user.phoneNumber?.toString()?.replace(AppSession.user.phonePrefix, "");
        this.formGroup.addControl(
          'phoneNumber',
          new FormControl(phoneNumer, Validators.required)
        );
      }

      if (this.stripePublicKey) {
        this.initStripePaymentGateway();
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.isDeliveryType) {
      new ScriptLoaderService().load(this.googleMapSrc).then(() => {
        this.searchPlace();
      });
    }
  }

  onClickBack(): void {
    this.location.back();
  }

  searchPlace(): void {
    if (!this.address) {
      return;
    }

    const uluru = { lat: 1.354916, lng: 103.867728 };

    this.googleMap = new google.maps.Map(
      document.getElementById('googleMap') as HTMLElement,
      {
        center: uluru,
        zoom: 4,
      }
    );
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.googleMap,
    });
    this.markers.push(marker);
    const request = {
      query: this.postCode,
      fields: ['name', 'geometry'],
    };

    this.checkOutService.AddressToGeoCode(this.address, (res) => {
      if (res && res.length) {
        const addressValue = res[0];
        if (addressValue) {
          this.createMarker(
            addressValue.geometry as google.maps.places.PlaceGeometry,
            addressValue.formatted_address
          );
        }
      } else {
        this.dineMessageService.showError(`Somethings went wrong`, 5000);
      }
    });
  }

  onSubmit(): void {
    this.isSubmitted = true;
    this.formGroup.markAsDirty();
    this.username.markAsDirty();
    this.email.markAsDirty();
    this.paymentType.markAsDirty();

    if (this.blockunit) {
      this.blockunit.markAsDirty();
    }

    if (!OrderSetting.askTheMobileNumberFirst) {
      this.phoneNumber.markAllAsTouched();
      this.phoneNumber.updateValueAndValidity();

      this.accountService.savePhoneNumber(
        this.phoneNumber.value?.e164Number,
        this.phoneNumber.value?.dialCode
      );
    }

    this.checkMinimumOder((result) => {
      debugger;
      if (result.isValid) {
        if (this.formGroup.valid) {
          this.initPayment();
        }
      } else {
        var message = this.getMessage(result.minimuOrderTotal?.toString());
        this.dineMessageService.showError(message);
      }
    });
  }

  get username() {
    return this.formGroup.controls['username'];
  }
  get plateNumber() {
    return this.formGroup.controls['plateNumber'];
  }
  get phoneNumber() {
    return this.formGroup.controls['phoneNumber'];
  }
  get email() {
    return this.formGroup.controls['email'];
  }

  get address1() {
    return this.formGroup.controls['address1'];
  }

  get addressFormControl() {
    return this.formGroup.controls;
  }

  get deliveryLocationControl() {
    return this.formGroup.controls['deliveryLocation'];
  }

  get deliveryAddressControl() {
    return this.formGroup.controls['deliveryAddress'];
  }

  get paymentType() {
    return this.formGroup.controls['paymentType'];
  }

  get note() {
    return this.formGroup.controls['note'];
  }

  get coupon() {
    return this.formGroup.controls['coupon'];
  }

  get blockunit() {
    return this.formGroup.controls['blockunit'];
  }

  get address2() {
    return this.formGroup.controls['address2'];
  }

  private loadItemsOrder() {
    this.orderService.getCart(true).subscribe(t => {
      this.orderList = t.result.cartItems;
      this.orderTotal = t.result.cartTotal;
      this.orderedList = t.result.orderedItems;

      this.subTotalPriceWithoutDiscount = this.orderTotal.subTotal;
      this.subTotalPrice = this.orderTotal.subTotal;

      this.updateTotal();

      this.checkMinimumOder((result) => {
        if (!result.isValid) {
          this.infoMinimumOrder = this.getMessage(
            result.minimuOrderTotal?.toString()
          );
        }
      });
    })
  }

  calculateDeliveryFee(callBack?) {
    this.checkOutService.isLocationInDeliveryZone(this.selectedLocation.id).subscribe((result) => {
      this.checkDeliveryResult = result.result;
      this.deliveryFee = this.checkDeliveryResult.deliveryFee;
      callBack();
    });
  }

  private initSubTotal() {
    this.subTotalPriceWithoutDiscount = this.orderTotal.subTotal;
    this.subTotalPrice = this.orderTotal.subTotal;
  }

  private countTotal() {
    this.totalPrice += this.subTotalPrice + this.orderTotal.serviceFee + this.deliveryFee;
  }

  private createAddressForm(model = null) {
    if (this.isDeliveryType) {
      return this.formBuilder.group({
        coupon: [''],
        note: [''],
        username: [
          this.accountService.userValue?.userName,
          Validators.required,
        ],
        email: [
          this.accountService.userValue?.emailAddress,
          [
            Validators.required,
            Validators.pattern(
              '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$'
            ),
          ],
        ],
        address1: [this.address ?? '', [Validators.required]],
        address2: [''],
        blockunit: ['', Validators.required],
        deliveryLocation: '',
        deliveryAddress: '',
        paymentType: ['', Validators.required],
        postalCode: [this.postCode, [Validators.required]],
        country: [this.selectedLocation.country, [Validators.nullValidator]],
        city: [this.selectedLocation.city, [Validators.nullValidator]],
      });
    } else if (this.isPickupPlateNumber) {
      return this.formBuilder.group({
        note: [''],
        coupon: [''],
        username: ['', Validators.required],
        plateNumber: ['', Validators.required],
        email: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$'
            ),
          ],
        ],
        paymentType: [PaymentMethods.Cash, Validators.required],
      });
    } else {
      return this.formBuilder.group({
        note: [''],
        coupon: [''],
        username: ['', Validators.required],
        email: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$'
            ),
          ],
        ],
        paymentType: [PaymentMethods.Cash, Validators.required],
      });
    }
  }

  private clearMarkers() {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
  }

  private createMarker(
    geometry: google.maps.places.PlaceGeometry,
    name: string
  ): void {
    this.clearMarkers();
    const marker = new google.maps.Marker({
      map: this.googleMap,
      position: geometry.location,
    });
    this.markers.push(marker);
    this.initLocationsInMap();
    this.lastMarker = geometry.location;

    this.googleMap.setCenter(marker.getPosition());
    this.googleMap.setZoom(15);
  }

  private initMap(data): void {
    // The location of Uluru
    const uluru = {
      lat: parseFloat(data ? data.latitude : 1.354916),
      lng: parseFloat(data ? data.longitude : 103.867728),
    };
    // The map, centered at Uluru
    this.googleMap = new google.maps.Map(
      document.getElementById('googleMap') as HTMLElement,
      {
        zoom: 15,
        center: uluru,
      }
    );

    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.googleMap,
    });

    this.initLocationsInMap();
    this.markers.push(marker);

    if (this.isDeliveryType) {
      this.calculateDeliveryFee(() => this.loadItemsOrder());
    }
  }

  private initLocationsInMap() {
    this.lstLocation.forEach((location) => {
      if (
        location.pickupAddressLat != null &&
        location.pickupAddressLong != null
      ) {
        let item = {
          lat: parseFloat(location.pickupAddressLat),
          lng: parseFloat(location.pickupAddressLong),
        };
        let marker = new google.maps.Marker({
          position: item,
          map: this.googleMap,
          title: location.name,
          label: {
            color: 'black',
            text: location.name,
          },
        });
        console.log('vao');
        this.markers.push(marker);
      }
    });
  }

  private initPayment() {
    this.omiseLabel = 'OnlinePayment';
    const paymentType = this.formGroup.controls['paymentType'].value;

    if (
      paymentType === PaymentMethods.Omise &&
      this.omisePaymentGatewayPublicKey
    ) {
      this.initOmiseElements();
      this.omiseLabel = 'OnmisePayment';
    } else if (paymentType === PaymentMethods.Cash) {
      this.submitPayment(null);
    } else if (paymentType === PaymentMethods.Stripe && this.stripePublicKey) {
      this.initStripeCheckoutElements();
    }
  }

  initStripeCheckoutElements() {
    let self = this;
    const strikeCheckout = (<any>window).StripeCheckout.configure({
      key: this.stripePublicKey,
      currency: this.systemCurrency,
      email: this.email.value,
      token: function (stripeToken: any) {
        self.submitPayment(stripeToken.id);
      },
    });

    strikeCheckout.open({
      name: 'Payment',
      amount: this.totalPrice * 100,
    });
  }

  private initOmiseElements() {
    if (this.omisePaymentGatewayPublicKey) {
      OmiseCard.configure({
        publicKey: this.omisePaymentGatewayPublicKey,
      });

      let self = this;
      OmiseCard.open({
        amount: this.totalPrice * 100,
        currency: this.systemCurrency,

        defaultPaymentMethod: 'credit_card',
        onCreateTokenSuccess: (nonce) => {
          if (nonce.startsWith('tokn_')) {
            self.submitPayment(nonce);
          }
        },
      });
    } else {
      this.dineMessageService.showError(
        'Omise payment is not available at this moment'
      );
    }
  }

  strikeCheckout: any = null;
  initStripePaymentGateway() {
    if (!window.document.getElementById('stripe-script')) {
      const scr = window.document.createElement('script');
      scr.id = 'stripe-script';
      scr.type = 'text/javascript';
      scr.src = 'https://checkout.stripe.com/checkout.js';

      scr.onload = () => {
        this.strikeCheckout = (<any>window).StripeCheckout.configure({
          key: this.stripePublicKey,
          locale: 'auto',
          token: function (token: any) {
            console.log(token);
          },
        });
      };

      window.document.body.appendChild(scr);
    }
  }

  private submitPayment(token: string) {
    let totalItems = 0;
    this.orderList.forEach((item) => {
      totalItems += item['quantity'];
    });

    const temp: any = {
      order: {
        tableId: this.tableId,
        deliveryTime: this.dateTimeDelivery || 'Not Available',
        deliveringTime: this.deliveryExpectdTime || 'Not Available',
        amount: parseFloat(this.subTotalPrice.toString()),
        paidAmount: this.totalPrice,
        quantity: totalItems,
        wheelOrderType: this.selectedDepartment.wheelOrderType,
        note: this.note.value,
        discountAmount: this.promotionDiscountPayment ? this.promotionDiscountPayment.totalClaimAmount : 0,
        wheelLocationId: this.selectedLocation?.id,
      },
      billingInfo: {
        name: this.username.value,
        plateNumber: this.isPickupPlateNumber ? this.plateNumber.value : '',
        emailAddress: this.email.value,
        address1: this.isDeliveryType ? this.address : null,
        address2: this.isDeliveryType ? this.address2.value : null,
        blockunit: this.isDeliveryType ? this.blockunit.value : null,
        city: this.selectedLocation?.city,
        country: this.selectedLocation?.country,
        lat: this.isDeliveryType ? parseFloat(this.oneMapValue.latitude) : null,
        long: this.isDeliveryType ? parseFloat(this.oneMapValue.longitude) : null,
        cityId: 0,
        countryId: 0,
        phoneNumber: AppSession.user?.phoneNumber,
        phonePrefix: AppSession.user?.phonePrefix,
        postcalCode: this.isDeliveryType ? this.postalCode.value : null,
        zone: 0,
        deliveryFee: this.deliveryFee,
        wheelServiceFees: JSON.stringify(this.wheelServiceFees),
        wheelDepartmentId: this.selectedDepartment.id,
        wheelLocationId: this.selectedLocation?.id,
        tax: this.taxAmount,
      },

      paymentOption: this.paymentType.value,
      paidAmount: this.totalPrice,
      callbackUrl: location.origin + '/order-status',
      omiseToken: this.paymentType.value === PaymentMethods.Omise ? token : '',
      source: 'string',
      adyenEncryptedCardNumber: 'string',
      adyenEncryptedExpiryMonth: 'string',
      adyenEncryptedExpiryYear: 'string',
      adyenEncryptedSecurityCode: 'string',
      paypalOrderId: 'string',
      stripeToken: this.paymentType.value === PaymentMethods.Stripe ? token : '',
      googlePayToken: 'string',
      discountDetails: this.promotionDiscountPayment ? this.promotionDiscountPayment.discountDetails : [],
    };

    this.createPaymentOrder(temp);
  }

  formatDate(date: Date, time: string) {
    var datetimeStr = this.datePipe.transform(date, 'yyyy-MM-dd') + ' ' + time;
    return new Date(datetimeStr);
  }

  private createPaymentOrder(temp) {
    if (this.isTimeSlot) {
      let input:any = {};
      input.wheelDepartmentTimeSlotId = this.timeSlot.id;
      temp.wheelOrderTracking = input;
    }

    this.checkOutService.createPaymentForOrder(temp).subscribe(
      (res) => {
        if (res.result.invoiceNo) {
          this.router.navigateByUrl(
            '/payment-information/' + res.result.invoiceNo
          );
        } else {
          if (res.result.message != null) {
            this.dineMessageService.showError(res.result.message);
          } else {
            this.dineMessageService.showError('Payment Error!');
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  searchPostalCode(): void {
    this.postalCode.markAsTouched();
    if (this.postalCode.value.length === 0) {
      this.getUserLocation();
      return;
    }

    this.getAddressByPostCode(this.postalCode.value).subscribe(
      (res: any) => {
        if (res.result.length === 0) {
          this.dineMessageService.showError('PostalCode is not found');
          this.postalCode.setErrors({ incorrect: true });
        } else {
          if (res.result && res.result.length) {
            const address = res.result[0];
            if (address) {
              this.checkOutService.saveCustomerSessionData(POSTAL_CODE_ONEMAP, JSON.stringify(address)).subscribe();
              this.checkOutService.saveCustomerSessionData(POSTAL_CODE, address.postal).subscribe();

              this.address1Control.setValue(address.address);
              this.postalCode.setValue(address.postal);

              this.initMap(this.oneMapValue);
            }
          }
        }
      },
      (error) => {
        this.dineMessageService.showError('PostalCode is not found');
        this.postalCode.setErrors({ incorrect: true });
      }
    );
  }

  private getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const longitude = position.coords.longitude;
        const latitude = position.coords.latitude;
        this.codeLatLng(latitude, longitude);

        this.initMap({ latitude: latitude, longitude: longitude });
      });
    } else {
      console.log('User not allow');
    }
  }

  private codeLatLng(lat, lng) {
    let self = this;
    let geocoder = new google.maps.Geocoder();

    let latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ location: latlng }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results.length) {
          //formatted address
          const addressValue = results[0];

          let oneMapadd = {
            address: addressValue.formatted_address,
            latitude: lat,
            longitude: lng,
            longtitude: lng,
            searchval: addressValue.formatted_address,
          };
          if (addressValue) {

            this.checkOutService.saveCustomerSessionData(POSTAL_CODE_ONEMAP, JSON.stringify(oneMapadd)).subscribe();

            let postCode =
              addressValue.address_components.find(
                (x) => x.types.indexOf('postal_code') >= 0
              )?.short_name ||
              addressValue.address_components[
                addressValue.address_components.length - 1
              ].short_name;

            this.checkOutService.saveCustomerSessionData(POSTAL_CODE, postCode).subscribe();

            this.postalCode.setValue(postCode);
            this.address1Control.setValue(addressValue.formatted_address);
          }
        }
      }
    });
  }

  private getAddressByPostCode(postcode: string): Observable<any> {
    let url_ = '/api/services/app/WheelLocations/GetAddressByPostCode?';
    if (postcode !== undefined && postcode !== null)
      url_ += 'postcode=' + encodeURIComponent('' + postcode) + '&';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get(options);
  }

  get postalCode(): AbstractControl {
    return this.formGroup.controls.postalCode;
  }

  get address1Control(): AbstractControl {
    return this.formGroup.controls.address1;
  }

  private updateTotal() {
    this.taxAmount = this.orderTotal.itemsTaxTotal + this.orderTotal.serviceFeeTaxTotal;

    if (this.selectedLocation?.wheelTaxes?.length) {
      this.selectedLocation.wheelTaxes.forEach((fee) => {
        this.taxAmount += (this.deliveryFee * fee.taxPercent) / 100;
      });
    }

    this.totalPrice = this.subTotalPrice + this.orderTotal.serviceFeeTotal + this.deliveryFee + this.taxAmount;
  }

  private getPromotionByCode(couponCode: string): Observable<any> {
    let url_ = '/api/services/app/WheelPromotion/GetWheellPromotionByCode?';
    if (couponCode !== undefined && couponCode !== null)
      url_ += 'code=' + encodeURIComponent('' + couponCode) + '';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };

    return this.http.get(options);
  }

  calculatePromotionDiscountPayment(
    body: ICalculatePromotionDiscountPaymentInputDto | undefined
  ): Observable<any> {
    let url_ =
      '/api/services/app/WheelPromotion/CalculatePromotionDiscountPayment';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);

    const postContent: HttpOptions = {
      url: url_,
      body: content_,
    };

    return this.http.post(postContent);
  }

  applyCoupon(): void {
    this.getPromotionByCode(this.coupon.value)
      .pipe(
        catchError((error) => {
          return throwError(error);
        }),
        switchMap((result) => {
          if (result.result && result.result.id) {
            var promotionCodes = this.promotionDiscountPayment
              ? this.promotionDiscountPayment.discountDetails.map((t) => t.code)
              : [];

            const request = {
              paidAmount: this.subTotalPriceWithoutDiscount,
              codes: [this.coupon.value, ...promotionCodes],
            } as ICalculatePromotionDiscountPaymentInputDto;

            return this.calculatePromotionDiscountPayment(request);
          } else {
            this.dineMessageService.showError(
              'The Coupon Code is wrong or expired. Please check it.'
            );
            return throwError(result);
          }
        })
      )
      .subscribe((result) => {
        if (result) {
          this.promotionDiscountPayment = result.result as PromotionDiscountPaymentDto;
          if (!this.promotionDiscountPayment.discountDetails?.length) {
            this.dineMessageService.showError(
              'The Coupon Code is wrong or expired. Please check it.'
            );
          }

          this.subTotalPrice = this.promotionDiscountPayment.discountedAmount;

          this.updateTotal()

          this.coupon.reset();
        }
      });
  }

  checkMinimumOder(callback?) {
    this.checkOutService.checkMinOrder().subscribe((t) => {
      if (callback) {
        callback(t.result);
      }
    });
  }
  private getMessage(tolalOrderMin) {
    return `Minimum order total required is ${this.systemCurrency} ${tolalOrderMin}. Please add more items.`;
  }
}

import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/shared/common';

@Component({
  selector: 'footer-ui',
  templateUrl: './footer-ui.component.html',
  styleUrls: ['./footer-ui.component.scss']
})
export class FooterUiComponent implements OnInit {

  isWheelSetupsAvailable: boolean;
  isWelComePage: boolean;

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.isWheelSetupsAvailable = this.commonService.isWheelSetupsAvailable;
    this.commonService.$isWelComePage.subscribe(isWelComePage => {
      this.isWelComePage = isWelComePage;
    });
  }
}

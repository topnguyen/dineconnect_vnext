import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/shared/service';
import { LOGO_URL } from 'src/shared/shared.const';
import { TermAndConditonService } from '../terms-and-conditions/term-and-conditon.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  logoUrl: string = '';
  userId = undefined;
  dynamicPage: any;
  constructor(
    private termAndConditonService: TermAndConditonService,
    private readonly accountService: AccountService,
    private router: Router
  ) {
    this.userId = this.accountService.userValue.id;
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }
  ngOnInit(): void {
    this.getPrivacyPolicy();
  }
  getPrivacyPolicy() {
    this.termAndConditonService.getDynamicPage(1).subscribe((res) => {
      if (res.success) {
        this.dynamicPage = res.result;
        this.appendCss(this.dynamicPage.css);
      }
    });
  }
  onClickBack() {
    this.router.navigateByUrl('/');
  }
  appendCss(customData) {
    $(document).ready(function () {
      $('style').append(customData);
    });
  }
}

import { IOrderItem } from './../../../model/cart.model';
import { Component, Input, OnInit } from '@angular/core';
import { MenuItemService } from 'src/shared/service';
import {
  CommonService,
  DineMessageService,
  LocalStorageService,
} from 'src/shared/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderSetting, StepName, wheelDataSetups } from 'src/shared/constant';
import * as _ from 'lodash';

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import {
  MenuItemData,
  Tag,
  WheelMenuComboItems,
  WheelMenuCombos,
} from 'src/model';
import { ToastService } from 'src/shared/toast';
import { SELECTED_DEPARTMENT } from 'src/shared/shared.const';
import { ICartModel } from 'src/model/cart.model';
import { AddItemToCartInputDto, OrderSettingDto } from 'src/shared/service-proxies/service-proxies';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';

@Component({
  selector: 'app-menu-item-ui',
  templateUrl: './menu-item-ui.component.html',
  styleUrls: ['./menu-item-ui.component.scss'],
  animations: [
    trigger('popupState', [
      state('default', style({})),
      state(
        'open',
        style({
          bottom: 0,
        })
      ),
      transition('default <=> open', animate(500)),
    ]),
    trigger('comboItemState', [
      state('hide', style({})),
      state(
        'show',
        style({
          bottom: 0,
        })
      ),
      transition('hide <=> show', animate(500)),
    ]),
  ],
})
export class MenuItemUiComponent implements OnInit {
  @Input() selectedTag: string;
  @Input() filter: string;

  @Input() set numberOfColumns(value: number) {
    this.calculatorCol(value);
  }

  @Input() set menuItemData(data: any[]) {
    this.data = data;
    this.menuItems = data ?? [];

    this.menuItems.forEach((element) => {
      if (element.type == 2) {
        element.states = { expanded: false };
        element.wheelMenuComboItems.forEach((item) => (item.expanded = false));
      }
    });
  }

  @Input() set viewType(data) {
    if (data) {
      if (data === 0) {
        // Grid =0,
        this.isListStyle = false;
      } else if (data === 1) {
        // List =1,
        this.isListStyle = true;
      } else {
        // Collapse = 2
        this.isListStyle = true;
      }
    }
  }
  calculatorCol(input) {
    if (input) {
      if (input == 1) {
        this.colSetting = 'col-md-12';
      } else if (input == 2) {
        this.colSetting = 'col-md-6';
      } else if (input == 3) {
        this.colSetting = 'col-md-4';
      } else if (input == 4) {
        this.colSetting = 'col-md-3';
      }
    }
  }
  isListStyle = false;
  colSetting: string;
  localMenuItems: CustomMenuItemData[] = [];
  portionsForm: FormGroup = new FormGroup({});
  tagsForm: FormGroup = new FormGroup({});
  isOpenAddon = false;

  menuItems: any[] = [];

  wheelMenuComboItems: WheelMenuComboItems;

  listMenuItem: MenuItemData[] = [];
  selectItem: MenuItemData = null;
  selectCombo: WheelMenuCombos = null;
  invalidAddCombo = false;
  displayTag: boolean;
  state = 'default';
  stateCombo = 'default';
  stateNutrition = 'default';
  comboItemState = 'hide';
  selectedPortion: any = null;
  portionGroup: string;
  totalItemsAdd = 1;
  totalPrice = 0;
  priceItem = 0;
  selectedSectionId: number = NaN;
  selectedSectionItems: MenuItemData = null;
  msg =
    'This item has multiple customizations added. Remove the correct item from the cart.';
  menuIdActive: number;
  systemCurrency = wheelDataSetups.currency;

  // popup
  isOpenComboAddon = false;
  isOpenComboItemsAddon = false;
  isOpenNutri = false;
  carts: ICartModel[] = [];
  sizeImg: any = OrderSetting;
  itemSelected: any;

  totalComboItemsAdd = 1;
  totalComboPrice = 0;
  data: any;
  constructor(
    private menuItemService: MenuItemService,
    private orderService: WheelOrderService,
    private localStorageService: LocalStorageService,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private dineMessageService: DineMessageService
  ) {
  }

  ngOnInit(): void {
    this.localStorageService.$orderListLocalStorage.subscribe((items) => {
      this.localMenuItems = items;
      this.menuItems = this.handleSyncOrder(items, this.menuItems);
    });

    this.commonService.$currentStepName.next(StepName.menu);
  }

  openAddon(menuItem: MenuItemData, isCombo: boolean, isComboMenuItem = false) {
    if (isCombo) {
      this.isOpenComboAddon = true;
      this.stateCombo === 'default'
        ? (this.stateCombo = 'open')
        : (this.stateCombo = 'default');
    } else {
      if (menuItem?.portions && menuItem.portions.length) {
        this.selectedPortion = menuItem.portions[0];
        this.priceItem = menuItem.portions[0].price;
        this.getFormPortion(menuItem);
      }
      this.isOpenAddon = true;
      if (!isComboMenuItem) {
        this.sumTotalPrice();
      }
      this.state === 'default'
        ? (this.state = 'open')
        : (this.state = 'default');
    }
  }

  closeAddonPopUp(isCombo = false) {
    if (isCombo) {
      this.isOpenComboAddon = false;
      this.stateCombo === 'default'
        ? (this.stateCombo = 'open')
        : (this.stateCombo = 'default');
    } else {
      this.totalItemsAdd = 1;
      this.isOpenAddon = false;
      this.isOpenComboItemsAddon = false;
      this.state === 'default'
        ? (this.state = 'open')
        : (this.state = 'default');
    }
  }

  onClickComboItem(comboItem): void {
    this.wheelMenuComboItems = comboItem;

    this.wheelMenuComboItems.wheelMenuCombosItemMenus.forEach((combo) => {
      if (combo.portions) {
        combo.portions[0].checked = true;
      }
    });

    this.closeAddonPopUp(true);
    setTimeout(() => {
      this.isOpenComboItemsAddon = true;
      this.state === 'default'
        ? (this.state = 'open')
        : (this.state = 'default');
    }, 550);
  }

  getFormPortion(item: MenuItemData) {
    this.portionsForm = this.formBuilder.group({
      portion: [item.portions[0]],
    });
  }

  onClickPortion(item: any, isComboItem: boolean = false): void {
    this.portionsForm.controls.portion.setValue(item);
    if (isComboItem) {
      this.wheelMenuComboItems.wheelMenuCombosItemMenus.forEach((combo) => {
        combo.portions.forEach((portion) => {
          if (portion.id === item.id) {
            portion.checked = !portion.checked;
          }
        });
      });
      this.totalPrice += item.price;
    } else {
      this.priceItem = item.price;

      this.sumTotalPrice();
    }
  }

  getDetailMenuItem(id: number) {
    return this.menuItemService.getMenuItemForEdit(id);
  }

  addItemNumber(isPlus) {
    if (!isPlus && this.totalItemsAdd === 1) {
      return;
    }
    if (isPlus) {
      this.totalItemsAdd = this.totalItemsAdd + 1;
    } else {
      this.totalItemsAdd = this.totalItemsAdd - 1;
    }
    this.sumTotalPrice();
  }

  getMenuItems(data): void {
    this.menuItems = data;
  }

  handleSyncOrder(
    items: CustomMenuItemData[],
    originItems: MenuItemData[]
  ): MenuItemData[] {
    if (!originItems || !originItems.length) {
      return [];
    }

    if (!items || !items.length) {
      originItems.forEach((x) => (x.total = 0));
    }

    originItems.forEach((x) => {
      const el = items.find((k) => k.id === x.id);
      if (el) {
        x.total = el.total;
      } else {
        x.total = 0;
      }
      let countTotalDisplay = 0;
      items.forEach((k) => {
        if (k.id === x.id) {
          countTotalDisplay += k.total;
        }
      });
      x.totalDisplay = countTotalDisplay;
    });

    return originItems;
  }

  OnDestroy(): void {
    this.commonService.$isHideFooterSubject.next(false);
    this.commonService.$isMenuSubject.next(false);
  }

  addItem(item: any, isCombo = false, isComboMenuItem = false) {
    if (!isCombo) {
      this.selectItem = Object.assign({}, item);
      item.orderTags?.find((orderTagItem) => {
        orderTagItem.tags.find((tags) => {
          tags.checked = false;
        });
      });
      this.selectItem.note = item.note || '';
    } else {
      this.selectCombo = Object.assign({}, item);

      if (!isComboMenuItem) {
        this.totalPrice = this.selectCombo.basePrice;
      }
      this.selectCombo.note = item.note || '';
    }

    this.openAddon(item, isCombo);
  }

  addComboItem(item: any, name) {
    this.closeAddonPopUp();
    this.selectItem = { ...item };
    this.selectItem.isComboItem = true;
    this.selectItem.wheelMenuComboName = name;
    this.priceItem += item.addPrice;
    this.selectItem.orderTags?.forEach((orderTagItem) => {
      orderTagItem.tags.forEach((tags) => {
        tags.checked = false;
      });
    });
    this.getFormPortion(this.selectItem);
    // this.totalPrice = parseFloat((this.totalPrice + item.addPrice).toFixed(2));
    setTimeout(() => {
      this.openAddon(this.selectItem, false, true);
    }, 500);
  }

  invalidMinTags() {
    var hasInvalidTags = [];
    this.selectItem.orderTags.forEach((orderTagItem) => {
      var count = 0;
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked === true) {
          count++;
        }
      });

      if (count < orderTagItem.minSelectedItems) {
        hasInvalidTags.push(orderTagItem.name);
      }
    });

    return hasInvalidTags;
  }

  addItemToOrder(item: MenuItemData, isAddOn: boolean = false, totalAdd) {

    var invalidTags = this.invalidMinTags();

    if (invalidTags.length) {
      this.dineMessageService.showError(
        `Min selected item required for ` + invalidTags.join(', ')
      );
      event.preventDefault();
      return;
    }

    const selectedTags = [];
    item.orderTags.forEach((orderTagItem) => {
      orderTagItem.isShowOrderTags = orderTagItem.tags.some(
        (tags) => tags.checked === true
      );
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked === true) {
          tags.tagGroupName = orderTagItem.name;
          selectedTags.push(tags);
        }
      });
    });

    let input = {
      screenMenuItemId: item.id,
      quantity: totalAdd,
      tags: selectedTags,
      portion: this.portionsForm.controls.portion.value,
      note: item.note
    };

    this.orderService.addOrderItemToCart(input);

    if (isAddOn) {
      this.closeAddonPopUp();
    }
  }

  onClickCheckBox(event, orderTagId: number, tagItem: Tag): void {
    this.selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        let countTags = 0;
        orderTagItem.tags.forEach((tags) => {
          if (tags.checked) {
            countTags = countTags + tags.quantity;
          }
        });
        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tagItem.id) {
            countTags = countTags + tags.quantity;
            if (!tags.checked && orderTagItem.maxSelectedItems && countTags > orderTagItem.maxSelectedItems) {
              this.dineMessageService.showError(
                'Maximum is selected for the item'
              );
              event.preventDefault();
            } else {
              tags.checked = !tags.checked;
            }
          }
        });
      }
    });
    this.sumTotalPrice();
  }

  incTags(event, orderTagId: number, tag: Tag): void {
    if (tag.maxQuantity === tag.quantity) {
      this.dineMessageService.showError('Maximum quantity items');
      return;
    }
    this.selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        let countTags = 0;
        orderTagItem.tags.forEach((tags) => {
          if (tags.checked || tags.id == tag.id) {
            countTags = countTags + tags.quantity;
          }
        });

        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tag.id) {
            if (orderTagItem.maxSelectedItems && countTags >= orderTagItem.maxSelectedItems) {
              this.dineMessageService.showError('Maximum Quantity is selected');
              event.preventDefault();
            } else {
              if (!tag.checked) {
                tag.checked = true;
              }
              tag.quantity = tag.quantity + 1;
            }
          }
        });
      }
    });
    this.sumTotalPrice();
  }

  decTags(orderTagId: number, tag: Tag): void {
    if (tag.quantity === 1) {
      this.dineMessageService.showError('Minimum Quantity should be selected');
      return;
    }
    this.selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tag.id) {
            tag.quantity = tag.quantity - 1;
          }
        });
      }
    });
    this.sumTotalPrice();
  }

  sumTotalPrice() {
    this.totalPrice = 0;
    this.totalPrice = this.portionsForm.controls.portion.value.price;
    this.selectItem.orderTags.forEach((orderTagItem) => {
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked) {
          this.totalPrice = this.totalPrice + tags.price * tags.quantity;
        }
      });
    });

    this.totalPrice = this.totalPrice * this.totalItemsAdd;
  }

  onChooseSection(sectionChildren): void {
    this.selectedSectionId = sectionChildren.id;
    this.selectedSectionItems = sectionChildren || null;
  }

  updateComboItemStatus(): void {
    if (this.selectCombo) {
      this.sumComboPrice();
    }
  }

  showMoredetail(data) {
    this.itemSelected = JSON.parse(data);
    this.isOpenNutri = true;
    this.stateNutrition === 'default'
      ? (this.stateNutrition = 'open')
      : (this.stateNutrition = 'default');
  }

  closeAddonPopUpMoreDetail() {
    this.stateNutrition === 'default'
      ? (this.stateNutrition = 'open')
      : (this.stateNutrition = 'default');
    this.isOpenNutri = false;
  }

  sumComboPrice() {
    if (this.selectCombo) {
      this.totalComboPrice = this.selectCombo.basePrice;
      this.selectCombo.wheelMenuComboItems.forEach((comboItem) => {
        if (comboItem.itemSelect?.length) {
          comboItem.itemSelect.map(
            (item) => (this.totalComboPrice += item.total ?? 0)
          );
        }
      });
      this.totalComboPrice = this.totalComboPrice * this.totalComboItemsAdd;
    }
  }

  addComboItemNumber(isPlus) {
    if (!isPlus && this.totalComboItemsAdd === 1) {
      return;
    }
    if (isPlus) {
      this.totalComboItemsAdd = this.totalComboItemsAdd + 1;
    } else {
      this.totalComboItemsAdd = this.totalComboItemsAdd - 1;
    }

    this.sumComboPrice();
  }

  addComboItemToOrder(item: WheelMenuCombos) {
    let isValid = true;
    item.wheelMenuComboItems.forEach((element) => {
      if (!element.isValid) {
        this.invalidAddCombo = true;
        isValid = false;
      }
    });
    if (isValid) {
      const orderItems = item.wheelMenuComboItems.map((e) => e.itemSelect);
      const newOrderItem = [];
      for (const odItem of orderItems) {
        odItem.map((orderItem) => {
          const tagChecked = orderItem.orderTags.map((orderTag) => {
            orderTag.tags.forEach((tag) => {
                tag.tagGroupName = orderTag.name;
            });
            return orderTag.tags.filter((x) => x.checked);;
          });
          const mergedTagIds = tagChecked.reduce(
            (arr, elem) => [...arr, ...elem],
            []
          );
          const tags = mergedTagIds.map((i) => {
            const orderTagItem = {
              id: i.id,
              name: i.name,
              price: i.price,
              tagGroupName: i.tagGroupName,
              quantity: i.quantity,
            } as any;
            return orderTagItem;
          });

          const order = {
            screenMenuItemId: orderItem.id,
            quantity: orderItem?.quantity ?? 0,
            tags: tags,
            portion: {price:orderItem.price },
            note: orderItem.note,
          } as any;

          newOrderItem.push(order);
        });
      }

      const input = {
        screenMenuItemId: item.id,
        quantity: this.totalComboItemsAdd,
        tags: null,
        portion: item.portions[0],
        note: item.note,
        subItems: newOrderItem,
      };

      this.orderService.addOrderItemToCart(input, true);
      this.closeAddonPopUp(true);
    }
  }
}

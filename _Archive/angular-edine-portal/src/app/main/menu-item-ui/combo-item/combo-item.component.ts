import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  MenuItemData,
  Tag,
  WheelMenuComboItems,
  WheelMenuCombos,
} from 'src/model';
import { wheelDataSetups } from 'src/shared/constant';
import { OrderSettingDto, ThemeSettingsDto } from 'src/shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import { DineMessageService } from 'src/shared/common';
import { WheelThemeSettingsService } from 'src/shared/service';

@Component({
  selector: 'app-combo-item',
  templateUrl: './combo-item.component.html',
  styleUrls: ['./combo-item.component.scss'],
})
export class ComboItemComponent implements OnInit {
  @Input() selectCombo: WheelMenuCombos = null;
  @Input() sizeImg: OrderSettingDto = new OrderSettingDto();
  @Output() stateChanged: EventEmitter<any> = new EventEmitter();
  @Input() comboItems: WheelMenuComboItems;
  @Input() invalidCombo: boolean;

  selectedItems: any[] = [];
  systemCurrency = wheelDataSetups.currency;

  menuItemTagSelected: any[] = [];

  errors: string[] = [];

  constructor(private dineMessageService: DineMessageService) { }

  ngOnInit(): void {
    this.comboItems.itemSelect = [];
    this.comboItems.wheelMenuCombosItemMenus.map((item) => {
      if (item.autoSelect) {
        this.autoSelect(item);
      }
    });

    this.selectedItems = [];
    this.comboItems.wheelMenuCombosItemMenus.map((item) => {
      var itemsToAddCount = item.addSeperately ? item.quantity : 1;
      for (let i = 0; i < itemsToAddCount; i++) {
        let selectItem = <any>JSON.parse(JSON.stringify(item));

        selectItem.seperatedIndex = i + 1;

        if (item.addSeperately) {
          selectItem.maxQuantity = 1;
        }
        else {
          selectItem.maxQuantity = item.quantity;
        }

        selectItem.quantity = 1;
        this.sumTotalPrice(selectItem);
        this.selectedItems.push(selectItem);
      }
    });

    this.updateComboItemStatus();
  }

  autoSelect(item) {
    item.selected = true;
  }

  changeQty(isAdd: boolean, selectItem: any) {
    if (isAdd) {
      if (selectItem.quantity < selectItem.maxQuantity)
        selectItem.quantity += 1;
    } else {
      if (selectItem.quantity === 1) {
        selectItem.selected = false;
        this.onSelectItem(selectItem);
        return;
      }
      else {
        selectItem.quantity -= 1;
      }
    }

    this.sumTotalPrice(selectItem);
    this.updateComboItemStatus();
  }

  changeTag(event, selectItem, orderTagId: number, tagItem: Tag): void {
    selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        let countTags = 0;
        orderTagItem.tags.forEach((tags) => {
          if (tags.checked) {
            countTags = countTags + tags.quantity;
          }
        });
        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tagItem.id) {
            countTags = countTags + tags.quantity;
            if (!tags.checked && countTags > orderTagItem.maxSelectedItems) {
              event.preventDefault();
            } else {
              tags.checked = !tags.checked;
            }
          }
        });
      }
    });

    this.sumTotalPrice(selectItem);
    this.updateComboItemStatus();
  }

  incTags(event, selectItem, orderTagId: number, tag: Tag): void {
    if (tag.maxQuantity === tag.quantity) {
      return;
    }
    selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        let countTags = 0;
        orderTagItem.tags.forEach((tags) => {
          if (tags.checked || tags.id == tag.id) {
            countTags = countTags + tags.quantity;
          }
        });

        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tag.id) {
            if (countTags >= orderTagItem.maxSelectedItems) {
              event.preventDefault();
            } else {
              if (!tag.checked) {
                tag.checked = true;
              }
              tag.quantity = tag.quantity + 1;
            }
          }
        });
      }
    });

    this.sumTotalPrice(selectItem);
    this.updateComboItemStatus();
  }

  decTags(selectItem, orderTagId: number, tag: Tag): void {
    if (tag.quantity === 1) {
      return;
    }
    selectItem.orderTags.forEach((orderTagItem) => {
      if (orderTagItem.id === orderTagId) {
        orderTagItem.tags.forEach((tags) => {
          if (tags.id === tag.id) {
            tag.quantity = tag.quantity - 1;
          }
        });
      }
    });

    this.sumTotalPrice(selectItem);
    this.updateComboItemStatus();
  }

  updateComboItemStatus(): any {
    let selectedItems = [];
    let itemsCount = 0;

    this.selectedItems.forEach((item) => {
      if (item.selected)
        itemsCount += item.quantity;
    });

    this.errors = [];

    if (this.comboItems.maximum < itemsCount) {
      this.errors.push('Maximum quantity of group ' + this.comboItems.name + '  is ' + this.comboItems.maximum);
    }

    if (this.comboItems.minimum > itemsCount) {
      this.errors.push('Minimum quantity of group ' + this.comboItems.name + '  is ' + this.comboItems.minimum);
    }

    this.selectedItems.forEach((item) => {
      if (item.selected) {
        this.validateComboItemItem(item);
        var comboItem = this.createSelectedComboItems(JSON.parse(JSON.stringify(item)));
        if (comboItem) {
          selectedItems.push(comboItem);
        }
      }
    });

    this.comboItems.errors = this.errors;
    this.comboItems.itemSelect = selectedItems;
    this.comboItems.isValid = this.errors.length == 0;

    this.stateChanged.emit(this)
  }

  private validateComboItemItem(item: any) {
    var invalidTags = this.invalidMinTags(item);

    if (invalidTags.length) {
      this.errors.push(`Min selected item required for ` + invalidTags.join(', '));
    }
  }

  invalidMinTags(item: any) {
    var hasInvalidTags = [];
    item.orderTags.forEach((orderTagItem) => {
      var count = 0;
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked === true) {
          count++;
        }
      });

      if (count < orderTagItem.minSelectedItems) {
        hasInvalidTags.push(orderTagItem.name);
      }
    });

    return hasInvalidTags;
  }

  private sumTotalPrice(item: any) {
    let totalPrice = item.price ?? 0;
    item.orderTags.forEach((orderTagItem) => {
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked) {
          totalPrice = totalPrice + tags.price * tags.quantity;
        }
      });
    });

    item.total = totalPrice * item.quantity ?? 1;
  }

  private createSelectedComboItems(selectItem) {
    let item = selectItem;

    let labelTag = '';
    const selectedTags = [];
    item.orderTags.forEach((orderTagItem) => {
      orderTagItem.isShowOrderTags = orderTagItem.tags.some(
        (tags) => tags.checked === true
      );
      orderTagItem.tags.forEach((tags) => {
        if (tags.checked === true) {
          tags.tagGroupName = orderTagItem.name;
          selectedTags.push(tags);
        }
      });

      labelTag = selectedTags.length
        ? selectedTags.map((tag) => tag.name).join(', ')
        : '';
    });

    const data: CustomMenuItemData = {
      name: item.name,
      published: item.published,
      image: item.image,
      price: item.price,
      order: item.order,
      type: item.type,
      parentId: item.parentId,
      screenMenuId: item.screenMenuId,
      portions: null,
      orderTags: item.orderTags,
      tags: selectedTags,
      lastModificationTime: item.lastModificationTime,
      lastModifierUserId: item.lastModifierUserId,
      creationTime: item.creationTime,
      creatorUserId: item.creatorUserId,
      id: item.id,
      note: item.note,
      tagLabel: labelTag,
      totalDisplay: item?.totalDisplay,
      totalPricePerItem: item?.totalPricePerItem,
      recommendedItems: item?.recommendedItems,
      description: item.description,
      quantity: item.quantity,
      total: item.total,
      addSeperately: item.addSeperately,
    };

    return data;
  }

  onSelectItem(selectItem: any): void {
    if (!selectItem.selected) {
      selectItem.orderTags.forEach((orderTagItem) => {
        orderTagItem.tags.forEach((tagItem) => {
          if (tagItem.checked) {
            tagItem.checked = false;
            tagItem.quantity = 1;
          }
        });
      });

      selectItem.quantity = 1;
    }
    this.sumTotalPrice(selectItem);
    this.updateComboItemStatus();
  }
}

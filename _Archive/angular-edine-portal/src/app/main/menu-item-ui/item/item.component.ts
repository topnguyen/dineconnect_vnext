import { Component, Input, OnInit } from '@angular/core';
import { MenuItemService } from 'src/shared/common';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  amountItems: number = 0;

  constructor(private menuItemService: MenuItemService) {}

  @Input() item: any;

  ngOnInit(): void {}

  onAddItems(e: MouseEvent) {
    e.stopPropagation();
    this.amountItems += 1;
    this.menuItemService.plusTotalPrice(10);
  }

  onRemoveItems(e: MouseEvent) {
    e.stopPropagation();
    this.amountItems -= 1;
    this.menuItemService.minorTotalPrice(10);
  }
}

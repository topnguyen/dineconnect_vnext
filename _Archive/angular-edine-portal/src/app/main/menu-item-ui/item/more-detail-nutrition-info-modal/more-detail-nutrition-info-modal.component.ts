import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-more-detail-nutrition-info-modal',
  templateUrl: './more-detail-nutrition-info-modal.component.html',
  styleUrls: ['./more-detail-nutrition-info-modal.component.scss']
})
export class MoreDetailNutritionInfoModalComponent implements OnInit {

  @Input() nutritionInfo: any;
  constructor(
  ) {}
  ngOnInit(): void {
    if(this.nutritionInfo == undefined || this.nutritionInfo == null){
     this.nutritionInfo = {
        servingSize: 0,
        calories: 0,
        caloriesFromFat: 0,
        totalFat: 0,
        totalFatPercent: 0,
        saturatedFat: 0,
        saturatedFatPercent: 0,
        transFat: 0,
        transFatPercent: 0,
        cholesterol: 0,
        cholesterolPercent: 0,
        sodium: 0,
        sodiumPercent: 0,
        totalCarbonhydrate: 0,
        totalCarbonhydratePercent: 0,
        dietaryFiber: 0,
        dietaryFiberPercent: 0,
        sugars: 0,
        sugarsPercent: 0,
        protein: 0,
        proteinPercent: 0,
        vitaminA: 0,
        vitaminAPercent: 0,
        vitaminC: 0,
        vitaminCPercent: 0,
        calcium: 0,
        calciumPercent: 0,
        iron: 0,
        ironPercent: 0,
    };
    }
  }
}

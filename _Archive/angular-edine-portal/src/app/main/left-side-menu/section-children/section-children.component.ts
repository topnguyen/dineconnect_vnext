import { Component, Input, OnInit } from '@angular/core';
import { MenuItemService } from 'src/shared/service';
import { LeftMenu } from 'src/model/left-menu.model';

@Component({
  selector: 'app-section-children',
  templateUrl: './section-children.component.html',
  styleUrls: ['./section-children.component.scss'],
})
export class SectionChildrenComponent implements OnInit {
  @Input() InputSectionChildren: LeftMenu = null;
  @Input() InputIsHaveMenuItem: boolean;
  OutputSectionChildren: LeftMenu = null;
  sectionChildren = [];
  selectItemID: number = null;
  itemActiveId: number;
  constructor(private menuItemService: MenuItemService) { }

  ngOnInit(): void {
    this.sectionChildren = this.InputSectionChildren.sectionChildren;
  }


  clickMenuItem(item: LeftMenu): void {
    if (!this.selectItemID) {
      this.selectItemID = item.id;
      this.InputSectionChildren = item;
    } else {
      this.selectItemID = null;
    }
    if (!item.sectionChildren) {
      this.menuItemService.$closeLeftMenuSubject.next(true);
    }
    this.itemActiveId = item.id;
  }
}

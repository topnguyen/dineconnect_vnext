import {
  AfterViewInit,
  Component,
  ElementRef,
  Injectable,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AccountService } from 'src/shared/service';
import { takeUntil } from 'rxjs/operators';
import { AppSession, CONSTANT, SelectCollapseData } from 'src/shared/constant';
import { fromEvent, Subject, timer } from 'rxjs';
import {
  languageItemList,
  SELECTED_DEPARTMENT,
  SELECTED_LOCATION,
} from 'src/shared/shared.const';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/shared/common';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { AppConsts } from 'src/shared/constant';

@Component({
  selector: 'app-left-side-menu',
  templateUrl: './left-side-menu.component.html',
  styleUrls: ['./left-side-menu.component.scss'],
})

@Injectable()
export class LeftSideMenuComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('containerRef', { static: false }) containerRef: ElementRef;
  @Input() hidden: boolean;

  userPhoneNumber: string = '';
  userAvatarUrl = '../../assets/image/user-avarta.png';
  languageDataItem = languageItemList;
  location = [];
  locationSelectData: SelectCollapseData[] = [];
  email = '';
  myAccount: SelectCollapseData[] = [
    {
      icon: 'assets/image/shopping-cart.png',
      label: 'Orders History',
      value: 'orders',
      url: '/my-profile/orders',
    },
    {
      icon: 'assets/image/location.png',
      label: 'Address',
      value: 'address',
      url: '/my-profile/address',
    },
  ];
  isContinueOrder = false;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  _appSession: any = AppSession;

  constructor(
    private accountService: AccountService,
    private checkOutService: CheckOutService,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {

  }


  show(): void {
    this.containerRef.nativeElement.style.display = 'block';
    document.querySelector('body').style.overflow = 'hidden';
  }

  close() {
    this.containerRef.nativeElement.style.display = 'none';
  }

  ngOnInit(): void {
    this.checkOutService.getCustomerCheckoutData().subscribe(res => {
      const selectedDepartment = res.result.department;
      const languageDisplayCode = res.result.language;
      const selectedLocation = res.result.location;

      this.languageDataItem.forEach((languageItem) => {
        languageItem.active = languageItem.value === languageDisplayCode;
      });

      this.location = selectedDepartment?.wheelLocationOutputs || [];
      this.location.forEach((locationItem) => {
        this.locationSelectData.push({
          icon: 'assets/image/location.png',
          label: locationItem.name,
          value: locationItem.id,
          active: selectedLocation?.id === locationItem.id,
        });
      });

      // check url for show ContinueOrder
      if (this.router.url == '/my-profile/orders' || this.router.url == '/my-profile/address') {
        this.isContinueOrder = true;
      } else {
        this.isContinueOrder = false;
      }
    })
  }

  ngAfterViewInit(): void {
    if (this.containerRef)
      fromEvent(this.containerRef.nativeElement, 'click')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event) => {
          if (event['target'].classList.contains('dine-container')) {
            event['target']
              .querySelector('.wrapper-left-menu')
              .classList.add('hide');
            timer(290)
              .pipe(takeUntil(this.destroy$))
              .subscribe(() => {
                this.containerRef.nativeElement.style.display = 'none';
                event['target']
                  .querySelector('.wrapper-left-menu')
                  .classList.remove('hide');
              });
          }
        });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }

  selectLocation($event): void {
    console.log($event);
  }

  logout() {
    this.checkOutService.resetCheckout().subscribe(() => {
      this.accountService.logOut().subscribe(() => {
        abp.auth.clearToken();
        abp.auth.clearRefreshToken();
        abp.utils.setCookieValue(AppConsts.authorization.encrptedAuthTokenName, undefined, undefined, abp.appPath);
        this.accountService.callBackLogOutEvent();
        location.href = '/';
      });
    });
  }

  continueOrder() {
    this.checkOutService.getCustomerCheckoutData().subscribe(res => {
      const selectedDepartment = res.result.department;
      const selectedLocation = res.result.location;

      this.router.navigateByUrl(`${selectedDepartment.id}/location/${selectedLocation.id}`);
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CommonService } from 'src/shared/common';
import { RegexPatterns, StepName } from 'src/shared/constant';
import { emailCustomValidators } from 'src/shared/custom-validation';

@Component({
  selector: 'app-guest-info',
  templateUrl: './guest-info.component.html',
  styleUrls: ['./guest-info.component.scss']
})
export class GuestInfoComponent implements OnInit {

  guestFormInfo: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.commonService.$currentStepName.next(StepName.guestInfo);
    this.createGuestFormInfo();
  }

  get guestFInfo() {
    return this.guestFormInfo.controls;
  }

  isError(control: AbstractControl) {
    return control.errors && (control.touched || control.dirty);
  }

  createGuestFormInfo() {
    this.guestFormInfo = this.formBuilder.group(
      {
        name: [''],
        emailAddress: ['', [
          Validators.required,
          emailCustomValidators(RegexPatterns.emailRegex),
        ]],
        phoneNumber: ['', [
          Validators.required,
          Validators.pattern('[0-9]+'),
          Validators.minLength(6),
          Validators.maxLength(15),
        ]],
      },
    );
  }

  subMit() {
    this.guestFormInfo.markAllAsTouched();
    if (this.guestFormInfo.invalid) return;
  }
}

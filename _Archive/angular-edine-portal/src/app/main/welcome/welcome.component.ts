import {
  SCAN_QR_CODE,
  CHOOSE_TABLE_BY_QR,
  SELECTED_DEPARTMENT,
  StatusDineMessage,
  SELECTED_LOCATION,
} from './../../../shared/shared.const';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { LOGO_URL } from 'src/shared/shared.const';
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import { AccountService } from 'src/shared/service';
import { DineMessageService } from 'src/shared/common';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { PAGE } from 'src/shared/constant';
import { PlatformLocation } from '@angular/common';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { HtmlHelper } from 'src/shared/helpers/HtmlHelper';
import { UrlHelper } from 'src/shared/helpers/UrlHelper';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  @ViewChild('leftMenuRef', { static: false })
  leftSideMenu: LeftSideMenuComponent;
  logoUrl: string = '';
  formGroup: FormGroup;

  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
  errPhoneMsg = 'Phone number is missing.';
  languages: Array<{ code: string; icon: string; name: string }> = [
    {
      code: 'EN',
      icon: 'assets/image/en-icon.png',
      name: 'ENGLISH',
    },
  ];
  constructor(
    private activeRouter: ActivatedRoute,
    private fb: FormBuilder,
    public accountService: AccountService,
    private router: Router,
    private wheelOrderService: WheelOrderService,
    private checkOutService: CheckOutService,
    private pl: PlatformLocation
  ) {
    let user = this.accountService.userValue;

    this.formGroup = this.fb.group({
      phoneNumber: [null, [Validators.required]],
    });

    this.formGroup.patchValue({
      phoneNumber: user?.phoneNumber,
    });

    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

    this.leftSideMenu?.close();
  }

  department: any;
  location: any;
  tableId: any;

  type: string;
  ngOnInit(): void {
    this.activeRouter.params.subscribe((params) => {
      this.type = params.type;

      this.checkOutService.getCustomerCheckoutData().subscribe(res => {
        this.location = res.result.location;
        this.department = res.result.department;
        
        if (res.result && res.result.table) {
          this.tableId = res.result.table.id;
        }
      });
    });
  }

  get phoneNumber(): AbstractControl {
    return this.formGroup.controls['phoneNumber'];
  }

  onSubmit(): void {
    this.phoneNumber.markAllAsTouched();
    this.phoneNumber.updateValueAndValidity();

    if (this.formGroup.invalid) {
      return;
    }
debugger;
    this.accountService.savePhoneNumber(this.phoneNumber.value.e164Number, this.phoneNumber.value.dialCode);

    if (this.type == 'verify') {
      debugger;
      if (this.department.isRequireOtp) {
        this.router.navigateByUrl(
          `/verify/${this.location.id}/${this.department.id}/${this.tableId}/${encodeURIComponent(this.phoneNumber.value.e164Number)}`
        );
      } else {
        this.onChooseLanguage();
      }
    } else {
      this.onChooseLanguage();
    }
  }

  onChooseLanguage(): void {
    if (
      this.department.wheelOrderType !== 1 &&
      this.department.allowPreOrder &&
      !this.department.isOpeningHour &&
      this.department.dateDefaultOrderFututer != null
    ) {
      this.router.navigateByUrl(`/select-date-future`);
    } else {
      this.router.navigateByUrl(
        `${this.department.id}/location/${this.location.id}`
      );
    }
  }

  onClickBack(): void {
    this.wheelOrderService
      .getPageToBack(PAGE.PhoneNumber, this.department.id, this.type == "verify")
      .subscribe(
        (res: any) => {
          switch (res.result) {
            case PAGE.Location:
              this.router.navigateByUrl('/location');
              break;
            case PAGE.Depaterment:
              this.router.navigateByUrl('');
              break;
            case PAGE.PostalCode:
              this.router.navigateByUrl('/postal-code');
              break;
          }
        },
        (error) => { }
      );
  }

  showMenu() {
    this.leftSideMenu.show();
  }
}

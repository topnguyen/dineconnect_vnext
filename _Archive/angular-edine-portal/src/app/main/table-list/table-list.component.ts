import { CHOOSE_TABLE_MANUAL, SELECTED_DEPARTMENT, SELECTED_LOCATION, SELECTED_TABLE } from './../../../shared/shared.const';
import { takeUntil } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common'
import { OrderContentComponent } from '../order-content/order-content.component';
import { WheelLocationsService } from 'src/shared/service';
import { ReplaySubject } from 'rxjs';
@Component({
  selector: 'table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent implements OnInit {
  orderContentComponent: OrderContentComponent;
  selectedLocation: any = null;
  selectedDepartment: any = null;
  tableList = [];

  headerLbl: string = 'Select Table'
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  constructor(
    private router: Router,
    private location: Location,
    private wheelTableService: WheelLocationsService,
  ) {
  }

  ngOnInit(): void {
    this.selectedLocation = JSON.parse(window.localStorage.getItem(SELECTED_LOCATION));
    this.selectedDepartment = JSON.parse(window.localStorage.getItem(SELECTED_DEPARTMENT));

    this.wheelTableService.getAllWheelTableForTableDropdown(this.selectedLocation.id, this.selectedDepartment.wheelOrderType)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.tableList = res.result.items;
      })
  }

  ngAfterViewInit(): void { }

  onClickBack(): void {
    this.location.back();
  }

  onClickTable(id): void {
    window.sessionStorage.setItem(CHOOSE_TABLE_MANUAL, JSON.stringify(true));
    window.sessionStorage.setItem(SELECTED_TABLE, JSON.stringify(id));
    this.router.navigateByUrl(`/verify/${this.selectedLocation.id}/${this.selectedDepartment.wheelOrderType}/${id}`)
  }
}
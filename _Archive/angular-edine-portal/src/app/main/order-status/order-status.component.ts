import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { IPageResult } from 'src/model';
import { OrderModel } from 'src/model/order.model';
import { CommonService } from 'src/shared/common';
import { StepName } from 'src/shared/constant';
import { MenuItemService } from 'src/shared/service/menu-item.service';
@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.scss'],
})
export class OrderStatusComponent implements OnInit, OnDestroy {
  value: string;
  elementType: 'url' | 'canvas' | 'img' = 'url';
  isHideInfo = false;
  data: OrderModel;
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  constructor(
    private commonService: CommonService,
    private menuItemService: MenuItemService,
    private activeRouter: ActivatedRoute,
    private router: Router
  ) {
    this.activeRouter.params.subscribe((params) => {
      this.getGetWheelOrder(params.id);
      this.value = params.id;
    });
  }

  ngOnInit(): void {
    this.commonService.$isWith70Subject.next(true);
    this.commonService.$isHideFooterSubject.next(true);
    this.commonService.$currentStepName.next(StepName.orderStatus);
  }

  ngOnDestroy(): void {
    this.commonService.$isWith70Subject.next(false);
    this.commonService.$isHideFooterSubject.next(false);
    this.destroy$.next(null);
  }

  openSide() {
    document.getElementById('information').style.height = '0';
    document.getElementById('information').style.paddingBottom = '1rem';

    this.isHideInfo = true;
  }

  closeSide() {
    document.getElementById('information').style.height = '17rem';
    document.getElementById('information').style.paddingBottom = '0';

    this.isHideInfo = false;
  }

  goToHome() {
    this.router.navigate(['/']);
  }

  getGetWheelOrder(id: string): void {
    this.menuItemService
      .getWheelOrder(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: IPageResult<OrderModel>) => {
        this.data = response.result;
      });
  }
}

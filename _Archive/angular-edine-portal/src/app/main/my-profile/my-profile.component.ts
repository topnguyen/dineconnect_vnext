import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ICreateOrUpdateUserInput, IUser, IUserRole } from 'src/model';
import { CommonService } from 'src/shared/common';
import { StepName } from 'src/shared/constant';
import { confirmPasswordValidators } from 'src/shared/custom-validation';
import { AccountService } from 'src/shared/service';
import { ToastService } from 'src/shared/toast';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  defaultStar: number = 3;
  userEditForm: FormGroup;
  passwordForm: FormGroup;
  roles: IUserRole[];
  userEditValue: IUser;
  _userName: string = '';
  display: boolean = false;
  isHiddenPassword: boolean = true;
  isHiddenConfirmPassword: boolean = true;

  constructor(
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.commonService.$currentStepName.next(StepName.myProfile);
    this.createForm();
    this.getUserForEdit();
  }

  get f() {
    return this.userEditForm.controls;
  }

  get fp() {
    return this.passwordForm.controls;
  }

  createChangePasswordForm() {
    this.passwordForm = this.formBuilder.group(
      {
        newPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      { validators: confirmPasswordValidators }
    );
  }

  createForm(user?: IUser) {
    this.userEditForm = this.formBuilder.group({
      id: new FormControl(user ? user.id : ''),
      name: new FormControl(user ? user.name : '', [Validators.required]),
      surname: new FormControl(user ? user.surname : '', [Validators.required]),
      emailAddress: new FormControl(user ? user.emailAddress : ''),
      phoneNumber: new FormControl(user ? user.phoneNumber : '', [
        Validators.required,
      ]),
      isActive: true,
      isLockoutEnabled: true,
      isTwoFactorEnabled: false,
      password: null,
      shouldChangePasswordOnNextLogin: true,
      userName: null,
    });
  }

  getUserForEdit() {
    this.accountService
      .getUserForEdit(this.accountService.userValue.id)
      .subscribe((value) => {
        if (value.success) {
          const { roles, user } = value.result;
          this.roles = roles;
          this.userEditValue = user;
          this._userName = `${user.name} ${user.surname}`;
          this.userEditForm.patchValue({ ...user });
        }
      });
  }

  updateUser(updateUserValue: IUser = null) {
    if (!updateUserValue) {
      updateUserValue = this.userEditForm.getRawValue();
    }
    const roleNnames = ['User'];

    let updateUser: ICreateOrUpdateUserInput = {
      user: updateUserValue,
      organizationUnits: [],
      sendActivationEmail: false,
      setRandomPassword: false,
      assignedRoleNames: roleNnames,
    };
    this.accountService.createOrUpdateUser(updateUser).subscribe((res) => {
      if (res.success) {
        const currentUser = this.accountService.userValue;
        let user = { ...updateUser.user };
        user.token = currentUser.token;
        this.accountService.userSubject.next(user);
        this.getUserForEdit();
        this.toastService.clear();
        this.toastService.showSuccess('Update User Successful');
      }
    });
  }

  saveChanges() {
    this.userEditForm.markAllAsTouched();
    if (this.userEditForm.valid) {
      this.updateUser();
    } else {
      this.toastService.clear();
      this.toastService.showWarn('Please enter all required fields!');
    }
  }

  saveChangePassword() {
    this.passwordForm.markAllAsTouched();
    if (this.passwordForm.valid) {
      const newPassForm = this.passwordForm.getRawValue();
      this.userEditValue.password = newPassForm.newPassword;
      this.updateUser(this.userEditValue);
    }
  }

  changePassword() {
    this.display = true;
    this.createChangePasswordForm();
  }
}

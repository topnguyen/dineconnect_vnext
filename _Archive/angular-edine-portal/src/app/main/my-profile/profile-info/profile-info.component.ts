import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TenantService } from 'src/app/tenant/tenant.service';
import { appModuleAnimation } from 'src/shared/animations/routerTransition';
import { DineMessageService } from 'src/shared/common';
import { CONSTANT } from 'src/shared/constant';
import { AccountService } from 'src/shared/service';
import {
  CreateCustomerDto,
  ERPComboboxItem,
} from 'src/shared/service-proxies/service-proxies';
import { CommonService } from 'src/shared/service/common.service';
import { LOGO_URL } from 'src/shared/shared.const';
import { AppSession } from 'src/shared/constant';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.scss'],
  animations: [appModuleAnimation()],
})
export class ProfileInfoComponent implements OnInit {
  headerLbl: string = 'Profile Info';
  isDeliveryType = true;
  logoUrl: string = '';
  formGroup: FormGroup;
  errInvalidEmailMsg = 'Invalid value. Please enter a valid email address.';
  errUsernameMsg = 'Name is missing.';
  errEmailMsg = 'Email is missing.';
  errAddress1Msg = 'Address1 is missing.';
  errAddressMsg = 'Address is missing.';

  errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
  errPhoneMsg = 'Phone number is missing.';
  country: ERPComboboxItem = new ERPComboboxItem();
  city: ERPComboboxItem = new ERPComboboxItem();
  zone: any;
  lstCity: ERPComboboxItem[] = [];
  lstCountry: ERPComboboxItem[] = [];
  tenantId: number;
  customer: CreateCustomerDto = new CreateCustomerDto();
  userId: number;
  zoneOptions = [
    { displayText: 'North', value: 1 },
    { displayText: 'NorthWest', value: 2 },
    { displayText: 'NorthEast', value: 3 },
    { displayText: 'Central', value: 4 },
    { displayText: 'West', value: 5 },
    { displayText: 'East', value: 6 },
    { displayText: 'South/Cbd', value: 7 },
  ];

  constructor(
    private dineMessageService: DineMessageService,
    private accountService: AccountService,
    private _commonService: CommonService,
    private tenantService: TenantService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
    const tenant = this.tenantService.getTenantAvailable();
    this.tenantId = tenant ? tenant.tenantId : 1;

    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

    if (AppSession && AppSession.user) {
      this.userId = AppSession.user.userId;
    }

    this.accountService.getCustomerForEdit().subscribe((res) => {
      if (res.result) this.customer = res.result;
      this.zone = this.zoneOptions.find(
        (x) => (x.value as any) == this.customer.zone
      );
      this.initForm();
    });
  }
  initForm() {
    this._commonService
      .getLookups('Cities', this.tenantId, undefined)
      .subscribe((res) => {
        if (res.result) this.lstCity = res.result;

        this.city = this.lstCity.find(
          (x) => (x.value as any) == this.customer.cityId
        );
      });
    this._commonService
      .getLookups('Countries', this.tenantId, undefined)
      .subscribe((res) => {
        if (res.result) this.lstCountry = res.result;
        this.country = this.lstCountry.find(
          (x) => (x.value as any) == this.customer.countryId
        );
      });
  }
  changeCountry(input) {
    this._commonService
      .getLookups('Cities', this.tenantId, input.value)
      .subscribe((res) => {
        if (res.result) this.lstCity = res.result;
      });
  }

  onSubmit() {
    if ((this.zone as any) !== undefined) {
      this.customer.zone = +this.zone.value;
    } else {
      this.customer.zone = undefined;
    }

    if ((this.country as any) !== undefined) {
      this.customer.countryId = +this.country.value;
    } else {
      this.customer.countryId = undefined;
    }

    if ((this.city as any) !== undefined) {
      this.customer.cityId = +this.city.value;
    } else {
      this.customer.cityId = undefined;
    }

    this.accountService
      .updateCustomer(this.customer)
      .subscribe((res) => {
        if (res.result) {
          this.customer.id = +res.result;
        }
        this.dineMessageService.showSuccess('Saved successfully');
      });
  }

  onClickBack(): void {
    this.location.back();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyProfileComponent } from './my-profile.component';
import { AuthGuard } from 'src/shared/common/authenticate';
import { ResetPasswordComponent } from '../account/reset-password/reset-password.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';

const routes: Routes = [
  {
    path: '',
    component: MyProfileComponent,
    children: [],
    canActivate: [AuthGuard]
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'orders',
    component: MyOrdersComponent
  },
  {
    path: 'address',
    component: ProfileInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MyProfileRoutingModule { }

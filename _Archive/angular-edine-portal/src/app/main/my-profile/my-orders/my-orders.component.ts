import {
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DataView } from 'primeng/dataview';
import { appModuleAnimation } from 'src/shared/animations/routerTransition';
import { DineMessageService } from 'src/shared/common';
import { CONSTANT } from 'src/shared/constant';
import { AccountService } from 'src/shared/service';
import { WheelOrderListDto } from 'src/shared/service-proxies/service-proxies';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { LOGO_URL } from 'src/shared/shared.const';
import { AppSession } from 'src/shared/constant';

@Component({
  selector: 'my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss'],
  animations: [appModuleAnimation()],

})
export class MyOrdersComponent implements OnInit {

  @ViewChild('ordersListView', { static: true }) ordersListView: DataView;
  orderList: WheelOrderListDto[] = [];
  state = 'default';

  filterStatus: number = undefined;
  emptyMessage = 'Loading orders...';
  first: number = 0;
  loading: boolean = false;
  sortField: string = 'CreationTime';
  sortOrder: number = -1;
  displayDialog: boolean;
  statusOptions: any[] = [
    { value: '0', label: 'Order Received', color: 'green' },
    { value: '1', label: 'Accepted', color: 'green' },
    { value: '2', label: 'Preparing', color: '#ff8d60' },
    { value: '3', label: 'Prepared', color: '#0cc27e' },
    { value: '4', label: 'Delivering', color: '#1cbcd8' },
    { value: '5', label: 'Delivered', color: '#1cbcd8' },
  ];

  ordersCountPerPage = [
    { name: '20', value: 20 },
    { name: '60', value: 60 },
    { name: '100', value: 100 },
    { name: '200', value: 200 },
  ];

  layout: string;
  logoUrl: string = '';

  selectedOrderCounts = this.ordersCountPerPage[0];

  currentPageReportTemplate = '{first} - {last} of {totalRecords}';
  orderTotalCount: number;
  selectedOrder: any;
  userId = undefined;

  constructor(
    private wheelOrderServiceProxyService: WheelOrderService,
    private router: Router,
    private readonly accountService: AccountService
  ) {
     // this.userId = this.accountService.userValue.id;
     if (AppSession && AppSession.user) {
      this.userId = AppSession.user.userId;
    }

    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }

  ngOnInit(): void {
    if (!this.userId) {
      this.router.navigateByUrl('/');
    } else {
      this.getOrders();
    }
  }

  loadData(event): void {
    const requestPage = event.first / event.rows;

    const nextPage = requestPage > 0 ? requestPage + 1 : 0;

    if (nextPage < requestPage) {
      return;
    }

    this.getOrders();
  }

  getOrders() {
    if (this.userId) {
      this.emptyMessage = 'Loading orders...';
      this.loading = true;

      this.wheelOrderServiceProxyService
        .getWheelOrdersByUser(this.userId)
        .subscribe((result) => {
          let orderListResult = result.result;
          this.loading = false;
          this.orderList = orderListResult.items;
          this.orderTotalCount = orderListResult.totalCount;
          console.log(this.orderList);

          if (!this.orderList.length) {
            this.emptyMessage = 'No orders found.';
          }
        });
    }
  }

  getOrderStatus(status: any): any {
    return this.statusOptions.find((t) => t.value == status.toString());
  }

  selectOrder(event: Event, order: WheelOrderListDto): void {
    this.selectedOrder = order;
    this.displayDialog = true;
    event.preventDefault();
  }

  onDialogHide(): void {
    this.selectedOrder = null;
  }
}

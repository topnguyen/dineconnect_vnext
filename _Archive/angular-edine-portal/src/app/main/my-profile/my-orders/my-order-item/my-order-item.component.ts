import { Component, Input, OnInit } from '@angular/core';
import { WheelBillingInfo } from 'src/model/order.model';
import { WheelOrderListDto } from 'src/shared/service-proxies/service-proxies';

@Component({
  selector: 'my-order-item',
  templateUrl: './my-order-item.component.html',
  styleUrls: ['./my-order-item.component.scss']
})
export class MyOrderItemComponent implements OnInit {
  orderInfo: WheelOrderListDto;

  get order(): WheelOrderListDto {
    return this.orderInfo;
  }

  @Input()
  set order(value: WheelOrderListDto) {
    this.orderInfo = value;
    this.wheelBillingInfo = this.orderInfo.wheelBillingInfo;
    this.wheelServiceFees = JSON.parse(
      this.orderInfo.wheelBillingInfo.wheelServiceFees
    );
    this.deliveryFee = this.orderInfo.wheelBillingInfo.deliveryFee;
    this.itemList = this.orderInfo.order.menuItems;
  }

  headerLbl = 'Complete Order';
  notifyPayment = 'Your order has been received Successfully.';
  trackingLbl = 'Track Your Order';
  orderInfoLbl = 'Order Information';
  orderSummaryLbl = 'Order Summary';
  systemCurrency = '$';

  deliveryFee: number = 0;
  orderID = '';
  wheelBillingInfo: WheelBillingInfo;
  wheelServiceFees = [];
  itemList = [];

  elementType: 'url' | 'canvas' | 'img' = 'url';
  constructor() {}

  ngOnInit(): void {}

  getItemInfo(json: string): string {
    const item = JSON.parse(json);
    return item[0].name + ' - ' + item[0].portions.name;
  }
}

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyProfileRoutingModule } from './my-profile-routing.module';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { TopOrdersComponent } from './top-orders/top-orders.component';
import { OrderItemComponent } from './top-orders/order-item/order-item.component';
import { MyProfileComponent } from './my-profile.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { PanelModule } from 'primeng/panel';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { MyOrderItemComponent } from './my-orders/my-order-item/my-order-item.component';
import { MenuModule } from 'primeng/menu';
import { MainModule } from '../main.module';
import { CardModule } from 'primeng/card';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { CommonService } from 'src/shared/service/common.service';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    TopOrdersComponent,
    OrderItemComponent,
    MyProfileComponent,
    MyOrdersComponent,
    MyOrderItemComponent,
    ProfileInfoComponent,
  ],
  imports: [
    CommonModule,
    TabViewModule,
    MyProfileRoutingModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    RippleModule,
    PanelModule,
    MenuModule,
    MainModule,
    CardModule,
    RadioButtonModule,
    NgxIntlTelInputModule,
    DropdownModule
  ],
  providers: [CommonService]
})

export class MyProfileModule { }

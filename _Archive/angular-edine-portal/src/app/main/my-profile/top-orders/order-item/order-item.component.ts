import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'top-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit {

  @Input() listItems = [];

  @Input() selected: number;
  @Output() selectedChange = new EventEmitter<Number>();

  constructor() { }

  ngOnInit(): void {
  }

  onEditSelectItem(id) {
    this.selectedChange.emit(id);
  }

  onAddItems($event) {
    console.log('on add item');
  }

  onRemoveItems($event) {
    console.log('on remove item');
  }
}

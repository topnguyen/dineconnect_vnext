import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'top-orders',
  templateUrl: './top-orders.component.html'
})
export class TopOrdersComponent implements OnInit {

  listOrders = [
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Cold Coffee',
      price: '$0.00',
      quantity: 3,
      category: 'Coffee',
      id: 0
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Sprite',
      price: '$2.99',
      quantity: 2,
      category: 'Coffee',
      id: 1
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Lemonade',
      price: '$3.99',
      quantity: 1,
      category: 'Waffles',
      id: 2
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Hot Chocolate',
      price: '$3.99',
      quantity: 3,
      category: 'Waffles',
      id: 3
    }
  ];

  orderItemSelected = -1;

  constructor() { }

  ngOnInit(): void {
  }

}

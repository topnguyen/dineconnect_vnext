import { animate, animateChild, group, query, style, transition, trigger } from '@angular/animations';
import { AnimationRouteItems } from 'src/shared/constant';

export const slideInAnimation =
    trigger('routeAnimations', [

        transition(`${AnimationRouteItems.location} => ${AnimationRouteItems.orderType}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.orderType} => ${AnimationRouteItems.location}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '-100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.orderType} => ${AnimationRouteItems.menuItems}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.menuItems} => ${AnimationRouteItems.orderType}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '-100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`* => ${AnimationRouteItems.home}`, [
            style({ position: 'relative' }),
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),
            query(':enter', [
                style({ left: '-100%' })
            ]),
            query(':leave', animateChild(), { optional: true }),
            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '100%' }))
                ], { optional: true }),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),
            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.home} => *`, [
            style({ position: 'relative' }),
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),
            query(':enter', [
                style({ left: '100%' })
            ]),
            query(':leave', animateChild()),
            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),
            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.menuItems} => ${AnimationRouteItems.combo}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`${AnimationRouteItems.combo} => ${AnimationRouteItems.menuItems}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '-100%' })
            ]),

            query(':leave', animateChild()),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '100%' }))
                ]),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`* => ${AnimationRouteItems.login}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '100%' })
            ]),

            query(':leave', animateChild(), { optional: true }),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ], { optional: true }),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`* => ${AnimationRouteItems.signUp}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '100%' })
            ]),

            query(':leave', animateChild(), { optional: true }),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '-100%' }))
                ], { optional: true }),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),

        transition(`* => ${AnimationRouteItems.myProfile}`, [
            style({ position: 'relative' }),

            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),

            query(':enter', [
                style({ left: '-100%' })
            ]),

            query(':leave', animateChild(), { optional: true }),

            group([
                query(':leave', [
                    animate('330ms ease-out', style({ left: '100%' }))
                ], { optional: true }),
                query(':enter', [
                    animate('330ms ease-out', style({ left: '0%' }))
                ])
            ]),

            query(':enter', animateChild()),
        ]),
    ]);

import { MessageService } from 'primeng/api';
import { confirmPasswordValidators } from './../../../shared/custom-validation/account-custom-validation';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
  AbstractControl,
} from '@angular/forms';
import { CommonService } from 'src/shared/common';
import { RegexPatterns, StepName } from 'src/shared/constant';
import { emailCustomValidators } from 'src/shared/custom-validation';
import { ICreateOrUpdateUserInput, IUser } from 'src/model';
import { AccountService } from 'src/shared/service';
import { Router } from '@angular/router';
import { RegisterMemberInput } from 'src/shared/service-proxies/service-proxies';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  registerForm: FormGroup;
  formAccess: FormGroup;

  isHiddenPassword = true;
  isHiddenConfirmPassword = true;

  constructor(
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private messageService: MessageService,
    private router: Router
  ) {}

  get password() {
    return this.formAccess.controls.password;
  }

  get registry() {
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    this.createRegisterUserForm();
    this.commonService.$currentStepName.next(StepName.signUp);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.commonService.$userEmail.subscribe((email) => {
        if (email) {
          this.registerForm.patchValue({
            emailAddress: email,
          });
        }
      });
    }, 0);
  }

  createRegisterUserForm() {
    this.registerForm = this.formBuilder.group(
      {
        name: new FormControl('', [Validators.required]),
        surname: new FormControl('', [Validators.required]),
        emailAddress: new FormControl('', [
          Validators.required,
          emailCustomValidators(RegexPatterns.emailRegex),
        ]),
        phoneNumber: new FormControl('', [
          Validators.required,
          Validators.pattern('[0-9]+'),
          Validators.minLength(6),
          Validators.maxLength(15),
        ]),
        userName: new FormControl('', [Validators.required]),
        newPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      { validators: confirmPasswordValidators }
    );
  }

  getError(f: AbstractControl) {
    return f.errors && (f.touched || f.dirty);
  }

  registryUser() {
    this.registerForm.markAllAsTouched();
    this.registerForm.updateValueAndValidity();
    if (this.registerForm.valid) {
      const formValue = this.registerForm.getRawValue();
      let userCreate: IUser = {
        emailAddress: '',
        id: null,
        isActive: true,
        isLockoutEnabled: true,
        isTwoFactorEnabled: false,
        name: '',
        password: '',
        phoneNumber: null,
        shouldChangePasswordOnNextLogin: true,
        surname: '',
        userName: '',
      };
      userCreate = { ...userCreate, ...formValue };
      userCreate.password = formValue.newPassword;

      let model = new RegisterMemberInput();
      model.init(userCreate);

      this.accountService.registerCustomer(model).subscribe((res) => {
        let response = res as any;

        if (response.success && response.result != 0) {
          this.messageService.clear();
          this.messageService.add({
            severity: 'success',
            summary: 'Service Message',
            detail: 'Register account Successful.',
          });
          this.accountService.$isOpenAccountComponent.next(true);
          this.router.navigateByUrl('/sign-in');
        }
      });
    }
  }
}

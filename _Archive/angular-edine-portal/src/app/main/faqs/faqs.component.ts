import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AccountService, WheelSetupsService } from 'src/shared/service';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { FaqListDto } from 'src/shared/service-proxies/service-proxies';
import { CollapseQuestionsComponent } from '../collapse-questions/collapse-questions.component';
import { Router } from '@angular/router';
import { LOGO_URL } from 'src/shared/shared.const';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FAQsComponent implements OnInit {
  questions: FaqListDto[];
  logoUrl: string = '';
  userId = undefined;
  // @ViewChild('leftMenuRef', { static: false })
  leftSideMenuComponent: CollapseQuestionsComponent;
  constructor(
    private wheelSetupsService: WheelSetupsService,
    private readonly accountService: AccountService,
    private router: Router
  ) {
    this.userId = this.accountService.userValue.id;
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }

  ngOnInit(): void {
    this.getFaqs();
  }

  getFaqs() {
    this.wheelSetupsService.getFaqs().subscribe((resp) => {
      this.questions = resp.result.items.filter(
        (x) => x.isDisplayAtHomePage == true
      );
    });
  }
  onClickBack() {
    this.router.navigateByUrl('/');
  }
}

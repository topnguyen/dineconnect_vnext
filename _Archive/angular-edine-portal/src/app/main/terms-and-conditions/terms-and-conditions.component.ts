import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/shared/service';
import { WheelDynamicPageEditDto } from 'src/shared/service-proxies/service-proxies';
import { TermAndConditonService } from './term-and-conditon.service';
import { LOGO_URL } from 'src/shared/shared.const';
import { ThirdPartyService } from 'src/shared/service/third-party.service';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TermsAndConditionsComponent implements OnInit {
  termsAndConditions: string = '';
  logoUrl: string = '';
  userId = undefined;
  dynamicPage: any;
  constructor(
    private termAndConditonService: TermAndConditonService,
    private readonly accountService: AccountService,
    private router: Router
  ) {
    this.userId = this.accountService.userValue.id;
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }
  ngOnInit(): void {
    this.getTermAndConditionDynamicPage();
  }
  getTermAndConditionDynamicPage() {
    this.termAndConditonService.getDynamicPage(0).subscribe((res) => {
      if (res.success) {
        this.dynamicPage = res.result;
        console.log(this.dynamicPage);
        this.appendCss(this.dynamicPage.css);
      }
    });
  }
  onClickBack() {
    this.router.navigateByUrl('/');
  }
  appendCss(customData) {
    $(document).ready(function () {
      $('style').append(customData);
    });
  }
}

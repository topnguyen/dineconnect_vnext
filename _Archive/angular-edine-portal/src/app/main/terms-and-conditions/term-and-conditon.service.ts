import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPageResultList } from 'src/model';
import { HttpClientService, HttpOptions, TokenType } from 'src/shared/common';
import { WheelDynamicPageEditDto } from 'src/shared/service-proxies/service-proxies';

@Injectable({
  providedIn: 'root',
})
export class TermAndConditonService {
  constructor(
    private http: HttpClientService,
    private httpClient: HttpClient
  ) {}

  getDynamicPage(typeId: any): Observable<IPageResultList<WheelDynamicPageEditDto>> {
    let url_ = '/api/services/app/WheelDynamicPages/GetDynamicPageTermOrPrivacy?';
    if (typeId !== undefined)
      url_ += 'key=' + encodeURIComponent('' + typeId) + '&';
    url_ = url_.replace(/[?&]$/, '');
    const options: HttpOptions = {
      url: url_,
      typeToken: TokenType.META_TOKEN,
    };
    return this.http.get(options);
  }
}

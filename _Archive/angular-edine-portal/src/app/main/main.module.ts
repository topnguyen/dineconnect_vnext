import { InputTextModule } from 'primeng/inputtext';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { ToastModule } from 'primeng/toast';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';
import { AccountModule } from './account/account.module';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TooltipModule } from 'primeng/tooltip';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CheckboxModule } from 'primeng/checkbox';
import { HeaderUiComponent } from './header-ui/header-ui.component';
import { FooterUiComponent } from './footer-ui/footer-ui.component';
import { MainContentComponent } from './main-content/main-content.component';
import { LeftSideMenuComponent } from './left-side-menu/left-side-menu.component';
import { OrderContentComponent } from './order-content/order-content.component';
import { MenuItemUiComponent } from './menu-item-ui/menu-item-ui.component';
import { MyCategoryFilterPipe } from '../../shared/custom-pipe';
import { MainRoutingModule } from './main.routing';
import { AccountComponent } from './account/account.component';
import { AlertComponent } from './alert/alert.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { TabViewModule } from 'primeng/tabview';
import { RadioButtonModule } from 'primeng/radiobutton';
import { LocationComponent } from './location/location.component';
import { DepartmentComponent } from './department/department.component';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CardModule } from 'primeng/card';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { DepartmentCardComponent } from './department/department-card/department-card.component';
import { RatingModule } from 'primeng/rating';
import { ItemComponent } from './menu-item-ui/item/item.component';
import { ComboComponent } from './combo/combo.component';
import { ComponentsModule } from './components/components.module';
import { MyOrderComponent } from './my-order/my-order.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AutofocusDirective } from 'src/shared/directives';
import { MenubarModule } from 'primeng/menubar';
import { GuestInfoComponent } from './guest-info/guest-info.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MenuComponent } from './menu/menu.component';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { MenuModule } from 'primeng/menu';
import { MenuContentComponent } from './menu/components/menu-content/menu-content.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { SectionChildrenComponent } from './left-side-menu/section-children/section-children.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { VerifyComponent } from './verify-code/verify-code.component';
import { LanguageComponent } from './language/language.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { PaymentsComponent } from './payments/payments.component';
import { PaymentsInformationComponent } from './payments-information/payments-information.component';
import { TableListComponent } from './table-list/table-list.component';
import { ComboCardComponent } from './combo-card/combo-card.component';
import { InputPostalCodeComponent } from './input-postal-code/input-postal-code.component';
import { SelectCollapseComponent } from 'src/shared/components/select-collapse/select-collapse.component';
import { CustomerQueueComponent } from './customer-queue/customer-queue.component';
import { DatePipe } from '@angular/common';
import { QueueDashboardComponent } from './customer-queue/queue-dashboard/queue-dashboard.component';
import { ProductImageDirective } from 'src/shared/directives';
import { SigninService } from './sign-in/sign-in.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { ComboItemComponent } from './menu-item-ui/combo-item/combo-item.component';
import { DialogModule } from 'primeng/dialog';
import { OrderDetailModalComponent } from './my-order/order-detail-modal/order-detail-modal.component';
import { MoreDetailNutritionInfoModalComponent } from './menu-item-ui/item/more-detail-nutrition-info-modal/more-detail-nutrition-info-modal.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { FAQsComponent } from './faqs/faqs.component';
import { CollapseQuestionsComponent } from './collapse-questions/collapse-questions.component';
import { SafePipe } from 'src/shared/pipes/safe-html.pipe';
import { AccordionModule } from 'primeng/accordion';
import { PanelModule } from 'primeng/panel';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ScriptLoaderService } from 'src/shared/common/common-service/script-loader.service';
import { SelectDateOrderFutureComponent } from './select-date-order-future/select-date-order-future.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LoginService } from './sign-in/login.service';

@NgModule({
  exports: [
    HeaderUiComponent,
    FooterUiComponent,
    LocationComponent,
    InputPostalCodeComponent,
    MainContentComponent,
    LeftSideMenuComponent,
    OrderContentComponent,
    MenuItemUiComponent,
    MenuComponent,
    MenuContentComponent,
    SectionChildrenComponent,
    AccountComponent,
    AlertComponent,
    DepartmentComponent,
    ResetPasswordComponent,
    WelcomeComponent,
    LanguageComponent,
    VerifyComponent,
    ComboComponent,
    PaymentsInformationComponent,
    TableListComponent,
    ItemCardComponent,
    ComboCardComponent,
  ],
  declarations: [
    SelectCollapseComponent,
    InputPostalCodeComponent,
    PaymentsInformationComponent,
    TableListComponent,
    PaymentsComponent,
    WelcomeComponent,
    LanguageComponent,
    VerifyComponent,
    LocationComponent,
    HeaderUiComponent,
    FooterUiComponent,
    MainContentComponent,
    LeftSideMenuComponent,
    OrderContentComponent,
    MenuItemUiComponent,
    MenuContentComponent,
    SectionChildrenComponent,
    MenuComponent,
    OrderStatusComponent,
    MyCategoryFilterPipe,
    AccountComponent,
    AlertComponent,
    ResetPasswordComponent,
    CheckoutComponent,
    DepartmentComponent,
    StarRatingComponent,
    DepartmentCardComponent,
    ItemComponent,
    ComboComponent,
    MyOrderComponent,
    SignUpComponent,
    SignInComponent,
    AutofocusDirective,
    GuestInfoComponent,
    ItemCardComponent,
    ComboCardComponent,
    CustomerQueueComponent,
    QueueDashboardComponent,
    ProductImageDirective,
    ComboItemComponent,
    MoreDetailNutritionInfoModalComponent,
    OrderDetailModalComponent,
    TermsAndConditionsComponent,
    FAQsComponent,
    CollapseQuestionsComponent,
    SafePipe,
    PrivacyPolicyComponent,
    SelectDateOrderFutureComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MenuModule,
    OAuthModule.forRoot(),
    SweetAlert2Module.forRoot(),
    CalendarModule,
    CommonModule,
    MainRoutingModule,
    DropdownModule,
    InputTextModule,
    BreadcrumbModule,
    NgxIntlTelInputModule,
    TooltipModule,
    NgxQRCodeModule,
    AutoCompleteModule,
    VirtualScrollerModule,
    AccountModule,
    CheckboxModule,
    TabViewModule,
    RadioButtonModule,
    ButtonModule,
    RippleModule,
    ToastModule,
    CardModule,
    RatingModule,
    ComponentsModule,
    MenubarModule,
    NgxSpinnerModule,
    DialogModule,
    AccordionModule,
    PanelModule,
    InfiniteScrollModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    DatePipe,
    DecimalPipe,
    SigninService,
    ScriptLoaderService,
    LoginService,
  ],
})
export class MainModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItemService } from 'src/shared/service';
import { Location } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import {
  OrderModel,
  PurcharsedOrder,
  WheelBillingInfo,
} from 'src/model/order.model';
import { IPageResult } from 'src/model';
import { wheelDataSetups } from 'src/shared/constant';
import {
  DATE_TIME_DELIVERY,
  POSTAL_CODE,
  POSTAL_CODE_ONEMAP,
  SELECTED_DEPARTMENT,
  SELECTED_FEE,
  SELECTED_LOCATION,
  SELECTED_TABLE,
} from 'src/shared/shared.const';

@Component({
  selector: 'dine-payments-information',
  templateUrl: './payments-information.component.html',
  styleUrls: ['./payments-information.component.scss'],
})
export class PaymentsInformationComponent implements OnInit {
  headerLbl = 'Complete Order';
  notifyPayment = 'Your order has been received Successfully.';
  trackingLbl = 'Track Your Order';
  orderInfoLbl = 'Order Information';
  orderSummaryLbl = 'Order Summary';

  deliveryFee: number = 0;
  taxAmount: number = 0;
  orderID = '';
  orderInfo: any;
  wheelBillingInfo: WheelBillingInfo;
  wheelServiceFees = [];
  itemList = [];

  systemCurrency = wheelDataSetups.currency;

  elementType: 'url' | 'canvas' | 'img' = 'url';
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  isDeliveryType = false;
  isOrderPickupFututer = false;
  isPickupPlateNumber: boolean = false;
  isDineIn = false;

  constructor(
    private location: Location,
    private router: Router,
    private menuItemService: MenuItemService,
    private activeRouter: ActivatedRoute
  ) {
    this.activeRouter.params.subscribe((params) => {
      this.getGetWheelOrder(params.id);
      this.orderID = params.id;
    });
  }

  ngOnInit(): void { }

  onClickBack(): void {
    this.location.back();
  }

  onClickContinue(): void {
    this.router.navigateByUrl(`/`);
  }

  getItemInfo(json: string): string {
    const item = JSON.parse(json);
    return item[0].name + ' - ' + item[0].portions.name;
  }

  private getGetWheelOrder(id: string): void {
    this.menuItemService
      .getWheelOrder(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((response: IPageResult<any>) => {
        this.orderInfo = response.result;
        this.wheelBillingInfo = this.orderInfo.wheelBillingInfo;
        this.wheelServiceFees = JSON.parse(this.orderInfo.wheelBillingInfo.wheelServiceFees);
        this.deliveryFee = this.orderInfo.wheelBillingInfo.deliveryFee;
        this.taxAmount = this.orderInfo.wheelBillingInfo.tax;
        this.itemList = this.orderInfo.order.orderItems;
        this.isDeliveryType = this.orderInfo?.order?.wheelOrderType === 1;
        this.isPickupPlateNumber = this.orderInfo?.order?.wheelOrderType === 3;
        this.isDineIn = this.orderInfo?.order?.wheelOrderType === 0;
        this.isOrderPickupFututer =
          this.orderInfo?.order?.wheelOrderType != 1 &&
          this.orderInfo?.order.deliveryTime != 'Not Available';
      });
  }
}

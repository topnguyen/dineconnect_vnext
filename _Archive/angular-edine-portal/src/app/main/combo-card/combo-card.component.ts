import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/shared/common';
import { CONSTANT, OrderSetting, wheelDataSetups } from 'src/shared/constant';
import { AccountService, MenuItemService } from 'src/shared/service';
import { OrderSettingDto } from 'src/shared/service-proxies/service-proxies';
import { MenuItemTagsConst } from 'src/shared/shared.const';
import { AppSession } from 'src/shared/constant';

@Component({
  selector: 'app-combo-card',
  templateUrl: './combo-card.component.html',
  styleUrls: ['./combo-card.component.scss'],
})
export class ComboCardComponent implements OnInit {
  @Input() itemData: any;
  @Input() isListStyle = true;
  @Output() choseItem: EventEmitter<any> = new EventEmitter();
  systemCurrency = wheelDataSetups.currency;
  @Input() sizeImg = new OrderSettingDto();
  @Input() colSetting = 'col-md-3';
  userId = undefined;
  displayDialog: boolean;

  constructor(private commonService: CommonService,
    private menuItemService: MenuItemService,
    public accountService: AccountService,
    private router: Router,
  ) {
    if (AppSession && AppSession.user) {
      this.userId = AppSession.user.userId;
    }
  }

  menuItemTags = MenuItemTagsConst;
  menuItemTagSelected: any[] = [];
  colImage = 'col-md-6';
  colDescription = 'col-md-6';

  ngOnInit(): void {
    this.menuItemTagSelected = this.menuItemTags.filter((el) => {
      if (this.itemData.tags != '' && this.itemData.tags != null) {
        let index = this.itemData.tags.includes(el.value);
        if (index) return el;
      }
    });
    this.calculatorColImage(this.colSetting);
  }
  calculatorColImage(input) {
    if (input == 'col-md-12' || input == 'col-md-6') {
      this.colImage = 'col-md-3';
      this.colDescription = 'col-md-9';
    } else if (input == 'col-md-4' || input == 'col-md-3') {
      this.colImage = 'col-md-12';
      this.colDescription = 'col-md-12';
    }
  }

  addItem(): void {
    if (OrderSetting) {
      if (!this.userId && OrderSetting.signUp) {
        this.router.navigateByUrl('/sign-in');
        return;
      }
      this.choseItem.emit(this.itemData);
    } else {
      this.choseItem.emit(this.itemData);
    }
  }

  getMarkItem() { }

  displayVideo(event: Event) {
    this.displayDialog = true;
    event.preventDefault();
  }

  onDialogHide(): void {
    // this.selectedOrder = null;
  }
}

import { map, takeUntil } from 'rxjs/operators';
import {
  WheelPaymentMethodsServiceProxy,
  AddonSettingsServiceProxy,
  AccountServiceProxy,
  TiffinOrderServiceProxy,
} from './../../../shared/service-proxies/service-proxies';
import {
  IOrderCheckout,
  CheckoutStatus,
  OrderPaymentMethod,
} from './../../../model/order-checkout.model';
import { environment } from 'src/environments/environment';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ThirdPartyService } from './../../../shared/service/third-party.service';
import { CommonService, LocalStorageService } from 'src/shared/common';
import { ISimpleLocation, IPostalCodeModel } from 'src/model';
import { OrderContentComponent } from './../order-content/order-content.component';
import {
  FormBuilder,
  FormControl,
  Validators,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { MessageService, MenuItem, MegaMenuItem } from 'primeng/api';
import { PAYMENT_METHOD, StepName, wheelDataSetups } from 'src/shared/constant';
import * as moment from 'moment';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { Router } from '@angular/router';
import { MenuItemService, AccountService } from 'src/shared/service';
import Swal from 'sweetalert2';
import { CustomMenuItemData } from 'src/model/custom-menu-item.model';
import {
  SELECTED_DEPARTMENT,
  SELECTED_FEE,
  SELECTED_LOCATION,
} from 'src/shared/shared.const';
declare var OmiseCard: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  @ViewChild(OrderContentComponent)
  orderContentComponent: OrderContentComponent;
  isOpenPopUpAddress: boolean;

  addressForm: FormGroup;
  addressDisplay: any;
  orderList: CustomMenuItemData[] = [];

  deliveryLocation: ISimpleLocation;
  postalCodeError: string;
  postalCode: IPostalCodeModel;
  confirmStep = false;
  tempBuildingCache = '';
  tempStreetCache = '';
  timeDelivery = 'ASAP';
  isASAP: boolean = true;
  timeValueDefault: Date = new Date();
  totalPrice: number = 0;
  subTotalPrice: number = 0;
  totalAtmountsIem: number = 0;
  PAYMENT_METHOD = PAYMENT_METHOD;
  systemCurrency = wheelDataSetups.currency;

  isDeliveryType = false;
  isAddressValid = false;
  isMarkerValid = false;
  pendingCheckout = false;
  checkoutSuccessful = false;
  // function enable:
  enableDiscount = false;
  deliveredBy = null;
  otherPaymentMethod = false;
  isOpenConfirmCheckout = false;
  selectedCityCode: any = null;
  selectedLocation: any = null;
  selectedDepartment: any = null;
  items: MegaMenuItem[];
  wheelServiceFees = [];
  markers: google.maps.Marker[] = [];
  formGroup: FormGroup;
  lastMarker: any;
  listAddress: Array<{ name: string; address: string; value: any }> = [];
  errorCheckOut = {
    building: {
      required: 'Please enter building.',
    },
    street: {
      required: 'Please enter street.',
      minLength: 'Please enter street. (At least 3 character)',
    },
  };
  paymentString: Array<{
    name: string;
    value: any;
    detail?: string;
    icon?: string;
    isActive: boolean;
  }> = [
      {
        name: 'Omise',
        detail: '',
        icon: 'assets/image/omise.png',
        value: 'omise',
        isActive: false,
      },
      {
        name: 'Cash on delivery (COD)',
        detail: '',
        icon: '',
        value: 'cod',
        isActive: false,
      },
    ];
  omisePaymentGatewayPublicKey = '';
  omiseLabel = '';
  omiseConfig = '';
  message: MessageService;
  readonly GOOGLE_MAP_KEY = 'AIzaSyCbrgvMPgVlrdwHg7IfHe064oUKEhxma3M';
  map: google.maps.Map;
  service: google.maps.places.PlacesService;
  infowindow: google.maps.InfoWindow;
  // end
  constructor(
    private formBuilder: FormBuilder,
    private menuItemService: MenuItemService,
    private localStorageService: LocalStorageService,
    private thirdPartyService: ThirdPartyService,
    private messageService: MessageService,
    private checkOutService: CheckOutService,
    private readonly accountService: AccountService,
    private accountServiceProxy: AccountServiceProxy,
    private commonService: CommonService,
    private wheelPaymentMethodsServiceProxy: WheelPaymentMethodsServiceProxy,
    private addonSettingsServiceProxy: AddonSettingsServiceProxy,
    private orderService: TiffinOrderServiceProxy,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formGroup = this.createAddressForm();
    this.selectedLocation = JSON.parse(
      window.localStorage.getItem(SELECTED_LOCATION)
    );
    this.selectedDepartment = JSON.parse(
      window.localStorage.getItem(SELECTED_DEPARTMENT)
    );
    this.isDeliveryType = this.selectedDepartment.wheelOrderType === 1;

    this.wheelServiceFees = JSON.parse(
      window.localStorage.getItem(SELECTED_FEE)
    );
    if (!this.localStorageService.getOrderItemsOnLocalStorage().length) {
      if (this.selectedLocation && this.selectedDepartment) {
        this.router.navigateByUrl(
          `location/${this.selectedLocation.id}/type/${this.selectedDepartment.id}`
        );
      } else if (!this.selectedLocation) {
        this.router.navigateByUrl(`/`);
      } else {
        this.router.navigateByUrl(`location/${this.selectedLocation.id}`);
      }
    }
    this.addressDisplay = { ...this.localStorageService.getAddress() };
    if (this.addressDisplay && this.addressDisplay.postalCode) {
      this.postalCode = this.addressDisplay.postalCode;
    }
    this.items = [
      {
        label: 'ASAP',
        icon: 'pi pi-clock',
        command: this.setTime.bind(this, true),
      },
      {
        label: 'Schedule Order',
        icon: 'pi pi-calendar',
        expanded: true,
        command: this.setTime.bind(this, false),
      },
    ];

    this.formGroup.controls.deliveryLocation.setValue('');
    this.localStorageService.$deliveryLocation.subscribe((value) => {
      if (value) {
        // this.deliveryLocation = value;
      } else {
        this.router.navigate(['']);
      }
    });

    this.commonService.$currentStepName.next(StepName.payment);
  }

  get timeDeliveryControl(): AbstractControl {
    return this.formGroup.controls.timeDelivery;
  }

  setTime(ASAP: boolean): void {
    this.isASAP = ASAP;
    this.timeDelivery = ASAP ? 'ASAP' : 'Schedule Order';
  }

  ngAfterViewInit() {
    this.init();
    this.localStorageService.$orderListLocalStorage.subscribe(() =>
      this.loadItemsOrder()
    );
    this.cdr.detectChanges();
    if (this.isDeliveryType) {
      this.initMap();
    }
  }

  private init() {
    this.wheelPaymentMethodsServiceProxy
      .getWheelPaymentMethodForEdit('Payments.Omise')
      .subscribe((result) => {
        this.omisePaymentGatewayPublicKey = result.dynamicFieldData.publishKey;
        this.omiseConfig = result.dynamicFieldData;
      });
  }

  private initPayment() {
    this.omiseLabel = 'OnlinePayment';
    const paymentType = this.formGroup.controls['paymentType'].value;
    if (this.omisePaymentGatewayPublicKey && paymentType === 'omise') {
      this.initOmiseElements();
    }
    if (paymentType === 'cod') {
      this.submitPayment('cod', null);
    }

    if (this.omisePaymentGatewayPublicKey) {
      this.omiseLabel = 'OnmisePayment';
    }
  }

  getServiceCharge(name) {
    this.addonSettingsServiceProxy
      .getServiceCharge(name)
      .subscribe((result) => {
        this.wheelServiceFees = result;

        result.map((item) => {
          this.totalPrice =
            this.totalPrice + (this.totalPrice * item.percentage) / 100;
        });
      });
  }

  private initOmiseElements() {
    OmiseCard.configure({
      publicKey: this.omisePaymentGatewayPublicKey,
    });

    if (!this.isValidBillingInformation()) {
      return;
    }

    let self = this;
    OmiseCard.open({
      amount: this.totalPrice * 100,
      currency: wheelDataSetups.currency,
      defaultPaymentMethod: 'credit_card',
      onCreateTokenSuccess: (nonce) => {
        if (nonce.startsWith('tokn_')) {
          self.submitPayment('omise', nonce);
        }
      },
    });
  }
  private isValidBillingInformation() {
    return true;
  }

  private submitPayment(paymentOption: string, token: string) {
    const temp: any = {
      purcharsedOrderList: [
        {
          orderDate: new Date(),
          amount: this.subTotalPrice,
          quantity: this.totalAtmountsIem,
          address: 'string',
          menuItemsDynamic: JSON.stringify(this.orderList),
          wheelOrderType: this.selectedDepartment.WheelOrderType,
        },
      ],
      wheelTicket: {
        orderDate: new Date(),
        wheelOrderType: this.selectedDepartment.WheelOrderType,
      },
      billingInfo: {
        name: this.accountService.userValue.userName,
        emailAddress: this.accountService.userValue.emailAddress,
        address1: this.address1.value,
        address2: this.address2.value,
        address3: 'string',
        city: this.city.value,
        country: this.state.value,
        lat: this.isDeliveryType ? this.lastMarker.lat : null,
        long: this.isDeliveryType ? this.lastMarker.lng : null,
        cityId: 0,
        countryId: 0,
        phoneNumber: this.accountService.userValue.phoneNumber,
        phonePrefix: this.accountService.userValue.phonePrefix,
        postcalCode: this.postCode.value,
        zone: 0,
      },
      paymentOption: paymentOption === 'cod' ? '' : paymentOption,
      paidAmount: this.totalPrice,
      callbackUrl: location.origin + '/order-status',
      omiseToken: paymentOption === 'omise' ? token : '',
      source: 'string',
      adyenEncryptedCardNumber: 'string',
      adyenEncryptedExpiryMonth: 'string',
      adyenEncryptedExpiryYear: 'string',
      adyenEncryptedSecurityCode: 'string',
      paypalOrderId: 'string',
      stripeToken: 'string',
      googlePayToken: 'string',
    };

    this.checkOutService.createPaymentForOrder(temp).subscribe(
      (res) => {
        this.localStorageService.storeOrderItemsOnLocalStorage([]);
        this.router.navigateByUrl('/order-status/' + res.result.invoiceNo);
        this.localStorageService.clearOrderList();
        this.localStorageService.removeCartItem();
      },
      () => {
        Swal.fire('Payment', `Payment Fail!`, 'info');
      }
    );
  }

  countSubTotal() {
    this.subTotalPrice = 0;
    let totalPrice = 0;

    this.orderList.map((item) => {
      totalPrice += item.totalPricePerItem;
    });
    return totalPrice;
  }
  countTotal() {
    let totalPrice = this.subTotalPrice;

    this.wheelServiceFees.map((item) => {
      totalPrice += item.feeType
        ? item.value
        : this.subTotalPrice * (item.value / 100);
    });
    return totalPrice;
  }

  getNoteValue() {
    const orderNotes = this.orderContentComponent.getNoteValue();
    return orderNotes;
  }
  getTotalPrice() {
    this.totalPrice = this.orderContentComponent.getTotalPrice();
  }
  
  getTotalAmountsItem() {
    this.totalAtmountsIem = this.orderContentComponent.getTotalAmountsItem();
  }

  loadItemsOrder() {
    this.menuItemService.orderList =
      this.localStorageService.getOrderItemsOnLocalStorage();
    this.orderList = [...this.menuItemService.orderList];
    this.subTotalPrice = this.countSubTotal();
    this.totalPrice = this.countTotal();
  }

  decItems(item: CustomMenuItemData) {
    //this.menuItemService.decItems(item);
  }

  getOrderList() {
    return this.orderContentComponent.orderList;
  }

  addNewAddress(mode: string) {
    this.isOpenPopUpAddress = true;
    if (mode == 'edit') {
      this.formGroup = this.createAddressForm(this.addressDisplay);
      return;
    }
    if (mode == 'add') {
      this.formGroup = this.createAddressForm(this.addressDisplay);
      return;
    }
  }
  closeAddressPopup() {
    this.isOpenPopUpAddress = false;
  }

  closeConfirmPopup() {
    this.isOpenConfirmCheckout = false;
  }

  continueStepPayment(confirmStep: boolean) {
    this.confirmStep = confirmStep;
    this.checkOutByOmise();
  }
  
  createAddressForm(model = null) {
    return this.formBuilder.group({
      deliveryLocation: '',
      deliveryAddress: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      postCode: '',
      paymentType: '',
      timeDelivery: [new Date()],
    });
  }

  get addressFormControl() {
    return this.formGroup.controls;
  }
  get deliveryLocationControl() {
    return this.formGroup.controls['deliveryLocation'];
  }
  get deliveryAddressControl() {
    return this.formGroup.controls['deliveryAddress'];
  }
  get address1() {
    return this.formGroup.controls['address1'];
  }
  get address2() {
    return this.formGroup.controls['address2'];
  }
  get city() {
    return this.formGroup.controls['city'];
  }
  get state() {
    return this.formGroup.controls['state'];
  }
  get postCode() {
    return this.formGroup.controls['postCode'];
  }

  getError(f: AbstractControl) {
    return f.errors && (f.touched || f.dirty);
  }

  searchPostalcode() {
    this.formGroup.markAllAsTouched();
    this.formGroup.updateValueAndValidity();

    if (!this.formGroup.valid) {
      return;
    }

    const { building, street, flatno, landmark } = this.formGroup.getRawValue();
    const deliveryLocation = this.deliveryLocation?.id;
    if (this.tempBuildingCache == building && this.tempStreetCache == street) {
      this.addressDisplay.flatno = flatno;
      this.addressDisplay.landmark = landmark;
      this.addNewAddressSuccess();
      return;
    }

    // this.thirdPartyService.searchPostalCode(building, street).subscribe(
    //   (response) => {
    //     if (response.ErrorCode != 1) {
    //       this.postalCodeError = response.ErrorDetails;
    //     } else {
    //       this.tempBuildingCache = building;
    //       this.tempStreetCache = street;
    //       this.postalCode = response.Postcodes[0];
    //       this.addressDisplay = {
    //         postalCode: this.postalCode,
    //         deliveryLocation,
    //         building,
    //         street,
    //         flatno,
    //         landmark,
    //       };
    //       this.addNewAddressSuccess();
    //     }
    //   },
    //   () => (this.postalCodeError = 'Some errors occurred')
    // );
  }

  addNewAddressSuccess() {
    this.messageService.clear();
    this.messageService.add({
      severity: 'success',
      summary: 'Service Message',
      detail: 'Add new Address successful',
    });

    this.isOpenPopUpAddress = false;
    this.localStorageService.saveAddress(this.addressDisplay);
  }

  confirmOrder(paymentMethod: PAYMENT_METHOD) {
    if (paymentMethod == PAYMENT_METHOD.CASH_ON_DELIVERY) {
      return;
    }
    if (paymentMethod == PAYMENT_METHOD.CARD_ON_DELIVERY) {
      return;
    }
    if (paymentMethod == PAYMENT_METHOD.CREDIT_CARD) {
      return;
    }
  }

  onClickConfirm(): void {
    this.checkoutSuccessful = true;
    this.isOpenConfirmCheckout = false;
    this.initPayment();
  }

  onClickCheckout(): void {
    this.isAddressValid = this.isDeliveryType
      ? this.address1.value || this.address2.value
      : true;
    this.isMarkerValid = this.isDeliveryType
      ? this.lastMarker && this.lastMarker.lat && this.lastMarker.lng
      : true;
    this.isOpenConfirmCheckout = true;
  }

  checkOutByOmise() {
    OmiseCard.configure({
      publicKey: environment.OMISE_PUBLIC_KEY,
    });

    var button = document.querySelector('#checkoutButton');
    var form = document.querySelector('#checkoutForm') as any;

    var ratio = environment.RATIO_CURRENCY;

    button.addEventListener('click', (event) => {
      event.preventDefault();
      OmiseCard.open({
        amount: this.totalPrice * ratio,
        currency: this.systemCurrency,
        defaultPaymentMethod: 'credit_card',
        onCreateTokenSuccess: (validToken) => {
          if (validToken.startsWith('tokn_')) {
            this.pendingCheckout = true;
            // form.omiseToken.value = validToken;

            //prepare check out
            const orderNotes = this.getNoteValue();
            const { postalCode, building, street, flatno, landmark } =
              this.addressDisplay;
            const deliveryLocationId = this.deliveryLocation.id;
            const orderList = this.getOrderList();
            // call api for save all Details and charge money
            const checkoutDetail: IOrderCheckout = {
              orderNotes,
              flatNo: flatno,
              landmark,
              building,
              street,
              postalCode: postalCode.Postcode,
              deliveryLocationId,
              creationTime: moment(new Date()),
              amount: this.totalPrice,
              orderDetails: JSON.stringify(orderList),
              checkoutStatus: CheckoutStatus.Pending,
              orderPaymentMethod: OrderPaymentMethod.CREDIT_CARD,
              chargedToken: validToken,
              currency: this.systemCurrency,
              paymentOption: environment.paymentOption,
            };

            this.checkOutService
              .chargedOrder(checkoutDetail)
              .subscribe((res: any) => {
                if (res.success) {
                  this.pendingCheckout = false;
                  this.checkoutSuccessful = true;
                  this.messageService.clear();
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Service Message',
                    detail: 'Check out successful',
                  });

                  this.localStorageService.clearOrderList();
                  this.localStorageService.removeCartItem();
                  this.router.navigate(['']);
                }
              });
          } else {
            // form.omiseSource.value = validToken;
          }
        },
      });
    });
  }

  initMap(): void {
    // The location of Uluru
    const uluru = { lat: 1.354916, lng: 103.867728 };
    // The map, centered at Uluru
    this.map = new google.maps.Map(
      document.getElementById('map') as HTMLElement,
      {
        zoom: 4,
        center: uluru,
      }
    );

    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.map,
    });
    this.markers.push(marker);
  }

  searchPlace(): void {
    if (this.deliveryLocationControl.value.toString().trim().length < 1) {
      return;
    }
    let service: google.maps.places.PlacesService;
    const uluru = { lat: 1.354916, lng: 103.867728 };

    this.map = new google.maps.Map(
      document.getElementById('map') as HTMLElement,
      {
        center: uluru,
        zoom: 4,
      }
    );
    const marker = new google.maps.Marker({
      position: uluru,
      map: this.map,
    });
    this.markers.push(marker);
    const request = {
      query: this.deliveryLocationControl.value,
      fields: ['name', 'geometry'],
    };

    service = new google.maps.places.PlacesService(this.map);
    service.textSearch(
      request,
      (
        results: google.maps.places.PlaceResult[],
        status: google.maps.places.PlacesServiceStatus
      ) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          this.listAddress = [];
          if (results.length > 9) {
            results = results.slice(0, 10);
          }
          results.forEach((item) => {
            this.listAddress.push({
              name: item.name,
              address: item.formatted_address,
              value: item,
            });
          });
        }
      }
    );
  }
  onChooseDeliveryPlace(event) {
    this.checkOutService
      .searchLocation(event.value.formatted_address)
      .subscribe(
        (res) => {
          const addressValue = res.results[0];
          this.formGroup.patchValue({
            address1: addressValue.formatted_address,
            city: this.getComponentByTypes(
              addressValue,
              'administrative_area_level_1'
            )
              ? this.getComponentByTypes(
                addressValue,
                'administrative_area_level_1'
              )?.long_name
              : this.getComponentByTypes(addressValue, 'locality')?.long_name,
            state: this.getComponentByTypes(addressValue, 'country')?.long_name,
            postCode: this.getComponentByTypes(addressValue, 'postal_code')
              ?.long_name,
          });
          this.createMarker(
            addressValue.geometry as google.maps.places.PlaceGeometry,
            addressValue.formatted_address
          );
        },
        (err) => {
          console.log(err);
        }
      );
  }
  getComponentByTypes(result: any, type: string): any {
    return result.address_components.find((data) => data.types.includes(type));
  }
  createMarker(geometry: google.maps.places.PlaceGeometry, name: string): void {
    this.clearMarkers();
    const marker = new google.maps.Marker({
      map: this.map,
      position: geometry.location,
    });
    this.markers.push(marker);
    this.lastMarker = geometry.location;

    this.map.setCenter(marker.getPosition());
    this.map.setZoom(15);
  }

  onChangeTypePayment(type: {
    name: string;
    value: any;
    detail?: string;
    icon?: string;
    isActive: boolean;
  }): void {
    this.paymentString.forEach((paymentItem) => {
      if (paymentItem.value === type.value) {
        paymentItem.isActive = true;
      } else {
        paymentItem.isActive = false;
      }
    });
  }
  onCheckLocation(): void {
    let service: google.maps.places.PlacesService;
    let location = `${this.address1.value.trim()},${this.address2.value.trim()},${this.city.value.trim()},${this.state.value.trim()},${this.postCode.value.trim()}`;
    location = location.replace(/, null/g, '');
    location = location.replace(/&/g, '%26');
    location = location.replace(/ /g, '+');
    this.checkOutService.searchLocation(location).subscribe(
      (res) => {
        const addressValue = res.results[0];
        this.formGroup.patchValue({
          address1: addressValue.formatted_address,
          city: this.getComponentByTypes(
            addressValue,
            'administrative_area_level_1'
          )
            ? this.getComponentByTypes(
              addressValue,
              'administrative_area_level_1'
            )?.long_name
            : this.getComponentByTypes(addressValue, 'locality')?.long_name,
          state: this.getComponentByTypes(addressValue, 'country')?.long_name,
          postCode: this.getComponentByTypes(addressValue, 'postal_code')
            ?.long_name,
        });
        this.createMarker(
          addressValue.geometry as google.maps.places.PlaceGeometry,
          addressValue.formatted_address
        );
      },
      (err) => {
        console.log(err);
      }
    );
  }
  clearMarkers() {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
  }
}

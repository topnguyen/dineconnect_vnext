import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { PAGE, StepName } from 'src/shared/constant';
import { LOGO_URL, SCAN_QR_CODE } from 'src/shared/shared.const';
import { NgxSpinnerService } from 'ngx-spinner';
import { SELECTED_LOCATION } from 'src/shared/shared.const';
import { SELECTED_DEPARTMENT } from 'src/shared/shared.const';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { Page } from 'src/shared/service-proxies/service-proxies';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss'],
})
export class LanguageComponent implements OnInit, OnDestroy {
  logoUrl = '';

  locations = [];
  selectedLanguage = undefined;
  themeSettings: any;
  isApiCalling: boolean = true;

  languages: Array<{ code: string; icon: string; name: string }> = [
    {
      code: 'EN',
      icon: 'assets/image/en-icon.png',
      name: 'ENGLISH',
    },
  ];

  constructor(
    private router: Router,
    private wheelOrderService: WheelOrderService,
    private pl: PlatformLocation
  ) {
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }

  ngOnInit(): void {
    if (!window.localStorage.getItem('LANGUAGE_DISPLAY')) {
      if (this.languages.length === 1) {
        this.onChooseLanguage(this.languages[0].code);
      }
    } else {
      this.onClickBack();
    }
  }

  ngOnDestroy(): void {}
  onClickLanguage(): void {
    window.localStorage.setItem('LANGUAGE_DISPLAY', this.selectedLanguage);
    let selectedDepartment = JSON.parse(
      window.localStorage.getItem(SELECTED_DEPARTMENT)
    );
    let selectedLocation = JSON.parse(
      window.localStorage.getItem(SELECTED_LOCATION)
    );
    if (
      selectedDepartment.wheelOrderType !== 1 &&
      selectedDepartment.allowPreOrder &&
      !selectedDepartment.isOpeningHour &&
      selectedDepartment.dateDefaultOrderFututer != null
    ) {
      this.router.navigateByUrl(`/select-date-future`);
    } else {
      this.router.navigateByUrl(
        `/location/${selectedLocation.id}/type/${selectedDepartment.id}`
      );
    }
  }
  onChooseLanguage(language: string): void {
    if (
      !this.selectedLanguage ||
      (this.selectedLanguage && language !== this.selectedLanguage.id)
    ) {
      this.selectedLanguage = language;
      this.onClickLanguage();
    } else {
      this.selectedLanguage = undefined;
    }
  }

  onClickContinue(): void {
    this.router.navigateByUrl('/department');
  }

  onClickBack(currentpage?: number) {
    let selectedDepartment = JSON.parse(
      window.localStorage.getItem(SELECTED_DEPARTMENT)
    );
    let qrCode = JSON.parse(window.sessionStorage.getItem(SCAN_QR_CODE));
    this.wheelOrderService
      .getPageToBack(
        currentpage ? currentpage : PAGE.Language,
        selectedDepartment.id,
        qrCode ? true : false
      )
      .subscribe(
        (res: any) => {
          switch (res.result) {
            case PAGE.PostalCode:
              this.router.navigateByUrl('/postal-code');
              break;

            case PAGE.Depaterment:
              this.router.navigateByUrl('');
              break;
            case PAGE.PhoneNumber:
              this.router.navigateByUrl('/phone-number');
              break;
            case PAGE.Location:
              this.router.navigateByUrl('/location');
              break;
          }
        },
        (error) => {}
      );
  }
}

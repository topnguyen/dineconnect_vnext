import { AlertService } from './../../../shared/service/alert.service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessageService } from 'primeng/api';
import { IPageResultList, IUser } from 'src/model';
import { CommonService, DineMessageService } from 'src/shared/common';
import { AutoUnsubscribe } from 'src/shared/common/config/unsubscribe/auto-unsubscribe';
import { AppConsts, RegexPatterns, StepName } from 'src/shared/constant';
import { emailCustomValidators } from 'src/shared/custom-validation';
import { AccountService } from 'src/shared/service';
import { ToastService } from 'src/shared/toast';
import { Router } from '@angular/router';
import { TenantService } from 'src/app/tenant/tenant.service';
import { NavigationService } from 'src/shared/navigation';
import {
  SendPasswordResetCodeInput,
  SessionServiceProxy,
  UpdateUserSignInTokenOutput,
  UserServiceProxy,
} from 'src/shared/service-proxies/service-proxies';
import { ExternalLoginProvider, SigninService } from './sign-in.service';
import { LOGO_URL } from 'src/shared/shared.const';
import { AbpSessionService } from 'abp-ng2-module';
import { LoginService } from './login.service';
import { UrlHelper } from 'src/shared/helpers/UrlHelper';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
@AutoUnsubscribe()
export class SignInComponent implements OnInit {
  phoneEmailForm: FormControl;
  formAccess: FormGroup;
  currentStep = 1;
  user: IUser;
  errMsg = 'Invalid value. Please enter a valid email address.';
  isHiddenPassword = true;
  logoUrl = '';
  submitting = false;
  model = new SendPasswordResetCodeInput({
    domain: AppConsts.appBaseUrl,
    emailAddress: '',
  });

  isMultiTenancyEnabled: boolean;
  recaptchaSiteKey: string;
  captchaResponse?: string;
  constructor(
    public loginService: LoginService,
    private _router: Router,
    private _sessionService: AbpSessionService,
    private _sessionAppService: SessionServiceProxy,
    private messageService: MessageService,
    private spinnerService: NgxSpinnerService,
    private accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
    this.loginService.init();
    if (
      this._sessionService.userId > 0 &&
      UrlHelper.getReturnUrl() &&
      UrlHelper.getSingleSignIn()
    ) {
      this._sessionAppService
        .updateUserSignInToken()
        .subscribe((result: UpdateUserSignInTokenOutput) => {
          const initialReturnUrl = UrlHelper.getReturnUrl();
          const returnUrl =
            initialReturnUrl +
            (initialReturnUrl.indexOf('?') >= 0 ? '&' : '?') +
            'accessToken=' +
            result.signInToken +
            '&userId=' +
            result.encodedUserId +
            '&tenantId=' +
            result.encodedTenantId;

          location.href = returnUrl;
        });
    }

    let state = UrlHelper.getQueryParametersUsingHash().state;
    if (state && state.indexOf('openIdConnect') >= 0) {
      this.loginService.openIdConnectLoginCallback({});
    }
  }

  login(): void {
    if (this.useCaptcha && !this.captchaResponse) {
      this.messageService.clear();
      this.messageService.add({
        severity: 'success',
        summary: 'Service Message',
        detail: 'CaptchaCanNotBeEmpty',
      });
      return;
    }

    this.spinnerService.show();

    this.submitting = true;
    this.loginService.authenticate(
      () => {
        this.submitting = false;
        this.spinnerService.hide();
      },
      null,
      this.captchaResponse
    );
  }

  externalLogin(provider: ExternalLoginProvider) {
    this.loginService.externalAuthenticate(provider);
  }

  forgotPassword() {
    this.model.emailAddress =
      this.loginService.authenticateModel.userNameOrEmailAddress;
    this.accountService.sendPasswordResetCode(this.model).subscribe(() => {
      this.messageService.clear();
      this.messageService.add({
        severity: 'success',
        summary: 'Service Message',
        detail:
          'A password reset link sent to your e-mail address. Please check your emails.',
      });
    });
  }

  get useCaptcha(): boolean {
    return false;
  }
}

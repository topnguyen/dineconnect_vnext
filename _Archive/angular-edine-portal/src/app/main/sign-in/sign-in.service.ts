import { Injectable } from '@angular/core';
import {
  AuthenticateModel,
  AuthenticateResultModel,
  ExternalAuthenticateModel,
  ExternalAuthenticateResultModel,
  ExternalLoginProviderInfoModel,
  TokenAuthServiceProxy,
} from 'src/shared/service-proxies/service-proxies';
import { UserAgentApplication, AuthResponse } from 'msal';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  DineMessageService,
  HttpClientService,
  HttpOptions,
  LocalStorageService,
} from 'src/shared/common';
import { OAuthService, AuthConfig } from 'angular-oauth2-oidc';
import { ScriptLoaderService } from 'src/shared/common/common-service/script-loader.service';
import { AppConsts, CONSTANT } from 'src/shared/constant';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import * as AuthenticationContext from 'adal-angular/lib/adal';
import { UrlHelper } from 'src/shared/helpers/UrlHelper';
import {
  TokenService,
  LogService,
  MessageService,
  LocalizationService,
} from 'abp-ng2-module';
import { Observable } from 'rxjs';
import { JwtService } from 'src/shared/common/authenticate';
import { IUser } from 'src/model';
import { AccountService } from 'src/shared/service';

declare const FB: any; // Facebook API
declare const gapi: any; // Facebook API

export class ExternalLoginProvider extends ExternalLoginProviderInfoModel {
  static readonly FACEBOOK: string = 'Facebook';
  static readonly GOOGLE: string = 'Google';
  static readonly MICROSOFT: string = 'Microsoft';
  static readonly OPENID: string = 'OpenIdConnect';
  static readonly WSFEDERATION: string = 'WsFederation';

  icon: string;
  initialized = false;

  constructor(providerInfo: ExternalLoginProviderInfoModel) {
    super();

    this.name = providerInfo.name;
    this.clientId = providerInfo.clientId;
    this.additionalParams = providerInfo.additionalParams;
    this.icon = providerInfo.name.toLowerCase();
  }
}

@Injectable()
export class SigninService {
  static readonly twoFactorRememberClientTokenName =
    'TwoFactorRememberClientToken';

  MSAL: UserAgentApplication; // Microsoft API
  authenticateModel: AuthenticateModel;
  authenticateResult: AuthenticateResultModel;
  externalLoginProviders: ExternalLoginProvider[] = [];
  rememberMe: boolean;

  wsFederationAuthenticationContext: any;
  user: IUser;

  constructor(
    private http: HttpClientService,
    private _router: Router,
    private _messageService: MessageService,
    private _tokenService: TokenService,
    private dineMessageService: DineMessageService,
    private oauthService: OAuthService,
    private spinnerService: NgxSpinnerService,
    private _localStorageService: LocalStorageService,
    private accountService: AccountService,
    private auth: JwtService
  ) {
    this.clear();
  }
  private login(
    accessToken: string,
    encryptedAccessToken: string,
    expireInSeconds: number,
    refreshToken: string,
    refreshTokenExpireInSeconds: number,
    rememberMe?: boolean,
    twoFactorRememberClientToken?: string,
    redirectUrl?: string
  ): void {
    let tokenExpireDate = rememberMe
      ? new Date(new Date().getTime() + 1000 * expireInSeconds)
      : undefined;

    this._tokenService.setToken(accessToken, tokenExpireDate);

    if (refreshToken && rememberMe) {
      let refreshTokenExpireDate = rememberMe
        ? new Date(new Date().getTime() + 1000 * refreshTokenExpireInSeconds)
        : undefined;
      this._tokenService.setRefreshToken(refreshToken, refreshTokenExpireDate);
    }

    this.user.token = accessToken;
    this.accountService.saveUser(this.user);

    this._localStorageService.setItem(
      AppConsts.authorization.encrptedAuthTokenName,
      {
        token: encryptedAccessToken,
        expireDate: tokenExpireDate,
      }
    );

    if (twoFactorRememberClientToken) {
      this._localStorageService.setItem(
        SigninService.twoFactorRememberClientTokenName,
        {
          token: twoFactorRememberClientToken,
          expireDate: new Date(new Date().getTime() + 365 * 86400000), // 1 year
        }
      );
    }

    if (redirectUrl) {
      location.href = redirectUrl;
    } else {
      location.href = AppConsts.appBaseUrl;
    }
  }
  private clear(): void {
    this.authenticateModel = new AuthenticateModel();
    this.authenticateModel.rememberClient = false;
    this.authenticateResult = null;
    this.rememberMe = false;
  }

  private initExternalLoginProviders(callback?: any) {
    this.getExternalAuthenticationProviders().subscribe((providers) => {
      this.externalLoginProviders = _.map(
        (providers as any).result,
        (p) => new ExternalLoginProvider(p)
      );

      if (callback) {
        callback();
      }
    });
  }

  externalAuthenticate(provider: ExternalLoginProvider): void {
    this.ensureExternalLoginProviderInitialized(provider, () => {
      if (provider.name === ExternalLoginProvider.FACEBOOK) {
        FB.login(
          (response) => {
            this.facebookLoginStatusChangeCallback(response);
          },
          { scope: 'email' }
        );
      } else if (provider.name === ExternalLoginProvider.GOOGLE) {
        gapi.auth2
          .getAuthInstance()
          .signIn()
          .then(() => {
            this.googleLoginStatusChangeCallback(
              gapi.auth2.getAuthInstance().isSignedIn.get()
            );
          });
      } else if (provider.name === ExternalLoginProvider.MICROSOFT) {
        let scopes = ['user.read'];
        this.spinnerService.show();
        this.MSAL.loginPopup({
          scopes: scopes,
        }).then((idTokenResponse: AuthResponse) => {
          this.MSAL.acquireTokenSilent({ scopes: scopes })
            .then((accessTokenResponse: AuthResponse) => {
              this.microsoftLoginCallback(accessTokenResponse);
              this.spinnerService.hide();
            })
            .catch((error) => {
              this.dineMessageService.showError(error.error.error.message);
            });
        });
      }
    });
  }

  init(): void {
    this.initExternalLoginProviders();
  }

  ensureExternalLoginProviderInitialized(
    loginProvider: ExternalLoginProvider,
    callback: () => void
  ) {
    if (loginProvider.initialized) {
      callback();
      return;
    }

    if (loginProvider.name === ExternalLoginProvider.FACEBOOK) {
      new ScriptLoaderService()
        .load('//connect.facebook.net/en_US/sdk.js')
        .then(() => {
          FB.init({
            appId: loginProvider.clientId,
            cookie: false,
            xfbml: true,
            version: 'v2.5',
          });

          FB.getLoginStatus((response) => {
            this.facebookLoginStatusChangeCallback(response);
            if (response.status !== 'connected') {
              callback();
            }
          });
        });
    } else if (loginProvider.name === ExternalLoginProvider.GOOGLE) {
      new ScriptLoaderService()
        .load('https://apis.google.com/js/api.js')
        .then(() => {
          gapi.load('client:auth2', () => {
            gapi.client
              .init({
                clientId: loginProvider.clientId,
                scope: 'openid profile email',
              })
              .then(() => {
                callback();
              });
          });
        });
    } else if (loginProvider.name === ExternalLoginProvider.MICROSOFT) {
      this.MSAL = new UserAgentApplication({
        auth: {
          clientId: loginProvider.clientId,
          redirectUri: AppConsts.appBaseUrl,
        },
      });
      callback();
    } else if (loginProvider.name === ExternalLoginProvider.OPENID) {
      const authConfig = this.getOpenIdConnectConfig(loginProvider);
      this.oauthService.configure(authConfig);
      this.oauthService.initImplicitFlow('openIdConnect=1');
    } else if (loginProvider.name === ExternalLoginProvider.WSFEDERATION) {
      let config = this.getWsFederationConnectConfig(loginProvider);
      this.wsFederationAuthenticationContext = new AuthenticationContext(
        config
      );
      this.wsFederationAuthenticationContext.login();
    }
  }

  private getWsFederationConnectConfig(
    loginProvider: ExternalLoginProvider
  ): any {
    let config = {
      clientId: loginProvider.clientId,
      popUp: true,
      callback: this.wsFederationLoginStatusChangeCallback.bind(this),
    } as any;

    if (loginProvider.additionalParams['Tenant']) {
      config.tenant = loginProvider.additionalParams['Tenant'];
    }

    return config;
  }

  private getOpenIdConnectConfig(
    loginProvider: ExternalLoginProvider
  ): AuthConfig {
    let authConfig = new AuthConfig();
    authConfig.loginUrl = loginProvider.additionalParams['LoginUrl'];
    authConfig.issuer = loginProvider.additionalParams['Authority'];
    authConfig.skipIssuerCheck =
      loginProvider.additionalParams['ValidateIssuer'] === 'false';
    authConfig.clientId = loginProvider.clientId;
    authConfig.responseType = 'id_token';
    authConfig.redirectUri = window.location.origin + '/account/login';
    authConfig.scope = 'openid profile';
    authConfig.requestAccessToken = false;
    return authConfig;
  }

  facebookLoginStatusChangeCallback(resp) {
    if (resp.status === 'connected') {
      const model = new ExternalAuthenticateModel();
      model.authProvider = ExternalLoginProvider.FACEBOOK;
      model.providerAccessCode = resp.authResponse.accessToken;
      model.providerKey = resp.authResponse.userID;
      model.singleSignIn = UrlHelper.getSingleSignIn();
      model.returnUrl = UrlHelper.getReturnUrl();

      this.auth.externalAuthenticate(model).subscribe(
        (ouput) => {
          let result = (ouput as any).result;
          if (result.waitingForActivation) {
            this._messageService.info(
              'You have successfully registered. Waiting for activation!'
            );
            return;
          }

          FB.api('/me', { fields: 'name, email' }, (response) => {
            this.accountService.getUsers(response.email).subscribe((value) => {
              if (value.result && value.result.totalCount == 1) {
                this.user = value.result.items[0];
                this.accountService.tempInfomationUser = this.user;
              }

              this.login(
                result.accessToken,
                result.encryptedAccessToken,
                result.expireInSeconds,
                result.refreshToken,
                result.refreshTokenExpireInSeconds,
                false,
                '',
                result.returnUrl
              );
            });
          });
        },
        (error) => {
          this.dineMessageService.showError(error.error.error.message);
        }
      );
    }
  }

  public openIdConnectLoginCallback(resp) {
    this.initExternalLoginProviders(() => {
      let openIdProvider = _.filter(this.externalLoginProviders, {
        name: 'OpenIdConnect',
      })[0];
      let authConfig = this.getOpenIdConnectConfig(openIdProvider);

      this.oauthService.configure(authConfig);

      this.spinnerService.show();

      this.oauthService.tryLogin().then(() => {
        let claims = this.oauthService.getIdentityClaims();

        const model = new ExternalAuthenticateModel();
        model.authProvider = ExternalLoginProvider.OPENID;
        model.providerAccessCode = this.oauthService.getIdToken();
        model.providerKey = claims['sub'];
        model.singleSignIn = UrlHelper.getSingleSignIn();
        model.returnUrl = UrlHelper.getReturnUrl();

        this.auth
          .externalAuthenticate(model)
          .pipe(
            finalize(() => {
              this.spinnerService.hide();
            })
          )
          .subscribe((result: ExternalAuthenticateResultModel) => {
            if (result.waitingForActivation) {
              this._messageService.info(
                'You have successfully registered. Waiting for activation!'
              );
              return;
            }

            this.login(
              result.accessToken,
              result.encryptedAccessToken,
              result.expireInSeconds,
              result.refreshToken,
              result.refreshTokenExpireInSeconds,
              false,
              '',
              result.returnUrl
            );
          });
      });
    });
  }

  private googleLoginStatusChangeCallback(isSignedIn) {
    if (isSignedIn) {
      const model = new ExternalAuthenticateModel();
      model.authProvider = ExternalLoginProvider.GOOGLE;
      model.providerAccessCode = gapi.auth2
        .getAuthInstance()
        .currentUser.get()
        .getAuthResponse().access_token;
      model.providerKey = gapi.auth2
        .getAuthInstance()
        .currentUser.get()
        .getBasicProfile()
        .getId();
      model.singleSignIn = UrlHelper.getSingleSignIn();
      model.returnUrl = UrlHelper.getReturnUrl();

      this.auth.externalAuthenticate(model).subscribe(
        (ouput) => {
          let result = (ouput as any).result;
          if (result.waitingForActivation) {
            this._messageService.info(
              'You have successfully registered. Waiting for activation!'
            );
            return;
          }

          let email = gapi.auth2
            .getAuthInstance()
            .currentUser.get()
            .getBasicProfile()
            .getEmail();

          this.accountService.getUsers(email).subscribe((value) => {
            if (value.result && value.result.totalCount == 1) {
              this.user = value.result.items[0];
              this.accountService.tempInfomationUser = this.user;
            }

            this.login(
              result.accessToken,
              result.encryptedAccessToken,
              result.expireInSeconds,
              result.refreshToken,
              result.refreshTokenExpireInSeconds,
              false,
              '',
              result.returnUrl
            );
          });
        },

        (error) => {
          this.dineMessageService.showError(error.error.error.message);
        }
      );
    }
  }

  public wsFederationLoginStatusChangeCallback(
    errorDesc,
    token,
    error,
    tokenType
  ) {
    let user = this.wsFederationAuthenticationContext.getCachedUser();

    const model = new ExternalAuthenticateModel();
    model.authProvider = ExternalLoginProvider.WSFEDERATION;
    model.providerAccessCode = token;
    model.providerKey = user.profile.sub;
    model.singleSignIn = UrlHelper.getSingleSignIn();
    model.returnUrl = UrlHelper.getReturnUrl();

    this.auth
      .externalAuthenticate(model)
      .subscribe((result: ExternalAuthenticateResultModel) => {
        if (result.waitingForActivation) {
          this._messageService.info(
            'You have successfully registered. Waiting for activation!'
          );
          this._router.navigate(['account/login']);
          return;
        }

        this.login(
          result.accessToken,
          result.encryptedAccessToken,
          result.expireInSeconds,
          result.refreshToken,
          result.refreshTokenExpireInSeconds,
          false,
          '',
          result.returnUrl
        );
      });
  }

  /**
   * Microsoft login is not completed yet, because of an error thrown by zone.js: https://github.com/angular/zone.js/issues/290
   */
  private microsoftLoginCallback(response: AuthResponse) {
    const model = new ExternalAuthenticateModel();
    model.authProvider = ExternalLoginProvider.MICROSOFT;
    model.providerAccessCode = response.accessToken;
    // remove dashes and starting 0 characters from objectId
    // 000-000-111-222 will be converted to 111222
    model.providerKey = response.idToken.objectId
      .replace(new RegExp('-', 'gm'), '')
      .replace(/^0*/g, '');
    model.singleSignIn = UrlHelper.getSingleSignIn();
    model.returnUrl = UrlHelper.getReturnUrl();

    this.spinnerService.show();

    this.auth
      .externalAuthenticate(model)
      .subscribe((result: ExternalAuthenticateResultModel) => {
        if (result.waitingForActivation) {
          this._messageService.info(
            'You have successfully registered. Waiting for activation!'
          );
          return;
        }

        this.login(
          result.accessToken,
          result.encryptedAccessToken,
          result.expireInSeconds,
          result.refreshToken,
          result.refreshTokenExpireInSeconds,
          false,
          '',
          result.returnUrl
        );
        this.spinnerService.hide();
      });
  }

  getExternalAuthenticationProviders(): Observable<
    ExternalLoginProviderInfoModel[]
  > {
    let url_ = '/api/TokenAuth/GetExternalAuthenticationProviders';
    url_ = url_.replace(/[?&]$/, '');

    const options: HttpOptions = {
      url: url_,
      forceReload: true,
    };
    return this.http.get(options);
  }
}

import {
  CommonService,
  DineMessageService,
  LocalStorageService,
} from 'src/shared/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AccountService,
  WheelLocationsService
} from 'src/shared/service';
import { AppSession, OrderSetting, wheelDataSetups } from 'src/shared/constant';
import {
  LOGO_URL,
  SELECTED_DEPARTMENT,
  SELECTED_LOCATION
} from 'src/shared/shared.const';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';
import { CheckOutService } from 'src/shared/service/check-out.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
})
export class DepartmentComponent implements OnInit {
  @ViewChild('leftMenuRef', { static: false })

  leftSideMenu: LeftSideMenuComponent;
  logoUrl = '';

  welcomeMessage: string = '';
  wheelDepartments = [];
  wheelServiceFees = [];
  selectedDepartment = undefined;
  enableContinueBtn: boolean = false;

  constructor(
    private wheelLocationsService: WheelLocationsService,
    private router: Router,
    private dineMessageService: DineMessageService,
    public accountService: AccountService,
    private checkoutService: CheckOutService,
    private localStorageService: LocalStorageService,
  ) {
    this.welcomeMessage = wheelDataSetups.welcomeMessage;
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
  }

  ngOnInit(): void {
    if (AppSession.user == null && OrderSetting.signUp) {
      this.router.navigateByUrl('/sign-in');
      return;
    }

    this.wheelLocationsService.getWheelDepartments().subscribe((resp) => {
      this.wheelDepartments = resp.result;
    });

    this.leftSideMenu?.close();
  }

  selectDepartment(department): void {
    if (department.isOpeningHour) {
      this.nextStep(department);
    }
    else {
      if (department.allowPreOrder && department.dateDefaultOrderFututer != null) {
        this.dineMessageService.showError('Currently it is not available for ordering. But you can pre-order.');
        this.nextStep(department);
      } else
        this.dineMessageService.showError('Currently it is not available for ordering');
    }
  }

  private nextStep(department: any) {
    this.checkoutService.saveCustomerSessionData(SELECTED_DEPARTMENT, JSON.stringify(department)).subscribe();

    if (department.wheelOrderType !== 1) {
      const onlyOneLocation = department.wheelLocationOutputs && department.wheelLocationOutputs.length === 1;
      const location = department.wheelLocationOutputs[0];
      if (onlyOneLocation) {
        this.checkoutService.isLocationSetup(location.id).subscribe(res => {
          if (res.result) {
            this.checkoutService.isLocationInDeliveryZone(location.id).subscribe((result) => {
              let checkDeliveryResult = result.result;
              if (!checkDeliveryResult.allowOutOfDeliveryZones && checkDeliveryResult.outOfAllDeliveryZones) {
                this.dineMessageService.showError(`Your address is out of develiry zones. Please select other location`);
              } else {
                this.localStorageService.saveDeliveryLocation(location);
                this.checkoutService.saveCustomerSessionData(SELECTED_LOCATION, JSON.stringify(location)).subscribe();
                this.router.navigateByUrl(department.id.toString() + '/location/' + location.id);
              }
            });
          }
          else {
            this.dineMessageService.showError('Selected Location is not activated for Ordering')
          }
        })

       
      } else {
        this.router.navigateByUrl(department.id.toString() + '/location');
      }
    }
    else {
      this.router.navigateByUrl(department.id.toString() + '/postal-code');
    }
  }

  showMenu() {
    this.leftSideMenu.show();
  }
}

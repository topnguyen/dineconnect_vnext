import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'department-card',
  templateUrl: './department-card.component.html',
  styleUrls: ['./department-card.component.scss']
})
export class DepartmentCardComponent implements OnInit {

  locationId: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  @Input() wheelDepartment: any;

  ngOnInit(): void {
    this.locationId = parseInt(this.route.snapshot.paramMap.get('locationId'));
  }

  goToMenuItem(departmentId) {
    this.router.navigateByUrl(`/location/${this.locationId}/type/${departmentId}`);
  }
}

import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { CommonService, MenuItemService } from 'src/shared/common';
import { StepName } from 'src/shared/constant';
@Component({
  selector: 'combo',
  templateUrl: './combo.component.html',
  styleUrls: ['./combo.component.scss']
})
export class ComboComponent implements OnInit {

  tabIndex = 0;
  selectedBurger = -1;
  selectedFry = -1;
  selectedDrink = -1;

  listItemsSelected = [];
  selectedItem = -1;

  listBurgers = [
    {
      imageUrl: 'assets/image/combos/combo-1/Layer-6.png',
      name: 'Beef Burger (S)',
      price: '+ $0.00',
      category: 'burger',
      id: 0
    },
    {
      imageUrl: 'assets/image/combos/combo-1/Layer-6.png',
      name: 'Beef Burger (M)',
      price: '+ $2.99',
      category: 'burger',
      id: 1
    },
    {
      imageUrl: 'assets/image/combos/combo-1/Layer-6.png',
      name: 'Beef Burger (L)',
      price: '+ $3.99',
      category: 'burger',
      id: 2
    }
  ]

  listFries = [
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-978.png',
      name: 'French Fries (S)',
      price: '+ $0.00',
      category: 'fry',
      id: 0
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-978.png',
      name: 'French Fries (M)',
      price: '+ $2.99',
      category: 'fry',
      id: 1
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-978.png',
      name: 'French Fries (L)',
      price: '+ $3.99',
      category: 'fry',
      id: 2
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-979.png',
      name: 'Potato Wedges',
      price: '+ $3.99',
      category: 'fry',
      id: 3
    }
  ]

  listDrinks = [
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Coke (300ml)',
      price: '+ $0.00',
      category: 'drink',
      id: 0
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Sprite (300ml)',
      price: '+ $2.99',
      category: 'drink',
      id: 1
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Lemonade',
      price: '+ $3.99',
      category: 'drink',
      id: 2
    },
    {
      imageUrl: 'assets/image/combos/combo-2/Layer-977.png',
      name: 'Hot Chocolate',
      price: '+ $3.99',
      category: 'drink',
      id: 3
    }
  ]

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.commonService.$currentStepName.next(StepName.combo);
  }

  handleTabChange(e) {
    this.tabIndex = e.index;
  }

  addToSelectedComboItems(item) {
    if (this.listItemsSelected.length) {
      let index = this.listItemsSelected.findIndex(i => i.category === item.category);
      if (index === -1) {
        this.listItemsSelected.push(item);
      } else {
        this.listItemsSelected[index] = item;
      }
    } else {
      this.listItemsSelected.push(item);
    }
  }

  onNextCompo() {
    switch (this.tabIndex) {
      case 0:
        if (this.selectedBurger === -1)
          return;
        else {
          this.tabIndex = this.tabIndex + 1;
          this.addToSelectedComboItems(this.listBurgers.find(x => x.id === this.selectedBurger));
        }
        break;

      case 1:
        if (this.selectedFry === -1)
          return;
        else {
          this.tabIndex = this.tabIndex + 1;
          this.addToSelectedComboItems(this.listFries.find(x => x.id === this.selectedFry));
        }
        break;

      case 2:
        if (this.selectedDrink === -1)
          return;
        else {
          this.tabIndex = this.tabIndex + 1;
          this.addToSelectedComboItems(this.listDrinks.find(x => x.id === this.selectedDrink));
        }
        break;

      default:
        break;
    }
  }

  onConfirmItemSelected(item) {
    switch (item.category) {
      case 'burger':
        this.tabIndex = 0;
        break;

      case 'fry':
        this.tabIndex = 1;
        break;

      case 'drink':
        this.tabIndex = 2;
        break;

      default:
        break;
    }
  }
}

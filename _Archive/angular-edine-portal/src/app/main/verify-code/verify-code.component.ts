import {
  LOGO_URL,
  POSTAL_CODE,
  POSTAL_CODE_ONEMAP,
  SAVED_OTP,
  SCAN_QR_CODE,
  SELECTED_DEPARTMENT,
  SELECTED_LOCATION,
  SELECTED_TABLE,
} from './../../../shared/shared.const';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, AfterViewInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  WheelOrderType,
  WheelTableServiceProxy,
} from 'src/shared/service-proxies/service-proxies';
import { AccountService, WheelLocationsService } from 'src/shared/service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { DineMessageService } from 'src/shared/common';
import { PlatformLocation } from '@angular/common';
import { CheckOutService } from 'src/shared/service/check-out.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify-code.component.html',
  styleUrls: ['./verify-code.component.scss'],
})
export class VerifyComponent implements AfterViewInit {
  isReady: boolean = false;
  logoUrl: string = '';

  locationId: number = -1;
  departmentId: number = -1;
  tableId: number = -1;
  phoneNumber: string;

  arrayCode: string[] = ['', '', '', ''];

  formGroup: FormGroup;
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  selectedDepartment: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private activeRouter: ActivatedRoute,
    private wheelTableService: WheelTableServiceProxy,
    private wheelLocationsService: WheelLocationsService,
    private readonly accountService: AccountService,
    private dineMessageService: DineMessageService,
    private checkOutService: CheckOutService,
    private pl: PlatformLocation
  ) {
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

    this.activeRouter.params.subscribe((params) => {
      this.locationId = parseFloat(params.locationId.toString());
      this.departmentId = parseFloat(params.departmentId.toString());
      this.tableId = parseFloat(params.tableId.toString());

      if (params.phoneNumber) {
        this.phoneNumber = decodeURIComponent(params.phoneNumber);
      }

      let body: any = {
        departmentId: this.departmentId,
        locationId: this.locationId,
        tableId: this.tableId
      };

      let savedOtpStr = window.sessionStorage.getItem(SAVED_OTP);

      if (savedOtpStr) {
        let savedOtp = JSON.parse(savedOtpStr);
        const expiredTime = parseFloat(savedOtp.expiredTime);
        const currentTime = new Date().getTime() + 0;

        if (expiredTime >= currentTime) {
          body.savedOTP = savedOtp.otp;
        }
      }

      if (this.phoneNumber) {
        body.phoneNumber = this.phoneNumber;
      }

      this.checkOutService.initQROrdering(body).subscribe(t => {
        if (t.result == 1) {
          this.router.navigateByUrl('/phone-number/verify');
        }
        else if (t.result == 2) {
          this.sendOtp();

          this.formGroup = this.fb.group({
            codeVerify: ['', Validators.required],
          });
        }
        else if (t.result == 3) {
          this.processOTPCorrect();
        }
        else {
          this.router.navigateByUrl('/');
        }
      });
    });
  }

  get codeVerify(): AbstractControl {
    return this.formGroup.controls.codeVerify;
  }

  ngAfterViewInit(): void {
    document.getElementById('input-otp0')?.focus();
  }

  onInputCode(position: number, event): void {
    this.arrayCode[position] = event.target.value;
    const nextPosition: number = position === 3 ? position : position + 1;
    if (event.target.value && !this.arrayCode[nextPosition].length) {
      document.getElementById('input-otp' + position).classList.remove('focus');
      document.getElementById('input-otp' + nextPosition).focus();
      document
        .getElementById('input-otp' + nextPosition)
        .classList.remove('disabled');
      document
        .getElementById('input-otp' + nextPosition)
        .classList.add('focus');
    } else if (this.arrayCode[nextPosition].length) {
      document.getElementById('input-otp' + nextPosition)?.focus();
      document
        .getElementById('input-otp' + nextPosition)
        ?.classList.add('focus');
    }
    this.codeVerify.setValue(this.arrayCode.join(''));
    event.preventDefault();
    event.stopPropagation();
  }

  checkValueCode(position: number): void {
    if (this.arrayCode[position].length) {
      for (let index = 0; index <= 3; index++) {
        if (index !== position) {
          document
            .getElementById('input-otp' + index)
            .classList.remove('focus');
        }
      }
      document.getElementById('input-otp' + position)?.focus();
      document.getElementById('input-otp' + position).classList.add('focus');
    } else if (this.arrayCode[position - 1]?.length) {
      let indexValueNull: number = null;
      this.arrayCode.find((code, index) => {
        if (!code.length && this.arrayCode[index + 1]?.length) {
          indexValueNull = index;
        }
        return code;
      });
      for (let index = 0; index <= 3; index++) {
        if (index !== indexValueNull) {
          document
            .getElementById('input-otp' + index)
            .classList.remove('focus');
        }
      }
      document.getElementById('input-otp' + indexValueNull)?.focus();
      document
        .getElementById('input-otp' + indexValueNull)
        ?.classList.add('focus');
    } else {
      for (let index = 0; index <= 3; index++) {
        if (index !== position) {
          document
            .getElementById('input-otp' + index)
            .classList.remove('focus');
        }
      }
      document.getElementById('input-otp' + (position - 1))?.focus();
      document
        .getElementById('input-otp' + (position - 1))
        ?.classList.add('focus');
    }
  }

  private sendOtp(): void {
    this.isReady = true;
    document.getElementById('input-otp0')?.focus();
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
    this.wheelTableService
      .sendOtp(
        this.locationId,
        this.departmentId,
        this.tableId,
        this.phoneNumber
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        const response: any = res;
        if (!response.result) {
          this.dineMessageService.showError(
            `Can't send OTP code at this Moment.`
          );
        } else {
          this.dineMessageService.showSuccess(
            `OTP code was sent successfully!`
          );
        }
      });
  }

  onSubmit(): void {
    this.wheelTableService
      .verifyOtp(
        this.locationId,
        this.departmentId,
        this.tableId,
        this.phoneNumber,
        this.codeVerify.value
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        const response: any = res;
        if (response.result === true) {
          var date = new Date() // your date object
          window.sessionStorage.setItem(SAVED_OTP, JSON.stringify({ otp: this.codeVerify.value, expiredTime: date.getTime() + 1 * 60 * 60 * 1000 }));
          this.processOTPCorrect();
        } else {
          this.dineMessageService.showError('OTP code is not correct!');
        }
      });
  }

  processOTPCorrect(): void {
    this.checkOutService.getCustomerSessionData(SELECTED_DEPARTMENT).subscribe(res => {
      let department = JSON.parse(res.result);
      if (department.wheelOrderType == WheelOrderType.Delivery)
        this.router.navigateByUrl(`${this.departmentId}/postal-code`);
      else
        this.router.navigateByUrl(
          `${this.departmentId}/location/${this.locationId}`
        );
    })
  }
}

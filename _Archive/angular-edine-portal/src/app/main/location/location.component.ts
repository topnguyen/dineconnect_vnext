import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  AccountService,
  LocationService,
  WheelLocationsService,
} from 'src/shared/service';
import {
  CommonService,
  DineMessageService,
  LocalStorageService,
} from 'src/shared/common';
import { debounceTime, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderSetting, PAGE, StepName } from 'src/shared/constant';
import {
  LOGO_URL,
  SCAN_QR_CODE,
  SELECTED_LOCATION,
} from 'src/shared/shared.const';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import { PlatformLocation } from '@angular/common';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit, OnDestroy {
  @ViewChild('leftMenuRef', { static: false })
  leftSideMenu: LeftSideMenuComponent;
  displayPopupLocation = false;
  enableContinueBtn: boolean = false;
  logoUrl = '';
  valLocation = '';
  suggestionsLocation = [];
  placeHolderText = 'Search here...';
  custInput = 'custom-input';
  locations = [];
  selectedLocation = undefined;
  themeSettings: any;
  isApiCalling: boolean = true;
  isback = false;

  languages: Array<{ code: string; icon: string; name: string }> = [
    {
      code: 'EN',
      icon: 'assets/image/en-icon.png',
      name: 'ENGLISH',
    },
  ];

  departmentId: number;
  department: any;

  constructor(
    private route: ActivatedRoute,
    private wheelLocationsService: WheelLocationsService,
    private router: Router,
    private locationService: LocationService,
    private localStorageService: LocalStorageService,
    private commonService: CommonService,
    private checkOutService: CheckOutService,
    private dineMessageService: DineMessageService,
    public accountService: AccountService,
    private wheelOrderService: WheelOrderService,
    private pl: PlatformLocation,
  ) {
  }

  ngOnInit(): void {
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);

    this.route.paramMap.subscribe((paramMap) => {
      let routeParams = paramMap["params"];
      this.departmentId = routeParams['departmentId'];

      if (!this.departmentId) {
        this.router.navigateByUrl(``);
      }

      this.wheelLocationsService.getWheelDepartment(this.departmentId).subscribe(res => {
        this.department = res.result;

        if (!this.department) {
          this.router.navigateByUrl(``);
        } else {
          this.locations = this.department.wheelLocationOutputs || [];

          if (this.locations.length === 1) {
            this.selectLocation(this.locations[0]);
          }
        }
      })

      this.commonService.$isWith70Subject.next(true);

      this.locationService.$isOpenLocation.subscribe((isOpen) => {
        this.displayPopupLocation = isOpen;
      });

      this.commonService.$currentStepName.next(StepName.chooseLocation);

      this.leftSideMenu?.close();
    });
  }

  ngOnDestroy(): void {
    this.commonService.$isWith70Subject.next(false);
  }

  selectLocation(location): void {
    this.checkOutService.isLocationSetup(location.id).subscribe(res => {
      if (res.result) {
        this.checkOutService.isLocationInDeliveryZone(location.id).subscribe((result) => {
          let checkDeliveryResult = result.result;
          if (!checkDeliveryResult.allowOutOfDeliveryZones && checkDeliveryResult.outOfAllDeliveryZones) {
            this.dineMessageService.showError(`Your address is out of develiry zones. Please select other location`);
          } else {
            this.chooseLocation(location);
          }
        });
      }
      else {
        this.dineMessageService.showError('Selected Location is not activated for Ordering')
      }
    })
  }

  closePopup() {
    this.displayPopupLocation = false;
  }

  onClickBack(): void {
    this.wheelOrderService
      .getPageToBack(PAGE.Location, this.departmentId, false)
      .subscribe(
        (res: any) => {
          switch (res.result) {
            case PAGE.PostalCode:
              this.router.navigateByUrl(this.departmentId + '/postal-code');
              break;

            case PAGE.Depaterment:
              this.router.navigateByUrl('/');
              break;
          }
        },
        (error) => { }
      );
  }

  search(event) {
    this.getSimpleLocation(event.query, 0, '', 10, 0)
      .pipe(debounceTime(400))
      .subscribe((data) => {
        this.suggestionsLocation = [...data.result.items];
      });
  }

  continueOrder(autocomplete) {
    if (autocomplete) {
      this.localStorageService.saveDeliveryLocation(autocomplete.value);
      this.closePopup();
    }
  }

  clearLocation(autocomplete) {
    if (!autocomplete) {
      autocomplete.show();
    }
  }

  getSimpleLocation(
    filter: string | null | undefined,
    organizationId: number | undefined,
    sorting: string | null | undefined,
    maxResultCount: number | undefined,
    skipCount: number | undefined
  ) {
    return this.locationService.getSimpleLocations(
      filter,
      organizationId,
      sorting,
      maxResultCount,
      skipCount
    );
  }

  onClickContinue(): void {
    this.router.navigateByUrl('/delivery-time');
  }

  chooseLocation(location) {
    this.localStorageService.saveDeliveryLocation(location);
    this.checkOutService.saveCustomerSessionData(SELECTED_LOCATION, JSON.stringify(location)).subscribe();

    if (OrderSetting.askTheMobileNumberFirst) {
      this.router.navigateByUrl('/phone-number');
    } else {
      const scanData = JSON.parse(window.sessionStorage.getItem(SCAN_QR_CODE));

      if (scanData) {
        if (this.department.isRequireOtp) {
          this.router.navigateByUrl(
            `/verify/${scanData.locationID}/${scanData.departmentId}/${scanData.tableID}`
          );
        } else {
          this.onChooseLanguage(location);
        }
      } else {
        this.onChooseLanguage(location);
      }
    }
  }

  onChooseLanguage(location): void {
    this.checkOutService.saveCustomerSessionData('LANGUAGE_DISPLAY', this.languages[0].code).subscribe();
    if (
      this.department.wheelOrderType !== 1 &&
      this.department.allowPreOrder &&
      !this.department.isOpeningHour &&
      this.department.dateDefaultOrderFututer != null
    ) {
      this.router.navigateByUrl(`/select-date-future`);
    } else {
      this.router.navigateByUrl(
        this.departmentId + `/location/${location.id}`
      );
    }
  }
}

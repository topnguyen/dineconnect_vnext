import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckOutService } from 'src/shared/service/check-out.service';
import moment from 'moment';
import { PAGE } from 'src/shared/constant';
import { AccountService, WheelSetupsService } from 'src/shared/service';
import { WheelOrderService } from 'src/shared/service/wheel-order.service';
import {
  DATE_TIME_DELIVERY,
  SELECTED_DEPARTMENT,
  SELECTED_LOCATION,
} from 'src/shared/shared.const';
import { LeftSideMenuComponent } from '../left-side-menu/left-side-menu.component';

@Component({
  selector: 'app-select-date-order-future',
  templateUrl: './select-date-order-future.component.html',
  styleUrls: ['./select-date-order-future.component.scss'],
})
export class SelectDateOrderFutureComponent implements OnInit {
  minDate = new Date();
  // maxDate = new Date();
  dateDefaultForOnlyTime = new Date();
  dataFututer = new Date();
  timeOrderFututer = new Date();
  timeValueDefault = new Date();
  maxDefault: Date = null;
  closeDays: any = [];
  leftSideMenu: LeftSideMenuComponent;
  logoUrl = '';
  selectedDepartment: any;
  constructor(
    public accountService: AccountService,
    private router: Router,
    private wheelOrderService: WheelOrderService,
    private checkOutService: CheckOutService
  ) {}

  ngOnInit(): void {
    this.checkOutService
      .getCustomerSessionData(SELECTED_DEPARTMENT)
      .subscribe((res) => {
        this.selectedDepartment = JSON.parse(res.result);

        let today = new Date();
        this.dataFututer = moment(
          this.selectedDepartment.dateDefaultOrderFututer
        ).toDate();
        this.timeValueDefault = this.dataFututer;
        let result = new Date(today);

        result.setDate(
          today.getDate() + this.selectedDepartment.futureOrderDaysLimit
        );

        this.maxDefault = result;
        this.closeDays = this.selectedDepartment.closeDayOfWeek;
      });
  }

  onSubmit() {
    if (!this.dataFututer && !this.timeOrderFututer) {
      return;
    }
    this.saveTimeOrderFututer();
    this.goToMenuOrder();
  }
  onClickBack(): void {
    this.wheelOrderService
      .getPageToBack(PAGE.DateOrderFututer, this.selectedDepartment.id, false)
      .subscribe(
        (res: any) => {
          switch (res.result) {
            case PAGE.Location:
              this.router.navigateByUrl('/location');
              break;
            case PAGE.PhoneNumber:
              this.router.navigateByUrl('/phone-number');
              break;
            case PAGE.Depaterment:
              this.router.navigateByUrl('/');
              break;
          }
        },
        (error) => {}
      );
  }
  goToMenuOrder() {
    let selectedDepartment = JSON.parse(
      window.localStorage.getItem(SELECTED_DEPARTMENT)
    );
    let selectedLocation = JSON.parse(
      window.localStorage.getItem(SELECTED_LOCATION)
    );
    this.router.navigateByUrl(
      `/location/${selectedLocation.id}/type/${selectedDepartment.id}`
    );
  }
  saveTimeOrderFututer() {
    let dateTimeOrderFuter = '';
    let dateDelivery = moment(this.dataFututer).format('YYYY-MM-DD');
    let timeDelivery = moment(this.timeOrderFututer).format('HH:mm');
    dateTimeOrderFuter = `${dateDelivery} ${timeDelivery}`;
    window.localStorage.setItem(DATE_TIME_DELIVERY, dateTimeOrderFuter);
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent implements OnInit {

  constructor() { }

  @Input() listItems = [];

  @Input() selected: number;
  @Output() selectedChange = new EventEmitter<Number>();

  ngOnInit(): void {
  }

  onEditSelectItem(id) {
    this.selectedChange.emit(id);
  }

  onAddItems($event) {

  }

  onRemoveItems($event) {

  }
}

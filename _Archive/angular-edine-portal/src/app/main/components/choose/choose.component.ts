import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.scss']
})
export class ChooseComponent implements OnInit {

  choosenList = [
    {
      imageUrl: 'assets/image/combos/combo-1/Layer-6.png',
      extraText: "Check Today's",
      name: 'Hot Deals',
      id: 0
    },
    {
      imageUrl: 'assets/image/location/location-1.png',
      extraText: "View All",
      name: 'Locations',
      id: 1
    },
    {
      imageUrl: 'assets/image/combos/combo-1/Layer-6.png',
      extraText: "Explore",
      name: 'Menu',
      id: 2
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

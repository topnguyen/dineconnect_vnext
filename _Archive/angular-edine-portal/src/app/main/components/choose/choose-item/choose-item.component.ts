import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'choose-item',
  templateUrl: './choose-item.component.html',
  styleUrls: ['./choose-item.component.scss']
})
export class ChooseItemComponent implements OnInit {

  @Input() listItems = [];

  @Input() selected: number;
  @Output() selectedChange = new EventEmitter<Number>();

  @Output() confirmItemSelected = new EventEmitter<{}>();

  constructor() { }

  ngOnInit(): void {
  }

  onSelectItem(id) {
    this.selectedChange.emit(id);
  }

  onEditSelectItem(item) {
    this.confirmItemSelected.emit(item);
  }
}

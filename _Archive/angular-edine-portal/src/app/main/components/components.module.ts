import { RippleModule } from 'primeng/ripple';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComboItemComponent } from './combo-item/combo-item.component';
import { ButtonModule } from 'primeng/button';
import { OrderItemComponent } from './order-item/order-item.component';
import { ChooseComponent } from './choose/choose.component';
import { ChooseItemComponent } from './choose/choose-item/choose-item.component';

@NgModule({
  declarations: [ComboItemComponent, OrderItemComponent, ChooseComponent, ChooseItemComponent],
  imports: [
    CommonModule,
    ButtonModule,
    RippleModule
  ],
  exports: [ComboItemComponent, OrderItemComponent, ChooseComponent]
})
export class ComponentsModule { }

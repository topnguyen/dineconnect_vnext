import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'combo-item',
  templateUrl: './combo-item.component.html',
  styleUrls: ['./combo-item.component.scss']
})
export class ComboItemComponent implements OnInit {

  constructor() { }

  @Input() listItems = [];
  @Input() isConfirmItems = false;

  @Input() selected: number;
  @Output() selectedChange = new EventEmitter<Number>();

  @Output() confirmItemSelected = new EventEmitter<{}>();

  ngOnInit(): void { }

  onSelectItem(id) {
    this.selectedChange.emit(id);
  }

  onEditSelectItem(item) {
    this.confirmItemSelected.emit(item);
  }
}

import { Component, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-collapse-questions',
  templateUrl: './collapse-questions.component.html',
  styleUrls: ['./collapse-questions.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class CollapseQuestionsComponent implements OnInit {
  @Input() questions: any[] = []; 
  constructor() { }

  ngOnInit(): void {}
}

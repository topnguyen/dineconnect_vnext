import { Router } from '@angular/router';
import { AccountService } from './../../../shared/service/account.service';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { wheelDataSetups } from './../../../shared/constant/constant';
import { CreateOrEditWheelDynamicPagesInput, WheelDynamicPagesService } from './../../../shared/service/wheel-dynamic-pages.service';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import grapesjs from 'grapesjs';
import { CommonService } from 'src/shared/common';
import { SearchCountryField, TooltipLabel, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { LOGO_URL } from 'src/shared/shared.const';
@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss'],
})
export class MainContentComponent implements OnInit, AfterViewInit, OnDestroy {
  items: MenuItem[]; logoUrl: string = '';
  editor: any;
  dynamicPage: CreateOrEditWheelDynamicPagesInput;
  isWheelSetupsAvailable: boolean;
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  formGroup: FormGroup;
  errInvalidEmailMsg = 'Invalid value. Please enter a valid email address.';
  errInvalidPhoneMsg = 'Invalid value. Please enter a valid phone number.';
  errUsernameMsg = 'Name is missing.';
  errEmailMsg = 'Email is missing.';
  errPhoneMsg = 'Phone is missing.';

  constructor(
    private commonService: CommonService,
    private fb: FormBuilder,
    private readonly accountService: AccountService,
    private readonly router: Router,
    private wheelDynamicPagesService: WheelDynamicPagesService
  ) {
    this.commonService.$isWith70Subject.next(true);
    this.logoUrl = window.sessionStorage.getItem(LOGO_URL);
    this.formGroup = this.fb.group({
      phoneNumber: [null, [Validators.required]]
    });
  }

  get phoneNumber(): AbstractControl {
    return this.formGroup.controls['phoneNumber'];
  }

  onSubmit(): void {
    this.phoneNumber.markAllAsTouched();
    this.phoneNumber.updateValueAndValidity();
    if (this.formGroup.invalid) {
      return;
    }
    this.accountService.savePhoneNumber(
      this.phoneNumber.value.e164Number,
      this.phoneNumber.value.dialCode,
    )
    this.router.navigateByUrl('/location')
  }

  initGrapes() {
    this.editor = grapesjs.init({
      container: "#gjs",
      fromElement: true,
      width: '100%',
      height: '100%',
      compoments: this.dynamicPage.components,
      panels: { defaults: [] },
      storageManager: { type: null },
    });

    this.editor.on('load', () => {
      this.editor.DomComponents.getComponents().each(c => {
        c.onAll(x => {
          x.set({
            editable: false,
            selectable: false,
            hoverable: false,
            highlightable: false,
            droppable: false,
            draggable: false,
          })
        })
      })
    });
  }

  ngOnInit(): void {
    this.dynamicPage = new CreateOrEditWheelDynamicPagesInput();
    this.isWheelSetupsAvailable = this.commonService.isWheelSetupsAvailable;
    if (this.isWheelSetupsAvailable) {
      // this.getWheelDynamicPages();
    }

    // if (this.accountService.userValue) {
    //   this.router.navigateByUrl('/location');
    // }

    // this.items = [
    //   {
    //     label: 'Home',
    //     routerLink: '/',
    //     routerLinkActiveOptions: { exact: true },
    //   },
    //   {
    //     label: 'Order Online',
    //     routerLink: '/',
    //     routerLinkActiveOptions: { exact: true },
    //   },
    // ];
  }

  ngAfterViewInit(): void {
    // this.initGrapes();
    this.phoneNumber.markAsPristine();
  }
  ngOnDestroy(): void {
    this.commonService.$isWith70Subject.next(false);
  }

  required(control: AbstractControl): { [key: string]: any } | null {
    if (!control.touched) {
      return null
    }
    if (!control.value) {
      return { 'required': true }; // return object if the validation is not passed.
    } else {
      const controlValue: string = control.value.trim();
      if (!controlValue.length) {
        return { 'required': true }; // return object if the validation is not passed.
      }
    }
    return null; // return null if validation is passed.
  }

}

import { IResolveTenantIdInput } from './../../../../model/reset-password.model';
import { MessageService } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/shared/service';
import { IResetPasswordInput, IUser } from 'src/model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { confirmPasswordValidators } from 'src/shared/custom-validation';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  passwordForm: any;
  showResetPass = false;
  user: IUser;
  cParams: string;
  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}
  resetPasswordModel: IResetPasswordInput = {
    password: null,
  };

  ngOnInit(): void {
    this.createChangePasswordForm();
    this.cParams = this.activatedRoute.snapshot.queryParamMap.get('c');
    // show this component when access via email link address
    if (this.cParams) {
      this.resetPasswordModel.c = this.cParams;
      this.showResetPass = true;
      this.accountService
        .resolveTenantId({ c: this.resetPasswordModel.c })
        .subscribe();
    }
    // other else redirect to home page
    if (this.router.url == '/account/reset-password' && !this.cParams) {
      this.router.navigate(['']);
    }
    this.accountService.$changePassword.subscribe((value) => {
      if (value) {
        this.showResetPass = true;
        this.resetPasswordModel = {
          userId: value.userId,
          resetCode: value.resetCode,
          password: null,
        };
      }
    });
  }
  closePopup() {
    this.showResetPass = false;
    this.passwordForm.reset();
    this.accountService.$isOpenAccountComponent.next(false);
    if(this.cParams){
      this.router.navigate(['']);
    }
  }
  createChangePasswordForm() {
    this.passwordForm = this.formBuilder.group(
      {
        newPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      { validators: confirmPasswordValidators }
    );
  }
  get fp() {
    return this.passwordForm.controls;
  }
  resetPassword() {
    if (this.passwordForm.valid) {
      const newPassForm = this.passwordForm.getRawValue();
      this.resetPasswordModel.password = newPassForm.newPassword;
      this.accountService
        .resetPassword(this.resetPasswordModel)
        .subscribe((res) => {
          if (res.success) {
            if (!res.result.canLogin) {
              this.accountService.$isOpenAccountComponent.next(true);
            } else {
              this.accountService
                .signIn(
                  res.result.userName,
                  this.resetPasswordModel.password,
                  true
                )
                .subscribe((response) => {
                  if (response.success) {
                    if (response.result.accessToken) {
                      const user = this.accountService.tempInfomationUser;
                      if (!user) {
                        this.messageService.clear();
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Service Message',
                          detail:
                            'Reset Password Successfull.Please try to login again.',
                        });
                        this.closePopup();
                        this.router.navigate(['']);
                      } else {
                        this.messageService.clear();
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Service Message',
                          detail: 'Login Successful',
                        });
                        this.closePopup();
                        user.token = response.result.accessToken;
                        this.accountService.saveUser(user);
                        this.router.navigateByUrl(
                          this.accountService.returnUrl
                        );
                      }
                    }
                  }
                });
            }
          }
        });
    }
  }
}

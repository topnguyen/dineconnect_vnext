import { IUser, ICreateOrUpdateUserInput } from './../../../model/user.model';
import { Component, OnInit } from '@angular/core';
import { AlertService, AccountService } from 'src/shared/service';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  AbstractControl,
} from '@angular/forms';
import {
  emailCustomValidators,
  confirmPasswordValidators,
} from 'src/shared/custom-validation';
import { AutoUnsubscribe } from 'src/shared/common/config/unsubscribe/auto-unsubscribe';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { TenantService } from 'src/app/tenant/tenant.service';
import { ToastService } from 'src/shared/toast';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
@AutoUnsubscribe()
export class AccountComponent implements OnInit {
  isOpenAccount = false;
  registerForm: FormGroup;
  phoneEmailForm: FormControl;
  regex = /^(?:\d{6,11}|\w+@\w+\.\w{2,3})$/;
  emailRegex = /^(\w+@\w+\.\w{2,3})$/;
  errMsg = 'Invalid value. Please enter a valid email address.';
  errMsg1 =
    'Invalid value. Please enter a valid  mobile number or email address.';

  currentStep = 1;
  formAccess: FormGroup;
  loading: boolean = false;
  user: IUser;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private toastService: ToastService,
    private tenantService: TenantService
  ) { }

  ngOnInit(): void {
    this.createForm();
    this.createFormAccess();
    this.createRegisterUserForm();
    this.accountService.$isOpenAccountComponent.subscribe((val) => {
      this.resetForm();
      this.isOpenAccount = val;
      this.accountService.returnUrl =
        this.route.snapshot.queryParams['returnUrl'] || '/';
    });
  }

  get cssCurrentStep() {
    return this.currentStep == 1 ? 'step_one' : 'step_two';
  }

  get password() {
    return this.formAccess.controls.password;
  }
  
  get registry() {
    return this.registerForm.controls;
  }

  getError(f: AbstractControl) {
    return f.errors && (f.touched || f.dirty);
  }
  
  closePopup() {
    this.resetForm();
    this.accountService.$isOpenAccountComponent.next(false);
  }

  resetForm() {
    this.currentStep = 1;

    this.phoneEmailForm.reset();
    this.formAccess.reset();
    this.registerForm.reset();
  }

  continueLogin() {
    this.phoneEmailForm.markAllAsTouched();
    this.phoneEmailForm.updateValueAndValidity();
    if (this.phoneEmailForm.valid) {
      this.accountService
        .getUsers(this.phoneEmailForm.value)
        .subscribe((value) => {
          if (value.result && value.result.totalCount == 1) {
            this.currentStep = 2;
            this.user = value.result.items[0];
            this.accountService.tempInfomationUser = this.user;
            this.formAccess.patchValue(this.user);
          } else if (value.result && value.result.totalCount == 0) {
            this.registerForm.controls.emailAddress.setValue(
              this.phoneEmailForm.value
            );
            this.currentStep = 3;
          }
        });
    }
  }

  signIn() {
    const { emailAddress, password } = this.formAccess.value;
    this.alertService.clear();
    if (this.formAccess.invalid) {
      return;
    }
    this.loading = true;
    this.accountService.signIn(emailAddress, password, true).subscribe(
      (response) => {
        if (response.success) {
          if (response.result.accessToken) {
            this.toastService.clear();
            this.toastService.showSuccess('Login Successful')
            this.closePopup();
            this.user.token = response.result.accessToken;
            this.accountService.saveUser(this.user);
            this.router.navigateByUrl(this.accountService.returnUrl);
          } else if (response.result.shouldResetPassword) {
            // Password reset
            const tenant = this.tenantService.getTenantAvailable();
            const changePasswordModel = {
              userId: response.result.userId,
              tenantId: tenant ? tenant.tenantId : '1',
              resetCode: response.result.passwordResetCode,
              returnUrl: this.accountService.returnUrl,
            };
            this.accountService.$changePassword.next(changePasswordModel);
          }
        }
      },
      (error) => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

  registryUser() {
    this.registerForm.markAllAsTouched();
    this.registerForm.updateValueAndValidity();
    if (this.registerForm.valid) {
      const formValue = this.registerForm.getRawValue();
      let userCreate: IUser = {
        emailAddress: '',
        id: null,
        isActive: true,
        isLockoutEnabled: true,
        isTwoFactorEnabled: false,
        name: '',
        password: '',
        phoneNumber: null,
        shouldChangePasswordOnNextLogin: true,
        surname: '',
        userName: '',
      };
      userCreate = { ...userCreate, ...formValue };
      userCreate.password = formValue.newPassword;
      let updateUser: ICreateOrUpdateUserInput = {
        user: userCreate,
        organizationUnits: [],
        sendActivationEmail: true,
        setRandomPassword: false,
        assignedRoleNames: ['User'],
      };
      this.accountService.createOrUpdateUser(updateUser).subscribe((res) => {
        if (res.success && res.result != 0) {
          this.closePopup();
          this.messageService.clear();
          this.messageService.add({
            severity: 'success',
            summary: 'Service Message',
            detail: 'Register account Successful.',
          });
          this.accountService.$isOpenAccountComponent.next(true);
        }
      });
    }
  }
  forgotPassword() {
    const emailAddress = this.formAccess.controls.emailAddress.value;
    this.accountService
      .sendPasswordResetCode({ emailAddress })
      .subscribe(() => {
        this.messageService.clear();
        this.messageService.add({
          severity: 'success',
          summary: 'Service Message',
          detail:
            'A password reset link sent to your e-mail address. Please check your emails.',
        });
      });
  }

  createForm() {
    this.phoneEmailForm = new FormControl('', [
      Validators.required,
      emailCustomValidators(this.emailRegex),
    ]);
  }
  
  createFormAccess() {
    this.formAccess = this.formBuilder.group({
      emailAddress: new FormControl(''),
      password: new FormControl('', [Validators.required]),
      name: new FormControl(''),
    });
  }

  createRegisterUserForm() {
    this.registerForm = this.formBuilder.group(
      {
        name: new FormControl('', [Validators.required]),
        surname: new FormControl('', [Validators.required]),
        emailAddress: new FormControl('', [
          Validators.required,
          emailCustomValidators(this.emailRegex),
        ]),
        phoneNumber: new FormControl('', [
          Validators.required,
          Validators.pattern('[0-9]+'),
          Validators.minLength(6),
          Validators.maxLength(15),
        ]),
        userName: new FormControl('', [Validators.required]),
        newPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      { validators: confirmPasswordValidators }
    );
  }
}

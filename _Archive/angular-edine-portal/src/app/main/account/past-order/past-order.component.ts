import { switchMap, map, mergeAll, mergeMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { CheckOutService } from 'src/shared/service/check-out.service';
import { IOrderDetail } from './../../../../model/past-order.model';
import * as moment from 'moment';
import { LocationService } from 'src/shared/service';

@Component({
  selector: 'app-past-order',
  templateUrl: './past-order.component.html',
  styleUrls: ['./past-order.component.scss'],
})
export class PastOrderComponent implements OnInit {
  orderDetails: IOrderDetail[] = [];
  customtTableView;
  cols = [
    { field: 'createdTime', header: 'Creation Time' },
    { field: 'price', header: 'Amount' },
    { field: 'deliveryLocation', header: 'Delivery Location' },
    { field: 'building', header: 'Building' },
    { field: 'street', header: 'Street' },
    { field: 'flatNo', header: 'FlatNo' },
    { field: 'landmark', header: 'Landmark' },
  ];
  display: boolean;
  orderList: any;
  itemDetail: IOrderDetail;
  constructor(
    private checkOutService: CheckOutService,
    private locationService: LocationService
  ) {}

  ngOnInit(): void {
    this.getPastOrder();
  }

  getLocationById(id) {
    return this.locationService.getLocationById(id);
  }
  viewDetails(item: IOrderDetail) {
    this.display = true;
    this.itemDetail = item;
    this.orderList = JSON.parse(item.orderDetails);
  }
  getPastOrder() {
    this.checkOutService.getPastOrder().subscribe((res) => {
      if (res.success) {
        this.orderDetails = res.result.items.map((x) => x.orderDetail);
        this.customtTableView = this.orderDetails.map((x) => {
          this.getLocationById(x.deliveryLocationId).subscribe((res) => {
            x['price'] = `${x.amount} ${x.currency}`;
            x['createdTime'] = moment(x.creationTime).format(
              'MMM Do YY, h:mm:ss a'
            );
            if (res.success) {
              x['deliveryLocation'] = res.result.name;
            }
          });
        });
      }
    });
  }
}

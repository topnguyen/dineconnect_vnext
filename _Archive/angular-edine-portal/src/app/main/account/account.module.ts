import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountLayoutComponent } from './account-layout/account-layout.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { MenuModule } from 'primeng/menu';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PastOrderComponent } from './past-order/past-order.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountRoutingModule,
    BreadcrumbModule,
    MenuModule,
    FormsModule,
    TableModule,
    DialogModule,
    InputTextModule,
  ],
  declarations: [
    AccountLayoutComponent,
    UserProfileComponent,
    PastOrderComponent,
  ],
  exports: [
    InputTextModule
  ]
})
export class AccountModule { }

import { MenuItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/shared/service';

@Component({
  selector: 'app-account-layout',
  templateUrl: './account-layout.component.html',
  styleUrls: ['./account-layout.component.scss'],
})
export class AccountLayoutComponent implements OnInit {
  items: MenuItem[];
  accountMenu: MenuItem[];
  constructor(private router: Router, private accountService: AccountService) {
    // redirect to home if already logged in
    if (this.accountService.userValue) {
      this.router.navigate([this.router.url]);
    }
  }

  ngOnInit(): void {
    this.accountMenu = [
      { label: 'Profile', icon: 'pi pi-fw pi-user', routerLink: '/account'},
      { label: 'Past Order', icon: 'pi pi-fw pi-file-o', routerLink:'/account/past-order'},
    ];
    this.items = [
      {
        label: 'Home',
        routerLink: '/',
        routerLinkActiveOptions: { exact: true },
      },
      {
        label: 'Order Online',
        routerLink: '/',
        routerLinkActiveOptions: { exact: true },
      },
      {
        label: 'Account',
        routerLink: '',
        routerLinkActiveOptions: { exact: true },
      },
    ];
  }
}

import {
  IUser,
  ICreateOrUpdateUserInput,
  IUserRole,
} from './../../../../model/user.model';
import { AccountService } from 'src/shared/service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessageService } from 'primeng/api';
import { confirmPasswordValidators } from 'src/shared/custom-validation';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  userEditForm: FormGroup;
  roles: IUserRole[];
  showChangePassword: boolean;
  passwordForm: FormGroup;
  userEditValue: IUser;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.createChangePasswordForm();
    this.getUserForEdit();
  }

  get f() {
    return this.userEditForm.controls;
  }

  get fp() {
    return this.passwordForm.controls;
  }

  createChangePasswordForm() {
    this.passwordForm = this.formBuilder.group(
      {
        newPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      { validators: confirmPasswordValidators }
    );
  }

  getUserForEdit() {
    this.accountService
      .getUserForEdit(this.accountService.userValue.id)
      .subscribe((value) => {
        if (value.success) {
          this.roles = value.result.roles;
          this.userEditValue = value.result.user;
          this.userEditForm.patchValue(value.result.user);
        }
      });
  }

  updateUser(updateUserValue: IUser = null) {
    if (!updateUserValue) {
      updateUserValue = this.userEditForm.getRawValue();
    }
    const roleNnames = ['User'];

    let updateUser: ICreateOrUpdateUserInput = {
      user: updateUserValue,
      organizationUnits: [],
      sendActivationEmail: false,
      setRandomPassword: false,
      assignedRoleNames: roleNnames,
    };
    this.accountService.createOrUpdateUser(updateUser).subscribe((res) => {
      if (res.success) {
        const currentUser = this.accountService.userValue;
        let user = { ...updateUser.user };
        user.token = currentUser.token;
        this.accountService.userSubject.next(user);
        this.closePopup();
        this.messageService.clear();
        this.messageService.add({
          severity: 'success',
          summary: 'Service Message',
          detail: 'Update User Successful',
        });
      }
    });
  }

  saveChanges() {
    if (this.userEditForm.valid) {
      this.updateUser();
    } else {
      this.messageService.clear();
      this.messageService.add({
        severity: 'warn',
        detail: 'Please enter all required field',
      });
    }
  }

  getEditUser() {
    this.accountService.getUserFromLocal();
  }

  createForm(user?: IUser) {
    this.userEditForm = this.formBuilder.group({
      id: new FormControl(user ? user.id : ''),
      name: new FormControl(user ? user.name : '', [Validators.required]),
      surname: new FormControl(user ? user.surname : '', [Validators.required]),
      emailAddress: new FormControl(user ? user.emailAddress : ''),
      phoneNumber: new FormControl(user ? user.phoneNumber : '', [
        Validators.required,
      ]),
      isActive: true,
      isLockoutEnabled: true,
      isTwoFactorEnabled: false,
      password: null,
      shouldChangePasswordOnNextLogin: true,
      userName: null,
    });
  }

  changePassword() {
    this.showChangePassword = true;
  }

  closePopup() {
    this.showChangePassword = false;
    this.passwordForm.reset();
  }
  
  saveChangePassword() {
    if (this.passwordForm.valid) {
      const newPassForm = this.passwordForm.getRawValue();
      this.userEditValue.password = newPassForm.newPassword;
      this.updateUser(this.userEditValue);
    }
  }
}

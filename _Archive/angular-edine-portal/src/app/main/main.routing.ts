import { LanguageComponent } from './language/language.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/shared/common/authenticate/auth.guard';
import { LocationComponent } from './location/location.component';
import { LocationGuard, MainGuard } from 'src/shared/guard';
import { DepartmentComponent } from './department/department.component';
import { ComboComponent } from './combo/combo.component';
import { AnimationRouteItems } from 'src/shared/constant';
import { MyOrderComponent } from './my-order/my-order.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ChooseComponent } from './components/choose/choose.component';
import { GuestInfoComponent } from './guest-info/guest-info.component';
import { MenuComponent } from './menu/menu.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { VerifyComponent } from './verify-code/verify-code.component';
import { PaymentsComponent } from './payments/payments.component';
import { PaymentsInformationComponent } from './payments-information/payments-information.component';
import { TableListComponent } from './table-list/table-list.component';
import { InputPostalCodeComponent } from './input-postal-code/input-postal-code.component';
import { CustomerQueueComponent } from './customer-queue/customer-queue.component';
import { QueueDashboardComponent } from './customer-queue/queue-dashboard/queue-dashboard.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { FAQsComponent } from './faqs/faqs.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SelectDateOrderFutureComponent } from './select-date-order-future/select-date-order-future.component';

const accountModule = () => import('./account/account.module').then((x) => x.AccountModule);

const profileModule = () => import('./my-profile/my-profile.module').then((x) => x.MyProfileModule);

export const routes: Routes = [
  { path: '', component: DepartmentComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'phone-number', component: WelcomeComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'phone-number/:type', component: WelcomeComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'terms-and-conditions', component: TermsAndConditionsComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'privacy-policy', component: PrivacyPolicyComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'faqs', component: FAQsComponent, canActivate: [], data: { animation: AnimationRouteItems.home } },
  { path: 'language', component: LanguageComponent, data: { animation: AnimationRouteItems.location } },
  { path: ':departmentId/postal-code', component: InputPostalCodeComponent, data: { animation: AnimationRouteItems.location } },
  { path: 'table-list', component: TableListComponent, canActivate: [], data: { animation: AnimationRouteItems.location } },
  { path: 'select-date-future', component: SelectDateOrderFutureComponent, canActivate: [], data: { animation: AnimationRouteItems.location } },
  { path: 'verify/:locationId/:departmentId/:tableId', component: VerifyComponent, canActivate: [], data: { animation: AnimationRouteItems.location } },
  { path: 'verify/:locationId/:departmentId/:tableId/:phoneNumber', component: VerifyComponent, canActivate: [], data: { animation: AnimationRouteItems.location } },
  { path: ':departmentId/location', component: LocationComponent, canActivate: [], data: { animation: AnimationRouteItems.location } },
  { path: ':departmentId/location/:locationId', component: MenuComponent, canActivate: [LocationGuard], data: { animation: AnimationRouteItems.menuItems } },
  { path: 'account', loadChildren: accountModule, canActivate: [MainGuard,] },
  { path: 'choose', component: ChooseComponent, canActivate: [MainGuard,] },
  { path: 'guest-info', component: GuestInfoComponent, canActivate: [MainGuard,] },
  { path: 'sign-in', component: SignInComponent, canActivate: [MainGuard,], data: { animation: AnimationRouteItems.login } },
  { path: 'sign-up', component: SignUpComponent, canActivate: [MainGuard,], data: { animation: AnimationRouteItems.signUp } },
  { path: 'my-profile', loadChildren: profileModule, canActivate: [MainGuard,], data: { animation: AnimationRouteItems.myProfile } },
  { path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard, MainGuard,] },
  { path: 'payment', component: PaymentsComponent, canActivate: [AuthGuard, MainGuard,] },
  { path: 'payment-information/:id', component: PaymentsInformationComponent, canActivate: [AuthGuard, MainGuard,] },
  { path: 'my-order', component: MyOrderComponent },
  { path: 'menu', component: MenuComponent, canActivate: [MainGuard,] },
  { path: 'order-status/:id', component: OrderStatusComponent, canActivate: [MainGuard,] },
  { path: 'queue/:id', component: CustomerQueueComponent, canActivate: [MainGuard,] },
  { path: 'queue/:id/dashboard', component: QueueDashboardComponent, canActivate: [MainGuard,] },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class MainRoutingModule { }

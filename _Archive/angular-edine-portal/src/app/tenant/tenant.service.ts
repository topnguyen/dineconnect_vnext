import { LocalStorageService } from './../../shared/common/local-storage/local-storage.service';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import {
  IIsTenantAvailableInput,
  IPageResult,
  IIsTenantAvailableOutput,
} from 'src/model';
import { Observable } from 'rxjs';
import { HttpOptions, HttpClientService } from 'src/shared/common';
import { CONSTANT } from 'src/shared/constant';

@Injectable({
  providedIn: 'root',
})
export class TenantService {
  tenantsName = environment.tenantsName;
  constructor(private http: HttpClientService, private localStorageService :LocalStorageService) {}

  getTenantByHostname(hostname: string): string {
    return this.getTenantByHost(hostname.split('.')[0]);
  }

  getTenantByHost(host: string): string {
    return this.getTenantByString(host);
  }

  getTenantByString(s: string): string {
    for (const e in this.tenantsName) {
      if (this.tenantsName[e].toLowerCase() === s.toLowerCase()) {
        return this.tenantsName[e];
      }
    }
    return null;
  }

  getTenant(): string {
    return this.getTenantByHostname(location.hostname);
  }

  addTenantToHeader(headers: HttpHeaders): HttpHeaders {
    return headers.append('tenancyName', this.getTenant());
  }

  isTenantAvailable(
    body: IIsTenantAvailableInput | undefined
  ): Observable<IPageResult<IIsTenantAvailableOutput>> {
    let url_ = '/api/services/app/Account/IsTenantAvailable';
    url_ = url_.replace(/[?&]$/, '');

    const content_ = JSON.stringify(body);

    const postContent: HttpOptions = {
      url: url_,
      body: content_,
      cacheMins: 60,
      specificCacheKey: CONSTANT.TENANT_AVAILABLE_KEY.toString(),
    };

    return this.http.post(postContent);
  }

  getTenantAvailable(): IIsTenantAvailableOutput{
    const item = this.localStorageService.getByKey(CONSTANT.TENANT_AVAILABLE_KEY);
    
    if (item != '' && item != null) {
      const record = item;
      const now = new Date().getTime();
      if (!record || (record.hasExpiration && record.expiration <= now)) {
        return null;
      } else {
        const  tenantAvailable =  JSON.parse(record.value);
        return tenantAvailable.result
      }
    }
    return null;
  }
}

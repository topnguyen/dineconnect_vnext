import { IOrganizationUnit } from './IOrganizationUnit.model';

export interface IUser {
  name: string | undefined;
  surname: string | undefined;
  userName: string | undefined;
  emailAddress: string | undefined;
  phoneNumber: string | undefined;
  phonePrefix?: string | undefined;
  profilePictureId?: string | undefined;
  isEmailConfirmed?: boolean;
  roles?: IUserListRole[] | undefined;
  isActive: boolean;
  creationTime?: moment.Moment;
  id: number;
  token?: string;
  shouldChangePasswordOnNextLogin: boolean;
  isTwoFactorEnabled: boolean;
  isLockoutEnabled: boolean;
  password?: string | null;
}

export interface IUserListRole {
  roleId: number;
  roleName: string | undefined;
}

export interface IUserRole {
  roleId: number;
  roleName: string | undefined;
  roleDisplayName: string | undefined;
  isAssigned: boolean;
  inheritedFromOrganizationUnit: boolean;
}

export interface IUserForEditOutput {
  profilePictureId: string | undefined;
  user: IUser;
  roles: IUserRole[] | undefined;
  allOrganizationUnits: IOrganizationUnit[] | undefined;
  memberedOrganizationUnits: string[] | undefined;
}
export interface ICreateOrUpdateUserInput {
  user: IUser;
  assignedRoleNames: string[] | undefined;
  sendActivationEmail: boolean;
  setRandomPassword: boolean;
  organizationUnits: number[] | undefined;
}

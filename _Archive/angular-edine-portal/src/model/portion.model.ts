export interface IPortion {
  id: number;
  name: string;
  multiPlier: number;
  price: number;
  menuItemId: number;
  checked?: boolean | null;
}

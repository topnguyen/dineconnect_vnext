export interface ILocationGroup {
  name: string | undefined;
  code: string | undefined;
  id: number;
}

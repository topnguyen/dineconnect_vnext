export interface ICartModel {
  note: string,
  key: string;
  name: string;
  description: string;
  image: string;
  comboId?: number;
  totalPrice: number;
  totalItems: number;
  pricePerItem: number;
  orderItems: IOrderItem[];
  recommendedItems: IRecommendItem[];
}

export interface IOrderItem {
  id: number;
  sectionComboId?: number;
  name: string;
  description: string;
  image: string;
  totalPrice: number;
  addPrice: number;
  tags: IOrderItemTag[];
  portion: IPortion;
  note?: string;
}

export interface IOrderItemTag {
  id: number;
  description: string;
  name: string;
  price: number;
  quantity: number;
  tagGroupName: string;
  tagGroupId: number;
}

export interface IPortion {
  name: string;
  price: number;
}
export interface IRecommendItem {
  id: number;
  name: string;
}

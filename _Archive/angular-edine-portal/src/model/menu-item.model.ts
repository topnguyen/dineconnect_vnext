import { CustomMenuItemData } from "./custom-menu-item.model";

export interface MenuItemData {
  name: string;
  published: boolean;
  image: string;
  price: number;
  order: number;
  type: number;
  parentId: number;
  screenMenuId: number;
  displayAs: number;
  portions: Portions[];
  orderTags: OrderTags[];
  lastModificationTime: moment.Moment;
  lastModifierUserId: number;
  creationTime: moment.Moment;
  creatorUserId: number;
  id: number;
  items: MenuItemData[];
  sectionChildren: MenuItemData[];
  wheelMenuCombos: WheelMenuCombos[];
  isComboItem?: boolean;
  wheelMenuComboName?: string;
  tagLabel?: string;
  note?: string;
  total?: number;
  addPrice?: number;
  totalDisplay?: number;
  totalPricePerItem?: number;
  recommendedItems?: IRecommendItem[];
  tags?: string[];
  description: string;
  displayNutritionFacts: boolean;
  calories: string;
  nutritionInfo: string;
  quantity?: number;
  autoSelect?: boolean;
  addSeperately?: boolean;
  selected: boolean;
  numberOfColumns:number;
}

export interface Portions {
  name: string;
  multiplier: number;
  price: number;
  itemId: number;
  tenantId: number;
  lastModificationTime: Date;
  lastModifierUserId: number;
  creationTime: Date;
  creatorUserId: number;
  checked: boolean;
  id: number;
}

export interface OrderTags {
  id: number;
  name: string;
  sortOrder: number;
  maxSelectedItems: number;
  minSelectedItems: number;
  addTagPriceToOrderPrice: boolean;
  saveFreeTags: boolean;
  freeTagging: boolean;
  taxFree: boolean;
  prefix: string;
  displayLocation: string;
  locations: string;
  departments: string;
  displayDepartments: string;
  tags: Tag[];
  isShowOrderTags?: boolean;
}

export interface Tag {
  id: number;
  name: string;
  alternateName: string;
  price: number;
  menuItemId: number;
  maxQuantity: number;
  sortOrder: number;
  orderTagGroupId: number;
  tagGroupName: string;
  checked: boolean;
  quantity: number;
}

export interface MenuActiveInfo {
  menuIdActive: number;
  categoryIdActive: number;
}

export interface WheelMenuCombos {
  id: number;
  basePrice: number;
  description: string;
  dynamic: string;
  image: string;
  name: string;
  note?: string;
  published: boolean;
  screenCategoryId: number;
  screenMenuId: number;
  wheelMenuComboItems: WheelMenuComboItems[];
  recommendedItems: IRecommendItem[];
  portions: Portions[];
  tags?: string[];
  states?: any;
}

export interface WheelMenuComboItems {
  id: number;
  description: string;
  dynamic: string;
  name: string;
  wheelMenuComboId: number;
  wheelMenuCombosItemMenus: MenuItemData[];
  itemSelect?: CustomMenuItemData[];
  expanded?: boolean;
  minimum?: number;
  maximum?: number;
  isValid: boolean;
  errors : string[];
}

export interface IRecommendItem {
  recommendedItemId: number;
  recommendedItemName: string | undefined;
}

export interface IPostalCodeResponse {
  IsSuccess: boolean;
  ErrorCode: number;
  ErrorDetails: string;
  Postcodes: IPostalCodeModel[];
}

export interface IPostalCodeModel {
  Postcode: number;
  BuildingNumber: string;
  StreetName: string;
  BuildingName: string;
  BuildingCode: string;
  BuildingDescription: string;
  Longitude: number;
  Latitude: number;
  Floors: IFloorModel[];
}

export interface IFloorModel {
  FloorNumber: string;
  Units: IUnitNumber[];
}

export interface IUnitNumber {
  UnitNumber: string;
}

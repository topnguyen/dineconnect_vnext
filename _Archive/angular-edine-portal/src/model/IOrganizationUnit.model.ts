export interface IOrganizationUnit {
  parentId: number | undefined;
  code: string | undefined;
  displayName: string | undefined;
  memberCount: number;
  roleCount: number;
  lastModificationTime: moment.Moment | undefined;
  lastModifierUserId: number | undefined;
  creationTime: moment.Moment;
  creatorUserId: number | undefined;
  id: number;
}

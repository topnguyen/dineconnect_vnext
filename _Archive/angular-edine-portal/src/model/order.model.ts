export interface OrderModel {
  order: PurcharsedOrder;
  wheelBillingInfo: WheelBillingInfo;
  paymentOption: string;
  paidAmount: number;
  invoiceNo: string;
  orderTime: Date;
  dateFrom?: Date;
  dateTo?: Date;
  note?: string;
  deliveryTime: number;
}

export interface PurcharsedOrder {
  wheelCustomerProductOfferId: number;
  orderDate: Date;
  amount: number;
  quantity: number;
  address: string;
  addOn: string;
  customerAddressId: number;
  menuItemsDynamic: string;
  wheelOrderType: number;
  deliveryCharge: number;
  selfPickupLocationId: number;
  paymentId: number;
  deliveryTime: string;
  note: string;
}

export interface WheelBillingInfo {
  name: string;
  emailAddress: string;
  phoneNumber: string;
  phonePrefix?: string;
  address1: string;
  address2: string;
  address3: string;
  city: string;
  country: string;
  cityId: number;
  countryId: number;
  postcalCode: string;
  zone: number;
  lat: string;
  long: string;
  deliveryFee: number;
  wheelServiceFees: string;
  tax: number;
}

export interface ISimpleLocation {
    id: number;
    name: string | undefined;
    code: string | undefined;
    organizationId: number;
    organizationUnitId: number;
}
export interface IAuthenticateModel {
  userNameOrEmailAddress: string | undefined;
  password: string | undefined;
  twoFactorVerificationCode?: string | undefined;
  rememberClient: boolean;
  twoFactorRememberClientToken?: string | undefined;
  singleSignIn?: boolean | undefined;
  returnUrl?: string | undefined;
  captchaResponse?: string | undefined;
}

export interface IAuthenticateResultModel {
  accessToken: string | undefined;
  encryptedAccessToken: string | undefined;
  expireInSeconds: number;
  shouldResetPassword: boolean;
  passwordResetCode: string | undefined;
  userId: number;
  requiresTwoFactorVerification: boolean;
  twoFactorAuthProviders: string[] | undefined;
  twoFactorRememberClientToken: string | undefined;
  returnUrl: string | undefined;
  refreshToken: string | undefined;
  refreshTokenExpireInSeconds: number;
}

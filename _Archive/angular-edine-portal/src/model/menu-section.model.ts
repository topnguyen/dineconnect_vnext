export interface MenuSection {
  id: number;
  name: string;
}

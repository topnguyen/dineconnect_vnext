export interface IProductComboGroupEdit {
  id: number | undefined;
  name: string | undefined;
  sortOrder: number;
  minimum: number;
  maximum: number;
  comboItems: IProductComboItemEdit[] | undefined;
}

export interface IProductComboItemEdit {
  id: number | undefined;
  name: string | undefined;
  menuItemId: number;
  autoSelect: boolean;
  price: number;
  sortOrder: number;
  count: number;
  addSeperately: boolean;
}

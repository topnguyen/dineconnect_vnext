import { WheelScreenMenuItemType } from 'src/shared/service-proxies/service-proxies';

export interface LeftMenu {
  name: string | undefined;
  published: boolean;
  image: string | undefined;
  price: number;
  order: number;
  type: WheelScreenMenuItemType;
  parentId: number | undefined;
  screenMenuId: number;
  sectionChildren: LeftMenu[];
  lastModificationTime: moment.Moment | undefined;
  lastModifierUserId: number | undefined;
  creationTime: moment.Moment;
  creatorUserId: number | undefined;
  id: number;
}

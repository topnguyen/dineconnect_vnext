export const environment = {
  production: true,
  apiUrl: '//localhost:22742',
  serviceUserAccount: 'admin',
  servicePasswordAccount: '123qwe',
  tenantsName: [''],
  phoneNumber: 123456789,
  postalCodeUrl:'https://www.sglocate.com/api/json/searchwithblocknumberandstreetname.aspx',
  postalCodeApiKey:'',
  posttalCodeApiSecret:'',
  systemCurrency: 'USD',
  OMISE_PUBLIC_KEY: '',
  RATIO_CURRENCY: 100,
  paymentOption: 'omise',
  appConfig: 'appconfig.prod.json'
};

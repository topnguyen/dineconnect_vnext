# AngularCustomerPortal

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Setting for production

*Must be setting up some variables in environment.prod.ts file

-`apiUrl`: url to backend server
-`serviceUserAccount` and `servicePasswordAccount`: tenant admin's account and password for authentication api
-`tenantsName` is set put in string array `['']`: for validation tenant name in url
-`phoneNumber` - this field can be nullable: this variable for display phone number of this site in header
-`postalCodeApiKey` and `posttalCodeApiSecret`: put api key and secret key of the registed account in the postal code link
-`systemCurrency`: default is `USD` - used for display and payment currency in this site
-`OMISE_PUBLIC_KEY`: Omise public key for payment
-`RATIO_CURRENCY`: the smallest unit of currency of `systemCurrency` default is `100`: this variable is used for payment
*Should not be changed
-`postalCodeUrl`: has default value - link of the site for getting postal code
-`paymentOption`: default is `omise` - this is payment method in system now.
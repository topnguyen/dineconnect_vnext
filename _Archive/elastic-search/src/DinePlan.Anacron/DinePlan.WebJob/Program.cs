﻿using DinePlan.Anacron.Core.Common;
using DinePlan.Anacron.Core.Queue;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using DinePlan.Anacron.Core.Managers;
using DinePlan.Anacron.Core.Managers.Impl;

namespace DinePlan.WebJob
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var logger = new LoggerConfiguration()
                .WriteTo.File("logs/webjob.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
                  
            var builder = new HostBuilder();
            //var builder = Host.CreateDefaultBuilder(args);

            try
            {
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                builder.UseEnvironment("development");
                builder.ConfigureWebJobs(b =>
                {
                    b.AddAzureStorageCoreServices();
                });
                builder.ConfigureLogging((context, b) =>
                {
                    b.AddConsole();
                });
                builder.ConfigureAppConfiguration((hostContext, configApp) =>
                 {
                     configApp
                      .SetBasePath(Directory.GetCurrentDirectory())
                     .AddJsonFile("appsettings.json", optional: true)
                     .AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: false)
                     .AddEnvironmentVariables(); //

                 });

                builder.ConfigureServices((hostContext, services) =>
                    {
                        services.Configure<RabbitMqConfiguration>(hostContext.Configuration.GetSection("RabbitMq"));
                        services.Configure<DbSource>(dbSource =>
                        {
                            dbSource.ConnectionString = hostContext.Configuration.GetConnectionString("Default");
                        });

                        var connectionString = hostContext.Configuration.GetSection("ElasticSearch");

                        //Register NEST ElasticClient
                        var settings = new ConnectionSettings(new Uri(connectionString["Uri"]))
                                            .BasicAuthentication(connectionString["UserName"],
                                                                 connectionString["Password"])
                                            .EnableTcpStats();
                        settings.PrettyJson();
                        var client = new ElasticClient(settings);
                        services.AddSingleton<IElasticClient>(client);

                        services.AddTransient<IPushManager, PushManager>();
                        services.AddHostedService<QueueManager>();
                        services.AddLogging(b =>
                        {
                            b.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
                            b.AddSerilog(logger: logger, dispose: true);
                        });
                    });


                var host = builder.Build();
                using (host)
                {
                    await host.RunAsync();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
    }
}

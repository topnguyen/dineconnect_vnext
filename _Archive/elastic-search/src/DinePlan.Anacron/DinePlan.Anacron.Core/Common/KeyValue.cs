﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.Anacron.Core.Common
{
    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}

﻿using System;
using System.Threading.Tasks;
using DinePlan.Anacron.Core.Models.Logger;
using DinePlan.Anacron.Core.Models.Period;
using DinePlan.Anacron.Core.Models.Trans;
using Microsoft.Extensions.Logging;
using Nest;

namespace DinePlan.Anacron.Core.Managers.Impl
{
    public class PushManager : IPushManager
    {
        private readonly ILogger _logger;
        private readonly IElasticClient _client;

        public PushManager(ILogger<PushManager> logger,
                            IElasticClient client)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _client = client;
        }

        public async Task IndexTicketData(string indexName, Ticket ticket)
        {
            await _client.Indices.CreateAsync(indexName, c => c.Map<Ticket>(m => m.AutoMap()));
            var response =await _client.IndexAsync(ticket, idx => idx.Index(indexName));

            if (response.IsValid)
            {
                _logger.LogInformation($"Successfully index ticket data into '{indexName}' index");
            }
            else
            {
                _logger.LogError(response.OriginalException.Message);
            }
        }

        public async Task IndexWorkPeriodData(string indexName, WorkPeriod workPeriod)
        {
            await _client.Indices.CreateAsync(indexName, c => c.Map<WorkPeriod>(m => m.AutoMap()));
            var response =await _client.IndexAsync(workPeriod, idx => idx.Index(indexName));

            if (response.IsValid)
            {
                _logger.LogInformation($"Successfully index WorkPeriod data into '{indexName}' index");
            }
            else
            {
                _logger.LogError(response.OriginalException.Message);
            }
        }

        public async Task IndexPlanLogData(string indexName, PlanLogger planLogger)
        {
            await _client.Indices.CreateAsync(indexName, c => c.Map<PlanLogger>(m => m.AutoMap()));
            var response =await _client.IndexAsync(planLogger, idx => idx.Index(indexName));

            if (response.IsValid)
            {
                _logger.LogInformation($"Successfully index PlanLogger data into '{indexName}' index");
            }
            else
            {
                _logger.LogError(response.OriginalException.Message);
            }
        }
    }
}

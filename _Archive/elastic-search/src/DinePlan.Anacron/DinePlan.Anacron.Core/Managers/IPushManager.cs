﻿using System.Threading.Tasks;
using DinePlan.Anacron.Core.Models.Logger;
using DinePlan.Anacron.Core.Models.Period;
using DinePlan.Anacron.Core.Models.Trans;

namespace DinePlan.Anacron.Core.Managers
{
    public interface IPushManager
    {
        Task IndexTicketData(string indexName, Ticket ticket);
        Task IndexWorkPeriodData(string indexName, WorkPeriod workPeriod);
        Task IndexPlanLogData(string indexName, PlanLogger planLogger);


    }
}

﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using DinePlan.Anacron.Core.Managers;

namespace DinePlan.Anacron.Core.Queue
{
    public class QueueManager : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly string _hostname;
        private readonly string _queueName;
        private readonly string _username;
        private readonly string _password;
        private readonly string _vHost;

        private IModel _channel;
        private IConnection _connection;
        private readonly IPushManager _pushManager;

        public QueueManager(ILogger<QueueManager> logger,
                            IOptions<RabbitMqConfiguration> _options,
                            IPushManager pushManager
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _hostname = _options.Value.HostName;
            _queueName = _options.Value.QueueName;
            _username = _options.Value.UserName;
            _password = _options.Value.Password;
            _vHost = _options.Value.vHost;
            _pushManager = pushManager;

            InitializeRabbitMqListener();
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("QueueManager started...");
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                try
                {
                    var message = JsonSerializer.Deserialize<QueueMessage>(content);

                    if (message != null)
                    {
                        if (message.EnqueueMessageType == EnqueueMessageType.Ticket)
                        {
                            await _pushManager.IndexTicketData(message.IndexName, message.Ticket);
                            _logger.LogInformation("Received {0}", message.Ticket.TicketNumber);
                        }

                        if (message.EnqueueMessageType == EnqueueMessageType.Task)
                        {
                            await _pushManager.IndexPlanLogData(message.IndexName, message.Task);
                            _logger.LogInformation("Received {0}", message.Task.Id);

                        }

                        if (message.EnqueueMessageType == EnqueueMessageType.WorkPeriod)
                        {
                            await _pushManager.IndexWorkPeriodData(message.IndexName, message.WorkPeriod);
                            _logger.LogInformation("Received {0}", message.WorkPeriod.Id);
                        }
                    }

                    
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message, ex);
                }

                _channel.BasicAck(ea.DeliveryTag, false);
                
            };
            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            _channel.BasicConsume(_queueName, false, consumer);
        }

        private void InitializeRabbitMqListener()
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _hostname,
                    UserName = _username,
                    Password = _password,
                    VirtualHost = _vHost,
                    Port = 5672
                };

                _connection = factory.CreateConnection();
                _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
                _channel = _connection.CreateModel();
                _channel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            // Method intentionally left empty.
        }
        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e)
        {
            // Method intentionally left empty.
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
            // Method intentionally left empty.
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
            // Method intentionally left empty.
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
            // Method intentionally left empty.
        }
    }
}

﻿using DinePlan.Anacron.Core.Models.Logger;
using DinePlan.Anacron.Core.Models.Period;
using DinePlan.Anacron.Core.Models.Trans;

namespace DinePlan.Anacron.Core.Queue
{
    public class QueueMessage
    {
        public Ticket Ticket { get; set; }
        public EnqueueMessageType EnqueueMessageType{ get; set; }
        public WorkPeriod WorkPeriod { get; set; }
        public PlanLogger Task { get; set; }
        public string IndexName { get; set; }
    }

    public enum EnqueueMessageType
    {
        Ticket =0,
        WorkPeriod = 1,
        Task =2,
        Clock = 3
    }
}

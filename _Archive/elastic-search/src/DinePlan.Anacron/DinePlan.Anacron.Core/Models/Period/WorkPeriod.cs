﻿using System;
using System.Text.Json.Serialization;
using Nest;

namespace DinePlan.Anacron.Core.Models.Period
{
    [ElasticsearchType(IdProperty = nameof(Key))]
    public class WorkPeriod
    {
        [JsonPropertyName("Id")] public long Id { get; set; }
        [JsonPropertyName("LocationId")] public int LocationId { get; set; }
        [JsonPropertyName("TenantId")] public int TenantId { get; set; }
        public string Key => $"{LocationId}{TenantId}{Id}";

        [JsonPropertyName("StartUser")] public string StartUser { get; set; }

        [JsonPropertyName("SndUser")] public string EndUser { get; set; }

        [JsonPropertyName("StartTime")] public DateTime StartTime { get; set; }

        [JsonPropertyName("EndTime")] public DateTime EndTime { get; set; }

        [JsonPropertyName("Wid")] public int Wid { get; set; }

        [JsonPropertyName("TotalSales")] public decimal TotalSales { get; set; }

        [JsonPropertyName("TotalTicketCount")] public int TotalTicketCount { get; set; }

        [JsonPropertyName("WorkPeriodInformations")]
        public string WorkPeriodInformations { get; set; }

        [JsonPropertyName("DepartmentTicketInformations")]
        public string DepartmentTicketInformations { get; set; }

        [JsonPropertyName("AutoClosed")] public bool AutoClosed { get; set; }

        [JsonPropertyName("TotalTaxes")] public decimal TotalTaxes { get; set; }
    }
}
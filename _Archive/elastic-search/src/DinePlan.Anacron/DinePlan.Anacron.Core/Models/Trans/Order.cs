﻿using System;
using System.Text.Json.Serialization;

namespace DinePlan.Anacron.Core.Models.Trans
{
    public class Order
    {
        [JsonPropertyName("orderPromotionDetails")]
        public string OrderPromotionDetails { get; set; }
        [JsonPropertyName("orderId")]
        public int OrderId { get; set; }
        [JsonPropertyName("ticketId")]
        public long TicketId { get; set; }
        [JsonPropertyName("departmentName")]
        public string DepartmentName { get; set; }
        [JsonPropertyName("menuItemId")]
        public int MenuItemId { get; set; }
        [JsonPropertyName("menuItemName")]
        public string MenuItemName { get; set; }
        [JsonPropertyName("portionName")]
        public string PortionName { get; set; }
        [JsonPropertyName("price")]
        public decimal Price { get; set; }
        [JsonPropertyName("quantity")]
        public double Quantity { get; set; }
        [JsonPropertyName("portionCount")]
        public int PortionCount { get; set; }
        [JsonPropertyName("note")]
        public string Note { get; set; }
        [JsonPropertyName("locked")]
        public bool Locked { get; set; }
        [JsonPropertyName("calculatePrice")]
        public bool CalculatePrice { get; set; }
        [JsonPropertyName("increaseInventory")]
        public bool IncreaseInventory { get; set; }
        [JsonPropertyName("decreaseInventory")]
        public bool DecreaseInventory { get; set; }
        [JsonPropertyName("orderNumber")]
        public string OrderNumber { get; set; }
        [JsonPropertyName("creatingUserName")]
        public string CreatingUserName { get; set; }
        [JsonPropertyName("orderCreatedTime")]
        public DateTime OrderCreatedTime { get; set; }
        [JsonPropertyName("priceTag")]
        public string PriceTag { get; set; }
        [JsonPropertyName("taxes")]
        public string Taxes { get; set; } //"[{\"AT\":10,\"RN\":0,\"TN\":\"VAT\",\"TR\":7.00,\"TT\":1}]";
        [JsonPropertyName("orderTags")]
        public string OrderTags { get; set; }
        //[JsonPropertyName("orderStates")]
        //public List<OrderState> OrderStates { get; set; }
        [JsonPropertyName("isPromotionOrder")]
        public bool IsPromotionOrder { get; set; }
        [JsonPropertyName("promotionAmount")]
        public decimal PromotionAmount { get; set; }
        [JsonPropertyName("promotionSyncId")]
        public int PromotionSyncId { get; set; }
        [JsonPropertyName("menuItemPortionId")]
        public int MenuItemPortionId { get; set; }
        public int MenuItemType { get; set; }
        public string OrderRef { get; set; }
        public bool IsParent { get; set; }
        public bool IsUpSelling { get; set; }
        public string UpSellingOrderRef { get; set; }
        public string OrderLog { get; set; }
        //public Status OrderStatus { get; set; }
        public decimal OriginalPrice { get; set; }
        public string BindState { get; set; }
        public string BindStateValues { get; set; }
        public bool BindCompleted { get; set; }
    }
}

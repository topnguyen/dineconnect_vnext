﻿using System.Text.Json.Serialization;

namespace DinePlan.Anacron.Core.Models.Trans
{
    public class Transaction
    {
        [JsonPropertyName("transactionNote")]
        public string TransactionNote { get; set; }
        [JsonPropertyName("accountId")]
        public int AccountId { get; set; }
        [JsonPropertyName("transactionTypeId")]
        public int TransactionTypeId { get; set; }
        [JsonPropertyName("amount")]
        public decimal Amount { get; set; }

        [JsonPropertyName("transactionTypeName")]
        public string TransactionTypeName { get; set; }

    }
}

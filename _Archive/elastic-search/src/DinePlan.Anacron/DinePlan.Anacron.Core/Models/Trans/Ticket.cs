﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Nest;

namespace DinePlan.Anacron.Core.Models.Trans
{
    [ElasticsearchType(IdProperty = nameof(Key))]
    public class Ticket
    {
        [JsonPropertyName("id")] public long Id { get; set; }

        public string Key => $"{LocationId}{TicketNumber}";

        [JsonPropertyName("locationId")] public int LocationId { get; set; }

        [JsonPropertyName("tenantId")] public int TenantId { get; set; }

        [JsonPropertyName("ticketNumber")] public string TicketNumber { get; set; }

        [JsonPropertyName("invoiceNo")] public string InvoiceNo { get; set; }

        [JsonPropertyName("ticketCreatedTime")]
        public DateTime TicketCreatedTime { get; set; }

        [JsonPropertyName("lastUpdateTime")] public DateTime LastUpdateTime { get; set; }

        [JsonPropertyName("lastOrderTime")] public DateTime LastOrderTime { get; set; }

        [JsonPropertyName("lastPaymentTime")] public DateTime LastPaymentTime { get; set; }

        [JsonPropertyName("isClosed")] public bool IsClosed { get; set; }

        [JsonPropertyName("isLocked")] public bool IsLocked { get; set; }

        [JsonPropertyName("remainingAmount")] public decimal RemainingAmount { get; set; }

        [JsonPropertyName("totalAmount")] public decimal TotalAmount { get; set; }

        [JsonPropertyName("departmentName")] public string DepartmentName { get; set; }

        [JsonPropertyName("note")] public string Note { get; set; }

        [JsonPropertyName("lastModifiedUserName")]
        public string LastModifiedUserName { get; set; }

        [JsonPropertyName("ticketTags")] public string TicketTags { get; set; }

        [JsonPropertyName("ticketStates")] public string TicketStates { get; set; }

        [JsonPropertyName("ticketLogs")] public string TicketLogs { get; set; }

        [JsonPropertyName("taxIncluded")] public bool TaxIncluded { get; set; }

        [JsonPropertyName("terminalName")] public string TerminalName { get; set; }

        [JsonPropertyName("preOrder")] public bool PreOrder { get; set; }

        [JsonPropertyName("ticketEntities")] public string TicketEntities { get; set; }

        public int WorkPeriodId { get; set; }

        [JsonPropertyName("credit")] public bool Credit { get; set; }

        [JsonPropertyName("referenceNumber")] public string ReferenceNumber { get; set; }

        [JsonPropertyName("ticketPromotionDetails")]
        public string TicketPromotionDetails { get; set; }

        [JsonPropertyName("departmentGroup")] public string DepartmentGroup { get; set; }

        [JsonPropertyName("ticketStatus")] public string TicketStatus { get; set; }

        [JsonPropertyName("ticketLog1")] public string TicketLog1 { get; set; }

        [JsonPropertyName("lastPaymentTimeTruc")]
        public string LastPaymentTimeTruc { get; set; }


        [JsonPropertyName("orders")]
        [Nested]
        [PropertyName("orders")]

        public List<Order> Orders { get; set; }

        [JsonPropertyName("payments")]
        [Nested]
        [PropertyName("payments")]

        public List<Payment> Payments { get; set; }

        [JsonPropertyName("transactions")]
        [Nested]
        [PropertyName("transactions")]
        public List<Transaction> Transactions { get; set; }
    }
}
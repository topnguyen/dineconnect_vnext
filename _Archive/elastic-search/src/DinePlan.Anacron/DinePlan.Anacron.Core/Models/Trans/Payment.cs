﻿using System;
using System.Text.Json.Serialization;

namespace DinePlan.Anacron.Core.Models.Trans
{
    public class Payment
    {
        [JsonPropertyName("paymentTags")]
        public string PaymentTags { get; set; }
        [JsonPropertyName("paymentTypeId")]
        public int PaymentTypeId { get; set; }
        [JsonPropertyName("tenderedAmount")]
        public decimal TenderedAmount { get; set; }
        [JsonPropertyName("terminalName")]
        public string TerminalName { get; set; }
        [JsonPropertyName("paymentUserName")]
        public string PaymentUserName { get; set; }
        [JsonPropertyName("paymentCreatedTime")]
        public DateTime PaymentCreatedTime { get; set; }
        [JsonPropertyName("amount")]
        public decimal Amount { get; set; }
       
        [JsonPropertyName("paymentTypeName")]
        public string PaymentTypeName { get; set; }

    }
}

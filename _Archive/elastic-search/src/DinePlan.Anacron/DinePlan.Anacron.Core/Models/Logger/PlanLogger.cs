﻿using System;
using System.Text.Json.Serialization;
using Nest;

namespace DinePlan.Anacron.Core.Models.Logger
{
    [ElasticsearchType(IdProperty = nameof(Key))]
    public class PlanLogger
    {
        [JsonPropertyName("Id")] public long Id { get; set; }
        [JsonPropertyName("LocationId")] public int LocationId { get; set; }
        [JsonPropertyName("TenantId")] public int TenantId { get; set; }
        public string Key => $"{LocationId}{TenantId}{Id}";
        [JsonPropertyName("Terminal")] public string Terminal { get; set; }
        [JsonPropertyName("TicketId")] public int TicketId { get; set; }
        [JsonPropertyName("TicketNo")] public string TicketNo { get; set; }
        [JsonPropertyName("TicketTotal")] public decimal TicketTotal { get; set; }
        [JsonPropertyName("UserName")] public string UserName { get; set; }
        [JsonPropertyName("EventLog")] public string EventLog { get; set; }
        [JsonPropertyName("EventName")] public string EventName { get; set; }
        [JsonPropertyName("EventTime")] public DateTime EventTime { get; set; }
    }
}
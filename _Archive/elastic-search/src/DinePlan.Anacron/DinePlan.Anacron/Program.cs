﻿using DinePlan.Anacron.Core.Common;
using DinePlan.Anacron.Core.Queue;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using DinePlan.Anacron.Core.Managers;
using DinePlan.Anacron.Core.Managers.Impl;

namespace DinePlan.Anacron
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var logger = new LoggerConfiguration()
                .WriteTo.File("logs/cornjob.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile("appsettings.json", optional: false);
                    configApp.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                })
                 .ConfigureLogging((hostingContext, logging) =>
                 {
                     logging.AddConsole();
                 })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<RabbitMqConfiguration>(hostContext.Configuration.GetSection("RabbitMq"));
                    services.Configure<DbSource>(dbSource =>
                    {
                        dbSource.ConnectionString = hostContext.Configuration.GetConnectionString("Default");
                    });

                    var connectionString = hostContext.Configuration.GetSection("ElasticSearch");

                    //Register NEST ElasticClient
                    var settings = new ConnectionSettings(new Uri(connectionString["Uri"]))
                                    .BasicAuthentication(connectionString["UserName"],
                                                         connectionString["Password"])
                                    .EnableTcpStats();
                    settings.PrettyJson();
                    var client = new ElasticClient(settings);
                    services.AddSingleton<IElasticClient>(client);

                    services.AddTransient<IPushManager, PushManager>();
                    services.AddHostedService<QueueManager>();
                    services.AddLogging(b =>
                    {
                        b.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
                        b.AddSerilog(logger: logger, dispose: true);
                    });
                });

            await builder.RunConsoleAsync();
        }
    }
}

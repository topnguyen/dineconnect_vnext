﻿using DinePlan.Migrator.Common;
using System;
using System.Collections.Generic;

namespace DinePlan.Migrator.ElasticSearch.Models
{
    public class TicketLog
    {
        public string C { get; set; }
        public DateTime D { get; set; }
        public List<KeyValue> L { get; set; }
        public string N { get; set; }
        public string U { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.Migrator.ElasticSearch.Models
{
    public class Status
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTime { get; set; }
        public string PackingTime { get; set; }
        public string SosType { get; set; }
        public int TerminalId { get; set; }
    }
}

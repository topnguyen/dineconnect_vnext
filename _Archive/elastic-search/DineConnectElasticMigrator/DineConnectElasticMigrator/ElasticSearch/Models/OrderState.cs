﻿using System;

namespace DinePlan.Migrator.ElasticSearch.Models
{
    public class OrderState
    {
        public DateTime D { get; set; }
        public string OK { get; set; }
        public string S { get; set; }
        public string SN { get; set; }
        public string SV { get; set; }
        public int U { get; set; }
        public string UN { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace DinePlan.Migrator.ElasticSearch.Models
{
    public class Ticket
    {
        [JsonPropertyName("id")]
        [DataMember(Name = "id")]
        public long Id { get; set; }
        [JsonPropertyName("key")]
        [DataMember(Name = "key")]
        public string Key
        {
            get
            {
                return $"{TicketNumber}{LocationId}";
            }
        }
        [JsonPropertyName("locationId")]
        [DataMember(Name = "locationId")]
        public int LocationId { get; set; }
        [JsonPropertyName("ticketId")]
        [DataMember(Name = "ticketId")]
        public long TicketId { get; set; }
        [JsonPropertyName("tenantId")]
        [DataMember(Name = "tenantId")]
        public int TenantId { get; set; }
        [JsonPropertyName("ticketNumber")]
        [DataMember(Name = "ticketNumber")]
        public string TicketNumber { get; set; }
        [JsonPropertyName("invoiceNo")]
        [DataMember(Name = "invoiceNo")]
        public string InvoiceNo { get; set; }
        [JsonPropertyName("ticketCreatedTime")]
        [DataMember(Name = "ticketCreatedTime")]
        public DateTime TicketCreatedTime { get; set; }
        [JsonPropertyName("lastUpdateTime")]
        [DataMember(Name = "lastUpdateTime")]
        public DateTime LastUpdateTime { get; set; }
        [JsonPropertyName("lastOrderTime")]
        [DataMember(Name = "lastOrderTime")]
        public DateTime LastOrderTime { get; set; }
        [JsonPropertyName("lastPaymentTime")]
        [DataMember(Name = "lastPaymentTime")]
        public DateTime LastPaymentTime { get; set; }
        [JsonPropertyName("isClosed")]
        [DataMember(Name = "isClosed")]
        public bool IsClosed { get; set; }
        [JsonPropertyName("isLocked")]
        [DataMember(Name = "isLocked")]
        public bool IsLocked { get; set; }
        [JsonPropertyName("remainingAmount")]
        [DataMember(Name = "remainingAmount")]
        public decimal RemainingAmount { get; set; }
        [JsonPropertyName("totalAmount")]
        [DataMember(Name = "totalAmount")]
        public decimal TotalAmount { get; set; }
        [JsonPropertyName("departmentName")]
        [DataMember(Name = "departmentName")]
        public string DepartmentName { get; set; }
        [JsonPropertyName("ticketTypeName")]
        [DataMember(Name = "ticketTypeName")]
        public string TicketTypeName { get; set; }
        [JsonPropertyName("note")]
        [DataMember(Name = "note")]
        public string Note { get; set; }
        [JsonPropertyName("lastModifiedUserName")]
        [DataMember(Name = "lastModifiedUserName")]
        public string LastModifiedUserName { get; set; }
        [JsonPropertyName("orders")]
        [DataMember(Name = "orders")]
        public List<Order> Orders { get; set; }
        [JsonPropertyName("payments")]
        [DataMember(Name = "payments")]
        public List<Payment> Payments { get; set; }
        [JsonPropertyName("transactions")]
        [DataMember(Name = "transactions")]
        public List<Transaction> Transactions { get; set; }
        [JsonPropertyName("ticketTags")]
        [DataMember(Name = "ticketTags")]
        public string TicketTags { get; set; } //"[{\"TID\":0,\"TN\":\"ChangeDue\",\"TT\":0,\"TV\":\"0\"},{\"TID\":0,\"TN\":\"Tendered\",\"TT\":0,\"TV\":\"10.70\"}]";
        //[JsonPropertyName("ticketStates")]
        //public List<TicketState> TicketStates { get; set; }
        //[JsonPropertyName("ticketLogs")]
        //public List<TicketLog> TicketLogs { get; set; }
        [JsonPropertyName("taxIncluded")]
        [DataMember(Name = "taxIncluded")]
        public bool TaxIncluded { get; set; }
        [JsonPropertyName("terminalName")]
        [DataMember(Name = "terminalName")]
        public string TerminalName { get; set; }
        [JsonPropertyName("preOrder")]
        [DataMember(Name = "preOrder")]
        public bool PreOrder { get; set; }
        [JsonPropertyName("ticketEntities")]
        [DataMember(Name = "ticketEntities")]
        public string TicketEntities { get; set; } // "[]";
        [DataMember(Name = "workPeriodId")]
        public int WorkPeriodId { get; set; }
        [JsonPropertyName("credit")]
        [DataMember(Name = "credit")]
        public bool Credit { get; set; }
        [JsonPropertyName("referenceNumber")]
        [DataMember(Name = "referenceNumber")]
        public string ReferenceNumber { get; set; }
        [JsonPropertyName("ticketPromotionDetails")]
        [DataMember(Name = "ticketPromotionDetails")]
        public string TicketPromotionDetails { get; set; } //"[]";
        [JsonPropertyName("departmentGroup")]
        [DataMember(Name = "departmentGroup")]
        public string DepartmentGroup { get; set; }
        //[JsonPropertyName("ticketStatus")]
        //public Status TicketStatus { get; set; }
        [JsonPropertyName("ticketLog1")]
        [DataMember(Name = "ticketLog1")]
        public string TicketLog1 { get; set; }
    }
}

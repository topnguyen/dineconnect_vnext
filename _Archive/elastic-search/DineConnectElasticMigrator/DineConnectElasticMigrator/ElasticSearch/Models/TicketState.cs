﻿using System;

namespace DinePlan.Migrator.ElasticSearch.Models
{
    public class TicketState
    {
        public DateTime D { get; set; }
        public string S { get; set; }
        public string SN { get; set; }
        public string SV { get; set; }
    }
}

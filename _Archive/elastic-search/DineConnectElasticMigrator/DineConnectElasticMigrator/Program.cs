﻿using DinePlan.Migrator.Common;
using DinePlan.Migrator.SqlToEs;
using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DinePlan.Migrator
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var logger = new LoggerConfiguration()
               .CreateLogger();

            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile("appsettings.json", optional: false);
                    configApp.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
                })
                 .ConfigureLogging((hostingContext, logging) =>
                 {
                     logging.AddConsole();
                 })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<DbSource>(dbSource =>
                    {
                        dbSource.ConnectionString = hostContext.Configuration.GetConnectionString("Default");
                    });
                    services.Configure<ElasticSearchConfig>(hostContext.Configuration.GetSection("ElasticSearch"));

                    var connectionString = hostContext.Configuration.GetSection("ElasticSearch");

                    var pool = new SingleNodeConnectionPool(new Uri(connectionString["Uri"]));
                    var config = new ConnectionConfiguration(pool);
                    config.BasicAuthentication(connectionString["UserName"],
                                               connectionString["Password"]);
                    config.PrettyJson();
                    var client = new ElasticLowLevelClient(config);
                    services.AddSingleton<IElasticLowLevelClient>(client);
                    if (CreateIndexIfNotExists(client, connectionString["Index"]))
                    {
                        logger.Information("Successfully created index '{0}'", connectionString["Index"]);
                    }
                    //services.AddTransient<ITicketManager, TicketManager>();
                    services.AddHostedService<MigrationService>();
                    services.AddLogging(b =>
                    {
                        b.SetMinimumLevel(LogLevel.Information);
                        b.AddSerilog(logger: logger, dispose: true);
                    });
                });

            await builder.RunConsoleAsync();
        }


        private static bool CreateIndexIfNotExists(ElasticLowLevelClient client, string index)
        {
            var mapping = @"{
    ""mappings"": {
      ""properties"": {
                ""credit"": {
                    ""type"": ""boolean""
                },
        ""departmentGroup"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""departmentName"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""id"": {
                    ""type"": ""long""
        },
        ""invoiceNo"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""isClosed"": {
                    ""type"": ""boolean""
        },
        ""isLocked"": {
                    ""type"": ""boolean""
        },
        ""key"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""lastModifiedUserName"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""lastOrderTime"": {
                    ""type"": ""date""
        },
        ""lastPaymentTime"": {
                    ""type"": ""date""
        },
        ""lastUpdateTime"": {
                    ""type"": ""date""
        },
        ""locationId"": {
                    ""type"": ""integer""
        },
        ""note"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""orders"": {
                    ""type"": ""nested"",
          ""properties"": {
                        ""bindCompleted"": {
                            ""type"": ""boolean""
                        },
            ""bindState"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""bindStateValues"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""calculatePrice"": {
                            ""type"": ""boolean""
            },
            ""creatingUserName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""decreaseInventory"": {
                            ""type"": ""boolean""
            },
            ""departmentName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""increaseInventory"": {
                            ""type"": ""boolean""
            },
            ""isParent"": {
                            ""type"": ""boolean""
            },
            ""isPromotionOrder"": {
                            ""type"": ""boolean""
            },
            ""isUpSelling"": {
                            ""type"": ""boolean""
            },
            ""locked"": {
                            ""type"": ""boolean""
            },
            ""menuItemId"": {
                            ""type"": ""integer""
            },
            ""menuItemName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""menuItemPortionId"": {
                            ""type"": ""integer""
            },
            ""menuItemType"": {
                            ""type"": ""integer""
            },
            ""note"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""orderCreatedTime"": {
                            ""type"": ""date""
            },
            ""orderId"": {
                            ""type"": ""integer""
            },
            ""orderLog"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""orderNumber"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""orderPromotionDetails"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""orderRef"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""orderTags"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""originalPrice"": {
                            ""type"": ""double""
            },
            ""portionCount"": {
                            ""type"": ""integer""
            },
            ""portionName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""price"": {
                            ""type"": ""double""
            },
            ""priceTag"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""promotionAmount"": {
                            ""type"": ""double""
            },
            ""promotionSyncId"": {
                            ""type"": ""integer""
            },
            ""quantity"": {
                            ""type"": ""double""
            },
            ""taxes"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""ticketId"": {
                            ""type"": ""long""
            },
            ""upSellingOrderRef"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        }
                    }
                },
        ""payments"": {
                    ""type"": ""nested"",
          ""properties"": {
                        ""amount"": {
                            ""type"": ""double""
                        },
            ""paymentCreatedTime"": {
                            ""type"": ""date""
            },
            ""paymentTags"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""paymentTypeId"": {
                            ""type"": ""integer""
            },
            ""paymentUserName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""tenderedAmount"": {
                            ""type"": ""double""
            },
            ""terminalName"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        }
                    }
                },
        ""preOrder"": {
                    ""type"": ""boolean""
        },
        ""referenceNumber"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""remainingAmount"": {
                    ""type"": ""double""
        },
        ""taxIncluded"": {
                    ""type"": ""boolean""
        },
        ""tenantId"": {
                    ""type"": ""integer""
        },
        ""terminalName"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketCreatedTime"": {
                    ""type"": ""date""
        },
        ""ticketEntities"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketId"": {
                    ""type"": ""long""
        },
        ""ticketLog1"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketNumber"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketPromotionDetails"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketTags"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""ticketTypeName"": {
                    ""type"": ""text"",
          ""fields"": {
                        ""keyword"": {
                            ""type"": ""keyword"",
              ""ignore_above"": 256
                        }
                    }
                },
        ""totalAmount"": {
                    ""type"": ""double""
        },
        ""transactions"": {
                    ""type"": ""nested"",
          ""properties"": {
                        ""accountId"": {
                            ""type"": ""integer""
                        },
            ""amount"": {
                            ""type"": ""double""
            },
            ""transactionNote"": {
                            ""type"": ""text"",
              ""fields"": {
                                ""keyword"": {
                                    ""type"": ""keyword"",
                  ""ignore_above"": 256
                                }
                            }
                        },
            ""transactionTypeId"": {
                            ""type"": ""integer""
            }
                    }
                },
        ""workPeriodId"": {
                    ""type"": ""integer""
        }
            }
        }
    }";
            var response = client.Indices.Create<StringResponse>(index, PostData.String(mapping));
            return response.Success;
        }
    }
}

﻿using DinePlan.Migrator.Common;
using DinePlan.Migrator.ElasticSearch.Models;
using Elasticsearch.Net;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace DinePlan.Migrator.SqlToEs
{
    public class MigrationService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IElasticLowLevelClient _client;

        private readonly string _connectionString;
        private readonly string _esIndexName;

        public MigrationService(ILogger<MigrationService> logger,
                                IOptions<DbSource> _options,
                                IOptions<ElasticSearchConfig> _esConfig,
                                IElasticLowLevelClient client)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _client = client;

            _connectionString = _options.Value.ConnectionString;
            _esIndexName = _esConfig.Value.Index;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Trying to connect with SQL...");
            var con = GetConnection();
            await OpenSqlConnection(con, stoppingToken);
            await GetDataFromSqlAndIndexIntoES(con, stoppingToken);
        }

        private async Task GetDataFromSqlAndIndexIntoES(SqlConnection connection, CancellationToken stoppingToken)
        {
            int nextPage = 0;
            while (true)
            {
                var data = await GetDataFromSQL(connection, stoppingToken, nextPage);
                if (data.Count == 0)
                {
                    _logger.LogInformation("Migration completed");
                    break;
                }
                await IndexData(data);
                nextPage++;
            }

        }

        private async Task OpenSqlConnection(SqlConnection connection, CancellationToken stoppingToken)
        {
            await connection.OpenAsync(stoppingToken);
            _logger.LogInformation("Connected to SQL Server: '{0}'", _connectionString);
        }

        private async Task<List<Ticket>> GetDataFromSQL(SqlConnection connection, CancellationToken stoppingToken, int nextPage)
        {
            string queryString = $"SELECT * FROM Tickets AS ticket " +
                $"JOIN Orders AS ord ON ticket.Id = ord.TicketId " +
                $"JOIN Payments AS payment ON ticket.Id = payment.TicketId " +
                $"JOIN AccountTransactions AS acc ON payment.AccountTransaction_Id = acc.Id " +
                $"ORDER BY ticket.Id ASC " +
                $"OFFSET {nextPage * 500} ROWS " +
                $"FETCH NEXT 500 ROWS ONLY";

            var list = new List<Ticket>();
            SqlCommand command = new(queryString, connection);
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    _logger.LogInformation("Connection lost, trying to reconnect...");
                    await connection.OpenAsync(stoppingToken);
                    _logger.LogInformation("Connected to SQL Server: '{0}'", _connectionString);
                }

                _logger.LogInformation("Executing query: {0}", queryString);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                while (await reader.ReadAsync(stoppingToken))
                {
                    var id = reader["Id"].ToString();

                    long.TryParse(id, out long _id);

                    var ticketNumber = reader["TicketNumber"].ToString();

                    if (!list.Any(x => x.Id == _id))
                    {
                        list.Add(new Ticket()
                        {
                            Id = _id,
                            TicketNumber = ticketNumber
                        });
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return list;
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(_connectionString);
        }

        private async Task IndexData(List<Ticket> data)
        {
            foreach (var item in data)
            {
                var response = await _client.IndexAsync<StringResponse>(_esIndexName, PostData.Serializable(item));
                if (!response.Success)
                {
                    _logger.LogError(response.OriginalException, response.OriginalException.Message);
                }
            }
        }
    }
}

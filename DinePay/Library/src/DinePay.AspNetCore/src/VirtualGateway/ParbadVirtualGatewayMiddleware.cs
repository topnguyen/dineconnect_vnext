// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DinePay.Gateway.DinePayVirtual.MiddlewareInvoker;

namespace DinePay.AspNetCore.VirtualGateway
{
    public class DinePayVirtualGatewayMiddleware
    {
        public DinePayVirtualGatewayMiddleware(RequestDelegate next)
        {
        }

        public Task Invoke(HttpContext httpContext, IDinePayVirtualGatewayMiddlewareInvoker invoker)
        {
            return invoker.InvokeAsync();
        }
    }
}

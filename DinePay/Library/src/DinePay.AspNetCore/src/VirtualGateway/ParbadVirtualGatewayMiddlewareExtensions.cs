// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using DinePay.AspNetCore.VirtualGateway;
using DinePay.Gateway.DinePayVirtual;

namespace Microsoft.AspNetCore.Builder
{
    public static class DinePayVirtualGatewayMiddlewareExtensions
    {
        /// <summary>
        /// Adds the DinePay Virtual Gateway middleware to the pipeline if the current
        /// hosting environment name is <see cref="Hosting.EnvironmentName.Development"/>.
        /// </summary>
        /// <param name="builder"></param>
        public static IApplicationBuilder UseDinePayVirtualGatewayWhenDeveloping(this IApplicationBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

#if NETCOREAPP3_0 || Net_5
            var hostEnvironment = builder.ApplicationServices.GetRequiredService<Hosting.IWebHostEnvironment>();
            var isDevelopment = Microsoft.Extensions.Hosting.HostEnvironmentEnvExtensions.IsDevelopment(hostEnvironment);
#else
            var hostEnvironment = builder.ApplicationServices.GetRequiredService<Hosting.IHostingEnvironment>();
            var isDevelopment = Hosting.HostingEnvironmentExtensions.IsDevelopment(hostEnvironment);
#endif
            return builder.UseDinePayVirtualGatewayWhen(context => isDevelopment);
        }

        /// <summary>
        /// Adds the DinePay Virtual Gateway middleware to the pipeline.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        public static IApplicationBuilder UseDinePayVirtualGatewayWhen(this IApplicationBuilder builder,
            Func<HttpContext, bool> predicate)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.MapWhen(predicate,
                applicationBuilder => applicationBuilder.UseDinePayVirtualGateway());
        }

        /// <summary>
        /// Adds the DinePay Virtual Gateway middleware to the pipeline.
        /// </summary>
        /// <param name="builder"></param>
        public static IApplicationBuilder UseDinePayVirtualGateway(this IApplicationBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            var options = builder
                .ApplicationServices
                .GetRequiredService<IOptions<DinePayVirtualGatewayOptions>>();

            if (options.Value == null ||
                options.Value.GatewayPath == null ||
                !options.Value.GatewayPath.HasValue)
            {
                throw new InvalidOperationException("Cannot get DinePay Virtual gateway path value. " +
                                                    "Make sure that you have already configured it.");
            }

            return builder.Map(options.Value.GatewayPath,
                applicationBuilder => applicationBuilder.UseMiddleware<DinePayVirtualGatewayMiddleware>());
        }
    }
}

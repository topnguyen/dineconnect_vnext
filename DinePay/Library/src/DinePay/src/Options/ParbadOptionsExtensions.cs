// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using DinePay.Options;
using System;

namespace DinePay.Builder
{
    public static class DinePayOptionsExtensions
    {
        /// <summary>
        /// Configures the DinePay options.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="setupOptions"></param>
        public static IDinePayBuilder ConfigureOptions(this IDinePayBuilder builder, Action<DinePayOptions> setupOptions)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (setupOptions == null) throw new ArgumentNullException(nameof(setupOptions));

            builder.Services.Configure(setupOptions);

            RegisterMessagesOptions(builder);

            return builder;
        }

        private static void RegisterMessagesOptions(IDinePayBuilder builder)
        {
            builder.Services.AddTransient<IOptions<MessagesOptions>>(provider =>
            {
                var messages = provider.GetRequiredService<IOptions<DinePayOptions>>().Value.Messages;

                return new OptionsWrapper<MessagesOptions>(messages);
            });
        }
    }
}

﻿namespace DinePay.Options
{
    /// <summary>
    /// Provides configuration for DinePay.
    /// </summary>
    public class DinePayOptions
    {
        /// <summary>
        /// Enables or disables the logging. The default value is true.
        /// </summary>
        public bool EnableLogging { get; set; } = true;

        /// <summary>
        /// Contains all messages that DinePay uses in results.
        /// </summary>
        public MessagesOptions Messages { get; set; } = new MessagesOptions();
    }
}

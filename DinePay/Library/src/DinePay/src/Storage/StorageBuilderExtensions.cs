﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using DinePay.Internal;
using DinePay.Storage;
using DinePay.Storage.Abstractions;
using System;

namespace DinePay.Builder
{
    public static class StorageBuilderExtensions
    {
        /// <summary>
        /// Configures the storage which required by DinePay for saving and loading data.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureStorage"></param>
        public static IDinePayBuilder ConfigureStorage(this IDinePayBuilder builder, Action<IStorageBuilder> configureStorage)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (configureStorage == null) throw new ArgumentNullException(nameof(configureStorage));

            var storageBuilder = new StorageBuilder(builder.Services);
            storageBuilder.UseDefaultStorageManager();

            configureStorage(storageBuilder);

            return builder;
        }

        /// <summary>
        /// Uses the default implementation of <see cref="IStorageManager"/>.
        /// </summary>
        /// <param name="builder"></param>
        public static IStorageBuilder UseDefaultStorageManager(this IStorageBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.AddStorageManager<StorageManager>(ServiceLifetime.Transient);
        }
    }
}

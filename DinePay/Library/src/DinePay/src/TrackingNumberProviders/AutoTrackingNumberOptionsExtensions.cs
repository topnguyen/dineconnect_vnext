// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using DinePay.TrackingNumberProviders;
using System;

namespace DinePay.Builder
{
    public static class AutoTrackingNumberOptionsExtensions
    {
        public static IDinePayBuilder ConfigureAutoIncrementTrackingNumber(
            this IDinePayBuilder builder,
            Action<AutoIncrementTrackingNumberOptions> configureOptions)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            builder.Services.Configure(configureOptions);

            return builder;
        }

        public static IDinePayBuilder ConfigureAutoRandomTrackingNumber(
            this IDinePayBuilder builder,
            Action<AutoRandomTrackingNumberOptions> configureOptions)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            builder.Services.Configure(configureOptions);

            return builder;
        }
    }
}

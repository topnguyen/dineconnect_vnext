// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using DinePay.Abstraction;
using DinePay.Builder;
using DinePay.Internal;
using DinePay.InvoiceBuilder;
using DinePay.TrackingNumberProviders;
using System;

namespace DinePay
{
    /// <inheritdoc />
    public class DinePayBuilder : IDinePayBuilder
    {
        /// <summary>
        /// Initializes an instance of <see cref="DinePayBuilder"/> class with the given <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services"></param>
        public DinePayBuilder(IServiceCollection services)
        {
            Services = services ?? throw new ArgumentNullException(nameof(services));
        }

        /// <inheritdoc />
        public IServiceCollection Services { get; }

        /// <inheritdoc />
        public IOnlinePaymentAccessor Build()
        {
            var serviceProvider = Services.BuildServiceProvider();

            var onlinePaymentAccessor = serviceProvider.GetRequiredService<IOnlinePaymentAccessor>();

            StaticOnlinePayment.Initialize(onlinePaymentAccessor);

            return onlinePaymentAccessor;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IDinePayBuilder"/> class with pre-configured services.
        /// </summary>
        public static IDinePayBuilder CreateDefaultBuilder(IServiceCollection services = null)
        {
            services ??= new ServiceCollection();

            var builder = new DinePayBuilder(services);

            builder.Services.AddOptions();

            builder.Services.AddHttpClient();

            builder.Services.TryAddTransient<IOnlinePayment, DefaultOnlinePayment>();
            builder.Services.TryAddSingleton<IOnlinePaymentAccessor, OnlinePaymentAccessor>();

            builder.Services.TryAddTransient<IInvoiceBuilder, DefaultInvoiceBuilder>();

            builder.Services.TryAddTransient<AutoIncrementTrackingNumber>();
            builder.Services.TryAddTransient<AutoRandomTrackingNumber>();

            builder.Services.TryAddTransient<IGatewayProvider, DefaultGatewayProvider>();

            builder.ConfigureOptions(options => { });

            builder.ConfigurePaymentToken(tokenBuilder => tokenBuilder.UseGuidQueryStringPaymentTokenProvider());

            builder.Services.TryAddTransient(typeof(IDinePayLogger<>), typeof(DinePayLogger<>));

            return builder;
        }
    }
}

// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System;

namespace DinePay.PaymentTokenProviders
{
    public class PaymentTokenProviderException : Exception
    {
        public PaymentTokenProviderException(string message) : base(message)
        {
        }
    }
}

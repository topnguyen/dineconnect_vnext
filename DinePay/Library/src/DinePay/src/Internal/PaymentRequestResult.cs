// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace DinePay.Internal
{
    public class PaymentRequestResult : PaymentResult, IPaymentRequestResult
    {
        /// <inheritdoc />
        public PaymentRequestResultStatus Status { get; set; }

        /// <inheritdoc />
        public IGatewayTransporter GatewayTransporter { get; set; }

        public override bool IsSucceed => Status == PaymentRequestResultStatus.Succeed;

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a successful result and a Gateway Transporter with type of Post.
        /// </summary>
        /// <param name="gatewayAccountName"></param>
        /// <param name="httpContext"></param>
        /// <param name="url">Page URL.</param>
        /// <param name="form">Form to post.</param>
        public static PaymentRequestResult SucceedWithPost(
            string gatewayAccountName,
            HttpContext httpContext,
            string url,
            IEnumerable<KeyValuePair<string, string>> form, IDictionary<string, string> DatabaseAdditionalData = null)
        {
            var descriptor = GatewayTransporterDescriptor.CreatePost(url, form);

            var transporter = new DefaultGatewayTransporter(httpContext, descriptor);

            return Succeed(transporter, gatewayAccountName, DatabaseAdditionalData);
        }

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a successful result and a Gateway Transporter with type of Redirect.
        /// </summary>
        /// <param name="gatewayAccountName"></param>
        /// <param name="httpContext"></param>
        /// <param name="url">Page URL.</param>
        public static PaymentRequestResult SucceedWithRedirect(
            string gatewayAccountName,
            HttpContext httpContext,
            string url,
            IDictionary<string, string> DatabaseAdditionalData = null
            )
        {
            var descriptor = GatewayTransporterDescriptor.CreateRedirect(url);

            var transporter = new DefaultGatewayTransporter(httpContext, descriptor);

            return Succeed(transporter, gatewayAccountName, DatabaseAdditionalData);
        }

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a successful result.
        /// </summary>
        public static PaymentRequestResult Succeed(IGatewayTransporter gatewayTransporter, string gatewayAccountName, IDictionary<string, string> DatabaseAdditionalData=null)
        {
            var data = new PaymentRequestResult
            {
                GatewayAccountName = gatewayAccountName,
                GatewayTransporter = gatewayTransporter,
                Status = PaymentRequestResultStatus.Succeed,
                DatabaseAdditionalData = DatabaseAdditionalData
            };

            return data;
        }

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a failure result.
        /// </summary>
        public static PaymentRequestResult Failed(string message) => Failed(message, null);

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a failure result.
        /// </summary>
        public static PaymentRequestResult Failed(string message, string gatewayAccountName) => Failed(message, gatewayAccountName, null);

        /// <summary>
        /// Creates an instance of <see cref="PaymentRequestResult"/> which indicates a failure result.
        /// </summary>
        public static PaymentRequestResult Failed(string message, string gatewayAccountName, string gatewayResponseCode)
        {
            return new PaymentRequestResult
            {
                Status = PaymentRequestResultStatus.Failed,
                Message = message,
                GatewayResponseCode = gatewayResponseCode,
                GatewayAccountName = gatewayAccountName,
                GatewayTransporter = new NullGatewayTransporter()
            };
        }
    }
}

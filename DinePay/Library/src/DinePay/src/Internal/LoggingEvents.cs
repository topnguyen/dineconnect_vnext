// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

namespace DinePay.Internal
{
    public class LoggingEvents
    {
        public const int RequestPayment = 100;

        public const int FetchPayment = 101;

        public const int VerifyPayment = 101;

        public const int CancelPayment = 102;

        public const int RefundPayment = 103;

        public const int DatabaseCreation = 200;
    }
}

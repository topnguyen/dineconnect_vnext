// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

namespace DinePay.Internal
{
    public class PaymentCancelResult : PaymentResult, IPaymentCancelResult
    {
    }
}

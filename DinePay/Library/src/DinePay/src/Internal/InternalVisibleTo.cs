// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System.Runtime.CompilerServices;

[assembly:
    InternalsVisibleTo("DinePay.Owin"),
    InternalsVisibleTo("DinePay.Mvc"),
    InternalsVisibleTo("DinePay.AspNetCore"),
    InternalsVisibleTo("DinePay.Storage.Cache"),
    InternalsVisibleTo("DinePay.Storage.EntityFrameworkCore"),
    InternalsVisibleTo("DinePay.Tests")]

﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System;

namespace DinePay.Gateway.Stripe
{
    public static class StripeVerifyAdditionalDataExtensions
    {
        private const string AdditionalDataKey = "StripeVerificationAdditionalData";

        public static StripeVerifyAdditionalData GetStripeAdditionalData(this IPaymentVerifyResult result)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));

            result.AdditionalData.TryGetValue(AdditionalDataKey, out var additionalData);

            return additionalData as StripeVerifyAdditionalData;
        }

        internal static void SetStripeAdditionalData(this IPaymentVerifyResult result, StripeVerifyAdditionalData additionalData)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));
            if (additionalData == null) throw new ArgumentNullException(nameof(additionalData));

            result.AdditionalData.Add(AdditionalDataKey, additionalData);
        }
    }
}

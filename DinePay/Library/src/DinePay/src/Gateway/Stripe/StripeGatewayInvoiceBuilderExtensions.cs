﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using DinePay.Gateway.Stripe.Internal;
using DinePay.InvoiceBuilder;
using System;
using DinePay.Abstraction;

namespace DinePay.Gateway.Stripe
{
    public static class StripeGatewayInvoiceBuilderExtensions
    {
        /// <summary>
        /// The invoice will be sent to Pay.ir gateway.
        /// </summary>
        /// <param name="builder"></param>
        public static IInvoiceBuilder UseStripe(this IInvoiceBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.SetGateway(StripeGateway.Name);
        }

        /// <summary>
        /// Sets additional data to the Invoice for <see cref="StripeGateway"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="data"></param>
        public static IInvoiceBuilder SetStripeAdditionalData(this IInvoiceBuilder builder, StripeRequestAdditionalData data)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (data == null) throw new ArgumentNullException(nameof(data));

            return builder.AddOrUpdateProperty(StripeHelper.RequestAdditionalDataKey, data);
        }

        internal static StripeRequestAdditionalData GetStripeAdditionalData(this Invoice invoice)
        {
            if (invoice == null) throw new ArgumentNullException(nameof(invoice));

            if (invoice.Properties.ContainsKey(StripeHelper.RequestAdditionalDataKey))
            {
                return (StripeRequestAdditionalData)invoice.Properties[StripeHelper.RequestAdditionalDataKey];
            }

            return null;
        }
    }
}

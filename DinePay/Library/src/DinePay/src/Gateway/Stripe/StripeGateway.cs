﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using DinePay.Abstraction;
using DinePay.Gateway.Stripe.Internal;
using DinePay.GatewayBuilders;
using DinePay.Internal;
using DinePay.Net;
using DinePay.Options;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Invoice = DinePay.Abstraction.Invoice;
using PaymentVerifyResult = DinePay.Internal.PaymentVerifyResult;

namespace DinePay.Gateway.Stripe
{
    public class StripeGateway : GatewayBase<StripeGatewayAccount>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly HttpClient _httpClient;
        private readonly StripeGatewayOptions _gatewayOptions;
        private readonly MessagesOptions _messagesOptions;

        public const string Name = "Stripe";

        private static JsonSerializerSettings DefaultSerializerSettings => new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public StripeGateway(
            IGatewayAccountProvider<StripeGatewayAccount> accountProvider,
            IHttpContextAccessor httpContextAccessor,
            IHttpClientFactory httpClientFactory,
            IOptions<StripeGatewayOptions> gatewayOptions,
            IOptions<MessagesOptions> messagesOptions) : base(accountProvider)
        {
            _httpContextAccessor = httpContextAccessor;
            _httpClient = httpClientFactory.CreateClient(this);
            _gatewayOptions = gatewayOptions.Value;
            _messagesOptions = messagesOptions.Value;
        }

        /// <inheritdoc />
        public override async Task<IPaymentRequestResult> RequestAsync(Invoice invoice, CancellationToken cancellationToken = default)
        {
            if (invoice == null) throw new ArgumentNullException(nameof(invoice));

            var account = await GetAccountAsync(invoice).ConfigureAwaitFalse();
            var data = StripeHelper.CreateRequestData(account, invoice);
            StripeConfiguration.ApiKey = account.SecretKey;
            var chargeService = new ChargeService();

            var result = await chargeService.CreateAsync(data);

            IDictionary<string, string> keys = new Dictionary<string, string>();
            keys.Add("id", result.Id);

            return StripeHelper.CreateRequestResult(result, _httpContextAccessor.HttpContext, account);
        }

        /// <inheritdoc />
        public override async Task<IPaymentFetchResult> FetchAsync(InvoiceContext context, CancellationToken cancellationToken = default)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var callbackResult = await StripeHelper.CreateCallbackResultAsync(_httpContextAccessor.HttpContext.Request, cancellationToken).ConfigureAwaitFalse();

            if (callbackResult.IsSucceed)
            {
                return PaymentFetchResult.ReadyForVerifying();
            }

            return PaymentFetchResult.Failed(callbackResult.Message);
        }

        /// <inheritdoc />
        public override async Task<IPaymentVerifyResult> VerifyAsync(InvoiceContext context, CancellationToken cancellationToken = default)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var callbackResult = await StripeHelper.CreateCallbackResultAsync(_httpContextAccessor.HttpContext.Request, cancellationToken).ConfigureAwaitFalse();

            if (!callbackResult.IsSucceed)
            {
                return PaymentVerifyResult.Failed(callbackResult.Message);
            }

            var account = await GetAccountAsync(context.Payment).ConfigureAwaitFalse();
            var data = StripeHelper.CreateVerifyData(account, callbackResult);

            var client = new StripeClient(account.SecretKey);
            var transaction = context.Transactions.FirstOrDefault(); // geting a specific Transaction
            var Adddata = JsonConvert.DeserializeObject<IDictionary<string, string>>(transaction.AdditionalData);

            var chargeService = new ChargeService(client);

            var result = chargeService.Get(Adddata["id"]);



            return StripeHelper.CreateVerifyResult(result, _messagesOptions);
        }

        /// <inheritdoc />
        public override Task<IPaymentRefundResult> RefundAsync(InvoiceContext context, Money amount, CancellationToken cancellationToken = default)
        {
            return PaymentRefundResult.Failed("The Refund operation is not supported by this gateway.").ToInterfaceAsync();
        }
    }
}

﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using DinePay.Abstraction;
using DinePay.Http;
using DinePay.Internal;
using DinePay.Options;
using Stripe;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Invoice = DinePay.Abstraction.Invoice;

namespace DinePay.Gateway.Stripe.Internal
{
    internal static class StripeHelper
    {
        public const string OkResult = "1";
        public static string RequestAdditionalDataKey => nameof(RequestAdditionalDataKey);

        public static ChargeCreateOptions CreateRequestData(StripeGatewayAccount account, Invoice invoice)
        {
      
            var additionalData = invoice.GetStripeAdditionalData();

            return new ChargeCreateOptions
            {
                Amount = invoice.Amount,
                Currency = additionalData.Currency,
                Source = additionalData.StripeToken
            };
        }

        public static PaymentRequestResult CreateRequestResult(Charge response, HttpContext httpContext, StripeGatewayAccount account, IDictionary<string, string> databaseAdditionalData = null)
        {

            if (response.Status != "succeeded")
            {
                return PaymentRequestResult.Failed(response.FailureMessage, account.Name);
            }


            return PaymentRequestResult.SucceedWithRedirect(account.Name, httpContext, response.ReceiptUrl, databaseAdditionalData);
        }

        public static async Task<StripeCallbackResult> CreateCallbackResultAsync(HttpRequest httpRequest, CancellationToken cancellationToken)
        {
            Console.WriteLine(httpRequest.Query.Keys);
            var token = await httpRequest.TryGetParamAsync("paymentToken", cancellationToken).ConfigureAwaitFalse();
            //var status = await httpRequest.TryGetParamAsync("Status", cancellationToken).ConfigureAwaitFalse();

            //var isSucceed = status.Exists && string.Equals(status.Value, OkResult, StringComparison.InvariantCultureIgnoreCase);
            //string message = null;

            //if (!isSucceed)
            //{
            //    message = $"Error {status}";
            //}

            return new StripeCallbackResult
            {
                Token = token.Value,
                IsSucceed = true,
                Message = ""
            };
        }

        public static StripeVerifyModel CreateVerifyData(StripeGatewayAccount account, StripeCallbackResult callbackResult)
        {
            return new StripeVerifyModel
            {
                Token = callbackResult.Token
            };
        }

        public static PaymentVerifyResult CreateVerifyResult(Charge response, MessagesOptions messagesOptions)
        {
            PaymentVerifyResult verifyResult;

            if (response.Status == "succeeded")
            {
                verifyResult = PaymentVerifyResult.Succeed(response.TransferId, messagesOptions.PaymentSucceed);
            }
            else
            {
                var message = $"ErrorCode: {response.FailureCode}, ErrorMessage: {response.FailureMessage}";

                verifyResult = PaymentVerifyResult.Failed(message);
            }

            var additionalData = new StripeVerifyAdditionalData
            {
                CardNumber = response.Id
            };

            verifyResult.SetStripeAdditionalData(additionalData);

            return verifyResult;
        }
    }
}

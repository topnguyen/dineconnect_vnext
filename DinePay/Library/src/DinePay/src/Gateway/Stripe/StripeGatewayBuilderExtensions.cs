﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using DinePay.GatewayBuilders;
using System;

namespace DinePay.Gateway.Stripe
{
    public static class StripeGatewayBuilderExtensions
    {
        /// <summary>
        /// Adds the Pay.ir gateway to DinePay services.
        /// </summary>
        /// <param name="builder"></param>
        public static IGatewayConfigurationBuilder<StripeGateway> AddStripe(this IGatewayBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder
                .AddGateway<StripeGateway>()
                .WithHttpClient(clientBuilder => { })
                .WithOptions(options => { });
        }

        /// <summary>
        /// Configures the accounts for Pay.ir.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureAccounts">Configures the accounts.</param>
        public static IGatewayConfigurationBuilder<StripeGateway> WithAccounts(
            this IGatewayConfigurationBuilder<StripeGateway> builder,
            Action<IGatewayAccountBuilder<StripeGatewayAccount>> configureAccounts)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.WithAccounts(configureAccounts);
        }
        /// <summary>
        /// Configures the options for Stripe Gateway.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureOptions">Configuration</param>
        public static IGatewayConfigurationBuilder<StripeGateway> WithOptions(
            this IGatewayConfigurationBuilder<StripeGateway> builder,
            Action<StripeGatewayOptions> configureOptions)
        {
            builder.Services.Configure(configureOptions);

            return builder;
        }

    }
}

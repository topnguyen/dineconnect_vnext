﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

namespace DinePay.Gateway.Stripe
{
    public class StripeRequestAdditionalData
    {

        public string StripeToken { get; set; }
        public string Currency { get; set; }


    }
}
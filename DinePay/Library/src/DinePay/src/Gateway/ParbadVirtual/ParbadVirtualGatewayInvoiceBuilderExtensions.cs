﻿using System;
using DinePay.InvoiceBuilder;

namespace DinePay.Gateway.DinePayVirtual
{
    public static class DinePayVirtualGatewayInvoiceBuilderExtensions
    {
        /// <summary>
        /// The invoice will be sent to DinePay Virtual gateway.
        /// </summary>
        /// <param name="builder"></param>
        public static IInvoiceBuilder UseDinePayVirtual(this IInvoiceBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.SetGateway(DinePayVirtualGateway.Name);
        }
    }
}

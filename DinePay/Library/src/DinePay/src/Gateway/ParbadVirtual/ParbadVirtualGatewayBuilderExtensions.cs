// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using DinePay.Builder;
using DinePay.Gateway.DinePayVirtual.MiddlewareInvoker;
using DinePay.GatewayBuilders;
using System;

namespace DinePay.Gateway.DinePayVirtual
{
    public static class DinePayVirtualGatewayBuilderExtensions
    {
        /// <summary>
        /// Adds the DinePay Virtual Gateway to DinePay services.
        /// </summary>
        /// <param name="builder"></param>
        public static IGatewayConfigurationBuilder<DinePayVirtualGateway> AddDinePayVirtual(this IGatewayBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            builder.Services.TryAddTransient<IDinePayVirtualGatewayMiddlewareInvoker, DinePayVirtualGatewayMiddlewareInvoker>();

            return builder
                .AddGateway<DinePayVirtualGateway>()
                .WithAccounts(accounts => accounts.AddInMemory(account => { }));
        }

        /// <summary>
        /// Configures the accounts for <see cref="DinePayVirtualGateway"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureAccounts">Configures the accounts.</param>
        public static IGatewayConfigurationBuilder<DinePayVirtualGateway> WithAccounts(
            this IGatewayConfigurationBuilder<DinePayVirtualGateway> builder,
            Action<IGatewayAccountBuilder<DinePayVirtualGatewayAccount>> configureAccounts)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.WithAccounts(configureAccounts);
        }

        /// <summary>
        /// Configures DinePay Virtual gateway options.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureOptions"></param>
        public static IGatewayConfigurationBuilder<DinePayVirtualGateway> WithOptions(
            this IGatewayConfigurationBuilder<DinePayVirtualGateway> builder,
            Action<DinePayVirtualGatewayOptions> configureOptions)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            builder.Services.Configure(configureOptions);

            return builder;
        }
    }
}

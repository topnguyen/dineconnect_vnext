// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;

namespace DinePay.Gateway.DinePayVirtual
{
    public class DinePayVirtualGatewayOptions
    {
        /// <summary>
        /// Path of Virtual Gateway. It would be like: /MyDinePayGateway
        /// </summary>
        public PathString GatewayPath { get; set; }
    }
}

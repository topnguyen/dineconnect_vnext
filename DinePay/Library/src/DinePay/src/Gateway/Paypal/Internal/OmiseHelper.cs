﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Omise.Models;
using DinePay.Abstraction;
using DinePay.Http;
using DinePay.Internal;
using DinePay.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PayPalCheckoutSdk.Orders;

namespace DinePay.Gateway.Paypal.Internal
{
    internal static class PaypalHelper
    {
        public const string OkResult = "1";
        public static string RequestAdditionalDataKey => nameof(RequestAdditionalDataKey);

        public static CreateChargeRequest CreateRequestData(PaypalGatewayAccount account, Invoice invoice)
        {
      
            var additionalData = invoice.GetPaypalAdditionalData();

            return new CreateChargeRequest
            {
                Amount = invoice.Amount,
                ReturnUri = invoice.CallbackUrl,
                Currency = additionalData.Currency,
                Card = additionalData.PaypalToken
            };
        }

        public static PaymentRequestResult CreateRequestResult(Order response, HttpContext httpContext, PaypalGatewayAccount account, IDictionary<string, string> databaseAdditionalData = null)
        {

            if (response.Status != "COMPLETED")
            {
                return PaymentRequestResult.Failed("Error", account.Name);
            }


            return PaymentRequestResult.SucceedWithRedirect(account.Name, httpContext, "", databaseAdditionalData);
        }

        public static async Task<PaypalCallbackResult> CreateCallbackResultAsync(HttpRequest httpRequest, CancellationToken cancellationToken)
        {
            Console.WriteLine(httpRequest.Query.Keys);
            var token = await httpRequest.TryGetParamAsync("paymentToken", cancellationToken).ConfigureAwaitFalse();

            return new PaypalCallbackResult
            {
                Token = token.Value,
                IsSucceed = true,
                Message = ""
            };
        }

        public static PaypalVerifyModel CreateVerifyData(PaypalGatewayAccount account, PaypalCallbackResult callbackResult)
        {
            return new PaypalVerifyModel
            {
                Token = callbackResult.Token
            };
        }

        public static PaymentVerifyResult CreateVerifyResult(Charge response, MessagesOptions messagesOptions)
        {
            PaymentVerifyResult verifyResult;

            if (response.Status == ChargeStatus.Successful)
            {
                verifyResult = PaymentVerifyResult.Succeed(response.Transaction, messagesOptions.PaymentSucceed);
            }
            else
            {
                var message = $"ErrorCode: {response.FailureCode}, ErrorMessage: {response.FailureMessage}";

                verifyResult = PaymentVerifyResult.Failed(message);
            }

            var additionalData = new PaypalVerifyAdditionalData
            {
                OrderId = response.Card.FirstDigits
            };

            verifyResult.SetPaypalAdditionalData(additionalData);

            return verifyResult;
        }
    }
}

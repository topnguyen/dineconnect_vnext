﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Omise;
using DinePay.Abstraction;
using DinePay.Gateway.Paypal.Internal;
using DinePay.GatewayBuilders;
using DinePay.Internal;
using DinePay.Net;
using DinePay.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using PaymentVerifyResult = DinePay.Internal.PaymentVerifyResult;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;

namespace DinePay.Gateway.Paypal
{
    public class PaypalGateway : GatewayBase<PaypalGatewayAccount>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly HttpClient _httpClient;
        private readonly PaypalGatewayOptions _gatewayOptions;
        private readonly MessagesOptions _messagesOptions;

        public const string Name = "Paypal";

        private static JsonSerializerSettings DefaultSerializerSettings => new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public PaypalGateway(
            IGatewayAccountProvider<PaypalGatewayAccount> accountProvider,
            IHttpContextAccessor httpContextAccessor,
            IHttpClientFactory httpClientFactory,
            IOptions<PaypalGatewayOptions> gatewayOptions,
            IOptions<MessagesOptions> messagesOptions) : base(accountProvider)
        {
            _httpContextAccessor = httpContextAccessor;
            _httpClient = httpClientFactory.CreateClient(this);
            _gatewayOptions = gatewayOptions.Value;
            _messagesOptions = messagesOptions.Value;
        }

        /// <inheritdoc />
        public override async Task<IPaymentRequestResult> RequestAsync(Invoice invoice, CancellationToken cancellationToken = default)
        {
            if (invoice == null) throw new ArgumentNullException(nameof(invoice));

            var account = await GetAccountAsync(invoice).ConfigureAwaitFalse();
            var environment = GetEnvironment(account);

            var client = new PayPalHttpClient(environment);
            var additionalData = invoice.GetPaypalAdditionalData();

            var request = new OrdersCaptureRequest(additionalData.OrderId);

            request.RequestBody(new OrderActionRequest());
            var response = await client.Execute(request);
            var result = response.Result<Order>();


            IDictionary<string, string> keys = new Dictionary<string, string>();
            keys.Add("id",result.Id);
            return PaypalHelper.CreateRequestResult(result, _httpContextAccessor.HttpContext, account);
        }

        /// <inheritdoc />
        public override async Task<IPaymentFetchResult> FetchAsync(InvoiceContext context, CancellationToken cancellationToken = default)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var callbackResult = await PaypalHelper.CreateCallbackResultAsync(_httpContextAccessor.HttpContext.Request, cancellationToken).ConfigureAwaitFalse();

            if (callbackResult.IsSucceed)
            {
                return PaymentFetchResult.ReadyForVerifying();
            }

            return PaymentFetchResult.Failed(callbackResult.Message);
        }

        /// <inheritdoc />
        public override async Task<IPaymentVerifyResult> VerifyAsync(InvoiceContext context, CancellationToken cancellationToken = default)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var callbackResult = await PaypalHelper.CreateCallbackResultAsync(_httpContextAccessor.HttpContext.Request, cancellationToken).ConfigureAwaitFalse();

            if (!callbackResult.IsSucceed)
            {
                return PaymentVerifyResult.Failed(callbackResult.Message);
            }

            var account = await GetAccountAsync(context.Payment).ConfigureAwaitFalse();
            var data = PaypalHelper.CreateVerifyData(account, callbackResult);

            var client = new Client("", "");
            var transaction = context.Transactions.FirstOrDefault(); // geting a specific Transaction
            var Adddata = JsonConvert.DeserializeObject<IDictionary<string, string>>(transaction.AdditionalData);

            var result = await client.Charges.Get(Adddata["id"]);


            return PaypalHelper.CreateVerifyResult(result, _messagesOptions);
        }

        /// <inheritdoc />
        public override Task<IPaymentRefundResult> RefundAsync(InvoiceContext context, Money amount, CancellationToken cancellationToken = default)
        {
            return PaymentRefundResult.Failed("The Refund operation is not supported by this gateway.").ToInterfaceAsync();
        }
        private static PayPalEnvironment GetEnvironment(PaypalGatewayAccount configuration)
        {
            switch (configuration.Environment)
            {
                case PaypalEnvironmentEnum.Sandbox:
                    {
                        return new SandboxEnvironment(configuration.ClientId, configuration.ClientSecret);
                    }
                case PaypalEnvironmentEnum.Live:
                    {
                        return new LiveEnvironment(configuration.ClientId, configuration.ClientSecret);
                    }
                default:
                    {
                        throw new ApplicationException("Unknown PayPal environment");
                    }
            }
        }
    }
}

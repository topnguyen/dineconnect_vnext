﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using DinePay.Gateway.Paypal.Internal;
using DinePay.InvoiceBuilder;
using System;
using DinePay.Abstraction;

namespace DinePay.Gateway.Paypal
{
    public static class PaypalGatewayInvoiceBuilderExtensions
    {
        /// <summary>
        /// The invoice will be sent to Pay.ir gateway.
        /// </summary>
        /// <param name="builder"></param>
        public static IInvoiceBuilder UsePaypal(this IInvoiceBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.SetGateway(PaypalGateway.Name);
        }

        /// <summary>
        /// Sets additional data to the Invoice for <see cref="PaypalGateway"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="data"></param>
        public static IInvoiceBuilder SetPaypalAdditionalData(this IInvoiceBuilder builder, PaypalRequestAdditionalData data)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (data == null) throw new ArgumentNullException(nameof(data));

            return builder.AddOrUpdateProperty(PaypalHelper.RequestAdditionalDataKey, data);
        }

        internal static PaypalRequestAdditionalData GetPaypalAdditionalData(this Invoice invoice)
        {
            if (invoice == null) throw new ArgumentNullException(nameof(invoice));

            if (invoice.Properties.ContainsKey(PaypalHelper.RequestAdditionalDataKey))
            {
                return (PaypalRequestAdditionalData)invoice.Properties[PaypalHelper.RequestAdditionalDataKey];
            }

            return null;
        }
    }
}

﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System;

namespace DinePay.Gateway.Paypal
{
    public static class PaypalVerifyAdditionalDataExtensions
    {
        private const string AdditionalDataKey = "PaypalVerificationAdditionalData";

        public static PaypalVerifyAdditionalData GetPaypalAdditionalData(this IPaymentVerifyResult result)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));

            result.AdditionalData.TryGetValue(AdditionalDataKey, out var additionalData);

            return additionalData as PaypalVerifyAdditionalData;
        }

        internal static void SetPaypalAdditionalData(this IPaymentVerifyResult result, PaypalVerifyAdditionalData additionalData)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));
            if (additionalData == null) throw new ArgumentNullException(nameof(additionalData));

            result.AdditionalData.Add(AdditionalDataKey, additionalData);
        }
    }
}

﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using DinePay.GatewayBuilders;
using System;

namespace DinePay.Gateway.Paypal
{
    public static class PaypalGatewayBuilderExtensions
    {
        /// <summary>
        /// Adds the Pay.ir gateway to DinePay services.
        /// </summary>
        /// <param name="builder"></param>
        public static IGatewayConfigurationBuilder<PaypalGateway> AddPaypal(this IGatewayBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder
                .AddGateway<PaypalGateway>()
                .WithHttpClient(clientBuilder => { })
                .WithOptions(options => { });
        }

        /// <summary>
        /// Configures the accounts for Pay.ir.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureAccounts">Configures the accounts.</param>
        public static IGatewayConfigurationBuilder<PaypalGateway> WithAccounts(
            this IGatewayConfigurationBuilder<PaypalGateway> builder,
            Action<IGatewayAccountBuilder<PaypalGatewayAccount>> configureAccounts)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.WithAccounts(configureAccounts);
        }
        /// <summary>
        /// Configures the options for Paypal Gateway.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureOptions">Configuration</param>
        public static IGatewayConfigurationBuilder<PaypalGateway> WithOptions(
            this IGatewayConfigurationBuilder<PaypalGateway> builder,
            Action<PaypalGatewayOptions> configureOptions)
        {
            builder.Services.Configure(configureOptions);

            return builder;
        }

    }
}

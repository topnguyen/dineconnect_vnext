﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.Extensions.DependencyInjection;
using DinePay.GatewayBuilders;
using System;

namespace DinePay.Gateway.Omise
{
    public static class OmiseGatewayBuilderExtensions
    {
        /// <summary>
        /// Adds the Pay.ir gateway to DinePay services.
        /// </summary>
        /// <param name="builder"></param>
        public static IGatewayConfigurationBuilder<OmiseGateway> AddOmise(this IGatewayBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder
                .AddGateway<OmiseGateway>()
                .WithHttpClient(clientBuilder => { })
                .WithOptions(options => { });
        }

        /// <summary>
        /// Configures the accounts for Pay.ir.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureAccounts">Configures the accounts.</param>
        public static IGatewayConfigurationBuilder<OmiseGateway> WithAccounts(
            this IGatewayConfigurationBuilder<OmiseGateway> builder,
            Action<IGatewayAccountBuilder<OmiseGatewayAccount>> configureAccounts)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.WithAccounts(configureAccounts);
        }
        /// <summary>
        /// Configures the options for Omise Gateway.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="configureOptions">Configuration</param>
        public static IGatewayConfigurationBuilder<OmiseGateway> WithOptions(
            this IGatewayConfigurationBuilder<OmiseGateway> builder,
            Action<OmiseGatewayOptions> configureOptions)
        {
            builder.Services.Configure(configureOptions);

            return builder;
        }

    }
}

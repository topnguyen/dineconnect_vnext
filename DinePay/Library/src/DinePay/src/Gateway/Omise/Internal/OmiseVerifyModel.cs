﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

namespace DinePay.Gateway.Omise.Internal
{
    internal class OmiseVerifyModel
    {
        public string Api { get; set; }

        public string Token { get; set; }
    }
}

﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Omise.Models;
using DinePay.Abstraction;
using DinePay.Http;
using DinePay.Internal;
using DinePay.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DinePay.Gateway.Omise.Internal
{
    internal static class OmiseHelper
    {
        public const string OkResult = "1";
        public static string RequestAdditionalDataKey => nameof(RequestAdditionalDataKey);

        public static CreateChargeRequest CreateRequestData(OmiseGatewayAccount account, Invoice invoice)
        {
      
            var additionalData = invoice.GetOmiseAdditionalData();

            return new CreateChargeRequest
            {
                Amount = invoice.Amount,
                ReturnUri = invoice.CallbackUrl,
                Currency = additionalData.Currency,
                Card = additionalData.OmiseToken
            };
        }

        public static PaymentRequestResult CreateRequestResult(Charge response, HttpContext httpContext, OmiseGatewayAccount account, IDictionary<string, string> databaseAdditionalData = null)
        {

            if (response.Status != ChargeStatus.Successful)
            {
                return PaymentRequestResult.Failed(response.FailureMessage, account.Name);
            }


            return PaymentRequestResult.SucceedWithRedirect(account.Name, httpContext, response.AuthorizeURI, databaseAdditionalData);
        }

        public static async Task<OmiseCallbackResult> CreateCallbackResultAsync(HttpRequest httpRequest, CancellationToken cancellationToken)
        {
            Console.WriteLine(httpRequest.Query.Keys);
            var token = await httpRequest.TryGetParamAsync("paymentToken", cancellationToken).ConfigureAwaitFalse();
            //var status = await httpRequest.TryGetParamAsync("Status", cancellationToken).ConfigureAwaitFalse();

            //var isSucceed = status.Exists && string.Equals(status.Value, OkResult, StringComparison.InvariantCultureIgnoreCase);
            //string message = null;

            //if (!isSucceed)
            //{
            //    message = $"Error {status}";
            //}

            return new OmiseCallbackResult
            {
                Token = token.Value,
                IsSucceed = true,
                Message = ""
            };
        }

        public static OmiseVerifyModel CreateVerifyData(OmiseGatewayAccount account, OmiseCallbackResult callbackResult)
        {
            return new OmiseVerifyModel
            {
                Token = callbackResult.Token
            };
        }

        public static PaymentVerifyResult CreateVerifyResult(Charge response, MessagesOptions messagesOptions)
        {
            PaymentVerifyResult verifyResult;

            if (response.Status == ChargeStatus.Successful)
            {
                verifyResult = PaymentVerifyResult.Succeed(response.Transaction, messagesOptions.PaymentSucceed);
            }
            else
            {
                var message = $"ErrorCode: {response.FailureCode}, ErrorMessage: {response.FailureMessage}";

                verifyResult = PaymentVerifyResult.Failed(message);
            }

            var additionalData = new OmiseVerifyAdditionalData
            {
                CardNumber = response.Card.FirstDigits
            };

            verifyResult.SetOmiseAdditionalData(additionalData);

            return verifyResult;
        }
    }
}

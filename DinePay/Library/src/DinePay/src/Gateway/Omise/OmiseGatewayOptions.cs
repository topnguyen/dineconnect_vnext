﻿namespace DinePay.Gateway.Omise
{
    public class OmiseGatewayOptions
    {
        public string ApiRequestUrl { get; set; } = "https://pay.ir/pg/send";

        public string ApiVerificationUrl { get; set; } = "https://pay.ir/pg/verify";

        public string PaymentPageUrl { get; set; } = "https://pay.ir/pg/";
    }
}

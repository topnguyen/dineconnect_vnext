﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using DinePay.Abstraction;

namespace DinePay.Gateway.Omise
{
    public class OmiseGatewayAccount : GatewayAccount
    {
        public string PublishKey { get; set; }

        public string SecretKey { get; set; }
    }
}

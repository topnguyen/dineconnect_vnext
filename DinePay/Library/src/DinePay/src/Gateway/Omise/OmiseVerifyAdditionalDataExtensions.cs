﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using System;

namespace DinePay.Gateway.Omise
{
    public static class OmiseVerifyAdditionalDataExtensions
    {
        private const string AdditionalDataKey = "OmiseVerificationAdditionalData";

        public static OmiseVerifyAdditionalData GetOmiseAdditionalData(this IPaymentVerifyResult result)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));

            result.AdditionalData.TryGetValue(AdditionalDataKey, out var additionalData);

            return additionalData as OmiseVerifyAdditionalData;
        }

        internal static void SetOmiseAdditionalData(this IPaymentVerifyResult result, OmiseVerifyAdditionalData additionalData)
        {
            if (result == null) throw new ArgumentNullException(nameof(result));
            if (additionalData == null) throw new ArgumentNullException(nameof(additionalData));

            result.AdditionalData.Add(AdditionalDataKey, additionalData);
        }
    }
}

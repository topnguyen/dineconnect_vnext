﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using DinePay.Gateway.Omise.Internal;
using DinePay.InvoiceBuilder;
using System;
using DinePay.Abstraction;

namespace DinePay.Gateway.Omise
{
    public static class OmiseGatewayInvoiceBuilderExtensions
    {
        /// <summary>
        /// The invoice will be sent to Pay.ir gateway.
        /// </summary>
        /// <param name="builder"></param>
        public static IInvoiceBuilder UseOmise(this IInvoiceBuilder builder)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));

            return builder.SetGateway(OmiseGateway.Name);
        }

        /// <summary>
        /// Sets additional data to the Invoice for <see cref="OmiseGateway"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="data"></param>
        public static IInvoiceBuilder SetOmiseAdditionalData(this IInvoiceBuilder builder, OmiseRequestAdditionalData data)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (data == null) throw new ArgumentNullException(nameof(data));

            return builder.AddOrUpdateProperty(OmiseHelper.RequestAdditionalDataKey, data);
        }

        internal static OmiseRequestAdditionalData GetOmiseAdditionalData(this Invoice invoice)
        {
            if (invoice == null) throw new ArgumentNullException(nameof(invoice));

            if (invoice.Properties.ContainsKey(OmiseHelper.RequestAdditionalDataKey))
            {
                return (OmiseRequestAdditionalData)invoice.Properties[OmiseHelper.RequestAdditionalDataKey];
            }

            return null;
        }
    }
}

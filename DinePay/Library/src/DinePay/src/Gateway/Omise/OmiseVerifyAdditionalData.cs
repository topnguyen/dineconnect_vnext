﻿// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

namespace DinePay.Gateway.Omise
{
    public class OmiseVerifyAdditionalData
    {
        public string FactorNumber { get; set; }

        public string Mobile { get; set; }

        public string Description { get; set; }

        public string CardNumber { get; set; }
    }
}

// Copyright (c) DinePay. All rights reserved.
// Licensed under the GNU GENERAL PUBLIC License, Version 3.0. See License.txt in the project root for license information.

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using DinePay.Storage.EntityFrameworkCore.Configuration;
using DinePay.Storage.EntityFrameworkCore.Domain;
using DinePay.Storage.EntityFrameworkCore.Options;

namespace DinePay.Storage.EntityFrameworkCore.Context
{
    public class DinePayDataContext : DbContext
    {
        public DinePayDataContext(DbContextOptions<DinePayDataContext> options, IOptions<EntityFrameworkCoreOptions> efCoreOptions) : base(options)
        {
            EntityFrameworkCoreOptions = efCoreOptions.Value;
        }

        public EntityFrameworkCoreOptions EntityFrameworkCoreOptions { get; }

        public DbSet<PaymentEntity> Payments { get; set; }

        public DbSet<TransactionEntity> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new PaymentConfiguration(EntityFrameworkCoreOptions))
                .ApplyConfiguration(new TransactionConfiguration(EntityFrameworkCoreOptions));
        }
    }
}

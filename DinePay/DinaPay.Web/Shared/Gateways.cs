﻿namespace Parbad.Sample.Shared
{
    public enum Gateways
    {
        Omise,
        Stripe,
        Paypal,
        ParbadVirtual
    }
}

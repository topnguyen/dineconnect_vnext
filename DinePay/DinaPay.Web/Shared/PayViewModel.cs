﻿using System.ComponentModel.DataAnnotations;

namespace Parbad.Sample.Shared
{
    public class PayViewModel
    {
        [Display(Name = "Tracking number")]
        public long TrackingNumber { get; set; }

        [Display(Name = "Generate the Tracking number automatically?")]
        public bool GenerateTrackingNumberAutomatically { get; set; } = true;
        public string RedirectUrl { get; set; }
        public decimal Amount { get; set; }
        public int TenantId { get; set; }
        public int CustomerId { get; set; }
        public int CardId { get; set; }
        [Display(Name = "Gateway")]
        public Gateways SelectedGateway { get; set; } = Gateways.ParbadVirtual;
    }
}

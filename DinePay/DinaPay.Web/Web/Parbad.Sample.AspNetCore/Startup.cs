﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DinePay.Builder;
using DinePay.Gateway.DinePayVirtual;
using DinePay.Gateway.Omise;
using DinePay.Gateway.Stripe;
using System;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Abp.AspNetCore.SignalR.Hubs;
using Abp.AspNetCore;
using Microsoft.Extensions.Hosting;
using Castle.Core.Logging;
using Microsoft.AspNetCore.Mvc;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Web.HealthCheck;
using DinePlan.DineConnect.Identity;
using DinePlan.DineConnect.Web.Public.Startup;
using Castle.Facilities.Logging;
using Abp.Castle.Logging.Log4Net;


namespace DinePay.Sample.AspNetCore
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfigurationRoot _appConfiguration;

        public Startup(IWebHostEnvironment env)
        {
            _hostingEnvironment = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //MVC
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }).AddNewtonsoftJson();

            if (bool.Parse(_appConfiguration["KestrelServer:IsEnabled"]))
            {
                ConfigureKestrel(services);
            }

            services.AddDinePay()
         .ConfigureGateways(gateways =>
         {
             gateways.AddStripe()
             .WithAccounts(accounts =>
             {
                 accounts.AddInMemory(account =>
                 {
                     account.Name = "Stripe";
                     account.SecretKey = "sk_test_4eC39HqLyjWDarjtT1zdp7dc";
                 });
             });

             gateways.AddOmise()
              .WithAccounts(accounts =>
              {
                  accounts.AddInMemory(account =>
                  {
                      account.Name = "Omise";
                      account.SecretKey = "skey_test_5qrp2g268dya2hsdpy5";
                      account.PublishKey = "pkey_test_5qrp2k2pbusalvkckqa";
                  });
              });

             gateways
                 .AddDinePayVirtual()
                 .WithOptions(options => options.GatewayPath = "/MyVirtualGateway");
         })
         .ConfigureHttpContext(builder => builder.UseDefaultAspNetCore())
         .ConfigureStorage(builder => builder.UseMemoryCache());

            IdentityRegistrar.Register(services);
            services.AddSignalR();

            if (bool.Parse(_appConfiguration["HealthChecks:HealthChecksEnabled"]))
            {
                services.AddAbpZeroHealthCheck();
            }

            //Configure Abp and Dependency Injection
            return services.AddAbp<DinePayModule>(options =>
            {
                    //Configure Log4Net logging
                    options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig(_hostingEnvironment.IsDevelopment()
                        ? "log4net.config"
                        : "log4net.Production.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(); //Initializes ABP framework.

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("~/Error?statusCode={0}");
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<AbpCommonHub>("/signalr");

                endpoints.MapControllerRoute("defaultWithArea", "{area}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });


        }

        private void ConfigureKestrel(IServiceCollection services)
        {
            services.Configure<Microsoft.AspNetCore.Server.Kestrel.Core.KestrelServerOptions>(options =>
            {
                options.Listen(new System.Net.IPEndPoint(System.Net.IPAddress.Any, 443),
                    listenOptions =>
                    {
                        var certPassword = _appConfiguration.GetValue<string>("Kestrel:Certificates:Default:Password");
                        var certPath = _appConfiguration.GetValue<string>("Kestrel:Certificates:Default:Path");
                        var cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(certPath, certPassword);
                        listenOptions.UseHttps(new HttpsConnectionAdapterOptions()
                        {
                            ServerCertificate = cert
                        });
                    });
            });
        }
    }
}




﻿using DinePlan.DineConnect.Web.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace DinePay.Sample.AspNetCore.Controllers
{
    public class HomeController : DineConnectControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}

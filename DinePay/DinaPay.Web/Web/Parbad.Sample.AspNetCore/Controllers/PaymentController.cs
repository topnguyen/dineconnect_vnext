﻿using Microsoft.AspNetCore.Mvc;
using DinePay.AspNetCore;
using DinePay.Gateway.Omise;
using DinePay.Gateway.Stripe;
using Parbad.Sample.Shared;
using System.Threading.Tasks;
using DinePay.Gateway.Paypal;
using System;
using DinePlan.DineConnect.Web.Controllers;
using DinePlan.DineConnect.Core.Swipe;
using Abp.Domain.Repositories;
using System.Linq;
using Abp.Domain.Uow;
using System.Collections.Generic;

namespace DinePay.Sample.AspNetCore.Controllers
{
    public class PaymentController : DineConnectControllerBase
    {
        private readonly IOnlinePayment _onlinePayment;
        private readonly IRepository<SwipeCustomerCard> _swipeCustomerCardRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public PaymentController(IUnitOfWorkManager unitOfWorkManager,IOnlinePayment onlinePayment, IRepository<SwipeCustomerCard> swipeCustomerCardRepository)
        {
            _onlinePayment = onlinePayment;
            _unitOfWorkManager = unitOfWorkManager;
            _swipeCustomerCardRepository = swipeCustomerCardRepository;
        }

        [HttpGet]
        public IActionResult Pay()
        {
            return View(new PayViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Pay(PayViewModel viewModel)
        {
            // set customer and card for testing and return url 
            viewModel.CustomerId = 5;
            viewModel.TenantId = 25;
            viewModel.CardId = 3;
            viewModel.RedirectUrl = Url.Action("Verify", "Payment", null, Request.Scheme);


            List<SwipeCustomerCard> getCustomerCard = new List<SwipeCustomerCard>();
            using (_unitOfWorkManager.Current.SetTenantId(viewModel.TenantId))
            {
                getCustomerCard = _swipeCustomerCardRepository.GetAll().Where(x => x.CustomerId == viewModel.CustomerId && x.CardRefId == viewModel.CardId && x.IsDeleted == false && x.Active == true).ToList();
            }


            if (getCustomerCard != null && getCustomerCard.Count() > 0 && getCustomerCard[0].ExpiryDate >= DateTime.Now.Date)
            {
                var result = await _onlinePayment.RequestAsync(invoice =>
                {
                    // we will take the token of card from front end 
                    if (viewModel.SelectedGateway == Gateways.Omise)
                    {
                        invoice
                        .SetAmount(Convert.ToInt64(viewModel.Amount * 100))
                        .SetCallbackUrl(viewModel.RedirectUrl)
                        .SetTenantId(viewModel.TenantId)
                        .AddProperty("RequestAdditionalDataKey", new OmiseRequestAdditionalData()
                        { Currency = "SGD", OmiseToken = "tokn_test_5qumw7nbecjbo8gx27t" })
                        .SetGateway(viewModel.SelectedGateway.ToString());

                    }
                    else if (viewModel.SelectedGateway == Gateways.Stripe)
                    {
                        // example for stripe
                        invoice
                                .SetAmount(viewModel.Amount)
                                .SetCallbackUrl(viewModel.RedirectUrl)
                                .SetTenantId(viewModel.TenantId)
                                .AddProperty("RequestAdditionalDataKey", new StripeRequestAdditionalData()
                                { Currency = "SGD", StripeToken = "tok_1KRMRG2eZvKYlo2CqhMyE06H" })
                                .SetGateway(viewModel.SelectedGateway.ToString());
                    }
                    else if (viewModel.SelectedGateway == Gateways.Paypal)
                    {
                        // example for paypal
                        invoice
                                .SetAmount(viewModel.Amount)
                                .SetCallbackUrl(viewModel.RedirectUrl)
                                .SetTenantId(viewModel.TenantId)
                                .AddProperty("RequestAdditionalDataKey", new PaypalRequestAdditionalData()
                                { OrderId = "orderid" })
                                .SetGateway(viewModel.SelectedGateway.ToString());
                    }

                    if (viewModel.GenerateTrackingNumberAutomatically)
                    {
                        invoice.UseAutoIncrementTrackingNumber();
                    }
                    else
                    {
                        invoice.SetTrackingNumber(viewModel.TrackingNumber);
                    }
                });

                // Save the result.TrackingNumber in your database.

                if (result.IsSucceed)
                {
                    return result.GatewayTransporter.TransportToGateway();
                }

                return View("PayRequestError", result);
            }
            else
            {
                throw new InvalidOperationException("Customer card not valid!");
            }
        }

        [HttpGet, HttpPost]
        public async Task<IActionResult> Verify()
        {
            var invoice = await _onlinePayment.FetchAsync();

            // Check if the invoice is new or it's already processed before.
            if (invoice.Status != PaymentFetchResultStatus.ReadyForVerifying)
            {
                // You can also see if the invoice is already verified before.
                var isAlreadyVerified = invoice.IsAlreadyVerified;
                return Content("The payment was not successful.");
            }

            // This is an example of cancelling an invoice when you think that the payment process must be stopped.
            if (!Is_There_Still_Product_In_Shop(invoice.TrackingNumber))
            {
                var cancelResult = await _onlinePayment.CancelAsync(invoice, cancellationReason: "Sorry, We have no more products to sell.");
                return View("CancelResult", cancelResult);
            }

            var verifyResult = await _onlinePayment.VerifyAsync(invoice);

            return View(verifyResult);
        }

        [HttpGet]
        public IActionResult Refund()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Refund(RefundViewModel viewModel)
        {
            var result = await _onlinePayment.RefundCompletelyAsync(viewModel.TrackingNumber);

            return View("RefundResult", result);
        }

        private static bool Is_There_Still_Product_In_Shop(long trackingNumber)
        {
            // Yes, we still have products :)

            return true;
        }
    }
}
